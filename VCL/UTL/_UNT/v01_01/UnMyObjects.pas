//{$I dmk.inc}
unit UnMyObjects;

{
063 - Multiplas Sele��es em DBGrid

var  contador: Integer;
 begin
  With Dbgrid1 do
  Begin
 for contador:= 0 to Pred(SelectedRows.Count) do
  Begin
  {posiciona nos registros selecionados do DBGrid
  Dataset.Bookmark:= SelectedRows[contador];
  end;
  end;
}
interface

uses
  Vcl.Themes, Vcl.Styles,
  mySQLDbTables, mySQLDirectQuery,
  // frxReport
{$IFNDef NAO_USA_FRX sFrx}
  frxClass, frxPreview, frxRich, frxExportPDF,
{$EndIf}
  mySQLExceptions,
  // TMS Software
  {$IfDef cAdvGrid}
  AdvGrid, DBAdvGrid,
  {$IfDef cAdvToolx} //Berlin
  {$EndIf} //Berlin
  AdvToolBar, AdvGlowButton, AdvToolBarStylers,
  // AdvToolBarOfficeStyler
  {$EndIf} //Berlin
  SHDocVw,
  // Indy
  IdIcmpClient,
  {$IfDef UsaNFe}
  // Certificado digital
  CAPICOM_TLB, //msxmldom, MSXML2_TLB,
  {$EndIf}
    ////////////////////////////////////// NORMAIS ///////////////////////////////
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Vcl.ExtDlgs, Menus, UnMsgInt, UnInternalConsts,
  Db, DbCtrls,
  UnInternalConsts2, ComCtrls, Registry, TypInfo,
  DmkCompoEnums, dmkEdit, dmkEditCB, dmkGeral, dmkDBGridZTO,
  dmkEditDateTimePicker, dmkDBLookupCombobox, dmkCheckBox, dmkRadioGroup,
  dmkDBEdit, dmkValUsu, dmkCheckGroup, dmkMemo, dmkDBGrid, Variants, ScktComp,
  dmkDBGridDAC, IdStack, InvokeRegistry, Grids,
  Buttons, DBGrids, ComObj, Mask, MaskUtils, dmkCompoStore,
  {$IFNDEF NO_USE_MYSQLMODULE} Module, MyDBCheck,{$ENDIF} dmkImage,
  dmkPermissoes, UnDmkEnums(*, RSSVGCtrls*),
  dmkPageControl, UnGrl_Vars, Math, MyListas, UnProjGroup_Consts,
  dmkWinMessages;

type
  TShemeColor=(csText,csTitleTextActive,csTitleTextNoActive,
         csButtonFace,csButtonText,
         csButtonHilight,csButtonlight,csButtonShadow,csButtonDkshadow,
         csSelectText,csSelectBg,csHilightText,csHilight,
         csMenuBar,csMenuBarText,csMenuText,csMenubg,csCaption,
         csScrollbar,csTextDisable);
  TShemeColors=array[csText..csTextDisable] of Tcolor;

  THackDBGrid = class(TCustomDBGrid);
  THackStringGrid = class(TStringGrid);
  TArrForms =  array of TForm;
  TMyArrArrStr = array of array of Variant;
  TContador = class(TThread)
  private
    FLabel1, FLabel2: TLabel;
    FAguarde: Boolean;
    FTexto: String;
    FMilisegundos: Integer;
  protected
    procedure Execute; override; // M�todo anulado
  public
    constructor Create(const LaAviso1, LaAviso2: TLabel;
                Aguarde: Boolean; Texto: String; Milisegundos: Integer);
    destructor Destroy; override;

 end;

 type
  HackWinControl = class(TWinControl);
  TPriorityPath = (ppAtual_Edit, ppInfo_Ini);
  TTreeViewImagesSet = (cTreeViewError,           // 0   erro do treeview
                        cCBMay, cCBNot, cCBYes,   // 1..3
                        cRBMay, cRBNot, cRBYes);  // 4..6

  //TArrInt = array of Integer;
  TUnMyObjects = class(TObject)
  private
    { Private declarations }
    procedure DoRestore(Sender: TObject);
    procedure MenuItemStyleClick(Sender: TObject);
    // T T D I
    function  FormTDINovaAba(PageControl: TPageControl): TTabSheet;
    // GERAL
{$IFNDEF NAO_USA_DB}
    procedure DefParams(Acao: Integer);
{$ENDIF}
  public
    { Public declarations }
    //  G E R A L
    //procedure ShowHint(Sender: TObject);
    function  ComponentePertenceAoForm(Componente: TComponent; FormClass:
              TFormClass): Boolean;
    procedure SelecionaLimpaImagemdefundoFormDescanso(FormPrincipal: TForm;
              Classe: TFormClass; PageControl: TPageControl; Limpar: Boolean = False);
    procedure MostraErro(Sender: TObject; E: Exception);
    function  ErroDeSocket(Errno: word; ErrMsg: String): String;
    procedure ExecutaCmd(cmd: String; MeResult: TMemo);
    function  CompactaArquivo(Arquivo, PastaOri, PastaDest: String; Memo: TMemo;
              ExcluiArqOri: Boolean; PastaDoZip: String): Boolean;
    function  IgnoraFIC(InfoCompoInesperado: Boolean; Compo: TWinControl;
              Mensagem: String): Boolean;
    function  FIC(FaltaInfoCompo: Boolean; Compo: TComponent;
              Mensagem: String; ExibeMsg: Boolean = True): Boolean;
    function  FIC_Edit_Int(const Edit: TdmkEdit; const Mensagem: String;
              var Valor: Integer): Boolean;
    function  FIC_Edit_Flu(const Edit: TdmkEdit; const Mensagem: String;
              var Valor: Double): Boolean;
    function  FIC_MCI(CkContinuar: TCheckBox; RGModoContinuarInserindo:
              TRadioGroup): Boolean;
    function  FIC_ArquivoNaoExiste(var Dir: String; const Arq: String;
              var Caminho: String): Boolean;
{$IFNDEF NAO_USA_SEL_RADIO_GROUP}
    function  SelRadioGroup(CaptionFm, CaptionRG: String; Itens: array of String;
              Colunas: Integer; Default: Integer = -1;
              SelOnClick: Boolean = False): Integer;
    function  Sel2RadioGroups(const Altura: Integer; const CaptionFm,
              CaptionRG1, CaptionRG2: String; const
              ItensRG1, ItensRG2: array of String; const ColunasRG1,
              ColunasRG2: Integer; const DefaultRG1, DefaultRG2: Integer;
              const SelOnClick: Boolean; var Result1, Result2: Integer):
              Boolean;
    function  Sel3RadioGroups(const Altura: Integer; const CaptionFm,
              CaptionRG1, CaptionRG2, CaptionRG3: String; const
              ItensRG1, ItensRG2, ItensRG3: array of String; const ColunasRG1,
              ColunasRG2, ColunasRG3: Integer; const DefaultRG1, DefaultRG2,
              DefaultRG3: Integer; const SelOnClick: Boolean; var Result1,
              Result2, Result3: Integer): Boolean;
    function  SelRadioGroup2(const CaptionFm, CaptionRG: String;
              const Itens: array of String; const Avisos: array of String;
              const Acoes: array of TSQLType; const Valores: array of String;
              const MyCods: array of Integer;
              const Colunas: Integer; var Valor: String;
              var MyOcorr: Integer; var SQLType: TSQLType;
              const DefaultItemIndex: Integer = -1): Boolean;
    function  SelCheckGroup(CaptionFm, CaptionCG: String; Itens: array of String;
              Colunas: Integer; Default: Integer = -1): Integer;
{$ENDIF}
    procedure ControleStrings(ScreenActiveForm: TForm);
    procedure EditCB_ValUnico(ScreenActiveForm: TForm);
    function  CompoMoeda(Caption: String): String;
    procedure ControleCor(ScreenActiveForm: TForm);
//{$IFNDEF NAO_USA_GLYFS}
    function  CorIniComponente(Handle: THandle = 0): Boolean;
//{$ENDIF}
    //function  CompoMinusculo(Nome: String): Boolean;
    function  ArquivoExiste(Caminho: String; MemoOut: TMemo; DoLocalize:
              Boolean = True): Boolean;
    function  GetAllFiles(Subdiretorios: Boolean; Mask: string;
              ListBox: TCustomListBox; ClearListBox: Boolean;
              LaAviso1, LaAviso2: TLabel): Integer;
    procedure ImportaConfigWeb(Form: TForm; EdDescricao, EdHost, EdDB, EdPorta,
              EdLogin, EdSenha: TdmkEdit);
    procedure LoadXML(MyMemo: TMemo; MyWebBrowser: TWebBrowser;
              PageControl: TPageControl; ActivePageIndex: Integer);

    function  CalculaCanvasTextWidth(Texto, FontName: String; FontSize:
              Integer): Integer;
    // R T T I
    function  ObtemObjeto(const AObject: TObject; const Nome: String; const
              Classe: TClass; var Objeto: TObject): Boolean;
    function  SVP_O(Compo: TObject; Propriede: String; Valor: Variant): Boolean;
    function  SVP_Objeto(const AObject: TObject; const Nome: String; const
              Classe: TClass; const Propriedade: String; const Valor: Variant):
              Boolean;
    procedure SVP_Primit(Compo: TComponent; Propriedade: String; Valor: Variant);
    function  ComponenteTemAPropriedade(const Compo: TComponent; const
              NomeProp: String): Boolean;
    // T C O N T R O L
    // nada
    // T W I N C O N T R O L
    function  WinControlSetFocus(WinCtrlName: String): Boolean;
    procedure DefineTextoEWidth(Compo: TPersistent; Texto, FontName: String;
              FontSize: Integer);
    // nada
    // S T R I N G   G R I D
    procedure LarguraAutomaticaGrade(AGrid: TStringGrid);  // AjustaLargurasColunas
    function  InsereLinhaStringGrid(Grade: TStringGrid; Pergunta: Boolean = True): Boolean; // InsereLinha
    function  ExcluiLinhaStringGrid(Grade: TStringGrid; Pergunta: Boolean = True): Boolean; // ExcluiLinha
    function  Xls_To_StringGrid(AGrid: TStringGrid; AXLSFile: String;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
              Planilha: Word = 0; MaxLin: Integer = 0): Boolean;
    function  Xls_To_StringGrid_Faster(AGrid: TStringGrid; AXLSFile, ASheetName:
              String; PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
              MeErros: TMemo; Planilha: Word = 1; MaxLin: Integer = 0): Boolean;
    function  Xls_To_Array(var MyArrStr: TMyArrArrStr; const AXLSFile: String;
              const PB1: TProgressBar; const LaAviso1, LaAviso2: TLabel;
              const Planilha: Word = 0): Boolean;
    function  Xls_To_StringGrid_OnlyOpen(AGrid: TStringGrid; AXLSFile: String;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
              var XLApp: OLEVariant): Boolean;
    function  Xls_To_StringGrid_OnlyProcess(AGrid: TStringGrid; AXLSFile: String;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
              var XLApp: OLEVariant): Boolean;
    function  Xls_To_StringGrid_OnlyClose(AGrid: TStringGrid; AXLSFile: String;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
              var XLApp: OLEVariant): Boolean;
    function  StringGrid_to_CSV(AGrid: TStringGrid; ACSVFile: String;
              Exclui: TYesNoAsk; InfoMsg: Boolean; PB1: TProgressBar;
              LaAviso1, LaAviso2: TLabel): Boolean;
    function  StringGrid_to_Xls(AGrid: TStringGrid; AXLSFile: String;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel): Boolean;
    procedure PulaCelulaGradeInput(GradePula, GradeCodi: TStringGrid;
              var Key: Word; Shift: TShiftState);  // XLS - EXCEL
    procedure PulaCelulaGradeInputEx(GradePula, GradeCodi: TStringGrid;
              var Key: Word; Shift: TShiftState; BtOK: TBitBtn);
    procedure DesenhaTextoEmStringGrid(Grade: TStringGrid; Rect: TRect;
              CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String;
              FixedCols, FixedRows: Integer; DesenhaSelecionado: Boolean);
    procedure DesenhaTextoEmStringGridEx(Grade: TStringGrid; Rect: TRect;
              CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String;
              FixedCols, FixedRows: Integer; DesenhaSelecionado: Boolean;
              FonteNome: String; FonteSize: Integer; FonteStyle: TFontStyles;
              PasswordChar: Char);
    procedure SalvaStringGridToFile(Grade: TStringGrid; Arq: String;
              Pergunta: Boolean = False);
    function  LoadStringGridFromFile(Grade: TStringGrid; Arq: String): Boolean;
    // DESENHOS EM GRADE
    //  Rota��o de texto
    procedure StringGridRotateTextOut(Grid: TStringGrid;
              ARow, ACol: Integer; Rect: TRect; NomeFonte: string;
              Size: Integer; Color: TColor; Alignment: TAlignment);
    procedure StringGridRotateTextOut2(Grid: TStringGrid; ARow, ACol:
              Integer; Rect: TRect; NomeFonte: String; Size: Integer;
              Color: TColor; Alignment:TAlignment);
    // Grade inteira de n�meros
{$IFNDEF NAO_USA_GLYFS}
    procedure DesenhaGradeNumeros(Grade: TStringGrid; ACol, ARow: Integer;
              Rect: TRect; State: TGridDrawState; AllColorFixo: Boolean);
    // Grades em geral
    procedure DesenhaGradeGeral(Grade: TStringGrid; ACol, ARow: Integer;
              Rect: TRect; State: TGridDrawState; Alinhamentos: array of TAlignment;
              AllColorFixo: Boolean);
    //  Grade de ativa��o
    procedure DesenhaGradeA(GradeA: TStringGrid; ACol, ARow: Integer;
              Rect: TRect; State: TGridDrawState; AllColorFixo: Boolean);
    procedure DesenhaGradeN(GradeDesenhar, GradeAtivos, GradeCompara:
              TStringGrid; ACol, ARow: Integer;Rect: TRect; State:
              TGridDrawState; Fmt: String;
              FixedCols, FixedRows: Integer; PermiteVazio: Boolean);
{$ENDIF}
    procedure LimpaGrade(MinhaGrade: TStringGrid; ColIni, RowIni:
              Integer; ExcluiLinhas: Boolean);
    procedure LimpaGrades(Grades: array of TStringGrid; ColIni, RowIni:
              Integer; ExcluiLinhas: Boolean);
    procedure ConfiguracaoRateioContas(DatabaseName: String;
              Lista: TStrings; Grade: TStringGrid);
    function  CriaSelListaArray(Lista: array of String; TituloForm: String;
              TitulosColunas: array of String; ScreenWidth: Integer;
              dmkEdit: TdmkEdit): Boolean;
    // D B G R I D
    procedure AjustaTamanhoMaximoColunaDBGrid(Grid: Vcl.DBGrids.TDBGrid; Campo: String;
              MinLarg: Integer);
    procedure DesenhaTextoEmDBGrid(Grade: Vcl.DBGrids.TDBGrid; Rect: TRect;
              CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String;
              Negrito: Integer = -1; CorPen: TColor = clSilver);
    procedure DBGridLarguraSegundoTitulo(DBGTitul, DBGDados: Vcl.DBGrids.TDBGrid;
              Colunas: array of Integer);
    procedure DefineTituloDBGrid(DBGrid: Vcl.DBGrids.TDBGrid;
              FieldName, Titulo: String);
    procedure DefineCorTextoSitLancto(DBGrid: Vcl.DBGrids.TDBGrid; Rect: TRect; FieldName,
              TextoSit: String);
    procedure DBGridSelectAll(AGrid: Vcl.DBGrids.TDBGrid);
    procedure SelecionarLinhasNoDBGrid(AGrid: Vcl.DBGrids.TDBGrid; Selecionar: Boolean);
    procedure ColoreSimNaoEmDBGrid(const DBGrid: Vcl.DBGrids.TDBGrid;
              const Rect: TRect; DataCol: Integer; Column: TColumn; State:
              TGridDrawState; const NomeCampo, SimNao: String);
    function  SetaItensBookmark(Form: TForm; AGrid: Vcl.DBGrids.TDBGrid; Campo: String;
              ArrXtn: array of Extended; DesmarcarOutros: Boolean): Boolean;
    procedure SetaItensBookmarkAtivos(Form: TForm; AGrid: Vcl.DBGrids.TDBGrid; FldAtivo:
              String);
    procedure SetaTodosItensBookmark(Form: TForm; DBG: Vcl.DBGrids.TDBGrid; Ativa: Boolean);
    procedure GetSelecionadosBookmark_Int1(Form: TForm; DBG: Vcl.DBGrids.TDBGrid; CampoInt1:
              String; Selecionados: TArrSelIdxInt1);
    procedure GetSelecionadosBookmark_Int2(Form: TForm; DBG: Vcl.DBGrids.TDBGrid;
              CampoInt0, CampoInt1: String; var Selecionados: TArrSelIdxInt2);
    function  GetSelecionadosBookmark_Int1_ToStr(Form: TForm; DBG: Vcl.DBGrids.TDBGrid;
              CampoInt1: String): String;
    // Corda Lista
    function  CordaDeZTOChecks(DBGridZTO: TdmkDBGridZTO; Query:
              TmySQLQuery; Campo: String; PermiteTodos: Boolean): String;
    function  CordaDeDBGridSelectedRows(DBGrid: Vcl.DBGrids.TDBGrid; Query:
              TmySQLQuery; Campo: String; PermiteTodos: Boolean): String;
    function  CordaDeQuery(Query: TmySQLQuery; Campo: String; DefRes: String = ''):
              String;
    function  CordaDeDirectQuery(Query: TmySQLDirectQuery; Campo: String): String;
    function  CordaDeArrayInt(Lista: array of Integer): String;
    function  CordaDeSQL_IN(Lista: array of String): String;
    function  CordaDeIntInConjunto(Conjunto: Integer; Valores:
              array of Integer): String;
    procedure CordaDeQuery_Mul(const Query: TmySQLQuery; const CampoFiltro,
              CampoValor: String; const ValoresIdx: Array of Integer; const
              DefRes: Array of String; var Cordas: Array of String; AvisaOrfaos:
              Boolean);
    function  CordaDeQuery_Mul_SelcionaItem(Cordas: Array of String; Item: Integer): String;
    function  ProximoRegistroBookMark(DBGrid: Vcl.DBGrids.TDBGrid; Indice: Integer): Boolean;
    function  CelulaDBGrid_PosDownRight(const Grade: TDBGrid; const MoreWid,
              MoreTop: Integer; const CorrigePosicao: Boolean; var Lf, Tp:
              Integer): Boolean;

    // O P E N    D I A L O G
    function FileOpenDialog(Form: TForm; const IniDir, Arquivo, Titulo, Filtro:
         String; Opcoes: TOpenOptions; var Resultado: String): Boolean;
    function  DefineDiretorio(Form: TForm; dmkEdit: TdmkEdit): String;
    function  DefineArquivo1(Form: TForm; dmkEdit: TdmkEdit): Boolean;
    function  DefineArquivo2(Form: TForm; dmkEdit: TdmkEdit; Pasta, Arquivo:
              String; Prioridade: TPriorityPath; SoArquivo: Boolean = False):
              Boolean;
    function  DefineArquivo3(Form: TForm; dmkEdit: TdmkEdit): String;
    function  DefineArquivoImagem(var Arquivo: String; const Filter: String): Boolean;
    // C O M B O    B O X
    function  PreencheCBAnoECBMes(CBAno, CBMes: TComboBox;
              IncremMes: Integer = 1; Nulo: Boolean = False): String;
    function  PreencheCBAnoECBMes2(CBAno, CBMesI, CBMesF: TComboBox;
              IncremMesI: Integer = 1; IncremMesF: Integer = 1;
              Nulo: Boolean = False): String;
    function  CBAnoECBMesToPeriodoTxt(CBAno, CBMes: TComboBox): String;
    function  CBAnoECBMesToDate(CBAno, CBMes: TComboBox;
              Dia, IncMes: Integer): TDateTime;
    function  DefineIndexComboBox(CBx: TComboBox; Valor: String): Boolean;
    function  SetaPeriodoDeCBANoMes(CBAno, CBMes: TComboBox): Integer;
    procedure CBAnoMesFromPeriodo(Periodo: Integer; CBAno, CBMes: TComboBox;
              Avisa: Boolean);
    // E D I T
    procedure UperCaseComponente();
    procedure SetTextAndGoEnd(EdTxt: TdmkEdit; Texto: String);
    procedure IncrementaNumeroEditTxt(Sender: TdmkEdit; var Key: Word;
              ShiftState: TShiftState; OutFormat: TAllFormat);
    // L A B E L  (+ ProgressBar)
    procedure Informa(Label1: TLabel; Aguarde: Boolean; Texto: String);
    procedure InformaEUpdPB(PB: TProgressBar; Label1: TLabel;
              Aguarde: Boolean; Texto: String);
    function  Informa2(Label1, Label2: TLabel; Aguarde: Boolean; Texto: String;
              IsThread: Boolean = False): Boolean;
    function  Informa2VAR_LABELs(Texto: String): Boolean;
    procedure InformaN(Labels: array of TLabel; Aguarde: Boolean; Texto: String);
    procedure Informa2EUpdPB(PB: TProgressBar; Label1, Label2: TLabel;
              Aguarde: Boolean; Texto: String);
    procedure UpdPB(PB: TProgressBar; Label1, Label2: TLabel);
    procedure UpdPBOnly(PB: TProgressBar);
    // B I T M A P
    procedure CarregaImagemEmTImage(Image: TImage; Arquivo: String;
              Bitmap: TBitmap; BrushColor: TColor);
    procedure DisplayBitmap(const Bitmap: TBitmap; const Image: TImage;
              BrushColor: TColor);
    procedure CarregaBitmapBranco(Image: TImage; H, W: Integer);
    // T R A D I O G R O U P
    procedure ConfiguraRadioGroup(RadioGroup: TRadioGroup; Itens:
              array of String; Colunas, Default: Integer);
    procedure ConfiguraDBRadioGroup(DBRadioGroup: TDBRadioGroup; Itens:
              array of String; Colunas: Integer);
    procedure ConfiguraCheckGroup(CheckGroup: TdmkCheckGroup; Itens:
              array of String; Colunas, Default: Integer; FirstAll: Boolean);
    function  TextosDeItensSelecionadosDeCheckGroup(CheckGroup: TdmkCheckGroup): String;
    procedure ConfiguraTipoCobranca(RGTipoCobranca: TRadioGroup; Colunas, Default: Integer);
    procedure ConfiguraAllFmtStr(RGAllFmtStr: TRadioGroup; Colunas, Default: Integer);
    procedure SetaRGCNAB(RGCNAB: TRadioGroup; CNAB: Integer);
    function  PreencheComponente(Componente: TComponent;
              MyArray: array of String; Colunas: Integer = 0): Boolean;
    function  NaoPermiteItemRadioGroup(RG: TRadioGroup; Permitidos:
              array of Integer; AvisoNaoPermitido: String): Boolean;
    function  IncrementaFolhaLinha(EdFolha, EdLinha: TdmkEdit; MaxLin: Integer):
              Boolean;
    // T A D V T O O L B A R P A G E R
    procedure CopiaItensDeMenu(PMGeral: TPopupMenu; Form: TForm);
    //procedure MaximizaAdvToolBarPager usar MaximizaPageMenu
    procedure MaximizaPageMenu(Pager: TWinControl;
              Maximiza: TDefTruOrFalse);
    procedure StylesListRefreshMenu(PMStls: TPopupMenu);
    function  StyleServiceObtemCor(CorSistema: TColor): TColor;

    //procedure MaximizaPageControl(PageControl: TPageControl; Maximiza: TDefTruOrFalse);

    // T P A G E C O N T R O L
    procedure DefinePageControlActivePageByName(PageControl: TPageControl;
              Nome: String; DefaultPageIndex: Integer);

    // F O R M S
    procedure Entitula(GroupBox: TGroupBox;
              Titulos, Avisos: array of TLabel; Titulo: String;
              RetiraIDForm: Boolean; Alignment: TAlignment;
              Left, Top, FontSize: Integer);
    function  LiberaPelaSenhaBoss(InstanceClass: TComponentClass;
              var Reference; Aviso: String = ''; SenhaExtra: String = ''): Boolean;
    function  CriaForm_AcessoTotal(InstanceClass: TComponentClass; var Reference): Boolean;
    function  FormShow(InstanceClass: TComponentClass; var Reference): Boolean;
    function  FormCria(InstanceClass: TComponentClass; var Reference): Boolean;
    function  Senha(ComponentClass: TComponentClass;
              Reference: TComponent; Tipo: Integer; LaVlr, EdVlr, LaPercent, EdPercent,
              VlrBase: String; Login: Boolean; LaData: String; Data: TDateTime;
              Janela: String; InfoCaption: String=''): Integer;
    function  LocalizaFormsIguais(InstanceClass: TComponentClass): TArrForms;
    function  ChangeValorDmk(ComponentClass: TComponentClass; Reference:
              TComponent; FormatType: TAllFormat; DefaultAtu, DefaultNew: Variant;
              CasasAtu, CasasNew, LeftZerosAtu, LeftZerosNew: Integer; ValMinAtu,
              ValMaxAtu, ValMinNew, ValMaxNew: String; Obrigatorio: Boolean;
              FormCaption, ValCaptionAtu, ValCaptionNew: String;
              WidthVal: Integer; var ResultadoAtu: Variant; var ResultadoNew:
              Variant): Boolean;
    function  GetValorDmk(ComponentClass: TComponentClass; Reference:
              TComponent; FormatType: TAllFormat; Default: Variant;
              Casas, LeftZeros: Integer; ValMin, ValMax: String;
              Obrigatorio: Boolean; FormCaption, ValCaption: String;
              WidthVal: Integer; var Resultado: Variant): Boolean;
    function  GetMinMaxDmk(ComponentClass: TComponentClass; Reference:
              TComponent; const FormatType: TAllFormat; const DefaultMin, DefaultMax:
              Variant; const Casas, LeftZeros: Integer; const ValMinMin, ValMaxMin,
              ValMinMax, ValMaxMax: String; const Obrigatorio: Boolean; const FormCaption,
              ValMinCaption, ValMaxCaption: String; const WidthVal: Integer; var
              ResultadoMin, ResultadoMax: Variant): Boolean;
    function  VerificaSeTabFormExiste(PageControl: TPageControl;
              Classe: TFormClass): Integer;
    function  GetValorEDataDmk(ComponentClass: TComponentClass; Reference:
              TComponent; FormatType: TAllFormat; Default: Variant;
              Casas, LeftZeros: Integer; ValMin, ValMax: String;
              Obrigatorio: Boolean; FormCaption, ValCaption: String;
              WidthVal: Integer; DateCaption: String;
              HabilitaEdit, HabilitaDate: Boolean;
              var Resultado: Variant; var Data: TDateTime): Boolean;
    function  GetTexto(ComponentClass: TComponentClass; Reference:
              TComponent; const FormCaption, ValCaption: String; var Texto:
              String): Boolean;
    function  GetCodigoDeBarras(ComponentClass: TComponentClass; Reference:
              TComponent; const FormCaption, ValCaption: String; var Texto:
              String): Boolean;
    procedure EscolhePeriodo_MesEAno(ComponentClass: TComponentClass;
              Reference: TComponent; var Mes, Ano: Word; var Cancelou: Boolean;
              MostraMes, MostraAno: Boolean);
    {$IfNDef sGetData}
    function  ObtemData(const DtDefault: TDateTime; var DtSelect:
              TDateTime; const MinData: TDateTime; const HrDefault: TTime = 0;
              const HabilitaHora: Boolean = False; Titulo: String = '';
              const InfoDetalhado: String = ''): Boolean;
    function  ObtemDatasPeriodo(const DtDefaultIni, DtDefaultFim: TDateTime;
              var SelectIni: TDateTime; var SelectFim: TDateTime;
              const HabilitaHora: Boolean = False; const HrDefaultIni: TTime = 0;
              const HrDefaultFim: TTime = 1 - (1/24/60/60); Titulo: String = '';
              const InfoDetalhado: String = ''): Boolean;
    {$EndIf}
//{$IFNDEF NAO_USA_DB}
    procedure FormMsg(var Msg: TMsg; var Handled: Boolean);
//{$ENDIF}
    function MeuwParamEnter(Controle : TWinControl) : Integer;
    function MeuwParamPonto(Controle : TWinControl) : Integer;

    // T R E E V I E W E V
    procedure ToggleTreeViewCheckBoxes_Unico(Node: TTreeNode);
    function  ToggleTreeViewCheckBoxes_Child(Node: TTreeNode): Integer;
    //procedure ExpandTree(Tree: TTreeView; Level: Integer);

    // T M E M O
    function  ExportaMemoToFile(Memo: TMemo; Arquivo: String;
              Exclui, EscreveLinhasVazias, SemAcentos: Boolean): Boolean;
    function  ExportaMemoToFileExt(Memo: TMemo; Arquivo: String;
              Exclui, EscreveLinhasVazias: Boolean; SemAcentos: TSemAcentos; CrLf: Integer;
              // 2012-05-01
              FileEnd: Variant): Boolean;
    procedure MeDmk_Aviso(Mensagem: String);

    // M E N U    I T E M
    procedure HabilitaMenuItemCabDel(MenuItem: TMenuItem; QrCab, QrIts: TDataSet);
    procedure HabilitaMenuItemCabDelC1I2(MenuItem: TMenuItem; QrCab,
              QrIts1, QrIts2: TDataSet);
    procedure HabilitaMenuItemCabDelC1I3(MenuItem: TMenuItem; QrCab,
              QrIts1, QrIts2, QrIts3: TDataSet);
    procedure HabilitaMenuItemCabDelC1I4(MenuItem: TMenuItem; QrCab,
              QrIts1, QrIts2, QrIts3, QrIts4: TDataSet);
    procedure HabilitaMenuItemCabUpd(MenuItem: TMenuItem; QrCab: TDataSet);
    procedure HabilitaMenuItemItsIns(MenuItem: TMenuItem; QrCab: TDataSet; Condicao: Boolean = True);
    procedure HabilitaMenuItemItsDel(MenuItem: TMenuItem; QrIts: TDataSet; Condicao: Boolean = True);
    procedure HabilitaMenuItemItsUpd(MenuItem: TMenuItem; QrIts: TDataSet; Condicao: Boolean = True);
    procedure HabilitaBtnToPopupMenuIts(Botao: TBitBtn; PodeInsUpd: Boolean;
              QrIts: TDataSet);
    // P O P   U P
    procedure MostraPopOnControlXY(PM:TPopUpMenu; ComponentePai:TControl;
              PosX, PosY: Integer);
    procedure MostraPopUpDeBotao(PM:TPopUpMenu; ComponentePai:TControl;
              ComponenteFilho: TControl = nil);
    procedure MostraPopUpNoCentro(PM:TPopUpMenu);
    procedure MostraPopUpDeBotaoObject(PM:TPopUpMenu; ComponentePai:
              TObject; MoreWid, MoreTop: Integer);
    function  MostraPMCEP(PMCEP: TPopupMenu; Botao: TObject): Boolean;
    procedure MostraPopupGeral();
    // F A S T   R E P O R T
{$IFNDEF NAO_USA_FRX}
    procedure frxMemoAlignment(v: TfrxMemoView; Align: Integer);
    procedure frxConfiguraPDF(frxPDFExport: TfrxPDFExport);
    function  frxMostra(frx: TfrxReport; Titulo: string; NomeArq: String = ''): Boolean;
    function  frxPrepara(frx: TfrxReport; Titulo: string; NomeArq: String = ''): Boolean;
    function  frxImprime(frx: TfrxReport; Titulo: string): Boolean;
    function  frxSalva(frx: TfrxReport; Titulo, Arquivo: string): Boolean;
    function  frxNextDesign(): Boolean;
    procedure frxDefineDataSets(frx: TfrxReport;
              DataSets: array of TfrxDataset; Avisa: Boolean = True);
{$ENDIF}
    // R E D E
    function TentaDefinirDiretorio(const Host, ServerDir: String;
             const Empresa: Integer; const Pasta: String;
             const LaAviso1, LaAviso2: TLabel; const AddDirEmp: Boolean;
             MemoNA: TMemo; var Dir: String): Boolean;
    // T L I S T B O X
    procedure ListaItensListBoxDeLista(ListBox: TListBox; Lista: array of String);
    // T R I C H E D I T
    procedure AdicionaTextoCorRichEdit(RichEdit: TRichEdit; Cor: TColor;
              EstilosFonte: TFontStyles; Texto: String; AtHead: Boolean = True);
    procedure DefineTextoRichEdit(RichEdit: TRichEdit; Texto: String);
    procedure TextoEntreRichEdits(Sorc, Dest: TRichEdit);
    function  ObtemTextoRichEdit(Form: TForm; RichEdit: TRichEdit): WideString;
    procedure TDBRichEdit_LoadMemo(RE: TRichEdit; Data: string);
    procedure LocalizaTextoEmRich(DBRich: TRichEdit; FindDialog: TFindDialog);
{$IFNDEF NAO_USA_TEXTOS}
    function  EditaTextoRichEdit(RichEdit: TRichEdit; IniAtributesSize: Integer;
              IniAtributesName: String; ListaDeTags: array of String): WideString;
{$ENDIF}
    function  GetRTF(RE: TRichedit): String;
{$IfNDef NAO_USA_FRX}
    function  GetfrxRTF(RE: TfrxRichView): String;
{$EndIf}
    // T D M K C O M P O S T O R E
    procedure DefineValDeCompoEmbeded(Container: TdmkCompoStore; Valor: Variant);
    // T L A B E L
    procedure AjustaFonte(MyLabel: TLabel; Qualidade: Integer);
    // T A D V S T R I N G G R I D
    {$IfDef cAdvGrid} //Berlin
    procedure UpdMergeDBAvGrid(ASG: TDBAdvGrid; Cols: array of Integer);
    {$EndIf}
    // T C O L O R D I A L O G
    function  SelecionaCor(Form: TForm; Atual: TColor): TColor;
    // M E S S A G E D I A L O G
    function MessageDlgCheck(Msg: string; AType: TMsgDlgType;
             AButtons: TMsgDlgButtons; IndiceHelp: LongInt; DefButton:
             TModalResult; Portugues: Boolean; Checar:Boolean; MsgCheck:
             string; Funcao: TProcedure): Word;
    // T T A B C O N T R O L
    // T T A B S H E E T
    procedure PageControlChange(Sender: TObject);
    procedure AjustarCaptionAbas(ClasseForm: TFormClass; PageControl:
              TPageControl);
    function  TotalFormsAbertos(ClasseForm: TFormClass; PageControl:
              TPageControl): Integer;
    function  NovaAbaEmTabSheet(Owner: TForm; PageControl: TPageControl;
              ClasseForm: TFormClass; IndiceImagem: Integer): TForm;
    // T T D I
    function  FormTDICria(Classe: TFormClass; InOwner: TWincontrol;
              Pager: TWinControl; Collaps: Boolean = True;
              Unique: Boolean = False; ModoAcesso:
              TAcessFmModo = afmoNegarComAviso): TForm; overload;
(*
    function  FormTDICria(Classe: TFormClass; InOwner: TWincontrol;
              AdvToolBarPager: TdmkPageControl; Collaps: Boolean = True;
              Unique: Boolean = False; ModoAcesso:
              TAcessFmModo = afmoNegarComAviso): TForm; overload;
*)
    procedure FormTDIFecha(Form: TForm; AbaQueChamou: TTabSheet);
    function  FormTDIPanel(InstanceClass: TComponentClass;
              var Reference; Painel: TPanel): TForm;
    // T D M K E D I T D A T E T I M E P I C K E R
    procedure LimitaDatasPeloAnoMesDateTimePicker(dmkEditDateTimePicker:
              TdmkEditDateTimePicker; AnoMes: Integer; DiaSelecionado: Integer = 1);
    procedure IncMesTPIniEFim(TPIni, TPFim: TdmkEditDateTimePicker;
              Incremento: Integer);
    // T B I T B T N
    procedure AlteraCaptionBtInsUpd(BtInsUpd: TBitBtn; Altera: Boolean);
    //
    function Ping(Host: String; ReceiveTimeout: Integer = 500): Boolean;
    //
    // REGISTRO DO WINDOWS
    function  DBsDA_CaminhoTabela(Tabela: String; Records: Boolean = True): String;
    function  DBsDA_CaminhoRegistro(Caminho: String; ID: Integer): String;
    function  DBsDA_FmtRegistro(ID: Integer): String;
    // S K I N
{
    procedure SkinMenu(Ad vToolBarOfficeStyler: T AdvToolBarOfficeStyler;
              Index: Integer);
}
    function  SubstituiCorParaSkin(Cor: TShemeColor): TColor;
    procedure ConfiguraF7AppCBGraGru1(CBGraGru1: TdmkDBLookupComboBox);
    procedure DefineDataFieldSelecionadoInt(dmkEditCB: TdmkEditCB; DBEdit: TDBEdit;
              DataField: String);

  end;

var
  MyObjects: TUnMyObjects;
  OldRestore: TNotifyEvent;

implementation


uses Principal,
{$IFNDEF NAO_USA_SEL_RADIO_GROUP}
  SelRadioGroup, Sel2RadioGroups, Sel3RadioGroups, SelCheckGroup, SelListArr,
{$ENDIF}
{$IFNDEF NAO_USA_GLYFS} MyGlyfs, {$ENDIF}
{$IFNDEF NAO_USA_DB} DmkDAC_PF, {$ENDIF}
{$IFNDEF NAO_USA_FRX} MeuFrx,  {$ENDIF}
{$IFNDEF NAO_USA_TEXTOS} Textos2, {$ENDIF}
{$IFNDEF NAO_USA_DB_GERAL} MeuDBUses, ModuleGeral, UMySQLDB,{$ENDIF}
{$IFDEF DB_SQLite} MeuDBUsesZ, {$ENDIF}
{$IFDEF UsaWSuport} UnDmkWeb, {$ENDIF}
{$IfNDef sGetData} GetData, GetDatasPeriodo, {$EndIf}
//CoresVCLSkin,
UnDmkProcFunc;

procedure TUnMyObjects.SalvaStringGridToFile(Grade: TStringGrid; Arq: String;
  Pergunta: Boolean);
var
  F: TextFile;
  x, y: Integer;
  Dir: String;
begin
  Dir := ExtractFilePath(Arq);
  if not DirectoryExists(Dir) then
  begin
    if Pergunta then
    begin
      if Geral.MB_Pergunta('O diret�rio "' + Dir + '" n�o existe!' +
        sLineBreak + 'Conforma a sua cria��o?') = ID_YES
      then
        ForceDirectories(Dir)
      else begin
        Geral.MB_Aviso('O arquivo "' + Arq + '" n�o poder� ser salvo!');
      end;
    end else ForceDirectories(Dir);
  end;
  //
  AssignFile(F, Arq);
  Rewrite(F);
  Writeln(F, Grade.ColCount);
  Writeln(F, Grade.RowCount);
  for x:=0 to Grade.ColCount-1 do
    for y:=0 to Grade.RowCount-1 do
      Writeln(F, Grade.Cells[x,y]);
  CloseFile(F);
end;

function TUnMyObjects.SelecionaCor(Form: TForm; Atual: TColor): TColor;
var
  ColorDialog1: TColorDialog;
begin
  Result := Atual;
  ColorDialog1 := TColorDialog.Create(Form);
  try
    if ColorDialog1.Execute then
      Result := ColorDialog1.Color;
  finally
    ColorDialog1.Free;
  end;
end;

procedure TUnMyObjects.SelecionarLinhasNoDBGrid(AGrid: Vcl.DBGrids.TDBGrid;
  Selecionar: Boolean);
begin
  AGrid.SelectedRows.Clear;
  with AGrid.DataSource.DataSet do
  begin
    DisableControls;
    First;
    try
      while not EOF do
      begin
        AGrid.SelectedRows.CurrentRowSelected := Selecionar;
        Next;
      end;
    finally
      EnableControls;
    end;
  end;
end;

{$IFNDEF NAO_USA_SEL_RADIO_GROUP}
function TUnMyObjects.SelRadioGroup(CaptionFm, CaptionRG: String;
Itens: array of String; Colunas: Integer; Default: Integer = -1;
SelOnClick: Boolean = False): Integer;
var
  I, t, Dif: Integer;
begin
  CriaForm_AcessoTotal(TFmSelRadioGroup, FmSelRadioGroup);
  FmSelRadioGroup.Caption := 'XXX-XXXXX-998 :: ' + CaptionFm;
  FmSelRadioGroup.RGItens.Caption := CaptionRG;
  FmSelRadioGroup.RGItens.Items.Clear;
  FmSelRadioGroup.RGItens.Columns := Colunas;
  for I := Low(Itens) to High(Itens) do
    FmSelRadioGroup.RGItens.Items.Add(Itens[I]);
  FmSelRadioGroup.RGItens.ItemIndex := Default;
  FmSelRadioGroup.FSelOnClick := SelOnClick;
// inicio 2019-01-18
  t := FmSelRadioGroup.LaTitulo1A.Canvas.TextWidth(CaptionFm);
  Dif := FmSelRadioGroup.GB_M.Width - t;
  if Dif < 8 then
    FmSelRadioGroup.Width := FmSelRadioGroup.Width - Dif + 8;
// fim 2019-01-18
  FmSelRadioGroup.ShowModal;
  Result := FmSelRadioGroup.RGItens.ItemIndex;
  FmSelRadioGroup.Destroy;
end;

function TUnMyObjects.Sel2RadioGroups(const Altura: Integer; const CaptionFm,
CaptionRG1, CaptionRG2: String; const ItensRG1, ItensRG2: array of String; const
ColunasRG1, ColunasRG2: Integer; const DefaultRG1, DefaultRG2: Integer;
   const SelOnClick: Boolean; var Result1, Result2: Integer): Boolean;
var
  I, t, Dif: Integer;
begin
  CriaForm_AcessoTotal(TFmSel2RadioGroups, FmSel2RadioGroups);
  FmSel2RadioGroups.Height := Altura;
  FmSel2RadioGroups.Caption := 'XXX-XXXXX-998 :: ' + CaptionFm;

  // RadioGroup1
  FmSel2RadioGroups.RGItens1.Caption := CaptionRG1;
  FmSel2RadioGroups.RGItens1.Items.Clear;
  FmSel2RadioGroups.RGItens1.Columns := ColunasRG1;
  for I := Low(ItensRG1) to High(ItensRG1) do
    FmSel2RadioGroups.RGItens1.Items.Add(ItensRG1[I]);
  FmSel2RadioGroups.RGItens1.ItemIndex := DefaultRG1;
  FmSel2RadioGroups.FSelOnClick := SelOnClick;

  // RadioGroup2
  FmSel2RadioGroups.RGItens2.Caption := CaptionRG2;
  FmSel2RadioGroups.RGItens2.Items.Clear;
  FmSel2RadioGroups.RGItens2.Columns := ColunasRG2;
  for I := Low(ItensRG2) to High(ItensRG2) do
    FmSel2RadioGroups.RGItens2.Items.Add(ItensRG2[I]);
  FmSel2RadioGroups.RGItens2.ItemIndex := DefaultRG2;
  FmSel2RadioGroups.FSelOnClick := SelOnClick;
  //
  t := FmSel2RadioGroups.LaTitulo1A.Canvas.TextWidth(CaptionFm);
  Dif := FmSel2RadioGroups.GB_M.Width - t;
  if Dif < 8 then
    FmSel2RadioGroups.Width := FmSel2RadioGroups.Width - Dif + 8;
  //
  FmSel2RadioGroups.ShowModal;
  Result1 := FmSel2RadioGroups.RGItens1.ItemIndex;
  Result2 := FmSel2RadioGroups.RGItens2.ItemIndex;
  Result  := (Result1 > -1) and (Result2 > -1);
  //
  FmSel2RadioGroups.Destroy;
end;

function TUnMyObjects.Sel3RadioGroups(const Altura: Integer; const CaptionFm,
  CaptionRG1, CaptionRG2, CaptionRG3: String; const ItensRG1, ItensRG2,
  ItensRG3: array of String; const ColunasRG1, ColunasRG2, ColunasRG3,
  DefaultRG1, DefaultRG2, DefaultRG3: Integer; const SelOnClick: Boolean;
  var Result1, Result2, Result3: Integer): Boolean;
var
  I, t, Dif: Integer;
begin
  CriaForm_AcessoTotal(TFmSel3RadioGroups, FmSel3RadioGroups);
  FmSel3RadioGroups.Height := Altura;
  FmSel3RadioGroups.Caption := 'XXX-XXXXX-998 :: ' + CaptionFm;

  // RadioGroup1
  FmSel3RadioGroups.RGItens1.Caption := CaptionRG1;
  FmSel3RadioGroups.RGItens1.Items.Clear;
  FmSel3RadioGroups.RGItens1.Columns := ColunasRG1;
  for I := Low(ItensRG1) to High(ItensRG1) do
    FmSel3RadioGroups.RGItens1.Items.Add(ItensRG1[I]);
  FmSel3RadioGroups.RGItens1.ItemIndex := DefaultRG1;
  FmSel3RadioGroups.FSelOnClick := SelOnClick;

  // RadioGroup2
  FmSel3RadioGroups.RGItens2.Caption := CaptionRG2;
  FmSel3RadioGroups.RGItens2.Items.Clear;
  FmSel3RadioGroups.RGItens2.Columns := ColunasRG2;
  for I := Low(ItensRG2) to High(ItensRG2) do
    FmSel3RadioGroups.RGItens2.Items.Add(ItensRG2[I]);
  FmSel3RadioGroups.RGItens2.ItemIndex := DefaultRG2;
  FmSel3RadioGroups.FSelOnClick := SelOnClick;

  // RadioGroup3
  FmSel3RadioGroups.RGItens3.Caption := CaptionRG3;
  FmSel3RadioGroups.RGItens3.Items.Clear;
  FmSel3RadioGroups.RGItens3.Columns := ColunasRG3;
  for I := Low(ItensRG3) to High(ItensRG3) do
    FmSel3RadioGroups.RGItens3.Items.Add(ItensRG3[I]);
  FmSel3RadioGroups.RGItens3.ItemIndex := DefaultRG3;
  FmSel3RadioGroups.FSelOnClick := SelOnClick;
  //
  t := FmSel3RadioGroups.LaTitulo1A.Canvas.TextWidth(CaptionFm);
  Dif := FmSel3RadioGroups.GB_M.Width - t;
  if Dif < 8 then
    FmSel3RadioGroups.Width := FmSel3RadioGroups.Width - Dif + 8;
  //
  FmSel3RadioGroups.ShowModal;
  Result1 := FmSel3RadioGroups.RGItens1.ItemIndex;
  Result2 := FmSel3RadioGroups.RGItens2.ItemIndex;
  Result3 := FmSel3RadioGroups.RGItens3.ItemIndex;
  Result  := (Result1 > -1) and (Result2 > -1) and (Result3 > -1);
  //
  FmSel3RadioGroups.Destroy;
end;

function TUnMyObjects.SelRadioGroup2(const CaptionFm, CaptionRG: String;
  const Itens, Avisos: array of String; const Acoes: array of TSQLType;
  const Valores: array of String; const MyCods: array of Integer;
  const Colunas: Integer; var Valor: String; var MyOcorr: Integer;
  var SQLType: TSQLType; const DefaultItemIndex: Integer): Boolean;
var
  I, K: Integer;
  Aviso: String;
begin
  CriaForm_AcessoTotal(TFmSelRadioGroup, FmSelRadioGroup);
  FmSelRadioGroup.Caption := 'XXX-XXXXX-998 :: ' + CaptionFm;
  FmSelRadioGroup.RGItens.Caption := CaptionRG;
  FmSelRadioGroup.RGItens.Items.Clear;
  FmSelRadioGroup.RGItens.Columns := Colunas;
  for I := Low(Itens) to High(Itens) do
    FmSelRadioGroup.RGItens.Items.Add(Itens[I]);
  FmSelRadioGroup.RGItens.ItemIndex := DefaultItemIndex;
  //
  K := Length(Avisos);
  SetLength(FmSelRadioGroup.FAvisos, K);
  for I := Low(Avisos) to High(Avisos) do
    FmSelRadioGroup.FAvisos[I] := Avisos[I];
  //
  K := Length(Acoes);
  SetLength(FmSelRadioGroup.FAcoes, K);
  for I := Low(Acoes) to High(Acoes) do
    FmSelRadioGroup.FAcoes[I] := Acoes[I];
  //
  K := Length(Valores);
  SetLength(FmSelRadioGroup.FValores, K);
  for I := Low(Valores) to High(Valores) do
    FmSelRadioGroup.FValores[I] := Valores[I];
  //
  K := Length(MyCods);
  SetLength(FmSelRadioGroup.FMyCods, K);
  for I := Low(MyCods) to High(MyCods) do
    FmSelRadioGroup.FMyCods[I] := MyCods[I];
  //
  FmSelRadioGroup.ShowModal;
  Result := FmSelRadioGroup.RGItens.ItemIndex > -1;
  if Result then
  begin
    Valor := Valores[FmSelRadioGroup.RGItens.ItemIndex];
    SQLType := Acoes[FmSelRadioGroup.RGItens.ItemIndex];
    Aviso :=  Avisos[FmSelRadioGroup.RGItens.ItemIndex];
    MyOcorr := MyCods[FmSelRadioGroup.RGItens.ItemIndex];
    if Aviso <> '' then
      Geral.MB_Aviso('AVISO!' + sLineBreak + Aviso);
  end else begin
    Valor := '';
    SQLType := stNil;
  end;
  FmSelRadioGroup.Destroy;
end;

function TUnMyObjects.SelCheckGroup(CaptionFm, CaptionCG: String;
Itens: array of String; Colunas: Integer; Default: Integer = -1): Integer;
var
  I, t, Dif: Integer;
begin
  CriaForm_AcessoTotal(TFmSelCheckGroup, FmSelCheckGroup);
  FmSelCheckGroup.Caption := 'XXX-XXXXX-998 :: ' + CaptionFm;
  FmSelCheckGroup.CGItens.Caption := CaptionCG;
  FmSelCheckGroup.CGItens.Items.Clear;
  FmSelCheckGroup.CGItens.Columns := Colunas;
  for I := Low(Itens) to High(Itens) do
    FmSelCheckGroup.CGItens.Items.Add(Itens[I]);
  FmSelCheckGroup.CGItens.Value := Default;
  //FmSelCheckGroup.FSelOnClick := SelOnClick;
  t := FmSelCheckGroup.LaTitulo1A.Canvas.TextWidth(CaptionFm);
  Dif := FmSelCheckGroup.GB_M.Width - t;
  if Dif < 8 then
    FmSelCheckGroup.Width := FmSelCheckGroup.Width - Dif + 8;
  FmSelCheckGroup.ShowModal;
  Result := FmSelCheckGroup.CGItens.Value;
  FmSelCheckGroup.Destroy;
end;

{$ENDIF}

function TUnMyObjects.Senha(ComponentClass: TComponentClass;
  Reference: TComponent; Tipo: Integer; LaVlr, EdVlr, LaPercent, EdPercent,
  VlrBase: String; Login: Boolean; LaData: String; Data: TDateTime;
  Janela: String; InfoCaption: String=''): Integer;
var
  i: Integer;
begin
  VAR_VALORSENHA := 0;
  VAR_FSENHA := Tipo;
  FormCria(ComponentClass, Reference);
  with Reference do
  begin
    for i := 0 to ComponentCount -1 do
    begin
      if (Components[i] is TLabel) then
      begin
        if TLabel(Components[i]).Name = 'LaJanela' then
          TLabel(Components[i]).Caption := Janela;
        if TLabel(Components[i]).Name = 'LaBase' then
          TLabel(Components[i]).Caption := VlrBase;
        if TLabel(Components[i]).Name = 'LaAviso1' then
          TLabel(Components[i]).Caption := InfoCaption;
        if TLabel(Components[i]).Name = 'LaAviso2' then
          TLabel(Components[i]).Caption := InfoCaption;
        if TLabel(Components[i]).Name = 'LaLogin' then
        begin
          if Login then
            TLabel(Components[i]).Visible := True;
        end;
        if TLabel(Components[i]).Name = 'LaValor' then
        begin
          if LaVlr <> CO_VAZIO then
          begin
            TLabel(Components[i]).Visible := True;
            TLabel(Components[i]).Caption := LaVlr;
          end;
        end;
        if TLabel(Components[i]).Name = 'LaPorcentagem' then
        begin
          if LaPercent <> CO_VAZIO then
          begin
            TLabel(Components[i]).Visible := True;
            TLabel(Components[i]).Caption := LaPercent;
          end;
        end;
        if TLabel(Components[i]).Name = 'LaData' then
        begin
          if LaData <> CO_VAZIO then
          begin
            TLabel(Components[i]).Visible := True;
            TLabel(Components[i]).Caption := LaPercent;
          end;
        end;
      end else if (Components[i] is TdmkEdit) then
      begin
        if TdmkEdit(Components[i]).Name = 'EdLogin' then
        begin
          if Login then
            TdmkEdit(Components[i]).Visible := True;
        end;
        if TdmkEdit(Components[i]).Name = 'EdValor' then
        begin
          if LaVlr <> CO_VAZIO then
          begin
            TdmkEdit(Components[i]).Visible := True;
            TdmkEdit(Components[i]).Text    := EdVlr;
          end;
        end;
        if TdmkEdit(Components[i]).Name = 'EdPorcentagem' then
        begin
          if LaPercent <> CO_VAZIO then
          begin
            TdmkEdit(Components[i]).Visible := True;
            TdmkEdit(Components[i]).Text    := EdPercent;
          end;
        end;
      end else if (Components[i] is TDateTimePicker) then
      begin
        if LaData <> '' then
        begin
          TDateTimePicker(Components[i]).Visible := True;
          TDateTimePicker(Components[i]).Date := Data;
        end;
      end;
    end;
  end;
  TForm(Reference).ShowModal;
  Reference.Destroy;
  Result := VAR_SENHARESULT; //  2 = OK
end;

function TUnMyObjects.SetaItensBookmark(Form: TForm; AGrid: Vcl.DBGrids.TDBGrid; Campo:
  String; ArrXtn: array of Extended; DesmarcarOutros: Boolean): Boolean;
var
  I, J , K: Integer;
  N: Extended;
  Qry: TmySQLQuery;
  DS: TDataset;
  NaoAchou: Integer;
begin
  if DesmarcarOutros then
    SelecionarLinhasNoDBGrid(AGrid, False);
  NaoAchou := 0;
  Ds := AGrid.DataSource.DataSet;
  K  := High(ArrXtn);
  for J := 0 to K do
  begin
    if Ds.Locate(Campo, ArrXtn[J], []) then
    begin
      AGrid.SelectedRows.CurrentRowSelected := True;
    end else
      NaoAchou := NaoAchou + 1;
  end;
  Result := NaoAchou = 0;
end;

procedure TUnMyObjects.SetaItensBookmarkAtivos(Form: TForm; AGrid: Vcl.DBGrids.TDBGrid;
  FldAtivo: String);
begin
  AGrid.SelectedRows.Clear;
  with AGrid.DataSource.DataSet do
  begin
    DisableControls;
    First;
    try
      while not EOF do
      begin
        if AGrid.DataSource.Dataset.FieldByName(FldAtivo).AsInteger = 1 then
          AGrid.SelectedRows.CurrentRowSelected := True;
        Next;
      end;
    finally
      EnableControls;
    end;
  end;
end;

function TUnMyObjects.SetaPeriodoDeCBANoMes(CBAno, CBMes: TComboBox): Integer;
var
  Ano, Mes: Integer;
begin
  Result := 0;
  Mes := CBMes.ItemIndex + 1;
  Ano := Geral.IMV(CBAno.Text);
  if Mes > 0 then
  begin
    Result := Mes;
    if Ano > 2000 then
      Result := Result + ((Ano - 2000) * 12);
  end;
end;

procedure TUnMyObjects.SetaRGCNAB(RGCNAB: TRadioGroup; CNAB: Integer);
begin
  case CNAB of
    0: RGCNAB.ItemIndex := 0;
    240: RGCNAB.ItemIndex := 1;
    400: RGCNAB.ItemIndex := 2;
    else Geral.MB_Erro('Defini��o de CNAB desconhecida: ' +
    Geral.FF0(CNAB) + '!');
  end;
end;

procedure TUnMyObjects.SetaTodosItensBookmark(Form: TForm; DBG: Vcl.DBGrids.TDBGrid; Ativa: Boolean);
var
  Bookmark: TBookmark;
  Qry: TDataset;
  MyCursor: TCursor;
begin
  MyCursor := Form.Cursor;
  Form.Cursor := crHourGlass;
  //
  Qry := DBG.DataSource.DataSet;
  BookMark := Qry.Bookmark;
  Qry.DisableControls;
  try
    Qry.First;
    while not Qry.Eof do
    begin
      DBG.SelectedRows.CurrentRowSelected := Ativa;
      Qry.Next;
    end;
  finally
    Form.Cursor := MyCursor;
    Qry.EnableControls;
    Qry.Bookmark := Bookmark;
  end;
end;

procedure TUnMyObjects.SetTextAndGoEnd(EdTxt: TdmkEdit; Texto: String);
begin
  EdTxt.Text := Texto;
  EdTxt.SelStart := Length(Texto);
end;

{
procedure TUnMyObjects.SkinMenu(Ad vToolBarOfficeStyler: T AdvToolBarOfficeStyler;
  Index: Integer);
begin
  case Index of
    0: AdvToolBarOfficeStyler.Style := bsOffice2003Blue;
    1: AdvToolBarOfficeStyler.Style := bsOffice2003Classic;
    2: AdvToolBarOfficeStyler.Style := bsOffice2003Olive;
    3: AdvToolBarOfficeStyler.Style := bsOffice2003Silver;
    4: AdvToolBarOfficeStyler.Style := bsOffice2007Luna;
    5: AdvToolBarOfficeStyler.Style := bsOffice2007Obsidian;
    6: AdvToolBarOfficeStyler.Style := bsOffice2007Silver;
    7: AdvToolBarOfficeStyler.Style := bsOfficeXP;
    8: AdvToolBarOfficeStyler.Style := bsWhidbeyStyle;
    9: AdvToolBarOfficeStyler.Style := bsWindowsXP;
  end;
end;
}

procedure TUnMyObjects.StringGridRotateTextOut(Grid: TStringGrid;
ARow, ACol: Integer; Rect: TRect; NomeFonte: string;
Size: Integer; Color: TColor; Alignment: TAlignment);
var
  lf: TLogFont;
  tf: TFont;
begin
  // if the font is to big, resize it
  // wenn Schrift zu gro� dann anpassen
  if (Size > Grid.ColWidths[ACol] div 2) then
    Size := Grid.ColWidths[ACol] div 2;
  with Grid.Canvas do
  begin
    // Replace the font
    // Font setzen
    Font.Name := NomeFonte;
    Font.Size := Size;
    Font.Color := Color;
    tf := TFont.Create;
    try
      tf.Assign(Font);
      GetObject(tf.Handle, SizeOf(lf), @lf);
      lf.lfEscapement  := 900;
      lf.lfOrientation := 0;
      tf.Handle := CreateFontIndirect(lf);
      Font.Assign(tf);
    finally
      tf.Free;
    end;
    // fill the rectangle
    // Rechteck f�llen
    FillRect(Rect);
    // Align text and write it
    // Text nach Ausrichtung ausgeben
    if Alignment = taLeftJustify then
      TextRect(Rect, Rect.Left + 2,Rect.Bottom - 2,Grid.Cells[ACol, ARow]);
    if Alignment = taCenter then
      TextRect(Rect, Rect.Left + Grid.ColWidths[ACol] div 2 - Size +
        Size div 3,Rect.Bottom - 2,Grid.Cells[ACol, ARow]);
    if Alignment = taRightJustify then
      TextRect(Rect, Rect.Right - Size - Size div 2 - 2,Rect.Bottom -
        2,Grid.Cells[ACol, ARow]);
  end;
end;

// 2. Alternative: Display text vertically in StringGrid cells
// 2. Variante: Vertikale Textausgabe in den Zellen eines StringGrid
procedure TUnMyObjects.StringGridRotateTextOut2(Grid: TStringGrid; ARow, ACol:
Integer; Rect: TRect; NomeFonte: String; Size: Integer;
Color: TColor; Alignment: TAlignment);
var
    NewFont, OldFont : Integer;
    FontStyle, FontItalic, FontUnderline, FontStrikeout: Integer;
begin
   // if the font is to big, resize it
   // wenn Schrift zu gro� dann anpassen
   If (Size > Grid.ColWidths[ACol] DIV 2) Then
       Size := Grid.ColWidths[ACol] DIV 2;
   with Grid.Canvas do
   begin
       // Set font
       // Font setzen
       If (fsBold IN Font.Style) Then
          FontStyle := FW_BOLD
       Else
          FontStyle := FW_NORMAL;

       If (fsItalic IN Font.Style) Then
          FontItalic := 1
       Else
          FontItalic := 0;

       If (fsUnderline IN Font.Style) Then
          FontUnderline := 1
       Else
          FontUnderline := 0;

       If (fsStrikeOut IN Font.Style) Then
          FontStrikeout:=1
       Else
          FontStrikeout:=0;

       Font.Color := Color;

       NewFont := CreateFont(Size, 0, 900, 0, FontStyle, FontItalic,
                             FontUnderline, FontStrikeout, DEFAULT_CHARSET,
                             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
                             DEFAULT_PITCH, PChar(NomeFonte));

       OldFont := SelectObject(Handle, NewFont);
       // fill the rectangle
       // Rechteck f�llen
       FillRect(Rect);
       // Write text depending on the alignment
       // Text nach Ausrichtung ausgeben
       If Alignment = taLeftJustify Then
          TextRect(Rect,Rect.Left+2,Rect.Bottom-2,Grid.Cells[ACol,ARow]);
       If Alignment = taCenter Then
          TextRect(Rect,Rect.Left+Grid.ColWidths[ACol] DIV 2 - Size + Size DIV 3,
            Rect.Bottom-2,Grid.Cells[ACol,ARow]);
       If Alignment = taRightJustify Then
          TextRect(Rect,Rect.Right-Size - Size DIV 2 - 2,Rect.Bottom-2,Grid.Cells[ACol,ARow]);

       // Recreate reference to the old font
       // Referenz auf alten Font wiederherstellen
       SelectObject(Handle, OldFont);
       // Recreate reference to the new font
       // Referenz auf neuen Font l�schen
       DeleteObject(NewFont);
   end;
end;

function TUnMyObjects.StringGrid_to_CSV(AGrid: TStringGrid; ACSVFile: String;
  Exclui: TYesNoAsk; InfoMsg: Boolean; PB1: TProgressBar; LaAviso1, LaAviso2:
  TLabel): Boolean;
begin
{
  Parei Aqui!
  if FileExists(ACSVFile) then
  begin
     case Exclui of
       (*0*)TYesNoAsk.ynaNo
       (*1*)TYesNoAsk.ynaYes=1,
       (*2*)TYesNoAsk.ynaAsk=2,
       (*3*)TYesNoAsk.ynaIndef=3

/
}
end;

function TUnMyObjects.StringGrid_to_Xls(AGrid: TStringGrid; AXLSFile: String;
  PB1: TProgressBar; LaAviso1, LaAviso2: TLabel): Boolean;
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r, Larg, m, n: Integer;
  ColWid: array[0..255] of Integer;
begin
  Screen.Cursor := crHourGlass;
  Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo excel ...');
  if PB1 <> nil then
    PB1.Position := 0;
  Result := False;
  try
    XLApp := CreateOleObject('Excel.Application');
  except
    try
      XLApp := CreateOleObject('Works.Application');
    except
      //raise;
      Geral.MB_Aviso('N�o foi poss�vel iniciar uma inst�ncia do "Excel"!' +
      sLineBreak + 'Verifique se o aplicativo "Excel" est� instalado neste computador!"');
      Exit;
    end;
  end;
  try
    MyObjects.LimpaGrade(AGrid, 1, 1, True);
    XLApp.Visible := False;
    XLApp.Workbooks.Open(AXLSFile);
    Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;
    AGrid.RowCount := x;
    AGrid.ColCount := y + 1;
    if PB1 <> nil then
    begin
      PB1.Max := x * (y + 1);
      PB1.Update;
    end;
    Informa2(LaAviso1, LaAviso2, True, 'Lendo arquivo excel. Pode demorar se o arquivo conter muitos dados ...');
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;   // >... pode demorar
    k := 1; // 1?
    m := Round(CharWid * 5); // 2012-02-25 de "CharWid * 3" para "CharWid * 5"
    for n := 0 to 255 do ColWid[n] := m;
    repeat
      Informa2(LaAviso1, LaAviso2, True, 'Carregando linha ' + Geral.FF0(k));
      Application.ProcessMessages;  // lendo linha
      AGrid.Cells[0, (k - 1)] := FormatFloat('000', (k - 1));
      for r := 1 to y do
      begin
        if PB1 <> nil then
        begin
          PB1.Position := PB1.Position + 1;
          PB1.Update;
        end;
        Application.ProcessMessages;
        //
        AGrid.Cells[r, (k - 1)] := RangeMatrix[K, R];
        Larg := Round((Length(RangeMatrix[K, R]) + 1) * CharWid);
        if Larg > ColWid[r] then
          ColWid[r] := Larg;
      end;
      Inc(k, 1);
      AGrid.RowCount := k + 2;
    until k > x;
    if PB1 <> nil then
    begin
      PB1.Position := PB1.Max;
      PB1.Update;
    end;
    Informa2(LaAviso1, LaAviso2, True, 'Limpando mem�ria ...');
    RangeMatrix := Unassigned; // Limpando mem�ria
  finally
    Informa2(LaAviso1, LaAviso2, True, 'Fechando arquivo ...');
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
  Informa2(LaAviso1, LaAviso2, True, 'Ajustando largura de colunas ...');
  for n := 0 to AGrid.ColCount -1 do
    AGrid.ColWidths[n] := ColWid[n];
  //
  Informa2(LaAviso1, LaAviso2, False, '...');
end;

function TUnMyObjects.StyleServiceObtemCor(CorSistema: TColor): TColor;
var
  LStyleService: TCustomStyleServices;
begin
  Result := CorSistema;
  // Mudar cor do componente
//https://theroadtodelphi.com/2012/02/06/changing-the-color-of-edit-controls-with-vcl-styles-enabled/

//procedure TFrmMain.LvSampleDataDrawItem(Sender: TCustomListView; Item: TListItem; Rect: TRect; State: TOwnerDrawState);
  LStyleService  := StyleServices;
  if not LStyleService.Enabled then exit;

  //Sender.Canvas.Brush.Style := bsSolid;
  //Sender.Canvas.Brush.Color := LStyleService.GetSystemColor(clWindow);
  //Sender.Canvas.FillRect(Rect);
  //LRect := Rect;
  Result := LStyleService.GetSystemColor(CorSistema);
end;

procedure TUnMyObjects.StylesListRefreshMenu(PMStls: TPopupMenu);
var
  StyleName: string;
  MenuItem: TMenuItem;
begin
////////////////////////////////////////////////////////////////////////////////
  // Cores:
  // https://theroadtodelphi.com/category/vcl-styles/
////////////////////////////////////////////////////////////////////////////////
  PMStls.Items.Clear;
  // retrieve all the styles linked in the executable
  for StyleName in TStyleManager.StyleNames do
  begin
    MenuItem := TMenuItem.Create(PMStls.Owner);
    //MenuItem.Name := NomeTBar;
    MenuItem.Caption := StyleName;
    MenuItem.OnClick := MenuItemStyleClick;
    PMStls.Items.Add(MenuItem);
  end;
end;

function TUnMyObjects.SubstituiCorParaSkin(Cor: TShemeColor): TColor;
var
  Ok: Boolean;
begin
  Ok := False;
  // TODO AlphaSkin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
    case TShemeColor(Cor) of
      csButtonShadow:      Result := clBtnFace;
      csScrollbar:         Result := clScrollBar;
      csButtonFace:        Result := clBtnFace;
      csText:              Result := clWindowText;

      // 2020-04-27
      csTitleTextActive:   Result := clActiveCaption;
      csTitleTextNoActive: Result := clInactiveCaption; // clInactiveCaptionText
      csButtonDkshadow:    Result := cl3DDkShadow; // clDarkGray
      csButtonLight:       Result := clHighlight; // clBtnHighlight;
      // Fim 2020-04-27

      else                 Result := clGray;
    end;
    Ok := True;
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.sd1.Active then
  begin
    Result := FmPrincipal.sd1.Colors[Cor];
    Ok := True;
  end;
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
(*
TacPaletteColors = (pcMainColor, pcLabelText, pcWebText, pcWebTextHot, pcEditText, pcEditBG,
    pcSelectionBG, pcSelectionText, pcSelectionBG_Focused, pcSelectionText_Focused,
    pcEditBG_Inverted, pcEditText_Inverted, pcEditBG_OddRow, pcEditBG_EvenRow,
    pcEditText_Ok, pcEditText_Warning, pcEditText_Alert, pcEditText_Caution, pcEditText_Bypassed,
    pcEditBG_Ok, pcEditBG_Warning, pcEditBG_Alert, pcEditBG_Caution, pcEditBG_Bypassed,
    pcEditText_Highlight1, pcEditText_Highlight2, pcEditText_Highlight3,
    // Round buttons colors
    pcBtnColor1Active,  pcBtnColor2Active,  pcBtnBorderActive,  pcBtnFontActive,
    pcBtnColor1Normal,  pcBtnColor2Normal,  pcBtnBorderNormal,  pcBtnFontNormal,
    pcBtnColor1Pressed, pcBtnColor2Pressed, pcBtnBorderPressed, pcBtnFontPressed,

    pcBorder, pcGrid, pcHintBG, pcHintText);
*)
  if FmPrincipal.sSkinManager1.Active then
  begin
    Ok := True;
    case TShemeColor(Cor) of
      csButtonShadow:      Result := FmPrincipal.sSkinManager1.Palette[pcMainColor];
      csScrollbar:         Result := FmPrincipal.sSkinManager1.Palette[pcEditText_Inverted];
      csButtonFace:        Result := FmPrincipal.sSkinManager1.Palette[pcEditBG];
      csText:              Result := FmPrincipal.sSkinManager1.Palette[pcLabelText];
      else
      begin
        Ok     := False;
        Result := clGray;
      end;
    end;
  end;
//  end;
{$EndIf}
  if OK = False then
  begin
    case TShemeColor(Cor) of
      csButtonShadow:      Result := clBtnFace;
      csScrollbar:         Result := clScrollBar;
      csButtonFace:        Result := clBtnFace;
      csText:              Result := clWindowText;
      else                 Result := clGray;
    end;
  end;
end;

function TUnMyObjects.SVP_O(Compo: TObject; Propriede: String;
  Valor: Variant): Boolean;
var
  PropInfo: PPropInfo;
begin
  Result := False;
  if Compo <> nil then
  begin
    PropInfo := GetPropInfo(Compo, Propriede);
    if PropInfo <> nil then
    begin
      SetPropValue(Compo, Propriede, Valor);
      Result := Valor;
    end;
  end;
end;

function TUnMyObjects.SVP_Objeto(const AObject: TObject; const Nome: String;
  const Classe: TClass; const Propriedade: String;
  const Valor: Variant): Boolean;
var
  Objeto: TObject;
begin
  Result := False;
  if ObtemObjeto(AObject, Nome, Classe, Objeto) then
    Result := SVP_O(Objeto, Propriedade, Valor);
end;

procedure TUnMyObjects.SVP_Primit(Compo: TComponent; Propriedade: String;
  Valor: Variant);
var
  PropInfo: PPropInfo;
begin
  if Compo <> nil then
  begin
    PropInfo := GetPropInfo(Compo, Propriedade);
    if PropInfo <> nil then
      SetPropValue(Compo, Propriedade, Valor)
  end;
end;

procedure TUnMyObjects.TDBRichEdit_LoadMemo(RE: TRichEdit; Data: string);
const
  RTFHeader = '{\rtf';  { Do not localize }
  URTFHeader = '{urtf'; { Do not localize }
var
  //Data: string;
  Stream: TStringStream;
begin
  // Evitar erro de fonte!
  RE.ParentFont := False;
  //
  //if not FMemoLoaded and Assigned(FDataLink.Field) and FDataLink.Field.IsBlob then
  begin
    try
      //Data := FDataLink.Field.AsString;
      if (Pos(RTFHeader, Data) = 1) or (Pos(URTFHeader, Data) = 1) then
      begin
        Stream := TStringStream.Create(Data);
        try
          RE.Lines.LoadFromStream(Stream);
        finally
          Stream.Free;
        end;
      end
      else
        RE.Text := Data;
      //FMemoLoaded := True;
    except
      { Rich Edit Load failure }
      on E:EOutOfResources do
        RE.Lines.Text := Format('(%s)', [E.Message]);
    end;
    //EditingChange(Self);
  end;
end;

function TUnMyObjects.TentaDefinirDiretorio(const Host, ServerDir: String;
  const Empresa: Integer; const Pasta: String; const LaAviso1, LaAviso2: TLabel;
  const AddDirEmp: Boolean; MemoNA: TMemo; var Dir: String): Boolean;

  procedure Msg(Txt: String);
  begin
    Geral.MB_Erro(Txt);
  end;

const
  ArqImg = 'teste.~txt';
var
  //Res, Titulo, 
  SrvIP, SrvDir, ArqDir, IniDir, FullArq, DirRaiz: String;
  Continua: Boolean;
  //I, F: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Result   := False;
    Continua := True;
    //
    SrvIP :=  dmkPF.IgnoraLocalhost(Host);
    SrvDir := dmkPF.VeSeEhDirDmk(ServerDir, Pasta, False);
    //
    if AddDirEmp then
      ArqDir  := 'Emp_' + Geral.Substitui(Geral.FFN(Empresa, 4), '-', '_')
    else
      ArqDir := '';
    //
    if SrvIP <> '' then
    begin
      Continua := MyObjects.Ping(SrvIP);
      //
      SrvIP :=  '\\' + SrvIP + '\';
      // Tirar C:\
      if pos(':\', SrvDir) = 2 then
        SrvDir := Copy(SrvDir, 4);
    end else
    if Copy(SrvDir, 0, 2) = '\\' then
    begin
      DirRaiz := Copy(SrvDir, 3);
      DirRaiz := Copy(DirRaiz, 1, Pos('\', DirRaiz) - 1);
      //
      Continua := MyObjects.Ping(DirRaiz);
    end;
    if (Copy(SrvDir, 0, 1) = '\') and (Copy(SrvDir, 0, 2) <> '\\') then
      SrvDir := Copy(SrvDir, 2);
    //
    IniDir := SrvIP + SrvDir;
    //
    Informa2(LaAviso1, LaAviso2, True, 'Verificando pasta: ' + IniDir);
    //
    if Continua = True then
      DirectoryExists(IniDir, False);
    //
    FullArq := Geral.Substitui(IniDir + '\' + ArqDir + '\' + ArqImg, '\\', '\');
    while pos('\\', FullArq) > 0 do
      FullArq := Geral.Substitui(IniDir + '\' + ArqDir, '\\', '\');
    if FullArq[1] = '\' then
      FullArq := '\' + FullArq;
    Dir := ExtractFileDir(FullArq);
    //
    if Continua = True then
    begin
      Result := True;
      if FileExists(FullArq) then
      begin
        try
          DeleteFile(FullArq);
        except
          Msg('N�o foi poss�vel excluir arquivo teste!');
        end;
      end;
      try
        Informa2(LaAviso1, LaAviso2, True, 'Verificando pasta: ' + Dir);
        ForceDirectories(Dir);
        Geral.SalvaTextoEmArquivo(FullArq, 'Teste', True);
        Result := True;
      except
          Msg('N�o foi poss�vel salvar arquivo teste: ' + sLineBreak + FullArq);
      end;
    end else
    begin
      if MemoNA <> nil then
        ArquivoExiste(IniDir, MemoNA, False)
      else
      Msg(
      'O diret�rio raiz obrigat�rio definido nas op��es espec�ficas n�o foi localizado!' +
        sLineBreak + IniDir);
    end;
  finally
    Informa2(LaAviso1, LaAviso2, False, '...');
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnMyObjects.TextoEntreRichEdits(Sorc, Dest: TRichEdit);
var
  MemoryStream: TMemoryStream;
begin
  MemoryStream := TMemoryStream.Create;
  try
    Sorc.Lines.SaveToStream(MemoryStream);
    MemoryStream.Position := 0;
    Dest.Lines.LoadFromStream(MemoryStream);
  finally
    MemoryStream.Free;
  end;
end;

function TUnMyObjects.TextosDeItensSelecionadosDeCheckGroup(
  CheckGroup: TdmkCheckGroup): String;
var
  I: Integer;
begin
  if CheckGroup.Value = 0 then
    Result := ''
  else
  begin
    for I := 0 to CheckGroup.Items.Count - 1 do
    begin
      if CheckGroup.Checked[I] = True then
        Result := Result + ', ' + CheckGroup.Items[I]
    end;
    Result := Copy(Result, 3);
  end;
end;

function TUnMyObjects.ToggleTreeViewCheckBoxes_Child(Node: TTreeNode): Integer;
  procedure SetaAncestrais(const Filho: TTreeNode; const StatusFilho:
  TTreeViewImagesSet; var Nivel: Integer);
  var
    Pai, Irmao: TTreeNode;
    StatusPai: TTreeViewImagesSet;
    Nao, Sim, May: Integer;
  begin
    Pai := Filho.Parent;
    if Assigned(Pai) then
    begin
      Nivel := Nivel + 1;
      if (Pai.Count = 1) or (StatusFilho = cCBMay) then
        StatusPai := StatusFilho
      else
      begin
        Nao := 0;
        Sim := 0;
        May := 0;
        Irmao := Pai.getFirstChild;
        while Assigned(Irmao) do
        begin
          case TTreeViewImagesSet(Irmao.StateIndex) of
            cCBMay: May := May + 1;
            cCBNot: Nao := Nao + 1;
            cCBYes: Sim := Sim + 1;
          end;
          if May > 0 then
            Break
          else
          if (Nao > 0) and (Sim > 0) then
            Break;
          Irmao := Irmao.getNextSibling;
        end;
        if Sim = Pai.Count then
          StatusPai := cCBYes
        else
        if Nao = Pai.Count then
          StatusPai := cCBNot
        else
          StatusPai := cCBMay;
      end;
      Pai.StateIndex := Integer(StatusPai);
      SetaAncestrais(Pai, StatusPai, Nivel);
    end;
  end;
  procedure SetaFilhos(Pai: TTreeNode; StatusPai: TTreeViewImagesSet);
  var
    Filho: TTreeNode;
  begin
    Filho := Pai.getFirstChild;
    while Assigned(Filho) do
    begin
      Filho.StateIndex := Integer(StatusPai);
      SetaFilhos(Filho, StatusPai);
      Filho := Filho.getNextSibling;
    end;
  end;
var
  Tmp: TTreeNode;
  Status: TTreeViewImagesSet;
begin
  //Exit;
  //
  Result := 0;
  if Assigned(Node) then
  begin
    Status := TTreeViewImagesSet(Node.StateIndex);
    case Status of
      cTreeViewError: ;
      cCBMay: Status := cCBNot;
      cCBNot: Status := cCBYes;
      cCBYes: Status := cCBNot;
      cRBMay: Status := cRBNot;
      cRBNot: Status := cRBYes;
      cRBYes: Status := cRBNot;
    end;
    if Status in ([cCBMay, cCBNot, cCBYes]) then
    begin
      Node.StateIndex := Integer(Status);
      // ini 2022-06-16
      if Status = cCbNot then
        SetaFilhos(Node, Status);
      // fim 2022-06-16
      SetaAncestrais(Node, Status, Result);
    end
    else
    begin
      tmp := Node.Parent;
      if not Assigned(tmp) then
        tmp := TTreeView(Node.TreeView).Items.getFirstNode
      else
        tmp := tmp.getFirstChild;
      while Assigned(tmp) do
      begin
        if (tmp.StateIndex in [Integer(cRBNot), Integer(cRBYes)]) then
          tmp.StateIndex := Integer(cRBNot);
        tmp := tmp.getNextSibling;
      end;
      Node.StateIndex := Integer(cRBYes);
    end;
  end;
end;

procedure TUnMyObjects.ToggleTreeViewCheckBoxes_Unico(Node: TTreeNode);
var
  tmp:TTreeNode;
begin
  if Assigned(Node) then
  begin
    if Node.StateIndex = Integer(cCBNot) then
      Node.StateIndex := Integer(cCBYes)
    else if Node.StateIndex = Integer(cCBYes) then
      Node.StateIndex := Integer(cCBNot)
    else if Node.StateIndex = Integer(cRBNot) then
    begin
      tmp := Node.Parent;
      if not Assigned(tmp) then
        tmp := TTreeView(Node.TreeView).Items.getFirstNode
      else
        tmp := tmp.getFirstChild;
      while Assigned(tmp) do
      begin
        if (tmp.StateIndex in [Integer(cRBNot), Integer(cRBYes)]) then
          tmp.StateIndex := Integer(cRBNot);
        tmp := tmp.getNextSibling;
      end;
      Node.StateIndex := Integer(cRBYes);
    end;
  end;
end;

function TUnMyObjects.TotalFormsAbertos(ClasseForm: TFormClass; PageControl:
  TPageControl): Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to PageControl.PageCount - 1 do
    if PageControl.Pages[I].Components[0].ClassType = ClasseForm then
      Inc(Result);
end;

procedure TUnMyObjects.UperCaseComponente;
var
  i : Integer;
  Compo : TComponent;
begin
  for i := 0 to Screen.ActiveForm.ComponentCount -1 do
  begin
    Compo := Screen.ActiveForm.Components[i];
    if Screen.ActiveForm.Components[i] is TEdit   then
       TEdit(Compo).CharCase := ecUpperCase;
    if Screen.ActiveForm.Components[i] is TdmkEdit then
    begin
      if TdmkEdit(Compo).NoForceUppercase = False then
        if TdmkEdit(Compo).CharCase = ecNormal then
          TdmkEdit(Compo).CharCase := ecUpperCase;
    end;
    if Screen.ActiveForm.Components[i] is TDBEdit then
       TDBEdit(Compo).CharCase := ecUpperCase;
{
    if Screen.ActiveForm.Components[i] is T L M D Edit then
       T L M D Edit(Compo).CharCase := ecUpperCase;
    if Screen.ActiveForm.Components[i] is T L M D DBEdit then
       T L M D DBEdit(Compo).CharCase := ecUpperCase;
}
    if Screen.ActiveForm.Components[i] is TMaskEdit then
       TMaskEdit(Compo).CharCase := ecUpperCase;
//    if Screen.ActiveForm.Components[i] is TDBMemo then
//       TDBMemo(Compo).CharCase := ecUpperCase;
//    if Screen.ActiveForm.Components[i] is TMemo then
//       TMemo(Compo).CharCase := ecUpperCase;
    if Screen.ActiveForm.Components[i] is TDBComboBox then
       TDBComboBox(Compo).CharCase := ecUpperCase;
//    if Screen.ActiveForm.Components[i] is TDBLookupComboBox then
//       TDBLookupComboBox(Compo).CharCase := ecUpperCase;
//    if Screen.ActiveForm.Components[i] is TListBox then
//       TListBox(Compo).CharCase := ecUpperCase;
  end;
end;

function TUnMyObjects.WinControlSetFocus(WinCtrlName: String): Boolean;
var
  WinCtrl: TComponent;
begin
  Result := False;
  WinCtrl := Screen.ActiveForm.FindComponent(WinCtrlName);
  if WinCtrl <> nil then
  begin
    TWinControl(WinCtrl).SetFocus;
    Result := True;
  end;
end;

function TUnMyObjects.Xls_To_Array(var MyArrStr: TMyArrArrStr; const AXLSFile:
  String; const PB1: TProgressBar; const LaAviso1, LaAviso2: TLabel; const
  Planilha: Word): Boolean;
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  I, M, N, K, R, X, Y(*, Larg*): Integer;
  ColWid: array[0..255] of Integer;
  Info: String;
  Valor: Variant;
begin
(*
Segmento
Account
GrupoEmpresa
Familia
Codcli
Pais
UF
Operacao
Produto
Cliente
Cidade
Data
CodProd
Embalagem
Perfil
Planta
Regiao
Trimestre
Unit
Quantidade
LiquidoR$
SomadeBrutoR$
SomadeLiquidoE$
SomadeLiquidoU$
*)
  Info := '';
  Screen.Cursor := crHourGlass;
  Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo excel ...');
  if PB1 <> nil then
    PB1.Position := 0;
  Result := False;
  try
    XLApp := CreateOleObject('Excel.Application');
  except
    try
      XLApp := CreateOleObject('Works.Application');
    except
      //raise;
      Geral.MB_Aviso('N�o foi poss�vel iniciar uma inst�ncia do "Excel"!' +
      sLineBreak + 'Verifique se o aplicativo "Excel" est� instalado neste computador!"');
      Exit;
    end;
  end;
  try
    //MyObjects.LimpaGrade(AGrid, 1, 1, True);
    XLApp.Visible := False;
    XLApp.Workbooks.Open(AXLSFile);
    try
      Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[Planilha];
    except
      Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    end;
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;
    //AGrid.RowCount := x;
    SetLength(MyArrStr, X);
    //AGrid.ColCount := y + 1;
    for I := 0 to X - 1 do
      SetLength(MyArrStr[I], Y);
    if PB1 <> nil then
    begin
      PB1.Max := x * (y + 1);
      PB1.Update;
    end;
    Informa2(LaAviso1, LaAviso2, True, 'Lendo arquivo excel. Pode demorar se o arquivo conter muitos dados ...');
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;   // >... pode demorar
    k := 1; // 1?
    m := Round(CharWid * 5); // 2012-02-25 de "CharWid * 3" para "CharWid * 5"
    for n := 0 to 255 do ColWid[n] := m;
    repeat
      Informa2(LaAviso1, LaAviso2, True, 'Carregando linha ' + Geral.FF0(K));
      Application.ProcessMessages;  // lendo linha
      //AGrid.Cells[0, (k - 1)] := FormatFloat('000', (k - 1));
      for r := 1 to y do
      begin
        if PB1 <> nil then
        begin
          PB1.Position := PB1.Position + 1;
          PB1.Update;
        end;
        Application.ProcessMessages;
        //
        try
          Valor := RangeMatrix[K, R];
          if VarType(Valor) = 10 (*vt_error*) then
            Valor := '#ERROR';
        except
          on E: Exception do
          begin
             Info := 'Erro na linha ' + Geral.FF0(K) + ' coluna "' +
               dmkPF.IntToColTxt(R) + '"';
             Geral.MB_Erro('Erro de importa��o do excel: ' +
               E.Message + sLineBreak + Info);
          end;
        end;
        //AGrid.Cells[r, (k - 1)] := Valor;
        try
          MyArrStr[R, (K -1)] := Valor;
        except
          on E: Exception do
          begin
             Info := 'Erro na linha ' + Geral.FF0(K) + ' coluna "' +
               dmkPF.IntToColTxt(R) + '"' +  sLineBreak + 'Tipo de vari�vel: ' +
            GetEnumName(TypeInfo(TVarType), Integer(VarType(Valor)));
             Geral.MB_Erro('Erro de importa��o do excel: ' +
               E.Message + sLineBreak + Info);
          end;
        end;
        {
        Larg := Round((Length(Valor) + 1) * CharWid);
        if Larg > ColWid[r] then
          ColWid[r] := Larg;
        }
      end;
      Inc(k, 1);
      //AGrid.RowCount := k + 2;
    until k > x;
    if PB1 <> nil then
    begin
      PB1.Position := PB1.Max;
      PB1.Update;
    end;
    Informa2(LaAviso1, LaAviso2, True, 'Limpando mem�ria ...');
    RangeMatrix := Unassigned; // Limpando mem�ria
    // Deixar vazio para concatena��o limpa!
    Info := '';
  finally
    if Info = '' then
      Informa2(LaAviso1, LaAviso2, True, 'Fechando arquivo');
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.ActiveWorkBook.Saved:= 1;
      XLApp.DisplayAlerts:= 0;
      XLApp.ActiveWorkBook.Close(SaveChanges:= 0);
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
{
  Informa2(LaAviso1, LaAviso2, True, 'Ajustando largura de colunas ...');
  for n := 0 to AGrid.ColCount -1 do
    AGrid.ColWidths[n] := ColWid[n];
}
  //
  Informa2(LaAviso1, LaAviso2, False, Info);
  //
end;

function TUnMyObjects.Xls_To_StringGrid(AGrid: TStringGrid;
  AXLSFile: String; PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
  Planilha: Word; MaxLin: Integer): Boolean;
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
{
xlPivotCellValue	=0;
xlPivotCellPivotItem	=1;
xlPivotCellSubtotal	=2;
xlPivotCellGrandTotal	=3;
xlPivotCellDataField	=4;
xlPivotCellPivotField	=5;
xlPivotCellPageFieldItem	=6;
xlPivotCellCustomSubtotal	=7;
xlPivotCellDataPivotField	=8;
xlPivotCellBlankCell	=9;
var
  myPivotCache: OleVariant;
  Tabelas, PTI: Integer;
  PT_Txt: String;
}
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r, Larg, m, n: Integer;
  ColWid: array[0..255] of Integer;
  Info: String;
  Valor: Variant;
begin
  Info := '';
  Screen.Cursor := crHourGlass;
  Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo excel ...');
  if PB1 <> nil then
    PB1.Position := 0;
  Result := False;
  try
    XLApp := CreateOleObject('Excel.Application');
  except
    try
      XLApp := CreateOleObject('Works.Application');
    except
      //raise;
      Geral.MB_Aviso('N�o foi poss�vel iniciar uma inst�ncia do "Excel"!' +
      sLineBreak + 'Verifique se o aplicativo "Excel" est� instalado neste computador!"');
      Exit;
    end;
  end;
  try
    AGrid.Visible := False;
    MyObjects.LimpaGrade(AGrid, 1, 1, True);
    XLApp.Visible := False;
    XLApp.Workbooks.Open(AXLSFile);
    try
      Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[Planilha];
    except
      Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    end;
//
{
    Tabelas := Sheet.PivotCaches.Count;
    if Tabelas > 0 then
    begin
      PTI := Sheet.Range('B29').PivotCell.PivotCellType;
      case PTI of
        xlPivotCellValue             : PT_Txt  := 'xlPivotCellValue';
        xlPivotCellPivotItem         : PT_Txt  := 'xlPivotCellPivotItem';
        xlPivotCellSubtotal          : PT_Txt  := 'xlPivotCellSubtotal';
        xlPivotCellGrandTotal        : PT_Txt  := 'xlPivotCellGrandTotal';
        xlPivotCellDataField         : PT_Txt  := 'xlPivotCellDataField';
        xlPivotCellPivotField        : PT_Txt  := 'xlPivotCellPivotField';
        xlPivotCellPageFieldItem     : PT_Txt  := 'xlPivotCellPageFieldItem';
        xlPivotCellCustomSubtotal    : PT_Txt  := 'xlPivotCellCustomSubtotal';
        xlPivotCellDataPivotField    : PT_Txt  := 'xlPivotCellDataPivotField';
        xlPivotCellBlankCell         : PT_Txt  := 'xlPivotCellBlankCell';
        else                           PT_Txt  := '???';
      end;
      Geral.MB_Info(PT_TxT);
      //
      if Sheet.Range('B29').PivotCell.PivotCellType = xlPivotCellValue Then
        Geral.MB_Info('The PivotCell at B29 is a data item.');
      myPivotCache := Sheet.PivotCaches[0];
    end;
}

    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    X := XLApp.ActiveCell.Row;
    if MaxLin > 0 then
      if X > MaxLin then
        X := MaxLin + 1;
    y := XLApp.ActiveCell.Column;
    AGrid.RowCount := x;
    AGrid.ColCount := y + 1;
    if PB1 <> nil then
    begin
      PB1.Max := x * (y + 1);
      PB1.Update;
    end;
    Informa2(LaAviso1, LaAviso2, True, 'Lendo arquivo excel. Pode demorar se o arquivo conter muitos dados ...');
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;   // >... pode demorar
    k := 1; // 1?
    m := Round(CharWid * 5); // 2012-02-25 de "CharWid * 3" para "CharWid * 5"
    for n := 0 to 255 do
      ColWid[n] := m;
    repeat
      Informa2(LaAviso1, LaAviso2, True, 'Carregando linha ' + Geral.FF0(K));
      Application.ProcessMessages;  // lendo linha
      AGrid.Cells[0, (k - 1)] := FormatFloat('000', (k - 1));
      for r := 1 to y do
      begin
        if PB1 <> nil then
        begin
          PB1.Position := PB1.Position + 1;
          PB1.Update;
        end;
        Application.ProcessMessages;
        //
        try
          Valor := RangeMatrix[K, R];
          if VarType(Valor) = 10 (*vt_error*) then
            Valor := '#ERROR';
        except
          on E: Exception do
          begin
             Info := 'Erro na linha ' + Geral.FF0(K) + ' coluna "' +
               dmkPF.IntToColTxt(R) + '"';
             Geral.MB_Erro('Erro de importa��o do excel: ' +
               E.Message + sLineBreak + Info);
          end;
        end;
        AGrid.Cells[r, (k - 1)] := Valor;
        Larg := Round((Length(Valor) + 1) * CharWid);
        if Larg > ColWid[r] then
          ColWid[r] := Larg;
      end;
      Inc(k, 1);
      AGrid.RowCount := k + 2;
    until k > x;
    if PB1 <> nil then
    begin
      PB1.Position := PB1.Max;
      PB1.Update;
    end;
    Informa2(LaAviso1, LaAviso2, True, 'Limpando mem�ria ...');
    RangeMatrix := Unassigned; // Limpando mem�ria
    // Deixar vazio para concatena��o limpa!
    Info := '';
  finally
    AGrid.Visible := True;
    if Info = '' then
      Informa2(LaAviso1, LaAviso2, True, 'Fechando arquivo');
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.ActiveWorkBook.Saved:= 1;
      XLApp.DisplayAlerts:= 0;
      XLApp.ActiveWorkBook.Close(SaveChanges:= 0);
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
  Informa2(LaAviso1, LaAviso2, True, 'Ajustando largura de colunas ...');
  for n := 0 to AGrid.ColCount -1 do
    AGrid.ColWidths[n] := ColWid[n];
  //
  Informa2(LaAviso1, LaAviso2, False, Info);
end;

function TUnMyObjects.Xls_To_StringGrid_Faster(AGrid: TStringGrid; AXLSFile,
  ASheetName: String; PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
  MeErros: TMemo; Planilha: Word = 1; MaxLin: Integer = 0): Boolean;
var
  Xlapp1, Sheet: Variant;
  MaxRow, MaxCol, X, Y, CellCount, IntrvCount, IntrvStep, TotCount: Integer;
  Str, MsgErr: String;
  ArrData: Variant;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  try
    Str := Trim(AXLSFile);

    Informa2(LaAviso1, LaAviso2, True, 'Abrindo inst�ncia do Excel');
    XLApp1 := CreateOleObject('excel.application');
    AGrid.Visible := False;
    MyObjects.LimpaGrade(AGrid, 1, 1, True);
    XLApp1.Visible := False;

    Informa2(LaAviso1, LaAviso2, True, 'Inst�ncia do Excel criada. Abrindo arquivo');
    XLApp1.Workbooks.Open(Str);


    //Sheet := XLApp1.WorkSheets[SheetIndex]; = 1?
    Informa2(LaAviso1, LaAviso2, True, 'Arquivo aberto. Buscando planilha (guia)');
    if ASheetName <> EmptyStr then
    begin
      try
        Sheet := XLApp1.ActiveWorkbook.Worksheets[ASheetName];
      except
        Sheet := XLApp1.ActiveWorkbook.Worksheets[1];
      end;
    end else
      Sheet := XLApp1.ActiveWorkbook.Worksheets[1];
      //
    MaxRow := Sheet.UsedRange.EntireRow.Count ;
    MaxCol := sheet.UsedRange.EntireColumn.Count;
    TotCount := MaxRow * MaxCol;
    IntrvStep := Trunc(TotCount / 100);
    Informa2(LaAviso1, LaAviso2, True, 'Buscando planilha "' + ASheetName +
    '" aberta. Colunas: ' + Geral.FF0(MaxCol) + ' Linhas: ' + Geral.FF0(MaxRow) +
    '. Abrindo Planilha');

    //read the used range to a variant array
    ArrData:= Sheet.UsedRange.Value;

    Informa2(LaAviso1, LaAviso2, True, 'Planilha aberta. Carregando dados: 0%');
    if MaxLin > 0 then
      AGrid.RowCount := MaxLin + 1
    else
      AGrid.RowCount := maxRow + 1;
    AGrid.ColCount := maxCol + 1;
    CellCount  := 0;
    IntrvCount := IntrvStep;
    for x := 1 to maxCol do
    begin
      for y := 1 to maxRow do
      begin
        //if (maxRow = 0) or (Y <= maxRow)  then
        if (MaxLin > 0) and (Y > MaxLin) then
          Break;
        begin
          AGrid.Cells[0,y-1] := IntToStr(y-1);
          //
          CellCount := CellCount + 1;
          if CellCount >= IntrvCount then
          begin
            Informa2(LaAviso1, LaAviso2, True,
            'Planilha aberta. Carregando dados: ' +
            Geral.FF0(Trunc((CellCount / TotCount) * 100)) + '%');
            //
            IntrvCount := IntrvCount + IntrvStep;
          end;
          //copy data to grid
          try
            if VarType(ArrData[y, x]) <> varError then
              AGrid.Cells[x,y-1] := ArrData[y, x] // do note that the indices are reversed (y, x)
            else
              AGrid.Cells[x,y-1] := '';
          except
            MsgErr := 'Erro ao ler c�lula Col(X) = ' + Geral.FF0(x) +
              ' Lin(Y) = ' + Geral.FF0(y) + '  -  Valor = "' +
              Geral.VariavelToString(ArrData[y, x]);
            //
            if MeErros <> nil then
              MeErros.Lines.Add(MsgErr)
            else
              Geral.MB_Erro(MsgErr);
          end;
        end;
      end;
    end;
    //XLApp1.Workbooks.Close;
(*
    AGrid.Visible := True;
    //
    ArrData := Unassigned;
    Xlapp1.ActiveWorkBook.Saved:= 1;
    Xlapp1.DisplayAlerts:= 0;
    Xlapp1.ActiveWorkBook.Close(SaveChanges:= 0);
    Xlapp1.Quit;
    Xlapp1 := Unassigned;
    Sheet := Unassigned;
*)
    //
    Result := True;
  finally
    Screen.Cursor := crDefault;
    //
    AGrid.Visible := True;
    //
    ArrData := Unassigned;
    Xlapp1.ActiveWorkBook.Saved:= 1;
    Xlapp1.DisplayAlerts:= 0;
    Xlapp1.ActiveWorkBook.Close(SaveChanges:= 0);
    Xlapp1.Quit;
    Xlapp1 := Unassigned;
    Sheet := Unassigned;
      //
    Informa2(LaAviso1, LaAviso2, True, 'Processo finalizado! ' +
    Geral.FF0(Trunc((CellCount / TotCount) * 100)) + '% dos dados foram carregados.');
  end;
end;

function TUnMyObjects.Xls_To_StringGrid_OnlyClose(AGrid: TStringGrid;
  AXLSFile: String; PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
  var XLApp: OLEVariant): Boolean;
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r, Larg, m, n: Integer;
  ColWid: array[0..255] of Integer;
  Info: String;
  Valor: Variant;
begin
  if Info = '' then
    Informa2(LaAviso1, LaAviso2, True, 'Fechando arquivo');
  if not VarIsEmpty(XLApp) then
  begin
    XLApp.ActiveWorkBook.Saved:= 1;
    XLApp.DisplayAlerts:= 0;
    XLApp.ActiveWorkBook.Close(SaveChanges:= 0);
    XLApp.Quit;
    XLAPP := Unassigned;
    Sheet := Unassigned;
    Result := True;
  end;
  Screen.Cursor := crDefault;
  Informa2(LaAviso1, LaAviso2, True, 'Ajustando largura de colunas ...');
  for n := 0 to AGrid.ColCount -1 do
    AGrid.ColWidths[n] := ColWid[n];
  //
  Informa2(LaAviso1, LaAviso2, False, Info);
end;

function TUnMyObjects.Xls_To_StringGrid_OnlyOpen(AGrid: TStringGrid;
  AXLSFile: String; PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
  var XLApp: OLEVariant): Boolean;
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  RangeMatrix: Variant;
  x, y, k, r, Larg, m, n: Integer;
  ColWid: array[0..255] of Integer;
  Info: String;
  Valor: Variant;
begin
  Info := '';
  Screen.Cursor := crHourGlass;
  Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo excel ...');
  if PB1 <> nil then
    PB1.Position := 0;
  Result := False;
  try
    XLApp := CreateOleObject('Excel.Application');
    XLApp.Visible := False;
  except
    try
      XLApp := CreateOleObject('Works.Application');
    except
      //raise;
      Geral.MB_Aviso('N�o foi poss�vel iniciar uma inst�ncia do "Excel"!' +
      sLineBreak + 'Verifique se o aplicativo "Excel" est� instalado neste computador!"');
      Exit;
    end;
  end;
end;
    //
{
    XLApp.Workbooks.Open(AXLSFile);
    Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;
    AGrid.RowCount := x;
    AGrid.ColCount := y + 1;
    if PB1 <> nil then
    begin
      PB1.Max := x * (y + 1);
      PB1.Update;
    end;
    Informa2(LaAviso1, LaAviso2, True, 'Lendo arquivo excel. Pode demorar se o arquivo conter muitos dados ...');
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;   // >... pode demorar
    k := 1; // 1?
    m := Round(CharWid * 5); // 2012-02-25 de "CharWid * 3" para "CharWid * 5"
    for n := 0 to 255 do ColWid[n] := m;
    repeat
      Informa2(LaAviso1, LaAviso2, True, 'Carregando linha ' + Geral.FF0(K));
      Application.ProcessMessages;  // lendo linha
      AGrid.Cells[0, (k - 1)] := FormatFloat('000', (k - 1));
      for r := 1 to y do
      begin
        if PB1 <> nil then
        begin
          PB1.Position := PB1.Position + 1;
          PB1.Update;
        end;
        Application.ProcessMessages;
        //
        try
          Valor := RangeMatrix[K, R];
          if VarType(Valor) = 10 (*vt_error*) then
            Valor := '#ERROR';
        except
          on E: Exception do
          begin
             Info := 'Erro na linha ' + Geral.FF0(K) + ' coluna "' +
               dmkPF.IntToColTxt(R) + '"';
             Geral.MB_Erro('Erro de importa��o do excel: ' +
               E.Message + sLineBreak + Info);
          end;
        end;
        AGrid.Cells[r, (k - 1)] := Valor;
        Larg := Round((Length(Valor) + 1) * CharWid);
        if Larg > ColWid[r] then
          ColWid[r] := Larg;
      end;
      Inc(k, 1);
      AGrid.RowCount := k + 2;
    until k > x;
    if PB1 <> nil then
    begin
      PB1.Position := PB1.Max;
      PB1.Update;
    end;
    Informa2(LaAviso1, LaAviso2, True, 'Limpando mem�ria ...');
    RangeMatrix := Unassigned; // Limpando mem�ria
    // Deixar vazio para concatena��o limpa!
    Info := '';
  finally
    if Info = '' then
      Informa2(LaAviso1, LaAviso2, True, 'Fechando arquivo');
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.ActiveWorkBook.Saved:= 1;
      XLApp.DisplayAlerts:= 0;
      XLApp.ActiveWorkBook.Close(SaveChanges:= 0);
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
  Informa2(LaAviso1, LaAviso2, True, 'Ajustando largura de colunas ...');
  for n := 0 to AGrid.ColCount -1 do
    AGrid.ColWidths[n] := ColWid[n];
  //
  Informa2(LaAviso1, LaAviso2, False, Info);
}

function TUnMyObjects.Xls_To_StringGrid_OnlyProcess(AGrid: TStringGrid;
  AXLSFile: String; PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
  var XLApp: OLEVariant): Boolean;
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r, Larg, m, n: Integer;
  ColWid: array[0..255] of Integer;
  Info: String;
  Valor: Variant;
begin
  try
    XLApp.Workbooks.Open(AXLSFile);
    Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;
    AGrid.RowCount := x;
    AGrid.ColCount := y + 1;
    if PB1 <> nil then
    begin
      PB1.Max := x * (y + 1);
      PB1.Update;
    end;
    Informa2(LaAviso1, LaAviso2, True, 'Lendo arquivo excel. Pode demorar se o arquivo conter muitos dados ...');
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;   // >... pode demorar
    k := 1; // 1?
    m := Round(CharWid * 5); // 2012-02-25 de "CharWid * 3" para "CharWid * 5"
    for n := 0 to 255 do ColWid[n] := m;
    repeat
      Informa2(LaAviso1, LaAviso2, True, 'Carregando linha ' + Geral.FF0(K));
      Application.ProcessMessages;  // lendo linha
      AGrid.Cells[0, (k - 1)] := FormatFloat('000', (k - 1));
      for r := 1 to y do
      begin
        if PB1 <> nil then
        begin
          PB1.Position := PB1.Position + 1;
          PB1.Update;
        end;
        Application.ProcessMessages;
        //
        try
          Valor := RangeMatrix[K, R];
          if VarType(Valor) = 10 (*vt_error*) then
            Valor := '#ERROR';
        except
          on E: Exception do
          begin
             Info := 'Erro na linha ' + Geral.FF0(K) + ' coluna "' +
               dmkPF.IntToColTxt(R) + '"';
             Geral.MB_Erro('Erro de importa��o do excel: ' +
               E.Message + sLineBreak + Info);
          end;
        end;
        AGrid.Cells[r, (k - 1)] := Valor;
        Larg := Round((Length(Valor) + 1) * CharWid);
        if Larg > ColWid[r] then
          ColWid[r] := Larg;
      end;
      Inc(k, 1);
      AGrid.RowCount := k + 2;
    until k > x;
    if PB1 <> nil then
    begin
      PB1.Position := PB1.Max;
      PB1.Update;
    end;
    Informa2(LaAviso1, LaAviso2, True, 'Limpando mem�ria ...');
    RangeMatrix := Unassigned; // Limpando mem�ria
    // Deixar vazio para concatena��o limpa!
    Info := '';
  except
    // ver o que fazer
  end;
end;

procedure TUnMyObjects.LarguraAutomaticaGrade(AGrid: TStringGrid);
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  Col, Row, T, L: Integer;
  ColWid: array[0..255] of Integer;
begin
  T := Round(CharWid * 5); // 2012-02-25 de "CharWid * 3" para "CharWid * 5"
  for Col := 1 to 255 do ColWid[Col] := T;
  for Row := 1 to AGrid.RowCount - 1 do
  begin
    for Col := 1 to 255 do
    begin
      L := Round((Length(AGrid.Cells[Col, Row]) + 1) * CharWid);
      if L > ColWid[Col] then
        ColWid[Col] := L;
    end;
  end;
  for Col := 1 to AGrid.ColCount -1 do
    AGrid.ColWidths[Col] := ColWid[Col];
end;

procedure TUnMyObjects.AdicionaTextoCorRichEdit(RichEdit: TRichEdit;
  Cor: TColor; EstilosFonte: TFontStyles; Texto: String; AtHead: Boolean);
begin
  if RichEdit <> nil then
  begin
    if AtHead then
      RichEdit.SelStart := 0;
    RichEdit.SelAttributes.Color := Cor;
    RichEdit.SelAttributes.Style := EstilosFonte;
    RichEdit.SelText := Texto;
    RichEdit.Update;
    Application.ProcessMessages;
  end;
end;

procedure TUnMyObjects.AjustaFonte(MyLabel: TLabel; Qualidade: Integer);
var
  tagLOGFONT: TLogFont;
  Font: TFont;
begin
  Font := MyLabel.Font;
  GetObject(
    Font.Handle,
    SizeOf(TLogFont),
    @tagLOGFONT);
  //
  tagLOGFONT.lfQuality  := Qualidade;
  {
  tagLOGFONT.lfQuality  := DEFAULT_QUALITY;
  tagLOGFONT.lfQuality  := DRAFT_QUALITY;
  tagLOGFONT.lfQuality  := PROOF_QUALITY;
  tagLOGFONT.lfQuality  := NONANTIALIASED_QUALITY;
  tagLOGFONT.lfQuality  := ANTIALIASED_QUALITY;
  }
  MyLabel.Font.Handle := CreateFontIndirect(tagLOGFONT);
end;

procedure TUnMyObjects.AjustarCaptionAbas(ClasseForm: TFormClass; PageControl:
  TPageControl);
var
  I, Indice, TotalForms: Integer;
begin
  TotalForms := TotalFormsAbertos(ClasseForm, PageControl);

  if TotalForms > 1 then
  begin
    Indice := 1;
    for I := 0 to PageControl.PageCount - 1 do
      with PageControl do
        if Pages[I].Components[0].ClassType = ClasseForm then
        begin
          Pages[I].Caption := Copy((Pages[I].Components[0] as TForm).Caption, 17) + ' (' +
            IntToStr(Indice) + ')';
          Inc(Indice);
        end;
  end;
end;

procedure TUnMyObjects.AjustaTamanhoMaximoColunaDBGrid(Grid: Vcl.DBGrids.TDBGrid;
  Campo: String; MinLarg: Integer);
var
  K, I, N: Integer;
  Txt: String;
begin
  Txt := '';
  N := 25 + THackDBGrid(Grid).ColWidths[0]; // 25 = Laterais do grid (4 + 4) + Scrollbar (16) + 1 de folga
  K := 0;
  for I := 0 to Grid.Columns.Count - 1 do
  begin
    if Uppercase(Grid.Columns[I].FieldName) <> Uppercase(Campo) then
      N := N + THackDBGrid(Grid).ColWidths[I+1]
    else
      K := I+1;
  end;
  if K <> 0 then
  begin
    N := Grid.Width - N;
    if N < MinLarg then
      N := MinLarg;
    THackDBGrid(Grid).ColWidths[K] := N;
  end;
end;

procedure TUnMyObjects.AlteraCaptionBtInsUpd(BtInsUpd: TBitBtn;
  Altera: Boolean);
begin
  if Altera then
  begin
    BtInsUpd.Caption := '&Altera';
    BtInsUpd.Tag := 11;
  end else begin
    BtInsUpd.Caption := '&Insere';
    BtInsUpd.Tag := 10;
  end;
  BtInsUpd.Glyph := nil;
{$IFNDEF NAO_USA_GLYFS}
  FmMyGlyfs.DefineGlyphBtnTag(BtInsUpd);
{$ENDIF}
end;

function TUnMyObjects.CBAnoECBMesToPeriodoTxt(CBAno, CBMes: TComboBox): String;
var
  Ano, Mes: Word;
begin
  Ano := Geral.IMV(CBAno.Text);
  Mes := CBMes.ItemIndex + 1;
  Result := FormatDateTime('MMMM" de "YYYY', EncodeDate(Ano, Mes, 1));
end;

procedure TUnMyObjects.CBAnoMesFromPeriodo(Periodo: Integer; CBAno,
  CBMes: TComboBox; Avisa: Boolean);
var
  I: Integer;
  Ano, Mes: Word;
  Achou: Boolean;
  Ano_Txt: String;
begin
  if (CBMes <> nil) and (CBAno <> nil) then
  begin
    if Periodo = 0 then
    begin
      CBMes.ItemIndex := -1;
      CBMes.Text := '';
      CBAno.ItemIndex := -1;
      CBAno.Text := '';
    end else begin
      dmkPF.PeriodoDecode(Periodo, Ano, Mes);
      Ano_Txt := FormatFloat('0', Ano);
      CBMes.ItemIndex := Mes -1;
      Achou := False;
      for I := 0 to CBAno.Items.Count - 1 do
        if CBAno.Items[I] = Ano_Txt then
        begin
          CBAno.ItemIndex := I;
          Achou := CBAno.ItemIndex = I;
        end;
      if not Achou and Avisa then
        Geral.MB_Aviso('N�o foi poss�vel setar o ano ' + Ano_Txt + sLineBreak +
        'no componente "' + CBAno.Name + '"');
    end;
  end;
end;

function TUnMyObjects.CelulaDBGrid_PosDownRight(const Grade: Vcl.DBGrids.TDBGrid; const
  MoreWid, MoreTop: Integer; const CorrigePosicao: Boolean; var Lf, Tp:
  Integer): Boolean;
var
  pt: TPoint;
  rt: TRect;
  //
  I, X,Y, Lin, Col, Hei, Wid: Integer;
begin
  Hei  := 0;
  Wid  := 0;
  //
  Lf   := 0;
  Tp   := 0;
  //
  GetCursorPos(pt);
(*
  if ComponentePai is TBitBtn then GetWindowRect(TBitBtn(ComponentePai).Handle, rt)
  else if ComponentePai is TButton then GetWindowRect(TButton(ComponentePai).Handle, rt)
  //else if ComponentePai is T L M D Edit then GetWindowRect(T L M D Edit(ComponentePai).Handle, rt)
  else if ComponentePai is TListView then GetWindowRect(TListView(ComponentePai).Handle, rt)
  else if ComponentePai is TDBGrid then
  begin
*)
    GetWindowRect(Grade.Handle, rt);
    Lin := THackDBGrid(Grade).Row;
    for I := 0 to Lin do
      Hei := Hei + THackDBGrid(Grade).RowHeights[I];
    Col := THackDBGrid(Grade).Col;
    for I := 0 to Col - 1 do
      Wid := Wid + THackDBGrid(Grade).ColWidths[I];
    Wid := Wid + 6;
    Hei := Hei + 12;
(*
  end else if ComponentePai is TDrawGrid then // TStringGrid
  begin
    GetWindowRect(TDrawGrid(ComponentePai).Handle, rt);
    lin := TDrawGrid(ComponentePai).Row;
    for i := 0 to lin do Hei := Hei + TDrawGrid(ComponentePai).RowHeights[i];
    col := TDrawGrid(ComponentePai).Col;
    for i := 0 to Col-1 do Wid := Wid + TDrawGrid(ComponentePai).ColWidths[i];
    Wid := Wid + 6;
    Hei := Hei + 12;
  end else begin
    Geral.MB_Erro('Componente pai n�o definido para mostrar popup!' +
    sLineBreak + 'Classe do objeto: ' + TObject(ComponentePai).ClassName);
    Exit;
  end;
*)
  Y := rt.Left + Wid + MoreWid;
  (***X := (PM.Items.Count * 19) + 6;
  if ComponentePai is TDBGrid then***)
    X := rt.Top + Hei (***else
  begin
    if (X + rt.Bottom) > (Screen.Height - 50)
      then X := rt.Top - X else X := rt.Bottom;
  end***)
  ;
  X := X + MoreTop;
  //PM.Popup(y, x);

  Lf := Y;
  Tp  := X;
  //
end;

function TUnMyObjects.ChangeValorDmk(ComponentClass: TComponentClass;
  Reference: TComponent; FormatType: TAllFormat; DefaultAtu,
  DefaultNew: Variant; CasasAtu, CasasNew, LeftZerosAtu, LeftZerosNew: Integer;
  ValMinAtu, ValMaxAtu, ValMinNew, ValMaxNew: String; Obrigatorio: Boolean;
  FormCaption, ValCaptionAtu, ValCaptionNew: String; WidthVal: Integer;
  var ResultadoAtu, ResultadoNew: Variant): Boolean;
var
  i: Integer;

  begin
  try
    Screen.Cursor := crHourGlass;
    try
      MyObjects.FormCria(ComponentClass, Reference);
      TForm(Reference).Caption := 'XXX-XXXXX-011 :: ' + FormCaption;
      with Reference do
      begin
        for i := 0 to ComponentCount -1 do
        begin
          if (Components[i] is TLabel) then
          begin
            if TLabel(Components[i]).Name = 'LaValAtu' then
            begin
              TLabel(Components[i]).Caption := ValCaptionAtu;
              TLabel(Components[i]).Visible := True;
            end else
            if TLabel(Components[i]).Name = 'LaValNew' then
            begin
              TLabel(Components[i]).Caption := ValCaptionNew;
              TLabel(Components[i]).Visible := True;
            end;
          end else
          if (Components[i] is TdmkEdit) then
          begin
            if TdmkEdit(Components[i]).Name = 'EdValAtu' then
            begin
               TdmkEdit(Components[i]).Visible      := True;
               //TdmkEdit(Components[i]).DataFormat   := ;
               //TdmkEdit(Components[i]).HoraFormat   := ;
               //TdmkEdit(Components[i]).ForceNextYear   := ;
               TdmkEdit(Components[i]).DecimalSize  := CasasAtu;
               TdmkEdit(Components[i]).LeftZeros    := LeftZerosAtu;
               TdmkEdit(Components[i]).FormatType   := FormatType;
               TdmkEdit(Components[i]).Obrigatorio  := Obrigatorio;
               TdmkEdit(Components[i]).ValMax       := ValMaxAtu;
               TdmkEdit(Components[i]).ValMin       := ValMinAtu;
               TdmkEdit(Components[i]).ValueVariant := DefaultAtu;
            end else
            if TdmkEdit(Components[i]).Name = 'EdValNew' then
            begin
               TdmkEdit(Components[i]).Visible      := True;
               //TdmkEdit(Components[i]).DataFormat   := ;
               //TdmkEdit(Components[i]).HoraFormat   := ;
               //TdmkEdit(Components[i]).ForceNextYear   := ;
               TdmkEdit(Components[i]).DecimalSize  := CasasNew;
               TdmkEdit(Components[i]).LeftZeros    := LeftZerosNew;
               TdmkEdit(Components[i]).FormatType   := FormatType;
               TdmkEdit(Components[i]).Obrigatorio  := Obrigatorio;
               TdmkEdit(Components[i]).ValMax       := ValMaxNew;
               TdmkEdit(Components[i]).ValMin       := ValMinNew;
               TdmkEdit(Components[i]).ValueVariant := DefaultNew;
            end;
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
      TForm(Reference).ShowModal;
    end;
    Result        := VAR_GETVALOR_GET;
    if Result then
    begin
      with Reference do
      begin
        for i := 0 to ComponentCount -1 do
        begin
          if (Components[i] is TdmkEdit) then
          begin
            if TdmkEdit(Components[i]).Name = 'EdValAtu' then
              ResultadoAtu := TdmkEdit(Components[i]).ValueVariant
            else
            if TdmkEdit(Components[i]).Name = 'EdValNew' then
              ResultadoNew := TdmkEdit(Components[i]).ValueVariant
            else
            ;
          end;
        end;
      end;
    end;
    Reference.Destroy;
    Screen.Cursor := crDefault;
  except
    //Result    := False;
    ResultadoAtu := NULL;
    ResultadoNew := NULL;
    raise;
  end;
end;

procedure TUnMyObjects.ColoreSimNaoEmDBGrid(const DBGrid: Vcl.DBGrids.TDBGrid;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState;
  const NomeCampo, SimNao: String);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = NomeCampo) then
  begin
    //Txt := clWindow;
    //Bak := clBlack;
    if Uppercase(SimNao) = 'SIM' then
    begin
      Txt := clBlue ;
      Bak := clWindow
    end else
    begin
      Txt := clRed;
      Bak := $009FFFFF; // Amarelo clarinho
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
end;

function TUnMyObjects.CompactaArquivo(Arquivo, PastaOri, PastaDest: String;
Memo: TMemo; ExcluiArqOri: Boolean; PastaDoZip: String): Boolean;
var
  ExecDOS : PChar;
  i, Verificacoes: Integer;
  ArqOri, ArqDes, Options, ZipPath, Origem, Destino: String;
begin
//C:\PROGRA~1\WinZip\wzzip C:\_PARTI~1\Tmp\Cred1100512_113924.zip C:\_Particular\Tmp\Cred1100512_113924.sql
  Result := False;
  try
    Options := '';
    Origem := PastaOri;
    Geral.VerificaDir(Origem, '\', 'Compacta��o de arquivo', True);
    Destino := PastaDest;
    Geral.VerificaDir(Destino, '\', 'Compacta��o de arquivo', True);
    // Zipa arquivos criados no dir A: e C:
    if Destino[1] = 'A' then Options := Options + '&';
    // s� funciona se o arquivo existe!
    //if SobrepoeZip then Options := Options + 'w'; // exclui anterior?
    if Options <> '' then Options := ' -' + Options;
    Verificacoes := 500;
    /////////////////////////////////////////////////////////////////////////////
    // -yp deixa DOS Aberto
    // -& (Multiplos discos)
    /////////////////////////////////////////////////////////////////////////////
    //ZipPath := FmWetBlueMLA_BK.EdZipPath.Text;
    ZipPath := PastaDoZip;
    if not FileExists(ZipPath+'\wzzip.exe') then
    begin
      if Memo <> nil then
        Memo.Lines.Add('O arquivo "'+ZipPath+'\wzzip.exe" n�o foi encontrado!');
      raise EAbort.Create('');
    end else begin
      ZipPath := String(dmkPF.NomeLongoparaCurto(AnsiString(ZipPath)));
      ArqOri  := ExtractShortPathName(Origem) + Arquivo;
      ArqDes  := ExtractShortPathName(Destino) + dmkPF.MudaExtensaoDeArquivo(Arquivo, 'zip');
      //
      ExecDOS := PChar(PChar(ZipPath + '\wzzip' + Options + ' ' + ArqDes + ' ' + ArqOri));
      if Memo <> nil then
      begin
        Memo.Lines.Add('');
        Memo.Lines.Add('*** Linha de comando: ***');
        Memo.Lines.Add(ExecDOS);
        Memo.Lines.Add('*** Fim linha de comando ***');
        Memo.Lines.Add('');
      end;
      (*
      Geral.MB_Aviso(ExecDos, 'Exec DOS');
      Exit;
      *)
      try
        //WinExec(ExecDOS, SW_SHOW);
        ExecutaCmd(ExecDOS, Memo);
        //dmkPF.WinExecAndWaitOrNot(ExecDOS, SW_SHOW, nil, True);
        // Erro!!!!!!!!!!!
        // M L A G e r a l.WinExecAndWait32(ExecDOS, SW_HIDE, Memo2);
        // M L A G e r a l.LeSaidaDOS(ExecDOS, Memo2);
      except
        Memo.Lines.Add('*** Compacta��o falhou! ***');
      end;
    end;
    for i := 1 to Verificacoes do
    begin
      Application.ProcessMessages;
      (*
      if FParar then
      begin
        FParar := False;
        Screen.Cursor := crDefault;
        Memo.Lines.Add('Compacta��o interrompida pelo usu�rio!');
        Break;
      end;
      *)
      Sleep(1000);
      if FileExists(ArqDes) then
      begin
        Result := True;
        if ExcluiArqOri then
        begin
          if FileExists(ArqOri) then
            DeleteFile(ArqOri);
        end;
        Break;
      end;
    end;
(*    ExecDOS := PChar(CO_DIR_RAIZ_DMK + '\wzzip ' + CO_DIR_RAIZ_DMK + '\Backups\'+ArqNome+
                     ' ' + CO_DIR_RAIZ_DMK + '\'+Arquivo);
    WinExec(ExecDOS, SW_HIDE);*)
    /////////////////////////////////////////////////////////////////////////////
  except
    if Memo <> nil then
      Memo.Lines.Add('Erro na compacta��o com Win Zip!');
  end;
end;

{ Substituido pela propriedade "NoForceUppercase"
function TUnMyObjects.CompoMinusculo(Nome: String): Boolean;
var
  N: String;
begin
  N := Uppercase(Nome);
  Result := False;
  if N = 'EDEMMAIL' then Result := True
  else if N = 'EDEEMMAIL' then Result := True
  else if N = 'EDPEMMAIL' then Result := True
end;
}

function TUnMyObjects.CompoMoeda(Caption: String): String;
var
  N: String;
  i: Integer;
begin
  if pos(VAR_MOEDA, Caption) = 1 then
    N := Caption
  else begin
    for i := 1 to Length(Caption) do
    begin
      if Caption[i] <> '$' then N := N + Caption[i]
      else N := N + VAR_MOEDA;
    end;
  end;
  Result := N;
end;

function TUnMyObjects.ComponentePertenceAoForm(Componente: TComponent;
  FormClass: TFormClass): Boolean;
const
  sProcName = 'MyObjects.ComponentePertenceAoForm()';
begin
  Result := False;
  //
  if Componente <> nil then
  begin
      if TComponent(Componente).Owner <> nil then
        if TComponent(Componente).Owner is FormClass then
          Result := True;
  end else
    Geral.MB_Erro('Componente n�o definido em ' + sProcName);
end;

function TUnMyObjects.ComponenteTemAPropriedade(const Compo: TComponent; const
  NomeProp: String): Boolean;
var
  I, Count: Integer;
  PropInfo: PPropInfo;
  TempList: PPropList;
  LObject: TObject;
begin
  Result := False;
  Count := GetPropList(Compo, TempList);
  if Count > 0 then
  try
    for I := 0 to Count - 1 do
    begin
      PropInfo := TempList^[I];
      if GetPropName(PropInfo) = NomeProp then
      begin
        Result := True;
        Exit;
      end;
    end;
  finally
    FreeMem(TempList);
  end;
end;

procedure TUnMyObjects.ConfiguraAllFmtStr(RGAllFmtStr: TRadioGroup; Colunas,
  Default: Integer);
var
  I: Integer;
begin
  RGAllFmtStr.Columns := Colunas;
  RGAllFmtStr.Items.Clear;
  for I := 0 to Integer(dmktfUnknown) do
    RGAllFmtStr.Items.Add(Geral.ObtemTextoDeAllFormat(I));
  //
  RGAllFmtStr.ItemIndex := Default;
end;

procedure TUnMyObjects.ConfiguracaoRateioContas(DatabaseName: String;
  Lista: TStrings; Grade: TStringGrid);
var
  i, j: Integer;
begin
  Lista.Clear;
  if Grade <> nil  then
  begin
    for i := 0 to Grade.ColCount-1 do
    begin
      for j := 0 to Grade.RowCount-1 do Grade.Cells[i,j] := '';
    end;
  end;
  if Trim(DatabaseName) = '' then
    Geral.MB_Erro(
    'Database para configura��es de rateio de contas n�o definido!')
  else begin
    if Uppercase(DatabaseName) = 'ACADEMY' then
    begin
      (*0*)Lista.Add('Geral');
      (*1*)Lista.Add('Area m�');
      (*2*)Lista.Add('Energia kw');
      (*3*)Lista.Add('Instrutores');
      if Grade <> nil then
      begin
        Grade.Cells[00,00] := '';
        Grade.Cells[00,01] := '';
        Grade.Cells[00,02] := '';
        Grade.Cells[00,03] := 'Entidade';
      end;
    end else if (Uppercase(DatabaseName) = 'BLUEWET')
    or  (Uppercase(DatabaseName) = 'BLUESKIN')
    or  (Uppercase(DatabaseName) = 'BLUEDERM') then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = Uppercase('BUGSTROL') then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = Uppercase(CO_APP_TITLE_003) then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'EMPORIUM' then
    begin
      //Lista.Add('');
    end else
    if (Uppercase(DatabaseName) = 'EXULT')
    or (Uppercase(DatabaseName) = 'EXUL2') then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'FLAIX' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'GIGASTOR' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'INFRAJOB' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'IRENT' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'LOGIDEK' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'MEGASTOR' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'LEATHER' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'MYMONEY' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'OFERSALE' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'OUTGRAF' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'RECOVERY' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'SEDA' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'SEVEN' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'STORE' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'TAURUS' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'TEACH' then
    begin
      (*0*)Lista.Add('Geral');
      (*1*)Lista.Add('Area m�');
      (*2*)Lista.Add('Energia kw');
      (*3*)Lista.Add('Professores');
      if Grade <> nil then
      begin
        Grade.Cells[00,00] := '';
        Grade.Cells[00,01] := '';
        Grade.Cells[00,02] := '';
        Grade.Cells[00,03] := 'Entidade';
      end;
    end else if Uppercase(DatabaseName) = 'TOOLRENT' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'XIPMERT' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = 'WASHED' then
    begin
      //Lista.Add('');
    end else if Uppercase(DatabaseName) = '_PROJETO_BASE_4' then
    begin
      //Lista.Add('');
    end else
    begin
      //Lista.Add('');
    end;
  end;
  //
end;

procedure TUnMyObjects.ConfiguraCheckGroup(CheckGroup: TdmkCheckGroup;
  Itens: array of String; Colunas, Default: Integer; FirstAll: Boolean);
var
  I: Integer;
begin
  CheckGroup.Columns := Colunas;
  CheckGroup.Items.Clear;
  for I := Low(Itens) to High(Itens) do
    CheckGroup.Items.Add(Itens[I]);
  //
  if (Default = High(Integer)) then
    CheckGroup.SetMaxValue
  else
  if Default < -1  then
    CheckGroup.Value := -Default
  else
    CheckGroup.ItemIndex := Default;
  //
  if FirstAll then
    CheckGroup.Items[0] := 'TODOS';
end;

procedure TUnMyObjects.ConfiguraDBRadioGroup(DBRadioGroup: TDBRadioGroup;
  Itens: array of String; Colunas: Integer);
var
  I: Integer;
begin
  DBRadioGroup.Columns := Colunas;
  DBRadioGroup.Items.Clear;
  DBRadioGroup.Values.Clear;
  for I := Low(Itens) to High(Itens) do
  begin
    DBRadioGroup.Items.Add(Itens[I]);
    DBRadioGroup.Values.Add(Geral.FF0(I));
  end;
end;

procedure TUnMyObjects.ConfiguraF7AppCBGraGru1(CBGraGru1: TdmkDBLookupComboBox);
begin
  if CO_SIGLA_APP = 'TREN' then
  begin
    CBGraGru1.LocF7PreDefProc  := f7pNone;
    CBGraGru1.LocF7TableName   := EmptyStr;
    CBGraGru1.LocF7NameFldName := EmptyStr;
    CBGraGru1.LocF7SQLMasc := '$#';
    CBGraGru1.LocF7SQLText.Text := Geral.ATS([
    'SELECT lpc.GraGruX _CODIGO, gg1.REFERENCIA, gg1.Nome _NOME ',
    'FROM gragxvend lpc ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE gg1.Nome LIKE "%$#%" ',
    'OR gg1.Referencia LIKE "%$#%" ',
    'OR gg1.Patrimonio LIKE "%$#%" ',
    '']);
  end;
end;

procedure TUnMyObjects.ConfiguraRadioGroup(RadioGroup: TRadioGroup;
  Itens: array of String; Colunas, Default: Integer);
var
  I: Integer;
begin
  RadioGroup.Columns := Colunas;
  RadioGroup.Items.Clear;
  for I := Low(Itens) to High(Itens) do
    RadioGroup.Items.Add(Itens[I]);
  //
  RadioGroup.ItemIndex := Default;
end;

procedure TUnMyObjects.ConfiguraTipoCobranca(RGTipoCobranca: TRadioGroup;
  Colunas, Default: Integer);
begin
  RGTipoCobranca.Columns := Colunas;
  RGTipoCobranca.ItemIndex := Default;
  //
  RGTipoCobranca.Items.Clear;
  RGTipoCobranca.Items.Add((*[0] :=*) 'Padr�o');
  RGTipoCobranca.Items.Add((*[1] :=*) 'SIGCB - CEF');
end;

procedure TUnMyObjects.ControleCor(ScreenActiveForm: TForm);
var
  s: String;
begin
  begin
    try
      if VAR_MUDACORCOMP < 1 then Exit;
      if VAR_ATUALIZANDO then Exit;
      if ScreenActiveForm = nil then exit;
      if ScreenActiveForm.Name = IC2_NOMEFORMAPP then
       exit;
      if ScreenActiveForm.Name = 'FmLogoff' then
       exit;
      if ScreenActiveForm.Name = 'FmLocalBD' then
       exit;
      IC2_COMPATU := ScreenActiveForm.ActiveControl;
{
      if IC2_COMPATU is T L M D Edit then
      begin
         VAR_T L M D EDITNOME := T L M D Edit(IC2_COMPATU).Name;
         VAR_T L M D EDITNEW := VAR_T L M D EDITOLD;
         VAR_T L M D EDITOLD := T L M D Edit(IC2_COMPATU).Text;
      end;
}
      //
      if VAR_SEMCONTROLECOR then Exit;
      //
      if IC2_COMPANT = IC2_COMPATU then
      begin
        IC2_COMPFM := ScreenActiveForm.Name;
        Exit;
      end;
      if IC2_COMPFM <> ScreenActiveForm.Name then IC2_COMPANT := nil;
      if IC2_COMPANT <> nil then
      if IC2_COMPANT is TForm then
        if IC2_COMPFM = TForm(IC2_COMPANT).Name then
      begin
        IC2_COMPFM := ScreenActiveForm.Name;
        Exit;
      end;
      if IC2_COMPATU <> nil then
      begin
        if IC2_COMPATU.TabStop = False then
        begin
          IC2_COMPFM := ScreenActiveForm.Name;
          Exit;
        end;
        if TComponent(IC2_COMPATU).Tag = 999 then
        begin
          IC2_COMPFM := ScreenActiveForm.Name;
          Exit;
        end;
{
        if IC2_COMPATU is T L M D Edit then
        begin
           T L M D Edit(IC2_COMPATU).Color := IC2_AparenciasEditItemIn;
           T L M D Edit(IC2_COMPATU).Font.Color := IC2_AparenciasEditTextIn;
           VAR_T L M D EDITNOME := T L M D Edit(IC2_COMPATU).Name;
           VAR_T L M D EDITNEW := VAR_T L M D EDITOLD;
           VAR_T L M D EDITOLD := T L M D Edit(IC2_COMPATU).Text;
        end;
}
        //
        if IC2_COMPATU is TEdit then
           TEdit(IC2_COMPATU).Color := IC2_AparenciasEditItemIn;
        if IC2_COMPATU is TEdit then
           TEdit(IC2_COMPATU).Font.Color := IC2_AparenciasEditTextIn;
        //
        if IC2_COMPATU is TDBLookupComboBox then
           TDBLookupComboBox(IC2_COMPATU).Color := IC2_AparenciasEditItemIn;
        //
        if IC2_COMPATU is TDBLookupComboBox then
           TDBLookupComboBox(IC2_COMPATU).Font.Color := IC2_AparenciasEditTextIn;
        //
        if IC2_COMPATU is TListBox then
           TListBox(IC2_COMPATU).Font.Color := IC2_AparenciasEditTextIn;
        //
        if IC2_COMPATU is TMemo then
           TMemo(IC2_COMPATU).Color := IC2_AparenciasEditItemIn;
        if IC2_COMPATU is TMemo then
           TMemo(IC2_COMPATU).Font.Color := IC2_AparenciasEditTextIn;
        //
        if IC2_COMPATU is TStaticText then
           TStaticText(IC2_COMPATU).Color := IC2_AparenciasEditItemIn;
        if IC2_COMPATU is TStaticText then
           TStaticText(IC2_COMPATU).Font.Color := IC2_AparenciasEditTextIn;
      end;
      if IC2_COMPANT <> nil then
      begin
        if IC2_COMPANT is TForm then
        begin
          IC2_COMPFM := ScreenActiveForm.Name;
          Exit;
        end;
        if ScreenActiveForm.Name = '' then
        begin
          IC2_COMPFM := ScreenActiveForm.Name;
          Exit;
        end;
        if TComponent(IC2_COMPANT).Tag = 999 then
        begin
          IC2_COMPFM := ScreenActiveForm.Name;
          Exit;
        end;
{
        if IC2_COMPANT is T L M D Edit then
        begin
           T L M D Edit(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
           T L M D Edit(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
           if VAR_SOMAIUSCULAS then
           T L M D Edit(IC2_COMPANT).Text := ConverteMinuscEmMaiusc(T L M D Edit(IC2_COMPANT).Text);
        end;
}
        if IC2_COMPANT is TdmkEdit then
        begin
           TdmkEdit(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
           TdmkEdit(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
           if VAR_SOMAIUSCULAS then
           TdmkEdit(IC2_COMPANT).Text := dmkPF.ConverteMinuscEmMaiusc(TdmkEdit(IC2_COMPANT).Text);
        end;
        if IC2_COMPANT is TdmkEditCB then
        begin
           TdmkEditCB(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
           TdmkEditCB(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
           if VAR_SOMAIUSCULAS then
           TdmkEditCB(IC2_COMPANT).Text := dmkPF.ConverteMinuscEmMaiusc(TdmkEditCB(IC2_COMPANT).Text);
        end;
        if IC2_COMPANT is TEdit then
        begin
           TEdit(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
           TEdit(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
           if VAR_SOMAIUSCULAS then
           TEdit(IC2_COMPANT).Text := dmkPF.ConverteMinuscEmMaiusc(TEdit(IC2_COMPANT).Text);
        end;
        if IC2_COMPANT is TDBLookupComboBox then
        begin
           TDBLookupComboBox(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
           TDBLookupComboBox(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
        end;
        //
        if IC2_COMPANT is TListBox then
           TListBox(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
        //
        if IC2_COMPANT is TMemo then
           TMemo(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
        if IC2_COMPANT is TMemo then
           TMemo(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
        //
        if IC2_COMPANT is TStaticText then
           TStaticText(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
        if IC2_COMPANT is TStaticText then
           TStaticText(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
      end;
      IC2_COMPANT := IC2_COMPATU;
    finally
    end;
  end;
  IC2_COMPFM := ScreenActiveForm.Name;
  //SoMaiusculas(ScreenActiveForm.ActiveControl);  n�o funciona ????
end;

procedure TUnMyObjects.ControleStrings(ScreenActiveForm: TForm);
var
  i, j: Integer;
  Compo: TComponent;
  KeyboardState: TKeyboardState;
begin
  GetKeyBoardState(KeyboardState);
  for i := 0 to ScreenActiveForm.ComponentCount -1 do
  begin
    Compo := ScreenActiveForm.Components[i];
    ////////////////////////////////////////////////////////////////////////////
    if ScreenActiveForm.Components[i] is TDBGrid then
    begin
      for j := 0 to TDBGrid(Compo).Columns.Count-1 do
      begin
        if Pos('$', TDBGrid(Compo).Columns[j].Title.Caption) > 0 then
            TDBGrid(Compo).Columns[j].Title.Caption :=
              CompoMoeda(TDBGrid(Compo).Columns[j].Title.Caption);
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    if ScreenActiveForm.Components[i] is TLabel then
    begin
      if TLabel(Compo).Font.Color <> clWindowText then
        TLabel(Compo).StyleElements := TLabel(Compo).StyleElements - [seFont];
      if TLabel(Compo).Visible then
        if Pos('$', TLabel(Compo).Caption) > 0 then
          TLabel(Compo).Caption := CompoMoeda(TLabel(Compo).Caption);
    end;
    ////////////////////////////////////////////////////////////////////////////
    if ScreenActiveForm.Components[i] is TGroupBox then
      if TGroupBox(Compo).Visible then
        if Pos('$', TGroupBox(Compo).Caption) > 0 then
          TGroupBox(Compo).Caption := CompoMoeda(TGroupBox(Compo).Caption);
    ////////////////////////////////////////////////////////////////////////////
    if VAR_SOMAIUSCULAS and (not VAR_MAIUSC_MINUSC) then
    begin
      if GetKeyState(VK_CAPITAL) = 0 then
      KeyBoardState[VK_CAPITAL] := 1;
      ////////////////////////////////////////////////////////////////////////////
      if ScreenActiveForm.Components[i] is TEdit then
        //if not CompoMinusculo(TEdit(Compo).Name) then
          TEdit(Compo).CharCase := ecUpperCase;
      ////////////////////////////////////////////////////////////////////////////
      if ScreenActiveForm.Components[i] is TDBEdit then
        //if not CompoMinusculo(TDBEdit(Compo).Name) then
          TDBEdit(Compo).CharCase := ecUpperCase;
      ////////////////////////////////////////////////////////////////////////////
      if ScreenActiveForm.Components[i] is TdmkEdit then
        //if not CompoMinusculo(TdmkEdit(Compo).Name) then
          if TdmkEdit(Compo).NoForceUppercase = False then
            TdmkEdit(Compo).CharCase := ecUpperCase;
      ////////////////////////////////////////////////////////////////////////////
      if ScreenActiveForm.Components[i] is TdmkDBEdit then
        //if not CompoMinusculo(TdmkDBEdit(Compo).Name) then
          TdmkDBEdit(Compo).CharCase := ecUpperCase;
      ////////////////////////////////////////////////////////////////////////////
    end else KeyBoardState[VK_CAPITAL] := KeyBoardState[VK_CAPITAL] and not 1;
  end;
end;

procedure TUnMyObjects.CopiaItensDeMenu(PMGeral: TPopupMenu; Form: TForm);
  {$IfDef cAdvToolx} //Berlin
var
  Seq: Integer;
  //
  procedure ItensDeMenu(Item, Dest: TMenuItem);
  var
    I: Integer;
    Novo: TMenuItem;
  begin
    Seq := Seq + 1;
    Novo := TMenuItem.Create(Form);
    //Novo.Name := 'Item_Menu_' + Item.Name;
    Novo.Name := 'Item_Menu_' + Item.Name + Geral.FFN(Seq, 9);
    Novo.Caption := Item.Caption;
    Novo.OnClick := Item.OnClick;
    try
      Dest.Add(Novo);
    except
      Geral.MB_Erro('ERRO!' + sLineBreak + Novo.Caption + sLineBreak +
      Dest.Caption);
    end;
    // Sub-itens
    for I := 0 to Item.Count - 1 do
    begin
      ItensDeMenu(Item.Items[I], Novo);
    end;
  end;
  function TiraQuebras(Texto: String): String;
  var
    I: Integer;
  begin
    Result := '';
    for I := 1 to Length(Texto) do
    begin
      case Ord(Texto[I]) of
        10: Result := Result + ' ';
        13: ;//nada
        else Result := Result + Texto[I];
      end;
    end;
    Result := Trim(Result);
  end;
var
  I, J, K, L, Codigo, Controle, Conta: Integer;
  Compo, Page, TBar: TComponent;
  //
  NomePage, NomeTBar, NomeButn: String;
  //
  MenuItem, Dest, MItem: TMenuItem;
  Achou: Boolean;
  DropDownMenu: TPopupMenu;
begin
  Seq := 0;
  Conta := 0;
  with Form do
  begin
    for i := 0 to ComponentCount -1 do
    begin
      Compo := Components[i];
      if Compo is TAdvPage then
      begin
        Codigo := TAdvPage(Compo).PageIndex + 1;
        NomePage := 'IPM_AdvPage_' + FormatFloat('0', Codigo);
        //
        Dest := TMenuItem.Create(Form);
        Dest.Name := NomePage;
        Dest.Caption := TiraQuebras(TAdvPage(Compo).Caption);
        PMGeral.Items.Add(Dest);
      end;
    end;
    for i := 0 to ComponentCount -1 do
    begin
      Compo := Components[i];
      if Compo is TAdvToolBar then
      begin
        Page := Compo.GetParentComponent;
        if (Page <> nil) and (Page is TAdvPage) then
        begin
          Codigo := TAdvPage(Page).PageIndex + 1;
          NomePage := 'IPM_AdvPage_' + FormatFloat('0', Codigo);
          Achou := False;
          for J := 0 to PMGeral.Items.Count - 1 do
          begin
            if PMGeral.Items[J].Name = NomePage then
            begin
              Achou := True;
              Controle := TAdvToolBar(Compo).ToolBarIndex + 1;
              NomeTBar := 'IPM_AdvTBar_' + FormatFloat('0', Codigo) + '_' + FormatFloat('0', Controle);
              //
              Dest := TMenuItem.Create(Form);
              Dest.Name := NomeTBar;
              Dest.Caption := TiraQuebras((TAdvToolBar(Compo).Caption));
              MenuItem := PMGeral.Items[J];
              MenuItem.Add(Dest);
            end;
          end;
          if not Achou then
            ShowMessage('N�o achei o AdvPage: ' + NomePage);
        end;
      end;
    end;
    for i := 0 to ComponentCount -1 do
    begin
      Compo := Components[i];
      if Compo is TAdvCustomGlowButton then
      begin
        TBar := Compo.GetParentComponent;
        if (TBar <> nil) and (TBar is TAdvToolBar) then
        begin
          Page := TBar.GetParentComponent;
          if (Page <> nil) and (Page is TAdvPage) then
          begin
            Codigo := TAdvPage(Page).PageIndex + 1;
            NomePage := 'IPM_AdvPage_' + FormatFloat('0', Codigo);
            Controle := TAdvToolBar(TBar).ToolBarIndex + 1;
            NomeTBar := 'IPM_AdvTBar_' + FormatFloat('0', Codigo) + '_' + FormatFloat('0', Controle);
            //
            for J := 0 to PMGeral.Items.Count - 1 do
            begin
              if PMGeral.Items[J].Name = NomePage then
              begin
                MItem := PMGeral.Items[J];
                Achou := False;
                for K := 0 to MItem.Count - 1 do
                begin
                  if MItem.Items[K].Name = NomeTBar then
                  begin
                    Achou := True;
                    Conta := Conta + 1;
                    NomeButn := 'IPM_AdvButn_' + FormatFloat('0', Codigo) + '_' +
                      FormatFloat('0', Controle) + '_' + FormatFloat('0', Conta);
                    //
                    if (TAdvCustomGlowButton(Compo).Enabled) and
                      (TAdvCustomGlowButton(Compo).Visible) then
                    begin
                      Dest := TMenuItem.Create(Form);
                      Dest.Name := NomeButn;
                      Dest.Caption := TiraQuebras((TAdvCustomGlowButton(Compo).Caption));
                      Dest.OnClick := TAdvCustomGlowButton(Compo).OnClick;
                      MenuItem := MItem.Items[K];
                      try
                        MenuItem.Add(Dest);
                      except
                        Geral.MB_Erro('ERRO!' + sLineBreak + NomeButn + sLineBreak +
                        Dest.Caption);
                      end;
                      // 2012-09-23
                      if GetPropInfo(TAdvCustomGlowButton(Compo), 'DropDownMenu') <> nil then
                      begin
                        DropDownMenu := TAdvGlowButton(Compo).DropDownMenu;
                        if DropDownMenu <> nil then
                        begin
                          for L := 0 to DropDownMenu.Items.Count - 1 do
                          begin
                            ItensDeMenu(DropDownMenu.Items[L], Dest);
                          end;
                        end;
                      end;
                      // Fim 2012-09-23
                    end;
                  end;
                end;
                if not Achou then
                  ShowMessage('N�o achei o AdvToolBar: ' + NomeTbar);
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  {$Else}
var
  Seq: Integer;
  //
  procedure ItensDeMenu(Item, Dest: TMenuItem);
  var
    I: Integer;
    Novo: TMenuItem;
  begin
    Seq := Seq + 1;
    Novo := TMenuItem.Create(Form);
    //Novo.Name := 'Item_Menu_' + Item.Name;
    Novo.Name := 'Item_Menu_' + Item.Name + Geral.FFN(Seq, 9);
    Novo.Caption := Item.Caption;
    Novo.OnClick := Item.OnClick;
    try
      Dest.Add(Novo);
    except
      Geral.MB_Erro('ERRO!' + sLineBreak + Novo.Caption + sLineBreak +
      Dest.Caption);
    end;
    // Sub-itens
    for I := 0 to Item.Count - 1 do
    begin
      ItensDeMenu(Item.Items[I], Novo);
    end;
  end;
  function TiraQuebras(Texto: String): String;
  var
    I: Integer;
  begin
    Result := '';
    for I := 1 to Length(Texto) do
    begin
      case Ord(Texto[I]) of
        10: Result := Result + ' ';
        13: ;//nada
        else Result := Result + Texto[I];
      end;
    end;
    Result := Trim(Result);
  end;
var
  I, J, K, L, M, Codigo, Controle, Conta: Integer;
  Compo, Page, TBar, SubPanel, PanelPai: TComponent;
  //
  NomePage, NomeTBar, NomeButn: String;
  //
  MenuItem, Dest, MItem: TMenuItem;
  Achou: Boolean;
  DropDownMenu: TPopupMenu;
begin
  Seq := 0;
  Conta := 0;
  Controle := 0;
  Codigo   := 0;
  with Form do
  begin
    for i := 0 to ComponentCount -1 do
    begin
      Compo := Components[i];
      if Compo is TTabSheet then
      begin
        Codigo := TTabSheet(Compo).PageIndex + 1;
        //Codigo := Codigo + 1;
        NomePage := TTabSheet(Compo).Name + 'IPM_AdvPage_' + FormatFloat('0', Codigo);
        //
        Dest := TMenuItem.Create(Form);
        Dest.Name := NomePage;
        Dest.Caption := TiraQuebras(TTabSheet(Compo).Caption);
        PMGeral.Items.Add(Dest);
      end;
    end;
    for i := 0 to ComponentCount -1 do
    begin
      Compo := Components[i];
      if Compo is TPanel then
      begin
        Page := Compo.GetParentComponent;
        if (Page <> nil) and (Page is TTabSheet) then
        begin
          Codigo := TTabSheet(Page).PageIndex + 1;
          //Codigo := Codigo + 1;
          NomePage := TTabSheet(Page).Name + 'IPM_AdvPage_' + FormatFloat('0', Codigo);
          Achou := False;
          for J := 0 to PMGeral.Items.Count - 1 do
          begin
            if PMGeral.Items[J].Name = NomePage then
            begin
              Achou := True;
              //Controle := TPanel(Compo).ToolBarIndex + 1;
              Controle := TPanel(Compo).TabOrder + 1;
              //Controle := Controle + 1;
              NomeTBar := TPanel(Compo).Name + 'IPM_AdvTBar_' + FormatFloat('0', Codigo) + '_' + FormatFloat('0', Controle);
              //
              Dest := TMenuItem.Create(Form);
              Dest.Name := NomeTBar;
              Dest.Caption := TiraQuebras((TPanel(Compo).Caption));
              // ver Caption do panel Titulo!
              //for M := 0 to Compo.ComponentCount - 1 do
              for M := 0 to ComponentCount -1 do
              begin
                SubPanel := Components[M];
                if (SubPanel is TPanel) then
                begin
                  if TPanel(SubPanel).Caption <> '' then
                  begin
                    PanelPai := TPanel(SubPanel).Parent;
                    if PanelPai is TPanel then
                    begin
                      if PanelPai = Compo then
                      begin
                        Dest.Caption := TiraQuebras((TPanel(SubPanel).Caption));
                        TPanel(PanelPai).Caption := '';
                      end;
                    end;
                  end;
                end;
              end;
              //
              MenuItem := PMGeral.Items[J];
              MenuItem.Add(Dest);
            end;
          end;
          if not Achou then
            ShowMessage('N�o achei o AdvPage: ' + NomePage);
        end;
      end;
    end;
    for i := 0 to ComponentCount -1 do
    begin
      Compo := Components[i];
      if Compo is TBitBtn then
      begin
        TBar := Compo.GetParentComponent;
        if (TBar <> nil) and (TBar is TPanel) then
        begin
          Page := TBar.GetParentComponent;
          if (Page <> nil) and (Page is TTabSheet) then
          begin
            Codigo := TTabSheet(Page).PageIndex + 1;
            //Codigo := Codigo + 1;
            NomePage := TTabSheet(Page).Name + 'IPM_AdvPage_' + FormatFloat('0', Codigo);
            Controle := TPanel(TBar).TabOrder + 1;
            //Controle := Controle + 1;
            NomeTBar := TPanel(TBar).Name + 'IPM_AdvTBar_' + FormatFloat('0', Codigo) + '_' + FormatFloat('0', Controle);
            //
            for J := 0 to PMGeral.Items.Count - 1 do
            begin
              if PMGeral.Items[J].Name = NomePage then
              begin
                MItem := PMGeral.Items[J];
                Achou := False;
                for K := 0 to MItem.Count - 1 do
                begin
                  if MItem.Items[K].Name = NomeTBar then
                  begin
                    Achou := True;
                    Conta := Conta + 1;
                    NomeButn := 'IPM_AdvButn_' + FormatFloat('0', Codigo) + '_' +
                      FormatFloat('0', Controle) + '_' + FormatFloat('0', Conta);
                    //
                    if (TBitBtn(Compo).Enabled) and
                      (TBitBtn(Compo).Visible) then
                    begin
                      Dest := TMenuItem.Create(Form);
                      Dest.Name := NomeButn;
                      Dest.Caption := TiraQuebras((TBitBtn(Compo).Caption));
                      Dest.OnClick := TBitBtn(Compo).OnClick;
                      MenuItem := MItem.Items[K];
                      try
                        MenuItem.Add(Dest);
                      except
                        Geral.MB_Erro('ERRO!' + sLineBreak + NomeButn + sLineBreak +
                        Dest.Caption);
                      end;
                      // 2012-09-23
                      if GetPropInfo(TBitBtn(Compo), 'DropDownMenu') <> nil then
                      begin
                        DropDownMenu := TBitBtn(Compo).DropDownMenu;
                        if DropDownMenu <> nil then
                        begin
                          for L := 0 to DropDownMenu.Items.Count - 1 do
                          begin
                            ItensDeMenu(DropDownMenu.Items[L], Dest);
                          end;
                        end;
                      end;
                      // Fim 2012-09-23
                    end;
                  end;
                end;
                if not Achou then
                  ShowMessage('N�o achei o Panel: ' + NomeTbar);
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  {$EndIf}
end;

function TUnMyObjects.CordaDeArrayInt(Lista: array of Integer): String;
var
  I, N: Integer;
begin
  Result := '';
  N := High(Lista);
  for I := Low(Lista) to N do
  begin
    Result := Result + sLineBreak + Geral.FF0(Lista[I]);
    if I < N then
      Result := Result + ',';
  end;
end;

function TUnMyObjects.CordaDeDBGridSelectedRows(DBGrid: Vcl.DBGrids.TDBGrid;
  Query: TmySQLQuery; Campo: String; PermiteTodos: Boolean): String;
var
  I: Integer;
begin
  Result := '';
  if (DBGrid.SelectedRows.Count > 0) and
  (
    (DBGrid.SelectedRows.Count < Query.RecordCount)
    or
    PermiteTodos
  ) then
  begin
    for I := 0 to DBGrid.SelectedRows.Count - 1 do
    begin
      Query.GotoBookmark(DBGrid.SelectedRows.Items[I]);
      //
      if Result <> '' then
        Result := Result + ',';
      Result := Result + Geral.FF0(Query.FieldByName(Campo).AsInteger);
    end;
  end;
end;

function TUnMyObjects.CordaDeDirectQuery(Query: TmySQLDirectQuery;
  Campo: String): String;
var
  I: Integer;
begin
  Result := '';
  Query.First;
  while not Query.Eof do
  begin
    if Result <> '' then
      Result := Result + ', ';
    Result := Result + Query.FieldValueByFieldName(Campo);
    //
    Query.Next;
  end;
end;

function TUnMyObjects.CordaDeIntInConjunto(Conjunto: Integer;
  Valores: array of Integer): String;
var
  I, N: Integer;
begin
  Result := '';
  if Geral.IntInConjunto(1, Conjunto) then
    Result := Result + ',' + Geral.FF0(Valores[0]);
  for I := 1 to Length(Valores) -1 do
  begin
    N := Trunc(Power(2, I));
    if Geral.IntInConjunto(N, Conjunto) then
      Result := Result + ',' + Geral.FF0(Valores[I]);
  end;
  Result := Copy(Result, 2);
end;

function TUnMyObjects.CordaDeQuery(Query: TmySQLQuery; Campo: String;
  DefRes: String = ''): String;
var
  I: Integer;
begin
  if Query.RecordCount > 0 then
  begin
    Result := '';
    try
      Query.DisableControls;
      Query.First;
      while not Query.Eof do
      begin
        if Result <> '' then
          Result := Result + ', ';
        Result := Result + Geral.FF0(Query.FieldByName(Campo).AsInteger);
        //
        Query.Next;
      end;
    finally
      Query.EnableControls;
    end;
  end else
    Result := DefRes;
end;

procedure TUnMyObjects.CordaDeQuery_Mul(const Query: TmySQLQuery; const
  CampoFiltro, CampoValor: String; const ValoresIdx: Array of Integer; const DefRes:
  Array of String; var Cordas: Array of String; AvisaOrfaos: Boolean);
const
  sProcName = 'TUnMyObjects.CordaDeQuery_Mul()';
var
  N, I, J, Orfaos: Integer;
begin
  Orfaos := 0;
  //N := Length(ValoresIdx);
  //SetLength(Cordas, N);
  if Query.RecordCount > 0 then
  begin
    try
      Query.DisableControls;
      Query.First;
      while not Query.Eof do
      begin
        J := -1;
        for I := Low(ValoresIdx) to High(ValoresIdx) do
        begin
          if Query.FieldByName(CampoFiltro).AsInteger = ValoresIdx[I] then
          begin
            J := I;
            Break;
          end;
        end;
        if J = -1 then
          Orfaos := Orfaos + 1
        else
        begin
          if Cordas[J] <> '' then
            Cordas[J] := Cordas[J] + ', ';
          Cordas[J] := Cordas[J] + Geral.FF0(Query.FieldByName(CampoValor).AsInteger);
        end;
        //
        Query.Next;
      end;
    finally
      Query.EnableControls;
    end;
  end;
  for I := Low(Cordas) to High(Cordas) do
  begin
    if Cordas[I] = EmptyStr then
      if Length(DefRes) > I then
        Cordas[I] := DefRes[I];
  end;
  if AvisaOrfaos and (Orfaos > 0) then
    Geral.MB_Aviso(Geral.FF0(Orfaos) +
    ' itens n�o foram adicionados em nenhuma corda!' +
    sProcName);
end;

function TUnMyObjects.CordaDeQuery_Mul_SelcionaItem(Cordas: array of String;
  Item: Integer): String;
begin
  if Item < Length(Cordas) then
    Result := Cordas[Item]
  else
    Result := '';
end;

function TUnMyObjects.CordaDeSQL_IN(Lista: array of String): String;
var
  I, N: Integer;
begin
  Result := '';
  N := High(Lista);
  for I := Low(Lista) to N do
  begin
    Result := Result + sLineBreak + '"' + Lista[I] + '"';
    if I < N then
      Result := Result + ',';
  end;
end;

function TUnMyObjects.CordaDeZTOChecks(DBGridZTO: TdmkDBGridZTO;
  Query: TmySQLQuery; Campo: String; PermiteTodos: Boolean): String;
var
  I: Integer;
begin
  Result := '';
  if (DBGridZTO.SelectedRows.Count > 0) and
  (
    (DBGridZTO.SelectedRows.Count < Query.RecordCount)
    or
    PermiteTodos
  ) then
  begin
    for I := 0 to DBGridZTO.SelectedRows.Count - 1 do
    begin
      //Query.GotoBookmark(DBGridZTO.SelectedRows.Items[I]);
      Query.GotoBookmark(DBGridZTO.SelectedRows.Items[I]);
      //
      if Result <> '' then
        Result := Result + ',';
      Result := Result + Geral.FF0(Query.FieldByName(Campo).AsInteger);
    end;
  end;
end;

function TUnMyObjects.CorIniComponente(Handle: THandle): Boolean;
{$IFNDEF NAO_USA_GLYFS}
var
  H, W : Integer;
  Form: TForm;
begin
  H := 0;
  Result := False;
  //Form := nil;
  //
  if Handle <> 0 then
  begin
    Form := TForm(Handle);
    if Form = nil then
      Exit;
  end else
  begin
    Form := Screen.ActiveForm;
    if Form = nil then
      Exit;
    {
      Form.AlphaBlendValue := 0;
      Form.AlphaBlend := True;
    }

    {
      FmMyGlyfs.FForm := Form;
      FmMyGlyfs.Timer1.Enabled := True;
    }
      H := Form.Height;
      W := Form.Width;

    {
      Form.Constraints.MinHeight := 0;
      Form.Constraints.MinWidth  := 0;
      Form.Height := 0;
      Form.Width := 0;
      Application.ProcessMessages;
    }
      if H > 600 then H := 600;
      if W > 800 then W := 800;
      Form.Constraints.MinHeight := H;
      Form.Constraints.MinWidth  := W;
      if Form.WindowState = wsMaximized then
        Form.Top := 0
      else
      begin
        //Form.Height := 0;
      //Form.Width := 0;
    end;
  end;
  // 2012-11-05 - Child form
  if Form <> nil then
  begin
    Form.Font.Name := 'Tahoma';
    if (H > 530) and (Screen.Height <= 600) then Form.Top := 0;
    //
    FmMyGlyfs.DefineGlyfs(Form);
    //
    //DefineCompoCor;
    //
    ControleCor(Form);
    //
    ControleStrings(Form);
    //
    // 2011-09-21
    FmPrincipal.ReCaptionComponentesDeForm(Form);
    //
  {
    Form.AlphaBlendValue := 255;
    Form.AlphaBlend := False;
  }
    //
    EditCB_ValUnico(Form);
  end;
  Result := True;
{$ELSE}
begin
  Result := True;
{$ENDIF}
end;

procedure TUnMyObjects.SelecionaLimpaImagemdefundoFormDescanso(FormPrincipal: TForm;
  Classe: TFormClass; PageControl: TPageControl; Limpar: Boolean = False);
var
  I: Integer;
  Form: TForm;
  Componente: TComponent;
  Achou: Boolean;
  OpenPicture: TOpenPictureDialog;
begin
  Form  := nil;
  Achou := False;
  //
  for I := 0 to PageControl.PageCount - 1 do
  begin
    if PageControl.Pages[I].Components[0] is TForm then
    begin
      Form := TForm(PageControl.Pages[I].Components[0]);
      //
      if Form is Classe then
      begin
        Achou := True;
        Break;
      end;
    end;
  end;
  if (Achou = True) and (Form <> nil) then
  begin
    Componente := Form.FindComponent('ImgPrincipal');
    //
    if (Componente <> nil) and (Componente is TImage) then
    begin
      if Limpar = False then
      begin
        OpenPicture := TOpenPictureDialog.Create(FormPrincipal);
        try
          OpenPicture.Filter :=
      'All (*.gif;*.jpg;*.jpeg;*.bmp;*.ico;*.png;*.emf;*.wmf)|*.gif;*.jpg;*.j' +
      'peg;*.bmp;*.ico;*.png;*.emf;*.wmf|GIF Image (*.gif)|*.gif|JPEG Image F' +
      'ile (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bitmaps (*.bmp' +
      ')|*.bmp|Icons (*.ico)|*.ico|PNG (*.png)|*.png|Enhanced Metafiles (*.emf)|*.emf|Met' +
      'afiles (*.wmf)|*.wmf';
          OpenPicture.FilterIndex := 1;
          OpenPicture.Options     := [ofHideReadOnly,ofEnableSizing];
          //
          if OpenPicture.Execute then
          begin
            TImage(Componente).Picture.LoadFromFile(OpenPicture.FileName);
            //
            Geral.WriteAppKeyLM2('ImagemFundo', Application.Title, OpenPicture.FileName, ktString);
          end;
        finally
          OpenPicture.Free;
        end;
      end else
      begin
        TImage(Componente).Picture := nil;
        //
        Geral.WriteAppKeyLM2('ImagemFundo', Application.Title, '', ktString);
      end;
    end else
      Geral.MB_Erro('Falha ao localizar imagem de descanso!');
  end else
    Geral.MB_Erro('Falha ao localizar janela de descanso!');
end;

function TUnMyObjects.VerificaSeTabFormExiste(PageControl: TPageControl;
  Classe: TFormClass): Integer;
var
  I, N, TabComps: Integer;
  Tab: TTabSheet;
  Form: TForm;
  Res: Boolean;
begin
  Res := False;
  //
  for I := 0 to PageControl.PageCount - 1 do
  begin
    Tab      := Pagecontrol.Pages[I];
    TabComps := Tab.ComponentCount;
    //
    if (Tab <> nil) and (TabComps > 0) and (Tab.Components[0] is TForm) then
    begin
      Form := TForm(Pagecontrol.Pages[I].Components[0]);
      //
      if Form is Classe then
      begin
        VAR_FORM_VerificaSeTabFormExiste_LOCALIZADO := Form;
        Res := True;
        N := I;
        Break;
      end;
    end;
  end;
  if Res = False then
    N := -1;
  //
  Result := N;
end;

// Cria o formulario a partir de sua classe
function TUnMyObjects.FormTDICria(Classe: TFormClass; InOwner: TWincontrol;
  Pager: TWinControl; Collaps: Boolean = True;
  Unique: Boolean = False; ModoAcesso: TAcessFmModo = afmoNegarComAviso): TForm;

  procedure InfoErro(Codigo: Integer);
  begin
    Geral.MB_Erro('"InOwner" n�o implementado na function "FormTDICria".' +
    sLineBreak + 'Localizador na function: ' + Geral.FFN(Codigo, 3) +
    sLineBreak + 'Avise a DERMATEK');
  end;

  procedure Habilita(Compo: TComponent; Visivel: Boolean);
  var
    PropInfo: PPropInfo;
  begin
    if Compo <> nil then
    begin
      PropInfo := GetPropInfo(Compo, 'Visible');
      if PropInfo <> nil then
        SetPropValue(Compo, 'Visible', Visivel);
    end;
  end;

var
  Form: TForm;
  Achou, Inclui, Altera, Exclui: Boolean;
  I, N, k, Acesso: Integer;
  ///
  PageControl: TPageControl;
  TabSheet: TTabSheet;
  //
  Objeto: TObject;
  PropInfo: PPropInfo;
  Form_ID, Nome: String;
  Continua: Boolean;
  ParentForm: TCustomForm;
begin
  Form        := nil;
  Achou       := False;
  I           := 0;
  PageControl := nil;
  //
  if InOwner is TPageControl then
    PageControl := TPageControl(InOwner)
  else
  if InOwner is TForm then
  begin
    if TForm(InOwner).Owner is TTabSheet then
    begin
      PageControl := TPageControl(TTabSheet(TForm(InOwner).Owner).Owner);
      if not (PageControl is TPageControl) then
        InfoErro(3);
    end else InfoErro(2);
  end else
  if InOwner is TPanel then
  begin
    Form := FormTDIPanel(Classe, Result, TPanel(InOwner));
    //Result := nil;
    Exit;
  end else InfoErro(1);
  //
  {TWincontrol(Sender).Enabled := False;
  PageControl.Enabled := False;
  }
  //try
    if Unique then
    begin
      N := VerificaSeTabFormExiste(PageControl, Classe);
      //
      if N = -1 then
        Achou := False
      else
        Achou := True;
    end;
    if Achou then
    begin
      try
        PageControl.ActivePage      := PageControl.Pages[N];
        PageControl.ActivePageIndex := PageControl.Pages[N].PageIndex;
        //
        Form := TForm(Pagecontrol.Pages[N].Components[0]);
      except
        Geral.MB_Erro('N�o foi poss�vel definir index no page control!');
      end;
    end else
    begin
      TabSheet := FormTDINovaAba(PageControl);
      Form := TFormClass(Classe).Create(TabSheet);
      TabSheet.Caption := 'Carregado. Ajustando form na aba...';
      //
      (*
      ParentForm := GetParentForm(Form);
      Geral.MB_Aviso(ParentForm.Name);
      *)
      //
      with Form do
      begin
        //configura o formulario
        Align       := alClient;
        BorderStyle := bsNone;
        //Parent      := PageControl.ActivePage;//ActivePage � ultima aba criada com NovaAba
        Parent      := TabSheet;
        {O evento onActive do TForm n�o � executado pq o que se torna ativo
         na verdade � o TTabSheet onde o formulario foi criado. Sendo assim qualquer
         coisa escrita no onActive do formul�rio n�o ser� executado.
         Para contornar esta situa��o nos passamos o evento onActive do Form para o
         evento onEnter do TTabSheet. E assim simulamos com seguran�a o evento onActive}
        //PageControl.ActivePage.OnEnter := OnActivate;
        TabSheet.OnEnter := OnActivate;

        //PageControl.ActivePage.Caption := Caption;//transfere o caption do form para o caption da aba
        //PageControl.ActivePage.Caption := Copy(Caption, 1, 13);//transfere o caption do form para o caption da aba
        TabSheet.Caption := Copy(Caption, 1, 13);//transfere o caption do form para o caption da aba

        {
        PageControl.Enabled := True;
        TWinControl(Sender).Enabled := HabilitaSender;
        }

        //Transforma tudo em ma�sculo
        if VAR_SOMAIUSCULAS then
        begin
          with Form do
          begin
            for i := 0 to ComponentCount -1 do
            begin
              Objeto := Components[i];
              PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'CharCase');
              if PropInfo <> nil then
              begin
                Nome := Uppercase(TComponent(Objeto).Name);

                if (Nome <> 'EDEMAIL') and (Nome <> 'CBEMAIL') and
                (Nome <> 'EDPEMAIL') and (Nome <> 'EDEEMAIL') then
                try
                  if (GetPropValue(TComponent(Objeto), 'CharCase') = 'ecNormal') then
                  begin
                    Continua := True;
                    PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'NoForceUppercase');
                    //
                    if PropInfo <> nil then
                    begin
                      if GetPropValue(TComponent(Objeto), 'NoForceUppercase') = 'True' then
                        Continua := False;
                    end;
                    //
                    if Continua then
                      SetPropValue(Objeto, 'CharCase', 'ecUpperCase');
                  end;
                except
                  //
                end;
              end;
            end;
          end;
        end;
        //Libera��o da janela
        if (VAR_LIBERA_TODOS_FORMS = False) and (ModoAcesso <> afmoLiberado) then
        begin
          k := 13;
          //
          Form_ID := Copy(Form.Caption, 1, k);
          {$IFNDEF NO_USE_MYSQLMODULE}
          DBCheck.Define_Form_ID(Form_ID);
          // Verifica se a janela existe
          DModG.QrPerfJan.Close;
          DModG.QrPerfJan.Params[0].AsString := Form_ID;
          UnDmkDAC_PF.AbreQuery(DModG.QrPerfJan, Dmod.MyDB);
          //
          if (DModG.QrPerfJan.RecordCount = 0) and (ModoAcesso <> afmoLiberado) then
          begin
            if (Form_ID <> 'MAS-CADAS-000') and (ModoAcesso <> afmoSemVerificar) then
            begin
              Geral.MB_Aviso('A janela ' + Form_ID +
                ' n�o est� cadastrada para ser consultada!' + sLineBreak +
                'Informe a DERMATEK!');
              if (VAR_USUARIO >= 0) or (Form_ID <> 'FER-VRFBD-001') then
              begin
                FormTDIFecha(Form, nil);
                Result := nil;
                Exit;
              end;
            end;
          end else
          begin
            Acesso := DBCheck.AcessoAoForm(Form_ID, (*Form.Caption, *)ModoAcesso);
            //
            if Acesso = 0 then
            begin
              FormTDIFecha(Form, nil);
              Result := nil;
              Exit;
            end else
            begin
              //Verifica bot�es
              if Acesso < 8 then
              begin
                Inclui := DmodG.QrPerfInclui.Value = 1;
                Altera := DmodG.QrPerfAltera.Value = 1;
                Exclui := DmodG.QrPerfExclui.Value = 1;
                //
                for i := 0 to Form.ComponentCount - 1 do
                begin
                  if Form.Components[i] is TdmkPermissoes then
                  begin
                    if TdmkPermissoes(TComponent(Classe)) <> nil then
                    begin
                      Habilita(TdmkPermissoes(Form.Components[i]).CanIns01, Inclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanIns02, Inclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanIns03, Inclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanIns04, Inclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanIns05, Inclui);
                      //
                      Habilita(TdmkPermissoes(Form.Components[i]).CanUpd01, Altera);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanUpd02, Altera);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanUpd03, Altera);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanUpd04, Altera);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanUpd05, Altera);
                      //
                      Habilita(TdmkPermissoes(Form.Components[i]).CanDel01, Exclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanDel02, Exclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanDel03, Exclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanDel04, Exclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanDel05, Exclui);
                    end;
                  end;
                end;
              end;
            end;
          end;
{$ENDIF}
        end;
        if Geral.ReadAppKeyCU('SkinAbas', Application.Title +
          '\UsaLinkRankSkin', ktInteger, 1) = 1
        then
          VAR_FORMTDI_NAME := Classe.ClassName;
        //
        Show;//mostra o formul�rio
        //
        VAR_FORMTDI_NAME := '';
        //
        {Embora comigo nunca tenha acontecido, algumas pessoas me avisaram sobre
         uma exce��o de focus que a linha abaixo gera em casos bem especificos.
         Eu deixo descomentado e se voc� prefefir pode comentar a linha abaixo.}
        try
          //Perform(WM_NEXTDLGCTL, 0, 0);//muda o foco para o primeiro controle do formulario
          HackWinControl(Form).SelectFirst;
          //HackWinControl(Form).SelectNext(HackWinControl(Form).ActiveControl, True, True);
        except
        end;
      end;
    end;
    if Pager <> nil then
    begin
      if Collaps then
  {$IfDef cAdvToolx} //Berlin
        TAdvToolBarPager(Pager).Collaps;
  {$Else}
       Pager.Visible := False;
  {$EndIf}
      (* Caso Collaps = False manter o menu do jeito que est�
      else
        TAdvTool BarPager(Pager).Expand;
      *)
    end;
  //finally
    {
    PageControl.Enabled := True;
    TWinControl(Sender).Enabled := HabilitaSender;
    }
  //end;
  //
  Result := Form;
end;

{
function TUnMyObjects.FormTDICria(Classe: TFormClass; InOwner: TWincontrol;
  Page: TdmkPageControl; Collaps, Unique: Boolean;
  ModoAcesso: TAcessFmModo): TForm;

  procedure InfoErro(Codigo: Integer);
  begin
    Geral.MB_Erro('"InOwner" n�o implementado na function "FormTDICria".' +
    sLineBreak + 'Localizador na function: ' + Geral.FFN(Codigo, 3) +
    sLineBreak + 'Avise a DERMATEK');
  end;

  procedure Habilita(Compo: TComponent; Visivel: Boolean);
  var
    PropInfo: PPropInfo;
  begin
    if Compo <> nil then
    begin
      PropInfo := GetPropInfo(Compo, 'Visible');
      if PropInfo <> nil then
        SetPropValue(Compo, 'Visible', Visivel);
    end;
  end;

var
  Form: TForm;
  Achou, Inclui, Altera, Exclui: Boolean;
  I, N, k, Acesso: Integer;
  ///
  PageControl: TPageControl;
  TabSheet: TTabSheet;
  //
  Objeto: TObject;
  PropInfo: PPropInfo;
  Form_ID, Nome: String;
  Continua: Boolean;
  ParentForm: TCustomForm;
begin
  Form        := nil;
  Achou       := False;
  I           := 0;
  PageControl := nil;
  //
  if InOwner is TPageControl then
    PageControl := TPageControl(InOwner)
  else
  if InOwner is TForm then
  begin
    if TForm(InOwner).Owner is TTabSheet then
    begin
      PageControl := TPageControl(TTabSheet(TForm(InOwner).Owner).Owner);
      if not (PageControl is TPageControl) then
        InfoErro(3);
    end else InfoErro(2);
  end else
  if InOwner is TPanel then
  begin
    Form := FormTDIPanel(Classe, Result, TPanel(InOwner));
    //Result := nil;
    Exit;
  end else InfoErro(1);
  //
  (*TWincontrol(Sender).Enabled := False;
  PageControl.Enabled := False;
  *)
  //try
    if Unique then
    begin
      N := VerificaSeTabFormExiste(PageControl, Classe);
      //
      if N = -1 then
        Achou := False
      else
        Achou := True;
    end;
    if Achou then
    begin
      try
        PageControl.ActivePage      := PageControl.Pages[N];
        PageControl.ActivePageIndex := PageControl.Pages[N].PageIndex;
        //
        Form := TForm(Pagecontrol.Pages[N].Components[0]);
      except
        Geral.MB_Erro('N�o foi poss�vel definir index no page control!');
      end;
    end else
    begin
      TabSheet := FormTDINovaAba(PageControl);
      Form := TFormClass(Classe).Create(TabSheet);
      TabSheet.Caption := 'Carregado. Ajustando form na aba...';
      //
      (*
      ParentForm := GetParentForm(Form);
      Geral.MB_Aviso(ParentForm.Name);
      *)
      //
      with Form do
      begin
        //configura o formulario
        Align       := alClient;
        BorderStyle := bsNone;
        //Parent      := PageControl.ActivePage;//ActivePage � ultima aba criada com NovaAba
        Parent      := TabSheet;
        (*O evento onActive do TForm n�o � executado pq o que se torna ativo
         na verdade � o TTabSheet onde o formulario foi criado. Sendo assim qualquer
         coisa escrita no onActive do formul�rio n�o ser� executado.
         Para contornar esta situa��o nos passamos o evento onActive do Form para o
         evento onEnter do TTabSheet. E assim simulamos com seguran�a o evento onActive*)
        //PageControl.ActivePage.OnEnter := OnActivate;
        TabSheet.OnEnter := OnActivate;

        //PageControl.ActivePage.Caption := Caption;//transfere o caption do form para o caption da aba
        //PageControl.ActivePage.Caption := Copy(Caption, 1, 13);//transfere o caption do form para o caption da aba
        TabSheet.Caption := Copy(Caption, 1, 13);//transfere o caption do form para o caption da aba

        (*
        PageControl.Enabled := True;
        TWinControl(Sender).Enabled := HabilitaSender;
        *)

        //Transforma tudo em ma�sculo
        if VAR_SOMAIUSCULAS then
        begin
          with Form do
          begin
            for i := 0 to ComponentCount -1 do
            begin
              Objeto := Components[i];
              PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'CharCase');
              if PropInfo <> nil then
              begin
                Nome := Uppercase(TComponent(Objeto).Name);

                if (Nome <> 'EDEMAIL') and (Nome <> 'CBEMAIL') and
                (Nome <> 'EDPEMAIL') and (Nome <> 'EDEEMAIL') then
                try
                  if (GetPropValue(TComponent(Objeto), 'CharCase') = 'ecNormal') then
                  begin
                    Continua := True;
                    PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'NoForceUppercase');
                    //
                    if PropInfo <> nil then
                    begin
                      if GetPropValue(TComponent(Objeto), 'NoForceUppercase') = 'True' then
                        Continua := False;
                    end;
                    //
                    if Continua then
                      SetPropValue(Objeto, 'CharCase', 'ecUpperCase');
                  end;
                except
                  //
                end;
              end;
            end;
          end;
        end;
        //Libera��o da janela
        if (VAR_LIBERA_TODOS_FORMS = False) and (ModoAcesso <> afmoLiberado) then
        begin
          k := 13;
          //
          Form_ID := Copy(Form.Caption, 1, k);
          {$IFNDEF NO_USE_MYSQLMODULE
          DBCheck.Define_Form_ID(Form_ID);
          // Verifica se a janela existe
          DModG.QrPerfJan.Close;
          DModG.QrPerfJan.Params[0].AsString := Form_ID;
          UnDmkDAC_PF.AbreQuery(DModG.QrPerfJan, Dmod.MyDB);
          //
          if (DModG.QrPerfJan.RecordCount = 0) and (ModoAcesso <> afmoLiberado) then
          begin
            if (Form_ID <> 'MAS-CADAS-000') and (ModoAcesso <> afmoSemVerificar) then
            begin
              Geral.MB_Aviso('A janela ' + Form_ID +
                ' n�o est� cadastrada para ser consultada!' + sLineBreak +
                'Informe a DERMATEK!');
              if (VAR_USUARIO >= 0) or (Form_ID <> 'FER-VRFBD-001') then
              begin
                FormTDIFecha(Form, nil);
                Result := nil;
                Exit;
              end;
            end;
          end else
          begin
            Acesso := DBCheck.AcessoAoForm(Form_ID, (*Form.Caption, *)ModoAcesso);
            //
            if Acesso = 0 then
            begin
              FormTDIFecha(Form, nil);
              Result := nil;
              Exit;
            end else
            begin
              //Verifica bot�es
              if Acesso < 8 then
              begin
                Inclui := DmodG.QrPerfInclui.Value = 1;
                Altera := DmodG.QrPerfAltera.Value = 1;
                Exclui := DmodG.QrPerfExclui.Value = 1;
                //
                for i := 0 to Form.ComponentCount - 1 do
                begin
                  if Form.Components[i] is TdmkPermissoes then
                  begin
                    if TdmkPermissoes(TComponent(Classe)) <> nil then
                    begin
                      Habilita(TdmkPermissoes(Form.Components[i]).CanIns01, Inclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanIns02, Inclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanIns03, Inclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanIns04, Inclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanIns05, Inclui);
                      //
                      Habilita(TdmkPermissoes(Form.Components[i]).CanUpd01, Altera);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanUpd02, Altera);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanUpd03, Altera);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanUpd04, Altera);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanUpd05, Altera);
                      //
                      Habilita(TdmkPermissoes(Form.Components[i]).CanDel01, Exclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanDel02, Exclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanDel03, Exclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanDel04, Exclui);
                      Habilita(TdmkPermissoes(Form.Components[i]).CanDel05, Exclui);
                    end;
                  end;
                end;
              end;
            end;
          end;
{$ENDIF
        end;
        if Geral.ReadAppKeyCU('SkinAbas', Application.Title +
          '\UsaLinkRankSkin', ktInteger, 1) = 1
        then
          VAR_FORMTDI_NAME := Classe.ClassName;
        //
        Show;//mostra o formul�rio
        //
        VAR_FORMTDI_NAME := '';
        //
        (*Embora comigo nunca tenha acontecido, algumas pessoas me avisaram sobre
         uma exce��o de focus que a linha abaixo gera em casos bem especificos.
         Eu deixo descomentado e se voc� prefefir pode comentar a linha abaixo.*)
        try
          //Perform(WM_NEXTDLGCTL, 0, 0);//muda o foco para o primeiro controle do formulario
          HackWinControl(Form).SelectFirst;
          //HackWinControl(Form).SelectNext(HackWinControl(Form).ActiveControl, True, True);
        except
        end;
      end;
    end;
    if AdvTool BarPager <> nil then
    begin
    (* ver o que fazer
      if Collaps then
        MaximizaPageMenu.Collaps;
    *)
    end;
  //finally
    (*
    PageControl.Enabled := True;
    TWinControl(Sender).Enabled := HabilitaSender;
    *)
  //end;
  //
  Result := Form;
end;
}

procedure TUnMyObjects.FormTDIFecha(Form: TForm; AbaQueChamou: TTabSheet);
  procedure VerificaTabSheetsVazios(PageControl: TPageControl);
{
  var
    I: Integer;
}
  begin
{
    I := PageControl.PageCount;
    while I > 0 do
    begin
      if PageControl.Pages[I].ComponentCount = 0 then
      begin
        PageControl.ActivePageIndex := I;
        PageControl.
      end
      else
        I := I -1;
    end;
}
  end;
var
  TabSheet: TTabSheet;
  PageControl: TPageControl;
  // 2020-09-22
  Indice: Integer;
begin
  VAR_FORMTDI_NAME := '';
  //
  if Form.Owner is TApplication then
  begin
    Form.Close;
  end else
  if Form.Owner is TTabSheet then
  begin
    TabSheet := TTabSheet(Form.Owner);
    PageControl := TPageControl(TabSheet.Owner);
    Indice := PageControl.ActivePageIndex;
    //ShowMessage(TWinControl(PageControl).Owner.ClassName);
    //
    Form.Close;
    if Assigned(TabSheet) then
    begin
      //FreeAndNil(TabSheet.Parent);
      //FreeAndNil(TabSheet.PageControl);
      // 2020-09-22
      //TabSheet.Free;
      TabSheet.Hide;
      PageControl.Pages[Indice].Hide;
      PageControl.Pages[Indice].Free;
    end;
    //PostMessage(PageControl.Handle, WM_CLOSE_TAB, PageControl.ActivePageIndex, 0);
    {
    TabSheet.Parent := nil;
    TabSheet := nil;
    //TabedInterface.Fechar(False);
    }
    if AbaQueChamou <> nil then
      PageControl.ActivePageIndex := AbaQueChamou.PageIndex
    else
      PageControl.ActivePageIndex := PageControl.PageCount -1;
    //
    //VerificaTabSheetsVazios()

  end else
  if Form.Owner is TPanel then
  begin
    Geral.MB_Aviso('Este "quadro" ser� fechado junto com a janela m�e!');
  end else
  if Form.Owner = nil then
  begin
    if Form.Parent is TPanel then
      Geral.MB_Aviso('Este "quadro" ser� fechado junto com a janela m�e!');
  end
  else
  begin
    Geral.MB_Erro('"Owner" de "Form" desconhecido! Avise a DERMATEK!' +
    sLineBreak + 'Nome: ' + TComponent(Form.Owner).Name);
  end;
end;

function TUnMyObjects.FormTDINovaAba(PageControl: TPageControl): TTabSheet;

{adiciona uma nova aba ao PageControl e retorna a nova aba como resultado}

{Alem de criar um novo TabSheet a funcao NovaAba ativa a TabSheet criada,
     assim apos executar NovaAba, a propriedade ActivePage sempre ser� a
     ultima TabSheet criada}
var
  Tab: TTabSheet;
begin
  Tab := TTabSheet.Create(PageControl);

  Tab.PageControl := PageControl;
  Tab.TabVisible  := True;
  Tab.Caption     := 'Carregando...';
  //Tab.OnHide      := OnTabHide; precisa?
  Tab.PopupMenu   := nil;

  PageControl.ActivePage := Tab;
  PageControl.ActivePageIndex := Tab.PageIndex;

  //PageControl.UpdateCloseButtons;

  Result := Tab;
end;

function TUnMyObjects.FormTDIPanel(InstanceClass: TComponentClass;
  var Reference; Painel: TPanel): TForm;
var
  Instance: TComponent;
  Form: String;
  MyCursor: TCursor;
  Campo: String;
begin
  Result := nil;
  Form := InstanceClass.ClassName;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(nil);
      //
      Result := TForm(Reference);
    except
      TComponent(Reference) := nil;
      raise;
    end;
    with Result do
    begin
      BorderStyle := bsNone;
      Parent := Painel;
      Show;
      Align := alClient;
    end;
    Screen.Cursor := MyCursor;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

function TUnMyObjects.LiberaPelaSenhaBoss(InstanceClass: TComponentClass;
  var Reference; Aviso: String = ''; SenhaExtra: String = ''): Boolean;
var
  LabelAviso1, LabelAviso2, LaSenhaExtra: TLabel;
begin
  VAR_FSENHA := 5;
  if MyObjects.CriaForm_AcessoTotal(InstanceClass, Reference) then
  begin
    LabelAviso1 := TForm(Reference).FindComponent('LaAviso1') as TLabel;
    LabelAviso2 := TForm(Reference).FindComponent('LaAviso2') as TLabel;
    LaSenhaExtra := TForm(Reference).FindComponent('LaSenhaExtra') as TLabel;
    //
    if (Aviso <> '') and (LabelAviso1 <> nil) and (LabelAviso2 <> nil) then
      MyObjects.Informa2(LabelAviso1, LabelAviso2, False, Aviso);
    //
    if LaSenhaExtra <> nil then
      LaSenhaExtra.Caption := SenhaExtra;
    TForm(Reference).ShowModal;
    TForm(Reference).Destroy;
  end;
  Result := VAR_SENHARESULT = 2;
end;

function TUnMyObjects.CriaForm_AcessoTotal(InstanceClass: TComponentClass;
  var Reference): Boolean;
var
  Instance: TComponent;
  Form: String;
  MyCursor: TCursor;
  Campo: String;
begin
  Result := False;
  Form := InstanceClass.ClassName;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(Application);
      Result := True;
    except
      TComponent(Reference) := nil;
      raise;
    end;
    with TForm(Reference) do
    begin
      Campo := 'ImgTipo';
      if (FindComponent(Campo) as TdmkImage) <> nil then
        TdmkImage(FindComponent(Campo) as TImage).SQLType := stLok;
    end;
    Screen.Cursor := MyCursor;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

function TUnMyObjects.CriaSelListaArray(Lista: array of String;
  TituloForm: String; TitulosColunas: array of String; ScreenWidth: Integer;
  dmkEdit: TdmkEdit): Boolean;
var
  nnn, i, kkk: Integer;
  //ttt: array of Integer;
  JaExiste: Boolean;
begin
  Result := False;
  nnn := High(Lista);
  if nnn = -1 then
  begin
    Geral.MB_Erro('Lista n�o definida!');
    Exit;
  end;
  kkk := High(TitulosColunas);
  if kkk = -1 then
  begin
    Geral.MB_Erro('Lista sem itens!');
    Exit;
  end;
(*
  SetLength(ttt, kkk + 1);
  // Largura m�nima das colunas
  for i := 0 to kkk do
    ttt[i] := 28;
*)
  if nnn < 0 then
  begin
    Geral.MB_Aviso('N�o h� itens a escolher para a op��o selecionada!');
    Exit;
  end;
  Application.CreateForm(TFmSelListArr, FmSelListArr);
  begin
    FmSelListArr.FEdit := dmkEdit;
    SetLength(FmSelListArr.FLista, Length(Lista));
    for I := Low(Lista) to High(Lista) do
      FmSelListArr.FLista[I] := Lista[I];
    FmSelListArr.Caption := TituloForm;
    FmSelListArr.LaTitulo1A.Caption := Copy(TituloForm, 17);
    FmSelListArr.LaTitulo1B.Caption := Copy(TituloForm, 17);
    FmSelListArr.LaTitulo1C.Caption := Copy(TituloForm, 17);
    FmSelListArr.Grade.ColCount := kkk + 1;
    FmSelListArr.Grade.RowCount := nnn + 2;
    for i := Low(TitulosColunas) to High(TitulosColunas) do
      FmSelListArr.Grade.Cells[i, 0] := TitulosColunas[i];
    for i := 0 to nnn do
    begin
(*    Provoca erro no OnDestroy!!!
      if Length(Lista[i]) * 10 > ttt[i] - 8 then
        ttt[i] := (Length(Lista[i]) * 8) + 8;
      //
*)
      FmSelListArr.Grade.Cells[0, i + 1] := Geral.FF0(I);
      FmSelListArr.Grade.Cells[1, i + 1] := Lista[i];
    end;
    nnn := 0;
    Geral.LarguraAutomaticaGrade(FmSelListArr.Grade, 0);
    for i := 0 to kkk do
    begin
      //nnn := nnn + ttt[i];
      nnn := nnn + FmSelListArr.Grade.ColWidths[I];
    end;
    nnn := nnn + 36;
    if nnn > ScreenWidth - 16 then nnn := ScreenWidth - 16;
    if nnn < 500 then
      nnn := 500;
    FmSelListArr.Width := nnn;
    FmSelListArr.ShowModal;
    Result := True;
  FmSelListArr.Destroy;
  end;
end;

function TUnMyObjects.ArquivoExiste(Caminho: String; MemoOut: TMemo;
DoLocalize: Boolean): Boolean;
var
  I: Integer;
  Achou: Boolean;
begin
  if (DoLocalize) and (Caminho <> '') then
    Result := FileExists(Caminho)
  else
    Result := False;
  //
  if not Result and (MemoOut <> nil) then
  begin
    Achou := False;
    for I := 0 to MemoOut.Lines.Count -1 do
    begin
      Achou := MemoOut.Lines[I] = Caminho;
      if Achou then
        Break;
    end;
    if not Achou then
      MemoOut.Text := Caminho + sLineBreak + MemoOut.Text;
  end;
end;

function TUnMyObjects.CalculaCanvasTextWidth(Texto, FontName: String;
  FontSize: Integer): Integer;
var
  Label1: TLabel;
begin
  Result := 9;
  Label1 := TLabel.Create(Application.MainForm);
  Label1.Parent := Application.MainForm;
  Label1.Visible := False;
  try
    Label1.Canvas.Font.Name := FontName;
    Label1.Canvas.Font.Size := FontSize;
    Result := Label1.Canvas.TextWidth(Texto);
  finally
    Label1.Free;
  end;
end;

procedure TUnMyObjects.CarregaBitmapBranco(Image: TImage; H, W: Integer);
var
  ImgBmp: TBitmap;
  X, Y: Integer;
begin
  ImgBmp := TBitmap.Create;
  try
    ImgBmp.PixelFormat := pf24bit;
    ImgBmp.Height := H;
    ImgBmp.Width  := W;
    //
    for X := 0 to ImgBmp.Width do
    begin
      for Y := 0 to ImgBmp.Height do
      ImgBmp.Canvas.Pixels[X, Y] := clWhite;
    end;
    Image.Picture.Bitmap.Assign(ImgBmp);
  finally
    ImgBmp.Free;
  end;
end;

procedure TUnMyObjects.CarregaImagemEmTImage(Image: TImage; Arquivo: String;
  Bitmap: TBitmap; BrushColor: TColor);
  function LoadGraphicsFile(const Filename: string): TBitmap;
  var
    Picture: TPicture;
  begin
    Result := nil;
    //
    if FileExists(Filename) then
    begin
      Result := TBitmap.Create;
      try
        Picture := TPicture.Create;
        try
          Picture.LoadFromFile(Filename);
          // Try converting picture to bitmap
          try
            Result.Assign(Picture.Graphic);
          except
            // Picture didn't support conversion to TBitmap.
            // Draw picture on bitmap instead.
            Result.Width  := Picture.Graphic.Width;
            Result.Height := Picture.Graphic.Height;
            Result.PixelFormat := pf24bit;
            Result.Canvas.Draw(0, 0, Picture.Graphic);
          end
        finally
          Picture.Free
        end
      except
        RESULT.Free;
        raise
      end
    end
  end {LoadGraphicFile};
begin
  if FileExists(Arquivo) then
  begin
    Bitmap.Free;  // get rid of old bitmap
    Bitmap := LoadGraphicsFile(Arquivo);
    DisplayBitmap(Bitmap, Image, BrushColor);
  end;
end;

function TUnMyObjects.CBAnoECBMesToDate(CBAno, CBMes: TComboBox;
Dia, IncMes: Integer): TDateTime;
var
  Ano, Mes: Word;
begin
  Ano := Geral.IMV(CBAno.Text);
  Mes := CBMes.ItemIndex + 1;
  Result := IncMonth(EncodeDate(Ano, Mes, Dia), incMes);
end;

procedure TUnMyObjects.DBGridSelectAll(AGrid: Vcl.DBGrids.TDBGrid);
// para espec�ficos implementar c�digo no topo desta unit
begin
  AGrid.SelectedRows.Clear;
  with AGrid.DataSource.DataSet do
  begin
    DisableControls;
    First;
    try
      while not EOF do
      begin
        AGrid.SelectedRows.CurrentRowSelected := True;
        Next;
      end;
    finally
      EnableControls;
    end;
  end;
end;

function TUnMyObjects.DBsDA_CaminhoRegistro(Caminho: String;
  ID: Integer): String;
begin
  Result := Caminho + '\' + DBsDA_FmtRegistro(ID);
end;

function TUnMyObjects.DBsDA_CaminhoTabela(Tabela: String; Records: Boolean): String;
begin
  Result := 'DermaDB\DBS\DermaDB\' + LowerCase(Tabela);
  if Records then
    Result := Result + '\Records';
end;

function TUnMyObjects.DBsDA_FmtRegistro(ID: Integer): String;
begin
  Result := Geral.FFN(ID, 9);
end;

procedure TUnMyObjects.DBGridLarguraSegundoTitulo(DBGTitul, DBGDados: Vcl.DBGrids.TDBGrid;
Colunas: array of Integer);
var
  I, J, T, N, K: Integer;
begin
  K := 0;
  for I := Low(Colunas) to High(Colunas) do
  begin
    T := 0;
    for J := 1 to Colunas[I] do
    begin
      K := K + 1;
      try
        N := THackDBGrid(DBGDados).ColWidths[K];
        T := T + N;
      except
        Geral.MB_Erro(('N�o foi poss�vel obter a largura da' +
        'coluna ' + IntToStr(K+1) + ' da grade "' + DBGDados.Name + '"!'));
      end;
    end;
    N := Colunas[I];
    THackDBGrid(DBGTitul).ColWidths[I + 1] := T + N -1;
  end;
end;

procedure TUnMyObjects.PageControlChange(Sender: TObject);
begin

{  Caption := CaptionAplicacao + ' - ' + PageControl.ActivePage.Caption;
  Application.Title := Caption;
}
  with (TPageControl(Sender).ActivePage.Components[0] as TForm) do
  begin
    VAR_CaptionFormOrigem := Caption;
    VAR_NomeCompoF7 := '';
    if not Assigned(Parent) then
      Show;
  end;
end;

function TUnMyObjects.Ping(Host: String; ReceiveTimeout: Integer): Boolean;
var
  IdICMPClient: TIdICMPClient;
begin
  try
    IdICMPClient := TIdICMPClient.Create(nil);
    IdICMPClient.Host := Host;
    //IdICMPClient.ReceiveTimeout := 500;
    if ReceiveTimeout < 10000 then
      ReceiveTimeout := 10000;
    IdICMPClient.ReceiveTimeout := ReceiveTimeout;
    IdICMPClient.Ping;
    //
    Result := (IdICMPClient.ReplyStatus.BytesReceived > 0);
    //
    IdICMPClient.Free;
  except
    IdICMPClient.Free;
    //
    Result := False;
  end
end;

function TUnMyObjects.PreencheCBAnoECBMes(CBAno, CBMes: TComboBox;
IncremMes: Integer = 1; Nulo: Boolean = False): String;
var
  Data: TDateTime;
  Ano, Mes, Dia: Word;
  I: Integer;
begin
  with CBMes.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  if Nulo then
  begin
    CBAno.ItemIndex := - 1;
    CBMes.ItemIndex := - 1;
  end else
  begin
    Data := IncMonth(Date, IncremMes);
    DecodeDate(Data, Ano, Mes, Dia);
    with CBAno.Items do
      for I := 2000 to Ano + 100 do Add(IntToStr(I));
    CBAno.ItemIndex := Ano - 2000;
    CBMes.ItemIndex := (Mes - 1);
  end;
end;

function TUnMyObjects.PreencheCBAnoECBMes2(CBAno, CBMesI, CBMesF: TComboBox;
  IncremMesI, IncremMesF: Integer; Nulo: Boolean): String;
var
  Data: TDateTime;
  Ano, Mes, Dia: Word;
  I: Integer;
  procedure PreencheMes(CBMes: TComboBox);
  begin
    with CBMes.Items do
    begin
      Add(FIN_JANEIRO);
      Add(FIN_FEVEREIRO);
      Add(FIN_MARCO);
      Add(FIN_ABRIL);
      Add(FIN_MAIO);
      Add(FIN_JUNHO);
      Add(FIN_JULHO);
      Add(FIN_AGOSTO);
      Add(FIN_SETEMBRO);
      Add(FIN_OUTUBRO);
      Add(FIN_NOVEMBRO);
      Add(FIN_DEZEMBRO);
    end;
  end;
begin
  if Nulo then
  begin
    CBAno.ItemIndex := - 1;
    CBMesI.ItemIndex := - 1;
    CBMesF.ItemIndex := - 1;
  end else
  begin
    PreencheMes(CBMesI);
    PreencheMes(CBMesF);
    Data := IncMonth(Date, IncremMesI);
    DecodeDate(Data, Ano, Mes, Dia);
    with CBAno.Items do
      for I := 2000 to Ano + 100 do Add(IntToStr(I));
    CBAno.ItemIndex := Ano - 2000;
    CBMesI.ItemIndex := (Mes - 1);
    //
    Data := IncMonth(Date, IncremMesF);
    DecodeDate(Data, Ano, Mes, Dia);
    CBMesF.ItemIndex := (Mes - 1);
  end;
end;

function TUnMyObjects.PreencheComponente(Componente: TComponent;
  MyArray: array of String; Colunas: Integer = 0): Boolean;
var
  i: Integer;
begin
  Result := False;
  if Componente is TRadioGroup then
  begin
    TRadioGroup(Componente).Columns := Colunas;
    TRadioGroup(Componente).Items.Clear;
    for i := Low(MyArray) to High(MyArray) do
      TRadioGroup(Componente).Items.Add(MyArray[i]);
    Result := True;
  end
  else
  if Componente is TDBRadioGroup then
  begin
    TDBRadioGroup(Componente).Columns := Colunas;
    TDBRadioGroup(Componente).Items.Clear;
    for i := Low(MyArray) to High(MyArray) do
      TDBRadioGroup(Componente).Items.Add(MyArray[i]);
    Result := True;
  end
  else
  if Componente is TdmkCheckGroup then
  begin
    TdmkCheckGroup(Componente).Columns := Colunas;
    TdmkCheckGroup(Componente).Items.Clear;
    for i := Low(MyArray) to High(MyArray) do
      TdmkCheckGroup(Componente).Items.Add(MyArray[i]);
    Result := True;
  end
  else
  if Componente is TComboBox then
  begin
    TComboBox(Componente).Items.Clear;
    for i := Low(MyArray) to High(MyArray) do
      TComboBox(Componente).Items.Add(MyArray[i]);
    Result := True;
  end
  else Geral.MB_Erro('Classe de componente n�o implementada na function: ' +
  sLineBreak + '"TUnMyObjects.PreencheComponente()"');
end;

function TUnMyObjects.ProximoRegistroBookMark(DBGrid: Vcl.DBGrids.TDBGrid; Indice: Integer): Boolean;
var
  I, X: Integer;
begin
(*
  for I := 0 to DBGrid.SelectedRows.Count - 1 do
  begin
    DBGrid.SelectedRows.Items[I].
    X := ?
  end;
*)
end;

function TUnMyObjects.DefineIndexComboBox(CBx: TComboBox; Valor: String): Boolean;
var
{
  Data: TDateTime;
  Ano, Mes, Dia: Word;
  FIni,
}
  I: Integer;
begin
  Result := False;
  for I := 0 to CBx.Items.Count - 1 do
  begin
    if CBx.Items[I] = Valor then
    begin
      CBx.ItemIndex := I;
      Result := True;
      Break;
    end;
  end;
  if not Result then Geral.MB_Aviso('N�o foi poss�vel setar o valor "' +
  Valor + '" para o ComboBox "' + CBx.Name + '".');
end;

procedure TUnMyObjects.DefinePageControlActivePageByName(
  PageControl: TPageControl; Nome: String; DefaultPageIndex: Integer);
var
  I: Integer;
begin
  if Nome = '' then
  begin
    PageControl.ActivePageIndex := DefaultPageIndex;
    Exit;
  end else
  begin
    for I := 0 to PageControl.PageCount -1 do
    begin
      if Lowercase(PageControl.Pages[I].Name) = Lowercase(Nome) then
      begin
        PageControl.ActivePageIndex := I;
        Exit;
      end;
    end;
  end;
end;

procedure TUnMyObjects.PulaCelulaGradeInput(GradePula, GradeCodi: TStringGrid;
var Key: Word; Shift: TShiftState);
  function PulaCelula(): Boolean;
  var
    Col, Row: Integer;
  begin
    //Result := False;
    Col := GradePula.Col + 1;
    if Col > GradePula.ColCount -1 then
    begin
      Col := 1;
      Row := GradePula.Row + 1;
    end else Row := GradePula.Row;
    try
      GradePula.Col := Col;
      GradePula.Row := Row;
      GradePula.Invalidate;
      Result := True;
    finally
      ;
    end;
  end;
var
  //Col, Row: Integer;
  SemErro: Boolean;
begin
  if VAR_VK_13_PRESSED or (Key = VK_RETURN) then
  begin
    if ((GradePula.ColCount = 1) and (GradePula.RowCount = 1))
    or ((GradePula.Col = GradePula.ColCount -1)
    and (GradePula.Row = GradePula.RowCount -1)) then Exit;
    Key := 0;
    //
    PulaCelula;
    while (GradeCodi.Cells[GradePula.Col, GradePula.Row] = '')
    and ((GradePula.Col < GradePula.ColCount -1)
    or (GradePula.Row < GradePula.RowCount -1)) do
    begin
      SemErro := PulaCelula();
      if not SemErro then
        Exit;
    end;
  end;
end;

procedure TUnMyObjects.PulaCelulaGradeInputEx(GradePula, GradeCodi: TStringGrid;
var Key: Word; Shift: TShiftState; BtOK: TBitBtn);
  function PulaCelula(): Boolean;
  var
    Col, Row: Integer;
  begin
    //Result := False;
    Col := GradePula.Col + 1;
    if Col > GradePula.ColCount -1 then
    begin
      Col := 1;
      Row := GradePula.Row + 1;
    end else Row := GradePula.Row;
    try
      GradePula.Col := Col;
      GradePula.Row := Row;
      GradePula.Invalidate;
      Result := True;
    finally
      ;
    end;
  end;
var
  //Col, Row: Integer;
  SemErro: Boolean;
begin
  {
  Col := GradePula.Col;
  Row := GradePula.Row;
  }
  if VAR_VK_13_PRESSED or (Key = VK_RETURN) then
  begin
    if ((GradePula.ColCount = 1) and (GradePula.RowCount = 1))
    or ((GradePula.Col = GradePula.ColCount -1)
    and (GradePula.Row = GradePula.RowCount -1)) then
    begin
      if ((GradePula.Col = GradePula.ColCount -1)
      and (GradePula.Row = GradePula.RowCount -1)) then
      begin
        if (BtOk <> nil) and (BtOK.Enabled) then
          BtOk.SetFocus;
      end;
      Exit;
    end;
    Key := 0;
    //
    PulaCelula;
    while (GradeCodi.Cells[GradePula.Col, GradePula.Row] = '')
    and ((GradePula.Col < GradePula.ColCount -1)
    or (GradePula.Row < GradePula.RowCount -1)) do
    begin
      SemErro := PulaCelula();
      if not SemErro then
        Exit;
    end;
    {
    if ((GradePula.Col = GradePula.ColCount -1)
    and (GradePula.Row = GradePula.RowCount -1)) then
    begin
      if ((GradePula.Col = Col)
      and (GradePula.Row = Row)) then
      begin
        if BtOk <> nil then
          BtOk.SetFocus;
      end;
    end;
    }
  end;
end;

{$IFNDEF NAO_USA_GLYFS}
procedure TUnMyObjects.DesenhaGradeGeral(Grade: TStringGrid; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState; Alinhamentos: array of TAlignment;
  AllColorFixo: Boolean);
var
  Cor: TColor;
  //N: integer;
  A: TAlignment;
begin
  if (ACol < Grade.FixedCols) or (ARow < Grade.FixedRows) then
    //Cor := FmPrincipal.sd1.Colors[csScrollbar]
    Cor := SubstituiCorParaSkin(csScrollbar)
  else if AllColorFixo then
    //Cor := FmPrincipal.sd1.Colors[csButtonFace]
    Cor := SubstituiCorParaSkin(csButtonFace)
  else
    Cor := clWindow;

  //

  if High(Alinhamentos) <> Grade.ColCount -1 then Geral.MB_Aviso(
  'Quantidade de alinhamentos incorreto no desenho de grade!');

  //

  A := taLeftJustify;
  if High(Alinhamentos) <= Grade.ColCount then
   A := Alinhamentos[ACol];

  DesenhaTextoEmStringGrid(Grade, Rect, clBlack, Cor, A,
    Grade.Cells[Acol, ARow], Grade.FixedCols, Grade.FixedRows, False)
end;

procedure TUnMyObjects.DesenhaGradeNumeros(Grade: TStringGrid; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState; AllColorFixo: Boolean);
var
  Cor: TColor;
  //N: integer;
  A: TAlignment;
begin
  if (ACol < Grade.FixedCols) or (ARow < Grade.FixedRows) then
    A := taLeftJustify
  else
    A := taRightJustify;

  //

  if (ACol < Grade.FixedCols) or (ARow < Grade.FixedRows) then
    //Cor := FmPrincipal.sd1.Colors[csScrollbar]
    Cor := SubstituiCorParaSkin(csScrollbar)
  else if AllColorFixo then
    //Cor := FmPrincipal.sd1.Colors[csButtonFace]
    Cor := SubstituiCorParaSkin(csButtonFace)
  else
    Cor := clWindow;

  //

  DesenhaTextoEmStringGrid(Grade, Rect, clBlack, Cor, A,
    Grade.Cells[Acol, ARow], Grade.FixedCols, Grade.FixedRows, False)
end;

procedure TUnMyObjects.DesenhaGradeA(GradeA: TStringGrid; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState; AllColorFixo: Boolean);
  procedure DrawGrid(Grade: TStringGrid; const Rect: TRect;
  FixedCols, FixedRows, Valor: Integer; CorFundo: TColor);
  var
    W1,H1,Glyph: Integer;
    Pos: MyArrayI02;
    Bmp: TBitmap;
  begin
    if Valor < 0 then Valor := 0;
    if Valor > 2 then Valor := 2;
    with Grade.Canvas do
    begin
      Brush.Color := CorFundo;
      FillRect(Rect);
      W1 := FmMyGlyfs.ListaCheck3.Width;
      H1 := FmMyGlyfs.ListaCheck3.Height;
      Pos := dmkPF.CoordenadasIniciais(Rect.Left,Rect.Top,Rect.Right,Rect.Bottom,W1,H1);
      //Brush.Color := FmPrincipal.sd1.colors[csButtonFace];
      //Font.Color  := FmPrincipal.sd1.colors[csButtonFace];
      FillRect(Rect);
      //TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      Glyph := {(VAR_IDTXTBMP*2)+}Valor;
      Bmp := TBitmap.Create;
      try
        Bmp.Width := W1;
        Bmp.Height := H1;
        FmMyGlyfs.ListaChecks.GetBitmap(Glyph, Bmp);
        Draw(Pos[1], Pos[2], Bmp);
      finally
        Bmp.Free;
      end;
    end;
    GradeA.FixedCols := FixedCols;
    GradeA.FixedRows := FixedRows;
  end;
var
  Ativo: Integer;
  Cor: TColor;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          //FmPrincipal.sd1.Colors[csScrollbar],
          SubstituiCorParaSkin(csScrollbar),
          taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          //FmPrincipal.sd1.Colors[csScrollbar],
          SubstituiCorParaSkin(csScrollbar),
          taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    if AllColorFixo then
      //Cor := FmPrincipal.sd1.Colors[csButtonFace]
      Cor := SubstituiCorParaSkin(csButtonFace)
    else
      Cor := clWindow;
    DrawGrid(GradeA, Rect, 0, 0, Ativo, Cor);
  end;
end;

procedure TUnMyObjects.DesenhaGradeN(GradeDesenhar, GradeAtivos, GradeCompara:
              TStringGrid; ACol, ARow: Integer; Rect: TRect; State:
              TGridDrawState; Fmt: String;
              FixedCols, FixedRows: Integer; PermiteVazio: Boolean);
var
  CorCel, CorTxt: Integer;
  Texto: String;
  Valor, Origi: Double;
begin
  CorTxt := clGray;
  if (ACol = 0) or (ARow = 0) then
    //CorCel := FmPrincipal.sd1.Colors[csScrollbar]//csButtonFace]
    CorCel := SubstituiCorParaSkin(csScrollbar)
  else begin
    //CorCel := FmPrincipal.SD1.Colors[csButtonFace];
    CorCel := SubstituiCorParaSkin(csButtonFace);
    Valor  := Geral.DMV(GradeDesenhar.Cells[ACol, ARow]);
    if PermiteVazio and (Valor = 0) then  Texto := '' else
    begin
      if Fmt <> '' then
        Texto := FormatFloat(Fmt(*Dmod.FStrFmtPrc*), Valor)
      else
        Texto := GradeDesenhar.Cells[ACol, ARow];
      //
      //CorTxt := FmPrincipal.sd1.Colors[csText];
      CorTxt := SubstituiCorParaSkin(csText);
      CorCel := clWindow;
      if GradeAtivos <> nil then
      begin
        if GradeAtivos.Cells[ACol, ARow] <> '' then
        begin
          if GradeCompara <> nil then
          begin
            Origi := Geral.DMV(GradeCompara.Cells[ACol, ARow]);
            if Origi > Valor then
              CorTxt := clRed
            else if Origi < Valor then
              CorTxt := clBlue
          end;
        end else begin
          if  (GradeDesenhar.Cells[ACol,ARow] <> '')
          then  GradeDesenhar.Cells[ACol,ARow] := '';
          Texto := '';
          CorCel := clMenu;
        end;
      end;
    end;
  end;
  //

  if (ACol = 0) or (ARow = 0) then
    DesenhaTextoEmStringGrid(GradeDesenhar, Rect, clBlack,
      CorCel(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
      GradeDesenhar.Cells[Acol, ARow], FixedCols, FixedRows, False)
  else
    DesenhaTextoEmStringGrid(GradeDesenhar, Rect, CorTxt,
      CorCel(*FmPrincipal.sd1.Colors[csButtonFace]*), taRightJustify,
      (*GradeF.Cells[AcolARow]*)Texto, FixedCols, FixedRows, False);
end;
{$ENDIF}

procedure TUnMyObjects.DesenhaTextoEmDBGrid(Grade: Vcl.DBGrids.TDBGrid; Rect: TRect;
  CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String;
  Negrito: Integer = -1; CorPen: TColor = clSilver);
{
antigo:
    procedure M L A G e r a l.DesenhaTextoEmGrade2(Grade: TDBGrid; Rect: TRect;
              CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String);
}
var
  //FuRect,
  MyRect: TRect;
  OldAlign: Integer;
  LE, RI, CE, L, T, R, B: Integer;
begin
  LE := Rect.Left+2;
  RI := Rect.Right-2;
  CE := (Rect.Right + Rect.Left) div 2;
  MyRect.Left   := Rect.Left+2;
  MyRect.Top    := Rect.Top+2;
  MyRect.Right  := Rect.Right-2;
  MyRect.Bottom := Rect.Bottom;
  L := Rect.Right;
  T := Rect.Top;
  R := Rect.Right;
  B := Rect.Bottom;
  with Grade.Canvas do
  begin
    Brush.Color := CorFundo;
    //Pen.Color := clSilver;
    Pen.Color := CorPen;
    FillRect(Rect);
    Font.Color := CorTexto;
    case Alinha of
      taRightJustify: OldAlign := SetTextAlign(Handle, TA_RIGHT);
      taLeftJustify:  OldAlign := SetTextAlign(Handle, TA_LEFT);
      taCenter:       OldAlign := SetTextAlign(Handle, TA_CENTER);
      else            OldAlign := SetTextAlign(Handle, TA_LEFT);
    end;
    FillRect(MyRect);
    if Negrito <> -1 then
    begin
      if Negrito = 1 then
        Font.Style := [fsBold]
      else
        Font.Style := [];
    end;
    case Alinha of
      taRightJustify: TextOut(RI, MyRect.Top, Texto);
      taLeftJustify:  TextOut(LE, MyRect.Top, Texto);
      taCenter:       TextOut(CE, MyRect.Top, Texto);
      else            TextOut(LE, MyRect.Top, Texto);
    end;
(*
    if Negrito <> -1 then
    begin
      if Negrito = 1 then
        Font.Style := [fsBold]
      else
        Font.Style := [];
    end;
*)
    //
    SetTextAlign(Handle, OldAlign);
    Polygon([Point(L, T), Point(L, B), Point(R, B), Point(R, T)]);
  end;
  //Grade.Invalidate;
  THackDBGrid(Grade).FixedCols := 1;
end;

procedure TUnMyObjects.DesenhaTextoEmStringGrid(Grade: TStringGrid; Rect: TRect;
  CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String; FixedCols,
  FixedRows: Integer; DesenhaSelecionado: Boolean);
var
  //FuRect,
  MyRect: TRect;
  OldAlign: Integer;
  LE, RI, CE, L, T, R, B: Integer;
begin
  LE := Rect.Left+2;
  RI := Rect.Right-2;
  CE := (Rect.Right + Rect.Left) div 2;
  MyRect.Left   := Rect.Left+2;
  MyRect.Top    := Rect.Top+2;
  MyRect.Right  := Rect.Right-2;
  MyRect.Bottom := Rect.Bottom;
  L := Rect.Right;
  T := Rect.Top;
  R := Rect.Right;
  B := Rect.Bottom;
  with Grade.Canvas do
  begin
    Brush.Color := CorFundo;
    FillRect(Rect);
    Font.Color := CorTexto;
    //Font.Name  := 'Arial';
    case Alinha of
      taRightJustify: OldAlign := SetTextAlign(Handle, TA_RIGHT);
      taLeftJustify:  OldAlign := SetTextAlign(Handle, TA_LEFT);
      taCenter:       OldAlign := SetTextAlign(Handle, TA_CENTER);
      else            OldAlign := SetTextAlign(Handle, TA_LEFT);
    end;
    FillRect(MyRect);
    case Alinha of
      taRightJustify: TextOut(RI, MyRect.Top, Texto);
      taLeftJustify:  TextOut(LE, MyRect.Top, Texto);
      taCenter:       TextOut(CE, MyRect.Top, Texto);
      else            TextOut(LE, MyRect.Top, Texto);
    end;
    SetTextAlign(Handle, OldAlign);
    //if DesenhaSelecionado then  (Nada a ver; ver outra coisa)
    Polygon([Point(L, T), Point(L, B), Point(R, B), Point(R, T)]);
  end;
  //
  if FixedCols < THackDBGrid(Grade).ColCount then
    THackDBGrid(Grade).FixedCols := FixedCols
  else
    THackDBGrid(Grade).FixedCols := 0;
  //
  if FixedRows < THackDBGrid(Grade).RowCount then
    THackDBGrid(Grade).FixedRows := FixedRows
  else
    THackDBGrid(Grade).FixedRows := 0;
end;

procedure TUnMyObjects.DesenhaTextoEmStringGridEx(Grade: TStringGrid;
  Rect: TRect; CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String;
  FixedCols, FixedRows: Integer; DesenhaSelecionado: Boolean; FonteNome: String;
  FonteSize: Integer; FonteStyle: TFontStyles; PasswordChar: Char);
var
  //FuRect,
  MyRect: TRect;
  OldAlign: Integer;
  LE, RI, CE, L, T, R, B: Integer;
begin
  LE := Rect.Left+2;
  RI := Rect.Right-2;
  CE := (Rect.Right + Rect.Left) div 2;
  MyRect.Left   := Rect.Left+2;
  MyRect.Top    := Rect.Top+2;
  MyRect.Right  := Rect.Right-2;
  MyRect.Bottom := Rect.Bottom;
  L := Rect.Right;
  T := Rect.Top;
  R := Rect.Right;
  B := Rect.Bottom;
  with Grade.Canvas do
  begin
    Brush.Color := CorFundo;
    FillRect(Rect);
    Font.Color  := CorTexto;
    Font.Name   := FonteNome;
    Font.Size   := FonteSize;
    Font.Style  := FonteStyle;
    if PasswordChar <> '' then
    //:= 'l'
    ;
    case Alinha of
      taRightJustify: OldAlign := SetTextAlign(Handle, TA_RIGHT);
      taLeftJustify:  OldAlign := SetTextAlign(Handle, TA_LEFT);
      taCenter:       OldAlign := SetTextAlign(Handle, TA_CENTER);
      else            OldAlign := SetTextAlign(Handle, TA_LEFT);
    end;
    FillRect(MyRect);
    case Alinha of
      taRightJustify: TextOut(RI, MyRect.Top, Texto);
      taLeftJustify:  TextOut(LE, MyRect.Top, Texto);
      taCenter:       TextOut(CE, MyRect.Top, Texto);
      else            TextOut(LE, MyRect.Top, Texto);
    end;
    SetTextAlign(Handle, OldAlign);
    //if DesenhaSelecionado then  (Nada a ver; ver outra coisa)
    Polygon([Point(L, T), Point(L, B), Point(R, B), Point(R, T)]);
  end;
  THackDBGrid(Grade).FixedCols := FixedCols;
  THackDBGrid(Grade).FixedRows := FixedRows;
end;

procedure TUnMyObjects.DisplayBitmap(const Bitmap: TBitmap;
  const Image: TImage; BrushColor: TColor);
var
  Half      :  Integer;
  Height    :  Integer;
  NewBitmap :  TBitmap;
  TargetArea:  TRect;
  Width     :  Integer;
begin
  if Bitmap = nil then
    Exit;
  //
  NewBitmap := TBitmap.Create;
  try
    NewBitmap.Width  := Image.Width;
    NewBitmap.Height := Image.Height;
    NewBitmap.PixelFormat := pf24bit;

    NewBitmap.Canvas.Brush.Style := bsSolid;
    NewBitmap.Canvas.Brush.Color := BrushColor;
    NewBitmap.Canvas.FillRect(NewBitmap.Canvas.ClipRect);

    // "equality" (=) case can go either way in this comparison

    if Bitmap.Width / Bitmap.Height  <  Image.Width / Image.Height then
    begin
      // Stretch Height to match.
      TargetArea.Top    := 0;
      TargetArea.Bottom := NewBitmap.Height;
      // Adjust and center Width.
      Width := MulDiv(NewBitmap.Height, Bitmap.Width, Bitmap.Height);
      Half := (NewBitmap.Width - Width) div 2;
      //
      TargetArea.Left  := Half;
      TargetArea.Right := TargetArea.Left + Width;
    end else
    begin
      // Stretch Width to match.
      TargetArea.Left    := 0;
      TargetArea.Right   := NewBitmap.Width;
      // Adjust and center Height.
      Height := MulDiv(NewBitmap.Width, Bitmap.Height, Bitmap.Width);
      Half := (NewBitmap.Height - Height) div 2;
      //
      TargetArea.Top    := Half;
      TargetArea.Bottom := TargetArea.Top + Height
    end;
    //
    NewBitmap.Canvas.StretchDraw(TargetArea, Bitmap);
    Image.Picture.Graphic := NewBitmap
  finally
    NewBitmap.Free
  end
end {DisplayBitmap};

function TUnMyObjects.FIC(FaltaInfoCompo: Boolean; Compo: TComponent;
  Mensagem: String; ExibeMsg: Boolean = True): Boolean;
var
  Comp: TWinControl;
begin
  if FaltaInfoCompo then
  begin
    Screen.Cursor := crDefault;
    Result := True;
    if (Mensagem <> '') and ExibeMsg then
      Geral.MB_Aviso(Mensagem);
    if Compo <> nil then
    begin
      Comp := TWinControl(Compo);
      //
      if (Comp.Visible) and (Comp.Enabled) and (Comp.CanFocus) then
      try
        Comp.SetFocus;
      except
        ;
      end;
    end;
  end else Result := False;
end;

function TUnMyObjects.FIC_Edit_Int(const Edit: TdmkEdit; const Mensagem: String;
  var Valor: Integer): Boolean;
{
var
  FaltaInfoCompo: Boolean;
}
begin
  Valor := Edit.ValueVariant;
  Result := FIC(Valor = 0, Edit, Mensagem);
end;

function TUnMyObjects.FIC_MCI(CkContinuar: TCheckBox;
  RGModoContinuarInserindo: TRadioGroup): Boolean;
begin
  Result := MyObjects.FIC(CkContinuar.Checked
  and (RGModoContinuarInserindo.ItemIndex < 1), RGModoContinuarInserindo,
  'Informe o "Modo de continuar inserindo"!');
end;

function TUnMyObjects.FIC_ArquivoNaoExiste(var Dir: String; const Arq: String;
  var Caminho: String): Boolean;
begin
  if (Length(Dir) > 0) and (Length(Arq) > 0) then
  Geral.VerificaDir(Dir, '\', Arq, True);
  Caminho := Dir + Arq;
  Result := FileExists(Caminho) = False;
  if Result then
    Geral.MB_Erro('Arquivo n�o encontrado: ' + Caminho);
end;

function TUnMyObjects.FIC_Edit_Flu(const Edit: TdmkEdit; const Mensagem: String;
  var Valor: Double): Boolean;
{
var
  FaltaInfoCompo: Boolean;
}
begin
  Valor := Edit.ValueVariant;
  Result := FIC(Valor = 0, Edit, Mensagem);
end;

function TUnMyObjects.FileOpenDialog(Form: TForm; const IniDir, Arquivo, Titulo, Filtro:
  String; Opcoes: TOpenOptions; var Resultado: String): Boolean;
const
  TxtPadraoDir = 'Sele��o de Pastas.';
var
  VerWin: MyArrayI03;
  OpenDialog: TOpenDialog;
{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}
  FileOpenDialog: TFileOpenDialog;
{$WARN UNIT_PLATFORM ON}
{$WARN SYMBOL_PLATFORM ON}
  Arq, NovoFiltro, Item, Tipo, Extensao: String;
begin
  Arq := Arquivo;
  Result := false;
  Resultado := '';
  VerWin := dmkPF.VersaoWindows;
  if VerWin[1] < 6 then
  begin
    OpenDialog := TOpenDialog.Create(Form);
    try
      OpenDialog.InitialDir := IniDir;
      OpenDialog.Title      := Titulo;//'Abrir e editar arquivo Excel - Empresa';
      OpenDialog.Filter     := Filtro;//'Arquivo EXCEL (*.XLS)|*.xls';
      OpenDialog.FileName   := Arquivo;
      OpenDialog.Options    := OpenDialog.Options + Opcoes;
      //
      if Length(OpenDialog.FileName) = 0 then
        OpenDialog.FileName := TxtPadraoDir;
      //
      if OpenDialog.Execute then
      begin
        Resultado := OpenDialog.FileName;
        Result := True;
      end;
    finally
      OpenDialog.free;
    end;
  end else begin
{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}
    FileOpenDialog := TFileOpenDialog.Create(Form);
    try
      try
        FileOpenDialog.DefaultFolder := IniDir;
        FileOpenDialog.Title      := Titulo;//'Abrir Arquivo de Concilia��o Banc�ria';
        NovoFiltro := Filtro; // Ex.: Filtro := 'Arquivos OFC|*.ofc;Arquivos OFX|*.ofx';
        while NovoFiltro <> '' do
        begin
          if Geral.SeparaPrimeiraOcorrenciaDeTexto(';', NovoFiltro, Item, NovoFiltro) then
          begin
            if Geral.SeparaPrimeiraOcorrenciaDeTexto('|', Item, Tipo, Extensao) then
            begin
              with FileOpenDialog.FileTypes.Add do
              begin
                DisplayName := Tipo;
                FileMask := Extensao;
              end;
            end;
          end;
        end;
        FileOpenDialog.FileName   := Arq;
        //
        if Length(FileOpenDialog.FileName) = 0 then
          FileOpenDialog.FileName := TxtPadraoDir;
        //
        //FileOpenDialog.Options    := FileOpenDialog.Options + Opcoes;
        //
      except
        // nada
      end;
      if FileOpenDialog.Execute then
      begin
        Resultado := FileOpenDialog.FileName;
        Result := True;
      end;
    finally
      FileOpenDialog.free;
    end;
  end;
{$WARN UNIT_PLATFORM ON}
{$WARN SYMBOL_PLATFORM ON}
end;

function TUnMyObjects.FormCria(InstanceClass: TComponentClass;
  var Reference): Boolean;
var
  Instance: TComponent;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(Application);
      Result := True;
    except
      TComponent(Reference) := nil;
      raise;
    end;
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;

//{$IFNDEF NAO_USA_DB}
procedure TUnMyObjects.FormMsg(var Msg: TMsg; var Handled: Boolean);
var
  Controle : TWinControl;
  LaTipo: TLabel;
  BitBtn: TBitBtn;
  NomeBtn: String;
  OK, OK1: Boolean;
  //x: String;
  Pai: TComponent;
  Ctrl: TWinControl;
begin
  { N�o funciona!
  if Msg.message = MSG_DMK_EditEdIdxTxt then
  begin
    ShowMessage('EdIdx OK!');
  end else}
  if Msg.Message = WM_SYSCOMMAND then
  begin
    //Minimiza a aplica��o ao minimizar o form
    if (Msg.wParam AND $fff0 ) = SC_MINIMIZE then
    begin
      EnableWindow (Application.handle, true );
      Application.Minimize;
      //
      OldRestore := Application.OnRestore;
      //
      Application.OnRestore := DoRestore;
      Msg.wParam := 0;
    end;
  end else
  if Msg.Message = 256 then
  begin
    (*
    if (Msg.wParam = 91) then //17 =ctrl
    begin
      FmCoresVCLSkin.Memo1.Lines.Add(Geral.FF0(Msg.wParam) + ' | ' +
      Geral.FF0(Msg.lParam));
    end else
    *)
    if (Msg.wParam = 9) or (Msg.wParam = 13) then //Tab / Enter
    begin
      Ctrl := FindControl(Msg.hWnd);
      if (Ctrl <> nil) then
      begin
        if (Ctrl is TDateTimePickerdmkEdit) then
        begin
          if GetKeyState(VK_SHIFT) < 0 then
          begin
            TDateTimePickerdmkEdit(Ctrl).GoBackCompo();
            Msg.Message := 256;
            Msg.wParam  := 0;
          end;
        end;
      end;
    end else
    if Msg.wParam = 32 then
    begin
      Ctrl := FindControl(Msg.hWnd);
      if (Ctrl <> nil) then
      begin
        if (Ctrl is TBitBtn)
        or (Ctrl is TButton)
        or (Ctrl is TCheckBox)
        or (Ctrl is TRadioButton) then
        begin
          if (Ctrl is TCheckBox) then
          begin
            if TCheckBox(Ctrl).Enabled then
            TCheckBox(Ctrl).Checked := not TCheckBox(Ctrl).Checked;
          end;
          //
          Msg.wParam := 13;
        end;
      end;
    end;
  end;
  if Msg.Message <> 275 then
  begin
    if VAR_TIMERIDLE <> nil then
    begin
      if VAR_TIMERIDLE.Interval > 0  then
      begin
        VAR_TIMERIDLE.Enabled := False;
        VAR_TIMERIDLE.Enabled := True;
      end;
    end;
  end;
  if Msg.Message= WM_KEYUP then
  begin
    if Msg.wParam = VK_CONTROL then
    VAR_GOTOYJUST := False;
    if Msg.wParam = VK_SNAPSHOT	(*44*) then
    begin
      if Screen.ActiveForm = nil then Exit;
      Controle := Screen.ActiveForm.ActiveControl;
      if Controle <> nil then
      begin
        //Geral.MB_Info(Controle.ClassName);
{$IFNDEF NAO_USA_DB}
        if (Controle is TCustomDBGrid) then
          FmMeuDBUses.ImprimeDBGrid(TDBGrid(Controle), TDBGrid(Controle).Name)
        else
        if (Controle is TDBGrid) or (Controle is TdmkDBGrid) or
        (Controle is TdmkDBGridDAC) or (Controle is TdmkDBGridZTO) then
          FmMeuDBUses.ImprimeDBGrid(TDBGrid(Controle), TDBGrid(Controle).Name)
        else
{$ENDIF}
{$IFNDEF NAO_USA_DB_GERAL}
        if (Controle is TStringGrid) then
        begin
          FmMeuDBUses.ImprimeStringGrid(TStringGrid(Controle), TStringGrid(Controle).Name);
        end;
{$ENDIF}
{$IFDEF DB_SQLite}
        if (Controle is TDBGrid) or (Controle is TdmkDBGrid) or
        (Controle is TdmkDBGridDAC) or (Controle is TdmkDBGridZTO) then
          FmMeuDBUsesZ.ImprimeDBGrid(TDBGrid(Controle), TDBGrid(Controle).Name)
        else
        if (Controle is TStringGrid) then
        begin
          FmMeuDBUsesZ.ImprimeStringGrid(TStringGrid(Controle), TStringGrid(Controle).Name);
        end;
{$ENDIF}
      end;
    end;
  end else if Msg.Message= WM_KEYDOWN then
  begin
    //VAR_VK_13_PRESSED := False;
    try
      if Msg.wParam = VK_CONTROL then
        VAR_GOTOYJUST := True;
      // n�o funciona
      {if (Msg.wParam = VK_TAB) then
      begin
        if Screen.ActiveForm = nil then Exit;
        Controle := Screen.ActiveForm.ActiveControl;
        if Controle is TdmkEdit then
        begin
          if TdmkEdit(Controle).FComponentePai = 'dmkEditDateTimePicker' then
          begin
            Control := THackControl(Controle).FindNextControl(Controle, False, True, False);
            if Control <> nil then Control.SetFocus;
          end;
        end;
      end else
      }
{$IFDEF UsaWSuport}
    if Msg.wParam = VK_F1 then
      DmkWeb.MostraWSuporte();
{$ENDIF}
      //
{$IFNDEF NO_USE_MYSQLMODULE}
      if (Msg.wParam = VK_F5) then
        FmMeuDBUses.VirtualKey_F5()
      else
      if (Msg.wParam = VK_F7) then
        FmMeuDBUses.VirtualKey_F7('')  // LocF7
      else
{$ENDIF}
{$IFDEF DB_SQLite}
      if (Msg.wParam = VK_F5) then
        FmMeuDBUsesZ.VirtualKey_F5()
      else
      if (Msg.wParam = VK_F7) then
        FmMeuDBUsesZ.VirtualKey_F7('') // LocF7
      else
{$ENDIF}
      if (Msg.wParam = VK_F11) then
      begin
        if VAR_CALCULADORA_COMPONENTCLASS <> nil then
        begin
          OK := False;//OK := Calculadora(VAR_CALCULADORA_COMPONENTCLASS, VAR_CALCULADORA_REFERENCE);
          if OK then
          begin
            if Screen.ActiveForm = nil then Exit;
            Controle := Screen.ActiveForm.ActiveControl;
            if Controle <> nil then
            begin
{
              if Controle is T L M DEdit then
                T L M D Edit(Controle).Text := VAR_CALCTEXTO
              else
}
              if Controle is TEdit then
                TEdit(Controle).Text := VAR_CALCTEXTO
            end;
          end;
        end;
      // Tecla do menu suspenso
      end else if (Msg.wParam = 93)
      //  Tecla do Windows
      or ( (Msg.wParam = 91) and VAR_GOTOYJUST) then
      begin
        if MAR_SQLx = nil then
        begin
          Geral.MB_Aviso('"MAR_SQLx" n�o definido no aplicativo!');
        end else begin
{$IFNDEF NAO_USA_DB}
          DefParams(0);
{$ENDIF}
          if VAR_CAD_POPUP <> nil then
          begin
            if VAR_SHOW_MASTER_POPUP then
            begin
(*20200919
              VAR_POPUP_ACTIVE_CONTROL := Screen.ActiveForm.ActiveControl;
              VAR_POPUP_ACTIVE_FORM    := Screen.ActiveForm;
*)
              MostraPopUpNoCentro(VAR_CAD_POPUP);
              //ReabreQuery := True;
            end else Geral.MB_Aviso('Master popup desabilitado.');
          end else Geral.MB_Aviso('"VAR_CAD_POPUP" n�o definido.');
        end;
      end;
      if Screen.ActiveForm = nil then Exit;
      if Screen.ActiveForm.ActiveOleControl is TWebBrowser then Exit;
      //
      Controle := Screen.ActiveForm.ActiveControl;
      if Controle = nil then
        Exit;
      //if (Screen.ActiveForm.Name = 'FmCalculadora') then
      //Exit;
      if (*(Screen.ActiveForm.Name = IC2_NOMEFORMAPP)
      and (Controle.Name = 'EdSenha') then exit;
      if (Screen.ActiveForm.Name = 'FmLogoff')
      and*) (Controle.Name = 'EdSenha') then exit;
      if Controle is TDateTimePickerdmkEdit then
      begin
        //Geral.MB_Aviso(IntToStr(Msg.Message));
        //Msg.wParam := VK_TAB;
      end;
      if Controle is TDBGrid then
        Exit;
      if Controle is TStringGrid then
        Exit;
      if Controle is TMemo then
        Exit;
      if Controle is TDBMemo then
        Exit;
      if Controle is TListBox then
        Exit;
      if Controle is TDBListBox then
        Exit;
      if Controle is TComboBox then
        Exit;
      if Controle is TDBComboBox then
        Exit;
      if Controle is TRichEdit then
        Exit;
      if Controle is TDBRichEdit then
        Exit;
      if Controle <> nil then
      begin
        if (Msg.wParam = IC2_LOpcoesTeclaEnter) then
        begin
          if (Controle.Name = 'EdLeituraCodigoDeBarras') then
            OK1 := False
          else
          if (Controle is TdmkEdit) then
            OK1 := not TdmkEdit(Controle).NoEnterToTab
          else
            OK1 := True;
          if OK1 then
          begin
            Msg.wParam := MeuwParamEnter(Controle);
          end;
        end else if (Msg.wParam = IC2_LOpcoesTeclaPonto) then
            Msg.wParam := MeuwParamPonto(Controle)
        (* desativado 2013-07-03
        else if Msg.wParam in ([VK_PRIOR, VK_NEXT]) then
        begin
          LaTipo := nil;
          if Screen.ActiveForm.FindComponent('LaTipo') <> nil then
          begin
            try
              LaTipo := TLabel(Screen.ActiveForm.FindComponent('LaTipo'));
            except
              ShowMessage('Componente "LaTipo" n�o � TLabel');
            end;
            if LaTipo.Caption = CO_TRAVADO then
            begin
              if VAR_GOTOYJUST then
              begin
                case Msg.wParam of
                  VK_PRIOR: NomeBtn := 'SpeedButton4'; // Contr�rio
                  VK_NEXT : NomeBtn := 'SpeedButton1'; // Contr�rio
                  else NomeBtn := '';
                end;
              end else begin
                case Msg.wParam of
                  VK_PRIOR: NomeBtn := 'SpeedButton3'; // Contr�rio
                  VK_NEXT : NomeBtn := 'SpeedButton2'; // Contr�rio
                  else NomeBtn := '';
                end;
              end;
              //
              //if Screen.ActiveForm.FindComponent(NomeBtn) <> nil then
              BitBtn := TBitBtn(Screen.ActiveForm.FindComponent(NomeBtn));
              if BitBtn <> nil then BitBtn.Click;
            end;
          end;
        end;
        *)
      end;
    except
       Geral.MB_Erro(('Erro em "WM_KEYDOWN" > Msg.wParam = ' +
       IntToStr(Msg.wParam)));
       raise;
    end;
  (*
  Desativado por incompatibilidade com o Delphi XE2

  end else if Msg.message = WM_MOUSEWHEEL then
  begin
    if Screen.ActiveForm = nil then Exit;
    Controle := Screen.ActiveForm.ActiveControl;
    if Controle <> nil
    then if Controle is TDBGrid then
    begin
      Msg.message := WM_KEYDOWN;
      Msg.lParam := 0;
      {i := HiWord(Msg.wParam);
      if i > 0 then
        Msg.wParam := VK_UP
      else
        Msg.wParam := VK_DOWN;}
      if Msg.wParam > 0 then
        Msg.wParam := VK_UP
      else
        Msg.wParam := VK_DOWN;

      Handled := False;
    end;
  *)
  end;
end;
//{$ENDIF}

function TUnMyObjects.FormShow(InstanceClass: TComponentClass;
  var Reference): Boolean;
var
  Instance: TComponent;
begin
  Screen.Cursor := crHourGlass;
  //Result := False;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(FmPrincipal);
      //Result := True;
    except
      TComponent(Reference) := nil;
      raise;
    end;
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
  //
  TForm(Reference).ShowModal;
  TForm(Reference).Destroy;
  Result := True;
end;

{$IFNDEF NAO_USA_FRX}
procedure TUnMyObjects.frxConfiguraPDF(frxPDFExport: TfrxPDFExport);
begin
  frxPDFExport.EmbeddedFonts         := True;
  frxPDFExport.EmbedFontsIfProtected := True;
  frxPDFExport.PdfA                  := True;
end;

procedure TUnMyObjects.frxDefineDataSets(frx: TfrxReport;
  DataSets: array of TfrxDataset; Avisa: Boolean);
var
  I, K, N, Ini, Fim, Vazios: Integer;
  ListaAntes: String;
begin
  N := Length(Datasets);
  K := frx.Datasets.Count;
  if (N <> K) and Avisa then
  begin
    Vazios := 0;
    ListaAntes := 'Lista anterior:';
    for I := 0 to K - 1 do
    begin
      if Trim(frx.Datasets[I].DataSetName) = '' then
        Vazios := Vazios + 1
      else
        ListaAntes := ListaAntes + sLineBreak + frx.Datasets[I].DataSetName;
    end;
    K := K - Vazios;
    if N <> K then
      Geral.MB_Aviso(ListaAntes);
  end;
  //
  N := 0;
  //K := frx.Datasets.Count;
  frx.Datasets.Clear;
  Ini := Low(Datasets);
  Fim := High(Datasets);
  for I := Ini to Fim do
  begin
    // 2013-03-30
    // N�o adianta! S� existe por compatibilidade no Fast Report!
    //DataSets[I].OpenDataSource := False;
    // Fim 2013-03-30
    frx.DataSets.Add(DataSets[I]);
    N := N + 1;
  end;
  if (N <> K) and Avisa then
  begin
    Geral.MB_Aviso('Quantidade de datasets difere!' + sLineBreak +
    'Componente frx: ' + frx.Name + sLineBreak +
    'Inicial: ' + Geral.FF0(K) + sLineBreak +
    'Final:   ' + Geral.FF0(N));
  end;
end;

function TUnMyObjects.frxImprime(frx: TfrxReport; Titulo: string): Boolean;
begin
  Result := frxPrepara(frx, Titulo);
  if Result then
  begin
    // 2012-05-12
    if VAR_NEXT_FRX_IN_DESIGN_MODE = True then
    begin
      VAR_NEXT_FRX_IN_DESIGN_MODE := False;
      frx.DesignReport;
    end else
    begin
    // FIM 2012-05-12
      FmMeuFrx.PvVer.Print;
      //
      FmMeuFrx.Destroy;
    end;
  end;
end;

procedure TUnMyObjects.frxMemoAlignment(v: TfrxMemoView; Align: Integer);
begin
  case Align of
    000: //'Esquerda no topo';
    begin
      v.VAlign := frxClass.vaTop;
      v.HAlign := frxClass.haLeft;
    end;
    001:  //'Esquerda no centro';
    begin
      v.VAlign := frxClass.vaCenter;
      v.HAlign := frxClass.haLeft;
    end;
    002: //'Esquerda em baixo';
    begin
      v.VAlign := frxClass.vaBottom;
      v.HAlign := frxClass.haLeft;
    end;
    100: //'Centro no topo';
    begin
      v.VAlign := frxClass.vaTop;
      v.HAlign := frxClass.haCenter;
    end;
    101: //'Centralizado';
    begin
      v.VAlign := frxClass.vaCenter;
      v.HAlign := frxClass.haCenter;
    end;
    102: //'Centro em baixo';
    begin
      v.VAlign := frxClass.vaBottom;
      v.HAlign := frxClass.haCenter;
    end;
    200: //'Direita no topo';
    begin
      v.VAlign := frxClass.vaTop;
      v.HAlign := frxClass.haRight;
    end;
    201: //'Direita no centro';
    begin
      v.VAlign := frxClass.vaCenter;
      v.HAlign := frxClass.haRight;
    end;
    202: //'Direita em baixo';
    begin
      v.VAlign := frxClass.vaBottom;
      v.HAlign := frxClass.haRight;
    end;
  end;
end;

function TUnMyObjects.frxMostra(frx: TfrxReport; Titulo, NomeArq: string): Boolean;
var
  Enab: Boolean;
{$IfNDef NO_USE_MYSQLMODULE}
  Query: TmySQLQuery;
  Usuario, EditRelat: Integer;
{$EndIf}
begin
  VAR_FRX_Imprimiu := False;

{$IfNDef NO_USE_MYSQLMODULE}
  Enab  := False;
  Query := TmySQLQuery.Create(Dmod);
  try
    Query.SQL.Add('SELECT sen.Numero, per.EditRelat');
    Query.SQL.Add('FROM senhas sen');
    Query.SQL.Add('LEFT JOIN perfis per ON per.Codigo = sen.Perfil');
    Query.SQL.Add('WHERE Numero=:P0');
    Query.Params[0].AsInteger := VAR_USUARIO;
    UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
    if Query.RecordCount > 0 then
    begin
      Usuario   := Query.FieldByName('Numero').AsInteger;
      EditRelat := Query.FieldByName('EditRelat').AsInteger;
      //
      if (Usuario > 0) and (EditRelat = 0) then
        Enab := False
      else
        Enab := True;
    end;
    Query.Close;
  finally
    Query.Destroy;
  end;
{$Else}
  Enab := True;
{$EndIf}
  frx.PreviewOptions.ZoomMode := zmPageWidth;
  Result := frxPrepara(frx, Titulo, NomeArq);
  if Result then
  begin
    // 2012-05-12
    if VAR_NEXT_FRX_IN_DESIGN_MODE = True then
    begin
      VAR_NEXT_FRX_IN_DESIGN_MODE := False;
      frx.DesignReport;
    end else
    // FIM 2012-05-12
    begin
      //FmMeuFrx.PvVer.Report.PreviewOptions.ZoomMode := zmPageWidth;
      FmMeuFrx.BtEdit.Enabled := Enab;
      FmMeuFrx.ShowModal;
      VAR_FRX_Imprimiu := FmMeuFrx.FImprimiu;
      FmMeuFrx.Destroy;
    end;
  end;
end;

function TUnMyObjects.frxNextDesign(): Boolean;
begin
{$IFNDEF NAO_USA_SEL_RADIO_GROUP}
  Result := SelRadioGroup('Mostrar Pr�ximo Relat�rio em Modo Desenho',
  'Selecione Inten��o', ['Sim', 'N�o'], 1) = 0;
  //
  VAR_NEXT_FRX_IN_DESIGN_MODE := Result;
{$ENDIF}
end;

function TUnMyObjects.frxPrepara(frx: TfrxReport; Titulo, NomeArq: string): Boolean;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  try
    try
      frx.PreviewOptions.AllowEdit := True;
      frx.ReportOptions.Name := Titulo;
      frx.Variables['MeuLogoExiste'] := VAR_MEULOGOEXISTE;
      frx.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
      frx.Variables['LogoNFExiste'] := VAR_LOGONFEXISTE;
      frx.Variables['LogoNFCaminho'] := QuotedStr(VAR_LOGONFPATH);
      frx.Variables['LogoDuplExiste'] := VAR_LOGODUPLEXISTE;
      frx.Variables['LogoDuplCaminho'] := QuotedStr(VAR_LOGODUPLPATH);
      frx.Variables['LogoBig1Existe'] := VAR_LOGOBIG1EXISTE;
      frx.Variables['LogoBig1Caminho'] := QuotedStr(VAR_LOGOBIG1PATH);
      frx.Variables['SloganFooter'] := QuotedStr(VAR_SLOGAN_FOOTER);
      frx.Variables['VARF_CODI_FRX'] := QuotedStr(Copy(frx.Name, 4));
      //
      try
        frx.PrepareReport;
      finally
      //
        FormCria(TFmMeuFrx, FmMeuFrx);
        frx.Preview := FmMeuFrx.PvVer;
        //
        FmMeuFrx.PvVer.OutlineWidth := frx.PreviewOptions.OutlineWidth;
        FmMeuFrx.PvVer.Zoom         := frx.PreviewOptions.Zoom;
        FmMeuFrx.PvVer.ZoomMode     := frx.PreviewOptions.ZoomMode;
        FmMeuFrx.UpdateZoom;
        //
        FmMeuFrx.FNomeArq := NomeArq;
        //
        Result := True;
        Screen.Cursor := crDefault;
      end;  
    except
      raise;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnMyObjects.frxSalva(frx: TfrxReport; Titulo,
  Arquivo: string): Boolean;
begin
  Result := frxPrepara(frx, Titulo);
  if Result then
  begin
    FmMeuFrx.PvVer.SaveToFile(Arquivo);
    FmMeuFrx.Destroy;
  end;
end;
{$ENDIF}

procedure TUnMyObjects.HabilitaBtnToPopupMenuIts(Botao: TBitBtn;
  PodeInsUpd: Boolean; QrIts: TDataSet);
begin
  Botao.Enabled := PodeInsUpd or (
    // Permitir excluir!
    (QrIts.State <> DsInactive) and (QrIts.RecordCount > 0)
  );
end;

procedure TUnMyObjects.HabilitaMenuItemCabDel(MenuItem: TMenuItem;
  QrCab, QrIts: TDataSet);
begin
  MenuItem.Enabled := (QrCab.State <> dsInactive) and (QrCab.RecordCount > 0);
  if MenuItem.Enabled and (QrIts <> nil) then
    MenuItem.Enabled := (QrIts.State <> dsInactive) and (QrIts.RecordCount = 0);
end;

procedure TUnMyObjects.HabilitaMenuItemCabDelC1I2(MenuItem: TMenuItem; QrCab,
  QrIts1, QrIts2: TDataSet);
begin
  MenuItem.Enabled := (QrCab.State <> dsInactive) and (QrCab.RecordCount > 0);
  if MenuItem.Enabled and (QrIts1 <> nil) then
    MenuItem.Enabled := (QrIts1.State <> dsInactive) and (QrIts1.RecordCount = 0);
  if MenuItem.Enabled and (QrIts2 <> nil) then
    MenuItem.Enabled := (QrIts2.State <> dsInactive) and (QrIts2.RecordCount = 0);
end;

procedure TUnMyObjects.HabilitaMenuItemCabDelC1I3(MenuItem: TMenuItem; QrCab,
  QrIts1, QrIts2, QrIts3: TDataSet);
begin
  MenuItem.Enabled := (QrCab.State <> dsInactive) and (QrCab.RecordCount > 0);
  if MenuItem.Enabled and (QrIts1 <> nil) then
    MenuItem.Enabled := (QrIts1.State <> dsInactive) and (QrIts1.RecordCount = 0);
  if MenuItem.Enabled and (QrIts2 <> nil) then
    MenuItem.Enabled := (QrIts2.State <> dsInactive) and (QrIts2.RecordCount = 0);
  if MenuItem.Enabled and (QrIts3 <> nil) then
    MenuItem.Enabled := (QrIts3.State <> dsInactive) and (QrIts3.RecordCount = 0);
end;

procedure TUnMyObjects.HabilitaMenuItemCabDelC1I4(MenuItem: TMenuItem; QrCab,
  QrIts1, QrIts2, QrIts3, QrIts4: TDataSet);
begin
  MenuItem.Enabled := (QrCab.State <> dsInactive) and (QrCab.RecordCount > 0);
  if MenuItem.Enabled and (QrIts1 <> nil) then
    MenuItem.Enabled := (QrIts1.State <> dsInactive) and (QrIts1.RecordCount = 0);
  if MenuItem.Enabled and (QrIts2 <> nil) then
    MenuItem.Enabled := (QrIts2.State <> dsInactive) and (QrIts2.RecordCount = 0);
  if MenuItem.Enabled and (QrIts3 <> nil) then
    MenuItem.Enabled := (QrIts3.State <> dsInactive) and (QrIts3.RecordCount = 0);
  if MenuItem.Enabled and (QrIts4 <> nil) then
    MenuItem.Enabled := (QrIts4.State <> dsInactive) and (QrIts4.RecordCount = 0);
end;

procedure TUnMyObjects.HabilitaMenuItemCabUpd(MenuItem: TMenuItem;
  QrCab: TDataSet);
begin
  MenuItem.Enabled := (QrCab.State <> dsInactive) and (QrCab.RecordCount > 0);
end;

procedure TUnMyObjects.HabilitaMenuItemItsDel(MenuItem: TMenuItem;
  QrIts: TDataSet; Condicao: Boolean);
begin
  MenuItem.Enabled := (QrIts.State <> dsInactive) and (QrIts.RecordCount > 0)
    and Condicao;
end;

procedure TUnMyObjects.HabilitaMenuItemItsIns(MenuItem: TMenuItem;
  QrCab: TDataSet; Condicao: Boolean);
begin
  MenuItem.Enabled := (QrCab.State <> dsInactive) and (QrCab.RecordCount > 0)
    and Condicao;
end;

procedure TUnMyObjects.HabilitaMenuItemItsUpd(MenuItem: TMenuItem;
  QrIts: TDataSet; Condicao: Boolean);
begin
  MenuItem.Enabled := (QrIts.State <> dsInactive) and (QrIts.RecordCount > 0)
    and Condicao;
end;

function TUnMyObjects.IgnoraFIC(InfoCompoInesperado: Boolean;
  Compo: TWinControl; Mensagem: String): Boolean;
begin
  if InfoCompoInesperado then
  begin
    if Geral.MB_Pergunta(Mensagem + sLineBreak +
    'Deseja continuar assim mesmo?') = ID_YES then
      Result := True
    else begin
      Screen.Cursor := crDefault;
      Result := False;
      if Compo <> nil then
      begin
        if Compo.Visible then
        try
          Compo.SetFocus;
        except
          ;
        end;
      end;
    end;
  end else Result := True;
end;

procedure TUnMyObjects.ImportaConfigWeb(Form: TForm; EdDescricao, EdHost, EdDB,
  EdPorta, EdLogin, EdSenha: TdmkEdit);
var
  Arquivo, Linha, Senha: String;
  F: TextFile;
  i, j, Num: Integer;
begin
  if FileOpenDialog(Form, '', '', 'Abrir', '*.llm', [], Arquivo) then
  begin
    i := 0;
    //
    if ExtractFileExt(Arquivo) = '.llm' then
    begin
      AssignFile(F, Arquivo);
      Reset(F);
      while not Eof(F) do
      begin
        i := i + 1;
        Readln(F, Linha);
        //
        case i of
           6: if EdDescricao <> nil then EdDescricao.Text := Linha;
           8: if EdHost <> nil then EdHost.Text           := Linha;
          10: if EdDB <> nil then EdDB.Text               := Linha;
          12: if EdPorta <> nil then EdPorta.Text         := Linha;
          14: if EdLogin <> nil then EdLogin.Text         := Linha;
          16:
          begin
            if Length(Linha) > 0 then
            begin
              for j := 1 to Trunc(Length(Linha) / 3) do
              begin
                try
                  Num := Geral.IMV(Copy(Linha, 1 + ((j-1) * 3), 3));
                  Senha := Senha + (Char(Num));
                except

                end;
              end;
              Linha := dmkPF.Criptografia(Senha, CO_LLM_SecureStr);
              if EdSenha <> nil then EdSenha.Text := Linha;
            end;
          end;
        end;
      end;
      CloseFile(f);
    end else
      Geral.MB_Aviso('Arquivo inv�lido!');
  end;
end;

procedure TUnMyObjects.IncMesTPIniEFim(TPIni, TPFim: TdmkEditDateTimePicker;
  Incremento: Integer);
var
  DataX, DataI, DataF: TDateTime;
  Ano, Mes, Dia: Word;
begin
  if Incremento > 0 then
    DataX := Int(TPFim.Date) + 1
  else
    DataX := Int(TPIni.Date) - 1;
  //
  DecodeDate(DataX, Ano, Mes, Dia);
  DataI := EncodeDate(Ano, Mes, 1);
  DataF := IncMonth(DataI) - 1;
  //
  TPIni.Date := DataI;
  TPFim.Date := DataF;
end;

function TUnMyObjects.IncrementaFolhaLinha(EdFolha, EdLinha: TdmkEdit; MaxLin:
  Integer): Boolean;
begin
  if EdLinha.ValueVariant > 0 then
  begin
    EdLinha.ValueVariant    := EdLinha.ValueVariant + 1;
    if EdLinha.ValueVariant > Maxlin then
    begin
      EdFolha.ValueVariant    := EdFolha.ValueVariant + 1;
      EdLinha.ValueVariant    := 1;
    end;
  end;
end;

procedure TUnMyObjects.IncrementaNumeroEditTxt(Sender: TdmkEdit;
  var Key: Word; ShiftState: TShiftState; OutFormat: TAllFormat);
var
  Increm: Integer;
  Valor: Double;
begin
  if Key in ([VK_DOWN, VK_UP]) then
  begin
    Increm := 0;
    if Key = VK_DOWN then Increm := -1;
    if Key = VK_UP   then Increm := +1;
    //
    if ssShift in ShiftState then Increm := Increm *  10;
    if ssCtrl  in ShiftState then Increm := Increm * 100;
    //
    if Increm <> 0 then
    begin
      case OutFormat of
        dmktfDouble:
        begin
          Valor := Increm / 100;
          if (Sender.ValueVariant <> Null) then
            Valor := Valor + Geral.DMV(Sender.Text);
          Sender.ValueVariant := Valor;
        end;
        dmktfInteger, dmktfLongInt, dmktfInt64:
        begin
          if (Sender.ValueVariant <> Null) then
            Increm := Increm + Geral.IMV(Sender.Text);
          Sender.ValueVariant := Increm;
        end;
      end;
    end;
  end;
end;

procedure TUnMyObjects.Informa(Label1: TLabel; Aguarde: Boolean; Texto: String);
begin
  if Label1 <> nil then
  begin
    if Aguarde then
      Label1.Caption := 'Aguarde... ' + Texto + '...'
    else
      Label1.Caption := Texto;
    Label1.Update;
    Application.ProcessMessages;
  end;
end;

function TUnMyObjects.Informa2(Label1, Label2: TLabel; Aguarde: Boolean; Texto: String;
 IsThread: Boolean): Boolean;
begin
  Result := False;
  try
    if Label1 <> nil then
    begin
      if Aguarde then
        Label1.Caption := 'Aguarde... ' + Texto + '...'
      else
        Label1.Caption := Texto;
      if not IsThread then
        Label1.Update;
    end;
    if Label2 <> nil then
    begin
      if Aguarde then
        Label2.Caption := 'Aguarde... ' + Texto + '...'
      else
        Label2.Caption := Texto;
      if not IsThread then
        Label2.Update;
    end;
    if not IsThread then
      Application.ProcessMessages;
    Result := True;
  except
    // nada
  end;
end;

procedure TUnMyObjects.Informa2EUpdPB(PB: TProgressBar; Label1, Label2: TLabel;
  Aguarde: Boolean; Texto: String);
begin
  if PB <> nil then
  begin
    PB.Position := PB.Position + 1;
    PB.Update;
  end;
  Informa2(Label1, Label2, Aguarde, Texto);
end;

function TUnMyObjects.Informa2VAR_LABELs(Texto: String): Boolean;
begin
(*20200919
  Result := Informa2(VAR_LABEL1, VAR_LABEL2, True, Texto);
*)end;

procedure TUnMyObjects.InformaEUpdPB(PB: TProgressBar; Label1: TLabel;
  Aguarde: Boolean; Texto: String);
begin
  if PB <> nil then
  begin
    PB.Position := PB.Position + 1;
    PB.Update;
  end;
  Informa(Label1, Aguarde, Texto);
end;

procedure TUnMyObjects.InformaN(Labels: array of TLabel; Aguarde: Boolean;
  Texto: String);
var
  I: Integer;
begin
  for I := Low(Labels) to High(Labels) do
  begin
    if Aguarde then
      Labels[I].Caption := 'Aguarde... ' + Texto + '...'
    else
      Labels[I].Caption := Texto;
    Labels[I].Update;
  end;
end;

function TUnMyObjects.InsereLinhaStringGrid(Grade: TStringGrid; Pergunta: Boolean): Boolean;
var
  Continua: Boolean;
  x, i, j, Line: Integer;
begin
  Result   := False;
  Continua := False;
  //
  if Pergunta = True then
  begin
    if Geral.MB_Pergunta('Confirma a inclus�o de linha ?') = ID_YES
    then
      Continua := True;
  end else
    Continua := True;
  //
  if Continua = True then
  begin
    Grade.RowCount := Grade.RowCount + 1;
    x := 1;
    Line := Grade.Row;
    for i := Grade.RowCount downto Line + 1 do
    begin
      for j := 1 to Grade.ColCount - x do
      begin
        Grade.Cells[j, i] := Grade.Cells[j, i-1];
      end;
    end;
    for i := 1 to Grade.ColCount do
      Grade.Cells[i, Line] := '';
(*
    Line := Grade.RowCount - x;
    if Line < 2 then
    begin
      //for i := 1 to Grade.ColCount do Grade.Cells[i, Grade.RowCount-1] := '';
    end else Grade.RowCount := Line;
*)
    Result := True;
  end;
end;

procedure TUnMyObjects.LimitaDatasPeloAnoMesDateTimePicker(
  dmkEditDateTimePicker: TdmkEditDateTimePicker; AnoMes: Integer;
  DiaSelecionado: Integer);
var
  DataMin, DataMax, DataSel: TDateTime;
begin
  dmkEditDateTimePicker.MinDate := 0;
  dmkEditDateTimePicker.MaxDate := 0;
  //
  DataMin := dmkPF.DatadeAnoMes(AnoMes, 1, 0);
  DataSel := dmkPF.DatadeAnoMes(AnoMes, DiaSelecionado, 0);
  DataMax := IncMonth(DataMin, 1) - 1;
  //
  dmkEditDateTimePicker.Date    := DataSel;
  dmkEditDateTimePicker.MinDate := DataMin;
  dmkEditDateTimePicker.MaxDate := DataMax;
end;

procedure TUnMyObjects.LimpaGrade(MinhaGrade: TStringGrid; ColIni,
  RowIni: Integer; ExcluiLinhas: Boolean);
var
  i, j: Integer;
begin
  for i := ColIni to MinhaGrade.ColCount -1 do
    for j := RowIni to MinhaGrade.RowCount -1 do MinhaGrade.Cells[i,j] := '';
  if ExcluiLinhas then
    MinhaGrade.RowCount := 2;
end;

procedure TUnMyObjects.LimpaGrades(Grades: array of TStringGrid; ColIni,
  RowIni: Integer; ExcluiLinhas: Boolean);
var
  i, j, k: Integer;
begin
  j := High(Grades);
  k := Low(Grades);
  //
  for i := k to j do
  begin
    if Grades[i] <> nil then
      LimpaGrade(Grades[i], ColIni, RowIni, ExcluiLinhas);
  end;
end;

procedure TUnMyObjects.ListaItensListBoxDeLista(ListBox: TListBox;
  Lista: array of String);
var
  I: Integer;
begin
  ListBox.Clear;
  for I := Low(Lista) to High(Lista) do
    ListBox.Items.Add(Lista[I]);
end;

function TUnMyObjects.LoadStringGridFromFile(Grade: TStringGrid; Arq: String):
  Boolean;
var
  F: TextFile;
  I, C, R, mR, mC: Integer;
  Dir, Linha: String;
  //Strings: TStrings;
begin
  Result := False;
  //
  AssignFile(F, Arq);
  try
    Reset(F);
    //
    R := 0;
    C := 0;
    I := 0;
    mR := 2;
    mC := 2;
    while not Eof(F) do
    begin
      Readln(F, Linha);
      if i < 2  then
      begin
        if I = 0 then
          mC := Geral.IMV(Linha)
        else
          mR := Geral.IMV(Linha);
        Grade.ColCount := mC;
        Grade.RowCount := mR;
      end else
      begin
        Grade.Cells[C, R] := Linha;
        //
        R := R + 1;
        if R >= mR then
        begin
          C := C + 1;
          R := 0;
        end;
      end;
      I := I + 1;
    end;
    Result := True;
  finally
    CloseFile(f);
  end;
end;

procedure TUnMyObjects.LoadXML(MyMemo: TMemo; MyWebBrowser: TWebBrowser;
  PageControl: TPageControl; ActivePageIndex: Integer);
var
  Arq: String;
begin
  Arq := ExtractFileDir(application.ExeName) + 'temp.xml';
  //
  MyWebBrowser.Visible := True;
  MyMemo.Lines.SaveToFile(Arq);
  MyWebBrowser.Navigate(Arq);
  DeleteFile(Arq);
  if (PageControl <> nil) and (ActivePageIndex > -1) then
    PageControl.ActivePageIndex := ActivePageIndex;
  Application.ProcessMessages;
end;

function TUnMyObjects.LocalizaFormsIguais(
  InstanceClass: TComponentClass): TArrForms;
var
  I, K: Integer;
  Forms: TArrForms;
begin
  K := 0;
  SetLength(Forms, K);
  for i := 0 to Screen.FormCount -1 do
  begin
    if Screen.Forms[i] is InstanceClass then
    begin
      K := K + 1;
      //
      SetLength(Forms, K);
      Forms[K - 1] := TForm(Screen.Forms[i]);
    end;
  end;
  Result := Forms;
end;

procedure TUnMyObjects.LocalizaTextoEmRich(DBRich: TRichEdit; FindDialog: TFindDialog);
var
  FoundAt: LongInt;
  StartPos, ToEnd: Integer;
  mySearchTypes : TSearchTypes;
begin
  if (DBRich <> nil) and (FindDialog <> nil) then
  begin
    mySearchTypes := [];
    //
    with DBRich do
    begin
      if frMatchCase in FindDialog.Options then
         mySearchTypes := mySearchTypes + [stMatchCase];
      if frWholeWord in FindDialog.Options then
         mySearchTypes := mySearchTypes + [stWholeWord];
      if SelLength <> 0 then
        StartPos := SelStart + SelLength
      else
        StartPos := 0;
      //
      ToEnd   := Length(Text) - StartPos;
      FoundAt := FindText(FindDialog.FindText, StartPos, ToEnd, mySearchTypes);
      //
      if FoundAt <> -1 then
      begin
        SelStart  := FoundAt;
        SelLength := Length(FindDialog.FindText);
        SetFocus;
        //
        Perform(EM_SCROLLCARET,SB_LINEDOWN,0);
      end else
        Beep;
    end;
  end;
end;

procedure TUnMyObjects.MaximizaPageMenu(
  Pager: TWinControl; Maximiza: TDefTruOrFalse);
begin
  {$IfDef cAdvToolx} //Berlin
  if TWinControl is TAdvToolBarPager then
  begin
    case Maximiza of
      deftfFalse: TAdvToolBarPager(Pager).Collaps;
      deftfTrue : TAdvToolBarPager(Pager).Expand;
      deftfInverse:
      begin
        if TAdvToolBarPager(Pager).Expanded then
            TAdvToolBarPager(Pager).Collaps
        else
          TAdvToolBarPager(Pager).Expand;
      end;
      else Geral.MB_Aviso('"TDefTruOrFalse" n�o implementado!');
    end;
  end;
  {$Else}
  //
  {$EndIf}
end;

procedure TUnMyObjects.MeDmk_Aviso(Mensagem: String);
begin
  if VAR_MeDmkAviso <> nil then
    VAR_MeDmkAviso.Lines.Add(Mensagem)
  else
    Geral.MB_Aviso(Mensagem);
end;

procedure TUnMyObjects.MenuItemStyleClick(Sender: TObject);
var
  StyleName: String;
begin
  StyleName :=  StringReplace(TMenuItem(Sender).Caption, '&', '',
    [rfReplaceAll, rfIgnoreCase]);
  //if TStyleManager.IsValidStyle(StyleName) then
  try
    TStyleManager.TrySetStyle(StyleName);
    //
    Geral.WriteAppKeyCU('StyleName', Application.Title, StyleName, ktString);
  except
    //
  end;
end;

function TUnMyObjects.MessageDlgCheck(Msg: string; AType: TMsgDlgType;
  AButtons: TMsgDlgButtons; IndiceHelp: LongInt; DefButton: TModalResult;
  Portugues, Checar: Boolean; MsgCheck: string; Funcao: TProcedure): Word;
var
  I: Integer;
  Mensagem: TForm;
  Check: TCheckBox;
begin
  Check := nil;
  Mensagem := CreateMessageDialog(Msg, AType, Abuttons);
  Mensagem.HelpContext := IndiceHelp;
  with Mensagem do
  begin
    for i := 0 to ComponentCount - 1 do
    begin
      if (Components[i] is TButton) then
      begin
        if (TButton(Components[i]).ModalResult = DefButton) then
        begin
          ActiveControl := TWincontrol(Components[i]);
        end;
      end;
    end;
    if Portugues then
    begin
      if Atype = mtConfirmation then
        Caption := 'Confirma��o'
      else if AType = mtWarning then
        Caption := 'Aviso'
      else if AType = mtError then
        Caption := 'Erro'
      else if AType = mtInformation then
        Caption := 'Informa��o';
    end;
  end;
  if Portugues then
  begin
    TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
    TButton(Mensagem.FindComponent('NO')).Caption := '&N�o';
    TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
    TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
    TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
    TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
    TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
    TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
  end;
  if Checar then
  begin
    Mensagem.ClientHeight := Mensagem.ClientHeight + 40;
    Check := TCheckBox.Create(Mensagem);
    Check.Parent := Mensagem;
    Check.Left := 15;
    Check.Top := Mensagem.ClientHeight - 40;
    Check.Visible := True;
    Check.Caption := MsgCheck;
    Check.Width := Mensagem.ClientWidth - 10;
  end;
  Result := Mensagem.ShowModal;
  if Check.Checked then Funcao;
  Mensagem.Free;
end;

function TUnMyObjects.MeuwParamEnter(Controle: TWinControl): Integer;
begin
  {
  begin
    if HiWord(GetKeyState(VK_SHIFT)) <> 0 then
      Screen.ActiveForm.Perform(WM_NEXTDLGCTL, 1, 0)
    else
      Screen.ActiveForm.Perform(WM_NEXTDLGCTL, 0, 0);
    Result := 0;
  end;
  exit;
  }
  Result := IC2_LOpcoesTeclaEnter;
  //Exit;
  if VAR_CANCEL_ENTER_TAB then Exit; // Fm???_MLA
  if Controle is TDBCombobox then
  begin
    if SendMessage(TDBComboBox(Controle).Handle, CB_GETDROPPEDSTATE, 0, 0) <> 1 then
     Result := IC2_LOpcoesTeclaTab
  end;

  if Controle is TdmkDBLookupCombobox then
  begin
    if not TdmkDBLookupCombobox(Controle).IsDropped then


       Result := IC2_LOpcoesTeclaTab;
    {
    Status := SendMessage(TCustomCombobox(Controle).Handle, CB_GETDROPPEDSTATE, 0, 0);
    if Status <> 1 then
       Result := IC2_LOpcoesTeclaTab;
    }
  end else
  if Controle is TDBLookupCombobox then
  begin
    if SendMessage(TDBLookupCombobox(Controle).Handle, CB_GETDROPPEDSTATE, 0, 0) <> 1 then
     Result := IC2_LOpcoesTeclaTab
  end;


  {if ((Controle is TDBCombobox) and
     (TDBCombobox(Controle).ReadOnly=True)) then Result := IC2_LOpcoesTeclaTab;
  if ((Controle is TDBLookupCombobox) and
     (TDBLookupCombobox(Controle).ReadOnly=True)) then Result := IC2_LOpcoesTeclaTab;}
  if Controle is TEdit then Result := IC2_LOpcoesTeclaTab else
  if Controle is TDBEdit then Result := IC2_LOpcoesTeclaTab else
  if Controle is TdmkEditDateTimePicker then
    Result := IC2_LOpcoesTeclaTab else
  if Controle is TDateTimePickerdmkEdit then
    Result := IC2_LOpcoesTeclaTab else
  if Controle is TdmkEdit then
  begin
    // dmkEdit do dmkDBGridDAC
    if (TdmkEdit(Controle).Name <> '') or
    (TdmkEdit(Controle).FComponentePai <> '') then
    Result := IC2_LOpcoesTeclaTab;
    {begin
      if HiWord(GetKeyState(VK_SHIFT)) <> 0 then
        Screen.ActiveForm.Perform(WM_NEXTDLGCTL, 1, 0)
      else
        Screen.ActiveForm.Perform(WM_NEXTDLGCTL, 0, 0);
      Result := 0;
    end;}
  end else
{  if Controle is T L M D Edit then
  begin
    if Copy(T L M D Edit(Controle).Name, 1, 8) <> 'EdDBGrid' then
    if T L M D Edit(Controle).Name <> 'EdLocate' then
      Result := IC2_LOpcoesTeclaTab

  end else
  if Controle is T L M D CheckBox then Result := IC2_LOpcoesTeclaTab else
  if Controle is T L M D DBEdit then Result := IC2_LOpcoesTeclaTab else
}
  if Controle is TMaskEdit then Result := IC2_LOpcoesTeclaTab else
  if Controle is TDateTimePicker then Result := IC2_LOpcoesTeclaTab else
  if Controle is TCheckBox then Result := IC2_LOpcoesTeclaTab else
  if Controle is TRadioButton then Result := IC2_LOpcoesTeclaTab else
  if Controle is TComboBox then
  begin
    if not TComboBox(Controle).DroppedDown then Result := IC2_LOpcoesTeclaTab;
  end;
end;

function TUnMyObjects.MeuwParamPonto(Controle: TWinControl): Integer;
begin
  Result := IC2_LOpcoesTeclaPonto;
{
  if Controle is T L M D Edit then Result := IC2_LOpcoesTeclaVirgula;
}
end;

procedure TUnMyObjects.MostraErro(Sender: TObject; E: Exception);

  function RegistraDLL(DLL: String): Boolean;
  var
    CapHand: HMODULE;
    ProcDeRegistro: procedure;
    //CertStore: IStore3;
  begin
    try
      CapHand := SafeLoadLibrary(DLL);
      try
        ProcDeRegistro := GetProcAddress(CapHand, 'DllRegisterServer');
        if (@ProcDeRegistro <> nil) then
          ProcDeRegistro;
      finally
        FreeLibrary(CapHand);
        //
        Result := True;
      end;
    except
      Result := False;
    end;
  end;

const
  MsgErroDll = 'Verifique se todas DLLs necess�rias est�o devidamente registradas no sistema operacional!';
var
  i, a, IniNum, Acao, n, p1, p2: Integer;
  Resultado, Texto, Erro, NumTxt, MensExtra, BancoDados, Tabela: String;
  Mensagem: WideString;
begin
{
  try
  Result := CreateComObject(CLASS_Store) as IStore3;
  except
    Geral.MB_Aviso('Classe n�o registrada! Verifique se a CAPICOM est� devidamente registrada no sistema operacional!');
  end;
}

  Texto := '';
  //Conta := 0;
  Acao  := 0;
  Erro := E.Message;
  // ini 2021-03-04
  if (E is EExternalException)
  or (E is EAccessViolation) then
  begin
    // N�o faz nada! � um erro externo!!!???
    VAR_HIDEN_ERR_MSGS_BY_DMK := Geral.FDT(Now(), 109) + sLineBreak +
      E.Message + sLineBreak +
      '=============================================================' +
      sLineBreak + VAR_HIDEN_ERR_MSGS_BY_DMK;
    //
    VAR_EvitaErroExterno := False;
    Exit;
  end;
  // fim 2021-03-04
  if Erro = 'Null Remote Address' then
  begin
    Resultado := 'Erro. O endere�o desejado est� indispon�vel.';
  end else if (E is EOleSysError) (*and (pos('Classe n�o registrada', Erro) = 1)*) then
  begin
    if Pos('Classe n�o registrada', Erro) > 0 then
    begin
      //Registra Capicom se n�o tiver registrada => IN�CIO
      if RegistraDLL('CAPICOM.DLL') then
      begin
        if RegistraDLL('MSXML5.DLL') then
        begin
          if not RegistraDLL('MSWINSCK.OCX') then
          begin
            Geral.MB_ERRO(Erro + sLineBreak + MsgErroDll);
            Exit;
          end;
        end else
        begin
          Geral.MB_ERRO(Erro + sLineBreak + MsgErroDll);
          Exit;
        end;
        Geral.MB_Aviso('DLLs registradas no sistema operacional!' + sLineBreak + 'Repita o procedimento!');
      end else
      begin
        Geral.MB_ERRO(Erro + sLineBreak + MsgErroDll);
        Exit;
      end;
    end else
    begin
      Geral.MB_ERRO(Erro);
      Exit;
    end;
  end else if (E is ESocketError) or (E is EIdSocketError) then
  begin
    //Socket Error # 10061 - Servidor inativo
    NumTxt := '';
    IniNum := 0;
    for i := 1 to Length(Erro) do
    begin
      if Erro[i] = '#' then IniNum := IniNum + 1
      else if (IniNum = 1) then
      begin
        //if Erro[i] in ([' ','0'..'9']) then
{$IFDEF DELPHI12_UP}
        if CharInSet(Erro[i], ([' ','0'..'9'])) then
{$ELSE}
        if Erro[i] in ([' ','0'..'9']) then
{$ENDIF}
          NumTxt := NumTxt + Erro[i]
        else
          IniNum := IniNum + 1;
      end;
    end;
    NumTxt := Trim(NumTxt);
    if NumTxt = '' then
      Resultado := Erro
    else
      Resultado := ErroDeSocket(StrToInt(NumTxt), Erro);
    Resultado := Resultado + ' (' + E.ClassName;
    if NumTxt <> '' then Resultado := Resultado + ' n� ' + NumTxt;
    Resultado := Resultado + ').';
  end else if E is EOleException then
  begin
    {
    for i := 1 to Length(Erro) do
    begin
      if Erro[i] <> #$A then
      begin
        Texto := Texto + Erro[i];
        Conta := Conta + 1;
        if (Conta > 35) and(Erro[i] = ' ') then
        begin
          Resultado := Resultado + Texto +sLineBreak;
          Texto := '';
          Conta := 0;
        end;
      end else begin
        Resultado := Resultado + Texto +sLineBreak;
        Texto := '';
        Conta := 0;
      end;
    end;
    if Trim(Resultado) = '' then
      Resultado := Erro;
    Resultado := E.ClassName +sLineBreak + Resultado;
    }
    Resultado := E.ClassName +sLineBreak + Erro;
  end else if E is EConvertError then
  begin
    IniNum := 0;
    a := 0;
    for i := 1 to Length(Erro) do if Erro[i] = '''' then a := i;
    for i := 1 to Length(Erro) do
    begin
      if Erro[i] = '''' then IniNum := IniNum + 1;
      if (IniNum>0) and (i < a) then NumTxt := NumTxt + Erro[i];
    end;
    Mensagem := Uppercase(Copy(Erro, a+1, Length(Erro)-a));
    if Mensagem = Uppercase(' is not a valid floating point value') then
    Resultado := 'Erro. Valor flutuante inv�lido: "'+NumTxt+'"'
    else  Resultado := Erro;
    //Resultado := E.ClassName +sLineBreak + Resultado;
  end else if (E is EDatabaseError) or (E is EmySQLDataBaseError) then
  //EMySQLDACException
  begin
    IniNum := 0;
    for i := 1 to Length(Erro) do
    begin
      if Erro[i] = '(' then IniNum := IniNum + 1
      else if Erro[i] = ')' then IniNum := IniNum + 1
      else if (IniNum = 1) then NumTxt := NumTxt + Erro[i];
    end;
    //VOSTRO_1320
    if NumTxt = '' then
      Resultado := Erro
    else begin
      case StrToInt(NumTxt) of
        1146:
        begin
          Mensextra := Copy(Erro, 34, Length(erro)-48);
          Resultado := 'Erro. Tabela n�o existe: '+ MensExtra;
          Acao := 1146;
        end;
        1062: Resultado := 'Erro. O registro j� existe, ou tem chave que n�o pode ser duplicada!';
        //Unknown column 'Numero' in 'field list'
        1054:
        begin
          Resultado := 'Erro. Coluna desconhecida na lista de campos: "'+
          Copy(Erro, 43, Length(erro)-58)+'"'+sLineBreak+'Fa�a uma verifica��o'+
          ' completa das tabelas dos bancos de dados de rede e local acessando no '+
          'menu principal -> Ferramentas -> BD -> verifica...' + slineBreak +
          'Caso o problema continue, contate a Dermatek!';
          //Acao := 1054;
        end
        else  Resultado := Erro;
      end;
    end;
    ////////////////////////// fim c�digos /////////////////////////////////////
    if Resultado = Erro then
    begin
      a := 0;
      for i := 1 to Length(Erro) do if Erro[i] = '''' then a := i;
      for i := 1 to Length(Erro) do
      begin
        if Erro[i] = '''' then IniNum := IniNum + 1;
        if (IniNum>0) and (i < a) then NumTxt := NumTxt + Erro[i];
      end;
      Mensagem := Uppercase(Copy(Erro, a+1, Length(Erro)-a));
      if Mensagem = Uppercase(' is not a valid floating point value') then
                    Resultado := 'Erro. Valor flutuante inv�lido: "'+NumTxt+'"'
      else if Mensagem = Uppercase(' in where clause is ambiguous') then
                    Resultado := 'Erro. Campo amb�guo na SQL.'(*+NumTxt+'"'*)
      else if Mensagem = Uppercase(' must have a value') then
                    Resultado := 'Erro. O campo "'+NumTxt+'" � obrigat�rio!'
      else if Copy(Mensagem, 1, 20) = Uppercase('Field value required') then
      begin
                    NumTxt := Copy(Mensagem, 31, Length(Mensagem)-30);
                    Resultado := 'Erro. O campo "'+NumTxt+'" � obrigat�rio!'
      end
      else  Resultado := Erro;
    end;
  end else if E is ECommonCalendarError then
  begin
    if LowerCase(e.Message) = 'failed to set calendar date or time' then
    Resultado := 'Erro ao definir data ou hora para o calend�rio!' else
    if Copy(LowerCase(e.Message), 1, 28) = 'date is less than minimum of' then
    Resultado := 'Data abaixo do m�nimo: ' + Copy(e.Message, 29) else
    Resultado := e.Message;
  end //else if Uppercase(Erro) = 'PROPERTY DATABASE IS NOT SET!' then Resultado := 'A "Query" ou "Table" n�o teve sua propriedade "Database" configurada.'
  else if E is ERemotableException then
  begin
    p1 := pos('Server did not recognize the value of HTTP Header SOAPAction:', Erro);
    p2 := pos(':', Erro);
    if p1 > 0 then
      Resultado :=
      'O servidor n�o reconheceu o valor definido para o cabe�alho HTTP SOAPAction'
      + Copy(Erro, p2) + sLineBreak +
      'Verifique se a vari�vel "InvRegistry.RegisterDefaultSOAPAction" est� correta!'
    else
      Resultado := Erro;
  end
  else if E is Exception then
  begin
    // erro se coloca antes do 1146, pois da� n�o passa por ele.
    p1 := pos('Field', Erro);
    p2 := pos('is not found in current dataset', Erro);
    if (p1 > 0) and (p2 > 0) then
    begin
      p1 := pos('"', Erro);
      Resultado := 'O campo ' + Copy(Erro, p1, p2-p1) + 'n�o existe ' +
      'na base de dados. (� possivel que seja um campo calculado.)';
    end else
      Resultado := E.ClassName + ' # ' + Erro;
  end
  else Application.ShowException(e);
  if (Acao = 0) and (Length(Resultado) > 0) then
  begin
    if Sender <> nil then
    begin
      if Sender is TControl then
        Resultado := Resultado + sLineBreak + 'Nome do controle: ' + TControl(Sender).Name;
    end;
    Geral.MB_Erro(Resultado);
    Screen.Cursor := crDefault;
  end;
  case Acao of
    1146:
    begin
      n := pos('.', MensExtra);
      if n > 0 then
      begin
        BancoDados := Copy(MensExtra, 1, n-1);
        Tabela     := Copy(MensExtra, n+1, Length(MensExtra)-n);
        {$IFNDEf SemDBLocal}
        if Uppercase(Bancodados) = Uppercase(TLocDB) then
        begin
          //if
          Geral.MB_Aviso('A tabela ' + Tabela +
          ' pertencente ao banco de dados local ' + BancoDados+' e n�o existe. '+
          'Verifique se ela pode ser criada acessando o menu principal ' +
          '> Ferramentas > BD > Verifica');
          (* = ID_YES
          then begin
            if FmPrincipal.Recria Tabela Local(Tabela, 1) then
            Geral.MB_Info('Tabela criada com sucesso!');
          end;
          *)
        end else
        {$EndIf}
          Application.ShowException(e);
      end;
    end;
  end;
end;

function TUnMyObjects.MostraPMCEP(PMCEP: TPopupMenu; Botao: TObject): Boolean;
var
  Lista: TStringList;
  i: Integer;
  Achou: Boolean;
begin
{$IfDef cDbTable}
  Result := False;
  if VAR_REGEDIT_MyCEP then
  begin
    Geral.MB_Aviso(('Foi configurada a base de dados de pesquisa'+
    ' de CEP. � necess�rio reiniciar o aplicativo!' + sLineBreak +
    'Base de dados: [' + CO_DIR_RAIZ_DMK + '\Correios\Access]'));
    Exit;
  end;
  Achou := False;
  Lista := TStringList.Create;
  Session.GetDatabaseNames(Lista);
  if Lista.Count > 0 then
  begin
    for i := 0 to Lista.Count-1 do
      if Lista[i] = 'MyCEP' then
      begin
        Achou := True;
        Break;
      end;
  end;
  if not Achou then
  begin
    (*
    Geral.WriteAppKey_Total('Driver', '\Software\ODBC\ODBC.INI\MyCEP',
    'C:\WINDOWS\system32\odbcjt32.dll', ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey_Total('DBQ', '\Software\ODBC\ODBC.INI\MyCEP',
    CO_DIR_RAIZ_DMK + '\Correios\Access\dbgpbe.mdb', ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey_Total('Description', '\Software\ODBC\ODBC.INI\MyCEP',
    'Consulta de CEP', ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey_Total('DriverId', '\Software\ODBC\ODBC.INI\MyCEP',
    25, ktInteger, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey_Total('FIL', '\Software\ODBC\ODBC.INI\MyCEP',
    'MS Access;', ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey_Total('PWD', '\Software\ODBC\ODBC.INI\MyCEP',
    '�����в�ޢ����', ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey_Total('SafeTransactions', '\Software\ODBC\ODBC.INI\MyCEP',
    0, ktInteger, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey_Total('UID', '\Software\ODBC\ODBC.INI\MyCEP',
    '', ktString, HKEY_LOCAL_MACHINE);
    //
    Geral.WriteAppKey_Total('ImplicitCommitSync', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    '', ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey_Total('MaxBufferSize', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    2048, ktInteger, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey_Total('PageTimeout', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    5, ktInteger, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey_Total('Threads', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    3, ktInteger, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey_Total('UserCommitSync', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    'Yes', ktString, HKEY_LOCAL_MACHINE);
    //
    Geral.WriteAppKey_Total('MyCEP', '\Software\ODBC\ODBC.INI\ODBC Data Sources',
    'Driver do Microsoft Access (*.mdb)', ktString, HKEY_LOCAL_MACHINE);
    *)
    //Mudado em: 2017-12-21 => Motivo: Compatibilizar com Windows 64 bits
    Geral.WriteAppKeyLM('Driver', 'ODBC\ODBC.INI\MyCEP', 'C:\WINDOWS\system32\odbcjt32.dll', ktString);
    Geral.WriteAppKeyLM('DBQ', 'ODBC\ODBC.INI\MyCEP', CO_DIR_RAIZ_DMK + '\Correios\Access\dbgpbe.mdb', ktString);
    Geral.WriteAppKeyLM('Description', 'ODBC\ODBC.INI\MyCEP', 'Consulta de CEP', ktString);
    Geral.WriteAppKeyLM('DriverId', 'ODBC\ODBC.INI\MyCEP', 25, ktInteger);
    Geral.WriteAppKeyLM('FIL', 'ODBC\ODBC.INI\MyCEP', 'MS Access;', ktString);
    Geral.WriteAppKeyLM('PWD', 'ODBC\ODBC.INI\MyCEP', '�����в�ޢ����', ktString);
    Geral.WriteAppKeyLM('SafeTransactions', 'ODBC\ODBC.INI\MyCEP', 0, ktInteger);
    Geral.WriteAppKeyLM('UID', 'ODBC\ODBC.INI\MyCEP', '', ktString);
    //
    Geral.WriteAppKeyLM('ImplicitCommitSync', 'ODBC\ODBC.INI\MyCEP\Engines\JET', '', ktString);
    Geral.WriteAppKeyLM('MaxBufferSize', 'ODBC\ODBC.INI\MyCEP\Engines\JET', 2048, ktInteger);
    Geral.WriteAppKeyLM('PageTimeout', 'ODBC\ODBC.INI\MyCEP\Engines\JET', 5, ktInteger);
    Geral.WriteAppKeyLM('Threads', 'ODBC\ODBC.INI\MyCEP\Engines\JET', 3, ktInteger);
    Geral.WriteAppKeyLM('UserCommitSync', 'ODBC\ODBC.INI\MyCEP\Engines\JET', 'Yes', ktString);
    //
    Geral.WriteAppKeyLM('MyCEP', 'ODBC\ODBC.INI\ODBC Data Sources', 'Driver do Microsoft Access (*.mdb)', ktString);
    //
    VAR_REGEDIT_MyCEP := True;
  end;
  if VAR_REGEDIT_MyCEP then
  begin
    Geral.MB_Aviso(('Foi configurada a base de dados de pesquisa'+
    ' de CEP. � necess�rio reiniciar o aplicativo!'));
    Exit;
  end;
  //
  MostraPopUpDeBotaoObject(PMCEP, Botao, 0, 0);
{$Else}
  //    TODO Berlin
  //Geral.MB_Info('Solicite implementa��o � Dermatek! (DBE Table)');
  MostraPopUpDeBotaoObject(PMCEP, Botao, 0, 0);
{$EndIf}
end;

procedure TUnMyObjects.MostraPopOnControlXY(PM: TPopUpMenu;
  ComponentePai: TControl; PosX, PosY: Integer);
var
  pt: TPoint;
  rt: TRect;
  i, X,Y, lin, col, Hei, Wid: Integer;
begin
  Hei := 0;
  Wid := 0;
  GetCursorPos(pt);
  X  := pt.Y;
  Y  := pt.X;
  if ComponentePai = nil then
  begin
    ;// N�o faz nada!
  end else
  if ComponentePai is TSpeedButton then
  begin
    X  := X + 11;
    Y  := Y + 11;
  end else begin
    //Y   := 0;
    //X   := 0;
    if ComponentePai is TBitBtn then GetWindowRect(TBitBtn(ComponentePai).Handle, rt)
    else if ComponentePai is TButton then GetWindowRect(TButton(ComponentePai).Handle, rt)
    else if ComponentePai is TSpeedButton then GetWindowRect(TButton(ComponentePai).Handle, rt)
    else if ComponentePai is TPanel then GetWindowRect(TBitBtn(ComponentePai).Handle, rt)
    //else if ComponentePai is T L M D Edit then GetWindowRect(T L M D Edit(ComponentePai).Handle, rt)
    else if ComponentePai is TListView then GetWindowRect(TListView(ComponentePai).Handle, rt)
    else if ComponentePai is TStringGrid then
    begin
      GetWindowRect(TStringGrid(ComponentePai).Handle, rt);
      lin := TStringGrid(ComponentePai).Row;
      for i := 0 to lin do Hei := Hei + TStringGrid(ComponentePai).RowHeights[i];
      col := TStringGrid(ComponentePai).Col;
      for i := 0 to Col-1 do Wid := Wid + TStringGrid(ComponentePai).ColWidths[i];
      Wid := Wid + 6;
      Hei := Hei + 12;
    end
    else if ComponentePai is TDBGrid then
    begin
      GetWindowRect(TDBGrid(ComponentePai).Handle, rt);
      lin := THackDBGrid(ComponentePai).Row;
      for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
      col := THackDBGrid(ComponentePai).Col;
      for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
      Wid := Wid + 6;
      Hei := Hei + 12;
    end
    else if ComponentePai is TdmkDBGrid then
    begin
      GetWindowRect(TdmkDBGrid(ComponentePai).Handle, rt);
      lin := THackDBGrid(ComponentePai).Row;
      for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
      col := THackDBGrid(ComponentePai).Col;
      for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
      Wid := Wid + 6;
      Hei := Hei + 12;
    end
    else if ComponentePai is TdmkDBGridDAC then
    begin
      GetWindowRect(TdmkDBGridDAC(ComponentePai).Handle, rt);
      lin := THackDBGrid(ComponentePai).Row;
      for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
      col := THackDBGrid(ComponentePai).Col;
      for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
      Wid := Wid + 6;
      Hei := Hei + 12;
    end else if ComponentePai is TTabSheet then
    begin
      GetWindowRect(TTabSheet(ComponentePai).Handle, rt);
      //Wid := Wid + pt.x;
      //Hei := Hei + pt.y;
    (*end else if ComponentePai is TImage then
    begin
      N�o funciona  usar componente pai do Image
      GetWindowRect(TImage(ComponentePai).Picture.Bitmap.Handle, rt);
      //Wid := Wid + pt.x;
      //Hei := Hei + pt.y;*)
    end else if ComponentePai is TdmkImage then
    begin
      //GetWindowRect(TPanel(ComponentePai).Handle, rt);
      rt.Left := 4;
      rt.Top  := 4;
    end else begin
      Geral.MB_Erro('Componente pai n�o definido para mostrar popup!' +
      sLineBreak + 'Classe: ' + TObject(ComponentePai).ClassName + sLineBreak + 'Nome: ' +
      TControl(ComponentePai).Name);
      Exit;
    end;
    if (PosX <> 0) or (PosY <> 0) then
    begin
      Y := rt.Left + PosX;
      X := rt.Top + PosY;
    end else begin
      Y := rt.Left + Wid;
      X := (PM.Items.Count * 19) + 6;
      if ComponentePai is TDBGrid then X := rt.Top + Hei else
      begin
        if (X + rt.Bottom) > (Screen.Height - 50)
          then X := rt.Top - X else X := rt.Bottom;
      end;
    end;
  end;  
  PM.Popup(y, x);
end;

procedure TUnMyObjects.MostraPopUpDeBotao(PM: TPopUpMenu;
  ComponentePai: TControl; ComponenteFilho: TControl);
var
  pt: TPoint;
  rt: TRect;
  i, X, Y, Z, lin, col, Hei, Wid, Itens: Integer;
begin
  Hei := 0;
  Wid := 0;
  GetCursorPos(pt);
  if ComponentePai is TSpeedButton then
  begin
{
    rt.Left,
    rt.Top,
    rt.Right,
    tr.Bottom: Longint
    GetWindowRect(TSpeedButton(ComponentePai).Handle, rt);
}
    GetCursorPos(pt);
    PM.Popup(pt.X, pt.Y);
    Exit;
  end
  else if ComponentePai is TBitBtn then
    GetWindowRect(TBitBtn(ComponentePai).Handle, rt)
  else if ComponentePai is TButton then GetWindowRect(TButton(ComponentePai).Handle, rt)
  {$IfDef cAdvToolx} //Berlin
  else if ComponentePai is TAdvGlowButton then GetWindowRect(TAdvGlowButton(ComponentePai).Handle, rt)
  else if ComponentePai is TAdvGlowMenuButton then GetWindowRect(TAdvGlowMenuButton(ComponentePai).Handle, rt)
  {$EndIf}
  //else if ComponentePai is T L M D Edit then GetWindowRect(T L M D Edit(ComponentePai).Handle, rt)
  else if ComponentePai is TListView then GetWindowRect(TListView(ComponentePai).Handle, rt)
  //else if ComponentePai is TSpeedButton then GetWindowRect(TSpeedButton(ComponentePai).Handle, rt)
  else if ComponentePai is TDBGrid then
  begin
    GetWindowRect(TDBGrid(ComponentePai).Handle, rt);
    lin := THackDBGrid(ComponentePai).Row;
    for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
    col := THackDBGrid(ComponentePai).Col;
    for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
    Wid := Wid + 6;
    Hei := Hei + 12;
  end
  else if ComponentePai is TdmkDBGrid then
  begin
    GetWindowRect(TdmkDBGrid(ComponentePai).Handle, rt);
    lin := THackDBGrid(ComponentePai).Row;
    for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
    col := THackDBGrid(ComponentePai).Col;
    for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
    Wid := Wid + 6;
    Hei := Hei + 12;
  end
  else if ComponentePai is TdmkDBGridDAC then
  begin
    GetWindowRect(TdmkDBGridDAC(ComponentePai).Handle, rt);
    lin := THackDBGrid(ComponentePai).Row;
    for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
    col := THackDBGrid(ComponentePai).Col;
    for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
    Wid := Wid + 6;
    Hei := Hei + 12;
  end else if ComponentePai is TTabSheet then
  begin
    GetWindowRect(TTabSheet(ComponentePai).Handle, rt);
    //Wid := Wid + pt.x;
    //Hei := Hei + pt.y;
  (*end else if ComponentePai is TImage then
  begin
    N�o funciona  usar componente pai do Image
    GetWindowRect(TImage(ComponentePai).Picture.Bitmap.Handle, rt);
    //Wid := Wid + pt.x;
    //Hei := Hei + pt.y;*)
  end else if ComponentePai is TScrollBox then
    GetWindowRect(TScrollBox(ComponentePai).Handle, rt)
  //end else if ComponentePai is TRSSVGImage then
    //GetWindowRect(TRSSVGImage(ComponentePai).Handle, rt)
  else begin
    Geral.MB_Erro('Componente pai n�o definido para mostrar popup!' +
    sLineBreak + 'Classe: ' + TObject(ComponentePai).ClassName +
    sLineBreak + 'Nome: ' + TControl(ComponentePai).Name);
    Exit;
  end;
  Y := rt.Left + Wid;
  Itens := 0;
  for i := 0 to PM.Items.Count -1 do
    if PM.Items[i].Visible then Itens := Itens + 1;
  X := (Itens * 19) + 6;
  //X := (PM.Items.Count * 19) + 6;
  if ComponenteFilho <> nil then
  begin
    Y := Y + TControl(ComponenteFilho).Left;
    Z := TControl(ComponenteFilho).Top + TControl(ComponenteFilho).Height;
    if Z + X  > (Screen.Height - 50)
      then X := Z - X - TControl(ComponenteFilho).Height
    else
      X := Z;
  end else
  if ComponentePai is TDBGrid then
    X := rt.Top + Hei
  else
  begin
    if (X + rt.Bottom) > (Screen.Height - 50)
      then X := rt.Top - X else X := rt.Bottom;
  end;
  PM.Popup(y, x);
end;

procedure TUnMyObjects.MostraPopUpDeBotaoObject(PM: TPopUpMenu;
  ComponentePai: TObject; MoreWid, MoreTop: Integer);
var
  pt: TPoint;
  rt: TRect;
  i, X,Y, lin, col, Hei, Wid: Integer;
begin
  Hei := 0;
  Wid := 0;
  GetCursorPos(pt);
  if ComponentePai is TBitBtn then GetWindowRect(TBitBtn(ComponentePai).Handle, rt)
  else if ComponentePai is TButton then GetWindowRect(TButton(ComponentePai).Handle, rt)
  //else if ComponentePai is T L M D Edit then GetWindowRect(T L M D Edit(ComponentePai).Handle, rt)
  else if ComponentePai is TListView then GetWindowRect(TListView(ComponentePai).Handle, rt)
  else if ComponentePai is TDBGrid then
  begin
    GetWindowRect(TDBGrid(ComponentePai).Handle, rt);
    lin := THackDBGrid(ComponentePai).Row;
    for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
    col := THackDBGrid(ComponentePai).Col;
    for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
    Wid := Wid + 6;
    Hei := Hei + 12;
  end else if ComponentePai is TDrawGrid then // TStringGrid
  begin
    GetWindowRect(TDrawGrid(ComponentePai).Handle, rt);
    lin := TDrawGrid(ComponentePai).Row;
    for i := 0 to lin do Hei := Hei + TDrawGrid(ComponentePai).RowHeights[i];
    col := TDrawGrid(ComponentePai).Col;
    for i := 0 to Col-1 do Wid := Wid + TDrawGrid(ComponentePai).ColWidths[i];
    Wid := Wid + 6;
    Hei := Hei + 12;
  end else begin
    Geral.MB_Erro('Componente pai n�o definido para mostrar popup!' +
    sLineBreak + 'Classe do objeto: ' + TObject(ComponentePai).ClassName);
    Exit;
  end;
  Y := rt.Left + Wid + MoreWid;
  X := (PM.Items.Count * 19) + 6;
  if ComponentePai is TDBGrid then X := rt.Top + Hei else
  begin
    if (X + rt.Bottom) > (Screen.Height - 50)
      then X := rt.Top - X else X := rt.Bottom;
  end;
  X := X + MoreTop;
  PM.Popup(y, x);
end;

procedure TUnMyObjects.MostraPopupGeral();
begin
  if VAR_CAD_POPUP <> nil then
  begin
    if VAR_SHOW_MASTER_POPUP then
    begin
(*20200919
      VAR_POPUP_ACTIVE_CONTROL := Screen.ActiveForm.ActiveControl;
      VAR_POPUP_ACTIVE_FORM    := Screen.ActiveForm;
*)      MostraPopUpNoCentro(VAR_CAD_POPUP);
    end else
      Geral.MB_Aviso('Master popup desabilitado.');
  end else
    Geral.MB_Aviso('"VAR_CAD_POPUP" n�o definido.');
end;

procedure TUnMyObjects.MostraPopUpNoCentro(PM: TPopUpMenu);
begin
  PM.Popup(Screen.Width div 2, Screen.Height div 2);
end;

function TUnMyObjects.NaoPermiteItemRadioGroup(RG: TRadioGroup;
  Permitidos: array of Integer; AvisoNaoPermitido: String): Boolean;
var
  Achow, NaoPermite: Boolean;
  I: Integer;
begin
  NaoPermite := False;
  Achow      := False;
  for I := Low(Permitidos) to High(Permitidos) do
    if Permitidos[I] = RG.ItemIndex then
    begin
      Achow := True;
      Break;
    end;
  NaoPermite := Achow = False;
  //if
  MyObjects.FIC(NaoPermite, RG, 'Valor selecionado n�o permitido no momento.'
  + sLineBreak + 'O �ndice ' + Geral.FF0(RG.ItemIndex) +
  ' pode ser obsoleto ou in�cuo ou ainda n�o foi implementado!' + sLineBreak +
  AvisoNaoPermitido + sLineBreak +
  'Selecione outro valor para "' + RG.Caption + '"');
  //
  Result := NaoPermite;
end;

function TUnMyObjects.NovaAbaEmTabSheet(Owner: TForm; PageControl: TPageControl;
  ClasseForm: TFormClass; IndiceImagem: Integer): TForm;
var
  TabSheet: TTabSheet;
  //Form: TForm;
begin
  {
  if not PodeAbrirFormulario(ClasseForm, TabSheet) then
  begin
    PageControl.ActivePage := TabSheet;
    Exit;
  end;
  }
  TabSheet := TTabSheet.Create(Owner);
  TabSheet.PageControl := PageControl;

  Result := ClasseForm.Create(TabSheet);
  with Result do
  begin
    Align       := alClient;
    BorderStyle := bsNone;
    Parent      := TabSheet;
  end;

  with TabSheet do
  begin
    Caption     := Copy(Result.Caption, 17);
    ImageIndex  := IndiceImagem;
  end;

  AjustarCaptionAbas(ClasseForm, PageControl);

  TForm(Result).Show;
  PageControl.ActivePage := TabSheet;

  PageControlChange(PageControl);
end;

{$IfNDef sGetData}
function TUnMyObjects.ObtemData(const DtDefault: TDateTime; var DtSelect:
              TDateTime; const MinData: TDateTime; const HrDefault: TTime = 0;
              const HabilitaHora: Boolean = False; Titulo: String = '';
              const InfoDetalhado: String = ''): Boolean;
begin
  Result := False;
  if MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData) then
  begin
    with FmGetData do
    begin
      FMinData        := MinData;
      TPData.Date     := DtDefault;
      LaMulta.Visible := False;
      EdMulta.Visible := False;
      LaJuros.Visible := False;
      EdJuros.Visible := False;
      EdHora.ValueVariant := HrDefault;
      EdHora.Enabled := HabilitaHora;
      LaHora.Enabled := HabilitaHora;
      FTitulo := Titulo;
      if InfoDetalhado <> '' then
      begin
        MeInfoDetalhado.Text    := InfoDetalhado;
        MeInfoDetalhado.Visible := True;
      end;
      ShowModal;
      if FSelecionou then
      begin
        DtSelect := Int(TPData.Date) + EdHora.ValueVariant;
        Result := True;
      end;
      Destroy;
    end;
  end;
end;

function TUnMyObjects.ObtemDatasPeriodo(const DtDefaultIni, DtDefaultFim:
TDateTime; var SelectIni: TDateTime; var SelectFim: TDateTime;
const HabilitaHora: Boolean = False; const HrDefaultIni: TTime = 0;
const HrDefaultFim: TTime = 1 - (1/24/60/60); Titulo: String = '';
const InfoDetalhado: String = ''): Boolean;
begin
  Result := False;
  if MyObjects.CriaForm_AcessoTotal(TFmGetDatasPeriodo, FmGetDatasPeriodo) then
  begin
    with FmGetDatasPeriodo do
    begin
      //FMinData        := MinData;
      TPDataIni.Date     := DtDefaultIni;
      EdHoraIni.ValueVariant := HrDefaultIni;
      EdHoraIni.Enabled := HabilitaHora;
      LaHoraIni.Enabled := HabilitaHora;
      //
      TPDataFim.Date     := DtDefaultFim;
      EdHoraFim.ValueVariant := HrDefaultFim;
      EdHoraFim.Enabled := HabilitaHora;
      LaHoraFim.Enabled := HabilitaHora;
      //
      ShowModal;
      if FSelecionou then
      begin
        SelectIni := Int(TPDataIni.Date) + EdHoraIni.ValueVariant;
        SelectFim := Int(TPDataFim.Date) + EdHoraFim.ValueVariant;
        //
        Result := True;
      end;
      Destroy;
    end;
  end;
end;

{$EndIf}

function TUnMyObjects.ObtemObjeto(const AObject: TObject; const Nome: String;
  const Classe: TClass; var Objeto: TObject): Boolean;
var
  I, Count: Integer;
  PropInfo: PPropInfo;
  TempList: PPropList;
  LObject: TObject;
begin
  Result := False;
  Objeto := nil;
  Count := GetPropList(AObject, TempList);
  if Count > 0 then
  try
    for I := 0 to Count - 1 do
    begin
      PropInfo := TempList^[I];
      if GetPropName(PropInfo) = Nome then
      begin
        //Geral.MB_Info('Nome PropInfo: ' + GetPropName(PropInfo));
        if (PropInfo^.PropType^.Kind = tkClass) and
        Assigned(PropInfo^.GetProc) and
        Assigned(PropInfo^.SetProc) then
        begin
          LObject := GetObjectProp(AObject, PropInfo);
          if LObject <> nil then
          begin
            Objeto := LObject;
            Result := True;
            //
            Exit;
          end;
        end;
      end;
    end;
  finally
    FreeMem(TempList);
  end;
end;

function TUnMyObjects.ObtemTextoRichEdit(Form: TForm; RichEdit: TRichEdit): WideString;
var
  Memo: TMemo;
  //Bufer: WideString;
  MemoryStream: TMemoryStream;
begin
  Result := #0;
  MemoryStream := TMemoryStream.Create;
  try
    RichEdit.Lines.SaveToStream(MemoryStream);
    MemoryStream.Position := 0;
    Memo := TMemo.Create(Form);
    Memo.Visible := False;
    try
      Memo.Parent := Form;
      Memo.Lines.LoadFromStream(MemoryStream);
      Result := Memo.Text;
    finally
      Memo.Free;
    end;
    { N�o funciona bem!
    Bufer := '';
    SetLength(Bufer, MemoryStream.Size);
    MemoryStream.ReadBuffer(Pointer(Bufer)^, MemoryStream.Size);
    Result := Bufer;
    }
  finally
    MemoryStream.Free;
  end;
end;

function TUnMyObjects.GetAllFiles(Subdiretorios: Boolean; Mask: string;
  ListBox: TCustomListBox; ClearListBox: Boolean; LaAviso1,
  LaAviso2: TLabel): Integer;
const
  Aviso = 'Arquivos localizados: ';
var
  search: TSearchRec;
  directory: string;
begin
  ListBox.Update;
  Application.ProcessMessages;
  //
  Result := 0;
  // N�o no loop!!!!
  if ClearListBox and (ListBox <> nil) then ListBox.Items.Clear;
  //
  directory := ExtractFilePath(mask);

  // find all files
  if FindFirst(mask, $23, search) = 0 then
  begin
    repeat
      // add the files to the listbox
      if ListBox <> nil then
      begin
        ListBox.Items.Add(directory + search.Name);
        if ListBox.Items.Count div 100 = ListBox.Items.Count / 100 then
          Informa2(LaAviso1, LaAviso2, True, Aviso + Geral.FF0(ListBox.Items.Count));
      end;
      Result := Result + 1;
      //Inc(Count);
    until FindNext(search) <> 0;
  end;

  if Subdiretorios then
  begin
    if FindFirst(directory + '*.*', faDirectory, search) = 0 then
    begin
      repeat
        if ((search.Attr and faDirectory) = faDirectory) and (search.Name[1] <> '.') then
          Result := Result + GetAllFiles(Subdiretorios, Directory + Search.Name + '\' +
          ExtractFileName(Mask), ListBox, False, LaAviso1, LaAviso2);
      until FindNext(search) <> 0;
      FindClose(search);
    end;
  end;
  Informa2(LaAviso1, LaAviso2, False, Aviso + Geral.FF0(ListBox.Items.Count) + ' !!!');
end;

{$IfNDef NAO_USA_FRX}
function TUnMyObjects.GetfrxRTF(RE: TfrxRichView): String;
var
  strStream: TStringStream;
  p: Integer;
begin
  if Re.RichEdit.Lines.Text = '' then
    Result := '' else
  begin
    strStream := TStringStream.Create('');
    try
      RE.PlainText := False;
      Re.RichEdit.Lines.SaveToStream(strStream);
      Result := strStream.DataString;
      p := Length(Result);
      if Result[p] = #0 then
        Result := Copy(Result, 1, p -1);
    finally
      strStream.Free;
    end;
  end;
end;

{$EndIf}

function TUnMyObjects.GetMinMaxDmk(ComponentClass: TComponentClass; Reference:
  TComponent; const FormatType: TAllFormat; const DefaultMin, DefaultMax:
  Variant; const Casas, LeftZeros: Integer; const ValMinMin, ValMaxMin,
  ValMinMax, ValMaxMax: String; const Obrigatorio: Boolean; const FormCaption,
  ValMinCaption, ValMaxCaption: String; const WidthVal: Integer; var
  ResultadoMin, ResultadoMax: Variant): Boolean;
var
  I: Integer;
  //
begin
  Result := False;
  ResultadoMin := 0;
  ResultadoMax := 0;
  //
  try
    Screen.Cursor := crHourGlass;
    try
      MyObjects.FormCria(ComponentClass, Reference);
      TForm(Reference).Caption := 'XXX-XXXXX-018 :: ' + FormCaption;
      with Reference do
      begin
        for I := 0 to ComponentCount -1 do
        begin
          if (Components[I] is TLabel) then
          begin
            if TLabel(Components[I]).Name = 'LaResultMin' then
            begin
              TLabel(Components[I]).Caption := ValMinCaption;
              TLabel(Components[I]).Visible := True;
            end;
            if TLabel(Components[I]).Name = 'LaResultMax' then
            begin
              TLabel(Components[I]).Caption := ValMaxCaption;
              TLabel(Components[I]).Visible := True;
            end;
          end else if (Components[I] is TdmkEdit) then
          begin
            if TdmkEdit(Components[I]).Name = 'EdResultMin' then
            begin
               TdmkEdit(Components[I]).Visible      := True;
               //TdmkEdit(Components[I]).DataFormat   := ;
               //TdmkEdit(Components[I]).HoraFormat   := ;
               //TdmkEdit(Components[I]).ForceNextYear   := ;
               TdmkEdit(Components[I]).DecimalSize  := Casas;
               TdmkEdit(Components[I]).LeftZeros    := LeftZeros;
               TdmkEdit(Components[I]).FormatType   := FormatType;
               TdmkEdit(Components[I]).Obrigatorio  := Obrigatorio;
               TdmkEdit(Components[I]).ValMax       := ValMaxMin;
               TdmkEdit(Components[I]).ValMin       := ValMinMin;
               TdmkEdit(Components[I]).ValueVariant := DefaultMin;
            end else
            if TdmkEdit(Components[I]).Name = 'EdResultMax' then
            begin
               TdmkEdit(Components[I]).Visible      := True;
               //TdmkEdit(Components[I]).DataFormat   := ;
               //TdmkEdit(Components[I]).HoraFormat   := ;
               //TdmkEdit(Components[I]).ForceNextYear   := ;
               TdmkEdit(Components[I]).DecimalSize  := Casas;
               TdmkEdit(Components[I]).LeftZeros    := LeftZeros;
               TdmkEdit(Components[I]).FormatType   := FormatType;
               TdmkEdit(Components[I]).Obrigatorio  := Obrigatorio;
               TdmkEdit(Components[I]).ValMax       := ValMaxMax;
               TdmkEdit(Components[I]).ValMin       := ValMinMax;
               TdmkEdit(Components[I]).ValueVariant := DefaultMax;
            end;
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
      TForm(Reference).ShowModal;
    end;
    Result := VAR_GETVALOR_GET;
    if Result then
    begin
      with Reference do
      begin
        for i := 0 to ComponentCount -1 do
        begin
          if (Components[I] is TdmkEdit) then
          begin
            if TdmkEdit(Components[I]).Name = 'EdResultMin' then
              ResultadoMin := TdmkEdit(Components[I]).ValueVariant
            else
            if TdmkEdit(Components[I]).Name = 'EdResultMax' then
              ResultadoMax := TdmkEdit(Components[I]).ValueVariant;
          end;
        end;
      end;
    end;
    Reference.Destroy;
    Screen.Cursor := crDefault;
  except
    //Result    := False;
    ResultadoMin := NULL;
    ResultadoMax := NULL;
    raise;
  end;
end;

function TUnMyObjects.GetRTF(RE: TRichedit): String;
var
  strStream: TStringStream;
  p: Integer;
begin
  if RE.Text = '' then
    Result := '' else
  begin
    strStream := TStringStream.Create('');
    try
      RE.PlainText := False;
      RE.Lines.SaveToStream(strStream);
      Result := strStream.DataString;
      p := Length(Result);
      if Result[p] = #0 then
        Result := Copy(Result, 1, p -1);
    finally
      strStream.Free;
    end;
  end;
end;

procedure TUnMyObjects.GetSelecionadosBookmark_Int1(Form: TForm; DBG: Vcl.DBGrids.TDBGrid;
  CampoInt1: String; Selecionados: TArrSelIdxInt1);
var
  Bookmark: TBookmark;
  Qry: TDataset;
  MyCursor: TCursor;
  I: Integer;
begin
  MyCursor := Form.Cursor;
  Form.Cursor := crHourGlass;
  //
  Qry := DBG.DataSource.DataSet;
  BookMark := Qry.Bookmark;
  Qry.DisableControls;
  try
    SetLength(Selecionados, DBG.SelectedRows.Count);
    for I := 0 to DBG.SelectedRows.Count - 1 do
    begin
      //Qry.GotoBookmark(DBG.SelectedRows.Items[I]);
      Qry.GotoBookmark(DBG.SelectedRows.Items[I]);
      //
      Selecionados[I] := Qry.FieldByName(CampoInt1).AsInteger;
    end;
  finally
    Form.Cursor := MyCursor;
    Qry.EnableControls;
    Qry.Bookmark := Bookmark;
  end;
end;

function TUnMyObjects.GetSelecionadosBookmark_Int1_ToStr(Form: TForm;
  DBG: Vcl.DBGrids.TDBGrid; CampoInt1: String): String;
var
  Bookmark: TBookmark;
  Qry: TDataset;
  MyCursor: TCursor;
  I, K: Integer;
begin
  Result := '';
  MyCursor := Form.Cursor;
  Form.Cursor := crHourGlass;
  //
  Qry := DBG.DataSource.DataSet;
  BookMark := Qry.Bookmark;
  Qry.DisableControls;
  try
    K := DBG.SelectedRows.Count - 1;
    for I := 0 to K - 1 do
    begin
      //Qry.GotoBookmark(DBG.SelectedRows.Items[I]);
      Qry.GotoBookmark(DBG.SelectedRows.Items[I]);
      //
      Result := Result + Geral.FF0(Qry.FieldByName(CampoInt1).AsInteger) + ', ';
    end;
    if K > -1 then
    begin
      //Qry.GotoBookmark(DBG.SelectedRows.Items[K]);
      Qry.GotoBookmark(DBG.SelectedRows.Items[K]);
      Result := Result + Geral.FF0(Qry.FieldByName(CampoInt1).AsInteger);
    end;
  finally
    Form.Cursor := MyCursor;
    Qry.EnableControls;
    Qry.Bookmark := Bookmark;
  end;
end;

procedure TUnMyObjects.GetSelecionadosBookmark_Int2(Form: TForm; DBG: Vcl.DBGrids.TDBGrid;
  CampoInt0, CampoInt1: String; var Selecionados: TArrSelIdxInt2);
var
  Bookmark: TBookmark;
  Qry: TDataset;
  MyCursor: TCursor;
  I: Integer;
begin
  MyCursor := Form.Cursor;
  Form.Cursor := crHourGlass;
  //
  Qry := DBG.DataSource.DataSet;
  BookMark := Qry.Bookmark;
  Qry.DisableControls;
  try
    SetLength(Selecionados, DBG.SelectedRows.Count);
    for I := 0 to DBG.SelectedRows.Count - 1 do
    begin
      //Qry.GotoBookmark(DBG.SelectedRows.Items[I]);
      Qry.GotoBookmark(DBG.SelectedRows.Items[I]);
      //
      Selecionados[I][0] := Qry.FieldByName(CampoInt0).AsInteger;
      Selecionados[I][1] := Qry.FieldByName(CampoInt1).AsInteger;
    end;
  finally
    Form.Cursor := MyCursor;
    Qry.EnableControls;
    Qry.Bookmark := Bookmark;
  end;
end;

function TUnMyObjects.GetTexto(ComponentClass: TComponentClass;
  Reference: TComponent; const FormCaption, ValCaption: String;
  var Texto: String): Boolean;
var
  i: Integer;

  begin
  try
    Screen.Cursor := crHourGlass;
    try
      MyObjects.FormCria(ComponentClass, Reference);
      TForm(Reference).Caption := 'XXX-XXXXX-016 :: ' + FormCaption;
      with Reference do
      begin
        for i := 0 to ComponentCount -1 do
        begin
          if (Components[i] is TdmkMemo) then
          begin
            if TdmkMemo(Components[i]).Name = 'MeTexto' then
            begin
               TdmkEdit(Components[i]).Visible      := True;
               TdmkMemo(Components[i]).Text         := Texto;
            end;
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
      TForm(Reference).ShowModal;
    end;
    Result := VAR_ALTEROU;
    if Result then
      Texto := VAR_GETTEXTO_GET;
    Reference.Destroy;
    Screen.Cursor := crDefault;
  except
    //Result    := False;
    Result := False;
    raise;
  end;
end;

function TUnMyObjects.GetCodigoDeBarras(ComponentClass: TComponentClass;
  Reference: TComponent; const FormCaption, ValCaption: String;
  var Texto: String): Boolean;
var
  i: Integer;

  begin
  try
    Screen.Cursor := crHourGlass;
    try
      MyObjects.FormCria(ComponentClass, Reference);
      TForm(Reference).Caption := 'XXX-XXXXX-017 :: ' + FormCaption;
      with Reference do
      begin
(*
        for i := 0 to ComponentCount -1 do
        begin
          if (Components[i] is TdmkMemo) then
          begin
            if TdmkMemo(Components[i]).Name = 'MeTexto' then
            begin
               TdmkEdit(Components[i]).Visible      := True;
               TdmkMemo(Components[i]).Text         := Texto;
            end;
          end;
        end;
*)
      end;
    finally
      Screen.Cursor := crDefault;
      TForm(Reference).ShowModal;
    end;
    Result := VAR_ALTEROU;
    if Result then
      Texto := VAR_GETEAN13_GET;
    Reference.Destroy;
    Screen.Cursor := crDefault;
  except
    //Result    := False;
    Result := False;
    raise;
  end;
end;

// GetInteiro, ObtemInteiro, GetInteger, ObtemInteger
// GetValor, ObtemValor, GetDouble, ObtemDouble
// GetData, ObtemData, GetDate, ObtemDate
// GetNumero, ObtemNumero, dmkCodigo, dmkNumero
// GetCodigo, ObtemCodigo, dmkGetValor, GetDmkValor
function TUnMyObjects.GetValorDmk(ComponentClass: TComponentClass;
  Reference: TComponent; FormatType: TAllFormat; Default: Variant; Casas,
  LeftZeros: Integer; ValMin, ValMax: String; Obrigatorio: Boolean; FormCaption,
  ValCaption: String; WidthVal: Integer; var Resultado: Variant): Boolean;
var
  i: Integer;

  begin
  try
    Screen.Cursor := crHourGlass;
    try
      MyObjects.FormCria(ComponentClass, Reference);
      TForm(Reference).Caption := 'XXX-XXXXX-011 :: ' + FormCaption;
      with Reference do
      begin
        for i := 0 to ComponentCount -1 do
        begin
          if (Components[i] is TLabel) then
          begin
            if TLabel(Components[i]).Name = 'LaResult' then
            begin
              TLabel(Components[i]).Caption := ValCaption;
              TLabel(Components[i]).Visible := True;
            end;
          end else if (Components[i] is TdmkEdit) then
          begin
            if TdmkEdit(Components[i]).Name = 'EdResult' then
            begin
               TdmkEdit(Components[i]).Visible      := True;
               //TdmkEdit(Components[i]).DataFormat   := ;
               //TdmkEdit(Components[i]).HoraFormat   := ;
               //TdmkEdit(Components[i]).ForceNextYear   := ;
               TdmkEdit(Components[i]).DecimalSize  := Casas;
               TdmkEdit(Components[i]).LeftZeros    := LeftZeros;
               TdmkEdit(Components[i]).FormatType   := FormatType;
               TdmkEdit(Components[i]).Obrigatorio  := Obrigatorio;
               TdmkEdit(Components[i]).ValMax       := ValMax;
               TdmkEdit(Components[i]).ValMin       := ValMin;
               TdmkEdit(Components[i]).ValueVariant := Default;
            end;
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
      TForm(Reference).ShowModal;
    end;
    Result        := VAR_GETVALOR_GET;
    if Result then
    begin
      with Reference do
      begin
        for i := 0 to ComponentCount -1 do
        begin
          if (Components[i] is TdmkEdit) then
          begin
            if TdmkEdit(Components[i]).Name = 'EdResult' then
            begin
              Resultado := TdmkEdit(Components[i]).ValueVariant;
            end;
          end;
        end;
      end;
    end;
    Reference.Destroy;
    Screen.Cursor := crDefault;
  except
    //Result    := False;
    Resultado := NULL;
    raise;
  end;
end;

function TUnMyObjects.GetValorEDataDmk(ComponentClass: TComponentClass;
  Reference: TComponent; FormatType: TAllFormat; Default: Variant; Casas,
  LeftZeros: Integer; ValMin, ValMax: String; Obrigatorio: Boolean; FormCaption,
  ValCaption: String; WidthVal: Integer; DateCaption: String; HabilitaEdit,
  HabilitaDate: Boolean; var Resultado: Variant; var Data: TDateTime): Boolean;
var
  i: Integer;
begin
  try
    Screen.Cursor := crHourGlass;
    try
      MyObjects.FormCria(ComponentClass, Reference);
      TForm(Reference).Caption := FormCaption;
      with Reference do
      begin
        for i := 0 to ComponentCount -1 do
        begin
          if (Components[i] is TLabel) then
          begin
            if TLabel(Components[i]).Name = 'LaResult' then
            begin
              TLabel(Components[i]).Caption := ValCaption;
              TLabel(Components[i]).Visible := True;
              TLabel(Components[i]).Enabled := HabilitaEdit;
            end else
            if TLabel(Components[i]).Name = 'LaData' then
            begin
              TLabel(Components[i]).Caption := DateCaption;
              TLabel(Components[i]).Visible := True;
              TLabel(Components[i]).Enabled := HabilitaDate;
            end;
          end else if (Components[i] is TdmkEdit) then
          begin
            if TdmkEdit(Components[i]).Name = 'EdResult' then
            begin
               TdmkEdit(Components[i]).Visible      := True;
               //TdmkEdit(Components[i]).DataFormat   := ;
               //TdmkEdit(Components[i]).HoraFormat   := ;
               //TdmkEdit(Components[i]).ForceNextYear   := ;
               TdmkEdit(Components[i]).DecimalSize  := Casas;
               TdmkEdit(Components[i]).LeftZeros    := LeftZeros;
               TdmkEdit(Components[i]).FormatType   := FormatType;
               TdmkEdit(Components[i]).Obrigatorio  := Obrigatorio;
               TdmkEdit(Components[i]).ValMax       := ValMax;
               TdmkEdit(Components[i]).ValMin       := ValMin;
               TdmkEdit(Components[i]).Enabled      := HabilitaEdit;
               TdmkEdit(Components[i]).ValueVariant := Default;
            end;
          end else if (Components[i] is TdmkEditDateTimePicker) then
          begin
            TdmkEditDateTimePicker(Components[i]).Visible := True;
            TdmkEditDateTimePicker(Components[i]).Date    := Data;
            TdmkEditDateTimePicker(Components[i]).Enabled := HabilitaDate;
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
      TForm(Reference).ShowModal;
    end;
    Result        := VAR_GETVALOR_GET;
    if Result then
    begin
      with Reference do
      begin
        for i := 0 to ComponentCount -1 do
        begin
          if (Components[i] is TdmkEdit) then
          begin
            if TdmkEdit(Components[i]).Name = 'EdResult' then
              Resultado := TdmkEdit(Components[i]).ValueVariant;
          end else if (Components[i] is TdmkEditDateTimePicker) then
          begin
            if TdmkEditDateTimePicker(Components[i]).Name = 'TPData' then
              Data := TdmkEditDateTimePicker(Components[i]).Date;
          end;
        end;
      end;
    end;
    Reference.Destroy;
    Screen.Cursor := crDefault;
  except
    //Result    := False;
    Resultado := NULL;
    raise;
  end;
end;

{$IFNDEF NAO_USA_TEXTOS}
function TUnMyObjects.EditaTextoRichEdit(RichEdit: TRichEdit;
  IniAtributesSize: Integer; IniAtributesName: String;
  ListaDeTags: array of String): WideString;
var
  //MemoryStream: TMemoryStream;
  I: Integer;
begin
  Application.CreateForm(TFmTextos2, FmTextos2);
  FmTextos2.FAtributesSize   := IniAtributesSize;
  FmTextos2.FAtributesName   := IniAtributesName;
  FmTextos2.FSaveSit         := 3;
  FmTextos2.FRichEdit        := RichEdit;
  FmTextos2.LBItensMD.Sorted := False;
  for I := Low(ListaDeTags) to high(ListaDeTags) do
    FmTextos2.LBItensMD.Items.Add(ListaDeTags[I]);
  FmTextos2.LBItensMD.Sorted := True;
  //
  MyObjects.TextoEntreRichEdits(RichEdit, FmTextos2.Editor);
  //FmTextos2.DefineNomeFonte(IniAtributesName);

  FmTextos2.FontName.Canvas.Font.Name := 'Tahoma';
  FmTextos2.FontName.FonteNome := 'Tahoma';

  FmTextos2.ShowModal;
  FmTextos2.Destroy;
end;
{$ENDIF}

procedure TUnMyObjects.EditCB_ValUnico(ScreenActiveForm: TForm);
var
  i: Integer;
  EditCB: TdmkEditCB;
(*
  CBLCB: TDBLookupComboBox;
  Ds: TDataSource;
  Qr: TDataSet;
*)
begin
  for i := 0 to ScreenActiveForm.ComponentCount -1 do
  begin
    ////////////////////////////////////////////////////////////////////////////
    if ScreenActiveForm.Components[i] is TdmkEditCB then
    begin
      EditCB := TdmkEditCB(ScreenActiveForm.Components[i]);
      EditCB.SelIfOnlyOneReg(TSetValCompo.setregOnFormActivate);
    end;
  end;
end;

procedure TUnMyObjects.Entitula(GroupBox: TGroupBox;
              Titulos, Avisos: array of TLabel; Titulo: String;
              RetiraIDForm: Boolean; Alignment: TAlignment;
              Left, Top, FontSize: Integer);
  procedure AjustaFonte(MyLabel: TLabel; Qualidade: Integer);
  var
    tagLOGFONT: TLogFont;
    Font: TFont;
  begin
    Font := MyLabel.Font;
    GetObject(
      Font.Handle,
      SizeOf(TLogFont),
      @tagLOGFONT);
    //
    tagLOGFONT.lfQuality  := Qualidade;
    {
    tagLOGFONT.lfQuality  := DEFAULT_QUALITY;
    tagLOGFONT.lfQuality  := DRAFT_QUALITY;
    tagLOGFONT.lfQuality  := PROOF_QUALITY;
    tagLOGFONT.lfQuality  := NONANTIALIASED_QUALITY;
    tagLOGFONT.lfQuality  := ANTIALIASED_QUALITY;
    }
    MyLabel.Font.Handle := CreateFontIndirect(tagLOGFONT);
  end;
const
  T = 18;
var
  Form: TCustomForm;
  Texto: String;
  Tam, I, J: Integer;
begin
  if GroupBox <> nil then
  begin
    Tam := GroupBox.Width - (2 * Left);
    if RetiraIDForm then
      Texto := Copy(Titulo, T)
    else
      Texto := Titulo;
    //
    j := High(Avisos);
    for I := 0 to J do
    begin
      if I div 2 = I / 2 then
      begin
        Avisos[I].Font.Color := VAR_COR_AVISO_A;
        Avisos[I].Visible := VAR_COR_AVISO_A <> clNone;
      end
      else
      begin
        Avisos[I].Font.Color := VAR_COR_AVISO_C;
        Avisos[I].Visible := VAR_COR_AVISO_C <> clNone;
      end;
      //AjustaFonte(Avisos[I], ANTIALIASED_QUALITY);
      AjustaFonte(Avisos[I], PROOF_QUALITY);
      // N�o sei se precisa, mas l� vai:
      Avisos[I].Invalidate;
    end;

    //

    j := High(Titulos);
    //
    if (j > -1) and (Titulos[0] <> nil) then
    begin
      Titulos[0].StyleElements := [seClient,seBorder];
      Titulos[0].Font.Color := VAR_COR_TIT_A;
      Titulos[0].Visible := VAR_COR_TIT_A <> clNone;
      Titulos[0].Left := Left + VAR_COR_POS_A_X;
      Titulos[0].Top := Top + VAR_COR_POS_A_Y;
      Titulos[0].BringToFront;
      //Titulos[0].BringToFront;
    end;
    if (j > 0) and (Titulos[1] <> nil) then
    begin
      Titulos[1].StyleElements := [seClient,seBorder];
      Titulos[1].Font.Color := VAR_COR_TIT_B;
      Titulos[1].Visible := VAR_COR_TIT_B <> clNone;
      Titulos[1].Left := Left + VAR_COR_POS_B_X;
      Titulos[1].Top := Top + VAR_COR_POS_B_Y;
      Titulos[1].BringToFront;
      //Titulos[1].BringToFront;
    end;
    if (j > 1) and (Titulos[2] <> nil) then
    begin
      Titulos[2].StyleElements := [seClient,seBorder];
      Titulos[2].Font.Color := VAR_COR_TIT_C;
      Titulos[2].Visible := VAR_COR_TIT_C <> clNone;
      Titulos[2].Left := Left;
      Titulos[2].Top := Top;
      Titulos[2].BringToFront;
      //Titulos[2].BringToFront;
    end;
    for I := 0 to J do
    begin
      if Titulos[I] <> nil then
      begin
        // Colocar individual no futuro!
        //AjustaFonte(Titulos[I], ANTIALIASED_QUALITY);
        AjustaFonte(Titulos[I], PROOF_QUALITY);
        Titulos[I].AutoSize := False;
        Titulos[I].Width := Tam;
        Titulos[I].Caption := Texto;
        Titulos[I].Alignment := Alignment;
        Titulos[I].Font.Size := FontSize;
        // N�o sei se precisa, mas l� vai:
        //Titulos[I].Invalidate;
      end;
    end;
    //
    Application.ProcessMessages;
  end;
end;

function TUnMyObjects.ErroDeSocket(Errno: word; ErrMsg: String): String;
(*  Anonymous socket error list
� Gepost op: september 06, 2005, 12:17:45 pm �
http://www.wannagame.nl/forum/index.php?PHPSESSID=167f4e1ad1325519ee91295b0163a5a0&topic=1263.0;prev_next=prev
--------------------------------------------------------------------------------*)
begin
  case Errno of
    10004: Result := 'Interrup��o da fun��o de chamada';
    10013: Result := 'Permiss�o negada';
    10014: Result := 'Endere�o inv�lido';
    10022: Result := 'Argumento inv�lido';
    10024: Result := 'Muitos arquivos abertos';
    10035: Result := 'Recursos temporariamente indispon�veis';
    10036: Result := 'Opera��o em progresso agora';
    10037: Result := 'A opera��o ainda est� em progresso';
    10038: Result := 'Opera��o de socket sem socket';
    10039: Result := 'Endere�o de destino obrigat�rio';
    10040: Result := 'Mensagem muito longa';
    10041: Result := 'Protocolo errado para o socket';
    10042: Result := 'Opc�o inv�lida de protocolo';
    10043: Result := 'Protocolo n�o suportado';
    10044: Result := 'Tipo de socket n�o suportado';
    10045: Result := 'Opera��o n�o suportada';
    10046: Result := 'Fam�lia de protocolos n�o suportada';
    10047: Result := 'Endere�o de fam�lia n�o suportado pelo protocolo de fam�lia';
    10048: Result := 'Endere�o j� est� sendo usado';


    10049: Result := 'Endere�o requisitado n�o acess�vel';
    10050: Result := 'A rede n�o est� ativa';
    10051: Result := 'A rede est� inacess�vel';
    10052: Result := 'A conex�o da rede caiu ao resetar';
    10053: Result := 'O software abortou a conex�o';


    10054: Result := 'Conex�o resetada pelo parceiro';


    10055: Result := 'Sem espa�o de mem�ria buffer';
    10056: Result := 'O socket j� est� conectado';
    10057: Result := 'O socket n�o est� conectado';
    10058: Result := 'Imposs�vel enviar ap�s o socket desconectar';
    10060: Result := 'O tempo de conex�o expirou';


    10061: Result := 'Conex�o recusada (ou o aplicativo n�o est� rodando no servidor)';


    10064: Result := 'O hospedeiro caiu';
    10065: Result := 'N�o h� roteamento ao hospedeiro';
    10067: Result := 'Muitos processos';
    10091: Result := 'O subsistema da rede n�o est� dispon�vel';
    10092: Result := 'vers�o WINSOCK.DLL ultrapassada';
    10093: Result := 'O WSAStartup ainda n�o executou com sucesso';

    10094: Result := 'Desligamento normal em progresso';
    11001: Result := 'Hospedeiro n�o encontrado';
    11002: Result := 'Hospedeiro n�o-autoritativo n�o encontrado';
    11003: Result := 'Este � um erro n�o recuper�vel';
    11004: Result := 'Nome v�lido, sem registro para esta requisi��o';

    ///////

    (*10004: 'Interrupted function call'
    10013: 'Permission denied'
    10014: 'Bad address'
    10022: 'Invalid argument'
    10024: 'Too many open files'
    10035: 'Resource temporarily unavailable'
    10036: 'Operation now in progress'
    10037: 'Operation already in progress'
    10038: 'Socket operation on non-socket'
    10039: 'Destination address required'
    10040: 'Message too long'
    10041: 'Protocol wrong type for socket'
    10042: 'Bad protocol option'
    10043: 'Protocol not supported'
    10044: 'Socket type not supported'
    10045: 'Operation not supported'
    10046: 'Protocol family not supported'
    10047: 'Address family not supported by protocol family'
    10048: 'Address already in use'


    10049: 'Cannot assign requested address'
    10050: 'Network is down'
    10051: 'Network is unreachable'
    10052: 'Network dropped connection on reset'
    10053: 'Software caused connection abort'


    10054: 'Connection reset by peer'


    10055: 'No buffer space available'
    10056: 'Socket is already connected'
    10057: 'Socket is not connected'
    10058: 'Cannot send after socket shutdown'
    10060: 'Connection timed out'


    10061: 'Connection refused'


    10064: 'Host is down'
    10065: 'No route to host'
    10067: 'Too many processes'
    10091: 'Network subsystem is unavailable'
    10092: 'WINSOCK.DLL version out of range'
    10093: 'Successful WSAStartup not yet performed'

    10094: 'Graceful shutdown in progress'
    11001: 'Host not found'
    11002: 'Non-authoritative host not found'
    11003: 'This is a non-recoverable error'
    11004: 'Valid name, no data record of requested type'*)

    else Result := ErrMsg;
  end;
end;

procedure TUnMyObjects.EscolhePeriodo_MesEAno(ComponentClass: TComponentClass;
  Reference: TComponent; var Mes, Ano: Word; var Cancelou: Boolean; MostraMes,
  MostraAno: Boolean);
var
  i, j: Integer;
begin
  MyObjects.FormCria(ComponentClass, Reference);
  with Reference do
  begin
    for i := 0 to ComponentCount -1 do
    begin
      if (Components[i] is TLabel) then
      begin
        if TLabel(Components[i]).Name = 'LaMes'  then
          TLabel(Components[i]).Visible := MostraMes else
        if TLabel(Components[i]).Name = 'LaAno'  then
          TLabel(Components[i]).Visible := MostraAno;
      end else
      if (Components[i] is TComboBox) then
      begin
        if TComboBox(Components[i]).Name = 'CBMes'  then
          TComboBox(Components[i]).Visible := MostraMes else
        if TComboBox(Components[i]).Name = 'CBAno'  then
          TComboBox(Components[i]).Visible := MostraAno;
      end;
    end;
    if (Mes > 0) or (Ano > 0) then
    begin
      for i := 0 to ComponentCount -1 do
      begin
        if (Components[i] is TComboBox) then
        begin
          if TComboBox(Components[i]).Name = 'CBMes'  then
          begin
            if Mes > 0 then
            TComboBox(Components[i]).ItemIndex := Mes -1;
          end else
          if TComboBox(Components[i]).Name = 'CBAno'  then
          begin
            if Ano > 0 then
            begin
              for j := 0 to TComboBox(Components[i]).Items.Count do
              begin
                if Geral.IMV(TComboBox(Components[i]).Items[j]) = Ano then
                begin
                  TComboBox(Components[i]).ItemIndex := j;
                  Break;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  TForm(Reference).ShowModal;
  with Reference do
  begin
    for i := 0 to ComponentCount -1 do
    begin
      if (Components[i] is TLabel) then
      begin
        if TLabel(Components[i]).Name = 'LaSelMes'  then
          Mes := Geral.IMV(TLabel(Components[i]).Caption) else
        if TLabel(Components[i]).Name = 'LaSelAno'  then
          Ano := Geral.IMV(TLabel(Components[i]).Caption);
      end;
    end;
  end;
  Reference.Destroy;
  if (Mes = 0) and (Ano = 0) then Cancelou := True else Cancelou := False;
end;

{$IfDef cAdvGrid} //Berlin
procedure TUnMyObjects.UpdMergeDBAvGrid(ASG: TDBAdvGrid;
  Cols: array of Integer);
var
  I: Integer;
begin
  ASG.SplitAllCells();
  //
  for I := Low(Cols) to High(Cols) do
    ASG.MergeColumnCells(Cols[I], True);
  //
  ASG.Invalidate;
end;
{$EndIf}

procedure TUnMyObjects.UpdPB(PB: TProgressBar; Label1, Label2: TLabel);
var
  Txt: String;
begin
  if PB <> nil then
  begin
    PB.Position := PB.Position + 1;
    PB.Update;
    //
    if (Label1 <> nil) or (Label2 <> nil) then
    begin
      Txt := IntToStr(PB.Position) + ' de ' + IntToStr(PB.Max);
      if Label1 <> nil then
        Label1.Caption := Txt;
      if Label2 <> nil then
        Label2.Caption := Txt;
    end;
  end;
  Application.ProcessMessages;
end;

procedure TUnMyObjects.UpdPBOnly(PB: TProgressBar);
var
  Txt: String;
begin
  PB.Position := PB.Position + 1;
  //PB.Update;
  Application.ProcessMessages;
end;

function TUnMyObjects.ExcluiLinhaStringGrid(Grade: TStringGrid;
  Pergunta: Boolean = True): Boolean;
var
  Continua: Boolean;
  x, i, j, Line: Integer;
begin
  Result   := False;
  Continua := False;
  //
  if Pergunta = True then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o de toda linha ' +
      Geral.FF0(Grade.Row) + '?') = ID_YES
    then
      Continua := True;
  end else
    Continua := True;
  //
  if Continua = True then
  begin
    x := 1;
    Line := Grade.Row;
    for i := Line to Grade.RowCount -2  do
    begin
      for j := 1 to Grade.ColCount - x do
      begin
        Grade.Cells[j, i] := Grade.Cells[j, i+1];
      end;
    end;
    for i := 1 to Grade.ColCount do Grade.Cells[i, Grade.RowCount-1] := '';
    Line := Grade.RowCount - x;
    if Line < 2 then
    begin
      //for i := 1 to Grade.ColCount do Grade.Cells[i, Grade.RowCount-1] := '';
    end else Grade.RowCount := Line;
    Result := True;
  end;
end;

procedure TUnMyObjects.ExecutaCmd(cmd: String; MeResult: TMemo);
var
  buf: array[0..4095] of byte;
  si: STARTUPINFO;
  sa: SECURITY_ATTRIBUTES;
  sd: SECURITY_DESCRIPTOR;
  pi: PROCESS_INFORMATION;
  newstdin,newstdout,read_stdout,write_stdin: THandle;
  exitcod,bread,avail: Cardinal;
begin
  if dmkPF.IsWinNT  then
    begin
      InitializeSecurityDescriptor(@sd,SECURITY_DESCRIPTOR_REVISION);
      SetSecurityDescriptorDacl(@sd, true, nil, false);
      sa.lpSecurityDescriptor := @sd;
    end else sa.lpSecurityDescriptor := nil;
  sa.nLength := sizeof(SECURITY_ATTRIBUTES);
  sa.bInheritHandle := true;
  // Tuberias
  if CreatePipe(newstdin,write_stdin,@sa,0) then
  begin
    if CreatePipe(read_stdout,newstdout,@sa,0) then
    begin
      GetStartupInfo(si);
      with si do
      begin
        dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
        wShowWindow := SW_HIDE;
        hStdOutput  := newstdout;
        hStdError   := newstdout;
        hStdInput   := newstdin;
      end;
      fillchar(buf,sizeof(buf),0);
      GetEnvironmentVariable('COMSPEC',@buf,sizeof(buf)-1);
      strcat(@buf, PChar(' /c '+cmd));
      if CreateProcess(nil,@buf,nil,nil,TRUE,CREATE_NEW_CONSOLE,nil,nil,si,pi) then
      begin
        if MeResult <> nil then
          MeResult.lines.Clear;
        GetExitCodeProcess(pi.hProcess,exitcod);
        PeekNamedPipe(read_stdout,@buf,sizeof(buf)-1,@bread,@avail,nil);
        while (exitcod = STILL_ACTIVE) or (bread > 0) do
        begin
          Application.ProcessMessages;
          if (bread > 0) then
          begin
            fillchar(buf,sizeof(buf),0);
            if (avail > sizeof(buf)-1) then
              while (bread >= sizeof(buf)-1)  do
              begin
                ReadFile(read_stdout,buf,sizeof(buf)-1,bread,nil);
                if MeResult <> nil then
                  MeResult.lines.text := MeResult.lines.Text + String(StrPas(PAnsiChar(@buf)));
                fillchar(buf,sizeof(buf),0);
              end else
              begin
                ReadFile(read_stdout,buf,sizeof(buf)-1,bread,nil);
                if MeResult <> nil then
                  MeResult.lines.text := String(MeResult.lines.Text + String(StrPas(PAnsiChar(@buf))));
              end;
            end;
            GetExitCodeProcess(pi.hProcess,exitcod);
            PeekNamedPipe(read_stdout,@buf,sizeof(buf)-1,@bread,@avail,nil);
          end;
        end;
       CloseHandle(read_stdout);
       CloseHandle(newstdout);
    end;
    CloseHandle(newstdin);
    CloseHandle(write_stdin);
  end;
end;

function TUnMyObjects.ExportaMemoToFile(Memo: TMemo; Arquivo: String; Exclui,
  EscreveLinhasVazias, SemAcentos: Boolean): Boolean;
var
  Text: TextFile;
  i: Integer;
  txt: String;
begin
  try
    if Exclui then if FileExists(Arquivo) then DeleteFile(Arquivo);
    AssignFile(Text, Arquivo);
    ReWrite(Text);
    //WriteLn(Text, Memo.Text);
    for i := 0 to Memo.Lines.Count -1 do
    begin
      txt := Memo.Lines[i];
      if SemAcentos then txt := Geral.SemAcento(txt);
      if not EscreveLinhasVazias then
      begin
        if txt <> '' then
          WriteLn(Text, txt);
      end else WriteLn(Text, txt);
    end;
    CloseFile(Text);
    Result := True;
  except
    raise;
  end;
end;

function TUnMyObjects.ExportaMemoToFileExt(Memo: TMemo; Arquivo: String; Exclui,
  EscreveLinhasVazias: Boolean; SemAcentos: TSemAcentos; CrLf: Integer;
  FileEnd: Variant): Boolean;
var
  Text: TextFile;
  i: Integer;
  txt: String;
  GravaLinha: Boolean;
begin
  try
    ForceDirectories(ExtractFilePath(Arquivo));
    if Exclui then if FileExists(Arquivo) then DeleteFile(Arquivo);
    AssignFile(Text, Arquivo);
    ReWrite(Text);
    for i := 0 to Memo.Lines.Count -1 do
    begin
      GravaLinha := False;
      txt := Memo.Lines[i];
      //if SemAcentos then txt := Geral.SemAcento(txt);
      case SemAcentos of
        etxtsaNaoMuda: ; //
        etxtsaSemAcento: txt := Geral.SemAcento(txt);
        //etxtsaSPED: txt := Geral.TxtSemPipe(txt);
      end;
      if not EscreveLinhasVazias then
      begin
        if txt <> '' then
          GravaLinha := True;
      end else GravaLinha := True;
      // 2010-06-27
      //while (Length(txt) > 0) and (txt[length(txt)] in [ # 1 0 , # 1 3 ]) do
{$IFDEF DELPHI12_UP}
      while (Length(txt) > 0) and (CharInSet(txt[length(txt)], [#10,#13])) do
{$ELSE}
      while (Length(txt) > 0) and (txt[length(txt)] in [#10,#13]) do
{$ENDIF}
        txt := Copy(txt, 1, length(txt) -1);
      //
      if GravaLinha then
      begin
        case CrLf of
            10: Write(Text, txt + Char( 10 ));
            13: Write(Text, txt + Char( 13 ));
          1013: Write(Text, txt + Char( 10 ) + Char( 13 ));
          1310: Write(Text, txt + Char( 13 ) + Char( 10 )); // => WriteLn = sLineBreak
          else Write(Text, txt);
        end;
      end;
    end;
    if FileEnd <> Null then
    begin
      Write(Text, FileEnd);  // Hexadecimal 1A ou ASCII 26 => FileEnd
    end;
    CloseFile(Text);
    Result := True;
  except
    raise;
  end;
end;

{
procedure TUnMyObjects.ExpandTree(Tree: TTreeView; Level: Integer);
  procedure ExpandParents(Node: TTreeNode);
  var
    aNode : TTreeNode;
  begin
    aNode := Node.Parent;
    while aNode <> nil do
    begin
      if not aNode.Expanded then
        aNode.Expand(false);
      aNode := aNode.Parent;
    end;
  end;
  //
var
  aNode : TTreeNode;
begin
  if Tree.Items.Count > 0 then
  begin
    aNode := Tree.Items[0];
    //
    while aNode <> nil do
    begin
      if aNode.Level = Level then
      begin
        aNode.Expand(false);
        ExpandParents(aNode);
      end;
      aNode := aNode.GetNext;
    end;
  end;
end;
}

procedure TUnMyObjects.DefineDataFieldSelecionadoInt(dmkEditCB: TdmkEditCB;
  DBEdit: TDBEdit; DataField: String);
begin
  if dmkEditCB.ValueVariant = 0 then
    DBEdit.DataField := ''
  else
    DBEdit.DataField := DataField;
end;

function TUnMyObjects.DefineDiretorio(Form: TForm; dmkEdit: TdmkEdit): String;
var
  IniDir, Arquivo: String;
begin
  Result := '';
  if dmkEdit <> nil then
  begin
    IniDir := ExtractFileDir(dmkEdit.Text);
    Arquivo := ExtractFileName(dmkEdit.Text);
  end else
  begin
    IniDir := '';
    Arquivo := '';
  end;
  if FileOpenDialog(Form, IniDir, Arquivo,
  'Selecione o diret�rio', '', [], Arquivo) then
  begin
    Result := ExtractFileDir(Arquivo);
    if dmkEdit <> nil then
      dmkEdit.Text := Result;
  end;
end;

function TUnMyObjects.DefineArquivo1(Form: TForm; dmkEdit: TdmkEdit): Boolean;
var
  IniDir, Arquivo: String;
begin
  Result := False;
  IniDir := ExtractFileDir(dmkEdit.Text);
  Arquivo := ExtractFileName(dmkEdit.Text);
  if FileOpenDialog(Form, IniDir, Arquivo,
    'Selecione o Arquivo', '', [], Arquivo) then
  begin
    dmkEdit.Text := Arquivo;
    Result := True;
  end;
end;

function TUnMyObjects.DefineArquivo2(Form: TForm; dmkEdit: TdmkEdit; Pasta,
Arquivo: String; Prioridade: TPriorityPath; SoArquivo: Boolean): Boolean;
var
  ArqA, ArqB, DirA, DirB, IniDir, IniArq: String;
begin
  Result := False;
  if Prioridade = ppInfo_Ini then
  begin
    DirA := Pasta;
    ArqA := Arquivo;
    DirB := ExtractFileDir(dmkEdit.Text);
    ArqB := ExtractFileName(dmkEdit.Text);
  end;
  if Prioridade = ppAtual_Edit then
  begin
    DirA := ExtractFileDir(dmkEdit.Text);
    ArqA := ExtractFileName(dmkEdit.Text);
    DirB := Pasta;
    ArqB := Arquivo;
  end;
  //
  if Trim(DirA) <> '' then
    IniDir := DirA
  else
    IniDir := DirB;
  if Trim(ArqA) <> '' then
    IniArq := ArqA
  else
    IniArq := ArqB;
  //
  if FileOpenDialog(Form, IniDir, IniArq,
  'Selecione o Arquivo', '', [], IniArq) then
  begin
    Result := True;
    if SoArquivo then
    begin
      if pos(AnsiUppercase(IniDir), AnsiUppercase(IniArq)) = 1 then
        dmkEdit.Text := Copy(IniArq, Length(IniDir) + 1)
      else
      begin
        dmkEdit.Text := IniArq;
        Geral.MB_Aviso('Diret�rio base difere do esperado!');
      end;
    end else
      dmkEdit.Text := IniArq;
  end;
end;

function TUnMyObjects.DefineArquivo3(Form: TForm; dmkEdit: TdmkEdit): String;
var
  IniDir, Arquivo: String;
begin
  Result := '';

  if dmkEdit <> nil then
  begin
    IniDir := ExtractFileDir(dmkEdit.Text);
    Arquivo := ExtractFileName(dmkEdit.Text);
  end else
  begin
    IniDir  := '';
    Arquivo := '';
  end;

  if FileOpenDialog(Form, IniDir, Arquivo,
    'Selecione o Arquivo', '', [], Arquivo) then
  begin
    if dmkEdit <> nil then
      dmkEdit.Text := Arquivo;
    Result := Arquivo;
  end;
end;

function TUnMyObjects.DefineArquivoImagem(var Arquivo: String; const Filter: String): Boolean;
var
  OPD: TOpenPictureDialog;
begin
  Arquivo := '';
  OPD := TOpenPictureDialog.Create(nil);
  try
    OPD.Filter := Filter;
    Result := OPD.Execute;
    if Result then
    begin
      Arquivo := OPD.FileName;
    end;
  finally
    OPD.Free;
  end;
end;

procedure TUnMyObjects.DefineCorTextoSitLancto(DBGrid: Vcl.DBGrids.TDBGrid; Rect: TRect;
  FieldName, TextoSit: String);
var
  Cor: TColor;
begin
  ///
  if (FieldName = 'NOMESIT') then
  begin
    if      TextoSit = CO_VENCIDA      then Cor := clRed
    else if TextoSit = CO_PAGTOPARCIAL then Cor := clPurple
    else if TextoSit = CO_PREDATADO    then Cor := $000202CC // averm
    else if TextoSit = CO_QUIT_AUTOM   then Cor := clGray
    else if TextoSit = CO_CANCELADO    then Cor := clSilver
    else if TextoSit = CO_BLOQUEADO    then Cor := clMaroon
    else Cor := clBlack;
    with DBGrid.Canvas do
    begin
      //if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
      Font.Style := [fsBold];
      Font.Color := Cor;
      FillRect(Rect);
      //TextOut(Rect.Left + 2, Rect.Top  + 2, TextoSit (*Column.Field.DisplayText*));
      TextOut(Rect.Left + 3, Rect.Top  + 2, TextoSit (*Column.Field.DisplayText*));
    end;
  end;
end;

procedure TUnMyObjects.DefineTextoEWidth(Compo: TPersistent; Texto, FontName:
  String; FontSize: Integer);
var
  PropInfo: PPropInfo;
  //
  function TryTexto(Propriedade: String): Boolean;
  begin
    Result := False;
    if Compo <> nil then
    begin
      PropInfo := GetPropInfo(Compo, Propriedade);
      if PropInfo <> nil then
      begin
        SetPropValue(Compo, Propriedade, Texto);
        Result := True;
      end;
    end;
  end;
  function TryWidth(Propriedade: String): Boolean;
  var
    Width: Integer;
  begin
    Width  := CalculaCanvasTextWidth(Texto, FontName, FontSize);
    Result := False;
    if Compo <> nil then
    begin
      PropInfo := GetPropInfo(Compo, Propriedade);
      if PropInfo <> nil then
      begin
        SetPropValue(Compo, Propriedade, Width + 10);
        Result := True;
      end;
    end;
  end;
begin
  if not TryTexto('Caption') then
  begin
    if not TryTexto('Text') then
    begin
      Geral.MB_Erro('N�o foi pssivel definir o texto em componente!');
      Exit;
    end;
  end;
  if not TryWidth('Width') then
  begin
    //Geral.MB_Erro('N�o foi pssivel definir o texto em componente!');
    //Exit;
  end;
end;

procedure TUnMyObjects.DefineTextoRichEdit(RichEdit: TRichEdit; Texto: String);
{$IFDEF DELPHI12_UP}
begin
  TDBRichEdit_LoadMemo(RichEdit,  Texto);
{$ELSE}
var
  MemoryStream: TMemoryStream;
begin
  RichEdit.ParentFont := False;
  //
  MemoryStream := TMemoryStream.Create;
  try
    // Write the string to the stream. We want to write from SourceString's
    // data, starting at a pointer to SourceString (this returns a pointer to
    // the first character), dereferenced (this gives the actual character).
    MemoryStream.WriteBuffer(Pointer(Texto)^, Length(Texto));
    // Go back to the start of the stream
    MemoryStream.Position := 0;
    RichEdit.Lines.LoadFromStream(MemoryStream);
    //
   {
    // Read it back in to make sure it worked.
    Bufer := 'I am doing just fine!';
    // Set the length, so we have space to read into
    SetLength(Bufer, MemoryStream.Size);
    MemoryStream.ReadBuffer(Pointer(Bufer)^, MemoryStream.Size);
    ShowMessage(Bufer);
    }
  finally
    MemoryStream.Free;
  end;
{$ENDIF}
end;

procedure TUnMyObjects.DefineTituloDBGrid(DBGrid: Vcl.DBGrids.TDBGrid;
FieldName, Titulo: String);
var
  I: Integer;
begin
  if Titulo <> '' then
  begin
    for I := 0 to DBGrid.Columns.Count - 1 do
      if Uppercase(DBGrid.Columns[I].FieldName) = Uppercase(FieldName) then
        DBGrid.Columns[I].Title.Caption := Titulo;
  end;
end;

procedure TUnMyObjects.DefineValDeCompoEmbeded(Container: TdmkCompoStore;
  Valor: Variant);
var
  Componente: TComponent;
begin
  if Container <> nil then
  begin
    Componente := Container.Component;
    if Componente <> nil then
    begin
      if Componente is TdmkEdit then
        TdmkEdit(Componente).ValueVariant := Valor
      else
        Geral.MB_Aviso('Componente n�o implementado na fun��o:' + sLineBreak +
        '"TUnMyObjects.DefineValDeCompoEmbeded()"');
    end;
  end;
end;

{$IFNDEF NAO_USA_DB}
procedure TUnMyObjects.DefParams(Acao: Integer);
begin
  if Acao = 0 then
  begin
    MAR_GOTOTABELA      := VAR_GOTOTABELA      ;
    MAR_GOTOMySQLTABLE  := VAR_GOTOMySQLTABLE  ;
    MAR_GOTONEG         := VAR_GOTONEG         ;
    MAR_GOTOCAMPO       := VAR_GOTOCAMPO       ;
    MAR_GOTONOME        := VAR_GOTONOME        ;
    MAR_GOTOMySQLDBNAME := VAR_GOTOMySQLDBNAME ;
    MAR_GOTOVAR         := VAR_GOTOVAR         ;
    //
    //MAR_SQLy.Clear;
    MAR_SQLx.Clear;
    MAR_SQL1.Clear;
    MAR_SQLa.Clear;
    //MAR_SQLy.Text := VAR_SQLy.Text;
    MAR_SQLx.Text := VAR_SQLx.Text;
    MAR_SQL1.Text := VAR_SQL1.Text;
    MAR_SQLa.Text := VAR_SQLa.Text;
  end else begin
    VAR_GOTOTABELA      := MAR_GOTOTABELA      ;
    VAR_GOTOMySQLTABLE  := MAR_GOTOMySQLTABLE  ;
    VAR_GOTONEG         := MAR_GOTONEG         ;
    VAR_GOTOCAMPO       := MAR_GOTOCAMPO       ;
    VAR_GOTONOME        := MAR_GOTONOME        ;
    VAR_GOTOMySQLDBNAME := MAR_GOTOMySQLDBNAME ;
    VAR_GOTOVAR         := MAR_GOTOVAR         ;
    //
    //VAR_SQLy.Clear;
    VAR_SQLx.Clear;
    VAR_SQL1.Clear;
    VAR_SQLa.Clear;
    //VAR_SQLy.Text := MAR_SQLy.Text;
    VAR_SQLx.Text := MAR_SQLx.Text;
    VAR_SQL1.Text := MAR_SQL1.Text;
    VAR_SQLa.Text := MAR_SQLa.Text;
  end;
end;
{$ENDIF}

{ TContador }

destructor TContador.Destroy;
begin
  FLabel1 := nil;
  FLabel2 := nil;
  inherited;
end;

procedure TUnMyObjects.DoRestore(Sender: TObject);
begin
  Application.OnRestore := OldRestore;
  SetForegroundWindow(Application.Handle);
end;

procedure TContador.Execute;
var
  Ini: TTime;
  Tempo: Integer;
  Para: Boolean;
begin
  Para := False;
  Tempo := 0;
  Ini := Now();
  MyObjects.Informa2(FLabel1, FLabel2, FAguarde, FTexto, True);
  while (FMilisegundos >= Tempo) and not Para and not Terminated do
  begin
    Sleep(100);
    Tempo := Trunc((Now() - Ini) * 84600000);
    //Para := not MyObjects.Informa2(FLabel1, FLabel2, FAguarde, FTexto + '  ' + Geral.FF0(Tempo));
    //
    Application.ProcessMessages();
  end;
  if (FLabel1 <> nil) or (FLabel2 <> nil) then
    MyObjects.Informa2(FLabel1, FLabel2, False, '...', True);
end;

constructor TContador.Create(const LaAviso1, LaAviso2: TLabel;
Aguarde: Boolean; Texto: String; Milisegundos: Integer);
begin
  inherited Create(True); { Chama o contrutor herdado. Ele ir� temporariamente colocar o thread em estado de espera para depois execut�-lo. }
  FLabel1       := LaAviso1;
  FLabel2       := LaAviso2;
  FAguarde      := Aguarde;
  FTexto        := Texto;
  FMilisegundos := Milisegundos;
  //
  FreeOnTerminate := True; // Libera o objeto ap�s terminar.
  Priority := TpLower; { Configura sua prioridade na lista de processos do Sistema operacional. }
{$IFDEF DELPHI12_UP}
  Start;
{$ELSE}
  Resume;
{$ENDIF}
end;

end.
