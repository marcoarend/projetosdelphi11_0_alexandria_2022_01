﻿// ini Delphi 28 Alexandria
//{$I dmk.inc}
// ini Delphi 28 Alexandria
unit UnDmkProcFunc;

interface

uses
  System.RTLConsts, DB, System.DateUtils, System.IOUtils, System.SysUtils,
  StdCtrls, ExtCtrls, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Math, Mask, Buttons, ComCtrls,
  Registry, Printers, CommCtrl, TypInfo, ComObj, ShlObj, RichEdit, ShellAPI,
  Consts, ActiveX, OleCtrls, SHDocVw, Variants, MaskUtils, IniFiles, URLMon,
  Wininet, WinSpool, WinSvc, PsAPI, TlHelp32, UnInternalConsts, ZCF2,
  XMLIntf, StrUtils, dmkGeral, UnDmkEnums, dmkCheckGroup, ResIntStrings,
 {$IFNDEF NAO_USA_UnitMD5} UnitMD5, {$ENDIF}
  UnMsgInt, IdBaseComponent, IdComponent, IdRawBase, IdRawClient, IdIcmpClient,
  IdUDPBase, IdUDPClient, IdSNTP,
  {$IfDef VER230} // XE2
  Web.Win.Sockets,
  {$Else}
  IdTCPConnection, IdTCPClient,
  {$EndIF}
  Vcl.CheckLst,  Vcl.Themes,
  (*&UnDmkListas,*) Rtti,
  IpHlpApi, IpTypes, Winapi.WinSock, Winapi.IpExport,
  //MD5 Novo 2020
  IdHashMessageDigest, UnProjGroup_Consts, UnGrl_Vars;

const
  Codes64 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/';
  CodeArq = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  ValidWord      : string = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ -.';
  // Gerar senhas
  pwdMaiusculas  : string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  pwdMinusculas  : string = 'abcdefghijklmnopqrstuvwxyz';
  pwdNumeros     : string = '1234567890';
  //
  PixelMax = 32768;

type
  TEXEVersionData = record
    CompanyName,
    FileDescription,
    FileVersion,
    InternalName,
    LegalCopyright,
    LegalTrademarks,
    OriginalFileName,
    ProductName,
    ProductVersion,
    Comments,
    PrivateBuild,
    SpecialBuild: string;
  end;

type
  //INI = Obtem versão do Windows (Compatível com Windows 10
  ListaItemQtde = record
    Item: Integer;
    Qtde: Double;
  end;
  //INI = Obtem versão do Windows (Compatível com Windows 10
  WKSTA_INFO_100 = record
    wki100_platform_id: DWORD;
    wki100_computername: LPWSTR;
    wki100_langroup: LPWSTR;
    wki100_ver_major: DWORD;
    wki100_ver_minor: DWORD;
  end;
  LPWKSTA_INFO_100 = ^WKSTA_INFO_100;
  //FIM = Obtem versão do Windows (Compatível com Windows 10
  pPixelArray = ^TPixelArray;
  TPixelArray = array[0..PixelMax-1] Of TRGBTriple;
  //
  TRGBArray = array[Word] of TRGBTriple;
  pRGBArray = ^TRGBArray;
  //
  ArrItemQtde = array of ListaItemQtde;
  TOSInfo = class(TObject)
  public
    class function IsWOW64: Boolean;
  end;
  TdmkTemaCores = (dmkcorXcel2k7Bom, dmkcorXcel2k7Neutra, dmkcorXcel2k7Saida,
    dmkcorXcel2k7Incorreto,
    dmkcorBrancoInvisivel, dmkcorPretoEBranco, dmkcorNotaVermelho,
    dmkcorEnfaseAzul01, dmkcorEnfaseAzul02, dmkcorEnfaseAzul03,
    dmkcorEnfaseAzul04, dmkcorEnfaseAzul05);
  TVaiPara = (vpFirst, vpPrior, vpNext, vpLast, vpThis);
  //
  MyArrInt = array of Integer;
  //
  MyArrayD02 =  array[01..02] of Real;
  MyArrayD04 =  array[01..04] of Real;
  MyArrayD05 =  array[01..05] of Real;
  MyArrayR05 =  array[00..04] of Real;
  MyArrayR07 =  array[00..06] of Real;
  //
  MyArrayX04 =  array[00..03] of Integer;
  MyArrayX05 =  array[00..04] of Integer;
  MyArrayX06 =  array[00..05] of Integer;
  MyArrayX10 =  array[00..10] of Integer;
  MyArrayI02 =  array[01..02] of Integer;
  MyArrayI03 =  array[01..03] of Integer;
  MyArrayI1k =  array[0..1024] of Integer;
  //
  MyArrayT02 =  array[01..02] of String;
  MyArrayT_3 =  array[00..02] of String;
  //
  MyArray07ArrayR03 = array[00..06] of array[00..02] of Real;
  //
  MyArray02ArrayTn = array[00..01] of array of String;
  TMyStrDynArr = array of String;
  //
  TTipoTroca = (trcOA, trcOsAs, trcPara);
  //
  PixArray = array [0..2] of Byte;
  ItensArray = array of Integer;
  ValrsArray = array of Double;
  //
  TUnDmkProcFunc = class(TObject)
  private
    { Private declarations }
    function  PortTCPIsOpen(dwPort : Word; ipAddressStr: AnsiString) : boolean;
    //INI = Obtem versão do Windows (Compatível com Windows 10
    function  GetProductVersion(const AFileName: string; var AMajor, AMinor, ABuild: Cardinal): Boolean;
    function  GetNetWkstaMajorMinor(var MajorVersion, MinorVersion: DWORD): Boolean;
    //FIM = Obtem versão do Windows (Compatível com Windows 10
  public
    { Public declarations }
    function  AcoesAntesDoFormPrincipal(ApplicationName, ApplicationTitle:
    String; LoginForm, SkinName: PWideChar): Boolean;
    //procedure AcoesDepoisDoFormPrincipal(): Boolean;
    procedure RetrieveLocalAdapterInformation(strings: TStrings);
    //  D A T A  /  H O R A
    procedure PeriodoDecode(const Periodo: Integer; var Ano, Mes: Word);
    procedure AnoMesDecode(const AnoMes: Integer; var Ano, Mes: Word);
    function  IncrementaAnoMes(AnoMes, Incremento: Integer): Integer;
    function  DateTime_MyTimeZoneToUTC(DataHora: TDateTime): TDateTime;
    function  DateTime_UTCToMyTimeZone(DataHora: TDateTime): TDateTime;
    function  DDS(Data: String): String;
    function  MensalToPeriodo(Mensal : String): Integer;
    function  PeriodoToMensal(Periodo: Integer): String;
    function  MezToMesEAno(Mez: Integer): String;
    function  MesEAnoToMes(MesEAno: String): Integer;
    function  MezToMesEAnoCurto(Mez: Integer): String;
    function  FDT_NULO(Data: TDateTime; Tipo: Integer): String;
    function  FDT_NFe_UTC(DataHora: TDateTime; TZD_UTC: Double): String;
    function  ValidaDataDeSQL(Texto: String; PermiteZero: Boolean): TDateTime;
    function  NomeDiaDaSemana(Dia: Integer; IniDom: Boolean = True): String; //
    function  NomeDiaDaSemana_Artigo(Dia: Integer; IniDom: Boolean = True): String;
    function  HorasMH(Minutos: Double; EmptyNull: Boolean): String;
    function  VerificaMes(Mes: Integer; Upper: Boolean; Sigla: Boolean = False): String;
    function  VerificaMesTxt(Mes: String): Integer;
    function  PeriodoImp1(DataI, DataF: TDateTime; CkI, CkF: Boolean;
              TextoI, Agregador, TextoF: String): String;
    function  PeriodoImp2(DataI, DataF: TDateTime; CkI, CkF: Boolean;
              TextoI, Agregador, TextoF: String): String;
    function  FormataTimeStamp(Texto: String) : TDateTime;
    function  PeriodoImp(Data1I, Data1F, Data2I, Data2F: TDateTime;
              Ck1I, Ck1F, Ck2I, Ck2F: Boolean; Texto1, Texto2: String): String;
    function  PeriodoImpMezAno(DataI, DataF: String; TextoI, Agregador,
              TextoF: String): String;
    function  TZD_UTC_FloatToSignedStr(TZD_UTC: Double): String;
    function  TZD_UTC_Define(Atual: Double): Double;
    procedure TZD_UTC_InfoHelp();
    function  ObtemDataStr(Date: TDateTime; MiliSegundos: Boolean): String;
    function  AnoDoPeriodo(Periodo: Integer): Integer;
    function  MesDoPeriodo(Periodo: Integer): Integer;
    function  MesEAnoDoPeriodo(Periodo: Integer): String;
    function  MesEAnoDoPeriodoLongo(Periodo: Integer): String;
    function  MesEAnoDePeriodoShort(Periodo: Integer; var Mes, Ano: Word): String;
    function  PeriodoEncode(Ano, Mes: Integer): Integer;
    function  CDS(Data: String; FormatoAtual, FormatoNovo: Integer): String;
    function  MezToFDT(Mez, AddDias, TipoFDT: Integer): String;
    function  DataJuliana_HSBC(Data: TDateTime): String;
    function  XFT(Numero: String; Casas: Integer; Sinal: TSinal) : String;
    function  CDS2(Data, BiFormatoAtual: String; FormatoNovo: Integer): String;
    function  PeriodoToAnoMes(Periodo: Integer): Integer;
    function  PrimeiroDiaDoPeriodo(Periodo: Integer; Tipo: TDataTipo): String;
    function  PrimeiroDiaDoPeriodo_Date(Periodo: Integer): TDateTime;
    function  UltimoDiaDoPeriodo(Periodo: Integer; Tipo: TDataTipo): String;
    function  UltimoDiaDoPeriodo_Date(Periodo: Integer): TDateTime;
    function  ObtemDataInserir(): TDateTime;
    function  IncrementaMeses(Data: TDateTime; Meses: Integer; SoUteis: Boolean):
              TDateTime;
    function  FDT_Null(Data: TDateTime; Tipo: Integer): Variant;
    function  DataToAnoMes(Data: TDateTime): Integer;
    function  DataToAnoEMes(Data: TDateTime): String;
    function  PrimeiroDiaAposPeriodo(Periodo: Integer; Tipo: TDataTipo): String;
    function  HorasMHT(Minutos: Integer; EmptyNull: Boolean): String;
    function  IntToTempo(Minutos: Integer): String;
    function  MTHT(Minutos: Integer; Formato: String): String;
    function  TTM(Hora: String): Integer;// texto para minutos
    function  HTH(Hora, Formato: String): String;
    function  HTT(Hora: TTime; Tamanho: Integer; UsaPonto: Boolean): String;
    function  THT(Hora: String): String;
    function  MinutosHT4(Hora: String): Integer;
    function  DatadeAnoMes(AnoMes, Dia, AddDias: Integer): TDateTime;
    function  CalculaIdade(DataNasc: TDateTime): Integer;
    function  TimeToMinutes(Time: TDateTime): Integer;
    function  MinutesToTime(Minutes: Integer): TDateTime;
    function  DateIsNull(Date: TDateTime): Boolean;
    function  DatasIniFimDoPeriodo(const AnoMes: Integer; var DtIni, DtFim:
              TDateTime): Boolean;
    function  DataZero(Data: TDateTime): Boolean;
    // N U M E R O S
    function  FFP(Numero: Double; Casas: Integer): String;
    function  IntToColTxt(Inteiro: Integer): String;
    function  ColTxtToInt(ColTxt: String): Integer;
    function  IntToCODIF_Receita(Numero: Integer): String;
    function  CODIFToInt_Receita(Codif: String): Integer;
    procedure SortIntArray(var IntArray: array of integer);
    function  MenorFloat(Valores: array of Double): Double;
    function  Divide2(Dividendo, Divisor: Double): Double;
    function  CalculaRendimento(ValIni, ValFim: Double): Double;
    function  IntInConjunto2(Numero, Conjunto: Integer): Boolean;
    function  IntInConjunto2Def(Numero, Conjunto, DefTrue, DefFalse: Integer): Integer;
    function  ITS_Null(Inteiro: Integer): String;
    function  ITS_Zero(Inteiro: Integer): String;
    function  ITB(Valor: Integer): Boolean;
    function  EhNumeroOuSinalOuSeparador(Caracter: Word): Boolean;
    function  BoolToInt2(Verdade: Boolean; ResultTrue, ResultFalse: Integer): Integer;
    function  BoolToIntDeVarios(Verdade: array of Boolean; Resultado:
              array of Integer; Default: Integer): Integer;
    //function  V_FToInt(V_F: AnsiChar): Integer; override;
    function  V_FToInt(V_F: WideChar): Integer;
    function  FFD(Numero: Double; Digitos: Integer; Sinal: TSinal) : String;
    function  I64MV(Texto: String) : Int64;
    function  DecodeDocumento(const Documento: String; var Serie: String;
              var Cheque: Double): String;
    function  FFT_Ponto1(Numero: Double) : String;
    function  XLSColCharToColNum(Coluna: String): Integer;
    function  SoNumeroValor_TT(Texto : String) : String;
    function  SoNumeroELetraESeparadores_TT(Texto : String) : String;
    function  SoNumeroEExtra_TT(Texto: String; Extra: Char;
              Tudo: Boolean): String;
    function  FormataCasas(Casas: Integer): String;
    function  StrFmt_Double(Casas: Integer): String;
    function  FFF(Valor: Double; Casas: Integer; Sinal: TSinal): Double;
    function  ObtemTipoDocTxtDeCPFCNPJ(CPFCNPJ: String): String;
    function  IntToBool_SoPositivo(Numero: Integer): Boolean;
    function  FloatToBool(Numero: Double; Casas: Integer): Boolean;
    function  NumTex(Text: String): Integer;
    function  SetArrayOfInt(var Arr: TArrSelIdxInt1;
              const Inteiros: array of Integer; const Zera: Boolean): Integer;
    function  PositFlu(Valor: Double): Double;
    function  HexToInt(s: string): Int64;
    function  VP(Valor: Double): Double; // Valor Positivo
    procedure IncrementaListaDeInteiros(var Lista: TMyGrlArrInt; const Inteiro:
              Integer);
    function  DesvioPadraoPopulacionalLista(const n: array of Double;
              var RangeMin, RangeMid, RangeMax: Double): Double;
    function  DesvioPadraoPopulacionalArrIntDbl(const Itens: ItensArray; const
              Valores: ValrsArray; var RangeMin, RangeMid, RangeMax: Double): Double;
    function  MinArrFlu(Valores: Array of Double): Double;
    function  Dmk_MonthsBetween(const ANow, AThen: TDateTime): Double;


    // T E X T O
    function  ParseText(Texto: AnsiString; Decode: Boolean = True;
              IsUTF8: Boolean = True ): AnsiString; // 2012-09-29
    function  ConverteMinuscEmMaiusc(Texto: String): String;
    function  LimpaTexto(Texto: String): String;
    function  DuplicaBarras(Atual: String): String;
    function  SimplificaBarras(Atual: String): String;
    function  DuplicaAspasDuplas(Atual: String): String;
    function  DuplicaSlash(Path: String): String;
    function  EncodeBase64(const inStr: string): string;
    function  ExtensoMil(Numero : String) : String;
    function  ExtensoMoney(Numero : String) : String;
    function  ExtensoFloat(Numero: String) : String;  // Flutuante (Double)
    function  AdicionaNoDiretorio(Diretorio, ItemAdicionar: String): String;
    function  InverteBarras(Atual: String): String;
    procedure LeMeuTexto(Texto: String; Titulo: String = 'Aviso do programa');
    procedure LeTexto_Permanente(Texto, Titulo: String);
    function  MudaExtensaoDeArquivo(Arq, Ext: String): String;
    function  RemoveExtensaoDeArquivo(Arq: String): String;
    function  NomeLongoparaCurto(NomeLongo: AnsiString): AnsiString;
    function  ExtractUrlFileName(URL: String): String;
    function  PWDExDecode(Data, SecurityString: string): string;
    function  PWDExEncode(Data, SecurityString: string; MinV: Integer = 0;
              MaxV: Integer = 5): string;
    function  PWDGenerateSecutityString: string;
    function  SeparaNumeroDaDireita_TT_Dot2(const Texto: String;
              var Num: Double; var Txt: String): Boolean;
    function  VerificaIPSintaxe(IP: String; Avisa: Boolean): Boolean;
    function  VerificaIPsMesmaRede(IP, Destino: String): Boolean;
    function  SQL_IntervInt(Texto: String; Ini,Fim: Largeint;
              CkIni, CkFim: Boolean): String;
    function  SQL_Periodo(Texto: String; Ini,Fim: TDateTime;
              CkIni, CkFim: Boolean): String;
{
    function  SQL_PerioCorrApo(WhereOrAnd: TWhereOrAnd; FldDtNormal,
              FldDtCorrApo: String; Ini,Fim: TDateTime): String;
}
    function  SQL_PeriodoComplexo(Texto, FldI, FldF: String; Ini,Fim: TDateTime;
              IniInn, EndInn, EndPos, IniOpn: Boolean): String;
    function  SQL_DataMenMai(Texto: String; Data: TDateTime;
              CkIni: Boolean): String;
    function  LoadFileToText(const FileName: TFileName): string;
    function  ScanFile(const FileName, forString: string;
              CaseSensitive: Boolean): Longint;
    function  VeSeEhDirDmk(Raiz, Pasta: String; Inverte: Boolean): String;
    function  IgnoraLocalhost(Host: String): String;
    function  ParcelaFatura(Parcela, Tipo: Integer): String;
    function  ArrayToTexto(Campo, NO_Campo: String; PosVirgula:
              TPosVirgula; IniZero: Boolean; MyArr: array of String;
              SomaIni: Integer = 0): String;
    function  SQL_ELT(Campo, NO_Campo: String; PosVirgula:
              TPosVirgula; IniZero: Boolean; MyArr: array of String;
              SomaIni: Integer = 0): String;
    function  ArrayToTextoCASEWHEN(Campo, NO_Campo: String; PosVirgula:
              TPosVirgula; MyArrStr: array of String; MyArrInt:
              array of Integer): String;
    function  ELT_FIND_IN_SET(Campo, NO_Campo: String; PosVirgula:
              TPosVirgula; IniZero: Boolean; MyIntArr: array of Integer;
              MyStrArr: array of String): String;
    function  CaminhoArquivo(Diretorio, Arquivo, Extensao: String): String;
    function  TxtGrandezaPeriodo(Grandeza: Integer; Plural: Boolean): String;
    function  FormaDeTratamentoBR(Val: Integer; Tipo: TTipoTroca): String;
    procedure AjustaCaracterInicial(const Xar: Char; const Sorc: String;
              var Dest: String);
    procedure AjustaCaracterFinal(const Xar: Char; const Sorc: String;
              var Dest: String);
    function  LoadArquivoToText(const Arquivo: String;
              var Texto: String): Boolean;
    function  IntToSimNao(Numero: Integer): String;
    function  BoolToSimNao(Verdade: Boolean): String;
    function  BoolToV_F(Verdade: Boolean): Char;
    procedure GravaEmLog(Arquivo, Msg: String);
    function  DefineJuricoSigla(Status: Integer): String;
    function  DefineJuricoTexto(Status: Integer): String;
    function  SQL_Mensal(Texto, Ini, Fim: String; NaoMensais: Boolean): String;
    function  SQL_Mensal_2(Texto, Ini, Fim: String; NaoMensais: Boolean): String;
    function  NomeTabelaLtc(Empresa: Integer; Letra: TTipoTab): String;
    function  SQL_DeLista(Lista: array of String; MaxItem: Integer): String;
    procedure AddTxtToArr2(var Arr1, Arr2: TMyStrDynArr; const Txt1, Txt2: String);
    function  ParValueCodTxt(Compl: String; Nome: String; Codigo: Variant;
              Todos: String = 'TODOS'): String;
    function  TipoDeCarteiraCashier(Tipo: Integer; Plural: Boolean): String;
    function  TipoDeCarteiraCashierSub(Tipo, PagRec: Integer; Plural: Boolean): String;
    function  TipoEPagRecDeCarteira(Tipo, PagRec: Integer): Integer;
    function  XMLEncode(Data: String): String;
    function  XMLDecode(Data: String): String;
    function  SeparaCoordenadas(const Coord: String; var Latitude, Longitude:
              Double): Boolean;
    function  CarregaArquivo(const Arquivo: String; var Texto: String): Boolean;
    function  ObtemNomeArqRandom(Tamanho: Integer): String;
    function  NumeroToLetras(Numero: Integer): String;
    function  ValidaPalavra(Txt: string; ConverteParaMaiusculas: Boolean = True): string;
    function  CheckCPFCNPJ(Entidade, Tipo: Integer; Nome, CPFCNPJ: String): Boolean;
    function  Duplicata(Texto: String): MyArrayT_3;
    function  DuplicataIncrementa(Texto: String; Incremento: Integer): String;
    function  FCE(Coluna: String): String;
    function  GetNomeTipoCart(TipoCart: Integer; DoDa: Boolean): String;
    function  SQL_Competencia_Mensal(Texto: String; Ini,Fim: Variant;
              CkIni, CkFim: Boolean): String;
    function  SQL_CodTxt(Texto: String; Codigo: String;  //RetornaTextoVazio por
              Zero, Negativo, RetornaTextoVazio: Boolean): String;
    function  ContinuarInserindo(SQLType: TSQLType): Boolean;
    procedure ExcluiSQLsDermatek();
    function  TxtUH(): String;
    function  EditDistance_Levenshtein(s, t: string): Integer;
    function  CampoReportTxt(Texto, TextoIfEmpty: String): String;
    function  FretePor_Txt(Quem: Integer): String;
    function  NomeMoeda(Moeda: Integer): String;
    function  SoTextoLayout(Texto: String) : String;
    function  VariavelToTexto(Formato: String; Variavel: Variant): String;
    function  GeraStringRandomica(Tamanho: Integer): string;
    function  SQL_ExplodeInBetween(const WHR_AND, Campo, Texto: String; var SQL_Res:
              String): Boolean;
    function  MontaDuplicata(FatNum, nNF, Parcela, tpFaturaNum, tpFaturaSeq:
              Integer; Prefixo, Separador: String; ApenasFatura: Boolean =
              False): String;
    function  PosLast(const SubStr, S: AnsiString): Integer;
    function  TextoDeLista(Lista: array of String; Indice: Integer): String;

    function  GeraPassword(UsaMinusculas, UsaMaiusculas, UsaNumeros: Boolean;
              TamanhoSenha: Integer): String;
    function  MyAnsiToUtf8(x: ansistring): string;
    function  VariantToString(Variant: Variant): String;
    function  TextoToHTMLUnicode(Str: string): string;
    function  ConcatTxtChkGroup(Conjunto, MaxItemsLista: Integer; Lista:
              array of String): String;
    function  IndexOfArrStr(Str: String; ArrStr: array of String): Integer;
    function  ValidaDiretorio(Diretorio: String; BarraNoFinal: Boolean): String;
    function  SQLStringReplaceFldByValFld(SQL, Campo: String; Valor: Variant): String;
    function  ComparaTextosComMascara(Txt1, Txt2: String; CharCase: Boolean): Boolean;
    function  LimpaSeSohCRLF(Texto: String): String;
    function  GeraSigla(Texto: String; MaxTam: Integer): String;
    //function  TrocaCaracterEspecial(aTexto : string; aLimExt : boolean) : string;
    function  TrocaCaracterEspecial(aTexto: String; aAcentos, aLimExt: Boolean):
              String;
    function  SQLNumRange(WHR_AND, GGXs: String): String;

    // I M A G E N S
    (*
    Movido para UnDmkImg
    // Bitmap
    procedure ResizeImage_Bitmap(var BitMap: TBitmap; const MaxWidth, MaxHeight: Integer);
    procedure ResizeImage_Bitmap_Novo(var BitMap: TBitmap; const MaxWidth, MaxHeight: Integer);
    procedure RotateBitmap_ads(SourceBitmap: TBitmap; out DestBitmap: TBitmap;
              Center: TPoint; Angle: Double) ;
    procedure SmoothResize(var Src, Dst: TBitmap);
    function  TColorToHex(Cor: TColor): String;
    function  HexToTColor(Cor: String): TColor;
    // Não testado!!!  { TODO : TESTAR }
    procedure Antialiasing(Bitmap: TBitmap; Rect: TRect; Percent: Integer);
    // Não testado!!!  { TODO : TESTAR }
    procedure DrawAntialisedLine(Canvas: TCanvas; const AX1, AY1, AX2, AY2: real; const LineColor: TColor);
    *)
    function  NaoQueroLocalizarAsImagens(Localizar: Boolean): Boolean;

    // C A L C U L O S
    function  CoordenadasIniciais(LeftCell, TopCell, RightCell, BottomCell,
              WidthImage, HeigthImage: Integer): MyArrayI02;
    function  ObtemInt32Random(): Integer;
    function  CalculaJuroComposto(TaxaM: Double; Prazo: Double): Double;
    function  CalculaJuroSimples(TaxaM: Double; Prazo: Double): Double;
    function  PorcentagemMes(): Double;
    function  DigitoVerificardorDmk3CasasInteger(Numero: Integer): Integer;
    function  CheckSumEAN13(CodEAN13: String; Avisa: Boolean): Boolean;

    // D I R E T Ó R I O S
    function  GetAllFiles(Subdiretorios: Boolean; Mask: string; ListBox: TCustomListBox;
              ClearListBox: Boolean): Integer;
    function  ObterCaminhoMeusDocumentos(): String;
    function  ObtemDermatekDiretorio(): String;

    // A R Q U I V O S
    function  RenomearArquivo(Arquivo, NomeNovo: String): Boolean;
    procedure MoverArquivo(OrigArquivo, DestArquivo: String);

    // D A T A B A S E
    function  DataTypeToString(Campo: TField): String;
    function  Table_FormataCampo(Campo: TField; Valor: String): Variant;

    // E M A I L
    function ObtemNomeEmailTipo(TipoEmail: TMaiEnvType): String;

    //  G E R A L
    function  ObtemValorDouble(var Valor: Double; const Casas: Integer): Boolean;
    procedure ConfigIniApp(UsaCoresRel: Integer);
    procedure CorFundoEFonte(const Tema: TdmkTemaCores; var CorFundo, CorFonte:
              TColor; const Inverte: Boolean);
    procedure AcoesAntesDeIniciarApp_dmk();
    function  Criptografia(mStr, mChave: string): string;
    function  HexCripto(mStr, mChave: string): string;
    function  HexDecripto(mStr, mChave: String): String;
    function  CalculaCor(Val, GG, GR, RG, RR, RB, BR: Double): TColor;

    // E S C O L H A S ( EscolheDe2
    function  EscolhaDe2Int(Verdade: Boolean; OpcaoV, OpcaoF: Integer): Integer;
    function  EscolhaDe2Str(Verdade: Boolean; OpcaoV, OpcaoF: String): String;
    function  EscolhaDe2Dta(Verdade: Boolean; OpcaoV, OpcaoF: TDateTime): TDateTime;
    function  EscolhaDe3Int(Val1, Val2: Double; Maior, Igual, Menor: Integer): Integer;

    // W I N D O W S
    function  DelDir(dir: string): Boolean;
    function  FecharOutraAplicacao(Nome: String; Handle: THandle): Integer;
    function  GetDiskSerialNumber(DriveLatter:String) : DWord;
    function  VersaoWindows(): MyArrayI03;
    function  IsWin10(): Boolean;
    function  IsWin81(): Boolean;
    function  IsWin8(): Boolean;
    function  IsWin7(): Boolean;
    function  IsWinVista(): Boolean;
    function  IsWinXP(): Boolean;
    function  IsWin2k(): Boolean;
    function  IsWinNT4(): Boolean;
    function  IsWinNT(): Boolean;
    function  IsWin3x(): Boolean;
    function  SistemaOperacional: Integer;
    function  ObtemSysInfo(): String;
    function  ObtemUsuarioCorrente_SistemaOperacional(): String; //Só funciona no Delphi XE10
    function  ObtemSysInfo2(Separador: String = ''): String;
    function  SistemaOperacional_txt: String;
    procedure SistemaOperacional2_txt(var OSVersao, OSArquitetura: String);
    function  RunningProcessesList(const List: TStrings; FullPath: Boolean): Boolean;
    function  GetProcessNameFromWnd(Wnd: HWND): string;
    function  ExecutavelEstaRodando(Executavel: String): Boolean;
    function  ServicoWindowsEstaRodando(sMachine, sService: PChar): Boolean;
    function  ExecutavelEstaInstalado(Descricao: String): Boolean;
    function  IsConnectedToNet(HostIP: AnsiString; HostPort, CancelTimeMs: Word;
              FirstOctet: Byte; PError: PChar): Boolean;
    //function TestePorta(): LongWord;
    function  FileAgeCreate(const FileName: string): Integer;
    function  IsFileInUse(FileName : string) : boolean;
    function  AlturaDaBarraDeTarefas(): Integer;
    function  TestaPortaPorHost(Port: Word; Host: String): Boolean;
    function  TestaPortaPorIP(Port: Word; IP: String; Memo: TMemo): Boolean;
    function  WinExecAndWaitOrNot(LinhaDeComando: string; Visibility: Integer;
              Memo: TMemo; Wait: Boolean): Longword;
    function  OSArchitectureToStr(const a: TOSVersion.TArchitecture): string;
    function  VerificarDllRegistrada(var Res: String; const NomeLib: String): Boolean;
    function  ObtemDiretorioDesktop(AllUsers: Boolean): String;
    procedure SaveTextToFile(const FileName: TFileName; const Content: string);
    function  TamanhoDeArquivo(fileName : wideString): Int64;
    function  RemoveArquivosDeDiretorio(Diretorio: String; ApenasArquivos: Boolean = False): Boolean;
    function  RenameAppOlder(AppName: String): Boolean;
    procedure RenameAppOlder_Cancel;
    procedure CopiaArq(const FileName, DestName: string);
    procedure MoveArq(const FileName, DestName: string);
    function  ObtemInfoRegEdit(Key, NomeForm: String; Default: Variant; KeyType:
              TKeyType): Variant;
    procedure SalvaInfoRegEdit(Key, NomeForm: String; Value: Variant; KeyType:
              TKeyType);
    function  MoveDir(const fromDir, toDir: string;
              Sobrescrever: Boolean = False): Boolean;
    procedure ObtemListaDeDrivesAtivos(Lista: TStrings);
    procedure MostraCalculadoraDoWindows(Sender: TObject);
    function  ObtemEspacoLivreDrive(ListaDrives: TStringList; Driver: String ): Int64;
    function  ObtemEspacoTotalDrive(ListaDrives: TStringList; Driver: String ): Int64;
    function  ObtemTempoInativoDoAplicativo(): Word;
    procedure TrimAppMemorySize(); // LimpaMemoria - Limpa memoria residual
    function  TerminarProcesso(sFile: String): Boolean;

    function  ExecuteProcess((*const FileName, Params: string; Folder: string;
              WaitUntilTerminated, WaitUntilIdle, RunMinimized: boolean;
              var ErrorCode: integer): boolean;*)
              const FileName, Params: string;
              Folder: string; WaitUntilTerminated, WaitUntilIdle(*, RunMinimized*): Boolean;
              EstadoJanela: Integer;
              var ErrorCode: integer): boolean;
    function  EstadoJanelaToHWND_CmdShow(EstadoJanela: Integer): Integer;
    function  HWND_CmdShowToEstadoJanela(HWND_CmdShow: Integer): Integer;
    function  ConfereVersaoApp(Informa: Boolean): Boolean;
    function  GetEXEVersionData(const FileName: string): TEXEVersionData;

    // M E N S A G E N S
    procedure InfoKIND_DEPTO(Procedimento: String);
    (* 2017-10-14 => Movido para UnGrl_Geral
    function  ObtemSiglaModulo(Modulo: TDmkModuloApp): String;
    procedure InfoSemModulo(Modulo: TDmkModuloApp);
    *)
    function  MessageDlgCheck(Msg: string; AType: TMsgDlgType;
              AButtons: TMsgDlgButtons; IndiceHelp: LongInt; DefButton:
              TModalResult; Portugues: Boolean; Checar:Boolean; MsgCheck:
              string; Funcao: TProcedure): Word;

    // X M L
    function  ObtemValorDeTag(const Tag: String; var Texto, Valor: String): Boolean;
    function  ObtemValorDeTag2(Texto, Chave: String; MantemChave:
              Boolean): String;
    function  SeparaDadosCampoDeXML(Texto, Chave: String; MantemChave:
              Boolean = False): String;
    function  XMLValBol(Noh: IXMLNode): Boolean;
    function  XMLValDta(Noh: IXMLNode): String;
    function  XMLValDth(Noh: IXMLNode): String;
    function  XMLValTZD(const Noh: IXMLNode; var TZD: Double): String;
    function  XMLValHra(Noh: IXMLNode): String;
    function  XMLValZzz(Noh: IXMLNode): TTime;
    function  XMLValFlu(Noh: IXMLNode): Double;
    function  XMLValInt(Noh: IXMLNode): Integer;
    function  XMLValI64(Noh: IXMLNode): Int64;
    function  XMLValTDt(Noh: IXMLNode): TDateTime;
    function  XMLValTxt(Noh: IXMLNode): WideString;
    function  ConverteANSItoUTF8(Texto: String): String;
{
    function  ValidaTexto_XML(Txt, Codigo, ID: String): String;
    function  CharToMyXML_Enviar(Codigo: Integer): String;
}
    //
    function  HRDT_PeriodoEIntervalos(PerioDd, IntervDd: Integer;
              MsgWhenEmpty: String): String;
    function  HRDT_Periodo(PerioDd: Integer): String;
    function  HRDT_Intervalo(IntervDd: Integer): String;
    function  HRDT_DefinePorDivisao(Intervalo, SubPeriodo: Integer;
              TxtSingular, TxtPlural: String; var Resultado: String): Boolean;
    // I D E
    //procedure DiretivasCompilador();
    //
    function  FatIDProibidoEmLct(FatID: Integer): Boolean;
{$IFNDEF NAO_USA_UnitMD5}
    function  CalculaValSerialKey2(DataVerifi: String; Web: Boolean): String;

    function  CalculaValSerialNum2(VctoLicen, CNPJ: String; CodAplic,
                StatAplic: Integer; Web: Boolean): String;
{$ENDIF}
    function  CalculaValSerialKey_VendaApp(const CNPJ_CPF: String; var SerialHD,
              CPUID: String): String;
    // F I N A N C A S
    function  NomeDoNivelSelecionado(Tipo, Nivel: Integer): String;
    function  NivelSelecionado(Nivel, Tipo: Integer;
              Compl_Pre, Compl_Pos: String): String;
    // LISTAS
    procedure AddLin(var Res: MyArrayLista; var Linha: Integer;
              const Codigo: String; Descricao: array of String);
    function ListaIndicadorDeIE(): MyArrayLista;
    // O U T R O S
    function  ObterDataHoraOficialDeBrasiliaInternet: TDateTime;
    function  ConfiguraIconeAplicativo(AllUsers: Boolean;
              Pergunta: Boolean = True; NomeApp: String = ''): Boolean;
    function  GetFileList(const Path: String; const Ext: array of String): TStringList;
    function  GetVersion(sFileName: String; ForcaTrezeCarac: Boolean = False): String;
    function  EhControlF(Sender: TObject; var Key: Word; Shift: TShiftState):
              Boolean;
    procedure AdicionaRegraPortaFirewall(const Nome, Executavel: String;
              Porta: Word; RegraEntrada: Boolean);
    function  ScheduleRunAtStartup(const ATaskName, AFileName, AUserAccount,
              AUserPassword: string): Integer;
    procedure UnScheduleRunAtStartup(const ATaskName: String);
    // C H E C K L I S T B O X
    function  CheckedItemsCount(Lista: TCheckListBox): Integer;
    // D M K C H E C K G R O U P
    procedure CheckGroupTodos(CheckGroup: TdmkCheckGroup; Seleciona: Boolean);
    // D M K   D E L E T E
    function  MotivDel_ValidaCodigo(Motivo: Integer): Integer;
    function  MotivDel_ObtemDescricao(Motivo: Integer): String;
    // Novos
    function  NomeSimplesValido(const Nome: String): Boolean;
  end;

var
  dmkPF: TUnDmkProcFunc;
  WaitTimeMs: WORD;
  InitialTick, DifTick: DWORD;

const
  RsSystemIdleProcess = 'System Idle Process';
  RsSystemProcess = 'System Process';

function RtlIpv4AddressToString(const Addr: PInAddr; S: PChar): PChar; stdcall;
external 'Ntdll.dll' name {$IFDEF UNICODE}'RtlIpv4AddressToStringW'{$ELSE}'RtlIpv4AddressToStringA'{$ENDIF};
function RtlIpv6AddressToString(const Addr: PIn6Addr; S: PChar): PChar; stdcall;
external 'Ntdll.dll' name {$IFDEF UNICODE}'RtlIpv6AddressToStringW'{$ELSE}'RtlIpv6AddressToStringA'{$ENDIF};

implementation

{ TOSInfo }

class function TOSInfo.IsWOW64: Boolean;
type
  TIsWow64Process = function(
    Handle: THandle;
    var Res: BOOL
  ): BOOL; stdcall;
var
  IsWow64Result: BOOL;
  IsWow64Process: TIsWow64Process;
begin
  IsWow64Process := GetProcAddress(
    GetModuleHandle('kernel32'), 'IsWow64Process'
  );
  if Assigned(IsWow64Process) then
  begin
    if not IsWow64Process(GetCurrentProcess, IsWow64Result) then
      raise Exception.Create('Bad process handle');
    Result := IsWow64Result;
  end
  else
    Result := False;
end;

{ TUnDmkProcFunc }

// '/', '\'
procedure TUnDmkProcFunc.AcoesAntesDeIniciarApp_dmk();
begin
  //DiretivasCompilador();
end;

function TUnDmkProcFunc.AcoesAntesDoFormPrincipal(ApplicationName,
ApplicationTitle: String; LoginForm, SkinName: PWideChar): Boolean;
  function GetStyleCU(Default: String): String;
  begin
    Result := Geral.ReadAppKeyCU('StyleName', Application.Title, ktString, Default);
  end;
var
  Form : PChar;
  Estilo: String;
begin
  Form := PChar(ApplicationTitle);
  Application.MainFormOnTaskbar := True;
  if OpenMutex(MUTEX_ALL_ACCESS, False, Form) = 0 then
  begin
    Result := True;
    CreateMutex(nil, False, Form);
    Application.Initialize;
    if ApplicationName <> EmptyStr then
      Application.Name := Application.Name; // 'BlueDer2';
    if ApplicationTitle <> EmptyStr then
      Application.Title := ApplicationTitle; // 'Blue Derm';
    // ivMsgVersaoDifere
  // ivMsgVersaoDifere
    if CO_VERMCW > CO_VERMLA then  // mover de MyListas para UnProjGroup_Consts !!!
      CO_VERSAO := CO_VERMCW
    else
      CO_VERSAO := CO_VERMLA;
    {$IfDef VER350}
    if CO_VERM28 > CO_VERSAO then
      CO_VERSAO := CO_VERM28;
    {$EndIf}
    //
    Estilo := GetStyleCU(SkinName);
    TStyleManager.TrySetStyle(Estilo);
  end else
{
  except
      Application.Terminate;
      Exit;
    end;
    Application.Run;
  end
  else}
  begin
    Result := False;
    Geral.MB_Aviso('O Aplicativo já esta em uso.');
    SetForeGroundWindow(FindWindow(LoginForm, Form));
  end;
end;

procedure TUnDmkProcFunc.AddLin(var Res: MyArrayLista; var Linha: Integer;
  const Codigo: String; Descricao: array of String);
var
  i: Integer;
begin
  SetLength(Res, Linha+1);
  SetLength(Res[Linha], 2);
  Res[Linha][0] :=  Codigo;
  Res[Linha][1] :=  '';
  for i := Low(Descricao) to High(Descricao) do
    Res[Linha][1] :=  Res[Linha][1] + Descricao[i];
  inc(Linha, 1);
  //
end;

procedure TUnDmkProcFunc.CheckGroupTodos(CheckGroup: TdmkCheckGroup; Seleciona: Boolean);
var
  I: Integer;
begin
  if CheckGroup.Items.Count = 0 then
    Exit;
  //
  for I := 0 to CheckGroup.Items.Count - 1 do
    CheckGroup.Checked[I] := Seleciona;
end;

function TUnDmkProcFunc.ObterCaminhoMeusDocumentos: String;
var
   r: BOOL;
   Caminho: array[0..Max_Path] of Char;
begin
    r := ShGetSpecialFolderPath(0, Caminho, CSIDL_Personal, False) ;
    if not r then
       raise Exception.Create('Não foi possível encontrar a localização da Pasta "Meus Documentos".') ;
    Result := Caminho;
end;

function TUnDmkProcFunc.ObterDataHoraOficialDeBrasiliaInternet: TDateTime;
var
  IDSntp: TIDSntp;
begin
  IDSntp := TIDSntp.Create(nil);
  try
    IDSntp.Host := 'pool.ntp.br';
    Result := IDSntp.DateTime;
  finally
    IDSntp.Free;
  end;
end;

procedure TUnDmkProcFunc.AddTxtToArr2(var Arr1, Arr2: TMyStrDynArr;
  const Txt1, Txt2: String);
var
  p: Integer;
begin
  p := Length(Arr1);
  //
  SetLength(Arr1, p + 1);
  Arr1[p] := Txt1;
  //
  SetLength(Arr2, p + 1);
  Arr2[p] := Txt2;
  //
end;

procedure TUnDmkProcFunc.AdicionaRegraPortaFirewall(const Nome,
  Executavel: String; Porta: Word; RegraEntrada: Boolean);
const
  NET_FW_PROFILE2_DOMAIN = 1;
  NET_FW_PROFILE2_PRIVATE = 2;
  NET_FW_PROFILE2_PUBLIC = 4;
  NET_FW_IP_PROTOCOL_TCP = 6;
  NET_FW_ACTION_ALLOW = 1;
  NET_FW_RULE_DIR_IN = 1;
  NET_FW_RULE_DIR_OUT = 2;
var
  fwPolicy2, RulesObject, NewRule: OleVariant;
  Profile: Integer;
begin
  Profile                 := NET_FW_PROFILE2_PRIVATE OR NET_FW_PROFILE2_PUBLIC;
  fwPolicy2               := CreateOleObject('HNetCfg.FwPolicy2');
  RulesObject             := fwPolicy2.Rules;
  NewRule                 := CreateOleObject('HNetCfg.FWRule');
  NewRule.Name            := Nome;
  NewRule.Description     := Nome;
  NewRule.Applicationname := Executavel;
  NewRule.Protocol        := NET_FW_IP_PROTOCOL_TCP;
  NewRule.LocalPorts      := Porta;
  NewRule.Enabled         := True;
  NewRule.Profiles        := Profile;
  NewRule.Action          := NET_FW_ACTION_ALLOW;
  //
  if RegraEntrada then
    NewRule.Direction := NET_FW_RULE_DIR_IN
  else
    NewRule.Direction := NET_FW_RULE_DIR_OUT;
  //
  RulesObject.Add(NewRule);
end;

procedure TUnDmkProcFunc.AjustaCaracterFinal(const Xar: Char;
  const Sorc: String; var Dest: String);
begin
  Dest := Sorc;
  if Length(Dest) = 0 then
    Dest := Xar
  else begin
    if Dest[Length(Dest)] <> Xar then
      Dest := Dest + Xar;
  end;
end;

procedure TUnDmkProcFunc.AjustaCaracterInicial(const Xar: Char;
  const Sorc: String; var Dest: String);
begin
  Dest := Sorc;
  if Length(Dest) = 0 then
    Dest := Xar
  else begin
    if Dest[1] <> Xar then
      Dest := Xar + Dest;
  end;
end;

function TUnDmkProcFunc.AlturaDaBarraDeTarefas(): Integer;
var
  TaskBarH: THandle;
  TaskBarR: TRect;
begin
  TaskBarH := FindWindow('Shell_TrayWnd', nil);
  GetWindowRect(TaskBarH, TaskBarR);
  Result := TaskBarR.Bottom - TaskBarR.Top;
end;

function TUnDmkProcFunc.AnoDoPeriodo(Periodo: Integer): Integer;
var
  Ano, Mes: Word;
begin
  Ano := (Periodo + (2000 * 12)) div 12 ;
  Mes := (Periodo + (2000 * 12)) mod 12;
  if Mes = 0 then
  begin
    //Mes := 12;
    Ano := Ano -1;
  end;
  Result := Ano;
end;

procedure TUnDmkProcFunc.AnoMesDecode(const AnoMes: Integer; var Ano, Mes: Word);
begin
  Ano := AnoMes div 100;
  Mes := AnoMes mod 100;
end;

// .ELT(  E L T   *ELT* ELT _ELT_
function TUnDmkProcFunc.ArrayToTexto(Campo, NO_Campo: String;
  PosVirgula: TPosVirgula; IniZero: Boolean; MyArr: array of String;
  SomaIni: Integer): String;
var
  I: Integer;
begin
{
ELT(Estilo+1, "Teste0", "teste1", "Teste 2", "Testet3", "Testet4") NO_ESTILO
}
  Result := ' ELT(' + Campo;
  if SomaIni > 0 then
    Result := Result + ' + ' + Geral.FF0(SomaIni) + ','
  else
  if IniZero then
    Result := Result + ' + 1,'
  else
    Result := Result + ',';
  //
  for I := Low(MyArr) to High(MyArr) do
  begin
    Result := Result + '"' + MyArr[I];
    if I = High(MyArr) then
      Result := Result + '") ' + NO_Campo
    else
      Result := Result + '",'
  end;
  case PosVirgula of
    pvNo: ;
    pvPre: Result := ',' + Result;
    pvPos: Result := Result + ',';
  end;
end;

function TUnDmkProcFunc.ArrayToTextoCASEWHEN(Campo, NO_Campo: String;
  PosVirgula: TPosVirgula; MyArrStr: array of String;
  MyArrInt: array of Integer): String;
const
  sProcName = 'TUnDmkProcFunc.ArrayToTextoCASEWHEN()';
(*
procedure TEnumList.Crea{*word*249}um(TypeInfo: PTypeInfo);
var i : integer;
begin
 Clear;
  with GetTypeData(TypeInfo)^ do
    for I := MinValue to MaxValue do
AddObject(GetEnumName(TypeInfo,I),Pointer(i));
end;
*)
var
  I: Integer;
begin
  if Length(MyArrStr) <> Length(MyArrInt) then
  begin
    Geral.MB_Erro('Quantidade de textos difere da quantidade de inteiros em ' +
      sProcName);
    Result := ' CASE ?? WHEN  ? THEN "?" WHEN  ? then "?" ELSE "?" END ' + Campo;
    Exit;
  end;
{
CASE ??
  WHEN  ? then "?"
  WHEN  ? then "?"
  ELSE "?" END ???
}
  Result := ' CASE ' + Campo;
  //
  for I := Low(MyArrInt) to High(MyArrInt) do
    Result := Result + ' WHEN ' + Geral.FF0(MyArrInt[I]) + ' THEN "' + MyArrStr[I] + '" ';
  Result := Result + ' ELSE "?" END ' + NO_Campo;
  case PosVirgula of
    pvNo: Result := Result + ' ';
    pvPre: Result := ', ' + Result + ' ';
    pvPos: Result := Result + ', ';
  end;
end;

function TUnDmkProcFunc.ELT_FIND_IN_SET(Campo, NO_Campo: String;
  PosVirgula: TPosVirgula; IniZero: Boolean; MyIntArr: array of Integer;
  MyStrArr: array of String): String;
var
  I: Integer;
begin
{
ELT(FIND_IN_SET(Level1, '1000,2000'), "A", "B", "C") NO_TESTE,
}
  Result := 'ELT(FIND_IN_SET(' + Campo + ',''' + Geral.FF0(MyIntArr[0]);
  for I := 1 to High(MyIntArr) do
    Result := Result + ',' + Geral.FF0(MyIntArr[I]);
  Result := Result + '''),';
////////////////////////////////////////////////////////////////////////////////
  for I := Low(MyStrArr) to High(MyStrArr) do
  begin
    Result := Result + '"' + MyStrArr[I];
    if I = High(MyStrArr) then
      Result := Result + '") ' + NO_Campo
    else
      Result := Result + '",'
  end;
  case PosVirgula of
    pvNo: ;
    pvPre: Result := ',' + Result;
    pvPos: Result := Result + ',';
  end;
end;

function TUnDmkProcFunc.EncodeBase64(const inStr: string): string;

  function Encode_Byte(b: Byte): char;
  const
    Base64Code: string[64] =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
  begin
    Result := Char(Base64Code[(b and $3F)+1]);
  end;
var
  i: Integer;
begin
  i := 1;
  Result := '';
  while i <= Length(InStr) do
  begin
    Result := Result + Encode_Byte(Byte(inStr[i]) shr 2);
    Result := Result + Encode_Byte((Byte(inStr[i]) shl 4) or (Byte(inStr[i+1]) shr 4));
    if i+1 <= Length(inStr) then
      Result := Result + Encode_Byte((Byte(inStr[i+1]) shl 2) or (Byte(inStr[i+2]) shr 6))
    else
      Result := Result + '=';
    if i+2 <= Length(inStr) then
      Result := Result + Encode_Byte(Byte(inStr[i+2]))
    else
      Result := Result + '=';
    Inc(i, 3);
  end;
end;

function TUnDmkProcFunc.BoolToInt2(Verdade: Boolean; ResultTrue,
  ResultFalse: Integer): Integer;
begin
  if Verdade then
    Result := ResultTrue
  else
    Result := ResultFalse;
end;

function TUnDmkProcFunc.BoolToIntDeVarios(Verdade: array of Boolean;
  Resultado: array of Integer; Default: Integer): Integer;
var
  I: Integer;
begin
  Result := Default;
  if Length(Verdade) <> Length(Resultado) then
  begin
    Geral.MB_Erro('Quantidade de comparações difere de ' +
    'disponibilidade de resultados em "BoolToIntDeVarios"');
    Exit;
  end;
  for I := Low(Verdade) to High(Verdade) do
  begin
    if Verdade[I] then
    begin
      Result := Resultado[I];
      Break;
    end;
  end;
end;

function TUnDmkProcFunc.BoolToSimNao(Verdade: Boolean): String;
begin
  if Verdade then
    Result := 'SIM'
  else
    Result := 'NÃO';
end;

function TUnDmkProcFunc.BoolToV_F(Verdade: Boolean): Char;
begin
  if Verdade then
    Result := 'V'
  else
    Result := 'F';
end;

function TUnDmkProcFunc.CalculaCor(Val, GG, GR, RG, RR, RB, BR: Double): TColor;
  function MyRGB(R, G, B: Double): TColor;
  begin
    Result := RGB(Trunc(R), Trunc(G), Trunc(B));
  end;
var
  Max, Fim, Vlr: Double;
  R, G, B: Double;
begin
  Result := RGB(192, 192, 192);
  Fim := GG;
  if GR > Fim then
    Fim := GR;
  if RG > Fim then
    Fim := RG;
  if RR > Fim then
    Fim := RR;
  if RB > Fim then
    Fim := RB;
  if BR > Fim then
    Fim := BR;
  //VerdeOK: R=000 G=255 B=000
  if (Val <= GG) or (Fim = GG)  then
  begin
    Result := RGB(0, 255, 0);
    Exit;
  end;
  //Verde??: R=064 G=255 B=000
  //VerdeCl: R=128 G=255 B=000
  //VerdeLi: R=192 G=255 B=000
  //Amarelo: R=255 G=255 B=000
  if (Val <= GR) or (Fim = GR)  then
  begin
    Max := GR - GG;
    if Max > 0 then
    begin
      Vlr := Val - GG;
      if Vlr > Max then Vlr := Max;
      R := Vlr / Max * 255;
      Result := MyRGB(R, 255, 0);
      Exit;
    end;
  end;
  //LaFraco: R=255 G=192 B=000
  //LaForte: R=255 G=128 B=000
  if (Val <= RG) or (Fim = RG)  then
  begin
    Max := RG - GR;
    if Max > 0 then
    begin
      Vlr := Val - GR;
      if Vlr > Max then Vlr := Max;
      G := 255 - (Vlr / Max * 128);
      Result := MyRGB(255, G, 0);
      Exit;
    end;
  end;
  //Vem.Ala: R=255 G=064 B=000
  //Vemelho: R=255 G=000 B=000
  if (Val <= RR) or (Fim = RR)  then
  begin
    Max := RR - RG;
    if Max > 0 then
    begin
      Vlr := Val - RG;
      if Vlr > Max then Vlr := Max;
      G := 128 - (Vlr / Max * 128);
      Result := MyRGB(255, G, 0);
      Exit;
    end;
  end;
//Vem.Azu: R=255 G=000 B=064
//Vem.Pnk: R=255 G=000 B=128
//Pink   : R=255 G=000 B=192
//Magenta: R=255 G=000 B=255
  if (Val <= RB) or (Fim = RB)  then
  begin
    Max := RB - RR;
    if Max > 0 then
    begin
      Vlr := Val - RR;
      if Vlr > Max then Vlr := Max;
      B := (Vlr / Max * 255);
      Result := MyRGB(255, 0, B);
      Exit;
    end;
  end;
//Roxo   : R=192 G=000 B=255
//Liláz  : R=128 G=000 B=255
  //if Val < RR then
  if Val <= BR then
  begin
    Max := BR - RB;
    if Max > 0 then
    begin
      Vlr := Val - RB;
      if Vlr > Max then Vlr := Max;
      R := 255 - (Vlr / Max * 128);
      Result := MyRGB(R, 0, 255);
      Exit;
    end;
  end;
  Result := RGB(128, 0, 255)
end;

function TUnDmkProcFunc.CalculaIdade(DataNasc: TDateTime): Integer;
var
  DataAtual: TDateTime;
  DiaNasc, MesNasc, AnoNasc,
  DiaAtual, MesAtual, AnoAtual: Word;
begin
  if not dmkPF.DateIsNull(DataNasc) then
  begin
    DataAtual := Now;
    //
    if DataAtual > DataNasc then
    begin
      DecodeDate(DataNasc, AnoNasc, MesNasc, DiaNasc);
      DecodeDate(DataAtual, AnoAtual, MesAtual, DiaAtual);
      //
      if (MesAtual > MesNasc)  or ((MesAtual = MesNasc) and (DiaAtual >= DiaNasc)) then
        Result := AnoAtual - AnoNasc
      else
        Result := AnoAtual - AnoNasc - 1;
    end else
      Result := 0;
  end else
    Result := 0;
end;

function TUnDmkProcFunc.CalculaJuroComposto(TaxaM, Prazo: Double): Double;
var
  ddf, Fator, TaxaR(*, TaxaA*): Double;
begin
  if (TaxaM = 0) or (Prazo = 0) then
  begin
    Result := 0;
    Exit;
  end;
  Fator := 0;
  ddF := Prazo;
  TaxaR := 0;
  //TaxaA := 0;
  while ddF > 30 do
  begin
    //TaxaA := TaxaR;
    TaxaR := TaxaR * Fator + TaxaM;
    ddF := ddF-30;
    Fator := TaxaM / 100 + 1;
  end;
  Fator := (TaxaM * (ddF /30) / 100) * (*TaxaA*)TaxaR;
  TaxaR := TaxaR + (ddF / 30 * TaxaM);
  Result := TaxaR + Fator;
end;

function TUnDmkProcFunc.CalculaJuroSimples(TaxaM, Prazo: Double): Double;
begin
  if (TaxaM = 0) or (Prazo = 0) then
  begin
    Result := 0;
    Exit;
  end;
  Result := TaxaM * (Prazo / 30);
end;

function TUnDmkProcFunc.CalculaRendimento(ValIni, ValFim: Double): Double;
begin
  if ValIni > 0 then
  begin
    if ValFim > 0 then
      Result := (Divide2(ValFim, ValIni) - 1) * 100
    else
      Result := -100
  end else
    Result := 0;
end;

{$IFNDEF NAO_USA_UnitMD5}
function TUnDmkProcFunc.CalculaValSerialKey2(DataVerifi: String;
  Web: Boolean): String;

  function SerialNum(FDrive:String): String;
  var
    nVNameSer: PDWORD;
    pVolName: PChar;
    FSSysFlags, maxCmpLen: DWord;
    pFSBuf: PChar;
    drv: String;
  begin
    try
      drv := FDrive + '\';

      GetMem(pVolName, MAX_PATH);
      GetMem(pFSBuf, MAX_PATH);
      GetMem(nVNameSer, MAX_PATH);

      GetVolumeInformation(PChar(drv), pVolName, MAX_PATH, nVNameSer,
        maxCmpLen, FSSysFlags, pFSBuf, MAX_PATH);

      Result := IntToStr(nVNameSer^);

      FreeMem(pVolName, MAX_PATH);
      FreeMem(pFSBuf, MAX_PATH);
      FreeMem(nVNameSer, MAX_PATH);
    except
      Result := '';
    end;
  end;
  {
  //Destivado o notebook tem duas placas de rede wirelass e cabo
  function GetMACaddress : TWMacId;
  var
    i : integer;
    AdapterInfo : Pointer;
    pAdapterInfo : PIP_ADAPTER_INFO;
    dwBufLen, dwStatus : LongWord;
  begin
    dwBufLen := 0;
    AdapterInfo := Nil; //Inicialmente, limpa o retorno
    for i := 0 to MAX_ADAPTER_ADDRESS_LENGTH - 1 do
      Result [i] := 0;
    GetAdaptersInfo(PIP_ADAPTER_INFO (AdapterInfo), dwBufLen);
    if (dwBufLen > 0) then
    begin
      GetMem (AdapterInfo, dwBufLen);
      dwStatus := GetAdaptersInfo(PIP_ADAPTER_INFO (AdapterInfo), dwBufLen);
      if (dwStatus = ERROR_SUCCESS) then
      begin
        pAdapterInfo := PIP_ADAPTER_INFO (AdapterInfo);
        //Copia o endereço p/ nossa variável de retorno
        for i := 0 to MAX_ADAPTER_ADDRESS_LENGTH - 1 do
          Result[i] := pAdapterInfo.Address[i];
        FreeMem (AdapterInfo);
      end;
    end;
  end;
  function MacAddress: String;
  var
    id : TWMacId;
  begin
    id := getMacAddress;
    Result := Format ( '%.2X', [id [1]]) +
    Format ( '%.2X', [id [2]]) +
    Format ( '%.2X', [id [3]]) +
    Format ( '%.2X', [id [4]]) +
    Format ( '%.2X', [id [5]]);
  end;
  }
  function MacAddress2: String;
  var
    a,b,c,d: LongWord;
    CPUID: String;
  begin
    asm
      push EAX
      push EBX
      push ECX
      push EDX

      mov eax, 1
      db $0F, $A2
      mov a, EAX
      mov b, EBX
      mov c, ECX
      mov d, EDX

      pop EDX
      pop ECX
      pop EBX
      pop EAX

      {
      mov eax, 1
      db $0F, $A2
      mov a, EAX
      mov b, EBX
      mov c, ECX
      mov d, EDX
      }
    end;
    CPUID  := inttohex(a,8);// + inttohex(b,8) + inttohex(c,8) + inttohex(d,8);
    Result := CPUID;
  end;
var
  Unidade, HD, MAC: String;
begin
  //SerialKey = Número do HD + Número do CPU + Data da última verificação
  //
  Unidade := ExtractFileDrive(Application.ExeName);
  HD      := SerialNum(Unidade);
  MAC     := MacAddress2;
  //
  if Web then
    Result := UnMD5.StrMD5(HD + MAC)
  else
    Result := UnMD5.StrMD5(HD + MAC + DataVerifi);
end;

function TUnDmkProcFunc.CalculaValSerialNum2(VctoLicen, CNPJ: String; CodAplic,
  StatAplic: Integer; Web: Boolean): String;
begin
  //SerialNum = CNPJ do cliente + Código do aplicativo +
  //            Data do vencimento da licença + Status do aplicativo
  //
  if Web then
    Result := UnMD5.StrMD5(CNPJ + FormatFloat('0', CodAplic))
  else
    Result := UnMD5.StrMD5(CNPJ + FormatFloat('0', CodAplic) +
      VctoLicen + FormatFloat('0', StatAplic));
end;
{$ENDIF}

function TUnDmkProcFunc.CalculaValSerialKey_VendaApp(const CNPJ_CPF: String;
  var SerialHD, CPUID: String): String;

  function SerialNum(FDrive:String): String;
  var
    nVNameSer: PDWORD;
    pVolName: PChar;
    FSSysFlags, maxCmpLen: DWord;
    pFSBuf: PChar;
    drv: String;
  begin
    try
      drv := FDrive + '\';

      GetMem(pVolName, MAX_PATH);
      GetMem(pFSBuf, MAX_PATH);
      GetMem(nVNameSer, MAX_PATH);

      GetVolumeInformation(PChar(drv), pVolName, MAX_PATH, nVNameSer,
        maxCmpLen, FSSysFlags, pFSBuf, MAX_PATH);

      Result := IntToStr(nVNameSer^);

      FreeMem(pVolName, MAX_PATH);
      FreeMem(pFSBuf, MAX_PATH);
      FreeMem(nVNameSer, MAX_PATH);
    except
      Result := '';
    end;
  end;
  {
  //Destivado o notebook tem duas placas de rede wirelass e cabo
  function GetMACaddress : TWMacId;
  var
    i : integer;
    AdapterInfo : Pointer;
    pAdapterInfo : PIP_ADAPTER_INFO;
    dwBufLen, dwStatus : LongWord;
  begin
    dwBufLen := 0;
    AdapterInfo := Nil; //Inicialmente, limpa o retorno
    for i := 0 to MAX_ADAPTER_ADDRESS_LENGTH - 1 do
      Result [i] := 0;
    GetAdaptersInfo(PIP_ADAPTER_INFO (AdapterInfo), dwBufLen);
    if (dwBufLen > 0) then
    begin
      GetMem (AdapterInfo, dwBufLen);
      dwStatus := GetAdaptersInfo(PIP_ADAPTER_INFO (AdapterInfo), dwBufLen);
      if (dwStatus = ERROR_SUCCESS) then
      begin
        pAdapterInfo := PIP_ADAPTER_INFO (AdapterInfo);
        //Copia o endereço p/ nossa variável de retorno
        for i := 0 to MAX_ADAPTER_ADDRESS_LENGTH - 1 do
          Result[i] := pAdapterInfo.Address[i];
        FreeMem (AdapterInfo);
      end;
    end;
  end;
  function MacAddress: String;
  var
    id : TWMacId;
  begin
    id := getMacAddress;
    Result := Format ( '%.2X', [id [1]]) +
    Format ( '%.2X', [id [2]]) +
    Format ( '%.2X', [id [3]]) +
    Format ( '%.2X', [id [4]]) +
    Format ( '%.2X', [id [5]]);
  end;
  }
  function MacAddress2: String;
  var
    a,b,c,d: LongWord;
    CPUID: String;
  begin
    asm
      push EAX
      push EBX
      push ECX
      push EDX

      mov eax, 1
      db $0F, $A2
      mov a, EAX
      mov b, EBX
      mov c, ECX
      mov d, EDX

      pop EDX
      pop ECX
      pop EBX
      pop EAX

      {
      mov eax, 1
      db $0F, $A2
      mov a, EAX
      mov b, EBX
      mov c, ECX
      mov d, EDX
      }
    end;
    CPUID  := inttohex(a,8);// + inttohex(b,8) + inttohex(c,8) + inttohex(d,8);
    Result := CPUID;
  end;
var
  Unidade, (*HD, MAC,*) CNPJCPF: String;
  Md5: TIdHashMessageDigest5;
begin
  //SerialKey = Número do HD + Número do CPU + Data da última verificação
  //
  Unidade  := ExtractFileDrive(Application.ExeName);
  SerialHD := SerialNum(Unidade);
  CPUID    := MacAddress2;
  //
  CNPJCPF  := Geral.SoNumero_TT(CNPJ_CPF);
  //
  Md5 := TIdHashMessageDigest5.Create;
  Result := Md5.HashStringAsHex(CNPJCPF + SerialHD + CPUID);
  Md5.Free;
end;

function TUnDmkProcFunc.CaminhoArquivo(Diretorio, Arquivo,
  Extensao: String): String;
// Verifica caminho
// Verifica arquivo
// MontaCaminho MontaNomeArquivo
var
  Len: Integer;
  Arq: String;
begin
  Diretorio := Trim(Diretorio);
  Len :=  Length(Diretorio);
  if pos('\', Arquivo) = 1 then
    Arq := Copy(Arquivo, 2)
  else
    Arq := Arquivo;
  if Len > 0 then
  begin
    if Diretorio[Len] = '\' then
      Result := Diretorio + Arq
    else
      Result := Diretorio + '\' + Arq;
  end else Result := Arq;
  //
  Extensao := Trim(Extensao);
  Len :=  Length(Extensao);
  if Len > 0 then
    if Extensao[1] = '.' then Extensao := Copy(Extensao, 2, Len);
  //Len :=  Length(Extensao);
  if Extensao <> '' then
  begin
    Len :=  Length(Arq);
    if Arq[Len] = '.' then
      Result := Result + Extensao
    else
      Result := Result + '.' + Extensao;
    //
  end;
end;

function TUnDmkProcFunc.CampoReportTxt(Texto, TextoIfEmpty: String): String;
begin
  if Texto <> '' then
    Result := Texto
  else
    Result := TextoIfEmpty;
end;

function TUnDmkProcFunc.CarregaArquivo(const Arquivo: String; var Texto: String): Boolean;
var
  Lista: TStringList;
begin
  Result := False;
  Lista := TStringList.Create;
  try
    try
      if FileExists(Arquivo) then
        Lista.LoadFromFile(Arquivo);
      Texto := Lista.Text;
      Result := True;
    except
      on e: exception do
        Geral.MB_Erro(E.Message);
    end;
  finally
    // libera a lista
    Lista.Free;
  end;
end;

function TUnDmkProcFunc.CDS(Data: String; FormatoAtual,
  FormatoNovo: Integer): String;
var
  NewData: TDateTime;
  Ano, Mes, Dia: Word;
begin
  case FormatoAtual of
    1: NewData := StrToDate(Data);
    2: NewData := Geral.ValidaDataSimples(Data, True);
    3:
    begin
      Ano := StrToInt(Copy(Data, 1, 4));
      Mes := StrToInt(Copy(Data, 5, 2));
      Dia := StrToInt(Copy(Data, 7, 2));
      NewData := EncodeDate(Ano, Mes, Dia);
    end;
    12:
    begin
      DecodeDate(Date, Ano, Mes, Dia);
      Dia := StrToInt(Copy(Data, 1, 2));
      Mes := StrToInt(Copy(Data, 3, 2));
      NewData := EncodeDate(Ano, Mes, Dia);
      if NewData > Date then
      begin
        Ano := Ano -1;
        NewData := EncodeDate(Ano, Mes, Dia);
      end;
    end;
    else begin
      Geral.MB_Erro('Formato atual na function CDS não implementado!');
      Result := Data;
      Exit;
    end;
  end;
  Result := Geral.FDT(NewData, FormatoNovo);
end;

function TUnDmkProcFunc.CDS2(Data, BiFormatoAtual: String;
  FormatoNovo: Integer): String;
var
  FormatoAtual, yt, mt, dt: String;
  yi, mi, di, i: Integer;
begin
  if Trim(Data) = '' then
  begin
    Result := '';
    Exit;
  end;
  FormatoAtual := Uppercase(BiFormatoAtual);
  yt := '';
  mt := '';
  dt := '';
  for i := 1 to Length(FormatoAtual) do
  begin
    if FormatoAtual[i] = 'A' then yt := yt + Data[i] else
    if FormatoAtual[i] = 'Y' then yt := yt + Data[i] else
    if FormatoAtual[i] = 'M' then mt := mt + Data[i] else
    if FormatoAtual[i] = 'D' then dt := dt + Data[i];
  end;
  yi := Geral.IMV(yt);
  mi := Geral.IMV(mt);
  di := Geral.IMV(dt);
  if (yi = 0) or (mi = 0 ) or (di = 0) then
    Result := '' else
  begin
    if yi < 100 then yi := yi + 2000;
    Result := Geral.FDT(EncodeDate(yi, mi, di), FormatoNovo);
  end;
end;

function TUnDmkProcFunc.CheckCPFCNPJ(Entidade, Tipo: Integer; Nome,
  CPFCNPJ: String): Boolean;
const
  CPFMin = 11;
  CPFMax = 11;
  CNPJMin = 14;
  CNPJMax = 15;
var
  CNPJ, Num: String;
  Tam: Integer;
begin
  Result := False;
  CNPJ := Geral.SoNumero_TT(CPFCNPJ);
  if CNPJ <> '' then
  begin
    Num := Geral.CalculaCNPJCPF(CNPJ);
    if Geral.FormataCNPJ_TFT(CNPJ) <> Num then
    begin
      Geral.MB_Aviso(SMLA_NUMEROINVALIDO2);
      Exit;
    end;
  end else
  begin
    if Entidade <> 0 then
      Geral.MB_Aviso('A entidade ' + Geral.FF0(Entidade) + ' - ' + Nome +
      ' não possui CNPJ/CPF. ' + sLineBreak + 'A informação é obrigatória!');
    Exit;
  end;
  Tam := Length(CNPJ);
  case Tipo of
    0: Result := Tam in ([CNPJMin, CNPJMax]);
    1: Result := Tam in ([CPFMin, CPFMax]);
    else Result := False;
  end;
  if not Result then Geral.MB_Aviso('A entidade ' + Geral.FF0(Entidade) +
    ' - ' + Nome + ' possui CNPJ/CPF, mas a quantidade ' +
    'de dígitos não confere com o tipo de documento!');
end;

function TUnDmkProcFunc.CheckedItemsCount(Lista: TCheckListBox): Integer;
var
  I, J: Integer;
begin
  J := 0;
  //
  for I := Low(Lista.Items.Count) to High(Lista.Items.Count) do
  begin
    if Lista.Checked[I] = True then
      J := J + 1;
  end;
  Result := J;
end;

(*
function TUnDmkProcFunc.CharToMyXML_Enviar(Codigo: Integer): String;
begin
  case Codigo of
    000..009: Result := '';
    010:      Result := ' & # 1 0 ;';//'amp;';//' & # 1 0 ;';//Char( 10 ); Não aceita nas observações ????
    013:      Result := ' & # 1 3 ;';//'amp;';//' & # 1 3 ;';//Char( 13 ); Não aceita nas observações ????
    014..031: Result := '';
    032    : Result := ' '; // espaço em branco
    033 {!}: Result := '!';
    034 {"}: Result := '&quot;';
    035 {#}: Result := '#';
    036    : Result := '&#36;';  //'$'
    037 {%}: Result := '%';
    038 {&}: Result := '&amp;';
    039 {'}: Result := '&#39;';
    040 {(}: Result := '(';
    041 {)}: Result := ')';
    042 {*}: Result := '*';
    043 {+}: Result := '+';
    044 {,}: Result := ',';
    045 {-}: Result := '-';
    046 {.}: Result := '.';
    047 {/}: Result := '/';
    048 {0}: Result := '0';
    049 {1}: Result := '1';
    050 {2}: Result := '2';
    051 {3}: Result := '3';
    052 {4}: Result := '4';
    053 {5}: Result := '5';
    054 {6}: Result := '6';
    055 {7}: Result := '7';
    056 {8}: Result := '8';
    057 {9}: Result := '9';
    058 {:}: Result := ':';
    059 {;}: Result := ';';
    060 {<}: Result := '&lt;';
    061 {=}: Result := '=';
    062 {>}: Result := '&gt;';
    063 {?}: Result := '?';
    064 {@}: Result := '@';
    065 {A}: Result := 'A';
    066 {B}: Result := 'B';
    067 {C}: Result := 'C';
    068 {D}: Result := 'D';
    069 {E}: Result := 'E';
    070 {F}: Result := 'F';
    071 {G}: Result := 'G';
    072 {H}: Result := 'H';
    073 {I}: Result := 'I';
    074 {J}: Result := 'J';
    075 {K}: Result := 'K';
    076 {L}: Result := 'L';
    077 {M}: Result := 'M';
    078 {N}: Result := 'N';
    079 {O}: Result := 'O';
    080 {P}: Result := 'P';
    081 {Q}: Result := 'Q';
    082 {R}: Result := 'R';
    083 {S}: Result := 'S';
    084 {T}: Result := 'T';
    085 {U}: Result := 'U';
    086 {V}: Result := 'V';
    087 {W}: Result := 'W';
    088 {X}: Result := 'X';
    089 {Y}: Result := 'Y';
    090 {Z}: Result := 'Z';
    091 {[}: Result := '[';
    092 {\}: Result := '\';
    093 {]}: Result := ']';
    094 {^}: Result := '^';
    095 {_}: Result := '_';
    096 {`}: Result := '`';
    097 {a}: Result := 'A';
    098 {b}: Result := 'B';
    099 {c}: Result := 'C';
    100 {d}: Result := 'D';
    101 {e}: Result := 'E';
    102 {f}: Result := 'F';
    103 {g}: Result := 'G';
    104 {h}: Result := 'H';
    105 {i}: Result := 'I';
    106 {j}: Result := 'J';
    107 {k}: Result := 'K';
    108 {l}: Result := 'L';
    109 {m}: Result := 'M';
    110 {n}: Result := 'N';
    111 {o}: Result := 'O';
    112 {p}: Result := 'P';
    113 {q}: Result := 'Q';
    114 {r}: Result := 'R';
    115 {s}: Result := 'S';
    116 {t}: Result := 'T';
    117 {u}: Result := 'U';
    118 {v}: Result := 'V';
    119 {w}: Result := 'W';
    120 {x}: Result := 'X';
    121 {y}: Result := 'Y';
    122 {z}: Result := 'Z';
    123 {{}: Result := '{';
    124 {|}: Result := '|';
    125    : Result := '}';

    126..152: Result := '';  // Nada

    153 {™}: Result := 'TM';
    154 {š}: Result := 'S';
    155 {›}: Result := '&gt;';
    156 {œ}: Result := '';
    157    : Result := '';
    158 {ž}: Result := 'Z';
    159 {Ÿ}: Result := 'Y';
    160 { }: Result := ' ';
    161 {¡}: Result := 'I';
    162 {¢}: Result := 'C';
    163 {£}: Result := 'LIBRA';
    164 {¤}: Result := '';
    165 {¥}: Result := 'Y';
    166 {¦}: Result := ' ';
    167 {§}: Result := 'S';
    168 {¨}: Result := '';
    169 {©}: Result := 'C';
    170 {ª}: Result := 'A';
    171 {«}: Result := '&lt;&lt;';
    172 {¬}: Result := ' ';
    173 {­}: Result := '-';
    174 {®}: Result := 'R';
    175 {¯}: Result := '-';
    176 {°}: Result := 'O';
    177 {±}: Result := '+-';
    178 {²}: Result := 'O';
    179 {³}: Result := '3';
    180 {´}: Result := '&#39;';
    181 {µ}: Result := 'MICRA';
    182 {¶}: Result := 'P';
    183 {·}: Result := '.';
    184 {¸}: Result := '';
    185 {¹}: Result := '1';
    186 {º}: Result := 'O';
    187 {»}: Result := '&gt;&gt;';
    188 {¼}: Result := '1/4';
    189 {½}: Result := '1/2';
    190 {¾}: Result := '3/4';
    191 {¿}: Result := '?';
    192 {À}: Result := 'A';
    193 {Á}: Result := 'A';
    194 {Â}: Result := 'A';
    195 {Ã}: Result := 'A';
    196 {Ä}: Result := 'A';
    197 {Å}: Result := 'A';
    198 {Æ}: Result := 'E';
    199 {Ç}: Result := 'C';
    200 {È}: Result := 'E';
    201 {É}: Result := 'E';
    202 {Ê}: Result := 'E';
    203 {Ë}: Result := 'E';
    204 {Ì}: Result := 'E';
    205 {Í}: Result := 'I';
    206 {Î}: Result := 'I';
    207 {Ï}: Result := 'I';
    208 {Ð}: Result := 'D';
    209 {Ñ}: Result := 'N';
    210 {Ò}: Result := 'O';
    211 {Ó}: Result := 'O';
    212 {Ô}: Result := 'O';
    213 {Õ}: Result := 'O';
    214 {Ö}: Result := 'O';
    215 {×}: Result := 'X';
    216 {Ø}: Result := 'O';
    217 {Ù}: Result := 'U';
    218 {Ú}: Result := 'U';
    219 {Û}: Result := 'U';
    220 {Ü}: Result := 'U';
    221 {Ý}: Result := 'Y';
    222 {Þ}: Result := 'p';
    223 {ß}: Result := 'B';
    224 {à}: Result := 'A';
    225 {á}: Result := 'A';
    226 {â}: Result := 'A';
    227 {ã}: Result := 'A';
    228 {ä}: Result := 'A';
    229 {å}: Result := 'A';
    230 {æ}: Result := 'E';
    231 {ç}: Result := 'C';
    232 {è}: Result := 'E';
    233 {é}: Result := 'E';
    234 {ê}: Result := 'E';
    235 {ë}: Result := 'E';
    236 {ì}: Result := 'I';
    237 {í}: Result := 'I';
    238 {î}: Result := 'I';
    239 {ï}: Result := 'I';
    240 {ð}: Result := 'D';
    241 {ñ}: Result := 'N';
    242 {ò}: Result := 'O';
    243 {ó}: Result := 'O';
    244 {ô}: Result := 'O';
    245 {õ}: Result := 'O';
    246 {ö}: Result := 'O';
    247 {÷}: Result := '/';
    248 {ø}: Result := 'O';
    249 {ù}: Result := 'U';
    250 {ú}: Result := 'U';
    251 {û}: Result := 'U';
    252 {ü}: Result := 'U';
    253 {ý}: Result := 'Y';
    254 {þ}: Result := 'P';
    255 {ÿ}: Result := 'Y';
    else {001 a 032 ???} Result := Char(Codigo);
  end;
end;
*)

function TUnDmkProcFunc.ColTxtToInt(ColTxt: String): Integer;
var
  I, (*N,*) Res: Integer;
  V, o: Extended;
begin
  // Testado de A até ZZZZ e ficou OK! ZZZZ -> 475254
  if ColTxt = EmptyStr then
    Result := 0
  else
  begin
    Res := 0;
    //N := 0;
    o := 0;
    for I := Length(ColTxt) downto 1 do
    begin
      V := Ord(ColTxt[I]) - 64;
      V := V * Power(26, o);
      Res := Res + Trunc(V);
      //N := N + 1;
      o := o + 1;
    end;
    Result := Res;
  end;
end;

function TUnDmkProcFunc.ComparaTextosComMascara(Txt1, Txt2: String;
  CharCase: Boolean): Boolean;
var
  t1, t2, ta, tb, s: String;
  n, k: Integer;
begin
  if not CharCase then
    Result := AnsiCompareText(Txt1, Txt2) = 0
  else
    Result := AnsiCompareStr(Txt1, Txt2) = 0;
  if not Result then
  begin
    n := pos('*', Txt1);
    if n > 0  then
    begin
      if CharCase then
      begin
        t1 := Txt1;
        t2 := Txt2;
      end else begin
        t1 := AnsiLowercase(Txt1);
        t2 := AnsiLowercase(Txt2);
      end;
      // Ver se tem máscara antes da palavra
      if n > 1 then
      begin
        ta := Copy(t1, 1, n-1) + '*';
        tb := Copy(t2, 1, n-1) + '*';
        t2 := Copy(t2, n+1);
      end else begin
        ta := '';
        tb := '';
      end;
      t1 := Copy(t1, n+1);

      // se combinar, continua
      if ta = tb then
      begin
        n := pos('*', t1);
        while n > 0 do
        begin
          if n = 1 then
          begin
            //Result := True;
            Break;
          end else begin
            s := Copy(t1, 1, n-1);
            k := pos(s, t2);
            if k > 0 then
            begin
              t2 := Copy(t2, k + Length(s));
              t1 := Copy(t1, n+1);
            end;
            n := pos('*', t1);
          end;
        end;
        if t2 > t1 then
          s := Copy(t2, Length(t2) - Length(t1) + 1)
        else
          s := t2;
        Result := (t1 = '') or (t1 = s);
      end;
    end;
  end;
end;

function TUnDmkProcFunc.ConcatTxtChkGroup(Conjunto, MaxItemsLista: Integer;
  Lista: array of String): String;
var
  Nome: String;
  I: Integer;
  J: Extended;
  //
  procedure AddNome(Item: Integer);
  begin
    if Nome <> '' then
      Nome := Nome + ' - ';
    Nome := Nome + Lista[Item];
  end;
begin
  Nome := '';
  for I := 0 to MaxItemsLista + 1 do
  begin
    if I < 3 then
      J := I
    else
      J := Power(I - 1, 2);
    if dmkPF.IntInConjunto2(Trunc(J), Conjunto) then AddNome(I-1);
  end;
  Result := Nome;
end;

function TUnDmkProcFunc.ConfereVersaoApp(Informa: Boolean): Boolean;
var
  ExeVer, CplVer: WideString;
begin
  ExeVer := Geral.FileVerInfo(Application.ExeName, 3 (*Versao*));
  CplVer := Geral.VersaoTxt2006(CO_VERSAO);
  Result := CplVer = Exever;
  if (Result = False) and Informa then
    Geral.MB_Aviso('A versão de compilação (' + CplVer +
    ') difere do executável (' + ExeVer + ').');
end;

procedure TUnDmkProcFunc.ConfigIniApp(UsaCoresRel: Integer);
begin
  IC2_LOpcoesTeclaPonto := Geral.ReadAppKeyLM('TeclaPonto', Application.Title, ktInteger, 110);
  //
  if IC2_LOpcoesTeclaPonto = -1 then
  begin
    try
      Geral.WriteAppKeyLM2('TeclaPonto', Application.Title, 110, ktInteger);
    except

    end;
    IC2_LOpcoesTeclaPonto := 110;
  end;
  IC2_LOpcoesTeclaVirgula := Geral.ReadAppKeyLM('TeclaVirgula', Application.Title, ktInteger, 188);
  //
  if IC2_LOpcoesTeclaVirgula = -1 then
  begin
    try
      Geral.WriteAppKeyLM2('TeclaVirgula', Application.Title, 188, ktInteger);
      IC2_LOpcoesTeclaVirgula := 188;
    except

    end;
  end;
  IC2_LOpcoesTeclaEnter := Geral.ReadAppKeyLM('TeclaEnter', Application.Title, ktInteger, 13);
  //
  if IC2_LOpcoesTeclaEnter = -1 then
  begin
    try
      Geral.WriteAppKeyLM2('TeclaEnter', Application.Title, 13, ktInteger);
      IC2_LOpcoesTeclaEnter := 13;
    except

    end;
  end;
  IC2_LOpcoesTeclaTab := Geral.ReadAppKeyLM('TeclaTAB', Application.Title, ktInteger, 9);
  //
  if IC2_LOpcoesTeclaTab = -1 then
  begin
    try
      Geral.WriteAppKeyLM2('TeclaTAB', Application.Title, 9, ktInteger);
      IC2_LOpcoesTeclaTab := 9;
    except

    end;
  end;

  if UsaCoresRel > 0 then
  begin
    VAR_CORGRUPOXA1 := $B0B06A;
    VAR_CORGRUPOXA2 := clWhite;
    VAR_CORGRUPOXB1 := clWhite;
    VAR_CORGRUPOXB2 := clBlack;
    VAR_CORGRUPOYA1 := clWhite;
    VAR_CORGRUPOYA2 := clBlack;
    VAR_CORGRUPOZA1 := clWhite;
    VAR_CORGRUPOZA2 := clNavy;
    VAR_CORGRUPOA1  := $FF9F9F;
    VAR_CORGRUPOA2  := clWhite;
    VAR_CORGRUPOB1  := $C8B9B9;
    VAR_CORGRUPOB2  := clBlack;
    VAR_CORGRUPOC1  := $C8B9B9;
    VAR_CORGRUPOC2  := clBlack;
    VAR_CORGRUPOD1  := clWhite;
    VAR_CORGRUPOD2  := clMaroon;
    VAR_CORGRUPOZ1  := clWhite;
    VAR_CORGRUPOZ2  := clBlack;
  end else begin
    VAR_CORGRUPOXA1 := clWhite;
    VAR_CORGRUPOXA2 := clBlack;
    VAR_CORGRUPOXB1 := clWhite;
    VAR_CORGRUPOXB2 := clBlack;
    VAR_CORGRUPOYA1 := clWhite;
    VAR_CORGRUPOYA2 := clBlack;
    VAR_CORGRUPOZA1 := clWhite;
    VAR_CORGRUPOZA2 := clBlack;
    VAR_CORGRUPOA1  := clWhite;
    VAR_CORGRUPOA2  := clBlack;
    VAR_CORGRUPOB1  := clWhite;
    VAR_CORGRUPOB2  := clBlack;
    VAR_CORGRUPOC1  := clWhite;
    VAR_CORGRUPOC2  := clBlack;
    VAR_CORGRUPOD1  := clWhite;
    VAR_CORGRUPOD2  := clBlack;
    VAR_CORGRUPOZ1  := clWhite;
    VAR_CORGRUPOZ2  := clBlack;
  end;
  VAR_CLIENTE1 := CO_CLIENTE;
  VAR_CLIENTE2 := '';
  VAR_CLIENTE3 := '';
  VAR_CLIENTE4 := '';
  VAR_FORNECE1 := CO_FORNECEDOR;
  VAR_FORNECE2 := CO_TRANSPORTADOR;
  VAR_FORNECE3 := '';
  VAR_FORNECE4 := '';
  VAR_FORNECE5 := '';
  VAR_FORNECE6 := '';
  VAR_TERCEIR2 := 'Outros';
  VAR_CLIENTEC := '';
  VAR_FORNECEF := '';
  VAR_FORNECEV := '';
  //
  VAR_FP_EMPRESA := 'Cliente1="V"';
  VAR_FP_FUNCION := '(Fornece2="V" OR Fornece4="V")';
  //
  VAR_TITULO_FATNUM := '? ? ?';
  //
end;

function TUnDmkProcFunc.ConfiguraIconeAplicativo(AllUsers: Boolean;
  Pergunta: Boolean; NomeApp: String): Boolean;
var
  Continua: Boolean;
  App, LinkNome: String;
  IObject: IUnknown;
  ISLink: IShellLink;
  IPFile: IPersistFile;
  PIDL: PItemIDList;
  InFolder: array [0..MAX_PATH-1] of Char;
begin
  Result := False;
  //
  if NomeApp <> '' then
    App := NomeApp
  else
    App := Application.ExeName;
  //
  if Pergunta then
    Continua := Geral.MB_Pergunta('Deseja criar um ícone para o executável?') = ID_YES
  else
    Continua := True;
  //
  if (Continua) and (FileExists(App)) then
  begin
    IObject := CreateComObject(CLSID_ShellLink);
    ISLink  := IObject as IShellLink;
    IPFile  := IObject as IPersistFile;
    //
    with ISLink do
    begin
      SetPath(PChar(App));
      SetWorkingDirectory(PChar(ExtractFilePath(App)));
    end;
    //
    SHGetSpecialFolderLocation(0, CSIDL_DESKTOPDIRECTORY, PIDL);
    SHGetPathFromIDList(PIDL, InFolder) ;
    //
    LinkNome := IncludeTrailingBackslash(ObtemDiretorioDesktop(AllUsers));
    LinkNome := LinkNome + ExtractFileName(App) + '.lnk';
    //
    if not FileExists(LinkNome) then
    begin
      if IPFile.Save(PWideChar(LinkNome), False) = S_OK then
      begin
        Result := True;
      end;
    end;
  end;
end;

function TUnDmkProcFunc.ContinuarInserindo(SQLType: TSQLType): Boolean;
var
  TipoTxt: String;
begin
  TipoTxt := DmkEnums.NomeTipoSQL(SQLType);
  if Geral.MB_Pergunta(TipoTxt + ' realizada com sucesso!'+ sLineBreak +
  'Deseja continuar inserindo?') = ID_YES then
    Result := True
  else
    Result := False;
end;

function TUnDmkProcFunc.ConverteANSItoUTF8(Texto: String): String;
var
  I, K: Integer;
  Res, Tmp: String;
begin
  Res := '';
  for I := 1 to Length(Texto) do
  begin
    K := Ord(Texto[I]);
    if K < 127 then
    begin
      case K of
      (*&*) 38: Tmp := '038'; // &amp; AMPERSAND
      (*<*) 60: Tmp := '060'; // &lt; LESS-THAN SIGN
      (*>*) 62: Tmp := '062'; // &gt; GREATER-THAN SIGN
           else Tmp := '';
      end;
    end else
    begin
      Tmp := FormatFloat('000', K);
    end;
    if Tmp = '' then
      Res := Res + Texto[I]
    else
      Res := Res + '&#' + Tmp + ';'



{
  Res := '';
  for I := 1 to Length(Texto) do
  begin
    K := Ord(Texto[I]);
    if K < 127 then
    begin
      case K of
      (*&*) 38: Tmp := '0026'; // &amp; AMPERSAND
      (*<*) 60: Tmp := '003C'; // &lt; LESS-THAN SIGN
      (*>*) 62: Tmp := '003E'; // &gt; GREATER-THAN SIGN
           else Tmp := '';
      end;
    end else
    begin
      case K of
        (*DEL*) 127: Tmp := '007F'; // delete (rubout)
         //
        (*€*) 128: Tmp := '0080'; // CONTROL
        (**) 129: Tmp := '0081'; // CONTROL
        (*‚*) 130: Tmp := '0082'; // BREAK PERMITTED HERE
        (*ƒ*) 131: Tmp := '0083'; // NO BREAK HERE
        (*„*) 132: Tmp := '0084'; // INDEX
        (*…*) 133: Tmp := '0085'; // NEXT LINE (NEL)
        (*†*) 134: Tmp := '0086'; // START OF SELECTED AREA
        (*‡*) 135: Tmp := '0087'; // END OF SELECTED AREA
        (*ˆ*) 136: Tmp := '0088'; // CHARACTER TABULATION SET
        (*‰*) 137: Tmp := '0089'; // CHARACTER TABULATION WITH JUSTIFICATION
        (*Š*) 138: Tmp := '008A'; // LINE TABULATION SET
        (*‹*) 139: Tmp := '008B'; // PARTIAL LINE FORWARD
        (*Œ*) 140: Tmp := '008C'; // PARTIAL LINE BACKWARD
        (**) 141: Tmp := '008D'; // REVERSE LINE FEED
        (*Ž*) 142: Tmp := '008E'; // SINGLE SHIFT TWO
        (**) 143: Tmp := '008F'; // SINGLE SHIFT THREE
        (**) 144: Tmp := '0090'; // DEVICE CONTROL STRING
        (*‘*) 145: Tmp := '0091'; // PRIVATE USE ONE
        (*’*) 146: Tmp := '0092'; // PRIVATE USE TWO
        (*“*) 147: Tmp := '0093'; // SET TRANSMIT STATE
        (*”*) 148: Tmp := '0094'; // CANCEL CHARACTER
        (*•*) 149: Tmp := '0095'; // MESSAGE WAITING
        (*–*) 150: Tmp := '0096'; // START OF GUARDED AREA
        (*—*) 151: Tmp := '0097'; // END OF GUARDED AREA
        (*˜*) 152: Tmp := '0098'; // START OF STRING
        (*™*) 153: Tmp := '0099'; // CONTROL
        (*š*) 154: Tmp := '009A'; // SINGLE CHARACTER INTRODUCER
        (*›*) 155: Tmp := '009B'; // CONTROL SEQUENCE INTRODUCER
        (*œ*) 156: Tmp := '009C'; // STRING TERMINATOR
        (**) 157: Tmp := '009D'; // OPERATING SYSTEM COMMAND
        (*ž*) 158: Tmp := '009E'; // PRIVACY MESSAGE
        (*Ÿ*) 159: Tmp := '009F'; // APPLICATION PROGRAM COMMAND
         //
        (* *) 160: Tmp := '00A0'; // &nbsp; NO-BREAK SPACE
        (*¡*) 161: Tmp := '00A1'; // &iexcl; INVERTED EXCLAMATION MARK
        (*¢*) 162: Tmp := '00A2'; //  &cent; CENT SIGN
        (*£*) 163: Tmp := '00A3'; //  &pound; POUND SIGN
        (*¤*) 164: Tmp := '00A4'; //  &curren; CURRENCY SIGN
        (*¥*) 165: Tmp := '00A5'; //  &yen; YEN SIGN
        (*¦*) 166: Tmp := '00A6'; //  &brvbar; BROKEN BAR
        (*§*) 167: Tmp := '00A7'; //  &sect; SECTION SIGN
        (*¨*) 168: Tmp := '00A8'; //  &uml; DIAERESIS
        (*©*) 169: Tmp := '00A9'; //  &copy; COPYRIGHT SIGN
        (*ª*) 170: Tmp := '00AA'; //  &ordf; FEMININE ORDINAL INDICATOR
        (*«*) 171: Tmp := '00AB'; //  &laquo; LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
        (*¬*) 172: Tmp := '00AC'; //  &not; NOT SIGN
        (*­*) 173: Tmp := '00AD'; //  &shy; SOFT HYPHEN
        (*®*) 174: Tmp := '00AE'; //  &reg; REGISTERED SIGN
        (*¯*) 175: Tmp := '00AF'; //  &macr; MACRON
        (*°*) 176: Tmp := '00B0'; //  &deg; DEGREE SIGN
        (*±*) 177: Tmp := '00B1'; //  &plusmn; PLUS-MINUS SIGN
        (*²*) 178: Tmp := '00B2'; //  &sup2; SUPERSCRIPT TWO
        (*³*) 179: Tmp := '00B3'; //  &sup3; SUPERSCRIPT THREE
        (*´*) 180: Tmp := '00B4'; //  &acute; ACUTE ACCENT
        (*µ*) 181: Tmp := '00B5'; //  &micro; MICRO SIGN
        (*¶*) 182: Tmp := '00B6'; //  &para; PILCROW SIGN
        (*·*) 183: Tmp := '00B7'; //  &middot; MIDDLE DOT
        (*¸*) 184: Tmp := '00B8'; //  &cedil; CEDILLA
        (*¹*) 185: Tmp := '00B9'; //  &sup1; SUPERSCRIPT ONE
        (*º*) 186: Tmp := '00BA'; //  &ordm; MASCULINE ORDINAL INDICATOR
        (*»*) 187: Tmp := '00BB'; //  &raquo; RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK
        (*¼*) 188: Tmp := '00BC'; //  &frac14; VULGAR FRACTION ONE QUARTER
        (*½*) 189: Tmp := '00BD'; //  &frac12; VULGAR FRACTION ONE HALF
        (*¾*) 190: Tmp := '00BE'; //  &frac34; VULGAR FRACTION THREE QUARTERS
        (*¿*) 191: Tmp := '00BF'; //  &iquest; INVERTED QUESTION MARK
        (*À*) 192: Tmp := '00C0'; //  &Agrave; LATIN CAPITAL LETTER A WITH GRAVE
        (*Á*) 193: Tmp := '00C1'; //  &Aacute; LATIN CAPITAL LETTER A WITH ACUTE
        (*Â*) 194: Tmp := '00C2'; //  &Acirc; LATIN CAPITAL LETTER A WITH CIRCUMFLEX
        (*Ã*) 195: Tmp := '00C3'; //  &Atilde; LATIN CAPITAL LETTER A WITH TILDE
        (*Ä*) 196: Tmp := '00C4'; //  &Auml; LATIN CAPITAL LETTER A WITH DIAERESIS
        (*Å*) 197: Tmp := '00C5'; //  &Aring; LATIN CAPITAL LETTER A WITH RING ABOVE
        (*Æ*) 198: Tmp := '00C6'; //  &AElig; LATIN CAPITAL LETTER AE
        (*Ç*) 199: Tmp := '00C7'; //  &Ccedil; LATIN CAPITAL LETTER C WITH CEDILLA
        (*È*) 200: Tmp := '00C8'; //  &Egrave; LATIN CAPITAL LETTER E WITH GRAVE
        (*É*) 201: Tmp := '00C9'; //  &Eacute; LATIN CAPITAL LETTER E WITH ACUTE
        (*Ê*) 202: Tmp := '00CA'; //  &Ecirc; LATIN CAPITAL LETTER E WITH CIRCUMFLEX
        (*Ë*) 203: Tmp := '00CB'; //  &Euml; LATIN CAPITAL LETTER E WITH DIAERESIS
        (*Ì*) 204: Tmp := '00CC'; //  &Igrave; LATIN CAPITAL LETTER I WITH GRAVE
        (*Í*) 205: Tmp := '00CD'; //  &Iacute; LATIN CAPITAL LETTER I WITH ACUTE
        (*Î*) 206: Tmp := '00CE'; //  &Icirc; LATIN CAPITAL LETTER I WITH CIRCUMFLEX
        (*Ï*) 207: Tmp := '00CF'; //  &Iuml; LATIN CAPITAL LETTER I WITH DIAERESIS
        (*Ð*) 208: Tmp := '00D0'; //  &ETH; LATIN CAPITAL LETTER ETH
        (*Ñ*) 209: Tmp := '00D1'; //  &Ntilde; LATIN CAPITAL LETTER N WITH TILDE
        (*Ò*) 210: Tmp := '00D2'; //  &Ograve; LATIN CAPITAL LETTER O WITH GRAVE
        (*Ó*) 211: Tmp := '00D3'; //  &Oacute; LATIN CAPITAL LETTER O WITH ACUTE
        (*Ô*) 212: Tmp := '00D4'; //  &Ocirc; LATIN CAPITAL LETTER O WITH CIRCUMFLEX
        (*Õ*) 213: Tmp := '00D5'; //  &Otilde; LATIN CAPITAL LETTER O WITH TILDE
        (*Ö*) 214: Tmp := '00D6'; //  &Ouml; LATIN CAPITAL LETTER O WITH DIAERESIS
        (*×*) 215: Tmp := '00D7'; //  &times; MULTIPLICATION SIGN
        (*Ø*) 216: Tmp := '00D8'; //  &Oslash; LATIN CAPITAL LETTER O WITH STROKE
        (*Ù*) 217: Tmp := '00D9'; //  &Ugrave; LATIN CAPITAL LETTER U WITH GRAVE
        (*Ú*) 218: Tmp := '00DA'; //  &Uacute; LATIN CAPITAL LETTER U WITH ACUTE
        (*Û*) 219: Tmp := '00DB'; //  &Ucirc; LATIN CAPITAL LETTER U WITH CIRCUMFLEX
        (*Ü*) 220: Tmp := '00DC'; //  &Uuml; LATIN CAPITAL LETTER U WITH DIAERESIS
        (*Ý*) 221: Tmp := '00DD'; //  &Yacute; LATIN CAPITAL LETTER Y WITH ACUTE
        (*Þ*) 222: Tmp := '00DE'; //  &THORN; LATIN CAPITAL LETTER THORN
        (*ß*) 223: Tmp := '00DF'; //  &szlig; LATIN SMALL LETTER SHARP S
        (*à*) 224: Tmp := '00E0'; //  &agrave; LATIN SMALL LETTER A WITH GRAVE
        (*á*) 225: Tmp := '00E1'; //  &aacute; LATIN SMALL LETTER A WITH ACUTE
        (*â*) 226: Tmp := '00E2'; //  &acirc; LATIN SMALL LETTER A WITH CIRCUMFLEX
        (*ã*) 227: Tmp := '00E3'; //  &atilde; LATIN SMALL LETTER A WITH TILDE
        (*ä*) 228: Tmp := '00E4'; //  &auml; LATIN SMALL LETTER A WITH DIAERESIS
        (*å*) 229: Tmp := '00E5'; //  &aring; LATIN SMALL LETTER A WITH RING ABOVE
        (*æ*) 230: Tmp := '00E6'; //  &aelig; LATIN SMALL LETTER AE
        (*ç*) 231: Tmp := '00E7'; //  &ccedil; LATIN SMALL LETTER C WITH CEDILLA
        (*è*) 232: Tmp := '00E8'; //  &egrave; LATIN SMALL LETTER E WITH GRAVE
        (*é*) 233: Tmp := '00E9'; //  &eacute; LATIN SMALL LETTER E WITH ACUTE
        (*ê*) 234: Tmp := '00EA'; //  &ecirc; LATIN SMALL LETTER E WITH CIRCUMFLEX
        (*ë*) 235: Tmp := '00EB'; //  &euml; LATIN SMALL LETTER E WITH DIAERESIS
        (*ì*) 236: Tmp := '00EC'; //  &igrave; LATIN SMALL LETTER I WITH GRAVE
        (*í*) 237: Tmp := '00ED'; //  &iacute; LATIN SMALL LETTER I WITH ACUTE
        (*î*) 238: Tmp := '00EE'; //  &icirc; LATIN SMALL LETTER I WITH CIRCUMFLEX
        (*ï*) 239: Tmp := '00EF'; //  &iuml; LATIN SMALL LETTER I WITH DIAERESIS
        (*ð*) 240: Tmp := '00F0'; //  &eth; LATIN SMALL LETTER ETH
        (*ñ*) 241: Tmp := '00F1'; //  &ntilde; LATIN SMALL LETTER N WITH TILDE
        (*ò*) 242: Tmp := '00F2'; //  &ograve; LATIN SMALL LETTER O WITH GRAVE
        (*ó*) 243: Tmp := '00F3'; //  &oacute; LATIN SMALL LETTER O WITH ACUTE
        (*ô*) 244: Tmp := '00F4'; //  &ocirc; LATIN SMALL LETTER O WITH CIRCUMFLEX
        (*õ*) 245: Tmp := '00F5'; //  &otilde; LATIN SMALL LETTER O WITH TILDE
        (*ö*) 246: Tmp := '00F6'; //  &ouml; LATIN SMALL LETTER O WITH DIAERESIS
        (*÷*) 247: Tmp := '00F7'; //  &divide; DIVISION SIGN
        (*ø*) 248: Tmp := '00F8'; //  &oslash; LATIN SMALL LETTER O WITH STROKE
        (*ù*) 249: Tmp := '00F9'; //  &ugrave; LATIN SMALL LETTER U WITH GRAVE
        (*ú*) 250: Tmp := '00FA'; //  &uacute; LATIN SMALL LETTER U WITH ACUTE
        (*û*) 251: Tmp := '00FB'; //  &ucirc; LATIN SMALL LETTER U WITH CIRCUMFLEX
        (*ü*) 252: Tmp := '00FC'; //  &uuml; LATIN SMALL LETTER U WITH DIAERESIS
        (*ý*) 253: Tmp := '00FD'; //  &yacute; LATIN SMALL LETTER Y WITH ACUTE
        (*þ*) 254: Tmp := '00FE'; //  &thorn; LATIN SMALL LETTER THORN
        (*ÿ*) 255: Tmp := '00FF'; //  &yuml; LATIN SMALL LETTER Y WITH DIAERESIS
         else Tmp := '';
      end;
    end;
(*
--------------------------------------------------------------------------------
UTF-8 C0 Controls and Basic Latin
Range: Decimal 0-127. Hex 0020-007F.
--------------------------------------------------------------------------------
Char Dec Hex Entity Name
 32 0020   SPACE
! 33 0021   EXCLAMATION MARK
" 34 0022 &quot; QUOTATION MARK
# 35 0023   NUMBER SIGN
$ 36 0024   DOLLAR SIGN
% 37 0025   PERCENT SIGN
& 38 0026 &amp; AMPERSAND
' 39 0027   APOSTROPHE
( 40 0028   LEFT PARENTHESIS
) 41 0029   RIGHT PARENTHESIS
* 42 002A   ASTERISK
+ 43 002B   PLUS SIGN
, 44 002C   COMMA
- 45 002D   HYPHEN-MINUS
. 46 002E   FULL STOP
/ 47 002F   SOLIDUS
0 48 0030   DIGIT ZERO
1 49 0031   DIGIT ONE
2 50 0032   DIGIT TWO
3 51 0033   DIGIT THREE
4 52 0034   DIGIT FOUR
5 53 0035   DIGIT FIVE
6 54 0036   DIGIT SIX
7 55 0037   DIGIT SEVEN
8 56 0038   DIGIT EIGHT
9 57 0039   DIGIT NINE
: 58 003A   COLON
; 59 003B   SEMICOLON
< 60 003C &lt; LESS-THAN SIGN
= 61 003D   EQUALS SIGN
> 62 003E &gt; GREATER-THAN SIGN
? 63 003F   QUESTION MARK
@ 64 0040   COMMERCIAL AT
A 65 0041   LATIN CAPITAL LETTER A
B 66 0042   LATIN CAPITAL LETTER B
C 67 0043   LATIN CAPITAL LETTER C
D 68 0044   LATIN CAPITAL LETTER D
E 69 0045   LATIN CAPITAL LETTER E
F 70 0046   LATIN CAPITAL LETTER F
G 71 0047   LATIN CAPITAL LETTER G
H 72 0048   LATIN CAPITAL LETTER H
I 73 0049   LATIN CAPITAL LETTER I
J 74 004A   LATIN CAPITAL LETTER J
K 75 004B   LATIN CAPITAL LETTER K
L 76 004C   LATIN CAPITAL LETTER L
M 77 004D   LATIN CAPITAL LETTER M
N 78 004E   LATIN CAPITAL LETTER N
O 79 004F   LATIN CAPITAL LETTER O
P 80 0050   LATIN CAPITAL LETTER P
Q 81 0051   LATIN CAPITAL LETTER Q
R 82 0052   LATIN CAPITAL LETTER R
S 83 0053   LATIN CAPITAL LETTER S
T 84 0054   LATIN CAPITAL LETTER T
U 85 0055   LATIN CAPITAL LETTER U
V 86 0056   LATIN CAPITAL LETTER V
W 87 0057   LATIN CAPITAL LETTER W
X 88 0058   LATIN CAPITAL LETTER X
Y 89 0059   LATIN CAPITAL LETTER Y
Z 90 005A   LATIN CAPITAL LETTER Z
[ 91 005B   LEFT SQUARE BRACKET
\ 92 005C   REVERSE SOLIDUS
] 93 005D   RIGHT SQUARE BRACKET
^ 94 005E   CIRCUMFLEX ACCENT
_ 95 005F   LOW LINE
` 96 0060   GRAVE ACCENT
a 97 0061   LATIN SMALL LETTER A
b 98 0062   LATIN SMALL LETTER B
c 99 0063   LATIN SMALL LETTER C
d 100 0064   LATIN SMALL LETTER D
e 101 0065   LATIN SMALL LETTER E
f 102 0066   LATIN SMALL LETTER F
g 103 0067   LATIN SMALL LETTER G
h 104 0068   LATIN SMALL LETTER H
i 105 0069   LATIN SMALL LETTER I
j 106 006A   LATIN SMALL LETTER J
k 107 006B   LATIN SMALL LETTER K
l 108 006C   LATIN SMALL LETTER L
m 109 006D   LATIN SMALL LETTER M
n 110 006E   LATIN SMALL LETTER N
o 111 006F   LATIN SMALL LETTER O
p 112 0070   LATIN SMALL LETTER P
q 113 0071   LATIN SMALL LETTER Q
r 114 0072   LATIN SMALL LETTER R
s 115 0073   LATIN SMALL LETTER S
t 116 0074   LATIN SMALL LETTER T
u 117 0075   LATIN SMALL LETTER U
v 118 0076   LATIN SMALL LETTER V
w 119 0077   LATIN SMALL LETTER W
x 120 0078   LATIN SMALL LETTER X
y 121 0079   LATIN SMALL LETTER Y
z 122 007A   LATIN SMALL LETTER Z
{ 123 007B   LEFT CURLY BRACKET
| 124 007C   VERTICAL LINE
}{ 125 007D   RIGHT CURLY BRACKET
~ 126 007E   TILDE


(*
C0 Controls
The control characters were originally designed to control hardware devices.
Control characters (except horizontal tab, carriage return, and line feed) have nothing to do inside an HTML document.

Char Dec Hex Description
NUL 0 0000 null character
SOH 1 0001 start of header
STX 2 0002 start of text
ETX 3 0003 end of text
EOT 4 0004 end of transmission
ENQ 5 0005 enquiry
ACK 6 0006 acknowledge
BEL 7 0007 bell (ring)
BS 8 0008 backspace
HT 9 0009 horizontal tab
LF 10 000A line feed
VT 11 000B vertical tab
FF 12 000C form feed
CR 13 000D carriage return
SO 14 000E shift out
SI 15 000F shift in
DLE 16 0010 data link escape
DC1 17 0011 device control 1
DC2 18 0012 device control 2
DC3 19 0013 device control 3
DC4 20 0014 device control 4
NAK 21 0015 negative acknowledge
SYN 22 0016 synchronize
ETB 23 0017 end transmission block
CAN 24 0018 cancel
EM 25 0019 end of medium
SUB 26 001A substitute
ESC 27 001B escape
FS 28 001C file separator
GS 29 001D group separator
RS 30 001E record separator
US 31 001F unit separator
DEL 127 007F delete (rubout)
*)
(*
--------------------------------------------------------------------------------
UTF-8 C1 Controls and Latin1 Supplement
Range: Decimal 128-255. Hex 0080-00FF.
--------------------------------------------------------------------------------
Char Dec Hex Entity Name
  160 00A0 &nbsp; NO-BREAK SPACE
¡ 161 00A1 &iexcl; INVERTED EXCLAMATION MARK
¢ 162 00A2 &cent; CENT SIGN
£ 163 00A3 &pound; POUND SIGN
¤ 164 00A4 &curren; CURRENCY SIGN
¥ 165 00A5 &yen; YEN SIGN
¦ 166 00A6 &brvbar; BROKEN BAR
§ 167 00A7 &sect; SECTION SIGN
¨ 168 00A8 &uml; DIAERESIS
© 169 00A9 &copy; COPYRIGHT SIGN
ª 170 00AA &ordf; FEMININE ORDINAL INDICATOR
« 171 00AB &laquo; LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
¬ 172 00AC &not; NOT SIGN
­ 173 00AD &shy; SOFT HYPHEN
® 174 00AE &reg; REGISTERED SIGN
¯ 175 00AF &macr; MACRON
° 176 00B0 &deg; DEGREE SIGN
± 177 00B1 &plusmn; PLUS-MINUS SIGN
² 178 00B2 &sup2; SUPERSCRIPT TWO
³ 179 00B3 &sup3; SUPERSCRIPT THREE
´ 180 00B4 &acute; ACUTE ACCENT
µ 181 00B5 &micro; MICRO SIGN
¶ 182 00B6 &para; PILCROW SIGN
· 183 00B7 &middot; MIDDLE DOT
¸ 184 00B8 &cedil; CEDILLA
¹ 185 00B9 &sup1; SUPERSCRIPT ONE
º 186 00BA &ordm; MASCULINE ORDINAL INDICATOR
» 187 00BB &raquo; RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK
¼ 188 00BC &frac14; VULGAR FRACTION ONE QUARTER
½ 189 00BD &frac12; VULGAR FRACTION ONE HALF
¾ 190 00BE &frac34; VULGAR FRACTION THREE QUARTERS
¿ 191 00BF &iquest; INVERTED QUESTION MARK
À 192 00C0 &Agrave; LATIN CAPITAL LETTER A WITH GRAVE
Á 193 00C1 &Aacute; LATIN CAPITAL LETTER A WITH ACUTE
Â 194 00C2 &Acirc; LATIN CAPITAL LETTER A WITH CIRCUMFLEX
Ã 195 00C3 &Atilde; LATIN CAPITAL LETTER A WITH TILDE
Ä 196 00C4 &Auml; LATIN CAPITAL LETTER A WITH DIAERESIS
Å 197 00C5 &Aring; LATIN CAPITAL LETTER A WITH RING ABOVE
Æ 198 00C6 &AElig; LATIN CAPITAL LETTER AE
Ç 199 00C7 &Ccedil; LATIN CAPITAL LETTER C WITH CEDILLA
È 200 00C8 &Egrave; LATIN CAPITAL LETTER E WITH GRAVE
É 201 00C9 &Eacute; LATIN CAPITAL LETTER E WITH ACUTE
Ê 202 00CA &Ecirc; LATIN CAPITAL LETTER E WITH CIRCUMFLEX
Ë 203 00CB &Euml; LATIN CAPITAL LETTER E WITH DIAERESIS
Ì 204 00CC &Igrave; LATIN CAPITAL LETTER I WITH GRAVE
Í 205 00CD &Iacute; LATIN CAPITAL LETTER I WITH ACUTE
Î 206 00CE &Icirc; LATIN CAPITAL LETTER I WITH CIRCUMFLEX
Ï 207 00CF &Iuml; LATIN CAPITAL LETTER I WITH DIAERESIS
Ð 208 00D0 &ETH; LATIN CAPITAL LETTER ETH
Ñ 209 00D1 &Ntilde; LATIN CAPITAL LETTER N WITH TILDE
Ò 210 00D2 &Ograve; LATIN CAPITAL LETTER O WITH GRAVE
Ó 211 00D3 &Oacute; LATIN CAPITAL LETTER O WITH ACUTE
Ô 212 00D4 &Ocirc; LATIN CAPITAL LETTER O WITH CIRCUMFLEX
Õ 213 00D5 &Otilde; LATIN CAPITAL LETTER O WITH TILDE
Ö 214 00D6 &Ouml; LATIN CAPITAL LETTER O WITH DIAERESIS
× 215 00D7 &times; MULTIPLICATION SIGN
Ø 216 00D8 &Oslash; LATIN CAPITAL LETTER O WITH STROKE
Ù 217 00D9 &Ugrave; LATIN CAPITAL LETTER U WITH GRAVE
Ú 218 00DA &Uacute; LATIN CAPITAL LETTER U WITH ACUTE
Û 219 00DB &Ucirc; LATIN CAPITAL LETTER U WITH CIRCUMFLEX
Ü 220 00DC &Uuml; LATIN CAPITAL LETTER U WITH DIAERESIS
Ý 221 00DD &Yacute; LATIN CAPITAL LETTER Y WITH ACUTE
Þ 222 00DE &THORN; LATIN CAPITAL LETTER THORN
ß 223 00DF &szlig; LATIN SMALL LETTER SHARP S
à 224 00E0 &agrave; LATIN SMALL LETTER A WITH GRAVE
á 225 00E1 &aacute; LATIN SMALL LETTER A WITH ACUTE
â 226 00E2 &acirc; LATIN SMALL LETTER A WITH CIRCUMFLEX
ã 227 00E3 &atilde; LATIN SMALL LETTER A WITH TILDE
ä 228 00E4 &auml; LATIN SMALL LETTER A WITH DIAERESIS
å 229 00E5 &aring; LATIN SMALL LETTER A WITH RING ABOVE
æ 230 00E6 &aelig; LATIN SMALL LETTER AE
ç 231 00E7 &ccedil; LATIN SMALL LETTER C WITH CEDILLA
è 232 00E8 &egrave; LATIN SMALL LETTER E WITH GRAVE
é 233 00E9 &eacute; LATIN SMALL LETTER E WITH ACUTE
ê 234 00EA &ecirc; LATIN SMALL LETTER E WITH CIRCUMFLEX
ë 235 00EB &euml; LATIN SMALL LETTER E WITH DIAERESIS
ì 236 00EC &igrave; LATIN SMALL LETTER I WITH GRAVE
í 237 00ED &iacute; LATIN SMALL LETTER I WITH ACUTE
î 238 00EE &icirc; LATIN SMALL LETTER I WITH CIRCUMFLEX
ï 239 00EF &iuml; LATIN SMALL LETTER I WITH DIAERESIS
ð 240 00F0 &eth; LATIN SMALL LETTER ETH
ñ 241 00F1 &ntilde; LATIN SMALL LETTER N WITH TILDE
ò 242 00F2 &ograve; LATIN SMALL LETTER O WITH GRAVE
ó 243 00F3 &oacute; LATIN SMALL LETTER O WITH ACUTE
ô 244 00F4 &ocirc; LATIN SMALL LETTER O WITH CIRCUMFLEX
õ 245 00F5 &otilde; LATIN SMALL LETTER O WITH TILDE
ö 246 00F6 &ouml; LATIN SMALL LETTER O WITH DIAERESIS
÷ 247 00F7 &divide; DIVISION SIGN
ø 248 00F8 &oslash; LATIN SMALL LETTER O WITH STROKE
ù 249 00F9 &ugrave; LATIN SMALL LETTER U WITH GRAVE
ú 250 00FA &uacute; LATIN SMALL LETTER U WITH ACUTE
û 251 00FB &ucirc; LATIN SMALL LETTER U WITH CIRCUMFLEX
ü 252 00FC &uuml; LATIN SMALL LETTER U WITH DIAERESIS
ý 253 00FD &yacute; LATIN SMALL LETTER Y WITH ACUTE
þ 254 00FE &thorn; LATIN SMALL LETTER THORN
ÿ 255 00FF &yuml; LATIN SMALL LETTER Y WITH DIAERESIS

(*
C1 Controls
The control characters were originally designed to control hardware devices.
The control characters are not supposed to be displayed in HTML.
However, because they are defined as characters, in the ANSI character set used
by Windows, they might be displayed if you are using Windows.

Char Dec Hex Control
€ 128 0080 CONTROL
 129 0081 CONTROL
‚ 130 0082 BREAK PERMITTED HERE
ƒ 131 0083 NO BREAK HERE
„ 132 0084 INDEX
… 133 0085 NEXT LINE (NEL)
† 134 0086 START OF SELECTED AREA
‡ 135 0087 END OF SELECTED AREA
ˆ 136 0088 CHARACTER TABULATION SET
‰ 137 0089 CHARACTER TABULATION WITH JUSTIFICATION
Š 138 008A LINE TABULATION SET
‹ 139 008B PARTIAL LINE FORWARD
Œ 140 008C PARTIAL LINE BACKWARD
 141 008D REVERSE LINE FEED
Ž 142 008E SINGLE SHIFT TWO
 143 008F SINGLE SHIFT THREE
 144 0090 DEVICE CONTROL STRING
‘ 145 0091 PRIVATE USE ONE
’ 146 0092 PRIVATE USE TWO
“ 147 0093 SET TRANSMIT STATE
” 148 0094 CANCEL CHARACTER
• 149 0095 MESSAGE WAITING
– 150 0096 START OF GUARDED AREA
— 151 0097 END OF GUARDED AREA
˜ 152 0098 START OF STRING
™ 153 0099 CONTROL
š 154 009A SINGLE CHARACTER INTRODUCER
› 155 009B CONTROL SEQUENCE INTRODUCER
œ 156 009C STRING TERMINATOR
 157 009D OPERATING SYSTEM COMMAND
ž 158 009E PRIVACY MESSAGE
Ÿ 159 009F APPLICATION PROGRAM COMMAND

*)

(*
--------------------------------------------------------------------------------
UTF-8 Latin Extended A
Range: Decimal 256-383. Hex 0100-017F.


Char Dec Hex Entity Name

Ā 256 0100 &Amacr; LATIN CAPITAL LETTER A WITH MACRON
ā 257 0101 &amacr; LATIN SMALL LETTER A WITH MACRON
Ă 258 0102 &Abreve; LATIN CAPITAL LETTER A WITH BREVE
ă 259 0103 &abreve; LATIN SMALL LETTER A WITH BREVE
Ą 260 0104 &Aogon; LATIN CAPITAL LETTER A WITH OGONEK
ą 261 0105 &aogon; LATIN SMALL LETTER A WITH OGONEK
Ć 262 0106 &Cacute; LATIN CAPITAL LETTER C WITH ACUTE
ć 263 0107 &cacute; LATIN SMALL LETTER C WITH ACUTE
Ĉ 264 0108 &Ccirc; LATIN CAPITAL LETTER C WITH CIRCUMFLEX
ĉ 265 0109 &ccirc; LATIN SMALL LETTER C WITH CIRCUMFLEX
Ċ 266 010A &Cdod; LATIN CAPITAL LETTER C WITH DOT ABOVE
ċ 267 010B &cdot; LATIN SMALL LETTER C WITH DOT ABOVE
Č 268 010C &Ccaron; LATIN CAPITAL LETTER C WITH CARON
č 269 010D &ccaron; LATIN SMALL LETTER C WITH CARON
Ď 270 010E &Dcaron; LATIN CAPITAL LETTER D WITH CARON
ď 271 010F &dcaron; LATIN SMALL LETTER D WITH CARON
Đ 272 0110 &Dstrok; LATIN CAPITAL LETTER D WITH STROKE
đ 273 0111 &dstrok; LATIN SMALL LETTER D WITH STROKE
Ē 274 0112 &Emacr; LATIN CAPITAL LETTER E WITH MACRON
ē 275 0113 &emacr; LATIN SMALL LETTER E WITH MACRON
Ĕ 276 0114   LATIN CAPITAL LETTER E WITH BREVE
ĕ 277 0115   LATIN SMALL LETTER E WITH BREVE
Ė 278 0116 &Edot; LATIN CAPITAL LETTER E WITH DOT ABOVE
ė 279 0117 &edot; LATIN SMALL LETTER E WITH DOT ABOVE
Ę 280 0118 &Eogon; LATIN CAPITAL LETTER E WITH OGONEK
ę 281 0119 &eogon; LATIN SMALL LETTER E WITH OGONEK
Ě 282 011A &Ecaron; LATIN CAPITAL LETTER E WITH CARON
ě 283 011B &ecaron; LATIN SMALL LETTER E WITH CARON
Ĝ 284 011C &Gcirc; LATIN CAPITAL LETTER G WITH CIRCUMFLEX
ĝ 285 011D &gcirc; LATIN SMALL LETTER G WITH CIRCUMFLEX
Ğ 286 011E &Gbreve; LATIN CAPITAL LETTER G WITH BREVE
ğ 287 011F &gbreve; LATIN SMALL LETTER G WITH BREVE
Ġ 288 0120 &GDot; LATIN CAPITAL LETTER G WITH DOT ABOVE
ġ 289 0121 &gdot; LATIN SMALL LETTER G WITH DOT ABOVE
Ģ 290 0122 &Gcedil; LATIN CAPITAL LETTER G WITH CEDILLA
ģ 291 0123 &gcedil; LATIN SMALL LETTER G WITH CEDILLA
Ĥ 292 0124 &Hcirc; LATIN CAPITAL LETTER H WITH CIRCUMFLEX
ĥ 293 0125 &hcirc; LATIN SMALL LETTER H WITH CIRCUMFLEX
Ħ 294 0126 &Hstrok; LATIN CAPITAL LETTER H WITH STROKE
ħ 295 0127 &hstrok; LATIN SMALL LETTER H WITH STROKE
Ĩ 296 0128 &Itilde; LATIN CAPITAL LETTER I WITH TILDE
ĩ 297 0129 &itilde; LATIN SMALL LETTER I WITH TILDE
Ī 298 012A &Imacr; LATIN CAPITAL LETTER I WITH MACRON
ī 299 012B &imacr; LATIN SMALL LETTER I WITH MACRON
Ĭ 300 012C   LATIN CAPITAL LETTER I WITH BREVE
ĭ 301 012D   LATIN SMALL LETTER I WITH BREVE
Į 302 012E &Iogon; LATIN CAPITAL LETTER I WITH OGONEK
į 303 012F &iogon; LATIN SMALL LETTER I WITH OGONEK
İ 304 0130 &Idot; LATIN CAPITAL LETTER I WITH DOT ABOVE
ı 305 0131 &inodot; LATIN SMALL LETTER DOTLESS I
Ĳ 306 0132 &IJlog; LATIN CAPITAL LIGATURE IJ
ĳ 307 0133 &ijlig; LATIN SMALL LIGATURE IJ
Ĵ 308 0134 &Jcirc; LATIN CAPITAL LETTER J WITH CIRCUMFLEX
ĵ 309 0135 &jcirc; LATIN SMALL LETTER J WITH CIRCUMFLEX
Ķ 310 0136 &Kcedil; LATIN CAPITAL LETTER K WITH CEDILLA
ķ 311 0137 &kcedli; LATIN SMALL LETTER K WITH CEDILLA
ĸ 312 0138 &kgreen; LATIN SMALL LETTER KRA
Ĺ 313 0139 &Lacute; LATIN CAPITAL LETTER L WITH ACUTE
ĺ 314 013A &lacute; LATIN SMALL LETTER L WITH ACUTE
Ļ 315 013B &Lcedil; LATIN CAPITAL LETTER L WITH CEDILLA
ļ 316 013C &lcedil; LATIN SMALL LETTER L WITH CEDILLA
Ľ 317 013D &Lcaron; LATIN CAPITAL LETTER L WITH CARON
ľ 318 013E &lcaron; LATIN SMALL LETTER L WITH CARON
Ŀ 319 013F &Lmodot; LATIN CAPITAL LETTER L WITH MIDDLE DOT
ŀ 320 0140 &lmidot; LATIN SMALL LETTER L WITH MIDDLE DOT
Ł 321 0141 &Lstrok; LATIN CAPITAL LETTER L WITH STROKE
ł 322 0142 &lstrok; LATIN SMALL LETTER L WITH STROKE
Ń 323 0143 &Nacute; LATIN CAPITAL LETTER N WITH ACUTE
ń 324 0144 &nacute; LATIN SMALL LETTER N WITH ACUTE
Ņ 325 0145 &Ncedil; LATIN CAPITAL LETTER N WITH CEDILLA
ņ 326 0146 &ncedil; LATIN SMALL LETTER N WITH CEDILLA
Ň 327 0147 &Ncaron; LATIN CAPITAL LETTER N WITH CARON
ň 328 0148 &ncaron; LATIN SMALL LETTER N WITH CARON
ŉ 329 0149 &napos; LATIN SMALL LETTER N PRECEDED BY APOSTROPHE
Ŋ 330 014A &ENG; LATIN CAPITAL LETTER ENG
ŋ 331 014B &eng; LATIN SMALL LETTER ENG
Ō 332 014C &Omacr; LATIN CAPITAL LETTER O WITH MACRON
ō 333 014D &omacr; LATIN SMALL LETTER O WITH MACRON
Ŏ 334 014E   LATIN CAPITAL LETTER O WITH BREVE
ŏ 335 014F   LATIN SMALL LETTER O WITH BREVE
Ő 336 0150 &Odblac; LATIN CAPITAL LETTER O WITH DOUBLE ACUTE
ő 337 0151 &odblac; LATIN SMALL LETTER O WITH DOUBLE ACUTE
Œ 338 0152 &OElig; LATIN CAPITAL LIGATURE OE
œ 339 0153 &oelig; LATIN SMALL LIGATURE OE
Ŕ 340 0154 &Racute; LATIN CAPITAL LETTER R WITH ACUTE
ŕ 341 0155 &racute; LATIN SMALL LETTER R WITH ACUTE
Ŗ 342 0156 &Rcedil; LATIN CAPITAL LETTER R WITH CEDILLA
ŗ 343 0157 &rcedil; LATIN SMALL LETTER R WITH CEDILLA
Ř 344 0158 &Rcaron; LATIN CAPITAL LETTER R WITH CARON
ř 345 0159 &rcaron; LATIN SMALL LETTER R WITH CARON
Ś 346 015A &Sacute; LATIN CAPITAL LETTER S WITH ACUTE
ś 347 015B &sacute; LATIN SMALL LETTER S WITH ACUTE
Ŝ 348 015C &Scirc; LATIN CAPITAL LETTER S WITH CIRCUMFLEX
ŝ 349 015D &scirc; LATIN SMALL LETTER S WITH CIRCUMFLEX
Ş 350 015E &Scedil; LATIN CAPITAL LETTER S WITH CEDILLA
ş 351 015F &scedil; LATIN SMALL LETTER S WITH CEDILLA
Š 352 0160 &Scaron; LATIN CAPITAL LETTER S WITH CARON
š 353 0161 &scaron; LATIN SMALL LETTER S WITH CARON
Ţ 354 0162 &Tcedil; LATIN CAPITAL LETTER T WITH CEDILLA
ţ 355 0163 &tcedil; LATIN SMALL LETTER T WITH CEDILLA
Ť 356 0164 &Tcaron; LATIN CAPITAL LETTER T WITH CARON
ť 357 0165 &tcaron; LATIN SMALL LETTER T WITH CARON
Ŧ 358 0166 &Tstrok; LATIN CAPITAL LETTER T WITH STROKE
ŧ 359 0167 &tstrok; LATIN SMALL LETTER T WITH STROKE
Ũ 360 0168 &Utilde; LATIN CAPITAL LETTER U WITH TILDE
ũ 361 0169 &utilde; LATIN SMALL LETTER U WITH TILDE
Ū 362 016A &Umacr; LATIN CAPITAL LETTER U WITH MACRON
ū 363 016B &umacr; LATIN SMALL LETTER U WITH MACRON
Ŭ 364 016C &Ubreve; LATIN CAPITAL LETTER U WITH BREVE
ŭ 365 016D &ubeve; LATIN SMALL LETTER U WITH BREVE
Ů 366 016E &Uring; LATIN CAPITAL LETTER U WITH RING ABOVE
ů 367 016F &uring; LATIN SMALL LETTER U WITH RING ABOVE
Ű 368 0170 &Udblac; LATIN CAPITAL LETTER U WITH DOUBLE ACUTE
ű 369 0171 &udblac; LATIN SMALL LETTER U WITH DOUBLE ACUTE
Ų 370 0172 &Uogon; LATIN CAPITAL LETTER U WITH OGONEK
ų 371 0173 &uogon; LATIN SMALL LETTER U WITH OGONEK
Ŵ 372 0174 &Wcirc; LATIN CAPITAL LETTER W WITH CIRCUMFLEX
ŵ 373 0175 &wcirc; LATIN SMALL LETTER W WITH CIRCUMFLEX
Ŷ 374 0176 &Ycirc; LATIN CAPITAL LETTER Y WITH CIRCUMFLEX
ŷ 375 0177 &ycirc; LATIN SMALL LETTER Y WITH CIRCUMFLEX
Ÿ 376 0178 &Yuml; LATIN CAPITAL LETTER Y WITH DIAERESIS
Ź 377 0179 &Zacute; LATIN CAPITAL LETTER Z WITH ACUTE
ź 378 017A &zacute; LATIN SMALL LETTER Z WITH ACUTE
Ż 379 017B &Zdot; LATIN CAPITAL LETTER Z WITH DOT ABOVE
ż 380 017C &zdot; LATIN SMALL LETTER Z WITH DOT ABOVE
Ž 381 017D &Zcaron; LATIN CAPITAL LETTER Z WITH CARON
ž 382 017E &Zcaron; LATIN SMALL LETTER Z WITH CARON
ſ 383 017F   LATIN SMALL LETTER LONG S

--------------------------------------------------------------------------------
UTF-8 Latin Extended B
Range: Decimal 384-591. Hex 0180-024F.
--------------------------------------------------------------------------------
Char Dec Hex Entity Name
ƀ 384 0180   LATIN SMALL LETTER B WITH STROKE
Ɓ 385 0181   LATIN CAPITAL LETTER B WITH HOOK
Ƃ 386 0182   LATIN CAPITAL LETTER B WITH TOPBAR
ƃ 387 0183   LATIN SMALL LETTER B WITH TOPBAR
Ƅ 388 0184   LATIN CAPITAL LETTER TONE SIX
ƅ 389 0185   LATIN SMALL LETTER TONE SIX
Ɔ 390 0186   LATIN CAPITAL LETTER OPEN O
Ƈ 391 0187   LATIN CAPITAL LETTER C WITH HOOK
ƈ 392 0188   LATIN SMALL LETTER C WITH HOOK
Ɖ 393 0189   LATIN CAPITAL LETTER AFRICAN D
Ɗ 394 018A   LATIN CAPITAL LETTER D WITH HOOK
Ƌ 395 018B   LATIN CAPITAL LETTER D WITH TOPBAR
ƌ 396 018C   LATIN SMALL LETTER D WITH TOPBAR
ƍ 397 018D   LATIN SMALL LETTER TURNED DELTA
Ǝ 398 018E   LATIN CAPITAL LETTER REVERSED E
Ə 399 018F   LATIN CAPITAL LETTER SCHWA
Ɛ 400 0190   LATIN CAPITAL LETTER OPEN E
Ƒ 401 0191   LATIN CAPITAL LETTER F WITH HOOK
ƒ 402 0192 &fnof; LATIN SMALL LETTER F WITH HOOK
Ɠ 403 0193   LATIN CAPITAL LETTER G WITH HOOK
Ɣ 404 0194   LATIN CAPITAL LETTER GAMMA
ƕ 405 0195   LATIN SMALL LETTER HV
Ɩ 406 0196   LATIN CAPITAL LETTER IOTA
Ɨ 407 0197   LATIN CAPITAL LETTER I WITH STROKE
Ƙ 408 0198   LATIN CAPITAL LETTER K WITH HOOK
ƙ 409 0199   LATIN SMALL LETTER K WITH HOOK
ƚ 410 019A   LATIN SMALL LETTER L WITH BAR
ƛ 411 019B   LATIN SMALL LETTER LAMBDA WITH STROKE
Ɯ 412 019C   LATIN CAPITAL LETTER TURNED M
Ɲ 413 019D   LATIN CAPITAL LETTER N WITH LEFT HOOK
ƞ 414 019E   LATIN SMALL LETTER N WITH LONG RIGHT LEG
Ɵ 415 019F   LATIN CAPITAL LETTER O WITH MIDDLE TILDE
Ơ 416 01A0   LATIN CAPITAL LETTER O WITH HORN
ơ 417 01A1   LATIN SMALL LETTER O WITH HORN
Ƣ 418 01A2   LATIN CAPITAL LETTER OI
ƣ 419 01A3   LATIN SMALL LETTER OI
Ƥ 420 01A4   LATIN CAPITAL LETTER P WITH HOOK
ƥ 421 01A5   LATIN SMALL LETTER P WITH HOOK
Ʀ 422 01A6   LATIN LETTER YR
Ƨ 423 01A7   LATIN CAPITAL LETTER TONE TWO
ƨ 424 01A8   LATIN SMALL LETTER TONE TWO
Ʃ 425 01A9   LATIN CAPITAL LETTER ESH
ƪ 426 01AA   LATIN LETTER REVERSED ESH LOOP
ƫ 427 01AB   LATIN SMALL LETTER T WITH PALATAL HOOK
Ƭ 428 01AC   LATIN CAPITAL LETTER T WITH HOOK
ƭ 429 01AD   LATIN SMALL LETTER T WITH HOOK
Ʈ 430 01AE   LATIN CAPITAL LETTER T WITH RETROFLEX HOOK
Ư 431 01AF   LATIN CAPITAL LETTER U WITH HORN
ư 432 01B0   LATIN SMALL LETTER U WITH HORN
Ʊ 433 01B1   LATIN CAPITAL LETTER UPSILON
Ʋ 434 01B2   LATIN CAPITAL LETTER V WITH HOOK
Ƴ 435 01B3   LATIN CAPITAL LETTER Y WITH HOOK
ƴ 436 01B4   LATIN SMALL LETTER Y WITH HOOK
Ƶ 437 01B5 &imped; LATIN CAPITAL LETTER Z WITH STROKE
ƶ 438 01B6   LATIN SMALL LETTER Z WITH STROKE
Ʒ 439 01B7   LATIN CAPITAL LETTER EZH
Ƹ 440 01B8   LATIN CAPITAL LETTER EZH REVERSED
ƹ 441 01B9   LATIN SMALL LETTER EZH REVERSED
ƺ 442 01BA   LATIN SMALL LETTER EZH WITH TAIL
ƻ 443 01BB   LATIN LETTER TWO WITH STROKE
Ƽ 444 01BC   LATIN CAPITAL LETTER TONE FIVE
ƽ 445 01BD   LATIN SMALL LETTER TONE FIVE
ƾ 446 01BE   LATIN LETTER INVERTED GLOTTAL STOP WITH STROKE
ƿ 447 01BF   LATIN LETTER WYNN
ǀ 448 01C0   LATIN LETTER DENTAL CLICK
ǁ 449 01C1   LATIN LETTER LATERAL CLICK
ǂ 450 01C2   LATIN LETTER ALVEOLAR CLICK
ǃ 451 01C3   LATIN LETTER RETROFLEX CLICK
Ǆ 452 01C4   LATIN CAPITAL LETTER DZ WITH CARON
ǅ 453 01C5   LATIN CAPITAL LETTER D WITH SMALL LETTER Z WITH CARON
ǆ 454 01C6   LATIN SMALL LETTER DZ WITH CARON
Ǉ 455 01C7   LATIN CAPITAL LETTER LJ
ǈ 456 01C8   LATIN CAPITAL LETTER L WITH SMALL LETTER J
ǉ 457 01C9   LATIN SMALL LETTER LJ
Ǌ 458 01CA   LATIN CAPITAL LETTER NJ
ǋ 459 01CB   LATIN CAPITAL LETTER N WITH SMALL LETTER J
ǌ 460 01CC   LATIN SMALL LETTER NJ
Ǎ 461 01CD   LATIN CAPITAL LETTER A WITH CARON
ǎ 462 01CE   LATIN SMALL LETTER A WITH CARON
Ǐ 463 01CF   LATIN CAPITAL LETTER I WITH CARON
ǐ 464 01D0   LATIN SMALL LETTER I WITH CARON
Ǒ 465 01D1   LATIN CAPITAL LETTER O WITH CARON
ǒ 466 01D2   LATIN SMALL LETTER O WITH CARON
Ǔ 467 01D3   LATIN CAPITAL LETTER U WITH CARON
ǔ 468 01D4   LATIN SMALL LETTER U WITH CARON
Ǖ 469 01D5   LATIN CAPITAL LETTER U WITH DIAERESIS AND MACRON
ǖ 470 01D6   LATIN SMALL LETTER U WITH DIAERESIS AND MACRON
Ǘ 471 01D7   LATIN CAPITAL LETTER U WITH DIAERESIS AND ACUTE
ǘ 472 01D8   LATIN SMALL LETTER U WITH DIAERESIS AND ACUTE
Ǚ 473 01D9   LATIN CAPITAL LETTER U WITH DIAERESIS AND CARON
ǚ 474 01DA   LATIN SMALL LETTER U WITH DIAERESIS AND CARON
Ǜ 475 01DB   LATIN CAPITAL LETTER U WITH DIAERESIS AND GRAVE
ǜ 476 01DC   LATIN SMALL LETTER U WITH DIAERESIS AND GRAVE
ǝ 477 01DD   LATIN SMALL LETTER TURNED E
Ǟ 478 01DE   LATIN CAPITAL LETTER A WITH DIAERESIS AND MACRON
ǟ 479 01DF   LATIN SMALL LETTER A WITH DIAERESIS AND MACRON
Ǡ 480 01E0   LATIN CAPITAL LETTER A WITH DOT ABOVE AND MACRON
ǡ 481 01E1   LATIN SMALL LETTER A WITH DOT ABOVE AND MACRON
Ǣ 482 01E2   LATIN CAPITAL LETTER AE WITH MACRON
ǣ 483 01E3   LATIN SMALL LETTER AE WITH MACRON
Ǥ 484 01E4   LATIN CAPITAL LETTER G WITH STROKE
ǥ 485 01E5   LATIN SMALL LETTER G WITH STROKE
Ǧ 486 01E6   LATIN CAPITAL LETTER G WITH CARON
ǧ 487 01E7   LATIN SMALL LETTER G WITH CARON
Ǩ 488 01E8   LATIN CAPITAL LETTER K WITH CARON
ǩ 489 01E9   LATIN SMALL LETTER K WITH CARON
Ǫ 490 01EA   LATIN CAPITAL LETTER O WITH OGONEK
ǫ 491 01EB   LATIN SMALL LETTER O WITH OGONEK
Ǭ 492 01EC   LATIN CAPITAL LETTER O WITH OGONEK AND MACRON
ǭ 493 01ED   LATIN SMALL LETTER O WITH OGONEK AND MACRON
Ǯ 494 01EE   LATIN CAPITAL LETTER EZH WITH CARON
ǯ 495 01EF   LATIN SMALL LETTER EZH WITH CARON
ǰ 496 01F0   LATIN SMALL LETTER J WITH CARON
Ǳ 497 01F1   LATIN CAPITAL LETTER DZ
ǲ 498 01F2   LATIN CAPITAL LETTER D WITH SMALL LETTER Z
ǳ 499 01F3   LATIN SMALL LETTER DZ
Ǵ 500 01F4   LATIN CAPITAL LETTER G WITH ACUTE
ǵ 501 01F5 &gacute; LATIN SMALL LETTER G WITH ACUTE
Ƕ 502 01F6   LATIN CAPITAL LETTER HWAIR
Ƿ 503 01F7   LATIN CAPITAL LETTER WYNN
Ǹ 504 01F8   LATIN CAPITAL LETTER N WITH GRAVE
ǹ 505 01F9   LATIN SMALL LETTER N WITH GRAVE
Ǻ 506 01FA   LATIN CAPITAL LETTER A WITH RING ABOVE AND ACUTE
ǻ 507 01FB   LATIN SMALL LETTER A WITH RING ABOVE AND ACUTE
Ǽ 508 01FC   LATIN CAPITAL LETTER AE WITH ACUTE
ǽ 509 01FD   LATIN SMALL LETTER AE WITH ACUTE
Ǿ 510 01FE   LATIN CAPITAL LETTER O WITH STROKE AND ACUTE
ǿ 511 01FF   LATIN SMALL LETTER O WITH STROKE AND ACUTE
Ȁ 512 0200   LATIN CAPITAL LETTER A WITH DOUBLE GRAVE
ȁ 513 0201   LATIN SMALL LETTER A WITH DOUBLE GRAVE
Ȃ 514 0202   LATIN CAPITAL LETTER A WITH INVERTED BREVE
ȃ 515 0203   LATIN SMALL LETTER A WITH INVERTED BREVE
Ȅ 516 0204   LATIN CAPITAL LETTER E WITH DOUBLE GRAVE
ȅ 517 0205   LATIN SMALL LETTER E WITH DOUBLE GRAVE
Ȇ 518 0206   LATIN CAPITAL LETTER E WITH INVERTED BREVE
ȇ 519 0207   LATIN SMALL LETTER E WITH INVERTED BREVE
Ȉ 520 0208   LATIN CAPITAL LETTER I WITH DOUBLE GRAVE
ȉ 521 0209   LATIN SMALL LETTER I WITH DOUBLE GRAVE
Ȋ 522 020A   LATIN CAPITAL LETTER I WITH INVERTED BREVE
ȋ 523 020B   LATIN SMALL LETTER I WITH INVERTED BREVE
Ȍ 524 020C   LATIN CAPITAL LETTER O WITH DOUBLE GRAVE
ȍ 525 020D   LATIN SMALL LETTER O WITH DOUBLE GRAVE
Ȏ 526 020E   LATIN CAPITAL LETTER O WITH INVERTED BREVE
ȏ 527 020F   LATIN SMALL LETTER O WITH INVERTED BREVE
Ȑ 528 0210   LATIN CAPITAL LETTER R WITH DOUBLE GRAVE
ȑ 529 0211   LATIN SMALL LETTER R WITH DOUBLE GRAVE
Ȓ 530 0212   LATIN CAPITAL LETTER R WITH INVERTED BREVE
ȓ 531 0213   LATIN SMALL LETTER R WITH INVERTED BREVE
Ȕ 532 0214   LATIN CAPITAL LETTER U WITH DOUBLE GRAVE
ȕ 533 0215   LATIN SMALL LETTER U WITH DOUBLE GRAVE
Ȗ 534 0216   LATIN CAPITAL LETTER U WITH INVERTED BREVE
ȗ 535 0217   LATIN SMALL LETTER U WITH INVERTED BREVE
Ș 536 0218   LATIN CAPITAL LETTER S WITH COMMA BELOW
ș 537 0219   LATIN SMALL LETTER S WITH COMMA BELOW
Ț 538 021A   LATIN CAPITAL LETTER T WITH COMMA BELOW
ț 539 021B   LATIN SMALL LETTER T WITH COMMA BELOW
Ȝ 540 021C   LATIN CAPITAL LETTER YOGH
ȝ 541 021D   LATIN SMALL LETTER YOGH
Ȟ 542 021E   LATIN CAPITAL LETTER H WITH CARON
ȟ 543 021F   LATIN SMALL LETTER H WITH CARON
Ƞ 544 0220   LATIN CAPITAL LETTER N WITH LONG RIGHT LEG
ȡ 545 0221   LATIN SMALL LETTER D WITH CURL
Ȣ 546 0222   LATIN CAPITAL LETTER OU
ȣ 547 0223   LATIN SMALL LETTER OU
Ȥ 548 0224   LATIN CAPITAL LETTER Z WITH HOOK
ȥ 549 0225   LATIN SMALL LETTER Z WITH HOOK
Ȧ 550 0226   LATIN CAPITAL LETTER A WITH DOT ABOVE
ȧ 551 0227   LATIN SMALL LETTER A WITH DOT ABOVE
Ȩ 552 0228   LATIN CAPITAL LETTER E WITH CEDILLA
ȩ 553 0229   LATIN SMALL LETTER E WITH CEDILLA
Ȫ 554 022A   LATIN CAPITAL LETTER O WITH DIAERESIS AND MACRON
ȫ 555 022B   LATIN SMALL LETTER O WITH DIAERESIS AND MACRON
Ȭ 556 022C   LATIN CAPITAL LETTER O WITH TILDE AND MACRON
ȭ 557 022D   LATIN SMALL LETTER O WITH TILDE AND MACRON
Ȯ 558 022E   LATIN CAPITAL LETTER O WITH DOT ABOVE
ȯ 559 022F   LATIN SMALL LETTER O WITH DOT ABOVE
Ȱ 560 0230   LATIN CAPITAL LETTER O WITH DOT ABOVE AND MACRON
ȱ 561 0231   LATIN SMALL LETTER O WITH DOT ABOVE AND MACRON
Ȳ 562 0232   LATIN CAPITAL LETTER Y WITH MACRON
ȳ 563 0233   LATIN SMALL LETTER Y WITH MACRON
ȴ 564 0234   LATIN SMALL LETTER L WITH CURL
ȵ 565 0235   LATIN SMALL LETTER N WITH CURL
ȶ 566 0236   LATIN SMALL LETTER T WITH CURL
ȷ 567 0237 &jmath; LATIN SMALL LETTER DOTLESS J
ȸ 568 0238   LATIN SMALL LETTER DB DIGRAPH
ȹ 569 0239   LATIN SMALL LETTER QP DIGRAPH
Ⱥ 570 023A   LATIN CAPITAL LETTER A WITH STROKE
Ȼ 571 023B   LATIN CAPITAL LETTER C WITH STROKE
ȼ 572 023C   LATIN SMALL LETTER C WITH STROKE
Ƚ 573 023D   LATIN CAPITAL LETTER L WITH BAR
Ⱦ 574 023E   LATIN CAPITAL LETTER T WITH DIAGONAL STROKE
ȿ 575 023F   LATIN SMALL LETTER S WITH SWASH TAIL
ɀ 576 0240   LATIN SMALL LETTER Z WITH SWASH TAIL
Ɂ 577 0241   LATIN CAPITAL LETTER GLOTTAL STOP
ɂ 578 0242   LATIN SMALL LETTER GLOTTAL STOP
Ƀ 579 0243   LATIN CAPITAL LETTER B WITH STROKE
Ʉ 580 0244   LATIN CAPITAL LETTER U BAR
Ʌ 581 0245   LATIN CAPITAL LETTER TURNED V
Ɇ 582 0246   LATIN CAPITAL LETTER E WITH STROKE
ɇ 583 0247   LATIN SMALL LETTER E WITH STROKE
Ɉ 584 0248   LATIN CAPITAL LETTER J WITH STROKE
ɉ 585 0249   LATIN SMALL LETTER J WITH STROKE
Ɋ 586 024A   LATIN CAPITAL LETTER SMALL Q WITH HOOK TAIL
ɋ 587 024B   LATIN SMALL LETTER Q WITH HOOK TAIL
Ɍ 588 024C   LATIN CAPITAL LETTER R WITH STROKE
ɍ 589 024D   LATIN SMALL LETTER R WITH STROKE
Ɏ 590 024E   LATIN CAPITAL LETTER Y WITH STROKE
ɏ 591 024F   LATIN SMALL LETTER Y WITH STROKE
--------------------------------------------------------------------------------
*)
    if Tmp = '' then
      Res := Res + Texto[I]
    else
      Res := Res + '&#x' + Tmp + ';'
}
  end;
  Result := Res;
end;

function TUnDmkProcFunc.ConverteMinuscEmMaiusc(Texto: String): String;
var
  i: Integer;
begin
  for i := 1 to Length(Texto) do
  begin
    {
    if not (Texto[i] in (['a'..'z'])) then
      if not (Texto[i] in (['A'..'Z'])) then
        if not (Texto[i] in (['0'..'9'])) then
    }
{$IFDEF DELPHI12_UP}
    if not (CharInSet(Texto[i], (['a'..'z']))) then
      if not (CharInSet(Texto[i], (['A'..'Z']))) then
        if not (CharInSet(Texto[i], (['0'..'9']))) then
{$ELSE}
    if not (Texto[i] in (['a'..'z'])) then
      if not (Texto[i] in (['A'..'Z'])) then
        if not (Texto[i] in (['0'..'9'])) then
{$ENDIF}
          if Texto[i] = 'ç' then Texto[i] := 'Ç'
          else if Texto[i] = 'ã' then Texto[i] := 'Ã'
          else if Texto[i] = 'á' then Texto[i] := 'Á'
          else if Texto[i] = 'à' then Texto[i] := 'À'
          else if Texto[i] = 'â' then Texto[i] := 'Â'
          else if Texto[i] = 'é' then Texto[i] := 'É'
          else if Texto[i] = 'ê' then Texto[i] := 'Ê'
          else if Texto[i] = 'í' then Texto[i] := 'Í'
          else if Texto[i] = 'õ' then Texto[i] := 'Õ'
          else if Texto[i] = 'ó' then Texto[i] := 'Ó'
          else if Texto[i] = 'ô' then Texto[i] := 'Ô'
          else if Texto[i] = 'ú' then Texto[i] := 'Ú'
          else if Texto[i] = 'ü' then Texto[i] := 'Ü'
  end;
  Result := Texto;
end;

function TUnDmkProcFunc.LimpaTexto(Texto: String): String;
var
  i: Integer;
  Res: String;
begin
  Result := '';
  for i := 1 to Length(Texto) do
  begin
    if (CharInSet(Texto[i], (['a'..'z']))) then
     Result := Result + Texto[i]
    else
    if (CharInSet(Texto[i], (['A'..'Z']))) then
      Result := Result + Texto[i]
    else
    if (CharInSet(Texto[i], (['0'..'9']))) then
      Result := Result + Texto[i]
    else if Texto[i] = 'ç' then Result := Result + 'c'
    else if Texto[i] = 'ã' then Result := Result + 'a'
    else if Texto[i] = 'á' then Result := Result + 'a'
    else if Texto[i] = 'à' then Result := Result + 'a'
    else if Texto[i] = 'â' then Result := Result + 'a'
    else if Texto[i] = 'é' then Result := Result + 'e'
    else if Texto[i] = 'ê' then Result := Result + 'e'
    else if Texto[i] = 'í' then Result := Result + 'i'
    else if Texto[i] = 'õ' then Result := Result + 'o'
    else if Texto[i] = 'ó' then Result := Result + 'o'
    else if Texto[i] = 'ô' then Result := Result + 'o'
    else if Texto[i] = 'ú' then Result := Result + 'u'
    else if Texto[i] = 'ü' then Result := Result + 'u'
    else if Texto[i] = 'Ç' then Result := Result + 'C'
    else if Texto[i] = 'Ã' then Result := Result + 'A'
    else if Texto[i] = 'Á' then Result := Result + 'A'
    else if Texto[i] = 'À' then Result := Result + 'A'
    else if Texto[i] = 'Â' then Result := Result + 'A'
    else if Texto[i] = 'É' then Result := Result + 'E'
    else if Texto[i] = 'Ê' then Result := Result + 'E'
    else if Texto[i] = 'Í' then Result := Result + 'I'
    else if Texto[i] = 'Õ' then Result := Result + 'O'
    else if Texto[i] = 'Ó' then Result := Result + 'O'
    else if Texto[i] = 'Ô' then Result := Result + 'O'
    else if Texto[i] = 'Ú' then Result := Result + 'U'
    else if Texto[i] = 'Ü' then Result := Result + 'U'
    else ; // nada
  end;
end;

function TUnDmkProcFunc.CoordenadasIniciais(LeftCell, TopCell, RightCell,
  BottomCell, WidthImage, HeigthImage: Integer): MyArrayI02;
begin
  Result[1] := 0;
  Result[2] := 0;
  try
    Result[1]:= (((RightCell-LeftCell)-WidthImage) div 2) + LeftCell;
    Result[2]:= (((BottomCell-TopCell)-HeigthImage) div 2) + TopCell;
  except
    raise;
  end;
end;

procedure TUnDmkProcFunc.CopiaArq(const FileName, DestName: string);
  function HasAttr(const FileName: string; Attr: Word): Boolean;
  var
   FileAttr: Integer;
  begin
  {$WARN UNIT_PLATFORM OFF}
  {$WARN SYMBOL_PLATFORM OFF}
    FileAttr := FileGetAttr(FileName);
  {$WARN UNIT_PLATFORM ON}
  {$WARN SYMBOL_PLATFORM ON}
    if FileAttr = -1 then FileAttr := 0;
    Result := (FileAttr and Attr) = Attr;
  end;
var
  CopyBuffer: Pointer; { buffer for copying }
  BytesCopied: Longint;
  Source, Dest: Integer; { handles }
  Len: Integer;
  Destination: TFileName; { holder for expanded destination name }
  Pasta: String;
const
  ChunkSize: Longint = 8192; { copy in 8K chunks }
begin
  Destination := ExpandFileName(DestName); { expand the destination path }
  if HasAttr(Destination, faDirectory) then { if destination is a directory... }
  begin
    Len :=  Length(Destination);
    if Destination[Len] = '\' then
      Destination := Destination + ExtractFileName(FileName) { ...clone file name }
    else
      Destination := Destination + '\' + ExtractFileName(FileName); { ...clone file name }
  end;
GetMem(CopyBuffer, ChunkSize); { allocate the buffer }
  try
    Source := FileOpen(FileName, fmShareDenyWrite); { open source file }
    if Source < 0 then
    raise EFOpenError.CreateFmt(SFOpenError, [FileName]);
    try
      Pasta := ExtractFilePath(DestName);
      ForceDirectories(Pasta);
      Dest := FileCreate(Destination); { create output file; overwrite existing }
      if Dest < 0 then raise EFCreateError.CreateFmt(SFCreateError, [Destination]);
      try
        repeat
          BytesCopied := FileRead(Source, CopyBuffer^, ChunkSize); { read chunk }
          if BytesCopied > 0 then { if we read anything... }
            FileWrite(Dest, CopyBuffer^, BytesCopied); { ...write chunk }
        until BytesCopied < ChunkSize; { until we run out of chunks }
      finally
        FileClose(Dest); { close the destination file }
      end;
    finally
      FileClose(Source); { close the source file }
    end;
  finally
    FreeMem(CopyBuffer, ChunkSize); { free the buffer }
  end;
end;

procedure TUnDmkProcFunc.CorFundoEFonte(const Tema: TdmkTemaCores;
  var CorFundo, CorFonte: TColor; const Inverte: Boolean);
var
  x: TColor;
begin
  case Tema of
    // Office Excel 2007
    dmkcorXcel2k7Saida:
    begin
      CorFundo := RGB(242, 242, 242); // Cinza clarinho
      CorFonte := RGB(063, 063, 063); // Cinza escuro
    end;
    dmkcorXcel2k7Bom:
    begin
      CorFundo := RGB(198, 239, 206); // Verde água
      CorFonte := RGB(000, 097, 000); // Verde escuro
    end;
    dmkcorXcel2k7Neutra:
    begin
      CorFundo := RGB(255, 235, 156); // Laranja creme
      CorFonte := RGB(156, 101, 000); // Castanho médio
    end;
    dmkcorXcel2k7Incorreto:
    begin
      CorFundo := RGB(255, 199, 206); // Rosa
      CorFonte := RGB(156, 000, 006); // Rubro
    end;
    // Marco
    dmkcorBrancoInvisivel:
    begin
      CorFundo := RGB(255, 255, 255); // Branco
      CorFonte := RGB(255, 255, 255); // Branco
    end;
    dmkcorPretoEBranco:
    begin
      CorFundo := RGB(255, 255, 255); // Branco
      CorFonte := RGB(000, 000, 000); // Preto
    end;
    dmkcorNotaVermelho:
    begin
      CorFundo := RGB(255, 255, 204); // Amarelo Creme
      CorFonte := RGB(255, 000, 000); // Vermelho
    end;
    dmkcorEnfaseAzul01:
    begin
      CorFundo := RGB(255, 255, 255); // Branco
      CorFonte := RGB(079, 129, 174); // Turqueza médio-escuro
    end;
    dmkcorEnfaseAzul02:
    begin
      CorFundo := RGB(219, 229, 241); // Azul clarinho (Roxo Dermatek?)
      CorFonte := RGB(070, 065, 115); // Roxo (Dermatek?);
    end;
    dmkcorEnfaseAzul03:
    begin
      CorFonte := RGB(255, 255, 255); // Branco
      CorFundo := RGB(149, 179, 215); // Azul plano de fundo (enfase1 60% xcell 2007)
    end;
    dmkcorEnfaseAzul04:
    begin
      CorFonte := RGB(255, 255, 255); // Branco
      CorFundo := RGB(178, 161, 199); // Roxo plano de fundo (enfase4 60% xcell 2007)
    end;
    dmkcorEnfaseAzul05:
    begin
      CorFonte := RGB(255, 255, 255); // Branco
      CorFundo := RGB(204, 192, 218); // Roxo plano de fundo (enfase4 40% xcell 2007)
    end;
  end;
  if Inverte then
  begin
    x := CorFonte;
    CorFonte := corFundo;
    CorFundo := x;
  end;
end;

function TUnDmkProcFunc.ObtemDataInserir: TDateTime;
begin
  if VAR_DATAINSERIR < Date - 366 then
    VAR_DATAINSERIR := Date;
  Result := VAR_DATAINSERIR;
end;

function TUnDmkProcFunc.ObtemDataStr(Date: TDateTime; MiliSegundos: Boolean): String;
var
  AA, MM, DD, HH, NN, SS, ZZZ: String;
  Ano, Mes, Dia, Hora, Min, Seg, MSeg : Word;
begin
  DecodeDate(Date, Ano, Mes, Dia);
  AA := FormatFloat('0', Ano);
  if Length(AA) > 2 then AA := AA[Length(AA)-1]+AA[Length(AA)];
  if Mes < 10 then MM := '0'+FormatFloat('0', Mes) else MM := FormatFloat('0', Mes);
  // Estraga ordem alfabetica
  //MM := Copy(dmkPF.VerificaMes(Mes, True), 1, 3);
  if Dia < 10 then DD := '0'+FormatFloat('0', Dia) else DD := FormatFloat('0', Dia);
  DecodeTime(Now(), Hora, Min, Seg, MSeg);
  if Hora < 10 then HH := '0'+FormatFloat('0', Hora) else HH := FormatFloat('0', Hora);
  if Min < 10 then NN := '0'+FormatFloat('0', Min) else NN := FormatFloat('0', Min);
  if Seg < 10 then SS := '0'+FormatFloat('0', Seg) else SS := FormatFloat('0', Seg);
  if MiliSegundos then
  begin
    if MSeg < 10 then ZZZ := '00'+FormatFloat('0', MSeg)
    else if MSeg < 100 then ZZZ := '0'+FormatFloat('0', MSeg)
    else ZZZ := FormatFloat('0', Seg);
    ZZZ := '_'+ZZZ;
  end else ZZZ := '';
  Result := AA+MM+DD+'_'+HH+NN+SS+ZZZ;
end;

function TUnDmkProcFunc.ObtemDermatekDiretorio: String;
var
  Dir, Bar, Drive: String;
begin
  Dir   := ExtractFilePath(Application.ExeName);
  Drive := ExtractFileDrive(Dir);
  //
  if Pos('/', Dir) > 0 then
    Bar := '/'
  else
    Bar := '\';
  //
  Result := Drive + Bar + 'Dermatek' + Bar;
end;

function TUnDmkProcFunc.ObtemDiretorioDesktop(AllUsers: Boolean): String;
var
  PIDList: PItemIDList;
  Buffer: array [0..MAX_PATH-1] of Char;
begin
  Result := '';
  //
  if AllUsers then
    SHGetSpecialFolderLocation(Application.Handle, CSIDL_COMMON_DESKTOPDIRECTORY, PIDList)
  else
    SHGetSpecialFolderLocation(Application.Handle, CSIDL_DESKTOP, PIDList);
  //
  if Assigned(PIDList) then
  begin
    if SHGetPathFromIDList(PIDList, Buffer) then
    begin
      Result := Buffer;
    end;
  end;
end;

function TUnDmkProcFunc.ObtemEspacoLivreDrive(ListaDrives: TStringList;
  Driver: String): Int64;
var
  Index: Integer;
begin
  if ListaDrives.Count > 0 then
  begin
    if ListaDrives.Find(Driver + PathDelim, Index) then
    begin
      Result := DiskFree(Index);
    end;
  end else
    Result := -1;
end;

function TUnDmkProcFunc.ObtemEspacoTotalDrive(ListaDrives: TStringList;
  Driver: String): Int64;
var
  Index: Integer;
begin
  if ListaDrives.Count > 0 then
  begin
    if ListaDrives.Find(Driver + PathDelim, Index) then
    begin
      Result := DiskSize(Index);
    end;
  end else
    Result := -1;
end;

function TUnDmkProcFunc.Criptografia(mStr, mChave: string): string;
var
  i, TamanhoString, pos, PosLetra, TamanhoChave: Integer;
begin
  Result := mStr;
  TamanhoString := Length(mStr);
  TamanhoChave := Length(mChave);
  for i := 1 to TamanhoString do
  begin
    pos := (i mod TamanhoChave);
    if pos = 0 then
      pos := TamanhoChave;
    posLetra := ord(Result[i]) xor ord(mChave[pos]);
    if posLetra = 0 then
      posLetra := ord(Result[i]);
    Result[i] := chr(posLetra);
  end;
end;

function TUnDmkProcFunc.DatadeAnoMes(AnoMes, Dia, AddDias: Integer): TDateTime;
var
  Ano, Mes: Word;
begin
  Ano := AnoMes div 100;
  Mes := AnoMes mod 100;
  Result := EncodeDate(Ano, Mes, Dia) + AddDias;
end;

function TUnDmkProcFunc.DataJuliana_HSBC(Data: TDateTime): String;
var
  Ano, Mes, Dia, Y1: Word;
  Dias: Integer;
  Jan1: TDateTime;
begin
  DecodeDate(Data, Ano, Mes, Dia);
  Jan1 := EncodeDate(Ano, 1, 1);
  Dias := Trunc(Int(Data) - Int(Jan1) + 1);
  Y1   := Ano mod 10;
  //
  Result := FormatFloat('000', Dias) + FormatFloat('0', Y1);
end;

function TUnDmkProcFunc.DatasIniFimDoPeriodo(const AnoMes: Integer; var DtIni,
  DtFim: TDateTime): Boolean;
begin
  Result := False;
  DtIni  := 0;
  DtFim  := 0;
  if AnoMes > 0 then
  begin
    DtIni  := dmkPF.DatadeAnoMes(AnoMes, 1, 0);
    DtFim  := IncMonth(DtIni, 1) - (1 / 86400);
    Result := True;
  end;
  //Geral.MB_Info(FormatDateTime('dd/mm/yy hh:nn:ss:zzz', DtFim)); OK!
end;

function TUnDmkProcFunc.DataToAnoMes(Data: TDateTime): Integer;
var
  Ano, Mes, Dia: Word;
begin
  Decodedate(Data, Ano, Mes, Dia);
  Result := (Ano * 100) + Mes;
end;

function TUnDmkProcFunc.DataToAnoEMes(Data: TDateTime): String;
var
  Ano, Mes, Dia: Word;
begin
  Decodedate(Data, Ano, Mes, Dia);
  Result := FFD(Mes, 2, siPositivo) + '/'  + FFD(Ano, 4, siPositivo);
end;

function TUnDmkProcFunc.DataTypeToString(Campo: TField): String;
begin
  (*TFieldType = (ftUnknown, ftString, ftSmallint, ftInteger, ftWord,
    ftBoolean, ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime,
    ftBytes, ftVarBytes, ftAutoInc, ftBlob, ftMemo, ftGraphic, ftFmtMemo,
    ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftFixedChar, ftWideString,
    ftLargeint, ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob,
    ftVariant, ftInterface, ftIDispatch, ftGuid);*)
  case Campo.DataType of
    ftString       : Result := Campo.AsString;
    ftSmallint     : Result := FormatFloat('0', Campo.AsInteger);
    ftInteger      : Result := FormatFloat('0', Campo.AsInteger);
    ftWord         : Result := FormatFloat('0', Campo.AsInteger);
    ftBoolean      : Result := FormatFloat('0', Geral.BoolToInt(Campo.AsBoolean));
    ftFloat        : Result := FloatToStrF(Campo.AsFloat, ffFixed, 15, Campo.DataSize);
    ftCurrency     : Result := FloatToStrF(Campo.AsFloat, ffFixed, 15, Campo.DataSize);

    ftDate         : Result := FormatDateTime('yyyy-mm-dd', Campo.AsDateTime);
    ftTime         : Result := FormatDateTime('00:00:00', Campo.AsDateTime);
    ftDateTime     : Result := FormatDateTime('yyyy-mm-dd 00:00:00', Campo.AsDateTime);

    ftMemo         : Result := Campo.AsString;

    ftLargeInt     : Result := FormatFloat('0', Campo.AsInteger);

    else
    begin
      Geral.MB_Erro('"DataType" desconhecido na função "DataTypeToString"');
      Result := '';
    end;
  end;
  if Campo.DataType in ([ftFloat, ftCurrency]) then
  begin
      if pos(',', Result) > 0 then Result[pos(',', Result)] := '.';
  end;
end;

function TUnDmkProcFunc.DataZero(Data: TDateTime): Boolean;
var
 Dt0: TDateTime;
begin
  //Result := Data <= StrToDate('1899/12/31 12:00:00');
  Dt0 := EncodeDateTime(1899, 12, 31, 12, 0, 0, 0);
  Result := Data <= Dt0;
end;

function TUnDmkProcFunc.DateTime_UTCToMyTimeZone(DataHora: TDateTime): TDateTime;
begin
  Result := TTimeZone.Local.ToLocalTime(DataHora);
end;

function TUnDmkProcFunc.DateTime_MyTimeZoneToUTC(DataHora: TDateTime): TDateTime;
begin
  Result := TTimeZone.Local.ToUniversalTime(DataHora);
end;

function TUnDmkProcFunc.DDS(Data: String): String;
var
  Ano, Mes, Dia: String;
begin
  Dia := Copy(Data, 1, 2);
  Mes := Copy(Data, 4, 2);
  Ano := Copy(Data, 7, 4);
  Result := Ano + '/' + Mes + '/' + Dia;
end;

function TUnDmkProcFunc.DecodeDocumento(const Documento: String;
  var Serie: String; var Cheque: Double): String;
var
  DocTxt: String;
  n, i, step: Integer;
begin
  DocTxt := '';
  Serie := '';
  n := Length(Documento);
  if n > 0 then
  begin
    Step := 0;
    for i := 1 to n do
    begin
      //if Documento[i] in ['0'..'9'] then
{$IFDEF DELPHI12_UP}
      if CharInSet(Documento[i], ['0'..'9']) then
{$ELSE}
      if Documento[i] in ['0'..'9'] then
{$ENDIF}
      begin
        if Step < 3 then
        begin
          Step := 2;
          DocTxt := DocTxt + Documento[i];
        end else Serie := Serie + Documento[i];
      end else begin
        if Step < 2 then Step := 1 else Step := 3;
        Serie := Serie + Documento[i];
      end;
    end;
  end;
  Cheque := Geral.IMV(DocTxt);
end;

function TUnDmkProcFunc.DefineJuricoSigla(Status: Integer): String;
begin
  case Status of
    0: Result := '';
    1: Result := 'EM';
    2: Result := 'PR';
    3: Result := 'CO';
    4: Result := 'PC';
    else Result := '??';
  end;
end;

function TUnDmkProcFunc.DefineJuricoTexto(Status: Integer): String;
begin
  case Status of
    0: Result := 'Sem restrições';
    1: Result := 'Em processo de protesto';
    2: Result := 'Protestado';
    3: Result := 'Com ação de cobrança';
    4: Result := 'Protestado + ação cobrança';
    else Result := '??';
  end;
end;

function TUnDmkProcFunc.DelDir(dir: string): Boolean;
var
  fos: TSHFileOpStruct;
begin
  //Result := False;
  try
    Screen.Cursor := crHourGlass;
    //
    ZeroMemory(@fos, SizeOf(fos));
    with fos do
    begin
      wFunc  := FO_DELETE;
      fFlags := FOF_SILENT or FOF_NOCONFIRMATION;
      pFrom  := PChar(dir + #0);
      //
      Application.ProcessMessages;
    end;
    Result := (0 = ShFileOperation(fos));
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnDmkProcFunc.DesvioPadraoPopulacionalArrIntDbl(const Itens: ItensArray;
const Valores: ValrsArray; var RangeMin, RangeMid, RangeMax: Double): Double;
var
  I, Qtde: Integer;
  SomaItens: Double;
  Desvio: Double;
  SomaSimples, MediaSimples, SomaDesvios, MediaDesvios: Extended;
begin
  Qtde := Length(Itens);
  // Soma Simples
  SomaItens := 0;
  SomaSimples := 0;
  for I := Low(Itens) to High(Itens) do
  begin
    SomaItens   := SomaItens + Valores[I];
    SomaSimples := SomaSimples + (Itens[I] * Valores[I]);
  end;
  // Média Simples
  MediaSimples := SomaSimples / SomaItens;
  // Soma Desvios
  SomaDesvios := 0;
  for I := Low(Itens) to High(Itens) do
  begin
    Desvio := Sqr(Itens[I] - MediaSimples) * Valores[I];
    SomaDesvios := SomaDesvios + Desvio;
    //
  end;
  MediaDesvios := SomaDesvios / SomaItens;
  Result   := Sqrt(MediaDesvios);
  RangeMin := MediaSimples - Result;
  RangeMid := MediaSimples;
  RangeMax := MediaSimples + Result;
end;

function TUnDmkProcFunc.DesvioPadraoPopulacionalLista(const n: array of Double;
  var RangeMin, RangeMid, RangeMax: Double): Double;
var
  I, Qtde: Integer;
  Desvio: Double;
  SomaSimples, MediaSimples, SomaDesvios, MediaDesvios: Extended;
begin
{
Desvio padrão populacional[editar | editar código-fonte]
Para um conjunto de dados finito, o desvio padrão é calculado a partir da raiz
quadrada da média dos desvios entre os valores e a média dos valores dos dados
elevado ao quadrado.

Sejam as notas de 8 estudantes (n = 8 ) 2, 4, 4, 4, 5, 5, 7, 9.

A média das notas dos 8 estudantes é:
(2 + 4 + 4 + 4 + 5 + 5 + 7 + 9) / 8 = 5.

Os desvios entre as notas e a média das notas elevados ao quadrado são:
(2 - 5)² = (-3)² =  9
(4 - 5)² = (-1)² =  1
(4 - 5)² = (-1)² =  1
(4 - 5)² = (-1)² =  1
(5 - 5)² = ( 0)² =  0
(5 - 5)² = ( 0)² =  0
(7 - 5)² = (+2)² =  4
(9 - 5)² = (+4)² = 16

A variância ou a média de todos os valores é:
9 + 1 + 1 + 1 + 0 + 0 + 4 + 16
______________________________ = 4
               8 = 4

O desvio padrão ou a raiz quadrada da variância é
Srqt(4) = 2

Isto é, o desvio padrão é igual a 2.

}
  Qtde := Length(n);
  // Soma Simples
  SomaSimples := 0;
  for I := Low(n) to High(n) do
    SomaSimples := SomaSimples + n[I];
  // Média Simples
  MediaSimples := SomaSimples / Qtde;
  // Soma Desvios
  SomaDesvios := 0;
  for I := Low(n) to High(n) do
  begin
    Desvio := Sqr(n[I] - MediaSimples);
    SomaDesvios := SomaDesvios + Desvio;
    //
  end;
  MediaDesvios := SomaDesvios / Qtde;
  Result := Sqrt(MediaDesvios);
  RangeMin := MediaSimples - Result;
  RangeMid := MediaSimples;
  RangeMax := MediaSimples + Result;
end;

function TUnDmkProcFunc.DigitoVerificardorDmk3CasasInteger(
  Numero: Integer): Integer;
var
  Tripa, Res: String;
  I, Val: Integer;
begin
  Result := 0;
  Res := '';
  Tripa := IntToStr(Numero);
  while Length(Tripa) < 9 do
    Tripa := '0' + Tripa;
  while Length(Tripa) > 9 do
    Tripa := Copy(Tripa, 2);
  Val := 0;
  for I := 1 to 9 do
  begin
    case I of
      1: Val := Val + (Geral.IMV(Tripa[I]) * 11);
      2: Val := Val + (Geral.IMV(Tripa[I]) * 11);
      3: Val := Val + (Geral.IMV(Tripa[I]) * 11);
      4: Val := Val + (Geral.IMV(Tripa[I]) * 11);
      5: Val := Val + (Geral.IMV(Tripa[I]) * 11);
      //
      6: Val := Val + (Geral.IMV(Tripa[I]) * 13);
      7: Val := Val + (Geral.IMV(Tripa[I]) * 13);
      8: Val := Val + (Geral.IMV(Tripa[I]) * 13);
      //
      9: Val := Val + (Geral.IMV(Tripa[I]) * 17);
    end;
  end;
  Tripa := IntToStr(Val);
  Tripa := IntToStr(Val);
  while Length(Tripa) < 3 do
    Tripa := '0' + Tripa;
  while Length(Tripa) > 3 do
    Tripa := Copy(Tripa, 2);
  Res := Tripa[3] + Tripa[2] + Tripa[1];
  Result := Geral.IMV(Res);
end;

function TUnDmkProcFunc.Divide2(Dividendo, Divisor: Double): Double;
begin
  if Divisor = 0 then
    Result := 0
  else
    Result := Dividendo / Divisor;
end;

// Inspirado no System.DateUtils.MonthsBetween(...
function TUnDmkProcFunc.Dmk_MonthsBetween(const ANow, AThen: TDateTime): Double;
{
  function DateTimeToMilliseconds(const ADateTime: TDateTime): Int64;
  var
    LTimeStamp: TTimeStamp;
  begin
    LTimeStamp := DateTimeToTimeStamp(ADateTime);
    Result := LTimeStamp.Date;
    Result := (Result * MSecsPerDay) + LTimeStamp.Time;
  end;
const
  CMillisPerDay = Int64(MSecsPerSec * SecsPerMin * MinsPerHour * HoursPerDay);
begin
  Result := Abs(DateTimeToMilliseconds(ANow) - DateTimeToMilliseconds(AThen))
    //div Round(CMillisPerDay * ApproxDaysPerMonth);
    / (CMillisPerDay * ApproxDaysPerMonth);
}
var
  AnoIni, AnoFim, MesIni, MesFim, DiaIni, DiaFim: Word;
  MesesA, MesesM, MesesD: Double;
begin
  DecodeDate(ANow, AnoFim, MesFim, DiaFim);
  DecodeDate(AThen, AnoIni, MesIni, DiaIni);
  //
  MesesA := (AnoFim - AnoIni) * 12;
  MesesM := MesFim - MesIni;
  MesesD := 0;
  if DiaIni <> DiaFim then
    MesesD := (DiaFim - DiaIni) / 30;
  Result := MesesA + MesesM + MesesD;
end;

(*
procedure TUnDmkProcFunc.DiretivasCompilador();
begin
//------------------------------------------------------------------------------
// DELPHIX_UP à partir dos mapeamentos DELPHIX
//------------------------------------------------------------------------------

{$IFDEF DELPHI17}
  {$DEFINE DELPHI17_UP}
  {$DEFINE DELPHI16_UP}
  {$DEFINE DELPHI15_UP}
  {$DEFINE DELPHI14_UP}
  {$DEFINE DELPHI13_UP}
  {$DEFINE DELPHI12_UP}
  {$DEFINE DELPHI11_UP}
  {$DEFINE DELPHI10_UP}
  {$DEFINE DELPHI9_UP}
  {$DEFINE DELPHI8_UP}
  {$DEFINE DELPHI7_UP}
  {$DEFINE DELPHI6_UP}
  {$DEFINE DELPHI5_UP}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI16}
  {$DEFINE DELPHI16_UP}
  {$DEFINE DELPHI15_UP}
  {$DEFINE DELPHI14_UP}
  {$DEFINE DELPHI13_UP}
  {$DEFINE DELPHI12_UP}
  {$DEFINE DELPHI11_UP}
  {$DEFINE DELPHI10_UP}
  {$DEFINE DELPHI9_UP}
  {$DEFINE DELPHI8_UP}
  {$DEFINE DELPHI7_UP}
  {$DEFINE DELPHI6_UP}
  {$DEFINE DELPHI5_UP}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI15}
  {$DEFINE DELPHI15_UP}
  {$DEFINE DELPHI14_UP}
  {$DEFINE DELPHI13_UP}
  {$DEFINE DELPHI12_UP}
  {$DEFINE DELPHI11_UP}
  {$DEFINE DELPHI10_UP}
  {$DEFINE DELPHI9_UP}
  {$DEFINE DELPHI8_UP}
  {$DEFINE DELPHI7_UP}
  {$DEFINE DELPHI6_UP}
  {$DEFINE DELPHI5_UP}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI14}
  {$DEFINE DELPHI14_UP}
  {$DEFINE DELPHI13_UP}
  {$DEFINE DELPHI12_UP}
  {$DEFINE DELPHI11_UP}
  {$DEFINE DELPHI10_UP}
  {$DEFINE DELPHI9_UP}
  {$DEFINE DELPHI8_UP}
  {$DEFINE DELPHI7_UP}
  {$DEFINE DELPHI6_UP}
  {$DEFINE DELPHI5_UP}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI12}
//  {$DEFINE DELPHI12_UP} // Essa linha dispara um erro na compilação no D2009 - "ACBrBase.pas(247) Error: E2003 Undeclared identifier: 'Start'"
  {$DEFINE DELPHI11_UP}
  {$DEFINE DELPHI10_UP}
  {$DEFINE DELPHI9_UP}
  {$DEFINE DELPHI8_UP}
  {$DEFINE DELPHI7_UP}
  {$DEFINE DELPHI6_UP}
  {$DEFINE DELPHI5_UP}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI11}
  {$DEFINE DELPHI11_UP}
  {$DEFINE DELPHI10_UP}
  {$DEFINE DELPHI9_UP}
  {$DEFINE DELPHI8_UP}
  {$DEFINE DELPHI7_UP}
  {$DEFINE DELPHI6_UP}
  {$DEFINE DELPHI5_UP}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI10}
  {$DEFINE DELPHI10_UP}
  {$DEFINE DELPHI9_UP}
  {$DEFINE DELPHI8_UP}
  {$DEFINE DELPHI7_UP}
  {$DEFINE DELPHI6_UP}
  {$DEFINE DELPHI5_UP}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI9}
  {$DEFINE DELPHI9_UP}
//  {$DEFINE DELPHI8_UP} // Essa linha dispara um erro na compilação no D2005 - "ACBrRFD.pas(1861) Error: E2034 Too many actual parameters"
  {$DEFINE DELPHI7_UP}
  {$DEFINE DELPHI6_UP}
  {$DEFINE DELPHI5_UP}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI8}
  {$DEFINE DELPHI8_UP}
  {$DEFINE DELPHI7_UP}
  {$DEFINE DELPHI6_UP}
  {$DEFINE DELPHI5_UP}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI7}
  {$DEFINE DELPHI7_UP}
  {$DEFINE DELPHI6_UP}
  {$DEFINE DELPHI5_UP}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI6}
  {$DEFINE DELPHI6_UP}
  {$DEFINE DELPHI5_UP}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI5}
  {$DEFINE DELPHI5_UP}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI4}
  {$DEFINE DELPHI4_UP}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI3}
  {$DEFINE DELPHI3_UP}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI2}
  {$DEFINE DELPHI2_UP}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

{$IFDEF DELPHI1}
  {$DEFINE DELPHI1_UP}
{$ENDIF}

  VAR_DIRETIVAS_COMPILER_VERSION_DEFINIDAS := True;
end;
*)

function TUnDmkProcFunc.DuplicaAspasDuplas(Atual: String): String;
var
  i: integer;
begin
  Result := '';
  if Atual = '' then exit;
  for i := 1 to Length(Atual) do
  begin
    if Atual[i] = '"' then Result := Result + '""' else
                           Result := Result + Atual[i];
  end;
  Result := Geral.Substitui(Result, '""""', '""');
  Result := Geral.Substitui(Result, '"""', '""');
end;

function TUnDmkProcFunc.SimplificaBarras(Atual: String): String;
var
  i: integer;
begin
  Result := '';
  //
  if Atual = '' then
    Exit;
  //
  if (Pos('\\', Atual) > 0) then
  begin
    Result := StringReplace(Atual, '\\', '\', [rfReplaceAll, rfIgnoreCase]);
    //
    if (Pos('\', Atual) = 1) then
      Result := '\' + Result;
  end else
  if (Pos('//', Atual) > 0) then
  begin
    Result := StringReplace(Atual, '//', '/', [rfReplaceAll, rfIgnoreCase]);
    //
    if (Pos('/', Atual) = 1) then
      Result := '/' + Result;
  end else
    Result := Atual;
end;

function TUnDmkProcFunc.DuplicaBarras(Atual: String): String;
var
  i: integer;
begin
  Result := '';
  if Atual = '' then exit;
  for i := 1 to Length(Atual) do
  begin
    if Atual[i] = '\' then Result := Result + '\\' else
    if Atual[i] = '/' then Result := Result + '//' else
                           Result := Result + Atual[i];
  end;
end;

function TUnDmkProcFunc.DuplicaSlash(Path: String): String;
var
  Txt: String;
  i: Integer;
begin
  for i := 1 to length(Path) do
  begin
    if Path[i] = '\' then Txt := Txt + '\\' else Txt := Txt + Path[i];
  end;
  Result := Txt;
end;

function TUnDmkProcFunc.Duplicata(Texto: String): MyArrayT_3;
var
  i: Integer;
  Parcela: Boolean;
  a,l,b: String;
begin
  Result[0] := CO_VAZIO;
  Result[1] := CO_VAZIO;
  Result[2] := CO_VAZIO;
  Parcela := False;
  try
    if Trim(Texto) = CO_VAZIO then Texto := '0';
    for i := 1 to Length(Texto) do
    begin
      if (Texto[i] = '-') or (Texto[i] = '/')  or (Texto[i] = '.')  then
      begin
        l := Texto[i];
        Parcela := True;
      end else begin
        //if Texto[i] in ['0'..'9'] then
        //begin
          if Parcela then b := b + Texto[i]
          else            a := a + Texto[i]
        //end;
      end;
    end;
    Result[0] := l;
    Result[1] := a;
    Result[2] := b;
  except
    Raise EAbort.Create('O texto '+Texto+' é um número inválido.');
  end;
end;

function TUnDmkProcFunc.DuplicataIncrementa(Texto: String;
  Incremento: Integer): String;
var
  i, Par: Integer;
  Dup: MyArrayT_3;
  fmt: String;
begin
  Dup := Duplicata(Texto);
  fmt := '0';
  if Dup[0] <> CO_VAZIO then
  begin
    for i := 1 to Length(Dup[2]) -1 do
      fmt := fmt + '0';
    Par := Geral.IMV(Dup[2])+Incremento;
    if Par < 0 then Par := 0;
    Result := Dup[1]+Dup[0]+FormatFloat(fmt, Par);
  end else begin
    for i := 1 to Length(Dup[1]) -1 do
      fmt := fmt + '0';
    Par := Geral.IMV(Dup[1])+Incremento;
    if Par < 0 then Par := 0;
    Result := FormatFloat(fmt, Par);
  end;
end;

function TUnDmkProcFunc.EditDistance_Levenshtein(s, t: string): Integer;
var
  d : array of array of integer;
  i,j,cost : integer;
begin
  // Compara a distância ( distancia ) entre duas palavras!
  {
  Compute the edit-distance between two strings.
  Algorithm and description may be found at either of these two links:
  http://en.wikipedia.org/wiki/Levenshtein_distance
  http://www.google.com/search?q=Levenshtein+distance
  }
  try
    //initialize our cost array
    SetLength(d,Length(s)+1);
    for i := Low(d) to High(d) do begin
      SetLength(d[i],Length(t)+1);
    end;

    for i := Low(d) to High(d) do begin
      d[i,0] := i;
      for j := Low(d[i]) to High(d[i]) do begin
        d[0,j] := j;
      end;
    end;

    //store our costs in a 2-d grid
    for i := Low(d)+1 to High(d) do begin
      for j := Low(d[i])+1 to High(d[i]) do begin
        if s[i] = t[j] then begin
          cost := 0;
        end
        else begin
          cost := 1;
        end;

        //to use "Min", add "Math" to your uses clause!
        d[i,j] := Min(Min(
                   d[i-1,j]+1,      //deletion
                   d[i,j-1]+1),     //insertion
                   d[i-1,j-1]+cost  //substitution
                   );
      end;  //for j
    end;  //for i

    //now that we've stored the costs, return the final one
    Result := d[Length(s),Length(t)];
  finally
    //cleanup
    for i := Low(d) to High(d) do begin
      for j := Low(d[i]) to High(d[i]) do begin
        SetLength(d[i],0);
      end;  //for j
    end;  //for i
    SetLength(d,0);
  end;  //try-finally
end;

function TUnDmkProcFunc.EhControlF(Sender: TObject; var Key: Word;
  Shift: TShiftState): Boolean;
begin
  Result :=
    (Shift = [ssCtrl])
  and
    ((Key = Ord('F')) or (Key = Ord('f')));
end;

function TUnDmkProcFunc.EhNumeroOuSinalOuSeparador(Caracter: Word): Boolean;
begin
  Result := False;
  //if (char(Caracter) in (['0'..'9'])) then Result := True
{$IFDEF DELPHI12_UP}
  if (CharInSet(char(Caracter), (['0'..'9']))) then Result := True
{$ELSE}
  if (char(Caracter) in (['0'..'9'])) then Result := True
{$ENDIF}
  else if (char(Caracter) = '-') then Result := True
  else if (char(Caracter) = '+') then Result := True
  else if (char(Caracter) = '.') then Result := True
  else if (char(Caracter) = ',') then Result := True
end;

function TUnDmkProcFunc.EscolhaDe2Dta(Verdade: Boolean; OpcaoV,
  OpcaoF: TDateTime): TDateTime;
begin
  if Verdade then
    Result := OpcaoV
  else
    Result := OpcaoF;
end;

function TUnDmkProcFunc.EscolhaDe2Int(Verdade: Boolean; OpcaoV,
  OpcaoF: Integer): Integer;
begin
  if Verdade then
    Result := OpcaoV
  else
    Result := OpcaoF;
end;

function TUnDmkProcFunc.EscolhaDe2Str(Verdade: Boolean; OpcaoV,
  OpcaoF: String): String;
begin
  if Verdade then Result := OpcaoV else Result := OpcaoF;
end;

function TUnDmkProcFunc.EscolhaDe3Int(Val1, Val2: Double; Maior, Igual,
  Menor: Integer): Integer;
begin
  if Val1 > Val2 then Result := Maior else
  if Val1 < Val2 then Result := Menor else
                      Result := Igual;
end;

function TUnDmkProcFunc.EstadoJanelaToHWND_CmdShow(
  EstadoJanela: Integer): Integer;
begin
  case EstadoJanela of
    //Padrão do app
    0: Result := SW_SHOW;
    //Normal
    1: Result := SW_NORMAL;
    //Maximizada
    2: Result := SW_MAXIMIZE;
    //Minimizada
    3: Result := SW_MINIMIZE;
    //Oculta
    4: Result := SW_HIDE;
  end;
end;

procedure TUnDmkProcFunc.ExcluiSQLsDermatek();
var
  SearchRec: TSearchRec;
  Result: Integer;
  Name: String;
begin
  Name := CO_DERMATEK_SISTEMA;
  Result := FindFirst(Name + '*.sql', faAnyFile, SearchRec);
  while Result = 0 do
  begin
    System.SysUtils.DeleteFile(Name+SearchRec.Name);
    Result := FindNext(SearchRec);
  end;
end;

function TUnDmkProcFunc.ExecutavelEstaInstalado(Descricao: String): Boolean;
const
  UNINST_PATH = 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall';
var
  Reg: TRegistry;
  SubKeys: TStringList;
  //ListItem: TlistItem;
  i: integer;
  sDisplayName, sUninstallString: string;
begin
  Result := False;
  Reg := TRegistry.Create(KEY_ALL_ACCESS);
  with Reg do
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKeyReadOnly(UNINST_PATH) then
      begin
        SubKeys := TStringList.Create;
        try
          GetKeyNames(SubKeys);
          CloseKey;
          for i := 0 to subKeys.Count - 1 do
            if OpenKeyReadOnly(Format('%s\%s', [UNINST_PATH, SubKeys[i]])) then
              try
                sDisplayName     := ReadString('DisplayName');
                sUninstallString := ReadString('UninstallString');
                if sDisplayName <> '' then
                begin
                  if Length(sDisplayName) > 11 then
                    if Uppercase(Copy(sDisplayName, 1, 12)) =
                    Uppercase(Descricao) then
                    begin
                      VAR_MYSQLSERVERDISPLAYNAME := sDisplayName;
                      Result := True;
                      Break;
                    end;
                end;
              finally
                CloseKey;
              end;
        finally
          SubKeys.Free;
        end;
      end;
    finally
      CloseKey;
      Free;
    end;
end;

function TUnDmkProcFunc.ExecutavelEstaRodando(Executavel: String): Boolean;
var
  List: TStringList;
  I: Integer;
begin
  Result := False;
  List := TStringList.Create;
  try
    if RunningProcessesList(List, False) then
    begin
      I := List.IndexOf(Executavel);
      if I > -1 then Result := True;
    end;
  finally
    List.Free;
  end;
end;

function TUnDmkProcFunc.ExecuteProcess(const FileName, Params: string;
  Folder: string; WaitUntilTerminated, WaitUntilIdle(*, RunMinimized*): Boolean;
 EstadoJanela: Integer;
  var ErrorCode: integer): boolean;
{  Como usar:
var
  FileName, Parameters, WorkingFolder: string;
  Error: integer;
  OK: boolean;
begin
  FileName := 'C:\FullPath\myapp.exe';
  WorkingFolder := ''; // if empty function will extract path from FileName
  Parameters := '-p'; // can be empty
  OK := ExecuteProcess(FileName, Parameters, WorkingFolder, false, false, false, Error);
  if not OK then ShowMessage('Error: ' + IntToStr(Error));
end;
}
var
  CmdLine: string;
  WorkingDirP: PChar;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  Result := true;
  CmdLine := '"' + FileName + '" ' + Params;
  if Folder = '' then Folder := ExcludeTrailingPathDelimiter(ExtractFilePath(FileName));
  ZeroMemory(@StartupInfo, SizeOf(StartupInfo));
  StartupInfo.cb := SizeOf(StartupInfo);
  (* 2020-04-14*)
  (*
  if RunMinimized then
  begin
    StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartupInfo.wShowWindow := SW_SHOWMINIMIZED;
  end;
  *)
  if EstadoJanela > -1 then
  begin
    StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartupInfo.wShowWindow := EstadoJanela;
  end;
  // Fim 2020-04-14*)
  if Folder <> '' then WorkingDirP := PChar(Folder)
  else WorkingDirP := nil;
  if not CreateProcess(nil, PChar(CmdLine), nil, nil, false, 0, nil, WorkingDirP, StartupInfo, ProcessInfo) then
    begin
      Result := false;
      ErrorCode := GetLastError;
      exit;
    end;
  with ProcessInfo do
    begin
      CloseHandle(hThread);
      if WaitUntilIdle then WaitForInputIdle(hProcess, INFINITE);
      if WaitUntilTerminated then
        repeat
          Application.ProcessMessages;
        until MsgWaitForMultipleObjects(1, hProcess, false, INFINITE, QS_ALLINPUT) <> WAIT_OBJECT_0 + 1;
      CloseHandle(hProcess);
    end;
end;

function TUnDmkProcFunc.ExtensoFloat(Numero: String): String;
const
  UnidSing: Array[0..5] of String = ('','','mil','milhão','bilhão','trilhão');
  UnidPlur: Array[0..5] of String = ('','','mil','milhões','bilhões','trilhões');
var
  Lista : TStringList;
  NumStr : String;
  i : Integer;
  JaLigou : Boolean;
  Texto, Milhar, NumReal, Virgula: String;
  PosDec : Integer;
begin
  if Length(Numero) = 0 then exit;
  if {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DecimalSeparator = ',' then Milhar := '.' else milhar := ',';
  NumReal := '';
  for i := 1 to Length(Numero) do
    if Numero[i] <> Milhar then NumReal := NumReal + Numero[i];
  Numero := NumReal;
  Lista := TStringList.Create;

  PosDec := Pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DecimalSeparator,Numero);
  if PosDec > 0 then begin
    Lista.Add('0'+Copy(Numero,PosDec+1,2));
    NumStr := Copy(Numero,1,PosDec-1);
  end
  else begin
    Lista.Add('000');
    NumStr := Numero;
  end;
  while NumStr <> '' do
    if Length(NumStr) > 3 then begin
      Lista.Add(Copy(NumStr,Length(NumStr)-2,3));
      NumStr := Copy(NumStr,1,Length(NumStr)-3);
    end
    else begin
      while Length(NumStr) < 3 do
        NumStr := '0'+NumStr;
      Lista.Add(NumStr);
      NumStr := '';
    end;
  Result := '';
  JaLigou := False;
  for i := 0 to Lista.Count - 1 do begin
    if Lista[i] <> '000' then begin
      Texto := ExtensoMil(Lista[i]);
      if (Lista[i] <> '001') or ((i = 1) and (Lista.Count > 2)) then
        Texto := Texto + ' ' + UnidPlur[i]
      else
        Texto := Texto + ' ' + UnidSing[i];
      (*if (i = 2) and (Lista[1] = '000') then
        Texto := Texto + ' reais'
      else if (i > 2) and (Lista[2] = '000') and (Lista[1] = '000') then
        Texto := Texto + ' de reais';*)
      if Result <> '' then begin
        if JaLigou then
          Result := Texto + ', ' + Result
        else begin
          if pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DecimalSeparator+'0', Numero) > 0 then
            Virgula := 'vírgula zero '
          else
            Virgula := 'vírgula ';
          Result := Texto + Virgula + Result;
          JaLigou := True;
        end;
      end
      else Result := Texto;
    end;
  end;
  if Length(Result)>1 then
    Result := Geral.Maiusculas(Result[1], False)+Copy(Result, 2, Length(Result)-1);
end;

function TUnDmkProcFunc.ExtensoMil(Numero: String): String;
const
  Unidades : Array[1..19] of String = ('um','dois','três','quatro',
    'cinco','seis','sete','oito','nove','dez','onze','doze','treze',
    'quatorze','quinze','dezesseis','dezessete','dezoito','dezenove');
  Dezenas : Array[2..9] of String = ('vinte','trinta','quarenta',
    'cinqüenta','sessenta','setenta','oitenta','noventa');
  Centenas : Array[1..9] of String = ('cento','duzentos','trezentos',
    'quatrocentos','quinhentos','seiscentos','setecentos','oitocentos',
    'novecentos');
begin
  Result := '';
  if Numero = '100' then Result := 'cem' else
  begin
    if Numero[1] <> '0' then
      Result := Centenas[Ord(Numero[1])-Ord('0')];
    if StrToInt(Copy(Numero,2,2)) > 0 then begin
      if Result <> '' then
        Result := Result + ' e ';
      if StrToInt(Copy(Numero,2,2)) <= 19 then
        Result := Result + Unidades[StrToInt(Copy(Numero,2,2))]
      else begin
        Result := Result + Dezenas[Ord(Numero[2])-Ord('0')];
        if Numero[3] <> '0' then
          Result := Result + ' e ' + Unidades[Ord(Numero[3])-Ord('0')];
      end;
    end;
  end;
end;

function TUnDmkProcFunc.ExtensoMoney(Numero: String): String;
const
  UnidSing: Array[0..5] of String = ('centavo','real','mil','milhão','bilhão','trilhão');
  UnidPlur: Array[0..5] of String = ('centavos','reais','mil','milhões','bilhões','trilhões');
var
  Lista : TStringList;
  NumStr : String;
  i : Integer;
  JaLigou : Boolean;
  Texto, Milhar, NumReal : String;
  PosDec : Integer;
begin
  // 2011-07-22
  if Geral.DMV(Numero) = 0 then
  begin
    Result := ' Zero reais';
    Exit;
  end;
  // fim 2011-07-22
  if Length(Numero) = 0 then exit;
  if {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DecimalSeparator = ',' then Milhar := '.' else milhar := ',';
  NumReal := '';
  for i := 1 to Length(Numero) do
    if Numero[i] <> Milhar then NumReal := NumReal + Numero[i];
  Numero := NumReal;
  Lista := TStringList.Create;

  PosDec := Pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DecimalSeparator,Numero);
  if PosDec > 0 then begin
    Lista.Add('0'+Copy(Numero,PosDec+1,2));
    NumStr := Copy(Numero,1,PosDec-1);
  end
  else begin
    Lista.Add('000');
    NumStr := Numero;
  end;
  while NumStr <> '' do
    if Length(NumStr) > 3 then begin
      Lista.Add(Copy(NumStr,Length(NumStr)-2,3));
      NumStr := Copy(NumStr,1,Length(NumStr)-3);
    end
    else begin
      while Length(NumStr) < 3 do
        NumStr := '0'+NumStr;
      Lista.Add(NumStr);
      NumStr := '';
    end;
  Result := '';
  JaLigou := False;
  for i := 0 to Lista.Count - 1 do begin
    if Lista[i] <> '000' then begin
      Texto := ExtensoMil(Lista[i]);
      if (Lista[i] <> '001') or ((i = 1) and (Lista.Count > 2)) then
        Texto := Texto + ' ' + UnidPlur[i]
      else
        Texto := Texto + ' ' + UnidSing[i];
      if (i = 2) and (Lista[1] = '000') then
        Texto := Texto + ' reais'
      else if (i > 2) and (Lista[2] = '000') and (Lista[1] = '000') then
        Texto := Texto + ' de reais';
      if Result <> '' then begin
        if JaLigou then
          Result := Texto + ', ' + Result
        else begin
          Result := Texto + ' e ' + Result;
          JaLigou := True;
        end;
      end
      else Result := Texto;
    end;
  end;
  if Length(Result)>1 then
    Result := Geral.Maiusculas(Result[1], False)+Copy(Result, 2, Length(Result)-1);
end;

function TUnDmkProcFunc.ExtractUrlFileName(URL: String): String;
var
  i: Integer;
begin
  i := LastDelimiter('/', URL);
  //
  Result := Copy(URL, i + 1, Length(URL) - (i));
end;

function TUnDmkProcFunc.FatIDProibidoEmLct(FatID: Integer): Boolean;
begin
  Result := True;
  //
  //C r e d i t o 2
  if ((FatID >= 300)
  and (FatID <= 399 )) then
  begin
    Geral.MB_Info('Este lançamento é gerenciado por janela específica!' +
    'Não é permitido seu pagamento aqui!');
    Exit;
  end else
  if (FatID <> 0) and
  (
    //NF-e
    (FatID <> VAR_FATID_0001) and
    //S y n d i 2
    (FatID <> VAR_FATID_0600) and
    (FatID <> VAR_FATID_0601) and
    (FatID <> VAR_FATID_0610) and
    // T o o l r e n t 2015-07-21
    (FatID <> VAR_FATID_3001) and // Bugstrol -> Faturamento de serviço) 2020-01-01
    (FatID <> VAR_FATID_3002) and // Bugstrol -> Faturamento de serviço) desmarcado em 2020-01-01
    // B u g s t r o l 2013-07-01
    (FatID <> VAR_FATID_4001) and // Bugstrol -> Faturamento de serviço)
    // Y R O d 2020-01-01
    (FatID <> VAR_FATID_7001) // YROd -> Faturamento de venda/serviço)
    // Outros???
  ) then
  begin
    Geral.MB_Info('Este lançamento é gerenciado por janela específica!' +
    sLineBreak +
    'Solicite à DERMATEK a viabilidade da liberação para pagamento aqui!');
    Exit;
  end else
    Result := False;
end;

function TUnDmkProcFunc.FCE(Coluna: String): String;
var
  k: Integer;
  Col: String;
  Erro: Boolean;
begin
  Col := Uppercase(Coluna);
  k := Length(Coluna);
  Erro := True;
  case k of
    0: Erro := False;
    //1: if Col[1] in (['A'..'Z']) then Erro := False;
    //2: if (Col[1] in (['A'..'Z'])) and (Col[2] in (['A'..'Z'])) then Erro := False;
{$IFDEF DELPHI12_UP}
    1: if (CharInSet(Col[1], (['A'..'Z']))) then Erro := False;
    2: if (CharInSet(Col[1], (['A'..'Z']))) and (CharInSet(Col[2], (['A'..'Z']))) then Erro := False;
{$ELSE}
    1: if Col[1] in (['A'..'Z']) then Erro := False;
    2: if (Col[1] in (['A'..'Z'])) and (Col[2] in (['A'..'Z'])) then Erro := False;
{$ENDIF}
  end;
  if Erro then
  begin
    Geral.MB_Aviso('Endereço de coluna inválido: "' + Coluna + '"');
    Result := '';
  end else Result := Col;
end;

function TUnDmkProcFunc.FDT_NFe_UTC(DataHora: TDateTime;
  TZD_UTC: Double): String;
var
  Sinal: String;
begin
  if TZD_UTC < 0 then
    Sinal := '-'
  else
    Sinal := '+';
  Result := FormatDateTime('yyyy-mm-dd', DataHora) + 'T' +
            FormatDateTime('hh:nn:ss', DataHora) + Sinal +
            FormatDateTime('hh:nn', TZD_UTC);
end;

function TUnDmkProcFunc.FDT_Null(Data: TDateTime; Tipo: Integer): Variant;
begin
  if Data < 2 then
    Result := Null
  else
    Result := Geral.FDT(Data, Tipo);
end;

function TUnDmkProcFunc.FDT_NULO(Data: TDateTime; Tipo: Integer): String;
begin
  if Data < 2 then
    Result := ''
  else
    Result := Geral.FDT(Data, Tipo);
end;

function TUnDmkProcFunc.FecharOutraAplicacao(Nome: String;
  Handle: THandle): Integer;
var
  TopWindow: HWND;
  WinName, WinClass: array[0..80] of Char;
  x: Integer;
  NoError: Boolean;
  WindowList1: TList;
begin
  Result := -1;
  WindowList1 := nil;
  try
    WindowList1 := TList.Create;
    TopWindow := Handle;
    NoError := EnumWindows(@GetWindow, LongInt(@TopWindow));
    if NoError then Exit;
    for x := 0 to WindowList1.Count -1 do
    begin
      GetWindowText(HWND(WindowList1[x]), WinName, SizeOf(WinName)-1);
      GetClassName(HWND(WindowList1[x]), WinClass, SizeOf(WinName)-1);
      if WinClass = 'TApplication' then
        if String(WinName) = Nome then
          Result := SendMessage(HWND(WindowList1[x]), WM_CLOSE, 0, 0);
    end;
  finally
    if WindowList1 <> nil then WindowList1.Free;
  end;
end;

function TUnDmkProcFunc.FFD(Numero: Double; Digitos: Integer;
  Sinal: TSinal): String;
begin
  Result := Geral.TFD(FloatToStr(Trunc(Numero)), Digitos, Sinal);
end;

function TUnDmkProcFunc.FFF(Valor: Double; Casas: Integer;
  Sinal: TSinal): Double;
var
  Multpl: Extended;
begin
  Multpl := IntPower(10, Casas);
  Result := Round(Multpl * Valor) / Multpl;
end;

function TUnDmkProcFunc.FFP(Numero: Double; Casas: Integer): String;
var
  Txt: String;
  k: Integer;
begin
  if Casas = -1 then
    Txt := FloatToStr(Numero)
  else
    Txt := FloatToStrF(Numero, ffFixed, 15, Casas);
  k := pos(',', Txt);
  if k > 0 then
    Txt[k] := '.';
  Result := Txt;
end;

function TUnDmkProcFunc.FFT_Ponto1(Numero: Double): String;
var
  k : Integer;
begin
  Result := FormatFloat('0.00000000000000000000', Numero);
  k := Pos(',', Result);
  if k > 0 then
    Result[k] := '.';
end;

function TUnDmkProcFunc.FileAgeCreate(const FileName: String): Integer;
var
  Handle: THandle;
  FindData: TWin32FindData;
  LocalFileTime: TFileTime;
begin
  Handle := FindFirstFile(PChar(FileName), FindData);
  if Handle <> INVALID_HANDLE_VALUE then
  begin
    Windows.FindClose(Handle);
    if (FindData.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) = 0 then
    begin
      FileTimeToLocalFileTime(FindData.ftCreationTime, LocalFileTime);
      if FileTimeToDosDateTime(LocalFileTime, LongRec(Result).Hi,
        LongRec(Result).Lo) then Exit;
    end;
  end;
  Result := -1;
end;

function TUnDmkProcFunc.FloatToBool(Numero: Double; Casas: Integer): Boolean;
var
  i, MeuNumero: Integer;
  Fator: String;
begin
  Fator := '1';
  for i := 1 to Casas do Fator := Fator + '0';
  MeuNumero := Trunc(Numero * StrToInt(Fator));
  if MeuNumero = 0 then Result := False else Result := True;
end;

function TUnDmkProcFunc.FormaDeTratamentoBR(Val: Integer; Tipo: TTipoTroca): String;
begin
  Result := '[]';
  case Val of
    0: // masculino
    case Tipo of
      trcOA   : Result := 'o';
      trcOsAs : Result := 'os';
      trcPara : Result := 'ao';
    end;
    1:  // feminino
    case Tipo of
      trcOA   : Result := 'a';
      trcOsAs : Result := 'as';
      trcPara : Result := 'à';
    end;
  end;
end;

function TUnDmkProcFunc.FormataCasas(Casas: Integer): String;
var
  i: Integer;
  Texto: String;
begin
  Texto := '#,###,###,##0';
  if Casas > 0 then
  begin
    Texto := Texto + '.';
    for i := 1 to Casas do Texto := Texto + '0';
  end;
  Result := Texto + ';-' + Texto + '; ';
end;

function TUnDmkProcFunc.FormataTimeStamp(Texto: String): TDateTime;
var
  Ano, Mes, Dia, Hora, Min, Seg: Word;
  Data: TDateTime;
  Tempo: TTime;
begin
  try
    if Length(Texto) <> 14 then
    begin
      Geral.MB_Erro('Data inválida');
      Result := 0;
      Exit;
    end;
    Ano    := StrToInt(Copy(Texto,  1, 4));
    Mes    := StrToInt(Copy(Texto,  5, 2));
    Dia    := StrToInt(Copy(Texto,  7, 2));
    Hora   := StrToInt(Copy(Texto,  9, 2));
    Min    := StrToInt(Copy(Texto,  11, 2));
    Seg    := StrToInt(Copy(Texto,  13, 2));
    Data   := EncodeDate(Ano, Mes, Dia);
    Tempo  := EncodeTime(Hora, Min, Seg, 0);
    Result := Data + Tempo;
  except
    Raise EAbort.Create('');
    Geral.MB_Erro('Data inválida');
  end;
end;

function TUnDmkProcFunc.FretePor_Txt(Quem: Integer): String;
begin
  case Quem of
    0: Result := 'Por conta do emitente';
    1: Result := 'Por conta do destinatário';
    2: Result := 'Por conta de terceiros';
    9: Result := 'Sem Frete';
    else Result := '? ? ? ? ?';
  end;
end;

function TUnDmkProcFunc.GeraPassword(UsaMinusculas, UsaMaiusculas,
  UsaNumeros: Boolean; TamanhoSenha: Integer): String;
var
  Senha, Letras: String;
  n, k: Integer;
begin
  Result := '?';
  //
  if TamanhoSenha < 1 then
  begin
    Geral.MB_Aviso('Tamanho de senha inválido: ' + Geral.FF0(TamanhoSenha));
    Exit;
  end;
  //
  Letras := '';
  n      := 0;
  //
  if UsaMinusculas then
  begin
    Letras := Letras + pwdMinusculas;
    n      := n + Length(pwdMinusculas);
  end;
  if UsaMaiusculas then
  begin
    Letras := Letras + pwdMaiusculas;
    n      := n + Length(pwdMaiusculas);
  end;
  if UsaNumeros then
  begin
    Letras := Letras + pwdNumeros;
    n      := n + Length(pwdNumeros);
  end;
  if n < 10 then
  begin
    Geral.MB_Aviso('Erro de string base na gereção de senha!');
    Exit;
  end;
  Randomize;
  while Length(Senha) < TamanhoSenha do
  begin
    k := Round(Random(n));
    //
    while k = 0 do
      k := Round(Random(n));
    //
    Senha := Trim(Senha + Letras[k]);
  end;
  Result := Senha;
end;

function TUnDmkProcFunc.GeraSigla(Texto: String; MaxTam: Integer): String;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(Texto) do
  begin
    if (CharInSet(Texto[I], (['a'..'z']))) then
      Result := Result + Texto[I]
    else
    if (CharInSet(Texto[i], (['A'..'Z']))) then
      Result := Result + Texto[I]
    else
    if (CharInSet(Texto[i], (['0'..'9']))) then
      Result := Result + Texto[I];
    //]
    if Length(Result) = MaxTam then Exit;
  end;
end;

function TUnDmkProcFunc.GeraStringRandomica(Tamanho: Integer): string;
var
  x: integer;
begin
  Randomize;
  Result := '';
  while length(Result) < Tamanho do
  begin
    x  := Random(61) + 1;
    Result := Result + Codes64[x];
  end;
end;

function TUnDmkProcFunc.GetAllFiles(Subdiretorios: Boolean; Mask: string;
  ListBox: TCustomListBox; ClearListBox: Boolean): Integer;
var
  search: TSearchRec;
  directory: string;
begin
  if ListBox <> nil then
    ListBox.Update;
  //
  Application.ProcessMessages;
  //
  Result := 0;
  // Não no loop!!!!
  if ClearListBox and (ListBox <> nil) then ListBox.Items.Clear;
  //
  directory := ExtractFilePath(mask);

  // find all files
  if FindFirst(mask, $23, search) = 0 then
  begin
    repeat
      // add the files to the listbox
      if ListBox <> nil then
        ListBox.Items.Add(directory + search.Name);
      Result := Result + 1;
      //Inc(Count);
    until FindNext(search) <> 0;
  end;

  if Subdiretorios then
  begin
    if FindFirst(directory + '*.*', faDirectory, search) = 0 then
    begin
      repeat
        if ((search.Attr and faDirectory) = faDirectory) and (search.Name[1] <> '.') then
          Result := Result + GetAllFiles(Subdiretorios, Directory + Search.Name + '\' +
          ExtractFileName(Mask), ListBox, False);
      until FindNext(search) <> 0;
      System.SysUtils.FindClose(search);
    end;
  end;
end;

function TUnDmkProcFunc.GetDiskSerialNumber(DriveLatter: String): DWord;
var
//  a,b : Integer;
  buffer : array[0..255] of char;
  B1 : PChar;
  a,b,SerialNumber : DWord;
begin
  //New(B1);
  B1 := PChar(DriveLatter + ':\');
  //B1 := StrPCopy(B1, DriveLatter + ':\');
  GetVolumeInformation(B1, Buffer, SizeOf(buffer),@SerialNumber,a,b,nil,0);
  Result := SerialNumber;
  //Dispose(B1);
end;

function TUnDmkProcFunc.GetEXEVersionData(
  const FileName: string): TEXEVersionData;
type
  PLandCodepage = ^TLandCodepage;
  TLandCodepage = record
    wLanguage,
    wCodePage: word;
  end;
var
  dummy,
  len: cardinal;
  buf, pntr: pointer;
  lang: string;
begin
  len := GetFileVersionInfoSize(PChar(FileName), dummy);
  if len = 0 then
    RaiseLastOSError;
  GetMem(buf, len);
  try
    if not GetFileVersionInfo(PChar(FileName), 0, len, buf) then
      RaiseLastOSError;

    if not VerQueryValue(buf, '\VarFileInfo\Translation\', pntr, len) then
      RaiseLastOSError;

    lang := Format('%.4x%.4x', [PLandCodepage(pntr)^.wLanguage, PLandCodepage(pntr)^.wCodePage]);

    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\CompanyName'), pntr, len){ and (@len <> nil)} then
      result.CompanyName := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\FileDescription'), pntr, len){ and (@len <> nil)} then
      result.FileDescription := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\FileVersion'), pntr, len){ and (@len <> nil)} then
      result.FileVersion := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\InternalName'), pntr, len){ and (@len <> nil)} then
      result.InternalName := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\LegalCopyright'), pntr, len){ and (@len <> nil)} then
      result.LegalCopyright := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\LegalTrademarks'), pntr, len){ and (@len <> nil)} then
      result.LegalTrademarks := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\OriginalFileName'), pntr, len){ and (@len <> nil)} then
      result.OriginalFileName := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\ProductName'), pntr, len){ and (@len <> nil)} then
      result.ProductName := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\ProductVersion'), pntr, len){ and (@len <> nil)} then
      result.ProductVersion := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\Comments'), pntr, len){ and (@len <> nil)} then
      result.Comments := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\PrivateBuild'), pntr, len){ and (@len <> nil)} then
      result.PrivateBuild := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\SpecialBuild'), pntr, len){ and (@len <> nil)} then
      result.SpecialBuild := PChar(pntr);
  finally
    FreeMem(buf);
  end;
end;

function TUnDmkProcFunc.GetFileList(const Path: String; const Ext: array of string): TStringList;
var
  I, J: Integer;
  Txt: String;
  Lista: TStringList;
  SearchRec: TSearchRec;
begin
  Result := TStringList.Create;
  Lista  := TStringList.Create;
  try
    for I := 0 to Length(Ext) - 1 do
    begin
      Txt := LowerCase(Ext[I]);

      Lista.Add(Txt);
    end;
    //
    I := FindFirst(Path + '\*.*', 0, SearchRec);
    while I = 0 do
    begin
      if Lista.Count > 0 then
      begin
        if Lista.Find(LowerCase(ExtractFileExt(SearchRec.Name)), J) then
          Result.Add(SearchRec.Name);
      end else
        Result.Add(SearchRec.Name);
      I := FindNext(SearchRec);
    end;
    Lista.Free;
  except
    Lista.Free;
    Result.Free;
    raise;
  end;
end;

//INI = Obtem versão do Windows (Compatível com Windows 10
function NetWkstaGetInfo(ServerName: LPWSTR; Level: DWORD; BufPtr: LPBYTE): Longint; stdcall;
  external 'netapi32.dll' name 'NetWkstaGetInfo';

function TUnDmkProcFunc.GetNetWkstaMajorMinor(var MajorVersion,
  MinorVersion: DWORD): Boolean;
var
  LBuf: LPWKSTA_INFO_100;
begin
  Result := NetWkstaGetInfo(nil, 100, @LBuf) = 0;
  if Result then
  begin
    MajorVersion := LBuf^.wki100_ver_major;
    MinorVersion := LBuf^.wki100_ver_minor;
  end;
end;

function TUnDmkProcFunc.GetProductVersion(const AFileName: string; var AMajor,
  AMinor, ABuild: Cardinal): Boolean;
var
  FileName: string;
  InfoSize, Wnd: DWORD;
  VerBuf: Pointer;
  FI: PVSFixedFileInfo;
  VerSize: DWORD;
begin
  Result := False;
  // GetFileVersionInfo modifies the filename parameter data while parsing.
  // Copy the string const into a local variable to create a writeable copy.
  FileName := AFileName;
  UniqueString(FileName);
  InfoSize := GetFileVersionInfoSize(PChar(FileName), Wnd);
  if InfoSize <> 0 then
  begin
    GetMem(VerBuf, InfoSize);
    try
      if GetFileVersionInfo(PChar(FileName), Wnd, InfoSize, VerBuf) then
        if VerQueryValue(VerBuf, '\', Pointer(FI), VerSize) then
        begin
          AMajor := HiWord(FI.dwProductVersionMS);
          AMinor := LoWord(FI.dwProductVersionMS);
          ABuild := HiWord(FI.dwProductVersionLS);
          Result:= True;
        end;
    finally
      FreeMem(VerBuf);
    end;
  end;
end;

function TUnDmkProcFunc.GetVersion(sFileName: String;
  ForcaTrezeCarac: Boolean): String;
var
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
begin
  //Obtem versão do arquivo selecionado
  VerInfoSize := GetFileVersionInfoSize(PChar(sFileName), Dummy);
  if VerInfoSize > 0 then
  begin
    GetMem(VerInfo, VerInfoSize);
    GetFileVersionInfo(PChar(sFileName), 0, VerInfoSize, VerInfo);
    VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
    with VerValue^ do
    begin
      if ForcaTrezeCarac then
      begin
        Result := FormatFloat('00', dwFileVersionMS shr 16);
        Result := Result + '.' + FormatFloat('00', dwFileVersionMS and $FFFF);
        Result := Result + '.' + FormatFloat('00', dwFileVersionLS shr 16);
        Result := Result + '.' + FormatFloat('0000', dwFileVersionLS and $FFFF);
      end else
      begin
        Result := FormatFloat('0', dwFileVersionMS shr 16);
        Result := Result + '.' + FormatFloat('0', dwFileVersionMS and $FFFF);
        Result := Result + '.' + FormatFloat('0', dwFileVersionLS shr 16);
        Result := Result + '.' + FormatFloat('0', dwFileVersionLS and $FFFF);
      end;
    end;
    FreeMem(VerInfo, VerInfoSize);
  end else
    Result := '';
end;

//FIM = Obtem versão do Windows (Compatível com Windows 10

function TUnDmkProcFunc.GetNomeTipoCart(TipoCart: Integer;
  DoDa: Boolean): String;
var
  Txt: String;
begin
  case TipoCart of
    0: Result := 'caixa';
    1: Result := 'conta corrente';
    2: Result := 'emissão';
    else Result := '?';
  end;
  if DoDa then
  begin
    case TipoCart of
      0: Txt := 'do ';
      1: Txt := 'da ';
      2: Txt := 'da ';
      else Txt := 'de ';
    end;
  end else Txt := '';
  Result := Txt + Result;
end;

function TUnDmkProcFunc.GetProcessNameFromWnd(Wnd: HWND): string;
var
  List: TStringList;
  PID: DWORD;
  I: Integer;
begin
  Result := '';
  if IsWindow(Wnd) then
  begin
    PID := INVALID_HANDLE_VALUE;
    GetWindowThreadProcessId(Wnd, @PID);
    List := TStringList.Create;
    try
      if RunningProcessesList(List, True) then
      begin
        I := List.IndexOfObject(Pointer(PID));
        if I > -1 then
          Result := List[I];
      end;
    finally
      List.Free;
    end;
  end;
end;

procedure TUnDmkProcFunc.GravaEmLog(Arquivo, Msg: String);
var
  Lista: TStringList;
begin
  // cria uma lista de strings para armazenar o conteúdo em log
  Lista := TStringList.Create;
  try
    try
      // se o log ja existe carrega ele
      if FileExists(Arquivo) then
        Lista.LoadFromFile(Arquivo);
      // adiciona a nova string ao log
      Lista.Add(Geral.FDT(Now(), 109) + ' > ' + Msg);
    except
      on e: exception do
        Lista.Add(Geral.FDT(Now(), 109) + ' >  Erro ' + E.Message);
    end;
  finally
    // atualiza o log
    Lista.SaveToFile(Arquivo);
    // libera a lista
    Lista.Free;
  end;
end;

function TUnDmkProcFunc.HexCripto(mStr, mChave: string): string;
var
  Crip: String;
  I: Integer;
begin
  Result := '';
  Crip := Criptografia(mStr, mChave);
  for I := 1 to Length(Crip) do
    Result := Result + IntToHex(Ord(Crip[I]), 2);
end;

function TUnDmkProcFunc.HexDecripto(mStr, mChave: String): String;
var
  I: Integer;
  Txt, Txt2, Txt3, Txt4: String;
begin
  Txt := mStr;
  Txt3 := '';
  for I := 1 to (Length(Txt) div 2) do
  begin
    Txt2 := Copy(Txt, I * 2 -1, 2);
    Txt3 := Txt3 + Char(StrToInt('$' + Txt2));
  end;
  Result := dmkPF.Criptografia(Txt3, mChave);
end;

function TUnDmkProcFunc.HexToInt(s: string): Int64;
var
  b: Byte;
  c: Char;
begin
  Result := 0;
  s := UpperCase(s);
  for b := 1 to Length(s) do
  begin
    Result := Result * 16;
    c := s[b];
    case c of
      '0'..'9': Inc(Result, Ord(c) - Ord('0'));
      'A'..'F': Inc(Result, Ord(c) - Ord('A') + 10);
      else
        raise EConvertError.Create('No Hex-Number');
    end;
  end;
end;

function TUnDmkProcFunc.HorasMH(Minutos: Double; EmptyNull: Boolean): String;
var
  H, M: Integer;
begin
  if (Minutos = 0) and EmptyNull then Result := ''
  else begin
    H := Trunc(Minutos / 60);
    M := Trunc(Minutos - (H*60));
    Result := FormatFloat('00',H) + ':' + FormatFloat('00',M);
  end;
end;

function TUnDmkProcFunc.HorasMHT(Minutos: Integer; EmptyNull: Boolean): String;
var
  H, M: Integer;
begin
  if (Minutos = 0) and EmptyNull then Result := ''
  else begin
    H := Trunc(Minutos / 60);
    M := Trunc(Minutos - (H*60));
    Result := '';
    if H > 0 then Result := IntToStr(H) + 'h ';
    if (H > 0) and (M > 0) then Result := Result + ' e ';
    if M > 0 then Result := Result + FormatFloat('00',M)+ ' min';
  end;
end;

function TUnDmkProcFunc.HRDT_DefinePorDivisao(Intervalo, SubPeriodo: Integer;
TxtSingular, TxtPlural: String; var Resultado: String): Boolean;
var
  Qtde: Integer;
  TxtPre, TxtPos: String;
begin
  Result := (Intervalo div SubPeriodo) = (Intervalo / SubPeriodo);
  if Result then
  begin
    Qtde := Intervalo div SubPeriodo;
    if Qtde = 1 then
      TxtPre := 'um'
    else
      TxtPre := Geral.FF0(Qtde);
    //
    if TxtSingular <> '' then
    begin
      if Qtde = 1 then
        TxtPos := TxtSingular
      else
        TxtPos := TxtPlural;
    end else
      TxtPos := TxtPlural;
    Resultado := Resultado + TxtPre + ' ' + TxtPos;
  end;
end;

function TUnDmkProcFunc.HRDT_Intervalo(IntervDd: Integer): String;
begin
  Result := '';
  if IntervDd > 0 then
  begin
    case IntervDd of
        7: Result := 'Semanalmente';
       10: Result := 'Decendialmente';
       15: Result := 'Quinzenalmente';
       30: Result := 'Mensalmente';
       60: Result := 'Bimestralmente';
       90: Result := 'Trimestralmente';
      120: Result := 'Quadrimestralmente';
      180: Result := 'Semestralmente';
      //210: Result := 'Septomestral';
      //240: Result := 'Octomestral';
      365: Result := 'Anualmente';
    end;
    if Result = '' then
    begin
      Result := 'A cada ';
      if not HRDT_DefinePorDivisao(IntervDd, 365, '', 'anos', Result) then
      if not HRDT_DefinePorDivisao(IntervDd, 30, '', 'meses', Result) then
      if not HRDT_DefinePorDivisao(IntervDd, 7, '', 'semanas', Result) then
      HRDT_DefinePorDivisao(IntervDd, 1, '', 'dias', Result);
    end;
  end;
end;

function TUnDmkProcFunc.HRDT_Periodo(PerioDd: Integer): String;
begin
  Result := '';
  if PerioDd > 0 then
  begin
    Result := 'por ';
    if not HRDT_DefinePorDivisao(PerioDd, 365, 'ano', 'anos', Result) then
    if not HRDT_DefinePorDivisao(PerioDd, 30, 'mês', 'meses', Result) then
    if not HRDT_DefinePorDivisao(PerioDd, 7, 'semana', 'semanas', Result) then
    HRDT_DefinePorDivisao(PerioDd, 1, 'dia', 'dias', Result);
  end;
end;

function TUnDmkProcFunc.HRDT_PeriodoEIntervalos(PerioDd, IntervDd: Integer;
MsgWhenEmpty: String): String;
begin
  if (PerioDd = 0) and (IntervDd = 0) then
    Result := MsgWhenEmpty
  else
    Result := HRDT_Intervalo(IntervDd) + ' ' + HRDT_Periodo(PerioDd);
end;

function TUnDmkProcFunc.HTH(Hora, Formato: String): String;
var
  Txt: String;
  fmt: Integer;
begin
  try
    if Hora = '' then Hora := '00:00';
    fmt := NumTex(Hora);
    case fmt of
          1: Txt := '0'+Hora+':00';
         11: Txt := Hora+':00';
        111: Txt := '0'+Hora[1]+':'+Hora[2]+Hora[3];
        121: Txt := '0' + Hora[1]+':0'+Hora[3];
       1111: Txt := Hora[1]+Hora[2]+':'+Hora[3]+Hora[4];
       1121: Txt := Hora[1]+Hora[2]+':0'+Hora[4];
       1211: Txt := '0'+Hora[1]+':'+Hora[3]+Hora[4];
      11211: Txt := Hora[1]+Hora[2]+':'+Hora[4]+Hora[5];
      else Txt := Hora;
    end;
    Result := FormatDateTime(Formato, StrToTime(Txt));
  except
    Geral.MB_Aviso('Hora inválida: ' + Hora);
    Result := '00:00';
  end;
end;

function TUnDmkProcFunc.HTT(Hora: TTime; Tamanho: Integer;
  UsaPonto: Boolean): String;
var
  h, n, s, m: word;
  ht, nt, st, mt, pt: String;
begin
  if UsaPonto then pt := ':' else pt := '';
  DecodeTime(Hora, h, n, s, m);
  ht := FormatFloat('00', h);
  nt := FormatFloat('00', n);
  st := FormatFloat('00', s);
  mt := FormatFloat('000', m);
  //
  Result := ht;
  if Tamanho > 2 then Result := Result + pt + nt;
  if Tamanho > 4 then Result := Result + pt + st;
  if Tamanho > 6 then Result := Result + pt + mt;
end;

function TUnDmkProcFunc.HWND_CmdShowToEstadoJanela(
  HWND_CmdShow: Integer): Integer;
begin
  case HWND_CmdShow of
    //Padrão do app
    SW_SHOW       : Result := 0;
    //Normal
    SW_NORMAL     : Result := 1;
    //Maximizada
    SW_MAXIMIZE   : Result := 2;
    //Minimizada
    SW_MINIMIZE   : Result := 3;
    //Oculta
    SW_HIDE       : Result := 4;
  end;
end;

function TUnDmkProcFunc.I64MV(Texto: String): Int64;
var
  Num: String;
  i: Integer;
begin
  try
    if Trim(Texto) = CO_VAZIO then Texto := '0';
    Num := CO_VAZIO;
    for i := 1 to Length(Texto) do
    begin
      if (Texto[i] = '-') and (i = 1) then Num := '-';
      //if Texto[i] in ['0'..'9'] then Num := Num + Texto[i];
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], ['0'..'9']) then Num := Num + Texto[i];
{$ELSE}
      if Texto[i] in ['0'..'9'] then Num := Num + Texto[i];
{$ENDIF}
    end;
    if (Texto = '') or (Texto = '-') then Result := 0 else
    Result := StrToInt64(Num);
  except
    raise EAbort.Create('O texto '+Texto+' é um número inválido.');
    Result := 0;
  end;
end;

function TUnDmkProcFunc.IgnoraLocalhost(Host: String): String;
begin
  if (LowerCase(Host) = 'localhost')
  or (Host = '127.0.0.1') then
    Result := ''
  else
    Result := Host;
end;

function TUnDmkProcFunc.IncrementaAnoMes(AnoMes, Incremento: Integer): Integer;
var
  Ano, Mes: Word;
  Anos, Meses: Integer;
begin
  DmkPF.AnoMesDecode(AnoMes, Ano, Mes);
  Anos  := Incremento div 12;
  Meses := Incremento mod 12;
  if Meses < 1 then
  begin
    Mes := Mes + 12 + Meses;
    Ano := Ano - 1 + Anos;
  end else
  begin
    Mes := Mes + Meses;
    Ano := Ano + Anos;
  end;
  if Mes > 12 then
  begin
    Mes := Mes - 12;
    Ano := Ano + 1;
  end;
  Result := (Ano * 100) + Mes;
end;

procedure TUnDmkProcFunc.IncrementaListaDeInteiros(var Lista: TMyGrlArrInt;
  const Inteiro: Integer);
var
  I: Integer;
  Achou: Boolean;
begin
  Achou := False;
  for I := Low(Lista) to High(Lista) do
  begin
    if Lista[I] = Inteiro then
    begin
      Achou := True;
      Break;
    end;
  end;
  if Achou = False then
  begin
    I := Length(Lista);
    SetLength(Lista, I + 1);
    Lista[I] := Inteiro;
  end;
end;

function TUnDmkProcFunc.IncrementaMeses(Data: TDateTime; Meses: Integer;
  SoUteis: Boolean): TDateTime;
var
  Ano, Mes, Dia, AnoN, MesN, DiaN: Word;
  Dias: Integer;
  NovaData: TDateTime;
begin
  DecodeDate(Data, Ano, Mes, Dia);
  Mes := Mes + Meses;
  AnoN := Ano;
  MesN := Mes;
  if Mes > 12 then
  begin
    MesN := Mes Mod 12;
    AnoN := Ano + Mes div 12;
  end;
  if MesN = 0 then
  begin
    MesN := 12;
    if Ano > 0 then Ano  := Ano - 1;
  end;
  case MesN of
    1  : Dias := 31;
    3  : Dias := 31;
    4  : Dias := 30;
    5  : Dias := 31;
    6  : Dias := 30;
    7  : Dias := 31;
    8  : Dias := 31;
    9  : Dias := 30;
    10 : Dias := 31;
    11 : Dias := 30;
    12 : Dias := 31;
  else
    if (int(ano/4) = ano/4) then Dias := 29 else Dias := 28;
  end;
  if Dia > Dias then DiaN := Dias else DiaN := Dia;
  NovaData := EncodeDate(AnoN, MesN, DiaN);
  if SoUteis then
  begin
    if DayOfWeek(NovaData) = 1 then NovaData := NovaData + 1;
    if DayOfWeek(NovaData) = 7 then NovaData := NovaData + 2;
  end;
  Result := NovaData;
end;

function TUnDmkProcFunc.IndexOfArrStr(Str: String;
  ArrStr: array of String): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to High(ArrStr) do
    if Str = ArrStr[I] then
      Result := I;
end;

procedure TUnDmkProcFunc.InfoKIND_DEPTO(Procedimento: String);
begin
  Geral.MB_Aviso('Tipo de "depto" não implementado!' + sLineBreak +
    Procedimento);
end;

function TUnDmkProcFunc.IntInConjunto2(Numero, Conjunto: Integer): Boolean;
begin
  Result := Conjunto and Numero > 0;
end;

function TUnDmkProcFunc.IntInConjunto2Def(Numero, Conjunto, DefTrue,
  DefFalse: Integer): Integer;
var
  Verdade: Boolean;
begin
  Verdade := IntInConjunto2(Numero, Conjunto);
  if Verdade then
    Result := DefTrue
  else
    Result := DefFalse;
end;

function TUnDmkProcFunc.IntToBool_SoPositivo(Numero: Integer): Boolean;
begin
  if Numero > 0 then
    Result := True
  else
    Result := False;
end;

function TUnDmkProcFunc.IntToCODIF_Receita(Numero: Integer): String;
var
  iToTxt, iToNum: Integer;
  xToTxt, xToNum: String;
begin
  if Numero >= 0 then
    Result := IntToStr(Numero)
  else
  begin
    iToTxt := -Numero div 100;
    iToNum := -Numero mod 100;
    //
    xToNum := IntToStr(iToNum);
    while Length(xToNum) < 2 do
      xToNum := '0' + xToNum;
    //
    //iToTxt := iToTxt + 676; // AA => 26 * 26
    iToTxt := iToTxt + 27; // AA => 26 * 26
    xToTxt := IntToColTxt(iToTxt);
    Result := xToTxt + xToNum;
  end;
end;

function TUnDmkProcFunc.IntToColTxt(Inteiro: Integer): String;
var
  Numero, i, k, x, m: Integer;
  Letras: array[1..11] of Integer;
begin
  Result := '';
  for i := 1 to 11 do Letras[i] := 0;
  Numero := Inteiro;
  k := 0;
  while Numero > 26 do
  begin
    m := 0;
    inc(k, 1);
    x := Numero mod 26;
    if x = 0 then
    begin
      x := 26;
      m := 1;
    end;
    Letras[k] := x;
    Numero := (Numero div 26) - m;
  end;
  if Numero > 0 then
  begin
    inc(k, 1);
    Letras[k] := Numero;
  end;
  {for i := k downto 1 do
    Show Message('Letra ' + IntToStr(i) + ' = ' + IntToStr(Letras[i]));}
  for i := k downto 1 do
    Result := Result + Char(64 + Letras[i]);
end;

function TUnDmkProcFunc.IntToSimNao(Numero: Integer): String;
begin
  case Numero of
    0: Result := 'NÃO';
    1: Result := 'SIM';
    else begin
      Geral.MB_Erro('Número não definido na "function Geral.IntToSimNao".');
      Result := '???';
    end;
  end;
end;

function TUnDmkProcFunc.IntToTempo(Minutos: Integer): String;
var
  d, h, n, x: Integer;
  s: String;
begin
  s := '';
  d := Minutos div 1440;
  x := Minutos mod 1440;
  h := x div 60;
  n := x mod 60;
  //
  if d > 0 then s := s + FormatFloat('0', d) + ' d';
  if h > 0 then s := s + '  ' + FormatFloat('0', h) + ' h';
  if n > 0 then s := s + '  ' + FormatFloat('0', n) + ' min';
  //
  Result := s;
end;

function TUnDmkProcFunc.AdicionaNoDiretorio(Diretorio, ItemAdicionar: String): String;
var
  Dir: String;
begin
  if Diretorio <> EmptyStr then
  begin
    if Pos('/', Diretorio) > 0 then
      Dir := InverteBarras(Diretorio)
    else
      Dir := Diretorio;
    //
    if Dir[Length(Dir)] <> '\' then
      Dir := Dir + '\';
    //
    Result := Dir + ItemAdicionar;
  end else
    Result := ItemAdicionar;
end;

function TUnDmkProcFunc.InverteBarras(Atual: String): String;
var
  i: integer;
begin
  Result := '';
  if Atual = '' then exit;
  for i := 1 to Length(Atual) do
  begin
    if Atual[i] = '\' then Result := Result + '/' else
    if Atual[i] = '/' then Result := Result + '\' else
                           Result := Result + Atual[i];
  end;
end;

function TUnDmkProcFunc.IsConnectedToNet(HostIP: AnsiString; HostPort,
  CancelTimeMs: Word; FirstOctet: Byte; PError: PChar): Boolean;
  function BlockingHookProc: Boolean; stdcall;
  var
    Posicao: LongWord;
    Aviso: String;
  begin
    { Returns False to end Winsock internal testing loop }
    Result := False;

    { Verify time expiration, taking into account rare but possible counter recycling (49.7 days) }
    if GetTickCount < InitialTick then DifTick := $FFFFFFFF - InitialTick + GetTickCount
    else
      DifTick := GetTickCount - InitialTick;

    //
    // Inicio Marco Arend
    Aviso := 'Conectando a porta '+ IntToStr(VAR_PORTA) + ' do IP ' + VAR_IP +
             '...' + FormatFloat('0', DifTick/1000);
    if VAR_CONNECTIPLABEL1TX <> nil then VAR_CONNECTIPLABEL1TX.Caption := Aviso;
    if VAR_CONNECTIPLABEL2TX <> nil then VAR_CONNECTIPLABEL2TX.Caption := Aviso;
    if VAR_CONNECTIPLABEL1TX <> nil then VAR_CONNECTIPLABEL1TX.Update;
    if VAR_CONNECTIPLABEL2TX <> nil then VAR_CONNECTIPLABEL2TX.Update;
    //
    if VAR_CONNECTIPPROGRESS <> nil then
    begin
      // em teste... ver...
      Posicao := DifTick;//VAR_CONNECTIPPROGRESS.Max - DifTick;
      VAR_CONNECTIPPROGRESS.Position  := Posicao;//VAR_CONNECTIPPROGRESS.Max - DifTick;
      VAR_CONNECTIPPROGRESS.Update;
      // Não fazer...problemas
      //Application.ProcessMessages;
    end;
    //
    // Final Marco Arend
    //
    { Limit time expired, then cancel Winsock operation }
    //if (DifTick > WaitTimeMs) and WSAIsBlocking then WSACancelBlockingCall;
    if (DifTick > WaitTimeMs) then
    begin
      Result := True;
      Exit;
    end;
  end;
var
  GInitData: TWSADATA;
  SockDescript: TSocket;
  SockAddr: Winapi.WinSock.TSockAddr;
  NameLen: Integer;

  { Auxiliary procedure just to format error string }
  procedure SaveError(Proc: string; const LastError: Integer);
  begin
    //ver aqui erros: win SDK WSAGetyLastErrors
    StrLCopy(PError, PChar(Proc + ' - Erro nº.' + IntToStr(LastError)), 255);
  end;

{ Auxiliary function to return a random IP address, but keeping some desired octets fixed at left.
  FirstOctet gives the order of the octet (1 to 4, left to right) from which to randomize }
  function GetRandomSimilarIP(InitIP: AnsiString): AnsiString;
  var
    Index: Integer;
    P1, P2: PChar;
  begin
    Result := '';

    InitIP := InitIP + '.';  // Final dot added to simplify algorithm

    P1 := @InitIP[1];

    for Index := 1 to 4 do
    begin  // Extracts octets from initial IP address
      P2 := StrPos(P1, '.');

      if Index < FirstOctet then Result := Result + AnsiString(Copy(P1, 0, P2 - P1))
      else
        Result := Result + AnsiString(IntToStr(1 + Random(254)));

      if Index < 4 then Result := Result + '.'
      else
        Break;

      P1 := P2 + 1;
    end;
  end;
begin
  { Inicializes as not connected }
  Result := False;

(*
  //  Marcelo 2009-07-??
  if String(HostIP) <> Geral.SoNumeroEPonto_TT(String(HostIP)) then
  begin
    Geral.MB_Aviso('IP inválido: [' +HostIP+']'+
    sLineBreak + 'O IP dever ser formado somente por números e pontos.');
    Application.Terminate;
  end;
*)

  WaitTimeMs := CancelTimeMs;

  { Inicializes error string }
  if PError <> nil then PError[0] := #0;

  { Inicializes Winsock 1.1 (don't use Winsock 2+, which doesn't implement such blocking hook) }
  if WSAStartup($101, GInitData) <> 0 then
  begin
    if PError <> nil then SaveError('WSAStartup', WSAGetLastError);
    Exit;
  end;

  try
    { Establishes Winsock blocking hook routine }

    if WSASetBlockingHook(@BlockingHookProc) = nil then
    begin
      if PError <> nil then SaveError('WSASetBlockingHook', WSAGetLastError);
      Exit;
    end;

    try
      { Creates a new socket }
      SockDescript := Socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

      if SockDescript = INVALID_SOCKET then
      begin
        if PError <> nil then SaveError('Socket', WSAGetLastError);
        Exit;
      end;

      try
        { Initializes local socket data }
        SockAddr.sin_family      := AF_INET;
        SockAddr.sin_port        := 0;       // System will choose local port from 1024 to 5000
        SockAddr.sin_addr.S_addr := 0;
        // System will choose the right local IP address, if multi-homed

        { Associates local IP and port with local socket }
        if Bind(SockDescript, SockAddr, SizeOf(SockAddr)) <> 0 then
        begin
          if PError <> nil then SaveError('Bind', WSAGetLastError);
          Exit;
        end;

        { Initializes remote socket data }
        SockAddr.sin_family := AF_INET;
        SockAddr.sin_port   := htons(HostPort);  // Any port number different from 0

        { Does random variation on last octets of specified IP (any valid IP address on desired subnet) }
        if FirstOctet in [1..4] then
          SockAddr.sin_addr := Winapi.WinSock.in_addr(inet_addr(PAnsiChar(PAnsiString(GetRandomSimilarIP(HostIP)))))
            { If FirstOctet = 0 or > 4, does not generate random octets (use exact IP specified) }
        else
          SockAddr.sin_addr := Winapi.WinSock.in_addr(inet_addr(PAnsiChar(PAnsiString(HostIP))));

        { Inicializes time counter }
        InitialTick := GetTickCount;

        { Tries to connect }
        if Connect(SockDescript, SockAddr, SizeOf(SockAddr)) <> 0 then
        begin
          { Tests if it is connected }
          Result := (WSAGetLastError = WSAECONNREFUSED) or  // Connection refused (10061)
            (WSAGetLastError = WSAEINTR) or
            // Interrupted system call (10004)
            (WSAGetLastError = WSAETIMEDOUT);
          // Connection timed out (10060)

          { It may have occurred an error but testing indicated being connected }
          if PError <> nil then SaveError('Conexão', WSAGetLastError);
          ZZTerminate := True;
        end
        { No error }
        else
        begin
          NameLen := SizeOf(SockAddr);

          { Tries to get remote IP address and port }
          Result := (GetPeerName(SockDescript, SockAddr, NameLen) = 0);

          if not Result and (PError <> nil) then
            SaveError('GetPeerName', WSAGetLastError);
        end;
      finally
        CloseSocket(SockDescript);  // Frees the socket
      end;
    finally
      WSAUnhookBlockingHook;  // Deactivates the blocking hook
    end;
  finally
    WSACleanup;  // Frees Winsock (or decreases use count)
  end;
end;

function TUnDmkProcFunc.IsFileInUse(FileName: string): boolean;
var
 HFileRes : HFILE;
 Res: string[6];

  function CheckAttributes(FileNam: string; CheckAttr: string): Boolean;
  var
    fa: Integer;
  begin
    fa := GetFileAttributes(PChar(FileNam)) ;
    Res := '';

    if (fa and FILE_ATTRIBUTE_NORMAL) <> 0 then
    begin
      Result := False;
      Exit;
    end;

    if (fa and FILE_ATTRIBUTE_ARCHIVE) <> 0 then
      Res := Res + 'A';
    if (fa and FILE_ATTRIBUTE_COMPRESSED) <> 0 then
      Res := Res + 'C';
    if (fa and FILE_ATTRIBUTE_DIRECTORY) <> 0 then
      Res := Res + 'D';
    if (fa and FILE_ATTRIBUTE_HIDDEN) <> 0 then
      Res := Res + 'H';
    if (fa and FILE_ATTRIBUTE_READONLY) <> 0 then
      Res := Res + 'R';
    if (fa and FILE_ATTRIBUTE_SYSTEM) <> 0 then
      Res := Res + 'S';

    Result := AnsiContainsText(String(Res), CheckAttr);
  end; (*CheckAttributes*)

  procedure SetAttr(fName: string) ;
  var
    Attr: Integer;
  begin
    Attr := 0;
    if AnsiContainsText(String(Res), 'A') then
    Attr := Attr + FILE_ATTRIBUTE_ARCHIVE;
    if AnsiContainsText(String(Res), 'C') then
    Attr := Attr + FILE_ATTRIBUTE_COMPRESSED;
    if AnsiContainsText(String(Res), 'D') then
    Attr := Attr + FILE_ATTRIBUTE_DIRECTORY;
    if AnsiContainsText(String(Res), 'H') then
    Attr := Attr + FILE_ATTRIBUTE_HIDDEN;
    if AnsiContainsText(String(Res), 'S') then
    Attr := Attr + FILE_ATTRIBUTE_SYSTEM;

    SetFileAttributes(PChar(fName), Attr) ;
  end; (*SetAttr*)
begin //IsFileInUse
  Result := False;
  if not FileExists(FileName) then exit;
  if CheckAttributes(FileName, 'R') then
  begin
    {
    Result := False;
    if not FileExists(FileName) then exit;
    }
    if Geral.MB_Pergunta(ExtractFileName(FileName) +
    ' é um arquivo somente leitura.' + sLineBreak +
    'Você deseja limpar o sinalizador de leitura?') <> ID_YES then
    begin
      Result := True;
      Exit;
    end;
  end;

  SetFileAttributes(PChar(FileName), FILE_ATTRIBUTE_NORMAL) ;

  SetAttr(FileName) ;

  HFileRes := CreateFile(pchar(FileName), GENERIC_READ or GENERIC_WRITE, 0, nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0) ;
  Result := (HFileRes = INVALID_HANDLE_VALUE) ;
  if not Result then CloseHandle(HFileRes) ;
end; //IsFileInUse

function TUnDmkProcFunc.IsWin2k: Boolean;
begin
  Result := (Win32MajorVersion >= 5) and
    (Win32Platform = VER_PLATFORM_WIN32_NT);
end;

function TUnDmkProcFunc.IsWin3x(): Boolean;
begin
  Result := Win32Platform = VER_PLATFORM_WIN32_NT;
  Result := Result and (Win32MajorVersion = 3) and
    ((Win32MinorVersion = 1) or (Win32MinorVersion = 5) or
    (Win32MinorVersion = 51));
end;

function TUnDmkProcFunc.IsWin7: Boolean;
begin
  Result := (Win32Platform = VER_PLATFORM_WIN32_NT) and
    (Win32MajorVersion = 6) and (Win32MinorVersion = 1);
end;

function TUnDmkProcFunc.IsWin8: Boolean;
begin
  Result := (Win32Platform = VER_PLATFORM_WIN32_NT) and
    (Win32MajorVersion = 6) and (Win32MinorVersion = 2);
end;

function TUnDmkProcFunc.IsWin81: Boolean;
begin
  Result := (Win32Platform = VER_PLATFORM_WIN32_NT) and
    (Win32MajorVersion = 6) and (Win32MinorVersion = 3);
end;

function TUnDmkProcFunc.IsWin10: Boolean;
begin
  Result := (Win32Platform = VER_PLATFORM_WIN32_NT) and
    (Win32MajorVersion = 10) and (Win32MinorVersion = 0);
end;

function TUnDmkProcFunc.IsWinNT(): Boolean;
var
  osv: OSVERSIONINFO;
begin
  osv.dwOSVersionInfoSize := sizeof(osv);
  GetVersionEx(osv);
  result:= osv.dwPlatformId = VER_PLATFORM_WIN32_NT;
end;

function TUnDmkProcFunc.IsWinNT4(): Boolean;
begin
  Result := Win32Platform = VER_PLATFORM_WIN32_NT;
  Result := Result and (Win32MajorVersion = 4);
end;

function TUnDmkProcFunc.IsWinVista: Boolean;
begin
  Result := (Win32Platform = VER_PLATFORM_WIN32_NT) and
    (Win32MajorVersion = 6) and (Win32MinorVersion = 0);
end;

function TUnDmkProcFunc.IsWinXP(): Boolean;
begin
  Result := (Win32Platform = VER_PLATFORM_WIN32_NT) and
    (Win32MajorVersion = 5) and (Win32MinorVersion = 1);
end;

function TUnDmkProcFunc.ITB(Valor: Integer): Boolean;
begin
  Result := not ((Valor/2) = (Valor div 2));
end;

function TUnDmkProcFunc.ITS_Null(Inteiro: Integer): String;
begin
  if Inteiro = 0 then
    Result := ''
  else
    Result := FormatFloat('0', Inteiro);
end;

function TUnDmkProcFunc.ITS_Zero(Inteiro: Integer): String;
begin
  Result := FormatFloat('0', Inteiro);
end;

procedure TUnDmkProcFunc.LeMeuTexto(Texto, Titulo: String);
begin
  Geral.MB_Info(Texto);
end;

procedure TUnDmkProcFunc.LeTexto_Permanente(Texto, Titulo: String);
begin
  LeMeuTexto(Texto, Titulo);
end;

function TUnDmkProcFunc.LimpaSeSohCRLF(Texto: String): String;
begin      // #$D#$A
  if Texto = sLineBreak then
    Result := ''
  else
    Result := Texto;
end;

function TUnDmkProcFunc.ListaIndicadorDeIE(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  AddLin(Result, Linha, '0', ['Não definido.']);
  AddLin(Result, Linha, '1', ['Contribuinte ICMS.']);
  AddLin(Result, Linha, '2', ['Contribuinte isento de inscrição no cadastro de contribuintes do ICMS.']);
  AddLin(Result, Linha, '9', ['Não Contribuinte. (ex.: Operação com exterior na NFe ou NFCe)']);
end;

function TUnDmkProcFunc.LoadArquivoToText(const Arquivo: String;
  var Texto: String): Boolean;
var
  fEntrada: file;
  NumRead, i: integer;
  buf: char;
begin
  AssignFile(fEntrada, Arquivo);
  Reset(fEntrada, 1);
  Texto := '';

  for i := 1 to FileSize(fEntrada) do
  begin
    Blockread(fEntrada, buf, 1, NumRead);
    Texto := Texto + buf;
  end;

  Result := True;

  CloseFile(fEntrada);
end;

function TUnDmkProcFunc.LoadFileToText(const FileName: TFileName): string;
begin
(* Erro no Delphi XE2 *)
 //Alterado em 2014-10-26
  CarregaArquivo(FileName, Result);
(*
  with TFileStream.Create(FileName,
      fmOpenRead or fmShareDenyWrite) do begin
    try
      SetLength(Result, Size);
      Read(Pointer(Result)^, Size);
    except
      Result := '';
      Free;
      raise;
    end;
    Free;
  end;
*)
end;

function TUnDmkProcFunc.MenorFloat(Valores: array of Double): Double;
var
  I, N: Integer;
begin
  Result := 0;
  N := Length(Valores);
  if N > 0 then
  begin
    Result := Valores[0];
    if N > 1 then
    begin
      for I := Low(Valores) to High(Valores) do
      begin
        if Result > Valores[I] then
          Result := Valores[I];
      end;
    end;
  end;
end;

function TUnDmkProcFunc.MensalToPeriodo(Mensal: String): Integer;
var
  MeuMensal: String;
  Ano, Mes: Integer;
begin
  MeuMensal := Geral.TST(Mensal, True);
  if Length(MeuMensal) = 7 then
  begin
    Ano := StrToInt(MeuMensal[4]+MeuMensal[5]+MeuMensal[6]+MeuMensal[7]);
    Mes := StrToInt(MeuMensal[1]+MeuMensal[2]);
    Result := ((Ano - 2000) * 12) + Mes;
  end else Result := 0;
end;

function TUnDmkProcFunc.MesDoPeriodo(Periodo: Integer): Integer;
var
  (*Ano,*) Mes: Word;
begin
  //Ano := (Periodo div 12 )+ 2000;
  Mes := (Periodo + (2000 * 12)) mod 12;
  if Mes = 0 then
  begin
    Mes := 12;
    //Ano := Ano -1;
  end;
  Result := Mes;
end;

function TUnDmkProcFunc.MesEAnoDePeriodoShort(Periodo: Integer; var Mes,
  Ano: Word): String;
begin
  if Periodo = -1 then Result := 'ANTES' else
  begin
    Mes := MesDoPeriodo(Periodo);
    Ano := AnoDoPeriodo(Periodo);

    Result := Copy(dmkPF.VerificaMes(Mes, True), 1, 3)+'/'+
              Copy(FormatFloat('0000', Ano), 3, 2);
  end;
end;

function TUnDmkProcFunc.MesEAnoDoPeriodo(Periodo: Integer): String;
var
  Mes, Ano: Word;
begin
  if Periodo = -1 then Result := 'ANTES' else
  begin
    Mes := MesDoPeriodo(Periodo);
    Ano := AnoDoPeriodo(Periodo);

    Result := Copy(dmkPF.VerificaMes(Mes, True), 1, 3)+'/'+
              Copy(FormatFloat('0000', Ano), 3, 2);
  end;
end;

function TUnDmkProcFunc.MesEAnoDoPeriodoLongo(Periodo: Integer): String;
var
  Mes, Ano: Word;
begin
  if Periodo = -10000000 then Result := 'ANTES' else
  begin
    Mes := MesDoPeriodo(Periodo);
    Ano := AnoDoPeriodo(Periodo);

    Result := dmkPF.VerificaMes(Mes, True)+' DE '+FormatFloat('0000', Ano);
  end;
end;

function TUnDmkProcFunc.MesEAnoToMes(MesEAno: String): Integer;
const
  sProcName = 'TUnDmkProcFunc.MesEAnoToMes()';
var
 s: String;
begin
  Result := 0;
  try
    s := MesEAno[4] +
              MesEAno[5] +
              MesEAno[6] +
              MesEAno[7] +
              (*'/'+*)
              MesEAno[1] +
              MesEAno[2]
              (*+'/01'*);
    Result := Geral.IMV(s);
  except
    on E: Exception do
      Geral.MB_Erro(E.Message + sLineBreak + sProcName);
  end;
end;

function TUnDmkProcFunc.MessageDlgCheck(Msg: string; AType: TMsgDlgType;
  AButtons: TMsgDlgButtons; IndiceHelp: Integer; DefButton: TModalResult;
  Portugues, Checar: Boolean; MsgCheck: string; Funcao: TProcedure): Word;
var
  I: Integer;
  Mensagem: TForm;
  Check: TCheckBox;
begin
  Check := nil;
  Mensagem := CreateMessageDialog(Msg, AType, Abuttons);
  Mensagem.HelpContext := IndiceHelp;
  with Mensagem do
  begin
    for i := 0 to ComponentCount - 1 do
    begin
      if (Components[i] is TButton) then
      begin
        if (TButton(Components[i]).ModalResult = DefButton) then
        begin
          ActiveControl := TWincontrol(Components[i]);
        end;
      end;
    end;
    if Portugues then
    begin
      if Atype = mtConfirmation then
        Caption := 'Confirmação'
      else if AType = mtWarning then
        Caption := 'Aviso'
      else if AType = mtError then
        Caption := 'Erro'
      else if AType = mtInformation then
        Caption := 'Informação';
    end;
  end;
  if Portugues then
  begin
    TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
    TButton(Mensagem.FindComponent('NO')).Caption := '&Não';
    TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
    TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
    TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
    TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
    TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
    TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
  end;
  if Checar then
  begin
    Mensagem.ClientHeight := Mensagem.ClientHeight + 40;
    Check := TCheckBox.Create(Mensagem);
    Check.Parent := Mensagem;
    Check.Left := 15;
    Check.Top := Mensagem.ClientHeight - 40;
    Check.Visible := True;
    Check.Caption := MsgCheck;
    Check.Width := Mensagem.ClientWidth - 10;
  end;
  Result := Mensagem.ShowModal;
  if Check.Checked then Funcao;
  Mensagem.Free;
end;

function TUnDmkProcFunc.MezToFDT(Mez, AddDias, TipoFDT: Integer): String;
var
  Data: TDateTime;
  Ano, Mes: Word;
begin
  if Mez = 0 then Result := '' else
  begin
    //Modificado 2010-05-11
    //Ano := Mez div 100 + 2000;
    Ano := Mez div 100;
    if Ano < 1900 then
     Ano := Ano + 2000;
    // fim 2010-05-11
    Mes := Mez mod 100;
    if Mes = 0 then
    begin
      Mes := 12;
      Ano := Ano - 1;
    end;
    Data := EncodeDate(Ano, Mes, 1);
    Data := Data + AddDias;
    //
    Result := Geral.FDT(Data, TipoFDT);
  end;
end;

function TUnDmkProcFunc.MezToMesEAno(Mez: Integer): String;
var
  Ano, Mes: Word;
begin
  if Mez = 0 then Result := '' else
  begin
    //
    //Modificado 2010-05-11
    //Ano := Mez div 100 + 2000;
    Ano := Mez div 100;
    if Ano < 1900 then
     Ano := Ano + 2000;
    // fim 2010-05-11
    //
    Mes := Mez mod 100;
    //
    if Mes = 0 then
    begin
      Mes := 12;
      Ano := Ano - 1;
    end;
    Result := FormatFloat('00', Mes) + '/' + FormatFloat('0000', Ano);
  end;
end;

function TUnDmkProcFunc.MezToMesEAnoCurto(Mez: Integer): String;
var
  Ano, Mes: Word;
begin
  if Mez = 0 then Result := '' else
  begin
    Ano := Mez div 100;
    // Adicionado em 2010-05-11
    while Ano > 100 do
      Ano := Ano - 100;
    // FIm 2010-05-11
    Mes := Mez mod 100;
    //
    if Mes = 0 then
    begin
      Mes := 12;
      Ano := Ano - 1;
    end;
    Result := FormatFloat('00', Mes) + '/' + FormatFloat('00', Ano);
  end;
end;

function TUnDmkProcFunc.MinArrFlu(Valores: array of Double): Double;
var
  I: Integer;
begin
  if Length(Valores) = 0 then
    Result := 0
  //1: Result := Valores[0];
  else
  begin
    Result := Valores[0];
    for I := 1 to Length(Valores) - 1 do
    begin
      if Valores[I] < Result then
        Result := Valores[I];
    end;
  end;
end;

function TUnDmkProcFunc.MinutesToTime(Minutes: Integer): TDateTime;
var
  rHor, rMin: Real;
  iDias: integer;
begin
  rHor := Minutes / 60;
  rMin := Frac(rHor);
  rMin := Round(rMin * 60);
  rHor := Trunc(rHor);

  if rHor >= 24 then
  begin
   iDias := Trunc(rHor / 24);
   rHor  := rHor - (24 * iDias);
  end;

  Result := StrToTime(FloatToStr(rHor) + ':' + FloatToStr(rMin));
end;

function TUnDmkProcFunc.MinutosHT4(Hora: String): Integer;
begin
  while Length(Hora) < 4 do
    Hora := '0' + Hora;
  Result := (Geral.IMV(Hora[1] + Hora[2])) * 60 + Geral.IMV(Hora[3] + Hora[4]);
end;

function TUnDmkProcFunc.MontaDuplicata(FatNum, nNF, Parcela, tpFaturaNum,
  tpFaturaSeq: Integer; Prefixo, Separador: String; ApenasFatura: Boolean): String;
var
  FaturaNum, ParcTxt: String;
begin
  if tpFaturaNum = 0 then
    FaturaNum := FormatFloat('000000', FatNum)
  else
    FaturaNum := FormatFloat('000000', nNF);
  Result := Prefixo + FaturaNum ;
  if not ApenasFatura then
  begin
    ParcTxt := dmkPF.ParcelaFatura(Parcela, tpFaturaSeq);
    if Trim(ParcTxt) <> '' then
      Result := Result + Separador + ParcTxt;
  end;
end;

procedure TUnDmkProcFunc.MostraCalculadoraDoWindows(Sender: TObject);
begin
  WinExecAndWaitOrNot('Calc.exe', SW_SHOWNORMAL, nil, True);
end;

function TUnDmkProcFunc.MotivDel_ObtemDescricao(Motivo: Integer): String;
var
  I, Motiv: Integer;
begin
  Result := '';
  //
  if MotivDel_ValidaCodigo(Motivo) > 0 then
  begin
    for I := Low(ListaMotivosExclusao_Int) to High(ListaMotivosExclusao_Int) do
    begin
      Motiv := ListaMotivosExclusao_Int[I];
      //
      if Motiv = Motivo then
      begin
        Result := ListaMotivosExclusao_Str[I];
        Break;
      end;
    end;
  end;
end;

function TUnDmkProcFunc.MotivDel_ValidaCodigo(Motivo: Integer): Integer;
var
  I, Motiv: Integer;
begin
  Result := -1;
  //
  for I := Low(ListaMotivosExclusao_Int) to High(ListaMotivosExclusao_Int) do
  begin
    Motiv := ListaMotivosExclusao_Int[I];
    //
    if Motiv = Motivo then
    begin
      Result := Motiv;
      Break;
    end;
  end;
  if Result = -1 then
    Geral.MB_Erro('O motivo informado não foi localizado!' + sLineBreak +
      'TUnDmkProcFunc.MotivDel_ValidaCodigo');
end;

procedure TUnDmkProcFunc.MoveArq(const FileName, DestName: string);
var
  Destination: string;
begin
  Destination := ExpandFileName(DestName); { expand the destination path }
  if not RenameFile(FileName, Destination) then { try just renaming }
  begin
    //if HasAttr(FileName, faReadOnly) then  { if it's read-only... }
    //raise EFCantMove.Create(Format(SFCantMove, [FileName])); { we wouldn't be able to delete it }
    CopiaArq(FileName, Destination); { copy it over to destination...}
    System.SysUtils.DeleteFile(FileName); { ...and delete the original }
  end;
end;

function TUnDmkProcFunc.MoveDir(const fromDir, toDir: string;
  Sobrescrever: Boolean): Boolean;
var
  fos: TSHFileOpStruct;
begin
  ZeroMemory(@fos, SizeOf(fos));
  with fos do
  begin
    wFunc  := FO_MOVE;
    if Sobrescrever then
      fFlags := FOF_NOCONFIRMATION
    else
      fFlags := FOF_FILESONLY;
    pFrom  := PChar(fromDir + #0);
    pTo    := PChar(toDir)
  end;
  Result := (0 = ShFileOperation(fos));
end;

procedure TUnDmkProcFunc.MoverArquivo(OrigArquivo, DestArquivo: String);
begin
  //////////////////////////////////////////////////////////////////////////////
  // OrigArquivo => Caminho completo do diretório origem + o nome do arquivo  //
  // DestArquivo => Caminho completo do diretório destino + o nome do arquivo //
  //////////////////////////////////////////////////////////////////////////////
  TFile.Move(OrigArquivo, DestArquivo);
end;

function TUnDmkProcFunc.MTHT(Minutos: Integer; Formato: String): String;
begin
  Result := FormatDateTime(Formato, Minutos / 1440);
end;

function TUnDmkProcFunc.MudaExtensaoDeArquivo(Arq, Ext: String): String;
var
  I : integer;
begin
  I := Pos('.', Arq);
  if I > 0 then
    Result := String(PChar(Copy(Arq, 1, Length(Arq)-3))) + Ext
  else
    Result := Arq;
end;

function TUnDmkProcFunc.MyAnsiToUtf8(x: ansistring): string;
  { Function that recieves ansi string and converts
    to UTF8 string }
var
  i: integer;
  b1, b2: byte;
begin
  Result := x;
  for i := Length(Result) downto 1 do
    if Result[i] >= #127 then begin
      b1 := $C0 or (ord(Result[i]) shr 6);
      b2 := $80 or (ord(Result[i]) and $3F);
      Result[i] := chr(b1);
      Insert(chr(b2), Result, i + 1);
    end;
end;

function TUnDmkProcFunc.NomeDiaDaSemana_Artigo(Dia: Integer; IniDom: Boolean): String;
begin
  if IniDom then
  begin
    case Dia of
      1: Result := 'o'; //'Domingo';
      2: Result := 'a'; //'Segunda-feira';
      3: Result := 'a'; //'Terça-feira';
      4: Result := 'a'; //'Quarta-feira';
      5: Result := 'a'; //'Quinta-feira';
      6: Result := 'a'; //'Sexta-feira';
      7: Result := 'o'; //'Sábado';
      else Result := '?';
    end;
  end else
  begin
    case Dia of
      1: Result := 'a'; //'Segunda-feira';
      2: Result := 'a'; //'Terça-feira';
      3: Result := 'a'; //'Quarta-feira';
      4: Result := 'a'; //'Quinta-feira';
      5: Result := 'a'; //'Sexta-feira';
      6: Result := 'o'; //'Sábado';
      7: Result := 'o'; //'Domingo';
      else Result := '?';
    end;
  end;
end;

function TUnDmkProcFunc.NomeDoNivelSelecionado(Tipo, Nivel: Integer): String;
begin
  case Tipo of
    1:
    case Nivel of
      1: Result := 'contas';
      2: Result := 'subgrupos';
      3: Result := 'grupos';
      4: Result := 'conjuntos';
      5: Result := 'plano';
      else Result := '?';
    end;
    2:
    case Nivel of
      1: Result := 'la.Genero';
      2: Result := 'co.Subgrupo';
      3: Result := 'sg.Grupo';
      4: Result := 'gr.Conjunto';
      5: Result := 'cj.Plano';
      else Result := '?';
    end;
    3:
    case Nivel of
      1: Result := 'da Conta';
      2: Result := 'do Sub-grupo';
      3: Result := 'do Grupo';
      4: Result := 'do Conjunto';
      5: Result := 'do Plano';
      else Result := ' do Nivel ? ';
    end;
    4:
    case Nivel of
      1: Result := 'lct';
      2: Result := 'con';
      3: Result := 'sgr';
      4: Result := 'gru';
      5: Result := 'cjt';
      else Result := '?';
    end;
    5:
    case Nivel of
      1: Result := 'Genero';
      2: Result := 'Subgrupo';
      3: Result := 'Grupo';
      4: Result := 'Conjunto';
      5: Result := 'Plano';
      else Result := '?';
    end;
    else Result := '(Tipo)=?';
  end;
end;

function TUnDmkProcFunc.NaoQueroLocalizarAsImagens(Localizar: Boolean): Boolean;
begin
  // Porque nao tenho os arquivos de imagem!
  if (VAR_USUARIO = -1) and Localizar then
  begin
    Result := Geral.MB_Pergunta(
    'Deseja realmente tentar localizar as imagens?') <> ID_YES;
  end else
    Result := False;
end;

function TUnDmkProcFunc.NivelSelecionado(Nivel, Tipo: Integer; Compl_Pre,
  Compl_Pos: String): String;
const
  Txt0: array[0..5] of String = ('Nenhum', 'Conta', 'Sub-grupo', 'Grupo', 'Conjunto', 'Plano');
  Txt1: array[0..5] of String = ('do ???', 'da conta', 'do sub-grupo', 'do grupo', 'do conjunto', 'do plano');
  Txt2: array[0..5] of String = ('#ERRO#', 'contas', 'subgrupos', 'grupos', 'conjuntos', 'plano');
var
  Texto: String;
begin
  case Tipo of
    0: Texto := Txt0[Nivel];
    1: Texto := Txt1[Nivel];
    2: Texto := Txt2[Nivel];
    else Texto := '? ? ?';
  end;
  Result := Compl_pre + Texto + Compl_Pos;
end;

function TUnDmkProcFunc.NomeDiaDaSemana(Dia: Integer; IniDom: Boolean): String;
begin
  if IniDom then
  begin
    case Dia of
      1: Result := 'Domingo';
      2: Result := 'Segunda-feira';
      3: Result := 'Terça-feira';
      4: Result := 'Quarta-feira';
      5: Result := 'Quinta-feira';
      6: Result := 'Sexta-feira';
      7: Result := 'Sábado';
      else Result := '* ERRO *';
    end;
  end else
  begin
    case Dia of
      1: Result := 'Segunda-feira';
      2: Result := 'Terça-feira';
      3: Result := 'Quarta-feira';
      4: Result := 'Quinta-feira';
      5: Result := 'Sexta-feira';
      6: Result := 'Sábado';
      7: Result := 'Domingo';
      else Result := '* ERRO *';
    end;
  end;
end;

function TUnDmkProcFunc.NomeLongoparaCurto(NomeLongo: AnsiString): AnsiString;
begin
// Acerta tamanho para o nome curto
  Result := '';
  SetLength(Result, MAX_PATH);
  if GetShortPathNameA(PAnsiChar(NomeLongo), PAnsiChar(Result),
      Length(Result)) = 0 then
    Result := NomeLongo
  else
    SetLength(Result,StrLen(PAnsiChar(Result)));
end;

function TUnDmkProcFunc.NomeMoeda(Moeda: Integer): String;
begin
  case Moeda of
    0: Result := VAR_MOEDA_R;
    1: Result := VAR_MOEDA_U;
    2: Result := VAR_MOEDA_E;
    3: Result := 'Idx';
    else Result := '??';
  end;
end;

function TUnDmkProcFunc.NomeSimplesValido(const Nome: String): Boolean;
var
  I: Integer;
begin
  Result := True;
  for I := Low(Nome) to High(Nome) - 1 do
  begin
    if (Nome[I] <> '_') and (pos(Nome[I], CodeArq) = 0) then
    begin
      Result := False;
      Geral.MB_Aviso(Nome + ' não é um nome válido!');
      Exit;
    end;
  end;
end;

function TUnDmkProcFunc.NomeTabelaLtc(Empresa: Integer;
  Letra: TTipoTab): String;
var
  Caractere: String;
begin
  case Letra of
    ttA: Caractere := 'a';
    ttB: Caractere := 'b';
    //ttC: Caractere := 'c';
    ttD: Caractere := 'd';
    else Caractere := '?'
  end;
  Result := TMeuDB + '.lct' + FormatFloat('0000', Empresa) + Caractere;
end;

function TUnDmkProcFunc.NumeroToLetras(Numero: Integer): String;
const
  Letras = 26;
var
  Num, I, N: Extended;
begin
  Result := '';
  Num := Numero;
  while Num > 0 do
  begin
    I := Num;
    N := 0;
    while I > 26 do
    begin
      N := N + 1;
      I := Int(I / 26);
    end;
    Num := Num - (I * Power(26, N));
    //
    Result := Result + Char(64 + Trunc(I+0.5));
  end;
end;

function TUnDmkProcFunc.NumTex(Text: String): Integer;
var
  i, j: Integer;
  s: String;
begin
  j := Length(Text);
  if j = 0 then
  begin
    Result := 0;
    Exit;
  end;
  s := '';
  for i := 1 to j do
  //if Text[i] in (['0'..'9']) then s := s + '1' else s := s + '2';
{$IFDEF DELPHI12_UP}
  if CharInSet(Text[i], (['0'..'9'])) then s := s + '1' else s := s + '2';
{$ELSE}
  if Text[i] in (['0'..'9']) then s := s + '1' else s := s + '2';
{$ENDIF}
  Result := StrToInt(s);
end;

function TUnDmkProcFunc.ObtemInfoRegEdit(Key, NomeForm: String;
  Default: Variant; KeyType: TKeyType): Variant;
var
  Cam: String;
begin
  Cam := Application.Title+'\InitialConfig\'+NomeForm;
  // ini 2022-03-01
  //Result := Geral.ReadAppKeyLM(Key, Cam, KeyType, Default);
  Result := Geral.ReadAppKeyCU(Key, Cam, KeyType, Default);
  // fim 2022-03-01
end;

function TUnDmkProcFunc.ObtemInt32Random(): Integer;
begin
  Randomize();
  Result := Random(2147483647);
end;

procedure TUnDmkProcFunc.ObtemListaDeDrivesAtivos(Lista: TStrings);
var
  vDrivesSize: Cardinal;
  vDrives: array[0..128] of Char;
  vDrive: PChar;
begin
  Lista.BeginUpdate;
  try
  Lista.Clear;
  vDrivesSize := GetLogicalDriveStrings(SizeOf(vDrives), vDrives);
  //
  if vDrivesSize = 0 then
    Exit;
  //
  vDrive := vDrives;
  //
  while vDrive^ <> #0 do
  begin
    Lista.Add(StrPas(vDrive));
    Inc(vDrive, SizeOf(vDrive));
  end;
  finally
    Lista.EndUpdate;
  end;
end;

function TUnDmkProcFunc.ObtemNomeArqRandom(Tamanho: Integer): String;
var
  I, N, K: Integer;
begin
  Randomize();
  Result := '';
  K := Length(CodeArq);
  for I := 1 to Tamanho do
  begin
    //Randomize();
    N := Random(K);
    while (N < 1) or (N > K) do
      N := Random(K);
    Result := Result + CodeArq[N];
  end;
end;

function TUnDmkProcFunc.ObtemNomeEmailTipo(TipoEmail: TMaiEnvType): String;
begin
  case TipoEmail of
    meAvul:
      Result := 'Avulso';
    meNFSe:
      Result := 'NFSe - Nota fiscal eletrônica de Serviço';
    meNFe:
      Result := 'NFe - Nota fiscal eletrônica';
    meNFeEveCCE:
      Result := 'NFe - Evento Carta De Correção Eletrônica';
    meNFeEveCan:
      Result := 'NFe - Evento de cancelamento';
    meBloq:
      Result := 'Envio de bloquetos';
    meCobr:
      Result := 'Cobrança';
    meCTe:
      Result := 'CTe - Conhecimento de Transporte Eletrônico';
    meMDFe:
      Result := 'MDFe - Manifesto Eletrônico de Documentos Fiscais';
    meBloqCnd:
      Result := 'Envio de bloquetos - Condomínios';
    meHisAlt:
      Result := 'Aplicativos - Histórico de alterações';
  end;
end;

function TUnDmkProcFunc.ObtemTempoInativoDoAplicativo: Word;
var
  liInfo: TLastInputInfo;
begin
  liInfo.cbSize := SizeOf(TLastInputInfo);
  GetLastInputInfo(liInfo);
  //
  Result := (GetTickCount - liInfo.dwTime) DIV 1000; //Segundos
end;

function TUnDmkProcFunc.ObtemTipoDocTxtDeCPFCNPJ(CPFCNPJ: String): String;
begin
  case Length(Geral.SoNumero_TT(CPFCNPJ)) of
    11: Result := 'CPF';
    14..15: Result := 'CNPJ';
    else Result := 'Doc.';
  end;
end;

function TUnDmkProcFunc.ObtemValorDeTag(const Tag: String;
  var Texto, Valor: String): Boolean;
var
  TagI, TagF: String;
  posI, posF, TamI, TamF: Integer;
begin
  Result := False;
  Valor := '';
  TagI := '<' + Tag;
  TagF := '</' + Tag + '>';
  TamI := Length(TagI);
  TamF := Length(TagF);
  case pos(TagI, Texto) of
    0: Geral.MB_Erro('Tag "' + Tag + '" não localizada no texto:' + sLineBreak +
    Texto);
    1:
    begin
      posI := pos(TagI, Texto);
      Texto := Copy(Texto, posI + TamI);
      posI := pos('>', Texto) + 1;
      posF := pos(TagF, Texto);
      Valor := Copy(Texto, posI, posF - PosI);
      Texto := Copy(Texto, posF + TamF);
      Result := True;
    end;
    else Geral.MB_Erro('Tag "' + Tag + '" não localizada na posição 1 do texto:' + sLineBreak +
    Texto);
  end;
end;

function TUnDmkProcFunc.ObtemValorDeTag2(Texto, Chave: String;
  MantemChave: Boolean): String;
begin
  Result := SeparaDadosCampoDeXML(Texto, Chave, MantemChave);
end;

function TUnDmkProcFunc.ObtemValorDouble(var Valor: Double;
  const Casas: Integer): Boolean;
var
  ValTxt, Confere: String;
begin
  ValTxt := Geral.FFT(Valor, Casas, siNegativo);
  if InputQuery('Obtenção de valor', 'Informe o valor', ValTxt) then
  begin
    Valor   := Geral.DMV(ValTxt);
    Confere := Geral.FFT(Valor, Casas, siNegativo);
    Result := Geral.DMV(Confere) = Geral.DMV(ValTxt);
    if not Result then Geral.MB_Aviso('Valor inválido: ' + Confere);
  end else Result := False;
end;

function TUnDmkProcFunc.OSArchitectureToStr(
  const a: TOSVersion.TArchitecture): string;
begin
  case a of
    arIntelX86: Result := 'Intel X86';
    arIntelX64: Result := 'Intel X64';
    else
      Result := 'UNKNOWN OS Architecture';
  end;
end;

function TUnDmkProcFunc.ParcelaFatura(Parcela, Tipo: Integer): String;
begin
  case Tipo of
    0: Result := dmkPF.FFP(Parcela, 0);
    1: Result := IntToColTxt(Parcela);
    else Result := '?';
  end;
end;

function TUnDmkProcFunc.ParseText(Texto: AnsiString; Decode: Boolean = True;
   IsUTF8: Boolean = True ): AnsiString;
begin
  if Decode then
   begin
    Texto := AnsiString(StringReplace(String(Texto), 'amp;amp;', '&', [rfReplaceAll]));  // 2023-12-25
    Texto := AnsiString(StringReplace(String(Texto), '&amp;', '&', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&lt;', '<', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&gt;', '>', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&quot;', '"', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&#39;', #39, [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&aacute;', 'á', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Aacute;', 'Á', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&acirc;' , 'â', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Acirc;' , 'Â', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&atilde;', 'ã', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Atilde;', 'Ã', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&agrave;', 'à', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Agrave;', 'À', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&eacute;', 'é', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Eacute;', 'É', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&ecirc;' , 'ê', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Ecirc;' , 'Ê', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&iacute;', 'í', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Iacute;', 'Í', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&oacute;', 'ó', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Oacute;', 'Ó', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&otilde;', 'õ', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Otilde;', 'Õ', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&ocirc;' , 'ô', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Ocirc;' , 'Ô', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&uacute;', 'ú', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Uacute;', 'Ú', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&uuml;'  , 'ü', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Uuml;'  , 'Ü', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&ccedil;', 'ç', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&Ccedil;', 'Ç', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '&apos;', '''', [rfReplaceAll]));
    if IsUTF8 then
    begin
      {$IFDEF DELPHI12_UP}  // delphi 2009 em diante
       Texto := AnsiString(UTF8ToString(Texto));
      {$ELSE}
       Texto := AnsiString(UTF8Decode(Texto));
      {$ENDIF}
    end ;
   end
  else
   begin
    Texto := AnsiString(StringReplace(String(Texto), '&', '&amp;', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '<', '&lt;', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '>', '&gt;', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), '"', '&quot;', [rfReplaceAll]));
    Texto := AnsiString(StringReplace(String(Texto), #39, '&#39;', [rfReplaceAll]));
    if IsUTF8 then
    begin
      {$IFDEF DELPHI12_UP}  // delphi 2009 em diante
       Texto := AnsiString(UTF8ToString(Texto));
      {$ELSE}
       Texto := AnsiString(UTF8Decode(Texto));
      {$ENDIF}
    end ;
   end;

  Result := Texto;
end;

function TUnDmkProcFunc.ParValueCodTxt(Compl, Nome: String;
  Codigo: Variant; Todos: String): String;
begin
  Result := '';
  //
  if Compl <> '' then
    Result := Result + Compl + ' ';
  if (Codigo <> Null) and (Nome <> '') then
    Result := Result + Geral.VariantToString(Codigo) + ' - ';
  if Nome = '' then
    Result := Result + Todos
  else
    Result := Result + Nome;
end;

{
function TUnDmkProcFunc.ParseText(Texto: AnsiString;
  Decode: Boolean): AnsiString;
begin
  Result := Texto;
  if Decode then
  begin
    Result := StringReplace(Result, '&amp;', '&', [rfReplaceAll]);
    Result := StringReplace(Result, '&lt;', '<', [rfReplaceAll]);
    Result := StringReplace(Result, '&gt;', '>', [rfReplaceAll]);
    Result := StringReplace(Result, '&quot;', '"', [rfReplaceAll]);
    Result := StringReplace(Result, '&#39;', #39, [rfReplaceAll]);
    Result := StringReplace(Result, '&aacute;', 'á', [rfReplaceAll]);
    Result := StringReplace(Result, '&Aacute;', 'Á', [rfReplaceAll]);
    Result := StringReplace(Result, '&acirc;' , 'â', [rfReplaceAll]);
    Result := StringReplace(Result, '&Acirc;' , 'Â', [rfReplaceAll]);
    Result := StringReplace(Result, '&atilde;', 'ã', [rfReplaceAll]);
    Result := StringReplace(Result, '&Atilde;', 'Ã', [rfReplaceAll]);
    Result := StringReplace(Result, '&agrave;', 'à', [rfReplaceAll]);
    Result := StringReplace(Result, '&Agrave;', 'À', [rfReplaceAll]);
    Result := StringReplace(Result, '&eacute;', 'é', [rfReplaceAll]);
    Result := StringReplace(Result, '&Eacute;', 'É', [rfReplaceAll]);
    Result := StringReplace(Result, '&ecirc;' , 'ê', [rfReplaceAll]);
    Result := StringReplace(Result, '&Ecirc;' , 'Ê', [rfReplaceAll]);
    Result := StringReplace(Result, '&iacute;', 'í', [rfReplaceAll]);
    Result := StringReplace(Result, '&Iacute;', 'Í', [rfReplaceAll]);
    Result := StringReplace(Result, '&oacute;', 'ó', [rfReplaceAll]);
    Result := StringReplace(Result, '&Oacute;', 'Ó', [rfReplaceAll]);
    Result := StringReplace(Result, '&otilde;', 'õ', [rfReplaceAll]);
    Result := StringReplace(Result, '&Otilde;', 'Õ', [rfReplaceAll]);
    Result := StringReplace(Result, '&ocirc;' , 'ô', [rfReplaceAll]);
    Result := StringReplace(Result, '&Ocirc;' , 'Ô', [rfReplaceAll]);
    Result := StringReplace(Result, '&uacute;', 'ú', [rfReplaceAll]);
    Result := StringReplace(Result, '&Uacute;', 'Ú', [rfReplaceAll]);
    Result := StringReplace(Result, '&uuml;'  , 'ü', [rfReplaceAll]);
    Result := StringReplace(Result, '&Uuml;'  , 'Ü', [rfReplaceAll]);
    Result := StringReplace(Result, '&ccedil;', 'ç', [rfReplaceAll]);
    Result := StringReplace(Result, '&Ccedil;', 'Ç', [rfReplaceAll]);
    Result := StringReplace(Result, '&copy', '©', [rfReplaceAll]);
    Result := StringReplace(Result, '&Copy', '©', [rfReplaceAll]);
    Result := UTF8Decode(Result);
  end
  else
  begin
    Result := StringReplace(Result, '&', '&amp;', [rfReplaceAll]);
    Result := StringReplace(Result, '<', '&lt;', [rfReplaceAll]);
    Result := StringReplace(Result, '>', '&gt;', [rfReplaceAll]);
    Result := StringReplace(Result, '"', '&quot;', [rfReplaceAll]);
    Result := StringReplace(Result, #39, '&#39;', [rfReplaceAll]);
    Result := UTF8Encode(Result);
  end;
  //
  //Result := Result;
end;
}

procedure TUnDmkProcFunc.PeriodoDecode(const Periodo: Integer; var Ano, Mes: Word);
begin
  Ano := (Periodo div 12 ) + 2000;
  Mes := Periodo mod 12;
  if Mes = 0 then
  begin
    Mes := 12;
    if Ano > 0 then
      Ano := Ano -1;
  end;
end;

function TUnDmkProcFunc.PeriodoEncode(Ano, Mes: Integer): Integer;
begin
  Result := ((Ano - 2000) * 12) + Mes;
end;

function TUnDmkProcFunc.PeriodoImp(Data1I, Data1F, Data2I, Data2F: TDateTime;
  Ck1I, Ck1F, Ck2I, Ck2F: Boolean; Texto1, Texto2: String): String;
begin
  Result := '';
  if (Ck1I or Ck1F) then
  begin
    Result := Result + Texto1 + ' ';
    if (Ck1I and Ck1F) then
      Result := Result + FormatDateTime(String(VAR_FORMATDATE2), Data1I) +
      CO_ATE + FormatDateTime(VAR_FORMATDATE2, data1F)
    else if Ck1I then
      Result := Result + FormatDateTime(String(VAR_FORMATDATE2), Data1I)
    else if Ck1F then
      Result := Result + FormatDateTime(String(VAR_FORMATDATE2), Data1F);
    if Texto2 <> '' then
      Result := Result + sLineBreak + Texto2 + ' ';
  end;
  if (Ck2I or Ck2F) then
  begin
    Result := Result + Texto2 + ' ';
    if (Ck2I and Ck2F) then
      Result := Result + FormatDateTime(String(VAR_FORMATDATE2), Data2I) +
      CO_ATE + FormatDateTime(VAR_FORMATDATE2, data1F)
    else if Ck2I then
      Result := Result + FormatDateTime(String(VAR_FORMATDATE2), Data2I)
    else if Ck2F then
      Result := Result + FormatDateTime(String(VAR_FORMATDATE2), Data2F);
  end;
end;

function TUnDmkProcFunc.PeriodoImp1(DataI, DataF: TDateTime; CkI, CkF: Boolean;
  TextoI, Agregador, TextoF: String): String;
begin
  Result := '';
  if (CkI or CkF) then
  begin
    Result := Result + TextoI + ' ';
    if (CkI and CkF) then
      Result := Result + FormatDateTime(String(VAR_FORMATDATE2), DataI) +
      CO_ATE + FormatDateTime(VAR_FORMATDATE2, dataF)
    else if CkI then
// ini 2023-04-27
      //Result := Result + FormatDateTime(String(VAR_FORMATDATE2), DataI)
      Result := Result + FormatDateTime(String(VAR_FORMATDATE2), DataI) + ' em diante'
    else if CkF then
      //Result := Result + FormatDateTime(String(VAR_FORMATDATE2), DataF);
      Result := Result + CO_ATE + FormatDateTime(String(VAR_FORMATDATE2), DataF);
// fim 2023-04-27
    if TextoF <> '' then Result := Result + ' ' + Agregador;
  end;
  Result := Result + ' ' + TextoF;
end;

function TUnDmkProcFunc.PeriodoImp2(DataI, DataF: TDateTime; CkI, CkF: Boolean;
  TextoI, Agregador, TextoF: String): String;
var
 i, f: Integer;
begin
  Result := '';
  if (CkI or CkF) then
  begin
    Result := Result + TextoI + ' ';
    if (CkI and CkF) then
    begin
      i := Trunc(DataI);
      f := Trunc(DataF);
      if i > f then
      begin
        i := Trunc(DataF);
        f := Trunc(DataI);
      end;
      if (Geral.PrimeiroDiaDoMes(i) = i) and (Geral.UltimoDiaDoMes(f) = f) and
      (f - i > 27) and (f-i < 32) then
        Result := Result + AnsiUppercase (Geral.FDT(i, 17))
      else
        Result := Result + FormatDateTime(VAR_FORMATDATE2, DataI) +
        CO_ATE + FormatDateTime(VAR_FORMATDATE2, dataF)
    end else if CkI then
      Result := Result + FormatDateTime(VAR_FORMATDATE2, DataI) + ' em diante'
    else if CkF then
      Result := Result + 'até ' + FormatDateTime(VAR_FORMATDATE2, DataF);

    if TextoF <> '' then Result := Result + ' ' + Agregador;
  //end else Result:= TextoI + 'Indefinido';
  end else
  // 2015-08-18
  begin
    //if Trim(Result) <> '' then
      Result := Result + '(Qualquer período)';
  end;
  Result := Result + ' ' + TextoF;
end;

function TUnDmkProcFunc.PeriodoImpMezAno(DataI, DataF, TextoI, Agregador,
  TextoF: String): String;
var
  CkI, CkF: Boolean;
begin
  Result := '';
  CkI := Trim(DataI) <> '';
  CkF := Trim(DataF) <> '';
  if (CkI or CkF) then
  begin
    Result := Result + TextoI + ' ';
    if (CkI and CkF) then
      Result := Result + DataI + CO_ATE + DataF
    else if CkI then
      Result := Result + DataI + ' em diante'
    else if CkF then
      Result := Result + 'até ' + DataF;

    if TextoF <> '' then Result := Result + ' ' + Agregador;
  end else Result:= TextoI + 'Indefinido';
  Result := Result + ' ' + TextoF;
end;

function TUnDmkProcFunc.PeriodoToAnoMes(Periodo: Integer): Integer;
var
  Ano, Mes: Integer; // Word não permite negativo, vai de 0 a 65536
begin
  if Periodo < 0 then Periodo := Periodo + (2000 * 12) + 1;
  //
  if Periodo > 0 then
  begin
    Ano := (Periodo div 12);// + 2000;
    Mes := Periodo mod 12;
    if Mes = 0 then
    begin
      Mes := 12;
      Ano := Ano -1;
    end;
    Result := (Ano * 100) + Mes;// - (2000 * 100);
  end else Result := 0;
end;

function TUnDmkProcFunc.PeriodoToMensal(Periodo: Integer): String;
var
  Ano, Mes: Word;
begin
  if Periodo < 1 then Result := '' else
  begin
    Ano := (Periodo div 12 )+ 2000;
    Mes := Periodo mod 12;
    if Mes = 0 then
    begin
      Mes := 12;
      Ano := Ano -1;
    end;
    Result := FormatFloat('00', Mes)+'/'+FormatFloat('0000', Ano);
  end;
end;

function TUnDmkProcFunc.PorcentagemMes: Double;
var
  DiaH, DiaM, Mes, Ano: Word;
begin
  DecodeDate(Date, Ano, Mes, DiaH);
  DecodeDate(Geral.UltimoDiaDoMes(Date), Ano, Mes, DiaM);
  if DiaM = 0 then
    Result := 0
  else
    Result := DiaH / DiaM;
end;

function TUnDmkProcFunc.PortTCPIsOpen(dwPort: Word;
  ipAddressStr: Ansistring): boolean;
var
  client : Winapi.WinSock.sockaddr_in;//sockaddr_in is used by Windows Sockets to specify a local or remote endpoint address
  sock   : Integer;
begin
    client.sin_family      := AF_INET;
    client.sin_port        := htons(dwPort);//htons converts a u_short from host to TCP/IP network byte order.
    client.sin_addr.s_addr := inet_addr(PAnsiChar(ipAddressStr)); //the inet_addr function converts a string containing an IPv4 dotted-decimal address into a proper address for the IN_ADDR structure.
    sock  :=socket(AF_INET, SOCK_STREAM, 0);//The socket function creates a socket
    Result:=connect(sock,client,SizeOf(client))=0;//establishes a connection to a specified socket.
end;

function TUnDmkProcFunc.PositFlu(Valor: Double): Double;
begin
  if Valor < 0 then
    Result := -Valor
  else
    Result := Valor;
end;

function TUnDmkProcFunc.PosLast(const SubStr, S: AnsiString): Integer;
var
  P : Integer;
begin
  Result := 0;
  P := Pos( SubStr, S);
  while P <> 0 do
  begin
     Result := P;
     P := PosEx( SubStr, S, P+1);
  end;
end;

function TUnDmkProcFunc.PrimeiroDiaAposPeriodo(Periodo: Integer;
  Tipo: TDataTipo): String;
var
  Ano, Mes, Dia: Word;
  MyPeriod: Integer;
begin
  if Periodo < -23999 then
  begin
    Ano := 1899;
    Mes := 12;
    Dia := 30;
  end else
  begin
    if Periodo < 0 then
    begin
      MyPeriod := Periodo + (2000 * 12);
      Ano := MyPeriod div 12;
      Mes := MyPeriod mod 12;
    end else begin
      Ano := Periodo div 12 + 2000;
      Mes := Periodo mod 12;
    end;
    if Mes = 0 then
    begin
      Mes := 12;
      Ano := Ano -1;
    end;
    if Mes = 12 then
    begin
      Mes := 1;
      Ano := Ano + 1;
    end else Mes := Mes + 1;
  end;
  if Tipo = dtMySQL then
  begin
    DecodeDate(EncodeDate(Ano, Mes, 1), Ano, Mes, Dia);
    Result := FormatFloat('0000', Ano)+FormatFloat('00', Mes)+FormatFloat('00', Dia);
  end;
  if Tipo = dtDelphi then
    Result := FormatDateTime({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat, EncodeDate(Ano, Mes, 1));
  if Tipo = dtSystem then
    Result := FormatDateTime(String(VAR_FORMATDATE), EncodeDate(Ano, Mes, 1));
  if Tipo = dtSystem2 then
    Result := FormatDateTime(String(VAR_FORMATDATE2), EncodeDate(Ano, Mes, 1));
  if Tipo = dtSystem3 then
    Result := FormatDateTime(String(VAR_FORMATDATE3), EncodeDate(Ano, Mes, 1));
  if Tipo = dtTexto then
    Result := FormatDateTime('mmmmm/yyyy', EncodeDate(Ano, Mes, 1));
end;

function TUnDmkProcFunc.PrimeiroDiaDoPeriodo(Periodo: Integer;
  Tipo: TDataTipo): String;
var
  Ano, Mes: Word;
begin
  Ano := (Periodo div 12 ) + 2000;
  Mes := Periodo mod 12;
  if Mes = 0 then
  begin
    if Ano = 1 then Mes := 1 else
    begin
      Mes := 12;
      Ano := Ano -1;
    end;
  end else if Mes > 12 then Mes := 1;
  try
    if Tipo = dtSystem3 then
      Result := FormatDateTime(String(VAR_FORMATDATE3), EncodeDate(Ano, Mes, 1));
    if Tipo = dtSystem2 then
      Result := FormatDateTime(String(VAR_FORMATDATE2), EncodeDate(Ano, Mes, 1));
    if Tipo = dtSystem then
      Result := FormatDateTime(String(VAR_FORMATDATE), EncodeDate(Ano, Mes, 1));
    if Tipo = dtTexto then
      //  FormatDateTime('mmmmm/yyyy', EncodeDate(Ano, Mes, 1));
      Result := dmkPF.VerificaMes(Mes, False)+'/'+FormatFloat('0', Ano);
  except
    Geral.MB_Aviso('Dados inválidos para geração de data:'+ sLineBreak +
    'Ano = ' + FormatFloat('0', Ano)+ sLineBreak +
    'Mês = ' + FormatFloat('0', Mes)+ sLineBreak +
    'Dia = 01');
    raise;
  end;
end;

function TUnDmkProcFunc.PrimeiroDiaDoPeriodo_Date(Periodo: Integer): TDateTime;
var
  Ano, Mes: Word;
begin
  Ano := (Periodo div 12 )+ 2000;
  Mes := Periodo mod 12;
  if Mes = 0 then
  begin
    Mes := 12;
    Ano := Ano -1;
  end;
  Result := EncodeDate(Ano, Mes, 1);
end;

function TUnDmkProcFunc.PWDExDecode(Data, SecurityString: string): string;
var
  i, x, x2: integer;
  s1, s2, ss: string;
begin
  Result := #1;
  if Length(SecurityString) < 16 then Exit;
  for i := 1 to Length(SecurityString) do
  begin
    s1 := Copy(SecurityString, i + 1,Length(securitystring));
    if Pos(SecurityString[i], s1) > 0 then Exit;
    if Pos(SecurityString[i], Codes64) <= 0 then Exit;
  end;
  s1 := Codes64;
  s2 := '';
  ss := securitystring;
  for i := 1 to Length(Data) do if Pos(Data[i], ss) > 0 then s2 := s2 + Data[i];
  Data := s2;
  s2   := '';
  if Length(Data) mod 2 <> 0 then Exit;
  for i := 0 to Length(Data) div 2 - 1 do
  begin
    x := Pos(Data[i * 2 + 1], ss) - 1;
    if x < 0 then Exit;
    ss := Copy(ss, Length(ss), 1) + Copy(ss, 1,Length(ss) - 1);
    x2 := Pos(Data[i * 2 + 2], ss) - 1;
    if x2 < 0 then Exit;
    x  := x + x2 * 16;
    s2 := s2 + chr(x);
    ss := Copy(ss, Length(ss), 1) + Copy(ss, 1,Length(ss) - 1);
  end;
  Result := s2;
end;

function TUnDmkProcFunc.PWDExEncode(Data, SecurityString: string; MinV,
  MaxV: Integer): string;
  function MakeRNDString(Chars: string; Count: Integer): string;
  var
    i, x: integer;
  begin
    Result := '';
    for i := 0 to Count - 1 do
    begin
      x := Length(chars) -  2;//*)Random(Length(chars));
      Result := Result + chars[x];
      chars := Copy(chars, 1,x - 1) + Copy(chars, x + 1,Length(chars));
    end;
  end;
var
  i, x: integer;
  s1, s2, ss, s0, sx: string;
  k: Integer;
begin
  Result := '';
  if Length(SecurityString) < 16 then Exit;
  for i := 1 to Length(SecurityString) do
  begin
    s1 := Copy(SecurityString, i + 1,Length(securitystring));
    s0 := SecurityString[i];
    if Pos(s0, s1) > 0 then Exit;
    if Pos(s0, Codes64) <= 0 then Exit;
  end;
  s1 := Codes64;
  s2 := '';
  for i := 1 to Length(SecurityString) do
  begin
    x := Pos(SecurityString[i], s1);
    sx := Copy(s1, x, 1);
    if x > 0 then s1 := Copy(s1, 1, x - 1) + Copy(s1, x + 1, Length(s1));
  end;
  ss := securitystring;
  for i := 1 to Length(Data) do
  begin
    s2 := s2 + ss[Ord(Data[i]) mod 16 + 1];
    ss := Copy(ss, Length(ss), 1) + Copy(ss, 1,Length(ss) - 1);
    k  := Ord(Data[i]) div 16;
    s2 := s2 + ss[k + 1];
    ss := Copy(ss, Length(ss), 1) + Copy(ss, 1,Length(ss) - 1);
  end;
  Result := MakeRNDString(s1, 2);//*)Random(MaxV - MinV) + minV + 1);
  for i := 1 to Length(s2) do Result := Result + s2[i] + MakeRNDString(s1,
  2);//*)Random(MaxV - MinV) + minV);
end;

function TUnDmkProcFunc.PWDGenerateSecutityString(): string;
var
  i, x: integer;
  s1, s2: string;
begin
  s1 := Codes64;
  s2 := '';
  for i := 0 to 15 do
  begin
    x  := Random(Length(s1));
    x  := Length(s1) - x;
    s2 := s2 + s1[x];
    s1 := Copy(s1, 1,x - 1) + Copy(s1, x + 1,Length(s1));
  end;
  Result := s2;
end;

function TUnDmkProcFunc.RemoveArquivosDeDiretorio(Diretorio: String;
  ApenasArquivos: Boolean = False): Boolean;
var
  Dir, Caminho: string;
  SearchRec: TSearchRec;
begin
  Dir := dmkPF.SimplificaBarras(Diretorio);
  Dir := IncludeTrailingPathDelimiter(Dir);
  //
  try
    Caminho := Dir + '*.*';
    //
    if FindFirst(Caminho, faAnyFile, SearchRec) = 0 then
    begin
      repeat
        if (SearchRec.Attr and faDirectory) <> 0 then
        begin
          if (SearchRec.Name <> '.') and (SearchRec.Name <> '..') then
            RemoveArquivosDeDiretorio(Dir + SearchRec.Name);
        end else
          System.SysUtils.DeleteFile(Dir + SearchRec.Name);
      until FindNext(SearchRec) <> 0;
      System.SysUtils.FindClose(SearchRec);
    end;
    if ApenasArquivos = True then
      Result := True
    else
      Result := RemoveDir(ExcludeTrailingPathDelimiter(Dir));
  except
    Result := False;
  end;
end;

function TUnDmkProcFunc.RemoveExtensaoDeArquivo(Arq: String): String;
var
  I, Pos: integer;
begin
  Result := Arq;
  Pos := -1;
  for I := Length(Arq) downto 1 do
  begin
    if Arq[I] = '.' then
    begin
      Pos := I;
      Break;
    end;
  end;
  if I > 0 then
    Result := Copy(Arq, 1, Pos -1);
end;

function TUnDmkProcFunc.RenameAppOlder(AppName: String): Boolean;
var
  App, Older: String;
  poi: Integer;
begin
  App := ExtractFileName(AppName);
  poi := Pos('.', App);
  if poi > 0 then App := Copy(App, 1, poi-1);
  Older := ExtractFilePath(AppName) + App +
    IntToStr(Geral.VersaoInt2006(Geral.FileVerInfo(AppName, 3)))  +
    ExtractFileExt(AppName);
  if FileExists(Older) then
  begin
    Geral.MB_Erro('Impossível renomear '+
    AppName+' para '+Older+', pois este já existe!');
    Result := False;
  end else
  begin
    RenameFile(AppName, Older);
    Result := True;
  end;
end;

procedure TUnDmkProcFunc.RenameAppOlder_Cancel();
var
  Older: String;
begin
  Older := ExtractFilePath(Application.ExeName) + '\Old\' + Application.Title +
    IntToStr(Geral.VersaoInt2006(Geral.FileVerInfo(Application.ExeName, 3)))+'.exe';
  RenameFile(Older, Application.ExeName);
end;

function TUnDmkProcFunc.RenomearArquivo(Arquivo, NomeNovo: String): Boolean;
var
  ArqDir, ArqExt: String;
begin
  ////////////////////////////////////////////////////////
  // Arquivo  => Caminho do arquivo + nome com extensão //
  // NomeNovo => Apenas o nome sem extensão             //
  ////////////////////////////////////////////////////////
  //
  Result := False;
  //
  if FileExists(Arquivo) then
  begin
    ArqDir := ExtractFileDir(Arquivo);
    ArqExt := ExtractFileExt(Arquivo);
    //
    Result := RenameFile(Arquivo, ArqDir + '\' + NomeNovo + ArqExt);
  end;
end;

procedure TUnDmkProcFunc.RetrieveLocalAdapterInformation(strings: TStrings);
var
  pAdapterInfo, pTempAdapterInfo: PIP_ADAPTER_INFO;
  BufLen: DWORD;
  Status: DWORD;
  strMAC: String;
  i: Integer;
begin
  strings.Clear;

  BufLen := SizeOf(pAdapterInfo);
  GetMem(pAdapterInfo, BufLen);
  try
    repeat
      Status := GetAdaptersInfo(pAdapterInfo, BufLen);
      if (Status = ERROR_SUCCESS) then
      begin
        if BufLen <> 0 then Break;
        Status := ERROR_NO_DATA;
      end;
      if (Status = ERROR_BUFFER_OVERFLOW) then
      begin
        ReallocMem(pAdapterInfo, BufLen);
      end else
      begin
        case Status of
          ERROR_NOT_SUPPORTED:
            strings.Add('GetAdaptersInfo is not supported by the operating system running on the local computer.');
          ERROR_NO_DATA:
            strings.Add('No network adapter on the local computer.');
        else
            strings.Add('GetAdaptersInfo failed with error #' + IntToStr(Status));
        end;
        Exit;
      end;
    until False;

    pTempAdapterInfo := pAdapterInfo;
    while (pTempAdapterInfo <> nil) do
    begin
      strings.Add('Description: ' + pTempAdapterInfo^.Description);
      strings.Add('Name: ' + pTempAdapterInfo^.AdapterName);

      strMAC := '';
      for I := 0 to pTempAdapterInfo^.AddressLength - 1 do
        strMAC := strMAC + '-' + IntToHex(pTempAdapterInfo^.Address[I], 2);

      Delete(strMAC, 1, 1);
      strings.Add('MAC address: ' + strMAC);
      strings.Add('IP address: ' + pTempAdapterInfo^.IpAddressList.IpAddress.S);
      strings.Add('IP subnet mask: ' + pTempAdapterInfo^.IpAddressList.IpMask.S);
      strings.Add('Gateway: ' + pTempAdapterInfo^.GatewayList.IpAddress.S);
      strings.Add('DHCP enabled: ' + IntToStr(pTempAdapterInfo^.DhcpEnabled));
      strings.Add('DHCP: ' + pTempAdapterInfo^.DhcpServer.IpAddress.S);
      strings.Add('Have WINS: ' + BoolToStr(pTempAdapterInfo^.HaveWins,True));
      strings.Add('Primary WINS: ' + pTempAdapterInfo^.PrimaryWinsServer.IpAddress.S);
      strings.Add('Secondary WINS: ' + pTempAdapterInfo^.SecondaryWinsServer.IpAddress.S);

      pTempAdapterInfo := pTempAdapterInfo^.Next;
    end;
  finally
    FreeMem(pAdapterInfo);
  end;
end;

function TUnDmkProcFunc.RunningProcessesList(const List: TStrings;
  FullPath: Boolean): Boolean;

  function ProcessFileName(PID: DWORD): AnsiString;
  var
    Handle: THandle;
  begin
    Result := '';
    Handle := OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ, False, PID);
    if Handle <> 0 then
      try
        SetLength(Result, MAX_PATH);
        if FullPath then
        begin
          if GetModuleFileNameEx(Handle, 0, PChar(String(Result)), MAX_PATH) > 0 then
            SetLength(Result, StrLen(PAnsiChar(Result)))
          else
            Result := '';
        end
        else
        begin
          if GetModuleBaseNameA(Handle, 0, PAnsiChar(Result), MAX_PATH) > 0 then
            SetLength(Result, StrLen(PAnsiChar(Result)))
          else
            Result := '';
        end;
      finally
        CloseHandle(Handle);
      end;
  end;

  function BuildListTH: Boolean;
  var
    SnapProcHandle: THandle;
    ProcEntry: TProcessEntry32;
    NextProc: Boolean;
    FileName: Ansistring;
  begin
    SnapProcHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    Result := (SnapProcHandle <> INVALID_HANDLE_VALUE);
    if Result then
      try
        ProcEntry.dwSize := SizeOf(ProcEntry);
        NextProc := Process32First(SnapProcHandle, ProcEntry);
        while NextProc do
        begin
          if ProcEntry.th32ProcessID = 0 then
          begin
            // PID 0 is always the "System Idle Process" but this name cannot be
            // retrieved from the system and has to be fabricated.
            FileName := RsSystemIdleProcess;
          end
          else
          begin
            if IsWin2k or IsWinXP then
            begin
              FileName := ProcessFileName(ProcEntry.th32ProcessID);
              if FileName = '' then
                FileName := AnsiString(ProcEntry.szExeFile);
            end
            else
            begin
              FileName := AnsiString(ProcEntry.szExeFile);
              if not FullPath then
                FileName := AnsiString(ExtractFileName(String(FileName)));
            end;
          end;
          List.AddObject(String(FileName), Pointer(ProcEntry.th32ProcessID));
          NextProc := Process32Next(SnapProcHandle, ProcEntry);
        end;
      finally
        CloseHandle(SnapProcHandle);
      end;
  end;

  function BuildListPS: Boolean;
  var
    PIDs: array [0..1024] of DWORD;
    Needed: DWORD;
    I: Integer;
    FileName: string;
  begin
    Result := EnumProcesses(@PIDs, SizeOf(PIDs), Needed);
    if Result then
    begin
      for I := 0 to (Needed div SizeOf(DWORD)) - 1 do
      begin
        case PIDs[I] of
          0:
            // PID 0 is always the "System Idle Process" but this name cannot be
            // retrieved from the system and has to be fabricated.
            FileName := RsSystemIdleProcess;
          2:
            // On NT 4 PID 2 is the "System Process" but this name cannot be
            // retrieved from the system and has to be fabricated.
            if IsWinNT4 then
              FileName := RsSystemProcess
            else
              FileName := String(ProcessFileName(PIDs[I]));
            8:
            // On Win2K PID 8 is the "System Process" but this name cannot be
            // retrieved from the system and has to be fabricated.
            if IsWin2k or IsWinXP then
              FileName := RsSystemProcess
            else
              FileName := String(ProcessFileName(PIDs[I]));
            else
              FileName := String(ProcessFileName(PIDs[I]));
        end;
        if FileName <> '' then
          List.AddObject(FileName, Pointer(PIDs[I]));
      end;
    end;
  end;
begin
  if IsWin3X or IsWinNT4 then
    Result := BuildListPS
  else
    Result := BuildListTH;
end;

procedure TUnDmkProcFunc.SalvaInfoRegEdit(Key, NomeForm: String; Value: Variant;
  KeyType: TKeyType);
var
  Cam: String;
begin
  Cam := Application.Title+'\InitialConfig\'+NomeForm;
  // ini 2022-03-01
  //Geral.WriteAppKeyLM(Key, Cam, Value, KeyType);
  Geral.WriteAppKeyCU(Key, Cam, Value, KeyType);
  // fim 2022-03-01
end;

procedure TUnDmkProcFunc.SaveTextToFile(const FileName: TFileName;
  const Content: string);
var
  Outfile: TStreamWriter;
begin
  Outfile := TStreamWriter.Create(FileName, False, TEncoding.ANSI);
  try
    Outfile.Write(Content);
  finally
    Outfile.Free;
  end;
end;

function TUnDmkProcFunc.ScanFile(const FileName, forString: string;
  CaseSensitive: Boolean): Longint;
  { returns position of string in file or -1, if not found }
const
  BufferSize = $8001;  { 32K+1 bytes }
var
  //pBuf, pEnd, pScan, pPos: PChar;
  pBuf, pEnd, pScan, pPos: PAnsiChar;
  filesize: LongInt;
  bytesRemaining: LongInt;
  bytesToRead: Integer;//Word;
  F: file;
  //SearchFor: PChar;
{$IFDEF DELPHI12_UP}
  SearchFor: PAnsiChar;
{$ELSE}
  SearchFor: PChar;
{$ENDIF}
  oldMode: Word;
begin
  Result := -1;  { assume failure }
  if (Length(forString) = 0) or (Length(FileName) = 0) then Exit;
  SearchFor := nil;
  pBuf      := nil;

  { open file as binary, 1 byte recordsize }
  AssignFile(F, FileName);
  oldMode  := FileMode;
  FileMode := 0;    { read-only access }
  Reset(F, 1);
  FileMode := oldMode;
  try { allocate memory for buffer and pchar search string }
{$IFDEF DELPHI12_UP}
    SearchFor := AnsiStrAlloc(Length(forString) + 1);
{$ELSE}
    SearchFor := StrAlloc(Length(forString) + 1);
{$ENDIF}
    StrPCopy(SearchFor, forString);
    if not caseSensitive then  { convert to upper case }
      UpperCase(String(SearchFor));
    GetMem(pBuf, BufferSize);
    filesize       := System.Filesize(F);
    bytesRemaining := filesize;
    pPos           := nil;
    while bytesRemaining > 0 do
    begin
      { calc how many bytes to read this round }
      if bytesRemaining >= BufferSize then
        bytesToRead := Pred(BufferSize)
      else
        bytesToRead := bytesRemaining;

      { read a buffer full and zero-terminate the buffer }
      //BlockRead(F, pBuf^, bytesToRead);
      BlockRead(F, pBuf^, bytesToRead, bytesToRead);
      pEnd  := @pBuf[bytesToRead];
      pEnd^ := #0;
       { scan the buffer. Problem: buffer may contain #0 chars! So we
         treat it as a concatenation of zero-terminated strings. }
      pScan := pBuf;
      while pScan < pEnd do
      begin
        if not caseSensitive then { convert to upper case }
          UpperCase(String(pScan));
        pPos := StrPos(pScan, SearchFor);  { search for substring }
        if pPos <> nil then
        begin { Found it! }
          Result := FileSize - bytesRemaining +
            Longint(pPos) - Longint(pBuf);
          Break;
        end;
        pScan := StrEnd(pScan);
        Inc(pScan);
      end;
      if pPos <> nil then Break;
      bytesRemaining := bytesRemaining - bytesToRead;
      if bytesRemaining > 0 then
      begin
       { no luck in this buffers load. We need to handle the case of
         the search string spanning two chunks of file now. We simply
         go back a bit in the file and read from there, thus inspecting
         some characters twice
       }
        Seek(F, FilePos(F) - Length(forString));
        bytesRemaining := bytesRemaining + Length(forString);
      end;
    end; { While }
  finally
    CloseFile(F);
    if SearchFor <> nil then StrDispose(SearchFor);
    if pBuf <> nil then FreeMem(pBuf, BufferSize);
  end;
end;

function TUnDmkProcFunc.ScheduleRunAtStartup(const ATaskName, AFileName,
  AUserAccount, AUserPassword: string): Integer;
  // https://docs.microsoft.com/pt-br/windows-server/administration/windows-commands/schtasks#BKMK_syntax
  // schtasks /create /sc <ScheduleType> /tn <TaskName> /tr <TaskRun> [/s <Computer> [/u [<Domain>\]<User> [/p <Password>]]] [/ru {[<Domain>\]<User> | System}] [/rp <Password>] [/mo <Modifier>] [/d <Day>[,<Day>...] | *] [/m <Month>[,<Month>...]] [/i <IdleTime>] [/st <StartTime>] [/ri <Interval>] [{/et <EndTime> | /du <Duration>} [/k]] [/sd <StartDate>] [/ed <EndDate>] [/it] [/z] [/f]
var
  NovoComando: String;
begin
  Result := 0;
  ShellExecute(0, nil, 'schtasks', PChar('/delete /f /tn "' + ATaskName + '"'),
    nil, SW_HIDE);
  (*
  ShellExecute(0, nil, 'schtasks', PChar('/create /tn "' + ATaskName + '" ' +
  '/tr "' + AFileName + '" /sc ONSTART /ru "' + AUserAccount + '"'), nil, SW_HIDE);
  *)
  //NovoComando := '/create /tn "' + ATaskName + '" ' +  '/tr "' + AFileName + '" /sc ONSTART';
  NovoComando := '/create /tn "' + ATaskName + '" ' +  '/tr "' + AFileName + '" /sc ONLOGON';
  if AUserAccount <> '' then
    NovoComando := NovoComando + ' /ru "' + AUserAccount + '"';
  if AUserPassword <> '' then
    NovoComando := NovoComando + ' /rp "' + AUserPassword + '"';
  Result := ShellExecute(0, nil, 'schtasks', PWideChar(NovoComando), nil, SW_HIDE);
end;

function TUnDmkProcFunc.SeparaCoordenadas(const Coord: String; var Latitude,
  Longitude: Double): Boolean;
  procedure Avisa();
  begin
    Geral.MB_Erro('Formatação de coordenadas não implementado!' + sLineBreak +
    'Solicite à Dermatek informando o texto abaixo:' + sLineBreak + Coord)
  end;
var
  I, A, B, nVirg, nPont: Integer;
  Txt, X, Y: String;
begin
  Result := False;
  Latitude  := 0;
  Longitude := 0;
  //
  Txt := Trim(Coord);
  nVirg := 0;
  nPont := 0;
  for I := 0 to Length(Txt) - 1 do
  begin
    if (Coord[I] = ',') and
    (Coord[I + 1] = ' ') then
    begin
      nVirg := nVirg + 1;
      A := I;
    end else
    if (Coord[I] = '.') and
    (Coord[I + 1] = ' ') then
    begin
      nPont := nPont + 1;
      B := I;
    end;
  end;
  //
  if (nVirg = 1) and (nPont = 0) then
  begin
    X := Copy(Txt, 1, A - 1);
    Y := Copy(Txt, A + 2);
    //
    Latitude  := Geral.DMV_Dot(X);
    Longitude := Geral.DMV_Dot(Y);
    Result := True;
  end else
    Avisa;
end;

function TUnDmkProcFunc.SeparaDadosCampoDeXML(Texto, Chave: String;
  MantemChave: Boolean): String;
var
  PosIni, PosFim : Integer;
begin
  if MantemChave then
   begin
     PosIni := Pos(Chave,Texto)-1;
     PosFim := Pos('/'+Chave,Texto)+length(Chave)+3;

     if (PosIni = 0) or (PosFim = 0) then
      begin
        PosIni := Pos('ns2:'+Chave,Texto)-1;
        PosFim := Pos('/ns2:'+Chave,Texto)+length(Chave)+3;
      end;
   end
  else
   begin
     PosIni := Pos(Chave,Texto)+Pos('>',copy(Texto,Pos(Chave,Texto),length(Texto)));
     PosFim := Pos('/'+Chave,Texto);

     if (PosIni = 0) or (PosFim = 0) then
      begin
        PosIni := Pos('ns2:'+Chave,Texto)+Pos('>',copy(Texto,Pos('ns2:'+Chave,Texto),length(Texto)));
        PosFim := Pos('/ns2:'+Chave,Texto);
      end;
   end;
  Result := copy(Texto,PosIni,PosFim-(PosIni+1));
  //if Result = '' then
    //Geral.MB_Erro(Texto);
end;

function TUnDmkProcFunc.SeparaNumeroDaDireita_TT_Dot2(const Texto: String;
  var Num: Double; var Txt: String): Boolean;
var
  I: Integer;
  N: String;
  Ponto: Boolean;
begin
  Result := False;
  Num    := 0;
  Txt    := '';
  N      := '';
  Ponto  := False;
  //
  for I := 1 to Length(Texto) do
  begin
    if Texto[I] = '.' then
    begin
      if Ponto = False then
      begin
        Ponto := True;
        N := N + Texto[I];
      end else
        Break;
    end else
    if CharInSet(Texto[i], (['0'..'9','-','.'])) then
      N := N + Texto[I]
    else Break;
  end;
  if N <> '' then
  begin
    I := pos('.', N);
    if I > 0 then
      N[I] := ',';
    Num := StrToFloat(N);
  end;
  Txt := Copy(Texto, Length(N) + 1);
end;

function TUnDmkProcFunc.SetArrayOfInt(var Arr: TArrSelIdxInt1;
  const Inteiros: array of Integer; const Zera: Boolean): Integer;
var
  N, I, J: Integer;
begin
  N := Length(Inteiros);
  if Zera then
    SetLength(Arr, 0);
  //
  if N > 0 then
  begin
    J := Length(Arr);
    SetLength(Arr, N + J);
    for I := 0 to N - 1 do
    begin
      Arr[J + I] := Inteiros[I];
    end;
  end;
end;

function TUnDmkProcFunc.SistemaOperacional(): Integer;
begin
       if IsWinXP  then Result := 3
  else if IsWin2k  then Result := 2
  else if IsWinNT4 then Result := 1
  else if IsWin3X  then Result := 0
  else Result := -1;
end;

procedure TUnDmkProcFunc.SistemaOperacional2_txt(var OSVersao, OSArquitetura: String);
var
  SysInfo: TSystemInfo;
  VerInfo: TOSVersionInfoEx;
  MajorNumero, MinorNumero, BuildNum: DWORD;
  OSName, Arquitetura: String;

  function Check(AMajor, AMinor: Integer): Boolean;
  begin
    Result := (MajorNumero > AMajor) or ((MajorNumero = AMajor) and (MinorNumero >= AMinor));
  end;

  function ObtemArquitetura(): String;
  begin
    if TOSVersion.Architecture = arIntelX86 then
      Result := SVersion32
    else if TOSVersion.Architecture = arIntelX64 then
      Result := SVersion64
    else
      Result := '';
  end;

const
{$IF Defined(NEXTGEN) and Declared(System.Embedded)}
  kernelbase = 'kernelbase.dll';
{$Else}
  kernelbase = 'kernel32.dll';
{$EndIf}
  SWindowsServer2012 = 'Windows Server 2012';
  SWindows8 = 'Windows 8';
  SWindows8Point1 = 'Windows 8.1';
  SWindows10 = 'Windows 10';
begin
  OSName      := '';
  Arquitetura := '';

  ZeroMemory(@VerInfo, SizeOf(VerInfo));
  VerInfo.dwOSVersionInfoSize := SizeOf(VerInfo);
  GetVersionEx(VerInfo);

  MajorNumero := VerInfo.dwMajorVersion;
  MinorNumero := VerInfo.dwMinorVersion;
  BuildNum    := VerInfo.dwBuildNumber;

  ZeroMemory(@SysInfo, SizeOf(SysInfo));
  if Check(5, 1) then // GetNativeSystemInfo not supported on Windows 2000
    GetNativeSystemInfo(SysInfo);

  Arquitetura := ObtemArquitetura;

  if (MajorNumero > 6) or ((MajorNumero = 6) and  (MinorNumero > 1)) then
  begin
    if not GetNetWkstaMajorMinor(MajorNumero, MinorNumero) then
      GetProductVersion(kernelbase, MajorNumero, MinorNumero, BuildNum);
  end;

  OSName := SWindows;
  case MajorNumero of
    10: case MinorNumero of
          0: OSName := SWindows10;
        end;
    6:  case MinorNumero of
          0: if VerInfo.wProductType = VER_NT_WORKSTATION then
               OSName := SWindowsVista
             else
               OSName := SWindowsServer2008;
          1: if VerInfo.wProductType = VER_NT_WORKSTATION then
               OSName := SWindows7
             else
               OSName := SWindowsServer2008R2;
          2: if VerInfo.wProductType = VER_NT_WORKSTATION then
               OSName := SWindows8
             else
               OSName := SWindowsServer2012;
          3: OSName := SWindows8Point1;
        end;
    5:  case MinorNumero of
          0: OSName := SWindows2000;
          1: OSName := SWindowsXP;
          2:
            begin
              if (VerInfo.wProductType = VER_NT_WORKSTATION) and
                 (SysInfo.wProcessorArchitecture = PROCESSOR_ARCHITECTURE_AMD64) then
                OSName := SWindowsXP
              else
              begin
                if GetSystemMetrics(SM_SERVERR2) = 0 then
                  OSName := SWindowsServer2003
                else
                  OSName := SWindowsServer2003R2
              end;
            end;
        end;
  end;
  OSArquitetura := Arquitetura;
  OSVersao      := OSName;
end;

function TUnDmkProcFunc.SistemaOperacional_txt: String;
begin
       if IsWin10     then Result := 'Windows 10'
  else if IsWin81     then Result := 'Windows 8.1'
  else if IsWin8      then Result := 'Windows 8'
  else if IsWin7      then Result := 'Windows 7'
  else if IsWinVista  then Result := 'Windows Vista'
  else if IsWinXP     then Result := 'Windows XP'
  else if IsWin2k     then Result := 'Windows 2000'
  else if IsWinNT4    then Result := 'Windows NT 4'
  else if IsWin3X     then Result := 'Windows 95/98'
  else Result := 'DESCONHECIDO';
end;

function TUnDmkProcFunc.ObtemSysInfo(): String;
var
  Info: String;
begin
  Info := SistemaOperacional_txt;
  if TOSInfo.IsWOW64 = True then
    Info := Info + ' 64 bits '
  else
    Info := Info + ' 32 bits ';
  Result := Info;
end;

function TUnDmkProcFunc.ObtemUsuarioCorrente_SistemaOperacional(): String;
var
  BufSize: DWord;
  Buffer: PChar;
begin
  //Só funciona no Delphi XE10
  //Nos outros delphi ele busca o usuário principal do Windows
 Result  := '';
 BufSize := 1024;
 Buffer  := StrAlloc(BufSize);
 try
  if GetUserName(Buffer, BufSize) then
  begin
    SetString(Result, Buffer, BufSize-1);
  end else
  begin
    Result := '';
    RaiseLastOSError;
  end;
 finally
   StrDispose(Buffer);
 end;
end;

function TUnDmkProcFunc.ObtemSysInfo2(Separador: String = ''): String;
var
  OSVer, OSArq, Info: String;
begin
  SistemaOperacional2_txt(OSVer, OSArq);
  //
  if OSVer <> '' then
    Info := OSVer + ' ' + Separador + ' ' + OSArq
  else
    Info := '';
  //
  Result := Info;
end;

function TUnDmkProcFunc.SoNumeroEExtra_TT(Texto: String; Extra: Char;
  Tudo: Boolean): String;
var
  i:Integer;
  Ini: Boolean;
begin
  Result := CO_VAZIO;
  Ini := False;
  if Length(Texto) > 0 then
  begin
    for i := 1 to Length(Texto) do
    begin
      //if Texto[i] in (['0'..'9', Extra]) then
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], (['0'..'9', Extra])) then
{$ELSE}
      if Texto[i] in (['0'..'9', Extra]) then
{$ENDIF}
      begin
        Ini := True;
        Result := Result + Texto[i];
      end else if (Ini and not Tudo) then Break;
    end;
  end;
end;

function TUnDmkProcFunc.SoNumeroELetraESeparadores_TT(Texto: String): String;
var
  i:Integer;
begin
  Result := CO_VAZIO;
  for i := 1 to Length(Texto) do
  begin
    {
    if Texto[i] in (['0'..'9']) then Result := Result + Texto[i] else
    if Texto[i] in (['A'..'Z']) then Result := Result + Texto[i] else
    if Texto[i] in (['a'..'z']) then Result := Result + Texto[i] else
    if Texto[i] in (['-', '.', '/', '\']) then Result := Result + Texto[i];
    }
{$IFDEF DELPHI12_UP}
    if CharInSet(Texto[i], (['0'..'9'])) then Result := Result + Texto[i] else
    if CharInSet(Texto[i], (['A'..'Z'])) then Result := Result + Texto[i] else
    if CharInSet(Texto[i], (['a'..'z'])) then Result := Result + Texto[i] else
    if CharInSet(Texto[i], (['-', '.', '/', '\'])) then Result := Result + Texto[i];
{$ELSE}
    if Texto[i] in (['0'..'9']) then Result := Result + Texto[i] else
    if Texto[i] in (['A'..'Z']) then Result := Result + Texto[i] else
    if Texto[i] in (['a'..'z']) then Result := Result + Texto[i] else
    if Texto[i] in (['-', '.', '/', '\']) then Result := Result + Texto[i];
{$ENDIF}
  end;
end;

function TUnDmkProcFunc.SoNumeroValor_TT(Texto: String): String;
var
  i:Integer;
begin
  Result := CO_VAZIO;
  if Length(Texto) > 0 then
  begin
    {if Texto[1] = '+' then Result := '+';
    if Texto[1] = '-' then Result := '-';
    if Texto[Length(Texto)] = '+' then Result := '+';
    if Texto[Length(Texto)] = '-' then Result := '-';}
    for i := 1 to Length(Texto) do
      //if Texto[i] in (['0'..'9', ',', '+', '-', '.']) then Result := Result + Texto[i];
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], (['0'..'9', ',', '+', '-', '.'])) then Result := Result + Texto[i];
{$ELSE}
      if Texto[i] in (['0'..'9', ',', '+', '-', '.']) then Result := Result + Texto[i];
{$ENDIF}
  end;
end;

procedure TUnDmkProcFunc.SortIntArray(var IntArray: array of integer);
  function _SortInts(List: TStringList; Index1, Index2: Integer): Integer;
  begin
    Result := StrToInt(List[Index1]) - StrToInt(List[Index2]);
  end;
var
  Strs : TStringList;
  i : integer;
begin
  if High(IntArray) > -1 then
  begin
    Strs := TStringList.Create;
    try
      for i := 0 to High(IntArray) do
        Strs.Add(IntToStr(IntArray[i]));
      //
      Strs.CustomSort( @_SortInts );
      //
      for i := 0 to Strs.Count -1 do
        IntArray[i] := StrToInt(Strs[i]);
    finally
      Strs.Free;
    end;
  end;
end;

function TUnDmkProcFunc.SoTextoLayout(Texto: String): String;
var
  i, o:Integer;
begin
  Result := CO_VAZIO;
  for i := 1 to Length(Texto) do
  begin
    o := Ord(Texto[i]);
    if (o > 31) and (o < 256) then
      Result := Result + Texto[i];
  end;
end;

{
function TUnDmkProcFunc.SQL_PerioCorrApo(WhereOrAnd: TWhereOrAnd; FldDtNormal,
  FldDtCorrApo: String; Ini, Fim: TDateTime): String;
const
  Fmt = 'YYYY-MM-DD'; // Obriga ao MySQL fazer o campo como data e não texto
var
  DtaI, DtaF: String;
begin
  if Int(Ini) = 0 then
    DtaI := '0000-00-00'
  else
    DtaI := FormatDateTime(Fmt, Ini);
  //
  if Int(Fim) = 0 then
    DtaF := '9999-12-31'
  else
    DtaF := FormatDateTime(Fmt, Fim);

//  Result := Texto + ' BETWEEN "' + DtaI + '" AND "' + DtaF + ' 23:59:59"';
//WHERE (
  Result := ' ' + sWhereOrAnd[Integer(WhereOrAnd)] + ' (' + sLineBreak +
//  (
  '  (' + slinebreak +
//    DataHora BETWEEN "2017-01-01" AND "2017-01-31 23:59:59"
  '    ' + FldDtNormal + ' BETWEEN "' + DtaI + '" AND "' + DtaF + ' 23:59:59" ' + sLineBreak +
//    AND DtCorrApo <= "2017-02-01"
  '    AND ' + FldDtCorrApo + ' <= "' + DtaF + ' 23:59:59" ' + sLineBreak +
//  )
  '  ) ' + sLineBreak +
//  OR
  '  OR ' + sLineBreak +
//  (
  '  (' + slinebreak +
//    DtCorrApo BETWEEN "2017-01-01" AND "2017-01-31 23:59:59"
  '    ' + FldDtCorrApo + ' BETWEEN "' + DtaI + '" AND "' + DtaF + ' 23:59:59" ' + sLineBreak +
//    AND DataHora < "2017-01-01"
  '    AND ' + FldDtNormal + ' < "' + DtaI + '" ' + sLineBreak +
//  )
  '  ) ' + sLineBreak +
//)
  ') ';
end;
}

function TUnDmkProcFunc.SQL_Periodo(Texto: String; Ini, Fim: TDateTime; CkIni,
  CkFim: Boolean): String;
// 2013-08-11
// No XE2 com MySQL 5.5 tem problema quando data= 1899-12-30 deve ser 0000-00-00
(*
const
  Fmt = 'YYYY-MM-DD'; // Obriga ao MySQL fazer o campo como data e não texto
begin
  if CkIni and CkFim then
    Result := Texto + ' BETWEEN "'+FormatDateTime(Fmt, Ini)+
    '" AND "'+FormatDateTime(Fmt, Fim)+' 23:59:59"'
  else if CkIni then
    Result := Texto + ' >= "'+FormatDateTime(Fmt, Int(Ini))+'"'
  else if CkFim then
    Result := Texto + ' < "'+FormatDateTime(Fmt, Int(Fim) + 1)+'"'
  else begin
    if Uppercase(Copy(Trim(Texto), 1, 5)) = 'WHERE' then
      Result := Texto + ' > -1000'
    else
      Result := '';
  end;
*)
const
  Fmt = 'YYYY-MM-DD'; // Obriga ao MySQL fazer o campo como data e não texto
var
  DtaI, DtaF: String;
begin
  if CkIni and CkFim then
  begin
    if (Int(Ini) = 0) and (Int(Fim) = 0) then
      Result := Texto + ' = "0000-00-00"'
    else begin
      if Int(Ini) = 0 then
        DtaI := '0000-00-00'
      else
        DtaI := FormatDateTime(Fmt, Ini);
      //
      DtaF := FormatDateTime(Fmt, Fim);
      Result := Texto + ' BETWEEN "' + DtaI + '" AND "' + DtaF + ' 23:59:59"';
    end;
  end else
  if CkIni then
  begin
    if Int(Ini) = 0 then
      DtaI := '0000-00-00'
    else
      DtaI := FormatDateTime(Fmt, Int(Ini));
    //
    Result := Texto + ' >= "' + DtaI + '"'
  end else
  if CkFim then
  begin
    if Int(Fim) = 0 then
      DtaF := '0000-00-00'
    else
      DtaF := FormatDateTime(Fmt, Int(Fim) + 1);
    //
    Result := Texto + ' < "' + DtaF + '"'
  end else
  begin
    if Uppercase(Copy(Trim(Texto), 1, 5)) = 'WHERE' then
      Result := Texto + ' > -999999999'
    else
      Result := '';
  end;
// FIM 2013-08-11
end;

function TUnDmkProcFunc.SQL_PeriodoComplexo(Texto, FldI, FldF: String; Ini,
  Fim: TDateTime; IniInn, EndInn, EndPos, IniOpn: Boolean): String;
const
  Fmt = 'YYYY-MM-DD'; // Obriga ao MySQL fazer o campo como data e não texto
var
  DtaI, DtaF: String;
begin
  Result := Texto + ' (' + sLineBreak;
  //
  if Int(Ini) = 0 then
    DtaI := '0000-00-00'
  else
    DtaI := FormatDateTime(Fmt, Ini);
  //
  DtaF := FormatDateTime(Fmt, Fim);
  //
  //WHERE (DtHrAberto BETWEEN "2000-01-01" AND "2999-01-01 23:59:59")
  if IniInn then
    Result := Result + '(' + FldI + ' BETWEEN "' + DtaI + '" AND "' + DtaF + ' 23:59:59")' + sLineBreak;
  //OR (DtHrFimOpe BETWEEN "2000-01-01" AND "2999-01-01 23:59:59")
  if EndInn then
    Result := Result + ' OR (' + FldF + ' BETWEEN "' + DtaI + '" AND "' + DtaF + ' 23:59:59")' + sLineBreak;
  //OR (DtHrAberto < "2000-01-01" AND DtHrFimOpe > "2999-01-01 23:59:59")
  if EndPos then
    Result := Result + ' OR (' + FldI + ' < "' + DtaI + '" AND ' + FldF + ' > "' + DtaF + ' 23:59:59")' + sLineBreak;
  //OR (DtHrAberto < "2000-01-01" AND DtHrFimOpe < "1900-01-01")
  if IniOpn then
    Result := Result + ' OR (' + FldI + ' < "' + DtaI + '" AND ' + FldF + ' < "1900-01-01")' + sLineBreak;
  //
  Result := Result + ')' + sLineBreak;
end;

function TUnDmkProcFunc.StrFmt_Double(Casas: Integer): String;
var
  StrFmt: String;
  i: Integer;
begin
  StrFmt := '#,###,###,##0';
  if Casas > 0 then
  begin
    StrFmt := StrFmt + '.';
    for i := 1 to casas do
      StrFmt := StrFmt + '0';
  end;
  Result := StrFmt;
end;

function TUnDmkProcFunc.Table_FormataCampo(Campo: TField;
  Valor: String): Variant;
var
  Val: String;
begin
  //Executar esta função no evento onSetText do campo a ser formatado
  case Campo.DataType of
    ftFloat:
    begin
      Val := Geral.SoNumeroEVirgula_TT(Valor);
      //
      Result := Geral.DMV(Val);
    end;
    ftDate:
    begin
      Val := Valor;
      //
      if Val = '' then
        Val := '0'
      else if Val = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DateSeparator then
        Val := '0';
      //
      Result := Geral.ValidaDataBr(Val, True, True);
    end;
    else
    begin
      Geral.MB_Aviso('Tipo de campo não implementado!');
      Result := Valor;
    end;
  end;
end;

function TUnDmkProcFunc.TamanhoDeArquivo(fileName: wideString): Int64;
var
  sr : TSearchRec;
begin
  if FindFirst(fileName, faAnyFile, sr ) = 0 then
    Result := Int64(sr.FindData.nFileSizeHigh) shl Int64(32) + Int64(sr.FindData.nFileSizeLow)
  else
    Result := -1;
  System.SysUtils.FindClose(sr) ;
end;

function TUnDmkProcFunc.TerminarProcesso(sFile: String): Boolean;
//Use assim:
// if TerminarProcesso('mirc.exe') then ShowMessage('Processo finalizado!');
// paulourio@gmail.com
var
  verSystem: TOSVersionInfo;
  hdlSnap,hdlProcess: THandle;
  bPath,bLoop: Bool;
  peEntry: TProcessEntry32;
  arrPid: Array [0..1023] of DWORD;
  iC: DWord;
  k,iCount: Integer;
  arrModul: Array [0..299] of Char;
  hdlModul: HMODULE;
begin
  Result := False;
  if ExtractFileName(sFile)=sFile then
    bPath:=false
  else
    bPath:=true;
  verSystem.dwOSVersionInfoSize:=SizeOf(TOSVersionInfo);
  GetVersionEx(verSystem);
  if verSystem.dwPlatformId=VER_PLATFORM_WIN32_WINDOWS then
  begin
    hdlSnap:=CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    peEntry.dwSize:=Sizeof(peEntry);
    bLoop:=Process32First(hdlSnap,peEntry);
    while integer(bLoop)<>0 do
    begin
      if bPath then
      begin
        if CompareText(peEntry.szExeFile,sFile) = 0 then
        begin
          TerminateProcess(OpenProcess(PROCESS_TERMINATE,false,peEntry.th32ProcessID), 0);
          Result := True;
        end;
      end
      else
      begin
        if CompareText(ExtractFileName(peEntry.szExeFile),sFile) = 0 then
        begin
          TerminateProcess(OpenProcess(PROCESS_TERMINATE,false,peEntry.th32ProcessID), 0);
          Result := True;
        end;
      end;
      bLoop := Process32Next(hdlSnap,peEntry);
    end;
    CloseHandle(hdlSnap);
  end
  else
    if verSystem.dwPlatformId=VER_PLATFORM_WIN32_NT then
    begin
      EnumProcesses(@arrPid,SizeOf(arrPid),iC);
      iCount := iC div SizeOf(DWORD);
      for k := 0 to Pred(iCount) do
      begin
        hdlProcess:=OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ,false,arrPid [k]);
        if (hdlProcess<>0) then
        begin
          EnumProcessModules(hdlProcess,@hdlModul,SizeOf(hdlModul),iC);
          GetModuleFilenameEx(hdlProcess,hdlModul,arrModul,SizeOf(arrModul));
          if bPath then
          begin
            if CompareText(arrModul,sFile) = 0 then
            begin
              TerminateProcess(OpenProcess(PROCESS_TERMINATE or PROCESS_QUERY_INFORMATION,False,arrPid [k]), 0);
              Result := True;
            end;
          end
          else
          begin
            if CompareText(ExtractFileName(arrModul),sFile) = 0 then
            begin
              TerminateProcess(OpenProcess(PROCESS_TERMINATE or PROCESS_QUERY_INFORMATION,False,arrPid [k]), 0);
              Result := True;
            end;
          end;
          CloseHandle(hdlProcess);
        end;
      end;
    end;
end;

function TUnDmkProcFunc.TestaPortaPorHost(Port: Word; Host: String): Boolean;
{$IfDef VER230} //XE2
var
  TcpClient: TTcpClient;
begin
  Result    := False;
  TcpClient := TTcpClient.Create(nil);
  try
    if TcpClient.Connected then
      TcpClient.Disconnect;
    //
    TcpClient.RemoteHost := Host;
    TcpClient.RemotePort := Geral.FF0(Port);
    //
    TcpClient.Connect;
    //
    if TcpClient.Connected then
    begin
      Result := True;
      TcpClient.Disconnect;
    end;
  finally
    TcpClient.Free;
  end;
end;
{$Else}
var
  TcpClient: TIdTCPClient;
begin
  Result    := False;
  TcpClient := TIdTCPClient.Create(nil);
  try
    if TcpClient.Connected then
      TcpClient.Disconnect;
    //
    TcpClient.Host := Host;
    TcpClient.Port := Port
    ;
    //
    TcpClient.Connect;
    //
    if TcpClient.Connected then
    begin
      Result := True;
      TcpClient.Disconnect;
    end;
  finally
    TcpClient.Free;
  end;
end;
{$EndIF}

function TUnDmkProcFunc.TestaPortaPorIP(Port: Word; IP: String; Memo: TMemo): Boolean;
var
  ret   : Integer;
  wsdata: WSAData;
begin
  Screen.Cursor := crHourGlass;
  Result        := False;
  //
  if Memo <> nil then
    Memo.lines.Add('Iniciando consulta...');
  //
  ret := WSAStartup($0002, wsdata);//initiates use of the Winsock
  if ret <> 0 then
    Exit;
  try
    if Memo <> nil then
    begin
      Memo.lines.Add('Descrição : '+wsData.szDescription);
      Memo.lines.Add('Status    : '+wsData.szSystemStatus);
    end;

    if PortTCPIsOpen(Port, AnsiString(IP)) then
    begin
      if Memo <> nil then
        Memo.lines.Add('Porta Aberta!');
      //
      Result := True;
    end else
    begin
      if Memo <> nil then
        Memo.lines.Add('Porta fechada!');
    end;
  finally
    Screen.Cursor := crDefault;
    WSACleanup; //terminates use of the Winsock
    //
    if Memo <> nil then
      Memo.lines.Add('Consulta finalizada com sucesso!');
  end;
end;

function TUnDmkProcFunc.TextoDeLista(Lista: array of String;
  Indice: Integer): String;
//var
  //MOT_INV: Integer;
begin
  //MOT_INV := EdMOT_INV.ValueVariant;
  if (Indice < 0) or (Indice >= Length(Lista)) then
    Result := ''
  else
    Result := Lista[Indice];
end;

function TUnDmkProcFunc.TextoToHTMLUnicode(Str: string): string;
var
  i, l : Integer;
begin
  l := Length(Str);
  Result := '';
  for i := 1 to l do
  begin
    case Str[i] of
    (*
    '<' : Result := Result + '&lt;';    { Do not localize }
    '>' : Result := Result + '&gt;';    { Do not localize }
    '"' : Result := Result + '&quot;';  { Do not localize }
    '&' : Result := Result + '&amp;';   { Do not localize }
    *)
{$IFNDEF UNICODE}
    #92, Char(160) .. #255 : Result := Result + '&#' + IntToStr(Ord(Str[ i ])) +';';  { Do not localize }
{$ELSE}
    // NOTE: Not very efficient
    #$0080..#$FFFF : Result := Result + '&#' + IntToStr(Ord(Str[ i ])) +';'; { Do not localize }
{$ENDIF}
    else
      Result := Result + Str[i];
    end;
  end;
end;

function TUnDmkProcFunc.THT(Hora: String): String;
var
  Soma, h, n: Integer;
begin
  result := '00:00';
  if length(Hora) = 8 then Hora := Copy(Hora, 1, 5);
  if (length(Hora) <> 4) and (length(Hora) <> 5) then exit;
  if (length(Hora) = 5) then
  begin
    if (Hora[3] <> ':') and (Hora[3] <> ';') and (Hora[3] <> '-')
    and (Hora[3] <> '_') and (Hora[3] <> ',') and (Hora[3] <> '.') then
    begin
      Geral.MB_Aviso('Hora inválida.');
      Exit;
    end;
    Soma := 1;
  end else Soma := 0;
  h := Geral.IMV(Hora[1]+Hora[2]);
  if (h < 0) or (h > 23) then
  begin
    Geral.MB_Aviso('Hora inválida.');
    Exit;
  end;
  n := Geral.IMV(Hora[(3+Soma)]+Hora[(4+Soma)]);
  if (n < 0) or (n > 59) then
  begin
    Geral.MB_Aviso('Hora inválida.');
    Exit;
  end;
  result := FormatDateTime(String(VAR_FORMATTIME2),
  StrToDateTime(IntToStr(h)+':'+IntToStr(n)));
end;

function TUnDmkProcFunc.DateIsNull(Date: TDateTime): Boolean;
var
  DtaNull: TDateTime;
begin
  DtaNull := StrToDate('30/12/1899');
  //
  if Date <= DtaNull then
    Result := True
  else
    Result := False;
end;

function TUnDmkProcFunc.TimeToMinutes(Time: TDateTime): Integer;
begin
  Result := MinutesBetween(EncodeTime(0, 0, 0,0 ), Time);
end;

function TUnDmkProcFunc.TipoDeCarteiraCashier(Tipo: Integer;
  Plural: Boolean): String;
begin
  if Plural then
  begin
    case Tipo of
      0: Result := CO_CAIXAS;
      1: Result := CO_BANCOS;
      2: Result := CO_EMISSOES;
      else Result := CO_DESCONHECIDO;
    end;
  end else begin
    case Tipo of
      0: Result := CO_CAIXA;
      1: Result := CO_BANCO;
      2: Result := CO_EMISS;
      else Result := CO_DESCONHECIDO;
    end;
  end;
end;

function TUnDmkProcFunc.TipoDeCarteiraCashierSub(Tipo, PagRec: Integer;
  Plural: Boolean): String;
begin
  if Plural then
  begin
    case Tipo of
      0: Result := CO_CAIXAS;
      1: Result := CO_BANCOS;
      2:
      case PagRec of
        -1: Result := CO_EMISSS_PAGAR;
         0: Result := CO_EMISSS_AMBOS;
         1: Result := CO_EMISSS_RECEB;
         else Result := CO_DESCONHECIDO;
      end;
      else Result := CO_DESCONHECIDO;
    end;
  end else begin
    case Tipo of
      0: Result := CO_CAIXA;
      1: Result := CO_BANCO;
      2:
      case PagRec of
        -1: Result := CO_EMISS_PAGAR;
         0: Result := CO_EMISS_AMBOS;
         1: Result := CO_EMISS_RECEB;
         else Result := CO_DESCONHECIDO;
      end;
      else Result := CO_DESCONHECIDO;
    end;
  end;
end;

function TUnDmkProcFunc.TipoEPagRecDeCarteira(Tipo, PagRec: Integer): Integer;
begin
  Result := Tipo * 10;
  if Tipo = 2 then
    Result := Result + PagRec + 1;
end;

// LimpaMemoria - Limpa memoria residual
// Cuidado! Manda para o disco!
procedure TUnDmkProcFunc.TrimAppMemorySize();
var
  MainHandle : THandle;
begin
  try
    MainHandle := OpenProcess(PROCESS_ALL_ACCESS, False, GetCurrentProcessID) ;
    SetProcessWorkingSetSize(MainHandle, $FFFFFFFF, $FFFFFFFF) ;
    CloseHandle(MainHandle) ;
  except
    //
  end;
  Application.ProcessMessages;
end;

function TUnDmkProcFunc.TrocaCaracterEspecial(aTexto: String; aAcentos,
  aLimExt: Boolean): String;
const
  //Lista de caracteres especiais
  xCarEsp: array[1..38] of String = ('á', 'à', 'ã', 'â', 'ä','Á', 'À', 'Ã', 'Â', 'Ä',
                                     'é', 'è','É', 'È','í', 'ì','Í', 'Ì',
                                     'ó', 'ò', 'ö','õ', 'ô','Ó', 'Ò', 'Ö', 'Õ', 'Ô',
                                     'ú', 'ù', 'ü','Ú','Ù', 'Ü','ç','Ç','ñ','Ñ');
  //Lista de caracteres para troca
  xCarTro: array[1..38] of String = ('a', 'a', 'a', 'a', 'a','A', 'A', 'A', 'A', 'A',
                                     'e', 'e','E', 'E','i', 'i','I', 'I',
                                     'o', 'o', 'o','o', 'o','O', 'O', 'O', 'O', 'O',
                                     'u', 'u', 'u','u','u', 'u','c','C','n', 'N');
  //Lista de Caracteres Extras
  xCarExt: array[1..48] of string = ('<','>','!','@','#','$','%','¨','&','*',
                                     '(',')','_','+','=','{','}','[',']','?',
                                     ';',':',',','|','*','"','~','^','´','`',
                                     '¨','æ','Æ','ø','£','Ø','ƒ','ª','º','¿',
                                     '®','½','¼','ß','µ','þ','ý','Ý');
var
  xTexto : string;
  i : Integer;

begin
  Result := aTexto;
   xTexto := aTexto;
   if aAcentos then
   begin
     for i:=1 to 38 do
       xTexto := StringReplace(xTexto, xCarEsp[i], xCarTro[i], [rfreplaceall]);
   end;
   //De acordo com o parâmetro aLimExt, elimina caracteres extras.
   if (aLimExt) then
   begin
     for i:=1 to 48 do
       xTexto := StringReplace(xTexto, xCarExt[i], '', [rfreplaceall]);
   end;
   for i := 1 to Length(xTexto)  do
   begin
     if Ord(xTexto[i]) > 167 then
       xTexto[i] := ' ';
   end;
   Result := xTexto;
end;

function TUnDmkProcFunc.TTM(Hora: String): Integer;
var
 H, M: String;
 i, l: Integer;
begin
  l := Length(Hora);
  i := Pos(':', Hora);
  if i > 0 then
  begin
    H := Copy(Hora, 1, i-1);
    if l > i then M := Copy(Hora, i+1, 2);
  end else begin
    case l of
      0: M := '00';
      1: M := '0'+Hora;
      2: M := Hora;
      else M := Copy(Hora, l-1, 2);
    end;
    case l of
      0: H := '00';
      1: H := '00';
      2: H := '00';
      3: H := Hora[1];
      else H := Hora[1]+Hora[2]
    end;
  end;
  //
  Result := (Geral.IMV(H)*60) + Geral.IMV(M);
end;

function TUnDmkProcFunc.TxtGrandezaPeriodo(Grandeza: Integer; Plural: Boolean): String;
begin
  if Plural then
  begin
    case Grandeza of
      0: Result := 'dias';
      1: Result := 'semanas';
      2: Result := 'meses';
      3: Result := 'anos';
      else Result := '?????';
    end;
  end else
  begin
    case Grandeza of
      0: Result := 'dia';
      1: Result := 'semana';
      2: Result := 'mês';
      3: Result := 'ano';
      else Result := '?????';
    end;
  end;
end;

function TUnDmkProcFunc.TxtUH: String;
begin
  case VAR_KIND_DEPTO of
    kdUH: Result := 'Unidade';
    kdObra: Result := 'Obra';
    kdOS1: Result := 'O.S.';
    kdNenhum: Result := '[Depto]';
  end;
end;

function TUnDmkProcFunc.TZD_UTC_Define(Atual: Double): Double;
var
  Sinal, Hora: String;
  TZD_UTC, Fator: Double;
begin
  Result := Atual;
  TZD_UTC := Atual;
  if Atual < 0 then
  begin
    Sinal := '-';
    TZD_UTC := TZD_UTC * -1;
  end
  else
    Sinal := '+';
  Hora := Sinal + Geral.FDT(TZD_UTC, 102);
  //
  if InputQuery('Hora TZD UTC', 'Informe a hora para conversão:', Hora) then
  begin
    Fator := 1;
    Hora := Trim(Hora);
    if Hora[1] = '-' then
    begin
      Fator := -1;
      Hora := Copy(Hora, 2);
    end else
    if Hora[1] = '+' then
      Hora := Copy(Hora, 2);
    //
    TZD_UTC := StrToDateTime(Hora) * Fator;
    //
    Result := TZD_UTC;
  end;
end;

function TUnDmkProcFunc.TZD_UTC_FloatToSignedStr(TZD_UTC: Double): String;
begin
  //Result := '+00:00';
  if TZD_UTC < 0 then
    Result := '-' + Geral.FDT(TZD_UTC, 102)
  else
    Result := '+' + Geral.FDT(TZD_UTC, 102);
end;

procedure TUnDmkProcFunc.TZD_UTC_InfoHelp;
begin
  Geral.MB_Aviso('Exemplo: no formato UTC a hora, "TZD" pode ser: ' +
  sLineBreak + sLineBreak +
  ' -02:00 (Fernando de Noronha), ' + sLineBreak +
  ' -03:00 (Brasília), ' + sLineBreak +
  ' -04:00 (Manaus) ' + sLineBreak + sLineBreak +
  'No horário de verão serão: ' + sLineBreak + sLineBreak +
  ' -01:00 (Fernando de Noronha), ' + sLineBreak +
  ' -02:00 (Brasília), ' + sLineBreak +
  ' -03:00 (Manaus) ');
end;

function TUnDmkProcFunc.UltimoDiaDoPeriodo(Periodo: Integer;
  Tipo: TDataTipo): String;
var
  Ano, Mes, Dia: Word;
begin
  Ano := Periodo div 12 + 2000;
  Mes := Periodo mod 12;
  if Mes = 0 then
  begin
    Mes := 12;
    Ano := Ano -1;
  end;
  if Mes = 12 then
  begin
    Mes := 1;
    Ano := Ano + 1;
  end else if Mes > 12 then Mes := 1
  else Mes := Mes + 1;
  if Tipo = dtMySQL then
  begin
    DecodeDate(EncodeDate(Ano, Mes, 1)-1, Ano, Mes, Dia);
    Result := FormatFloat('0000', Ano)+FormatFloat('00', Mes)+FormatFloat('00', Dia);
  end;
  if Tipo = dtSystem3 then
    Result := FormatDateTime(String(VAR_FORMATDATE3), EncodeDate(Ano, Mes, 1)-1);
  if Tipo = dtSystem2 then
    Result := FormatDateTime(String(VAR_FORMATDATE2), EncodeDate(Ano, Mes, 1)-1);
  if Tipo = dtSystem then
    Result := FormatDateTime(String(VAR_FORMATDATE), EncodeDate(Ano, Mes, 1)-1);
  if Tipo = dtTexto then
    Result := FormatDateTime('mmmmm/yyyy', EncodeDate(Ano, Mes, 1)-1);
end;

function TUnDmkProcFunc.UltimoDiaDoPeriodo_Date(Periodo: Integer): TDateTime;
var
  Ano, Mes: Word;
begin
  Ano := Periodo div 12 + 2000;
  Mes := Periodo mod 12;
  if Mes = 0 then
  begin
    Mes := 12;
    Ano := Ano -1;
  end;
  if Mes = 12 then
  begin
    Mes := 1;
    Ano := Ano + 1;
  end else Mes := Mes + 1;
  Result := EncodeDate(Ano, Mes, 1)-1;
end;

procedure TUnDmkProcFunc.UnScheduleRunAtStartup(const ATaskName: String);
begin
  ShellExecute(0, nil, 'schtasks', PChar('/delete /f /tn "' + ATaskName + '"'),
    nil, SW_HIDE);
end;

function TUnDmkProcFunc.SQLNumRange(WHR_AND, GGXs: String): String;
begin
  //Explod
end;

function TUnDmkProcFunc.SQLStringReplaceFldByValFld(SQL, Campo: String;
  Valor: Variant): String;
var
  Campo_TXT, Valor_TXT, ns: String;
begin
  ns := SQL;
  Campo_TXT := ' ' + Campo + ' ';
  Valor_TXT :=  ' ' + Geral.VariavelToString(Valor) + ' ' + Campo + ' ';
  ns := StringReplace(ns, Campo_TXT, Valor_TXT, []);
  Result := ns;
end;

function TUnDmkProcFunc.SQL_CodTxt(Texto, Codigo: String; Zero, Negativo,
  RetornaTextoVazio: Boolean): String;
var
  Cod: Integer;
begin
  Result := '';
  if RetornaTextoVazio then Exit;
  Cod := Geral.IMV(Codigo);
  if (Cod < 0) and (Negativo = False) then Exit;
  if (Cod = 0) and (Zero     = False) then Exit;
  Result := Texto + ' = ' + FormatFloat('0', Cod);
end;

function TUnDmkProcFunc.SQL_Competencia_Mensal(Texto: String; Ini, Fim: Variant;
  CkIni, CkFim: Boolean): String;
  function TxtToMez(Txt: String): String;
  begin
    Result := FormatDateTime('YYMM', Geral.ValidaDataSimples(Txt, True));
  end;
const
  Fmt = '0'; // Obriga ao MySQL fazer o campo sem ponto
begin
  if CkIni and CkFim then
    Result := Texto + ' BETWEEN "'+TxtToMez(Ini)+
    '" AND "'+TxtToMez(Fim)+'"'
  else if CkIni then
    Result := Texto + ' >= "'+TxtToMez(Ini)+'"'
  else if CkFim then
    Result := Texto + ' <= "'+TxtToMez(Fim)+'"'
  else begin
    if Uppercase(Copy(Trim(Texto), 1, 5)) = 'WHERE' then
      Result := Texto + ' > -1000'
    else
      Result := '';
  end;
end;

function TUnDmkProcFunc.SQL_DataMenMai(Texto: String; Data: TDateTime;
  CkIni: Boolean): String;
const
  Fmt = 'YYYY-MM-DD'; // Obriga ao MySQL fazer o campo como data e não texto
begin
  if CkIni then
    Result := Texto + '"' + FormatDateTime(Fmt, Int(Data)) + '"'
  else
    Result := Texto + '"' + FormatDateTime(Fmt, 0) + '"'
end;

function TUnDmkProcFunc.SQL_DeLista(Lista: array of String;
  MaxItem: Integer): String;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to  MaxItem do
    Result := Result + ', "' + Lista[I] + '"';
  Result := Copy(Result, 3);
end;

function TUnDmkProcFunc.SQL_ELT(Campo, NO_Campo: String;
  PosVirgula: TPosVirgula; IniZero: Boolean; MyArr: array of String;
  SomaIni: Integer): String;
begin
  Result := ArrayToTexto(Campo, NO_Campo, PosVirgula, IniZero, MyArr, SomaIni);
end;

function TUnDmkProcFunc.SQL_ExplodeInBetween(const WHR_AND, Campo, Texto: String;
  var SQL_Res: String): Boolean;
const
  SepUni = ',';
  SepSeq = '-';
var
  I, J, N: Integer;
  Lista: array of String;
  SQL_IN, SQL_BETWEEN, Str: String;
begin
  Str         := Texto;
  Result      := False;
  SQL_Res     := '';
  SQL_IN      := '';
  SQL_BETWEEN := '';
  //
  N := 0;
  SetLength(Lista, N);
  I := Pos(SepUni, Str);
  //
  while (i > 0) do
  begin
    N := N + 1;
    SetLength(Lista, N);
    Lista[N - 1] := Copy(Str, 1, i-1);
    Delete(Str, 1, I + Length(SepUni) - 1);
    //
    I := Pos(SepUni, Str);
  end;
  if (Str <> '') then
  begin
    N := N + 1;
    SetLength(Lista, N);
    Lista[N - 1] := Str;
  end;
  for I := 0 to N - 1 do
  begin
    J := Pos(SepSeq, Lista[I]);
    if (J = 1) then
    begin
      Lista[I] := Copy(Lista[I], 2);
      J := Pos(SepSeq, Lista[I]);
    end;
    if J = Length(Lista[I]) then
    begin
      Lista[I] := Copy(Lista[I], 1, Length(Lista[I]) - 1);
      J := Pos(SepSeq, Lista[I]);
    end;
    if J = 0 then
    begin
      if SQL_IN <> '' then
        SQL_IN := SQL_IN + ',';
      SQL_IN := SQL_IN + Lista[I];
    end else
    begin
      if SQL_BETWEEN <> '' then
        SQL_BETWEEN := SQL_BETWEEN + ' OR ' + slineBreak;
      SQL_BETWEEN := SQL_BETWEEN + ' (' +
        Campo + ' BETWEEN ' + Geral.Substitui(Lista[I], SepSeq, ' AND ') + ') ';
    end;
  end;
  if SQL_BETWEEN <> '' then
  begin
    SQL_Res := SQL_BETWEEN;
    if SQL_IN <> '' then
      SQL_Res := SQL_Res + sLineBreak + ' OR ';
  end;
  if SQL_IN <> '' then
    SQL_Res := SQL_Res + '(' + Campo + ' IN (' + SQL_IN + '))';
  Result := Trim(SQL_Res) <> '';
  if Result then
    SQL_Res := WHR_AND + ' (' + slineBreak + SQL_Res + sLineBreak + ')' + sLineBreak;
end;

function TUnDmkProcFunc.SQL_IntervInt(Texto: String; Ini, Fim: Largeint; CkIni,
  CkFim: Boolean): String;
var
  IntI, IntF: String;
begin
  if CkIni and CkFim then
  begin
    IntI := IntToStr(Ini);
    IntF := IntToStr(Fim);
    Result := Texto + ' BETWEEN ' + IntI + ' AND ' + IntF;
  end else
  if CkIni then
  begin
    IntI := IntToStr(Ini);
    Result := Texto + ' >= "' + IntI + '"'
  end else
  if CkFim then
  begin
    IntF := IntToStr(Fim);
    Result := Texto + ' < "' + IntF + '"'
  end else
  begin
    if Uppercase(Copy(Trim(Texto), 1, 5)) = 'WHERE' then
      Result := Texto + ' > -99999999999999999999999999999999'
    else
      Result := '';
  end;
end;

function TUnDmkProcFunc.SQL_Mensal(Texto, Ini, Fim: String;
  NaoMensais: Boolean): String;
var
  MezIni, MezFim: String;
  i, f: Boolean;
begin
  Result := '';
  if not NaoMensais then
    Result := Result + ' AND ' + Texto + ' > 0 ';

  i := Length(Ini) = 7;
  if not i then
    MezIni := ''
  else
    MezIni := Ini[6] + Ini[7] + Ini[1] + Ini[2];

  f := Length(Fim) = 7;
  if not f then
    MezFim := ''
  else
    MezFim := Fim[6] + Fim[7] + Fim[1] + Fim[2];


  if i and f then
    Result := Result + ' AND ' + Texto + ' BETWEEN ' + MezIni + ' AND ' + MezFim
  else if i then
    Result := Result + ' AND ' + Texto + ' >= ' + MezIni
  else if f then
    Result := Result + ' AND ' + Texto + ' <= ' + MezFIm;
end;


function TUnDmkProcFunc.SQL_Mensal_2(Texto, Ini, Fim: String;
  NaoMensais: Boolean): String;
var
  MezIni, MezFim: String;
  i, f: Boolean;
  n, p1, p2: Integer;
begin
  if NaoMensais then
    Result := ''
  else
    Result := Texto + ' > 0 ';

  n := pos('/', Ini);
  i := n > 0;
  if not i then
    MezIni := ''
  else begin
    p1 := Geral.IMV(Copy(Ini, n+1, Length(Ini)));
    if p1 > 2000 then p1 := p1 - 2000;
    p1 := p1 * 100;
    p1 := p1 + Geral.IMV(Copy(Ini, 1, n-1));
    MezIni := IntToStr(p1);
  end;

  n := pos('/', Fim);
  f := n > 0;
  if not f then
    MezFim := ''
  else begin
    p2 := Geral.IMV(Copy(Fim, n+1, Length(Fim)));
    if p2 > 2000 then p2 := p2 - 2000;
    p2 := p2 * 100;
    p2 := p2 + Geral.IMV(Copy(Fim, 1, n-1));
    MezFim := IntToStr(p2);
  end;

  if i and f then
    Result := Result + Texto + ' BETWEEN ' + MezIni + ' AND ' + MezFim
  else if i then
    Result := Result + Texto + ' >= ' + MezIni
  else if f then
    Result := Result + Texto + ' <= ' + MezFIm;
end;

{  A partir do Win2K e XP não há mais provilégios para acessar a porta!
function TUnDmkProcFunc.TestePorta(): LongWord;
(*
Hi Eric
In assembly language, it's fairly simple to write to port 388h.
Simply write
   mov   dx,388h            ;don't use any other register than dx
   mov   al,<byte of data you wish to write to the port>  ;ax if it's a
                                                          ;word port
   out   dx,al     ;for writing to the port
OR
   in    al,dx     ;for reading from the port, again ax if it's a word
                   ;port
Maybe you can blend this assembly language code in whatever code you will
be working.
Hope this helps.
--
Regards,
---------------------------------------------------
Danny Hendrickx   |                               |
Alcatel Telecom   |                               |
Antwerp, Belgium  | hendr...@btmaa.bel.alcatel.be |
*)
var
  a: LongWord;
begin
  asm
     mov   dx,388h;            // don't use any other register than dx
     //mov   al,<byte of data you wish to write to the port>  ;ax if it's a word port
     //mov   al,a;
(*
     out   dx,al     ;for writing to the port
  OR
     in    al,dx     ;for reading from the port, again ax if it's a word
*)
     in    al,dx;     //;for reading from the port, again ax if it's a word
     //mov a, dx;
  end;
end;
}

//function TUn M L A G e r a l.ValidaData M y SQL(Texto: String; PermiteZero: Boolean):
function TUnDmkProcFunc.ValidaDataDeSQL(Texto: String;
  PermiteZero: Boolean): TDateTime;
var
  //Data: TDateTime;
  Dia, Mes, Ano : Word;
  ADia, AMes, AAno : Word;
  TextChar : PChar;
  i: Integer;
  Compara: String;
  Sep: Char;
begin
  try
    Compara := '';
    if pos('-', Texto) > 0 then
      Sep := '-'
    else
    if pos('/', Texto) > 0 then
      Sep := '/'
    else
      Sep := ' ';
    //
    if PermiteZero then
    begin
      for i := 1 to Length(Texto) do
      begin
        if Texto[i] = Sep then
          Compara := Compara + Texto[i]
        else
          Compara := Compara + '0';
      end;
      if Texto = Compara then
      begin
        Result := 0;
        Exit;
      end;
    end;
    TextChar := PChar(Texto);
    if (Pos(Sep, Texto) = 3) and (Length(Texto) = 5) then
    begin
      DecodeDate(Date, Ano, Mes, Dia);
      Mes := Geral.IMV(Copy(Texto, 1, 2));
      Dia := Geral.IMV(Copy(Texto, 4, 2));
    end
    else
    if (Pos(Sep, Texto) = 3) and (Length(Texto) = 8) then
    begin
      Ano := Geral.IMV(Copy(Texto, 1, 2));
      Mes := Geral.IMV(Copy(Texto, 4, 2));
      Dia := Geral.IMV(Copy(Texto, 7, 2));
    end
    else
    if (Pos(Sep, Texto) = 5) and (Length(Texto) = 10) then
    begin
      Ano := Geral.IMV(Copy(Texto, 1, 4));
      Mes := Geral.IMV(Copy(Texto, 6, 2));
      Dia := Geral.IMV(Copy(Texto, 9, 2));
    end
    else
    if (StrRScan(TextChar,Sep) = nil) then
    begin
      case Length(Texto) of
        4:
        begin
          DecodeDate(Date, AAno, AMes, ADia);
          Ano := AAno;
          Mes := StrToInt(Texto[1]+Texto[2]);
          Dia := StrToInt(Texto[3]+Texto[4]);
        end;
        6:
        begin
          Ano := StrToInt(Texto[1]+Texto[2]);
          Mes := StrToInt(Texto[3]+Texto[4]);
          Dia := StrToInt(Texto[5]+Texto[6]);
        end;
        8:
        begin
          Ano := StrToInt(Texto[1]+Texto[2]+Texto[3]+Texto[4]);
          Mes := StrToInt(Texto[5]+Texto[6]);
          Dia := StrToInt(Texto[7]+Texto[8]);
        end;
      end;
    end else
    begin
      Ano := 0;
      Mes := 0;
      Dia := 0;
    end;
    //
    if Ano < 100 then
      if Ano > 49 then Ano := Ano + 1900 else Ano := Ano + 2000;
    //
    if (Ano=0) or (Mes=0) or (Dia=0) then
      Result := 0
    else
      Result := EncodeDate(Ano, Mes, Dia);
  except
    on EConvertError do
    begin
      Geral.MB_Erro('Data inválida!');
      Result := 0;
    end;
  end;
end;

function TUnDmkProcFunc.ValidaDiretorio(Diretorio: String;
  BarraNoFinal: Boolean): String;
var
  CaracFinal, TipoBarra: String;
begin
  if Diretorio <> '' then
  begin
    if Pos('/', Diretorio) > 0 then
      TipoBarra := '/'
    else
      TipoBarra := '\';
    //
    CaracFinal := Copy(Diretorio, Length(Diretorio), 1);
    //
    if CaracFinal = TipoBarra then
    begin
      if BarraNoFinal = False then
        Result := Copy(Diretorio, 0, Length(Diretorio) - 1)
      else
        Result := Diretorio;
    end else
    begin
      if BarraNoFinal = True then
        Result := Diretorio + TipoBarra
      else
        Result := Diretorio;
    end;
  end else
    Result := Diretorio;
end;

function TUnDmkProcFunc.ValidaPalavra(Txt: string;
  ConverteParaMaiusculas: Boolean): string;
var
  Ch: Char;
  i: Integer;
  Texto, Fonte: String;
begin
  if Trim(Txt) = '' then Result := Txt else
  begin
    Fonte := Geral.SemAcento(Txt);
    if ConverteParaMaiusculas then
      Fonte := Geral.Maiusculas(Fonte, False);
    Texto := '';
    for i := 1 to Length(Fonte) do
    begin
      Ch := Fonte[i];
      if Pos(Ch, ValidWord) <> 0 then
        Texto:= Texto + Ch;
    end;
    Result := Texto;
  end;
end;

function TUnDmkProcFunc.VariantToString(Variant: Variant): String;
var
  DataHora: TDateTime;
begin
  case TVarData(Variant).VType of
    varDate:
    begin
      DataHora := VarToDateTime(Variant);
      Result   := Geral.FDT(DataHora, 109);
    end;
    varDouble:
      Result := Geral.FFT_Dot(Variant, 2, siNegativo);
    else
      Result := VarToStr(Variant);
  end;
end;

function TUnDmkProcFunc.VariavelToTexto(Formato: String;
  Variavel: Variant): String;
  function Inteiro(Formato: String; Variavel: Variant): String;
  begin
    if Variavel = Null then
      Result := ''
    else if ((VarType(Variavel) = varString)
{$IFDEF DELPHI12_UP} // nao tenho certeza em qual delphi comecou, mas acho que eh no 2009
or (VarType(Variavel) = VarUString)
{$ENDIF}
    ) and (Variavel = '') then
      Result := '0'
    else begin
      if Formato <> '' then
        Result := FormatFloat(Formato, Integer(Variavel))
      else
        Result := FormatFloat('0', Integer(Variavel));
    end;
  end;
  function Texto(Formato: String; Variavel: Variant): String;
  begin
    if Uppercase(Formato) = 'CNPJ' then
      Result := String(Geral.FormataCNPJ_TT(AnsiString(Variavel)))
    else
      Result := Variavel;
  end;
begin
  ///
  ///  C U I D A D O !
  //////////////////////////////////////////////////////////////////////////////
  // para ver a variavel sem aspas use                                        //
  // VariantToString                                                          //
  //////////////////////////////////////////////////////////////////////////////
  ///
  ///
  case VarType(Variavel) of
    varEmpty    {= $0000;} { vt_empty        0 } : Result := '';
    varNull     {= $0001;} { vt_null         1 } : Result := '';
    varSmallint {= $0002;} { vt_i2           2 } : Result := Inteiro(Formato, Variavel);
    varInteger  {= $0003;} { vt_i4           3 } : Result := Inteiro(Formato, Variavel);
    varSingle   {= $0004;} { vt_r4           4 } : Result := FormatFloat(Formato, Variavel);
    varDouble   {= $0005;} { vt_r8           5 } : Result := FormatFloat(Formato, Variavel);
    varCurrency {= $0006;} { vt_cy           6 } : Result := FormatFloat(Formato, Variavel);
    varDate     {= $0007;} { vt_date         7 } : Result := FormatDateTime(Formato, Variavel);
    varOleStr   {= $0008;} { vt_bstr         8 } : Result := Variavel;
    varDispatch {= $0009;} { vt_dispatch     9 } : Result := Variavel;
    varError    {= $000A;} { vt_error       10 } : Result := Variavel;
    varBoolean  {= $000B;} { vt_bool        11 } : Result := IntToStr(Geral.BoolToInt(Variavel));
    varVariant  {= $000C;} { vt_variant     12 } : Result := Variavel;
    varUnknown  {= $000D;} { vt_unknown     13 } : Result := Variavel;
  //varDecimal  {= $000E;} { vt_decimal     14 } {UNSUPPORTED as of v6.x code base}
  //varUndef0F  {= $000F;} { undefined      15 } {UNSUPPORTED per Microsoft}
    varShortInt {= $0010;} { vt_i1          16 } : Result := Inteiro(Formato, Variavel);
    varByte     {= $0011;} { vt_ui1         17 } : Result := Inteiro(Formato, Variavel);
    varWord     {= $0012;} { vt_ui2         18 } : Result := Inteiro(Formato, Variavel);
    varLongWord {= $0013;} { vt_ui4         19 } : Result := Inteiro(Formato, Variavel);
    varInt64    {= $0014;} { vt_i8          20 } : Result := Inteiro(Formato, Variavel);
  //varWord64   {= $0015;} { vt_ui8         21 } {UNSUPPORTED as of v6.x code base}
  {  if adding new items, update Variants' varLast, BaseTypeMap and OpTypeMap }

    varStrArg   {= $0048;} { vt_clsid       72 } : Result := Variavel;
    varString   {= $0100;} { Pascal string 256 } {not OLE compatible } : Result := Variavel;
    varAny      {= $0101;} { Corba any     257 } {not OLE compatible } : Result := Variavel;
{$IFDEF DELPHI12_UP} // nao tenho certeza em qual delphi comecou, mas acho que eh no 2009
    varUString  {= $0102;} { Unicode string 258 } {not OLE compatible }: Result := Variavel;
{$ENDIF}
    // custom types range from $110 (272) to $7FF (2047)
    else begin
      //raise EAbort.Create('Variável não definida!');
      Geral.MB_ERRO(
      'Variável não definida!' + sLineBreak +
      'dmkPF.VariavelToTexto()' + sLineBreak +
      'VarType desconhecido: ' + IntToStr(VarType(Variavel)));
      //
      Result := '"' + Geral.VariantToString(Variavel) + '"';
    end;
  end;
  {  Não Pode, atrapalha FmCNAB_Rem
  if VarType(Variavel) in ([varSingle, varDouble, varCurrency]) then
  begin
      if pos(',', Result) > 0 then Result[pos(',', Result)] := '.';
  end;
  }
end;

{
function TUnDmkProcFunc.ValidaTexto_XML(Txt: String): String;
var
  I: Integer;
  Res: String;
begin
  Res := '';
  for I := 1 to Length(Txt) do
    Res := Res + CharToMyXML_Enviar(Ord(Txt[I]));
  Result := Res;
end;
}

function TUnDmkProcFunc.CheckSumEAN13(CodEAN13: String; Avisa: Boolean): Boolean;
var
  Codigo: String;
  I, Digito, Res: Integer;
begin
  Codigo := CodEAN13;
  while Length(Codigo) < 13 do
    Codigo := '0' + Codigo;
  Res := 0;
(*
L = Split("789100031550","")   'Converte string em Array de Caracteres
For i = 0 to L.Ubound
    RE = RE + L(i).Val * ((i MOD 2) * 2 + 1)  ' soma todos multiplicando apenas os pares por 3
Next
*)
  for I := 1 to 12 do
  begin
    Digito := Geral.IMV(Codigo[I]);
    Res := Res + Digito * (((I + 1) mod 2) * 2 + 1);
  end;
(*
RE = ((1 - ((re / 10)-(re \ 10))) * 10) MOD 10   'Acha o múltiplo referido a cima
'neste caso RE será 7    //-> por @windrson
*)
  Res := 10 - (Res mod 10);
  //
  Result := Geral.FF0(Res) = Codigo[13];
  if not Result and Avisa then
    Geral.MB_Aviso('Número EAN13 inválido: ' + sLineBreak + CodEAN13);
end;

function TUnDmkProcFunc.CODIFToInt_Receita(Codif: String): Integer;
var
  iNum, iCol: Integer;
  xNum, xCol: String;
begin
  xnum := Copy(Codif, Length(Codif) - 1);
  iNum := Geral.IMV(xNum);
  xCol := Copy(Codif, 1, Length(Codif) - 2);
  iCol := ColTxtToInt(xCol);
  iCol := iCol - 27; // -> inicia com AA!
  Result := (iCol * 100) + iNum;
end;

function TUnDmkProcFunc.VerificaIPSintaxe(IP: String; Avisa: Boolean): Boolean;
var
  //MeuIP: String;
  i, p: Integer;
  n0, n1, n2, n3: String;
  Erro: Boolean;
begin
  Result := True;
  Erro := False;
  if Trim(IP) = '' then Exit;
  if IP <> Geral.SoNumeroEPonto_TT(IP) then Erro := True
  else if IP[1] = '.' then Erro := True
  else if IP[Length(IP)] = '.' then Erro := True
  else begin
    p := 0;
    for i := 1 to Length(IP) do
    begin
      if IP[i] = '.' then
      begin
        p := p + 1;
        if p > 3 then
        begin
          Erro := True;
          Break;
        end;
      end else
      begin
        case p of
          0: n0 := n0 + IP[i];
          1: n1 := n1 + IP[i];
          2: n2 := n2 + IP[i];
          3: n3 := n3 + IP[i];
          else Erro := true;
        end;
        if Erro = True then Break;
      end;
    end;
    if p <> 3 then Erro := True;
    if n0 = '' then Erro := True;
    if n1 = '' then Erro := True;
    if n2 = '' then Erro := True;
    if n3 = '' then Erro := True;
    if Geral.IMV(n0) > 255 then Erro := True;
    if Geral.IMV(n1) > 255 then Erro := True;
    if Geral.IMV(n2) > 255 then Erro := True;
    if Geral.IMV(n3) > 255 then Erro := True;
  end;
  if Erro then
  begin
    Result := False;
    if Avisa then Geral.MB_Erro('Sintaxe de IP incorreta!');
  end else Result := True;
end;

function TUnDmkProcFunc.VerificaIPsMesmaRede(IP, Destino: String): Boolean;
var
  p1, p2, p3, p4, MascaraIP, MascaraDestino: String;
begin
  Result := False;
  //
  if Geral.SplitIPv4(IP, p1, p2, p3, p4) then
  begin
    MascaraIP := p1 + '.' + p2 + '.' + p3;
  end;
  if Geral.SplitIPv4(Destino, p1, p2, p3, p4) then
  begin
    MascaraDestino := p1 + '.' + p2 + '.' + p3;
  end;
  Result := (MascaraIP = MascaraDestino) and (IP <> '');
end;

function TUnDmkProcFunc.VerificaMes(Mes: Integer; Upper: Boolean; Sigla:
  Boolean = False): String;
begin
  if Upper then
  begin
    case Mes of
      01: Result := CO_JANEIRO_UPPER;
      02: Result := CO_FEVEREIRO_UPPER;
      03: Result := CO_MARCO_UPPER;
      04: Result := CO_ABRIL_UPPER;
      05: Result := CO_MAIO_UPPER;
      06: Result := CO_JUNHO_UPPER;
      07: Result := CO_JULHO_UPPER;
      08: Result := CO_AGOSTO_UPPER;
      09: Result := CO_SETEMBRO_UPPER;
      10: Result := CO_OUTUBRO_UPPER;
      11: Result := CO_NOVEMBRO_UPPER;
      12: Result := CO_DEZEMBRO_UPPER;
      13: Result := '13º';
      else Result := CO_DESCONHECIDO;
    end;
  end else begin
    case Mes of
      01: Result := CO_JANEIRO;
      02: Result := CO_FEVEREIRO;
      03: Result := CO_MARCO;
      04: Result := CO_ABRIL;
      05: Result := CO_MAIO;
      06: Result := CO_JUNHO;
      07: Result := CO_JULHO;
      08: Result := CO_AGOSTO;
      09: Result := CO_SETEMBRO;
      10: Result := CO_OUTUBRO;
      11: Result := CO_NOVEMBRO;
      12: Result := CO_DEZEMBRO;
      13: Result := '13º';
      else Result := CO_DESCONHECIDO;
    end;
  end;
  if Sigla then
    Result := Copy(Result, 1, 3);
end;

function TUnDmkProcFunc.VerificaMesTxt(Mes: String): Integer;
begin
  if Mes = CO_JANEIRO   then Result := 01 else
  if Mes = CO_FEVEREIRO then Result := 02 else
  if Mes = CO_MARCO     then Result := 03 else
  if Mes = CO_ABRIL     then Result := 04 else
  if Mes = CO_MAIO      then Result := 05 else
  if Mes = CO_JUNHO     then Result := 06 else
  if Mes = CO_JULHO     then Result := 07 else
  if Mes = CO_AGOSTO    then Result := 08 else
  if Mes = CO_SETEMBRO  then Result := 09 else
  if Mes = CO_OUTUBRO   then Result := 10 else
  if Mes = CO_NOVEMBRO  then Result := 11 else
  if Mes = CO_DEZEMBRO  then Result := 12 else
  if Mes = '13º'        then Result := 13 else
  Result := 0;
end;

function TUnDmkProcFunc.VerificarDllRegistrada(var Res: String;
  const NomeLib: String): Boolean;
var
  Retorno  : boolean;
  ObjDll   : Variant;
begin
  Res := 'Erro desconhecido!';
  Retorno := false;
  try
    try
      ObjDll  := CreateOleObject(NomeLib);

      Retorno := True;
      Res := '"' + NomeLib + '" acessado com sucesso!';
    except
      on E: Exception do
      begin
        Retorno := false;
        Res :=  E.Message;
      end;
    end;
  finally
    ObjDll := Unassigned;
    Result := Retorno;
  end;
end;

function TUnDmkProcFunc.VersaoWindows(): MyArrayI03;
//var
  //Menor, Maior: Integer;
begin
  {
  Win95    := (Win32MajorVersion = 4) and (Win32MinorVersion = 0)  and (Win32Platform = );
  Win98    := (Win32MajorVersion = 4) and (Win32MinorVersion = 10) and (Win32Platform = VER_PLATFORM_WIN32_WINDOWS);
  WinME    := (Win32MajorVersion = 4) and (Win32MinorVersion = 90) and (Win32Platform = VER_PLATFORM_WIN32_WINDOWS);
  WinNT    := (Win32MajorVersion = 4) and (Win32MinorVersion = 0)  and (Win32Platform = VER_PLATFORM_WIN32_NT);
  Win2K    := (Win32MajorVersion = 5) and (Win32MinorVersion = 0)  and (Win32Platform = VER_PLATFORM_WIN32_NT);
  WinXP    := (Win32MajorVersion = 5) and (Win32MinorVersion = 1)  and (Win32Platform = VER_PLATFORM_WIN32_NT);
  WinVista := (Win32MajorVersion = 6) and (Win32Platform = VER_PLATFORM_WIN32_NT);
  }
  Result[1] := Win32MajorVersion;
  Result[2] := Win32MinorVersion;
  Result[3] := Win32Platform;
end;

function TUnDmkProcFunc.VeSeEhDirDmk(Raiz, Pasta: String; Inverte: Boolean): String;
var
  Dir: String;
  C: Char;
begin
  Dir := Raiz;
  if (Dir <> '') and (Pasta <> '') then
  begin
    C := Dir[Length(Dir)];
    if (C <> '/') and (C <> '\') and (C <> '|') then
    begin
      C := Pasta[1];
      if (C <> '/') and (C <> '\') and (C <> '|') then
      begin
        if Inverte then
          Dir := Dir + '/'
        else
          Dir := Dir + '\';
      end;
    end;
  end;
  Dir := Dir + Pasta;
  //
  if (Dir <> '') and (Dir[1] = '|') then
  begin
    Result := Copy(Dir, 2);
    if Inverte then
      Result := Geral.Substitui(Result, '|', '/')
    else
      Result := Geral.Substitui(Result, '|', '\');
  end else
    Result := Dir;
end;


function TUnDmkProcFunc.VP(Valor: Double): Double;
begin
  if Valor < 1 then
    Result := - Valor
  else
    Result := Valor;
end;

function TUnDmkProcFunc.V_FToInt(V_F: WideChar): Integer; (* BERLIN Ansi to Wide*)
begin
  if V_F = #0 then Result := 0
  else if Uppercase(String(V_F)) = 'V' then Result := 1
  else if Uppercase(String(V_F)) = 'F' then Result := 0
  else begin
    Geral.MB_Erro('Letra não definida na "function V_FToInt".');
    Result := -1;
  end;
end;

{
function TUnDmkProcFunc.V_FToInt(V_F: AnsiChar): Integer; override;
begin
  if V_F = #0 then Result := 0
  else if Uppercase(String(V_F)) = 'V' then Result := 1
  else if Uppercase(String(V_F)) = 'F' then Result := 0
  else begin
    Geral.MB_Erro('Letra não definida na "function V_FToInt".');
    Result := -1;
  end;
end;
}

function TUnDmkProcFunc.ServicoWindowsEstaRodando(sMachine, sService: PChar): Boolean;

  function ServiceGetStatus(sMachine, sService: PChar): DWORD;
    {******************************************}
    {*** Parameters: ***}
    {*** sService: specifies the name of the service to open
    {*** sMachine: specifies the name of the target computer
    {*** ***}
    {*** Return Values: ***}
    {*** -1 = Error opening service ***}
    {*** 1 = SERVICE_STOPPED ***}
    {*** 2 = SERVICE_START_PENDING ***}
    {*** 3 = SERVICE_STOP_PENDING ***}
    {*** 4 = SERVICE_RUNNING ***}
    {*** 5 = SERVICE_CONTINUE_PENDING ***}
    {*** 6 = SERVICE_PAUSE_PENDING ***}
    {*** 7 = SERVICE_PAUSED ***}
    {******************************************}
  var
    SCManHandle, SvcHandle: SC_Handle;
    SS: TServiceStatus;
    dwStat: DWORD;
  begin
    dwStat := 0;
    // Open service manager handle.
    SCManHandle := OpenSCManager(sMachine, nil, SC_MANAGER_CONNECT);
    if (SCManHandle > 0) then
    begin
      SvcHandle := OpenService(SCManHandle, sService, SERVICE_QUERY_STATUS);
      // if Service installed
      if (SvcHandle > 0) then
      begin
        // SS structure holds the service status (TServiceStatus);
        if (QueryServiceStatus(SvcHandle, SS)) then
          dwStat := ss.dwCurrentState;
        CloseServiceHandle(SvcHandle);
      end;
      CloseServiceHandle(SCManHandle);
    end;
    Result := dwStat;
  end;

begin
  Result := SERVICE_RUNNING = ServiceGetStatus(sMachine, sService);
end;

function TUnDmkProcFunc.WinExecAndWaitOrNot(LinhaDeComando: string;
  Visibility: Integer; Memo: TMemo; Wait: Boolean): Longword;
var { by Pat Ritchey }
  zAppName: array[0..512] of Char;
  zCurDir: array[0..255] of Char;
  WorkDir: string;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
  //
  Handle : THandle;
  Saida: Boolean;
  Text: TextFile;
  ArqTemp, S: String;
begin

  if (Visibility = SW_HIDE) and (Memo <> nil) then Saida := True else Saida := False;
  if Saida then
  begin
    //ArqTemp := 'C:\_'+ObtemDataStr(Now, True)+'.txt';
    ArqTemp := 'C:\_cltest.txt';
    if FileExists(ArqTemp) then System.SysUtils.DeleteFile(ArqTemp);
    GetStartupInfo(StartupInfo);
    Handle := CreateFile(PChar(ArqTemp), GENERIC_READ
      or GENERIC_WRITE,0,nil,CREATE_ALWAYS,0,0);
  end else Handle := 0;
  //
  VAR_APLICATION_TERMINATE := False;
  StrPCopy(zAppName, LinhaDeComando);
  GetDir(0, WorkDir);
  StrPCopy(zCurDir, WorkDir);
  FillChar(StartupInfo, SizeOf(StartupInfo), #0);
  if Saida then StartupInfo.hStdOutput := Handle;
  StartupInfo.cb          := SizeOf(StartupInfo);
  if Saida then
    StartupInfo.dwFlags   := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES
  else
    StartupInfo.dwFlags   := STARTF_USESHOWWINDOW;
  StartupInfo.wShowWindow := Visibility;
  if not CreateProcess(nil,
    zAppName, // pointer to command line string
    nil, // pointer to process security attributes
    nil, // pointer to thread security attributes
    False, // handle inheritance flag
    CREATE_NEW_CONSOLE or // creation flags
    NORMAL_PRIORITY_CLASS,
    nil, //pointer to new environment block
    nil, // pointer to current directory name
    StartupInfo, // pointer to STARTUPINFO
    ProcessInfo) // pointer to PROCESS_INF
    then Result := WAIT_FAILED
  else
  begin
    if Wait then
    begin
      WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
      GetExitCodeProcess(ProcessInfo.hProcess, Result);
      CloseHandle(ProcessInfo.hProcess);
      CloseHandle(ProcessInfo.hThread);
      //VAR_APLICATION_TERMINATE := True; //desabilitado em 2022-03-31 - Impedindo de criar forms!
    end;
  end;
  if Saida then
  begin
    CloseHandle(Handle);
    if FileExists(ArqTemp) then
    begin
      AssignFile(Text, ArqTemp);
      // Está gerando erro I/O 32
      Reset(Text);
      while not Eof(Text) do
      begin
        Readln(Text, S);
        Memo.Lines.Add(S);
      end;
      CloseFile(Text);
    end;
  end;
end;

function TUnDmkProcFunc.XFT(Numero: String; Casas: Integer;
  Sinal: TSinal): String;
begin
  if Casas > 0 then
    if Pos(',', Numero) = 0 then
      Insert(',', Numero, Length(Numero) - Casas + 1);
  Result := Geral.TFT(Numero, Casas, Sinal);
end;

function TUnDmkProcFunc.XLSColCharToColNum(Coluna: String): Integer;
var
  Col: String;
  Index1, Index2: Integer;
begin
  Result := 0;
  Col := FCE(Coluna);
  if Col = '' then Exit;
  Index1 := Ord(Col[1]) - Ord('A') + 1;
  if Length(Col) > 1 then
  begin
    Index2 := Ord(Col[2]) - Ord('A') + 1;
    Result := (Index1 * 26) + Index2;
  end else Result := Index1;
end;

function TUnDmkProcFunc.XMLDecode(Data: String): String;
begin
  Result:=
    StringReplace(
    StringReplace(
    StringReplace(
      Data,
      '\-', '--',[rfReplaceAll]),
      '\"', '""',[rfReplaceAll]),
      '-- >', '-->',[rfReplaceAll]);
end;

function TUnDmkProcFunc.XMLEncode(Data: String): String;
begin
  Result:=
    StringReplace(
    StringReplace(
      Data,
      '--','\--',[rfReplaceAll]),
      '""','\""',[rfReplaceAll]);
end;

function TUnDmkProcFunc.XMLValBol(Noh: IXMLNode): Boolean;
begin
  if Noh.NodeValue = Null then
    Result := False
  else
    Result := Geral.IMV(Noh.NodeValue) = 1;
end;

function TUnDmkProcFunc.XMLValDta(Noh: IXMLNode): String;
begin
  if Noh.NodeValue = Null then
    Result := '0000/00/00'
  else
    Result := Geral.FDT(ValidaDataDeSQL(Noh.NodeValue, True), 1);
end;

function TUnDmkProcFunc.XMLValDth(Noh: IXMLNode): String;
var
  Txt, Dta, Hra: String;
begin
  if Noh.NodeValue = Null then
    Result := '0000/00/00 00:00:00'
  else
  begin
    Txt := Noh.NodeValue;
    if pos('/', Txt) = 3 then
      Dta := Copy(Txt, 07, 4) + '-' + Copy(Txt, 04, 2) + '-' + Copy(Txt, 01, 2)
    else
      Dta := Copy(Txt, 1, 10);
    Hra := Copy(Txt, 12, 8);
    Result := Geral.FDT(ValidaDataDeSQL(Dta, True), 1) + ' ' + Hra;
  end;
end;

function TUnDmkProcFunc.XMLValFlu(Noh: IXMLNode): Double;
var
  Txt: String;
begin
  if Noh.NodeValue = Null then
    Result := 0
  else begin
    Txt := Noh.NodeValue;
    Txt := Geral.Substitui(Txt, ',', '');
    Txt := Geral.Substitui(Txt, '.', ',');
    Result := Geral.DMV(Txt);
  end;
end;

function TUnDmkProcFunc.XMLValHra(Noh: IXMLNode): String;
begin
  if Noh.NodeValue = Null then
    Result := '00:00:00'
  else
    Result := Noh.NodeValue;
end;

function TUnDmkProcFunc.XMLValI64(Noh: IXMLNode): Int64;
begin
  if Noh.NodeValue = Null then
    Result := 0
  else
    Result := Geral.I64(Noh.NodeValue);
end;

function TUnDmkProcFunc.XMLValInt(Noh: IXMLNode): Integer;
begin
  if Noh.NodeValue = Null then
    Result := 0
  else
    Result := Geral.IMV(Noh.NodeValue);
end;

function TUnDmkProcFunc.XMLValTDt(Noh: IXMLNode): TDateTime;
begin
  if Noh.NodeValue = Null then
    Result := 0
  else
    Result := Geral.ValidaData8Num(Geral.SoNumero_TT(Noh.NodeValue), True, False);
end;

function TUnDmkProcFunc.XMLValTxt(Noh: IXMLNode): WideString;
begin
  if Noh.NodeValue = Null then
    Result := ''
  else
    //Result := Noh.NodeValue;
    Result := ParseText(Noh.NodeValue, True, True);
end;

function TUnDmkProcFunc.XMLValTZD(const Noh: IXMLNode; var TZD: Double): String;
var
  Txt, Dta, Hra, Sinal: String;
  p: Integer;
begin
  if Noh.NodeValue = Null then
    Result := '0000/00/00 00:00:00'
  else
  begin
    Txt := Noh.NodeValue;
//
    if Length(Txt) = 25 then
    begin
      Sinal := Txt[20];
      TZD := StrToTime(Copy(Txt, 21));
      if Sinal = '-' then
        TZD := -TZD;
    end else
    begin
      TZD := 0;
    end;
    //
    if pos('/', Txt) = 3 then
      Dta := Copy(Txt, 07, 4) + '-' + Copy(Txt, 04, 2) + '-' + Copy(Txt, 01, 2)
    else
      Dta := Copy(Txt, 1, 10);
    Hra := Copy(Txt, 12, 8);
    Result := Geral.FDT(ValidaDataDeSQL(Dta, True), 1) + ' ' + Hra;
  end;
end;

function TUnDmkProcFunc.XMLValZzz(Noh: IXMLNode): TTime;
var
  Txt: String;
begin
  Result := 0;
  if Noh.NodeValue = Null then
    Result := 0
  else
  begin
    Txt := Noh.NodeValue;
    Txt := Geral.Substitui(Txt, ',', '.');
    try
      Result := Geral.StrToTimeZZZ(Txt);
    except
      //
    end;
  end;
end;

{
How to print files programmatically using the ShellExecute function
 
The code sample below demonstrates how to print files programmatically on either a physical or a virtual printer using the ShellExecute function. It also demonstrates how to change the default system printer.
 
function GetDefaultPrinter(szPrinter:PAnsiChar; var bufferSize:DWORD):
BOOL; stdcall; external 'winspool.drv' name 'GetDefaultPrinterA';

function SetDefaultPrinter(szPrinter:PAnsiChar):
BOOL; stdcall; external 'winspool.drv' name 'SetDefaultPrinterA';

procedure PrintDocumentUsingShellExecute (szPrinter: String,  szDocumentPath: String)
    var
    szDefaultPrinter, szNamePrinterBuff: String;
    bufferSize: DWord;

    begin
    
    // get the default printer
    SetLength(szDefaultPrinter, MAX_PATH);
    bufferSize:= MAX_PATH;
    GetDefaultPrinter(Pchar(szDefaultPrinter), bufferSize);
    szDefaultPrinter:= String (PChar(szDefaultPrinter));

    //change the default printer
    if(szPrinter <> szDefPrinter)
    then SetDefaultPrinter(PChar(szPrinter));

    // send the document  to the print
    ShellExecute(0, 'print', PChar(szDocumentPath), nil, nil, SW_HIDE);  

    //set default printer back to original
    if(szPrinter <> szDefaultPrinter)
    then SetDefaultPrinter(PChar(szDefaultPrinter));

    end;

 
Then it's necessary to call these functions with the required parameters. For example, you can print MS Word and PDF documents this way:
 
PrintDocumentUsingShellExecute('Your Virtual Printer', 'c:\Documents\AnyDocument.doc')
PrintDocumentUsingShellExecute('Your Virtual Printer', 'c:\ Documents \AnyDocument.pdf')

}

{ TODO : IP Jangadas: 192.168.2.104 / 255.255.255.0 / 198.162.2.254 / 8.8.8.8 / 4.4.4.4}



end.
