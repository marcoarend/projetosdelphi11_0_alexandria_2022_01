unit UnMyVCLref;
interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, Variants,
  UnInternalConsts2, ComCtrls, Registry, TypInfo, dmkEdit, dmkEditCB, dmkGeral,
  dmkEditDateTimePicker, dmkDBLookupCombobox, dmkCheckBox, dmkRadioGroup,
  dmkDBEdit, dmkValUsu, dmkCheckGroup, dmkPopOutFntCBox, dmkMemo, dmkVariable,
  dmkCompoStore, dmkRichEdit, UnDmkProcFunc, UnDmkEnums, dmkImage, Grids,
  DBGrids, DmkDBGridZTO, DmkDBGrid;

type
  THackDBGrid = class(TDBGrid);
  TUnMyVCLRef = class(TObject)
  private
    { Private declarations }
    function  ObtemValorUpdTypeDeDmkComp(Objeto: TObject; var Val: Variant;
              var UpdTyp: TUpdType): Boolean;
  public
    { Public declarations }

    function  SET_Label(Form: TForm; Nome, Propriedade: String; Valor: Variant; DeveExistir: Boolean = False): Boolean;
    function  SET_Edit(Form: TForm; Nome, Propriedade: String; Valor: Variant; DeveExistir: Boolean = False): Boolean;
    function  SET_dmkEdit(Form: TForm; Nome, Propriedade: String; Valor: Variant; DeveExistir: Boolean = False): Boolean;
    function  SET_dmkEditCB(Form: TForm; Nome, Propriedade: String; Valor: Variant; DeveExistir: Boolean = False): Boolean;
    function  SET_dmkEditDateTimePicker(Form: TForm; Nome, Propriedade: String; Valor: Variant; DeveExistir: Boolean = False): Boolean;
    function  SET_dmkDBLookupComboBox(Form: TForm; Nome, Propriedade: String; Valor: Variant; DeveExistir: Boolean = False): Boolean;
    function  GET_dmkEdit(Form: TForm; Nome, Propriedade: String): Variant;
    function  SET_dmkMemo(Form: TForm; Nome, Propriedade: String; Valor: Variant; DeveExistir: Boolean = False): Boolean;
    function  SET_dmkVariable(Form: TForm; Nome, Propriedade: String; Valor: Variant; DeveExistir: Boolean = False): Boolean;
    function  SET_dmkCompoStore(Form: TForm; Nome: String; Component: TComponent; DeveExistir: Boolean = False): Boolean;
    function  SET_Component(Form: TForm; Nome, Propriedade: String;
              Valor: Variant; Classe: TClass): Boolean;
    function  SET_Object(Form: TForm; Objeto: TObject; Propriedade: String;
              Valor: Variant): Boolean;
    //
    function  GET_Compo_Val(const Form: TForm; const Nome, Propriedade: String;
              const Classe: TClass; var Valor: Variant): Boolean;
    function  GET_Compo_Val_Objeto(const Objeto: TObject; const Propriedade:
              String; AvisaNaoExist: Boolean; var Valor: Variant): Boolean;
    function  GET_InteiroDeObjeto(var Inteiro: Integer): Boolean;
    function  GET_ValorDeObjeto_Inteiro(): Integer;
    function  GET_ValorDeObjeto_Double(): Double;
    //
    function  GET_CVO_VPAC(const Propriedade: String; const AvisaNaoExist:
              Boolean; var Valor: Variant): Boolean;
    function  GET_CVO_VPAC_Txt(var Valor: Variant): Boolean;
    //
    procedure AvisaInexistencia(DeveExistir: Boolean; Nome, Propriedade: String;
              Valor: Variant);
    function  CriaSQLdmkForm(Form: TForm; Acao: TSQLType; Tabela: String;
              NewItem: Variant; UsarSetOfValues: Boolean = False): String;
    function  ObtemChaveEValordmkPanel(Form: TForm; WinControl: TWinControl): TStringList;
    function  CriaSQLdmkPanel_Lins(Form: TForm; WinControl: TWinControl; Acao: TSQLType;
              Tabela: String; NewItem: Variant; UserDataAlterweb: Boolean): String;
    function  CriaSQLdmkPanel_Sets(Form: TForm; Panel: TPanel; Acao: TSQLType;
              Tabela: String; NewItem: Variant; UserDataAlterweb: Boolean): String;
    function  CriaForm_AcessoTotal(InstanceClass: TComponentClass; var Reference): Boolean;
    function  CalculaPercentual(const InstanceClass: TComponentClass; var Reference;
              const FormaCalcPercent: TFormaCalcPercent;
              const CasasValI, CasasValF, CasasPerc: Integer;
              var ValorValI, ValorValF, ValorPerc: Double): Boolean;
    function  SetaValorDePropEmWinCtrl(WinCtrl: TWinControl; Propriedade: String;
              Valor: Variant): Boolean;
    function  NomeMuitoGrande(var Nome: String; const MaxTam: Integer;
              const WinCtrl: TWinControl): Boolean;
  end;

var
  MyVCLref: TUnMyVCLref;

implementation

uses
//{$IFNDEF NO_USE_MYSQLMODULE} MeuDBUses, {$ENDIF}
{$IFNDEF NAO_USA_DB} MeuDBUses,  {$ENDIF}
UnMyObjects;

function TUnMyVCLref.SET_Label(Form: TForm; Nome, Propriedade: String;
Valor: Variant; DeveExistir: Boolean = False): Boolean;
var
  MyLabel: TLabel;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as TLabel) <> nil then
    MyLabel := Form.FindComponent(Nome) as TLabel
  else MyLabel := nil;
  if MyLabel <> nil then
  begin
    PropInfo := GetPropInfo(MyLabel.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        SetPropValue(MyLabel, Propriedade, Valor);
        Result := True;
      except
        EAbort.Create('A propriedade n�o pode ser setada!');
      end;
    end;
  end else AvisaInexistencia(DeveExistir, Nome, Propriedade, Valor);
end;

function TUnMyVCLRef.SET_Object(Form: TForm; Objeto: TObject;
  Propriedade: String; Valor: Variant): Boolean;
var
  PropInfo: PPropInfo;
begin
  Result := False;
  if Objeto <> nil then
  begin
    PropInfo := GetPropInfo(Objeto.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        SetPropValue(Objeto, Propriedade, Valor);
        Result := True;
      except
        Geral.MB_Erro('A propriedade ' + Propriedade +
        ' n�o pode ser setada no componente ' + TComponent(Objeto).Name + '.' +
        sLineBreak + 'AVISE A DERMATEK!');
      end;
    end else
      Geral.MB_Erro('O componente ' + TComponent(Objeto).Name +
      ' n�o possui a propriedade ' + Propriedade + '.' + sLineBreak +
      'AVISE A DERMATEK!');
  end;
end;

function TUnMyVCLref.SET_Edit(Form: TForm; Nome, Propriedade: String;
Valor: Variant; DeveExistir: Boolean = False): Boolean;
var
  MyEdit: TEdit;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as TEdit) <> nil then
    MyEdit := Form.FindComponent(Nome) as TEdit
  else MyEdit := nil;
  if MyEdit <> nil then
  begin
    PropInfo := GetPropInfo(MyEdit.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        SetPropValue(MyEdit, Propriedade, Valor);
        Result := True;
      except
        EAbort.Create('A propriedade n�o pode ser setada!');
      end;
    end;
  end else AvisaInexistencia(DeveExistir, Nome, Propriedade, Valor);
end;

function TUnMyVCLref.SET_dmkEdit(Form: TForm; Nome, Propriedade: String;
Valor: Variant; DeveExistir: Boolean = False): Boolean;
var
  MydmkEdit: TdmkEdit;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as TdmkEdit) <> nil then
    MydmkEdit := Form.FindComponent(Nome) as TdmkEdit
  else MydmkEdit := nil;
  if MydmkEdit <> nil then
  begin
    PropInfo := GetPropInfo(MydmkEdit.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        SetPropValue(MydmkEdit, Propriedade, Valor);
        Result := True;
      except
        EAbort.Create('A propriedade n�o pode ser setada!');
      end;
    end;
  end else AvisaInexistencia(DeveExistir, Nome, Propriedade, Valor);
end;

function TUnMyVCLref.SET_dmkEditCB(Form: TForm; Nome, Propriedade: String;
Valor: Variant; DeveExistir: Boolean = False): Boolean;
var
  MydmkEditCB: TdmkEditCB;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as TdmkEditCB) <> nil then
    MydmkEditCB := Form.FindComponent(Nome) as TdmkEditCB
  else MydmkEditCB := nil;
  if MydmkEditCB <> nil then
  begin
    PropInfo := GetPropInfo(MydmkEditCB.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        SetPropValue(MydmkEditCB, Propriedade, Valor);
        Result := True;
      except
        EAbort.Create('A propriedade n�o pode ser setada!');
      end;
    end;
  end else AvisaInexistencia(DeveExistir, Nome, Propriedade, Valor);
end;

function TUnMyVCLRef.SET_dmkCompoStore(Form: TForm; Nome: String; Component: TComponent;
  DeveExistir: Boolean): Boolean;
var
  MydmkCompoStore: TdmkCompoStore;
  //PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as TdmkCompoStore) <> nil then
    MydmkCompoStore := Form.FindComponent(Nome) as TdmkCompoStore
  else MydmkCompoStore := nil;
  if MydmkCompoStore <> nil then
  begin
    MydmkCompoStore.Component := Component; 
  end else AvisaInexistencia(DeveExistir, Nome, 'Component', TComponent(Component).Name);
end;

function TUnMyVCLref.SET_dmkDBLookupComboBox(Form: TForm; Nome, Propriedade: String;
Valor: Variant; DeveExistir: Boolean = False): Boolean;
var
  MydmkDBLookupComboBox: TdmkDBLookupComboBox;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as TdmkDBLookupComboBox) <> nil then
    MydmkDBLookupComboBox := Form.FindComponent(Nome) as TdmkDBLookupComboBox
  else MydmkDBLookupComboBox := nil;
  if MydmkDBLookupComboBox <> nil then
  begin
    PropInfo := GetPropInfo(MydmkDBLookupComboBox.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        SetPropValue(MydmkDBLookupComboBox, Propriedade, Valor);
        Result := True;
      except
        EAbort.Create('A propriedade n�o pode ser setada!');
      end;
    end;
  end else AvisaInexistencia(DeveExistir, Nome, Propriedade, Valor);
end;

function TUnMyVCLref.SET_dmkEditDateTimePicker(Form: TForm; Nome,
Propriedade: String; Valor: Variant; DeveExistir: Boolean = False): Boolean;
var
  MydmkEditDateTimePicker: TdmkEditDateTimePicker;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as TdmkEditDateTimePicker) <> nil then
    MydmkEditDateTimePicker := Form.FindComponent(Nome) as TdmkEditDateTimePicker
  else MydmkEditDateTimePicker := nil;
  if MydmkEditDateTimePicker <> nil then
  begin
    PropInfo := GetPropInfo(MydmkEditDateTimePicker.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        SetPropValue(MydmkEditDateTimePicker, Propriedade, Valor);
        Result := True;
      except
        EAbort.Create('A propriedade n�o pode ser setada!');
      end;
    end;
  end else AvisaInexistencia(DeveExistir, Nome, Propriedade, Valor);
end;


function TUnMyVCLref.SET_dmkMemo(Form: TForm; Nome, Propriedade: String; Valor:
Variant; DeveExistir: Boolean = False): Boolean;
var
  MydmkMemo: TdmkMemo;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as TdmkMemo) <> nil then
    MydmkMemo := Form.FindComponent(Nome) as TdmkMemo
  else MydmkMemo := nil;
  if MydmkMemo <> nil then
  begin
    if Lowercase(Propriedade) = 'lines.text' then
    begin
      MydmkMemo.Lines.Text := Valor;
      Result := True;
    end
    else
    begin
      PropInfo := GetPropInfo(MydmkMemo.ClassInfo, Propriedade);
      if PropInfo <> nil then
      begin
        try
          SetPropValue(MydmkMemo, Propriedade, Valor);
          Result := True;
        except
          EAbort.Create('A propriedade n�o pode ser setada!');
        end;
      end;
    end;
  end else AvisaInexistencia(DeveExistir, Nome, Propriedade, Valor);
end;

function TUnMyVCLRef.SET_dmkVariable(Form: TForm; Nome, Propriedade: String;
  Valor: Variant; DeveExistir: Boolean = False): Boolean;
var
  MydmkVariable: TdmkVariable;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as TdmkVariable) <> nil then
    MydmkVariable := Form.FindComponent(Nome) as TdmkVariable
  else MydmkVariable := nil;
  if MydmkVariable <> nil then
  begin
    PropInfo := GetPropInfo(MydmkVariable.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        SetPropValue(MydmkVariable, Propriedade, Valor);
        Result := True;
      except
        EAbort.Create('A propriedade n�o pode ser setada!');
      end;
    end;
  end else AvisaInexistencia(DeveExistir, Nome, Propriedade, Valor);
end;

////////////////////////  G  E  T  ////////////////////////


function TUnMyVCLRef.GET_Compo_Val(const Form: TForm; const Nome,
  Propriedade: String; const Classe: TClass; var Valor: Variant): Boolean;
var
  Objeto: TObject;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as Classe) <> nil then
    Objeto := Form.FindComponent(Nome) as Classe
  else begin
    Geral.MB_Erro('O componente ' + Nome +
    ' n�o existe ou n�o pode ser setado!' + sLineBreak + 'AVISE A DERMATEK!');
    Exit;
  end;
  if Objeto <> nil then
  begin
    PropInfo := GetPropInfo(Objeto.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        Valor := GetPropValue(Objeto, Propriedade, Valor);
        Result := True;
      except
        Geral.MB_Erro('A propriedade ' + Propriedade +
        ' n�o pode ser setada no componente ' + TComponent(Objeto).Name + '.' +
        sLineBreak + 'AVISE A DERMATEK!');
      end;
    end else
      Geral.MB_Erro('O componente ' + TComponent(Objeto).Name +
      ' n�o possui a propriedade ' + Propriedade + '.' + sLineBreak +
      'AVISE A DERMATEK!');
  end;
end;

function TUnMyVCLRef.GET_Compo_Val_Objeto(const Objeto: TObject;
  const Propriedade: String; AvisaNaoExist: Boolean; var Valor: Variant): Boolean;
  procedure GetPropertyValues(AObj: TObject; AValues: TStrings);
  var
    count:    integer;
    data:     PTypeData;
    default:  string;
    i:        integer;
    info:     PTypeInfo;
    propList: PPropList;
    propInfo: PPropInfo;
    propName: string;
    value:    variant;
    //
  begin
    info := AObj.ClassInfo;
    data := GetTypeData(info);
    GetMem(propList, data^.PropCount * SizeOf(PPropInfo));
    try
      count := GetPropList(info, tkAny,  propList);
      for i := 0 to count - 1 do
      begin
        propName := propList^[i]^.Name;
        propInfo := GetPropInfo(info, propName);
        if propInfo <> nil then begin
          case propInfo^.PropType^.Kind of
            tkClass,
            tkMethod:      value := '$' + IntToHex(GetOrdProp(AObj, propInfo), 8);
            tkFloat:       value := GetFloatProp(AObj, propInfo);
            tkInteger:     value := GetOrdProp(AObj, propInfo);
            tkString,
            tkLString,
            tkWString:     value := GetStrProp(AObj, propInfo);
            tkEnumeration: value := GetEnumProp(AObj, propInfo);
            else           value := '???';
          end;
          if propInfo.default = longint($80000000) then
            default := 'none'
          else
            default := IntToStr(propInfo.default);
          AValues.Add(Format('%s: %s [default: %s]',
                             [propName, value, default]));
          // $80000000 apparently indicates "no default".
        end;
      end;
    finally
      FreeMem(propList, data^.PropCount * SizeOf(PPropInfo));
    end;
  end;
var
  PropInfo: PPropInfo;
  MyProp: String;
  ListaProp: TStringList;
  //
  procedure Outros();
  begin
    PropInfo := GetPropInfo(Objeto.ClassInfo, MyProp);
    if PropInfo <> nil then
    begin
      try
        Valor := GetPropValue(Objeto, MyProp, Valor);
        Result := True;
      except
        Geral.MB_Erro('A propriedade ' + MyProp +
        ' existe, mas n�o pode ser obtida do componente ' +
        TComponent(Objeto).Name + '.' + sLineBreak + 'AVISE A DERMATEK!');
      end;
    end
    else
    begin
      if AvisaNaoExist then
        Geral.MB_Erro('O componente ' + TComponent(Objeto).Name +
        ' n�o possui a propriedade ' + MyProp + '.' + sLineBreak +
        'AVISE A DERMATEK!')
      else
      begin
        ListaProp := TStringList.Create;
        try
          GetPropertyValues(Objeto, ListaProp);
          Geral.MB_Info(ListaProp.Text);
        finally
          ListaProp.Free;
        end;
      end;
    end;
  end;
begin
  MyProp := Propriedade;
  Result := False;
  if Objeto <> nil then
  begin
    if (Objeto is TDBEdit) and (Lowercase(MyProp) = 'text') then
    begin
      Valor  := TDBEdit(Objeto).Text;
      Result := True;
    end else
    if (Objeto is TDBGrid)
    or (Objeto is TDmkDBGridZTO)
    or (Objeto is TDmkDBGrid) then
    begin
      Valor := THackDBGrid(Objeto).GetEditText(
        THackDBGrid(Objeto).Col, THackDBGrid(Objeto).Row);
      Result := True;
    end else
    if (
      (Objeto is TdmkEdit) or (Objeto is TdmkEditCB)
    )
    and
    (
      (Lowercase(MyProp) = 'text') or (Lowercase(MyProp) = 'texto')
    ) then
    begin
      Valor := TdmkEdit(Objeto).ValueVariant;
      Result := True;
    end else
      Outros;
  end;
end;

function TUnMyVCLRef.GET_CVO_VPAC(const Propriedade: String;
  const AvisaNaoExist: Boolean; var Valor: Variant): Boolean;
begin
  Result := GET_Compo_Val_Objeto(VAR_POPUP_ACTIVE_CONTROL, Propriedade,
    AvisaNaoExist, Valor);
end;

function TUnMyVCLRef.GET_CVO_VPAC_Txt(var Valor: Variant): Boolean;
begin
  Result := GET_CVO_VPAC('Text', False, Valor)
  or GET_CVO_VPAC('Texto', True, Valor);
end;

function TUnMyVCLref.GET_dmkEdit(Form: TForm; Nome, Propriedade: String): Variant;
var
  MydmkEdit: TdmkEdit;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as TdmkEdit) <> nil then
    MydmkEdit := Form.FindComponent(Nome) as TdmkEdit
  else MydmkEdit := nil;
  if MydmkEdit <> nil then
  begin
    PropInfo := GetPropInfo(MydmkEdit.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        Result := GetPropValue(MydmkEdit, Propriedade);
      except
        EAbort.Create('A propriedade n�o pode ser setada!');
      end;
    end;
  end;
end;

function TUnMyVCLRef.GET_InteiroDeObjeto(var Inteiro: Integer): Boolean;
var
  Valor: Variant;
begin
  Result  := False;
  Inteiro := 0;
  if VAR_POPUP_ACTIVE_CONTROL <> nil then
  begin
    Result := GET_CVO_VPAC_Txt(Valor);
    //
    if Valor <> Null then
      Inteiro := Valor;
  end;
end;

function TUnMyVCLRef.GET_ValorDeObjeto_Double(): Double;
var
  Texto: String;
  Valor: Variant;
begin
  Valor := 0;
  if VAR_POPUP_ACTIVE_CONTROL <> nil then
  begin
    GET_CVO_VPAC_Txt(Valor);
    if Valor <> Null then
    begin
      //Texto := String(Valor);
      //Texto := Geral.Substitui(Texto, '.', '');
      //Texto := Geral.Substitui(Texto, ',', '.');
      Result := Geral.DMV(Geral.SoNumeroESinalEVirgula_TT(String(Valor)));
    end;
  end;
end;

function TUnMyVCLRef.GET_ValorDeObjeto_Inteiro(): Integer;
var
  Valor: Variant;
begin
  Valor := 0;
  //if ActiveControl <> nil then
  if VAR_POPUP_ACTIVE_CONTROL <> nil then
  begin
    GET_CVO_VPAC_Txt(Valor);
    //
    Result := Geral.IMV(Geral.SoNumero_TT(String(Valor)))
  end;
end;

function TUnMyVCLRef.NomeMuitoGrande(var Nome: String; const MaxTam: Integer;
  const WinCtrl: TWinControl): Boolean;
var
  Novo: String;
begin
  Result := Length(Nome) > MaxTam;
  if Result then
  begin
    Novo := Copy(Nome, 1, MaxTam);
    if Geral.MB_Pergunta('A descri��o: ' + sLineBreak + Nome + slineBreak +
    ' exceder o m�ximo de ' + Geral.FF0(MaxTam) + ' caracteres!' + sLineBreak +
    'Deseja alterar (diminuir) a descri��o para ter o tamanho m�ximo? ' +
    sLineBreak + Novo) = ID_Yes then
    begin
      Nome := Novo;
      if not SetaValorDePropEmWinCtrl(WinCtrl, 'Text', Novo) then
      if not SetaValorDePropEmWinCtrl(WinCtrl, 'Texto', Novo) then
      if not SetaValorDePropEmWinCtrl(WinCtrl, 'Caption', Novo) then
    end;
  end;
end;

{
function TUnMyVCLref.SET_Label(Form: TForm; Nome, Propriedade: String;
Valor: Variant): Boolean;
var
  MyLabel: TLabel;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as TLabel) <> nil then
    MyLabel := Form.FindComponent(Nome) as TLabel
  else MyLabel := nil;
  if MyLabel <> nil then
  begin
    PropInfo := GetPropInfo(MyLabel.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        SetPropValue(MyLabel, Propriedade, Valor);
        Result := True;
      except
        EAbort.Create('A propriedade n�o pode ser setada!');
      end;
    end;
  end;
end;
}

function TUnMyVCLRef.SetaValorDePropEmWinCtrl(WinCtrl: TWinControl;
  Propriedade: String; Valor: Variant): Boolean;
var
  PropInfo: PPropInfo;
begin
  Result := False;
  PropInfo := GetPropInfo(WinCtrl.ClassInfo, Propriedade);
  if PropInfo <> nil then
  begin
    try
      SetPropValue(WinCtrl, Propriedade, Valor);
      Result := True;
    except
      EAbort.Create('O novo valor n�o pode ser definido no componente!');
    end;
  end;
end;

function TUnMyVCLref.SET_Component(Form: TForm; Nome, Propriedade: String;
Valor: Variant; Classe: TClass): Boolean;
var
  Objeto: TObject;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as Classe) <> nil then
    Objeto := Form.FindComponent(Nome) as Classe
  else begin
    Geral.MB_Erro('O componente ' + Nome +
    ' n�o existe ou n�o pode ser setado!' + sLineBreak + 'AVISE A DERMATEK!');
    Exit;
  end;
  if Objeto <> nil then
  begin
    PropInfo := GetPropInfo(Objeto.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        SetPropValue(Objeto, Propriedade, Valor);
        Result := True;
      except
        Geral.MB_Erro('A propriedade ' + Propriedade +
        ' n�o pode ser setada no componente ' + TComponent(Objeto).Name + '.' +
        sLineBreak + 'AVISE A DERMATEK!');
      end;
    end else
      Geral.MB_Erro('O componente ' + TComponent(Objeto).Name +
      ' n�o possui a propriedade ' + Propriedade + '.' + sLineBreak +
      'AVISE A DERMATEK!');
  end;
end;

procedure TUnMyVCLRef.AvisaInexistencia(DeveExistir: Boolean; Nome,
  Propriedade: String; Valor: Variant);
//var
  //ValTxt: String;
begin
  if DeveExistir then
  begin
    //ValTxt := Geral.VariantToString(Valor);
    // pode ser uma senha!
    EAbort.Create('A propriedade "' + Propriedade +
    '" n�o pode ser setada no componente "' + Nome +
    '" pois ele n�o foi localizado!');//, 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

function TUnMyVCLRef.CalculaPercentual(const InstanceClass: TComponentClass;
  var Reference; const FormaCalcPercent: TFormaCalcPercent;
  const CasasValI, CasasValF, CasasPerc: Integer;
  var ValorValI, ValorValF, ValorPerc: Double): Boolean;
var
  MyLabel: TLabel;
  v: Variant;
begin
  Result := False;
  if CriaForm_AcessoTotal(InstanceClass, Reference) then
  begin
    with TForm(Reference) do
    begin
      SET_Component(TForm(Reference), 'RGForma', 'ItemIndex', Integer(FormaCalcPercent), TRadioGroup);
      //
      SET_Component(TForm(Reference), 'EdValorIni', 'DecimalSize', CasasValI, TdmkEdit);
      SET_Component(TForm(Reference), 'EdValorIni', 'ValueVariant', ValorValI, TdmkEdit);
      //
      SET_Component(TForm(Reference), 'EdValorFim', 'DecimalSize', CasasValF, TdmkEdit);
      SET_Component(TForm(Reference), 'EdValorFim', 'ValueVariant', ValorValF, TdmkEdit);
      //
      SET_Component(TForm(Reference), 'EdPorcento', 'DecimalSize', CasasPerc, TdmkEdit);
      SET_Component(TForm(Reference), 'EdPorcento', 'ValueVariant', ValorPerc, TdmkEdit);
      //
    end;
    TForm(Reference).ShowModal;
    Result := TForm(Reference).ModalResult = mrOk;
    if Result then
    begin
      if GET_Compo_Val(TForm(Reference), 'EdValorIni', 'ValueVariant', TdmkEdit, v) then
        ValorValI := v;
      if GET_Compo_Val(TForm(Reference), 'EdValorFim', 'ValueVariant', TdmkEdit, v) then
        ValorValF := v;
      if GET_Compo_Val(TForm(Reference), 'EdPorcento', 'ValueVariant', TdmkEdit, v) then
        ValorPerc := v;
    end;
    TForm(Reference).Destroy;
  end;
end;

function TUnMyVCLRef.CriaForm_AcessoTotal(InstanceClass: TComponentClass;
  var Reference): Boolean;
var
  Instance: TComponent;
  Form: String;
  MyCursor: TCursor;
  Campo: String;
begin
  Result := False;
  Form := InstanceClass.ClassName;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(Application);
      Result := True;
    except
      TComponent(Reference) := nil;
      raise;
    end;
    with TForm(Reference) do
    begin
      Campo := 'ImgTipo';
      if (FindComponent(Campo) as TdmkImage) <> nil then
        TdmkImage(FindComponent(Campo) as TImage).SQLType := stLok;
    end;
    Screen.Cursor := MyCursor;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

function TUnMyVCLref.CriaSQLdmkForm(Form: TForm; Acao: TSQLType; Tabela: String;
  NewItem: Variant; UsarSetOfValues: Boolean): String;
const
  UpdCampo = 'UpdCampo';
  //Teste = ' ';
var
  Objeto: TObject;
  PIUpdType, PIUpdCamp: PPropInfo;
  i: Integer;
  Campo, Texto, Liga1, CamposSet, CamposVal, CamposWhere, LigaSet, LigaWhere: String;
  Valor: Variant;
  UpdType: UnDmkEnums.TUpdType;
  //
  SQL, SetIni, SetMid, SetEnd, Msg: String;
  Setar: Boolean;
begin
  SQL := ' ';
  CamposSet   := '';
  CamposVal   := '';
  CamposWhere := '';
  LigaSet := '';
  Setar := UsarSetOfValues and (Acao = stIns);
  if Setar then
  begin
    SetIni := ' (';
    SetMid := ') VALUES (';
    SetEnd := ');';
  end else begin
    SetIni := ' SET ';
    SetMid := '';
    SetEnd := ';';
  end;
  if Acao = stUpd then
    LigaWhere := sLineBreak + ' WHERE '
  else
    LigaWhere := '';
  //SQL := ' ';
  case Acao of
    // 2011-06-25
    //stIns: SQL := 'INSERT INTO ' + lowercase(Tabela) + ' SET ';
    stIns: SQL := SQL + 'INSERT INTO ' + lowercase(Tabela) + SetIni;
    //stUpd: SQL := 'UPDATE ' + lowercase(Tabela) + ' SET ';
    stUpd: SQL := SQL + 'UPDATE ' + lowercase(Tabela) + SetIni;
    stDel: SQL := SQL + 'DELETE ';
  end;
  //n := 0;
  for i := 0 to Form.ComponentCount -1 do
  begin
    Objeto := Form.Components[i];
    PIUpdCamp := GetPropInfo(Objeto.ClassInfo, UpdCampo);
    PIUpdType := GetPropInfo(Objeto.ClassInfo, 'UpdType');
    if (PIUpdCamp <> nil) and (PIUpdType <> nil) then
    begin
      //try
        Campo := GetPropValue(Objeto, UpdCampo);
        UpdType := utNil;

        if Campo <> '' then
        begin
          if (Objeto is TdmkEdit) then
          begin
            if TdmkEdit(Objeto).FormatType = dmktfMesAno then
            begin
              if TdmkEdit(Objeto).ValueVariant = Null then
                Valor := 0
              else
                //Valor := Geral.ValidaMesAno(TdmkEdit(Objeto).ValueVariant)
                Valor := Geral.DataToMez(TdmkEdit(Objeto).ValueVariant)
            end else
            if TdmkEdit(Objeto).FormatType = dmktf_AAAAMM then
            begin
              if TdmkEdit(Objeto).ValueVariant = Null then
                Valor := 0
              else
                Valor := Geral.FDT(TdmkEdit(Objeto).ValueVariant, 20)
            end else
            if TdmkEdit(Objeto).FormatType = dmktfTime then
            begin
              if TdmkEdit(Objeto).ValueVariant = Null then
                Valor := 0
              else
              begin
                case TdmkEdit(Objeto).HoraFormat of
                  dmkhfLong: Valor := FormatDateTime('hh:nn:ss', TdmkEdit(Objeto).ValueVariant);
                  dmkhfShort: Valor := FormatDateTime('hh:nn', TdmkEdit(Objeto).ValueVariant);
                  dmkhfMiliSeconds: Valor := (*FormatDateTime('hh:nn:ss,zzz',*) TdmkEdit(Objeto).ValueVariant;
                  //  Guardar como double no MySQL?
                  dmkhfExtended: Valor := (*FormatDateTime('hh:nn:ss,zzz',*) TdmkEdit(Objeto).ValueVariant;
                  dmkhfTZD_UTC: Valor := (*FormatDateTime('hh:nn:ss,zzz',*) TdmkEdit(Objeto).ValueVariant;
                  else Valor := '00:00';
                end;
              end;
            end else
            if TdmkEdit(Objeto).MskType <> fmtNone then
            begin
              if TdmkEdit(Objeto).MskType in ([fmtTelCurto,fmtTelLongo]) then
              begin
                if Geral.PareceTelefoneBR(TdmkEdit(Objeto).ValueVariant) then
                  Valor := Geral.SoNumero_TT(TdmkEdit(Objeto).ValueVariant)
                else
                  Valor := Trim(TdmkEdit(Objeto).ValueVariant);
              end else
              begin
                Valor := Geral.SoNumeroELetra_TT(TdmkEdit(Objeto).ValueVariant);
                if TdmkEdit(Objeto).MskType = fmtUF then
                  Valor := Geral.GetCodigoUF_da_SiglaUF(Valor);
              end;
            end else
              Valor := TdmkEdit(Objeto).ValueVariant;
            UpdType := TdmkEdit(Objeto).UpdType;
          end
          //
          else if (Objeto is TdmkDBLookupCombobox) then
          begin
            Valor := TdmkDBLookupCombobox(Objeto).KeyValue;
            UpdType := TdmkDBLookupComboBox(Objeto).UpdType;
          end
          //
          else if (Objeto is TdmkEditCB) then
          begin
            Valor := TdmkEditCB(Objeto).ValueVariant;
            UpdType := TdmkEditCB(Objeto).UpdType;
          end
          //
          else if (Objeto is TdmkEditDateTimePicker) then
          begin
            //VarType(Valor) := varDate;
            case TdmkEditDateTimePicker(Objeto).Kind of
              dtkDate: Valor := FormatDateTime('yyyy-mm-dd', TdmkEditDateTimePicker(Objeto).Date);
              dtkTime: Valor := FormatDateTime('hh:nn:ss', TdmkEditDateTimePicker(Objeto).Time);
            end;
            UpdType := TdmkEditDateTimePicker(Objeto).UpdType;
          end
          //
          else if (Objeto is TdmkCheckBox) then
          begin
            //Mudado em: 2018-07-12 => Motivo evitar erros para o caso de Char
            //Valor := TdmkCheckBox(Objeto).Checked;
            if (TdmkCheckBox(Objeto).ValCheck = #0) and
              (TdmkCheckBox(Objeto).ValUnCheck = #0) then
            begin
              Valor := TdmkCheckBox(Objeto).Checked;
            end else
            if (TdmkCheckBox(Objeto).ValCheck <> #0) and
              (TdmkCheckBox(Objeto).ValUnCheck <> #0) then
            begin
              if TdmkCheckBox(Objeto).Checked then
                Valor := TdmkCheckBox(Objeto).ValCheck
              else
                Valor := TdmkCheckBox(Objeto).ValUnCheck;
            end else
            begin
              Msg := 'Componente "' + TdmkCheckBox(Objeto).Name +
                '" com "ValCheck" ou "ValUnCheck" indefinido!';
              //
              Geral.MB_Erro(Msg);
              raise Exception.Create(Msg);
            end;
            //
            UpdType := TdmkCheckBox(Objeto).UpdType;
          end
          //
          else if (Objeto is TdmkCheckGroup) then
          begin
            Valor := TdmkCheckGroup(Objeto).Value;
            UpdType := TdmkCheckGroup(Objeto).UpdType;
          end
          //
          else if (Objeto is TdmkRadioGroup) then
          begin
            Valor := TdmkRadioGroup(Objeto).ItemIndex;
            UpdType := TdmkRadioGroup(Objeto).UpdType;
          end
          //
          else if (Objeto is TdmkMemo) then
          begin
            Valor := TdmkMemo(Objeto).Text;
            UpdType := TdmkMemo(Objeto).UpdType;
          end
          //
//{$IFNDEF NO_USE_MYSQLMODULE}
{$IFNDEF NAO_USA_DB}
            else if (Objeto is TdmkDBEdit) then
            FmMeuDBUses.ObtemValorDe_dmkDBEdit(Objeto, Valor, UpdType)
{$ENDIF}
          //
          else if (Objeto is TdmkPopOutFntCBox) then
          begin
            Valor := TdmkPopOutFntCBox(Objeto).FonteNome;
            UpdType := TdmkPopOutFntCBox(Objeto).UpdType;
          end
          else if (Objeto is TdmkRichEdit) then
          begin
            Valor := MyObjects.ObtemTextoRichEdit(
            TForm(TdmkRichEdit(Objeto).Parent),
            TdmkRichEdit(Objeto));
            UpdType := TdmkRichEdit(Objeto).UpdType;
          end
          //
          else if (Objeto is TdmkValUsu) then
          begin
            Valor := TdmkValUsu(Objeto).ValueVariant;
            UpdType := TdmkValUsu(Objeto).UpdType;
          end
          //
          else
          begin
            //Incluir := False;
            Geral.MB_Erro('O componente ' + TComponent(
            Objeto).Name + ' n�o possui a propriedade ' + UpdCampo +
            '.' + sLineBreak + 'AVISE A DERMATEK!');
          end;
          if (UpdType <> utNil) then
          begin
            if (Acao = stIns) then
            begin
              if (UpdType = utInc) then
                Valor := NewItem;
            end;
            Texto := Geral.VariavelToString(Valor);
            Liga1 := LigaSet;
            if Setar then
            begin
              CamposSet := CamposSet + Liga1 + Campo;
              CamposVal := CamposVal + Liga1 + Texto;
            end else begin
              CamposSet := CamposSet + Liga1 + Campo + '=' + Texto;
            end;
            LigaSet := ', ' + sLineBreak;
            if (UpdType in ([utInc, utIdx])) and (Acao in ([stUpd, stDel])) then
            begin
              if (Acao = stUpd) then
              begin
                if (UpdType in ([utInc, utIdx])) then
                begin
                  if (Objeto is TdmkEditDateTimePicker) then
                  begin
                    case TdmkEditDateTimePicker(Objeto).Kind of
                      dtkDate: Valor := FormatDateTime('yyyy-mm-dd', GetPropValue(TObject(Objeto), 'OldValor'));
                      dtkTime: Valor := FormatDateTime('hh:nn:ss', GetPropValue(TObject(Objeto), 'OldValor'));
                    end;
                    //UpdType := TdmkEditDateTimePicker(Objeto).UpdType;
                  end else
                    Valor := GetPropValue(TObject(Objeto), 'OldValor');
                  //ver
                  Texto := Geral.VariavelToString(Valor);
                end;
              end;
              CamposWhere := CamposWhere + ' ' +LigaWhere + sLineBreak + Campo + '=' + Texto;
              LigaWhere := sLineBreak + ' AND ';
            end;
          end;
        end;
      //except
        //  ???
        //raise;
      //end;
    end;
  end;
  if Setar then
    SQL := SQL + CamposSet + SetMid + CamposVal + SetEnd + CamposWhere
  else
    SQL := SQL + CamposSet + CamposWhere;
  //if Length(SQL) > 0 then SQL := Copy(SQL, 2, Length(SQL)-1);
  //
  Result := SQL;
end;

{$WARNINGS OFF}

function TUnMyVCLref.ObtemChaveEValordmkPanel(Form: TForm;
  WinControl: TWinControl): TStringlist;
const
  UpdCampo = 'UpdCampo';
var
  I: Integer;
  Objeto: TObject;
  c: TComponent;
  IsOk: Boolean;
  PIUpdType, PIUpdCamp: PPropInfo;
  UpdType: TUpdType;
  Valor: Variant;
  Campo, Texto: String;
begin
  Result := TStringList.Create;
  try
    for I := 0 to Form.ComponentCount -1 do
    begin
      Objeto := Form.Components[i];
      if (Objeto is TdmkValUsu) then
      begin
        IsOK := True;
      end else begin
        IsOK := False;
        //
        c := TComponent(Form.Components[i]).GetParentComponent;
        while c <> nil do
        begin
          if TComponent(c).Name = WinControl.Name then
          begin
            IsOk := True;
            Break;
          end
          else c := TComponent(c).GetParentComponent;
        end;
      end;
      if IsOk then
      begin
        PIUpdCamp := GetPropInfo(Objeto.ClassInfo, UpdCampo);
        PIUpdType := GetPropInfo(Objeto.ClassInfo, 'UpdType');
        //
        if (PIUpdCamp <> nil) and (PIUpdType <> nil) then
        begin
          //try
            Campo   := GetPropValue(Objeto, UpdCampo);
            UpdType := utNil;
            //
            if Campo <> '' then
            begin
              ObtemValorUpdTypeDeDmkComp(Objeto, Valor, UpdType);
              //
              if (UpdType <> utNil) then
              begin
                if UpdType in ([utInc, utIdx]) then
                begin
                  Texto := Geral.VariavelToString(Valor);
                  //
                  Result.Add(Campo + '=' + Texto);
                end;
              end;
            end;
        end;
      end;
    end;
  finally
    ;
  end;
end;

function TUnMyVCLref.CriaSQLdmkPanel_Lins(Form: TForm; WinControl: TWinControl;
  Acao: TSQLType; Tabela: String; NewItem: Variant; UserDataAlterweb: Boolean): String;
const
  LigaSet  = ',' + sLineBreak;
  UpdCampo = 'UpdCampo';
var
  Objeto: TObject;
  PIUpdType, PIUpdCamp: PPropInfo;
  i: Integer;
  Campo, Texto, Liga1, CamposSet, CamposWhere, LigaWhere: String;
  Valor: Variant;
  UpdType: TUpdType;
  c: TComponent;
  IsOk: Boolean;
  //Tipo: TVarType;
begin
  //Result := '';
  //Tipo := Unassigned;
  CamposSet   := '';
  CamposWhere := '';
  if Acao = stUpd then
    LigaWhere := sLineBreak + ' WHERE '
  else
    LigaWhere := '';
  case Acao of
    stIns: Result := 'INSERT INTO ' + lowercase(Tabela) + ' SET ';
    stUpd: Result := 'UPDATE ' + lowercase(Tabela) + ' SET ';
    stDel: Result := 'DELETE ';
    else
    begin
      Result := '# ERRO ## ';
      Geral.MB_Erro(
      'A��o inv�lida na fun��o "CriaSQLdmkPanel"!');
    end;
  end;
  if UserDataAlterweb then
  begin
    Liga1 := LigaSet;
    case Acao of
      stIns: Result := Result + 'AlterWeb = 1, UserCad=' +
             FormatFloat('0', VAR_USUARIO) + ', DataCad="' + Geral.FDT(
             Date, 1) + '" ' + sLineBreak;
      stUpd: Result := Result + 'AlterWeb = 1, UserAlt=' +
             FormatFloat('0', VAR_USUARIO) + ', DataAlt="' + Geral.FDT(
             Date, 1) + '" ' + sLineBreak;
      //stDel: Result := '';
    end;
  end;
  //n := 0;
  for i := 0 to Form.ComponentCount -1 do
  begin
    Objeto := Form.Components[i];
    if (Objeto is TdmkValUsu) then
    begin
      IsOK := True;
    end else begin
      IsOK := False;
      //
      c := TComponent(Form.Components[i]).GetParentComponent;
      while c <> nil do
      begin
        if TComponent(c).Name = WinControl.Name then
        begin
          IsOk := True;
          Break;
        end
        else c := TComponent(c).GetParentComponent;
      end;
    end;
    if IsOk then
    begin
      PIUpdCamp := GetPropInfo(Objeto.ClassInfo, UpdCampo);
      PIUpdType := GetPropInfo(Objeto.ClassInfo, 'UpdType');
      //
      if (PIUpdCamp <> nil) and (PIUpdType <> nil) then
      begin
        //try
          Campo   := GetPropValue(Objeto, UpdCampo);
          UpdType := utNil;
          //
          if Campo <> '' then
          begin
            ObtemValorUpdTypeDeDmkComp(Objeto, Valor, UpdType);
            //
            if (UpdType <> utNil) then
            begin
              if (UpdType = utInc) and (Acao = stIns) then
                Valor := NewItem;
              Texto := Geral.VariavelToString(Valor);
              CamposSet := CamposSet + Liga1 + Campo + '=' + Texto;
              // Mudei 2011-06-24
              Liga1 := LigaSet;
              if (UpdType in ([utInc, utIdx])) and (Acao in ([stUpd, stDel])) then
              begin
                CamposWhere := CamposWhere + LigaWhere + sLineBreak + Campo + '=' + Texto;
                LigaWhere := sLineBreak + ' AND ';
              end;
            end;
          end;
        //except
          //  ???
          //raise;
        //end;
      end;
    end;
  end;
  Result := Result + CamposSet + CamposWhere;
  //if Length(Result) > 0 then Result := Copy(Result, 2, Length(Result)-1);
end;

function TUnMyVCLref.ObtemValorUpdTypeDeDmkComp(Objeto: TObject;
  var Val: Variant; var UpdTyp: TUpdType): Boolean;
const
  UpdCampo = 'UpdCampo';
var
  Aux: String;
  Valor: Variant;
  UpdType: TUpdType;
begin
  Result  := False;
  Valor   := Null;
  UpdType := utNil;
  //
  if (Objeto is TdmkEdit) then
  begin
    if TdmkEdit(Objeto).FormatType = dmktfMesAno then
      Valor := Geral.ValidaMesAno(TdmkEdit(Objeto).ValueVariant)
    else
    if TdmkEdit(Objeto).FormatType = dmktf_AAAAMM then
    begin
      if TdmkEdit(Objeto).ValueVariant = Null then
        Aux := ''
      else
        Aux := Geral.VariantToString(TdmkEdit(Objeto).ValueVariant);
      //
      // Caso n�o funcione em algum lugar, tentar como no TdmkEdit.SetValue  > 2011/02/18
      if (Length(Aux) = 10) and (Aux[3] = '/') and (Aux[6] = '/') then
        Valor := Geral.IMV(Copy(Aux, 7) + Copy(Aux, 4, 2))
      else
      if TdmkEdit(Objeto).ValueVariant = Null then
        Valor := 0
      else
        Valor := Geral.IMV(FormatDateTime('YYYYMM', TdmkEdit(Objeto).ValueVariant))
    end else
    if TdmkEdit(Objeto).FormatType = dmktfDateTime then
      Valor := FormatDateTime('yyyy-mm-dd hh:nn:ss',
        TdmkEdit(Objeto).ValueVariant)
    else
    if TdmkEdit(Objeto).FormatType = dmktfTime then
    begin
       case TdmkEdit(Objeto).HoraFormat of
         dmkhfLong: Valor := FormatDateTime('hh:nn:ss', TdmkEdit(Objeto).ValueVariant);
         dmkhfShort: Valor := FormatDateTime('hh:nn', TdmkEdit(Objeto).ValueVariant);
         dmkhfMiliSeconds: Valor := FormatDateTime('hh:nn:ss,zzz', TdmkEdit(Objeto).ValueVariant);
         //  Guardar como double no MySQL?
         dmkhfExtended: Valor := (*FormatDateTime('hh:nn:ss,zzz',*) TdmkEdit(Objeto).ValueVariant;
         dmkhfTZD_UTC: Valor := (*FormatDateTime('hh:nn:ss,zzz',*) TdmkEdit(Objeto).ValueVariant;
         else Valor := '00:00';
       end;
    end
    else
    if TdmkEdit(Objeto).MskType <> fmtNone then
    begin
      if TdmkEdit(Objeto).MskType in ([fmtTelCurto,fmtTelLongo]) then
      begin
        if Geral.PareceTelefoneBR(TdmkEdit(Objeto).ValueVariant) then
          Valor := Geral.SoNumero_TT(TdmkEdit(Objeto).ValueVariant)
        else
          Valor := Trim(TdmkEdit(Objeto).ValueVariant);
      end else
      begin
        Valor := Geral.SoNumeroELetra_TT(TdmkEdit(Objeto).ValueVariant);
        if TdmkEdit(Objeto).MskType = fmtUF then
          Valor := Geral.GetCodigoUF_da_SiglaUF(Valor)
        // XE2 2013-05-30 > fmtCEP nao pode ser string vazia
        else
        if TdmkEdit(Objeto).MskType = fmtCEP then
          if Valor = '' then
            Valor := '0'
        // fim XE2 2013-05-30
      end;
    end else
      Valor := TdmkEdit(Objeto).ValueVariant;
    UpdType := TdmkEdit(Objeto).UpdType;
  end
  //
  else if (Objeto is TdmkDBLookupCombobox) then
  begin
    Valor := TdmkDBLookupCombobox(Objeto).KeyValue;
    UpdType := TdmkDBLookupComboBox(Objeto).UpdType;
  end
  //
  else if (Objeto is TdmkEditCB) then
  begin
    Valor := TdmkEditCB(Objeto).ValueVariant;
    UpdType := TdmkEditCB(Objeto).UpdType;
  end
  //
  else if (Objeto is TdmkEditDateTimePicker) then
  begin
    Valor := TdmkEditDateTimePicker(Objeto).DateTime;
    UpdType := TdmkEditDateTimePicker(Objeto).UpdType;
  end
  //
  else if (Objeto is TdmkCheckBox) then
  begin
    if TdmkCheckBox(Objeto).Checked then
    begin
      if TdmkCheckBox(Objeto).ValCheck <> '' then
      Valor := TdmkCheckBox(Objeto).ValCheck
      else
      Valor := TdmkCheckBox(Objeto).Checked;
    end else begin
      if TdmkCheckBox(Objeto).ValUncheck <> '' then
      Valor := TdmkCheckBox(Objeto).ValUncheck
      else
      Valor := TdmkCheckBox(Objeto).Checked;
    end;
    UpdType := TdmkCheckBox(Objeto).UpdType;
  end
  //
  else if (Objeto is TdmkCheckGroup) then
  begin
    Valor := TdmkCheckGroup(Objeto).Value;
    UpdType := TdmkCheckGroup(Objeto).UpdType;
  end
  //
  else if (Objeto is TdmkRadioGroup) then
  begin
    Valor := TdmkRadioGroup(Objeto).ItemIndex;
    UpdType := TdmkRadioGroup(Objeto).UpdType;
  end
  //
  else if (Objeto is TdmkMemo) then
  begin
    Valor := TdmkMemo(Objeto).Text;
    UpdType := TdmkMemo(Objeto).UpdType;
  end
  //
  else if (Objeto is TdmkDBEdit) then
  begin
    try
      Valor := Null;
  //{$IFNDEF NO_USE_MYSQLMODULE}
  {$IFNDEF NAO_USA_DB}
      FmMeuDBUses.ObtemValorDe_dmkDBEdit(Objeto, Valor, UpdType);
  {$ENDIF}
  {
      Valor := T m y S Q L DataSet(TDataSource(TdmkDBEdit(Objeto).DataSource).
        DataSet).FieldByName(Campo).Value;
      UpdType := TdmkDBEdit(Objeto).UpdType;
  }
    except
      if Valor = Null then
      begin
        //raise EAbort.Create('N�o foi poss�vel obter o valor do TdmkDBEdit.');
        Geral.MB_Erro('N�o foi poss�vel obter o valor' +
        'do "TdmkDBEdit.' + TdmkDBEdit(Objeto).Name +
        '". Avise a Dermatek');
        // N�o mostra, mas pelo menos interrompe a execuss�o!
        raise EAbort.Create('ERRO');
      end;
    end;
  end
  //
  else if (Objeto is TdmkPopOutFntCBox) then
  begin
    Valor := TdmkPopOutFntCBox(Objeto).FonteNome;
    UpdType := TdmkPopOutFntCBox(Objeto).UpdType;
  end
  else if (Objeto is TdmkRichEdit) then
  begin
    {
    Valor := dmkPF.DuplicaBarras(MyObjects.ObtemTextoRichEdit(
    TForm(TdmkRichEdit(Objeto).Parent),
    TdmkRichEdit(Objeto)));
    }
    Valor := MyObjects.ObtemTextoRichEdit(
    TForm(TdmkRichEdit(Objeto).Parent),
    TdmkRichEdit(Objeto));
    UpdType := TdmkRichEdit(Objeto).UpdType;
  end
  //
  else if (Objeto is TdmkValUsu) then
  begin
    Valor := TdmkValUsu(Objeto).ValueVariant;
    UpdType := TdmkValUsu(Objeto).UpdType;
  end
  else
  begin
    //Incluir := False;
    Geral.MB_Erro('O componente ' + TComponent(
    Objeto).Name + ' n�o possui a propriedade ' + UpdCampo +
    '.' + sLineBreak + 'AVISE A DERMATEK!');
  end;
  Val    := Valor;
  UpdTyp := UpdType;
  Result := UpdType <> utNil;
end;

function TUnMyVCLref.CriaSQLdmkPanel_Sets(Form: TForm; Panel: TPanel; Acao: TSQLType;
  Tabela: String; NewItem: Variant; UserDataAlterweb: Boolean): String;
const
  LigaSet  = ',';
  UpdCampo = 'UpdCampo';
var
  Objeto: TObject;
  PIUpdType, PIUpdCamp: PPropInfo;
  i: Integer;
  Campo, Texto, Liga1, CamposSet, CamposVal, CamposWhere, LigaWhere: String;
  Valor: Variant;
  UpdType: TUpdType;
  c: TComponent;
  IsOk: Boolean;
  //Tipo: TVarType;
  Aux: String;
begin
  //Result := '';
  //Tipo := Unassigned;
  CamposSet   := '';
  CamposVal   := '';
  CamposWhere := '';
  if Acao = stUpd then
    LigaWhere := sLineBreak + ' WHERE '
  else
    LigaWhere := '';
  case Acao of
    stIns: Result := 'INSERT INTO ' + lowercase(Tabela);
    stUpd: Result := 'UPDATE ' + lowercase(Tabela) + ' SET ';
    stDel: Result := 'DELETE ';
    else
    begin
      Result := '# ERRO ## ';
      Geral.MB_Erro(
      'A��o inv�lida na fun��o "CriaSQLdmkPanel"!');
    end;
  end;
  if UserDataAlterweb then
  begin
    Liga1 := LigaSet;
    case Acao of
      stIns: Result := Result + 'AlterWeb = 1, UserCad=' +
             FormatFloat('0', VAR_USUARIO) + ', DataCad="' + Geral.FDT(
             Date, 1) + '" ' + sLineBreak;
      stUpd: Result := Result + 'AlterWeb = 1, UserAlt=' +
             FormatFloat('0', VAR_USUARIO) + ', DataAlt="' + Geral.FDT(
             Date, 1) + '" ' + sLineBreak;
      //stDel: Result := '';
    end;
  end;
  //n := 0;
  for i := 0 to Form.ComponentCount -1 do
  begin
    Objeto := Form.Components[i];
    if (Objeto is TdmkValUsu) then
    begin
      IsOK := True;
    end else begin
      IsOK := False;
      //
      c := TComponent(Form.Components[i]).GetParentComponent;
      while c <> nil do
      begin
        if TComponent(c).Name = Panel.Name then
        begin
          IsOk := True;
          Break;
        end
        else c := TComponent(c).GetParentComponent;
      end;
    end;
    if IsOk then
    begin
      PIUpdCamp := GetPropInfo(Objeto.ClassInfo, UpdCampo);
      PIUpdType := GetPropInfo(Objeto.ClassInfo, 'UpdType');
      //
      if (PIUpdCamp <> nil) and (PIUpdType <> nil) then
      begin
        //try
          Campo := GetPropValue(Objeto, UpdCampo);
          UpdType := utNil;

          if Campo <> '' then
          begin



            if (Objeto is TdmkEdit) then
            begin
              if TdmkEdit(Objeto).FormatType = dmktfMesAno then
                Valor := Geral.ValidaMesAno(TdmkEdit(Objeto).ValueVariant)
              else
              if TdmkEdit(Objeto).FormatType = dmktf_AAAAMM then
              begin
                if TdmkEdit(Objeto).ValueVariant = Null then
                  Aux := ''
                else
                  Aux := Geral.VariantToString(TdmkEdit(Objeto).ValueVariant);
                //
                // Caso n�o funcione em algum lugar, tentar como no TdmkEdit.SetValue  > 2011/02/18
                if (Length(Aux) = 10) and (Aux[3] = '/') and (Aux[6] = '/') then
                  Valor := Geral.IMV(Copy(Aux, 7) + Copy(Aux, 4, 2))
                else
                if TdmkEdit(Objeto).ValueVariant = Null then
                  Valor := 0
                else
                  Valor := Geral.IMV(FormatDateTime('YYYYMM', TdmkEdit(Objeto).ValueVariant))
              end else
              if TdmkEdit(Objeto).FormatType = dmktfDateTime then
                Valor := FormatDateTime('yyyy-mm-dd hh:nn:ss',
                  TdmkEdit(Objeto).ValueVariant)
              else
              if TdmkEdit(Objeto).FormatType = dmktfDate then
                Valor := FormatDateTime('yyyy-mm-dd',
                  TdmkEdit(Objeto).ValueVariant)
              else
              if TdmkEdit(Objeto).FormatType = dmktfTime then
              begin
                if TdmkEdit(Objeto).ValueVariant < 0 then
                  Valor := '-' + FormatDateTime('hh:nn',
                  - TdmkEdit(Objeto).ValueVariant)
                else
                  Valor := FormatDateTime('hh:nn:ss',
                  TdmkEdit(Objeto).ValueVariant);
              end else
              if TdmkEdit(Objeto).MskType <> fmtNone then
              begin
                if TdmkEdit(Objeto).MskType in ([fmtTelCurto,fmtTelLongo]) then
                begin
                  if Geral.PareceTelefoneBR(TdmkEdit(Objeto).ValueVariant) then
                    Valor := Geral.SoNumero_TT(TdmkEdit(Objeto).ValueVariant)
                  else
                    Valor := Trim(TdmkEdit(Objeto).ValueVariant);
                end else
                begin
                  Valor := Geral.SoNumeroELetra_TT(TdmkEdit(Objeto).ValueVariant);
                  if TdmkEdit(Objeto).MskType = fmtUF then
                    Valor := Geral.GetCodigoUF_da_SiglaUF(Valor);
                end;
              end else
                Valor := TdmkEdit(Objeto).ValueVariant;
              UpdType := TdmkEdit(Objeto).UpdType;
            end
            //
            else if (Objeto is TdmkDBLookupCombobox) then
            begin
              Valor := TdmkDBLookupCombobox(Objeto).KeyValue;
              UpdType := TdmkDBLookupComboBox(Objeto).UpdType;
            end
            //
            else if (Objeto is TdmkEditCB) then
            begin
              Valor := TdmkEditCB(Objeto).ValueVariant;
              UpdType := TdmkEditCB(Objeto).UpdType;
            end
            //
            else if (Objeto is TdmkEditDateTimePicker) then
            begin
              Valor := TdmkEditDateTimePicker(Objeto).DateTime;
              UpdType := TdmkEditDateTimePicker(Objeto).UpdType;
              case TdmkEditDateTimePicker(Objeto).Kind of
                dtkDate: Valor := FormatDateTime('yyyy-mm-dd', Valor);
                dtkTime: Valor := FormatDateTime('hh:nn:ss', Valor);
              end;
            end
            //
            else if (Objeto is TdmkCheckBox) then
            begin
              if TdmkCheckBox(Objeto).Checked then
              begin
                if TdmkCheckBox(Objeto).ValCheck <> '' then
                Valor := TdmkCheckBox(Objeto).ValCheck
                else
                Valor := TdmkCheckBox(Objeto).Checked;
              end else begin
                if TdmkCheckBox(Objeto).ValUncheck <> '' then
                Valor := TdmkCheckBox(Objeto).ValUncheck
                else
                Valor := TdmkCheckBox(Objeto).Checked;
              end;
              UpdType := TdmkCheckBox(Objeto).UpdType;
            end
            //
            else if (Objeto is TdmkCheckGroup) then
            begin
              Valor := TdmkCheckGroup(Objeto).Value;
              UpdType := TdmkCheckGroup(Objeto).UpdType;
            end
            //
            else if (Objeto is TdmkRadioGroup) then
            begin
              Valor := TdmkRadioGroup(Objeto).ItemIndex;
              UpdType := TdmkRadioGroup(Objeto).UpdType;
            end
            //
            else if (Objeto is TdmkMemo) then
            begin
              Valor := TdmkMemo(Objeto).Text;
              UpdType := TdmkMemo(Objeto).UpdType;
            end
            //
            else if (Objeto is TdmkDBEdit) then
            begin
              try
                Valor := Null;
{
                Valor := T m y S Q L DataSet(TDataSource(TdmkDBEdit(Objeto).DataSource).
                  DataSet).FieldByName(Campo).Value;
                UpdType := TdmkDBEdit(Objeto).UpdType;
}
//{$IFNDEF NO_USE_MYSQLMODULE}
{$IFNDEF NAO_USA_DB}
              FmMeuDBUses.ObtemValorDe_dmkDBEdit(Objeto, Valor, UpdType);
{$ENDIF}              
              except
                if Valor = Null then
                begin
                  //raise EAbort.Create('N�o foi poss�vel obter o valor do TdmkDBEdit.');
                  Geral.MB_Erro('N�o foi poss�vel obter o valor' +
                  'do "TdmkDBEdit.' + TdmkDBEdit(Objeto).Name +
                  '". Avise a Dermatek');
                  // N�o mostra, mas pelo menos interrompe a execuss�o!
                  raise EAbort.Create('ERRO');
                end;
              end;
            end
            //
            else if (Objeto is TdmkPopOutFntCBox) then
            begin
              Valor := TdmkPopOutFntCBox(Objeto).FonteNome;
              UpdType := TdmkPopOutFntCBox(Objeto).UpdType;
            end
            //
            else if (Objeto is TdmkRichEdit) then
            begin
              Valor := MyObjects.ObtemTextoRichEdit(
              TForm(TdmkRichEdit(Objeto).Parent),
              TdmkRichEdit(Objeto));
              UpdType := TdmkRichEdit(Objeto).UpdType;
            end
            else if (Objeto is TdmkValUsu) then
            begin
              Valor := TdmkValUsu(Objeto).ValueVariant;
              UpdType := TdmkValUsu(Objeto).UpdType;
            end
            else
            begin
              //Incluir := False;
              Geral.MB_Erro('O componente ' + TComponent(
              Objeto).Name + ' n�o possui a propriedade ' + UpdCampo +
              '.' + sLineBreak + 'AVISE A DERMATEK!');
            end;





            if (UpdType <> utNil) then
            begin
              if (UpdType = utInc) and (Acao = stIns) then
                Valor := NewItem;
              Texto := Geral.VariavelToString(Valor);
              case Acao of
                stIns:
                begin
                  CamposSet := CamposSet + Liga1 + Campo;
                  CamposVal := CamposVal + Liga1 + Texto;
                end;
                stUpd, stDel:
                  CamposSet := CamposSet + Liga1 + sLineBreak + Campo + '=' + Texto;
              end;
              Liga1 := LigaSet;
              if (UpdType in ([utInc, utIdx])) and (Acao in ([stUpd, stDel])) then
              begin
                CamposWhere := CamposWhere + LigaWhere + sLineBreak + Campo + '=' + Texto;
                LigaWhere := sLineBreak + ' AND ';
              end;
            end;
          end;
        //except
          //  ???
          //raise;
        //end;
      end;
    end;
  end;
  case Acao of
    stIns: Result := Result + '(' + CamposSet + ') VALUES (' + CamposVal + ')' + CamposWhere + ';';
    stUpd, stDel:   Result := Result + CamposSet + CamposWhere;
  end;
  //if Length(Result) > 0 then Result := Copy(Result, 2, Length(Result)-1);
end;
{$WARNINGS ON}

end.
