unit UnReordena;

interface

uses Classes, StdCtrls, Forms, Controls, dmkGeral, ExtCtrls, Windows, Graphics,
  SysUtils, mySQLDbTables, Dialogs, Messages, UnDmkProcFunc, UnMyObjects,
  Variants, ComCtrls, DmkDAC_PF, UnDmkEnums, Grids,DBGrids;

type
  TUnReordena = class(TObject)

  private
    { Private declarations }
  public
    { Public declarations }
    function  VerificaOrdem(Query: TMySQLQuery; DataBase: TMySQLDataBase;
              PriFld, OrdFld, Tabela: String; PriVal, OrdVal: Integer;
              ZeraAntes: Boolean): Integer;
    procedure ReordenaItens(Query, QueryUpd: TMySQLQuery; TabelaAOrdenar,
              CampoAOrdenar, FldIndice, FldDescricao, SQLExtra1, Trocavel1,
              Trocador1: String; DBGrid: TDBGrid);
  end;

var
  UReordena: TUnReordena;

implementation

uses MyDBGridReorder;

{ TUnReordena }

function TUnReordena.VerificaOrdem(Query: TMySQLQuery; DataBase: TMySQLDataBase;
  PriFld, OrdFld, Tabela: String; PriVal, OrdVal: Integer; ZeraAntes: Boolean): Integer;
var
  Res: Integer;
begin
  if ZeraAntes then
    Res := 0;
  //
  if Res = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT MAX(' + OrdFld + ') Ordem',
      'FROM ' + Tabela,
      'WHERE ' + PriFld + '=' + Geral.FF0(PriVal),
      '']);
    //
    Res := Query.FieldByName('Ordem').AsInteger + 1;
  end else
    Res := OrdVal;
  //
  Result := Res;
end;

procedure TUnReordena.ReordenaItens(Query, QueryUpd: TMySQLQuery; TabelaAOrdenar,
  CampoAOrdenar, FldIndice, FldDescricao, SQLExtra1, Trocavel1, Trocador1: String;
  DBGrid: TDBGrid);
begin
  if FldIndice = '' then
  begin
    Geral.MB_Aviso('FldIndice n�o definido!');
    Exit;
  end;
  if FldDescricao = '' then
  begin
    Geral.MB_Aviso('FldDescricao n�o definido!');
    Exit;
  end;
  Application.CreateForm(TFmMyDBGridReorder, FmMyDBGridReorder);
  FmMyDBGridReorder.FMySQLQuery     := Query;
  FmMyDBGridReorder.FMySQLQueryUpd  := QueryUpd;
  FmMyDBGridReorder.FTabelaAOrdenar := TabelaAOrdenar;
  FmMyDBGridReorder.FCampoAOrdenar  := CampoAOrdenar;
  FmMyDBGridReorder.FCampoControle  := FldIndice;
  FmMyDBGridReorder.FCampoDescricao := FldDescricao;
  FmMyDBGridReorder.FSQLExtra1      := SQLExtra1;
  FmMyDBGridReorder.FDBGrid         := DBGrid;
  FmMyDBGridReorder.FTrocavel1      := Trocavel1;
  FmMyDBGridReorder.FTrocador1      := Trocador1;
  //
  FmMyDBGridReorder.ConfiguraGrade;
  //
  FmMyDBGridReorder.BtConfirma.Visible := True;
  FmMyDBGridReorder.Grid.Options       := FmMyDBGridReorder.Grid.Options + [goRowMoving];
  FmMyDBGridReorder.ShowModal;
  FmMyDBGridReorder.Destroy;
end;

end.
