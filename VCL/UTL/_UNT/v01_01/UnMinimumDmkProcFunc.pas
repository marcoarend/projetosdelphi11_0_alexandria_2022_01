unit UnMinimumDmkProcFunc;

interface

uses Winapi.Windows, Winapi.Shellapi, System.SysUtils, System.DateUtils,
  Vcl.Forms, Vcl.Graphics, Winapi.ShlObj, System.RTLConsts,
  UnInternalConsts, UnInternalConsts2,
  //DB, System.IOUtils,
  //StdCtrls,
  //ExtCtrls,
  //Messages,
  //Classes,
  //Controls,
  //Dialogs, Menus,  Math, Mask, Buttons, ComCtrls,
  //Registry, Printers, CommCtrl, TypInfo, ComObj, RichEdit,
  //Consts, ActiveX, OleCtrls, SHDocVw, Variants, MaskUtils, IniFiles, URLMon,
  //Wininet, WinSpool, WinSvc, PsAPI, TlHelp32, ZCF2,
  //XMLIntf, StrUtils,
  //IdBaseComponent, IdComponent, IdRawBase, IdRawClient, IdIcmpClient,
  //IdUDPBase, IdUDPClient, IdSNTP,
  //Vcl.CheckLst, Rtti, IpHlpApi, IpTypes, Winapi.WinSock, Winapi.IpExport,

    //MD5 Novo 2020
  IdHashMessageDigest;

type
  //INI = Obtem vers�o do Windows (Compat�vel com Windows 10
  WKSTA_INFO_100 = record
    wki100_platform_id: DWORD;
    wki100_computername: LPWSTR;
    wki100_langroup: LPWSTR;
    wki100_ver_major: DWORD;
    wki100_ver_minor: DWORD;
  end;
  LPWKSTA_INFO_100 = ^WKSTA_INFO_100;
  MyArrayI03 =  array[01..03] of Integer;
  TUnDmkProcFunc = class(TObject)
  private
    { Private declarations }
    function  GetNetWkstaMajorMinor(var MajorVersion, MinorVersion: DWORD): Boolean;
  public
    { Public declarations }
    function  CalculaValSerialKey_VendaApp(const CNPJ_CPF: String; var SerialHD,
              CPUID: String): String;
    procedure ConfigIniApp(UsaCoresRel: Integer);
    function  DateTime_MyTimeZoneToUTC(DataHora: TDateTime): TDateTime;
    function  DateTime_UTCToMyTimeZone(DataHora: TDateTime): TDateTime;
    function  ObterCaminhoMeusDocumentos(): String;
    function  ObtemSysInfo2(Separador: String = ''): String;
    function  ScheduleRunAtStartup(const ATaskName, AFileName, AUserAccount,
              AUserPassword: string): Integer;
    procedure SistemaOperacional2_txt(var OSVersao, OSArquitetura: String);
    procedure UnScheduleRunAtStartup(const ATaskName: String);
    function  VersaoWindows(): MyArrayI03;
  end;
var
  DmkPF: TUnDmkProcFunc;

implementation

uses dmkGeral;

//INI = Obtem vers�o do Windows (Compat�vel com Windows 10
function NetWkstaGetInfo(ServerName: LPWSTR; Level: DWORD; BufPtr: LPBYTE): Longint; stdcall;
  external 'netapi32.dll' name 'NetWkstaGetInfo';

{ TUnDmkProcFunc }

function TUnDmkProcFunc.CalculaValSerialKey_VendaApp(const CNPJ_CPF: String;
  var SerialHD, CPUID: String): String;

  function SerialNum(FDrive:String): String;
  var
    nVNameSer: PDWORD;
    pVolName: PChar;
    FSSysFlags, maxCmpLen: DWord;
    pFSBuf: PChar;
    drv: String;
  begin
    try
      drv := FDrive + '\';

      GetMem(pVolName, MAX_PATH);
      GetMem(pFSBuf, MAX_PATH);
      GetMem(nVNameSer, MAX_PATH);

      GetVolumeInformation(PChar(drv), pVolName, MAX_PATH, nVNameSer,
        maxCmpLen, FSSysFlags, pFSBuf, MAX_PATH);

      Result := IntToStr(nVNameSer^);

      FreeMem(pVolName, MAX_PATH);
      FreeMem(pFSBuf, MAX_PATH);
      FreeMem(nVNameSer, MAX_PATH);
    except
      Result := '';
    end;
  end;
  {
  //Destivado o notebook tem duas placas de rede wirelass e cabo
  function GetMACaddress : TWMacId;
  var
    i : integer;
    AdapterInfo : Pointer;
    pAdapterInfo : PIP_ADAPTER_INFO;
    dwBufLen, dwStatus : LongWord;
  begin
    dwBufLen := 0;
    AdapterInfo := Nil; //Inicialmente, limpa o retorno
    for i := 0 to MAX_ADAPTER_ADDRESS_LENGTH - 1 do
      Result [i] := 0;
    GetAdaptersInfo(PIP_ADAPTER_INFO (AdapterInfo), dwBufLen);
    if (dwBufLen > 0) then
    begin
      GetMem (AdapterInfo, dwBufLen);
      dwStatus := GetAdaptersInfo(PIP_ADAPTER_INFO (AdapterInfo), dwBufLen);
      if (dwStatus = ERROR_SUCCESS) then
      begin
        pAdapterInfo := PIP_ADAPTER_INFO (AdapterInfo);
        //Copia o endere�o p/ nossa vari�vel de retorno
        for i := 0 to MAX_ADAPTER_ADDRESS_LENGTH - 1 do
          Result[i] := pAdapterInfo.Address[i];
        FreeMem (AdapterInfo);
      end;
    end;
  end;
  function MacAddress: String;
  var
    id : TWMacId;
  begin
    id := getMacAddress;
    Result := Format ( '%.2X', [id [1]]) +
    Format ( '%.2X', [id [2]]) +
    Format ( '%.2X', [id [3]]) +
    Format ( '%.2X', [id [4]]) +
    Format ( '%.2X', [id [5]]);
  end;
  }
  function MacAddress2: String;
  var
    a,b,c,d: LongWord;
    CPUID: String;
  begin
    asm
      push EAX
      push EBX
      push ECX
      push EDX

      mov eax, 1
      db $0F, $A2
      mov a, EAX
      mov b, EBX
      mov c, ECX
      mov d, EDX

      pop EDX
      pop ECX
      pop EBX
      pop EAX

      {
      mov eax, 1
      db $0F, $A2
      mov a, EAX
      mov b, EBX
      mov c, ECX
      mov d, EDX
      }
    end;
    CPUID  := inttohex(a,8);// + inttohex(b,8) + inttohex(c,8) + inttohex(d,8);
    Result := CPUID;
  end;
var
  Unidade, (*HD, MAC,*) CNPJCPF: String;
  Md5: TIdHashMessageDigest5;
begin
  //SerialKey = N�mero do HD + N�mero do CPU + Data da �ltima verifica��o
  //
  Unidade  := ExtractFileDrive(Application.ExeName);
  SerialHD := SerialNum(Unidade);
  CPUID    := MacAddress2;
  //
  CNPJCPF  := Geral.SoNumero_TT(CNPJ_CPF);
  //
  Md5 := TIdHashMessageDigest5.Create;
  Result := Md5.HashStringAsHex(CNPJCPF + SerialHD + CPUID);
  Md5.Free;
end;

procedure TUnDmkProcFunc.ConfigIniApp(UsaCoresRel: Integer);
begin
  IC2_LOpcoesTeclaPonto := Geral.ReadAppKeyLM('TeclaPonto', Application.Title, ktInteger, 110);
  //
  if IC2_LOpcoesTeclaPonto = -1 then
  begin
    try
      Geral.WriteAppKeyLM2('TeclaPonto', Application.Title, 110, ktInteger);
    except

    end;
    IC2_LOpcoesTeclaPonto := 110;
  end;
  IC2_LOpcoesTeclaVirgula := Geral.ReadAppKeyLM('TeclaVirgula', Application.Title, ktInteger, 188);
  //
  if IC2_LOpcoesTeclaVirgula = -1 then
  begin
    try
      Geral.WriteAppKeyLM2('TeclaVirgula', Application.Title, 188, ktInteger);
      IC2_LOpcoesTeclaVirgula := 188;
    except

    end;
  end;
  IC2_LOpcoesTeclaEnter := Geral.ReadAppKeyLM('TeclaEnter', Application.Title, ktInteger, 13);
  //
  if IC2_LOpcoesTeclaEnter = -1 then
  begin
    try
      Geral.WriteAppKeyLM2('TeclaEnter', Application.Title, 13, ktInteger);
      IC2_LOpcoesTeclaEnter := 13;
    except

    end;
  end;
  IC2_LOpcoesTeclaTab := Geral.ReadAppKeyLM('TeclaTAB', Application.Title, ktInteger, 9);
  //
  if IC2_LOpcoesTeclaTab = -1 then
  begin
    try
      Geral.WriteAppKeyLM2('TeclaTAB', Application.Title, 9, ktInteger);
      IC2_LOpcoesTeclaTab := 9;
    except

    end;
  end;

  if UsaCoresRel > 0 then
  begin
    VAR_CORGRUPOXA1 := $B0B06A;
    VAR_CORGRUPOXA2 := clWhite;
    VAR_CORGRUPOXB1 := clWhite;
    VAR_CORGRUPOXB2 := clBlack;
    VAR_CORGRUPOYA1 := clWhite;
    VAR_CORGRUPOYA2 := clBlack;
    VAR_CORGRUPOZA1 := clWhite;
    VAR_CORGRUPOZA2 := clNavy;
    VAR_CORGRUPOA1  := $FF9F9F;
    VAR_CORGRUPOA2  := clWhite;
    VAR_CORGRUPOB1  := $C8B9B9;
    VAR_CORGRUPOB2  := clBlack;
    VAR_CORGRUPOC1  := $C8B9B9;
    VAR_CORGRUPOC2  := clBlack;
    VAR_CORGRUPOD1  := clWhite;
    VAR_CORGRUPOD2  := clMaroon;
    VAR_CORGRUPOZ1  := clWhite;
    VAR_CORGRUPOZ2  := clBlack;
  end else begin
    VAR_CORGRUPOXA1 := clWhite;
    VAR_CORGRUPOXA2 := clBlack;
    VAR_CORGRUPOXB1 := clWhite;
    VAR_CORGRUPOXB2 := clBlack;
    VAR_CORGRUPOYA1 := clWhite;
    VAR_CORGRUPOYA2 := clBlack;
    VAR_CORGRUPOZA1 := clWhite;
    VAR_CORGRUPOZA2 := clBlack;
    VAR_CORGRUPOA1  := clWhite;
    VAR_CORGRUPOA2  := clBlack;
    VAR_CORGRUPOB1  := clWhite;
    VAR_CORGRUPOB2  := clBlack;
    VAR_CORGRUPOC1  := clWhite;
    VAR_CORGRUPOC2  := clBlack;
    VAR_CORGRUPOD1  := clWhite;
    VAR_CORGRUPOD2  := clBlack;
    VAR_CORGRUPOZ1  := clWhite;
    VAR_CORGRUPOZ2  := clBlack;
  end;
  VAR_CLIENTE1 := CO_CLIENTE;
  VAR_CLIENTE2 := '';
  VAR_CLIENTE3 := '';
  VAR_CLIENTE4 := '';
  VAR_FORNECE1 := CO_FORNECEDOR;
  VAR_FORNECE2 := CO_TRANSPORTADOR;
  VAR_FORNECE3 := '';
  VAR_FORNECE4 := '';
  VAR_FORNECE5 := '';
  VAR_FORNECE6 := '';
  VAR_TERCEIR2 := 'Outros';
  VAR_CLIENTEC := '';
  VAR_FORNECEF := '';
  VAR_FORNECEV := '';
  //
  VAR_FP_EMPRESA := 'Cliente1="V"';
  VAR_FP_FUNCION := '(Fornece2="V" OR Fornece4="V")';
  //
  VAR_TITULO_FATNUM := '? ? ?';
  //
end;

function TUnDmkProcFunc.DateTime_MyTimeZoneToUTC(
  DataHora: TDateTime): TDateTime;
begin
  Result := TTimeZone.Local.ToUniversalTime(DataHora);
end;

function TUnDmkProcFunc.DateTime_UTCToMyTimeZone(
  DataHora: TDateTime): TDateTime;
begin
  Result := TTimeZone.Local.ToLocalTime(DataHora);
end;

function TUnDmkProcFunc.GetNetWkstaMajorMinor(var MajorVersion,
  MinorVersion: DWORD): Boolean;
var
  LBuf: LPWKSTA_INFO_100;
begin
  Result := NetWkstaGetInfo(nil, 100, @LBuf) = 0;
  if Result then
  begin
    MajorVersion := LBuf^.wki100_ver_major;
    MinorVersion := LBuf^.wki100_ver_minor;
  end;
end;

function TUnDmkProcFunc.ObtemSysInfo2(Separador: String): String;
var
  OSVer, OSArq, Info: String;
begin
  SistemaOperacional2_txt(OSVer, OSArq);
  //
  if OSVer <> '' then
    Info := OSVer + ' ' + Separador + ' ' + OSArq
  else
    Info := '';
  //
  Result := Info;
end;

function TUnDmkProcFunc.ObterCaminhoMeusDocumentos: String;
var
   r: BOOL;
   Caminho: array[0..Max_Path] of Char;
begin
    r := ShGetSpecialFolderPath(0, Caminho, CSIDL_Personal, False) ;
    if not r then
       raise Exception.Create('N�o foi poss�vel encontrar a localiza��o da Pasta "Meus Documentos".') ;
    Result := Caminho;
end;

function TUnDmkProcFunc.ScheduleRunAtStartup(const ATaskName, AFileName,
  AUserAccount, AUserPassword: string): Integer;
  // https://docs.microsoft.com/pt-br/windows-server/administration/windows-commands/schtasks#BKMK_syntax
  // schtasks /create /sc <ScheduleType> /tn <TaskName> /tr <TaskRun> [/s <Computer> [/u [<Domain>\]<User> [/p <Password>]]] [/ru {[<Domain>\]<User> | System}] [/rp <Password>] [/mo <Modifier>] [/d <Day>[,<Day>...] | *] [/m <Month>[,<Month>...]] [/i <IdleTime>] [/st <StartTime>] [/ri <Interval>] [{/et <EndTime> | /du <Duration>} [/k]] [/sd <StartDate>] [/ed <EndDate>] [/it] [/z] [/f]
var
  NovoComando: String;
begin
  Result := 0;
  ShellExecute(0, nil, 'schtasks', PChar('/delete /f /tn "' + ATaskName + '"'),
    nil, SW_HIDE);
  (*
  ShellExecute(0, nil, 'schtasks', PChar('/create /tn "' + ATaskName + '" ' +
  '/tr "' + AFileName + '" /sc ONSTART /ru "' + AUserAccount + '"'), nil, SW_HIDE);
  *)
  //NovoComando := '/create /tn "' + ATaskName + '" ' +  '/tr "' + AFileName + '" /sc ONSTART';
  NovoComando := '/create /tn "' + ATaskName + '" ' +  '/tr "' + AFileName + '" /sc ONLOGON';
  if AUserAccount <> '' then
    NovoComando := NovoComando + ' /ru "' + AUserAccount + '"';
  if AUserPassword <> '' then
    NovoComando := NovoComando + ' /rp "' + AUserPassword + '"';
  Result := ShellExecute(0, nil, 'schtasks', PWideChar(NovoComando), nil, SW_HIDE);
end;

procedure TUnDmkProcFunc.SistemaOperacional2_txt(var OSVersao,
  OSArquitetura: String);
var
  SysInfo: TSystemInfo;
  VerInfo: TOSVersionInfoEx;
  MajorNumero, MinorNumero, BuildNum: DWORD;
  OSName, Arquitetura: String;

  function Check(AMajor, AMinor: Integer): Boolean;
  begin
    Result := (MajorNumero > AMajor) or ((MajorNumero = AMajor) and (MinorNumero >= AMinor));
  end;

  function ObtemArquitetura(): String;
  begin
    if TOSVersion.Architecture = arIntelX86 then
      Result := SVersion32
    else if TOSVersion.Architecture = arIntelX64 then
      Result := SVersion64
    else
      Result := '';
  end;

const
{$IF Defined(NEXTGEN) and Declared(System.Embedded)}
  kernelbase = 'kernelbase.dll';
{$ELSE}
  kernelbase = 'kernel32.dll';
{$IFEND}
  SWindowsServer2012 = 'Windows Server 2012';
  SWindows8 = 'Windows 8';
  SWindows8Point1 = 'Windows 8.1';
  SWindows10 = 'Windows 10';
begin
  OSName      := '';
  Arquitetura := '';

  ZeroMemory(@VerInfo, SizeOf(VerInfo));
  VerInfo.dwOSVersionInfoSize := SizeOf(VerInfo);
  GetVersionEx(VerInfo);

  MajorNumero := VerInfo.dwMajorVersion;
  MinorNumero := VerInfo.dwMinorVersion;
  BuildNum    := VerInfo.dwBuildNumber;

  ZeroMemory(@SysInfo, SizeOf(SysInfo));
  if Check(5, 1) then // GetNativeSystemInfo not supported on Windows 2000
    GetNativeSystemInfo(SysInfo);

  Arquitetura := ObtemArquitetura;

  if (MajorNumero > 6) or ((MajorNumero = 6) and  (MinorNumero > 1)) then
  begin
    if not GetNetWkstaMajorMinor(MajorNumero, MinorNumero) then
      GetProductVersion(kernelbase, MajorNumero, MinorNumero, BuildNum);
  end;

  OSName := SWindows;
  case MajorNumero of
    10: case MinorNumero of
          0: OSName := SWindows10;
        end;
    6:  case MinorNumero of
          0: if VerInfo.wProductType = VER_NT_WORKSTATION then
               OSName := SWindowsVista
             else
               OSName := SWindowsServer2008;
          1: if VerInfo.wProductType = VER_NT_WORKSTATION then
               OSName := SWindows7
             else
               OSName := SWindowsServer2008R2;
          2: if VerInfo.wProductType = VER_NT_WORKSTATION then
               OSName := SWindows8
             else
               OSName := SWindowsServer2012;
          3: OSName := SWindows8Point1;
        end;
    5:  case MinorNumero of
          0: OSName := SWindows2000;
          1: OSName := SWindowsXP;
          2:
            begin
              if (VerInfo.wProductType = VER_NT_WORKSTATION) and
                 (SysInfo.wProcessorArchitecture = PROCESSOR_ARCHITECTURE_AMD64) then
                OSName := SWindowsXP
              else
              begin
                if GetSystemMetrics(SM_SERVERR2) = 0 then
                  OSName := SWindowsServer2003
                else
                  OSName := SWindowsServer2003R2
              end;
            end;
        end;
  end;
  OSArquitetura := Arquitetura;
  OSVersao      := OSName;
end;

procedure TUnDmkProcFunc.UnScheduleRunAtStartup(const ATaskName: String);
begin
  ShellExecute(0, nil, 'schtasks', PChar('/delete /f /tn "' + ATaskName + '"'),
    nil, SW_HIDE);
end;

function TUnDmkProcFunc.VersaoWindows: MyArrayI03;
//var
  //Menor, Maior: Integer;
begin
  {
  Win95    := (Win32MajorVersion = 4) and (Win32MinorVersion = 0)  and (Win32Platform = );
  Win98    := (Win32MajorVersion = 4) and (Win32MinorVersion = 10) and (Win32Platform = VER_PLATFORM_WIN32_WINDOWS);
  WinME    := (Win32MajorVersion = 4) and (Win32MinorVersion = 90) and (Win32Platform = VER_PLATFORM_WIN32_WINDOWS);
  WinNT    := (Win32MajorVersion = 4) and (Win32MinorVersion = 0)  and (Win32Platform = VER_PLATFORM_WIN32_NT);
  Win2K    := (Win32MajorVersion = 5) and (Win32MinorVersion = 0)  and (Win32Platform = VER_PLATFORM_WIN32_NT);
  WinXP    := (Win32MajorVersion = 5) and (Win32MinorVersion = 1)  and (Win32Platform = VER_PLATFORM_WIN32_NT);
  WinVista := (Win32MajorVersion = 6) and (Win32Platform = VER_PLATFORM_WIN32_NT);
  }
  Result[1] := Win32MajorVersion;
  Result[2] := Win32MinorVersion;
  Result[3] := Win32Platform;
end;

end.
