unit UnLicVendaApp_Dmk;

interface

uses Winapi.Windows, System.SysUtils, SntpSend, System.DateUtils,
  // DCP
  DCPcrypt2, DCPblockciphers, DCPtwofish, DCPsha1,
  // Indy
  //IdBaseComponent, IdSASL, IdSASLUserPass, IdSASLDigest, IdSASL_CRAMBase, IdSASL_CRAM_MD5;
  IdHashMessageDigest;

type
  TUnLicVendaApp_Dmk = class(TObject)
  private
    function  TrySNTPDateTime(out ADate: TDateTime; const AServer: string; const
              ATimeOut: Integer = 2000): Boolean;
    function  ContinuarSeSemInternet(Pergunta: Boolean): Boolean;

  public
    function  LiberaUso(const CNPJ, PIN: String; const CodAplic, Versao: Integer;
              var AtualizadoEm, SerialHD, CPUID, SerialKey: String): Boolean;
    function  ObtemDataHoraWeb(out ADate: TDateTime; const ATimeOut:
              Integer = 2000): Boolean;
    //////////// C R Y P T /////////////////////////////////////////////////////
    function  ObtemCodigoPinDeCnpj(const CNPJ: String; const Posicao: Integer;
              var CodigoPIN: String): Boolean;
    function  DeCryptCPFCNPJ(Crypt: String; Posicao: Integer; twofish: TDCP_twofish): string;
    function  EnCryptCPFCNPJ(CPFCNPJ: string; Posicao: Integer; twofish: TDCP_twofish): string;
    function  ObtemCodigoPinDeCpfCnpj(const CNPJ: String; const Posicao: Integer;
              twofish: TDCP_twofish; var CodigoPIN: String): Boolean;
    //////////// FIM C R Y P T /////////////////////////////////////////////////
  end;

var
  LicVendaApp_Dmk: TUnLicVendaApp_Dmk;

implementation


uses
{$IfDef uDmkPFMin}UnMinimumDmkProcFunc,
{$Else} UnDmkProcFunc,
{$EndIf}
dmkGeral, UnGrl_DmkREST, UnGrl_Vars, Windows_FMX_ProcFunc;

{ TUnLicVendaApp_Dmk }

const
  FPosicoesPinCnpj: array[0..9] of string = (
    'EspiritualmentePobres',
    'PessoasQueChoram',
    'HumildesPessoas',
    'TemFomeESede',
    'Misericordia',
    'CoracaoPuro',
    'TrabalhamPelaPaz',
    'SofremPerseguicao',
    'InsultamPerseguem',
    'DizemCalunia');

function TUnLicVendaApp_Dmk.ContinuarSeSemInternet(Pergunta: Boolean): Boolean;
var
  Msg: String;
begin
  Result := Windows_FMX_dmkPF.VerificaConexaoWeb(Msg) > 0;
  if not Result then
  begin
    if Pergunta then
      Result := Geral.MB_Pergunta(
      'Parace que este dispositivo est� desconectado da internet!' + sLineBreak +
      'Talvez n�o seja poss�vel terminar a requisi��o!' + sLineBreak +
      'Deseja continuar assim mesmo?') = ID_YES;
  end;
end;

function TUnLicVendaApp_Dmk.DeCryptCPFCNPJ(Crypt: string; Posicao: Integer;
  twofish: TDCP_twofish): string;
var
  Chave: string;
begin
  Result := '';
  if (Posicao < 1) or (Posicao > Length(FPosicoesPinCnpj)) then
  begin
    Geral.MB_Info('Posi��o inv�lida!');
    Exit;
  end;
  Chave := FPosicoesPinCnpj[Posicao - 1];
  twofish.InitStr(Chave, TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.DecryptString(Crypt); // Descriptografa o JSON.
  twofish.Burn();
end;

function TUnLicVendaApp_Dmk.EnCryptCPFCNPJ(CPFCNPJ: string; Posicao: Integer;
  twofish: TDCP_twofish): string;
var
  Chave: string;
begin
  Result := '';
  if (Posicao < 1) or (Posicao > Length(FPosicoesPinCnpj)) then
  begin
    Geral.MB_Info('Posi��o inv�lida!');
    Exit;
  end;
  Chave := FPosicoesPinCnpj[Posicao - 1];
  twofish.InitStr(Chave, TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.EncryptString(CPFCNPJ); // Criptografa o CPFCNPJ.
  twofish.Burn();
end;

function TUnLicVendaApp_Dmk.LiberaUso(const CNPJ, PIN: String; const CodAplic,
  Versao: Integer; var AtualizadoEm, SerialHD, CPUID, SerialKey: String): Boolean;
var
  IP, NomePC, OSVersion, StatusCode,
  Mensagem: String;
  CliInt: Integer;
  Resul: Boolean;
  Vencimento: String;
begin
  Result    := False;
  IP        := Geral.ObtemIP(1);
  SerialKey := DmkPF.CalculaValSerialKey_VendaApp(CNPJ, SerialHD, CPUID);
  NomePC    := Geral.IndyComputerName();
  OSVersion := dmkPF.ObtemSysInfo2('||');
  //
  if not ContinuarSeSemInternet(True) then Exit;
  //Resul := Grl_DmkREST.UnLockVendaApp(False, SerialKey, (*SerialNum,*) CNPJ,

  //Result := Grl_DmkREST.UnLockVendaApp(False, SerialKey, PIN, CNPJ, NomePC, IP,
  Result := Grl_DmkREST.UnLockVendaApp(False, SerialKey, PIN, CNPJ, NomePC, IP,
              Geral.FF0(CO_VERSAO), CodAplic, OSVersion, Vencimento, StatusCode,
              CliInt, AtualizadoEm, Mensagem);
{
  Resul := Grl_DmkREST.UnLockVendaApp(True, SerialKey, (*SerialNum,*) CNPJ,
         (*DescriPC,*) NomePC, IP, Geral.FF0(CO_VERSAO), (*Servidor,*) CodAplic,
         (*AtualizaDescriPC,*) OSVersion, (*DBVersion, IPServ,*) Vencimento,
         StatusCode, CliInt, AtualizadoEm, Mensagem);
}
  //if StatusCode = '200' then //HTTP_OK
  if Resul and (StatusCode = '1') then
  begin
    Result := True;
  end else
  begin
    if Mensagem <> EmptyStr then
      Geral.MB_Aviso(Mensagem);
  end;
end;

function TUnLicVendaApp_Dmk.ObtemCodigoPinDeCnpj(const CNPJ: String;
  const Posicao: Integer; var CodigoPIN: String): Boolean;
var
  sMD5, Contrasenha: String;
  I: Integer;
  Md5: TIdHashMessageDigest5;
begin
  Result := False;
  CodigoPin := '';
  if (Posicao < 1) or (Posicao > Length(FPosicoesPinCnpj)) then
  begin
    Geral.MB_Aviso('Posi��o inv�lida!');
    Exit;
  end;
  Contrasenha := FPosicoesPinCnpj[Posicao-1];
(* Repete resultados (c�digos PIN) com cnpjs diferentes!
  sMD5 := UnMD5.StrMD5(Contrasenha + Geral.SoNumero_TT(CNPJ));
*)

  Md5 := TIdHashMessageDigest5.Create;
  sMD5 := Md5.HashStringAsHex(Contrasenha + Geral.SoNumero_TT(CNPJ));
  Md5.Free;

  for I := 27 to 32 do
  begin
    if CharInSet(sMD5[I], (['0'..'9'])) then CodigoPin := CodigoPin + sMD5[I] else
    if CharInSet(sMD5[I], (['A'..'Z'])) then CodigoPin := CodigoPin + Geral.FF0(Ord(sMD5[I]) - Ord('A')) else
    CodigoPin := CodigoPin + '9';
  end;
  Result := True;
end;

function TUnLicVendaApp_Dmk.ObtemCodigoPinDeCpfCnpj(const CNPJ: String;
  const Posicao: Integer; twofish: TDCP_twofish; var CodigoPIN: String): Boolean;
var
  sCrypt, ByteX: String;
  I, N, ByteI: Integer;
begin
  CodigoPIN := '';
  sCrypt := EnCryptCPFCNPJ(CNPJ, Posicao, twofish);
  N := 0;
  for I := Length(sCrypt) downto 1 do
  begin
    ByteI := Ord(sCrypt[I]);
    ByteX := IntToStr(ByteI);
    while Length(ByteX) < 3 do
      ByteX := '0' + ByteX;
    CodigoPIN := CodigoPIN + ByteX[2];
    N := N + 1;
    if N >= 10 then
      Break;
  end;
  while Length(CodigoPIN) < 10 do
    CodigoPIN := '0' + CodigoPIN;
  //
  Result := CodigoPIN <> '';
end;

function TUnLicVendaApp_Dmk.ObtemDataHoraWeb(out ADate: TDateTime; const
  ATimeOut: Integer = 2000): Boolean;
var
  Server, Msg: String;
begin
  ADate := 0;
  Result := False;
  //
  if ContinuarSeSemInternet(False) = False then Exit;
  //
  Server := 'pool.ntp.org';
  Result := TrySNTPDateTime(ADate, Server, 2000);
  if not Result then
  begin
    Server := 'time.windows.com';
    Result := TrySNTPDateTime(ADate, Server, 2000);
  end
end;

function TUnLicVendaApp_Dmk.TrySNTPDateTime(out ADate: TDateTime;
  const AServer: string; const ATimeOut: Integer): Boolean;
var
  V: TSNTPSend;
begin
  V := TSNTPSend.Create;
  try
    try
      V.Timeout := ATimeOut;
      V.TargetHost := AServer;

      with V.Sock do
      begin
        SetTimeout(ATimeOut);
        ConnectionTimeout := ATimeOut;
        InterPacketTimeout := False;
        NonblockSendTimeout := ATimeOut;
        SocksTimeout := ATimeOut;
      end;

      Result := V.GetSNTP;
      if Result then
        ADate := TTimeZone.local.ToLocalTime(V.NTPTime);
    except
      on E: Exception do
      begin
        ADate := 0;
        Result := False;
      end;
    end;
  finally
    V.Free;
  end;
end;


end.
