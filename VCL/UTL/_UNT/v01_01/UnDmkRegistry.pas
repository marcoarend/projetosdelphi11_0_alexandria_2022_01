unit UnDmkRegistry;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  comctrls, stdctrls, Variants, Menus, UnInternalConsts, UnInternalConsts2,
  ExtCtrls, Registry,
  //  Des/Criptografia
  DCPsha1, DCPTwoFish,
  //
  UnDmkEnums, DmkGeral;

type
  TUnDmkRegistry = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function  ReadAppKey(const Key, AppTitle: String; KeyType: TKeyType;
              DefValue: Variant; xHKEY: HKEY): Variant;
    function  ReadAppKeyCU(const Key, AppTitle: String; KeyType: TKeyType;
              DefValue: Variant): Variant;
    function  ReadAppKeyLM_Net(const NomeServ, Key, AppTitle: String; KeyType:
              TKeyType; DefValue: Variant; Retorno: Variant): Boolean;
    function  ReadKey(const Key, Path: String; KeyType: TKeyType; DefValue:
              Variant; xHKEY: HKEY): Variant;
    function  ReadAppKeyLM(const Key, AppTitle: String; KeyType: TKeyType;
              DefValue: Variant): Variant;
    function  DelKeyAppKeyCU(const App: String): Boolean;
    function  DelValAppKeyCU(const App, Key: String): Boolean;
    // N�o testado!!!!
    function  GiveEveryoneFullAccessToRegistryKey(const RootKey: HKey;
              const RegPath: String; out ErrorMsg: string): Boolean;
    procedure WriteAppKey(const Key, App: String; const Value: Variant; KeyType:
              TKeyType; xHKEY: HKEY);
    procedure WriteAppKeyCU(const Key, App: String; const Value: Variant;
              KeyType: TKeyType);
    function  WriteAppKeyLM_Net(const Server, Key, App: String; const Value:
              Variant; KeyType: TKeyType): Boolean;
    procedure WriteAppKeyMULTI_SZ(const Key, App: String; const Value: Variant);
    function  ReadAppKeyMULTI_SZ(const Key, AppTitle: String; DefValue:
              Variant): Variant;
    procedure WriteAppKey_Total(const Key, App: String; const Value: Variant;
              KeyType: TKeyType; xHKEY: HKEY);
    procedure WriteAppKeyLM(const Key, App: String; const Value: Variant;
              KeyType: TKeyType);
    procedure WriteAppKeyLM2(const Key, App: String; const Value: Variant;
              KeyType: TKeyType);
    // Outros!
    function  EnCryptStr(Texto, Chave: string; twofish: TDCP_twofish): string;
    function  DeCryptStr(Texto, Chave: string; twofish: TDCP_twofish): string;

 end;

var
  DmkRegistry: TUnDmkRegistry;
  FFatorCrypt: Integer;
const
  FLength  = 12;//16;
  FCrypto1 = 17342;//70563;
  FCrypto2 = 75840;//87389;

implementation

{ TUnDmkRegistry }

function TUnDmkRegistry.ReadAppKey(const Key, AppTitle: String; KeyType: TKeyType;
  DefValue: Variant; xHKEY: HKEY): Variant;
var
  r : TRegistry;
  Path: String;
begin
  Path := 'Software\'+AppTitle;
  r := TRegistry.Create(KEY_ALL_ACCESS);
  //r := TRegistry.Create(KEY_READ);
  //r.RootKey := HKEY_LOCAL_MACHINE;
  r.RootKey := xHKEY;
  try
    r.OpenKeyReadOnly(Path);
    Result := DefValue;
    //if r.KeyExists(Path + '\' + Key) then
    begin
      if r.ValueExists(Key) then
      begin
        case KeyType of
          ktString: Result := r.ReadString(Key);
          ktBoolean: Result := r.ReadBool(Key);
          ktInteger: Result := r.ReadInteger(Key);
          ktCurrency: Result := r.ReadCurrency(Key);
          ktDate: Result := r.ReadDate(Key);
          ktTime: Result := r.ReadTime(Key);
          ktDateTime: Result := r.ReadDateTime(Key);
          //ktBinary: Result := r.ReadBinaryData(Key);
        end;
      end else
      begin
        if xHKEY = HKEY_LOCAL_MACHINE then
          Result :=
            ReadAppKey(Key, AppTitle, KeyType, DefValue, HKEY_CURRENT_USER);
      end;
    end;
  finally
    r.Free;
  end;
end;

function TUnDmkRegistry.ReadAppKeyCU(const Key, AppTitle: String; KeyType: TKeyType;
  DefValue: Variant): Variant;
var
  r : TRegistry;
begin
  r := TRegistry.Create(KEY_READ);
  //r.RootKey := HKEY_LOCAL_MACHINE;
  r.RootKey := HKEY_CURRENT_USER;
  try
    r.OpenKeyReadOnly('Software\'+AppTitle);
    Result := DefValue;
    if r.ValueExists(Key) then
    begin
      case KeyType of
        ktString: Result := r.ReadString(Key);
        ktBoolean: Result := r.ReadBool(Key);
        ktInteger: Result := r.ReadInteger(Key);
        ktCurrency: Result := r.ReadCurrency(Key);
        ktDate: Result := r.ReadDate(Key);
        ktTime: Result := r.ReadTime(Key);
        ktDateTime: Result := r.ReadDateTime(Key);
        //ktBinary: Result := r.ReadBinaryData(Key);
      end;
    end;
  finally
    r.Free;
  end;
end;

function TUnDmkRegistry.ReadAppKeyLM_Net(const NomeServ, Key, AppTitle: String; KeyType: TKeyType;
  DefValue: Variant; Retorno: Variant): Boolean;
var
  r : TRegistry;
  n: String;
  k: Integer;
begin
  Result := False;
  r := TRegistry.Create(KEY_READ);
  try
    r.RootKey := HKEY_LOCAL_MACHINE;
    n := NomeServ;
    while n[1] = '\' do
      n := Copy(n, 2);
    k := pos('\', n);
    if k > 0 then
      n := Copy(n, 1, k-1);
    n := '\\' + n;
    //
    if r.RegistryConnect(n) then // '\\peters_notebook'
    begin
      Retorno := DefValue;
      // Result deve ser aqui! N�o mudar!
      Result := True;
      if r.OpenKeyReadOnly('Software\'+AppTitle) then
      begin
        if r.ValueExists(Key) then
        begin
          case KeyType of
            ktString: Retorno := r.ReadString(Key);
            ktBoolean: Retorno := r.ReadBool(Key);
            ktInteger: Retorno := r.ReadInteger(Key);
            ktCurrency: Retorno := r.ReadCurrency(Key);
            ktDate: Retorno := r.ReadDate(Key);
            ktTime: Retorno := r.ReadTime(Key);
            ktDateTime: Retorno := r.ReadDateTime(Key);
            //ktBinary: Retorno := r.ReadBinaryData(Key);
          end;
        end;
      end;
    end else Geral.MB_Erro('N�o foi poss�vel acessar o servidor "' + n + '"');
  finally
    r.Free;
  end;
end;

function TUnDmkRegistry.ReadKey(const Key, Path: String; KeyType: TKeyType;
  DefValue: Variant; xHKEY: HKEY): Variant;
var
  r : TRegistry;
begin
  r := TRegistry.Create(KEY_READ);
  r.RootKey := xHKEY;
  try
    r.OpenKeyReadOnly(Path);
    Result := DefValue;
    if r.ValueExists(Key) then
    begin
      case KeyType of
        ktString: Result := r.ReadString(Key);
        ktBoolean: Result := r.ReadBool(Key);
        ktInteger: Result := r.ReadInteger(Key);
        ktCurrency: Result := r.ReadCurrency(Key);
        ktDate: Result := r.ReadDate(Key);
        ktTime: Result := r.ReadTime(Key);
        ktDateTime: Result := r.ReadDateTime(Key);
        //ktBinary: Result := r.ReadBinaryData(Key);
      end;
    end;
  finally
    r.Free;
  end;
end;

function TUnDmkRegistry.ReadAppKeyLM(const Key, AppTitle: String; KeyType: TKeyType;
  DefValue: Variant): Variant;
var
  r : TRegistry;
begin
  r := TRegistry.Create(KEY_READ);
  r.RootKey := HKEY_LOCAL_MACHINE;
  try
    r.OpenKeyReadOnly('Software\'+AppTitle);
    Result := DefValue;
    if r.ValueExists(Key) then
    begin
      case KeyType of
        ktString: Result := r.ReadString(Key);
        ktBoolean: Result := r.ReadBool(Key);
        ktInteger: Result := r.ReadInteger(Key);
        ktCurrency: Result := r.ReadCurrency(Key);
        ktDate: Result := r.ReadDate(Key);
        ktTime: Result := r.ReadTime(Key);
        ktDateTime: Result := r.ReadDateTime(Key);
        //ktBinary: Result := r.ReadBinaryData(Key);
      end;
    end;
  finally
    r.Free;
  end;
end;

function TUnDmkRegistry.DeCryptStr(Texto, Chave: string;
  twofish: TDCP_twofish): string;
begin
  twofish.InitStr(Chave, TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.DecryptString(Texto); // Descriptografa o texto.
  twofish.Burn();
end;

function TUnDmkRegistry.DelKeyAppKeyCU(const App: String): Boolean;
var
  r : TRegistry;
begin
  Result := false;
  r := TRegistry.Create(KEY_ALL_ACCESS);
  r.RootKey := HKEY_CURRENT_USER;
  try
    Result := r.DeleteKey('Software\' + App);
  finally
    r.Free;
  end;
end;

function TUnDmkRegistry.DelValAppKeyCU(const App, Key: String): Boolean;
var
  r : TRegistry;
begin
  Result := False;
  r := TRegistry.Create(KEY_ALL_ACCESS);
  r.RootKey := HKEY_CURRENT_USER;
  try
    if r.OpenKey('Software\' + App, True) then
      Result := r.DeleteValue(Key);
  finally
    r.Free;
  end;
end;

function TUnDmkRegistry.EnCryptStr(Texto, Chave: string;
  twofish: TDCP_twofish): string;
begin
  twofish.InitStr(Chave, TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.EncryptString(Texto); // Criptografa o texto.
  twofish.Burn();
end;

// N�o testado!!!!
function TUnDmkRegistry.GiveEveryoneFullAccessToRegistryKey(const RootKey: HKey;
  const RegPath: String; out ErrorMsg: string): Boolean;
var
  Access : LongWord;
  WinResult : LongWord;
  SD : PSecurity_Descriptor;
  LastError : DWORD;
  Reg : TRegistry;
begin
  Result := TRUE;
  ErrorMsg := '';
  if (Win32Platform = VER_PLATFORM_WIN32_NT) then
  begin
////////////////////////////////////////////////////////////////////////////////
    {
    if TOOLS.UserHasAdminToken then
      Access := KEY_ALL_ACCESS
    else
      Access := KEY_READ OR KEY_WRITE;
    }
    Access := KEY_ALL_ACCESS;
////////////////////////////////////////////////////////////////////////////////
    Reg := TRegistry.Create(Access);
    try
      Reg.RootKey := RootKey;
      if NOT Reg.OpenKey(RegPath, TRUE) then
        Exit;
      GetMem(SD, SECURITY_DESCRIPTOR_MIN_LENGTH);
      try
        Result := InitializeSecurityDescriptor(SD, SECURITY_DESCRIPTOR_REVISION);
        if Result then
          Result := SetSecurityDescriptorDacl(SD, TRUE, NIL, FALSE); // Fails here!
        if NOT Result then
          begin
            LastError := WINDOWS.GetLastError;
            if NOT Result then
              ErrorMsg := SYSUTILS.SysErrorMessage(LastError);
          end
        else
          begin
            WinResult := RegSetKeySecurity(Reg.CurrentKey, DACL_SECURITY_INFORMATION, SD);
            Result := (WinResult = ERROR_SUCCESS);
            ErrorMsg := SYSUTILS.SysErrorMessage(WinResult);
          end;
      finally
        FreeMem(SD);
      end;
      Reg.CloseKey;
    finally
      FreeAndNIL(Reg);
    end;
  end;
end;


procedure TUnDmkRegistry.WriteAppKey(const Key, App: String; const Value: Variant; KeyType:
 TKeyType; xHKEY: HKEY);
var
  r : TRegistry;
  ValTxt, Chave: String;
begin
  r := TRegistry.Create(KEY_ALL_ACCESS);
  //
  case TOSVersion.Architecture of
    arIntelX86: r.RootKey := xHKEY; //HKEY_LOCAL_MACHINE;
    arIntelX64:
    begin
      if xHKEY = HKEY_LOCAL_MACHINE then
        r.RootKey := HKEY_CURRENT_USER
      else
        r.RootKey := xHKEY; //HKEY_LOCAL_MACHINE;
    end;
    else
    begin
      Geral.MB_Aviso(
      'N�o foi poss�vel definir a arquitetura do sistema operacional!' +
      'Ser� considerado 64 Bits!');
      //
      if xHKEY = HKEY_LOCAL_MACHINE then
        r.RootKey := HKEY_CURRENT_USER
      else
        r.RootKey := xHKEY; //HKEY_LOCAL_MACHINE;
    end;
  end;
  try
    Chave := 'Software\' + App;
    //if r.OpenKey('Software\'+App(*Application.Title*), True) then
    {
    if r.OpenKey(Chave, True) then
    begin
    }
    r.OpenKey(Chave, True);// then
      try
        case KeyType of
          ktString: r.WriteString(Key, Value);
          ktBoolean: r.WriteBool(Key, Value);
          ktInteger: r.WriteInteger(Key, Value);
          ktCurrency: r.WriteCurrency(Key, Value);
          ktDate: r.WriteDate(Key, Value);
          ktTime: r.WriteTime(Key, Value);
          ktDateTime: r.WriteDateTime(Key, Value);
          //ktBinary: Result := r.WriteBinaryData(Key);
        end;
      except
        //ValTxt := String(VariavelToString(Value);
        ValTxt := VarToStr(Value);
        //
        Geral.MB_Erro('N�o foi poss�vel configurar o valor "' +
        ValTxt + '" para a chave "' + key +
        '" no registro do windows!. ' + sLineBreak +
        'A causa mais prov�vel � falta de permiss�o do usu�rio no sistema operacional. ' +
        sLineBreak +
        'O aplicativo n�o ser� executado corretamente. Avise a DERMATEK!' +
        sLineBreak + 'procedure TUnDmkRegistry.WriteAppKey()');
        // Desmarcar at'e ver o que fazer para win 8
        //Application.Terminate;
      end;
    {
    end
    else
    begin
      case xHKey of
        HKEY_CLASSES_ROOT: Nome_HKEY := 'HKEY_CLASSES_ROOT';
        HKEY_CURRENT_USER: Nome_HKEY := 'HKEY_CURRENT_USER';
        HKEY_LOCAL_MACHINE: Nome_HKEY := 'HKEY_LOCAL_MACHINE';
        HKEY_USERS: Nome_HKEY := 'HKEY_USERS';
        HKEY_PERFORMANCE_DATA: Nome_HKEY := 'HKEY_PERFORMANCE_DATA';
        HKEY_CURRENT_CONFIG: Nome_HKEY := 'HKEY_CURRENT_CONFIG';
        HKEY_DYN_DATA: Nome_HKEY := 'HKEY_DYN_DATA';
        else Nome_HKEY := 'HKEY_?????';
      end;
      //
      (*MB_Erro*)ShowMessage
      'N�o foi poss�vel abrir a seguinte chave no registro do windows: ' +
      sLineBreak +
      'Raiz: ' + Nome_HKEY + sLineBreak +
      'Chave: ' + Chave + sLineBreak +
      'Campo: ' + Key + sLineBreak);
    end;
    }
  finally
    r.Free;
  end;
end;

procedure TUnDmkRegistry.WriteAppKeyCU(const Key, App: String; const Value: Variant;
  KeyType: TKeyType);
var
  r : TRegistry;
begin
  r := TRegistry.Create(KEY_ALL_ACCESS);
  r.RootKey := HKEY_CURRENT_USER;
  try
    r.OpenKey('Software\'+App(*Application.Title*), True);
    case KeyType of
      ktString: r.WriteString(Key, Value);
      ktBoolean: r.WriteBool(Key, Value);
      ktInteger: r.WriteInteger(Key, Value);
      ktCurrency: r.WriteCurrency(Key, Value);
      ktDate: r.WriteDate(Key, Value);
      ktTime: r.WriteTime(Key, Value);
      ktDateTime: r.WriteDateTime(Key, Value);
      //ktBinary: Result := r.WriteBinaryData(Key);
    end;
  finally
    r.Free;
  end;
end;

function TUnDmkRegistry.WriteAppKeyLM_Net(const Server, Key, App: String; const Value: Variant;
  KeyType: TKeyType): Boolean;
var
  r : TRegistry;
  n: String;
  k: Integer;
begin
  Result := False;
  r := TRegistry.Create(KEY_ALL_ACCESS);
  r.RootKey := HKEY_LOCAL_MACHINE;
  n := Server;
  while n[1] = '\' do
    n := Copy(n, 2);
  k := pos('\', n);
  if k > 0 then
    n := Copy(n, 1, k-1);
  n := '\\' + n;
  //
  if r.RegistryConnect(n) then // '\\Servidor'
  begin
    Result := True;
    if r.OpenKey('Software\'+App(*Application.Title*), True) then
    begin
      try
        case KeyType of
          ktString: r.WriteString(Key, Value);
          ktBoolean: r.WriteBool(Key, Value);
          ktInteger: r.WriteInteger(Key, Value);
          ktCurrency: r.WriteCurrency(Key, Value);
          ktDate: r.WriteDate(Key, Value);
          ktTime: r.WriteTime(Key, Value);
          ktDateTime: r.WriteDateTime(Key, Value);
          //ktBinary: Result := r.WriteBinaryData(Key);
        end;
      except
       Geral.MB_Erro(
       'N�o foi poss�vel escrever um registro do windows no servidor "' + n + '"');
      end;
    end else Geral.MB_Erro(
    'N�o foi poss�vel acessar uma chave do registro do windows no servidor "' + n + '"');
  end else Geral.MB_Erro(
  'N�o foi poss�vel acessar o registro do windows no servidor "' + n + '"');
end;

procedure TUnDmkRegistry.WriteAppKeyMULTI_SZ(const Key, App: String; const Value: Variant);
var
  r: TRegistry;
  Res: Integer;
  Str: String;
begin
  r := TRegistry.Create;
  try
    Str := Value;
    r.RootKey := HKEY_CURRENT_USER;
    if not r.OpenKey('\Software\' + App, True) then
      raise Exception.Create('N�o foi poss�vel abrir chave!');
    Res := RegSetValueEx(
      r.CurrentKey,
      PWideChar(Key),
      0,
      REG_MULTI_SZ,
      PChar(Str),
      Length(Str) + 1);
    if Res <> ERROR_SUCCESS then
      ShowMessage('Falha ao atualizar chave!');
  finally
    r.Free;
  end;
end;

function TUnDmkRegistry.ReadAppKeyMULTI_SZ(const Key, AppTitle: String;
  DefValue: Variant): Variant;
var
  r: TRegistry;
  DataType: Cardinal;
  DataSize: Cardinal;
  Res: Integer;
  Str: String;
begin
  r := TRegistry.Create;
  try
    r.RootKey := HKEY_CURRENT_USER;
    if not r.OpenKeyReadOnly('\Software\' + AppTitle) then
    begin
      Result := DefValue;
      Exit;
    end;
    DataSize := 0;
    {
    Res := RegQueryValueEx(
      r.CurrentKey,
      PAnsiChar(Key),
      nil,
      @DataType,
      nil,
      @DataSize);
    if Res <> ERROR_SUCCESS then
      Geral.MB_Aviso('N�o foi poss�vel escrever um registro do windows!',
        'Erro', MB_OK+MB_ICONERROR);
    if DataType <> REG_MULTI_SZ then
      raise Exception.Create('Wrong data type');
    }
    SetLength(Str, DataSize - 1);
    if DataSize > 1 then
    begin
      Res := RegQueryValueEx(
        r.CurrentKey,
        PWideChar(Key),
        nil,
        @DataType,
        PByte(Str),
        @DataSize);
      if Res <> ERROR_SUCCESS then
        Geral.MB_Erro('N�o foi poss�vel escrever um registro do Windows!');
    end;
    if Str <> '' then
      Result := Str
    else
      Result := DefValue;
  finally
    r.Free;
  end;
end;

procedure TUnDmkRegistry.WriteAppKey_Total(const Key, App: String; const Value: Variant;
  KeyType: TKeyType; xHKEY: HKEY);
var
  r : TRegistry;
begin
  r := TRegistry.Create(KEY_ALL_ACCESS);
  r.RootKey := xHKEY; //HKEY_LOCAL_MACHINE;
  try
    r.OpenKey(App, True);
    case KeyType of
      ktString: r.WriteString(Key, Value);
      ktBoolean: r.WriteBool(Key, Value);
      ktInteger: r.WriteInteger(Key, Value);
      ktCurrency: r.WriteCurrency(Key, Value);
      ktDate: r.WriteDate(Key, Value);
      ktTime: r.WriteTime(Key, Value);
      ktDateTime: r.WriteDateTime(Key, Value);
      //ktBinary: Result := r.WriteBinaryData(Key);
    end;
  finally
    r.Free;
  end;
end;

procedure TUnDmkRegistry.WriteAppKeyLM(const Key, App: String; const Value: Variant;
  KeyType: TKeyType);
var
  r : TRegistry;
begin
  r := TRegistry.Create(KEY_ALL_ACCESS);
  r.RootKey := HKEY_LOCAL_MACHINE;
  try
    r.OpenKey('Software\'+App(*Application.Title*), True);
    case KeyType of
      ktString: r.WriteString(Key, Value);
      ktBoolean: r.WriteBool(Key, Value);
      ktInteger: r.WriteInteger(Key, Value);
      ktCurrency: r.WriteCurrency(Key, Value);
      ktDate: r.WriteDate(Key, Value);
      ktTime: r.WriteTime(Key, Value);
      ktDateTime: r.WriteDateTime(Key, Value);
      //ktBinary: Result := r.WriteBinaryData(Key);
    end;
  finally
    r.Free;
  end;
end;

procedure TUnDmkRegistry.WriteAppKeyLM2(const Key, App: String; const Value: Variant;
  KeyType: TKeyType);
var
  r: TRegistry;
  Local: String;
  Res: Boolean;
begin
  r         := TRegistry.Create(KEY_READ);
  r.RootKey := HKEY_LOCAL_MACHINE;
  try
    Local := 'Software\' + App;
    //
    r.Access := KEY_WRITE;
    Res := r.OpenKey(Local, True);
    //
    if not Res then
    begin
      (*MB_Erro*)ShowMessage('N�o � poss�vel criar chave!' + sLineBreak +
        'Verifique se voc� est� conectado com um usu�rio do tipo Adminsitrador!');
      Exit();
    end else
    begin
      case KeyType of
        ktString:
          r.WriteString(Key, Value);
        ktBoolean:
          r.WriteBool(Key, Value);
        ktInteger:
          r.WriteInteger(Key, Value);
        ktCurrency:
          r.WriteCurrency(Key, Value);
        ktDate:
          r.WriteDate(Key, Value);
        ktTime:
          r.WriteTime(Key, Value);
        ktDateTime:
          r.WriteDateTime(Key, Value);
        //ktBinary:
          //Result := r.WriteBinaryData(Key);
      end;
    end;
  finally
    r.CloseKey;
    r.Free;
  end;
end;

end.
