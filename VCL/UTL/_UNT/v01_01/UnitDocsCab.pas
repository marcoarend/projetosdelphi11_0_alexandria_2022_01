unit UnitDocsCab;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db,
  DbCtrls, UnInternalConsts2, Variants;

type
  TUnitDocsCab = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DefineVariavelEmFormTDIPnl(
      Form: TForm; (*ArrVar: array of Variant;*) Index: Integer; Valor: Variant;
      ZeroIsNull: Boolean);
  end;

var
  UnDocsCab: TUnitDocsCab;

implementation

uses DocsCab;

{ TUnitDocsCab }

procedure TUnitDocsCab.DefineVariavelEmFormTDIPnl(Form: TForm;
  (*ArrVar: array of Variant;*) Index: Integer; Valor: Variant;
  ZeroIsNull: Boolean);
var
  Inteiro: Integer;
  Res: Variant;
begin
  if Form <> nil then
  begin
    Res := Valor;
    if Res <> Null then
    begin
    //if VarType(Valor) = vtInteger then
    //begin
      Inteiro := Valor;
      if ZeroIsNull and (Inteiro = 0) then
        Res := Null
      else
        Res := Valor;
    //end else
      //Res := Valor;
    end;
    TFmDocsCab(Form).FLinkID1[Index] := Res;
  end;
end;

end.
