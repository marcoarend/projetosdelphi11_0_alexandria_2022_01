unit UnDmkSystem;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, UnMsgInt,
  Variants, MaskUtils, RTLConsts, SHFolder;
//const
type
  TUnDmkSystem = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function DiretoriodoSystema(Preferencial: Integer; Complemento,
             DirAlternativo: String): string;
  end;

var
  DmkSystem: TUnDmkSystem;

implementation

(*
  CSIDL_PERSONAL = $0005; { My Documents }
  CSIDL_APPDATA = $001A; { Application Data, new for NT4 }
  CSIDL_LOCAL_APPDATA = $001C; { non roaming, user\Local Settings\Application Data }
  CSIDL_INTERNET_CACHE = $0020;
  CSIDL_COOKIES = $0021;
  CSIDL_HISTORY = $0022;
  CSIDL_COMMON_APPDATA = $0023; { All Users\Application Data }
  CSIDL_WINDOWS = $0024; { GetWindowsDirectory() }
  CSIDL_SYSTEM = $0025; { GetSystemDirectory() }
  CSIDL_PROGRAM_FILES = $0026; { C:\Program Files }
  CSIDL_MYPICTURES = $0027; { My Pictures, new for Win2K }
  CSIDL_PROGRAM_FILES_COMMON = $002b; { C:\Program Files\Common }
  CSIDL_COMMON_DOCUMENTS = $002e; { All Users\Documents }
  CSIDL_FLAG_CREATE = $8000; { new for Win2K, or this in to force creation of folder }
  CSIDL_COMMON_ADMINTOOLS = $002f; { All Users\Start Menu\Programs\Administrative Tools }
  CSIDL_ADMINTOOLS = $0030; { <user name>\Start Menu\Programs\Administrative Tools }
*)
function TUnDmkSystem.DiretoriodoSystema(Preferencial: Integer; Complemento,
DirAlternativo: String): string;
var
  P: PChar;
begin
  P := nil;
  try
    P := AllocMem(MAX_PATH);
    if SHGetFolderPath(0, Preferencial, 0, 0, P) = S_OK then
      Result := P + '\' + Complemento
    else
      Result := DirAlternativo;
  finally
    FreeMem(P);
  end;
end;
// CSIDL's: for GetSpecialFolderPath(CSIDL_APPDATA)
// CSIDL_ADMINTOOLS "administrative tools " = 48
// CSIDL_ALTSTARTUP "user's nonlocalized Startup program group" = 29
// CSIDL_APPDATA "common repository for application-specific data (must have IE 4 or above) = 26
// CSIDL_BITBUCKET "Recycle Bin" = 10
// CSIDL_CDBURN_AREA "staging area for files written to CD" = 59
// CSIDL_COMMON_ADMINTOOLS "administrative tools for all users" = 47
// CSIDL_COMMON_ALTSTARTUP "nonlocalized Startup program group for all users" = 30
// CSIDL_COMMON_APPDATA "application data for all users" = 35
// CSIDL_COMMON_DESKTOPDIRECTORY "files and folders that appear on the desktop for all users (NT and above)" = 25
// CSIDL_COMMON_DOCUMENTS "documents that are common to all users (NT and above systems and 95 - 98 with Shfolder.dll)" = 46
// CSIDL_COMMON_FAVORITES "common repository for favorite items (NT and above)" = 31
// CSIDL_COMMON_MUSIC "music files common to all users" = 53
// CSIDL_COMMON_PICTURES "image files common to all users" = 54
// CSIDL_COMMON_PROGRAMS "common program groups on the Start menu for all users (NT and above)" = 23
// CSIDL_COMMON_STARTMENU "programs and folders on the Start menu for all users (NT and above)" = 22
// CSIDL_COMMON_STARTUP "Startup folder for all users (NT and above)" = 24
// CSIDL_COMMON_TEMPLATES "templates that are available to all users (NT and above)" = 45
// CSIDL_COMMON_VIDEO "video files common to all users" = 55
// CSIDL_CONTROLS "Control Panel" = 3
// CSIDL_COOKIES "Internet cookies" = 33
// CSIDL_DESKTOP "Windows desktop, the root of the namespace" = 0
// CSIDL_DESKTOPDIRECTORY "directory used to physically store file objects on the desktop" = 16
// CSIDL_DRIVES "My Computer" = 17
// CSIDL_FAVORITES "user's favorite items" = 6
// CSIDL_FONTS "folder containing fonts" = 20
// CSIDL_HISTORY "Internet history items" = 34
// CSIDL_INTERNET "virtual folder representing the Internet" = 1
// CSIDL_INTERNET_CACHE "temporary Internet files" = 32
// CSIDL_LOCAL_APPDATA "data repository for local (nonroaming) applications" = 28
// CSIDL_MYDOCUMENTS "Documents" = 12
// CSIDL_MYMUSIC "music files." = 13
// CSIDL_MYPICTURES "image files" = 39
// CSIDL_MYVIDEO "video files" = 14
// CSIDL_NETHOOD "Network Places" = 19
// CSIDL_NETWORK "Network Neighborhood" = 18
// CSIDL_PERSONAL "equivalent to CSIDL_MYDOCUMENTS" = 5
// Fim

end.
