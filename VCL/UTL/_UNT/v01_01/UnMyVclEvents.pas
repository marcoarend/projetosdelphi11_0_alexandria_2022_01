unit UnMyVclEvents;

interface

uses
  Windows, Messages, Classes, SysUtils, Dialogs,
  Vcl.Forms, Vcl.Controls, Vcl.StdCtrls,
  (*,
  Graphics,
  ExtCtrls, Buttons, Mask, ExtDlgs,
  //Grids, DBGrids, Menus,  ComCtrls,
  Variants*)
  Winapi.PsAPI;
type

   TShiftKeyInfo = record
     shift: Byte ;
     vkey: Byte ;
   end;
   ByteSet = set of 0..7 ;

type

  TMyMouseClickSouce = (mmcsUnknown=0, mmcsLeft=1, mmcsRight=2, mmcsMidle=3,
                        mmcsDblLeft=4, mmcsDblRight=5, mmcsDblMidle=6);
  TMyVclEvents = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ObtemCodigoDeTeclaEspecial(NomeTecla: String): Integer;
    function  ObtemTeclaEspecialDoCodigo(Codigo: Integer): String;
    procedure SimulaDigitacaoDeTexto(Texto: String);
    procedure SimulaKeyBoardPress_0_PostKeyEx32Arr(Keys: array of Word; const
              Shift: TShiftState; SpecialKey: Boolean) ;
    procedure SimulaKeyBoardPress_1_PostKeyEx32(key: Word; const shift:
              TShiftState; specialkey: Boolean);
    procedure SimulaKeyBoardPress_2_WithFocusControl(WithFocusControl:
              TWinControl; Key: Char; KeyEvent: TKeyEvent);
    procedure SimulaMouseClickXY(x, y: Integer; MouseClickSouce:
              TMyMouseClickSouce);
    procedure SimulaMouseClickControl(Control: TWinControl; x, y: Integer;
              MouseClickSouce: TMyMouseClickSouce);
    //
{             Transferido para UnMyProcesses
    function  FindProcessesByName(aKey: String; aList: TList = nil): Integer;
    function  FindProcessesByHProcess(hProcess: Integer): Boolean;
    function  KillProcess(aProcessId: Cardinal): Boolean;
}

  end;

var
  MyVclEvents: TMyVclEvents;

const
  CO_MaxSigleMouseClickSouce = 3; // Left, Right, Center


implementation

{ TMyVclEvents }


{
function TMyVclEvents.FindProcessesByHProcess(hProcess: Integer): Boolean;
var
  hMod: HMODULE;
 cbNeeded: Cardinal;
begin
  Result := False;
  cbNeeded := 0;
   if (hProcess <> 0) then
      Result := (EnumProcessModules(hProcess, @hMod, sizeof(hMod), cbNeeded));
end;

function TMyVclEvents.FindProcessesByName(aKey: String; aList: TList): Integer;
var
 szProcessName : Array [0..1024] of Char;
 ProcessName   : String;
 hProcess      : Integer;
 aProcesses    : Array [0..1024] of DWORD;
 cProcesses    : DWORD;
 cbNeeded      : Cardinal;
 i             : UINT;
 hMod          : HMODULE;
begin

 Result := -1;
 aKey:= lowercase(akey);

 if not (EnumProcesses(@aProcesses, sizeof(aProcesses), cbNeeded)) then
    exit;

 // Calculate how many process identifiers were returned.
 cProcesses := cbNeeded div sizeof(DWORD);

 // Print the name and process identifier for each process.
 for i:= 0 to cProcesses - 1 do
 begin
   szProcessName := 'unknown';
   hProcess := OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ,
                           FALSE, aProcesses[i]);

   // Get the process name.
   if (hProcess <> 0) then
      if (EnumProcessModules(hProcess, @hMod, sizeof(hMod),cbNeeded)) then
         GetModuleBaseName (hProcess, hMod, szProcessName, sizeof(szProcessName));

   ProcessName:= lowercase (szProcessName);

   CloseHandle(hProcess);
   if pos (aKey, ProcessName) <> 0 then
   begin
     result:=aProcesses[i];
     if aList <> nil then
        aList.add(Pointer (aProcesses[i]))
     else
       exit;
   end;
 end;

   if aList <> nil then
    Result:= aList.count;
 //else
    //Result:=0;
end;

function TMyVclEvents.KillProcess(aProcessId: Cardinal): Boolean;
var
  hProcess : integer;
begin
  hProcess:= OpenProcess(PROCESS_ALL_ACCESS, TRUE, aProcessId);
  Result:= False;
  //
  if (hProcess <>0 ) then
  begin
    Result:= TerminateProcess(hProcess, 0);
    exit;
  end;
end;
}

function TMyVclEvents.ObtemCodigoDeTeclaEspecial(NomeTecla: String): Integer;
begin
  if Uppercase(NomeTecla) = Uppercase('CANCEL')              then Result :=   3 else//
  if Uppercase(NomeTecla) = Uppercase('BACK')                then Result :=   8 else//
  if Uppercase(NomeTecla) = Uppercase('TAB')                 then Result :=   9 else
  if Uppercase(NomeTecla) = Uppercase('CLEAR')               then Result :=  12 else
  if Uppercase(NomeTecla) = Uppercase('RETURN')              then Result :=  13 else
  if Uppercase(NomeTecla) = Uppercase('SHIFT')               then Result :=  16 else
  if Uppercase(NomeTecla) = Uppercase('CONTROL')             then Result :=  17 else
  if Uppercase(NomeTecla) = Uppercase('MENU')                then Result :=  18 else
  if Uppercase(NomeTecla) = Uppercase('PAUSE')               then Result :=  19 else
  if Uppercase(NomeTecla) = Uppercase('CAPITAL')             then Result :=  20 else
  if Uppercase(NomeTecla) = Uppercase('ESCAPE')              then Result :=  27 else
  if Uppercase(NomeTecla) = Uppercase('SPACE')               then Result :=  32 else  // = $20
  if Uppercase(NomeTecla) = Uppercase('PRIOR')               then Result :=  33 else
  if Uppercase(NomeTecla) = Uppercase('NEXT')                then Result :=  34 else
  if Uppercase(NomeTecla) = Uppercase('END')                 then Result :=  35 else
  if Uppercase(NomeTecla) = Uppercase('HOME')                then Result :=  36 else //35?
  if Uppercase(NomeTecla) = Uppercase('LEFT')                then Result :=  37 else
  if Uppercase(NomeTecla) = Uppercase('UP')                  then Result :=  38 else
  if Uppercase(NomeTecla) = Uppercase('RIGHT')               then Result :=  39 else
  if Uppercase(NomeTecla) = Uppercase('DOWN')                then Result :=  40 else
  if Uppercase(NomeTecla) = Uppercase('SELECT')              then Result :=  41 else
  if Uppercase(NomeTecla) = Uppercase('PRINT')               then Result :=  42 else
  if Uppercase(NomeTecla) = Uppercase('EXECUTE')             then Result :=  43 else
  if Uppercase(NomeTecla) = Uppercase('SNAPSHOT')            then Result :=  44 else
  if Uppercase(NomeTecla) = Uppercase('INSERT')              then Result :=  45 else
  if Uppercase(NomeTecla) = Uppercase('DELETE')              then Result :=  46 else
  if Uppercase(NomeTecla) = Uppercase('HELP')                then Result :=  47 else
  if Uppercase(NomeTecla) = Uppercase('LWIN')                then Result :=  91 else
  if Uppercase(NomeTecla) = Uppercase('RWIN')                then Result :=  92 else
  if Uppercase(NomeTecla) = Uppercase('APPS')                then Result :=  93 else
  if Uppercase(NomeTecla) = Uppercase('SLEEP')               then Result :=  95 else
  if Uppercase(NomeTecla) = Uppercase('NUMPAD0')             then Result :=  96 else
  if Uppercase(NomeTecla) = Uppercase('NUMPAD1')             then Result :=  97 else
  if Uppercase(NomeTecla) = Uppercase('NUMPAD2')             then Result :=  98 else
  if Uppercase(NomeTecla) = Uppercase('NUMPAD3')             then Result :=  99 else
  if Uppercase(NomeTecla) = Uppercase('NUMPAD4')             then Result := 100 else
  if Uppercase(NomeTecla) = Uppercase('NUMPAD5')             then Result := 101 else
  if Uppercase(NomeTecla) = Uppercase('NUMPAD6')             then Result := 102 else
  if Uppercase(NomeTecla) = Uppercase('NUMPAD7')             then Result := 103 else
  if Uppercase(NomeTecla) = Uppercase('NUMPAD8')             then Result := 104 else
  if Uppercase(NomeTecla) = Uppercase('NUMPAD9')             then Result := 105 else
  if Uppercase(NomeTecla) = Uppercase('MULTIPLY')            then Result := 106 else
  if Uppercase(NomeTecla) = Uppercase('ADD')                 then Result := 107 else
  if Uppercase(NomeTecla) = Uppercase('SEPARATOR')           then Result := 108 else
  if Uppercase(NomeTecla) = Uppercase('SUBTRACT')            then Result := 109 else
  if Uppercase(NomeTecla) = Uppercase('DECIMAL')             then Result := 110 else
  if Uppercase(NomeTecla) = Uppercase('DIVIDE')              then Result := 111 else
  if Uppercase(NomeTecla) = Uppercase('F1')                  then Result := 112 else
  if Uppercase(NomeTecla) = Uppercase('F2')                  then Result := 113 else
  if Uppercase(NomeTecla) = Uppercase('F3')                  then Result := 114 else
  if Uppercase(NomeTecla) = Uppercase('F4')                  then Result := 115 else
  if Uppercase(NomeTecla) = Uppercase('F5')                  then Result := 116 else
  if Uppercase(NomeTecla) = Uppercase('F6')                  then Result := 117 else
  if Uppercase(NomeTecla) = Uppercase('F7')                  then Result := 118 else
  if Uppercase(NomeTecla) = Uppercase('F8')                  then Result := 119 else
  if Uppercase(NomeTecla) = Uppercase('F9')                  then Result := 120 else
  if Uppercase(NomeTecla) = Uppercase('F10')                 then Result := 121 else
  if Uppercase(NomeTecla) = Uppercase('F11')                 then Result := 122 else
  if Uppercase(NomeTecla) = Uppercase('F12')                 then Result := 123 else
  if Uppercase(NomeTecla) = Uppercase('F13')                 then Result := 124 else
  if Uppercase(NomeTecla) = Uppercase('F14')                 then Result := 125 else
  if Uppercase(NomeTecla) = Uppercase('F15')                 then Result := 126 else
  if Uppercase(NomeTecla) = Uppercase('F16')                 then Result := 127 else
  if Uppercase(NomeTecla) = Uppercase('F17')                 then Result := 128 else
  if Uppercase(NomeTecla) = Uppercase('F18')                 then Result := 129 else
  if Uppercase(NomeTecla) = Uppercase('F19')                 then Result := 130 else
  if Uppercase(NomeTecla) = Uppercase('F20')                 then Result := 131 else
  if Uppercase(NomeTecla) = Uppercase('F21')                 then Result := 132 else
  if Uppercase(NomeTecla) = Uppercase('F22')                 then Result := 133 else
  if Uppercase(NomeTecla) = Uppercase('F23')                 then Result := 134 else
  if Uppercase(NomeTecla) = Uppercase('F24')                 then Result := 135 else
  if Uppercase(NomeTecla) = Uppercase('NUMLOCK')             then Result := 144 else
  if Uppercase(NomeTecla) = Uppercase('SCROLL')              then Result := 145 else
  if Uppercase(NomeTecla) = Uppercase('LSHIFT')              then Result := 160 else
  if Uppercase(NomeTecla) = Uppercase('RSHIFT')              then Result := 161 else
  if Uppercase(NomeTecla) = Uppercase('LCONTROL')            then Result := 162 else
  if Uppercase(NomeTecla) = Uppercase('RCONTROL')            then Result := 163 else
  if Uppercase(NomeTecla) = Uppercase('LMENU')               then Result := 164 else //163?
  if Uppercase(NomeTecla) = Uppercase('RMENU')               then Result := 165 else
  if Uppercase(NomeTecla) = Uppercase('BROWSER_BACK')        then Result := 166 else
  if Uppercase(NomeTecla) = Uppercase('BROWSER_FORWARD')     then Result := 167 else
  if Uppercase(NomeTecla) = Uppercase('BROWSER_REFRESH')     then Result := 168 else
  if Uppercase(NomeTecla) = Uppercase('BROWSER_STOP')        then Result := 169 else
  if Uppercase(NomeTecla) = Uppercase('BROWSER_SEARCH')      then Result := 170 else
  if Uppercase(NomeTecla) = Uppercase('BROWSER_FAVORITES')   then Result := 171 else
  if Uppercase(NomeTecla) = Uppercase('BROWSER_HOME')        then Result := 172 else
  if Uppercase(NomeTecla) = Uppercase('VOLUME_MUTE')         then Result := 173 else
  if Uppercase(NomeTecla) = Uppercase('VOLUME_DOWN')         then Result := 174 else
  if Uppercase(NomeTecla) = Uppercase('VOLUME_UP')           then Result := 175 else
  if Uppercase(NomeTecla) = Uppercase('MEDIA_NEXT_TRACK')    then Result := 176 else
  if Uppercase(NomeTecla) = Uppercase('MEDIA_PREV_TRACK')    then Result := 177 else
  if Uppercase(NomeTecla) = Uppercase('MEDIA_STOP')          then Result := 178 else
  if Uppercase(NomeTecla) = Uppercase('MEDIA_PLAY_PAUSE')    then Result := 179 else
  if Uppercase(NomeTecla) = Uppercase('LAUNCH_MAIL')         then Result := 180 else
  if Uppercase(NomeTecla) = Uppercase('LAUNCH_MEDIA_SELECT') then Result := 181 else
  if Uppercase(NomeTecla) = Uppercase('LAUNCH_APP1')         then Result := 182 else
  if Uppercase(NomeTecla) = Uppercase('LAUNCH_APP2')         then Result := 183 else
  if Uppercase(NomeTecla) = Uppercase('PLAY')                then Result := 250 else
  if Uppercase(NomeTecla) = Uppercase('ZOOM')                then Result := 251 else
     Result := 0;
end;

function TMyVclEvents.ObtemTeclaEspecialDoCodigo(Codigo: Integer): String;
begin
  case Codigo of
      3: Result := 'CANCEL';
      8: Result := 'BACK';
      9: Result := 'TAB';
     12: Result := 'CLEAR';
     13: Result := 'RETURN';
     16: Result := 'SHIFT';
     17: Result := 'CONTROL';
     18: Result := 'MENU';
     19: Result := 'PAUSE';
     20: Result := 'CAPITAL';
     27: Result := 'ESCAPE';
     $20: Result := 'SPACE';
     33: Result := 'PRIOR';
     34: Result := 'NEXT';
     35: Result := 'END';
     36: Result := 'HOME';
     37: Result := 'LEFT';
     38: Result := 'UP';
     39: Result := 'RIGHT';
     40: Result := 'DOWN';
     41: Result := 'SELECT';
     42: Result := 'PRINT';
     43: Result := 'EXECUTE';
     44: Result := 'SNAPSHOT';
     45: Result := 'INSERT';
     46: Result := 'DELETE';
     47: Result := 'HELP';
     91: Result := 'LWIN';
     92: Result := 'RWIN';
     93: Result := 'APPS';
     95: Result := 'SLEEP';
     96: Result := 'NUMPAD0';
     97: Result := 'NUMPAD1';
     98: Result := 'NUMPAD2';
     99: Result := 'NUMPAD3';
    100: Result := 'NUMPAD4';
    101: Result := 'NUMPAD5';
    102: Result := 'NUMPAD6';
    103: Result := 'NUMPAD7';
    104: Result := 'NUMPAD8';
    105: Result := 'NUMPAD9';
    106: Result := 'MULTIPLY';
    107: Result := 'ADD';
    108: Result := 'SEPARATOR';
    109: Result := 'SUBTRACT';
    110: Result := 'DECIMAL';
    111: Result := 'DIVIDE';
    112: Result := 'F1';
    113: Result := 'F2';
    114: Result := 'F3';
    115: Result := 'F4';
    116: Result := 'F5';
    117: Result := 'F6';
    118: Result := 'F7';
    119: Result := 'F8';
    120: Result := 'F9';
    121: Result := 'F10';
    122: Result := 'F11';
    123: Result := 'F12';
    124: Result := 'F13';
    125: Result := 'F14';
    126: Result := 'F15';
    127: Result := 'F16';
    128: Result := 'F17';
    129: Result := 'F18';
    130: Result := 'F19';
    131: Result := 'F20';
    132: Result := 'F21';
    133: Result := 'F22';
    134: Result := 'F23';
    135: Result := 'F24';
    144: Result := 'NUMLOCK';
    145: Result := 'SCROLL';
    160: Result := 'LSHIFT';
    161: Result := 'RSHIFT';
    162: Result := 'LCONTROL';
    163: Result := 'RCONTROL';
    164: Result := 'LMENU';
    165: Result := 'RMENU';
    166: Result := 'BROWSER_BACK';
    167: Result := 'BROWSER_FORWARD';
    168: Result := 'BROWSER_REFRESH';
    169: Result := 'BROWSER_STOP';
    170: Result := 'BROWSER_SEARCH';
    171: Result := 'BROWSER_FAVORITES';
    172: Result := 'BROWSER_HOME';
    173: Result := 'VOLUME_MUTE';
    174: Result := 'VOLUME_DOWN';
    175: Result := 'VOLUME_UP';
    176: Result := 'MEDIA_NEXT_TRACK';
    177: Result := 'MEDIA_PREV_TRACK';
    178: Result := 'MEDIA_STOP';
    179: Result := 'MEDIA_PLAY_PAUSE';
    180: Result := 'LAUNCH_MAIL';
    181: Result := 'LAUNCH_MEDIA_SELECT';
    182: Result := 'LAUNCH_APP1';
    183: Result := 'LAUNCH_APP2';
    250: Result := 'PLAY';
    251: Result := 'ZOOM';
    else Result := '';
  end;
end;

procedure TMyVclEvents.SimulaDigitacaoDeTexto(Texto: String);
var
  I: Integer;
  s: String;
  o, v0, v1: Word;
  us, s0: Boolean;
begin
  //Memo1.Lines.Clear;
  //Texto := EdTexto1.ValueVariant;
  //EdTexto2.SetFocus;
  for I := 1 to Length(Texto) do
  begin
    v0 := 0;
    V1 := 1;
    //Memo1.Lines.Add(IntToStr(I) + ' - ');
    s := Texto[I];
    if s = ' ' then begin v1 := 32; us := False; end else
    if s = '(' then begin v1 := 57; us := True; end else
    if s = '*' then begin v1 := 56; us := True; end else
    if s = '&' then begin v1 := 55; us := True; end else
    if s = '�' then begin v1 := 54; us := True; end else
    if s = '%' then begin v1 := 53; us := True; end else  // Acentua!
    if s = '$' then begin v1 := 52; us := True; end else
    if s = '#' then begin v1 := 51; us := True; end else
    if s = '@' then begin v1 := 50; us := True; end else
    if s = '!' then begin v1 := 49; us := True; end else
    if s = ')' then begin v1 := 48; us := True; end else
    // Num Pad
    if s = '/' then begin v1 := 111; us := False; end else
    //if s = '.' then begin v1 := 110; us := False; end else  Virgula
    if s = '-' then begin v1 := 109; us := False; end else
    //if s = ' ' then begin v1 := 108; us := False; end else
    if s = '+' then begin v1 := 107; us := False; end else
    if s = '*' then begin v1 := 106; us := False; end else


(*    // NumLock -> 144
112: Coloca(' [F1] ');
113: Coloca(' [F2] ');
114: Coloca(' [F3] ');
115: Coloca(' [F4] ');
116: Coloca(' [F5] ');
117: Coloca(' [F6] ');
118: Coloca(' [F7] ');
119: Coloca(' [F8] ');
120: Coloca(' [F9] ');
121: Coloca(' [F10] ');
122: Coloca(' [F11] ');
123: Coloca(' [F12] ');
144: Coloca(' [NUMLCK] ');
*)

    if s = '.' then begin v1 := 194; us := True; end else
    if s = '/' then begin v1 := 193; us := True; end else
    if s = '''' then begin v1 := 192; us := True; end else
    if s = ';' then begin v1 := 191; us := False; end else
    if s = '.' then begin v1 := 190; us := False; end else
    if s = '-' then begin v1 := 189; us := False; end else
    if s = ',' then begin v1 := 188; us := False; end else
    if s = '=' then begin v1 := 187; us := False; end else
    if s = '�' then begin v1 := 186; us := False; end else

    if s = '.' then begin v1 := 194; us := True; end else
    if s = '?' then begin v1 := 193; us := True; end else
    if s = '"' then begin v1 := 192; us := True; end else
    if s = ':' then begin v1 := 191; us := True; end else
    if s = '>' then begin v1 := 190; us := True; end else
    if s = '_' then begin v1 := 189; us := True; end else
    if s = '<' then begin v1 := 188; us := True; end else
    if s = '+' then begin v1 := 187; us := True; end else
    if s = '�' then begin v1 := 186; us := True; end else

    //219 + Shift = crase
    if (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�') then
    begin
      v0 := 219;
      s0 := True;
      if (s = '�') then begin v1 := 65; us := False; end else
      if (s = '�') then begin v1 := 69; us := False; end else
      if (s = '�') then begin v1 := 73; us := False; end else
      if (s = '�') then begin v1 := 79; us := False; end else
      if (s = '�') then begin v1 := 85; us := False; end else
      if (s = '�') then begin v1 := 65; us := True; end else
      if (s = '�') then begin v1 := 69; us := True; end else
      if (s = '�') then begin v1 := 73; us := True; end else
      if (s = '�') then begin v1 := 79; us := True; end else
      if (s = '�') then begin v1 := 85; us := True; end;
    end
    else
    //219 = acento agudo
    if (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�') then
    begin
      v0 := 219;
      s0 := False;
      if (s = '�') then begin v1 := 65; us := False; end else
      if (s = '�') then begin v1 := 69; us := False; end else
      if (s = '�') then begin v1 := 73; us := False; end else
      if (s = '�') then begin v1 := 79; us := False; end else
      if (s = '�') then begin v1 := 85; us := False; end else
      if (s = '�') then begin v1 := 65; us := True; end else
      if (s = '�') then begin v1 := 69; us := True; end else
      if (s = '�') then begin v1 := 73; us := True; end else
      if (s = '�') then begin v1 := 79; us := True; end else
      if (s = '�') then begin v1 := 85; us := True; end;
    end
    else
    //222 + Shift = ^
    if (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�') then
    begin
      v0 := 222;
      s0 := True;
      if (s = '�') then begin v1 := 65; us := False; end else
      if (s = '�') then begin v1 := 69; us := False; end else
      if (s = '�') then begin v1 := 73; us := False; end else
      if (s = '�') then begin v1 := 79; us := False; end else
      if (s = '�') then begin v1 := 85; us := False; end else
      if (s = '�') then begin v1 := 65; us := True; end else
      if (s = '�') then begin v1 := 69; us := True; end else
      if (s = '�') then begin v1 := 73; us := True; end else
      if (s = '�') then begin v1 := 79; us := True; end else
      if (s = '�') then begin v1 := 85; us := True; end;
    end
    else
    //222  =
    if (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�') then
    begin
      v0 := 222;
      s0 := False;
      if (s = '�') then begin v1 := 65; us := False; end else
      if (s = '�') then begin v1 := 79; us := False; end else
      if (s = '�') then begin v1 := 65; us := True; end else
      if (s = '�') then begin v1 := 79; us := True; end;
    end
    else
    if s = '[' then begin v1 := 221; us := False; end else
    if s = ']' then begin v1 := 220; us := False; end else
    if s = '{' then begin v1 := 221; us := True; end else
    if s = '}' then begin v1 := 220; us := True; end else


    if s = '\' then begin v1 := 226; us := False; end else
    if s = '|' then begin v1 := 226; us := True; end else


    //if s = '' then begin v1 := ; us := True; end else
    begin
      o := Ord(s[1]); //Ord(s);
      case o of
        // 96..105
        48..57  : begin v1 := o + 48; us := False; end; // 96.. (0 a 9)
        65..90  : begin v1 := o + 00; us :=  True; end; // MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(o, [ssShift], False);  // Uppercase
        97..122 : begin v1 := o - 32; us :=  False; end; // MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(o-32, [], False);     // Lowercase
      end;
    end;
    if v0 > 0 then
    begin
      if s0 then
        MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(v0, [ssShift], False)
      else
        MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(v0, [], False);
    end;
    //
    if us then
      MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(v1, [ssShift], False)
    else
      MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(v1, [], False);
    //
    Sleep(25);
    Application.ProcessMessages();
    //Memo1.Text := Memo1.Text + sLineBreak;
  end;
end;

procedure TMyVclEvents.SimulaKeyBoardPress_0_PostKeyEx32Arr(Keys: array of Word;
  const Shift: TShiftState; SpecialKey: Boolean);
{
Parameters :
* keys : Array of virtual keycode of the key to send. For printable keys this is simply the ANSI code (Ord(character)) .
* shift : state of the modifier keys. This is a set, so you can set several of these keys (shift, control, alt, mouse buttons) in tandem. The TShiftState type is declared in the Classes Unit.
* specialkey: normally this should be False. Set it to True to specify a key on the numeric keypad, for example.

Description:
Uses keybd_event to manufacture a series of key events matching the passed parameters. The events go to the control with focus. Note that for characters key is always the upper-case version of the character. Sending without any modifier keys will result in a lower-case character, sending it with [ ssShift ] will result in an upper-case character!
}
const
   shiftkeys: array [1..3] of TShiftKeyInfo =
     ((shift: Ord(ssCtrl) ; vkey: VK_CONTROL),
     (shift: Ord(ssShift) ; vkey: VK_SHIFT),
     (shift: Ord(ssAlt) ; vkey: VK_MENU)) ;
var
   flag: DWORD;
   bShift: ByteSet absolute shift;
   i, j: Integer;
begin
   for j := 1 to 3 do
   begin
     if shiftkeys[j].shift in bShift then
       keybd_event(shiftkeys[j].vkey, MapVirtualKey(shiftkeys[j].vkey, 0), 0, 0) ;
   end;
   if specialkey then
     flag := KEYEVENTF_EXTENDEDKEY
   else
     flag := 0;

   for i := Low(Keys) to High(Keys) do
   begin
     keybd_event(keys[i], MapvirtualKey(keys[i], 0), flag, 0);
   end;

   flag := flag or KEYEVENTF_KEYUP;

   for i := Low(Keys) to High(Keys) do
   begin
     keybd_event(keys[i], MapvirtualKey(keys[i], 0), flag, 0) ;
   end;

   for j := 3 downto 1 do
   begin
     if shiftkeys[j].shift in bShift then
       keybd_event(shiftkeys[j].vkey, MapVirtualKey(shiftkeys[j].vkey, 0), KEYEVENTF_KEYUP, 0) ;
   end;
end;


procedure TMyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(key: Word;
  const shift: TShiftState; specialkey: Boolean);

(*
////////////////////// Example: ////////////////////////////////////////////////
procedure TForm1.Button1Click(Sender: TObject);
begin
  //Pressing the Left Windows Key
  PostKeyEx32(VK_LWIN, [], False);

  //Pressing the letter D
  PostKeyEx32(Ord('D'), [], False);

  //Pressing Ctrl-Alt-C
  PostKeyEx32(Ord('C'), [ssctrl, ssAlt], False);
end;
////////////////////// Fim Example /////////////////////////////////////////////
*)

{************************************************************
* Procedure PostKeyEx32
*
* Parameters:
*  key    : virtual keycode of the key to send. For printable
*           keys this is simply the ANSI code (Ord(character)).
*  shift  : state of the modifier keys. This is a set, so you
*           can set several of these keys (shift, control, alt,
*           mouse buttons) in tandem. The TShiftState type is
*           declared in the Classes Unit.
*  specialkey: normally this should be False. Set it to True to
*           specify a key on the numeric keypad, for example.
* Description:
*  Uses keybd_event to manufacture a series of key events matching
*  the passed parameters. The events go to the control with focus.
*  Note that for characters key is always the upper-case version of
*  the character. Sending without any modifier keys will result in
*  a lower-case character, sending it with [ssShift] will result
*  in an upper-case character!
// Code by P. Below
************************************************************}
type
  TShiftKeyInfo = record
    shift: Byte;
    vkey: Byte;
  end;
  byteset = set of 0..7;
const
  shiftkeys: array [1..3] of TShiftKeyInfo =
    ((shift: Ord(ssCtrl); vkey: VK_CONTROL),
    (shift: Ord(ssShift); vkey: VK_SHIFT),
    (shift: Ord(ssAlt); vkey: VK_MENU));
var
  flag: DWORD;
  bShift: ByteSet absolute shift;
  i: Integer;
begin
  for i := 1 to 3 do
  begin
    if shiftkeys[i].shift in bShift then
      keybd_event(shiftkeys[i].vkey, MapVirtualKey(shiftkeys[i].vkey, 0), 0, 0);
  end; { For }
  if specialkey then
    flag := KEYEVENTF_EXTENDEDKEY
  else
    flag := 0;

  keybd_event(key, MapvirtualKey(key, 0), flag, 0);
  flag := flag or KEYEVENTF_KEYUP;
  keybd_event(key, MapvirtualKey(key, 0), flag, 0);

  for i := 3 downto 1 do
  begin
    if shiftkeys[i].shift in bShift then
      keybd_event(shiftkeys[i].vkey, MapVirtualKey(shiftkeys[i].vkey, 0),
        KEYEVENTF_KEYUP, 0);
  end; { For }
end; { PostKeyEx32 }


procedure TMyVclEvents.SimulaKeyBoardPress_2_WithFocusControl(
  WithFocusControl: TWinControl; Key: Char; KeyEvent: TKeyEvent);
{************************************************************}
{2. With keybd_event API}
begin
  {or you can also try this simple example to send any
   amount of keystrokes at the same time. }

  {Pressing the A Key and showing it in the Edit1.Text}
  //Edit1.SetFocus;
  TWinControl(WithFocusControl).SetFocus;
  keybd_event(VK_SHIFT, 0, 0, 0);
  keybd_event(Ord('A'), 0, 0, 0);
  keybd_event(VK_SHIFT, 0, KEYEVENTF_KEYUP, 0);

  {Presses the Left Window Key and starts the Run}
  keybd_event(VK_LWIN, 0, 0, 0);
  keybd_event(Ord('R'), 0, 0, 0);
  keybd_event(VK_LWIN, 0, KEYEVENTF_KEYUP, 0);
end;

procedure TMyVclEvents.SimulaMouseClickControl(Control: TWinControl; x, y: Integer;
  MouseClickSouce: TMyMouseClickSouce);
const
  sProcName = 'TMyVclEvents.SimulaMouseClickControl()';
begin
  case MouseClickSouce of
    TMyMouseClickSouce. mmcsLeft:
      SendMessage(Control.Handle, WM_LBUTTONDOWN, x, y);
    TMyMouseClickSouce. mmcsRight:
      SendMessage(Control.Handle, WM_RBUTTONDOWN, x, y);
    TMyMouseClickSouce.mmcsDblLeft:
      SendMessage(Control.Handle, WM_LBUTTONDBLCLK, x, y);
    else
      //Geral.MB_Erro('"TMyMouseClickSouce" n�o implementado em ' + sProcName);
      ShowMessage('"TMyMouseClickSouce" n�o implementado em ' + sProcName);
  end;
end;

procedure TMyVclEvents.SimulaMouseClickXY(x, y: Integer;
  MouseClickSouce: TMyMouseClickSouce);
const
  sProcName = 'TMyVclEvents.SimulaMouseClickXY()';
begin
// Set the mouse cursor to position x,y:
// Maus an Position x,y setzen:
  SetCursorPos(x, y);

  case MouseClickSouce of
    TMyMouseClickSouce.mmcsLeft:
    begin
    // Simulate the left mouse button down
    // Linke Maustaste simulieren
      mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
      mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
    end;
    TMyMouseClickSouce.mmcsRight:
    begin
    // Simulate the right mouse button down
    // Rechte Maustaste simulieren
      mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
      mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
    end;
    TMyMouseClickSouce.mmcsMidle:
    begin
    // Simulate the right mouse button down
    // Rechte Maustaste simulieren
      mouse_event(MOUSEEVENTF_MIDDLEDOWN, 0, 0, 0, 0);
      mouse_event(MOUSEEVENTF_MIDDLEUP, 0, 0, 0, 0);
    end;
    TMyMouseClickSouce.mmcsDblLeft:
    begin
    // Simulate a double click
    // Einen Doppelklick simulieren
      mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
      mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
      GetDoubleClickTime;
      mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
      mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
    end;
    TMyMouseClickSouce.mmcsDblRight:
    begin
      mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
      mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
      GetDoubleClickTime;
      mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
      mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
    end;
    TMyMouseClickSouce.mmcsDblMidle:
    begin
      mouse_event(MOUSEEVENTF_MIDDLEDOWN, 0, 0, 0, 0);
      mouse_event(MOUSEEVENTF_MIDDLEUP, 0, 0, 0, 0);
      GetDoubleClickTime;
      mouse_event(MOUSEEVENTF_MIDDLEDOWN, 0, 0, 0, 0);
      mouse_event(MOUSEEVENTF_MIDDLEUP, 0, 0, 0, 0);
    end;
    else
      //Geral.MB_Erro('"TMyMouseClickSouce" n�o implementado em ' + sProcName);
      ShowMessage('"TMyMouseClickSouce" n�o implementado em ' + sProcName);
  end;
end;

end.
