unit UnLic_Dmk;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, comctrls, frxClass, frxDBSet, stdctrls, dmkGeral, dmkLabel,
  dmkEdit, Variants, (*&ABSMain,*) dmkEditCB, dmkDBLookupComboBox, Menus,
  UnInternalConsts, UnInternalConsts2, ExtCtrls, IdBaseComponent, IdComponent,
  IdRawBase, IdRawClient, IdIcmpClient, Registry, WinInet,
  //JwaIpHlpApi, JwaIpTypes,
  nb30, UnDMkEnums, UnProjGroup_Consts;

type
  //TWMacId = array [0..MAX_ADAPTER_ADDRESS_LENGTH - 1] of BYTE;
  TUnLic_Dmk = class(TObject)
  private
    { Private declarations }
    procedure AtualizaVencimentoDaLicenca();
    function  ReopenTabelasSite(CNPJ_Dono: String): Boolean;
    function  ConectouADermatek(Avisa: Boolean): Boolean;
    function  CriaFmUnLock_Dmk(DataF: TDateTime): Boolean;
(* Reativar no DControl?
    function  CriaFmUnLock_MLA(): Boolean;
*)
    //function  DestravaLoc(DataFinal: TDateTime): Boolean;
    function  DestravaLoc4(DataFinal: TDateTime; CNPJ, RazaoSocial, NomePC,
              Telefone, EMail: String): Boolean;
    //function  LiberaUso3(NomeServer: String; SoLocal: Boolean): Boolean;
    function  LiberaUso4(SoLocal: Boolean): Boolean;
    function  VerificaLiberacaoLoc4(): Boolean;
    function  VerificaLiberacaoNet4(): Boolean;
    function  ChecaMonitoramento(): Byte;
    function  CalculaValMonitoramento(CNPJ: String; ValMonit: Byte): String;
    // Componentes
    procedure TimerTimer(Sender: TObject);
    procedure IdIcmpClientReply(ASender: TComponent; const AReplyStatus: TReplyStatus);
  public
    { Public declarations }
    // PROCEDURE PRINCIPAL - Chamar na cria��o do Dmod, ap�s a cria��o do DModG
    procedure VerificaLicencaDoAplicativo(LaAviso1, LaAviso2: TLabel; Form: TForm);
    //
    function  ChecksumLicenca(Data: TDateTime; CNPJ_Dono: String): String;
    function  CalculaValLicenca(const CNPJ_Dono: String; var Licenca:
              String): Boolean;
    function  CalculaValSerialKey(): String;
    function  CalculaValSerialNum(): String;
    function  CalculaValLicInfo(CNPJ_Dono: String; DataF: TDateTime): String;

    procedure MostraUnLock_Dmk(LiberaUso6: Boolean);
    procedure ReverteVersao(NomeAplicativo: String; Handle: THandle);
    //function  ValidaVersao: Boolean;
    function  LiberaUso5(): Boolean;
{$IFDEF UsaREST}
    function  LiberaUso6(): Boolean;
{$EndIf}
  end;

var
  Lic_Dmk: TUnLic_Dmk;
  FTimer_UnLic: TTimer;
  FIdIcmpClient_UnLic: TIdIcmpClient;
  FLaAvisa1, FLaAvisa2: TLabel;

const
  ContatoComDermatek = sLineBreak + 'Entre em contato com a DERMATEK' + sLineBreak +
  'Telefone: (44) 99152 7300' + sLineBreak + 'E-mails: ' + sLineBreak +
  'cleide@dermatek.com.br' + sLineBreak +
  'marcelo@dermatek.com.br' + sLineBreak +
  'dermatek@dermatek.com.br' + sLineBreak;
  //
  FRandStrLic00 = 'o�EWR^FVNRU~Y4TRsI;O3hU4.LIwHE+43QWF=7IHO';
  FRandStrLic01 = 'm8834rj0329-=~~q09w5rqde';
  FRandStrLic02 = ';~�zq-2-w021092354320lxw�j,';
  FRandStrLic03 = '/;.;]~]��[-0-21ski�%rd4in';
  FRandStrLic04 = 'pq25ad4qe7w@#$%wdew]qkf=o33968';
  FRandStrLic05 = 'h}&H4S9]6523qkmfaPopwd�P3RE8DWWEQR21435#@$';


implementation

uses UnMyObjects, Module, ModuleGeral, MyListas, UnLock_Dmk, UMySQLDB,
Terminais, ZCF2, UMySQLModule, MyDBCheck, VerifiDB, (*Principal,*)
 //{$IFDEF USA_UNLOCK_MLA}UnLock_MLA, {$ENDIF}
 {$IFNDEF NAO_USA_UnitMD5} UnitMD5, {$ENDIF}
 {$IFDEF UsaWSuport} UnDmkWeb, {$ENDIF}
GetBiosInformation, UnDmkProcFunc, DmkDAC_PF;

{ TUnLic_Dmk }

var
    //FBloquear: Boolean;
    FRazaoSocial, FNome, FCNPJ, FCPF, FETE1, FPTe1, FEEMail, FPEMail: String;
    FTipo, FCodUsu: Integer;

procedure TUnLic_Dmk.AtualizaVencimentoDaLicenca;
begin
  if ConectouADermatek(True) then
  begin
    DModG.ReopenMaster('TUnLic_Dmk.100');
    //
    DModG.QrEntDmk.Close;
    DModG.QrEntDmk.Params[00].AsString := DModG.QrMasterCNPJ.Value;
    DModG.QrEntDmk.Params[01].AsString := DModG.QrMasterCNPJ.Value;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrEntDmk);
    //
    DModG.QrCliAplic.Close;
    DModG.QrCliAplic.Params[00].AsInteger := DModG.QrEntDmkCodigo.Value;
    DModG.QrCliAplic.Params[01].AsInteger := CO_DMKID_APP;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplic);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE master SET DataF=:P0, ChekF=:P1, DiasExtra=0'); // N�o � dias extras, � tentativas de conex�o
    Dmod.QrUpd.Params[00].AsString := Geral.FDT(DModG.QrCliAplicFimData.Value, 1);
    Dmod.QrUpd.Params[01].AsString := DModG.QrCliAplicFimChek.Value;
    Dmod.QrUpd.ExecSQL;
  end;
end;

(*
function TUnLic_Dmk.ValidaVersao(): Boolean;
var
  VersaoMax: Integer;
begin
  Result := True;
  //
  DModG.ReopenTerminal(True);
  //
  if DModG.QrTerminal.RecordCount > 0 then
  begin
    VersaoMax := Geral.IMV(DModG.QrTerminal.FieldByName('VersaoMax').AsString);
    //
    if (CO_VERSAO > VersaoMax) and (DModG.QrTerminal.FieldByName('VersaoMax').AsString <> '') then
      Result := False;
  end;
end;
*)

procedure TUnLic_Dmk.ReverteVersao(NomeAplicativo: String; Handle: THandle);
const
  sMsg = 'TUnLic_Dmk.ReverteVersao()';
var
  VersaoAtu, VersaoAnt, AppAnt, AppAtu, SerialKey, SerialNum: String;
begin
  DModG.ReopenTerminal(True);
  //
  if DModG.QrTerminal.RecordCount > 0 then
  begin
    VersaoAtu := DModG.QrTerminal.FieldByName('VersaoMax').AsString;
    VersaoAnt := DModG.QrTerminal.FieldByName('VersaoMin').AsString;
    SerialKey := DModG.QrTerminal.FieldByName('SerialKey').AsString;
    SerialNum := DModG.QrTerminal.FieldByName('SerialNum').AsString;
    //
    if VersaoAnt <> '' then
    begin
      AppAnt := ExtractFilePath(Application.ExeName) + NomeAplicativo + VersaoAnt +
                   ExtractFileExt(Application.ExeName);
      AppAtu := ExtractFilePath(Application.ExeName) + NomeAplicativo +
                  ExtractFileExt(Application.ExeName);
      //
      if (FileExists(AppAnt)) and (FileExists(AppAtu)) then
      begin
        if Geral.MB_Pergunta('Confirma a revers�o da vers�o do aplicativo?' + sLineBreak
          + 'ATEN��O: Ao realizar este procedimento ser� restaurada a vers�o instalada antes da �ltima atualiza��o.') = ID_YES then
        begin
          if not dmkPF.RenameAppOlder(AppAtu) then
          begin
            Geral.MB_Aviso('Erro... N�o foi poss�vel renomear o arquivo!' +
              sLineBreak + 'Processo de revers�o abortado!');
            Exit;
          end;
          if not RenameFile(AppAnt, AppAtu) then
          begin
            Geral.MB_Aviso('Erro... N�o foi poss�vel renomear o arquivo!' +
              sLineBreak + 'Processo de revers�o abortado!');
            Exit;
          end;
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
            'UPDATE terminais SET VersaoMin="", VersaoMax="' + VersaoAnt + '" ',
            'WHERE SerialKey="' + SerialKey + '" AND SerialNum="' + SerialNum + '"',
            '']);
          //- 1 para for�ar a verifica��o do banco de dados ao abrir
          // ini 2022-01-26
          //Geral.WriteAppKeyLM2('Versao', Application.Title, Geral.IMV(VersaoAnt) - 1, ktInteger);
          {$ifDef VER350} // Delphi 28 Alexandria
              Geral.WriteAppKeyCU('Vers28', Application.Title, Geral.IMV(VersaoAnt) - 1, ktString);
              Geral.WriteAppKeyLM2('Vers28', Application.Title, Geral.IMV(VersaoAnt) - 1, ktString);
          {$Else}
            try
              Geral.WriteAppKeyCU('Versao', Application.Title, Geral.IMV(VersaoAnt) - 1, ktInteger);
              Geral.WriteAppKeyLM2('Versao', Application.Title, Geral.IMV(VersaoAnt) - 1, ktInteger);
            except
              on E: exception do Geral.ErroVersaoDoCompilador(sMsg, E.Message);
            end;
          {$EndIf}
          // fim 2022-01-26
          //
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
            'UPDATE controle SET Versao = "' + VersaoAnt + '"',
            '']);
          //
          dmkPF.FecharOutraAplicacao(NomeAplicativo, Handle);
          dmkPF.WinExecAndWaitOrNot(AppAtu, 0, nil, False);
          // For�ar encerramento
          Halt(0);
        end;
      end else
        Geral.MB_Aviso('N�o foi poss�vel localizar a vers�o anterior!' +
          sLineBreak + 'Processo de revers�o abortado!');
    end else
      Geral.MB_Aviso('N�o � poss�vel fazer a revers�o a partir desta vers�o!');
  end;
end;

function TUnLic_Dmk.CalculaValLicenca(const CNPJ_Dono: String; var Licenca:
String): Boolean;
var
  ServSK, ServSN: String;
begin
  Result  := False;
  Licenca := '';
  if Trim(CNPJ_Dono) = '' then
  begin
    Geral.MB_Aviso('CNPJ principal n�o definido!');
    Exit;
  end;
  ServSK  := '';
  ServSN  := '';
  Licenca := '';
  if DModG.EhOServidor() then
  begin
    ServSK := DModG.FSerialKey;
    ServSN := DModG.FSerialNum;
  end else
  begin
    DModG.QrTermiServ.Close;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrTermiServ);
    case DModG.QrTermiServ.RecordCount of
      0:
      begin
        Geral.MB_Aviso('Terminal servidor n�o definido!');
        Exit;
      end;
      1:
      begin
        ServSK := DModG.QrTermiServSerialKey.Value;
        ServSN := DModG.QrTermiServSerialNum.Value;
      end;
      else
      begin
        Geral.MB_Aviso('H� mais de um terminal servidor definido!');
        Exit;
      end;
    end;
  end;
  //
  if (Length(Trim(ServSN)) <> 32) or (Length(Trim(ServSK)) <> 32) then
  begin
    Geral.MB_Erro('Serial com problemas [1]');
    Exit;
  end else begin
    {$IFNDEF NAO_USA_UnitMD5}
      Licenca := UnMD5.StrMD5(CNPJ_Dono + ServSK + ServSN + FRandStrLic04);
      Result := True;
    {$ELSE}
      Result := False;
    {$ENDIF}
  end;
end;

function TUnLic_Dmk.CalculaValLicInfo(CNPJ_Dono: String; DataF: TDateTime): String;
var
  DataTxt: String;
begin
{$IFNDEF NAO_USA_UnitMD5}
  DataTxt := Geral.FDT(DataF, 1);
  Result := UnMD5.StrMD5(CNPJ_Dono + DataTxt + FRandStrLic05);
{$ELSE}
  Result := '';
{$ENDIF}
end;

function TUnLic_Dmk.CalculaValMonitoramento(CNPJ: String;
  ValMonit: Byte): String;
begin
{$IFNDEF NAO_USA_UnitMD5}
  Result := UnMD5.StrMD5(
    CNPJ + FormatFloat('0', ValMonit) + FRandStrLic01);
{$ELSE}
  Result := '';
{$ENDIF}
end;

function TUnLic_Dmk.CalculaValSerialKey(): String;
var
  Dump: TRomBiosDump;
  Txt: WideString;
begin
  if TOSInfo.IsWOW64 = True then
  begin

    // Running on 64-bit OS!!
{ TODO 5 : Ver o que fazer. }
    Result := '000000000';
  end else
  begin

   //  NOT running on 64-bit OS!!

    if ReadRomBios(Dump, rrbmAutomatic) then
    begin
      Txt := 'Data OffSet = ' + GetRomBiosString(Dump, RomBiosDateOffset) +
      'Nome OffSet = ' + GetRomBiosString(Dump, RomBiosNameOffset) +
      '(R) OffSet = ' + GetRomBiosString(Dump, RomBiosCopyrightOffset) +
      'ID OffSet = ' + GetRomBiosString(Dump, RomBiosIdOffset);
{$IFNDEF NAO_USA_UnitMD5}
      Result := UnMD5.StrMD5(Txt + FRandStrLic02);
{$ELSE}
      Result := '';
{$ENDIF}
    end else
    begin
      Geral.MB_Aviso('"SerialKey" inv�lido!');
      Result := CalculaValSerialNum();
    end;
  end;
end;

function TUnLic_Dmk.CalculaValSerialNum(): String;
var
  Serial: String;
begin
{$IFNDEF NAO_USA_UnitMD5}
  Serial := FormatFloat('0', dmkPF.GetDiskSerialNumber('C'));
  Result := UnMD5.StrMD5(Serial + FRandStrLic03);
{$ELSE}
  Result := '';
{$ENDIF}
end;

function TUnLic_Dmk.ChecaMonitoramento(): Byte;
var
  CNPJ, ValorMD5: String;
begin
  if DModG.QrMasterCNPJ.Value = '' then Result := 0 else
  begin
    CNPJ    := DModG.QrMasterCNPJ.Value;
    ValorMD5  := DModG.QrMasterChekMonit.Value;
    if CalculaValMonitoramento(CNPJ, 1) = ValorMD5 then Result := 1 else // sob avalia��o
    if CalculaValMonitoramento(CNPJ, 2) = ValorMD5 then Result := 2 else // aluguel vencido
    if CalculaValMonitoramento(CNPJ, 3) = ValorMD5 then Result := 3 else // sob avalia��o
    if CalculaValMonitoramento(CNPJ, 4) = ValorMD5 then Result := 4 else // Liberado eternamente
    Result := 0; // travado ap�s per�odo de avalia��o
  end;
end;

function TUnLic_Dmk.ChecksumLicenca(Data: TDateTime;
  CNPJ_Dono: String): String;
var
  Data_TXT: String;
begin
{$IFNDEF NAO_USA_UnitMD5}
  Data_TXT := Geral.FDT(Data, 1);
  Result := UnMD5.StrMD5(Data_TXT + FRandStrLic00 + CNPJ_Dono);
{$ELSE}
  Result := '';
{$ENDIF}
end;

function TUnLic_Dmk.ConectouADermatek(Avisa: Boolean): Boolean;
begin
  Result := False;
  if not DModG.DBDmk.Connected then
  begin
    DModG.DBDmk.Disconnect;
    //
    DModG.DBDmk.Host         := DermaBD2_Host;
    DModG.DBDmk.DatabaseName := DermaBD2_DBN;
    DModG.DBDmk.Port         := DermaBD2_Port;
    DModG.DBDmk.UserName     := DermaBD2_UName;
    DModG.DBDmk.UserPassword := DermaBD2_UKhei;
    try
      try
        DModG.DBDmk.Connect;
        Result := True;
      except
        ;
      end;
    finally
      if Avisa then
      begin
        if (FLaAvisa1 <> nil) and Avisa then
        begin
          if not DModG.DBDmk.Connected then
          begin
            FLaAvisa1.Font.Color := clSilver;
            FLaAvisa2.Font.Color := clRed;
            MyObjects.Informa2(FLaAvisa1, FLaAvisa2, False, 'N�o foi poss�vel se conectar � internet!');
          end else begin
            FLaAvisa1.Font.Color := clSilver;
            FLaAvisa2.Font.Color := clBlue;
            MyObjects.Informa2(FLaAvisa1, FLaAvisa2, False, '...');
          end;
        end;
      end;
    end;
  end else Result := True;
end;

function TUnLic_Dmk.CriaFmUnLock_Dmk(DataF: TDateTime): Boolean;
begin
  Result := False;
  if not ConectouADermatek(True) then Exit;
  //
  DModG.ReopenMaster('TUnLic_Dmk.415');
  if DModG.QrMasterLimite.Value < 30 then
  begin
    Dmod.ReopenControle();
    MyObjects.FormCria(TFmUnLock_Dmk, FmUnLock_Dmk);

    FmUnLock_Dmk.FTipLibera := Libera4;

    FmUnLock_Dmk.FContraSenha := Dmod.QrControle.FieldByName('ContraSenha').AsString;
    if (Trim(DModG.QrMasterEm.Value) <> '') and (DModG.QrMasterCNPJ.Value <> '') then
    begin
      FmUnLock_Dmk.EdRazao.Text            := DModG.QrMasterEm.Value;
      FmUnLock_Dmk.EdCNPJ.ValueVariant     := DModG.QrMasterCNPJ.Value;
      FmUnLock_Dmk.EdTelefone.ValueVariant := DModG.QrMasterLicTel.Value;
      FmUnLock_Dmk.EdEMail.Text            := DModG.QrMasterLicMail.Value;
    end else begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT IF(Tipo=0,RazaoSocial,Nome) NO_ENT,');
      Dmod.QrAux.SQL.Add('IF(Tipo=0,CNPJ,CPF) CNPJ_CPF,');
      Dmod.QrAux.SQL.Add('IF(Tipo=0,ETe1,PTe1) Te1,');
      Dmod.QrAux.SQL.Add('IF(Tipo=0,EEMail,PEMail) EMail');
      Dmod.QrAux.SQL.Add('FROM entidades');
      Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrAux.Params[0].AsInteger := Dmod.QrControle.FieldByName('Dono').AsInteger;
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      FmUnLock_Dmk.EdRazao.Text        := Dmod.QrAux.FieldByName('NO_ENT').AsString;
      FmUnLock_Dmk.EdCNPJ.ValueVariant := Dmod.QrAux.FieldByName('CNPJ_CPF').AsString;
      FmUnLock_Dmk.EdTelefone.Text     := Dmod.QrAux.FieldByName('Te1').AsString;
      FmUnLock_Dmk.EdEMail.Text        := Dmod.QrAux.FieldByName('EMail').AsString;
    end;
    if DataF > 0 then
    begin
      if DataF > USQLDB.ObtemAgora(Dmod.MyDB) then
        FmUnLock_Dmk.EdFimData.ValueVariant := DataF;
    end;
    FmUnLock_Dmk.ShowModal;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE controle SET ContraSenha=:P0');
    Dmod.QrUpd.Params[0].AsString := FmUnLock_Dmk.FContraSenha;
    Dmod.QrUpd.ExecSQL;
    //
    DModG.ReopenMaster('TUnLic_Dmk.457');
    {
    if FmUnLock_Dmk.FCrypt then Destrava(FmUnLock_Dmk.MCNova.Date)
    else begin
      Dmod.QrUpdU.SQL.Clear;
      if DModG.QrMaster.RecordCount = 0 then
        Dmod.QrUpdU.SQL.Add('INSERT INTO master SET Limite=1')
      else Dmod.QrUpdU.SQL.Add('UPDATE master SET Limite=Limite+1');
      Dmod.QrUpdU.ExecSQL;
      Geral.MB_('Senha inv�lida.'+sLineBreak+
      'H� possibilidade de mais '+IntToStr(29-DModG.QrMasterLimite.Value)+
      ' tentativas de reativa��o do sistema. Ap�s, '+
      'o sistema travar� em definitivo.', 'Erro', MB_OK+MB_ICONERROR);
    end;
    }
    FmUnLock_Dmk.Destroy;
    //
    Result := True;
  end else Geral.MB_Erro('Tentativas de reativa��o do sistema esgotadas.');
end;

(* Reativar no DControl?
function TUnLic_Dmk.CriaFmUnLock_MLA(): Boolean;
begin
  Result := False;
  //
  DModG.ReopenMaster();
  if DModG.QrMasterLimite.Value < 30 then
  begin
    Dmod.ReopenControle();
    MyObjects.FormCria(TFmUnLock_MLA, FmUnLock_MLA);
    FmUnLock_MLA.FContraSenha := Dmod.QrControle.FieldByName('ContraSenha').AsString;
    if (Trim(DModG.QrMasterEm.Value) <> '') and (DModG.QrMasterCNPJ.Value <> '') then
    begin
      FmUnLock_MLA.EdRazao.Text            := DModG.QrMasterEm.Value;
      FmUnLock_MLA.EdCNPJ.ValueVariant     := DModG.QrMasterCNPJ.Value;
      FmUnLock_MLA.EdTelefone.ValueVariant := DModG.QrMasterLicTel.Value;
      FmUnLock_MLA.EdEMail.Text            := DModG.QrMasterLicMail.Value;
    end else begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT IF(Tipo=0,RazaoSocial,Nome) NO_ENT,');
      Dmod.QrAux.SQL.Add('IF(Tipo=0,CNPJ,CPF) CNPJ_CPF,');
      Dmod.QrAux.SQL.Add('IF(Tipo=0,ETe1,PTe1) Te1,');
      Dmod.QrAux.SQL.Add('IF(Tipo=0,EEMail,PEMail) EMail');
      Dmod.QrAux.SQL.Add('FROM entidades');
      Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrAux.Params[0].AsInteger := Dmod.QrControle.FieldByName('Dono').AsInteger;
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      FmUnLock_MLA.EdRazao.Text        := Dmod.QrAux.FieldByName('NO_ENT').AsString;
      FmUnLock_MLA.EdCNPJ.ValueVariant := Dmod.QrAux.FieldByName('CNPJ_CPF').AsString;
      FmUnLock_MLA.EdTelefone.Text     := Dmod.QrAux.FieldByName('Te1').AsString;
      FmUnLock_MLA.EdEMail.Text        := Dmod.QrAux.FieldByName('EMail').AsString;
    end;
    FmUnLock_MLA.ShowModal;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE controle SET ContraSenha=:P0');
    Dmod.QrUpd.Params[0].AsString := FmUnLock_MLA.FContraSenha;
    Dmod.QrUpd.ExecSQL;
    //
    DModG.ReopenMaster();
    //
    if FmUnLock_MLA.FCrypt then
      DestravaLoc4(FmUnLock_MLA.MCNova.Date,
      FmUnLock_MLA.FCNPJ, FmUnLock_MLA.FRazaoSocial, FmUnLock_MLA.FNomePC,
      FmUnLock_MLA.FTelefone, FmUnLock_MLA.FEMail)
    else begin
      Dmod.QrUpdU.SQL.Clear;
      if DModG.QrMaster.RecordCount = 0 then
        Dmod.QrUpdU.SQL.Add('INSERT INTO master SET Limite=1')
      else Dmod.QrUpdU.SQL.Add('UPDATE master SET Limite=Limite+1');
      Dmod.QrUpdU.ExecSQL;
      Geral.MB_('Senha inv�lida.'+sLineBreak+
      'H� possibilidade de mais '+IntToStr(29-DModG.QrMasterLimite.Value)+
      ' tentativas de reativa��o do sistema. Ap�s, '+
      'o sistema travar� em definitivo.', 'Erro', MB_OK+MB_ICONERROR);
    end;
    FmUnLock_MLA.Destroy;
    //
    Result := True;
  end else Geral.MB_('Tentativas de reativa��o do sistema esgotadas.'
  ,' Sistema Travado', MB_OK+MB_ICONERROR);
end;
*)

{
function TUnLic_Dmk.DestravaLoc(DataFinal: TDateTime): Boolean;
var
  Hoje : TDate;
  Agora: TTime;
  Licenca: int64;
  Chave: Double;
  Serial: Integer;
  CNPJ, Enti, FimChek: String;
begin
  Result := False;
  try
    ?????.CriaChavedeAcesso;
    ?????.CriaChavedeAcesso;
    Chave  := Geral.DMV(UMyMod.LeChavedeAcesso);
    Serial := dmkPF.GetDiskSerialNumber('C');
    Licenca := Trunc((Serial * Chave) - Serial - Chave);
    //
    DModG.ReopenMaster();
    USQLDB.ObtemDataHora(Dmod.MyDB, Hoje, Agora);
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Dono FROM controle');
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('');
    if Dmod.QrAux.RecordCount > 0 then
      Dmod.QrUpd.SQL.Add('UPDATE controle SET ContraSenha="", Dono=:P0, CNPJ=:P1')
    else
      Dmod.QrUpd.SQL.Add('INSERT INTO controle SET Dono=:P0, CNPJ=:P1');
    Dmod.QrUpd.Params[0].AsInteger := -11;
    Dmod.QrUpd.Params[1].AsString  := FmUnLock_MLA.FCNPJ;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    // Pode ainda n�o haver dono!!!
    DModG.ReopenMaster();
    if DModG.QrMaster.RecordCount > 0 then
    begin
      Dmod.QrUpd.SQL.Add('UPDATE master SET ');
    end else
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO master SET DataI="' + FormatDateTime(VAR_FORMATDATE,  Hoje) + '", ');
    end;
    FimChek := ChecksumLicenca(DataFinal, FmUnLock_MLA.FCNPJ);
    DModG.QrCliAplicFimChek.Value;
    Dmod.QrUpd.SQL.Add('Monitorar=1, Distorcao=0, Limite=0, DiasExtra=0, ');
    Dmod.QrUpd.SQL.Add('Hoje="' + FormatDateTime(VAR_FORMATDATE,  Hoje) + '", ');
    Dmod.QrUpd.SQL.Add('Hora="'+FormatDateTime(VAR_FORMATTIME2, Agora)+'", ');
    Dmod.QrUpd.SQL.Add('DataF="'+FormatDateTime(VAR_FORMATDATE,  DataFinal)+'",');
    Dmod.QrUpd.SQL.Add('Em="'+FmUnLock_MLA.FRazaoSocial+'", ');
    Dmod.QrUpd.SQL.Add('CNPJ="'+FmUnLock_MLA.FCNPJ+'", ');
    Dmod.QrUpd.SQL.Add('Licenca="' + FormatFloat('0', Licenca) + '", ChekF="' + FimChek + '"');
    if (DModG.QrMasterMasSenha.Value = '') and (DModG.QrMasterMasLogin.Value = '') then
    begin
      Dmod.QrUpd.SQL.Add(', SitSenha=1, MasLogin=''geren'', MasSenha=AES_ENCRYPT(''geren'', ''' +
      CO_USERSPNOW + ''')');
    end;
    Dmod.QrUpd.ExecSQL;
    //
    VAR_ESTEIP := Geral.ObtemIP(1);
    Dmod.QrTerminal.Close;
    Dmod.QrTerminal.Params[0].AsString := VAR_IP_LICENCA;
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrTerminal);
    //
    if Dmod.QrTerminal.RecordCount = 0 then
    begin
      VAR_IP_LICENCA := '';
      //
      MyObjects.FormShow(TFmTerminais, FmTerminais);
      //
      Geral.WriteAppKeyLM2('IPClient', Application.Title, VAR_IP_LICENCA, ktString);
    end;
    if VAR_IP_LICENCA <> '' then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE terminais SET Licenca=:P0 WHERE IP=:P1 ');
      Dmod.QrUpd.Params[0].AsFloat  := Licenca;
      Dmod.QrUpd.Params[1].AsString := VAR_IP_LICENCA;
      Dmod.QrUpd.ExecSQL;
      //
    end else begin
      Geral.MB_('IP sem Terminal definido!'+
      sLineBreak+'IP: '+VAR_IP_LICENCA), 'Aviso', MB_OK+MB_ICONWARNING);
    end;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Tipo FROM entidades WHERE Codigo in (-1,-11)');
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('');
    if Dmod.QrAux.RecordCount > 0 then
      Dmod.QrUpd.SQL.Add('UPDATE entidades SET AlterWeb=1, ')
    else Dmod.QrUpd.SQL.Add('INSERT INTO entidades SET Tipo=0, ');
    if Dmod.QrAux.FieldByName('Tipo').AsInteger = 1 then Enti := 'Nome' else Enti := 'RazaoSocial';
    if Dmod.QrAux.FieldByName('Tipo').AsInteger = 1 then CNPJ := 'CPF' else CNPJ := 'CNPJ';
    Dmod.QrUpd.SQL.Add(Enti+'=:P0, '+CNPJ+'=:P1');
    if Dmod.QrAux.RecordCount > 0 then
      Dmod.QrUpd.SQL.Add('WHERE Codigo in (-1,-11)')
    else Dmod.QrUpd.SQL.Add(', Filial=1, Codigo=-11');
    Dmod.QrUpd.Params[0].AsString := FmUnLock_MLA.FRazaoSocial;
    Dmod.QrUpd.Params[1].AsString := FmUnLock_MLA.FCNPJ;
    Dmod.QrUpd.ExecSQL;
  except;
    raise;
  end;
end;
}

function TUnLic_Dmk.DestravaLoc4(DataFinal: TDateTime; CNPJ, RazaoSocial,
NomePC, Telefone, EMail: String): Boolean;
  function DestravaAplicativo(InfoN, InfoC, InfoT, InfoE: String; DataF: TDateTime): Boolean;
  var
    Licenca, LicInfo, ChekF: String;
    Dono: Integer;
    TipoSQL: TSQLType;
    Agora: TDateTime;
  begin
    Result := False;
    try
      //
      {
      DModG.QrCliAplic.Close;
      UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplic);
      //
      DModG.QrEntDmk.Close;
      UnDmkDAC_PF.AbreQueryApenas(DModG.QrEntDmk);
      //
      }
      DModG.ReopenMaster('TUnLic_Dmk.675');
      //
      Dmod.ReopenControle();
      //
      Agora := USQLDB.ObtemAgora(Dmod.MyDB);
      Dono  := Dmod.QrControle.FieldByName('Dono').AsInteger;
      //
      if Dono = 0 then Dono := -11;
      //CNPJ := DModG.QrEntDmkCPFJ.Value;
      //
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT Dono FROM controle');
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('');
      if Dmod.QrAux.RecordCount > 0 then
        Dmod.QrUpd.SQL.Add('UPDATE controle SET ContraSenha="", Dono=:P0, CNPJ=:P1')
      else
        Dmod.QrUpd.SQL.Add('INSERT INTO controle SET Dono=:P0, CNPJ=:P1');
      Dmod.QrUpd.Params[00].AsInteger := Dono;
      Dmod.QrUpd.Params[01].AsString  := CNPJ;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;

      // Pode ainda n�o haver dono!!!
      if not CalculaValLicenca(InfoC, Licenca) then
        Exit;
      //
      LicInfo := CalculaValLicInfo(InfoC, DataF);
      ChekF   := ChecksumLicenca(DataF, InfoC);
      //
      if DModG.QrMaster.RecordCount > 0 then
      begin
        Dmod.QrUpd.SQL.Add('UPDATE master SET ');
      end else
      begin
        Dmod.QrUpd.SQL.Add('INSERT INTO master SET ');
      end;
      Dmod.QrUpd.SQL.Add('Monitorar=1, Distorcao=0, Limite=0, DiasExtra=0, ');
      Dmod.QrUpd.SQL.Add('LicInfo="' + LicInfo + '", ');
      Dmod.QrUpd.SQL.Add('Hoje="' + Geral.FDT(Agora, 1) + '", ');
      Dmod.QrUpd.SQL.Add('Hora="'+ Geral.FDT(Agora, 102) + '", ');
      Dmod.QrUpd.SQL.Add('DataF="'+ Geral.FDT(DataF, 1) +'", ');
      Dmod.QrUpd.SQL.Add('Em="' + InfoN + '", ');
      Dmod.QrUpd.SQL.Add('CNPJ="' + InfoC + '" , ');
      Dmod.QrUpd.SQL.Add('Licenca="' + Licenca + '", ');
      Dmod.QrUpd.SQL.Add('ChekF="' + ChekF + '", ');
      Dmod.QrUpd.SQL.Add('LicTel="' + InfoT + '", ');
      Dmod.QrUpd.SQL.Add('LicMail="' + InfoE + '"');
      if (DModG.QrMasterMasSenha.Value = '') and (DModG.QrMasterMasLogin.Value = '') then
      begin
        Dmod.QrUpd.SQL.Add(', SitSenha=1, MasLogin=''geren'', MasSenha=AES_ENCRYPT(''geren'', ''' +
        CO_USERSPNOW + ''')');
      end;
      Dmod.QrUpd.ExecSQL;
      //  O terminal ser� cadastrado automaticamente
      (*
      VAR_ESTEIP := ObtemIP(1);
      Dmod.QrTerminal.Close;
      Dmod.QrTerminal.Params[0].AsString := VAR_IP_LICENCA;
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrTerminal);
      //
      if Dmod.QrTerminal.RecordCount = 0 then
      begin
        VAR_IP_LICENCA := '';
        MyObjects.FormShow(TFmTerminais, FmTerminais);
        Geral.WriteAppKeyLM2('IPClient', Application.Title, VAR_IP_LICENCA, ktString);
      end;
      if VAR_IP_LICENCA <> '' then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE terminais SET Licenca=:P0 WHERE IP=:P1 ');
        Dmod.QrUpd.Params[0].AsFloat  := Licenca;
        Dmod.QrUpd.Params[1].AsString := VAR_IP_LICENCA;
        Dmod.QrUpd.ExecSQL;
        //
      end else begin
        Geral.MB_('IP sem Terminal definido!'+
        sLineBreak+'IP: '+VAR_IP_LICENCA), 'Aviso', MB_OK+MB_ICONWARNING);
      end;
      *)
      //
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT Codigo, CodUsu');
      Dmod.QrAux.SQL.Add('FROM entidades');
      Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrAux.Params[0].AsInteger := Dono;
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      //
      if Dmod.QrAux.RecordCount > 0 then
      begin
        TipoSQL := stUpd;
        FCodUsu := Dmod.QrAux.FieldByName('CodUsu').AsInteger;
      end else begin
        TipoSQL := stIns;
        FCodUsu := Dono;
      end;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, TipoSQL, 'entidades', False, [
      'Tipo', 'CodUsu', 'RazaoSocial', 'Nome', 'CNPJ', 'CPF', 'ETe1', 'PTe1',
      'EEMail', 'PEMail'], ['Codigo'
      ], [FTipo, FCodUsu, FRazaoSocial, FNome, FCNPJ, FCPF, FETE1, FPTe1,
      FEEMail, FPEMail], [Dono], True);
      //
      Result := True;
    except;
      raise;
    end;
  end;

var
  Licenca, InfoN, InfoC,InfoT, InfoE, strIP: String;
  Servidor: Integer;
  TipoSQL: TSQLType;
begin
  Result := False;
  InfoN := RazaoSocial;
  InfoC := Geral.SoNumero_TT(CNPJ);
  InfoT := Geral.SoNumero_TT(Telefone);
  InfoE := EMail;
  //
  Screen.Cursor := crHourGlass;
  try
    if Length(InfoC) >= 14 then
    begin
      FRazaoSocial := InfoN;
      FNome        := '';
      FCNPJ        := InfoC;
      FCPF         := '';
      FTipo        := 0;
      FETe1        := InfoT;
      FPTe1        := '';
      FEEMail      := InfoE;
      FPEMail      := '';
    end else begin
      FRazaoSocial := '';
      FNome        := InfoN;
      FCNPJ        := '';
      FCPF         := InfoC;
      FTipo        := 1;
      FETe1        := '';
      FPTe1        := InfoT;
      FEEMail      := '';
      FPEMail      := InfoE;
    end;
{
    DModG.QrEntDmk.Close;
    DModG.QrEntDmk.Params[00].AsString := InfoC;
    DModG.QrEntDmk.Params[01].AsString := InfoC;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrEntDmk);
    if DModG.EhOServidor() then
    begin
      Servidor := 1;
      // Cadastra o cliente no site da dermatek
      if DModG.QrEntDmk.RecordCount = 0 then
      begin
        QrAux.Close;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('LOCK TABLES entidades WRITE;');
        QrAux.ExecSQL;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('SELECT MIN(Codigo) Codigo');
        QrAux.SQL.Add('FROM entidades;');
        UnDmkDAC_PF.AbreQueryApenas(QrAux);
        Codigo := QrAux.FieldByName('Codigo').AsInteger;
        //
        if Codigo > -1000 then
          Codigo := -1001
        else Codigo := Codigo -1;
        //
        FCodUsu := Codigo;
        QrAux.Close;

        UMyMod.SQLInsUpd(QrUpd, stIns, 'entidades', False, [
        'Tipo', 'CodUsu', 'RazaoSocial', 'Nome', 'CNPJ', 'CPF', 'ETe1', 'PTe1',
        'EEMail', 'PEMail'], ['Codigo'
        ], [FTipo, FCodUsu, FRazaoSocial, FNome, FCNPJ, FCPF, FETE1, FPTe1,
        FEEMail, FPEMail], [Codigo
        ], True);
        //
        QrAux.Close;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('UNLOCK TABLES;');
        QrAux.ExecSQL;
        //
      end else Codigo := DModG.QrEntDmkCodigo.Value;
      //
      DModG.QrCliAplic.Close;
      DModG.QrCliAplic.Params[00].AsInteger := Codigo;
      DModG.QrCliAplic.Params[01].AsInteger := CO_DMKID_APP;
      UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplic);
      //
      //  Cadastra Aplicativo para o cliente
      if DModG.QrCliAplic.RecordCount = 0 then
      begin
        QrAux.Close;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('LOCK TABLES cliaplic WRITE;');
        QrAux.ExecSQL;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('SELECT MIN(Controle) Controle');
        QrAux.SQL.Add('FROM cliaplic;');
        UnDmkDAC_PF.AbreQueryApenas(QrAux);
        Controle := QrAux.FieldByName('Controle').AsInteger;
        //
        if Controle > -1000 then
          Controle := -1001
        else Controle := Controle -1;
        //
        QrAux.Close;
        UMyMod.SQLInsUpd(QrUpd, stIns, 'cliaplic', False, [
        'Codigo', 'Aplicativo', 'Ativo', 'Telefone', 'EMail', 'NomeServer'], ['Controle'
        ], [Codigo, CO_DMKID_APP, 0, InfoT, InfoE, NomePC], [Controle
        ], True);
        //
        QrAux.Close;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('UNLOCK TABLES;');
        QrAux.ExecSQL;
        //
      end else Controle := DModG.QrCliAplicControle.Value;
      //
      FimData := Geral.FDT(EdFimData.ValueVariant, 1);
      FimChek := Lic_Dmk.ChecksumLicenca(
        EdFimData.ValueVariant, InfoC);
      UMyMod.SQLInsUpd(QrUpd, stUpd, 'cliaplic', False, [
      'FimData', 'FimChek'], ['Controle'
      ], [FimData, FimChek], [Controle
      ], True);
    end // Fim se for servidor
    else begin
      Servidor := 0;
      Codigo := DModG.QrEntDmkCodigo.Value;
      //
      DModG.QrCliAplic.Close;
      DModG.QrCliAplic.Params[00].AsInteger := Codigo;
      DModG.QrCliAplic.Params[01].AsInteger := CO_DMKID_APP;
      UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplic);
      //
      Controle := DModG.QrCliAplicControle.Value;
    end;
    //
    //Cadastra a m�quina
    DModG.QrCliAplicLi.Close;
    DModG.QrCliAplicLi.Params[00].AsInteger := Controle;
    DModG.QrCliAplicLi.Params[01].AsString  := DModG.FSerialKey;
    DModG.QrCliAplicLi.Params[02].AsString  := DModG.FSerialNum;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplicLi);
    Vezes := DModG.QrCliAplicLiConsultaNV.Value + 1;
    Conta := DModG.QrCliAplicLiConta.Value;
    DModG.QrWAgora.Close;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrWAgora);
    CadAlt := Geral.FDT(DModG.QrWAgoraAgora.Value, 105);
    strIP := Geral.ObtemIP(1);
    if DModG.QrCliAplicLi.RecordCount > 0 then
    begin
      UMyMod.SQLInsUpd(QrUpd, stUpd, 'cliaplicli', False, [
      'Controle', 'NomePC', 'IP', 'SerialKey', 'SerialNum', 'Cadastro'], ['Conta'], [
      Controle, NomePC, strIP, DModG.FSerialKey, DModG.FSerialNum, CadAlt], [Conta], True);
    end else begin
      UMyMod.SQLInsUpd(QrUpd, stIns, 'cliaplicli', True, [
      'Controle', 'NomePC', 'IP', 'SerialKey', 'SerialNum', 'ConsultaDH', 'ConsultaNV'], [(*'Conta'*)], [
      Controle, NomePC, strIP, DModG.FSerialKey, DModG.FSerialNum, CadAlt, Vezes], [(*Conta*)], True);
      DModG.QrCliAplicLi.Close;
      DModG.QrCliAplicLi.Params[00].AsInteger := Controle;
      DModG.QrCliAplicLi.Params[01].AsString  := DModG.FSerialKey;
      DModG.QrCliAplicLi.Params[02].AsString  := DModG.FSerialNum;
      UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplicLi);
      Conta := DModG.QrCliAplicLiConta.Value;
    end;
    AtualizaDadosNaDmk(Controle);
    //
}
    //2010-08-30
    if DestravaAplicativo(InfoN, InfoC, InfoT, InfoE, DataFinal) then
    begin
      if DModG.EhOServidor() then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'terminais', False, [
        'Servidor'], [], [0], [], False);
        Servidor := 1;
      end else Servidor := 0;
      DModG.ReopenTerminal();
      if DModG.QrTerminal.RecordCount = 0 then
        TipoSQL := stIns
      else
        TipoSQL := stUpd;
      UMyMod.SQLInsUpd(Dmod.QrUpd, TipoSQL, 'terminais', False, [
      'Licenca', 'Servidor', 'IP'], ['SerialKey', 'SerialNum'],[
      Licenca, Servidor, strIP], [DModG.FSerialKey, DModG.FSerialNum], True);
      //
      Geral.MB_Aviso(
      'O aplicativo ser� finalizado. � necess�rio reinici�-lo manualmente!');
      Result := True;
    end else
      Geral.MB_Aviso('N�o foi poss�vel validar esta c�pia do ' +
      Application.Title + '!' + sLineBreak + 'Avise a DERMATEK!');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnLic_Dmk.TimerTimer(Sender: TObject);
  procedure Ping2(HostName: String);
  {
  var
    i, Soma, BytesRecebidos : Integer;
    PckEntregue : Array[1..3] of Integer;
    Texto: String;
  }
  begin
    try
      if FIdIcmpClient_UnLic = nil then
      begin
        FIdIcmpClient_UnLic := TIdIcmpClient.Create(FTimer_UnLic(*FForm_UnLic*));
        FIdIcmpClient_UnLic.OnReply := IdIcmpClientReply;
      end;
      FIdIcmpClient_UnLic.Host := HostName;
      FIdIcmpClient_UnLic.ReceiveTimeout := 500;
      FIdIcmpClient_UnLic.Ping;
    except
      ;
    end;
  end;
begin
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE master SET DiasExtra=DiasExtra+1');
  DMod.QrUpd.ExecSQL;
  Ping2(DermaBD2_Host);
end;

procedure TUnLic_Dmk.IdIcmpClientReply(ASender: TComponent;
  const AReplyStatus: TReplyStatus);
begin
  if ConectouADermatek(False) then
  begin
    FTimer_UnLic.Enabled := False;
    //if not LiberaUso3(DModG.QrCtrlGeralDmkNetPath.Value, False) then Halt;
    if not LiberaUso4(False) then
      Halt;
  end;
end;
{
function TUnLic_Dmk.LiberaUso3(NomeServer: String; SoLocal: Boolean): Boolean;
var
  Licenca: String;
  Dias, Chave: Double;
  Serial: Integer;
  Data, DataF: TDateTime;
  ChekF: String;
begin
  Result := False;
  //
  if VAR_STDATABASES <> nil then
  begin
    VAR_STDATABASES.Text := Dmod.MyDB.DatabaseName + ' :: ' +
    Dmod.MyLocDataBase.DataBaseName + VAR_BDsEXTRAS;
    VAR_STDATABASES.Width := Length(VAR_STDATABASES.Text) * 7 + 8;
  end;
  DModG.ReopenMaster();
  if VAR_STDATALICENCA <> nil then
  begin
    Dias := DModG.QrMasterDistorcao.Value;
    Data := DModG.QrMasterDataF.Value;
    VAR_STDATALICENCA.Text :=
    FormatDateTime(VAR_FORMATDATE3, Data-Dias)+' ['+FloatToStr(Dias)+']';
    //if Data - Dias - Date < 10 then VAR_STDATALICENCA.Font.Color := clRed;
  end;
  Licenca    := DModG.QrMasterLicenca.Value;
  Chave := ObtemChave(NomeServer);
  Serial     := Geral.GetVolumeSerialNumber(DModG.EhOServidor(), NomeServer);
  //
  if (Chave = 0) then
  begin
    Geral.MB_('ERRO FATAL Dermatek 001', 'Erro', MB_OK+MB_ICONERROR);
    Halt;
  end;
  //
  DataF      := DModG.QrMasterDataF.Value;
  ChekF      := CalculaChecksumLicenca(DModG.QrMasterDataF.Value, DModG.QrMasterCNPJ.Value);
  //
  numero muito grande
  FBloquear   := Licenca <> FloatToStr(Trunc(((Chave * Serial) - Chave - Serial)));
  if not FBloquear then FBloquear := ChekF <> DModG.QrMasterChekF.Value;
  if FBloquear then
  begin
    DModG.ReopenMaster();
    Dmod.QrUpdU.SQL.Clear;
    if DModG.QrMaster.RecordCount=0 then
      Dmod.QrUpdU.SQL.Add('INSERT INTO master SET Monitorar=2')
    else Dmod.QrUpdU.SQL.Add('UPDATE master SET Monitorar=2');
    Dmod.QrUpdU.ExecSQL;
    DModG.ReopenMaster();
    if not SoLocal then
      Result := VerificaLiberacaoNet();
  end else if ChecaMonitoramento() <> 2 then
  begin
    if not SoLocal then
      Result := VerificaLiberacaoNet()
  end else Result := True;
  //
  if Result then
  begin
    Dias := Trunc(DataF-Date);
    if Dias < 0 then Application.Terminate
    else
    if Dias < 31 then
    begin
      Geral.MB_('A licen�a do aplicativo ir� expirar em ' + Geral.FDT(DataF, 2) +
      '. Faltam ' + FormatFloat('0', Dias) + ' dias!' + sLineBreak +
      'Entre em contato com a DERMATEK e solicite aumento do tempo da licen�a!' +
      sLineBreak + 'Telefone (44) 99152 7300' + sLineBreak + 'E-mail: cleide@dermatek.com.br'
      + sLineBreak + 'E-mail: dermatek@dermatek.com.br',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;
}

function TUnLic_Dmk.LiberaUso4(SoLocal: Boolean): Boolean;
var
  CNPJ, ChekF, Licenca, DataF, LicInfo: String;
  Erro: Integer;
  EhServ, VeTerminal: Boolean;
  Agora: TDateTime;
begin
  Result := False;
  Erro   := 0;
  //
  if VAR_STDATABASES <> nil then
  begin
    {$IfNDef SemDBLocal}
    VAR_STDATABASES.Text := Dmod.MyDB.DatabaseName + ' :: ' + Dmod.MyLocDataBase.DataBaseName + VAR_BDsEXTRAS;
    {$Else}
    VAR_STDATABASES.Text := Dmod.MyDB.DatabaseName + ' :: ' + VAR_BDsEXTRAS;
    {$EndIf}
    VAR_STDATABASES.Width := Length(VAR_STDATABASES.Text) * 7 + 8;
  end else
    Geral.MB_Info('"VAR_STDATABASES" n�o definido!');
  // Prepara info auxiliar
  EhServ := DModG.EhOServidor();
  DModG.ReopenMaster('TUnLic_Dmk.1120');
  CNPJ := DModG.QrMasterCNPJ.Value;
  //
  // Se n�o � s� local ent�o abre tabelas do site
  if not SoLocal then
  begin
    if not  ReopenTabelasSite(CNPJ) then
    begin
      Geral.MB_Aviso('N�o foi poss�vel verificar a licen�a! [1]');
      Exit;
    end;
  end;
  //
  // Verifica se j� h� licen�a:
  if DModG.QrMaster.RecordCount = 0 then
  begin
    // Se n�o existe e � pc cliente ent�o encerra...
    if not EhServ then
    begin
      Geral.MB_Aviso('M�quina cliente, sem um servidor dermatek definido!');
      Exit;
    end
    // ... caso contr�rio abre janela de cadastro do sistema no site
    else begin
      if not SoLocal then
        Result := VerificaLiberacaoNet4();
      Exit;
    end;
  end;

  // Se j� existe licen�a, verifica se � valida:
  if not CalculaValLicenca(DModG.QrMasterCNPJ.Value, Licenca) then
    Exit;

  if Licenca <> DModG.QrMasterLicenca.Value then
    Erro := 11;

  // se a licen�a � v�lida, verifica se o checksum da data final � v�lida
  ChekF := ChecksumLicenca(DModG.QrMasterDataF.Value, DModG.QrMasterCNPJ.Value);
  if ChekF <> DModG.QrMasterChekF.Value then
    Erro := 21;
  //

  // Verifica se � uma licen�a gerada manualmente (sem net)
  if DModG.QrMasterLicInfo.Value <> '' then
  begin
    LicInfo := CalculaValLicInfo(DModG.QrMasterCNPJ.Value, DModG.QrMasterDataF.Value);
    // Caso for, verifica se � v�lida.
    if LicInfo <> DModG.QrMasterLicInfo.Value then
      Erro := 31;
  end;

  // Se encontrou alguma tentativa de pirataria, for�a o cadastro na web
  if Erro > 0 then
  begin
    Geral.MB_Aviso('Licen�a inv�lida [' + IntToStr(Erro) + ']!');

    //Se n�o for v�lida, exclui dados da tabela master
    UMyMod.ExcluiTodosRegistros('', 'master');
    //
    // Caso for o servidor e estiver conectado na net ent�o recadastra no site
    if not SoLocal and EhServ then
        Result := VerificaLiberacaoNet4()
    else if not EhServ then begin
      // caso contr�rio, sai.
      Geral.MB_Aviso(
      'Execute o aplicativo no servidor para obter uma nova licen�a!');
      Exit;
    end else Exit;
  end;
  // Se o checksum da data � valida ent�o verifica se a licen�a venceu:
  Erro := 0;
  if not SoLocal then
  begin
    // Se estiver conectado, v� no site...
    if Int(DModG.QrCliAplicFimData.Value) <> Int(DModG.QrMasterDataF.Value) then
    begin
      // Se a licen�a foi validada local ent�o atualiza na net!
      if DModG.QrMasterLicInfo.Value <> '' then
      begin
        // Cadastra o cliente caso ele n�o esteja cadastrado ainda.
        if (DModG.QrEntDmk.RecordCount = 0)
        or (DModG.QrCliAplic.RecordCount = 0)
        or (DModG.QrCliAplicLi.RecordCount = 0)
        or (DModG.QrEntDmkCodigo.Value = 0)
        or (DModG.QrCliAplicControle.Value = 0)
        or (DModG.QrCliAplicLiConta.Value = 0)
        then begin
          CriaFmUnLock_Dmk(DModG.QrMasterDataF.Value);
          if not  ReopenTabelasSite(CNPJ) then
          begin
            Geral.MB_Aviso('N�o foi poss�vel verificar a licen�a! [2]');
            Exit;
          end;
        end;
      end;
      //
      DataF := Geral.FDT(DModG.QrCliAplicFimData.Value, 1);
      ChekF := DModG.QrCliAplicFimChek.Value;
      // Conserta data na tabela master
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'master', False, [
      'DataF', 'ChekF'], [], [DataF, ChekF], [], False);
      // Reabre a tabela master
      DModG.ReopenMaster('TUnLic_Dmk.1223');
      // Obtem data do site
      DModG.QrWAgora.Close;
      UnDmkDAC_PF.AbreQueryApenas(DModG.QrWAgora);
      // Verifica se a licen�a venceu
      if Int(DModG.QrCliAplicFimData.Value) < Int(DModG.QrWAgoraAGORA.Value) then
        Erro := 41;
    end
  end else
  begin
    // caso contr�rio v� local mesmo.
    // Obtem data do site
    Agora := USQLDB.ObtemAgora(Dmod.MyDB);
    //
    if Int(DModG.QrMasterDataF.Value) < Int(Agora) then
        Erro := 51;
  end;
  //
  if Erro > 0 then
  begin
    Geral.MB_Aviso('A licen�a de uso do "' + Application.Title + '" venceu.'
    + ContatoComDermatek);
    Exit;
  end;
  //
{
  // se estiver conectado na dermatek verifica licenca
  if not SoLocal then
  begin
    if DModG.QrCliAplic then

  end;
}
  //
  // se estiver conectado na dermatek verifica cadastro do terminal
  //Erro := 0;
  //VeTerminal := False;
  if not SoLocal then
  begin
    if DModG.QrCliAplicLi.RecordCount = 0 then
    begin
      CriaFmUnLock_Dmk(0);
      ZZTerminate := True;
      Application.Terminate;
      Exit;
    end else begin
      VeTerminal := True;
    end;
  end else begin
    // Caso contr�rio verifica local
    VeTerminal := True;
  end;
  if VeTerminal then
  begin
    DModG.ReopenTerminal();
    if DModG.QrTerminal.RecordCount > 0 then
    begin
      Result := True;
      if VAR_STTERMINAL <> nil then
      begin
        VAR_STTERMINAL.Text  := FormatFloat('0', DModG.QrTerminal.FieldByName('Terminal').AsInteger);
        VAR_STTERMINAL.Width := Length(VAR_STTERMINAL.Text) * 6 + 8;
      end else Geral.MB_Aviso('"VAR_STTERMINAL" n�o definido!');
    end;
  end;
  //
  // 2010-08-29



{
  if VAR_STDATALICENCA <> nil then
  begin
    Dias := DModG.QrMasterDistorcao.Value;
    Data := DModG.QrMasterDataF.Value;
    VAR_STDATALICENCA.Text :=
    FormatDateTime(VAR_FORMATDATE3, Data-Dias)+' ['+FloatToStr(Dias)+']';
    //if Data - Dias - Date < 10 then VAR_STDATALICENCA.Font.Color := clRed;
  end;
  Licenca    := DModG.QrMasterLicenca.Value;
  Chave := ObtemChave(NomeServer);
  Serial     := Geral.GetVolumeSerialNumber(DModG.EhOServidor(), NomeServer);
  //
  if (Chave = 0) then
  begin
    Geral.MB_('ERRO FATAL Dermatek 001', 'Erro', MB_OK+MB_ICONERROR);
    Halt;
  end;
  //
  DataF      := DModG.QrMasterDataF.Value;
  ChekF      := CalculaChecksumLicenca(DModG.QrMasterDataF.Value, DModG.QrMasterCNPJ.Value);
  //
  numero muito grande
  FBloquear   := Licenca <> FloatToStr(Trunc(((Chave * Serial) - Chave - Serial)));
  if not FBloquear then FBloquear := ChekF <> DModG.QrMasterChekF.Value;
  if FBloquear then
  begin
    DModG.ReopenMaster();
    Dmod.QrUpdU.SQL.Clear;
    if DModG.QrMaster.RecordCount=0 then
      Dmod.QrUpdU.SQL.Add('INSERT INTO master SET Monitorar=2')
    else Dmod.QrUpdU.SQL.Add('UPDATE master SET Monitorar=2');
    Dmod.QrUpdU.ExecSQL;
    DModG.ReopenMaster();
    if not SoLocal then
      Result := VerificaLiberacaoNet();
  end else if ChecaMonitoramento() <> 2 then
  begin
    if not SoLocal then
      Result := VerificaLiberacaoNet()
  end else Result := True;
  //
  if Result then
  begin
    Dias := Trunc(DataF-Date);
    if Dias < 0 then Application.Terminate
    else
    if Dias < 31 then
    begin
      Geral.MB_('A licen�a do aplicativo ir� expirar em ' + Geral.FDT(DataF, 2) +
      '. Faltam ' + FormatFloat('0', Dias) + ' dias!' + sLineBreak +
      'Entre em contato com a DERMATEK e solicite aumento do tempo da licen�a!' +
      ContatoComDermatek,
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
}
end;

function TUnLic_Dmk.ReopenTabelasSite(CNPJ_Dono: String): Boolean;
begin
  try
    DModG.QrEntDmk.Close;
    // Cadastro da empresa no site
    DModG.QrEntDmk.Params[00].AsString := CNPJ_Dono;
    DModG.QrEntDmk.Params[01].AsString := CNPJ_DOno;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrEntDmk);
    // Licen�a do aplicativo no site
    DModG.QrCliAplic.Close;
    DModG.QrCliAplic.Params[00].AsInteger := DModG.QrEntDmkCodigo.Value;
    DModG.QrCliAplic.Params[01].AsInteger := CO_DMKID_APP;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplic);
    // Licen�a da m�quina no site
    DModG.QrCliAplicLi.Close;
    DModG.QrCliAplicLi.Params[00].AsInteger := DModG.QrCliAplicControle.Value;
    DModG.QrCliAplicLi.Params[01].AsString  := DModG.FSerialKey;
    DModG.QrCliAplicLi.Params[02].AsString  := DModG.FSerialNum;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplicLi);
    //
    Result := True;
  except
    Result := False;
  end;
end;

{
function TUnLic_Dmk.ObtemChave(Server: String): Integer;
  function GeraNumero(): Integer;
  const
    Min = 1;
    Max = 999999999;
  begin
    Randomize();
    Result := 0;
    while (Result < Min) or (Result > Max) do
      Result := Random(10000000000);
  end;
var
  EhServ: Boolean;
  Chave: Integer;
begin
  Fazer chave
  Chave := 0;
  if  DModG.EhOServidor() then
  begin
    Chave := Geral.ReadAppKeyLM('Dmk001', TMeuDB, ktInteger, 0);
    if Chave = 0 then
    begin
      Chave := GeraNumero();
      Geral.WriteAppKeyLM('Dmk001', TMeuDB, Chave, ktInteger);
    end;
  end else begin
    if not Geral.ReadAppKeyLM_Net(Server, 'Dmk001', TMeuDB, ktInteger, 0, Chave) then Halt;
    if Chave = 0 then
    begin
      Chave := GeraNumero();
      if not Geral.WriteAppKeyLM_Net(Server, 'Dmk001', TMeuDB, Chave, ktInteger) then Halt;
    end;
  end;
  Result := Chave;
end;
}

function TUnLic_Dmk.VerificaLiberacaoLoc4(): Boolean;
var
  Dias, Tempo : Double;
  Distorcao, Monitorar: Integer;
  //Ano, Mes, Dia, Hora, Min, Seg, Mili: Word;
  Hoje, Data: TDateTime;
  Agora: TTime;
  Exec: String;
begin
  Result := False;
  //
  Exec := Uppercase(ExtractFileName(Application.ExeName));
  //if Exec = Uppercase('Syndic.exe') then Exit;
  DModG.ReopenMaster('TUnLic_Dmk.1429');
  if ChecaMonitoramento() = 0 then  // travado ap�s per�odo de avalia��o
  begin
(* Reativar no DControl?
    CriaFmUnLock_MLA;
*)
    ZZTerminate := True;
    Application.Terminate;
    Exit;
  end else begin
    if ChecaMonitoramento() in [1, 3] then // sob avalia��o
    begin
      Monitorar := ChecaMonitoramento();
      Distorcao := DModG.QrMasterDistorcao.Value;
      //
      USQLDB.ObtemDataHora(Dmod.MyDB, Hoje, Agora);
      //
      Dias  := Trunc(Hoje) - Trunc(DModG.QrMasterHoje.Value);
      Tempo := Agora - DModG.QrMasterHora.Value;
      //
      if Dias < 0 then inc(Distorcao, 1)
      else if (Dias = 0) and (Tempo < 0) then inc(Distorcao, 1)
      else if Trunc(Agora) < DModG.QrMasterHoje.Value then
        Agora := DModG.QrMasterHoje.Value + 1 + Round(SQRT(Distorcao));
      if (Hoje + Distorcao ) > DModG.QrMasterDataF.Value then
        inc(Monitorar, -1);
      //
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE master SET Monitorar=:P0, Distorcao=:P1, ');
      Dmod.QrUpdU.SQL.Add('Hoje=:P2, Hora=:P3');
      Dmod.QrUpdU.Params[0].AsInteger := Monitorar;
      Dmod.QrUpdU.Params[1].AsInteger := Distorcao;
      Dmod.QrUpdU.Params[2].AsString := FormatDateTime(VAR_FORMATDATE,  Hoje);
      Dmod.QrUpdU.Params[3].AsString := FormatDateTime(VAR_FORMATTIME2, Agora);
      Dmod.QrUpdU.ExecSQL;
      Result := True;
    end
    else
  //          Close;
  end;
  if ChecaMonitoramento() = 2  then  // aluguel vencido
  begin
(* Reativar no DControl?
    CriaFmUnLock_MLA;
*)
    ZZTerminate := True;
    Application.Terminate;
    Exit;
  end;
  if ChecaMonitoramento() = 4  then  // Liberado eternamente
  begin
    Result := True;
  end;
  if VAR_STDATALICENCA <> nil then
  begin
    Dias := DModG.QrMasterDistorcao.Value;
    Data := DModG.QrMasterDataF.Value;
    VAR_STDATALICENCA.Text :=
    FormatDateTime(VAR_FORMATDATE3, Data)+' ['+
    FormatDateTime(VAR_FORMATDATE3, Data-Dias)+']';
    //if Data - Dias - Date < 10 then VAR_STDATALICENCA.Font.Color := clRed;
    Result := True;
  end;
end;

{
function TUnLic_Dmk.VerificaLiberacaoNet: Boolean;
var
  Dias, Tempo : Double;
  Distorcao, Monitorar: Integer;
  //Ano, Mes, Dia, Hora, Min, Seg, Mili: Word;
  Hoje, Data: TDate;
  Agora: TTime;
  Exec: String;
begin
  Result := False;
  //
  Exec := Uppercase(ExtractFileName(Application.ExeName));
  //if Exec = Uppercase('Syndic.exe') then Exit;
  DModG.ReopenMaster();
  if ChecaMonitoramento() = 0 then  // travado ap�s per�odo de avalia��o
  begin
    CriaFmUnLock_Dmk();
    ZZTerminate := True;
    Application.Terminate;
    Exit;
  end else begin
    if ChecaMonitoramento() in [1, 3] then // sob avalia��o
    begin
      Monitorar := ChecaMonitoramento();
      Distorcao := DModG.QrMasterDistorcao.Value;
      //
      USQLDB.ObtemDataHora(Dmod.MyDB, Hoje, Agora);
      //
      Dias  := Trunc(Hoje) - Trunc(DModG.QrMasterHoje.Value);
      Tempo := Agora - DModG.QrMasterHora.Value;
      //
      if Dias < 0 then inc(Distorcao, 1)
      else if (Dias = 0) and (Tempo < 0) then inc(Distorcao, 1)
      else if Trunc(Agora) < DModG.QrMasterHoje.Value then
         Agora := DModG.QrMasterHoje.Value + 1 + Round(SQRT(Distorcao));
      if (Hoje + Distorcao ) > DModG.QrMasterDataF.Value then
        inc(Monitorar, -1);
      //
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE master SET Monitorar=:P0, Distorcao=:P1, ');
      Dmod.QrUpdU.SQL.Add('Hoje=:P2, Hora=:P3');
      Dmod.QrUpdU.Params[0].AsInteger := Monitorar;
      Dmod.QrUpdU.Params[1].AsInteger := Distorcao;
      Dmod.QrUpdU.Params[2].AsString := FormatDateTime(VAR_FORMATDATE,  Hoje);
      Dmod.QrUpdU.Params[3].AsString := FormatDateTime(VAR_FORMATTIME2, Agora);
      Dmod.QrUpdU.ExecSQL;
      Result := True;
    end
    else
  //          Close;
  end;
  if ChecaMonitoramento() = 2  then  // aluguel vencido
  begin
    CriaFmUnLock_Dmk;
    ZZTerminate := True;
    Application.Terminate;
    Exit;
  end;
  if ChecaMonitoramento() = 4  then  // Liberado eternamente
  begin
    Result := True;
  end;
  if VAR_STDATALICENCA <> nil then
  begin
    Dias := DModG.QrMasterDistorcao.Value;
    Data := DModG.QrMasterDataF.Value;
    VAR_STDATALICENCA.Text :=
    FormatDateTime(VAR_FORMATDATE3, Data)+' ['+
    FormatDateTime(VAR_FORMATDATE3, Data-Dias)+']';
    //if Data - Dias - Date < 10 then VAR_STDATALICENCA.Font.Color := clRed;
    Result := True;
  end;
end;
}

function TUnLic_Dmk.VerificaLiberacaoNet4(): Boolean;
var
  Dias, Tempo : Double;
  Distorcao, Monitorar: Integer;
  //Ano, Mes, Dia, Hora, Min, Seg, Mili: Word;
  Hoje, Data: TDateTime;
  Agora: TTime;
  Exec: String;
begin
  Result := False;
  //
  Exec := Uppercase(ExtractFileName(Application.ExeName));
  //if Exec = Uppercase('Syndic.exe') then Exit;
  DModG.ReopenMaster('TUnLic_Dmk.1583');
  if ChecaMonitoramento() = 0 then  // travado ap�s per�odo de avalia��o
  begin
    //Parei aqui!
    CriaFmUnLock_Dmk(0);
    ZZTerminate := True;
    Application.Terminate;
    Exit;
  end else begin
    if ChecaMonitoramento() in [1, 3] then // sob avalia��o
    begin
      Monitorar := ChecaMonitoramento();
      Distorcao := DModG.QrMasterDistorcao.Value;
      //
      USQLDB.ObtemDataHora(Dmod.MyDB, Hoje, Agora);
      //
      Dias  := Trunc(Hoje) - Trunc(DModG.QrMasterHoje.Value);
      Tempo := Agora - DModG.QrMasterHora.Value;
      //
      if Dias < 0 then inc(Distorcao, 1)
      else if (Dias = 0) and (Tempo < 0) then inc(Distorcao, 1)
      else if Trunc(Agora) < DModG.QrMasterHoje.Value then
         Agora := DModG.QrMasterHoje.Value + 1 + Round(SQRT(Distorcao));
      if (Hoje + Distorcao ) > DModG.QrMasterDataF.Value then
        inc(Monitorar, -1);
      //
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE master SET Monitorar=:P0, Distorcao=:P1, ');
      Dmod.QrUpdU.SQL.Add('Hoje=:P2, Hora=:P3');
      Dmod.QrUpdU.Params[0].AsInteger := Monitorar;
      Dmod.QrUpdU.Params[1].AsInteger := Distorcao;
      Dmod.QrUpdU.Params[2].AsString := FormatDateTime(VAR_FORMATDATE,  Hoje);
      Dmod.QrUpdU.Params[3].AsString := FormatDateTime(VAR_FORMATTIME2, Agora);
      Dmod.QrUpdU.ExecSQL;
      Result := True;
    end
    else
  //          Close;
  end;
  if ChecaMonitoramento() = 2  then  // aluguel vencido
  begin
    CriaFmUnLock_Dmk(0);
    ZZTerminate := True;
    Application.Terminate;
    Exit;
  end;
  if ChecaMonitoramento() = 4  then  // Liberado eternamente
  begin
    Result := True;
  end;
  if VAR_STDATALICENCA <> nil then
  begin
    Dias := DModG.QrMasterDistorcao.Value;
    Data := DModG.QrMasterDataF.Value;
    VAR_STDATALICENCA.Text :=
    FormatDateTime(VAR_FORMATDATE3, Data)+' ['+
    FormatDateTime(VAR_FORMATDATE3, Data-Dias)+']';
    //if Data - Dias - Date < 10 then VAR_STDATALICENCA.Font.Color := clRed;
    Result := True;
  end;
end;

procedure TUnLic_Dmk.VerificaLicencaDoAplicativo(LaAviso1, LaAviso2: TLabel; Form: TForm);
var
  //DmkNetPath: String;
  Cria, Continua, Sorteado: Boolean;
  R, Dias, Data: Integer;
  //
  //Server: String;
begin
  Sorteado := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inicializando aplicativo');
  {  N�o ir� usar Pasta Dermatek no Servidor?
  if Trim(DModG.QrCtrlGeralDmkNetPath.Value) = '' then
  begin
    if DModG.EhOServidor() then
    begin
      DmkNetPath := '\\' + ????.IndyComputerName() + '\Dermatek\';
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctrlgeral', False, [
      'DmkNetPath'], ['Codigo'], [DmkNetPath], [1], False);
    end else
    begin
      if DBCheck.CriaFm(TFmPCSNaNet, FMPCSNaNet, afmoLiberado) then
      begin
        FmPCSNaNet.ShowModal;
        FmPCSNaNet.Destroy;
      end else begin
       VAR_PORQUE_VERIFICA := VAR_PORQUE_VERIFICA + ';UnLic_Dmk:1552';
       DModG.VerificaDB();
        Halt;
      end;
    end;
    DModG.QrCtrlGeral.Close;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrCtrlGeral);
  end;
  if Trim(DModG.QrCtrlGeralDmkNetPath.Value) = '' then
  begin
    Geral.MB_('Pasta "Dermatek" do servidor n�o informada!' + sLineBreak +
    'Termine a instala��o do aplicativo no servidor!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
    Halt;
  end;
  }

  //
  //Fazer aqui a verifica��o local!
  //if LiberaUso3(DModG.QrCtrlGeralDmkNetPath.Value, True) then
  // Parei Aqui 2010-08-28
  //acompanhar primeira execu��o ap�s instala��o
  if LiberaUso4(True) then
  begin
    // Parei aqui! Fazer random de 10 para verificar!
    Randomize();
    R := Random(10);
    Continua := R = 1;
    //Continua := True;
    Sorteado := True;
  end else Continua := True;
  //
  if Continua then
  begin
    if ConectouADermatek(True) then
    begin
      if not LiberaUso4(False) then
        Halt;
      //
      AtualizaVencimentoDaLicenca();
    end else begin
      if not Sorteado then
      begin
        VerificaLiberacaoLoc4();
        Application.Terminate;
      end else
      begin
        try
          // N�o precisa de try..except, mas vai por garantia!!!????
          Cria := FTimer_UnLic = nil;
        except
          Cria := True;
        end;
        if Cria then
        begin
          FTimer_UnLic := TTimer.Create(Form);
          //FForm_UnLic  := Form;
        end;
        FTimer_UnLic.Interval := 1000;//30 * 60000; // 30 minutos
        FTimer_UnLic.OnTimer  := TimerTimer;
        FTimer_UnLic.Enabled  := True;
      end;
    end;
  end else MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  if VAR_STDATALICENCA <> nil then
  begin
    DModG.ReopenMaster('TUnLic_Dmk.1747');
    //
    Dias := DModG.QrMasterDistorcao.Value;
    Data := Trunc(DModG.QrMasterDataF.Value);
    VAR_STDATALICENCA.Text :=
      FormatDateTime(VAR_FORMATDATE3, Data-Dias)+' ['+FloatToStr(Dias)+']';
    //if Data - Dias - Date < 10 then VAR_STDATALICENCA.Font.Color := clRed;
  end else Geral.MB_Info('"VAR_STDATALICENCA" n�o definido!');
  // N�o fazer. Deixar mostrando o aviso de que n�o conectou
  //MyObjects.Informa(LaAviso, False, '...');
  //
  if ZZTerminate then
    Application.Terminate;
end;

{$IFDEF UsaWSuport}
function TUnLic_Dmk.LiberaUso5(): Boolean;
  function Verifica(Hoje, DataVeri: TDateTime; Dias: Integer): Integer;
  begin
    if DataVeri < Hoje then
    begin
      if not DmkWeb.RemoteConnection then
      begin
        //N�o tem internet
        if DataVeri <= (Hoje + Dias) then
          Result := 2
        else
          Result := 0;
      end else
      begin
        Result := FmUnLock_Dmk.AtualizaLicenca5(DModG.QrMasterEm.Value,
                    DModG.QrMasterCNPJ.Value, Geral.IndyComputerName(), '', 
                    False, False, False);
        if Result = 1 then
          DmkWeb.ValidaModulosAplicativo(Dmod.QrUpd, DModG.QrMasterCNPJ.Value,
            CO_DMKID_APP);
      end;
    end
    else if DataVeri = Hoje then
      Result := 1
    else
      Result := 0;
  end;
const
  DiasLimiteVerificacao = 30;//Permite ficar 30 dias sem verificar. Caso o cliente n�o tenha internet
var
  Resul: Integer;
  Hoje, DataVeri: TDateTime;
  VctoLicen, CNPJ, SerialKey, SerialNum, SerialKeyRE, SerialNumRE: String;
  CodAplic, StatAplic: Integer;
  DiasRest: Double;
begin
  Result       := False;
  DataVeri     := 0;
  Hoje         := Trunc(DModG.ObtemAgora);
  VAR_TERMINAL := 0;
  //
  DModG.ReopenMaster('TUnLic_Dmk.1804');
  //
  //SerialKeyRE := Geral.ReadAppKeyCU('SerialKey', Application.Title, ktString, '');
  //SerialNumRE := Geral.ReadAppKeyCU('SerialNum', Application.Title, ktString, '');
  SerialKeyRE := Geral.ReadAppKeyCU(TMeuDB + '_SerialKey', Application.Title, ktString, '');
  SerialNumRE := Geral.ReadAppKeyCU(TMeuDB + '_SerialNum', Application.Title, ktString, '');
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT IF(DataAlt > DataCad, "Alt", "Cad") TipData,');
  Dmod.QrAux.SQL.Add('DataCad, DataAlt, Terminal');
  Dmod.QrAux.SQL.Add('FROM terminais');
  Dmod.QrAux.SQL.Add('WHERE SerialNum=:P0');
  Dmod.QrAux.SQL.Add('AND SerialKey=:P1');
  Dmod.QrAux.Params[0].AsString := SerialNumRE;
  Dmod.QrAux.Params[1].AsString := SerialKeyRE;
  UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    VAR_TERMINAL := Dmod.QrAux.FieldByName('Terminal').AsInteger;
    CNPJ         := DModG.QrMasterCNPJ.Value;
    CodAplic     := CO_DMKID_APP;
    StatAplic    := DModG.QrMasterMonitorar.Value;
    VctoLicen    := Geral.FDT(DModG.QrMasterDataF.Value, 1);
    //
    if LowerCase(Dmod.QrAux.FieldByName('TipData').AsString) = 'cad' then
      DataVeri  := Dmod.QrAux.FieldByName('DataCad').AsDateTime
    else
      DataVeri  := Dmod.QrAux.FieldByName('DataAlt').AsDateTime;
    if DataVeri < 2 then
      DataVeri := Hoje;
    SerialKey := DmkPF.CalculaValSerialKey2(Geral.FDT(DataVeri, 1), False);
    SerialNum := DmkPF.CalculaValSerialNum2(VctoLicen, CNPJ, CodAplic, StatAplic, False);
    //
    if (SerialKey = SerialKeyRE) and (SerialNum = SerialNumRE) then
    begin
      if DataVeri > Hoje then //Verifica se n�o alterou data do computador
        Resul := 0
      else begin
        case DModG.QrMasterMonitorar.Value of
          0,2: //Bloqueado, Vencido
            Resul := 0;
          1,3: //Monitorado, Alugado
          begin
            if DModG.QrMasterDataF.Value < Hoje then
              Resul := 0
            else
              Resul := Verifica(Hoje, DataVeri, DiasLimiteVerificacao);
          end;
          4: //Liberado
            Resul := Verifica(Hoje, DataVeri, DiasLimiteVerificacao);
          else
            Resul := 0;
        end;
      end;
    end else
      Resul := 0;
  end else
    Resul := 0;
  //
  if Resul <> 1 then
  begin
    if (Length(SerialKeyRE) = 0) and (Length(SerialNumRE) = 0) then
    begin
      Resul := FmUnLock_Dmk.AtualizaLicenca5(DModG.QrMasterEm.Value,
                 DModG.QrMasterCNPJ.Value, Geral.IndyComputerName(), '',
                 False, False, False);
      if Resul = 1 then
        DmkWeb.ValidaModulosAplicativo(Dmod.QrUpd, DModG.QrMasterCNPJ.Value, CO_DMKID_APP);
    end;
    //N�o Conseguiu verificar no site Dermatek tem alguns dias
    if (DataVeri <> Hoje) and (Hoje <= (DataVeri + DiasLimiteVerificacao))
      and (Resul = 2) then
    begin
      DiasRest := (DataVeri + DiasLimiteVerificacao) - Hoje;
      //
      Geral.MB_Aviso('N�o foi poss�vel incrementar automaticamente a validade da licen�a do aplicativo!' + sLineBreak +
        'Voc� tem ' + Geral.FF0(Trunc(DiasRest)) + ' dias restantes.' + sLineBreak + sLineBreak + 'Prov�veis motivos: ' + sLineBreak +
        '=> Sua conex�o com a internet est� com problema.' + sLineBreak +
        '=> O seu firewal est� bloqueado o acesso do aplicativo com � internet.' + sLineBreak + sLineBreak +
        'Ap�s resolver os problemas citados acima esta mensagem n�o aparecer� novamente.' + sLineBreak +
        'Somenete entre em contato com o suporte t�cnico do aplicativo caso o per�do restante esteja pr�ximo do fim!');
      Resul := 1;
    end;
    if Resul <> 1 then
    begin
      MostraUnLock_Dmk(False);
      //
      Result := False;
      //
      Application.Terminate;
    end;
  end else
  begin
    DiasRest := DModG.QrMasterDataF.Value - Hoje;
    //
    if DiasRest <= DiasLimiteVerificacao then
    begin
      if DiasRest = 0 then
        Geral.MB_Aviso('Licen�a do aplicativo expira hoje!')
      else
        Geral.MB_Aviso('Licen�a do aplicativo expira em "' + Geral.FF0(Trunc(DiasRest)) + '" dias!');
    end;
    //
    Result := True;
  end;
end;
{$ENDIF}
{$IFNDEF UsaWSuport}
function TUnLic_Dmk.LiberaUso5(): Boolean;
begin
  Result := False;
end;
{$ENDIF}

procedure TUnLic_Dmk.MostraUnLock_Dmk(LiberaUso6: Boolean);
begin
  MyObjects.FormCria(TFmUnLock_Dmk, FmUnLock_Dmk);
  FmUnLock_Dmk.FTipLibera := Libera5;
  if (Trim(DModG.QrMasterEm.Value) <> '') and (DModG.QrMasterCNPJ.Value <> '') then
  begin
    DModG.ReopenTerminal(True);
    //
    FmUnLock_Dmk.EdRazao.Text            := DModG.QrMasterEm.Value;
    FmUnLock_Dmk.EdCNPJ.ValueVariant     := DModG.QrMasterCNPJ.Value;
    FmUnLock_Dmk.EdTelefone.ValueVariant := DModG.QrMasterLicTel.Value;
    FmUnLock_Dmk.EdEMail.Text            := DModG.QrMasterLicMail.Value;
    FmUnLock_Dmk.EdNomePC.ValueVariant   := DModG.QrTerminal.FieldByName('NomePC').AsString;
    FmUnLock_Dmk.EdEMail.ValueVariant    := DModG.QrTerminal.FieldByName('DescriPC').AsString;
  end else begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT IF(Tipo=0,RazaoSocial,Nome) NO_ENT,');
    Dmod.QrAux.SQL.Add('IF(Tipo=0,CNPJ,CPF) CNPJ_CPF,');
    Dmod.QrAux.SQL.Add('IF(Tipo=0,ETe1,PTe1) Te1,');
    Dmod.QrAux.SQL.Add('IF(Tipo=0,EEMail,PEMail) EMail');
    Dmod.QrAux.SQL.Add('FROM entidades');
    Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrAux.Params[0].AsInteger := Dmod.QrControle.FieldByName('Dono').AsInteger;
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
    //
    FmUnLock_Dmk.EdRazao.Text        := Dmod.QrAux.FieldByName('NO_ENT').AsString;
    FmUnLock_Dmk.EdCNPJ.ValueVariant := Dmod.QrAux.FieldByName('CNPJ_CPF').AsString;
    FmUnLock_Dmk.EdTelefone.Text     := Dmod.QrAux.FieldByName('Te1').AsString;
    FmUnLock_Dmk.EdEMail.Text        := Dmod.QrAux.FieldByName('EMail').AsString;
  end;
  //
  FmUnLock_Dmk.FLicenca6 := LiberaUso6;
  //
  FmUnLock_Dmk.ShowModal;
  FmUnLock_Dmk.Destroy;
end;

{$IFDEF UsaWSuport}
{$IFDEF UsaREST}
function TUnLic_Dmk.LiberaUso6(): Boolean;
  function Verifica(Hoje, DataVeri: TDateTime; Dias: Integer): Integer;
  begin
    if DataVeri < Hoje then
    begin
      if not DmkWeb.RemoteConnection then
      begin
        //N�o tem internet
        if DataVeri <= (Hoje + Dias) then
          Result := 2
        else
          Result := 0;
      end else
      begin
        Result := FmUnLock_Dmk.AtualizaLicenca6(DModG.QrMasterEm.Value,
                    DModG.QrMasterCNPJ.Value, Geral.IndyComputerName(), '',
                    False, False, False);
        if Result = 1 then
          DmkWeb.ValidaModulosAplicativo(Dmod.QrUpd, DModG.QrMasterCNPJ.Value,
            CO_DMKID_APP);
      end;
    end
    else if DataVeri = Hoje then
      Result := 1
    else
      Result := 0;
  end;
const
  DiasLimiteVerificacao = 30;//Permite ficar 30 dias sem verificar. Caso o cliente n�o tenha internet
var
  Resul: Integer;
  Hoje, DataVeri: TDateTime;
  VctoLicen, CNPJ, SerialKey, SerialNum, SerialKeyRE, SerialNumRE: String;
  CodAplic, StatAplic: Integer;
  DiasRest: Double;
begin
  Result       := False;
  DataVeri     := 0;
  Hoje         := Trunc(DModG.ObtemAgora(True));
  VAR_TERMINAL := 0;
  //
  DModG.ReopenMaster('TUnLic_Dmk.2000');
  //
  SerialKeyRE := Geral.ReadAppKeyCU(TMeuDB + '_SerialKey', Application.Title, ktString, '');
  SerialNumRE := Geral.ReadAppKeyCU(TMeuDB + '_SerialNum', Application.Title, ktString, '');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT IF(DataAlt > DataCad, "Alt", "Cad") TipData,',
    'DataCad, DataAlt, Terminal ',
    'FROM terminais ',
    'WHERE SerialNum = "' + SerialNumRE + '"',
    'AND SerialKey = "' + SerialKeyRE + '"',
    '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    VAR_TERMINAL := Dmod.QrAux.FieldByName('Terminal').AsInteger;
    CNPJ         := DModG.QrMasterCNPJ.Value;
    CodAplic     := CO_DMKID_APP;
    StatAplic    := DModG.QrMasterMonitorar.Value;
    VctoLicen    := Geral.FDT(DModG.QrMasterDataF.Value, 1);
    if VAR_STDATALICENCA <> nil then
      VAR_STDATALICENCA.Text := Geral.FDT(DModG.QrMasterDataF.Value, 2);
    //
    if LowerCase(Dmod.QrAux.FieldByName('TipData').AsString) = 'cad' then
      DataVeri  := Dmod.QrAux.FieldByName('DataCad').AsDateTime
    else
      DataVeri  := Dmod.QrAux.FieldByName('DataAlt').AsDateTime;
    //
    if DataVeri < 2 then
      DataVeri := Hoje;
    //
    SerialKey := DmkPF.CalculaValSerialKey2(Geral.FDT(DataVeri, 1), False);
    SerialNum := DmkPF.CalculaValSerialNum2(VctoLicen, CNPJ, CodAplic, StatAplic, False);
    //
    if (SerialKey = SerialKeyRE) and (SerialNum = SerialNumRE) then
    begin
      if DataVeri > Hoje then //Verifica se n�o alterou data do computador
        Resul := 0
      else begin
        case DModG.QrMasterMonitorar.Value of
          0,2: //Bloqueado, Vencido
            Resul := 0;
          1,3: //Monitorado, Alugado
          begin
            if DModG.QrMasterDataF.Value < Hoje then
              Resul := 0
            else
              Resul := Verifica(Hoje, DataVeri, DiasLimiteVerificacao);
          end;
          4: //Liberado
            Resul := Verifica(Hoje, DataVeri, DiasLimiteVerificacao);
          else
            Resul := 0;
        end;
      end;
    end else
      Resul := 0;
  end else
    Resul := 0;
  //
  if Resul <> 1 then
  begin
    if (Length(SerialKeyRE) = 0) and (Length(SerialNumRE) = 0) then
    begin
      Resul := FmUnLock_Dmk.AtualizaLicenca6(DModG.QrMasterEm.Value,
                 DModG.QrMasterCNPJ.Value, Geral.IndyComputerName(), '',
                 False, False, False);
      if Resul = 1 then
        DmkWeb.ValidaModulosAplicativo(Dmod.QrUpd, DModG.QrMasterCNPJ.Value, CO_DMKID_APP);
    end;
    //N�o Conseguiu verificar no site Dermatek tem alguns dias
    if (DataVeri <> Hoje) and (Hoje <= (DataVeri + DiasLimiteVerificacao))
      and (Resul = 2) then
    begin
      DiasRest := (DataVeri + DiasLimiteVerificacao) - Hoje;
      //
      Geral.MB_Aviso('N�o foi poss�vel incrementar autom�ticamente a validade da licen�a do aplicativo!' + sLineBreak +
        'Voc� tem ' + Geral.FF0(Trunc(DiasRest)) + ' dias restantes.' + sLineBreak + sLineBreak + 'Prov�veis motivos: ' + sLineBreak +
        '=> Sua conex�o com a internet est� com problema.' + sLineBreak +
        '=> O seu firewal est� bloqueado o acesso do aplicativo com � internet.' + sLineBreak + sLineBreak +
        'Ap�s resolver os problemas citados acima esta mensagem n�o aparecer� novamente.' + sLineBreak +
        'Somenete entre em contato com o suporte t�cnico do aplicativo caso o per�do restante esteja pr�ximo do fim!');
      Resul := 1;
    end;
    if Resul <> 1 then
    begin
      MostraUnLock_Dmk(True);
      //
      Result := False;
      //
      Application.Terminate;
    end;
  end else
  begin
    DiasRest := DModG.QrMasterDataF.Value - Hoje;
    //
    if DiasRest <= DiasLimiteVerificacao then
    begin
      if DiasRest = 0 then
        Geral.MB_Aviso('Licen�a do aplicativo expira hoje!')
      else
        Geral.MB_Aviso('Licen�a do aplicativo expira em "' + Geral.FF0(Trunc(DiasRest)) + '" dias!');
    end;
    //
    Result := True;
  end;
end;
{$EndIf}
{$ENDIF}

end.
