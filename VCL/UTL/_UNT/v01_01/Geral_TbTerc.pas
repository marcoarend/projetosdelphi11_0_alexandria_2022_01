unit Geral_TbTerc;
{ Colocar no MyListas:

Uses Geral_TbTerc;


//
function TMyListas.CriaListaTabelas(:
      UnGeral_TbTerc.CarregaListaTabelas(Lista);
//
function TMyListas.CriaListaSQL:
    UnGeral_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
    UnGeral_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      UnGeral_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      UnGeral_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    UnGeral_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  UnGeral_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, dmkGeral, UnDmkEnums;

type
  TGeral_TbTerc = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

var
  UnGeral_TbTerc: TGeral_TbTerc;


implementation

uses UMySQLModule;

function TGeral_TbTerc.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, Lowercase('CNAE21Niv'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('CNAE21Cad'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('GerlSeq1'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('IBPTax'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('NFeMsg'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('UnMedComl'), '');
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TGeral_TbTerc.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('cnae21niv') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|""');
    FListaSQL.Add('1|"Subclasse"');
    FListaSQL.Add('2|"Classe"');
    FListaSQL.Add('3|"Grupo"');
    FListaSQL.Add('4|"Divis�o"');
    FListaSQL.Add('5|"Se��o"');
  end else
  if Uppercase(Tabela) = Uppercase('UnMedComl') then
  begin
    FListaSQL.Add('Sigla|Nome');
    FListaSQL.Add('"AMPOLA"|"AMPOLA"');
    FListaSQL.Add('"BALDE"|"BALDE"');
    FListaSQL.Add('"BANDEJ"|"BANDEJA"');
    FListaSQL.Add('"BARRA"|"BARRA"');
    FListaSQL.Add('"BISNAG"|"BISNAGA"');
    FListaSQL.Add('"BLOCO"|"BLOCO"');
    FListaSQL.Add('"BOBINA"|"BOBINA"');
    FListaSQL.Add('"BOMB"|"BOMBONA"');
    FListaSQL.Add('"CAPS"|"CAPSULA"');
    FListaSQL.Add('"CART"|"CARTELA"');
    FListaSQL.Add('"CENTO"|"CENTO"');
    FListaSQL.Add('"CJ"|"CONJUNTO"');
    FListaSQL.Add('"CM"|"CENTIMETRO"');
    FListaSQL.Add('"CM2"|"CENTIMETRO QUADRADO"');
    FListaSQL.Add('"CX"|"CAIXA"');
    FListaSQL.Add('"CX2"|"CAIXA COM 2 UNIDADES"');
    FListaSQL.Add('"CX3"|"CAIXA COM 3 UNIDADES"');
    FListaSQL.Add('"CX5"|"CAIXA COM 5 UNIDADES"');
    FListaSQL.Add('"CX10"|"CAIXA COM 10 UNIDADES"');
    FListaSQL.Add('"CX15"|"CAIXA COM 15 UNIDADES"');
    FListaSQL.Add('"CX20"|"CAIXA COM 20 UNIDADES"');
    FListaSQL.Add('"CX25"|"CAIXA COM 25 UNIDADES"');
    FListaSQL.Add('"CX50"|"CAIXA COM 50 UNIDADES"');
    FListaSQL.Add('"CX100"|"CAIXA COM 100 UNIDADES"');
    FListaSQL.Add('"DISP"|"DISPLAY"');
    FListaSQL.Add('"DUZIA"|"DUZIA"');
    FListaSQL.Add('"EMBAL"|"EMBALAGEM"');
    FListaSQL.Add('"FARDO"|"FARDO"');
    FListaSQL.Add('"FOLHA"|"FOLHA"');
    FListaSQL.Add('"FRASCO"|"FRASCO"');
    FListaSQL.Add('"GALAO"|"GAL�O"');
    FListaSQL.Add('"GF"|"GARRAFA"');
    FListaSQL.Add('"GRAMAS"|"GRAMAS"');
    FListaSQL.Add('"JOGO"|"JOGO"');
    FListaSQL.Add('"KG"|"QUILOGRAMA"');
    FListaSQL.Add('"KIT"|"KIT"');
    FListaSQL.Add('"LATA"|"LATA"');
    FListaSQL.Add('"LITRO"|"LITRO"');
    FListaSQL.Add('"M"|"METRO"');
    FListaSQL.Add('"M2"|"METRO QUADRADO"');
    FListaSQL.Add('"M3"|"METRO C�BICO"');
    FListaSQL.Add('"MILHEI"|"MILHEIRO"');
    FListaSQL.Add('"ML"|"MILILITRO"');
    FListaSQL.Add('"MWH"|"MEGAWATT HORA"');
    FListaSQL.Add('"PACOTE"|"PACOTE"');
    FListaSQL.Add('"PALETE"|"PALETE"');
    FListaSQL.Add('"PARES"|"PARES"');
    FListaSQL.Add('"PC"|"PE�A"');
    FListaSQL.Add('"POTE"|"POTE"');
    FListaSQL.Add('"K"|"QUILATE"');
    FListaSQL.Add('"RESMA"|"RESMA"');
    FListaSQL.Add('"ROLO"|"ROLO"');
    FListaSQL.Add('"SACO"|"SACO"');
    FListaSQL.Add('"SACOLA"|"SACOLA"');
    FListaSQL.Add('"TAMBOR"|"TAMBOR"');
    FListaSQL.Add('"TANQUE"|"TANQUE"');
    FListaSQL.Add('"TON"|"TONELADA"');
    FListaSQL.Add('"TUBO"|"TUBO"');
    FListaSQL.Add('"UNID"|"UNIDADE"');
    FListaSQL.Add('"VASIL"|"VASILHAME"');
    FListaSQL.Add('"VIDRO"|"VIDRO"');
  end else
  ;
end;

function TGeral_TbTerc.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades","Cadastro de pessoas f�sicas e jur�dicas (clientes, fornecedores, etc.)"');
    //FListaSQL.Add('"",""');
  end;
end;


function TGeral_TbTerc.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
{
  if Uppercase(Tabela) = Uppercase('GerlSeq1') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Tabela';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Campo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := '_WHERE';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := '_AND';
    FLIndices.Add(FRIndices);
    //
  end else
}
  if Uppercase(Tabela) = Uppercase('cnae21niv') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('cnae21cad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Nivel';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'CodTxt';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE2';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'CodAlf';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('IBPTax') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Ex';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Tabela';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'UF';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'Versao';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(Tabela) = Uppercase('NFeMsg') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('UnMedComl') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Sigla';
    FLIndices.Add(FRIndices);
    //
  end else

end;

function TGeral_TbTerc.CarregaListaFRQeiLnk(TabelaCria: String;
  FRQeiLnk: TQeiLnk; FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TGeral_TbTerc.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
{
    if Uppercase(Tabela) = Uppercase('GerlSeq1') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Campo';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := '_WHERE';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := '_AND';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BigIntPos';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BigIntNeg';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
}
    if Uppercase(Tabela) = Uppercase('cnae21niv') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('cnae21cad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel';   // 5=Se��o	4=Divis�o	3=Grupo	2=Classe	1=Subclasse
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodTxt';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodAlf';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel1';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel2';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel3';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel4';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel5';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('IBPTax') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'bigint(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ex';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UF';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Versao';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fonte';     // TIBPTaxFont.ibptaxfontNaoInfo>0-Desconhecido := ''
      FRCampos.Tipo       := 'int(11)';   // TIBPTaxFont.ibptaxfontIBPTax>1-IBPT := 'Fonte IBPT'
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR          // TIBPTaxFont.ibptaxfontCalculoProprio>2 := 'Fonte: C�lculo pr�prio';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FonteStr';
      FRCampos.Tipo       := 'varchar(34)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AliqNac';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AliqImp';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estadual';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Municipal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VigenIni';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VigenFim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Chave';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('NFeMsg') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('UnMedComl') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := ''; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '???';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else

    ;
  except
    raise;
    Result := False;
  end;
end;

function TGeral_TbTerc.CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      {
      New(FRCampos);
      FRCampos.Field      := '???';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      }
    end else
    ;
  except
    raise;
    Result := False;
  end;
end;

function TGeral_TbTerc.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  //  ENT-CNAE2-001 :: Grupos CNAE 2.1
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CNAE2-001';
  FRJanelas.Nome      := 'FmCNAE21';
  FRJanelas.Descricao := 'Grupos CNAE 2.1';
  FLJanelas.Add(FRJanelas);
  //
  //  ENT-CNAE2-002 :: Pesquisa CNAE 20
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CNAE2-002';
  FRJanelas.Nome      := 'FmPesqCNAE21';
  FRJanelas.Descricao := 'Pesquisa CNAE 2.1';
  FLJanelas.Add(FRJanelas);
  //
  //  ENT-CNAE2-003 :: Pesquisa R�pida CNAE 2.1
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CNAE2-003';
  FRJanelas.Nome      := 'FmPesqRCNAE21';
  FRJanelas.Descricao := 'Pesquisa R�pida CNAE 2.1';
  FLJanelas.Add(FRJanelas);
  //
  // GER-TBTER-001 :: CNAE CONCLA - Estrutura Detalhada CNAE 2.1 - Subclasses
  New(FRJanelas);
  FRJanelas.ID        := 'GER-TBTER-001';
  FRJanelas.Nome      := 'FmCNAE21Cad';
  FRJanelas.Descricao := 'CNAE CONCLA - Estrutura Detalhada CNAE 2.1 - Subclasses';
  FLJanelas.Add(FRJanelas);
  //
  // GER-TBTER-002 :: IBPTax - Tabela de Al�quotas Aproximadas
  New(FRJanelas);
  FRJanelas.ID        := 'GER-TBTER-002';
  FRJanelas.Nome      := 'FmIBPTaxCad';
  FRJanelas.Descricao := 'IBPTax - Tabela de Al�quotas Aproximadas';
  FLJanelas.Add(FRJanelas);
  //
  // GER-TBTER-003 :: Carregamento da Tabela de Al�quotas Aproximadas IBPTax
  New(FRJanelas);
  FRJanelas.ID        := 'GER-TBTER-003';
  FRJanelas.Nome      := 'FmIBPTaxLoad';
  FRJanelas.Descricao := 'Caregamento da Tabela de Al�quotas Aproximadas IBPTax';
  FLJanelas.Add(FRJanelas);
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
