unit UnitDmkTags;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, Menus, UnMsgInt, Variants, UnDmkProcFunc,
  Clipbrd;

type
  TDmkTags = record
    Tag: String;
    Descricao: String;
    Grupo: TMaiEnvType;
  end;
  TTagTipo = (ttPreEmail=0, ttCNABCfg=1, ttWhatsAppBol=2, ttWhatsAppNFSe=3);
  TUnitDmkTags = class(TObject)
  private
    { Private declarations }
    procedure ConfiguraAdvToolPanel(Panel: TPanel);
    procedure ConfiguraListView(ListView: TListView);
    procedure ConfiguraGrupos(TagTipo: TTagTipo; ListView: TListView;
              DMKID_APP: Integer = 0; HabilModulos: String = '');
    procedure InsereTags(TagTipo: TTagTipo; ListView: TListView;
              DMKID_APP: Integer = 0; HabilModulos: String = '');
    function  ValidaPreEmailTipo(Tipo: TMaiEnvType; DMKID_APP: Integer;
              HabilModulos: String): Boolean;
  public
    { Public declarations }
    procedure CarregaPreEmailTags(DMKID_APP: Integer; ListView: TListView;
              Panel: TPanel; HabilModulos: String);
    procedure CarregaCNAB_CfgTags(ListView: TListView);
    procedure CarregaWhatsAppBolTags(ListView: TListView);
    procedure CarregaWhatsAppNFSeTags(ListView: TListView);
    procedure SubstituiVariaveis_Avulso(Saudacao, Cliente, Data: String;
              var Texto: String);
    procedure SubstituiVariaveis_HistoricoAlteracoes(Aplicativo, Versao,
              App_HisAlter, Link: String; var Texto: String);
    procedure SubstituiVariaveis_NFSe(Empresa, ChaveNFe, Saudacao, Cliente,
              NFSe_Serie, NFSe_Numero, Protocolo_e_Impressao,
              Protocolo: String; var Texto: String);
    procedure SubstituiVariaveis_NFe(Empresa, ChaveNFe, SerieNFe, NumeroNFe,
              Saudacao, Cliente, ClienteFantasia: String; var Texto: String);
    procedure SubstituiVariaveis_CCe(Empresa, ChaveNFe, SerieNFe, NumeroNFe,
              Saudacao, Cliente, versao, ORGAO, AMBIENTE, CNPJ_CPF, CHAVE_ACESSO,
              DATA, CODIGO_EVENTO, SEQUENCIAL_EVENTO, verEvento, descEvento,
              xCorrecao, xCondUso: String; var Texto: String);
    procedure SubstituiVariaveis_NFe_Cancelamento(Empresa, Saudacao,
              Cliente, ChaveNFe, SerieNFe, NumeroNFe: String; var Texto: String);
    procedure SubstituiVariaveis_Boletos(Empresa, Nome, Saudacao, Valor,
              Vencimento, LinhaDigitavel, Protocolo_e_Impressao, Protocolo: String;
              var Texto: String);
    procedure SubstituiVariaveis_Boletos_Condominio(Administradora, Nome,
              Saudacao, Pronome, Valor, Vencimento, LinkConfirma, Protocolo,
              Condominio: String; var Texto: String);
    procedure SubstituiVariaveis_Cobranca(Time, Date, Administradora, Saudacao,
              Pronome, Nome, Condominio, Valor, Vencimento, LinkConfirma,
              Protocolo, UH: String; var Texto: String);
    procedure AdicionaTag(ListView: TListView; Memo: TMemo);
  end;

var
  UnDmkTags: TUnitDmkTags;
const
  MaxVarsCNABCfg = 1;
  sListaVarsCNABCfg: array[0..MaxVarsCNABCfg] of TDmkTags =
  (
    (Tag: '[MULTA_ATRASO]'; Descricao: 'Valor da multa por atraso'; Grupo: meAvul),
    (Tag: '[JUROS_ATRASO]'; Descricao: 'Valor de juros ao dia'; Grupo: meAvul)
  );
  MaxVarsPreEmail = 70;
  sListaVarsPreEmail: array[0..MaxVarsPreEmail] of TDmkTags =
  (
    //Avulso
    (Tag: '[Saudacao]'; Descricao: 'Texto preenchido no campo: "Sauda��o" do pr� e-mail'; Grupo: meAvul),
    (Tag: '[Cliente]';  Descricao: 'Nome do cliente que receber� o e-mail enviando';      Grupo: meAvul),
    (Tag: '[Data]';     Descricao: '';                                                    Grupo: meAvul),
    //NFSe
    (Tag: '[Empresa]';               Descricao: 'Nome da empresa que est� enviando o e-mail';                       Grupo: meNFSe),
    (Tag: '[ChaveNFe]';              Descricao: 'Chave gerada para a NFSe a ser enviada';                           Grupo: meNFSe),
    (Tag: '[Saudacao]';              Descricao: 'Texto preenchido no campo: "Sauda��o" do pr� e-mail';              Grupo: meNFSe),
    (Tag: '[Cliente]';               Descricao: 'Nome do cliente que receber� o e-mail enviando';                   Grupo: meNFSe),
    (Tag: '[NFSe_Serie]';            Descricao: 'S�rie da nota fiscal';                                             Grupo: meNFSe),
    (Tag: '[NFSe_Numero]';           Descricao: 'N�mero da nota fiscal';                                            Grupo: meNFSe),
    (Tag: '[Protocolo_e_Impressao]'; Descricao: 'Protocolo para confirma�ao de recebimento de e-mails e impress�o'; Grupo: meNFSe),
    (Tag: '[Protocolo]';             Descricao: 'Protocolo para confirma�ao de recebimento de e-mails';             Grupo: meNFSe),
    //NFe
    (Tag: '[Empresa]';         Descricao: 'Nome da empresa que est� enviando o e-mail';                 Grupo: meNFe),
    (Tag: '[ChaveNFe]';        Descricao: 'Chave gerada para a NFe a ser enviada';                      Grupo: meNFe),
    (Tag: '[Saudacao]';        Descricao: 'Texto preenchido no campo: "Sauda��o" do pr� e-mail';        Grupo: meNFe),
    (Tag: '[Cliente]';         Descricao: 'Nome do cliente que receber� o e-mail enviando';             Grupo: meNFe),
    (Tag: '[SerieNFe]';        Descricao: 'S�rie gerada para a NFe a ser enviada';                      Grupo: meNFe),
    (Tag: '[NumeroNFe]';       Descricao: 'N�mero gerado para a NFe a ser enviada';                     Grupo: meNFe),
    (Tag: '[ClienteFantasia]'; Descricao: 'Nome fantasia do cliente que recebber� a NFe a ser enviada'; Grupo: meNFe),
    //NFe Evento Carta De Corre��o Eletr�nica
    (Tag: '[Empresa]';           Descricao: 'Nome da empresa que est� enviando o e-mail';          Grupo: meNFeEveCCE),
    (Tag: '[ChaveNFe]';          Descricao: 'Chave gerada para a NFSe a ser enviada';              Grupo: meNFeEveCCE),
    (Tag: '[Saudacao]';          Descricao: 'Texto preenchido no campo: "Sauda��o" do pr� e-mail'; Grupo: meNFeEveCCE),
    (Tag: '[Cliente]';           Descricao: 'Nome do cliente que receber� o e-mail enviando';      Grupo: meNFeEveCCE),
    (Tag: '[versao]';            Descricao: 'Nome do cliente que receber� o e-mail enviando';      Grupo: meNFeEveCCE),
    (Tag: '[ORGAO]';             Descricao: '';                                                    Grupo: meNFeEveCCE),
    (Tag: '[AMBIENTE]';          Descricao: '';                                                    Grupo: meNFeEveCCE),
    (Tag: '[CNPJ_CPF]';          Descricao: '';                                                    Grupo: meNFeEveCCE),
    (Tag: '[CHAVE_ACESSO]';      Descricao: '';                                                    Grupo: meNFeEveCCE),
    (Tag: '[DATA]';              Descricao: '';                                                    Grupo: meNFeEveCCE),
    (Tag: '[CODIGO_EVENTO]';     Descricao: '';                                                    Grupo: meNFeEveCCE),
    (Tag: '[SEQUENCIAL_EVENTO]'; Descricao: '';                                                    Grupo: meNFeEveCCE),
    (Tag: '[verEvento]';         Descricao: '';                                                    Grupo: meNFeEveCCE),
    (Tag: '[descEvento]';        Descricao: '';                                                    Grupo: meNFeEveCCE),
    (Tag: '[xCorrecao]';         Descricao: '';                                                    Grupo: meNFeEveCCE),
    (Tag: '[xCondUso]';          Descricao: '';                                                    Grupo: meNFeEveCCE),
    //NFe Evento de cancelamento
    (Tag: '[Empresa]';  Descricao: 'Nome da empresa que est� enviando o e-mail';          Grupo: meNFeEveCan),
    (Tag: '[Saudacao]'; Descricao: 'Texto preenchido no campo: "Sauda��o" do pr� e-mail'; Grupo: meNFeEveCan),
    (Tag: '[Cliente]';  Descricao: 'Nome do cliente que receber� o e-mail enviando';      Grupo: meNFeEveCan),
    (Tag: '[ChaveNFe]'; Descricao: 'Chave gerada para a NFSe a ser enviada';              Grupo: meNFeEveCan),
    //Envio de boletos
    (Tag: '[Empresa]';               Descricao: 'Nome da empresa que est� enviando o e-mail';                       Grupo: meBloq),
    (Tag: '[Nome]';                  Descricao: 'Nome do sacado do boleto';                                         Grupo: meBloq),
    (Tag: '[Saudacao]';              Descricao: 'Texto preenchido no campo: "Sauda��o" do pr� e-mail';              Grupo: meBloq),
    (Tag: '[Valor]';                 Descricao: 'Valor do boleto';                                                  Grupo: meBloq),
    (Tag: '[Vencimento]';            Descricao: 'Vencimento do boleto';                                             Grupo: meBloq),
    (Tag: '[LinhaDigitavel]';        Descricao: 'Link para confirma��o de recebimento';                             Grupo: meBloq),
    (Tag: '[Protocolo_e_Impressao]'; Descricao: 'Protocolo para confirma�ao de recebimento de e-mails e impress�o'; Grupo: meBloq),
    (Tag: '[Protocolo]';             Descricao: 'Protocolo para confirma�ao de recebimento de e-mails';             Grupo: meBloq),
    //Envio de boletos - Condom�nios
    (Tag: '[Administradora]'; Descricao: 'Nome da administradora que est� enviando o e-mail';     Grupo: meBloqCnd),
    (Tag: '[Nome]';           Descricao: 'Nome do sacado do boleto';                              Grupo: meBloqCnd),
    (Tag: '[Saudacao]';       Descricao: 'Texto preenchido no campo: "Sauda��o" do pr� e-mail';   Grupo: meBloqCnd),
    (Tag: '[Pronome]';        Descricao: '';                                                      Grupo: meBloqCnd),
    (Tag: '[Valor]';          Descricao: 'Valor do boleto';                                       Grupo: meBloqCnd),
    (Tag: '[Vencimento]';     Descricao: 'Vencimento do boleto';                                  Grupo: meBloqCnd),
    (Tag: '[LinkConfirma]';   Descricao: 'Link para confirma��o de recebimento';                  Grupo: meBloqCnd),
    (Tag: '[Protocolo]';      Descricao: 'Protocolo para confirma�ao em e-mails sem confirma��o'; Grupo: meBloqCnd),
    (Tag: '[Condominio]';     Descricao: 'Nome do condom�nio que est� enviando o e-mail';         Grupo: meBloqCnd),
    // Cobran�a
    (Tag: '[Time]';           Descricao: '';                                                      Grupo: meCobr),
    (Tag: '[Date]';           Descricao: '';                                                      Grupo: meCobr),
    (Tag: '[Administradora]'; Descricao: 'Nome da administradora que est� enviando o e-mail';     Grupo: meCobr),
    (Tag: '[Saudacao]';       Descricao: 'Texto preenchido no campo: "Sauda��o" do pr� e-mail';   Grupo: meCobr),
    (Tag: '[Pronome]';        Descricao: '';                                                      Grupo: meCobr),
    (Tag: '[Nome]';           Descricao: 'Nome do devedor';                                       Grupo: meCobr),
    (Tag: '[Condominio]';     Descricao: 'Nome do condom�nio que est� enviando o e-mail';         Grupo: meCobr),
    (Tag: '[Valor]';          Descricao: 'Valor da cobran�a';                                     Grupo: meCobr),
    (Tag: '[Vencimento]';     Descricao: 'Vencimento da cobran�a';                                Grupo: meCobr),
    (Tag: '[LinkConfirma]';   Descricao: 'Link para confirma��o de recebimento';                  Grupo: meCobr),
    (Tag: '[Protocolo]';      Descricao: 'Protocolo para confirma�ao em e-mails sem confirma��o'; Grupo: meCobr),
    (Tag: '[UH]';             Descricao: 'N�mero da unidade habitacional que est� sendo cobrada'; Grupo: meCobr),
    // Aplicativos - Hist�rico de altera��es
    (Tag: '[APLICATIVO]';   Descricao: 'Nome do aplicativo';                                Grupo: meHisAlt),
    (Tag: '[VERSAO]';       Descricao: 'Vers�o do aplicativo';                              Grupo: meHisAlt),
    (Tag: '[APP_HISALTER]'; Descricao: 'Lista com as novidades da vers�o';                  Grupo: meHisAlt),
    (Tag: '[LINK]';         Descricao: 'URL para acessar o hist�rico de altera��es na WEB'; Grupo: meHisAlt)
  );

implementation

uses MyDBCheck, UnDmkEnums, UnGrl_Geral;

{ TUnitDmkTags }

procedure TUnitDmkTags.CarregaPreEmailTags(DMKID_APP: Integer; ListView: TListView;
  Panel: TPanel; HabilModulos: String);
begin
  //////////////////////////////////////////////////////////////////////////
  // * Ao criar um novo modelo de e-mail que utilize TAGs adicionar       //
  //   no "sListaVarsPreEmail" e "TFmMailEnv.SubstituiVariaveis"          //
  // * Ao criar um novo tipo de e-mail adicionar nas fun��es:             //
  //   "TUnitDmkTags.ConfiguraGrupos", "TUnitDmkTags.ValidaPreEmailTipo"  //
  //////////////////////////////////////////////////////////////////////////
  //
  ConfiguraAdvToolPanel(Panel);
  ConfiguraListView(ListView);
  ConfiguraGrupos(ttPreEmail, ListView, DMKID_APP, HabilModulos);
  InsereTags(ttPreEmail, ListView, DMKID_APP, HabilModulos);
end;

procedure TUnitDmkTags.CarregaWhatsAppBolTags(ListView: TListView);
begin
  ///////////////////////////////////////////////////////////////////////////
  // * Ao criar uma nova tag nas op��es de boletos Whatsapp adicionar      //
  //   no "sListaVarsPreEmail" e "TUnitDmkTags.SubstituiVariaveis_Boletos" //
  ///////////////////////////////////////////////////////////////////////////
  //
  ConfiguraListView(ListView);
  ConfiguraGrupos(ttWhatsAppBol, ListView);
  InsereTags(ttWhatsAppBol, ListView);
end;

procedure TUnitDmkTags.CarregaWhatsAppNFSeTags(ListView: TListView);
begin
  //////////////////////////////////////////////////////////////////////////
  // * Ao criar uma nova tag nas op��es de NFSe Whatsapp adicionar        //
  //   no "sListaVarsPreEmail" e "TUnitDmkTags.SubstituiVariaveis_NFSe"   //
  //////////////////////////////////////////////////////////////////////////
  //
  ConfiguraListView(ListView);
  ConfiguraGrupos(ttWhatsAppNFSe, ListView);
  InsereTags(ttWhatsAppNFSe, ListView);
end;

procedure TUnitDmkTags.CarregaCNAB_CfgTags(ListView: TListView);
begin
  //////////////////////////////////////////////////////////////////////////
  // * Ao criar uma nova tag no CNAB_Cfg que utilize TAGs adicionar       //
  //   no "sListaVarsCNABCfg" e "TDmBco.TraduzInstrucaoBloqueto"          //
  //////////////////////////////////////////////////////////////////////////
  //
  ConfiguraListView(ListView);
  ConfiguraGrupos(ttCNABCfg, ListView);
  InsereTags(ttCNABCfg, ListView);
end;

procedure TUnitDmkTags.AdicionaTag(ListView: TListView; Memo: TMemo);
var
  Texto: String;
  Ate: Integer;
  Sel: TListItem;
begin
  Sel := ListView.Selected;
  //
  if Sel <> nil then
  begin
    Texto := Sel.Caption;
    //
    if Texto <> '' then
    begin
      Ate := Pos(']', Texto);
      Texto := Copy(Texto, 1, Ate);
      Clipboard.AsText := Texto;
      Memo.PasteFromClipboard;
    end;
    ListView.ClearSelection;
    Memo.SetFocus;
  end;
end;

procedure TUnitDmkTags.ConfiguraAdvToolPanel(
  Panel: TPanel);
begin
{
  Panel.Caption := 'TAGs';
  //
  Panel.ShowCaption := True;
  Panel.ShowClose   := False;
  Panel.ShowLock    := True;
  //
  Panel.Style := esTerminal;
  //
  Panel.OpenWidth := 700;
  Panel.Width     := 700;
}
end;

procedure TUnitDmkTags.ConfiguraGrupos(TagTipo: TTagTipo; ListView: TListView;
  DMKID_APP: Integer = 0; HabilModulos: String = '');

  procedure InsereGrupos(ListView: TListView);

    procedure CriaGrupo(Grupo: TMaiEnvType);
    var
      Item: TListGroup;
      Id: Integer;
      Texto: String;
    begin
      Id    := Integer(Grupo);
      Texto := dmkPF.ObtemNomeEmailTipo(Grupo);
      //
      Item := ListView.Groups.Add;
      Item.GroupID := Id;
      Item.Header  := Texto;
      Item.State   := [lgsCollapsible];
    end;

  begin
    if TagTipo = ttPreEmail then
    begin
      if ValidaPreEmailTipo(meAvul, DMKID_APP, HabilModulos) then
        CriaGrupo(meAvul);
      if ValidaPreEmailTipo(meNFSe, DMKID_APP, HabilModulos) then
        CriaGrupo(meNFSe);
      if ValidaPreEmailTipo(meNFe, DMKID_APP, HabilModulos) then
        CriaGrupo(meNFe);
      if ValidaPreEmailTipo(meNFeEveCCE, DMKID_APP, HabilModulos) then
        CriaGrupo(meNFeEveCCE);
      if ValidaPreEmailTipo(meNFeEveCan, DMKID_APP, HabilModulos) then
        CriaGrupo(meNFeEveCan);
      if ValidaPreEmailTipo(meBloq, DMKID_APP, HabilModulos) then
        CriaGrupo(meBloq);
      if ValidaPreEmailTipo(meBloqCnd, DMKID_APP, HabilModulos) then
        CriaGrupo(meBloqCnd);
      if ValidaPreEmailTipo(meCobr, DMKID_APP, HabilModulos) then
        CriaGrupo(meCobr);
      if ValidaPreEmailTipo(meHisAlt, DMKID_APP, HabilModulos) then
        CriaGrupo(meHisAlt);
    end else
    if TagTipo = ttCNABCfg then
      CriaGrupo(meAvul)
    else if TagTipo = ttWhatsAppBol then
      CriaGrupo(meBloq)
    else
      CriaGrupo(meNFSe);
  end;

begin
  ListView.Groups.BeginUpdate;
  try
    InsereGrupos(ListView);
  finally
    ListView.Groups.EndUpdate;
  end;
end;

function TUnitDmkTags.ValidaPreEmailTipo(Tipo: TMaiEnvType; DMKID_APP: Integer;
  HabilModulos: String): Boolean;
begin
  case Tipo of
    meAvul:
    begin
      Result := True;
    end;
    meNFSe:
    begin
      {$IfNDef sNFSe}
        Result := Grl_Geral.LiberaModulo(DMKID_APP, Grl_Geral.ObtemSiglaModulo(mdlappNFSe),
                    HabilModulos);
      {$Else}
        Result := False;
      {$EndIf}
    end;
    meNFe, meNFeEveCCE, meNFeEveCan:
    begin
      {$IfNDef SemNFe_0000}
        Result := Grl_Geral.LiberaModulo(DMKID_APP, Grl_Geral.ObtemSiglaModulo(mdlappNFe),
                    HabilModulos);
      {$Else}
        Result := False;
      {$EndIf}
    end;
    meBloq:
    begin
    {$IfNDef sBLQ}
      Result := Grl_Geral.LiberaModulo(DMKID_APP, Grl_Geral.ObtemSiglaModulo(mdlappBloquetos),
                  HabilModulos);
    {$Else}
      Result := False;
    {$EndIf}
    end;
    meCobr, meBloqCnd:
    begin
      if DMKID_APP in [4, 43] then //Syndi2 e Syndi3
        Result := True
      else
        Result := False;
    end;
    meHisAlt:
    begin
      if DMKID_APP = 17 then //DControl
        Result := True
      else
        Result := False;
    end;
    else
    begin
      (*
      Geral.MB_Erro('Tipo "' + dmkPF.ObtemNomeEmailTipo(Tipo) +
        '" n�o implementado!' + sLineBreak +
        'Fun��o: "TUnitDmkTags.ValidaPreEmailTipo"');
      *)
      Result := False;
    end;
  end;
end;

procedure TUnitDmkTags.ConfiguraListView(ListView: TListView);
var
  Item: TListColumn;
begin
  ListView.Columns.Clear;
  ListView.Groups.Clear;
  ListView.Items.Clear;
  //
  Item := ListView.Columns.Add;
  Item.Caption  := 'TAG';
  Item.Width    := 150;
  //
  Item := ListView.Columns.Add;
  Item.Caption  := 'Descri��o';
  Item.Width    := 450;
  //
  ListView.HideSelection     := True;
  ListView.ViewStyle         := vsReport;
  ListView.ShowColumnHeaders := False;
  ListView.GroupView         := True;
  ListView.RowSelect         := True;
  ListView.ReadOnly          := True;
  ListView.Font.Size         := 10;
  ListView.SortType          := stData;
end;

procedure TUnitDmkTags.SubstituiVariaveis_Cobranca(Time, Date, Administradora,
  Saudacao, Pronome, Nome, Condominio, Valor, Vencimento, LinkConfirma,
  Protocolo, UH: String; var Texto: String);
begin
  Texto := Geral.Substitui(Texto, '[Time]'           , Time);
  Texto := Geral.Substitui(Texto, '[Date]'           , Date);
  Texto := Geral.Substitui(Texto, '[Administradora]' , Administradora);
  Texto := Geral.Substitui(Texto, '[Saudacao]'       , Saudacao);
  Texto := Geral.Substitui(Texto, '[Pronome]'        , Pronome);
  Texto := Geral.Substitui(Texto, '[Nome]'           , Nome);
  Texto := Geral.Substitui(Texto, '[Condominio]'     , Condominio);
  Texto := Geral.Substitui(Texto, '[Valor]'          , Valor);
  Texto := Geral.Substitui(Texto, '[Vencimento]'     , Vencimento);
  Texto := Geral.Substitui(Texto, '[LinkConfirma]'   , LinkConfirma);
  Texto := Geral.Substitui(Texto, '[Protocolo]'      , Protocolo);
  Texto := Geral.Substitui(Texto, '[UH]'             , UH);
end;

procedure TUnitDmkTags.SubstituiVariaveis_Boletos_Condominio(Administradora,
  Nome, Saudacao, Pronome, Valor, Vencimento, LinkConfirma, Protocolo,
  Condominio: String; var Texto: String);
begin
  Texto := Geral.Substitui(Texto, '[Administradora]', Administradora);
  Texto := Geral.Substitui(Texto, '[Nome]'          , Nome);
  Texto := Geral.Substitui(Texto, '[Saudacao]'      , Saudacao);
  Texto := Geral.Substitui(Texto, '[Pronome]'       , Pronome);
  Texto := Geral.Substitui(Texto, '[Valor]'         , Valor);
  Texto := Geral.Substitui(Texto, '[Vencimento]'    , Vencimento);
  Texto := Geral.Substitui(Texto, '[LinkConfirma]'  , LinkConfirma);
  Texto := Geral.Substitui(Texto, '[Protocolo]'     , Protocolo);
  Texto := Geral.Substitui(Texto, '[Condominio]'    , Condominio);
end;

procedure TUnitDmkTags.SubstituiVariaveis_Boletos(Empresa, Nome, Saudacao,
  Valor, Vencimento, LinhaDigitavel, Protocolo_e_Impressao, Protocolo: String;
  var Texto: String);
begin
  Texto := Geral.Substitui(Texto, '[Empresa]'              , Empresa);
  Texto := Geral.Substitui(Texto, '[Nome]'                 , Nome);
  Texto := Geral.Substitui(Texto, '[Saudacao]'             , Saudacao);
  Texto := Geral.Substitui(Texto, '[Valor]'                , Valor);
  Texto := Geral.Substitui(Texto, '[Vencimento]'           , Vencimento);
  Texto := Geral.Substitui(Texto, '[LinhaDigitavel]'       , LinhaDigitavel);
  Texto := Geral.Substitui(Texto, '[Protocolo_e_Impressao]', Protocolo_e_Impressao);
  Texto := Geral.Substitui(Texto, '[Protocolo]'            , Protocolo);
end;

procedure TUnitDmkTags.SubstituiVariaveis_NFe_Cancelamento(Empresa,
  Saudacao, Cliente, ChaveNFe, SerieNFe, NumeroNFe: String; var Texto: String);
begin
  Texto := Geral.Substitui(Texto, '[Empresa]' ,  Empresa);
  Texto := Geral.Substitui(Texto, '[Saudacao]',  Saudacao);
  Texto := Geral.Substitui(Texto, '[Cliente]' ,  Cliente);
  Texto := Geral.Substitui(Texto, '[ChaveNFe]',  ChaveNFe);
  Texto := Geral.Substitui(Texto, '[SerieNFe]',  SerieNFe);
  Texto := Geral.Substitui(Texto, '[NumeroNFe]', NumeroNFe);
end;

procedure TUnitDmkTags.SubstituiVariaveis_CCe(Empresa, ChaveNFe, SerieNFe,
  NumeroNFe, Saudacao, Cliente, versao, ORGAO, AMBIENTE, CNPJ_CPF, CHAVE_ACESSO,
  DATA, CODIGO_EVENTO, SEQUENCIAL_EVENTO, verEvento, descEvento, xCorrecao,
  xCondUso: String; var Texto: String);
begin
  Texto := Geral.Substitui(Texto, '[Empresa]'          , Empresa);
  Texto := Geral.Substitui(Texto, '[ChaveNFe]'         , ChaveNFe);
  Texto := Geral.Substitui(Texto, '[SerieNFe]'         , SerieNFe);
  Texto := Geral.Substitui(Texto, '[NumeroNFe]'        , NumeroNFe);
  Texto := Geral.Substitui(Texto, '[Saudacao]'         , Saudacao);
  Texto := Geral.Substitui(Texto, '[Cliente]'          , Cliente);
  Texto := Geral.Substitui(Texto, '[versao]'           , versao);
  Texto := Geral.Substitui(Texto, '[ORGAO]'            , ORGAO);
  Texto := Geral.Substitui(Texto, '[AMBIENTE]'         , AMBIENTE);
  Texto := Geral.Substitui(Texto, '[CNPJ_CPF]'         , CNPJ_CPF);
  Texto := Geral.Substitui(Texto, '[CHAVE_ACESSO]'     , CHAVE_ACESSO);
  Texto := Geral.Substitui(Texto, '[DATA]'             , DATA);
  Texto := Geral.Substitui(Texto, '[CODIGO_EVENTO]'    , CODIGO_EVENTO);
  Texto := Geral.Substitui(Texto, '[SEQUENCIAL_EVENTO]', SEQUENCIAL_EVENTO);
  Texto := Geral.Substitui(Texto, '[verEvento]'        , verEvento);
  Texto := Geral.Substitui(Texto, '[descEvento]'       , descEvento);
  Texto := Geral.Substitui(Texto, '[xCorrecao]'        , xCorrecao);
  Texto := Geral.Substitui(Texto, '[xCondUso]'         , xCondUso);
end;

procedure TUnitDmkTags.SubstituiVariaveis_NFe(Empresa, ChaveNFe, SerieNFe,
  NumeroNFe, Saudacao, Cliente, ClienteFantasia: String; var Texto: String);
begin
  Texto := Geral.Substitui(Texto, '[Empresa]' ,        Empresa);
  Texto := Geral.Substitui(Texto, '[ChaveNFe]',        ChaveNFe);
  Texto := Geral.Substitui(Texto, '[SerieNFe]',        SerieNFe);
  Texto := Geral.Substitui(Texto, '[NumeroNFe]',       NumeroNFe);
  Texto := Geral.Substitui(Texto, '[Saudacao]',        Saudacao);
  Texto := Geral.Substitui(Texto, '[Cliente]',         Cliente);
  Texto := Geral.Substitui(Texto, '[ClienteFantasia]', ClienteFantasia);
end;

procedure TUnitDmkTags.SubstituiVariaveis_NFSe(Empresa, ChaveNFe, Saudacao,
  Cliente, NFSe_Serie, NFSe_Numero, Protocolo_e_Impressao, Protocolo: String;
  var Texto: String);
begin
  Texto := Geral.Substitui(Texto, '[Empresa]' ,              Empresa);
  Texto := Geral.Substitui(Texto, '[ChaveNFe]',              ChaveNFe);
  Texto := Geral.Substitui(Texto, '[Saudacao]',              Saudacao);
  Texto := Geral.Substitui(Texto, '[Cliente]' ,              Cliente);
  Texto := Geral.Substitui(Texto, '[NFSe_Serie]',            NFSe_Serie);
  Texto := Geral.Substitui(Texto, '[NFSe_Numero]',           NFSe_Numero);
  Texto := Geral.Substitui(Texto, '[Protocolo_e_Impressao]', Protocolo_e_Impressao);
  Texto := Geral.Substitui(Texto, '[Protocolo]',             Protocolo);
end;

procedure TUnitDmkTags.SubstituiVariaveis_HistoricoAlteracoes(Aplicativo,
  Versao, App_HisAlter, Link: String; var Texto: String);
begin
  Texto := Geral.Substitui(Texto, '[APLICATIVO]',   Aplicativo);
  Texto := Geral.Substitui(Texto, '[VERSAO]' ,      Versao);
  Texto := Geral.Substitui(Texto, '[APP_HISALTER]', App_HisAlter);
  Texto := Geral.Substitui(Texto, '[LINK]' ,        Link);
end;

procedure TUnitDmkTags.SubstituiVariaveis_Avulso(Saudacao, Cliente, Data: String;
  var Texto: String);
begin
  Texto := Geral.Substitui(Texto, '[Saudacao]', Saudacao);
  Texto := Geral.Substitui(Texto, '[Cliente]' , Cliente);
  Texto := Geral.Substitui(Texto, '[Data]'    , Data);
end;

procedure TUnitDmkTags.InsereTags(TagTipo: TTagTipo; ListView: TListView;
  DMKID_APP: Integer = 0; HabilModulos: String = '');
var
  Adiciona: Boolean;
  I, Tot: Integer;
  Tag: TDmkTags;
  Descricao, TagNome: String;
  Grupo: TMaiEnvType;
  Item: TListItem;
begin
  ListView.Groups.BeginUpdate;
  try
    ListView.Items.Clear;
    //
    if (TagTipo = ttPreEmail) or (TagTipo = ttWhatsAppBol) or
      (TagTipo = ttWhatsAppNFSe)
    then
      Tot := Length(sListaVarsPreEmail)
    else
      Tot := Length(sListaVarsCNABCfg);
    //
    for I := 0 to Tot - 1 do
    begin
      if TagTipo = ttPreEmail then
        Tag := sListaVarsPreEmail[I]
      else
        Tag := sListaVarsCNABCfg[I];
      //
      TagNome   := Tag.Tag;
      Descricao := Tag.Descricao;
      Grupo     := Tag.Grupo;
      //
      if TagTipo = ttPreEmail then
        Adiciona := ValidaPreEmailTipo(Grupo, DMKID_APP, HabilModulos)
      else
      if TagTipo = ttCNABCfg then
        Adiciona := True
      else
      if TagTipo = ttWhatsAppBol then
        Adiciona := Grupo = meBloq
      else
        Adiciona := Grupo = meNFSe;
      //
      if Adiciona then
      begin
        Item := ListView.Items.Add;
        Item.GroupID    := Integer(Grupo);
        Item.Caption    := TagNome;
        Item.SubItems.Add(Descricao);
      end;
    end;
  finally
    ListView.Items.EndUpdate;
  end;
end;

end.
