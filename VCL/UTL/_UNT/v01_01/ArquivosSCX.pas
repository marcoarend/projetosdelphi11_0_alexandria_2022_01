unit ArquivosSCX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  (*DBTables,*) StdCtrls, comctrls, mySQLDbTables, Menus, Grids,
  dmkEdit, dmkGeral, UnDmkEnums;

type
  TArquivosSCX = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ConfiguraGrades(GradeC, GradeD: TStringGrid;
              Pagina: TPageControl);
    procedure DefineContagens(GradeC, GradeD: TStringGrid);
    procedure CalculaGradeC(EdQtdeCh, EdValorCH, EdQtdeTo, EdValorTO, EdValorDU:
              TdmkEdit; GradeC, GradeD: TStringGrid; TPCheque: TDateTimePicker);
    procedure CalculaGradeD(EdQtdeDU, EdValorDU, EdQtdeTo, EdValorTO,
              EdValorCH: TdmkEdit; GradeC, GradeD: TStringGrid; TPCheque: TDateTimePicker);
    procedure GravaSetor(Setor: String);
    procedure GravaItem(Chave, Valor: String);
    procedure LimpaGrade(GradeALimpar: TStringGrid; ColIni, RowIni: Integer);
    procedure SalvaArquivo(Pergunta: Boolean; EdCodCli, EdNomCli: TdmkEdit;
              Progress: TProgressBar; GradeC, GradeD: TStringGrid;
              TPCheque: TDateTimePicker; FileName: String;
              ChangeData: Boolean);

    function GravaArquivoCript(S: String; ContaStr, FatorCript: Integer): String;
    function AbreArquivoSCX(GradeC, GradeD: TStringGrid; EdCodCli, EdNomCli:
             TdmkEdit;  TPCheque: TDateTimePicker; Progress: TProgressBar;
             DirIni: String): Boolean;
    function AbreArquivoCript(NomeArq: String; FatorCript: Integer): TStringList;
    function PegaChaveDeColunaC(Coluna: Integer): String;
    function PegaColunaDeChaveC(Chave: String): Integer;
    function PegaChaveDeColunaD(Coluna: Integer): String;
    function PegaColunaDeChaveD(Chave: String): Integer;
  end;

const

  CO_C1 = 52845;
  CO_C2 = 22719;
  //
  COL_01 = 1;
  COL_02 = 2;
  COL_03 = 3;
  COL_04 = 4;
  COL_05 = 5;
  COL_06 = 6;
  COL_07 = 7;
  COL_08 = 8;
  COL_09 = 9;
  CMsgErroLendoArq = 'Erro ao ler arquivo!';
  CMsgArqDif       = 'A vers�o do arquivo a ser aberto � superior a'+
                     'vers�o gerada por este aplicativo. Para prevenir a '+
                     'perda de dados, n�o salve o arquivo com o mesmo nome '+
                     'ao utilizar esta vers�o do aplicativo';
  //
  CCaption = 'Malotes de Cheques';
  CVirgula = ',';
  //
  CTitulo = 'T�tulo';
  CVersao = 'Vers�o';
  CTitArq = 'SuitCash';
  CSetVer = '[VERS]';
  CSetCab = '[HEAD]';
  CCodCli = 'CodCli';
  CNomCli = 'NomCli';
  CDatadd = 'Datadd';
  CDatamm = 'Datamm';
  CDataYY = 'DataYY';
  CSetBod = '[BODX';
  CSetBoc = '[BODY';

var
  ArqSCX: TArquivosSCX;

implementation

uses UnMyObjects, Principal, Module;

var
  FTextoStr: String;
  FContaStr, FFatorCript: Integer;

procedure TArquivosSCX.GravaSetor(Setor: String);
begin
  FTextoStr := FTextoStr + Setor + sLineBreak;
  FContaStr := FContaStr + Length(Setor) + 2;
end;

procedure TArquivosSCX.GravaItem(Chave, Valor: String);
begin
  if Valor <> '' then
  begin
    FTextoStr := FTextoStr + Chave+'='+Valor + sLineBreak;
    FContaStr := FContaStr + Length(Chave)+Length(Valor)+3;
  end;
end;

procedure TArquivosSCX.LimpaGrade(GradeALimpar: TStringGrid; ColIni, RowIni: Integer);
var
  i, j: Integer;
begin
  for i := ColIni to GradeALimpar.ColCount -1 do
    for j := RowIni to GradeALimpar.RowCount -1 do GradeALimpar.Cells[i,j] := '';
  GradeALimpar.RowCount := 2;
end;

function TArquivosSCX.AbreArquivoSCX(GradeC, GradeD: TStringGrid; EdCodCli,
EdNomCli: TdmkEdit; TPCheque: TDateTimePicker; Progress: TProgressBar;
DirIni: String): Boolean;
var
  //ver,
  i, TamC, LiC, LiD: Integer;
  Atual, Chave, Valor: String;
  ListaRKC: TStringList;
  Datadd, Datamm, DataYY: Word;

begin
  Result := False;
  LiC := 0;
  LiD := 0;
  Datadd := 0;
  Datamm := 0;

  if DirIni <> '' then
    FmPrincipal.OpenDialog1.InitialDir := DirIni
  else if FmPrincipal.FImportPath <> '' then
    FmPrincipal.OpenDialog1.InitialDir := FmPrincipal.FImportPath
  else
    FmPrincipal.OpenDialog1.InitialDir := ExtractFilePath(Application.ExeName);
  if FmPrincipal.OpenDialog1.Execute then
  begin
    LimpaGrade(GradeC, 1, 1);
    LimpaGrade(GradeD, 1, 1);
    ListaRKC := TStringList.Create;
    try
      ListaRKC := AbreArquivoCript(FmPrincipal.OpenDialog1.FileName, FFatorCript);
      //ListBox1.Items.Clear;
      //ListBox1.Items := ListaRKC;
      i := 0;
      if Progress <> nil then
      begin
        Progress.Position := 0;
        Progress.Visible := True;
        Progress.Max := ListaRKC.Count;
      end;
      while i < ListaRKC.Count do
      begin
        if Progress <> nil then
        begin
          Progress.Position := Progress.Position + 1;
          Progress.Update;
          Application.ProcessMessages;
        end;
        if ListaRKC[i][1] = '[' then Atual := ListaRKC[i]
        else begin
          TamC := Pos('=', ListaRKC[i]) -1;
          if TamC < 1 then
          begin
            Geral.MB_Erro(CMsgErroLendoArq);
            Exit;
          end else begin
            Chave := Copy(ListaRKC[i], 1, TamC);
            Valor := Copy(ListaRKC[i], TamC+2, Length(ListaRKC[i])-TamC-1);
            //
            if (Atual = CSetVer) then
            begin
              if Chave = CTitulo then
              begin
                if Valor <> CTitArq then
                begin
                  Geral.MB_Aviso('Arquivo inv�lido!');
                  Exit;
                end;
              end else if Chave = CVersao then
              begin
                FmPrincipal.FVerArqSCX := Geral.IMV(Valor);
                //Ver := Geral.IMV(Geral.SoNumero_TT(
                // M L A G e r a l .FileVerInfo(Application.ExeName, 3)));
                (*if FmPrincipal.FVerArqSCX > Ver then
                begin
                  Geral.MB_Aviso(CMsgArqDif);
                end;*)
              end;
            end else if (Atual = CSetCab) then
            begin
              if Chave = CCodCli then EdCodCli.Text := Valor
              else if Chave = CNomCli then EdNomCli.Text := Valor
              else if Chave = CDatadd then Datadd := Geral.IMV(Valor)
              else if Chave = CDatamm then Datamm := Geral.IMV(Valor)
              else if Chave = CDataYY then
              begin
                 DataYY := Geral.IMV(Valor);
                 TPCheque.Date := EncodeDate(DataYY, Datamm, Datadd);
              end;
              /////////////////////
            end else if (Copy(Atual, 1, 5) = CSetBoc) then
            begin
              GradeC.Cells[PegaColunaDeChaveC(Chave), LiC] := Valor;
            //
            end else if (Copy(Atual, 1, 5) = CSetBod) then
            begin
              GradeD.Cells[PegaColunaDeChaveD(Chave), LiD] := Valor;
            //
            end;
          end;
        end;
        //
        if (Copy(Atual, 1, 5) = CSetBoc) then
        begin
          LiC := Geral.IMV(Copy(Atual, 6, Length(Atual)-5));
          GradeC.RowCount := LiC+1;
        end else
        if (Copy(Atual, 1, 5) = CSetBod) then
        begin
          LiD := Geral.IMV(Copy(Atual, 6, Length(Atual)-5));
          GradeD.RowCount := LiD+1;
        end;
        //
        inc(i);
      end;
      Result := True;
    except
      Result := False;
      ListaRKC.Free;
    end;
  end;
  if Progress <> nil then Progress.Visible := False;
end;

function TArquivosSCX.PegaChaveDeColunaC(Coluna: Integer): String;
var
  Val: String;
begin
  case Coluna of
    00: Val := 'Sq';
    01: Val := 'Cp';
    02: Val := 'Bc';
    03: Val := 'Ag';
    04: Val := 'CC';
    05: Val := 'Ch';
    06: Val := 'R$';
    07: Val := 'Vc';
    08: Val := 'dd';
    09: Val := 'CP';
    10: Val := 'Em';
    else Val := '?';
  end;
  Result := Val;
end;

function TArquivosSCX.PegaColunaDeChaveC(Chave: String): Integer;
var
  Val: Integer;
begin
  if Chave =
   'Sq' then Val := 00 else if Chave =
   'Cp' then Val := 01 else if Chave =
   'Bc' then Val := 02 else if Chave =
   'Ag' then Val := 03 else if Chave =
   'CC' then Val := 04 else if Chave =
   'Ch' then Val := 05 else if Chave =
   'R$' then Val := 06 else if Chave =
   'Vc' then Val := 07 else if Chave =
   'dd' then Val := 08 else if Chave =
   'CP' then Val := 09 else if Chave =
   'Em' then Val := 10 else Val := 0;
  Result := Val;
end;

function TArquivosSCX.PegaChaveDeColunaD(Coluna: Integer): String;
var
  Val: String;
begin
  case Coluna of
    00: Val := 'Sq';
    01: Val := 'Em';
    02: Val := 'Du';
    03: Val := 'T$';
    04: Val := 'D$';
    05: Val := 'R$';
    06: Val := 'Vc';
    07: Val := 'CN';
    08: Val := 'Sc';
    09: Val := 'Ru';
    10: Val := 'Nu';
    11: Val := 'Cm';
    12: Val := 'Ba';
    13: Val := 'Ci';
    14: Val := 'CP';
    15: Val := 'UF';
    16: Val := 'Te';
    17: Val := 'IE';
    else Val := '?';
  end;
  Result := Val;
end;

function TArquivosSCX.PegaColunaDeChaveD(Chave: String): Integer;
var
  Val: Integer;
begin
  if Chave =
   'Sq' then Val := 00 else if Chave =
   'Em' then Val := 01 else if Chave =
   'Du' then Val := 02 else if Chave =
   'T$' then Val := 03 else if Chave =
   'D$' then Val := 04 else if Chave =
   'R$' then Val := 05 else if Chave =
   'Vc' then Val := 06 else if Chave =
   'CN' then Val := 07 else if Chave =
   'Sc' then Val := 08 else if Chave =
   'Ru' then Val := 09 else if Chave =
   'Nu' then Val := 10 else if Chave =
   'Cm' then Val := 11 else if Chave =
   'Ba' then Val := 12 else if Chave =
   'Ci' then Val := 13 else if Chave =
   'CP' then Val := 14 else if Chave =
   'UF' then Val := 15 else if Chave =
   'Te' then Val := 16 else if Chave =
   'IE' then Val := 17
   else Val := 0;
  Result := Val;
end;

function TArquivosSCX.AbreArquivoCript(NomeArq: String; FatorCript: Integer): TStringList;
var
  iFileHandle: Integer;
  iFileLength: Integer;
  iBytesRead: Integer;
  Buffer: PChar;
  Caracter: Char;
  i, C3: Integer;
  b: byte;
  Char10: Boolean;
  Linha, num, Fator: String;
  Resultado: TStringList;
begin
  Resultado := TStringList.Create;
  try
    b := 0;
    iFileHandle := FileOpen(NomeArq, fmOpenRead);
    iFileLength := FileSeek(iFileHandle,0,2);
    FileSeek(iFileHandle,0,0);
    Buffer := PChar(AllocMem(iFileLength + 1));
    Char10 := False;
    Fator := '00000';
    try
      iBytesRead := FileRead(iFileHandle, Buffer^, iFileLength);
      FileClose(iFileHandle);
      for i := 0 to 4 do
      begin
        num := FormatFloat('000', Byte(Buffer[i]));
        Fator[i+1] := num[3];
      end;
      FFatorCript := StrToInt(Fator);
      C3 := FFatorCript;
      for i := 5 to iBytesRead -3 do
      begin
        if b = 255 then
        begin
          b := 0;
          C3 := FFatorCript;
        end else b := b + 1;
        Caracter := char(Byte(Buffer[i]) xor (C3 shr 8));
        C3 := (b + C3) * CO_C1 + CO_C2;
        if Caracter = Char(13) then Char10 := True
        else if (Caracter = Char(10)) and Char10 then
        begin
          Resultado.Add(Linha);
          Char10 := False;
          Linha := '';
        end else Linha := Linha + Caracter;
      end;
    finally
      FreeMem(Buffer);
    end;
    Result := Resultado;
  except
    raise;
    Result := Resultado;
    Result.Add('ERRO');
    Resultado.Free;
  end;
end;

procedure TArquivosSCX.SalvaArquivo(Pergunta: Boolean; EdCodCli, EdNomCli: TdmkEdit;
Progress: TProgressBar; GradeC, GradeD: TStringGrid; TPCheque: TDateTimePicker;
FileName: String; ChangeData: Boolean);
var
  i, j: Integer;
  Continua: Integer;
  Arq: TextFile;
begin
  if Geral.IMV(EdCodCli.Text) < 1 then
  begin
    Geral.MB_Aviso('Informe o c�digo de cliente!');
    Exit;
  end;
  Continua := ID_YES;
  if Pergunta then
    Continua :=  Geral.MB_Pergunta(
    'Deseja salvar as altera��es do arquivo "' + FileName + '"?');
  if Continua <> ID_YES then Exit;
  ///
  Screen.Cursor := crHourGlass;
  try
    FTextoStr := '';
    FContaStr := 0;
    //
    Progress.Position := 0;
    Progress.Max := GradeC.RowCount -1;
    Progress.Visible := True;
    //
    if FileExists(FileName) then DeleteFile(FileName);
    AssignFile(Arq, FileName);
    ReWrite(Arq);
    GravaSetor(CSetVer);
    GravaItem(CTitulo, CTitArq);
    GravaItem(CVersao, IntToStr(Geral.VersaoInt2006(
      Geral.FileVerInfo(Application.ExeName, 3))));
    ////////////////////////////////////////////////
    GravaSetor(CSetCab);
    GravaItem(CCodCli, EdCodCli.Text);
    GravaItem(CNomCli, EdNomCli.Text);
    GravaItem(CDatadd, FormatDateTime('dd', TPCheque.Date));
    GravaItem(CDatamm, FormatDateTime('mm', TPCheque.Date));
    GravaItem(CDataYY, FormatDateTime('yyyy', TPCheque.Date));
    /////////////////////////////////////////////////////
    for i := 1 to GradeC.RowCount -1 do
    begin
      Progress.Position := Progress.Position +1;
      GravaSetor(CSetBoc+IntToStr(i)+']');
      for j := 0 to GradeC.ColCount -1 do
      begin
        GravaItem(PegaChaveDeColunaC(j), GradeC.Cells[j,i]);
      end;
    end;
    /////////////////////////////////////////////////////
    for i := 1 to GradeD.RowCount -1 do
    begin
      Progress.Position := Progress.Position +1;
      GravaSetor(CSetBod+IntToStr(i)+']');
      for j := 0 to GradeD.ColCount -1 do
      begin
        GravaItem(PegaChaveDeColunaD(j), GradeD.Cells[j,i]);
      end;
    end;
    /////////////////////////////////////////////////////
    WriteLn(Arq, GravaArquivoCript(FTextoStr, FContaStr, FFatorCript));
    CloseFile(Arq);
    //ChangeData := False;
    /////////////////////////////////////////////////////
    Progress.Visible := False;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    Progress.Visible := False;
    raise;
  end;
end;

function TArquivosSCX.GravaArquivoCript(S: String; ContaStr, FatorCript:
Integer): String;
var
  C3, i: Integer;
  Key: Char;
  b: Byte;
  //
  Fator: String;
begin
  Randomize;
  C3 := 0;
  while C3 in [0, FatorCript] do
    C3 := Random(99999);
  FatorCript := C3;
  Fator := FormatFloat('00000', C3);
  for i := 1 to 5 do
  begin
    b := 0;
    while ((b < 33) or (b > 254)) do
      b := (Round(Random(99)/3.92)*10) + StrToInt(Fator[i]);
    Fator[i] := Char(b);
  end;
  ///////////////////////////////////////////////////////////////////
  Result := S;
  i := 1;
  b := 0;
  try
    while i <= (ContaStr) do
    begin
      if b = 255 then
      begin
        b := 0;
        C3 := FatorCript;
      end else b := b + 1;
      Key := char(Byte(S[i]) xor (C3 shr 8));
      Result[i] := Key;
      C3 := (b + C3) * CO_C1 + CO_C2;
      inc(i);
    end;
    Result := Fator + Result;
  except
    ;
  end;
end;

procedure TArquivosSCX.CalculaGradeC(EdQtdeCh, EdValorCH, EdQtdeTo, EdValorTO,
EdValorDU: TdmkEdit; GradeC, GradeD: TStringGrid; TPCheque: TDateTimePicker);
var
  i, Dias: Integer;
  SomaD, Valor, Total, ValorDU: Double;
  Data: TDateTime;
begin
  DefineContagens(GradeC, GradeD);
  Total := 0;
  SomaD := 0;
  if FmPrincipal.FCheque = 0 then
  begin
    //EdPrazoM.Text := '0';
    EdQtdeCh.Text := '0';
    EdValorCH.Text  := '0,00';
  end else begin
    for i := 1 to GradeC.RowCount -1 do
    begin
      Valor := Geral.DMV(GradeC.Cells[ArqSCX.PegaColunaDeChaveC('R$'), i]);
      Data  := Geral.ValidaDataSimples(GradeC.Cells[ArqSCX.PegaColunaDeChaveC('Vc'), i], True);
      Dias  := Trunc(Int(Data) - Int(TPCheque.Date) + 0.5);
      if Dias <= 0 then Dias := 0;
      Total := Total + Valor;
      SomaD := SomaD + (Dias * Valor);
      GradeC.Cells[ArqSCX.PegaColunaDeChaveC('dd'), i] := IntToStr(Dias);
    end;
    //if Total <> 0 then
      //Dias := Trunc((SomaD / Total) + 0.5) else Dias := 0;
    DefineContagens(GradeC, GradeD);
    EdQtdeCh.Text   := IntToStr(FmPrincipal.FCheque);
    EdValorCH.Text  := Geral.FFT(Total, 2, siPositivo);
    if EdQtdeTo <> nil then
    EdQtdeTo.Text   := IntToStr(FmPrincipal.FCheque+FmPrincipal.FDuplicata);
    if EdValorTO <> nil then
    begin
      if EdValorDU <> nil then ValorDU := Geral.DMV(EdValorDU.Text)
      else ValorDU := 0;
      EdValorTO.Text  := Geral.FFT(Total+ValorDU, 2, siPositivo);
    end;
  end;
end;

procedure TArquivosSCX.CalculaGradeD(EdQtdeDU, EdValorDU, EdQtdeTo, EdValorTO,
EdValorCH: TdmkEdit; GradeC, GradeD: TStringGrid; TPCheque: TDateTimePicker);
var
  i, Dias: Integer;
  SomaD, Valor, Total: Double;
  Data: TDateTime;
begin
  DefineContagens(GradeC, GradeD);
  Total := 0;
  SomaD := 0;
  if FmPrincipal.FDuplicata = 0 then
  begin
    //EdPrazoM.Text := '0';
    EdQtdeDU.Text := '0';
    EdValorDU.Text  := '0,00';
  end else begin
    for i := 1 to GradeD.RowCount -1 do
    begin
      Valor := Geral.DMV(GradeD.Cells[ArqSCX.PegaColunaDeChaveD('R$'), i]);
      Data  := Geral.ValidaDataSimples(GradeD.Cells[ArqSCX.PegaColunaDeChaveD('Vc'), i], True);
      Dias  := Trunc(Int(Data) - Int(TPCheque.Date));
      if Dias <= 0 then Dias := 0;
      Total := Total + Valor;
      SomaD := SomaD + (Dias * Valor);
      //GradeD.Cells[PegaColunaDeChaveD('dd'), i] := IntToStr(Dias);
    end;
    //if Total <> 0 then
      //Dias := Trunc((SomaD / Total) + 0.5) else Dias := 0;
    //EdPrazoM.Text := IntToStr(Dias);
    DefineContagens(GradeC, GradeD);
    EdQtdeDU.Text   := IntToStr(FmPrincipal.FDuplicata);
    EdValorDU.Text  := Geral.FFT(Total, 2, siPositivo);
    EdQtdeTo.Text   := IntToStr(FmPrincipal.FCheque+FmPrincipal.FDuplicata);
    EdValorTO.Text  := Geral.FFT(Total+Geral.DMV(EdValorCH.Text), 2, siPositivo);
  end;
end;

procedure TArquivosSCX.DefineContagens(GradeC, GradeD: TStringGrid);
var
  t: String;
  i: Integer;
begin
  if GradeC <> nil then
  begin
    FmPrincipal.FCheque    := GradeC.RowCount -1;
    if FmPrincipal.FCheque = 1 then
    begin
      t := '';
      for i := 1 to GradeC.ColCount-1 do
        t := t + GradeC.Cells[i, 1];
      if (Trim(t) = '') or (Trim(t) = '0') then FmPrincipal.FCheque := 0;
    end;
  end;
  if GradeD <> nil then
  begin
    FmPrincipal.FDuplicata := GradeD.RowCount -1;
    if FmPrincipal.FDuplicata = 1 then
    begin
      t := '';
      for i := 1 to GradeD.ColCount-1 do t := t + GradeD.Cells[i, 1];
      if Trim(t) = '' then FmPrincipal.FDuplicata := 0;
    end;
  end;
end;

procedure TArquivosSCX.ConfiguraGrades(GradeC, GradeD: TStringGrid;
Pagina: TPageControl);
begin
  if GradeC <> nil then
  begin
    GradeC.ColCount := 11;
    //
    GradeC.Cells[00,00] := 'Seq.';
    GradeC.Cells[01,00] := 'Comp.';
    GradeC.Cells[02,00] := 'Banco';
    GradeC.Cells[03,00] := 'Ag�ncia';
    GradeC.Cells[04,00] := 'Conta';
    GradeC.Cells[05,00] := 'Cheque';
    GradeC.Cells[06,00] := 'Valor';
    GradeC.Cells[07,00] := 'Vencimento';
    GradeC.Cells[08,00] := 'Dias';
    GradeC.Cells[09,00] := 'CGC / CPF';
    GradeC.Cells[10,00] := 'Nome do emitente';
    //
    GradeC.ColWidths[00] := 32;
    GradeC.ColWidths[01] := 36;
    GradeC.ColWidths[02] := 36;
    GradeC.ColWidths[03] := 44;
    GradeC.ColWidths[04] := 72;
    GradeC.ColWidths[05] := 44;
    GradeC.ColWidths[06] := 72;
    GradeC.ColWidths[07] := 68;
    GradeC.ColWidths[08] := 40;
    GradeC.ColWidths[09] := 104;
    GradeC.ColWidths[10] := 256;
  end;
  //
  if GradeD <> nil then
  begin
    GradeD.ColCount := 18;
    //
    GradeD.Cells[00,00] := 'Seq.';
    GradeD.Cells[01,00] := 'Emiss�o';
    GradeD.Cells[02,00] := 'Duplicata';
    GradeD.Cells[03,00] := 'Valor Total';
    GradeD.Cells[04,00] := 'Desc.Vcto';
    GradeD.Cells[05,00] := 'Valor.Vcto';
    GradeD.Cells[06,00] := 'Vencimento';
    GradeD.Cells[07,00] := 'CNPJ';
    GradeD.Cells[08,00] := 'Sacado';
    GradeD.Cells[09,00] := 'Logradouro';
    GradeD.Cells[10,00] := 'N�mero';
    GradeD.Cells[11,00] := 'Complemento';
    GradeD.Cells[12,00] := 'Bairro';
    GradeD.Cells[13,00] := 'Cidade';
    GradeD.Cells[14,00] := 'CEP';
    GradeD.Cells[15,00] := 'UF';
    GradeD.Cells[16,00] := 'Telefone';
    GradeD.Cells[17,00] := 'Inscr. Est.';
    //
    GradeD.ColWidths[00] := 32;
    GradeD.ColWidths[01] := 70;
    GradeD.ColWidths[02] := 64;
    GradeD.ColWidths[03] := 68;
    GradeD.ColWidths[04] := 64;
    GradeD.ColWidths[05] := 72;
    GradeD.ColWidths[06] := 70;
    GradeD.ColWidths[07] := 112;
    GradeD.ColWidths[08] := 160;
    GradeD.ColWidths[09] := 150;
    GradeD.ColWidths[10] := 64;
    GradeD.ColWidths[11] := 80;
    GradeD.ColWidths[12] := 140;
    GradeD.ColWidths[13] := 150;
    GradeD.ColWidths[14] := 64;
    GradeD.ColWidths[15] := 24;
    GradeD.ColWidths[16] := 100;
    GradeD.ColWidths[17] := 104;
    //
  end;
  if Pagina <> nil then Pagina.ActivePageIndex := 0;
end;

end.
