unit UnDmkPing;

interface


uses Windows, SysUtils, WinSock, blcksock;
type
  TEchoReply = packed record
    Addr: In_Addr;
    Status: DWORD;
    RoundTripTime: DWORD;
  end;

  PEchoReply = ^TEchoReply;

    function IcmpCreateFile: THandle; stdcall; external 'iphlpapi.dll';
    function IcmpCloseHandle(icmpHandle: THandle): boolean; stdcall; external 'iphlpapi.dll';
    function IcmpSendEcho(icmpHandle: THandle; DestinationAddress: In_Addr; RequestData: Pointer; RequestSize: Smallint; RequestOptions: Pointer; ReplyBuffer: Pointer; ReplySize: DWORD; Timeout: DWORD): DWORD; stdcall; external 'iphlpapi.dll';
 type
  TUnDmkPing = class(TObject)
  private
    { Private declarations }
    procedure Startup();
    procedure Cleanup();
  public
    { Public declarations }
    function PingHost(const HostName: AnsiString; TimeoutMS: cardinal = 500): Boolean;
    function PortTCPIsOpen(dwPort: Word; ipAddressStr: String): Boolean;
  end;

var
  dmkPing: TUnDmkPing;

var
  WSAData: TWSAData;


implementation

procedure TUnDmkPing.Startup();
begin
  if WSAStartup($0101, WSAData) <> 0 then
    raise Exception.Create('WSAStartup');
end;

procedure TUnDmkPing.Cleanup();
begin
  if WSACleanup <> 0 then
    raise Exception.Create('WSACleanup');
end;

{ TUnDmkPing }

function TUnDmkPing.PingHost(const HostName: AnsiString;
  TimeoutMS: cardinal): Boolean;
const
  rSize = $400;
var
  e: PHostEnt;
  a: PInAddr;
  h: THandle;
  d: string;
  r: array [0 .. rSize - 1] of byte;
  i: cardinal;
begin
  Startup;
  e := gethostbyname(PAnsiChar(HostName));
  if e = nil then
    RaiseLastOSError;
  if e.h_addrtype = AF_INET then
    Pointer(a) := e.h_addr^
  else
    raise Exception.Create('Name doesn''t resolve to an IPv4 address');

  d := FormatDateTime('yyyymmddhhnnsszzz', Now);

  h := IcmpCreateFile;
  if h = INVALID_HANDLE_VALUE then
    RaiseLastOSError;
  try
    i := IcmpSendEcho(h, a^, PChar(d), Length(d), nil, @r[0], rSize, TimeoutMS);
    Result := (i <> 0) and (PEchoReply(@r[0]).Status = 0);
  finally
    IcmpCloseHandle(h);
  end;
  Cleanup;
end;

function TUnDmkPing.PortTCPIsOpen(dwPort: Word; ipAddressStr: String): Boolean;
var
      client : sockaddr_in;//sockaddr_in is used by Windows Sockets to specify a local or remote endpoint address
      sock   : Integer;
    begin
        client.sin_family      := AF_INET;
        client.sin_port        := htons(dwPort);//htons converts a u_short from host to TCP/IP network byte order.
        //client.sin_addr.s_addr := inet_addr(PAnsiChar(ipAddressStr)); //the inet_addr function converts a string containing an IPv4 dotted-decimal address into a proper address for the IN_ADDR structure.
        client.sin_addr.s_addr := inet_addr(PAnsiChar(PAnsiString(ipAddressStr))); //the inet_addr function converts a string containing an IPv4 dotted-decimal address into a proper address for the IN_ADDR structure.
        sock  :=socket(AF_INET, SOCK_STREAM, 0);//The socket function creates a socket
        Result:=connect(sock,client,SizeOf(client))=0;//establishes a connection to a specified socket.
    end;

end.

