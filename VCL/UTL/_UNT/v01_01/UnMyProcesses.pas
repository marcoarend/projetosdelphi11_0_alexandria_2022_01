unit UnMyProcesses;

interface

uses
  Winapi.Windows,
  Vcl.Forms,
  System.SysUtils,
  System.Classes,
  Winapi.PsAPI,
  ShellApi;

type

  TMyProcesses = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ConnectAs(const lpszUsername, lpszPassword: string): Boolean;
    function  ExecutaAppAlvo(const  Handle: Hwnd; const ExecTargetAsAdmin: Boolean;
              const Pasta, Executavel, Parametros: String; const EstadoJanela:
              Integer; var HProcesso, Retorno: Integer): Boolean;
    function  FindProcessesByName(aKey: String; aList: TList = nil): Integer;
    function  FindProcessesByHProcess(hProcess: Integer): Boolean;
    function  KillProcess(aProcessId: Cardinal): Boolean;

  end;

var
  MyProcesses: TMyProcesses;


implementation

{ TMyProcesses }

function TMyProcesses.ConnectAs(const lpszUsername, lpszPassword: string): Boolean;
var
  hToken: THandle;
begin
  Result := LogonUser(PChar(lpszUsername), nil, PChar(lpszPassword), LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, hToken);
  if Result then
    Result := ImpersonateLoggedOnUser(hToken)
  else
    RaiseLastOSError;
end;

function TMyProcesses.ExecutaAppAlvo(const Handle: HWND;
  const ExecTargetAsAdmin: Boolean; const Pasta, Executavel, Parametros: String;
  const EstadoJanela: Integer; var HProcesso, Retorno: Integer): Boolean;
  //
  function RunAsAdmin(const Handle: Hwnd; const Exec, Dir, Params: string; const HWND_CmdShow: Integer): Boolean;
  var
    sei: TShellExecuteInfoA;
    //InstApp: Integer;
  begin
    FillChar(sei, SizeOf(sei), 0);
    sei.cbSize := SizeOf(sei);
    sei.Wnd := Handle;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS;
    //sei.lpVerb := 'open';
    sei.lpVerb := 'runas'; // Run as admin
    //sei.lpFile := 'executavel.exe';
    sei.lpFile := PAnsiChar(AnsiString(Exec));
    //sei.lpDirectory  := 'C:\Program Files (x86)\Dermatek\';
    sei.lpDirectory  := PAnsiChar(AnsiString(Dir));
    //sei.lpParameters := '-h www.???.com.br -u root -p teste123 -c -a "MyApp"'; //
    sei.lpParameters := PAnsiChar(AnsiString(Params));
    sei.nShow := HWND_CmdShow; //SW_SHOWNORMAL;
    Result := ShellExecuteExA(@sei);
    if Result then
    begin
      //InstApp  := sei.hInstApp;
      HProcesso := sei.hProcess;
      Retorno := 0;
    end else
      Retorno := GetLastError;
  end;
var
  FileName, Folder: string;
  //WaitUntilIdle,
  //RunMinimized,
  //WaitUntilTerminated: Boolean;
  //ErrorCode: Integer;
  //OK: Boolean;
  //EstadoJanela: Integer;
  //HWND_CmdShow: Integer;
  //
  //Keys: array of Word;
  //Shift: TShiftState;
  //SpecialKey: Boolean;
  //Retorno: Integer;
//var
  //Path: string;
begin
  //Result := False;
  //FileName  := EdPasta.Text + EdExecutavel.Text;
  FileName  := Pasta + Executavel;
  //Params    := EdParametros.Text; // '-p'; // can be empty
  Folder    := ''; //EdPasta.Text; // if empty function will extract path from FileName
  //ErrorCode := ;
  //
  //WaitUntilTerminated := False;
  //WaitUntilIdle       := True;//True;
  //HWND_CmdShow        := AppPF.EstadoJanelaToHWND_CmdShow(EstadoJanela);

  //if FmPrincipal.FBtAtivado = False then Exit;

  if not ExecTargetAsAdmin then
  begin
    //Funcionando local e Server! *)
    Retorno := ShellExecute(Application.Handle, 'open', PChar(Executavel),
      PChar(Parametros), PChar(Pasta), SW_MAXIMIZE(*HWND_CmdShow*));
    Result := Retorno > 32;
  end else
  begin
    Result :=  RunAsAdmin(Handle, Executavel, Pasta, Parametros, SW_MAXIMIZE);
  end;
end;

function TMyProcesses.FindProcessesByHProcess(hProcess: Integer): Boolean;
var
  hMod: HMODULE;
 cbNeeded: Cardinal;
begin
  Result := False;
  cbNeeded := 0;
   if (hProcess <> 0) then
      Result := (EnumProcessModules(hProcess, @hMod, sizeof(hMod), cbNeeded));
end;

function TMyProcesses.FindProcessesByName(aKey: String; aList: TList): Integer;
var
 szProcessName : Array [0..1024] of Char;
 ProcessName   : String;
 hProcess      : Integer;
 aProcesses    : Array [0..1024] of DWORD;
 cProcesses    : DWORD;
 cbNeeded      : Cardinal;
 i             : UINT;
 hMod          : HMODULE;
begin

 Result := -1;
 aKey:= lowercase(akey);

 if not (EnumProcesses(@aProcesses, sizeof(aProcesses), cbNeeded)) then
    exit;

 // Calculate how many process identifiers were returned.
 cProcesses := cbNeeded div sizeof(DWORD);

 // Print the name and process identifier for each process.
 for i:= 0 to cProcesses - 1 do
 begin
   szProcessName := 'unknown';
   hProcess := OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ,
                           FALSE, aProcesses[i]);

   // Get the process name.
   if (hProcess <> 0) then
      if (EnumProcessModules(hProcess, @hMod, sizeof(hMod),cbNeeded)) then
         GetModuleBaseName (hProcess, hMod, szProcessName, sizeof(szProcessName));

   ProcessName:= lowercase (szProcessName);

   CloseHandle(hProcess);
   if pos (aKey, ProcessName) <> 0 then
   begin
     result:=aProcesses[i];
     if aList <> nil then
        aList.add(Pointer (aProcesses[i]))
     else
       exit;
   end;
 end;

   if aList <> nil then
    Result:= aList.count;
 //else
    //Result:=0;
end;

function TMyProcesses.KillProcess(aProcessId: Cardinal): Boolean;
var
  hProcess : integer;
begin
  hProcess:= OpenProcess(PROCESS_ALL_ACCESS, TRUE, aProcessId);
  Result:= False;
  //
  if (hProcess <>0 ) then
  begin
    Result:= TerminateProcess(hProcess, 0);
    exit;
  end;
end;

end.
