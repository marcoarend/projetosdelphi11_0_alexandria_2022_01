unit UnMinimumMyObjects;

interface

 uses
{$IFNDEF NAO_USA_GLYFS}MyGlyfs, {$ENDIF}
  Vcl.Grids,
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Vcl.ExtDlgs, Menus, UnMsgInt, UnInternalConsts;


type
  TUnMyObjects = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CorIniComponente(Handle: THandle = 0): Boolean;
    function  FIC(FaltaInfoCompo: Boolean; Compo: TComponent;
              Mensagem: String; ExibeMsg: Boolean = True): Boolean;
function FileOpenDialog(Form: TForm; const IniDir, Arquivo, Titulo, Filtro:
  String; Opcoes: TOpenOptions; var Resultado: String): Boolean;
    function  ExcluiLinhaStringGrid(Grade: TStringGrid; Pergunta: Boolean = True): Boolean; // ExcluiLinha
    function  InsereLinhaStringGrid(Grade: TStringGrid; Pergunta: Boolean = True): Boolean; // InsereLinha
    procedure LarguraAutomaticaGrade(AGrid: TStringGrid);  // AjustaLargurasColunas
    procedure LimpaGrade(MinhaGrade: TStringGrid; ColIni, RowIni:
              Integer; ExcluiLinhas: Boolean);
    procedure Informa(Label1: TLabel; Aguarde: Boolean; Texto: String);
    function  Informa2(Label1, Label2: TLabel; Aguarde: Boolean; Texto: String;
              IsThread: Boolean = False): Boolean;
  end;
var
  MyObjects: TUnMyObjects;

implementation

uses dmkGeral, UnMinimumDmkProcFunc;

{ TUnMyObjects }

function TUnMyObjects.CorIniComponente(Handle: THandle): Boolean;
{$IFNDEF NAO_USA_GLYFS}
var
  H, W : Integer;
  Form: TForm;
begin
  H := 0;
  Result := False;
  //Form := nil;
  //
  if Handle <> 0 then
  begin
    Form := TForm(Handle);
    if Form = nil then
      Exit;
  end else
  begin
    Form := Screen.ActiveForm;
    if Form = nil then
      Exit;
    {
      Form.AlphaBlendValue := 0;
      Form.AlphaBlend := True;
    }

    {
      FmMyGlyfs.FForm := Form;
      FmMyGlyfs.Timer1.Enabled := True;
    }
      H := Form.Height;
      W := Form.Width;

    {
      Form.Constraints.MinHeight := 0;
      Form.Constraints.MinWidth  := 0;
      Form.Height := 0;
      Form.Width := 0;
      Application.ProcessMessages;
    }
      if H > 600 then H := 600;
      if W > 800 then W := 800;
      Form.Constraints.MinHeight := H;
      Form.Constraints.MinWidth  := W;
      if Form.WindowState = wsMaximized then
        Form.Top := 0
      else
      begin
        //Form.Height := 0;
      //Form.Width := 0;
    end;
  end;
  // 2012-11-05 - Child form
  if Form <> nil then
  begin
    Form.Font.Name := 'Tahoma';
    if (H > 530) and (Screen.Height <= 600) then Form.Top := 0;
    //
    FmMyGlyfs.DefineGlyfs(Form);
    //
    //DefineCompoCor;
    //
    ControleCor(Form);
    //
    ControleStrings(Form);
    //
    // 2011-09-21
    FmPrincipal.ReCaptionComponentesDeForm(Form);
    //
  {
    Form.AlphaBlendValue := 255;
    Form.AlphaBlend := False;
  }
    //
    EditCB_ValUnico(Form);
  end;
  Result := True;
{$ELSE}
begin
  Result := True;
{$ENDIF}
end;

function TUnMyObjects.ExcluiLinhaStringGrid(Grade: TStringGrid;
  Pergunta: Boolean): Boolean;
var
  Continua: Boolean;
  x, i, j, Line: Integer;
begin
  Result   := False;
  Continua := False;
  //
  if Pergunta = True then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o de toda linha ' +
      Geral.FF0(Grade.Row) + '?') = ID_YES
    then
      Continua := True;
  end else
    Continua := True;
  //
  if Continua = True then
  begin
    x := 1;
    Line := Grade.Row;
    for i := Line to Grade.RowCount -2  do
    begin
      for j := 1 to Grade.ColCount - x do
      begin
        Grade.Cells[j, i] := Grade.Cells[j, i+1];
      end;
    end;
    for i := 1 to Grade.ColCount do Grade.Cells[i, Grade.RowCount-1] := '';
    Line := Grade.RowCount - x;
    if Line < 2 then
    begin
      //for i := 1 to Grade.ColCount do Grade.Cells[i, Grade.RowCount-1] := '';
    end else Grade.RowCount := Line;
    Result := True;
  end;
end;


function TUnMyObjects.FIC(FaltaInfoCompo: Boolean; Compo: TComponent;
  Mensagem: String; ExibeMsg: Boolean): Boolean;
var
  Comp: TWinControl;
begin
  if FaltaInfoCompo then
  begin
    Screen.Cursor := crDefault;
    Result := True;
    if (Mensagem <> '') and ExibeMsg then
      Geral.MB_Aviso(Mensagem);
    if Compo <> nil then
    begin
      Comp := TWinControl(Compo);
      //
      if (Comp.Visible) and (Comp.Enabled) and (Comp.CanFocus) then
      try
        Comp.SetFocus;
      except
        ;
      end;
    end;
  end else Result := False;
end;

function TUnMyObjects.FileOpenDialog(Form: TForm; const IniDir, Arquivo, Titulo,
  Filtro: String; Opcoes: TOpenOptions; var Resultado: String): Boolean;
const
  TxtPadraoDir = 'Sele��o de Pastas.';
var
  VerWin: MyArrayI03;
  OpenDialog: TOpenDialog;
{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}
  FileOpenDialog: TFileOpenDialog;
{$WARN UNIT_PLATFORM ON}
{$WARN SYMBOL_PLATFORM ON}
  Arq, NovoFiltro, Item, Tipo, Extensao: String;
begin
  Arq := Arquivo;
  Result := false;
  Resultado := '';
  VerWin := dmkPF.VersaoWindows;
  if VerWin[1] < 6 then
  begin
    OpenDialog := TOpenDialog.Create(Form);
    try
      OpenDialog.InitialDir := IniDir;
      OpenDialog.Title      := Titulo;//'Abrir e editar arquivo Excel - Empresa';
      OpenDialog.Filter     := Filtro;//'Arquivo EXCEL (*.XLS)|*.xls';
      OpenDialog.FileName   := Arquivo;
      OpenDialog.Options    := OpenDialog.Options + Opcoes;
      //
      if Length(OpenDialog.FileName) = 0 then
        OpenDialog.FileName := TxtPadraoDir;
      //
      if OpenDialog.Execute then
      begin
        Resultado := OpenDialog.FileName;
        Result := True;
      end;
    finally
      OpenDialog.free;
    end;
  end else begin
{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}
    FileOpenDialog := TFileOpenDialog.Create(Form);
    try
      try
        FileOpenDialog.DefaultFolder := IniDir;
        FileOpenDialog.Title      := Titulo;//'Abrir Arquivo de Concilia��o Banc�ria';
        NovoFiltro := Filtro; // Ex.: Filtro := 'Arquivos OFC|*.ofc;Arquivos OFX|*.ofx';
        while NovoFiltro <> '' do
        begin
          if Geral.SeparaPrimeiraOcorrenciaDeTexto(';', NovoFiltro, Item, NovoFiltro) then
          begin
            if Geral.SeparaPrimeiraOcorrenciaDeTexto('|', Item, Tipo, Extensao) then
            begin
              with FileOpenDialog.FileTypes.Add do
              begin
                DisplayName := Tipo;
                FileMask := Extensao;
              end;
            end;
          end;
        end;
        FileOpenDialog.FileName   := Arq;
        //
        if Length(FileOpenDialog.FileName) = 0 then
          FileOpenDialog.FileName := TxtPadraoDir;
        //
        //FileOpenDialog.Options    := FileOpenDialog.Options + Opcoes;
        //
      except
        // nada
      end;
      if FileOpenDialog.Execute then
      begin
        Resultado := FileOpenDialog.FileName;
        Result := True;
      end;
    finally
      FileOpenDialog.free;
    end;
  end;
{$WARN UNIT_PLATFORM ON}
{$WARN SYMBOL_PLATFORM ON}
end;

procedure TUnMyObjects.Informa(Label1: TLabel; Aguarde: Boolean; Texto: String);
begin
  if Label1 <> nil then
  begin
    if Aguarde then
      Label1.Caption := 'Aguarde... ' + Texto + '...'
    else
      Label1.Caption := Texto;
    Label1.Update;
    Application.ProcessMessages;
  end;
end;

function TUnMyObjects.Informa2(Label1, Label2: TLabel; Aguarde: Boolean;
  Texto: String; IsThread: Boolean): Boolean;
begin
  Result := False;
  try
    if Label1 <> nil then
    begin
      if Aguarde then
        Label1.Caption := 'Aguarde... ' + Texto + '...'
      else
        Label1.Caption := Texto;
      if not IsThread then
        Label1.Update;
    end;
    if Label2 <> nil then
    begin
      if Aguarde then
        Label2.Caption := 'Aguarde... ' + Texto + '...'
      else
        Label2.Caption := Texto;
      if not IsThread then
        Label2.Update;
    end;
    if not IsThread then
      Application.ProcessMessages;
    Result := True;
  except
    // nada
  end;
end;

function TUnMyObjects.InsereLinhaStringGrid(Grade: TStringGrid;
  Pergunta: Boolean): Boolean;
var
  Continua: Boolean;
  x, i, j, Line: Integer;
begin
  Result   := False;
  Continua := False;
  //
  if Pergunta = True then
  begin
    if Geral.MB_Pergunta('Confirma a inclus�o de linha ?') = ID_YES
    then
      Continua := True;
  end else
    Continua := True;
  //
  if Continua = True then
  begin
    Grade.RowCount := Grade.RowCount + 1;
    x := 1;
    Line := Grade.Row;
    for i := Grade.RowCount downto Line + 1 do
    begin
      for j := 1 to Grade.ColCount - x do
      begin
        Grade.Cells[j, i] := Grade.Cells[j, i-1];
      end;
    end;
    for i := 1 to Grade.ColCount do
      Grade.Cells[i, Line] := '';
(*
    Line := Grade.RowCount - x;
    if Line < 2 then
    begin
      //for i := 1 to Grade.ColCount do Grade.Cells[i, Grade.RowCount-1] := '';
    end else Grade.RowCount := Line;
*)
    Result := True;
  end;
end;


procedure TUnMyObjects.LarguraAutomaticaGrade(AGrid: TStringGrid);
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  Col, Row, T, L: Integer;
  ColWid: array[0..255] of Integer;
begin
  T := Round(CharWid * 5); // 2012-02-25 de "CharWid * 3" para "CharWid * 5"
  for Col := 1 to 255 do ColWid[Col] := T;
  for Row := 1 to AGrid.RowCount - 1 do
  begin
    for Col := 1 to 255 do
    begin
      L := Round((Length(AGrid.Cells[Col, Row]) + 1) * CharWid);
      if L > ColWid[Col] then
        ColWid[Col] := L;
    end;
  end;
  for Col := 1 to AGrid.ColCount -1 do
    AGrid.ColWidths[Col] := ColWid[Col];
end;

procedure TUnMyObjects.LimpaGrade(MinhaGrade: TStringGrid; ColIni,
  RowIni: Integer; ExcluiLinhas: Boolean);
var
  i, j: Integer;
begin
  for i := ColIni to MinhaGrade.ColCount -1 do
    for j := RowIni to MinhaGrade.RowCount -1 do MinhaGrade.Cells[i,j] := '';
  if ExcluiLinhas then
    MinhaGrade.RowCount := 2;
end;

end.
