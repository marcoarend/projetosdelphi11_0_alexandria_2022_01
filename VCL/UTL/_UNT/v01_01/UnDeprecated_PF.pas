unit UnDeprecated_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, Math, UnMsgInt,
  Db, DbCtrls,   Mask, Buttons, ZCF2, (*DBTables,*) mySQLDbTables, ComCtrls,
  Registry, Grids, DBGrids, CheckLst,
  printers, CommCtrl, TypInfo, comobj, ShlObj, RichEdit, ShellAPI, Consts,
  ActiveX, OleCtrls, SHDocVw, UnDmkProcFunc,
  Variants, MaskUtils, RTLConsts,
  IniFiles,
  {$IFDEF GIFSUPPORT}
  GifImage,
  {$ENDIF GIFSUPPORT}
  Jpeg, axCtrls, //TOleGraphic
  PsAPI, TlHelp32,
  URLMon,
  Wininet,
  Winsock,
  WinSpool,
  WinSvc,
  (*GetMACAdress*)NB30,
  (*Enviar e-mail com anexo*)  Mapi,
  // frxReport
  frxClass,
  frxPreview,
  mySQLExceptions,
  //  CEP - descriptografia
  DCPsha1, DCPTwoFish,
  // ERemotableException
  InvokeRegistry,
  // Dermatek
  dmkGeral, dmkEdit, dmkDBEdit, dmkEditF7, dmkEditDateTimePicker, dmkCheckGroup,
  dmkDBLookupCombobox, dmkEditCB, dmkDBGrid, dmkDBGridDAC, dmkImage, UnDmkEnums;

type

  TUnDeprecated_PF = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function  ConverteArea(Area: Double; Grandeza: TConvGrandeza): Double;
    procedure VerificaAlteracaoMetrica(Grandeza: TConvGrandeza; EditM,
              EditP, AntM2, AntP2: TdmkEdit; Casas: Integer);
    procedure VerificaAlteracaoMetrica_dmk(const Grandeza: TConvGrandeza;
              const EditM, EditP: TdmkEdit; var AntM2, AntP2: Double);
    function  SQLZeroI(Valor: Integer; Msg: String; Compo: TWinControl):
              Boolean;
    function  SQLZeroD(Valor: Double; Msg: String;
              Compo: TWinControl): Boolean;

  end;

var
  Deprecated_PF: TUnDeprecated_PF;

implementation

{ TUnDeprecated_PF }

function TUnDeprecated_PF.ConverteArea(Area: Double;
  Grandeza: TConvGrandeza): Double;
const
  Conv = CO_M2toFT2;
var
  Valor, Sobra, Frac : Double;
begin
  if Grandeza = cgMtoFT then
  begin
    Valor := Area / Conv;
    Sobra := Valor - int(Valor);
    if Sobra < 0.125 then Frac := 0
    else if Sobra < 0.375 then Frac := 0.25
    else if Sobra < 0.625 then Frac := 0.50
    else if Sobra < 0.875 then Frac := 0.75
    else                       Frac := 1;
    Result := int(Valor) + Frac;
  end else begin
    Result := Round(Area * Conv * 100) / 100;
  end;
end;

function TUnDeprecated_PF.SQLZeroD(Valor: Double; Msg: String;
  Compo: TWinControl): Boolean;
begin
  if Valor = 0 then
  begin
    Result := True;
    Geral.MensagemBox(PChar(Msg), 'Erro', MB_OK+MB_ICONERROR);
    if Compo <> nil then TWinControl(Compo).SetFocus;
    Exit;
  end else Result := False;
end;

function TUnDeprecated_PF.SQLZeroI(Valor: Integer; Msg: String;
  Compo: TWinControl): Boolean;
begin
  if Valor = 0 then
  begin
    Result := True;
    Geral.MensagemBox(PChar(Msg), 'Erro', MB_OK+MB_ICONERROR);
    if Compo <> nil then TWinControl(Compo).SetFocus;
    Exit;
  end else Result := False;
end;

procedure TUnDeprecated_PF.VerificaAlteracaoMetrica(Grandeza: TConvGrandeza;
  EditM, EditP, AntM2, AntP2: TdmkEdit; Casas: Integer);
begin
  if Grandeza = cgMtoFT then
  begin
    EditM.Text := Geral.TFT(EditM.Text, casas, siNegativo);
    if AntM2.Text <> EditM.Text then
    begin
      AntM2.Text := EditM.Text;
      AntP2.Text := Geral.FFT(ConverteArea(Geral.DMV(EditM.Text), cgMtoFT), casas, siNegativo);
      EditP.Text := AntP2.Text;
    end;
  end else if Grandeza = cgFTtoM then
  begin
    EditP.Text := Geral.TFT(EditP.Text, casas, siNegativo);
    if AntP2.Text <> EditP.Text then
    begin
      AntP2.Text := EditP.Text;
      AntM2.Text := Geral.FFT(ConverteArea(Geral.DMV(EditP.Text), cgFTtoM), casas, siNegativo);
      EditM.Text := AntM2.Text;
    end;
  end else begin
    Geral.MensagemBox(PChar('Grandeza inv�lida em "Verifica��o de '+
    'altera��o m�trica"'), 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
end;

procedure TUnDeprecated_PF.VerificaAlteracaoMetrica_dmk(
  const Grandeza: TConvGrandeza; const EditM, EditP: TdmkEdit; var AntM2,
  AntP2: Double);
begin
  if Grandeza = cgMtoFT then
  begin
    //EditM.ValueVariant := Geral.TFT(EditM.Text, casas, siNegativo);
    if AntM2 <> EditM.ValueVariant then
    begin
      AntM2 := EditM.ValueVariant;
      AntP2 := ConverteArea(EditM.ValueVariant, cgMtoFT);
      EditP.ValueVariant := AntP2;
    end;
  end else if Grandeza = cgFTtoM then
  begin
    //EditP.Text := Geral.TFT(EditP.Text, casas, siNegativo);
    if AntP2 <> EditP.ValueVariant then
    begin
      AntP2 := EditP.ValueVariant;
      AntM2 := ConverteArea(EditP.ValueVariant, cgFTtoM);
      EditM.ValueVariant := AntM2;
    end;
  end else begin
    Geral.MensagemBox(PChar('Grandeza inv�lida em "Verifica��o de '+
    'altera��o m�trica"'), 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
end;

end.
