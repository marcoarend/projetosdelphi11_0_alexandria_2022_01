unit IO_DLL;


interface

uses  Windows, Classes, Forms, StdCtrls, ComCtrls,SysUtils;

Type

  TIO_DLL = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CheckPort(PortBaseAddress: Word): Boolean;
    function  CheckPortPertoChek(PortBaseAddress: Word): Boolean;
  end;

  EPrinterNotReady = class(Exception)
  private
    PortStatus: Byte;
  public
    constructor Create(PortStatus: Byte);
      //const Source, HelpFile: string; HelpContext: Integer);
    //property HelpFile: string read FHelpFile write FHelpFile;
    //property Source: string read FSource write FSource;
  end;

  //EOleError = class(Exception);
  //EOleSysError = class(EOleError)

  const
    pbTimeout = 1;
    pbNotUsed1 = 2;
    pbNotUsed2 = 4;
    pbIOError  = 8;
    pbSelected = 16;
    pbPaperOut = 32;
    pbNotAck   = 64;
    pbNotBusy  = 128;

    {Port statuses of particular interest: }
    psUnplugged  =             pbNotAck + pbPaperOut + pbSelected + pbIOError + pbNotUsed2 + pbNotUsed1 + pbTimeout;
    psOff        = pbNotBusy +                         pbSelected +             pbNotUsed2 + pbNotUsed1 + pbTimeout;
    psOnLine     = pbNotBusy + pbNotAck +              pbSelected + pbIOError + pbNotUsed2 + pbNotUsed1 + pbTimeout;
    psPaused     =             pbNotAck +              pbSelected +             pbNotUsed2 + pbNotUsed1 + pbTimeout;
    psOnLine2    = pbNotBusy + pbNotAck +              pbSelected + pbIOError                                      ;


procedure PortOut(Port : Word; Data : Byte); stdcall; external 'io.dll';
procedure PortWordOut(Port : Word; Data : Word); stdcall; external 'io.dll';
procedure PortDWordOut(Port : Word; Data : DWord); stdcall; external 'io.dll';
function PortIn(Port : Word) : Byte; stdcall; external 'io.dll';
function PortWordIn(Port : Word) : Word; stdcall; external 'io.dll';
function PortDWordIn(Port : Word) : DWord; stdcall; external 'io.dll';
procedure SetPortBit(Port : Word; Bit : Byte); stdcall; external 'io.dll';
procedure ClrPortBit(Port : Word; Bit : Byte); stdcall; external 'io.dll';
procedure NotPortBit(Port : Word; Bit : Byte); stdcall; external 'io.dll';
function GetPortBit(Port : Word; Bit : Byte) : WordBool; stdcall; external 'io.dll';
function RightPortShift(Port : Word; Val : WordBool) : WordBool; stdcall; external 'io.dll';
function LeftPortShift(Port : Word; Val : WordBool) : WordBool; stdcall; external 'io.dll';
function IsDriverInstalled : Boolean; stdcall; external 'io.dll';

var
  UnIO_DLL: TIO_DLL;

(*Important! To use these functions in your Delphi program, the correct calling convention of stdcall is required. For example:

procedure PortOut(Port : Word; Data : Byte); stdcall; external 'io.dll';*)
implementation

constructor EPrinterNotReady.Create(PortStatus: Byte);
var
  msg: string;
begin
  if PortStatus = 0 then { This is an impossible value for a parallel port
                         second (status) byte. We send this to ourselves only
                         in the case when the OS denied access to the lst file.}
    msg:='The printer is being used by another program'
  else if PortStatus = psOnline then
    msg:='The printer is on and on-line' + sLineBreak + 'It should be giving any problems, but it is.'
  else if (PortStatus = psOff) or (PortStatus = psUnplugged) then
    msg:= 'Help! The printer''''s been stolen!' + sLineBreak + 'The PC can''''t find the printer. Check if it isn''''t off or unplugged.'
  else if (PortStatus = psPaused) then
    msg:= 'The printer isn''''t listening. Please check if it isn''''t off-line or in pause'
  ///////////////////////////////////////////////////////////////////////////////////
  else if PortStatus = 120 then
    msg:='N�o foi poss�vel encontrar a impressora! Verifique se ela est� ligada e conectada.'
  ///////////////////////////////////////////////////////////////////////////////////
  else if (PortStatus and pbPaperOut) <> 0 then
    msg:= 'Printer is out of paper'
  else if not ((PortStatus and pbNotBusy) <> 0) then
    msg:= 'A impressora est� ocupada recebendo dados ou em pausa.'    // This is the one I'm getting
  else
    msg:= 'Um erro desconhecido ocorreu com a impressora! (Status = '
                + IntToStr(PortStatus) + ')';
  inherited Create(msg);
  Self.PortStatus:= PortStatus;
end; { Create }



function TIO_DLL.CheckPort(PortBaseAddress: Word): Boolean;
{ Reads the second byte of the port (port address + 1) to confirm that the
printer is there and ready. If it isn't, it raises an exception of class
EPrinterNotReady }
var
  ByteRead: byte;
  AttemptNumber: byte;
begin
  //ByteRead := IO_DLL.PortIn(PortBaseAddress);
  //ShowMessage('Byte lido: '+IntToStr(ByteRead));
  Result := True;
  ByteRead := IO_DLL.PortIn(PortBaseAddress + 1);
  if ByteRead <> psOnline then
  if ByteRead <> psOnline2 then
  begin
    AttemptNumber:= 1;
    repeat
      Sleep(50); { To give time to the port to finish the last I/O
                  (Value of 50 was determined by experimentation) }
      ByteRead:= PortIn(PortBaseAddress + 1);
      Inc(AttemptNumber);
    until (ByteRead = psOnline) or (ByteRead = psOnline2) or (AttemptNumber > 3);
    if not ((ByteRead = psOnline) or (ByteRead = psOnline))then
    begin
      //result := False;
      raise EPrinterNotReady.Create(ByteRead);
    end;
  end; { if }
end; { CheckPort }

function TIO_DLL.CheckPortPertoChek(PortBaseAddress: Word): Boolean;
{ Reads the second byte of the port (port address + 1) to confirm that the
printer is there and ready. If it isn't, it raises an exception of class
EPrinterNotReady }
var
  ByteRead: byte;
  AttemptNumber: byte;
  Mens: String;
begin
  //ByteRead := IO_DLL.PortIn(PortBaseAddress);
  //ShowMessage('Byte lido: '+IntToStr(ByteRead));
  Mens := 'Erro desconhecido na Pertochek';
  Result := True;
  ByteRead := IO_DLL.PortIn(PortBaseAddress + 1);
  if ByteRead <> 184 then // OK
  begin
    AttemptNumber:= 1;
    repeat
      Sleep(50); { To give time to the port to finish the last I/O
                  (Value of 50 was determined by experimentation) }
      ByteRead:= PortIn(PortBaseAddress + 1);
      Inc(AttemptNumber);
    until AttemptNumber > 3;
    if ByteRead <> 184 then // OK
    begin
      result := False;
      case ByteRead of
        120: Mens := 'N�o foi poss�vel localizar a Pertochek!';
        168: Mens := 'A Pertochek ainda n�o terminou de ser inicializada!';
      end;
      Mens := Mens + sLineBreak+'Erro n� '+IntToStr(ByteRead);
      Application.MessageBox(PChar(Mens), 'Erro', MB_OK+MB_ICONERROR);
    end;
  end;
end;

end.
