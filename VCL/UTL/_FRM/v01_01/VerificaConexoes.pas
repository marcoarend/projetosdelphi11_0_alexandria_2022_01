unit VerificaConexoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, IdAntiFreezeBase, IdAntiFreeze, IdBaseComponent, IdComponent,
  IdRawBase, IdRawClient, IdIcmpClient, UnDMkEnums, UnGrl_Vars, DCPcrypt2,
  DCPblockciphers, DCPtwofish, UMySQLDB,
  Winsock, Vcl.Menus;

type
  TFmVerificaConexoes = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    BtIP: TBitBtn;
    BtPorta: TBitBtn;
    ImgIP: TImage;
    ImgPorta: TImage;
    LaIP: TLabel;
    LaPorta: TLabel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    LaMeuIP: TLabel;
    LaServidor: TLabel;
    LaVersao: TLabel;
    MyDB: TmySQLDatabase;
    GroupBox3: TGroupBox;
    BtSalvar: TBitBtn;
    BtDB: TBitBtn;
    LaDB: TLabel;
    ImgDB: TImage;
    TabSheet3: TTabSheet;
    MePorta: TMemo;
    IdAntiFreeze1: TIdAntiFreeze;
    Label5: TLabel;
    Label6: TLabel;
    LaComputador: TLabel;
    DCP_twofish1: TDCP_twofish;
    RGDadosConexao: TRadioGroup;
    QrUpd: TMySQLQuery;
    Panel5: TPanel;
    LaRegPorta: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Panel6: TPanel;
    EdRegPortaApp: TdmkEdit;
    EdDatabaseApp: TdmkEdit;
    EdOthUserApp: TdmkEdit;
    EdOthSetConApp: TdmkEdit;
    LaRegIPServerAll: TLabel;
    EdRegIPServerApp: TdmkEdit;
    RGRegServerApp: TRadioGroup;
    AdvToolBar8: TPanel;
    BtBackupApp: TBitBtn;
    BtVerifiBDApp: TBitBtn;
    Panel24: TPanel;
    Panel7: TPanel;
    EdRegPortaCEP: TdmkEdit;
    EdDatabaseCEP: TdmkEdit;
    EdOthUserCEP: TdmkEdit;
    EdOthSetConCEP: TdmkEdit;
    EdRegIPServerCEP: TdmkEdit;
    RGRegServerCEP: TRadioGroup;
    Label9: TLabel;
    Label10: TLabel;
    BtAuto: TBitBtn;
    PMVerificaBD: TPopupMenu;
    VerificaBDServidor1: TMenuItem;
    VerificaTabelasPublicas1: TMenuItem;
    SbDatabaseApp: TSpeedButton;
    TabSheet4: TTabSheet;
    Memo1: TMemo;
    PMDatabaseApp: TPopupMenu;
    Verificarseexiste1: TMenuItem;
    Listarexistentes1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIPClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure RGRegServerAppClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BtDBClick(Sender: TObject);
    procedure BtPortaClick(Sender: TObject);
    procedure EdDatabaseAppEnter(Sender: TObject);
    procedure EdDatabaseAppExit(Sender: TObject);
    procedure EdOthUserAppEnter(Sender: TObject);
    procedure EdOthUserAppExit(Sender: TObject);
    procedure EdOthSetConAppEnter(Sender: TObject);
    procedure EdOthSetConAppExit(Sender: TObject);
    procedure BtAutoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtBackupAppClick(Sender: TObject);
    procedure BtVerifiBDAppClick(Sender: TObject);
    procedure RGDadosConexaoClick(Sender: TObject);
    procedure VerificaTabelasPublicas1Click(Sender: TObject);
    procedure VerificaBDServidor1Click(Sender: TObject);
    procedure SbDatabaseAppClick(Sender: TObject);
    procedure Verificarseexiste1Click(Sender: TObject);
    procedure PMDatabaseAppPopup(Sender: TObject);
    procedure Listarexistentes1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ConfiguraInfSuporte;
    procedure ConfiguraRegistroApp();
    procedure ConfiguraRegistroCEP();
    procedure DefineBitMap(Index: Integer; Image: TImage);
    function  VerificaDModG(): Boolean;
    function  VerificaSeDBExiste(Database: String; Informa: Boolean): Boolean;
  public
    { Public declarations }
    FForcaConfiguracao: Boolean;
  end;

  var
  FmVerificaConexoes: TFmVerificaConexoes;

implementation

uses UnMyObjects, MyListas,
{$IfNDef NAO_USA_DB_GERAL}
ModuleGeral,
{$EndIf}
{$IfNDef sVerifyDB}
VerifiDB,
VerifiDBTerceiros,
{$EndIf}
(*Module,*) (*MyGlyfs,*) (*MyListas,*)
{$IfNDef NAO_USA_MyDBCheck}
MyDBCheck, (*DmkDAC_PF,*) //> OverSeerRST

{$EndIf}
  DmkDAC_PF, UnDmkProcFunc, MyGlyfs;

{$R *.DFM}

procedure TFmVerificaConexoes.BtBackupAppClick(Sender: TObject);
begin
{$IfNDef NAO_USA_MyDBCheck}
  DModG.MostraBackup3();
{$EndIf}
end;

procedure TFmVerificaConexoes.BtVerifiBDAppClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMVerificaBD, BtVerifiBDApp);
end;

procedure TFmVerificaConexoes.BtAutoClick(Sender: TObject);
var
  Str: String;
begin
  Str := 'Local';
  if InputQuery('Preenchimento Autom�tico', 'Informe o ID do servidor',
  Str) then
  begin
    if Lowercase(Str) = Lowercase('Local') then
    begin
      EdRegPortaApp.ValueVariant     := '3306';
      RGRegServerApp.ItemIndex       := 1;
      EdRegIPServerApp.ValueVariant  := 'localhost';
      EdDataBaseApp.ValueVariant     := '';
      EdOthUserApp.ValueVariant      := 'root';
      EdOthSetConApp.ValueVariant    := CO_USERSPNOW;
    end else
    if Lowercase(Str) = Lowercase('dmkNayr') then
    begin
      EdRegPortaApp.ValueVariant     := '3306';
      RGRegServerApp.ItemIndex       := 0;
      EdRegIPServerApp.ValueVariant  := 'bdhost0118.servidorwebfacil.com';
      EdDataBaseApp.ValueVariant     := 'dermatek_nayr';
      EdOthUserApp.ValueVariant      := 'dermatek_nayr';
      EdOthSetConApp.ValueVariant    := ';+95l?-#dD6R';
    end else
    if Lowercase(Str) = Lowercase('N4ir') then
    begin
      EdRegPortaApp.ValueVariant     := '3306';
      RGRegServerApp.ItemIndex       := 0;
      EdRegIPServerApp.ValueVariant  := '52.67.64.57';
      EdDataBaseApp.ValueVariant     := '';
      EdOthUserApp.ValueVariant      := 'nayr';
      EdOthSetConApp.ValueVariant    := 'Nayr2019#';
    end;
  end;
end;

procedure TFmVerificaConexoes.BtDBClick(Sender: TObject);
  procedure ConfigAtual();
  begin
    MyDB.Host         := VAR_IP;
    MyDB.Port         := VAR_PORTA;
    MyDB.DatabaseName := TMeuDB;
    MyDB.UserName     := VAR_SQLUSER;
    MyDB.UserPassword := VAR_BDSENHA;
  end;
var
  Database: String;
begin
  if MyDB.Connected then
    MyDB.Disconnect;
  //
  case RGDadosConexao.ItemIndex of
    //0: Geral.MB_Aviso('Selecione um item "Dados para conex�o" v�lido!"');
    1: ConfigAtual();
    2:
    begin
      ConfigAtual();
      //
      if EdRegPortaApp.ValueVariant <> 0 then
        MyDB.Port := EdRegPortaApp.ValueVariant;
      //
      if EdDatabaseApp.ValueVariant <> EmptyStr  then
        MyDB.DatabaseName := EdDatabaseApp.ValueVariant;
      //
      if EdOthUserApp.ValueVariant <> EmptyStr  then
        MyDB.UserName := EdOthUserApp.ValueVariant;
      //
      if EdOthSetConApp.ValueVariant <> EmptyStr  then
        MyDB.UserPassword := EdOthSetConApp.ValueVariant;
      //
      if EdRegIPServerApp.ValueVariant <> EmptyStr  then
        MyDB.Host := EdRegIPServerApp.ValueVariant;
      //
    end;
    3:
    begin
      ConfigAtual();
      //
      if EdRegPortaCEP.ValueVariant <> 0 then
        MyDB.Port := EdRegPortaCEP.ValueVariant;
      //
      if EdDatabaseCEP.ValueVariant <> EmptyStr  then
        MyDB.DatabaseName := EdDatabaseCEP.ValueVariant;
      //
      if EdOthUserCEP.ValueVariant <> EmptyStr  then
        MyDB.UserName := EdOthUserCEP.ValueVariant;
      //
      if EdOthSetConCEP.ValueVariant <> EmptyStr  then
        MyDB.UserPassword := EdOthSetConCEP.ValueVariant;
      //
      if EdRegIPServerCEP.ValueVariant <> EmptyStr  then
        MyDB.Host := EdRegIPServerCEP.ValueVariant;
      //
    end else
    begin
      Geral.MB_Aviso('Selecione um item "Dados para conex�o" v�lido!"');
      Exit;
    end;
  end;

  try
    Screen.Cursor := crHourGlass;
    BtDB.Enabled  := False;
    //
    //MyDB.Connect;
    USQLDB.ConectaDB(MyDB, 'VerificaConexoes.163');
    //
    Screen.Cursor := crDefault;
    BtDB.Enabled  := True;
  except
    Screen.Cursor := crDefault;
    BtDB.Enabled  := True;
  end;
  LaDB.Visible  := True;
  ImgDB.Visible := True;
  //
  if MyDB.Connected then
  begin
    LaDB.Caption := 'Conex�o com o banco de dados estabelecida com �xito!';
    //FmMyGlyfs.Lista_32x64A.GetBitmap(14, ImgDB.Picture.Bitmap);
    DefineBitMap(14, ImgDB);
  end else
  begin
    //ConfigAtual();
    MyDB.DatabaseName := 'mysql';
    try
      USQLDB.ConectaDB(MyDB, 'VerificaConexoes.290');
    except
      // nada
    end;
    if MyDB.Connected then
    begin
      case RGDadosConexao.ItemIndex of
        1: Database := TMeuDB;
        2: Database := EdDatabaseApp.Text;
        3: Database := EdDatabaseCEP.Text;
        else
        begin
          Geral.MB_Aviso('Nenhum database foi definido! Defina o item "Dados para conex�o"');
        end;
      end;
      if VerificaSeDBExiste(Database, False) then
      begin
        LaDB.Caption := 'Falha ao estabelecer uma conex�o com o banco de dados!';
        //FmMyGlyfs.Lista_32x64A.GetBitmap(10, ImgDB.Picture.Bitmap) ;
        DefineBitMap(10, ImgDB);
      end;
    end else
    begin
      LaDB.Caption := 'Falha ao estabelecer uma conex�o com o banco de dados!';
      //FmMyGlyfs.Lista_32x64A.GetBitmap(10, ImgDB.Picture.Bitmap) ;
      DefineBitMap(10, ImgDB);
    end;
  end;
end;

procedure TFmVerificaConexoes.BtIPClick(Sender: TObject);
var
  IP: String;
  Conectou: Boolean;
  //
  procedure ConfigAtual();
  begin
    IP         := VAR_IP;
  end;
begin
  case RGDadosConexao.ItemIndex of
    //0: Geral.MB_Aviso('Selecione um item "Dados para conex�o" v�lido!"');
    1: ConfigAtual();
    2:
    begin
      ConfigAtual();
      //
      if EdRegIPServerApp.ValueVariant <> EmptyStr  then
        IP := EdRegIPServerApp.ValueVariant;
      //
    end;
    3:
    begin
      ConfigAtual();
      //
      if EdRegIPServerCEP.ValueVariant <> EmptyStr  then
        IP := EdRegIPServerCEP.ValueVariant;
      //
    end else
    begin
      Geral.MB_Aviso('Selecione um item "Dados para conex�o" v�lido!"');
      Exit;
    end;
  end;

  if IP = '' then
    IP := Geral.ObtemIP(1);
  try
    Screen.Cursor := crHourGlass;
    //
    BtIP.Enabled := False;
    //
    Conectou := MyObjects.Ping(IP);
    //
    if Conectou then
    begin
      LaIP.Caption := 'Conex�o com o IP ' + IP + ' estabelecida com �xito!';
      //FmMyGlyfs.Lista_32x64A.GetBitmap(14, ImgIP.Picture.Bitmap) ;
      DefineBitMap(14, ImgIP);
     end else
    begin
      LaIP.Caption := 'Falha ao estabelecer uma conex�o com o IP ' + IP + '!';
      //FmMyGlyfs.Lista_32x64A.GetBitmap(10, ImgIP.Picture.Bitmap) ;
      DefineBitMap(10, ImgIP);
    end;
    LaIP.Visible  := True;
    ImgIP.Visible := True;
  finally
    Screen.Cursor := crDefault;
    //
    BtIP.Enabled := True;
  end;
end;

procedure TFmVerificaConexoes.BtPortaClick(Sender: TObject);
  function PortTCP_IsOpen(dwPort : Word; ipAddressStr:AnsiString) : boolean;
  var
    client : sockaddr_in;
    sock   : Integer;

    ret    : Integer;
    wsdata : WSAData;
  begin
   //Result:=False;
   ret := WSAStartup($0002, wsdata); //initiates use of the Winsock DLL
    if ret<>0 then exit;
    try
      client.sin_family      := AF_INET;  //Set the protocol to use , in this case (IPv4)
      client.sin_port        := htons(dwPort); //convert to TCP/IP network byte order (big-endian)
      client.sin_addr.s_addr := inet_addr(PAnsiChar(ipAddressStr));  //convert to IN_ADDR  structure
      sock  :=socket(AF_INET, SOCK_STREAM, 0);    //creates a socket
      Result:=connect(sock,client,SizeOf(client))=0;  //establishes a connection to a specified socket
    finally
    WSACleanup;
    end;
  end;
var
  Porta: Integer;
  IP, Res: String;
  //
  procedure ConfigAtual();
  begin
    IP    := VAR_IP;
    Porta := VAR_PORTA;
  end;
begin
  LaPorta.Visible  := True;
  ImgPorta.Visible := True;
  MePorta.Text     := '';
  //
  case RGDadosConexao.ItemIndex of
    //0: Geral.MB_Aviso('Selecione um item "Dados para conex�o" v�lido!"');
    1: ConfigAtual();
    2:
    begin
      ConfigAtual();
      //
      if EdRegPortaApp.ValueVariant <> 0 then
        Porta := EdRegPortaApp.ValueVariant;
        //
      if EdRegIPServerApp.ValueVariant <> EmptyStr  then
        IP := EdRegIPServerApp.ValueVariant;
      //
    end;
    3:
    begin
      ConfigAtual();
      //
      if EdRegPortaCEP.ValueVariant <> 0 then
        Porta := EdRegPortaCEP.ValueVariant;
        //
      if EdRegIPServerCEP.ValueVariant <> EmptyStr  then
        IP := EdRegIPServerCEP.ValueVariant;
      //
    end else
    begin
      Geral.MB_Aviso('Selecione um item "Dados para conex�o" v�lido!"');
      Exit;
    end;
  end;
  //
  if (IP ='127.0.0.1') or (IP = 'localhost') then
  // N�o precisa testar
  begin
    LaPorta.Caption := 'Conex�o com a Porta ' + Geral.FF0(Porta) + ' no IP ' + IP + ' estabelecida com �xito!';
    //FmMyGlyfs.Lista_32x64A.GetBitmap(14, ImgPorta.Picture.Bitmap) ;
    DefineBitMap(14, ImgPorta);
  end else
  begin
    if PortTCP_IsOpen(Porta, IP) then
    //if dmkPF.TestaPortaPorHost(Porta, IP) then
    begin
      LaPorta.Caption := 'Conex�o com a Porta ' + Geral.FF0(Porta) + ' no IP ' + IP + ' estabelecida com �xito!';
      //FmMyGlyfs.Lista_32x64A.GetBitmap(14, ImgPorta.Picture.Bitmap) ;
      DefineBitMap(14, ImgPorta);
    end else
    begin
      LaPorta.Caption := 'Falha ao estabelecer uma conex�o com a porta ' + Geral.FF0(Porta) + ' no IP ' + IP + '!';
      //FmMyGlyfs.Lista_32x64A.GetBitmap(10, ImgPorta.Picture.Bitmap) ;
      DefineBitMap(10, ImgPorta);
    end;
  end;
end;

procedure TFmVerificaConexoes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVerificaConexoes.BtSalvarClick(Sender: TObject);
var
  Porta, Server: Integer;
  IPServer, DataBase, OthUser, OthSetCon: String;
  PortaCEP, ServerCEP: Integer;
  IPServerCEP, DataBaseCEP, OthUserCEP, OthSetConCEP: String;
begin
  if MyObjects.FIC(RGRegServerApp.ItemIndex < 1, RGRegServerApp,
  'Informe o Tipo de computadoe para o Banco de dados PRINCIPAL') then
    Exit;
  if MyObjects.FIC(RGRegServerCEP.ItemIndex < 1, RGRegServerCEP,
  'Informe o Tipo de computadoe para o Banco de dados CEP') then
    Exit;
  Porta     := EdRegPortaApp.ValueVariant;
  //Server    := RGRegServerApp.ItemIndex + 1;
  Server    := RGRegServerApp.ItemIndex;
  IPServer  := EdRegIPServerApp.ValueVariant;
  DataBase  := EdDataBaseApp.ValueVariant;
  OthUser   := EdOthUserApp.ValueVariant;
  OthSetCon := EdOthSetConApp.ValueVariant;
  OthSetCon := dmkPF.Criptografia(OthSetCon, CO_LLM_SecureStr);
  //
  if MyObjects.FIC(Porta = 0, EdRegPortaApp, 'Porta do banco de dados PRINCIPAL n�o definida!') then Exit;
  if MyObjects.FIC(Server < 0, RGRegServerApp, 'Tipo de computador do banco de dados PRINCIPAL n�o definido!') then Exit;
  //
  PortaCEP     := EdRegPortaCEP.ValueVariant;
  //ServerCEP    := RGRegServerCEP.ItemIndex + 1;
  ServerCEP    := RGRegServerCEP.ItemIndex;
  IPServerCEP  := EdRegIPServerCEP.ValueVariant;
  DataBaseCEP  := EdDataBaseCEP.ValueVariant;
  OthUserCEP   := EdOthUserCEP.ValueVariant;
  OthSetConCEP := EdOthSetConCEP.ValueVariant;
  OthSetConCEP := dmkPF.Criptografia(OthSetConCEP, CO_LLM_SecureStr);
  //
  if MyObjects.FIC(Porta = 0, EdRegPortaCEP, 'Porta do banco de dados CEP n�o definida!') then Exit;
  if MyObjects.FIC(Server < 0, RGRegServerCEP, 'Tipo de computador do banco de dados CEP n�o definido!') then Exit;
  //
  if Server = 1 then
  begin
    if MyObjects.FIC(IPServer = '', EdRegIPServerApp, 'IP do servidor PRINCIPAL n�o definido!') then Exit;
  end else
    IPServer := '127.0.0.1';

  if ServerCEP = 1 then
  begin
    if MyObjects.FIC(IPServerCEP = '', EdRegIPServerCEP, 'IP do servidor de CEP n�o definido!') then Exit;
  end else
    IPServerCEP := '127.0.0.1';

  try
    Geral.WriteAppKeyCU('Porta', Application.Title, Porta, ktInteger);
    Geral.WriteAppKeyCU('Server', Application.Title, Server, ktInteger);
    Geral.WriteAppKeyCU('IPServer', Application.Title, IPServer, ktString);
    Geral.WriteAppKeyCU('Database', Application.Title, DataBase, ktString);
    Geral.WriteAppKeyCU('OthUser', Application.Title, OthUser, ktString);
    Geral.WriteAppKeyCU('OthSetCon', Application.Title, OthSetCon, ktString);
    //
    Geral.WriteAppKeyCU('PortaCEP', Application.Title, PortaCEP, ktInteger);
    Geral.WriteAppKeyCU('ServerCEP', Application.Title, ServerCEP, ktInteger);
    Geral.WriteAppKeyCU('IPServerCEP', Application.Title, IPServerCEP, ktString);
    Geral.WriteAppKeyCU('DatabaseCEP', Application.Title, DataBaseCEP, ktString);
    Geral.WriteAppKeyCU('OthUserCEP', Application.Title, OthUserCEP, ktString);
    Geral.WriteAppKeyCU('OthSetConCEP', Application.Title, OthSetConCEP, ktString);
    //
    try
      //Erro LM Administrador - Compatibilidade com vers�es antigas! remover no futuro!
      Geral.WriteAppKeyLM2('Porta', 'Dermatek', Porta, ktInteger);
      Geral.WriteAppKeyLM2('Server', Application.Title, Server, ktInteger);
      Geral.WriteAppKeyLM2('IPServer', Application.Title, IPServer, ktString);
    except
      Geral.MB_Aviso('Falha ao salvar dados no CURRENT USER!' + sLineBreak +
        'OBSERVA��O: No LOCAL MACHINE os dados foram salvos com sucesso!');
    end;
  except
    Geral.MB_Aviso('Falha ao salvar dados!' + sLineBreak +
      'Certifique-se que voc� est� logado no sistema operacional com um usu�rio do tipo administrador!');
  end;
  VAR_TERMINATE := True;
  VAR_APLICATION_TERMINATE := True;
  Application.Terminate;
end;

procedure TFmVerificaConexoes.ConfiguraInfSuporte;
var
  MeuIP, Servidor, Versao: String;
begin
  MeuIP  := Geral.ObtemIP(1);
  Versao := Geral.VersaoAplicTxt(CO_VERSAO);
  //
  if VAR_SERVIDOR > 1 then
    Servidor := 'SIM'
  else
    Servidor := 'N�O';
  //
  LaMeuIP.Caption      := MeuIP;
  LaServidor.Caption   := Servidor;
  LaVersao.Caption     := Versao;
  LaComputador.Caption := Geral.IndyComputerName();
end;

procedure TFmVerificaConexoes.ConfiguraRegistroApp();
var
  Porta, Server: Integer;
  IPServ, DataBase, OthUser, OthSetCon: String;
begin
  Porta := Geral.ReadAppKeyCU('Porta', Application.Title, ktInteger, -1);
  //
  if Porta = -1 then
    Porta := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  //
  Server := Geral.ReadAppKeyCU('Server', Application.Title, ktInteger, 0);
  //
{
  case Server of
    1:   Server := 0;
    2:   Server := 1;
    3:   Server := 2;
    else Server := -1;
  end;
}
  //
  IPServ    := Geral.ReadAppKeyCU('IPServer', Application.Title, ktString, CO_VAZIO);
  DataBase  := Geral.ReadAppKeyCU('Database', Application.Title, ktString, '');
  OthUser   := Geral.ReadAppKeyCU('OthUser', Application.Title, ktString, '');
  OthSetCon := Geral.ReadAppKeyCU('OthSetCon', Application.Title, ktString, '');
  OthSetCon := dmkPF.Criptografia(OthSetCon, CO_LLM_SecureStr);
  //
  EdRegIPServerApp.Visible := Server < 1;
  //
  EdRegPortaApp.ValueVariant    := Porta;
  EdDatabaseApp.ValueVariant    := DataBase;
  RGRegServerApp.ItemIndex      := Server;
  EdRegIPServerApp.ValueVariant := IPServ;
  EdOthUserApp.ValueVariant     := OthUser;
  EdOthSetConApp.ValueVariant   := OthSetCon;
end;

procedure TFmVerificaConexoes.ConfiguraRegistroCEP();
var
  PortaCEP, ServerCEP: Integer;
  IPServCEP, DataBaseCEP, OthUserCEP, OthSetConCEP: String;
begin
  PortaCEP := Geral.ReadAppKeyCU('PortaCEP', Application.Title, ktInteger, 3306);
  ServerCEP := Geral.ReadAppKeyCU('ServerCEP', Application.Title, ktInteger, 0);
(*
  //
  case ServerCEP of
    1:   ServerCEP := 0;
    2:   ServerCEP := 1;
    3:   ServerCEP := 2;
    else ServerCEP := -1;
  end;
*)
  IPServCEP    := Geral.ReadAppKeyCU('IPServerCEP', Application.Title, ktString, CO_VAZIO);
  DataBaseCEP  := Geral.ReadAppKeyCU('DatabaseCEP', Application.Title, ktString, '');
  OthUserCEP   := Geral.ReadAppKeyCU('OthUserCEP', Application.Title, ktString, '');
  OthSetConCEP := Geral.ReadAppKeyCU('OthSetConCEP', Application.Title, ktString, '');
  OthSetConCEP := dmkPF.Criptografia(OthSetConCEP, CO_LLM_SecureStr);
  //
  EdRegIPServerCEP.Visible := ServerCEP < 1;
  //
  EdRegPortaCEP.ValueVariant    := PortaCEP;
  EdDatabaseCEP.ValueVariant    := DataBaseCEP;
  RGRegServerCEP.ItemIndex      := ServerCEP;
  EdRegIPServerCEP.ValueVariant := IPServCEP;
  EdOthUserCEP.ValueVariant     := OthUserCEP;
  EdOthSetConCEP.ValueVariant   := OthSetConCEP;
end;

procedure TFmVerificaConexoes.DefineBitMap(Index: Integer; Image: TImage);
begin
  while Image.Picture.Bitmap.Height <> 0 do
  begin
    Image.Picture.Assign(nil);
    Application.ProcessMessages;
  end;
  while Image.Picture.Bitmap.Height = 0 do
  begin
    FmMyGlyfs.Lista_32x64A.GetBitmap(Index, Image.Picture.Bitmap);
    Application.ProcessMessages;
  end;
end;

procedure TFmVerificaConexoes.EdOthSetConAppEnter(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Para usar o padr�o deixe o campo "Senha alternativa para o usu�rio acima" em branco');
end;

procedure TFmVerificaConexoes.EdOthSetConAppExit(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVerificaConexoes.EdDatabaseAppEnter(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Para usar o padr�o deixe o campo "Nome alternativo para o banco de dados" em branco');
end;

procedure TFmVerificaConexoes.EdDatabaseAppExit(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVerificaConexoes.EdOthUserAppEnter(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Para usar o padr�o deixe o campo "Nome alternativo para o usu�rio" em branco');
end;

procedure TFmVerificaConexoes.EdOthUserAppExit(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVerificaConexoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVerificaConexoes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FForcaConfiguracao := True;
  //
  LaIP.Visible     := False;
  ImgIP.Visible    := False;
  LaPorta.Visible  := False;
  ImgPorta.Visible := False;
  LaDB.Visible     := False;
  ImgDB.Visible    := False;
  //
  PageControl1.ActivePageIndex := 0;
  TabSheet3.TabVisible         := False;
  ConfiguraInfSuporte;
end;

procedure TFmVerificaConexoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVerificaConexoes.FormShow(Sender: TObject);
begin
  if FForcaConfiguracao then
  begin
    FForcaConfiguracao := False;
    ConfiguraRegistroApp();
    ConfiguraRegistroCEP();
    PageControl1.ActivePageIndex := 1;
  end;
end;

procedure TFmVerificaConexoes.Listarexistentes1Click(Sender: TObject);
var
  DBName, DBMain: String;
  BDs: Array of String;
  Item, P, N, Colunas: Integer;
begin
  try
    N := 0;
    DBName := MyDB.DatabaseName;
    //
    DBMain := TMeuDB;
    P := Pos('_', DBMain);
    if P > 0 then
      DBMain := Copy(DBMain, 1, P-1);
    //
    MyDB.Disconnect;
    MyDB.DatabaseName := 'mysql';
    MyDB.Connect;
    UnDmkDAC_PF.AbreMySQLQuery0(QrUpd, MyDB, [
    'SHOW DATABASES LIKE "' + DBMain + '%"',
    '']);
    if QrUpd.RecordCount > 0 then
    begin
      QrUpd.First;
      while not QrUpd.Eof do
      begin
        if Pos('_loc_', QrUpd.Fields[0].AsString) = 0 then
        begin
          N := N + 1;
          SetLength(BDs, N);
          BDs[N - 1] := QrUpd.Fields[0].AsString;
        end;
        //
        QrUpd.Next;
      end;
      if N > 0 then
      begin
        Colunas := (N div 7) + 1;
        Item := MyObjects.SelRadioGroup('Sele��o de Banco de dados',
          'Bancos de dados dispon�veis', BDs, Colunas, -1, True);
        if Item > -1 then
          EdDatabaseApp.Text := BDs[Item];
      end;
    end;
  finally
    MyDB.Disconnect;
    MyDB.DatabaseName := DBName;
  end;
end;

procedure TFmVerificaConexoes.PageControl1Change(Sender: TObject);
begin
{
  if PageControl1.ActivePageIndex = 0 then
    ConfiguraInfSuporte
  else
    ConfiguraRegistro;
}
end;

procedure TFmVerificaConexoes.PMDatabaseAppPopup(Sender: TObject);
begin
  Verificarseexiste1.Caption := '&Verificar se "' + EdDatabaseApp.Text + '" existe';
end;

procedure TFmVerificaConexoes.RGDadosConexaoClick(Sender: TObject);
begin
  if RGDadosConexao.ItemIndex = 1 then
  begin
    ConfiguraRegistroApp();
    ConfiguraRegistroCEP();
  end;
end;

procedure TFmVerificaConexoes.RGRegServerAppClick(Sender: TObject);
begin
  EdRegIPServerApp.Visible := RGRegServerApp.ItemIndex <> 2;
  EdRegIPServerCEP.Visible := RGRegServerCEP.ItemIndex <> 2;
end;

procedure TFmVerificaConexoes.SbDatabaseAppClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMDatabaseApp, SbDatabaseApp);
end;

procedure TFmVerificaConexoes.VerificaBDServidor1Click(Sender: TObject);
begin
{$IfNDef sVerifyDB}
  if VerificaDModG() then
  begin
    if DBCheck.CriaFm(TFmVerifiDB, FmVerifiDB, afmoNegarComAviso) then
    begin
      FmVerifiDB.ShowModal;
      FmVerifiDB.Destroy;
    end;
  end;
{$EndIf}
end;

function TFmVerificaConexoes.VerificaDModG(): Boolean;
var
  DataBAseName: String;
begin
  Result := False;
{$IfNDef NAO_USA_MyDBCheck}
  MyDB.Disconnect;
  MyDB.Host         := VAR_IP;
  MyDB.Port         := VAR_PORTA;
  MyDB.DatabaseName := '';
  MyDB.UserName     := VAR_SQLUSER;
  MyDB.UserPassword := VAR_BDSENHA;
  USQLDB.ConectaDB(MyDB, 'TFmVerificaConexoes.AGMBVerifiBDClick.149');
  //
  if EdDatabaseApp.ValueVariant <> EmptyStr  then
    DataBaseName := EdDatabaseApp.ValueVariant
  else
    DataBaseName := TMeuDB;
  //
  QrUpd.Close;
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('SHOW DATABASES LIKE "' + DataBaseName + '"');
  UnDmkDAC_PF.AbreQuery(QrUpd, MyDB);
  if QrUpd.Fields[0].AsString = EmptyStr then
  begin
    if Geral.MB_Pergunta('O banco de dados ' + DataBAseName +
    ' n�o existe e deve ser criado. Confirma a cria��o?') = ID_YES then
    begin
      MyDB.Execute('CREATE DATABASE ' + DataBaseName);
      Geral.MB_Aviso('O aplicativo ser� encerrado!');
      Application.Terminate;
      Exit;
    end;
  end;
  if DModG <> nil then
  begin
    Result := True;
  end else
{$EndIf}
    Geral.MB_Erro('Module Geral ainda n�o foi criado, ou j� foi exclu�do!');
end;

procedure TFmVerificaConexoes.Verificarseexiste1Click(Sender: TObject);
const
  Informa = True;
begin
  VerificaSeDBExiste(EdDatabaseApp.ValueVariant, Informa);
end;

function TFmVerificaConexoes.VerificaSeDBExiste(Database: String; Informa:
Boolean): Boolean;
const
  MsgExito = 'Conex�o com o banco de dados estabelecida com �xito!';
var
  DBName: String;
  DBFoiCriado: Boolean;
begin
  DBFoiCriado := False;
  DBName := MyDB.DatabaseName;
  MyDB.DatabaseName := 'mysql';
  MyDB.Connect;
  UnDmkDAC_PF.AbreMySQLQuery0(QrUpd, MyDB, [
  'SHOW DATABASES LIKE "' + Database + '"',
  '']);
  if QrUpd.Fields[0].AsString = EmptyStr then
  begin
    if Geral.MB_Pergunta('O banco de dados ' + Database +
    ' n�o existe e deve ser criado. Confirma a cria��o?') = ID_YES then
    begin
      MyDB.Execute('CREATE DATABASE ' + Database);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrUpd, MyDB, [
      'SHOW DATABASES LIKE "' + Database + '"',
      '']);
      //
      if QrUpd.Fields[0].AsString = EmptyStr then
      begin
        Geral.MB_Erro('N�o foi poss�vel criar o banco de dados ' + Database);
        Exit;
      end else
      begin
        DBFoiCriado := True;
        Geral.MB_Info('O banco de dados ' + Database + ' foi criado');
      end;
    end;
  end else
    Geral.MB_Info('O banco de dados ' + Database + ' j� existe!');
  //
  TMeuDB := Database;
  MyDB.Disconnect;
  MyDB.DatabaseName := TMeuDB;
  USQLDB.ConectaDB(MyDB, 'VerificaConexoes.865');
  if MyDB.Connected then
  begin
    LaDB.Caption := MsgExito;
    //FmMyGlyfs.Lista_32x64A.GetBitmap(14, ImgDB.Picture.Bitmap);
    DefineBitMap(14, ImgDB);
    //
    if DBFoiCriado then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrUpd, MyDB, [
      'SHOW TABLES FROM ' + Lowercase(TMeuDB) + ' LIKE "controle"',
      '']);
      if QrUpd.Fields[0].AsString = EmptyStr then
      begin
        DBCheck.CriaTabela(MyDB, 'controle', 'controle', Memo1, actCreate,
        (*Tem_Del*)False, LaAviso1, LaAviso2, LaAviso1, LaAviso2, (*Tem_Sync*)False);
      end;
      Halt(0);
    end;
  end;
  if Informa then
    Geral.MB_Info(MsgExito);
end;

procedure TFmVerificaConexoes.VerificaTabelasPublicas1Click(Sender: TObject);
begin
{$IfNDef sVerifyDB}
  if VerificaDModG() then
  begin
    if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoNegarComAviso) then
    begin
      FmVerifiDBTerceiros.ShowModal;
      FmVerifiDBTerceiros.Destroy;
    end;
  end;
{$EndIf}
end;

end.
