unit AtrCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckBox, UnDmkProcFunc, dmkImage, DmkDAC_PF, UnDmkEnums,
  dmkDBGridZTO;

type
  TFmAtrCad = class(TForm)
    PainelDados: TPanel;
    DsAtrCad: TDataSource;
    QrAtrCad: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrAtrCadCodigo: TIntegerField;
    QrAtrCadCodUsu: TIntegerField;
    QrAtrCadNome: TWideStringField;
    DBGItens: TdmkDBGridZTO;
    PMAtributos: TPopupMenu;
    Incluinovoatributo1: TMenuItem;
    Alteraatributoatual1: TMenuItem;
    ExcluiAtributoatual1: TMenuItem;
    PMItens: TPopupMenu;
    Incluinovoitemdeatributo1: TMenuItem;
    Alteraitemdeatributoatual1: TMenuItem;
    Excluiitemdeatributoatual1: TMenuItem;
    QrAtrIts: TmySQLQuery;
    DsAtrIts: TDataSource;
    QrAtrItsCodigo: TIntegerField;
    QrAtrItsControle: TIntegerField;
    QrAtrItsCodUsu: TIntegerField;
    QrAtrItsNome: TWideStringField;
    Panel4: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBEdit: TGroupBox;
    Panel6: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCabeca: TGroupBox;
    Painel01: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtAtributos: TBitBtn;
    BtItens: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    RGAtrTyp: TdmkRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    QrAtrCadAtrTyp: TIntegerField;
    RGImprime: TdmkRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    QrAtrCadImprime: TIntegerField;
    Label4: TLabel;
    EdPadrao: TdmkEdit;
    QrAtrCadPadrao: TWideStringField;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    Label11: TLabel;
    CBCorPie: TColorBox;
    QrAtrCadCorPie: TIntegerField;
    QrAtrItsCorPie: TIntegerField;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    QrAtrItsEfeito: TSmallintField;
    QrAtrItsNO_EFEITO: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrAtrCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrAtrCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure Incluinovoatributo1Click(Sender: TObject);
    procedure Alteraatributoatual1Click(Sender: TObject);
    procedure BtAtributosClick(Sender: TObject);
    procedure PMAtributosPopup(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure QrAtrCadBeforeClose(DataSet: TDataSet);
    procedure QrAtrCadAfterScroll(DataSet: TDataSet);
    procedure Incluinovoitemdeatributo1Click(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure Alteraitemdeatributoatual1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure DBGItensDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBEdit3Change(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraAtrIts(SQLType: TSQLType);
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    FNomeTabCad, FNomeTabIts, FNomeTabTxt: String;
    FPermiteSelecionarAtrTyp: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenAtrIts(Controle: Integer);
  end;

var
  FmAtrCad: TFmAtrCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, AtrIts, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmAtrCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmAtrCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrAtrCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmAtrCad.DefParams;
begin
  VAR_GOTOTABELA := FNomeTabCad;
  VAR_GOTOMYSQLTABLE := QrAtrCad;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM ' + FNomeTabCad);
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome LIKE :P0');
  //
end;

procedure TFmAtrCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, FNomeTabCad, 'CodUsu', [], [], stIns,
    0, siPositivo, EdCodUsu);
end;

procedure TFmAtrCad.MostraAtrIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmAtrIts, FmAtrIts, afmoNegarComAviso) then
  begin
    FmAtrIts.ImgTipo.SQLType := SQLType;
    FmAtrIts.FQrCab := QrAtrCad;
    FmAtrIts.FDsCab := DsAtrCad;
    FmAtrIts.FQrIts := QrAtrIts;
    FmAtrIts.FNomeTabIts := FNomeTabIts;
    FmAtrIts.FControle   := QrAtrItsControle.Value;
    if SQLType = stIns then
      //
    else
    begin
      FmAtrIts.EdControle.ValueVariant := QrAtrItsControle.Value;
      FmAtrIts.EdCodUsu.ValueVariant   := QrAtrItsCodUsu.Value;
      //
      FmAtrIts.CBCorPie.Selected       := QrAtrItsCorPie.Value;
      FmAtrIts.EdNome.Text             := QrAtrItsNome.Value;
    end;
    FmAtrIts.ShowModal;
    FmAtrIts.Destroy;
  end;
end;

procedure TFmAtrCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Erro('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmAtrCad.PMAtributosPopup(Sender: TObject);
begin
  Alteraatributoatual1.Enabled :=
    (QrAtrCad.State <> dsInactive) and (QrAtrCad.RecordCount > 0);
end;

procedure TFmAtrCad.PMItensPopup(Sender: TObject);
begin
  Incluinovoitemdeatributo1.Enabled := (QrAtrCad.State <> dsInactive) and
    (QrAtrCad.RecordCount >0);
  Alteraitemdeatributoatual1.Enabled := (QrAtrIts.State <> dsInactive) and
    (QrAtrIts.RecordCount >0);
end;

procedure TFmAtrCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  // N�o h� nome definido para a tebela ainda!
  //Va(vpLast);
end;

procedure TFmAtrCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmAtrCad.DBEdit3Change(Sender: TObject);
begin
  DBEdit3.Color      := QrAtrCadCorPie.Value;
  DBEdit3.Font.Color := QrAtrCadCorPie.Value;
end;

procedure TFmAtrCad.DBGItensDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if Column.FieldName = 'CorPie' then Cor := QrAtrItsCorPie.Value else
  Exit;
  //
  MyObjects.DesenhaTextoEmDBGrid(
    TDbGrid(DBGItens), Rect, Cor, Cor, Column.Alignment, '');
end;

procedure TFmAtrCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmAtrCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmAtrCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmAtrCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmAtrCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmAtrCad.Alteraatributoatual1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
  RGAtrTyp.Enabled := FPermiteSelecionarAtrTyp;
  if RGAtrTyp.Enabled then
  begin
    case TTypTabAtr(QrAtrCadAtrTyp.Value) of
      ttaPreDef: RGAtrTyp.Enabled := QrAtrIts.RecordCount = 0;
      ttaTxtFree:
      begin
        Qry := TmySQLQuery.Create(Dmod);
        try
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT * ',
          'FROM ' + FNomeTabTxt,
          '']);
          RGAtrTyp.Enabled := Qry.RecordCount = 0;
        except
          RGAtrTyp.Enabled := False;
          Qry.Free;
          Geral.MB_ERRO('� possivel que a tabela de textos "' + FNomeTabTxt +
            '" n�o exista!');
          raise;
        end;
      end;
      else RGAtrTyp.Enabled := False;
    end;
  end;
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrAtrCad, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, FNomeTabCad);
  CBCorPie.Selected := QrAtrCadCorPie.Value;
end;

procedure TFmAtrCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrAtrCadCodigo.Value;
  VAR_CAD_ITEM := QrAtrItsControle.Value;
  Close;
end;

procedure TFmAtrCad.Alteraitemdeatributoatual1Click(Sender: TObject);
begin
(*
  UmyMod.FormInsUpd_Show(TFmAtrIts, FmAtrIts, afmoNegarComAviso,
    QrAtrIts, stUpd);
*)
  MostraAtrIts(stUpd);
end;

procedure TFmAtrCad.BtAtributosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAtributos, BtAtributos);
end;

procedure TFmAtrCad.BtConfirmaClick(Sender: TObject);
var
  Codigo, CorPie: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o.') then
    Exit;
  //
  if MyObjects.FIC(RGAtrTyp.ItemIndex < 1, RGAtrTyp,
  'Informe a forma de defini��o do valor do atributo.') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32(
    FNomeTabCad, 'Codigo', '', '', tsDef, ImgTipo.SQLType, QrAtrCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PainelEdita,
    FNomeTabCad, Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    CorPie := CBCorPie.Selected;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, FNomeTabCad, False, [
    'CorPie'], ['Codigo'], [
    CorPie], [Codigo], True);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmAtrCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  //if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', FNomeTabCad, Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, FNomeTabCad, 'Codigo');
  MostraEdicao(0, stLok, 0);
  //UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, FNomeTabCad, 'Codigo');
end;

procedure TFmAtrCad.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmAtrCad.FormCreate(Sender: TObject);
begin
  FPermiteSelecionarAtrTyp := False;
  ImgTipo.SQLType := stLok;
  GBEdit.Align    := alClient;
  DBGItens.Align  := alClient;
  CriaOForm;
end;

procedure TFmAtrCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrAtrCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAtrCad.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmAtrCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmAtrCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrAtrCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmAtrCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmAtrCad.QrAtrCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmAtrCad.QrAtrCadAfterScroll(DataSet: TDataSet);
var
  Mostra: Boolean;
begin
  ReopenAtrIts(0);
  Mostra := TTypTabAtr(QrAtrCadAtrTyp.Value) = ttaPredef;
  DBGItens.Visible := Mostra;
  BtItens.Visible  := Mostra;
end;

procedure TFmAtrCad.FormActivate(Sender: TObject);
begin
  RGAtrTyp.Enabled := FPermiteSelecionarAtrTyp;
  MyObjects.CorIniComponente();
end;

procedure TFmAtrCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrAtrCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, FNomeTabCad, Dmod.MyDB, CO_VAZIO));
end;

procedure TFmAtrCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAtrCad.Incluinovoatributo1Click(Sender: TObject);
begin
  RGAtrTyp.Enabled := FPermiteSelecionarAtrTyp;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrAtrCad, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, FNomeTabCad);
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, FNomeTabCad, 'CodUsu', [], [], stIns,
    0, siPositivo, EdCodUsu);
  EdNome.ValueVariant := '';
end;

procedure TFmAtrCad.Incluinovoitemdeatributo1Click(Sender: TObject);
begin
  MostraAtrIts(stIns);
(*
  UmyMod.FormInsUpd_Show(TFmAtrIts, FmAtrIts, afmoNegarComAviso,
    QrAtrIts, stIns);
*)
end;

procedure TFmAtrCad.QrAtrCadBeforeClose(DataSet: TDataSet);
begin
  QrAtrIts.Close;
end;

procedure TFmAtrCad.QrAtrCadBeforeOpen(DataSet: TDataSet);
begin
  QrAtrCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmAtrCad.ReopenAtrIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtrIts, Dmod.MyDB, [
  'SELECT ',
  'ELT(Efeito + 1, "Negativo", "Neutro", "Positivo") NO_EFEITO, ',
  'Codigo, Controle, ',
  'CodUsu, Nome , CorPie, Efeito ',
  'FROM ' + FNomeTabIts,
  'WHERE Codigo=' + Geral.FF0(QrAtrCadCodigo.Value),
  '']);
  //
  if Controle <> 0 then
    QrAtrIts.Locate('Controle', Controle, []);
end;

end.

