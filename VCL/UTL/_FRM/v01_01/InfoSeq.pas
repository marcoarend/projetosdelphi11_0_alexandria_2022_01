unit InfoSeq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, Db, mySQLDbTables,
  frxClass, frxDBSet, dmkGeral, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmInfoSeq = class(TForm)
    DBGrid1: TDBGrid;
    QrInfoSeq: TmySQLQuery;
    DsInfoSeq: TDataSource;
    QrInfoSeqLinha: TIntegerField;
    QrInfoSeqLocal: TWideStringField;
    QrInfoSeqAcao: TWideStringField;
    frxInfoSeq: TfrxReport;
    frxDsInfoSeq: TfrxDBDataset;
    QrInfoSeqItem: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    BtMostra: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure BtMostraClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTmpTable: String;
  end;

  var
  FmInfoSeq: TFmInfoSeq;

implementation

uses UnMyObjects, Module, ModuleGeral, Principal;

{$R *.DFM}

procedure TFmInfoSeq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmInfoSeq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmInfoSeq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType  := stLok;
  BtMostra.Visible := True;
end;

procedure TFmInfoSeq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmInfoSeq.FormShow(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrInfoSeq, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + FTmpTable,
    'ORDER BY Linha ',
    '']);
end;

procedure TFmInfoSeq.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxInfoSeq, 'Configurações pendentes');
end;

procedure TFmInfoSeq.BtMostraClick(Sender: TObject);
begin
  //FmPrincipal.MostraJanela(QrInfoSeqItem.Value);
end;

end.
