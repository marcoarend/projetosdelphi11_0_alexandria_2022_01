object FmRecibo: TFmRecibo
  Left = 349
  Top = 173
  BorderStyle = bsSizeToolWin
  Caption = 'IMP-RECIB-001 :: Impress'#227'o de Recibo'
  ClientHeight = 661
  ClientWidth = 818
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 818
    Height = 345
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 4
      Width = 87
      Height = 13
      Caption = 'N'#250'mero do recibo:'
    end
    object Label10: TLabel
      Left = 596
      Top = 4
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object Label2: TLabel
      Left = 8
      Top = 56
      Width = 52
      Height = 13
      Caption = 'Recebi de:'
    end
    object Label3: TLabel
      Left = 8
      Top = 80
      Width = 52
      Height = 13
      Caption = 'Endere'#231'o: '
    end
    object Label21: TLabel
      Left = 556
      Top = 56
      Width = 55
      Height = 13
      Caption = 'CNPJ/CPF:'
    end
    object Label4: TLabel
      Left = 8
      Top = 120
      Width = 82
      Height = 13
      Caption = 'A import'#226'ncia de '
    end
    object Label5: TLabel
      Left = 8
      Top = 166
      Width = 50
      Height = 13
      Caption = 'Referente:'
    end
    object Label6: TLabel
      Left = 8
      Top = 238
      Width = 47
      Height = 13
      Caption = 'Emitente: '
    end
    object Label20: TLabel
      Left = 556
      Top = 236
      Width = 55
      Height = 13
      Caption = 'CNPJ/CPF:'
    end
    object Label7: TLabel
      Left = 8
      Top = 268
      Width = 49
      Height = 13
      Caption = 'Endere'#231'o:'
    end
    object Label23: TLabel
      Left = 92
      Top = 301
      Width = 142
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Local e data: (F5 para atual)'
      Transparent = True
    end
    object Label8: TLabel
      Left = 92
      Top = 323
      Width = 142
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Respons'#225'vel:'
      Transparent = True
    end
    object EdLocalData: TdmkEdit
      Left = 241
      Top = 298
      Width = 552
      Height = 20
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 26
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnKeyDown = EdLocalDataKeyDown
    end
    object EdNumero: TdmkEdit
      Left = 8
      Top = 20
      Width = 585
      Height = 29
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -23
      Font.Name = 'Lucida Console'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdValor: TdmkEdit
      Left = 596
      Top = 20
      Width = 197
      Height = 29
      Alignment = taRightJustify
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -23
      Font.Name = 'Lucida Console'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdValorChange
    end
    object EdEmitente: TdmkEdit
      Left = 92
      Top = 52
      Width = 461
      Height = 20
      TabStop = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdERua: TdmkEdit
      Left = 92
      Top = 74
      Width = 289
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdECidade: TdmkEdit
      Left = 92
      Top = 95
      Width = 289
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdENumero: TdmkEdit
      Left = 384
      Top = 74
      Width = 57
      Height = 20
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdEUF: TdmkEdit
      Left = 384
      Top = 95
      Width = 57
      Height = 20
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdECEP: TdmkEdit
      Left = 444
      Top = 95
      Width = 89
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdECompl: TdmkEdit
      Left = 444
      Top = 74
      Width = 201
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdEPais: TdmkEdit
      Left = 536
      Top = 95
      Width = 121
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdETe1: TdmkEdit
      Left = 660
      Top = 95
      Width = 133
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 12
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdEBairro: TdmkEdit
      Left = 648
      Top = 74
      Width = 145
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdECNPJ: TdmkEdit
      Left = 616
      Top = 52
      Width = 177
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtCPFJ
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object Extenso: TdmkMemo
      Left = 92
      Top = 116
      Width = 701
      Height = 45
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 13
      UpdType = utYes
    end
    object Texto: TdmkMemo
      Left = 92
      Top = 163
      Width = 701
      Height = 68
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 14
      UpdType = utYes
    end
    object EdBeneficiario: TdmkEdit
      Left = 92
      Top = 233
      Width = 461
      Height = 20
      TabStop = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 15
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBCNPJ: TdmkEdit
      Left = 616
      Top = 233
      Width = 177
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 16
      FormatType = dmktfString
      MskType = fmtCPFJ
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBBairro: TdmkEdit
      Left = 648
      Top = 255
      Width = 145
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 20
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBTe1: TdmkEdit
      Left = 660
      Top = 276
      Width = 133
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 25
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBPais: TdmkEdit
      Left = 536
      Top = 276
      Width = 121
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 24
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBCompl: TdmkEdit
      Left = 444
      Top = 255
      Width = 201
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 19
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBCEP: TdmkEdit
      Left = 444
      Top = 276
      Width = 89
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 23
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBUF: TdmkEdit
      Left = 384
      Top = 276
      Width = 57
      Height = 20
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 22
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBNumero: TdmkEdit
      Left = 384
      Top = 255
      Width = 57
      Height = 20
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 18
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBRua: TdmkEdit
      Left = 92
      Top = 255
      Width = 289
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 17
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBCidade: TdmkEdit
      Left = 92
      Top = 276
      Width = 289
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 21
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdResponsavel: TdmkEdit
      Left = 241
      Top = 320
      Width = 552
      Height = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 27
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 818
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 770
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 722
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 258
        Height = 32
        Caption = 'Impress'#227'o de Recibo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 258
        Height = 32
        Caption = 'Impress'#227'o de Recibo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 258
        Height = 32
        Caption = 'Impress'#227'o de Recibo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 591
    Width = 818
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 672
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 670
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImprime: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Hint = 'Preview'
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImprimeClick
      end
      object BtConfirmacao: TBitBtn
        Left = 136
        Top = 3
        Width = 120
        Height = 40
        Hint = 'Preview'
        Caption = '&Confirma'#231#227'o'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtConfirmacaoClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 547
    Width = 818
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 814
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 393
    Width = 818
    Height = 154
    Align = alClient
    TabOrder = 4
    object Splitter1: TSplitter
      Left = 561
      Top = 1
      Height = 152
      ExplicitLeft = 596
      ExplicitTop = 84
      ExplicitHeight = 100
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 560
      Height = 152
      Align = alLeft
      DataSource = DsReciboImpCab
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'DtConfrmad_TXT'
          Title.Caption = 'Confirmado'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CNPJCPF'
          Title.Caption = 'CNPJ/CPF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Emitente'
          Title.Caption = 'Nome do Requerente'
          Width = 166
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtPrnted'
          Title.Caption = 'Data da impress'#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Beneficiario'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtRecibo'
          Title.Caption = 'Data Recibo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Recibo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Visible = True
        end>
    end
    object DBGrid2: TDBGrid
      Left = 564
      Top = 1
      Width = 253
      Height = 152
      Align = alClient
      DataSource = DsReciboImpImp
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'DtPrnted'
          Title.Caption = 'Impresso'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtConfrmad_TXT'
          Title.Caption = 'Confirmado'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Recibo'
          Visible = True
        end>
    end
  end
  object frxRecibo1: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38009.439963981500000000
    ReportOptions.LastChange = 39098.923307291700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VARF_NOTCOLOR> = True then Shape8.Color := clWhite;'
      '  //    '
      '  if (<Logo2_4_x_1_0Existe> = True) then'
      '  begin              '
      
        '    Picture2.Visible := True;                                   ' +
        '           '
      '    Picture2.LoadFromFile(<Logo2_4_x_1_0Path>);'
      '  end else'
      '  begin'
      
        '    Picture2.Visible := False;                                  ' +
        '            '
      '  end;'
      '  //          '
      'end.')
    OnGetValue = frxRecibo1GetValue
    OnUserFunction = frxRecibo1UserFunction
    Left = 336
    Top = 36
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object Shape8: TfrxShapeView
        Width = 699.213050000000000000
        Height = 487.559370000000000000
        Fill.BackColor = 15132390
      end
      object Shape1: TfrxShapeView
        Left = 147.401670000000000000
        Top = 26.456710000000000000
        Width = 267.559060000000000000
        Height = 48.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo2: TfrxMemoView
        Left = 176.283550000000000000
        Top = 26.897650000000000000
        Width = 226.897650000000000000
        Height = 39.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[NUMERO_P]')
        ParentFont = False
      end
      object Memo28: TfrxMemoView
        Left = 151.401670000000000000
        Top = 26.456710000000000000
        Width = 24.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#186)
        ParentFont = False
      end
      object Shape2: TfrxShapeView
        Left = 515.590600000000000000
        Top = 26.456710000000000000
        Width = 173.070810000000000000
        Height = 48.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo29: TfrxMemoView
        Left = 552.031540000000000000
        Top = 30.897650000000000000
        Width = 128.629870000000000000
        Height = 39.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[VALOR_P]')
        ParentFont = False
      end
      object Memo30: TfrxMemoView
        Left = 519.590600000000000000
        Top = 30.456710000000000000
        Width = 28.440940000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor:')
        ParentFont = False
      end
      object Shape3: TfrxShapeView
        Left = 11.338590000000000000
        Top = 78.456710000000000000
        Width = 677.322820000000000000
        Height = 112.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo4: TfrxMemoView
        Left = 15.559060000000000000
        Top = 80.456710000000000000
        Width = 477.102350000000000000
        Height = 30.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Recebemos de [NOME_PC]')
        ParentFont = False
      end
      object Memo3: TfrxMemoView
        Left = 84.661410000000000000
        Top = 110.456710000000000000
        Width = 188.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[RUA_PC]')
        ParentFont = False
      end
      object Memo1: TfrxMemoView
        Left = 496.188930000000000000
        Top = 82.456710000000000000
        Width = 188.472480000000000000
        Height = 18.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CNPJ_PC]')
        ParentFont = False
      end
      object Memo6: TfrxMemoView
        Left = 556.661410000000000000
        Top = 130.456710000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[TE1_PC]')
        ParentFont = False
      end
      object Memo7: TfrxMemoView
        Left = 288.661410000000000000
        Top = 110.456710000000000000
        Width = 56.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[NUMERO_PC]')
        ParentFont = False
      end
      object Memo8: TfrxMemoView
        Left = 360.661410000000000000
        Top = 110.456710000000000000
        Width = 184.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[COMPL_PC]')
        ParentFont = False
      end
      object Memo9: TfrxMemoView
        Left = 556.661410000000000000
        Top = 110.456710000000000000
        Width = 128.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[BAIRRO_PC]')
        ParentFont = False
      end
      object Memo10: TfrxMemoView
        Left = 84.661410000000000000
        Top = 130.456710000000000000
        Width = 188.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CIDADE_PC]')
        ParentFont = False
      end
      object Memo11: TfrxMemoView
        Left = 276.661410000000000000
        Top = 130.456710000000000000
        Width = 40.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[UF_PC]')
        ParentFont = False
      end
      object Memo12: TfrxMemoView
        Left = 320.661410000000000000
        Top = 130.456710000000000000
        Width = 104.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CEP_PC]')
        ParentFont = False
      end
      object Memo13: TfrxMemoView
        Left = 428.661410000000000000
        Top = 130.456710000000000000
        Width = 116.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[PAIS_PC]')
        ParentFont = False
      end
      object Memo14: TfrxMemoView
        Left = 95.779530000000000000
        Top = 150.456710000000000000
        Width = 584.881880000000000000
        Height = 38.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[EXTENSO_P]')
        ParentFont = False
      end
      object Memo32: TfrxMemoView
        Left = 15.559060000000000000
        Top = 110.456710000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
      end
      object Memo33: TfrxMemoView
        Left = 15.559060000000000000
        Top = 146.677180000000000000
        Width = 80.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'A import'#226'ncia de:')
        ParentFont = False
      end
      object Shape4: TfrxShapeView
        Left = 11.338590000000000000
        Top = 194.456710000000000000
        Width = 677.322820000000000000
        Height = 68.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo16: TfrxMemoView
        Left = 15.779530000000000000
        Top = 198.456710000000000000
        Width = 668.881880000000000000
        Height = 62.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Referente a: [MOTIVO_P]')
        ParentFont = False
      end
      object Shape5: TfrxShapeView
        Left = 11.338590000000000000
        Top = 266.456710000000000000
        Width = 677.322820000000000000
        Height = 88.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo15: TfrxMemoView
        Left = 15.338590000000000000
        Top = 272.456710000000000000
        Width = 473.322820000000000000
        Height = 17.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Emitente: [EMITENTEP]')
        ParentFont = False
      end
      object Memo37: TfrxMemoView
        Left = 15.338590000000000000
        Top = 290.456710000000000000
        Width = 48.881880000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
      end
      object Shape6: TfrxShapeView
        Left = 11.338590000000000000
        Top = 358.456710000000000000
        Width = 677.322820000000000000
        Height = 32.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo55: TfrxMemoView
        Left = 15.559060000000000000
        Top = 370.456710000000000000
        Width = 661.102350000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[LOCAL_E_DATA]')
        ParentFont = False
      end
      object Shape7: TfrxShapeView
        Left = 11.338590000000000000
        Top = 394.456710000000000000
        Width = 677.322820000000000000
        Height = 56.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo27: TfrxMemoView
        Left = 316.661410000000000000
        Top = 430.456710000000000000
        Width = 368.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftTop]
        HAlign = haRight
        Memo.UTF8W = (
          '[RESPONSAVEL_E]')
        ParentFont = False
      end
      object Memo5: TfrxMemoView
        Left = 415.748300000000000000
        Top = 26.456710000000000000
        Width = 98.267780000000000000
        Height = 45.354360000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'RECIBO')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        Left = 83.149660000000000000
        Top = 300.126160000000000000
        Width = 188.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[RUA_PE]')
        ParentFont = False
      end
      object Memo18: TfrxMemoView
        Left = 494.677180000000000000
        Top = 272.126160000000000000
        Width = 188.472480000000000000
        Height = 18.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CNPJ_PE]')
        ParentFont = False
      end
      object Memo19: TfrxMemoView
        Left = 555.149660000000000000
        Top = 320.126160000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[TE1_PE]')
        ParentFont = False
      end
      object Memo20: TfrxMemoView
        Left = 359.149660000000000000
        Top = 300.126160000000000000
        Width = 184.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[COMPL_PE]')
        ParentFont = False
      end
      object Memo21: TfrxMemoView
        Left = 555.149660000000000000
        Top = 300.126160000000000000
        Width = 128.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[BAIRRO_PE]')
        ParentFont = False
      end
      object Memo22: TfrxMemoView
        Left = 83.149660000000000000
        Top = 320.126160000000000000
        Width = 188.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CIDADE_PE]')
        ParentFont = False
      end
      object Memo23: TfrxMemoView
        Left = 275.149660000000000000
        Top = 320.126160000000000000
        Width = 40.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[UF_PE]')
        ParentFont = False
      end
      object Memo24: TfrxMemoView
        Left = 319.149660000000000000
        Top = 320.126160000000000000
        Width = 104.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CEP_PE]')
        ParentFont = False
      end
      object Memo25: TfrxMemoView
        Left = 427.149660000000000000
        Top = 320.126160000000000000
        Width = 116.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[PAIS_PE]')
        ParentFont = False
      end
      object BarCode1: TfrxBarCodeView
        Left = 37.795300000000000000
        Top = 404.409710000000000000
        Width = 129.000000000000000000
        Height = 34.015770000000000000
        BarType = bcCode39Extended
        Expression = '<VARF_BARCODE_BARC>'
        Rotation = 0
        ShowText = False
        TestLine = False
        Text = '12345678'
        WideBarRatio = 2.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
      end
      object Shape9: TfrxShapeView
        Left = 11.338590000000000000
        Top = 18.897650000000000000
        Width = 129.259842519685000000
        Height = 54.047244094488190000
        Frame.Width = 0.500000000000000000
      end
      object Picture2: TfrxPictureView
        Tag = 1
        Left = 12.094488190000000000
        Top = 19.653543310000000000
        Width = 128.503937010000000000
        Height = 53.291338580000000000
        HightQuality = True
        Transparent = False
        TransparentColor = clWhite
      end
    end
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 312
    Top = 44
  end
  object QrReciboImpCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrReciboImpCabBeforeClose
    AfterScroll = QrReciboImpCabAfterScroll
    SQL.Strings = (
      'SELECT imp.*,'
      'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome) NO_Emitente, '
      'IF(imp.DtConfrmad < "1900-01-01", "", '
      'DATE_FORMAT(imp.DtConfrmad, "%d/%m/%Y %H:%i:%s")) DtConfrmad_TXT'
      'FROM reciboimp imp'
      'LEFT JOIN entidades emi ON emi.Codigo=imp.Emitente'
      'WHERE imp.Recibo = '#39'ALSV#29527-57'#39)
    Left = 108
    Top = 412
    object QrReciboImpCabNO_Emitente: TWideStringField
      FieldName = 'NO_Emitente'
      Size = 100
    end
    object QrReciboImpCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrReciboImpCabDtRecibo: TDateTimeField
      FieldName = 'DtRecibo'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrReciboImpCabDtPrnted: TDateTimeField
      FieldName = 'DtPrnted'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrReciboImpCabEmitente: TIntegerField
      FieldName = 'Emitente'
      Required = True
    end
    object QrReciboImpCabBeneficiario: TIntegerField
      FieldName = 'Beneficiario'
      Required = True
    end
    object QrReciboImpCabCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
    end
    object QrReciboImpCabValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrReciboImpCabDtConfrmad: TDateTimeField
      FieldName = 'DtConfrmad'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrReciboImpCabDtConfrmad_TXT: TWideStringField
      FieldName = 'DtConfrmad_TXT'
      Size = 19
    end
    object QrReciboImpCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 511
    end
  end
  object DsReciboImpCab: TDataSource
    DataSet = QrReciboImpCab
    Left = 108
    Top = 460
  end
  object PMConfirmacao: TPopupMenu
    OnPopup = PMConfirmacaoPopup
    Left = 258
    Top = 586
    object Confirma1: TMenuItem
      Caption = '&Confirma'
      OnClick = Confirma1Click
    end
    object Desconfirma1: TMenuItem
      Caption = '&Desconfirma'
      OnClick = Desconfirma1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
  object QrReciboImpImp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imp.*,'
      'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome) NO_Emitente, '
      'IF(imp.DtConfrmad < "1900-01-01", "", '
      'DATE_FORMAT(imp.DtConfrmad, "%d/%m/%Y %H:%i:%s")) DtConfrmad_TXT'
      'FROM reciboimp imp'
      'LEFT JOIN entidades emi ON emi.Codigo=imp.Emitente'
      'WHERE imp.Recibo = '#39'ALSV#29527-57'#39)
    Left = 640
    Top = 420
    object QrReciboImpImpCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrReciboImpImpControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrReciboImpImpDtPrnted: TDateTimeField
      FieldName = 'DtPrnted'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrReciboImpImpDtConfrmad: TDateTimeField
      FieldName = 'DtConfrmad'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrReciboImpImpDtConfrmad_TXT: TWideStringField
      FieldName = 'DtConfrmad_TXT'
      Size = 19
    end
    object QrReciboImpImpRecibo: TWideStringField
      FieldName = 'Recibo'
      Size = 255
    end
  end
  object DsReciboImpImp: TDataSource
    DataSet = QrReciboImpImp
    Left = 640
    Top = 468
  end
end
