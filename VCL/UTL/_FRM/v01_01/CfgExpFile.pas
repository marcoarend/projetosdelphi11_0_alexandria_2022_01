unit CfgExpFile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkEdit,
  UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmCfgExpFile = class(TForm)
    Panel1: TPanel;
    Panel5: TPanel;
    Memo: TMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel6: TPanel;
    BtOK: TBitBtn;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    SbDir: TSpeedButton;
    Label3: TLabel;
    EdDir: TdmkEdit;
    EdArq: TdmkEdit;
    EdExt: TdmkEdit;
    GroupBox2: TGroupBox;
    Panel4: TPanel;
    CkExcluir: TCheckBox;
    CkSemAcentos: TCheckBox;
    CkEscreveLinhasVazias: TCheckBox;
    CkQuebraDeLinha: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbDirClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FArqSalvo: String;
  end;

  var
  FmCfgExpFile: TFmCfgExpFile;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmCfgExpFile.BtOKClick(Sender: TObject);
{
function TUn M L A G e r a l .ExportaMemoToFileExt2(Memo: TMemo; Arquivo, Diretorio: String;
  Exclui, EscreveLinhasVazias, SemAcentos: Boolean; CrLf: Integer): Boolean;
}
var
  Text: TextFile;
  i: Integer;
  Arquivo, txt: String;
  GravaLinha: Boolean;
begin
  try
    if MyObjects.FIC(Trim(EdDir.Text) = '', EdDir, 'Informe o diret�rio!') then
      Exit;
    if MyObjects.FIC(Trim(EdArq.Text) = '', EdArq, 'Informe o arquivo!') then
      Exit;
    if MyObjects.FIC(Trim(EdExt.Text) = '', EdExt, 'Informe a extens�o do arquivo!') then
      Exit;
    Arquivo := dmkPF.CaminhoArquivo(EdDir.Text, EdArq.Text, EdExt.Text);
    ForceDirectories(EdDir.Text);
    if CkExcluir.Checked then if FileExists(Arquivo) then DeleteFile(Arquivo);
    AssignFile(Text, Arquivo);
    ReWrite(Text);
    for i := 0 to Memo.Lines.Count -1 do
    begin
      GravaLinha := False;
      txt := Memo.Lines[i];
      if CkSemAcentos.Checked then txt := Geral.SemAcento(txt);
      if not CkEscreveLinhasVazias.Checked then
      begin
        if txt <> '' then
          GravaLinha := True;
      end else GravaLinha := True;
      if GravaLinha then
      begin
        case CkQuebraDeLinha.ItemIndex of
          0{  10}: Write(Text, txt + Char( 10 ));
          1{  13}: Write(Text, txt + Char( 13 ));
          2{1013}: Write(Text, txt + Char( 10 ) + Char( 13 ));
          3{1310}: Write(Text, txt + Char( 13 ) + Char( 10 )); // => WriteLn =  # 1 3 # 1 0
          else Write(Text, txt);
        end;
      end;
    end;
    CloseFile(Text);
    if FileExists(Arquivo) then
      FArqSalvo := Arquivo;
    Close;
  except
    raise;
  end;
end;

procedure TFmCfgExpFile.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCfgExpFile.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCfgExpFile.FormCreate(Sender: TObject);
begin
  FArqSalvo := '';
  ImgTipo.SQLType := stCpy;
end;

procedure TFmCfgExpFile.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCfgExpFile.SbDirClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDir);
end;

end.
