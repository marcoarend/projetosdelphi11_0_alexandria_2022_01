object FmFeriados: TFmFeriados
  Left = 339
  Top = 185
  Caption = 'FER-IADOS-001 :: Cadastro de Feriados'
  ClientHeight = 347
  ClientWidth = 714
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 52
    Width = 714
    Height = 295
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object Label9: TLabel
      Left = 16
      Top = 8
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object Label10: TLabel
      Left = 124
      Top = 8
      Width = 35
      Height = 13
      Caption = 'Motivo:'
    end
    object TPData: TdmkEditDateTimePicker
      Left = 16
      Top = 24
      Width = 105
      Height = 21
      Date = 40444.452701678240000000
      Time = 40444.452701678240000000
      TabOrder = 0
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdNome: TdmkEdit
      Left = 124
      Top = 24
      Width = 541
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'EdNome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'EdNome'
      ValWarn = False
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 232
      Width = 714
      Height = 63
      Align = alBottom
      TabOrder = 3
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 18
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel2: TPanel
        Left = 576
        Top = 15
        Width = 136
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object CkAnual: TCheckBox
      Left = 16
      Top = 50
      Width = 97
      Height = 17
      Caption = 'Anual'
      TabOrder = 2
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 52
    Width = 714
    Height = 295
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 231
      Width = 714
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 710
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel1: TPanel
          Left = 601
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 204
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 20
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 112
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
      end
    end
    object GBAvisos1: TGroupBox
      Left = 0
      Top = 0
      Width = 714
      Height = 44
      Align = alTop
      Caption = ' Avisos: '
      TabOrder = 1
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 710
        Height = 27
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object DBGrid1: TdmkDBGrid
      Left = 48
      Top = 51
      Width = 590
      Height = 160
      Columns = <
        item
          Expanded = False
          FieldName = 'Data_Txt'
          Title.Caption = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Anual'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Motivo'
          Width = 300
          Visible = True
        end>
      Color = clWindow
      DataSource = DsFeriados
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Data_Txt'
          Title.Caption = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Anual'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Motivo'
          Width = 300
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 714
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 666
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 52
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 618
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 263
        Height = 32
        Caption = 'Cadastro de Feriados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 263
        Height = 32
        Caption = 'Cadastro de Feriados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 263
        Height = 32
        Caption = 'Cadastro de Feriados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object QrFeriados: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFeriadosAfterOpen
    SQL.Strings = (
      'SELECT *,'
      
        'IF(Anual=1, DATE_FORMAT(Data, "%d/%m"), DATE_FORMAT(Data, "%d/%m' +
        '/%Y")) Data_TXT,'
      
        'IF(Anual=1, CONCAT(DATE_FORMAT(Now(), "%Y"), DATE_FORMAT(Data, "' +
        '%m%d")), DATE_FORMAT(Data, "%Y%m%d")) Data_Ordena '
      'FROM feriados'
      'ORDER BY Data_Ordena DESC')
    Left = 20
    Top = 9
    object QrFeriadosData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFeriadosMotivo: TWideStringField
      FieldName = 'Motivo'
      Required = True
      Size = 100
    end
    object QrFeriadosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFeriadosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFeriadosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFeriadosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFeriadosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFeriadosAnual: TSmallintField
      FieldName = 'Anual'
      MaxValue = 1
    end
    object QrFeriadosData_Txt: TWideStringField
      FieldName = 'Data_Txt'
      Size = 10
    end
    object QrFeriadosData_Ordena: TWideStringField
      FieldName = 'Data_Ordena'
      Size = 10
    end
  end
  object DsFeriados: TDataSource
    DataSet = QrFeriados
    Left = 48
    Top = 9
  end
end
