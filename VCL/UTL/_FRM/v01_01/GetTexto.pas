unit GetTexto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkMemo;

type
  TFmGetTexto = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrEntiDocs: TMySQLQuery;
    DsEntiDocs: TDataSource;
    Panel2: TPanel;
    MeTexto: TdmkMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmGetTexto: TFmGetTexto;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmGetTexto.BtOKClick(Sender: TObject);
begin
  VAR_ALTEROU := True;
  VAR_GETTEXTO_GET := MeTexto.Text;
  //
  Close;
end;

procedure TFmGetTexto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGetTexto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGetTexto.FormCreate(Sender: TObject);
begin
  VAR_ALTEROU := False;
  VAR_GETTEXTO_GET := EmptyStr;
  ImgTipo.SQLType := stUpd;
end;

procedure TFmGetTexto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
