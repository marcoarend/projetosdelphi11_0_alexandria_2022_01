unit SelListArr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmSelListArr = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrStqaLocIts: TMySQLQuery;
    DsStqaLocIts: TDataSource;
    Panel2: TPanel;
    Grade: TStringGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FEdit: TdmkEdit;
    FResult: Integer;
    FLista: array of String;
  end;

  var
  FmSelListArr: TFmSelListArr;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmSelListArr.BtSaidaClick(Sender: TObject);
begin
  FResult := - 1;
  Close;
end;

procedure TFmSelListArr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSelListArr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmSelListArr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSelListArr.GradeDblClick(Sender: TObject);
begin
  FResult := Grade.Row - 1;
  if (FResult > -1) and (Length(FLista) > 0 ) then
  begin
    VAR_TEXTO_SEL := FLista[FResult];
    FEdit.ValueVariant := FResult;
  end;
  MyObjects.LimpaGrade(Grade, 0, 0, True);
  Close;
end;

end.
