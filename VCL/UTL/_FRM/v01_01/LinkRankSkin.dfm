object FmLinkRankSkin: TFmLinkRankSkin
  Left = 339
  Top = 185
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'SKN-SELEC-001 :: Sele'#231#227'o do Tema do Aplicativo'
  ClientHeight = 586
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnHide = FormHide
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 640
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 592
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 544
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 370
        Height = 32
        Caption = 'Sele'#231#227'o de Tema do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 370
        Height = 32
        Caption = 'Sele'#231#227'o de Tema do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 370
        Height = 32
        Caption = 'Sele'#231#227'o de Tema do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 640
    Height = 145
    Align = alTop
    Caption = ' Dados do skin: '
    TabOrder = 1
    object PainelEdit: TPanel
      Left = 2
      Top = 70
      Width = 636
      Height = 73
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 12
        Top = 3
        Width = 85
        Height = 13
        Caption = 'Nome do arquivo:'
      end
      object SBNadador: TSpeedButton
        Left = 584
        Top = 20
        Width = 24
        Height = 24
        Caption = '...'
        OnClick = SBNadadorClick
      end
      object EdArqLinkRankSkin: TdmkEdit
        Left = 12
        Top = 21
        Width = 569
        Height = 24
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdArqLinkRankSkinChange
      end
      object CkSkinAba: TCheckBox
        Left = 12
        Top = 50
        Width = 250
        Height = 17
        Caption = 'N'#227'o usar skin nas janelas em aba'
        TabOrder = 1
        OnClick = CkSkinAbaClick
      end
    end
    object RGQualUsaLinkRankSkin: TRadioGroup
      Left = 2
      Top = 15
      Width = 636
      Height = 55
      Align = alTop
      Caption = ' Qual usa: '
      Columns = 3
      ItemIndex = 1
      Items.Strings = (
        'N'#227'o quero usar'
        'Usar o padr'#227'o'
        'Usar o arquivo abaixo')
      TabOrder = 1
      OnClick = RGQualUsaLinkRankSkinClick
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 516
    Width = 640
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 494
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 492
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object BtRestaurar: TBitBtn
        Tag = 10013
        Left = 172
        Top = 3
        Width = 150
        Height = 40
        Cursor = crHandPoint
        Caption = '&Restaurar Padr'#245'es'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtRestaurarClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 193
    Width = 640
    Height = 266
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object RGComoUsaLinkRankSkin: TRadioGroup
      Left = 0
      Top = 0
      Width = 640
      Height = 55
      Align = alTop
      Caption = ' Como usa: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Em tudo'
        'N'#227'o usar nos itens de menu'
        'N'#227'o usar no menu principal')
      TabOrder = 0
      OnClick = RGComoUsaLinkRankSkinClick
    end
    object Panel3: TPanel
      Left = 0
      Top = 55
      Width = 640
      Height = 211
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 319
        Height = 211
        Align = alLeft
        Caption = ' T'#237'tulo das janelas:  '
        TabOrder = 0
        object GroupBox5: TGroupBox
          Left = 2
          Top = 15
          Width = 315
          Height = 111
          Align = alTop
          Caption = ' Cores do t'#237'tulo:'
          TabOrder = 0
          object Label1: TLabel
            Left = 12
            Top = 24
            Width = 30
            Height = 13
            Caption = 'T'#237'tulo:'
          end
          object Label2: TLabel
            Left = 12
            Top = 50
            Width = 49
            Height = 13
            Caption = 'Sombra 1:'
          end
          object Label3: TLabel
            Left = 12
            Top = 78
            Width = 49
            Height = 13
            Caption = 'Sombra 2:'
          end
          object SbTitCorC: TSpeedButton
            Left = 84
            Top = 20
            Width = 24
            Height = 24
            Caption = '...'
            OnClick = SbTitCorCClick
          end
          object ShTitCorC: TShape
            Left = 112
            Top = 20
            Width = 24
            Height = 24
            Pen.Style = psClear
            Pen.Width = 0
            OnMouseActivate = ShTitCorCMouseActivate
          end
          object LaTitCorC: TLabel
            Left = 140
            Top = 24
            Width = 47
            Height = 13
            AutoSize = False
            Caption = '[Nenhum]'
          end
          object SbTitCorA: TSpeedButton
            Left = 84
            Top = 48
            Width = 24
            Height = 24
            Caption = '...'
            OnClick = SbTitCorAClick
          end
          object ShTitCorA: TShape
            Left = 112
            Top = 48
            Width = 24
            Height = 24
            Pen.Style = psClear
            Pen.Width = 0
            OnMouseActivate = ShTitCorAMouseActivate
          end
          object LaTitCorA: TLabel
            Left = 140
            Top = 50
            Width = 47
            Height = 13
            Caption = '[Nenhum]'
          end
          object SbTitCorB: TSpeedButton
            Left = 84
            Top = 76
            Width = 24
            Height = 24
            Caption = '...'
            OnClick = SbTitCorBClick
          end
          object ShTitCorB: TShape
            Left = 112
            Top = 76
            Width = 24
            Height = 24
            Pen.Style = psClear
            Pen.Width = 0
            OnMouseActivate = ShTitCorBMouseActivate
          end
          object LaTitCorB: TLabel
            Left = 140
            Top = 78
            Width = 47
            Height = 13
            Caption = '[Nenhum]'
          end
        end
        object GroupBox6: TGroupBox
          Left = 2
          Top = 126
          Width = 315
          Height = 83
          Align = alClient
          Caption = ' Posi'#231#227'o relativa do t'#237'tulo: '
          TabOrder = 1
          object Label7: TLabel
            Left = 12
            Top = 42
            Width = 49
            Height = 13
            Caption = 'Sombra 1:'
          end
          object Label8: TLabel
            Left = 12
            Top = 70
            Width = 49
            Height = 13
            Caption = 'Sombra 2:'
          end
          object Label6: TLabel
            Left = 84
            Top = 22
            Width = 10
            Height = 13
            Caption = 'X:'
          end
          object Label10: TLabel
            Left = 124
            Top = 22
            Width = 10
            Height = 13
            Caption = 'Y:'
          end
          object EdCorPosAX: TdmkEdit
            Left = 84
            Top = 40
            Width = 37
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '0'
            ValWarn = False
            OnExit = EdCorPosAXExit
          end
          object EdCorPosBX: TdmkEdit
            Left = 84
            Top = 64
            Width = 37
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '0'
            ValWarn = False
            OnExit = EdCorPosBXExit
          end
          object EdCorPosBY: TdmkEdit
            Left = 124
            Top = 64
            Width = 37
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '0'
            ValWarn = False
            OnExit = EdCorPosBYExit
          end
          object EdCorPosAY: TdmkEdit
            Left = 124
            Top = 40
            Width = 37
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '0'
            ValWarn = False
            OnExit = EdCorPosAYExit
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 319
        Top = 0
        Width = 321
        Height = 211
        Align = alClient
        Caption = ' Cores das linhas de avisos: '
        TabOrder = 1
        object Label4: TLabel
          Left = 12
          Top = 24
          Width = 30
          Height = 13
          Caption = 'Aviso:'
        end
        object Label5: TLabel
          Left = 12
          Top = 50
          Width = 40
          Height = 13
          Caption = 'Sombra:'
        end
        object ShAvisoCorA: TShape
          Left = 112
          Top = 48
          Width = 24
          Height = 24
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseActivate = ShTitCorCMouseActivate
        end
        object ShAvisoCorC: TShape
          Left = 112
          Top = 20
          Width = 24
          Height = 24
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseActivate = ShTitCorAMouseActivate
        end
        object LaAvisoCorA: TLabel
          Left = 140
          Top = 52
          Width = 47
          Height = 13
          Caption = '[Nenhum]'
        end
        object LaAvisoCorC: TLabel
          Left = 140
          Top = 22
          Width = 47
          Height = 13
          Caption = '[Nenhum]'
        end
        object SbAvisoCorA: TSpeedButton
          Left = 84
          Top = 48
          Width = 24
          Height = 24
          Caption = '...'
          OnClick = SbAvisoCorAClick
        end
        object SbAvisoCorC: TSpeedButton
          Left = 84
          Top = 20
          Width = 24
          Height = 24
          Caption = '...'
          OnClick = SbAvisoCorCClick
        end
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 459
    Width = 640
    Height = 57
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 589
        Height = 32
        Caption = 
          'Aten'#231#227'o: Caso as cores n'#227'o ficarem corretas ao selecionar um nov' +
          'o skin, clique em "N'#227'o quero usar"'#13#10'e depois clique novamente no' +
          ' "Qual usa" desejado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 589
        Height = 32
        Caption = 
          'Aten'#231#227'o: Caso as cores n'#227'o ficarem corretas ao selecionar um nov' +
          'o skin, clique em "N'#227'o quero usar"'#13#10'e depois clique novamente no' +
          ' "Qual usa" desejado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
end
