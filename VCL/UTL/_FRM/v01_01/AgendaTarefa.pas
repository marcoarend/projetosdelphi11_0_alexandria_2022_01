unit AgendaTarefa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.Buttons, Vcl.StdCtrls, dmkEdit, ShellApi;

type
  TFmAgendaTarefa = class(TForm)
    Label25: TLabel;
    Label26: TLabel;
    EdSTUserName: TdmkEdit;
    EdSTUserPwd: TdmkEdit;
    SbUserPwd: TSpeedButton;
    BtOK: TBitBtn;
    procedure SbUserPwdMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SbUserPwdMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    function  ScheduleRunAtStartup(): Boolean;
  public
    { Public declarations }
  end;

var
  FmAgendaTarefa: TFmAgendaTarefa;

implementation

uses
{$IfDef uDmkPFMin}UnMinimumDmkProcFunc;
{$Else} UnDmkProcFunc;
{$EndIf}

//UnMinimumMyObjects,
//UnMyProcesses;

{$R *.dfm}

procedure TFmAgendaTarefa.BtOKClick(Sender: TObject);
var
  Usr, Pwd: String;
begin
{
  Usr := EdSTUserName.Text;
  Pwd := EdSTUserPwd.Text;
  //
  if MyObjects.FIC(Trim(Usr) = EmptyStr, EdSTUserName,
  'Informe o nome do usu�rio com direitos administrativos') then Exit;
  //
  if MyObjects.FIC(Trim(Pwd) = EmptyStr, EdSTUserName,
  'Informe a senha do usu�rio com direitos administrativos') then Exit;
  //
  if MyProcesses.ConnectAs(Usr, Pwd) then
}
    if ScheduleRunAtStartup() then
      Close;
end;

procedure TFmAgendaTarefa.SbUserPwdMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  EdSTUserPwd.PasswordChar := #0;
  EdSTUserPwd.Font.Charset := DEFAULT_CHARSET;
end;

procedure TFmAgendaTarefa.SbUserPwdMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  EdSTUserPwd.PasswordChar := 'l';
  EdSTUserPwd.Font.Charset := SYMBOL_CHARSET;
end;

function TFmAgendaTarefa.ScheduleRunAtStartup(): Boolean;
var
  ATaskName, AFileName, AUserAccount, AUserPassword: string;
  Retorno: Integer;
begin
  Result        := False;
  //
  ATaskName     := Application.Name + ' as admin'; // VAR_ATaskName
  AFileName     := Application.ExeName;
  AUserAccount  := EdSTUserName.Text;
  AUserPassword := EdSTUserPwd.Text;
  //
  DmkPF.ScheduleRunAtStartup(ATaskName, AFileName, AUserAccount, AUserPassword);
  //DmkPF.UnSheduleRunAtStatup(ATaskName);

  Retorno := ShellExecute(0, nil, 'schtasks', PChar('/query /tn "' + ATaskName + '"'),
    nil, SW_HIDE);
  if Retorno <= 32 then
    ShowMessage(SysErrorMessage(Retorno))
  else
  begin
    Result := True;
    ShowMessage('"' + Application.Name + '" ativado!');
  end;
end;

end.
