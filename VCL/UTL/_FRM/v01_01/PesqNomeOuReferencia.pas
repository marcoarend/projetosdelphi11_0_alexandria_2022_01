unit PesqNomeOuReferencia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBGridZTO, mySQLDbTables;

type
  TFmPesqNomeOuReferencia = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPesquisa: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    Label1: TLabel;
    EdPesqNome: TdmkEdit;
    LaDoc: TLabel;
    EdPesqRefe: TdmkEdit;
    BtSeleciona: TBitBtn;
    DBGPesquisa: TdmkDBGridZTO;
    QrPesq: TMySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    QrPesqReferencia: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure EdPesqNomeChange(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure EdPesqRefeChange(Sender: TObject);
    procedure DBGPesquisaDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisa(Forca: Boolean);
    procedure Seleciona();
  public
    { Public declarations }
    FNomeTabela, FNomeFldNome, FNomeFldRefe, FNomeFldCodi: String;
    FValorNome, FValorRefe: String;
    FValorCodi: Integer;
    FSelecionou: Boolean;
  end;

  var
  FmPesqNomeOuReferencia: TFmPesqNomeOuReferencia;

implementation

uses UnMyObjects, DmkDAC_PF,
  Module;

{$R *.DFM}

procedure TFmPesqNomeOuReferencia.BtPesquisaClick(Sender: TObject);
begin
  Pesquisa(True);
end;

procedure TFmPesqNomeOuReferencia.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqNomeOuReferencia.BtSelecionaClick(Sender: TObject);
begin
  Seleciona();
end;

procedure TFmPesqNomeOuReferencia.DBGPesquisaDblClick(Sender: TObject);
begin
  Seleciona();
end;

procedure TFmPesqNomeOuReferencia.EdPesqNomeChange(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmPesqNomeOuReferencia.EdPesqRefeChange(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmPesqNomeOuReferencia.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPesqNomeOuReferencia.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FSelecionou  := True;
  //
  FNomeTabela  := '';
  FNomeFldNome := 'Nome';
  FNomeFldRefe := 'Referencia';
  FNomeFldCodi := 'Codigo';
  //
  FValorNome   := '';
  FValorRefe   := '';
  FValorCodi   := 0;
  //
end;

procedure TFmPesqNomeOuReferencia.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesqNomeOuReferencia.Pesquisa(Forca: Boolean);
var
  sNome, sRefe, SQL_Text, Masc: String;
  NomeTabela, NomeFldNome, NomeFldRefe, NomeFldCodi: String;
begin
  QrPesq.Close;
  //
  sNome := EdPesqNome.ValueVariant;
  sRefe := EdPesqRefe.ValueVariant;
  //
  if sNome <> '' then
    sNome := StringReplace(sNome, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
  if sRefe <> '' then
    sRefe := StringReplace(sRefe, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
  //
  if Forca
  or ((Length(sNome) >= 2) or (Length(sRefe) > 3)) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
    'SELECT ' + FNomeFldCodi + ' Codigo, ',
    FNomeFldNome + ' Nome, ',
    FNomeFldRefe + ' Referencia ',
    'FROM ' + FNomeTabela,
    'WHERE ' + FNomeFldNome + ' LIKE "%' + sNome + '%"',
    'AND ' + FNomeFldRefe + ' LIKE "%' + sRefe + '%"',
    EmptyStr]);
  end;
end;

procedure TFmPesqNomeOuReferencia.Seleciona();
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    FSelecionou := True;
    FValorNome   := QrPesqNome.Value;
    FValorRefe   := QrPesqReferencia.Value;
    FValorCodi   := QrPesqCodigo.Value;
    //
    Close;
  end;
end;

end.
