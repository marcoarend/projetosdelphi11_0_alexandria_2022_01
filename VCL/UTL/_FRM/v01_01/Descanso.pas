unit Descanso;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, dmkGeral, UnInternalConsts, StdCtrls, Vcl.Buttons,
  Vcl.Imaging.pngimage {$IfNDef SNoti}, UnitNotificacoes {$EndIf};

type
  TFmDescanso = class(TForm)
    ImgPrincipal: TImage;
    TmNotifi: TTimer;
    PnNotifi: TPanel;
    ImgNotifi: TImage;
    LaNotifi: TLabel;
    BtNotifi: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BtNotifiClick(Sender: TObject);
    procedure TmNotifiTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmDescanso: TFmDescanso;

implementation

uses
  UnDmkProcFunc, Module, Principal, UnMyObjects, MyGlyfs;

{$R *.dfm}

procedure TFmDescanso.BtNotifiClick(Sender: TObject);
begin
  {$IfNDef SNoti}
  UnNotificacoes.MostraFmNotificacoes(FmPrincipal.PageControl1,
    FmPrincipal.PageControl1, PnNotifi, TmNotifi);
  {$EndIf}
end;

procedure TFmDescanso.FormActivate(Sender: TObject);
begin
  if TFmDescanso(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmDescanso.FormCreate(Sender: TObject);
var
  Imagem(*, Avisa*): String;
begin
  Imagem := Geral.ReadAppKey('ImagemFundo', Application.Title, ktString,
    VAR_APP + 'Fundo.jpg', HKEY_LOCAL_MACHINE);
  if FileExists(Imagem) then
    ImgPrincipal.Picture.LoadFromFile(Imagem);
  //
  {$IfNDef SNoti}
  UnNotificacoes.ConfiguracoesIniciaisNotifi(TmNotifi, Dmod.QrNotifi,
    PnNotifi, ImgNotifi);
  {$Else}
    PnNotifi.Visible := False;
    TmNotifi.Enabled := False;
  {$EndIf}
end;

procedure TFmDescanso.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmDescanso.TmNotifiTimer(Sender: TObject);
begin
  {$IfNDef SNoti}
  UnNotificacoes.AtualizaNotifi(Dmod.QrNotifi, PnNotifi, TmNotifi);
  {$EndIf}
end;

end.
