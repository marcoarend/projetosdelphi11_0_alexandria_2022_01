unit CfgAtributos;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, Variants, DB,
  dmkGeral, mySQLDbTables,(* DbTables,*) mySQLExceptions, UndmkProcFunc, dmkEdit,
  dmkEditCB, dmkDBLookupComboBox, DmkDAC_PF, UnDmkEnums;

type
  TTipoTab = (tiptbNON, tiptbCAB, tiptbITS(*, tiptbTXT*));
  TUnCfgAtributos = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure DefineTextoItem(const Tabela: String;
              const Codigo: Integer; const EdTxt: TdmkEdit);
    procedure InsAltAtrDef_CAB(TabCad, TabIts, TabTxt: String;
              EdCad, EdIts, EdTxt: TdmkEdit; CBCad, CBIts: TdmkDBLookupCombobox;
              QrCad, QrIts, QrTxt: TmySQLQuery);
    procedure InsAltAtrDef_ITS(ID_Sorc, ID_Item: Integer; TabDef, TabCad,
              TabIts, TabTxt: String; SQLType: TSQLType; AtrCad, AtrIts: Integer;
              AtrTxt: String; QryDef, QryTxt: TmySQLQuery;
              PermiteSelecionarAtrTyp: Boolean);
    procedure CadastraAtrCad(Qual: TTipoTab; NomeTabCad, NomeTabIts, NomeTabTxt:
              String; QrAtrCad, QrAtrIts, QrCad, QrIts: TMySQLQuery; EdAtrCad,
              EdAtrIts: TdmkEditCB; CBAtrCad, CBAtrIts: TdmkDBLookupComboBox;
              PermiteSelecionarAtrTyp: Boolean; Codigo: Integer = 0);

  end;

var
  UnCfgAtributos: TUnCfgAtributos;

implementation

uses MyDBCheck, UMySQLModule, AtrDef, AtrCad, Module;

{ TUnCfgAtributos }

procedure TUnCfgAtributos.CadastraAtrCad(Qual: TTipoTab; NomeTabCad, NomeTabIts,
  NomeTabTxt: String; QrAtrCad, QrAtrIts, QrCad, QrIts: TMySQLQuery; EdAtrCad,
  EdAtrIts: TdmkEditCB; CBAtrCad, CBAtrIts: TdmkDBLookupComboBox;
  PermiteSelecionarAtrTyp: Boolean; Codigo: Integer = 0);
begin
  VAR_CADASTRO := 0;
  VAR_CAD_ITEM := 0;
  if DBCheck.CriaFm(TFmAtrCad, FmAtrCad, afmoNegarComAviso) then
  begin
    FmAtrCad.FNomeTabCad := Lowercase(NomeTabCad);
    FmAtrCad.FNomeTabIts := Lowercase(NomeTabIts);
    FmAtrCad.FNomeTabTxt := NomeTabTxt;
    FmAtrCad.FPermiteSelecionarAtrTyp := PermiteSelecionarAtrTyp;
    //
    if Codigo <> 0 then
      FmAtrCad.LocCod(Codigo, Codigo);
    //
    FmAtrCad.ShowModal;
    FmAtrCad.Destroy;
    //
    if Qual = tiptbCAB then
    begin
      if VAR_CADASTRO <> 0 then
      begin
        if QrAtrCad <> nil then
        begin
          QrAtrCad.Close;
          UnDmkDAC_PF.AbreQuery(QrAtrCad, Dmod.MyDB);
        end;
        //
        if EdAtrCad <> nil then
          EdAtrCad.ValueVariant := VAR_CADASTRO;
        if CBAtrCad <> nil then
          CBAtrCad.KeyValue     := VAR_CADASTRO;
        if QrCad <> nil then
        begin
          QrCad.Close;
          UnDmkDAC_PF.AbreQuery(QrCad, Dmod.MyDB);
        end;
      end;
    end else
    if Qual = tiptbITS then
    begin
      if VAR_CAD_ITEM <> 0 then
      begin
        if QrAtrIts <> nil then
        begin
          QrAtrIts.Close;
          UnDmkDAC_PF.AbreQuery(QrAtrIts, Dmod.MyDB);
        end;
        //
        if EdAtrIts <> nil then
          EdAtrIts.ValueVariant := VAR_CAD_ITEM;
        if CBAtrIts <> nil then
          CBAtrIts.KeyValue     := VAR_CAD_ITEM;
        if QrIts <> nil then
        begin
          QrIts.Close;
          UnDmkDAC_PF.AbreQuery(QrIts, Dmod.MyDB);
        end;
      end;
    end;
  end;
end;

procedure TUnCfgAtributos.DefineTextoItem(const Tabela: String;
const Codigo: Integer; const EdTxt: TdmkEdit);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  Qry.Database := Dmod.MyDB;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Nome ',
  'FROM ' + Tabela,
  'WHERE ',
  'Codigo=' + Geral.FF0(Codigo),
  '']);
  EdTxt.Text := Qry.FieldByName('Nome').AsString;
  Qry.Close;
  Qry.Free;
end;

procedure TUnCfgAtributos.InsAltAtrDef_CAB(TabCad, TabIts, TabTxt: String;
  EdCad, EdIts, EdTxt: TdmkEdit; CBCad, CBIts: TdmkDBLookupCombobox;
  QrCad, QrIts, QrTxt: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmAtrDef, FmAtrDef, afmoNegarComAviso) then
  begin
    FmAtrDef.ImgTipo.SQLType := stCpy;
    FmAtrDef.FQryDef := nil;//QryDef;
    FmAtrDef.EdID_Sorc.ValueVariant := 0;//ID_Sorc;
    FmAtrDef.AbreAtrCad('', TabCad, TabIts, TabTxt, tiptbCAB);
    //if SQLType = stUpd then
    //begin
      FmAtrDef.EdID_Item.ValueVariant := 0;//ID_Item;
      //
      FmAtrDef.VUAtrCad.ValueVariant := EdCad.ValueVariant;
      //
      FmAtrDef.VUAtrIts.ValueVariant := EdIts.ValueVariant;
      //
    ///end;
    //
    FmAtrDef.FEdCad := EdCad;
    FmAtrDef.FEdIts := EdIts;
    FmAtrDef.FEdIts := EdTxt;
    FmAtrDef.FCBCad := CBCad;
    FmAtrDef.FCBIts := CBIts;
    FmAtrDef.FQrCad := QrCad;
    FmAtrDef.FQrIts := QrIts;
    FmAtrDef.FQrIts := QrTxt;
    FmAtrDef.ShowModal;
    FmAtrDef.Destroy;
  end;
end;

procedure TUnCfgAtributos.InsAltAtrDef_ITS(ID_Sorc, ID_Item: Integer; TabDef,
  TabCad, TabIts, TabTxt: String; SQLType: TSQLType; AtrCad, AtrIts: Integer;
  AtrTxt: String; QryDef, QryTxt: TmySQLQuery; PermiteSelecionarAtrTyp: Boolean);
begin
  if DBCheck.CriaFm(TFmAtrDef, FmAtrDef, afmoNegarComAviso) then
  begin
    FmAtrDef.ImgTipo.SQLType := SQLType;
    FmAtrDef.FPermiteSelecionarAtrTyp := PermiteSelecionarAtrTyp;
    FmAtrDef.FQryDef := QryDef;
    FmAtrDef.FQryTxt := QryTxt;
    FmAtrDef.EdID_Sorc.ValueVariant := ID_Sorc;
    FmAtrDef.AbreAtrCad(TabDef, TabCad, TabIts, TabTxt, tiptbITS);
    if SQLType = stUpd then
    begin
      FmAtrDef.EdID_Item.ValueVariant := ID_Item;
      //
{
      FmAtrDef.EdAtrCad.ValueVariant  := AtrCad;
      FmAtrDef.CBAtrCad.KeyValue      := AtrCad;
}
      FmAtrDef.VUAtrCad.ValueVariant := AtrCad;
      //
{
      FmAtrDef.EdAtrIts.ValueVariant  := AtrIts;
      FmAtrDef.CBAtrIts.KeyValue      := AtrIts;
}
      FmAtrDef.VUAtrIts.ValueVariant := AtrIts;
      FmAtrDef.EdAtrTxt.Text := AtrTxt;
      //
    end;
    //
    FmAtrDef.ShowModal;
    FmAtrDef.Destroy;
  end;
end;

end.

