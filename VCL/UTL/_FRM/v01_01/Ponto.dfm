object FmPonto: TFmPonto
  Left = 287
  Top = 321
  Caption = 'Cart'#227'o Ponto'
  ClientHeight = 395
  ClientWidth = 439
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 439
    Height = 233
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitTop = 40
    ExplicitHeight = 162
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 439
      Height = 109
      Align = alTop
      Caption = ' Motivo: '
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 437
      object RadioButton1: TRadioButton
        Left = 20
        Top = 20
        Width = 113
        Height = 17
        Caption = 'Entrada Turno'
        Enabled = False
        TabOrder = 0
      end
      object RadioButton2: TRadioButton
        Left = 20
        Top = 40
        Width = 113
        Height = 17
        Caption = 'Sa'#237'da descanso'
        Enabled = False
        TabOrder = 1
      end
      object RadioButton3: TRadioButton
        Left = 20
        Top = 60
        Width = 113
        Height = 17
        Caption = 'Volta descanso'
        Enabled = False
        TabOrder = 2
      end
      object RadioButton4: TRadioButton
        Left = 20
        Top = 80
        Width = 113
        Height = 17
        Caption = 'Sa'#237'da turno'
        Enabled = False
        TabOrder = 3
      end
      object RadioButton5: TRadioButton
        Left = 140
        Top = 20
        Width = 113
        Height = 17
        Caption = 'Entrada extra 1'
        Enabled = False
        TabOrder = 4
      end
      object RadioButton6: TRadioButton
        Left = 140
        Top = 40
        Width = 113
        Height = 17
        Caption = 'Sa'#237'da extra 1'
        Enabled = False
        TabOrder = 5
      end
      object RadioButton7: TRadioButton
        Left = 140
        Top = 60
        Width = 113
        Height = 17
        Caption = 'Entrada extra 2'
        Enabled = False
        TabOrder = 6
      end
      object RadioButton8: TRadioButton
        Left = 140
        Top = 80
        Width = 113
        Height = 17
        Caption = 'Sa'#237'da extra 2'
        Enabled = False
        TabOrder = 7
      end
      object RadioButton9: TRadioButton
        Left = 260
        Top = 20
        Width = 113
        Height = 17
        Caption = 'Entrada extra 3'
        Enabled = False
        TabOrder = 8
      end
      object RadioButton10: TRadioButton
        Left = 260
        Top = 40
        Width = 113
        Height = 17
        Caption = 'Sa'#237'da extra 3'
        Enabled = False
        TabOrder = 9
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 109
      Width = 439
      Height = 124
      Align = alClient
      DataSource = DsPonto
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'TraIniTXT'
          Title.Alignment = taCenter
          Title.Caption = 'Entra'
          Width = 40
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DesIniTXT'
          Title.Alignment = taCenter
          Title.Caption = 'Desc.I'
          Width = 40
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DesFimTXT'
          Title.Alignment = taCenter
          Title.Caption = 'Desc.F'
          Width = 40
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'TraFimTXT'
          Title.Alignment = taCenter
          Title.Caption = 'Sa'#237'da'
          Width = 40
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Ex1IniTXT'
          Title.Alignment = taCenter
          Title.Caption = 'Extra1I'
          Width = 40
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Ex1FimTXT'
          Title.Alignment = taCenter
          Title.Caption = 'Extra1F'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Ex2IniTXT'
          Title.Alignment = taCenter
          Title.Caption = 'Extra2I'
          Width = 40
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Ex2FimTXT'
          Title.Alignment = taCenter
          Title.Caption = 'Extra2F'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Ex3IniTXT'
          Title.Alignment = taCenter
          Title.Caption = 'Extra3I'
          Width = 40
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Ex3FimTXT'
          Title.Alignment = taCenter
          Title.Caption = 'Extra3F'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 439
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = -345
    ExplicitWidth = 784
    object GB_R: TGroupBox
      Left = 391
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 343
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 688
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 159
        Height = 32
        Caption = 'Cart'#227'o Ponto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 159
        Height = 32
        Caption = 'Cart'#227'o Ponto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 159
        Height = 32
        Caption = 'Cart'#227'o Ponto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 281
    Width = 439
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitLeft = -345
    ExplicitTop = 243
    ExplicitWidth = 784
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 435
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 780
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 325
    Width = 439
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = -345
    ExplicitTop = 217
    ExplicitWidth = 784
    object PnSaiDesis: TPanel
      Left = 293
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 638
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 291
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 636
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
    end
  end
  object QrPonto: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPontoCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM ponto'
      'WHERE Funci=:P0'
      'AND Data=:P1')
    Left = 172
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPontoFunci: TIntegerField
      FieldName = 'Funci'
    end
    object QrPontoData: TDateField
      FieldName = 'Data'
    end
    object QrPontoTraIni: TWideStringField
      FieldName = 'TraIni'
      Size = 4
    end
    object QrPontoDesIni: TWideStringField
      FieldName = 'DesIni'
      Size = 4
    end
    object QrPontoDesFim: TWideStringField
      FieldName = 'DesFim'
      Size = 4
    end
    object QrPontoTraFim: TWideStringField
      FieldName = 'TraFim'
      Size = 4
    end
    object QrPontoEx1Ini: TWideStringField
      FieldName = 'Ex1Ini'
      Size = 4
    end
    object QrPontoEx1Fim: TWideStringField
      FieldName = 'Ex1Fim'
      Size = 4
    end
    object QrPontoEx2Ini: TWideStringField
      FieldName = 'Ex2Ini'
      Size = 4
    end
    object QrPontoEx2Fim: TWideStringField
      FieldName = 'Ex2Fim'
      Size = 4
    end
    object QrPontoEx3Ini: TWideStringField
      FieldName = 'Ex3Ini'
      Size = 4
    end
    object QrPontoEx3Fim: TWideStringField
      FieldName = 'Ex3Fim'
      Size = 4
    end
    object QrPontoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPontoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPontoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPontoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPontoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPontoNormal: TIntegerField
      FieldName = 'Normal'
    end
    object QrPontoExtra: TIntegerField
      FieldName = 'Extra'
    end
    object QrPontoTraIniTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TraIniTXT'
      Size = 5
      Calculated = True
    end
    object QrPontoDesIniTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DesIniTXT'
      Size = 5
      Calculated = True
    end
    object QrPontoDesFimTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DesFimTXT'
      Size = 5
      Calculated = True
    end
    object QrPontoTraFimTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TraFimTXT'
      Size = 5
      Calculated = True
    end
    object QrPontoEx1IniTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Ex1IniTXT'
      Size = 5
      Calculated = True
    end
    object QrPontoEx1FimTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Ex1FimTXT'
      Size = 5
      Calculated = True
    end
    object QrPontoEx2IniTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Ex2IniTXT'
      Size = 5
      Calculated = True
    end
    object QrPontoEx2FimTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Ex2FimTXT'
      Size = 5
      Calculated = True
    end
    object QrPontoEx3IniTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Ex3IniTXT'
      Size = 5
      Calculated = True
    end
    object QrPontoEx3FimTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Ex3FimTXT'
      Size = 5
      Calculated = True
    end
  end
  object DsPonto: TDataSource
    DataSet = QrPonto
    Left = 200
    Top = 208
  end
end
