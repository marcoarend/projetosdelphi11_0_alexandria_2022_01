unit Sel2Cod;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkGeral, dmkImage, Variants, UnDmkEnums;

type
  TFmSel2Cod = class(TForm)
    PainelDados: TPanel;
    LaPrompt1: TLabel;
    CBSel1: TdmkDBLookupComboBox;
    DsSel1: TDataSource;
    EdSel1: TdmkEditCB;
    LaOrdem1: TLabel;
    QrSel1: TMySQLQuery;
    QrSel1Descricao: TWideStringField;
    QrSel1Codigo: TIntegerField;
    dmkEdOrdem1: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    SBCadastro1: TSpeedButton;
    CkContinua: TCheckBox;
    LaPrompt2: TLabel;
    EdSel2: TdmkEditCB;
    CBSel2: TdmkDBLookupComboBox;
    SBCadastro2: TSpeedButton;
    dmkEdOrdem2: TdmkEdit;
    LaOrdem: TLabel;
    QrSel2: TMySQLQuery;
    QrSel2Descricao: TWideStringField;
    QrSel2Codigo: TIntegerField;
    DsSel2: TDataSource;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure SBCadastro1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FSelected, FContinua: Boolean;
    FIDThis, FTabela: String;
    FNovoCodigo: TNovoCodigo;
    FPermiteSelZero: Boolean;
    //
    procedure Informa(Texto: String);
  end;

var
  FmSel2Cod: TFmSel2Cod;

implementation

uses {$IfNDef SemCfgCadLista} CfgCadLista,{$EndIf}
  UnMyObjects, Module;

{$R *.DFM}

procedure TFmSel2Cod.BtOKClick(Sender: TObject);
begin
  VAR_SELCOD := Geral.IMV(EdSel1.Text);
  VAR_SELNOM := CBSel1.Text;
  VAR_SELCO2 := Geral.IMV(EdSel2.Text);
  VAR_SELNO2 := CBSel2.Text;
  if (VAR_SELCOD = 0) and (FPermiteSelZero = False) then
  begin
    VAR_SELNOM := '';
    Geral.MB_Erro('Nenhuma escolha foi realizada!');
    Exit;
  end;
  if (VAR_SELCO2 = 0) and (FPermiteSelZero = False) then
  begin
    VAR_SELNO2 := '';
    Geral.MB_Erro('Nenhuma escolha foi realizada!');
    Exit;
  end;
  FSelected  := True;
  FContinua  := CkContinua.Checked;
  Close;
end;

procedure TFmSel2Cod.BtSaidaClick(Sender: TObject);
begin
  VAR_SELCOD := 0;
  Close;
end;

procedure TFmSel2Cod.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSel2Cod.Informa(Texto: String);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
end;

procedure TFmSel2Cod.SBCadastro1Click(Sender: TObject);
begin
{$IfNDef SemCfgCadLista}
  VAR_CADASTRO := 0;
  //
  if FTabela = EmptyStr then
  begin
    Geral.MB_Erro('Tabela n�o definida para cadastro! ' + sLineBreak +
    'SBCadastroClick > UnCfgCadLista.MostraCadLista()');
    Exit;
  end;
  if FNovoCodigo = TNovoCodigo.ncIdefinido then
  begin
    Geral.MB_Erro('Tipo de obten��o de novo c�digo indefinido!' + sLineBreak +
    'SBCadastroClick > UnCfgCadLista.MostraCadLista()');
    Exit;
  end;
  //
  UnCfgCadLista.MostraCadLista(QrSel1.Database, FTabela, QrSel1Descricao.Size,
    FNovoCodigo, 'Cadastro de "' +
    Geral.Substitui(LaPrompt1.Caption, ':', #0) + '"',
    [], False, Null, [], [], False, EdSel1.ValueVariant);
  //
  UMyMod.SetaCodigoPesquisado(EdSel1, CBSel1, QrSel1, VAR_CADASTRO);
  EdSel1.SetFocus;
{$EndIf}
end;

procedure TFmSel2Cod.FormActivate(Sender: TObject);
var
  IDForm: String;
begin
  MyObjects.CorIniComponente();
  IDForm := Copy(Caption, 1, 13);
  if Uppercase(IDForm) <> FIDThis then
  begin
    Caption := FIDThis + ' :: ' + Caption;
    MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  end;
end;

procedure TFmSel2Cod.FormCreate(Sender: TObject);
begin
  FIDThis := 'XXX-XXXXX-002';
  FPermiteSelZero := False;
  ImgTipo.SQLType := stPsq;
  //
  FSelected := False;
  FContinua := False;
  VAR_SELCOD := 0;
end;

end.
