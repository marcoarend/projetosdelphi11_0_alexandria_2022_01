object FmPontoFunci: TFmPontoFunci
  Left = 287
  Top = 321
  Caption = 'Funcionario'
  ClientHeight = 320
  ClientWidth = 850
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 850
    Height = 121
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 10
      Width = 73
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Funcion'#225'rio:'
    end
    object Label2: TLabel
      Left = 556
      Top = 64
      Width = 136
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Senha de permanente:'
    end
    object Label3: TLabel
      Left = 414
      Top = 64
      Width = 36
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Login:'
    end
    object Label5: TLabel
      Left = 20
      Top = 64
      Width = 73
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data e hora:'
    end
    object Label4: TLabel
      Left = 699
      Top = 64
      Width = 138
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Senha de atendimento:'
    end
    object EdFuncionario: TdmkEditCB
      Left = 20
      Top = 30
      Width = 80
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      DBLookupComboBox = CbFuncionario
      IgnoraDBLookupComboBox = False
    end
    object CbFuncionario: TdmkDBLookupComboBox
      Left = 103
      Top = 30
      Width = 735
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsFuncionarios
      TabOrder = 1
      dmkEditCB = EdFuncionario
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdLogin: TEdit
      Left = 414
      Top = 84
      Width = 137
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      PasswordChar = '*'
      TabOrder = 3
    end
    object EdSenhaP: TEdit
      Left = 556
      Top = 84
      Width = 138
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      PasswordChar = '*'
      TabOrder = 4
    end
    object TPData: TDateTimePicker
      Left = 20
      Top = 84
      Width = 286
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 38244.853556585700000000
      Time = 38244.853556585700000000
      DateFormat = dfLong
      Enabled = False
      TabOrder = 2
    end
    object EdSenhaA: TEdit
      Left = 699
      Top = 84
      Width = 138
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      PasswordChar = '*'
      TabOrder = 5
    end
    object TPHora: TDateTimePicker
      Left = 310
      Top = 84
      Width = 100
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 38244.853556585700000000
      Time = 38244.853556585700000000
      Enabled = False
      Kind = dtkTime
      TabOrder = 6
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 850
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 791
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 732
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 167
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Funcion'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 167
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Funcion'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 167
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Funcion'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 180
    Width = 850
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 846
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 234
    Width = 850
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 671
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 18
      Width = 669
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrFuncionarios: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT se.Login, se.Senha, se.SenhaDia, '
      'en.Codigo, en.Nome'
      'FROM entidades en, Senhas se'
      'WHERE se.Funcionario=en.Codigo '
      'AND en.Cliente2="V"'
      'AND se.Ativo="V"'
      'ORDER BY en.Nome')
    Left = 216
    Top = 60
    object QrFuncionariosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFuncionariosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrFuncionariosLogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrFuncionariosSenha: TWideStringField
      FieldName = 'Senha'
      Size = 30
    end
    object QrFuncionariosSenhaDia: TWideStringField
      FieldName = 'SenhaDia'
      Size = 30
    end
  end
  object DsFuncionarios: TDataSource
    DataSet = QrFuncionarios
    Left = 244
    Top = 60
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ponto'
      'WHERE Funci=:P0'
      'AND Data=:P1')
    Left = 352
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocFunci: TIntegerField
      FieldName = 'Funci'
    end
    object QrLocData: TDateField
      FieldName = 'Data'
    end
    object QrLocTraIni: TWideStringField
      FieldName = 'TraIni'
      Size = 4
    end
    object QrLocDesIni: TWideStringField
      FieldName = 'DesIni'
      Size = 4
    end
    object QrLocDesFim: TWideStringField
      FieldName = 'DesFim'
      Size = 4
    end
    object QrLocTraFim: TWideStringField
      FieldName = 'TraFim'
      Size = 4
    end
    object QrLocEx1Ini: TWideStringField
      FieldName = 'Ex1Ini'
      Size = 4
    end
    object QrLocEx1Fim: TWideStringField
      FieldName = 'Ex1Fim'
      Size = 4
    end
    object QrLocEx2Ini: TWideStringField
      FieldName = 'Ex2Ini'
      Size = 4
    end
    object QrLocEx2Fim: TWideStringField
      FieldName = 'Ex2Fim'
      Size = 4
    end
    object QrLocEx3Ini: TWideStringField
      FieldName = 'Ex3Ini'
      Size = 4
    end
    object QrLocEx3Fim: TWideStringField
      FieldName = 'Ex3Fim'
      Size = 4
    end
    object QrLocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLocNormal: TIntegerField
      FieldName = 'Normal'
    end
    object QrLocExtra: TIntegerField
      FieldName = 'Extra'
    end
  end
  object DsLoc: TDataSource
    DataSet = QrLoc
    Left = 380
    Top = 56
  end
end
