unit LinkRankSkin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  ComCtrls, dmkEditDateTimePicker, UnInternalConsts, dmkImage, dmkMemoBar,
{$IfDef cSkinRank}
 WinSkinData, WinSkinStore,
{$EndIf}
  UnDmkProcFunc, UnInternalConsts2;

type
  TFmLinkRankSkin = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GroupBox1: TGroupBox;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdArqLinkRankSkin: TdmkEdit;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    SBNadador: TSpeedButton;
    RGQualUsaLinkRankSkin: TRadioGroup;
    RGComoUsaLinkRankSkin: TRadioGroup;
    Panel3: TPanel;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    ShAvisoCorA: TShape;
    ShAvisoCorC: TShape;
    LaAvisoCorA: TLabel;
    LaAvisoCorC: TLabel;
    SbAvisoCorA: TSpeedButton;
    SbAvisoCorC: TSpeedButton;
    GroupBox5: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    SbTitCorC: TSpeedButton;
    ShTitCorC: TShape;
    LaTitCorC: TLabel;
    SbTitCorA: TSpeedButton;
    ShTitCorA: TShape;
    LaTitCorA: TLabel;
    SbTitCorB: TSpeedButton;
    ShTitCorB: TShape;
    LaTitCorB: TLabel;
    GroupBox6: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    EdCorPosAX: TdmkEdit;
    Label6: TLabel;
    EdCorPosBX: TdmkEdit;
    EdCorPosBY: TdmkEdit;
    Label10: TLabel;
    EdCorPosAY: TdmkEdit;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    BtRestaurar: TBitBtn;
    CkSkinAba: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBNadadorClick(Sender: TObject);
    procedure EdArqLinkRankSkinChange(Sender: TObject);
    procedure RGQualUsaLinkRankSkinClick(Sender: TObject);
    procedure RGComoUsaLinkRankSkinClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure SbTitCorCClick(Sender: TObject);
    procedure ShTitCorCMouseActivate(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y, HitTest: Integer;
      var MouseActivate: TMouseActivate);
    procedure SbTitCorAClick(Sender: TObject);
    procedure ShTitCorAMouseActivate(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y, HitTest: Integer;
      var MouseActivate: TMouseActivate);
    procedure SbTitCorBClick(Sender: TObject);
    procedure ShTitCorBMouseActivate(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y, HitTest: Integer;
      var MouseActivate: TMouseActivate);
    procedure SbAvisoCorAClick(Sender: TObject);
    procedure SbAvisoCorCClick(Sender: TObject);
    procedure EdCorPosAXExit(Sender: TObject);
    procedure EdCorPosBXExit(Sender: TObject);
    procedure EdCorPosAYExit(Sender: TObject);
    procedure EdCorPosBYExit(Sender: TObject);
    procedure BtRestaurarClick(Sender: TObject);
    procedure CkSkinAbaClick(Sender: TObject);
  private
    { Private declarations }
    FCam: String;
    FVisivel, FCriado: Boolean;
    //function Nadadores_Gera(): Boolean;
    procedure MostraCores();
{$IfDef cSkinRank}
    function  LinkRankSkinAtiva(SkinData: TSkinData; SkinStore: TSkinStore;
              QualUsa, ComoUsa: Integer): Boolean;
{$EndIf}
    function  SelecionaCor(NomeCor: String): Boolean;
    procedure MostraNomeSkin();
    procedure AtualizaTitulo();
  public
    { Public declarations }
{$IfDef cSkinRank}
    function  LinkRankSkinDefineUso(SkinData: TSkinData;
              SkinStore: TSkinStore): Boolean;
{$EndIf}

  end;

  var
  FmLinkRankSkin: TFmLinkRankSkin;

  const
  //
  ShemeColorName: array[0..19] of String = (
         'csText','csTitleTextActive','csTitleTextNoActive',
         'csButtonFace','csButtonText',
         'csButtonHilight','csButtonlight','csButtonShadow','csButtonDkshadow',
         'csSelectText','csSelectBg','csHilightText','csHilight',
         'csMenuBar','csMenuBarText','csMenuText','csMenubg','csCaption',
         'csScrollbar','csTextDisable');

implementation

uses UnMyObjects, Module, Principal, CoresVCLSkin, MyGlyfs;

{$R *.DFM}

const
  FPadraoTit_Cor_A = $00C8ADA4; // Lilas saturado 75%
  FPadraoTit_Cor_B = $00FFFBF8; // Azul  saturado  0%
  FPadraoTit_Cor_C = $00AA6A3E; // Azul  saturado 53%

  FPadraoTit_Pos_A_X = -1;
  FPadraoTit_Pos_A_Y =  0;
  FPadraoTit_Pos_B_X =  0;
  FPadraoTit_Pos_B_Y = +1;

  FTitCorA = 'TitCor1';
  FTitCorB = 'TitCor2';
  FTitCorC = 'TitCor3';
  FAvisoCorA = 'AvisoCor1';
  FAvisoCorC = 'AvisoCor2';
  //
  FTitPosAX = 'TitPosAX';
  FTitPosAY = 'TitPosAY';
  FTitPosBX = 'TitPosBX';
  FTitPosBY = 'TitPosBY';

procedure TFmLinkRankSkin.AtualizaTitulo();
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C], [LaAviso1,
  LaAviso2], Caption, True,
  taCenter, 2, 10, 20);
end;

procedure TFmLinkRankSkin.BtRestaurarClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU(FTitCorA, FCam, FPadraoTit_Cor_A, ktInteger);
  Geral.WriteAppKeyCU(FTitCorB, FCam, FPadraoTit_Cor_B, ktInteger);
  Geral.WriteAppKeyCU(FTitCorC, FCam, FPadraoTit_Cor_C, ktInteger);
  //
  Geral.WriteAppKeyCU(FTitPosAX, FCam, FPadraoTit_Pos_A_X, ktInteger);
  Geral.WriteAppKeyCU(FTitPosAY, FCam, FPadraoTit_Pos_A_Y, ktInteger);
  Geral.WriteAppKeyCU(FTitPosBX, FCam, FPadraoTit_Pos_B_X, ktInteger);
  Geral.WriteAppKeyCU(FTitPosBY, FCam, FPadraoTit_Pos_B_Y, ktInteger);
  //
  MostraCores();
  //
  RGQualUsaLinkRankSkin.Itemindex := 0;
  RGQualUsaLinkRankSkin.Itemindex := 1;
  //
  AtualizaTitulo();
end;

procedure TFmLinkRankSkin.BtSaidaClick(Sender: TObject);
begin
  Hide;
end;

procedure TFmLinkRankSkin.CkSkinAbaClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('SkinAbas', FCam, Geral.BoolToInt(CkSkinAba.Checked), ktInteger);
end;

procedure TFmLinkRankSkin.EdArqLinkRankSkinChange(Sender: TObject);
var
  Arq: String;
begin
{$IfDef cSkinRank} //Berlin
  Arq := EdArqLinkRankSkin.Text;
  VAR_CAMINHOTXTBMP := Arq;
  if not FCriado then
    Exit;
  if FileExists(Arq) then
  begin
    FmPrincipal.sd1.skinfile:= Arq;
    VAR_IDTXTBMP := FmMyGlyfs.DefineIdTxtBmp();
    Geral.WriteAppKeyCU('Caminho', FCam,
      Arq, ktString);
  end;
  LinkRankSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
{$EndIf} //Berlin
end;

procedure TFmLinkRankSkin.EdCorPosAXExit(Sender: TObject);
begin
  Geral.WriteAppKeyCU(FTitPosAX, FCam, EdCorPosAX.ValueVariant, ktInteger);
end;

procedure TFmLinkRankSkin.EdCorPosAYExit(Sender: TObject);
begin
  Geral.WriteAppKeyCU(FTitPosAY, FCam, EdCorPosAY.ValueVariant, ktInteger);
end;

procedure TFmLinkRankSkin.EdCorPosBXExit(Sender: TObject);
begin
  Geral.WriteAppKeyCU(FTitPosBX, FCam, EdCorPosBX.ValueVariant, ktInteger);
end;

procedure TFmLinkRankSkin.EdCorPosBYExit(Sender: TObject);
begin
  Geral.WriteAppKeyCU(FTitPosBY, FCam, EdCorPosBY.ValueVariant, ktInteger);
end;

procedure TFmLinkRankSkin.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLinkRankSkin.FormCreate(Sender: TObject);
begin
  //Self.DoubleBuffered := True;
  FCriado := False;
  FCam    := Application.Title + '\UsaLinkRankSkin';
  //
  RGQualUsaLinkRankSkin.ItemIndex := Geral.ReadAppKeyCU('QualUsa', FCam, ktInteger, 1);
  RGComoUsaLinkRankSkin.ItemIndex := Geral.ReadAppKeyCU('ComoUsa', FCam, ktInteger, 1);
  EdArqLinkRankSkin.Text          := Geral.ReadAppKeyCU('Caminho', FCam, ktString, '');
  CkSkinAba.Checked               := Geral.IntToBool(Geral.ReadAppKeyCU('SkinAbas', FCam, ktInteger, 1));
  //
  FCriado := True;
{$IfDef cSkinRank} //Berlin
  LinkRankSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
{$EndIf} //Berlin
  MostraCores();
end;

procedure TFmLinkRankSkin.FormHide(Sender: TObject);
begin
  FVisivel := False;
end;

procedure TFmLinkRankSkin.FormResize(Sender: TObject);
begin
  AtualizaTitulo();
end;

procedure TFmLinkRankSkin.FormShow(Sender: TObject);
begin
  AtualizaTitulo();
  FVisivel := True;
end;

{$IfDef cSkinRank}
function TFmLinkRankSkin.LinkRankSkinAtiva(SkinData: TSkinData;
  SkinStore: TSkinStore; QualUsa, ComoUsa: Integer): Boolean;
begin
{
  VCLSkin.LoadFromCollection(SkinStore, 1);
  VAR_IDTXTBMP := FmMyGlyfs.DefineIdTxtBmp;
}
  if SkinData.Active then
    SkinData.Active := False;
  //Self.Update;
  //Self.Invalidate;
  Application.ProcessMessages;
  if QualUsa > 0 then
  begin
    VAR_USAVCLSKIN := True;
    if FileExists(VAR_CAMINHOTXTBMP) then
    begin
      SkinData.SkinFile := VAR_CAMINHOTXTBMP;
      ////////////////////////////////////////////////////////////////
      case ComoUsa of
        0:
        begin
          SkinData.SkinControls := SkinData.SkinControls + [xcMainMenu];
          SkinData.SkinControls := SkinData.SkinControls + [xcMenuItem];
        end;
        1:
        begin
          SkinData.SkinControls := SkinData.SkinControls + [xcMainMenu];
          SkinData.SkinControls := SkinData.SkinControls - [xcMenuItem];
        end;
        2:
        begin
          SkinData.SkinControls := SkinData.SkinControls - [xcMainMenu];
          SkinData.SkinControls := SkinData.SkinControls - [xcMenuItem];
        end;
      end;
    end else if SkinStore.Store.Count > 0 then
    begin
      SkinData.LoadFromCollection(SkinStore, 0);
    end;
    // al�m disso, pode ser que tenha um carregado no SkinData.Skinstore, ent�o
    // ativa mesmo que n�o encontre no arquivo ou no LoadFromCollection(SkinStore
    ////////////////////////////////////////////////////////////////
    if not SkinData.Active then
    begin
      SkinData.Active := True;
      if FVisivel then
      begin
        Hide;
        Show;
        Update;
        Invalidate;
      end;
      FmPrincipal.BorderStyle := bsSizeable;
      FmPrincipal.Invalidate;
    end;
  end;
  //
  Result := True;
  MostraNomeSkin();
end;

function TFmLinkRankSkin.LinkRankSkinDefineUso(SkinData: TSkinData;
  SkinStore: TSkinStore): Boolean;
var
  QualUsa, ComoUsa: Integer;
begin
  Result := False;
  if not FCriado then
    Exit;
  //
  QualUsa := Geral.ReadAppKeyCU('QualUsa', FCam, ktInteger, 1);
  ComoUsa := Geral.ReadAppKeyCU('ComoUsa', FCam, ktInteger, 1);
  //
  case QualUsa of
    0: ;// Nada
    1: VAR_CAMINHOTXTBMP := VAR_CAMINHOSKINPADRAO;
    2: VAR_CAMINHOTXTBMP := Geral.ReadAppKeyCU('Caminho',
       FCam, ktString, VAR_CAMINHOSKINPADRAO);
  end;
{$IfDef cSkinRank} //Berlin
  LinkRankSkinAtiva(FmPrincipal.sd1, FmPrincipal.SkinStore1, QualUsa, ComoUsa);
{$EndIf} //Berlin
end;
{$EndIf}

procedure TFmLinkRankSkin.MostraCores();
  function ObtemCor(NomeCor: String; Padrao: Integer): Integer;
  begin
    Result := Geral.ReadAppKeyCU(NomeCor, FCam, ktInteger, Padrao);
  end;
  function ObtemPos(NomePos: String; Padrao: Integer): Integer;
  begin
    Result := Geral.ReadAppKeyCU(NomePos, FCam, ktInteger, Padrao);
  end;
begin
{
  VAR_COR_TIT_A := ObtemCor(FTitCorA, clGradientActiveCaption);
  VAR_COR_TIT_B := ObtemCor(FTitCorB, clSilver);
  VAR_COR_TIT_C := ObtemCor(FTitCorC, clHotLight);
}
  VAR_COR_TIT_A := ObtemCor(FTitCorA, FPadraoTit_Cor_A);
  VAR_COR_TIT_B := ObtemCor(FTitCorB, FPadraoTit_Cor_B);
  VAR_COR_TIT_C := ObtemCor(FTitCorC, FPadraoTit_Cor_C);
  VAR_COR_AVISO_A := ObtemCor(FAvisoCorA, clSilver);
  VAR_COR_AVISO_C := ObtemCor(FAvisoCorC, clRed);
  //
  ShTitCorA.Brush.Color := VAR_COR_TIT_A;
  ShTitCorB.Brush.Color := VAR_COR_TIT_B;
  ShTitCorC.Brush.Color := VAR_COR_TIT_C;
  ShAvisoCorA.Brush.Color := VAR_COR_AVISO_A;
  ShAvisoCorC.Brush.Color := VAR_COR_AVISO_C;
  //
  LaTitCorA.Caption := ColorToString(VAR_COR_TIT_A);
  LaTitCorB.Caption := ColorToString(VAR_COR_TIT_B);
  LaTitCorC.Caption := ColorToString(VAR_COR_TIT_C);
  LaAvisoCorA.Caption := ColorToString(VAR_COR_AVISO_A);
  LaAvisoCorC.Caption := ColorToString(VAR_COR_AVISO_C);
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  VAR_COR_POS_A_X := ObtemPos(FTitPosAX, FPadraoTit_Pos_A_X);
  VAR_COR_POS_A_Y := ObtemPos(FTitPosAY, FPadraoTit_Pos_A_Y);
  VAR_COR_POS_B_X := ObtemPos(FTitPosBX, FPadraoTit_Pos_B_X);
  VAR_COR_POS_B_Y := ObtemPos(FTitPosBY, FPadraoTit_Pos_B_Y);
  //
  EdCorPosAX.ValueVariant := VAR_COR_POS_A_X;
  EdCorPosAY.ValueVariant := VAR_COR_POS_A_Y;
  EdCorPosBX.ValueVariant := VAR_COR_POS_B_X;
  EdCorPosBY.ValueVariant := VAR_COR_POS_B_Y;

  //

{$IfDef cSkinRank} //Berlin
  VAR_DMK_COR_csText               := FmPrincipal.sd1.Colors[csText];
  VAR_DMK_COR_csTitleTextActive    := FmPrincipal.sd1.Colors[csTitleTextActive];
  VAR_DMK_COR_csTitleTextNoActive  := FmPrincipal.sd1.Colors[csTitleTextNoActive];
  VAR_DMK_COR_csButtonFace         := FmPrincipal.sd1.Colors[csButtonFace];
  VAR_DMK_COR_csButtonText         := FmPrincipal.sd1.Colors[csButtonText];
  VAR_DMK_COR_csButtonHilight      := FmPrincipal.sd1.Colors[csButtonHilight];
  VAR_DMK_COR_csButtonlight        := FmPrincipal.sd1.Colors[csButtonlight];
  VAR_DMK_COR_csButtonShadow       := FmPrincipal.sd1.Colors[csButtonShadow];
  VAR_DMK_COR_csButtonDkshadow     := FmPrincipal.sd1.Colors[csButtonDkshadow];
  VAR_DMK_COR_csSelectText         := FmPrincipal.sd1.Colors[csSelectText];
  VAR_DMK_COR_csSelectBg           := FmPrincipal.sd1.Colors[csSelectBg];
  VAR_DMK_COR_csHilightText        := FmPrincipal.sd1.Colors[csHilightText];
  VAR_DMK_COR_csHilight            := FmPrincipal.sd1.Colors[csHilight];
  VAR_DMK_COR_csMenuBar            := FmPrincipal.sd1.Colors[csMenuBar];
  VAR_DMK_COR_csMenuBarText        := FmPrincipal.sd1.Colors[csMenuBarText];
  VAR_DMK_COR_csMenuText           := FmPrincipal.sd1.Colors[csMenuText];
  VAR_DMK_COR_csMenubg             := FmPrincipal.sd1.Colors[csMenubg];
  VAR_DMK_COR_csCaption            := FmPrincipal.sd1.Colors[csCaption];
  VAR_DMK_COR_csScrollbar          := FmPrincipal.sd1.Colors[csScrollbar];
  VAR_DMK_COR_csTextDisable        := FmPrincipal.sd1.Colors[csTextDisable];
{$EndIf} //Berlin
end;

procedure TFmLinkRankSkin.MostraNomeSkin();
begin
  if VAR_SKINUSANDO <> nil then
     VAR_SKINUSANDO.Text := '  '+ExtractFileName(VAR_CAMINHOTXTBMP);
end;

procedure TFmLinkRankSkin.RGComoUsaLinkRankSkinClick(Sender: TObject);
begin
{$IfDef cSkinRank} //Berlin
  Geral.WriteAppKeyCU('ComoUsa', FCam,
    RGComoUsaLinkRankSkin.ItemIndex, ktInteger);
  LinkRankSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
{$EndIf} //Berlin
end;

procedure TFmLinkRankSkin.RGQualUsaLinkRankSkinClick(Sender: TObject);
begin
{$IfDef cSkinRank} //Berlin
  Geral.WriteAppKeyCU('QualUsa', FCam,
    RGQualUsaLinkRankSkin.ItemIndex, ktInteger);
  LinkRankSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
{$EndIf} //Berlin
end;

procedure TFmLinkRankSkin.SbAvisoCorAClick(Sender: TObject);
begin
  SelecionaCor(FAvisoCorA);
end;

procedure TFmLinkRankSkin.SbAvisoCorCClick(Sender: TObject);
begin
  SelecionaCor(FAvisoCorC);
end;

procedure TFmLinkRankSkin.SBNadadorClick(Sender: TObject);
const
  sSKN = 'skn';
  sJPG = 'jpg';
var
  Arq, Titulo, IniDir, Ext: String;
  p: Integer;
begin
  Arq     := EdArqLinkRankSkin.Text;
  Titulo  := 'Selecionar arquivo VCLSkin';
  IniDir  := 'C:\Dermatek\Skins\VCLSkin';
  if MyObjects.FileOpenDialog(Self, IniDir, Arq, Titulo,
  'Exemplos em jpg|*.jpg;Arquivos Skin|*.skn', [], Arq) then
  begin
    Ext := ExtractFileExt(Arq);
    p := pos('.', Ext);
    Ext := Copy(Ext, p+1);
    if Lowercase(Trim(Ext)) = sJPG then
      Arq := dmkPF.MudaExtensaoDeArquivo(Arq, sSKN);
    if FileExists(Arq) then
      EdArqLinkRankSkin.Text := Arq
    else
      Geral.MB_Erro('Arquivo n�o encontrado:' + sLineBreak +
      Arq);
  end;
end;

procedure TFmLinkRankSkin.SbTitCorAClick(Sender: TObject);
begin
  SelecionaCor(FTitCorA);
end;

procedure TFmLinkRankSkin.SbTitCorBClick(Sender: TObject);
begin
  SelecionaCor(FTitCorB);
end;

procedure TFmLinkRankSkin.SbTitCorCClick(Sender: TObject);
begin
  SelecionaCor(FTitCorC);
end;

function TFmLinkRankSkin.SelecionaCor(NomeCor: String): Boolean;
begin
  Result := False;
  if MyObjects.CriaForm_AcessoTotal(TFmCoresVCLSkin, FmCoresVCLSkin) then
  begin
    FmCoresVCLSkin.ShowModal;
    Geral.WriteAppKeyCU(NomeCor, FCam,
      FmCoresVCLSkin.FCor, ktInteger);
    FmCoresVCLSkin.Destroy;
    //
    MostraCores();
    //
    AtualizaTitulo();
  end;
end;

procedure TFmLinkRankSkin.ShTitCorAMouseActivate(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y, HitTest: Integer;
  var MouseActivate: TMouseActivate);
begin
  SelecionaCor(FTitCorA);
end;

procedure TFmLinkRankSkin.ShTitCorBMouseActivate(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y, HitTest: Integer;
  var MouseActivate: TMouseActivate);
begin
  SelecionaCor(FTitCorB);
end;

procedure TFmLinkRankSkin.ShTitCorCMouseActivate(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y, HitTest: Integer;
  var MouseActivate: TMouseActivate);
begin
  SelecionaCor(FTitCorC);
end;

end.
