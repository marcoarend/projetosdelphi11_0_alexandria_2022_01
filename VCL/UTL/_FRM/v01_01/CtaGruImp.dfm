object FmCtaGruImp: TFmCtaGruImp
  Left = 368
  Top = 194
  Caption = 'FIN-RELAT-008 :: Relat'#243'rios de Grupos Sazonais'
  ClientHeight = 544
  ClientWidth = 965
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 118
    Width = 965
    Height = 426
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1009
      Height = 113
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 10
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 69
        Top = 10
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 542
        Top = 10
        Width = 69
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data inicial:'
      end
      object Label4: TLabel
        Left = 684
        Top = 10
        Width = 64
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data Final:'
      end
      object Label7: TLabel
        Left = 10
        Top = 59
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 10
        Top = 30
        Width = 55
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TEdit
        Left = 69
        Top = 30
        Width = 469
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 255
        TabOrder = 1
      end
      object TPDataI: TdmkEditDateTimePicker
        Left = 542
        Top = 30
        Width = 137
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 39389.736000069400000000
        Time = 39389.736000069400000000
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object TPDataF: TdmkEditDateTimePicker
        Left = 684
        Top = 30
        Width = 138
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 39389.736000069400000000
        Time = 39389.736000069400000000
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdEmpresa: TdmkEditCB
        Left = 10
        Top = 79
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 84
        Top = 79
        Width = 734
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 5
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 340
      Width = 965
      Height = 86
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object PnSaiDesis: TPanel
        Left = 786
        Top = 18
        Width = 177
        Height = 66
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn1: TBitBtn
          Tag = 13
          Left = 15
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn1Click
        end
      end
      object Panel1: TPanel
        Left = 2
        Top = 18
        Width = 784
        Height = 66
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 15
          Top = 5
          Width = 147
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 118
    Width = 965
    Height = 426
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelHead: TPanel
      Left = 0
      Top = 0
      Width = 965
      Height = 118
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 10
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 69
        Top = 10
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 665
        Top = 10
        Width = 69
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data inicial:'
      end
      object Label6: TLabel
        Left = 807
        Top = 10
        Width = 64
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data Final:'
      end
      object Label8: TLabel
        Left = 10
        Top = 59
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
        FocusControl = DBEdit1
      end
      object Label11: TLabel
        Left = 862
        Top = 59
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
        FocusControl = DBEdit2
      end
      object DBEdCodigo: TDBEdit
        Left = 10
        Top = 30
        Width = 55
        Height = 24
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCtaGruImp
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        MaxLength = 1
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 69
        Top = 30
        Width = 592
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsCtaGruImp
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 28
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit01: TDBEdit
        Left = 665
        Top = 30
        Width = 137
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'DataI'
        DataSource = DsCtaGruImp
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object DBEdit02: TDBEdit
        Left = 807
        Top = 30
        Width = 138
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'DataF'
        DataSource = DsCtaGruImp
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
      end
      object DBEdit1: TDBEdit
        Left = 10
        Top = 79
        Width = 848
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_ENT'
        DataSource = DsCtaGruImp
        TabOrder = 4
      end
      object DBEdit2: TDBEdit
        Left = 862
        Top = 79
        Width = 83
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'CliInt'
        DataSource = DsCtaGruImp
        TabOrder = 5
      end
    end
    object StaticText1: TStaticText
      Left = 0
      Top = 118
      Width = 965
      Height = 22
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Alignment = taCenter
      Caption = 'CONTAS SAZONAIS VINCULADAS'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object PainelData: TPanel
      Left = 0
      Top = 140
      Width = 965
      Height = 156
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 2
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 963
        Height = 154
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsCtaGruIEG
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -15
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NOMECTASAZ'
            Title.Caption = 'Conta sazonal'
            Width = 439
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataI'
            Title.Caption = 'Data inicial'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataF'
            Title.Caption = 'Data final'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Saldo'
            Width = 68
            Visible = True
          end>
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 347
      Width = 965
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 3
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 137
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 351
        Top = 18
        Width = 612
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 448
          Top = 0
          Width = 164
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtGrupo: TBitBtn
          Tag = 353
          Left = 5
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Grupo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtGrupoClick
        end
        object BtConta: TBitBtn
          Tag = 354
          Left = 158
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Conta'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtContaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 906
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 640
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 443
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Relat'#243'rios de Grupos Sazonais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 443
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Relat'#243'rios de Grupos Sazonais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 443
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Relat'#243'rios de Grupos Sazonais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 965
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 961
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsCtaGruImp: TDataSource
    DataSet = QrCtaGruImp
    Left = 492
    Top = 161
  end
  object QrCtaGruImp: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCtaGruImpBeforeOpen
    AfterOpen = QrCtaGruImpAfterOpen
    AfterScroll = QrCtaGruImpAfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) '
      'NO_ENT, ent.CliInt, egi.* '
      'FROM excelgruimp egi'
      'LEFT JOIN entidades ent ON ent.Codigo=egi.Entidade'
      'WHERE egi.Codigo > 0'
      '')
    Left = 464
    Top = 161
    object QrCtaGruImpLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'excelgruimp.Lk'
    end
    object QrCtaGruImpDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'excelgruimp.DataCad'
    end
    object QrCtaGruImpDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'excelgruimp.DataAlt'
    end
    object QrCtaGruImpUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'excelgruimp.UserCad'
    end
    object QrCtaGruImpUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'excelgruimp.UserAlt'
    end
    object QrCtaGruImpCodigo: TSmallintField
      FieldName = 'Codigo'
      Origin = 'excelgruimp.Codigo'
    end
    object QrCtaGruImpNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'excelgruimp.Nome'
      Size = 30
    end
    object QrCtaGruImpDataI: TDateField
      FieldName = 'DataI'
      Origin = 'excelgruimp.DataI'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrCtaGruImpDataF: TDateField
      FieldName = 'DataF'
      Origin = 'excelgruimp.DataF'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrCtaGruImpEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrCtaGruImpNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrCtaGruImpAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCtaGruImpAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCtaGruImpCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object QrCtaGruIEG: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT csz.Nome NOMECTASAZ, csz.DataI, csz.DataF, '
      'csz.Saldo, ieg.* '
      'FROM excelgruieg ieg'
      'LEFT JOIN excelgru csz ON csz.Codigo=ieg.ExcelGru'
      'WHERE ieg.Codigo=:P0'
      'ORDER BY csz.DataI, csz.DataF, csz.Nome')
    Left = 464
    Top = 189
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCtaGruIEGCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'excelgruieg.Codigo'
    end
    object QrCtaGruIEGExcelGru: TIntegerField
      FieldName = 'ExcelGru'
      Origin = 'excelgruieg.ExcelGru'
    end
    object QrCtaGruIEGLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'excelgruieg.Lk'
    end
    object QrCtaGruIEGDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'excelgruieg.DataCad'
    end
    object QrCtaGruIEGDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'excelgruieg.DataAlt'
    end
    object QrCtaGruIEGUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'excelgruieg.UserCad'
    end
    object QrCtaGruIEGUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'excelgruieg.UserAlt'
    end
    object QrCtaGruIEGNOMECTASAZ: TWideStringField
      FieldName = 'NOMECTASAZ'
      Origin = 'excelgru.Nome'
      Size = 50
    end
    object QrCtaGruIEGDataI: TDateField
      FieldName = 'DataI'
      Origin = 'excelgru.DataI'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrCtaGruIEGDataF: TDateField
      FieldName = 'DataF'
      Origin = 'excelgru.DataF'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrCtaGruIEGSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'excelgru.Saldo'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsCtaGruIEG: TDataSource
    DataSet = QrCtaGruIEG
    Left = 492
    Top = 189
  end
  object PMGrupo: TPopupMenu
    OnPopup = PMGrupoPopup
    Left = 244
    Top = 260
    object Incluinovogrupo1: TMenuItem
      Caption = '&Inclui novo grupo'
      OnClick = Incluinovogrupo1Click
    end
    object Alteragrupoatual1: TMenuItem
      Caption = '&Altera grupo atual'
      OnClick = Alteragrupoatual1Click
    end
    object Excluigrupoatual1: TMenuItem
      Caption = '&Exclui grupo atual'
      Enabled = False
    end
  end
  object PMConta: TPopupMenu
    OnPopup = PMContaPopup
    Left = 340
    Top = 264
    object Agregacontasazonal1: TMenuItem
      Caption = '&Inclui conta sazonal'
      OnClick = Agregacontasazonal1Click
    end
    object Retiracontasazonal1: TMenuItem
      Caption = '&Exclui conta sazonal'
      OnClick = Retiracontasazonal1Click
    end
  end
  object frxGCZ: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39389.768529907400000000
    ReportOptions.LastChange = 39389.768529907400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 549
    Top = 162
    Datasets = <
      item
        DataSet = frxDsCtaGruIEG
        DataSetName = 'frxDsCtaGruIEG'
      end
      item
        DataSet = frxDsCtaGruImp
        DataSetName = 'frxDsCtaGruImp'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsLct
        DataSetName = 'frxDsLct'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 891.969080000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo1: TfrxMemoView
          Left = 75.590600000000000000
          Top = 170.078850000000000000
          Width = 642.520100000000000000
          Height = 151.181200000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -48
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 75.590600000000000000
          Top = 548.031849999999900000
          Width = 642.520100000000000000
          Height = 113.385900000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -37
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCtaGruImp."Nome"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Top = 812.598950000000000000
          Width = 642.520100000000000000
          Height = 71.811070000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mmm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCtaGruImp."DataI"] a [frxDsCtaGruImp."DataF"]')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object ReportTitle2: TfrxReportTitle
        FillType = ftBrush
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo4: TfrxMemoView
          Left = 75.590600000000000000
          Top = 34.015770000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'SUB-TOTAIS POR CONTAS')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 75.590600000000000000
          Top = 64.252010000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da conta')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 453.543600000000000000
          Top = 64.252010000000000000
          Width = 83.149611180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data inicial')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 536.693260000000000000
          Top = 64.252010000000000000
          Width = 83.149611180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data final')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 619.842920000000000000
          Top = 64.252010000000000000
          Width = 98.267731180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 162.519790000000000000
        Width = 793.701300000000000000
        DataSet = frxDsCtaGruIEG
        DataSetName = 'frxDsCtaGruIEG'
        RowCount = 0
        object Memo9: TfrxMemoView
          Left = 75.590600000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DataField = 'NOMECTASAZ'
          DataSet = frxDsCtaGruIEG
          DataSetName = 'frxDsCtaGruIEG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCtaGruIEG."NOMECTASAZ"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 453.543600000000000000
          Width = 83.149611180000000000
          Height = 18.897650000000000000
          DataField = 'DataI'
          DataSet = frxDsCtaGruIEG
          DataSetName = 'frxDsCtaGruIEG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCtaGruIEG."DataI"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 536.693260000000000000
          Width = 83.149611180000000000
          Height = 18.897650000000000000
          DataField = 'DataF'
          DataSet = frxDsCtaGruIEG
          DataSetName = 'frxDsCtaGruIEG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCtaGruIEG."DataF"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 619.842920000000000000
          Width = 98.267731180000000000
          Height = 18.897650000000000000
          DataField = 'Saldo'
          DataSet = frxDsCtaGruIEG
          DataSetName = 'frxDsCtaGruIEG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtaGruIEG."Saldo"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 204.094620000000000000
        Width = 793.701300000000000000
        object Memo13: TfrxMemoView
          Left = 75.590600000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCtaGruIEG
          DataSetName = 'frxDsCtaGruIEG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL GERAL')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 453.543600000000000000
          Width = 83.149611180000000000
          Height = 18.897650000000000000
          DataSet = frxDsCtaGruIEG
          DataSetName = 'frxDsCtaGruIEG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 536.693260000000000000
          Width = 83.149611180000000000
          Height = 18.897650000000000000
          DataSet = frxDsCtaGruIEG
          DataSetName = 'frxDsCtaGruIEG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 619.842920000000000000
          Width = 98.267731180000000000
          Height = 18.897650000000000000
          DataSet = frxDsCtaGruIEG
          DataSetName = 'frxDsCtaGruIEG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCtaGruIEG."Saldo">,MasterData1)]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 56.692950000000000000
        Top = 287.244280000000000000
        Width = 793.701300000000000000
        object Memo17: TfrxMemoView
          Left = 476.220780000000000000
          Top = 7.559059999999990000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
    object Page3: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 45.354360000000000000
        Top = 124.724490000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLct."NOMECTASAZ"'
        object Memo19: TfrxMemoView
          Left = 37.795300000000000000
          Top = 7.559059999999990000
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLct."NOMECTASAZ"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 79.370130000000000000
          Top = 30.236240000000000000
          Width = 279.685220000000000000
          Height = 15.118120000000000000
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hist'#243'rico')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 699.213050000000000000
          Top = 30.236240000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 642.520100000000000000
          Top = 30.236240000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 37.795300000000000000
          Top = 30.236240000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 359.055350000000000000
          Top = 30.236240000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 415.748300000000000000
          Top = 30.236240000000000000
          Width = 105.826840000000000000
          Height = 15.118120000000000000
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta (Plano)')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 521.575140000000000000
          Top = 30.236240000000000000
          Width = 120.944960000000000000
          Height = 15.118120000000000000
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor/Cliente')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 192.756030000000000000
        Width = 793.701300000000000000
        DataSet = frxDsLct
        DataSetName = 'frxDsLct'
        RowCount = 0
        object Memo20: TfrxMemoView
          Left = 79.370130000000000000
          Width = 279.685220000000000000
          Height = 15.118120000000000000
          DataField = 'QtdeEDescricao'
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct."QtdeEDescricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 699.213050000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataField = 'Debito'
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct."Debito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 642.520100000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataField = 'Credito'
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct."Credito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 37.795300000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          DataField = 'Data'
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLct."Data"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 359.055350000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataField = 'Controle'
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 415.748300000000000000
          Width = 105.826840000000000000
          Height = 15.118120000000000000
          DataField = 'NOMECTA'
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct."NOMECTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 521.575140000000000000
          Width = 120.944960000000000000
          Height = 15.118120000000000000
          DataField = 'NOMETER'
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct."NOMETER"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 230.551330000000000000
        Width = 793.701300000000000000
        object Memo36: TfrxMemoView
          Left = 37.795300000000000000
          Width = 604.724800000000000000
          Height = 15.118120000000000000
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL DA CONTA SAZONAL [frxDsLct."NOMECTASAZ"]:')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 699.213050000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct."Debito">,MasterData2)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          Left = 642.520100000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataSet = frxDsLct
          DataSetName = 'frxDsLct'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct."Credito">,MasterData2)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 56.692950000000000000
        Top = 313.700990000000000000
        Width = 793.701300000000000000
        object Memo39: TfrxMemoView
          Left = 514.016080000000000000
          Top = 7.559059999999990000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 45.354360000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo18: TfrxMemoView
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'LAN'#199'AMENTOS DAS CONTAS SAZONAIS')
          ParentFont = False
        end
      end
    end
  end
  object frxDsCtaGruImp: TfrxDBDataset
    UserName = 'frxDsCtaGruImp'
    CloseDataSource = False
    DataSet = QrCtaGruImp
    BCDToCurrency = False
    Left = 521
    Top = 162
  end
  object frxDsCtaGruIEG: TfrxDBDataset
    UserName = 'frxDsCtaGruIEG'
    CloseDataSource = False
    DataSet = QrCtaGruIEG
    BCDToCurrency = False
    Left = 521
    Top = 190
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLctCalcFields
    Left = 549
    Top = 190
    object QrLctNOMECTASAZ: TWideStringField
      FieldName = 'NOMECTASAZ'
      Size = 50
    end
    object QrLctData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctNOMECTA: TWideStringField
      FieldName = 'NOMECTA'
      Size = 50
    end
    object QrLctNOMEFNC: TWideStringField
      FieldName = 'NOMEFNC'
      Size = 100
    end
    object QrLctNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctNOMETER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETER'
      Size = 100
      Calculated = True
    end
    object QrLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLctQtdeEDescricao: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'QtdeEDescricao'
      Size = 255
      Calculated = True
    end
  end
  object frxDsLct: TfrxDBDataset
    UserName = 'frxDsLct'
    CloseDataSource = False
    DataSet = QrLct
    BCDToCurrency = False
    Left = 577
    Top = 190
  end
  object VUEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Filial'
    UpdCampo = 'Filial'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 344
    Top = 36
  end
end
