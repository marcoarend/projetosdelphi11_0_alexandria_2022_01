unit PesqContaCtrl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, Db, mySQLDbTables,
  ComCtrls, frxClass, frxDBSet, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkGeral, dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  TFmPesqContaCtrl = class(TForm)
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    DsContas: TDataSource;
    QrCtasResMes: TmySQLQuery;
    QrCtasResMesConta: TIntegerField;
    QrCtasResMesNome: TWideStringField;
    QrCtasResMesPeriodo: TIntegerField;
    QrCtasResMesTipo: TIntegerField;
    QrCtasResMesFator: TFloatField;
    QrCtasResMesValFator: TFloatField;
    QrCtasResMesDevido: TFloatField;
    QrCtasResMesPago: TFloatField;
    QrCtasResMesDiferenca: TFloatField;
    QrCtasResMesAcumulado: TFloatField;
    QrCtasResMesNOME_PERIODO: TWideStringField;
    frxCtasResMes: TfrxReport;
    frxDsCtasResMes: TfrxDBDataset;
    Panel1: TPanel;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    LaAviso3: TLabel;
    ProgressBar1: TProgressBar;
    ProgressBar2: TProgressBar;
    ProgressBar3: TProgressBar;
    Panel3: TPanel;
    Label1: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    CBMesIni: TComboBox;
    CBAnoIni: TComboBox;
    CBMesFim: TComboBox;
    CBAnoFim: TComboBox;
    EdEmpresa: TdmkEditCB;
    Label2: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    Label3: TLabel;
    Label8: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrCtasResMesCalcFields(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    //FPeriodo: Integer;
  public
    { Public declarations }
    FCliente_Txt, FFornece_Txt: String;
  end;

  var
  FmPesqContaCtrl: TFmPesqContaCtrl;

implementation

uses UnMyObjects, Module, UnMsgInt, UnGOTOy, ModuleFin, ModuleGeral,
  UnInternalConsts, DmkDAC_PF;

{$R *.DFM}

procedure TFmPesqContaCtrl.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqContaCtrl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPesqContaCtrl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesqContaCtrl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrCtasResMes.Database := DmodG.MyPID_DB;
  QrContas.Database     := Dmod.MyDB;
  //
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  //
  MyObjects.PreencheCBAnoECBMes(CBAnoIni, CBMesIni, -1);
  MyObjects.PreencheCBAnoECBMes(CBAnoFim, CBMesFim, -1);
  //
  LaAviso1.Caption := '';
  LaAviso2.Caption := '';
  LaAviso3.Caption := '';
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmPesqContaCtrl.QrCtasResMesCalcFields(DataSet: TDataSet);
begin
  QrCtasResMesNOME_PERIODO.Value :=
    dmkPF.MesEAnoDoPeriodo(QrCtasResMesPeriodo.Value);
end;

procedure TFmPesqContaCtrl.BtOKClick(Sender: TObject);
var
  AnoIni, MesIni, PeriodoIni, AnoFim, MesFim, PeriodoFim, Genero,
  Entidade, CliInt: Integer;
  DtEncer, DtMorto: TDateTime;
  TabLctA, TabLctB, TabLctD: String;
begin
  Entidade := DModG.QrEmpresasCodigo.Value;
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  CliInt   := DModG.QrEmpresasFilial.Value;
  //
  AnoIni := Geral.IMV(CBAnoIni.Text);
  if MyObjects.FIC(AnoIni = 0, CBAnoIni, 'Ano inicial n�o informado!') then Exit;
  MesIni := CBMesIni.ItemIndex + 1;
  PeriodoIni := ((AnoIni - 2000) * 12) + MesIni;
  //
  AnoFim := Geral.IMV(CBAnoFim.Text);
  if MyObjects.FIC(AnoFim = 0, CBAnoFim, 'Ano final n�o informado!') then Exit;
  MesFim := CBMesFim.ItemIndex + 1;
  PeriodoFim := ((AnoFim - 2000) * 12) + MesFim;
  //
  Genero := Geral.IMV(EdConta.Text);
  //
  DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
  //
  DModFin.AtualizaContasMensais2(PeriodoIni, PeriodoFim, Genero,
  ProgressBar1, ProgressBar2, ProgressBar3, LaAviso1, LaAviso2, LaAviso3,
    DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
  //
  QrCtasResMes.Close;
  QrCtasResMes.Params[00].AsInteger := PeriodoIni;
  QrCtasResMes.Params[01].AsInteger := PeriodoFim;
  UnDmkDAC_PF.AbreQuery(QrCtasResMes, DModG.MyPID_DB);
  //
  MyObjects.frxDefineDataSets(frxCtasResMes, [
    DModG.frxDsDono,
    frxDsCtasResMes
  ]);
  MyObjects.frxMostra(frxCtasResMes, 'Demonstrativo de contas controladas');
  QrCtasResMes.Close;
end;

end.
