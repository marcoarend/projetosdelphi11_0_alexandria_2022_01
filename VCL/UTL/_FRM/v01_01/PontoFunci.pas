unit PontoFunci;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnInternalConsts, ExtCtrls, StdCtrls, Buttons, DBCtrls, Db, UnDmkProcFunc,
  mySQLDbTables, ComCtrls, dmkGeral, dmkImage, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, UnDmkEnums, DmkDAC_PF;

type
  TFmPontoFunci = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdFuncionario: TdmkEditCB;
    CbFuncionario: TdmkDBLookupComboBox;
    EdLogin: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdSenhaP: TEdit;
    Label5: TLabel;
    TPData: TDateTimePicker;
    QrFuncionarios: TmySQLQuery;
    QrFuncionariosCodigo: TIntegerField;
    QrFuncionariosNome: TWideStringField;
    DsFuncionarios: TDataSource;
    Label4: TLabel;
    EdSenhaA: TEdit;
    QrFuncionariosLogin: TWideStringField;
    QrFuncionariosSenha: TWideStringField;
    QrFuncionariosSenhaDia: TWideStringField;
    QrLoc: TmySQLQuery;
    DsLoc: TDataSource;
    QrLocFunci: TIntegerField;
    QrLocData: TDateField;
    QrLocTraIni: TWideStringField;
    QrLocDesIni: TWideStringField;
    QrLocDesFim: TWideStringField;
    QrLocTraFim: TWideStringField;
    QrLocEx1Ini: TWideStringField;
    QrLocEx1Fim: TWideStringField;
    QrLocEx2Ini: TWideStringField;
    QrLocEx2Fim: TWideStringField;
    QrLocEx3Ini: TWideStringField;
    QrLocEx3Fim: TWideStringField;
    QrLocLk: TIntegerField;
    QrLocDataCad: TDateField;
    QrLocDataAlt: TDateField;
    QrLocUserCad: TIntegerField;
    QrLocUserAlt: TIntegerField;
    QrLocNormal: TIntegerField;
    QrLocExtra: TIntegerField;
    TPHora: TDateTimePicker;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTexto: String;
    function VerificaExtra: Integer;
    procedure AtualizaHorasFunci;
    procedure MostraPonto;
  end;

var
  FmPontoFunci: TFmPontoFunci;

implementation

uses UnMyObjects, Module, Ponto, UMySQLModule, MyDBCheck, ModuleGeral, UMySQLDB;

{$R *.DFM}

procedure TFmPontoFunci.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPontoFunci.BtOKClick(Sender: TObject);
begin
  MostraPonto;
end;

procedure TFmPontoFunci.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPontoFunci.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPontoFunci.FormCreate(Sender: TObject);
var
  Hoje: TDateTime;
  Agora: TTime;
begin
  USQLDB.ObtemDataHora(Dmod.MyDB, Hoje, Agora);
  UnDmkDAC_PF.AbreQuery(QrFuncionarios, Dmod.MyDB);
  //
  TPHora.Time := Agora;
  TPData.Date := Hoje;
end;

function TFmPontoFunci.VerificaExtra: Integer;
begin
  Result := 0;
  if QrLocEx1Ini.Value = '' then
  begin
    FmPonto.RadioButton5.Enabled := True;
    Result := 5;
  end else if QrLocEx1Fim.Value = '' then
  begin
    FmPonto.RadioButton6.Enabled := True;
    Result := 6;
  end else if QrLocEx2Ini.Value = '' then
  begin
    FmPonto.RadioButton7.Enabled := True;
    Result := 7;
  end else if QrLocEx2Fim.Value = '' then
  begin
    FmPonto.RadioButton8.Enabled := True;
    Result := 8;
  end else if QrLocEx3Ini.Value = '' then
  begin
    FmPonto.RadioButton9.Enabled := True;
    Result :=9;
  end else if QrLocEx3Fim.Value = '' then
  begin
    FmPonto.RadioButton10.Enabled := True;
    Result := 10;
  end;
end;

procedure TFmPontoFunci.AtualizaHorasFunci;
var
  Funcionario, Normal, Extra, TraIni, DesIni, DesFim, TraFim, Ex1Ini, Ex1Fim,
  Ex2Ini, Ex2Fim, Ex3Ini, Ex3Fim: Integer;
  Data, Hora: String;
begin
  Normal := 0;
  Funcionario := QrFuncionariosCodigo.Value;
  Data := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  Hora := dmkPF.HTT(TPHora.Time, 4, False);
  QrLoc.Close;
  QrLoc.Params[0].AsInteger := Funcionario;
  QrLoc.Params[1].AsString  := Data;
  UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
  //
  TraIni := dmkPF.MinutosHT4(QrLocTraIni.Value);
  TraFim := dmkPF.MinutosHT4(QrLocTraFim.Value);
  DesIni := dmkPF.MinutosHT4(QrLocDesIni.Value);
  DesFim := dmkPF.MinutosHT4(QrLocDesFim.Value);
  if (QrLocTraIni.Value<>'') and (QrLocTraFim.Value<>'') then
  begin
    if TraFim < TraIni then TraFim := TraFim + 1440;
    Normal := TraFim - TraIni;
    if (QrLocDesIni.Value<>'') and (QrLocDesFim.Value<>'') then
    begin
      if DesFim < DesIni then DesFim := DesFim + 1440;
      Normal := Normal - (DesFim - DesIni);
    end;
  end;
  Ex1Ini := dmkPF.MinutosHT4(QrLocEx1Ini.Value);
  Ex1Fim := dmkPF.MinutosHT4(QrLocEx1Fim.Value);
  Ex2Ini := dmkPF.MinutosHT4(QrLocEx2Ini.Value);
  Ex2Fim := dmkPF.MinutosHT4(QrLocEx2Fim.Value);
  Ex3Ini := dmkPF.MinutosHT4(QrLocEx3Ini.Value);
  Ex3Fim := dmkPF.MinutosHT4(QrLocEx3Fim.Value);
  Extra := 0;
  if (QrLocEx1Ini.Value<>'') and (QrLocEx1Fim.Value<>'') then
  begin
    if Ex1Fim < Ex1Ini then Ex1Fim := Ex1Fim + 1440;
    Extra := Extra + (Ex1Fim - Ex1Ini);
  end;
  if (QrLocEx2Ini.Value<>'') and (QrLocEx2Fim.Value<>'') then
  begin
    if Ex2Fim < Ex2Ini then Ex2Fim := Ex2Fim + 1440;
    Extra := Extra + (Ex2Fim - Ex2Ini);
  end;
  if (QrLocEx3Ini.Value<>'') and (QrLocEx3Fim.Value<>'') then
  begin
    if Ex3Fim < Ex3Ini then Ex3Fim := Ex3Fim + 1440;
    Extra := Extra + (Ex3Fim - Ex3Ini);
  end;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE Ponto SET Normal=:P0, Extra=:P1');
  Dmod.QrUpd.SQL.Add('WHERE Funci=:P2 AND Data=:P3');
  Dmod.QrUpd.Params[0].AsFloat   := Normal;
  Dmod.QrUpd.Params[1].AsFloat   := Extra;
  Dmod.QrUpd.Params[2].AsInteger := Funcionario;
  Dmod.QrUpd.Params[3].AsString  := Data;
  Dmod.QrUpd.ExecSQL;
end;

procedure TFmPontoFunci.MostraPonto;
var
  Funcionario, Extra: Integer;
  Data, Hora: String;
begin
  Funcionario := QrFuncionariosCodigo.Value;
  Data := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  Hora := dmkPF.HTT(TPHora.Time, 4, False);
  QrLoc.Close;
  QrLoc.Params[0].AsInteger := Funcionario;
  QrLoc.Params[1].AsString  := Data;
  UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
  //
  if (Uppercase(EdLogin.Text)   = Uppercase(QrFuncionariosLogin.Value))
  and (Uppercase(EdSenhaA.Text) = Uppercase(QrFuncionariosSenhaDia.Value))
  and (Uppercase(EdSenhaP.Text) = Uppercase(QrFuncionariosSenha.Value)) then
  begin
    if DBCheck.CriaFm(TFmPonto, FmPonto, afmoNegarComAviso) then
    begin
      with FmPonto do
      begin
        FFuncionario := Funcionario;
        FData := Data;
        FHora := Hora;
        if (QrLocTraIni.Value = '') then
        begin
          Extra := VerificaExtra;
          if Extra in ([5,7,9]) then RadioButton1.Enabled := True;
        end else if (QrLocDesIni.Value = '') then
        begin
          RadioButton2.Enabled := True;
          RadioButton4.Enabled := True;
        end else if (QrLocDesFim.Value = '') then
        begin
          RadioButton3.Enabled := True;
        end else if (QrLocTraFim.Value = '') then
        begin
          RadioButton4.Enabled := True;
        end else VerificaExtra;
        ShowModal;
        Destroy;
      end;
      FmPonto.ShowModal;
      FmPonto.Destroy;
    end;  
    AtualizaHorasFunci;
    Close;
  end else begin
    Geral.MB_Aviso('Senha inv�lida');
    Exit;
  end;
end;

end.

