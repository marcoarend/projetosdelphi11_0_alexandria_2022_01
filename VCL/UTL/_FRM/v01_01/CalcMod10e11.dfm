object FmCalcMod10e11: TFmCalcMod10e11
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: C'#225'lculos de M'#243'dulos 10 e 11'
  ClientHeight = 612
  ClientWidth = 804
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 804
    Height = 450
    Align = alClient
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 802
      Height = 448
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'M'#243'dulos 10 e 11'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label1: TLabel
          Left = 32
          Top = 24
          Width = 30
          Height = 13
          Caption = 'Texto:'
        end
        object Label2: TLabel
          Left = 32
          Top = 68
          Width = 51
          Height = 13
          Caption = 'Resultado:'
        end
        object Edit1: TEdit
          Left = 32
          Top = 40
          Width = 149
          Height = 21
          TabOrder = 0
        end
        object Edit2: TEdit
          Left = 32
          Top = 88
          Width = 149
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object RGCalculo: TRadioGroup
          Left = 192
          Top = 28
          Width = 185
          Height = 129
          Caption = ' C'#225'lculo: '
          ItemIndex = 0
          Items.Strings = (
            'Modulo10_1e2_9_Back'
            'Modulo10_2e1_X_Back'
            'Modulo11_2a9_Back'
            'Modulo11_2a7_Back'
            'EAN13')
          TabOrder = 2
        end
        object RGTipo: TRadioGroup
          Left = 28
          Top = 112
          Width = 153
          Height = 45
          Caption = ' Tipo: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            '0'
            '1')
          TabOrder = 3
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'C'#243'digo de barras de bloqueto'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 794
          Height = 220
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object Label4: TLabel
            Left = 4
            Top = 4
            Width = 34
            Height = 13
            Caption = 'Banco:'
          end
          object Label10: TLabel
            Left = 540
            Top = 48
            Width = 45
            Height = 13
            Caption = 'Bloqueto:'
          end
          object Label11: TLabel
            Left = 632
            Top = 4
            Width = 39
            Height = 13
            Caption = 'Carteira:'
          end
          object Label12: TLabel
            Left = 100
            Top = 48
            Width = 84
            Height = 13
            Caption = 'Identif. Cobran'#231'a:'
          end
          object Label13: TLabel
            Left = 196
            Top = 48
            Width = 93
            Height = 13
            Caption = 'C'#243'digo do cedente:'
          end
          object Label5: TLabel
            Left = 348
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Ag'#234'ncia:'
          end
          object Label6: TLabel
            Left = 400
            Top = 4
            Width = 18
            Height = 13
            Caption = 'DV:'
          end
          object Label7: TLabel
            Left = 424
            Top = 4
            Width = 30
            Height = 13
            Caption = 'Posto:'
          end
          object Label8: TLabel
            Left = 460
            Top = 4
            Width = 31
            Height = 13
            Caption = 'Conta:'
          end
          object Label9: TLabel
            Left = 556
            Top = 4
            Width = 18
            Height = 13
            Caption = 'DV:'
          end
          object Label16: TLabel
            Left = 452
            Top = 48
            Width = 59
            Height = 13
            Caption = 'Vencimento:'
          end
          object Label15: TLabel
            Left = 384
            Top = 48
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object Label14: TLabel
            Left = 296
            Top = 48
            Width = 85
            Height = 13
            Caption = 'Opera'#231#227'o c'#243'digo:'
          end
          object LaParcTit: TLabel
            Left = 580
            Top = 4
            Width = 39
            Height = 13
            Caption = 'Parcela:'
          end
          object Label19: TLabel
            Left = 4
            Top = 48
            Width = 62
            Height = 13
            Caption = 'Esp'#233'cie doc.'
          end
          object Label20: TLabel
            Left = 8
            Top = 136
            Width = 208
            Height = 13
            Caption = 'Conta Corrente Cooperado (Nosso N'#250'mero):'
          end
          object Label82: TLabel
            Left = 324
            Top = 136
            Width = 98
            Height = 13
            Caption = 'Layout remessa [F4]:'
          end
          object Label24: TLabel
            Left = 731
            Top = 4
            Width = 39
            Height = 13
            Caption = 'Carteira:'
          end
          object EdBanco: TdmkEditCB
            Left = 4
            Top = 20
            Width = 37
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdBancoChange
            DBLookupComboBox = CBBanco
            IgnoraDBLookupComboBox = False
          end
          object CBBanco: TdmkDBLookupComboBox
            Left = 44
            Top = 20
            Width = 297
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsBancos
            TabOrder = 1
            dmkEditCB = EdBanco
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object dmkEdBloqueto: TdmkEdit
            Left = 540
            Top = 64
            Width = 129
            Height = 21
            Alignment = taRightJustify
            TabOrder = 16
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdCarteira: TdmkEdit
            Left = 632
            Top = 20
            Width = 93
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object dmkEdIDCobranca: TdmkEdit
            Left = 100
            Top = 64
            Width = 93
            Height = 21
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object dmkEdCodCedente: TdmkEdit
            Left = 196
            Top = 64
            Width = 97
            Height = 21
            TabOrder = 12
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object dmkEdAgencia: TdmkEdit
            Left = 348
            Top = 20
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 4
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdDVAg: TdmkEdit
            Left = 400
            Top = 20
            Width = 21
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdPosto: TdmkEdit
            Left = 424
            Top = 20
            Width = 33
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdConta: TdmkEdit
            Left = 460
            Top = 20
            Width = 93
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object dmkEdDVCta: TdmkEdit
            Left = 556
            Top = 20
            Width = 21
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object TPVencto: TDateTimePicker
            Left = 452
            Top = 64
            Width = 85
            Height = 21
            Date = 39471.044775752300000000
            Time = 39471.044775752300000000
            TabOrder = 15
          end
          object dmkEdValor: TdmkEdit
            Left = 384
            Top = 64
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 14
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdOperCodi: TdmkEdit
            Left = 296
            Top = 64
            Width = 85
            Height = 21
            TabOrder = 13
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object BitBtn1: TBitBtn
            Left = 673
            Top = 62
            Width = 105
            Height = 25
            Caption = 'Gera n'#186' bloqueto'
            TabOrder = 17
            OnClick = BitBtn1Click
          end
          object dmkEdParcTit: TdmkEdit
            Left = 580
            Top = 20
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 3
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object RGModalCobr: TRadioGroup
            Left = 4
            Top = 88
            Width = 193
            Height = 41
            Caption = ' Modalidade de cobran'#231'a: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Sem registro'
              'Registrada')
            TabOrder = 18
          end
          object RGTipoCobranca: TRadioGroup
            Left = 200
            Top = 88
            Width = 257
            Height = 41
            Caption = ' Tipo de cobran'#231'a: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Padr'#227'o'
              'SIGCB - CEF')
            TabOrder = 19
          end
          object CkZerado: TCheckBox
            Left = 652
            Top = 96
            Width = 125
            Height = 29
            Caption = 'sem valor e '#13#10'sem vcto.'
            TabOrder = 21
          end
          object EdEspecieDoc: TdmkEdit
            Left = 4
            Top = 64
            Width = 93
            Height = 21
            TabOrder = 10
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object RGCNAB: TRadioGroup
            Left = 462
            Top = 88
            Width = 188
            Height = 41
            Caption = ' CNAB (Remessa / Retorno):  '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              '000'
              '240'
              '400')
            TabOrder = 20
          end
          object EdCtaCooper: TdmkEdit
            Left = 220
            Top = 132
            Width = 93
            Height = 21
            TabOrder = 22
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLayoutRem: TdmkEdit
            Left = 428
            Top = 131
            Width = 229
            Height = 21
            CharCase = ecUpperCase
            ReadOnly = True
            TabOrder = 23
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnKeyDown = EdLayoutRemKeyDown
          end
          object GroupBox3: TGroupBox
            Left = 4
            Top = 155
            Width = 341
            Height = 50
            Caption = 'Dados do banco correspondente'
            TabOrder = 24
            object Label21: TLabel
              Left = 7
              Top = 25
              Width = 34
              Height = 13
              Caption = 'Banco:'
            end
            object Label22: TLabel
              Left = 82
              Top = 25
              Width = 42
              Height = 13
              Caption = 'Ag'#234'ncia:'
            end
            object Label23: TLabel
              Left = 178
              Top = 25
              Width = 73
              Height = 13
              Caption = 'Conta corrente:'
            end
            object EdCorresBco: TdmkEditCB
              Left = 47
              Top = 22
              Width = 29
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              IgnoraDBLookupComboBox = False
            end
            object EdCorresAge: TdmkEdit
              Left = 127
              Top = 22
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 4
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdCorresCto: TdmkEdit
              Left = 255
              Top = 22
              Width = 80
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object dmkEdCartImp: TdmkEdit
            Left = 731
            Top = 20
            Width = 47
            Height = 21
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 220
          Width = 794
          Height = 120
          Align = alTop
          ParentBackground = False
          TabOrder = 1
          object Label3: TLabel
            Left = 4
            Top = 8
            Width = 83
            Height = 13
            Caption = 'C'#243'digo de barras:'
          end
          object Label17: TLabel
            Left = 4
            Top = 52
            Width = 71
            Height = 13
            Caption = 'Linha digit'#225'vel:'
          end
          object Label18: TLabel
            Left = 496
            Top = 8
            Width = 71
            Height = 13
            Caption = 'Nosso n'#250'mero:'
          end
          object SpeedButton1: TSpeedButton
            Left = 124
            Top = 92
            Width = 23
            Height = 22
            Visible = False
            OnClick = SpeedButton1Click
          end
          object Edit3: TEdit
            Left = 4
            Top = 24
            Width = 489
            Height = 21
            ReadOnly = True
            TabOrder = 0
          end
          object Edit4: TEdit
            Left = 4
            Top = 67
            Width = 641
            Height = 21
            ReadOnly = True
            TabOrder = 1
          end
          object Edit5: TEdit
            Left = 496
            Top = 24
            Width = 149
            Height = 21
            ReadOnly = True
            TabOrder = 2
          end
          object Edit6: TEdit
            Left = 4
            Top = 92
            Width = 117
            Height = 21
            TabOrder = 3
            Text = '0'
            Visible = False
          end
          object Edit7: TEdit
            Left = 152
            Top = 92
            Width = 121
            Height = 21
            TabOrder = 4
            Visible = False
          end
        end
        object Grade: TStringGrid
          Left = 0
          Top = 340
          Width = 794
          Height = 80
          Align = alClient
          ColCount = 14
          DefaultColWidth = 32
          DefaultRowHeight = 18
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          TabOrder = 2
          Visible = False
          ColWidths = (
            32
            34
            48
            19
            30
            73
            18
            69
            38
            37
            52
            67
            67
            51)
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Descriptografa'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter1: TSplitter
          Left = 0
          Top = 165
          Width = 794
          Height = 3
          Cursor = crVSplit
          Align = alTop
          ExplicitWidth = 782
        end
        object Panel5: TPanel
          Left = 0
          Top = 168
          Width = 794
          Height = 252
          Align = alClient
          TabOrder = 0
          object StaticText2: TStaticText
            Left = 1
            Top = 1
            Width = 113
            Height = 17
            Align = alTop
            Alignment = taCenter
            Caption = 'Texto descriptografado'
            TabOrder = 0
          end
          object Memo2: TMemo
            Left = 1
            Top = 18
            Width = 792
            Height = 233
            Align = alClient
            TabOrder = 1
            WordWrap = False
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 794
          Height = 165
          Align = alTop
          TabOrder = 1
          object StaticText1: TStaticText
            Left = 1
            Top = 1
            Width = 96
            Height = 17
            Align = alTop
            Alignment = taCenter
            Caption = 'Texto criptografado'
            TabOrder = 0
          end
          object Memo1: TMemo
            Left = 1
            Top = 18
            Width = 792
            Height = 146
            Align = alClient
            TabOrder = 1
            WordWrap = False
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 804
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 756
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 708
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 351
        Height = 32
        Caption = 'C'#225'lculos de M'#243'dulos 10 e 11'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 351
        Height = 32
        Caption = 'C'#225'lculos de M'#243'dulos 10 e 11'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 351
        Height = 32
        Caption = 'C'#225'lculos de M'#243'dulos 10 e 11'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 498
    Width = 804
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 800
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 542
    Width = 804
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 658
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 656
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object Button1: TButton
        Left = 148
        Top = 12
        Width = 165
        Height = 25
        Caption = '&Imprime c'#243'digo de barras'
        TabOrder = 1
        OnClick = Button1Click
      end
      object Edit8: TEdit
        Left = 320
        Top = 15
        Width = 245
        Height = 21
        TabOrder = 2
        Text = 'yxRyxeyxqyxWyxqyxByx '
      end
    end
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bancos'
      'ORDER BY Nome')
    Left = 12
    Top = 12
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 40
    Top = 12
  end
  object frxCodBarras: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39333.805305983800000000
    ReportOptions.LastChange = 39342.853423206000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure ReportTitle1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Shape3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Memo163OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Memo162OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxCodBarrasGetValue
    Left = 68
    Top = 12
    Datasets = <>
    Variables = <
      item
        Name = ' Meu'
        Value = Null
      end
      item
        Name = 'VARI_Soma'
        Value = Null
      end
      item
        Name = 'BancoLogoExiste'
        Value = Null
      end
      item
        Name = 'EmpresaLogoExiste'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Width = 0.100000000000000000
      object BarCode1: TfrxBarCodeView
        Left = 86.929131420000000000
        Top = 83.149374410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Rotation = 0
        ShowText = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
      end
      object Line28: TfrxLineView
        Left = 3.779530000000000000
        Top = 75.590316850000000000
        Width = 691.653990000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo1: TfrxMemoView
        Left = 702.992580000000000000
        Top = 75.590600000000000000
        Width = 30.236240000000000000
        Height = 1092.284170000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '1423t,342tm34ynj4nhyj')
        ParentFont = False
        Rotation = 270
      end
    end
  end
end
