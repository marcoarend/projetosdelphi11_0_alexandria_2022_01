unit Recibos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, DBCtrls, Menus, ComCtrls, jpeg, 
  UnInternalConsts, frxClass, dmkGeral, dmkEdit, dmkMemo, UnDmkEnums;

type
  TFmRecibos = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    PopupMenu1: TPopupMenu;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    EdPPagador: TdmkEdit;
    Label11: TLabel;
    EdNumeroE: TdmkEdit;
    Label12: TLabel;
    EdValorE: TdmkEdit;
    Label13: TLabel;
    EdEPagador: TdmkEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    EdPCidade: TdmkEdit;
    EdPCEP: TdmkEdit;
    EdPPais: TdmkEdit;
    EdPTe1: TdmkEdit;
    EdPUF: TdmkEdit;
    EdPRua: TdmkEdit;
    EdPNumero: TdmkEdit;
    EdPCompl: TdmkEdit;
    EdPBairro: TdmkEdit;
    EdERuaC: TdmkEdit;
    EdECidadeC: TdmkEdit;
    EdENumeroC: TdmkEdit;
    EdEUFC: TdmkEdit;
    EdEComplC: TdmkEdit;
    EdECEPC: TdmkEdit;
    EdEPaisC: TdmkEdit;
    EdETe1C: TdmkEdit;
    EdEBairroC: TdmkEdit;
    Label9: TLabel;
    EdERuaE: TdmkEdit;
    EdECidadeE: TdmkEdit;
    EdENumeroE: TdmkEdit;
    EdEUFE: TdmkEdit;
    EdECEPE: TdmkEdit;
    EdEComplE: TdmkEdit;
    EdEPaisE: TdmkEdit;
    EdEBairroE: TdmkEdit;
    EdETe1E: TdmkEdit;
    EdEmitenteE: TdmkEdit;
    EdECNPJE: TdmkEdit;
    EdECNPJC: TdmkEdit;
    EdPCNPJC: TdmkEdit;
    Label8: TLabel;
    EdEmitenteP: TdmkEdit;
    EdPRuaE: TdmkEdit;
    EdPCidadeE: TdmkEdit;
    EdPUFE: TdmkEdit;
    EdPNumeroE: TdmkEdit;
    EdPCEPE: TdmkEdit;
    EdPComplE: TdmkEdit;
    EdPPaisE: TdmkEdit;
    EdPTe1E: TdmkEdit;
    EdPBairroE: TdmkEdit;
    EdCNPJP: TdmkEdit;
    EdValorP: TdmkEdit;
    EdNumeroP: TdmkEdit;
    TextoP: TdmkMemo;
    ExtensoP: TdmkMemo;
    ExtensoE: TdmkMemo;
    TextoE: TdmkMemo;
    LaResponsavelP: TLabel;
    Label17: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    frxRecibo1: TfrxReport;
    frxRecibo2: TfrxReport;
    frx2Recibos: TfrxReport;
    EdPResponsavel: TdmkEdit;
    EdPLocalData: TdmkEdit;
    LaResponsavelE: TLabel;
    EdEResponsavel: TdmkEdit;
    EdELocalData: TdmkEdit;
    Label23: TLabel;
    Label24: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure EdValorEChange(Sender: TObject);
    procedure EdValorPChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure frxRecibo1GetValue(const VarName: String;
      var Value: Variant);
    function frxRecibo1UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxRecibo2UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frx2RecibosUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FEmitente, FBeneficiario, FCodigo: Integer;
    FDtRecibo, FDtPrnted: TDateTime;
    //
  end;

var
  FmRecibos: TFmRecibos;

implementation

uses UnMyObjects, Module, UnDmkProcFunc, UnGrl_Vars, UnGOTOy;

{$R *.DFM}

procedure TFmRecibos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRecibos.BtImprimeClick(Sender: TObject);
  procedure CadastraImpressaoDeReciboP();
  var
    Recibo: String;
    //_DtRecibo, _DtPrnted: String;
    CNPJCPF: String;
    Valor: Double;
  begin
    if VAR_USA_FRX_IMPR_Recibo and  VAR_FRX_Imprimiu then
    begin
      Recibo     := EdNumeroP.Text;
      //_DtRecibo  := Geral.FDT(FDtRecibo, 109);
      //_DtPrnted  := Geral.FDT(DtPrnted, 109);
      CNPJCPF    := Geral.SoNumero_TT(EdPCNPJC.Text);
      Valor      := EdValorP.ValueVariant;
      //
(*    Ver o que fazer!
      GOTOy.CadastraImpressaoDeRecibo_ReciboImp(Recibo, FDtRecibo, FDtPrnted,
      CNPJCPF, FEmitente, FBeneficiario, Valor, FCodigo);
*)
    end;
  end;
  procedure CadastraImpressaoDeReciboE();
  var
    Recibo: String;
    //_DtRecibo, _DtPrnted: String;
    CNPJCPF: String;
    Valor: Double;
  begin
    if VAR_USA_FRX_IMPR_Recibo and  VAR_FRX_Imprimiu then
    begin
      Recibo     := EdNumeroE.Text;
      //_DtRecibo  := Geral.FDT(FDtRecibo, 109);
      //_DtPrnted  := Geral.FDT(DtPrnted, 109);
      CNPJCPF    := Geral.SoNumero_TT(EdECNPJC.Text);
      Valor      := EdValorE.ValueVariant;
      //
(*    Ver o que fazer!
      GOTOy.CadastraImpressaoDeRecibo_ReciboImp(Recibo, FDtRecibo, FDtPrnted,
      CNPJCPF, FEmitente, FBeneficiario, Valor, FCodigo);
*)
    end;
  end;
var
  Recibo: Integer;
begin
  Recibo := 0;
  //
  if Geral.DMV(EdValorP.Text) > 0 then Recibo := Recibo + 1;
  if Geral.DMV(EdValorE.Text) > 0 then Recibo := Recibo + 2;
  case Recibo of
    0:
    begin
      Geral.MB_Aviso('Nenhum recibo possui valor!');
    end;
    1:
    begin
      frxRecibo1.Variables['VARF_NOTCOLOR'] :=
        not Geral.IntToBool_0(Dmod.QrControle.FieldByName('CorRecibo').AsInteger);
      //
      MyObjects.frxMostra(frxRecibo1, 'Recibo');
      CadastraImpressaoDeReciboP();
    end;
    2:
    begin
      frxRecibo2.Variables['VARF_NOTCOLOR'] :=
        not Geral.IntToBool_0(Dmod.QrControle.FieldByName('CorRecibo').AsInteger);
      //
      MyObjects.frxMostra(frxRecibo2, 'Recibo');
      CadastraImpressaoDeReciboE();
    end;
    3:
    begin
      frx2Recibos.Variables['VARF_NOTCOLOR'] :=
        not Geral.IntToBool_0(Dmod.QrControle.FieldByName('CorRecibo').AsInteger);
      //
      MyObjects.frxMostra(frx2Recibos, 'Recibo duplo');
      CadastraImpressaoDeReciboE();
      CadastraImpressaoDeReciboP();
    end
    else
    begin
      Geral.MB_Erro('Erro ao definir recibo!');
    end;
  end;
end;

procedure TFmRecibos.EdValorEChange(Sender: TObject);
begin
  ExtensoE.Text := dmkPF.ExtensoMoney(Geral.TFT(EdValorE.Text, 2, siNegativo));
end;

procedure TFmRecibos.EdValorPChange(Sender: TObject);
begin
  ExtensoP.Text := dmkPF.ExtensoMoney(Geral.TFT(EdValorP.Text, 2, siNegativo));
end;

procedure TFmRecibos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmRecibos.FormCreate(Sender: TObject);
begin
  //ImgTipo.SQLType := stLok;
  //
  FEmitente       := 0;
  FBeneficiario   := 0;
  FDtRecibo       := 0;
  FDtPrnted       := Now();
end;

function TFmRecibos.frx2RecibosUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  if Name = 'VARF_NOTCOLOR' then
    Params := not Geral.IntToBool_0(Dmod.QrControle.FieldByName('CorRecibo').AsInteger);
end;

procedure TFmRecibos.frxRecibo1GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'NUMERO_P'       then Value := EdNumeroP.Text;
  if VarName = 'VALOR_P'        then Value := String(Dmod.QrControle.FieldByName('Moeda').AsString) + ' ' + EdValorP.Text;
  if VarName = 'NOME_PC'        then Value := EdPPagador.Text;
  if VarName = 'RUA_PC'         then Value := EdPRua.Text;
  if VarName = 'NUMERO_PC'      then Value := EdPNumero.Text;
  if VarName = 'CNPJ_PC'        then Value := EdPCNPJC.Text;
  if VarName = 'COMPL_PC'       then Value := EdPCompl.Text;
  if VarName = 'BAIRRO_PC'      then Value := EdPBairro.Text;
  if VarName = 'CIDADE_PC'      then Value := EdPCidade.Text;
  if VarName = 'UF_PC'          then Value := EdPUF.Text;
  if VarName = 'CEP_PC'         then Value := EdPCEP.Text;
  if VarName = 'PAIS_PC'        then Value := EdPPais.Text;
  if VarName = 'TE1_PC'         then Value := EdPTe1.Text;
  if VarName = 'EXTENSO_P'      then Value := ExtensoP.Text;
  if VarName = 'MOTIVO_P'       then Value := TextoP.Text;
  if VarName = 'EMITENTEP'      then Value := EdEmitenteP.Text;
  if VarName = 'CNPJ_PE'        then Value := EdCNPJP.Text;
  if VarName = 'RUA_PE'         then Value := EdPRuaE.Text;
  if VarName = 'NUMERO_PE'      then Value := EdPNumeroE.Text;
  if VarName = 'COMPL_PE'       then Value := EdPComplE.Text;
  if VarName = 'BAIRRO_PE'      then Value := EdPBairroE.Text;
  if VarName = 'CIDADE_PE'      then Value := EdPCidadeE.Text;
  if VarName = 'UF_PE'          then Value := EdPUFE.Text;
  if VarName = 'CEP_PE'         then Value := EdPCEPE.Text;
  if VarName = 'PAIS_PE'        then Value := EdPPaisE.Text;
  if VarName = 'TE1_PE'         then Value := EdPTe1E.Text;
  //
  if VarName = 'NUMERO_E'       then Value := EdNumeroE.Text;
  if VarName = 'VALOR_E'        then Value := String(Dmod.QrControle.FieldByName('Moeda').AsString) + ' ' + EdValorE.Text;
  if VarName = 'NOME_EC'        then Value := EdEPagador.Text;
  if VarName = 'RUA_EC'         then Value := EdERuaC.Text;
  if VarName = 'NUMERO_EC'      then Value := EdENumeroC.Text;
  if VarName = 'CNPJ_EC'        then Value := EdECNPJC.Text;
  if VarName = 'COMPL_EC'       then Value := EdEComplC.Text;
  if VarName = 'BAIRRO_EC'      then Value := EdEBairroC.Text;
  if VarName = 'CIDADE_EC'      then Value := EdECidadeC.Text;
  if VarName = 'UF_EC'          then Value := EdEUFC.Text;
  if VarName = 'CEP_EC'         then Value := EdECEPC.Text;
  if VarName = 'PAIS_EC'        then Value := EdEPaisC.Text;
  if VarName = 'TE1_EC'         then Value := EdETe1C.Text;
  if VarName = 'EXTENSO_E'      then Value := ExtensoE.Text;
  if VarName = 'MOTIVO_E'       then Value := TextoE.Text;
  if VarName = 'EMITENTEE'      then Value := EdEmitenteP.Text;
  if VarName = 'CNPJ_EE'        then Value := EdCNPJP.Text;
  if VarName = 'RUA_EE'         then Value := EdERuaE.Text;
  if VarName = 'NUMERO_EE'      then Value := EdENumeroE.Text;
  if VarName = 'COMPL_EE'       then Value := EdEComplE.Text;
  if VarName = 'BAIRRO_EE'      then Value := EdEBairroE.Text;
  if VarName = 'CIDADE_EE'      then Value := EdECidadeE.Text;
  if VarName = 'UF_EE'          then Value := EdEUFE.Text;
  if VarName = 'CEP_EE'         then Value := EdECEPE.Text;
  if VarName = 'PAIS_EE'        then Value := EdEPaisE.Text;
  if VarName = 'TE1_EE'         then Value := EdETe1E.Text;
  //
  if VarName = 'RESPONSAVEL_E'  then Value := EdEResponsavel.Text;
  if VarName = 'RESPONSAVEL_P'  then Value := EdPResponsavel.Text;
  if VarName = 'LOCAL_E_DATA_P' then Value := EdPLocalData.Text;
  if VarName = 'LOCAL_E_DATA_E' then Value := EdELocalData.Text;
end;

function TFmRecibos.frxRecibo1UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  if Name = 'VARF_NOTCOLOR' then
    Params := not Geral.IntToBool_0(Dmod.QrControle.FieldByName('CorRecibo').AsInteger);
end;

function TFmRecibos.frxRecibo2UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  if Name = 'VARF_NOTCOLOR' then
    Params := not Geral.IntToBool_0(Dmod.QrControle.FieldByName('CorRecibo').AsInteger);
end;

end.
