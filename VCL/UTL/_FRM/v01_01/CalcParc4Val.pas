unit CalcParc4Val;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditCalc, dmkEdit;

type
  TFmCalcParc4Val = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    EdA1: TdmkEdit;
    LaA1: TLabel;
    LaA2: TLabel;
    EdA2: TdmkEditCalc;
    EdA3: TdmkEdit;
    LaA3: TLabel;
    EdB1: TdmkEdit;
    LaB1: TLabel;
    LaB2: TLabel;
    EdB2: TdmkEditCalc;
    EdB3: TdmkEdit;
    LaB3: TLabel;
    Ed03Peca: TdmkEdit;
    LaC1: TLabel;
    LaC2: TLabel;
    Ed03ArM2: TdmkEditCalc;
    Ed03Peso: TdmkEdit;
    LaC3: TLabel;
    EdA4: TdmkEdit;
    LaA4: TLabel;
    LaB4: TLabel;
    EdB4: TdmkEdit;
    LaC4: TLabel;
    Ed03Valr: TdmkEdit;
    LaD1: TLabel;
    Ed04Peca: TdmkEdit;
    Ed04ArM2: TdmkEditCalc;
    LaD2: TLabel;
    LaD3: TLabel;
    Ed04Peso: TdmkEdit;
    Ed04Valr: TdmkEdit;
    LaD4: TLabel;
    MeHelp: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdA1Change(Sender: TObject);
    procedure EdA2Change(Sender: TObject);
    procedure EdA3Change(Sender: TObject);
    procedure EdA4Change(Sender: TObject);
    procedure EdB2Change(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FUsaDados, FCalcAuto: Boolean;
    FCalcExec: TCalcExec;
  end;

  var
  FmCalcParc4Val: TFmCalcParc4Val;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmCalcParc4Val.BtOKClick(Sender: TObject);
var
  Executou: Boolean;
  Fator, Pecas, Peso: Double;
begin
  Executou := True;
  case FCalcExec of
    calcexec1:
    begin
      // Pe�as 3 = Area 3 / Area 4 * Pe�as 4
      Ed03Peca.ValueVariant :=
        Round(Ed03ArM2.ValueVariant / Ed04ArM2.ValueVariant * Ed04Peca.ValueVariant);
    end;
    calcexec2:
    begin
      // Pe�as 3 = Area 3 / Area 4 * Pe�as 4
      Pecas := EdB1.ValueVariant - EdA1.ValueVariant;
      if Pecas <= Ed03Peca.ValueVariant then
        Ed04Peca.ValueVariant := Pecas
      else
      begin
        Geral.MB_Aviso('Quantidade de pe�as insuficiente!');
        Ed04Peca.ValueVariant := Ed03Peca.ValueVariant;
      end;
      if Ed03Peca.ValueVariant > 0 then
        Peso := Ed04Peca.ValueVariant / Ed03Peca.ValueVariant * Ed03Peso.ValueVariant
      else
        Peso := 0;
      Ed04Peso.ValueVariant := Peso;
      //
    end;
    calcexecPecasPorRendArea:
    begin
      Fator := Ed03ArM2.ValueVariant / EdB2.ValueVariant;
      Ed03Peca.ValueVariant := Fator * EdA1.ValueVariant;
      Ed04ArM2.ValueVariant := Fator * EdA2.ValueVariant;
    end;
    //
    calcexecPecasPorKgTotalDivMediaKgOri:
    begin
      Fator := EdA3.ValueVariant * (1 + (EdA4.ValueVariant / 100));
      Pecas := Round(EdB2.ValueVariant / Fator);
      EdB1.ValueVariant := Pecas;
    end;
    else
    begin
      Geral.MB_Erro('"CalcExec" n�o implementado!');
      Executou := False;
    end;
  end;
  if Executou then
  begin
    FUsaDados := True;
    Close;
  end;
end;

procedure TFmCalcParc4Val.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCalcParc4Val.EdA1Change(Sender: TObject);
begin
  if FCalcAuto then
    Ed03Peca.ValueVariant := EdA1.ValueVariant - EdB1.ValueVariant;
end;

procedure TFmCalcParc4Val.EdA2Change(Sender: TObject);
begin
  if FCalcAuto then
    Ed03ArM2.ValueVariant := EdA2.ValueVariant - EdB2.ValueVariant;
end;

procedure TFmCalcParc4Val.EdB2Change(Sender: TObject);
begin
  if FCalcAuto then
    Ed03ArM2.ValueVariant := EdA2.ValueVariant - EdB2.ValueVariant;
end;

procedure TFmCalcParc4Val.EdA3Change(Sender: TObject);
begin
  if FCalcAuto then
    Ed03Peso.ValueVariant := EdA3.ValueVariant - EdB3.ValueVariant;
end;

procedure TFmCalcParc4Val.EdA4Change(Sender: TObject);
begin
  if FCalcAuto then
    Ed03Valr.ValueVariant := EdA4.ValueVariant - EdB4.ValueVariant;
end;

procedure TFmCalcParc4Val.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCalcParc4Val.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUsaDados := False;
  FCalcAuto := False;
end;

procedure TFmCalcParc4Val.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
