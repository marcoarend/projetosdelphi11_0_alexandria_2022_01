unit CodBarraRead;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmCodBarraRead = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    RGDirecao: TRadioGroup;
    Panel5: TPanel;
    MeLeitura: TMemo;
    Memo1: TMemo;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    LaLast1: TLabel;
    LaLast2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGDirecaoClick(Sender: TObject);
    procedure MeLeituraChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function AvisaDirecaoInvalida(): Boolean;
  end;

  var
  FmCodBarraRead: TFmCodBarraRead;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral, CodBarraSQLy;

{$R *.DFM}

function TFmCodBarraRead.AvisaDirecaoInvalida(): Boolean;
begin
  Result := False;
  //
  Geral.MB_Aviso(
  '"Direcionamento da informa��o inv�lido ou n�o implementado!"' +
  sLineBreak +
  'Selecione o "Direcionamento" desejado e repita a leitura!');
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
 'Selecione o "Direcionamento" desejado e repita a leitura!');
end;

procedure TFmCodBarraRead.BtOKClick(Sender: TObject);
var
  I: Integer;
begin
  Memo1.Lines.Clear;
  for I := 1 to Length(MeLeitura.Text) do
  begin
    Memo1.Lines.Add(Geral.FFN(Ord(MeLeitura.Text[I]), 3));
  end;
end;

procedure TFmCodBarraRead.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCodBarraRead.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCodBarraRead.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.Informa2(LaLast1, LaLast2, False, '');
end;

procedure TFmCodBarraRead.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCodBarraRead.MeLeituraChange(Sender: TObject);
var
  Leitura: String;
begin
  if MeLeitura.Lines.Count > 1 then
  begin
    Screen.Cursor := crHourGlass;
    try
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Processando dados da leitura');
      Leitura := MeLeitura.Lines[0];
      MyObjects.Informa2(LaLast1, LaLast2, False, Leitura);
      MeLeitura.Lines.Clear;
      //
      if not UnCodBarraSQLy.DecifraTipoDeCodigoDeBarras(Leitura,
      RGDirecao.ItemIndex, LaAviso1, LaAviso2, LaLast1, LaLast2) then
      begin
        Geral.MB_Erro('Dados da leitura n�o foram reconhecidos!' +
        sLineBreak + 'AVISE A DERMATEK!');
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Dados da leitura n�o foram reconhecidos!');
        Exit;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmCodBarraRead.RGDirecaoClick(Sender: TObject);
begin
  MeLeitura.Visible := RGDirecao.Itemindex >= 0;
  MeLeitura.SetFocus;
end;

end.
