object FmOpcoes: TFmOpcoes
  Left = 338
  Top = 175
  Caption = 'FER-OPCAO-001 :: Op'#231#245'es do Aplicativo'
  ClientHeight = 690
  ClientWidth = 926
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 926
    Height = 528
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 926
      Height = 528
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 926
        Height = 528
        ActivePage = TabSheet2
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Geral'
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 918
            Height = 500
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 488
              Height = 500
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label3: TLabel
                Left = 40
                Top = 208
                Width = 214
                Height = 13
                Caption = 'Caminho do logo para relat'#243'rios normais [F4]: '
              end
              object Label14: TLabel
                Left = 40
                Top = 248
                Width = 310
                Height = 13
                Caption = 
                  'Caminho do logo (m'#225'x. 64 X 168) para relat'#243'rios espec'#237'ficos [F4]' +
                  ': '
              end
              object Label5: TLabel
                Left = 40
                Top = 288
                Width = 263
                Height = 13
                Caption = 'Caminho do logo (m'#225'x. 64 X 168) para Nota Fiscal [F4]: '
              end
              object Label19: TLabel
                Left = 40
                Top = 328
                Width = 254
                Height = 13
                Caption = 'Caminho do logo gigante (propor'#231#227'o: 2,5 x 19,0) [F4]: '
              end
              object Label24: TLabel
                Left = 204
                Top = 140
                Width = 150
                Height = 13
                Caption = 'Senha de delega'#231#227'o provis'#243'ria:'
              end
              object Label27: TLabel
                Left = 40
                Top = 188
                Width = 311
                Height = 13
                Caption = 'Diferen'#231'a m'#225'xima aceit'#225'vel por item na entrada de pedido x NFe: '
              end
              object GroupBox5: TGroupBox
                Left = 40
                Top = 4
                Width = 301
                Height = 61
                Caption = ' Cidade - UF: '
                TabOrder = 0
                Visible = False
                object Label2: TLabel
                  Left = 64
                  Top = 16
                  Width = 72
                  Height = 13
                  Caption = 'Cidade padr'#227'o:'
                end
                object Label1: TLabel
                  Left = 8
                  Top = 16
                  Width = 53
                  Height = 13
                  Caption = 'UF padr'#227'o:'
                end
                object CBUF: TDBLookupComboBox
                  Left = 8
                  Top = 32
                  Width = 53
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsUFs
                  TabOrder = 0
                end
                object EdCidade: TdmkEdit
                  Left = 64
                  Top = 32
                  Width = 137
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'Maring'#225
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'Maring'#225
                  ValWarn = False
                end
                object CkTravaCidade: TCheckBox
                  Left = 208
                  Top = 36
                  Width = 85
                  Height = 17
                  Caption = 'Trava cidade.'
                  TabOrder = 2
                end
              end
              object BtCorrige: TBitBtn
                Left = 350
                Top = 20
                Width = 112
                Height = 44
                Cursor = crHandPoint
                Caption = 'Corrigir Logradouros'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                Visible = False
                OnClick = BtCorrigeClick
              end
              object CkMaiusculas: TCheckBox
                Left = 40
                Top = 68
                Width = 169
                Height = 17
                Caption = 'Permitir somente mai'#250'sculas.'
                TabOrder = 2
                OnClick = CkMaiusculasClick
              end
              object CkSolicitaSenha: TCheckBox
                Left = 40
                Top = 86
                Width = 284
                Height = 17
                Caption = 'Solicitar senha ao iniciar aplicativo.'
                TabOrder = 3
              end
              object CkEntraSemValor: TCheckBox
                Left = 40
                Top = 104
                Width = 284
                Height = 17
                Caption = 'Permitir entradas no estoque inicial sem valor.'
                TabOrder = 4
              end
              object GroupBox3: TGroupBox
                Left = 40
                Top = 120
                Width = 157
                Height = 61
                Caption = ' Juros e multa: '
                TabOrder = 5
                object Label9: TLabel
                  Left = 12
                  Top = 20
                  Width = 42
                  Height = 13
                  Caption = 'Mora dd:'
                end
                object Label10: TLabel
                  Left = 84
                  Top = 20
                  Width = 29
                  Height = 13
                  Caption = 'Multa:'
                end
                object EdMoraDD: TdmkEdit
                  Left = 12
                  Top = 36
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 6
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdMulta: TdmkEdit
                  Left = 84
                  Top = 36
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object EdMeuLogoPath: TdmkEdit
                Left = 40
                Top = 224
                Width = 440
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnKeyDown = EdMeuLogoPathKeyDown
              end
              object EdLogo0Path: TdmkEdit
                Left = 40
                Top = 264
                Width = 440
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnKeyDown = EdLogo0PathKeyDown
              end
              object EdLogoNF: TdmkEdit
                Left = 40
                Top = 304
                Width = 440
                Height = 21
                TabOrder = 8
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnKeyDown = EdLogoNFKeyDown
              end
              object EdLogoBig1: TdmkEdit
                Left = 40
                Top = 343
                Width = 440
                Height = 21
                TabOrder = 9
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnKeyDown = EdLogoBig1KeyDown
              end
              object CkCorRecibo: TCheckBox
                Left = 40
                Top = 366
                Width = 213
                Height = 17
                Caption = 'Imprime recibo com fundo colorido.'
                TabOrder = 10
              end
              object EdPwdLibFunc: TdmkEdit
                Left = 204
                Top = 156
                Width = 153
                Height = 21
                TabOrder = 11
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '123'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '123'
                ValWarn = False
              end
              object CkExigeNumDocCNPJCPF: TCheckBox
                Left = 240
                Top = 68
                Width = 261
                Height = 17
                Caption = 'Exige CNPJ/CPF no cadastro de entidade.'
                TabOrder = 12
                OnClick = CkMaiusculasClick
              end
              object EdDifMaxPedNFe: TdmkEdit
                Left = 356
                Top = 184
                Width = 68
                Height = 21
                Alignment = taRightJustify
                TabOrder = 13
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
            object Panel7: TPanel
              Left = 488
              Top = 0
              Width = 430
              Height = 500
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object Label21: TLabel
                Left = 0
                Top = 289
                Width = 237
                Height = 13
                Caption = 'Caminho na rede da pasta Dermatek (N'#227'o altere!):'
              end
              object SBDmkNetPath: TSpeedButton
                Left = 264
                Top = 304
                Width = 21
                Height = 21
                Caption = '...'
                Enabled = False
                OnClick = SBDmkNetPathClick
              end
              object LaHora: TLabel
                Left = 0
                Top = 348
                Width = 226
                Height = 13
                Caption = 'Toler'#226'ncia de escape de backup automatizado:'
              end
              object GroupBox6: TGroupBox
                Left = 0
                Top = 0
                Width = 430
                Height = 277
                Align = alTop
                Caption = ' Cheques: '
                TabOrder = 0
                object LaPortaImpCheque: TLabel
                  Left = 12
                  Top = 160
                  Width = 155
                  Height = 13
                  Caption = 'Porta da impressora de cheques:'
                  Enabled = False
                end
                object LaEdImpreCh_IP: TLabel
                  Left = 12
                  Top = 200
                  Width = 68
                  Height = 13
                  Caption = 'IP do servidor:'
                end
                object LaEdImpreChPorta: TLabel
                  Left = 104
                  Top = 200
                  Width = 28
                  Height = 13
                  Caption = 'Porta:'
                end
                object RGImpreCheque: TRadioGroup
                  Left = 12
                  Top = 15
                  Width = 165
                  Height = 138
                  Caption = ' Impressora de Cheques: '
                  ItemIndex = 0
                  Items.Strings = (
                    'Em outro computador'
                    'PertoChek Paralela'
                    'PertoChek Serial'
                    'Bematech DP-20'
                    'ESC P 9 pinos (LX 300)')
                  TabOrder = 0
                  OnClick = RGImpreChequeClick
                end
                object EdPortaImpCheque: TdmkEdit
                  Left = 12
                  Top = 176
                  Width = 165
                  Height = 21
                  Enabled = False
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'COM2'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'COM2'
                  ValWarn = False
                  OnChange = EdPortaImpChequeChange
                end
                object EdImpreCh_IP: TdmkEdit
                  Left = 12
                  Top = 216
                  Width = 90
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '127.0.0.1'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = '127.0.0.1'
                  ValWarn = False
                  OnChange = EdImpreCh_IPChange
                end
                object EdImpreChPorta: TdmkEdit
                  Left = 104
                  Top = 216
                  Width = 73
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '9520'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 9520
                  ValWarn = False
                  OnChange = EdImpreChPortaChange
                end
              end
              object EdDmkNetPath: TdmkEdit
                Left = 0
                Top = 304
                Width = 261
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdLastBckpIntrvl: TdmkEdit
                Left = 232
                Top = 344
                Width = 53
                Height = 21
                TabOrder = 2
                FormatType = dmktfTime
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '00:00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Financeiro'
          ImageIndex = 1
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 918
            Height = 500
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label48: TLabel
              Left = 352
              Top = 316
              Width = 229
              Height = 13
              Caption = 
                'Vazio para usar !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!' +
                '!!:'
              Visible = False
            end
            object Label25: TLabel
              Left = 20
              Top = 336
              Width = 109
              Height = 13
              Caption = 'Limite de cr'#233'dito inicial:'
            end
            object CkAvisaVendedor: TCheckBox
              Left = 16
              Top = 16
              Width = 285
              Height = 17
              Caption = 'Avisar sele'#231#227'o do vendedor em lan'#231'amentos.'
              TabOrder = 0
            end
            object CkAvisaRepresent: TCheckBox
              Left = 16
              Top = 36
              Width = 285
              Height = 17
              Caption = 'Avisar sele'#231#227'o do representante em lan'#231'amentos.'
              TabOrder = 1
            end
            object CkAvisaCliInt: TCheckBox
              Left = 16
              Top = 56
              Width = 285
              Height = 17
              Caption = 'Avisar sele'#231#227'o do cliente interno em lan'#231'amentos.'
              TabOrder = 2
            end
            object CkAvisaDescoPor: TCheckBox
              Left = 16
              Top = 76
              Width = 285
              Height = 17
              Caption = 'Avisar sele'#231#227'o do descontador de duplicata.'
              TabOrder = 3
            end
            object CkMensalSempre: TCheckBox
              Left = 16
              Top = 96
              Width = 285
              Height = 17
              Caption = 'Exigir sempre o m'#234's de compet'#234'ncia do lan'#231'amento.'
              TabOrder = 4
            end
            object CkDuplicLct: TCheckBox
              Left = 16
              Top = 116
              Width = 285
              Height = 17
              Caption = 'Permite duplica'#231#227'o (c'#243'pia) de lan'#231'amento.'
              TabOrder = 5
            end
            object GroupBox4: TGroupBox
              Left = 16
              Top = 200
              Width = 325
              Height = 109
              Caption = ' Meus Pagtos: '
              TabOrder = 7
              object Label11: TLabel
                Left = 12
                Top = 16
                Width = 39
                Height = 13
                Caption = 'Carteira:'
              end
              object Label13: TLabel
                Left = 12
                Top = 60
                Width = 44
                Height = 13
                Caption = 'Parcelas:'
              end
              object EdCarteira: TdmkEditCB
                Left = 12
                Top = 32
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBCarteira
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBCarteira: TdmkDBLookupComboBox
                Left = 68
                Top = 32
                Width = 248
                Height = 21
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCarteiras1
                TabOrder = 1
                dmkEditCB = EdCarteira
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdMyPgQtdP: TdmkEdit
                Left = 12
                Top = 76
                Width = 45
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object RGMyPgPeri: TRadioGroup
                Left = 64
                Top = 56
                Width = 147
                Height = 45
                Caption = ' Periodo: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'Mensal'
                  '             dias')
                TabOrder = 3
              end
              object EdMyPgDias: TdmkEdit
                Left = 158
                Top = 72
                Width = 25
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '7'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 7
                ValWarn = False
              end
            end
            object GroupBox1: TGroupBox
              Left = 352
              Top = 200
              Width = 325
              Height = 109
              Caption = ' Vendas: '
              TabOrder = 8
              object Label6: TLabel
                Left = 12
                Top = 16
                Width = 39
                Height = 13
                Caption = 'Carteira:'
              end
              object Label7: TLabel
                Left = 12
                Top = 60
                Width = 44
                Height = 13
                Caption = 'Parcelas:'
              end
              object EdVendaCartPg: TdmkEditCB
                Left = 12
                Top = 32
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBVendaCartPg
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBVendaCartPg: TdmkDBLookupComboBox
                Left = 68
                Top = 32
                Width = 248
                Height = 21
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCarteiras2
                TabOrder = 1
                dmkEditCB = EdVendaCartPg
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdVendaParcPg: TdmkEdit
                Left = 12
                Top = 76
                Width = 45
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object RGVendaPeriPg: TRadioGroup
                Left = 64
                Top = 56
                Width = 138
                Height = 45
                Caption = ' Periodo: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'Mensal'
                  '            dias')
                TabOrder = 3
              end
              object EdVendaDiasPg: TdmkEdit
                Left = 153
                Top = 72
                Width = 24
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '7'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 7
                ValWarn = False
              end
            end
            object EdDescoConta: TdmkEditCB
              Left = 352
              Top = 332
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              Visible = False
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBDescoConta
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBDescoConta: TdmkDBLookupComboBox
              Left = 408
              Top = 332
              Width = 265
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsPlaNiv5Jur
              TabOrder = 10
              Visible = False
              dmkEditCB = EdDescoConta
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object RGVctAutDefU: TRadioGroup
              Left = 324
              Top = 12
              Width = 441
              Height = 61
              Caption = ' Compensa'#231#227'o autom'#225'tica de cheques comprados: '
              ItemIndex = 0
              Items.Strings = (
                'Autom'#225'tica na primeira execu'#231#227'o do aplicativo (uma vez por dia).'
                'Somente manual.')
              TabOrder = 6
            end
            object CkUsaGenCtb: TCheckBox
              Left = 20
              Top = 312
              Width = 285
              Height = 17
              Caption = 'Exige conta cont'#225'bil nos lan'#231'amentos.'
              TabOrder = 11
            end
            object EdLimiCredIni: TdmkEdit
              Left = 20
              Top = 352
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 12
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object CkConcatNivOrdCta: TCheckBox
              Left = 324
              Top = 76
              Width = 445
              Height = 17
              Caption = 
                'Usar Ordena'#231#227'o ao inv'#233's de codifica'#231#227'o na  concatena'#231#227'o de n'#237'vei' +
                's de contas.'
              TabOrder = 13
            end
            object CkUsarFinCtb: TCheckBox
              Left = 324
              Top = 96
              Width = 445
              Height = 17
              Caption = 'Usar financeiro voltado ao cont'#225'bil.'
              TabOrder = 14
            end
            object CkUsarCtbEstqParaMP: TCheckBox
              Left = 324
              Top = 116
              Width = 445
              Height = 17
              Caption = 'Usar contas cont'#225'beis de estoque na entrada de mat'#233'ria-prima'
              TabOrder = 15
            end
            object CkUsarCtbEstqParaUC: TCheckBox
              Left = 324
              Top = 136
              Width = 445
              Height = 17
              Caption = 'Usar contas cont'#225'beis de estoque na entrada de uso e consumo.'
              TabOrder = 16
            end
            object CkUsarEntraFiscal: TCheckBox
              Left = 324
              Top = 156
              Width = 445
              Height = 17
              Caption = 'Usar entrada no estoque voltado ao fiscal'
              TabOrder = 17
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Impress'#227'o matricial'
          ImageIndex = 2
          object Memo1: TMemo
            Left = 571
            Top = 0
            Width = 347
            Height = 500
            Align = alRight
            Font.Charset = OEM_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Terminal'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object BtImprime: TBitBtn
            Tag = 100
            Left = 12
            Top = 228
            Width = 253
            Height = 40
            Caption = 'Imprime caracteres matriciais da impressora'
            TabOrder = 1
            OnClick = BtImprimeClick
          end
          object Memo2: TMemo
            Left = 12
            Top = 272
            Width = 253
            Height = 113
            Font.Charset = OEM_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Terminal'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Cobran'#231'a (CNAB)'
          ImageIndex = 3
          object Label8: TLabel
            Left = 12
            Top = 4
            Width = 115
            Height = 13
            Caption = 'Conta de juros de mora'#185':'
          end
          object Label15: TLabel
            Left = 12
            Top = 44
            Width = 141
            Height = 13
            Caption = 'Conta de cobran'#231'a de tarifa '#178':'
          end
          object Label16: TLabel
            Left = 396
            Top = 4
            Width = 79
            Height = 13
            Caption = 'Conta de multas:'
            Enabled = False
          end
          object Label17: TLabel
            Left = 16
            Top = 92
            Width = 734
            Height = 13
            Caption = 
              #185' Cr'#233'dito. Conta das cobran'#231'as de (diferen'#231'a de) juros sobre blo' +
              'quetos anteriores pagos em atraso que tiveram juros calculados a' +
              ' menor no seu pagamento.'
          end
          object Label18: TLabel
            Left = 16
            Top = 108
            Width = 391
            Height = 13
            Caption = 
              #178' D'#233'bito. Conta da tarifa cobrada pelos bancos sobre os pagament' +
              'os de bloquetos.'
          end
          object EdCNABCtaJur: TdmkEditCB
            Left = 12
            Top = 20
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCNABCtaJur
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCNABCtaJur: TdmkDBLookupComboBox
            Left = 80
            Top = 20
            Width = 313
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPlaNiv5Jur
            TabOrder = 1
            dmkEditCB = EdCNABCtaJur
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCNABCtaTar: TdmkEditCB
            Left = 12
            Top = 60
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCNABCtaTar
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCNABCtaTar: TdmkDBLookupComboBox
            Left = 80
            Top = 60
            Width = 313
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPlaNiv5Tar
            TabOrder = 3
            dmkEditCB = EdCNABCtaTar
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCNABCtaMul: TdmkEditCB
            Left = 396
            Top = 20
            Width = 65
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCNABCtaMul
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCNABCtaMul: TdmkDBLookupComboBox
            Left = 465
            Top = 20
            Width = 313
            Height = 21
            Enabled = False
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPlaNiv5Jur
            TabOrder = 5
            dmkEditCB = EdCNABCtaMul
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'C'#226'mbio e moedas'
          ImageIndex = 4
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 918
            Height = 69
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object BtUsuario: TBitBtn
              Tag = 24
              Left = 14
              Top = 13
              Width = 112
              Height = 44
              Cursor = crHandPoint
              Caption = '&Usu'#225'rios'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtUsuarioClick
            end
            object GroupBox2: TGroupBox
              Left = 144
              Top = 0
              Width = 241
              Height = 61
              Caption = ' Moeda: '
              TabOrder = 1
              object Label4: TLabel
                Left = 12
                Top = 16
                Width = 26
                Height = 13
                Caption = 'Sigla:'
              end
              object Label20: TLabel
                Left = 56
                Top = 16
                Width = 69
                Height = 13
                Caption = 'Moeda oficial: '
              end
              object EdMoeda: TdmkEdit
                Left = 12
                Top = 32
                Width = 41
                Height = 21
                MaxLength = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '$'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '$'
                ValWarn = False
              end
              object EdMoedaBr: TdmkEditCB
                Left = 56
                Top = 32
                Width = 33
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBMoedaBr
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBMoedaBr: TdmkDBLookupComboBox
                Left = 92
                Top = 32
                Width = 141
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCambioMda
                TabOrder = 2
                dmkEditCB = EdMoedaBr
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
          end
          object DBGCambioUsu: TDBGrid
            Left = 0
            Top = 69
            Width = 918
            Height = 431
            Align = alClient
            DataSource = DsCambioUsu
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Numero'
                Title.Caption = 'Usu'#225'rio'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Login'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Funcionario'
                Title.Caption = 'Funcion'#225'rio'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEFUNCI'
                Title.Caption = 'Nome funcion'#225'rio'
                Width = 428
                Visible = True
              end>
          end
        end
        object TabSheet6: TTabSheet
          Caption = 'Conex'#227'o'
          ImageIndex = 5
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 918
            Height = 500
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox7: TGroupBox
              Left = 13
              Top = 5
              Width = 390
              Height = 150
              Caption = ' Proxy: '
              TabOrder = 0
              object Label58: TLabel
                Left = 7
                Top = 102
                Width = 34
                Height = 13
                Caption = 'Senha:'
              end
              object Label59: TLabel
                Left = 7
                Top = 60
                Width = 39
                Height = 13
                Caption = 'Usu'#225'rio:'
              end
              object Label60: TLabel
                Left = 254
                Top = 60
                Width = 39
                Height = 13
                Caption = 'Sevidor:'
              end
              object Label61: TLabel
                Left = 254
                Top = 102
                Width = 28
                Height = 13
                Caption = 'Porta:'
              end
              object CkProxUse: TdmkCheckBox
                Left = 7
                Top = 16
                Width = 130
                Height = 17
                Caption = 'Usar conex'#227'o'
                TabOrder = 0
                UpdType = utYes
                ValCheck = '1'
                ValUncheck = '0'
                OldValor = #0
              end
              object CkProxBAut: TdmkCheckBox
                Left = 7
                Top = 36
                Width = 130
                Height = 17
                Caption = 'Autentica'#231#227'o b'#225'sica'
                TabOrder = 1
                UpdType = utYes
                ValCheck = '1'
                ValUncheck = '0'
                OldValor = #0
              end
              object EdProxUser: TdmkEdit
                Left = 7
                Top = 76
                Width = 240
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Web_FTPu'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdProxServ: TdmkEdit
                Left = 254
                Top = 76
                Width = 131
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Web_FTPu'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdProxPort: TdmkEdit
                Left = 254
                Top = 119
                Width = 131
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Web_FTPu'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdProxPass: TEdit
                Left = 7
                Top = 119
                Width = 240
                Height = 22
                Font.Charset = SYMBOL_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Wingdings'
                Font.Style = []
                ParentFont = False
                PasswordChar = 'l'
                TabOrder = 4
              end
            end
            object GroupBox8: TGroupBox
              Left = 13
              Top = 163
              Width = 492
              Height = 97
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Caption = ' Configura'#231#245'es de inatividade (em minutos) '
              TabOrder = 1
              object Label12: TLabel
                Left = 7
                Top = 20
                Width = 388
                Height = 13
                Caption = 
                  'Abrir janela de senha ap'#243's (esta op'#231#227'o sobrescreve a op'#231#227'o "Reco' +
                  'nectar ap'#243's"):'
              end
              object Label22: TLabel
                Left = 7
                Top = 47
                Width = 327
                Height = 13
                Caption = 
                  'Reconectar ap'#243's (esta op'#231#227'o deve ser configurada por computador)' +
                  ':'
              end
              object Label23: TLabel
                Left = 7
                Top = 73
                Width = 427
                Height = 13
                Caption = 
                  'OBS.: Para n'#227'o utilizar as configura'#231#245'es acima preencha o valor ' +
                  '"0 (zero)"'
                Color = clBtnFace
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentColor = False
                ParentFont = False
              end
              object EdIdleMinutos: TdmkEdit
                Left = 418
                Top = 18
                Width = 69
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '10'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 10
                ValWarn = False
              end
              object EdIdlePingMin: TdmkEdit
                Left = 418
                Top = 45
                Width = 69
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '10'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 10
                ValWarn = False
              end
            end
          end
        end
        object TabSheet7: TTabSheet
          Caption = 'Miscel'#226'nea'
          ImageIndex = 6
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 918
            Height = 500
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label26: TLabel
              Left = 16
              Top = 192
              Width = 70
              Height = 13
              Caption = 'Token CNPJa:'
            end
            object RGAbaIniApp: TdmkRadioGroup
              Left = 4
              Top = 4
              Width = 773
              Height = 153
              Caption = ' Aba selecionada ao iniciar aplicativo:'
              Columns = 5
              TabOrder = 0
              UpdType = utYes
              OldValor = 0
            end
            object CkMyPathsFrx: TdmkCheckBox
              Left = 16
              Top = 168
              Width = 441
              Height = 17
              Caption = 
                'Usar caminho sugerido pel aplicativo para exporta'#231#227'o PDF na jane' +
                'la de impress'#227'o.'
              TabOrder = 1
              QryCampo = 'MyPathsFrx'
              UpdCampo = 'MyPathsFrx'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
            object EdCNPJaToken: TdmkEdit
              Left = 16
              Top = 208
              Width = 757
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'CNPJaToken'
              UpdCampo = 'CNPJaToken'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 926
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 878
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 830
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 255
        Height = 32
        Caption = 'Op'#231#245'es do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 255
        Height = 32
        Caption = 'Op'#231#245'es do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 255
        Height = 32
        Caption = 'Op'#231#245'es do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 576
    Width = 926
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 922
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 620
    Width = 926
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 780
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 44
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 778
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtSalvar: TBitBtn
        Tag = 24
        Left = 18
        Top = 3
        Width = 120
        Height = 44
        Cursor = crHandPoint
        Caption = '&Salvar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSalvarClick
      end
    end
  end
  object QrUFs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM ufs'
      'ORDER BY Nome')
    Left = 797
    Top = 185
    object QrUFsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUFsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 2
    end
  end
  object DsUFs: TDataSource
    DataSet = QrUFs
    Left = 825
    Top = 185
  end
  object QrCarteiras1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo '
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 524
    Top = 132
    object QrCarteiras1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteiras1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteiras1Tipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsCarteiras1: TDataSource
    DataSet = QrCarteiras1
    Left = 552
    Top = 132
  end
  object PMImprime: TPopupMenu
    Left = 661
    Top = 193
    object Impressora1: TMenuItem
      Caption = '&Impressora (LPT1)'
      OnClick = Impressora1Click
    end
    object ArquivoMemoaolado1: TMenuItem
      Caption = '&Arquivo (Memo ao lado)'
      OnClick = ArquivoMemoaolado1Click
    end
  end
  object QrPlaNiv5Jur: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 349
    Top = 429
    object QrPlaNiv5JurCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPlaNiv5JurNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPlaNiv5Jur: TDataSource
    DataSet = QrPlaNiv5Jur
    Left = 349
    Top = 481
  end
  object OpenPictureDialog1: TOpenPictureDialog
    DefaultExt = 'bmp'
    Filter = 
      'Bitmaps (*.bmp)|*.bmp|Icons (*.ico)|*.ico|Metafiles (*.wmf)|*.wm' +
      'f'
    Left = 705
    Top = 513
  end
  object OpenPictureDialog2: TOpenPictureDialog
    DefaultExt = 'bmp'
    Filter = 
      'Bitmaps (*.bmp)|*.bmp|Icons (*.ico)|*.ico|Metafiles (*.wmf)|*.wm' +
      'f'
    Left = 629
    Top = 193
  end
  object QrCarteiras2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo '
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 704
    Top = 244
    object QrCarteiras2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteiras2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteiras2Tipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsCarteiras2: TDataSource
    DataSet = QrCarteiras2
    Left = 732
    Top = 244
  end
  object QrCambioMda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cambiomda'
      'ORDER BY Nome')
    Left = 168
    Top = 416
    object QrCambioMdaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCambioMdaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsCambioMda: TDataSource
    DataSet = QrCambioMda
    Left = 168
    Top = 468
  end
  object QrCambioUsu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sen.Login, sen.Numero, sen.Funcionario, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFUNCI'
      'FROM cambiousu usu'
      'LEFT JOIN senhas sen ON sen.Numero=usu.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=sen.Funcionario')
    Left = 256
    Top = 416
    object QrCambioUsuLogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrCambioUsuNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrCambioUsuFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrCambioUsuNOMEFUNCI: TWideStringField
      FieldName = 'NOMEFUNCI'
      Size = 100
    end
  end
  object DsCambioUsu: TDataSource
    DataSet = QrCambioUsu
    Left = 260
    Top = 468
  end
  object PMUsuario: TPopupMenu
    Left = 700
    Top = 420
    object Adicionausurio1: TMenuItem
      Caption = '&Adiciona usu'#225'rio'
      OnClick = Adicionausurio1Click
    end
    object Retirausurio1: TMenuItem
      Caption = '&Retira usu'#225'rio'
      OnClick = Retirausurio1Click
    end
  end
  object QrListaLograd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Trato'
      'FROM listalograd')
    Left = 648
    Top = 152
    object QrListaLogradCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaLogradNome: TWideStringField
      FieldName = 'Nome'
      Size = 10
    end
    object QrListaLogradTrato: TWideStringField
      FieldName = 'Trato'
      Size = 10
    end
  end
  object DsPlaNiv5Tar: TDataSource
    DataSet = QrPlaNiv5Tar
    Left = 521
    Top = 313
  end
  object QrPlaNiv5Tar: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'AND Debito = "V"'
      'ORDER BY Nome')
    Left = 521
    Top = 261
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
end
