object FmAtrIts: TFmAtrIts
  Left = 339
  Top = 185
  Caption = 'ATR-IBUTO-002 :: Cadastro de Item de Atributo'
  ClientHeight = 332
  ClientWidth = 540
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 540
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 492
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 444
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 349
        Height = 32
        Caption = 'Cadastro de Item de Atributo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 349
        Height = 32
        Caption = 'Cadastro de Item de Atributo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 349
        Height = 32
        Caption = 'Cadastro de Item de Atributo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 540
    Height = 170
    Align = alClient
    TabOrder = 1
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 536
      Height = 153
      Align = alClient
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 8
        Width = 53
        Height = 13
        Caption = 'ID Atributo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 96
        Top = 48
        Width = 57
        Height = 13
        Caption = 'C'#243'digo [F4]:'
      end
      object Label3: TLabel
        Left = 96
        Top = 8
        Width = 39
        Height = 13
        Caption = 'Atributo:'
      end
      object Label4: TLabel
        Left = 12
        Top = 48
        Width = 74
        Height = 13
        Caption = 'ID item atributo:'
        FocusControl = DBEdCodigo
      end
      object Label5: TLabel
        Left = 12
        Top = 88
        Width = 141
        Height = 13
        Caption = 'Descri'#231#227'o do item de atributo:'
      end
      object Label11: TLabel
        Left = 179
        Top = 49
        Width = 81
        Height = 13
        Caption = 'Cor gr'#225'fico pizza:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 24
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        Enabled = False
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object EdNome: TdmkEdit
        Left = 12
        Top = 104
        Width = 513
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkContinuar: TCheckBox
        Left = 12
        Top = 128
        Width = 117
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 7
      end
      object EdCodUsu: TdmkEdit
        Left = 96
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCodUsuChange
        OnKeyDown = EdCodUsuKeyDown
      end
      object DBEdNome: TdmkDBEdit
        Left = 96
        Top = 24
        Width = 429
        Height = 21
        TabStop = False
        DataField = 'Nome'
        Enabled = False
        TabOrder = 1
        UpdType = utNil
        Alignment = taLeftJustify
      end
      object EdControle: TdmkEdit
        Left = 12
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object CBCorPie: TColorBox
        Left = 180
        Top = 64
        Width = 112
        Height = 22
        Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
        TabOrder = 4
        TabStop = False
      end
      object RGEfeito: TdmkRadioGroup
        Left = 296
        Top = 48
        Width = 229
        Height = 41
        Caption = ' Efeito: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Ruim'
          'Neutro'
          'Bom')
        TabOrder = 5
        QryCampo = 'Efeito'
        UpdCampo = 'Efeito'
        UpdType = utYes
        OldValor = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 262
    Width = 540
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 394
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 392
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 218
    Width = 540
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 536
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
end
