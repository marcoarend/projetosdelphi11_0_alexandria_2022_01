unit CalcAreaEQtd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditCalc, dmkEdit;

type
  TFmFormBase = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    EdVal1A: TdmkEdit;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    EdVal2A: TdmkEditCalc;
    EdVal3A: TdmkEdit;
    LaPeso: TLabel;
    EdVal1B: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdVal2B: TdmkEditCalc;
    EdVal3B: TdmkEdit;
    Label3: TLabel;
    EdVal1C: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdVal2C: TdmkEditCalc;
    EdVal3C: TdmkEdit;
    Label6: TLabel;
    EdVal4A: TdmkEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdVal4B: TdmkEdit;
    Label9: TLabel;
    EdVal4C: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFormBase: TFmFormBase;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFormBase.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormBase.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormBase.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmFormBase.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
