object FmLogOff: TFmLogOff
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: LogOff'
  ClientHeight = 230
  ClientWidth = 362
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 362
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 315
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 268
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 77
        Height = 31
        Caption = 'LogOff'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 77
        Height = 31
        Caption = 'LogOff'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 77
        Height = 31
        Caption = 'LogOff'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 362
    Height = 71
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 362
      Height = 71
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 362
        Height = 71
        Align = alClient
        TabOrder = 0
        object Label1: TLabel
          Left = 14
          Top = 13
          Width = 29
          Height = 13
          Caption = 'Login:'
        end
        object Label2: TLabel
          Left = 158
          Top = 16
          Width = 34
          Height = 13
          Caption = 'Senha:'
        end
        object Label4: TLabel
          Left = 306
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object EdLogin: TEdit
          Left = 14
          Top = 31
          Width = 138
          Height = 22
          Font.Charset = SYMBOL_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          PasswordChar = 'l'
          TabOrder = 0
          OnKeyDown = EdLoginKeyDown
        end
        object EdSenha: TEdit
          Left = 158
          Top = 31
          Width = 138
          Height = 22
          Font.Charset = SYMBOL_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          PasswordChar = 'l'
          TabOrder = 1
          OnKeyDown = EdSenhaKeyDown
        end
        object EdEmpresa: TEdit
          Left = 306
          Top = 31
          Width = 44
          Height = 22
          AutoSize = False
          Font.Charset = SYMBOL_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Wingdings'
          Font.Style = []
          ParentFont = False
          PasswordChar = 'l'
          TabOrder = 2
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 118
    Width = 362
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 358
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 162
    Width = 362
    Height = 68
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 218
      Top = 15
      Width = 142
      Height = 51
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 14
        Top = 4
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 216
      Height = 51
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn1: TBitBtn
        Left = 136
        Top = 12
        Width = 75
        Height = 25
        Caption = 'BitBtn1'
        TabOrder = 1
        Visible = False
        OnClick = BitBtn1Click
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 240
    Top = 11
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 88
    Top = 63
  end
end
