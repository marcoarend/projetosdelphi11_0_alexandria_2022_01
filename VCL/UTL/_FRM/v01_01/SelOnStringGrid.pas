unit SelOnStringGrid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, dmkGeral, dmkImage,
  UnDmkEnums;

type
  TFmSelOnStringGrid = class(TForm)
    Panel1: TPanel;
    Grade: TStringGrid;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeDblClick(Sender: TObject);
    procedure GradeClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FColSel: Integer;
    FResult: String;
  end;

  var
  FmSelOnStringGrid: TFmSelOnStringGrid;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmSelOnStringGrid.BtOKClick(Sender: TObject);
begin
  if FResult <> '' then
    Close
  else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmSelOnStringGrid.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSelOnStringGrid.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSelOnStringGrid.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  FResult := '';
end;

procedure TFmSelOnStringGrid.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSelOnStringGrid.GradeClick(Sender: TObject);
begin
  if (Grade.Col <> 0) and (Grade.Row <> 0) then
    FResult := Grade.Cells[FColSel, Grade.Row];
end;

procedure TFmSelOnStringGrid.GradeDblClick(Sender: TObject);
begin
  if (Grade.Col <> 0) and (Grade.Row <> 0) then
    Close;
end;

end.
