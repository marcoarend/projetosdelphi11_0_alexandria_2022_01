unit CtaGruImp;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, Menus, frxClass, frxDBSet, dmkEdit, dmkEditDateTimePicker,
  dmkGeral, dmkDBLookupComboBox, dmkEditCB, Variants, UnDmkProcFunc, dmkImage,
  UnDmkEnums, dmkValUsu;

type
  TFmCtaGruImp = class(TForm)
    PainelDados: TPanel;
    DsCtaGruImp: TDataSource;
    QrCtaGruImp: TmySQLQuery;
    QrCtaGruImpLk: TIntegerField;
    QrCtaGruImpDataCad: TDateField;
    QrCtaGruImpDataAlt: TDateField;
    QrCtaGruImpUserCad: TIntegerField;
    QrCtaGruImpUserAlt: TIntegerField;
    QrCtaGruImpCodigo: TSmallintField;
    QrCtaGruImpNome: TWideStringField;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    Label10: TLabel;
    PainelHead: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    TPDataI: TdmkEditDateTimePicker;
    Label4: TLabel;
    TPDataF: TdmkEditDateTimePicker;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit01: TDBEdit;
    DBEdit02: TDBEdit;
    QrCtaGruImpDataI: TDateField;
    QrCtaGruImpDataF: TDateField;
    StaticText1: TStaticText;
    PainelData: TPanel;
    DBGrid1: TDBGrid;
    QrCtaGruIEG: TmySQLQuery;
    DsCtaGruIEG: TDataSource;
    QrCtaGruIEGCodigo: TIntegerField;
    QrCtaGruIEGExcelGru: TIntegerField;
    QrCtaGruIEGLk: TIntegerField;
    QrCtaGruIEGDataCad: TDateField;
    QrCtaGruIEGDataAlt: TDateField;
    QrCtaGruIEGUserCad: TIntegerField;
    QrCtaGruIEGUserAlt: TIntegerField;
    PMGrupo: TPopupMenu;
    Incluinovogrupo1: TMenuItem;
    Alteragrupoatual1: TMenuItem;
    Excluigrupoatual1: TMenuItem;
    PMConta: TPopupMenu;
    Agregacontasazonal1: TMenuItem;
    Retiracontasazonal1: TMenuItem;
    QrCtaGruIEGNOMECTASAZ: TWideStringField;
    QrCtaGruIEGDataI: TDateField;
    QrCtaGruIEGDataF: TDateField;
    QrCtaGruIEGSaldo: TFloatField;
    frxGCZ: TfrxReport;
    frxDsCtaGruImp: TfrxDBDataset;
    frxDsCtaGruIEG: TfrxDBDataset;
    QrLct: TmySQLQuery;
    frxDsLct: TfrxDBDataset;
    QrLctNOMECTASAZ: TWideStringField;
    QrLctData: TDateField;
    QrLctCredito: TFloatField;
    QrLctDebito: TFloatField;
    QrLctControle: TIntegerField;
    QrLctNOMECTA: TWideStringField;
    QrLctNOMEFNC: TWideStringField;
    QrLctNOMECLI: TWideStringField;
    QrLctDescricao: TWideStringField;
    QrLctNOMETER: TWideStringField;
    QrLctCliente: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctQtde: TFloatField;
    QrLctQtdeEDescricao: TWideStringField;
    EdEmpresa: TdmkEditCB;
    Label7: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    QrCtaGruImpEntidade: TIntegerField;
    QrCtaGruImpNO_ENT: TWideStringField;
    QrCtaGruImpAlterWeb: TSmallintField;
    QrCtaGruImpAtivo: TSmallintField;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    QrCtaGruImpCliInt: TIntegerField;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtGrupo: TBitBtn;
    BtConta: TBitBtn;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    VUEmpresa: TdmkValUsu;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtGrupoClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCtaGruImpAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrCtaGruImpAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCtaGruImpBeforeOpen(DataSet: TDataSet);
    procedure Incluinovogrupo1Click(Sender: TObject);
    procedure Alteragrupoatual1Click(Sender: TObject);
    procedure Agregacontasazonal1Click(Sender: TObject);
    procedure Retiracontasazonal1Click(Sender: TObject);
    procedure BtContaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrLctCalcFields(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure PMGrupoPopup(Sender: TObject);
    procedure PMContaPopup(Sender: TObject);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenExcelGruIEG(ExcelGru: Integer);
  public
    { Public declarations }
  end;

var
  FmCtaGruImp: TFmCtaGruImp;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, CtaGruCta, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCtaGruImp.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCtaGruImp.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCtaGruImpCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCtaGruImp.DefParams;
begin
  VAR_GOTOTABELA := 'ExcelGruImp';
  VAR_GOTOMYSQLTABLE := QrCtaGruImp;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ');
  VAR_SQLx.Add('NO_ENT, ent.CliInt, egi.*');
  VAR_SQLx.Add('FROM excelgruimp egi');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=egi.Entidade');
  VAR_SQLx.Add('WHERE egi.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND egi.Codigo=:P0');
  //
  VAR_SQLa.Add('AND egi.Nome Like :P0');
  //
end;

procedure TFmCtaGruImp.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    if SQLType = stIns then
    begin
      EdCodigo.Text          := FormatFloat(FFormatFloat, Codigo);
      TPDataI.Date           := Date;
      TPDataF.Date           := Date;
      VUEmpresa.ValueVariant := 0;
      EdEmpresa.ValueVariant := 0;
      CBEmpresa.KeyValue     := Null;
    end else begin
      EdCodigo.Text          := DBEdCodigo.Text;
      EdNome.Text            := DBEdNome.Text;
      TPDataI.Date           := QrCtaGruImpDataI.Value;
      TPDataF.Date           := QrCtaGruImpDataF.Value;
      VUEmpresa.ValueVariant := QrCtaGruImpEntidade.Value;
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCtaGruImp.PMContaPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrCtaGruImp.State <> dsInactive) and (QrCtaGruImp.RecordCount > 0);
  Enab2 := (QrCtaGruIEG.State <> dsInactive) and (QrCtaGruIEG.RecordCount > 0);
  //
  Agregacontasazonal1.Enabled := Enab;
  Retiracontasazonal1.Enabled := Enab and Enab2;
end;

procedure TFmCtaGruImp.PMGrupoPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrCtaGruImp.State <> dsInactive) and (QrCtaGruImp.RecordCount > 0);
  //
  Alteragrupoatual1.Enabled := Enab;
  Excluigrupoatual1.Enabled := False;
end;

procedure TFmCtaGruImp.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCtaGruImp.AlteraRegistro;
var
  ExcelGruImp : Integer;
begin
  ExcelGruImp := QrCtaGruImpCodigo.Value;
  if not UMyMod.SelLockY(ExcelGruImp, Dmod.MyDB, 'ExcelGruImp', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(ExcelGruImp, Dmod.MyDB, 'ExcelGruImp', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCtaGruImp.IncluiRegistro;
var
  Cursor : TCursor;
  ExcelGruImp : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    ExcelGruImp := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ExcelGruImp', 'ExcelGruImp', 'Codigo');
    if Length(FormatFloat(FFormatFloat, ExcelGruImp))>Length(FFormatFloat) then
    begin
      Geral.MB_Aviso(
      'Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, stIns, ExcelGruImp);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmCtaGruImp.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCtaGruImp.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCtaGruImp.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCtaGruImp.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCtaGruImp.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCtaGruImp.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCtaGruImp.BtGrupoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMGrupo, BtGrupo);
end;

procedure TFmCtaGruImp.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCtaGruImpCodigo.Value;
  Close;
end;

procedure TFmCtaGruImp.BitBtn1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  //
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ExcelGruImp', Codigo);
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'excelgruimp', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'excelgruimp', 'Codigo');
end;

procedure TFmCtaGruImp.BtConfirmaClick(Sender: TObject);
var
  Codigo, (*Empresa,*) Entidade: Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o.') then Exit;
  //
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Defina a empresa!') then Exit;
  Entidade := DModG.QrEmpresasCodigo.Value;
  //
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO excelgruimp SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE excelgruimp SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, DataI=:P1, DataF=:P2, Entidade=:P3, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsString  := Geral.FDT(TPDataI.Date, 1);
  Dmod.QrUpdU.Params[02].AsString  := Geral.FDT(TPDataF.Date, 1);
  Dmod.QrUpdU.Params[03].AsInteger := Entidade;
  //
  Dmod.QrUpdU.Params[04].AsString  := Geral.FDT(Date, 1);
  Dmod.QrUpdU.Params[05].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[06].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ExcelGruImp', 'Codigo');
  MostraEdicao(False, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmCtaGruImp.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ExcelGruImp', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ExcelGruImp', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ExcelGruImp', 'Codigo');
end;

procedure TFmCtaGruImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmCtaGruImp.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCtaGruImpCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCtaGruImp.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCtaGruImp.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmCtaGruImp.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCtaGruImp.QrCtaGruImpAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCtaGruImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'ExcelGruImp', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmCtaGruImp.QrCtaGruImpAfterScroll(DataSet: TDataSet);
begin
  ReopenExcelGruIEG(0);
end;

procedure TFmCtaGruImp.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCtaGruImpCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ExcelGruImp', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCtaGruImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtaGruImp.QrCtaGruImpBeforeOpen(DataSet: TDataSet);
begin
  QrCtaGruImpCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCtaGruImp.ReopenExcelGruIEG(ExcelGru: Integer);
begin
  QrCtaGruIEG.Close;
  QrCtaGruIEG.Params[0].AsInteger := QrCtaGruImpCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrCtaGruIEG, Dmod.MyDB);
  //
  QrCtaGruIEG.Locate('ExcelGru', ExcelGru, []);
end;

procedure TFmCtaGruImp.Incluinovogrupo1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmCtaGruImp.Alteragrupoatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmCtaGruImp.Agregacontasazonal1Click(Sender: TObject);
var
  ExcelGru: Integer;
begin
  Application.CreateForm(TFmCtaGruCta, FmCtaGruCta);

  FmCtaGruCta.QrExcelGru.Close;
  FmCtaGruCta.QrExcelGru.SQL.Clear;

  FmCtaGruCta.QrExcelGru.SQL.Add('SELECT Codigo, Nome');
  FmCtaGruCta.QrExcelGru.SQL.Add('FROM excelgru');
  FmCtaGruCta.QrExcelGru.SQL.Add('WHERE Codigo NOT IN (');
  FmCtaGruCta.QrExcelGru.SQL.Add('  SELECT ExcelGru');
  FmCtaGruCta.QrExcelGru.SQL.Add('  FROM excelgruieg');
  FmCtaGruCta.QrExcelGru.SQL.Add('  WHERE Codigo=:P0)');
  FmCtaGruCta.QrExcelGru.SQL.Add('');
  FmCtaGruCta.QrExcelGru.SQL.Add('ORDER BY Nome');
  FmCtaGruCta.QrExcelGru.Params[0].AsInteger := QrCtaGruImpCodigo.Value;
  UnDmkDAC_PF.AbreQuery(FmCtaGruCta.QrExcelGru, Dmod.MyDB);

  FmCtaGruCta.ShowModal;
  ExcelGru := FmCtaGruCta.FExcelGru;
  FmCtaGruCta.Destroy;
  //
  if ExcelGru = -1000 then Exit;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO excelgruieg SET Codigo=:P0, ExcelGru=:P1');
  Dmod.QrUpd.Params[00].AsInteger := QrCtaGruImpCodigo.Value;
  Dmod.QrUpd.Params[01].AsInteger := ExcelGru;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenExcelGruIEG(ExcelGru);
end;

procedure TFmCtaGruImp.Retiracontasazonal1Click(Sender: TObject);
var
  Prox: Integer;
begin
  if Geral.MB_Pergunta('Confirma a retirada da conta sazonal "' +
  QrCtaGruIEGNOMECTASAZ.Value + '" do grupo atual?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM excelgruieg ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND ExcelGru=:P1');
    Dmod.QrUpd.Params[00].AsInteger := QrCtaGruIEGCodigo.Value;
    Dmod.QrUpd.Params[01].AsInteger := QrCtaGruIEGExcelGru.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Prox := UMyMod.ProximoRegistro(QrCtaGruIEG, 'ExcelGru', 0);
    ReopenExcelGruIEG(Prox);
  end;
end;

procedure TFmCtaGruImp.BtContaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PmConta, BtConta);
end;

procedure TFmCtaGruImp.SbImprimeClick(Sender: TObject);
  procedure LctSQL(TabLct: String);
  begin
    QrLct.SQL.Add('SELECT exg.Nome NOMECTASAZ,  lct.Data, lct.Credito, lct.Debito,');
    QrLct.SQL.Add('lct.Controle,  lct.Cliente, lct.Fornecedor, lct.Qtde,');
    QrLct.SQL.Add('cta.Nome NOMECTA, CASE WHEN fnc.Tipo=0 THEN');
    QrLct.SQL.Add('fnc.RazaoSocial ELSE fnc.Nome END NOMEFNC,');
    QrLct.SQL.Add('exg.DataI, exg.DataF, exg.Nome NOMEEXG,');
    QrLct.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial');
    QrLct.SQL.Add('ELSE cli.Nome END NOMECLI, lct.Descricao');
    QrLct.SQL.Add('FROM ' + TabLct + ' lct');
    QrLct.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas cta ON cta.Codigo=lct.Genero');
    QrLct.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades fnc ON fnc.Codigo=lct.Fornecedor');
    QrLct.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades cli ON cli.Codigo=lct.Cliente');
    QrLct.SQL.Add('LEFT JOIN ' + TMeuDB + '.excelgru exg ON exg.Codigo=lct.ExcelGru');
    QrLct.SQL.Add('LEFT JOIN ' + TMeuDB + '.excelgruieg ieg ON ieg.ExcelGru=exg.Codigo');
    QrLct.SQL.Add('LEFT JOIN ' + TMeuDB + '.excelgruimp egi ON egi.Codigo=ieg.Codigo');
    QrLct.SQL.Add('WHERE egi.Codigo=' + FormatFloat('0', QrCtaGruImpCodigo.Value));
  end;
var
  //Entidade, CliInt: Integer;
  TabLctA, TabLctB, TabLctD: String;
begin
  if (QrCtaGruImp.State <> dsInactive) and (QrCtaGruImp.RecordCount > 0) then
  begin
    TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, QrCtaGruImpCliInt.Value);
    TabLctB := DModG.NomeTab(TMeuDB, ntLct, False, ttB, QrCtaGruImpCliInt.Value);
    TabLctD := DModG.NomeTab(TMeuDB, ntLct, False, ttD, QrCtaGruImpCliInt.Value);
    //
    QrLct.Close;
    QrLct.SQL.Clear;
    QrLct.SQL.Add('DROP TABLE IF EXISTS fin_relat_008_saz;');
    QrLct.SQL.Add('CREATE TABLE fin_relat_008_saz');
    QrLct.SQL.Add('');
    LctSQL(TabLctA);
    QrLct.SQL.Add('UNION');
    LctSQL(TabLctB);
    QrLct.SQL.Add('UNION');
    LctSQL(TabLctD);
    QrLct.SQL.Add(';');
    QrLct.SQL.Add('');
    QrLct.SQL.Add('SELECT * FROM fin_relat_008_saz');
    QrLct.SQL.Add('ORDER BY DataI, DataF, NOMEEXG, Data;');
    QrLct.SQL.Add('');
    QrLct.SQL.Add('DROP TABLE IF EXISTS fin_relat_008_saz;');
    UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
    //
    MyObjects.frxDefineDataSets(frxGCZ, [
      DmodG.frxDsDono,
      frxDsCtaGruImp,
      frxDsCtaGruIEG,
      frxDsLct
      ]);
    //
    MyObjects.frxMostra(frxGCZ, 'Relat�rio de Contas Sazonais');
  end;
end;

procedure TFmCtaGruImp.QrLctCalcFields(DataSet: TDataSet);
begin
  if QrLctFornecedor.Value <> 0 then
    QrLctNOMETER.Value := QrLctNOMEFNC.Value else
    if QrLctCliente.Value <> 0 then
      QrLctNOMETER.Value := QrLctNOMECLI.Value else
        QrLctNOMETER.Value := '';
  if QrLctQtde.Value > 0 then
    QrLctQtdeEDescricao.Value := FloatToStr(QrLctQtde.Value) + ' ' +
    QrLctDescricao.Value else QrLctQtdeEDescricao.Value := QrLctDescricao.Value;
end;

end.

