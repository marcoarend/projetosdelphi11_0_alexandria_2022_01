unit SelStr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkGeral, dmkImage, Variants, UnDmkEnums;

type
  TFmSelStr = class(TForm)
    PainelDados: TPanel;
    LaPrompt: TLabel;
    CBSel: TdmkDBLookupComboBox;
    DsSel: TDataSource;
    LaOrdem: TLabel;
    QrSel: TmySQLQuery;
    QrSelDescricao: TWideStringField;
    dmkEdOrdem: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    CkContinua: TCheckBox;
    QrSelCodigo: TWideStringField;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FSelected, FContinua: Boolean;
    FIDThis, FTabela: String;
    FNovoCodigo: TNovoCodigo;
    FPermiteVazio: Boolean;
    //
    procedure Informa(Texto: String);
  end;

var
  FmSelStr: TFmSelStr;

implementation

uses {$IfNDef SemCfgCadLista} CfgCadLista,{$EndIf}
  UnMyObjects, Module;

{$R *.DFM}

procedure TFmSelStr.BtOKClick(Sender: TObject);
begin
  VAR_SELNOM := CBSel.Text;
  if (VAR_SELNOM = '') and (FPermiteVazio = False) then
  begin
    VAR_SELNOM := '';
    Geral.MB_Aviso('Nenhuma escolha foi realizada!');
    Exit;
  end else
    VAR_SELCODTXT := QrSelCodigo.Value;
  FSelected  := True;
  FContinua  := CkContinua.Checked;
  Close;
end;

procedure TFmSelStr.BtSaidaClick(Sender: TObject);
begin
  VAR_SELCOD := 0;
  Close;
end;

procedure TFmSelStr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSelStr.Informa(Texto: String);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
end;

procedure TFmSelStr.FormActivate(Sender: TObject);
var
  IDForm: String;
begin
  MyObjects.CorIniComponente();
  IDForm := Copy(Caption, 1, 13);
  if Uppercase(IDForm) <> FIDThis then
  begin
    Caption := FIDThis + ' :: ' + Caption;
    MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  end;
end;

procedure TFmSelStr.FormCreate(Sender: TObject);
begin
  FIDThis := 'XXX-XXXXX-008';
  FPermiteVazio := False;
  ImgTipo.SQLType := stLok;
  //
  FSelected := False;
  FContinua := False;
  VAR_SELCOD := 0;
end;

end.
