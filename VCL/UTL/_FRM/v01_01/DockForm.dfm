�
 TFMDOCKFORM 0�  TPF0TFmDockForm
FmDockFormLeftJTop� BorderStylebsDialogCaption   Cálculos em ProgressoClientHeight� ClientWidth.Color	clBtnFaceDockSite	DragKinddkDockDragModedmAutomaticFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter
OnActivateFormActivateOnClose	FormCloseOnCreate
FormCreate
OnDockOverFormDockOverOnResize
FormResizePixelsPerInchx
TextHeight TPanelPanel1Left Top;Width.Height<Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClient
BevelOuterbvNoneTabOrder  TProgressBarProgressLeft Top Width.HeightMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalTopTabOrder   TPanelPainelDados2LeftTopWidth+HeightMargins.LeftMargins.TopMargins.RightMargins.Bottom
BevelOuterbvNoneTabOrder TPanelPanel3Left Top WidthJHeightMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalLeft	AlignmenttaLeftJustify
BevelOuter	bvLoweredCaption  Efetuados:Font.CharsetDEFAULT_CHARSET
Font.Color Pj Font.Height�	Font.NameMS Sans Serif
Font.Style ParentColor	
ParentFontTabOrder   TPanelPnContaLeftJTop Width]HeightMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalLeft	AlignmenttaRightJustify
BevelOuter	bvLoweredCaption0  Font.CharsetDEFAULT_CHARSET
Font.Color Pj Font.Height�	Font.NameMS Sans Serif
Font.Style ParentColor	
ParentFontTabOrder  TPanelPanel4LeftqTop WidthmHeightMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalLeft	AlignmenttaLeftJustify
BevelOuter	bvLoweredCaption  Tempo restante:Font.CharsetDEFAULT_CHARSET
Font.Color Pj Font.Height�	Font.NameMS Sans Serif
Font.Style ParentColor	
ParentFontTabOrder  TPanel	PnAscendeLeft'Top WidthJHeightMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalLeft	AlignmenttaRightJustify
BevelOuter	bvLoweredCaption	0:00:00  Font.CharsetDEFAULT_CHARSET
Font.Color Pj Font.Height�	Font.NameMS Sans Serif
Font.Style ParentColor	
ParentFontTabOrder  TPanel
PnDescendeLeft�Top WidthMHeightMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalClient	AlignmenttaRightJustify
BevelOuter	bvLoweredCaption	0:00:00  Font.CharsetDEFAULT_CHARSET
Font.Color Pj Font.Height�	Font.NameMS Sans Serif
Font.Style ParentColor	
ParentFontTabOrder  TPanelPanel5Left� Top Width� HeightMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalLeft	AlignmenttaLeftJustify
BevelOuter	bvLoweredCaption  Tempo progressivo:Font.CharsetDEFAULT_CHARSET
Font.Color Pj Font.Height�	Font.NameMS Sans Serif
Font.Style ParentColor	
ParentFontTabOrder    TPanelPnCabecaLeft Top Width.Height;Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalTop
BevelOuterbvNoneTabOrder 	TGroupBoxGB_RLeft�Top Width<Height;Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalRightTabOrder  	TdmkImageImgTipoLeft
TopWidth'Height'Margins.LeftMargins.TopMargins.RightMargins.BottomTransparent	SQLTypestNil   	TGroupBoxGB_LLeft Top Width;Height;Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalLeftTabOrder  	TGroupBoxGB_MLeft;Top Width�Height;Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientTabOrder TLabel
LaTitulo1ALeft	TopWidthMHeight&Margins.LeftMargins.TopMargins.RightMargins.BottomCaption   Cálculos em ProgressoColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFontVisible  TLabel
LaTitulo1BLeftTopWidthMHeight&Margins.LeftMargins.TopMargins.RightMargins.BottomCaption   Cálculos em ProgressoColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclGradientActiveCaptionFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFont  TLabel
LaTitulo1CLeft
TopWidthMHeight&Margins.LeftMargins.TopMargins.RightMargins.BottomCaption   Cálculos em ProgressoColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.Color
clHotLightFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFont    	TGroupBox	GBAvisos1Left TopwWidth.Height7Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalBottomCaption	 Avisos: TabOrder TPanelPanel2LeftTopWidth)Height"Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClient
BevelOuterbvNoneTabOrder  TLabelLaAviso1LeftTopWidth� HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption..............................Font.CharsetDEFAULT_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	  TLabelLaAviso2LeftTopWidth� HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption..............................Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	    	TGroupBoxGBRodaPeLeft Top� Width.HeightMMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalBottomTabOrder TPanel
PnSaiDesisLeftzTopWidth� Height9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalRight
BevelOuterbvNoneTabOrder  TPanelPanel6LeftTopWidthxHeight9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClient
BevelOuterbvNoneTabOrder  TBitBtn	BtDesisteTagLeft� Top WidthnHeight1CursorcrHandPointHintSai da janela atualMargins.LeftMargins.TopMargins.RightMargins.BottomCaption&Parar	NumGlyphsParentShowHintShowHint	TabOrder OnClickBtDesisteClick     