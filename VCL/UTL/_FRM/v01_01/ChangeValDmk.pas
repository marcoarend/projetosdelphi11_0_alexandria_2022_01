unit ChangeValDmk;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw;

type
  TFmChangeValDmk = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    EdValAtu: TdmkEdit;
    LaValAtu: TLabel;
    LaValNew: TLabel;
    EdValNew: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmChangeValDmk: TFmChangeValDmk;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmChangeValDmk.BtOKClick(Sender: TObject);
begin
{
  if (VAR_POPUP_ACTIVE_Numero) and (VAR_LOC_POPUP <> nil) then
  begin
    EdResult.SetFocus;
    VAR_POPUP_ACTIVE_CONTROL := EdResult;
    VAR_POPUP_ACTIVE_FORM    := Self;
    MyObjects.MostraPopUpNoCentro(VAR_LOC_POPUP);
  end else
}
    VAR_GETVALOR_GET := True;
  //
  Close;
end;

procedure TFmChangeValDmk.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChangeValDmk.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmChangeValDmk.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  VAR_GETVALOR_GET := False;
end;

procedure TFmChangeValDmk.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
