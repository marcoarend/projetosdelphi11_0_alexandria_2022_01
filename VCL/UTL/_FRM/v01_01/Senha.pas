unit Senha;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  UnInternalConsts, UnInternalConsts2, mySQLDbTables,
  UnMyLinguas, ComCtrls, dmkGeral, dmkEdit, dmkEditDateTimePicker, dmkImage,
  UnDmkEnums;

type
  TFmSenha = class(TForm)
    QrSenhas1: TmySQLQuery;
    QrSenhas1Login: TWideStringField;
    QrSenhas1Numero: TIntegerField;
    QrSenhas1Senha: TWideStringField;
    QrSenhas1Perfil: TIntegerField;
    QrSenhas1Lk: TIntegerField;
    QrPerfis1: TmySQLQuery;
    Panel2: TPanel;
    EdValor: TdmkEdit;
    LaValor: TLabel;
    EdPorcentagem: TdmkEdit;
    LaBase: TLabel;
    LaPorcentagem: TLabel;
    EdSenha: TEdit;
    LaSenha: TLabel;
    EdLogin: TEdit;
    LaLogin: TLabel;
    TPData: TdmkEditDateTimePicker;
    LaData: TLabel;
    QrSenha: TmySQLQuery;
    QrPerfisItsPerf: TmySQLQuery;
    QrPerfisItsPerfLibera: TSmallintField;
    QrPerfisItsPerfJanela: TWideStringField;
    LaJanela: TLabel;
    QrSenhaPerfil: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtCancela: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaSenhaExtra: TLabel;
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure EdPorcentagemExit(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FCaption: String;
    procedure CalculaOnEdit(Sit: Integer);
    procedure VerificaSenha(Login, Senha, Janela: String);
    procedure ResizeForm();
  public
    { Public declarations }
    FUsuario: Integer;
    FSenhaExtra: String;
  end;

var
  FmSenha: TFmSenha;

implementation

uses UnMyObjects, Module,
{$IFNDEF NAO_USA_DB_GERAL} ModuleGeral, {$ENDIF}DmkDAC_PF;

//uses UnMyObjects, Principal;

//uses Usuarios, UsuariosNew;

//uses ;

{$R *.DFM}

procedure TFmSenha.BtOKClick(Sender: TObject);
var
  Senha: String;
begin
  Senha := Uppercase(EdSenha.Text);
  VAR_DESCMAX2 := VAR_DESCMAX;
  case VAR_FSENHA of
    1 : // S� master
      if (Senha = CO_MASTER) and (VAR_USUARIO = -1) then VAR_SENHARESULT := 2
      else
        Geral.MB_Aviso('Acesso negado. Senha Inv�lida ou usu�rio logado inv�lido');
    2 :
    begin
      if (Senha = Uppercase(VAR_BOSS)) or (Senha = CO_MASTER) or
      ((Senha = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> '')) then
      begin
        if Geral.MB_Pergunta('Deseja eliminar esta senha ?') = ID_YES Then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add(DELETE_FROM + ' senhas WHERE Numero=:P0');
          Dmod.QrUpd.Params[0].AsInteger := FUsuario;
          Dmod.QrUpd.ExecSQL;
        end;
      end
      else
        Geral.MB_Aviso('Acesso negado. Senha Inv�lida');
    end;

    3 : // S� master e admin
      if ((Senha = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
      or (Senha = CO_MASTER) then VAR_SENHARESULT := 2
      else
        Geral.MB_Aviso('Acesso negado. Senha Inv�lida');
    4 :
      if (Senha = VAR_SENHATXT) then VAR_SENHARESULT := 1
      else if (Senha = VAR_BOSS) or (Senha = CO_MASTER) then VAR_SENHARESULT := 2
      else
        Geral.MB_Aviso('Acesso negado. Senha Inv�lida');
    5 : // S� master e admin e boss
      if (Senha = Uppercase(VAR_BOSSSENHA)) or (Senha = CO_MASTER)
      or ((Senha = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
      // ini 2021-01-19
      or ((LaSenhaExtra.Caption <> '') and (Lowercase(LaSenhaExtra.Caption) = Lowercase(Senha)))
      // fim 2021-01-09
      then VAR_SENHARESULT := 2
      else
        Geral.MB_Aviso('Acesso negado. Senha Inv�lida');
    6 :
    begin
      {$IFNDEF NAO_USA_DB_GERAL}
      if (Senha = VAR_BOSS) or (Senha = CO_MASTER)
      or ((Senha = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> '')) then
        VAR_SENHARESULT := 2 else
      begin
        QrSenhas1.Close;
        QrSenhas1.Params[0].AsString := EdLogin.Text;
        QrSenhas1.Params[1].AsString := Senha;
        UnDmkDAC_PF.AbreQuery(QrSenhas1, Dmod.MyDB);
        if QrSenhas1.RecordCount > 0 then
        begin
          QrPerfis1.Close;
          QrPerfis1.SQL.Clear;
          QrPerfis1.SQL.Add('SELECT * FROM perfis');
          QrPerfis1.SQL.Add('WHERE Codigo=:P0');
          QrPerfis1.Params[0].AsInteger := QrSenhas1Perfil.Value;
          UnDmkDAC_PF.AbreQuery(QrPerfis1, Dmod.MyDB);
          if not DModG.NaoPermiteExclusao(ivTabPerfis, 'Exclus�o de SP', 0) then
            VAR_SENHARESULT := 2;
          QrPerfis1.Close;
        end else ShowMessage('Login inv�lido.');
        QrSenhas1.Close;
      end;
      {$ENDIF}
    end;
    //7 : DMod.VerificaSenha(7, EdLogin.Text, Senha);
    //8 : DMod.VerificaSenha(8, EdLogin.Text, Senha);
    9:
    begin
{$IFNDEF NAO_USA_DB_GERAL}
      if Uppercase(DModG.QrCtrlGeralPwdLibFunc.Value) = Senha then
        VAR_SENHARESULT := 2
      else
      if (Senha = Uppercase(VAR_BOSS))
      or ((Senha = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
      or (Senha = CO_MASTER) then
        VAR_SENHARESULT := 2
      else
        Geral.MB_Aviso('Acesso negado. Senha Inv�lida! (9)');
{$ENDIF}
    end
    else VerificaSenha(EdLogin.Text, Senha, LaJanela.Caption);
  end;
  if (VAR_FSENHA = 8) and (VAR_SENHARESULT = 1) then
  begin
    if Geral.DMV(EdPorcentagem.Text) > VAR_DESCMAX2 then
    begin
      Geral.MB_Erro('Desconto maior que o permitido');
      Exit;
    end;
  end;
  VAR_VALORSENHA := Geral.DMV(EdValor.text);
  VAR_DATARESULT := TPData.Date;
  Close;
end;

procedure TFmSenha.BtCancelaClick(Sender: TObject);
begin
  VAR_SENHARESULT := -1;
  Close;
end;

procedure TFmSenha.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  if VAR_FSENHA = 9 then
    FCaption  := 'AD2-SENHA-001 :: Senha Especial'
  else
    FCaption  := 'AD2-SENHA-001 :: Senha';
  //
  LaSenhaExtra.Caption := '';

  //
  QrPerfis1.Database :=  VAR_GOTOMySQLDBNAME;
  QrSenhas1.Database :=  VAR_GOTOMySQLDBNAME;
  VAR_SENHARESULT    := 0;
  //
  Color      := IC2_AparenciasFormPesquisa;
  Font.Color := IC2_AparenciasLabelDigite;
  //
  BtCancela.Font.Color := IC2_AparenciasDadosTexto;
  BtOK.Font.Color      := IC2_AparenciasDadosTexto;
end;

procedure TFmSenha.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  VAR_SENHATXT := CO_VAZIO;
end;

procedure TFmSenha.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  BtCancela.SetFocus;
  if Edlogin.Visible then EdLogin.SetFocus else EdSenha.setFocus;
  ResizeForm();
end;

procedure TFmSenha.CalculaOnEdit(Sit: Integer);
var
  Base, Valor, Porcent: Double;
begin
  Base    := Geral.DMV(LaBase.Caption);
  Valor   := Geral.DMV(EdValor.Text);
  Porcent := Geral.DMV(EdPorcentagem.Text);
  //
  if Sit = 1 then Valor := Base * Porcent / 100;
  if Sit = 2 then
  begin
    if Base = 0 then Porcent := 0 else
    Porcent := (Valor / Base) * 100;
  end;
  EdPorcentagem.ValueVariant := Porcent;
  EdValor.ValueVariant       := Valor;
end;

procedure TFmSenha.EdPorcentagemExit(Sender: TObject);
begin
  CalculaOnEdit(1);
end;

procedure TFmSenha.EdValorExit(Sender: TObject);
begin
  CalculaOnEdit(2);
end;

procedure TFmSenha.FormResize(Sender: TObject);
begin
  ResizeForm();
end;

procedure TFmSenha.FormShow(Sender: TObject);
begin
  ResizeForm();
end;

procedure TFmSenha.ResizeForm();
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], FCaption, True, taCenter, 2, 10, 20);
end;

procedure TFmSenha.VerificaSenha(Login, Senha, Janela: String);
begin
  if (Senha = Uppercase(VAR_BOSS))
  or ((Senha = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
  or (Senha = CO_MASTER) then VAR_SENHARESULT := 2 else
  begin
    QrSenha.Close;
    QrSenha.Params[0].AsString := Login;
    QrSenha.Params[1].AsString := CO_USERSPNOW;
    QrSenha.Params[2].AsString := Senha;
    UnDmkDAC_PF.AbreQuery(QrSenha, Dmod.MyDB);
    //
    if QrSenha.RecordCount > 0 then
    begin
      QrPerfisItsPerf.Close;
      QrPerfisItsPerf.Params[0].AsInteger := QrSenhaPerfil.Value;
      QrPerfisItsPerf.Params[1].AsString  := Janela;
      UnDmkDAC_PF.AbreQuery(QrPerfisItsPerf, Dmod.MyDB);
      if QrPerfisItsPerfLibera.Value = 1 then VAR_SENHARESULT := 2
      else Geral.MB_Aviso('Acesso negado. Senha Inv�lida');
      QrPerfisItsPerf.Close;
    end else ShowMessage('Login inv�lido.');
    QrSenha.Close;
  end;
end;

end.

