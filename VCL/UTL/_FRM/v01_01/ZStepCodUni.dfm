object FmZStepCodUni: TFmZStepCodUni
  Left = 339
  Top = 185
  Caption = 'STP-CODIG-001 :: Intervalos de C'#243'digo (Unit'#225'rio)'
  ClientHeight = 561
  ClientWidth = 571
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 571
    Height = 399
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 571
      Height = 36
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object EdTabela: TdmkEdit
        Left = 12
        Top = 8
        Width = 113
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clActiveCaption
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '[Tabela]'
        UpdCampo = 'Tabela'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '[Tabela]'
        ValWarn = False
      end
      object EdTabDescri: TdmkEdit
        Left = 128
        Top = 8
        Width = 533
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clActiveCaption
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '[Descri'#231#227'o]'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '[Descri'#231#227'o]'
        ValWarn = False
      end
    end
    object PnEdita: TPanel
      Left = 0
      Top = 282
      Width = 571
      Height = 117
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      Visible = False
      object PnConfirma: TPanel
        Left = 0
        Top = 69
        Width = 571
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn1: TBitBtn
          Tag = 14
          Left = 20
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Confirma'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BitBtn1Click
        end
        object Panel6: TPanel
          Left = 424
          Top = 0
          Width = 147
          Height = 48
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn2: TBitBtn
            Tag = 15
            Left = 6
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn2Click
          end
        end
        object EdResult: TdmkEdit
          Left = 220
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          Visible = False
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 571
        Height = 64
        Align = alTop
        Caption = ' Cadastro: '
        TabOrder = 0
        object Label9: TLabel
          Left = 8
          Top = 16
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label1: TLabel
          Left = 396
          Top = 16
          Width = 65
          Height = 13
          Caption = 'C'#243'digo inicial:'
        end
        object Label2: TLabel
          Left = 480
          Top = 16
          Width = 58
          Height = 13
          Caption = 'C'#243'digo final:'
        end
        object EdDescricao: TdmkEdit
          Left = 8
          Top = 32
          Width = 384
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Descricao'
          UpdCampo = 'Descricao'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCodIni: TdmkEdit
          Left = 396
          Top = 32
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '1'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          QryCampo = 'CodIni'
          UpdCampo = 'CodIni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
        end
        object EdCodFim: TdmkEdit
          Left = 480
          Top = 32
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '1'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          QryCampo = 'CodFim'
          UpdCampo = 'CodFim'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
        end
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 36
      Width = 571
      Height = 229
      TabStop = False
      Align = alClient
      DataSource = DsZStepCod
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 403
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodIni'
          Title.Caption = 'In'#237'cio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodFim'
          Title.Caption = 'Final'
          Visible = True
        end>
    end
    object PB1: TProgressBar
      Left = 0
      Top = 265
      Width = 571
      Height = 17
      Align = alBottom
      TabOrder = 3
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 571
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 523
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 475
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 368
        Height = 32
        Caption = 'Intervalos de C'#243'digo (Unit'#225'rio)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 368
        Height = 32
        Caption = 'Intervalos de C'#243'digo (Unit'#225'rio)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 368
        Height = 32
        Caption = 'Intervalos de C'#243'digo (Unit'#225'rio)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 571
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 567
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PnGerencia: TPanel
    Left = 0
    Top = 491
    Width = 571
    Height = 70
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 0
      Width = 571
      Height = 70
      Align = alBottom
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 425
        Top = 15
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 6
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 423
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtCodUsu: TBitBtn
          Left = 20
          Top = 4
          Width = 120
          Height = 40
          Caption = '&C'#243'digo'
          Enabled = False
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtCodUsuClick
        end
        object BtIntervalos: TBitBtn
          Left = 244
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Intervalos'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtIntervalosClick
        end
      end
    end
  end
  object QrZStepCod: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrZStepCodAfterOpen
    BeforeClose = QrZStepCodBeforeClose
    SQL.Strings = (
      'SELECT * FROM zstepcod'
      'WHERE Tabela=:P0')
    Left = 272
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrZStepCodTabela: TWideStringField
      FieldName = 'Tabela'
    end
    object QrZStepCodDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 30
    end
    object QrZStepCodCodIni: TIntegerField
      FieldName = 'CodIni'
    end
    object QrZStepCodCodFim: TIntegerField
      FieldName = 'CodFim'
    end
  end
  object DsZStepCod: TDataSource
    DataSet = QrZStepCod
    Left = 300
    Top = 148
  end
  object QrMinMax: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MIN(CodIni)  Min, Max(CodFim) Max'
      'FROM zstepcod'
      'WHERE Tabela=:P0')
    Left = 384
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMinMaxMin: TIntegerField
      FieldName = 'Min'
    end
    object QrMinMaxMax: TIntegerField
      FieldName = 'Max'
    end
  end
  object QrJaExiste: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT zsc.Descricao'
      'FROM zstepcod zsc'
      'WHERE zsc.Tabela=:P0'
      'AND :P1 BETWEEN CodIni AND CodFim')
    Left = 412
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrJaExisteDescricao: TWideStringField
      FieldName = 'Descricao'
      Required = True
      Size = 30
    end
  end
  object PMIntervalos: TPopupMenu
    Left = 356
    Top = 148
    object Incluinovointervalo1: TMenuItem
      Caption = '&Inclui novo intervalo'
      OnClick = Incluinovointervalo1Click
    end
    object Alteraintervaloatual1: TMenuItem
      Caption = '&Altera intervalo atual'
      OnClick = Alteraintervaloatual1Click
    end
    object Excluiintervalos1: TMenuItem
      Caption = '&Exclui intervalo(s)'
      OnClick = Excluiintervalos1Click
    end
  end
  object PMCodUsu: TPopupMenu
    OnPopup = PMCodUsuPopup
    Left = 328
    Top = 148
    object Prximo1: TMenuItem
      Caption = 'Pr'#243'ximo da &Sequencia'
      OnClick = Prximo1Click
    end
    object Primeirodisponvel1: TMenuItem
      Caption = 'Primeiro &Dispon'#237'vel'
      OnClick = Primeirodisponvel1Click
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT (COUNT(CodUsu)+ :P0) Cadastros, '
      'MAX(CodUsu) Maximo'
      'FROM entidades'
      'WHERE CodUsu BETWEEN :P1 AND :P2')
    Left = 440
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPesqMaximo: TIntegerField
      FieldName = 'Maximo'
      Required = True
    end
    object QrPesqCadastros: TLargeintField
      FieldName = 'Cadastros'
    end
  end
  object QrDisp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu'
      'FROM entidades'
      'WHERE CodUsu BETWEEN 0 AND 1'
      'ORDER BY CodUsu')
    Left = 468
    Top = 148
    object QrDispCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
end
