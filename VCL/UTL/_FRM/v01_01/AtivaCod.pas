unit AtivaCod;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO,
  dmkEdit;

type
  TFmAtivaCod = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrAtiva: TmySQLQuery;
    DsAtiva: TDataSource;
    DBGAtiva: TdmkDBGridZTO;
    QrAtivaAtivo: TSmallintField;
    QrAtivaCodigo: TIntegerField;
    QrAtivaNome: TWideStringField;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    EdPsqNome: TdmkEdit;
    EdPsqCodi: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    RGAtivo: TRadioGroup;
    QrAtivaDataCad: TDateField;
    QrAtivaUserCad: TIntegerField;
    QrAtivaDataAlt: TDateField;
    QrAtivaUserAlt: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPsqCodiChange(Sender: TObject);
    procedure EdPsqNomeChange(Sender: TObject);
    procedure RGAtivoClick(Sender: TObject);
    procedure DBGAtivaCellClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
    FSQL, FFldCodi, FTabNome, FFldNome, FFldAtiv, FTitulo: String;
    FDB: TmySQLDatabase;
    //
    procedure ReopenAtivo(IDLoc: Integer);
  end;

  var
  FmAtivaCod: TFmAtivaCod;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmAtivaCod.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAtivaCod.DBGAtivaCellClick(Column: TColumn);
var
  Ativo, IDInt: Integer;
begin
  if Column.FieldName = FFldAtiv then
  begin
    if QrAtivaAtivo.Value = 0 then
      Ativo := 1
    else
      Ativo := 0;
    //
    IDInt := QrAtivaCodigo.Value;
    UMyMod.SQLInsUpd(QrAtiva, stUpd, FTabNome, False, [
    FFldAtiv], [FFldCodi], [
    Ativo], [IDInt], True);
    //
    ReopenAtivo(IDInt);
  end;
end;

procedure TFmAtivaCod.EdPsqCodiChange(Sender: TObject);
begin
  ReopenAtivo(0);
end;

procedure TFmAtivaCod.EdPsqNomeChange(Sender: TObject);
begin
  ReopenAtivo(0);
end;

procedure TFmAtivaCod.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAtivaCod.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmAtivaCod.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], FTitulo, True, taCenter, 2, 10, 20);
end;

procedure TFmAtivaCod.RGAtivoClick(Sender: TObject);
begin
  ReopenAtivo(0);
end;

procedure TFmAtivaCod.ReopenAtivo(IDLoc: Integer);
var
  Texto, Liga, Nome: String;
  Ativ, Codi: Integer;
begin
  Texto := FSQL;
  if pos(Uppercase('WHERE '), Uppercase(FSQL)) > 0 then
    Liga := 'AND '
  else
    Liga := 'WHERE ';
  //
  Ativ := RGAtivo.ItemIndex;
  if Ativ < 2 then
  begin
    Texto := Texto + sLineBreak + Liga + FFldAtiv + '=' + Geral.FF0(Ativ);
    Liga := 'AND ';
  end;
  Codi := EdPsqCodi.ValueVariant;
  if Codi <> 0 then
  begin
    Texto := Texto + sLineBreak + Liga + FFldCodi + '=' + Geral.FF0(Codi);
    Liga := 'AND ';
  end;
  Nome := EdPsqNome.ValueVariant;
  if Trim(Nome) <> '' then
  begin
    Texto := Texto + sLineBreak + Liga + FFldNome + ' LIKE "' + Nome + '"';
    //Liga := 'AND ';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(FmAtivaCod.QrAtiva, FDB, Texto);
  if IDLoc <> 0 then
    QrAtiva.Locate('Codigo', IDLoc, []);
end;

end.
