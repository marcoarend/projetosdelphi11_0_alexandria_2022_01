object FmCfgExpFile: TFmCfgExpFile
  Left = 339
  Top = 185
  Caption = 'ARQ-GRAVA-001 :: Salvar Arquivo'
  ClientHeight = 594
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 432
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 398
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 432
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 141
      ExplicitWidth = 782
      ExplicitHeight = 256
      object Memo: TMemo
        Left = 0
        Top = 168
        Width = 784
        Height = 264
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        WantReturns = False
        WordWrap = False
        ExplicitLeft = 1
        ExplicitTop = 252
        ExplicitWidth = 780
        ExplicitHeight = 98
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 104
        Align = alTop
        Caption = ' Local de grava'#231#227'o e nome do arquivo:'
        TabOrder = 1
        ExplicitLeft = 1
        ExplicitTop = 1
        ExplicitWidth = 780
        object Panel3: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 87
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitLeft = 1
          ExplicitTop = 1
          ExplicitWidth = 782
          ExplicitHeight = 92
          object Label1: TLabel
            Left = 12
            Top = 4
            Width = 178
            Height = 13
            Caption = 'Diret'#243'rio no qual o arquivo ser'#225' salvo:'
          end
          object Label2: TLabel
            Left = 12
            Top = 44
            Width = 138
            Height = 13
            Caption = 'Nome do arquivo a ser salvo:'
          end
          object SbDir: TSpeedButton
            Left = 732
            Top = 20
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SbDirClick
          end
          object Label3: TLabel
            Left = 708
            Top = 44
            Width = 47
            Height = 13
            Caption = 'Extens'#227'o:'
          end
          object EdDir: TdmkEdit
            Left = 12
            Top = 20
            Width = 717
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdArq: TdmkEdit
            Left = 12
            Top = 60
            Width = 693
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdExt: TdmkEdit
            Left = 708
            Top = 60
            Width = 48
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 104
        Width = 784
        Height = 64
        Align = alTop
        Caption = ' Configura'#231#227'o de exporta'#231#227'o do conte'#250'do do arquivo: '
        TabOrder = 2
        ExplicitLeft = 1
        ExplicitTop = 105
        ExplicitWidth = 780
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitLeft = 1
          ExplicitTop = 93
          ExplicitWidth = 782
          ExplicitHeight = 48
          object CkExcluir: TCheckBox
            Left = 12
            Top = 16
            Width = 181
            Height = 17
            Caption = 'Excluir arquivo caso ele j'#225' exista.'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
          object CkSemAcentos: TCheckBox
            Left = 196
            Top = 16
            Width = 173
            Height = 17
            Caption = 'Remover acentos das palavras.'
            TabOrder = 1
          end
          object CkEscreveLinhasVazias: TCheckBox
            Left = 372
            Top = 16
            Width = 181
            Height = 17
            Caption = 'Salvar linhas vazias (em branco).'
            Checked = True
            State = cbChecked
            TabOrder = 2
          end
          object CkQuebraDeLinha: TRadioGroup
            Left = 560
            Top = 0
            Width = 197
            Height = 41
            Caption = ' Quebra de linha: '
            Columns = 4
            ItemIndex = 3
            Items.Strings = (
              '10'
              '13'
              '1013'
              '1310')
            TabOrder = 3
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = 4
    ExplicitTop = 4
    ExplicitWidth = 630
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 582
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 534
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 174
        Height = 32
        Caption = 'Salvar Arquivo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 174
        Height = 32
        Caption = 'Salvar Arquivo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 174
        Height = 32
        Caption = 'Salvar Arquivo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 480
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 289
    ExplicitWidth = 630
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 626
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 524
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 333
    ExplicitWidth = 630
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 484
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 482
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
end
