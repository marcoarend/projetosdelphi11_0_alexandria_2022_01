unit Recibo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, DBCtrls, dmkEdit, Menus, ComCtrls, jpeg,
  dmkMemo, UnInternalConsts, frxClass, frxBarcode, dmkImage, dmkGeral,
  UnDmkProcFunc, UnDmkEnums, Data.DB, mySQLDbTables, Vcl.Grids, Vcl.DBGrids,
  UnGrl_Vars;

type
  TFmRecibo = class(TForm)
    Panel2: TPanel;
    EdLocalData: TdmkEdit;
    Label1: TLabel;
    EdNumero: TdmkEdit;
    Label10: TLabel;
    EdValor: TdmkEdit;
    Label2: TLabel;
    EdEmitente: TdmkEdit;
    EdERua: TdmkEdit;
    Label3: TLabel;
    EdECidade: TdmkEdit;
    EdENumero: TdmkEdit;
    EdEUF: TdmkEdit;
    EdECEP: TdmkEdit;
    EdECompl: TdmkEdit;
    EdEPais: TdmkEdit;
    EdETe1: TdmkEdit;
    EdEBairro: TdmkEdit;
    EdECNPJ: TdmkEdit;
    Label21: TLabel;
    Label4: TLabel;
    Extenso: TdmkMemo;
    Label5: TLabel;
    Texto: TdmkMemo;
    Label6: TLabel;
    EdBeneficiario: TdmkEdit;
    Label20: TLabel;
    EdBCNPJ: TdmkEdit;
    EdBBairro: TdmkEdit;
    EdBTe1: TdmkEdit;
    EdBPais: TdmkEdit;
    EdBCompl: TdmkEdit;
    EdBCEP: TdmkEdit;
    EdBUF: TdmkEdit;
    EdBNumero: TdmkEdit;
    EdBRua: TdmkEdit;
    Label7: TLabel;
    EdBCidade: TdmkEdit;
    EdResponsavel: TdmkEdit;
    frxRecibo1: TfrxReport;
    frxBarCodeObject1: TfrxBarCodeObject;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Label23: TLabel;
    Label8: TLabel;
    QrReciboImpCab: TMySQLQuery;
    QrReciboImpCabNO_Emitente: TWideStringField;
    QrReciboImpCabCodigo: TIntegerField;
    QrReciboImpCabDtRecibo: TDateTimeField;
    QrReciboImpCabDtPrnted: TDateTimeField;
    QrReciboImpCabEmitente: TIntegerField;
    QrReciboImpCabBeneficiario: TIntegerField;
    QrReciboImpCabCNPJCPF: TWideStringField;
    QrReciboImpCabValor: TFloatField;
    DsReciboImpCab: TDataSource;
    QrReciboImpCabDtConfrmad: TDateTimeField;
    QrReciboImpCabDtConfrmad_TXT: TWideStringField;
    BtConfirmacao: TBitBtn;
    PMConfirmacao: TPopupMenu;
    Confirma1: TMenuItem;
    Desconfirma1: TMenuItem;
    Exclui1: TMenuItem;
    QrReciboImpCabNome: TWideStringField;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    Splitter1: TSplitter;
    QrReciboImpImp: TMySQLQuery;
    QrReciboImpImpCodigo: TIntegerField;
    QrReciboImpImpDtPrnted: TDateTimeField;
    QrReciboImpImpDtConfrmad: TDateTimeField;
    QrReciboImpImpDtConfrmad_TXT: TWideStringField;
    DsReciboImpImp: TDataSource;
    QrReciboImpImpControle: TIntegerField;
    QrReciboImpImpRecibo: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure EdValorChange(Sender: TObject);
    procedure fr2RecibosGetValue(const ParName: String;
      var ParValue: Variant);
    procedure FormActivate(Sender: TObject);
    (*
    procedure frRecibo1UserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    *)
    procedure frxRecibo1GetValue(const VarName: String;
      var Value: Variant);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    function frxRecibo1UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure EdLocalDataKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Confirma1Click(Sender: TObject);
    procedure Desconfirma1Click(Sender: TObject);
    procedure BtConfirmacaoClick(Sender: TObject);
    procedure PMConfirmacaoPopup(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure QrReciboImpCabBeforeClose(DataSet: TDataSet);
    procedure QrReciboImpCabAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenReciboImpCab();
    procedure ReopenReciboImpImp();
  public
    { Public declarations }
    FEmitente, FBeneficiario, FCodigo: Integer;
    FDtRecibo, FDtPrnted: TDateTime;
    //
    FLocalEData_Atual: String;
  end;

var
  FmRecibo: TFmRecibo;

implementation

uses UnMyObjects, Module, UnGOTOy, DmkDAC_PF, MyDBCheck,
  UMySQLModule;

{$R *.DFM}

procedure TFmRecibo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRecibo.Confirma1Click(Sender: TObject);
var
  Codigo: Integer;
  DtConfrmad: String;
begin
{ Tisolin n�o quer
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
    Exit;
}
  //
  Codigo := QrReciboImpCabCodigo.Value;
  DtConfrmad := Geral.FDT(Now(), 109);
  GOTOy.ImpressaoDeRecibo_ConfOuDesconf(Codigo, 0, DtConfrmad);
  //
  ReopenReciboImpCab();
end;

procedure TFmRecibo.Desconfirma1Click(Sender: TObject);
var
  Codigo: Integer;
  DtConfrmad: String;
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
    Exit;
  //
  Codigo := QrReciboImpCabCodigo.Value;
  DtConfrmad := '1899-12-31 12:00:00';
  GOTOy.ImpressaoDeRecibo_ConfOuDesconf(Codigo, 0, DtConfrmad);
  //
  ReopenReciboImpCab();
end;

procedure TFmRecibo.EdLocalDataKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    EdLocalData.ValueVariant := FLocalEData_Atual;
end;

procedure TFmRecibo.EdValorChange(Sender: TObject);
begin
  Extenso.Text := dmkPF.ExtensoMoney(Geral.TFT(EdValor.Text, 2, siNegativo))+'.';
end;

procedure TFmRecibo.Exclui1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
    Exit;
  //
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'reciboimpimp', 'Controle', QrReciboImpImpControle.Value, Dmod.MyDB) = ID_YES then
    ReopenReciboImpCab();
end;

procedure TFmRecibo.fr2RecibosGetValue(const ParName: String;
  var ParValue: Variant);
begin
  if ParName = 'NUMERO_P'      then ParValue := EdNumero.Text;
  if ParName = 'VALOR_P'       then ParValue := String(Dmod.QrControle.FieldByName('Moeda').AsString) + ' '+ EdValor.Text;
  if ParName = 'NOME_PC'       then ParValue := EdEmitente.Text;
  if ParName = 'RUA_PC'        then ParValue := EdERua.Text;
  if ParName = 'NUMERO_PC'     then ParValue := EdENumero.Text;
  if ParName = 'CNPJ_PC'       then ParValue := EdECNPJ.Text;
  if ParName = 'COMPL_PC'      then ParValue := EdECompl.Text;
  if ParName = 'BAIRRO_PC'     then ParValue := EdEBairro.Text;
  if ParName = 'CIDADE_PC'     then ParValue := EdECidade.Text;
  if ParName = 'UF_PC'         then ParValue := EdEUF.Text;
  if ParName = 'CEP_PC'        then ParValue := EdECEP.Text;
  if ParName = 'PAIS_PC'       then ParValue := EdEPais.Text;
  if ParName = 'TE1_PC'        then ParValue := EdETe1.Text;
  if ParName = 'EXTENSO_P'     then ParValue := Extenso.Text;
  if ParName = 'MOTIVO_P'      then ParValue := Texto.Text;
  if ParName = 'EMITENTEP'     then ParValue := EdBeneficiario.Text;
  if ParName = 'CNPJ_PE'       then ParValue := EdBCNPJ.Text;
  if ParName = 'RUA_PE'        then ParValue := EdBRua.Text;
  if ParName = 'NUMERO_PE'     then ParValue := EdBNumero.Text;
  if ParName = 'COMPL_PE'      then ParValue := EdBCompl.Text;
  if ParName = 'BAIRRO_PE'     then ParValue := EdBBairro.Text;
  if ParName = 'CIDADE_PE'     then ParValue := EdBCidade.Text;
  if ParName = 'UF_PE'         then ParValue := EdBUF.Text;
  if ParName = 'CEP_PE'        then ParValue := EdBCEP.Text;
  if ParName = 'PAIS_PE'       then ParValue := EdBPais.Text;
  if ParName = 'TE1_PE'        then ParValue := EdBTe1.Text;
  //
  if ParName = 'RESPONSAVEL_E' then ParValue := EdResponsavel.Text;
  if ParName = 'RESPONSAVEL_P' then ParValue := EdResponsavel.Text;
  if ParName = 'LOCAL_E_DATA'  then ParValue := EdLocalData.Text;
end;

procedure TFmRecibo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmRecibo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FEmitente       := 0;
  FBeneficiario   := 0;
  FDtRecibo       := 0;
  FDtPrnted       := Now();
end;

procedure TFmRecibo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRecibo.FormShow(Sender: TObject);
begin
  ReopenReciboImpCab();
end;

procedure TFmRecibo.BtConfirmacaoClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMConfirmacao, BtConfirmacao);
end;

procedure TFmRecibo.BtImprimeClick(Sender: TObject);
var
  Recibo: String;
  //_DtRecibo, _DtPrnted: String;
  CNPJCPF: String;
  Valor: Double;
begin
  frxRecibo1.Variables['VARF_NOTCOLOR'] :=
    not Geral.IntToBool_0(Dmod.QrControle.FieldByName('CorRecibo').AsInteger);
  //
  MyObjects.frxMostra(frxRecibo1, 'Recibo');
  //
  if VAR_USA_FRX_IMPR_Recibo and  VAR_FRX_Imprimiu then
  begin
    Recibo     := EdNumero.Text;
    //_DtRecibo  := Geral.FDT(FDtRecibo, 109);
    //_DtPrnted  := Geral.FDT(DtPrnted, 109);
    CNPJCPF    := Geral.SoNumero_TT(EdECNPJ.Text);
    Valor      := EdValor.ValueVariant;
    //
(*
    GOTOy.CadastraImpressaoDeRecibo_ReciboImp(Recibo, FDtRecibo, FDtPrnted,
    CNPJCPF, FEmitente, FBeneficiario, Valor, FCodigo);
*)
    GOTOy.CadastraImpressaoDeRecibo_ReciboImpCab(FCodigo, EdNumero.Text);
    //
    ReopenReciboImpCab();
  end;
end;

procedure TFmRecibo.frxRecibo1GetValue(const VarName: String;
  var Value: Variant);
  function Valor(): String;
  begin
    Result := String(Dmod.QrControle.FieldByName('Moeda').AsString);
    if EdValor.ValueVariant <> 0 then
      Result := Result + ' ' + EdValor.Text;
  end;
begin
  if VarName = 'NUMERO_P'          then Value := EdNumero.Text;
  if VarName = 'VALOR_P'           then Value := Valor();
  if VarName = 'NOME_PC'           then Value := EdEmitente.Text;
  if VarName = 'RUA_PC'            then Value := EdERua.Text;
  if VarName = 'NUMERO_PC'         then Value := EdENumero.Text;
  if VarName = 'CNPJ_PC'           then Value := EdECNPJ.Text;
  if VarName = 'COMPL_PC'          then Value := EdECompl.Text;
  if VarName = 'BAIRRO_PC'         then Value := EdEBairro.Text;
  if VarName = 'CIDADE_PC'         then Value := EdECidade.Text;
  if VarName = 'UF_PC'             then Value := EdEUF.Text;
  if VarName = 'CEP_PC'            then Value := EdECEP.Text;
  if VarName = 'PAIS_PC'           then Value := EdEPais.Text;
  if VarName = 'TE1_PC'            then Value := EdETe1.Text;
  if VarName = 'EXTENSO_P'         then Value := Extenso.Text;
  if VarName = 'MOTIVO_P'          then Value := Texto.Text;
  if VarName = 'EMITENTEP'         then Value := EdBeneficiario.Text;
  //
  if VarName = 'CNPJ_PE'           then Value := EdBCNPJ.Text;
  if VarName = 'RUA_PE'            then Value := EdBRua.Text;
  if VarName = 'NUMERO_PE'         then Value := EdBNumero.Text;
  if VarName = 'COMPL_PE'          then Value := EdBCompl.Text;
  if VarName = 'BAIRRO_PE'         then Value := EdBBairro.Text;
  if VarName = 'CIDADE_PE'         then Value := EdBCidade.Text;
  if VarName = 'UF_PE'             then Value := EdBUF.Text;
  if VarName = 'CEP_PE'            then Value := EdBCEP.Text;
  if VarName = 'PAIS_PE'           then Value := EdBPais.Text;
  if VarName = 'TE1_PE'            then Value := EdBTe1.Text;
  //
  if VarName = 'RESPONSAVEL_E'     then Value := EdResponsavel.Text;
  if VarName = 'RESPONSAVEL_P'     then Value := EdResponsavel.Text;
  if VarName = 'LOCAL_E_DATA'      then Value := EdLocalData.Text;
  if VarName = 'VARF_BARCODE_BARC' then Value := EdNumero.Text;
  //
  // ini *Toolrent ???
  if VarName = 'Logo2_4_x_1_0Existe' then
  begin
    Value := FileExists(VAR_Logo2_4_x_1_0Path);
    //
    if VAR_Usuario = -1 then
      Geral.MB_Info('Caminho Logo2_4_x_1_0: ' + sLineBreak +
      VAR_Logo2_4_x_1_0Path);
  end else
  if VarName = 'Logo2_4_x_1_0Path' then
    Value := VAR_Logo2_4_x_1_0Path
  else
  if VarName = 'Logo3_4_x_3_4Existe' then
  begin
    Value := FileExists(VAR_Logo3_4_x_3_4Path);
    //
    if VAR_Usuario = -1 then
      Geral.MB_Info('Caminho Logo2_4_x_1_0: ' + sLineBreak +
      VAR_Logo2_4_x_1_0Path);
  end else
  if VarName = 'Logo3_4_x_3_4Path' then
    Value := VAR_Logo3_4_x_3_4Path
  // fim *Toolrent ????
  else
end;

function TFmRecibo.frxRecibo1UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  if Name = 'VARF_NOTCOLOR' then
    Params := not Geral.IntToBool_0(Dmod.QrControle.FieldByName('CorRecibo').AsInteger);
end;

procedure TFmRecibo.PMConfirmacaoPopup(Sender: TObject);
begin
  Confirma1.Enabled    :=     dmkPF.DataZero(QrReciboImpCabDtConfrmad.Value);
  Desconfirma1.Enabled := not dmkPF.DataZero(QrReciboImpCabDtConfrmad.Value);
  MyObjects.HabilitaMenuItemItsDel(Exclui1, QrReciboImpImp);
end;

procedure TFmRecibo.QrReciboImpCabAfterScroll(DataSet: TDataSet);
begin
  ReopenReciboImpImp();
end;

procedure TFmRecibo.QrReciboImpCabBeforeClose(DataSet: TDataSet);
begin
  QrReciboImpImp.Close;
end;

procedure TFmRecibo.ReopenReciboImpCab();
begin
  if FCodigo = 0 then
    QrReciboImpCab.Close
  else begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrReciboImpCab, Dmod.MyDB, [
    'SELECT imp.*, ',
    'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome) NO_Emitente,  ',
    'IF(imp.DtConfrmad < "1900-01-01", "",  ',
    'DATE_FORMAT(imp.DtConfrmad, "%d/%m/%Y %H:%i:%s")) DtConfrmad_TXT ',
    'FROM reciboimpcab imp ',
    'LEFT JOIN entidades emi ON emi.Codigo=imp.Emitente ',
    'WHERE imp.Codigo=' + Geral.FF0(FCodigo),
    '']);
  end;
end;

procedure TFmRecibo.ReopenReciboImpImp;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrReciboImpImp, Dmod.MyDB, [
  'SELECT imp.*, ',
  'IF(imp.DtConfrmad < "1900-01-01", "",  ',
  'DATE_FORMAT(imp.DtConfrmad, "%d/%m/%Y %H:%i:%s")) DtConfrmad_TXT ',
  'FROM reciboimpimp imp ',
  'WHERE imp.Codigo=' + Geral.FF0(FCodigo),
  'ORDER BY Controle DESC ',
  '']);
end;

(*
procedure TFmRecibo.frRecibo1UserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if Name = 'VARF_NOTCOLOR' then
    Val := not Geral.IntToBool_0(Dmod.QrControle.FieldByName('CorRecibo').AsInteger);
end;
*)

end.
