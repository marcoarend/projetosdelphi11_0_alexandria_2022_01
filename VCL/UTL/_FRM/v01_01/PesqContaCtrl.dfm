object FmPesqContaCtrl: TFmPesqContaCtrl
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-033 :: Pesquisa em Conta Controlada'
  ClientHeight = 510
  ClientWidth = 602
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 602
    Height = 310
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel5: TPanel
      Left = 0
      Top = 187
      Width = 602
      Height = 139
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 0
        Top = 0
        Width = 602
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 13
      end
      object LaAviso2: TLabel
        Left = 0
        Top = 37
        Width = 602
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 13
      end
      object LaAviso3: TLabel
        Left = 0
        Top = 74
        Width = 602
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 13
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 16
        Width = 602
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        TabOrder = 0
      end
      object ProgressBar2: TProgressBar
        Left = 0
        Top = 53
        Width = 602
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        TabOrder = 1
      end
      object ProgressBar3: TProgressBar
        Left = 0
        Top = 90
        Width = 602
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        TabOrder = 2
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 602
      Height = 187
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 10
        Top = 54
        Width = 38
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Conta:'
      end
      object Label2: TLabel
        Left = 10
        Top = 5
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
      end
      object EdConta: TdmkEditCB
        Left = 10
        Top = 74
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBConta
        IgnoraDBLookupComboBox = False
      end
      object CBConta: TdmkDBLookupComboBox
        Left = 79
        Top = 74
        Width = 453
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 1
        dmkEditCB = EdConta
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object GroupBox2: TGroupBox
        Left = 6
        Top = 103
        Width = 527
        Height = 75
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Per'#237'odo de compet'#234'ncia: '
        TabOrder = 2
        object Label4: TLabel
          Left = 10
          Top = 20
          Width = 66
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'M'#234's inicial:'
        end
        object Label5: TLabel
          Left = 167
          Top = 20
          Width = 64
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ano inicial:'
        end
        object Label6: TLabel
          Left = 266
          Top = 20
          Width = 56
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'M'#234's final:'
        end
        object Label7: TLabel
          Left = 428
          Top = 20
          Width = 54
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ano final:'
        end
        object CBMesIni: TComboBox
          Left = 11
          Top = 39
          Width = 148
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          Text = 'CBMesIni'
        end
        object CBAnoIni: TComboBox
          Left = 166
          Top = 39
          Width = 91
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Text = 'CBAnoIni'
        end
        object CBMesFim: TComboBox
          Left = 267
          Top = 39
          Width = 153
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          Text = 'CBMesFim'
        end
        object CBAnoFim: TComboBox
          Left = 427
          Top = 39
          Width = 91
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          Text = 'CBAnoFim'
        end
      end
      object EdEmpresa: TdmkEditCB
        Left = 10
        Top = 25
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 79
        Top = 25
        Width = 453
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'FILIAL'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 4
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 602
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 543
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 484
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 444
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pesquisa em Conta Controlada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 444
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pesquisa em Conta Controlada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 444
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pesquisa em Conta Controlada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 369
    Width = 602
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 598
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label3: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label8: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 423
    Width = 602
    Height = 87
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 18
      Width = 598
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 421
        Top = 0
        Width = 177
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 15
          Top = 4
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 25
        Top = 5
        Width = 110
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 332
    Top = 68
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.contas.Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.contas.Nome'
      Size = 25
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Origin = 'DBMMONEY.contas.Nome2'
      Size = 4
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Origin = 'DBMMONEY.contas.ID'
      Size = 128
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Origin = 'DBMMONEY.contas.Subgrupo'
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'DBMMONEY.contas.Empresa'
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.contas.Credito'
      FixedChar = True
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.contas.Debito'
      FixedChar = True
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Origin = 'DBMMONEY.contas.Mensal'
      FixedChar = True
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Origin = 'DBMMONEY.contas.Exclusivo'
      FixedChar = True
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
      Origin = 'DBMMONEY.contas.Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
      Origin = 'DBMMONEY.contas.Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
      Origin = 'DBMMONEY.contas.Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
      Origin = 'DBMMONEY.contas.Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
      Origin = 'DBMMONEY.contas.Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.contas.Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Origin = 'DBMMONEY.contas.Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Origin = 'DBMMONEY.contas.Excel'
      Size = 128
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 360
    Top = 68
  end
  object QrCtasResMes: TmySQLQuery
   
    OnCalcFields = QrCtasResMesCalcFields
    SQL.Strings = (
      'SELECT * FROM ctasresmes'
      'WHERE SeqImp=-1 OR Periodo BETWEEN :P0 AND :P1'
      'ORDER BY Nome, Conta, SeqImp, Periodo')
    Left = 224
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCtasResMesConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCtasResMesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCtasResMesPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrCtasResMesTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCtasResMesFator: TFloatField
      FieldName = 'Fator'
    end
    object QrCtasResMesValFator: TFloatField
      FieldName = 'ValFator'
    end
    object QrCtasResMesDevido: TFloatField
      FieldName = 'Devido'
    end
    object QrCtasResMesPago: TFloatField
      FieldName = 'Pago'
    end
    object QrCtasResMesDiferenca: TFloatField
      FieldName = 'Diferenca'
    end
    object QrCtasResMesAcumulado: TFloatField
      FieldName = 'Acumulado'
    end
    object QrCtasResMesNOME_PERIODO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_PERIODO'
      Size = 100
      Calculated = True
    end
  end
  object frxCtasResMes: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 39723.469712453700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 168
    Top = 56
    Datasets = <
      item
        DataSet = frxDsCtasResMes
        DataSetName = 'frxDsCtasResMes'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 19.779530000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo32: TfrxMemoView
          Left = 2.000000000000000000
          Top = 1.102350000000001000
          Width = 716.000000000000000000
          Height = 15.779530000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[frxDsDono."NOMEDONO"] - DEMOSTRATIVO DE CONTAS CONTROLADAS - [D' +
              'ATE], [TIME]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 13.000000000000000000
        Top = 158.740260000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCtasResMes
        DataSetName = 'frxDsCtasResMes'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 2.000000000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCtasResMes."NOME_PERIODO"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 62.000000000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtasResMes."Fator"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 122.000000000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtasResMes."ValFator"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 182.000000000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtasResMes."Devido"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 242.000000000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtasResMes."Pago"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 302.000000000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtasResMes."Diferenca"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 362.000000000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtasResMes."Acumulado"]')
          ParentFont = False
        end
      end
      object TfrxGroupHeader
        FillType = ftBrush
        Height = 37.850340000000000000
        Top = 98.267780000000000000
        Width = 755.906000000000000000
        Condition = 'frxDsCtasResMes."Nome"'
        object Memo9: TfrxMemoView
          Left = 2.000000000000000000
          Top = 0.850340000000002800
          Width = 716.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCtasResMes."Conta"] - [frxDsCtasResMes."Nome"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 2.000000000000000000
          Top = 24.850340000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 62.000000000000000000
          Top = 24.850340000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Fator')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 122.000000000000000000
          Top = 24.850340000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor refer'#234'ncia')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 182.000000000000000000
          Top = 24.850340000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor a pagar')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 242.000000000000000000
          Top = 24.850340000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor pago')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 302.000000000000000000
          Top = 24.850340000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Diferen'#231'a no m'#234's')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 362.000000000000000000
          Top = 24.850340000000000000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor acumulado')
          ParentFont = False
        end
      end
      object TfrxGroupFooter
        FillType = ftBrush
        Height = 20.000000000000000000
        Top = 192.756030000000000000
        Width = 755.906000000000000000
        object Memo8: TfrxMemoView
          Left = 122.000000000000000000
          Top = 3.779527559055112000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCtasResMes."ValFator">)]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 182.000000000000000000
          Top = 3.779527559055112000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCtasResMes."Devido">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 242.000000000000000000
          Top = 3.779527559055112000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCtasResMes."Pago">)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 62.000000000000000000
          Top = 3.779527559055112000
          Width = 60.000000000000000000
          Height = 13.000000000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Total per'#237'odo:')
          ParentFont = False
        end
      end
    end
  end
  object frxDsCtasResMes: TfrxDBDataset
    UserName = 'frxDsCtasResMes'
    CloseDataSource = False
    DataSet = QrCtasResMes
    BCDToCurrency = False
    Left = 196
    Top = 56
  end
end
