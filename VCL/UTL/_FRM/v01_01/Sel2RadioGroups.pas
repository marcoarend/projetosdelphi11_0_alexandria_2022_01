unit Sel2RadioGroups;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmSel2RadioGroups = class(TForm)
    RGItens1: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    RGItens2: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGItens1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure VerificaDefinicao();
  public
    { Public declarations }
    FSelOnClick: Boolean;
    FAvisos: array of String;
    FAcoes: array of TSQLType;
    FValores: array of String; 
    FMyCods: array of Integer; 
  end;

  var
  FmSel2RadioGroups: TFmSel2RadioGroups;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmSel2RadioGroups.BtOKClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSel2RadioGroups.BtSaidaClick(Sender: TObject);
begin
  RGItens1.ItemIndex := -1;
  RGItens2.ItemIndex := -1;
  Close;
end;

procedure TFmSel2RadioGroups.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmSel2RadioGroups.FormCreate(Sender: TObject);
begin
  FSelOnClick := False;
end;

procedure TFmSel2RadioGroups.FormResize(Sender: TObject);
var
  H_RGs: Integer;
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [(*LaAviso1, LaAviso2*)], Caption, True, taCenter, 2, 10, 20);
  //
  H_RGs := RGItens1.Height  + RGItens2.Height;
  RGItens1.Height := H_RGs div 2;
end;

procedure TFmSel2RadioGroups.RGItens1Click(Sender: TObject);
begin
  VerificaDefinicao();
end;

procedure TFmSel2RadioGroups.VerificaDefinicao();
var
  Definidos: Boolean;
begin
  BtOk.Enabled := False;
  Definidos := (RGItens1.ItemIndex > -1) and (RGItens2.ItemIndex > -1);
  //
  if Definidos then
  begin
    if FSelOnClick then
      BtOKClick(Self)
    else
      BtOk.Enabled := True;
  end;
end;

end.
