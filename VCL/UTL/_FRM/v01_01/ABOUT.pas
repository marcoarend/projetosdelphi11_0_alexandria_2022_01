unit About;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ShellApi, {$IfNDef NAO_USA_DB_GERAL} UnLic_Dmk, {$EndIf}
  dmkGeral, UnInternalConsts, UnGrl_Vars, UnProjGroup_Consts;

type
  TFmAbout = class(TForm)
    GroupBox1: TGroupBox;
    ProgramIcon: TImage;
    LaLink: TLabel;
    LaMsgs: TLabel;
    Panel1: TPanel;
    LaAplicativo: TLabel;
    LaInternalName: TLabel;
    LaVersao: TLabel;
    LaVerMCW: TLabel;
    LaVerMLA: TLabel;
    LaIpLocal: TLabel;
    LaIPServidor: TLabel;
    LaSO: TLabel;
    LaComputador: TLabel;
    LaDescriPC: TLabel;
    LaPerfil: TLabel;
    LaCopy: TLabel;
    Panel2: TPanel;
    OKButton: TButton;
    procedure OKButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LaLinkClick(Sender: TObject);
    procedure LaMsgsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmAbout: TFmAbout;
//
implementation

uses MyListas, {$IfNDef NAO_USA_DB_GERAL} ModuleGeral, {$EndIf} UnDmkProcFunc;

{$R *.dfm}

procedure TFmAbout.FormCreate(Sender: TObject);
var
  Aplicativo, Versao, VerMCW, VerMLA, Copy, IP, IPServ, IPServer, Computador,
  DescriPC, SO, Perfil, InternalName: String;
  FilePath: String;
  HLargeIco, HSmallIco: HICON;
  FLargeIcon: TIcon;
  EXEVersionData: TEXEVersionData;
begin
  {$IfNDef NAO_USA_DB_GERAL}
  DModG.ReopenSenhas;
  DModG.ReopenTerminal(True);
  //
  if (CO_DMKID_APP <> 12) and (CO_DMKID_APP <> 17) then //DermaAD2 / DControl usa o destravador antigo
    LaLink.Visible := True
  else
    LaLink.Visible := False;
  //
  IPServ := VAR_IP;
  //
  if DModG.EhOServidor then
    IPServ := Geral.ObtemIP(1)
  else
    IPServ := VAR_IP;
  //
  Computador := 'Computador: ' + DModG.QrTerminal.FieldByName('NomePC').AsString;
  DescriPC   := 'Descri��o do computador: ' + DModG.QrTerminal.FieldByName('DescriPC').AsString;
  //
  if VAR_PERFIL = 0 then
    Perfil := 'Perfil de senha: BOSS'
  else
    Perfil := 'Perfil de senha: ' + DmodG.QrSenhasPerfil_TXT.Value;
  {$Else}
  LaLink.Visible := False;
  //
  IPServ     := Geral.ObtemIP(1);
  Computador := '';
  DescriPC   := '';
  Perfil     := '';
  {$EndIf}
  //
  Aplicativo     := 'Aplicativo: ' + Application.Title;
  EXEVersionData := dmkPF.GetEXEVersionData(Application.ExeName);
  InternalName   := 'Nome interno: ' + EXEVersionData.InternalName;
  Versao         := 'Esta Vers�o: ' + Geral.VersaoTxt2006(CO_VERSAO);
  VerMCW         := 'Vers�o beta W: ' + Geral.VersaoTxt2006(CO_VERMCW);
  VerMLA         := 'Vers�o beta A: ' + Geral.VersaoTxt2006(CO_VERMLA);
  IP             := 'Meu IP (local): ' + Geral.ObtemIP(1);
  IPServer       := 'IP do servidor (local): ' + IPServ;
  SO             := 'Sistema operacional: ' + dmkPF.ObtemSysInfo2('-');
  Copy           := '� ' + Geral.FDT(Date, 25) + ' Dermatek';
  //
  LaAplicativo.Caption   := Aplicativo;
  LaInternalName.Caption := InternalName;
  LaVersao.Caption       := Versao;
  LaVerMCW.Caption       := VerMCW;
  LaVerMLA.Caption       := VerMLA;
  LaIpLocal.Caption      := IP;
  LaIPServidor.Caption   := IPServer;
  LaSO.Caption           := SO;
  LaComputador.Caption   := Computador;
  LaDescriPC.Caption     := DescriPC;
  LaPerfil.Caption       := Perfil;
  LaCopy.Caption         := Copy;
  //
  FilePath := Application.ExeName;
  //
  if ExtractIconEx(PChar(FilePath), 0, HLargeIco, HSmallIco, 1 ) = 2 then
  begin
    FLargeIcon := TIcon.Create;
    try
      FLargeIcon.Handle        := HLargeIco;
      ProgramIcon.Picture.Icon := FLargeIcon;
    finally
      FLargeIcon.Free;
    end;
  end
  else
  begin
    DestroyIcon(HLargeIco);
  end;
end;

procedure TFmAbout.LaLinkClick(Sender: TObject);
var
  LiberaUso6: Boolean;
begin
  {$IFDEF UsaREST}
    LiberaUso6 := True;
  {$Else}
    LiberaUso6 := False;
  {$EndIf}
  //
  {$IfNDef NAO_USA_DB_GERAL}
  Lic_Dmk.MostraUnLock_Dmk(LiberaUso6);
  {$EndIf}
  Close;
end;

procedure TFmAbout.LaMsgsClick(Sender: TObject);
begin
  if VAR_HIDEN_ERR_MSGS_BY_DMK <> EmptyStr then
    Geral.MB_Erro(VAR_HIDEN_ERR_MSGS_BY_DMK)
  else
    Geral.MB_Info('N�o h� mensagens a serem mostradas');
end;

procedure TFmAbout.OKButtonClick(Sender: TObject);
begin
  Close;
end;

end.

