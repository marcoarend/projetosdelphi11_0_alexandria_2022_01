unit GetValor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, ComCtrls,
  dmkEditDateTimePicker, dmkImage, dmkGeral, UnDmkEnums;

type
  TFmGetValor = class(TForm)
    Panel1: TPanel;
    EdResult: TdmkEdit;
    LaResult: TLabel;
    TPData: TdmkEditDateTimePicker;
    LaData: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGetValor: TFmGetValor;

implementation

uses UnMyObjects, UnInternalConsts;

{$R *.DFM}

procedure TFmGetValor.BtOKClick(Sender: TObject);
begin
  if (VAR_POPUP_ACTIVE_Numero) and (VAR_LOC_POPUP <> nil) then
  begin
    EdResult.SetFocus;
    VAR_POPUP_ACTIVE_CONTROL := EdResult;
    VAR_POPUP_ACTIVE_FORM    := Self;
    MyObjects.MostraPopUpNoCentro(VAR_LOC_POPUP);
  end else
    VAR_GETVALOR_GET := True;
  //
  Close;
end;

procedure TFmGetValor.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGetValor.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  EdResult.ValWarn := True;
end;

procedure TFmGetValor.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  VAR_GETVALOR_GET := False;
  EdResult.ValWarn := False;
end;

procedure TFmGetValor.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
