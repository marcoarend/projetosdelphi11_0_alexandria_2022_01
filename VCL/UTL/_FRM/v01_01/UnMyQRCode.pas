unit UnMyQRCode;

interface

uses QRCode, Vcl.ExtCtrls, Vcl.Graphics;

type
  TUnMyQRCode = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  GeraBmpQRCode(Texto: String; ImageWidth, ImageHeight: Integer;
              Encoding: TQRCodeEncoding; Versao: Integer;
              FNC1Mode: TQRCodeFNC1Mode; ECLevel: TQRCodeECLevel;
              moduleWidth, marginPixels, applicationIndicator: Integer;
              StructuredAppend: Boolean; StructuredAppendCounter,
              StructuredAppendIndex: Integer; AutoConfigurate: Boolean): TBitmap;
  end;
var
  MyQRCode: TUnMyQRCode;

//const

implementation

{ TUnMyQRCode }

{
GeraBmpQRCode(Texto, : String; (*Encoding*) QRCode.AUTO,
(*Versao*)1, (*FNC1Mode*)QRCode.NO, (*ECLevel*)QRCode.LEVEL_L,
(*moduleWidth*)4, (*marginPixels*)10, (*applicationIndicator*)-1,
(*StructuredAppend*)False, (*StructuredAppendCounter*)0,
(*StructuredAppendIndex*)0, (*AutoConfigurate*) True);
}

function TUnMyQRCode.GeraBmpQRCode(Texto: String; ImageWidth, ImageHeight:
  Integer; Encoding: TQRCodeEncoding; Versao: Integer;
  FNC1Mode: TQRCodeFNC1Mode; ECLevel: TQRCodeECLevel;
  moduleWidth, marginPixels, applicationIndicator: Integer;
  StructuredAppend: Boolean; StructuredAppendCounter,
  StructuredAppendIndex: Integer; AutoConfigurate: Boolean): TBitmap;
var
  Dummy: Integer;
  Valor: Integer;
  QRCode: TQRCode;
  //Image: TImage;
  //BMP: TBitmap;
begin
  QRCode := TQRCode.Create(nil);
  QRCode.autoConfigurate := True;

{
  Image := TImage.Create(nil);
  Image.Height := ImageHeight;
  Image.Width := ImageWidth;
}

  Result := TBitmap.Create;
  try
    Result.Width  := ImageWidth;
    Result.Height := ImageHeight;
    Result.PixelFormat := pf24bit;

    {create new barcode}
    {encoding}
  (*
    if (CBEncoding.itemIndex>=0) then
      QRCode.encoding:=TQRCodeEncoding(CBEncoding.itemIndex);
  *)
    QrCode.encoding := Encoding;

    {value}
    //QRCode.code := MeTexto.Text;
    QRCode.code := Texto;
    QRCode.processTilde := True;

    {format}
    //if (CBVersion.itemIndex>=0) then QRCode.preferredVersion:=CBVersion.itemIndex+1;
    QRCode.preferredVersion := Versao;

    //if (CBFNC1.itemIndex>=0) then QRCode.fnc1mode:=TQRCodeFNC1Mode(CBFNC1.itemIndex);
    QRCode.fnc1Mode := FNC1Mode;

    //if (ComboEC.itemIndex>=0) then QRCode.correctionLevel:=TQRCodeECLevel(ComboEC.itemIndex);
    QRCode.correctionLevel := ECLevel;

    {dots size}
    //Val(EdSize.Text, Valor, Dummy);
    Valor := moduleWidth;
    QRCode.moduleWidth := Valor;

    {margin}
    //Val(EdMargin.text, Valor, Dummy);
    Valor := marginPixels;
    QRCode.marginPixels := Valor;

    {rune}
    {
    if (EdAppIdentifier.Text = '') then EdAppIdentifier.Text := '-1';
    Val(EdAppIdentifier.Text, Valor, Dummy);
    }
    Valor := applicationIndicator;
    QRCode.applicationIndicator := Valor;
    {correction level}


    QRCode.width := ImageWidth;
    QRCode.height := ImageHeight;

    {structured append}
  (*  QRCode.structuredAppend := False;
    if (CkStructuredAppend.Checked) then
    begin
      QRCode.structuredAppend := True;
    end;*)
    QRCode.structuredAppend := StructuredAppend;

    QRCode.structuredAppendCounter:=1;
    //Val(EdTotal.Text, Valor, Dummy);
    Valor := StructuredAppendCounter;
    QRCode.structuredAppendCounter := Valor;

    QRCode.structuredAppendIndex := 1;
    //Val(EdIndex.Text, valor, Dummy);
    Valor := StructuredAppendindex;
    QRCode.structuredAppendindex := Valor;

    //QRCode.autoconfigurate := CkAutoConfigurate.Checked;
    QRCode.autoConfigurate := AutoConfigurate;
    QRCode.paintBarcode(Result.Canvas);
  except
    Result.Free;
    raise
  end
end;

end.
