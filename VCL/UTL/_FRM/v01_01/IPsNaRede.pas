unit IPsNaRede;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  IdContext, IdBaseComponent, IdComponent, IdCustomTCPServer, IdTCPServer,
  Web.Win.Sockets, IdTCPConnection, IdDNSResolver,
  Winsock, IdRawBase, IdRawClient, IdIcmpClient, Vcl.Menus, dmkStringGrid;
type
  PNetResourceArray = ^TNetResourceArray;
  TNetResourceArray = array [0 .. 100] of TNetResource;
  //
  TDosCommand = (cmdNone=0, cmdArp=1, cmdTracert=2);
  //
  TIdIcmpClientAccess = class(TIdIcmpClient);
  //
  TFmIPsNaRede = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPesquisa: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    ListBox1: TListBox;
    BitBtn2: TBitBtn;
    MeResult_: TMemo;
    Panel5: TPanel;
    Label1: TLabel;
    LaMeuIP: TLabel;
    CkNames: TCheckBox;
    Label2: TLabel;
    EdSystemPath: TEdit;
    PMPesquisa: TPopupMenu;
    Rastreianomes1: TMenuItem;
    IPseMACs1: TMenuItem;
    SGGeral: TdmkStringGrid;
    Button1: TButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure ServerAccept(Sender: TObject; ClientSocket: TCustomIpClient);
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure Rastreianomes1Click(Sender: TObject);
    procedure IPseMACs1Click(Sender: TObject);
    procedure SGGeralDblClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    FParar: Boolean;
    FMeuIP: String;
    function  CreateNetResourceList(ResourceType: DWord; NetResource:
              PNetResource; out Entries: DWord; out List: PNetResourceArray):
              Boolean;
    procedure ScanNetworkResources(ResourceType, DisplayType: DWord;
              List: TStrings);
    //  OK!!
    procedure Tracert(Rede: String);
    procedure Arp_a();
  public
    { Public declarations }
    FSelIP, FSelMAC: String;
    //
    function LocalizaIPdoMACRedesLocais(const MAC: String; var IP: String;
             var MesmaRede: Boolean; var Rede: String): Boolean;

  end;

  var
  FmIPsNaRede: TFmIPsNaRede;

implementation

uses UnMyObjects;

{$R *.DFM}

const
  SDefaultDNS = '????';
var
  FDosCommand: TDosCommand = TDosCommand.cmdNone;

procedure TFmIPsNaRede.Arp_a();
var
  I, N, P, L: Integer;
  IPDest, p1, p2, p3, p4, Interface_: String;
  Texto1, Linha: WideString;
begin
  FDosCommand := TDosCommand.cmdArp;
  //
  ListBox1.Clear;
  Texto1 := Trim(EdSystemPath.Text) + '\arp.exe -a';
  MyObjects.ExecutaCmd(Texto1, MeResult_);
  ListBox1.Items := MeResult_.Lines;
  SGGeral.RowCount := MeResult_.Lines.Count;
  //
  SGGeral.ColCount := 5;
  SGGeral.ColWidths[1] := 150;
  SGGeral.ColWidths[2] := 250;
  SGGeral.ColWidths[3] := 100;
  SGGeral.ColWidths[4] := 150;
  //
  SGGeral.Cells[0, 0] := 'Seq';
  SGGeral.Cells[1, 0] := 'IP destino';
  SGGeral.Cells[2, 0] := 'Endere�o MAC';
  SGGeral.Cells[3, 0] := 'Tipo';
  SGGeral.Cells[4, 0] := 'Interface';
  //
  Interface_ := '????';
  N := 1;
  SGGeral.RowCount := N + 1;
  //SGGeral.Cells[1, I - 1] := Trim(Copy(MeResult_.Lines[I], 1, 24));
  for I := 0 to MeResult_.Lines.Count - 1 do
  begin
    Linha := Trim(MeResult_.Lines[I]);
    if pos('interface', Lowercase(Linha)) > 0 then
    begin
      P := pos(':', Linha) + 1;
      L := pos('-', Linha) - P;
      if L <= 0 then
        L := 16;
      Interface_ := Geral.SoNumeroEPonto_TT(Copy(Linha, P, L));
    end;
    IPDest := Trim(Copy(MeResult_.Lines[I], 1, 24));
    if Geral.SplitIPv4(IPDest, p1, p2, p3, p4) then
    begin
      N := N + 1;
      SGGeral.RowCount := N;
      //
      SGGeral.Cells[0, N - 1] := Geral.FF0(N);
      SGGeral.Cells[1, N - 1] := IPDest;
      SGGeral.Cells[2, N - 1] := Trim(Copy(MeResult_.Lines[I], 25, 17));
      SGGeral.Cells[3, N - 1] := Trim(Copy(MeResult_.Lines[I], 42));
      SGGeral.Cells[4, N - 1] := Interface_;
    end;
  end;
{
Interface: 192.168.25.200 --- 0x3
  Endere�o IP           Endere�o f�sico       Tipo
  192.168.25.1          98-97-d1-1b-2b-80     din�mico
  192.168.25.2          74-23-44-bb-7d-93     din�mico
  192.168.25.3          cc-f9-57-1c-c0-c1     din�mico
  192.168.250.181       cc-f9-57-1c-c0-c1     din�mico
  224.0.0.2             01-00-5e-00-00-02     est�tico
  224.0.0.22            01-00-5e-00-00-16     est�tico
  224.0.0.251           01-00-5e-00-00-fb     est�tico
  224.0.0.252           01-00-5e-00-00-fc     est�tico
  239.255.255.250       01-00-5e-7f-ff-fa     est�tico
  255.255.255.255       ff-ff-ff-ff-ff-ff     est�tico

Interface: 25.50.42.155 --- 0x5
  Endere�o IP           Endere�o f�sico       Tipo
  25.0.0.1              7a-79-19-00-00-01     din�mico
  25.255.255.255        ff-ff-ff-ff-ff-ff     est�tico
  224.0.0.2             01-00-5e-00-00-02     est�tico
  224.0.0.22            01-00-5e-00-00-16     est�tico
  224.0.0.251           01-00-5e-00-00-fb     est�tico
  224.0.0.252           01-00-5e-00-00-fc     est�tico
  239.255.255.250       01-00-5e-7f-ff-fa     est�tico
  255.255.255.255       ff-ff-ff-ff-ff-ff     est�tico
}
end;

procedure TFmIPsNaRede.BitBtn2Click(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmIPsNaRede.BtPesquisaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesquisa, BtPesquisa);
end;

procedure TFmIPsNaRede.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmIPsNaRede.Button1Click(Sender: TObject);
  function IPAddrToName(IPAddr: string): string;
  var
    SockAddrIn: TSockAddrIn;
    HostEnt: PHostEnt;
    WSAData: TWSAData;
  begin
    WSAStartup($101, WSAData);
    SockAddrIn.sin_addr.s_addr:=inet_addr(PAnsiChar(IPAddr));
    HostEnt:= GetHostByAddr(@SockAddrIn.sin_addr.S_addr, 4, AF_INET);
    if HostEnt<>nil then
    begin
      Result := StrPas(Hostent^.h_name)
    end
    else
    begin
      Result:='';
    end;
  end;
  //
  function HostByAddr(AddrStr: String): String;
  var
    Addr: TInAddr;
    Host: PHostEnt;
  begin
    //AddrStr := '136.148.3.10';
    Addr.s_addr := Inet_Addr(PAnsiChar(AddrStr));
    Host := GetHostByAddr(@Addr.s_addr,4,PF_INET);
    if Host <> nil then
      Result := Host^.h_name;
  end;
begin
  Geral.MB_Info(HostByAddr('192.168.25.200'));
  Geral.MB_Info(HostByAddr('192.168.25.2'));
end;

function TFmIPsNaRede.CreateNetResourceList(ResourceType: DWord;
  NetResource: PNetResource; out Entries: DWord;
  out List: PNetResourceArray): Boolean;
var
  EnumHandle: THandle;
  BufSize: DWord;
  Res: DWord;
begin
  Result := False;
  List := Nil;
  Entries := 0;
  if WNetOpenEnum(RESOURCE_GLOBALNET, ResourceType, 0, NetResource, EnumHandle)
    = NO_ERROR then
  begin
    try
      BufSize := $4000; // 16 kByte
      GetMem(List, BufSize);
      try
        repeat
          Entries := DWord(-1);
          FillChar(List^, BufSize, 0);
          Res := WNetEnumResource(EnumHandle, Entries, List, BufSize);
          if Res = ERROR_MORE_DATA then
          begin
            ReAllocMem(List, BufSize);
          end;
        until Res <> ERROR_MORE_DATA;
        Result := Res = NO_ERROR;
        if not Result then
        begin
          FreeMem(List);
          List := Nil;
          Entries := 0;
        end;
      except
        FreeMem(List);
        raise;
      end;
    finally
      WNetCloseEnum(EnumHandle);
    end;
  end;
end;

procedure TFmIPsNaRede.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  FMeuIP := Geral.ObtemIP(1);
  LaMeuIP.Caption := FMeuIP;
end;

procedure TFmIPsNaRede.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSelIP := '';
end;

procedure TFmIPsNaRede.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmIPsNaRede.IPseMACs1Click(Sender: TObject);
begin
  Arp_A();
end;

procedure TFmIPsNaRede.ListBox1Click(Sender: TObject);
begin
    MeResult_.Text := ListBox1.Items[ListBox1.ItemIndex];
end;

procedure TFmIPsNaRede.ListBox1DblClick(Sender: TObject);
begin
  if ListBox1.ItemIndex > -1 then
  begin
    MeResult_.Text := ListBox1.Items[ListBox1.ItemIndex];
    FSelIP := MeResult_.Lines[0];
    //
    Close;
  end;
end;

function TFmIPsNaRede.LocalizaIPdoMACRedesLocais(const MAC: String; var IP:
  String; var MesmaRede: Boolean; var Rede: String): Boolean;
var
  I: Integer;
  MascaraInterface, MascaraIP, p1, p2, p3, p4: String;
begin
  Result := False;
  IP := '';
  MesmaRede := False;
  Rede := '';
  //
  Arp_a();
  for I := 1 to SGGeral.RowCount -1 do
  begin
    if SGGeral.Cells[2, I] = MAC then
    begin
      IP   := SGGeral.Cells[1, I];
      Rede := SGGeral.Cells[4, I];
      if Geral.SplitIPv4(IP, p1, p2, p3, p4) then
      begin
        MascaraIP := p1 + '.' + p2 + '.' + p3;
      end;
      if Geral.SplitIPv4(Rede, p1, p2, p3, p4) then
      begin
        MascaraInterface := p1 + '.' + p2 + '.' + p3;
      end;
      MesmaRede := (MascaraIP = MascaraInterface) and (IP <> '');
      Result := True;
      // Evitar pegar outra rede!
      if MesmaRede then
        Exit;
    end;
  end;
end;

procedure TFmIPsNaRede.Rastreianomes1Click(Sender: TObject);
var
  MeuIP, P1, P2, P3, P4, Mascara: String;
begin
  if not CkNames.Checked then
  begin
    if Geral.SplitIPv4(FMeuIP, p1, p2, p3, p4) then
    begin
      Mascara := p1 + '.' + p2 + '.' + p3 + '.';
      Tracert(Mascara);
      if ListBox1.Count > 0 then
        MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Duplo clique na lista de IPs seleciona o IP da linha clicada!')
    end;
  end else
  begin
    ScanNetworkResources(RESOURCETYPE_DISK, RESOURCEDISPLAYTYPE_SERVER,
    ListBox1.Items);
  end;
end;

procedure TFmIPsNaRede.ScanNetworkResources(ResourceType, DisplayType: DWord;
  List: TStrings);
  procedure ScanLevel(NetResource: PNetResource);
  var
    Entries: DWord;
    NetResourceList: PNetResourceArray;
    i: Integer;
  begin
    if CreateNetResourceList(ResourceType, NetResource, Entries, NetResourceList)
    then
      try
        for i := 0 to Integer(Entries) - 1 do
        begin
          ListBox1.Items.Add(NetResourceList[i].lpLocalName + '-' + NetResourceList[i].lpRemoteName + '-' + NetResourceList[i].lpComment + '-' + NetResourceList[i].lpProvider);
          if (DisplayType = RESOURCEDISPLAYTYPE_GENERIC) or
            (NetResourceList[i].dwDisplayType = DisplayType) then
          begin
            List.AddObject(NetResourceList[i].lpRemoteName,
              Pointer(NetResourceList[i].dwDisplayType));
          end;
          if (NetResourceList[i].dwUsage and RESOURCEUSAGE_CONTAINER) <> 0 then
            ScanLevel(@NetResourceList[i]);
        end;
      finally
        FreeMem(NetResourceList);
      end;
  end;

begin
  ScanLevel(Nil);
end;

procedure TFmIPsNaRede.ServerAccept(Sender: TObject;
  ClientSocket: TCustomIpClient);
var
  s: string;
begin
  MeResult_.Lines.Append( ClientSocket.LookupHostName( ClientSocket.RemoteHost ) );
  s := ClientSocket.ReceiveLn;
  while s <> '' do
  begin
    MeResult_.Lines.Append( s );
    s := ClientSocket.ReceiveLn;
  end;
end;

procedure TFmIPsNaRede.SGGeralDblClick(Sender: TObject);
begin
  case FDosCommand of
    cmdArp:
    begin
      FSelIP  := SGGeral.Cells[1, SGGeral.Row];
      FSelMAC := SGGeral.Cells[2, SGGeral.Row];
      //
      Close;
    end;
    else
      Geral.MB_Erro('"FDosCommand" n�o implementado!')
  end;
end;

procedure TFmIPsNaRede.Tracert(Rede: String);
var
  I: Integer;
  Host: String;
  Texto1: WideString;
begin
  FDosCommand := TDosCommand.cmdTracert;
  //
  FParar := False;
  Screen.Cursor := crHourGlass;
  try
    ListBox1.Clear;
    for I := 0 to 255 do
    begin
      Host := Rede + Geral.FF0(I);
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pingando host ' + Host);
      if MyObjects.Ping(Host, 100) then
      begin
        //Texto1 := 'nslookup ' + Host;
        Texto1 := Trim(EdSystemPath.Text) + '\tracert.exe ' + Host;
        MyObjects.ExecutaCmd(Texto1, MeResult_);
        ListBox1.Items.Add(Host + sLineBreak + ' :: ' + MeResult_.Text);
        //
      end;
      Application.ProcessMessages;
      if FParar then
        Exit;
    end;
  finally
    Screen.Cursor := crDefault;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
end;

end.
