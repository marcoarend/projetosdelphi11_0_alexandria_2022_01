object FmNovaVersao: TFmNovaVersao
  Left = 419
  Top = 217
  Caption = 'XXX-XXXXX-999 :: Download de vers'#245'es'
  ClientHeight = 494
  ClientWidth = 534
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 47
    Width = 534
    Height = 335
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object LaLink: TLabel
      Left = 0
      Top = 121
      Width = 25
      Height = 16
      Cursor = crHandPoint
      Align = alTop
      Alignment = taCenter
      Caption = '___'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -14
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = LaLinkClick
      OnMouseEnter = LaLinkMouseEnter
      OnMouseLeave = LaLinkMouseLeave
    end
    object PainelControle: TPanel
      Left = 0
      Top = 308
      Width = 534
      Height = 27
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
    end
    object STAviso: TStaticText
      Left = 0
      Top = 50
      Width = 34
      Height = 24
      Align = alTop
      Alignment = taCenter
      Caption = '-----'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object StVersao: TStaticText
      Left = 0
      Top = 74
      Width = 534
      Height = 47
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = '___'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object STAppArqName: TStaticText
      Left = 0
      Top = 0
      Width = 534
      Height = 25
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = '[.EXE]'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -14
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
    object STAppArqTitle: TStaticText
      Left = 0
      Top = 25
      Width = 534
      Height = 25
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = '[TITULO]'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -14
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
    object RGTipo: TRadioGroup
      Left = 0
      Top = 147
      Width = 534
      Height = 45
      Align = alBottom
      Caption = ' Tipo: '
      Columns = 5
      Enabled = False
      Items.Strings = (
        'Execut'#225'vel'
        'Tabela(s)'
        'Imagem'
        'SQL'
        'Exec. Auxiliar')
      TabOrder = 5
    end
    object Panel1: TPanel
      Left = 0
      Top = 192
      Width = 534
      Height = 116
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 6
      object Label1: TLabel
        Left = 4
        Top = 5
        Width = 50
        Height = 13
        Caption = 'Progresso:'
      end
      object ProgressBar1: TProgressBar
        Left = 2
        Top = 27
        Width = 527
        Height = 17
        TabOrder = 0
      end
      object GroupBox1: TGroupBox
        Left = 4
        Top = 51
        Width = 210
        Height = 60
        Caption = ' Tamanho do arquivo: '
        Enabled = False
        TabOrder = 1
        object Label2: TLabel
          Left = 12
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Total:'
        end
        object Label3: TLabel
          Left = 110
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Recebido:'
        end
        object LaTamTot: TLabel
          Left = 78
          Top = 35
          Width = 25
          Height = 13
          Caption = 'bytes'
        end
        object LaTamRec: TLabel
          Left = 178
          Top = 35
          Width = 25
          Height = 13
          Caption = 'bytes'
        end
        object EdTamTot: TdmkEdit
          Left = 12
          Top = 31
          Width = 63
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdTamRec: TdmkEdit
          Left = 110
          Top = 31
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox3: TGroupBox
        Left = 221
        Top = 51
        Width = 87
        Height = 60
        Caption = ' Percentual: '
        Enabled = False
        TabOrder = 2
        object Label9: TLabel
          Left = 12
          Top = 16
          Width = 52
          Height = 13
          Caption = '% Baixado:'
        end
        object EdPerRec: TdmkEdit
          Left = 12
          Top = 31
          Width = 63
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox2: TGroupBox
        Left = 315
        Top = 51
        Width = 214
        Height = 60
        Caption = ' Tempo: '
        Enabled = False
        TabOrder = 3
        object Label6: TLabel
          Left = 12
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Total:'
        end
        object Label7: TLabel
          Left = 78
          Top = 16
          Width = 62
          Height = 13
          Caption = 'Transcorrido:'
        end
        object Label8: TLabel
          Left = 146
          Top = 16
          Width = 46
          Height = 13
          Caption = 'Restante:'
        end
        object EdTmpTot: TdmkEdit
          Left = 12
          Top = 31
          Width = 63
          Height = 21
          TabOrder = 0
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdTmpRun: TdmkEdit
          Left = 78
          Top = 31
          Width = 64
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdTmpFal: TdmkEdit
          Left = 146
          Top = 31
          Width = 63
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 534
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 487
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 440
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 248
        Height = 31
        Caption = 'Download de vers'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 248
        Height = 31
        Caption = 'Download de vers'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 248
        Height = 31
        Caption = 'Download de vers'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 382
    Width = 534
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 14
      Width = 531
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 426
    Width = 534
    Height = 68
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 391
      Top = 14
      Width = 142
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 14
      Width = 389
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn2: TBitBtn
        Tag = 19
        Left = 8
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Baixar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BitBtn2Click
      end
      object BtOK: TBitBtn
        Tag = 19
        Left = 129
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Auto'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = BtOKClick
      end
      object BitBtn1: TBitBtn
        Tag = 15
        Left = 250
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Parar'
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
        OnClick = BitBtn1Click
      end
    end
  end
  object Archiver: TZipForge
    ExtractCorruptedFiles = False
    CompressionLevel = clFastest
    CompressionMode = 1
    CurrentVersion = '6.94 '
    SpanningMode = smNone
    SpanningOptions.AdvancedNaming = True
    SpanningOptions.FirstVolumeSize = 0
    SpanningOptions.VolumeSize = vsAutoDetect
    SpanningOptions.CustomVolumeSize = 65536
    Options.FlushBuffers = True
    Options.OEMFileNames = True
    InMemory = False
    Zip64Mode = zmDisabled
    UnicodeFilenames = True
    EncryptionMethod = caPkzipClassic
    Left = 128
    Top = 124
  end
  object IdHTTP1: TIdHTTP
    OnWork = IdHTTP1Work
    OnWorkBegin = IdHTTP1WorkBegin
    OnWorkEnd = IdHTTP1WorkEnd
    ProtocolVersion = pv1_0
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 156
    Top = 124
  end
end
