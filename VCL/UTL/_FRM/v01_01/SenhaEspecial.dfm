object FmSenhaEspecial: TFmSenhaEspecial
  Left = 419
  Top = 217
  Caption = 'XXX-XXXXX-999 :: Senha'
  ClientHeight = 259
  ClientWidth = 464
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 92
    Width = 464
    Height = 97
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 464
      Height = 97
      Align = alClient
      TabOrder = 1
      Visible = False
      object Label3: TLabel
        Left = 8
        Top = 4
        Width = 61
        Height = 13
        Caption = 'Nova senha:'
      end
      object Label4: TLabel
        Left = 8
        Top = 44
        Width = 110
        Height = 13
        Caption = 'Redigite a nova senha:'
      end
      object EdSenha1: TdmkEdit
        Left = 8
        Top = 20
        Width = 449
        Height = 21
        Alignment = taCenter
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSenha2: TdmkEdit
        Left = 8
        Top = 60
        Width = 449
        Height = 21
        Alignment = taCenter
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 464
      Height = 97
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 8
        Top = 8
        Width = 34
        Height = 13
        Caption = 'Senha:'
      end
      object EdSenha: TdmkEdit
        Left = 8
        Top = 24
        Width = 449
        Height = 21
        Alignment = taCenter
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object StaticText1: TStaticText
        Left = 8
        Top = 56
        Width = 309
        Height = 17
        Caption = #201' permitido somente 3 tentativas de informar a senha.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8455327
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object StaticText2: TStaticText
        Left = 8
        Top = 72
        Width = 262
        Height = 17
        Caption = 'H'#225' mais 3 tentativas para acionar o bloqueio.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 464
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 416
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 368
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 78
        Height = 32
        Caption = 'Senha'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 78
        Height = 32
        Caption = 'Senha'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 78
        Height = 32
        Caption = 'Senha'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 464
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 460
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 284
        Height = 16
        Caption = '  Esta se'#231#227'o do aplicativo requer senha especial.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 284
        Height = 16
        Caption = '  Esta se'#231#227'o do aplicativo requer senha especial.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 189
    Width = 464
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 318
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 316
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 172
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAlteraClick
      end
    end
  end
  object QrCtrl: TmySQLQuery
    AfterOpen = QrCtrlAfterOpen
    SQL.Strings = (
      'SELECT AES_DECRYPT(SenhaEspecial, '#39'KkjhgbhHGyh'#39') SED, '
      'SenhaEspecial, ContaSenhaEspecial '
      'FROM controle')
    Left = 16
    Top = 12
    object QrCtrlSenhaEspecial: TWideStringField
      FieldName = 'SenhaEspecial'
      Size = 30
    end
    object QrCtrlContaSenhaEspecial: TSmallintField
      FieldName = 'ContaSenhaEspecial'
    end
    object QrCtrlSED: TWideStringField
      FieldName = 'SED'
      Size = 30
    end
  end
end
