unit SenhaEspecial;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, dmkGeral, dmkEdit, dmkImage, UnDmkEnums;

type
  TFmSenhaEspecial = class(TForm)
    PainelDados: TPanel;
    Panel1: TPanel;
    Label2: TLabel;
    EdSenha: TdmkEdit;
    QrCtrl: TmySQLQuery;
    QrCtrlSenhaEspecial: TWideStringField;
    Panel2: TPanel;
    Label3: TLabel;
    EdSenha1: TdmkEdit;
    Label4: TLabel;
    EdSenha2: TdmkEdit;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    QrCtrlContaSenhaEspecial: TSmallintField;
    QrCtrlSED: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    BtAltera: TBitBtn;
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrCtrlAfterOpen(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    function SenhaCombina: Boolean;
  public
    { Public declarations }
    FStatusAcesso: Boolean;
  end;

  var
  FmSenhaEspecial: TFmSenhaEspecial;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UnInternalConsts, DmkDAC_PF;

procedure TFmSenhaEspecial.BtConfirmaClick(Sender: TObject);
begin
  if Panel1.Visible then
  begin
    FStatusAcesso := SenhaCombina;
    if (Trim(String(QrCtrlSenhaEspecial.Value)) = '')
    and (Trim(EdSenha.Text) = '')
    then begin
      Geral.MB_Info('Senha n�o definida! Pare definir a senha '+
      'deixe a caixa de edi��o da senha em branco e '+
      'clique no bot�o "Altera".');
      EdSenha.SetFocus;
    end;
  end else begin
    if EdSenha1.Text = EdSenha2.Text then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE Controle SET ');
      Dmod.QrUpd.SQL.Add('ContaSenhaEspecial=0, ');
      Dmod.QrUpd.SQL.Add('SenhaEspecial=AES_ENCRYPT(:P0, :P1)');
      Dmod.QrUpd.Params[0].AsString  := EdSenha1.Text;
      Dmod.QrUpd.Params[1].AsString  := 'KkjhgbhHGyh';
      Dmod.QrUpd.ExecSQL;
    end else begin
      Geral.MB_Erro('Senhas n�o combinam!');
      EdSenha1.SetFocus;
    end;
  end;
  Close;
end;

procedure TFmSenhaEspecial.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSenhaEspecial.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSenhaEspecial.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSenhaEspecial.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FStatusAcesso := False;
  UnDmkDAC_PF.AbreQuery(QrCtrl, Dmod.MyDB);
end;

function TFmSenhaEspecial.SenhaCombina: Boolean;
begin
  if Trim(String(QrCtrlSED.Value)) = '' then Result := True else
  begin
    if ((EdSenha.Text = String(QrCtrlSED.Value))
         and (QrCtrlContaSenhaEspecial.Value < 3))
    or (EdSenha.Text = CO_MASTER)
    or (EdSenha.Text = VAR_BDSENHA)
    then begin
      Result := True;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE Controle SET ');
      Dmod.QrUpd.SQL.Add('ContaSenhaEspecial=0');
      Dmod.QrUpd.ExecSQL;
    end else begin
      Result := False;
      if Trim(EdSenha.Text) = '' then
      begin
        Geral.MB_Erro('Informe a senha!');
        EdSenha.SetFocus;
      end else begin
        if QrCtrlContaSenhaEspecial.Value < 3 then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE controle SET ');
          Dmod.QrUpd.SQL.Add('ContaSenhaEspecial=ContaSenhaEspecial+1');
          Dmod.QrUpd.ExecSQL;
        end else
          Geral.MB_Aviso('Limite de tentativas esgotado!');
      end;
    end;
  end;
  QrCtrl.Close;
  UnDmkDAC_PF.AbreQuery(QrCtrl, Dmod.MyDB);
end;

procedure TFmSenhaEspecial.QrCtrlAfterOpen(DataSet: TDataSet);
begin
  if QrCtrlContaSenhaEspecial.Value >= 3 then
  begin
    BtAltera.Enabled := False;
    BtConfirma.Enabled := False;
  end else begin
    StaticText2.Caption := 'H� mais ' + IntToStr(3- QrCtrlContaSenhaEspecial.Value)+
    ' tentativas antes de acionar o bloqueio.';
    if QrCtrlContaSenhaEspecial.Value = 0 then StaticText2.Font.Color := clNavy
    else StaticText2.Font.Color := clRed;
  end;
end;

procedure TFmSenhaEspecial.BtAlteraClick(Sender: TObject);
begin
  if SenhaCombina then
  begin
    Panel1.Visible := False;
    Panel2.Visible := True;
    BtAltera.Visible := False;
    //
    EdSenha1.Text := '';
    EdSenha2.Text := '';
    EdSenha1.SetFocus;
  end;
end;

end.
