unit EventosCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkCheckBox, dmkMemo,
  ComCtrls, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, Grids,
  DBGrids, frxClass, frxDBSet, Menus, dmkDBGrid, UnDmkProcFunc, UnDmkEnums;

type
  TFmEventosCad = class(TForm)
    PainelDados: TPanel;
    DsEventosCad: TDataSource;
    QrEventosCad: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtLct: TBitBtn;
    BtEvento: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    CkAtivo: TdmkCheckBox;
    Label4: TLabel;
    EdLocal: TdmkEdit;
    Label6: TLabel;
    GroupBox1: TGroupBox;
    TPDadtaIni: TdmkEditDateTimePicker;
    EdHoraIni: TdmkEdit;
    GroupBox2: TGroupBox;
    TPDataFim: TdmkEditDateTimePicker;
    MeHistorico: TdmkMemo;
    Label5: TLabel;
    Label10: TLabel;
    GroupBox3: TGroupBox;
    MeDBHistorico: TDBMemo;
    QrEventosCadCodigo: TIntegerField;
    QrEventosCadCodUsu: TIntegerField;
    QrEventosCadDataIni: TDateField;
    QrEventosCadHoraIni: TTimeField;
    QrEventosCadDataFim: TDateField;
    QrEventosCadHoraFim: TTimeField;
    QrEventosCadLocal: TWideStringField;
    QrEventosCadHistorico: TWideMemoField;
    QrEventosCadAtivo: TSmallintField;
    DBCheckBox1: TDBCheckBox;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    GroupBox4: TGroupBox;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QrEventosCadNome: TWideStringField;
    QrCliInt: TmySQLQuery;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMEENTIDADE: TWideStringField;
    DsCliInt: TDataSource;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    Label11: TLabel;
    QrEventosCadCliInt: TIntegerField;
    QrEventosCadNOMECLIINT: TWideStringField;
    DBEdit7: TDBEdit;
    Label13: TLabel;
    DBEdit8: TDBEdit;
    EdHoraFim: TdmkEdit;
    DBGLct: TdmkDBGrid;
    QrLct1: TmySQLQuery;
    DsLct1: TDataSource;
    QrLct1Data: TDateField;
    QrLct1Vencimento: TDateField;
    QrLct1Controle: TIntegerField;
    QrLct1Descricao: TWideStringField;
    QrLct1NotaFiscal: TIntegerField;
    QrLct1Debito: TFloatField;
    QrLct1Credito: TFloatField;
    QrLct1Documento: TFloatField;
    QrLct1CO_TERCEIRO: TLargeintField;
    QrLct1NO_TERCEIRO: TWideStringField;
    Splitter1: TSplitter;
    frxEvento1: TfrxReport;
    frxDsEventosCad: TfrxDBDataset;
    frxDsLct1: TfrxDBDataset;
    frxEvento2: TfrxReport;
    QrLct2: TmySQLQuery;
    frxDsLct2: TfrxDBDataset;
    QrLct2Data: TDateField;
    QrLct2Vencimento: TDateField;
    QrLct2Controle: TIntegerField;
    QrLct2Descricao: TWideStringField;
    QrLct2NotaFiscal: TIntegerField;
    QrLct2Debito: TFloatField;
    QrLct2Credito: TFloatField;
    QrLct2Documento: TFloatField;
    QrLct2CO_TERCEIRO: TLargeintField;
    QrLct2NO_TERCEIRO: TWideStringField;
    QrLct2NO_CONTA: TWideStringField;
    QrLct2SubGrupo: TIntegerField;
    QrLct2NO_SUBGRUPO: TWideStringField;
    QrLct2Genero: TIntegerField;
    PMImprime: TPopupMenu;
    Resultadoporlistagememordemdedata1: TMenuItem;
    ResultadoporSubgruposecontas1: TMenuItem;
    QrLct3: TmySQLQuery;
    DateField1: TDateField;
    DateField2: TDateField;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    LargeintField1: TLargeintField;
    StringField2: TWideStringField;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    IntegerField4: TIntegerField;
    StringField4: TWideStringField;
    frxEvento3: TfrxReport;
    frxDsLct3: TfrxDBDataset;
    QrLct3Grupo: TIntegerField;
    QrLct3NO_GRUPO: TWideStringField;
    ResultadoporGruposeSuggrupos1: TMenuItem;
    QrLct1DataDoc: TDateField;
    QrLct1Sub: TSmallintField;
    PMEvento: TPopupMenu;
    Incluinovoevento1: TMenuItem;
    Alteraeventoatual1: TMenuItem;
    Excluieventoatual1: TMenuItem;
    PMLct: TPopupMenu;
    Atrelalanamentos1: TMenuItem;
    Retiralanamentos1: TMenuItem;
    QrSel: TmySQLQuery;
    QrSelControle: TIntegerField;
    QrSelSub: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtLctClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEventosCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEventosCadBeforeOpen(DataSet: TDataSet);
    procedure BtEventoClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrEventosCadBeforeClose(DataSet: TDataSet);
    procedure QrEventosCadAfterScroll(DataSet: TDataSet);
    procedure frxEvento1GetValue(const VarName: string; var Value: Variant);
    procedure SbImprimeClick(Sender: TObject);
    procedure Resultadoporlistagememordemdedata1Click(Sender: TObject);
    procedure ResultadoporSubgruposecontas1Click(Sender: TObject);
    procedure ResultadoporGruposeSuggrupos1Click(Sender: TObject);
    procedure Incluinovoevento1Click(Sender: TObject);
    procedure Alteraeventoatual1Click(Sender: TObject);
    procedure Atrelalanamentos1Click(Sender: TObject);
    procedure Retiralanamentos1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenLct(Controle: Integer);
  public
    { Public declarations }
  end;

var
  FmEventosCad: TFmEventosCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, LocDefs, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEventosCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEventosCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEventosCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEventosCad.DefParams;
begin
  VAR_GOTOTABELA := 'eventoscad';
  VAR_GOTOMYSQLTABLE := QrEventosCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;





  VAR_SQLx.Add('SELECT eve.*, IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMECLIINT');
  VAR_SQLx.Add('FROM eventoscad eve');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo= eve.CliInt');
  VAR_SQLx.Add('WHERE eve.Codigo > 0');
  //
  VAR_SQL1.Add('AND eve.Codigo=:P0');
  //
  VAR_SQL2.Add('AND eve.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND eve.Nome Like :P0');
  //
end;

procedure TFmEventosCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'eventoscad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmEventosCad.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else
      Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmEventosCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEventosCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEventosCad.ReopenLct(Controle: Integer);
begin
  QrLct1.Close;
  QrLct1.Params[0].AsInteger := QrEventosCadCodigo.Value;
  QrLct1.Open;
  //
  QrLct1.Locate('Controle', Controle, []);
end;

procedure TFmEventosCad.ResultadoporGruposeSuggrupos1Click(Sender: TObject);
begin
  QrLct3.Close;
  QrLct3.Params[0].AsInteger := QrEventosCadCodigo.Value;
  QrLct3.Open;
  //
  MyObjects.frxMostra(frxEvento3, 'Evento por Grupo e sub-grupo');
end;

procedure TFmEventosCad.Resultadoporlistagememordemdedata1Click(
  Sender: TObject);
begin
  MyObjects.frxMostra(frxEvento1, 'Evento por lista (Data)');
end;

procedure TFmEventosCad.ResultadoporSubgruposecontas1Click(Sender: TObject);
begin
  QrLct2.Close;
  QrLct2.Params[0].AsInteger := QrEventosCadCodigo.Value;
  QrLct2.Open;
  //
  MyObjects.frxMostra(frxEvento2, 'Evento por listagem de data');
end;

procedure TFmEventosCad.Retiralanamentos1Click(Sender: TObject);
var
  i, Controle, Sub, Ctrl: Integer;
begin
  if Geral.MensagemBox('Confirma a retirada dos itens selecionados?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Ctrl := QrLct1Controle.Value;
      with DBGLct.DataSource.DataSet do
      for i:= 0 to DBGLct.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DBGLct.SelectedRows.Items[i]));
        GotoBookmark(DBGLct.SelectedRows.Items[i]);
        Controle := QrLct1Controle.Value;
        Sub      := QrLct1Sub.Value;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, VAR_LCT, False, [
          'EventosCad'], ['Controle', 'Sub'], [
           0], [Controle, Sub], True)
      end;
      ReopenLct(Ctrl);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmEventosCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEventosCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEventosCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEventosCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEventosCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEventosCad.Alteraeventoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrEventosCad, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'eventoscad');
end;

procedure TFmEventosCad.BtLctClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLct, BtLct);
end;

procedure TFmEventosCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEventosCadCodigo.Value;
  Close;
end;

procedure TFmEventosCad.Atrelalanamentos1Click(Sender: TObject);
begin
  //if DBCheck.CriaFm(TFmLocDefs, FmLocDefs, afmoNegarComAviso) then
  //begin
    Application.CreateForm(TFmLocDefs, FmLocDefs);
    FmLocDefs.FModuleLctX := nil;
    FmLocDefs.FShowForm := 2;
    FmLocDefs.FQrLoc := QrSel;
    FmLocDefs.ShowModal;
    FmLocDefs.Destroy;
    //
    QrSel.Open;
    ShowMessage(IntToStr(QrSel.RecordCount));
    if QrSel.RecordCount > 0 then
    begin
      Screen.Cursor := crHourGlass;
      DMod.QrUpd.SQL.Clear;
      DMod.QrUpd.SQL.Add('UPDATE ' + VAR_LCT + ' SET EventosCad=:P0');
      DMod.QrUpd.SQL.Add('WHERE Controle=:P1 AND Sub=:P2');
      QrSel.First;
      while not QrSel.Eof do
      begin
        DMod.QrUpd.Params[00].AsInteger := QrEventosCadCodigo.Value;
        DMod.QrUpd.Params[01].AsInteger := QrSelControle.Value;
        DMod.QrUpd.Params[02].AsInteger := QrSelSub.Value;
        DMod.QrUpd.ExecSQL;
        //
        QrSel.Next;
      end;
      LocCod(QrEventosCadCodigo.Value, QrEventosCadCodigo.Value);
      Screen.Cursor := crDefault;
    end;
  //end;
end;

procedure TFmEventosCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  //
  if MyObjects.FIC(Length(Nome) = 0, nil, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('eventoscad', 'Codigo', LaTipo.SQLType,
            QrEventosCadCodigo.Value);
  //
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmEventosCad, PainelEdita,
    'eventoscad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEventosCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'eventoscad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'eventoscad', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'eventoscad', 'Codigo');
end;

procedure TFmEventosCad.BtEventoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEvento, BtEvento);
end;

procedure TFmEventosCad.FormCreate(Sender: TObject);
begin
  MeHistorico.Align   := alClient;
  MeDBHistorico.Align := alClient;
  QrCliInt.Open;
  CriaOForm;
end;

procedure TFmEventosCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEventosCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEventosCad.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmEventosCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEventosCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEventosCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmEventosCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmEventosCad.QrEventosCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEventosCad.QrEventosCadAfterScroll(DataSet: TDataSet);
begin
  ReopenLct(0);
end;

procedure TFmEventosCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEventosCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEventosCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'eventoscad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEventosCad.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmEventosCad.frxEvento1GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_PERIODO' then
  begin
    if QrEventosCadDataIni.Value = QrEventosCadDataFim.Value then
    begin
      Value := 'No dia ' +
      FormatDateTime('dd/mm/yyy',  QrEventosCadDataIni.Value) +
      ' das ' + FormatDateTime('hh:nn',  QrEventosCadHoraIni.Value) +
      ' �s ' + FormatDateTime('hh:nn',  QrEventosCadHoraFim.Value);
    end else begin
      Value := 'In�cio em ' +
      FormatDateTime('dd/mm/yyy',  QrEventosCadDataIni.Value) +
      ' �s ' + FormatDateTime('hh:nn',  QrEventosCadHoraIni.Value) +
      ' e t�rmino em ' +
      FormatDateTime('dd/mm/yyy',  QrEventosCadDataFim.Value) +
      ' �s ' + FormatDateTime('hh:nn',  QrEventosCadHoraFim.Value);
    end;
  end else  
end;

procedure TFmEventosCad.Incluinovoevento1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrEventosCad, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'eventoscad');
end;

procedure TFmEventosCad.QrEventosCadBeforeClose(DataSet: TDataSet);
begin
  QrLct1.Close;
end;

procedure TFmEventosCad.QrEventosCadBeforeOpen(DataSet: TDataSet);
begin
  QrEventosCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

