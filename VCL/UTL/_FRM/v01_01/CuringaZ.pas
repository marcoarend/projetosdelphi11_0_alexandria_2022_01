unit CuringaZ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  Grids, DBGrids, ZCF2, UnInternalConsts, dmkImage, dmkGeral, ComCtrls,
  (***UnMySQLCuringa,***) UnDmkEnums,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, ZAbstractConnection,
  ZConnection, UMySQLDB, UnGrl_DmkDB, UnMyObjects, UnGOTOz;

type
  TFmCuringaZ = class(TForm)
    PainelDados: TPanel;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    PnPesquisa: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    TBTam: TTrackBar;
    RGMascara: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure TBTamChange(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure RGMascaraClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReabrePesquisa();
    procedure VerificaOrigem();
  public
    { Public declarations }
    FPodeReabrirPesquisa: Boolean;
    FOrigemPesquisa: TFormaPesquisa; // = (fpcNenhum, fpcTexto, fpc2, fpc3, fpc4, fpc2Nomes);
    FTextoAPesquisar, FNomeDoCampoCodigo, FNomeDoCampoNome1, FNomeDoCampoNome2,
    FNomeDoCampoNome3, FNomeDoCampoNome4, FNomeDaTabela, FNomeDoCampoTipo: String;
    FTipo1: Integer;
    FSohNum: Boolean;
    //FDataBase: TmySQLDatabase;
    FDataBase: TZConnection;
    FExtra, FLEFT_JOIN: string;
  end;

var
  FmCuringaZ: TFmCuringaZ;

implementation

uses Module, ModuleGeralZ;

{$R *.DFM}

procedure TFmCuringaZ.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCuringaZ.VerificaOrigem();
begin
  if FOrigemPesquisa = fpcNenhum then
  begin
    if DmodGZ.QrSB.State <> dsInactive then
      FOrigemPesquisa := fpcTexto
    else
    if DmodGZ.QrSB2.State <> dsInactive then
      FOrigemPesquisa := fpc2
    else
    if DmodGZ.QrSB3.State <> dsInactive then
      FOrigemPesquisa := fpc3
    else
    if DmodGZ.QrSB4.State <> dsInactive then
      FOrigemPesquisa := fpc4
    else
    if DmodGZ.QrSB5.State <> dsInactive then
      FOrigemPesquisa := fpc5
  end;
  if (FOrigemPesquisa = fpcTexto)
  or (FOrigemPesquisa = fpc2Nomes) then
  begin
    if DmodGZ.QrSB.RecordCount > 0 then
    begin
      VAR_CODIGO := DmodGZ.QrSBCodigo.Value;
      VAR_CODUSU := DmodGZ.QrSBCodUsu.Value;
      VAR_NOME_X := DmodGZ.QrSBNome.Value;
    end else begin
      VAR_CODIGO := 0;
      VAR_CODUSU := 0;
      VAR_NOME_X := '';
    end;
  end
  else
  if FOrigemPesquisa = fpc2 then
  begin
    if DmodGZ.QrSB2.RecordCount > 0 then
    begin
      VAR_CODIGO2 := DmodGZ.QrSB2Codigo.Value;
      VAR_CODUSU := DmodGZ.QrSB2CodUsu.Value;
      VAR_NOME_X := DmodGZ.QrSB2Nome.Value;
    end else begin
      VAR_CODIGO2 := 0;
      VAR_CODUSU := 0;
      VAR_NOME_X := '';
    end;
  end
  else
  if FOrigemPesquisa = fpc3 then
  begin
    if DmodGZ.QrSB3.RecordCount > 0 then
    begin
      VAR_CODIGO3 := DmodGZ.QrSB3Codigo.Value;
      VAR_CODUSU := DmodGZ.QrSB3CodUsu.Value;
      VAR_NOME_X := DmodGZ.QrSB3Nome.Value;
    end else begin
      VAR_CODIGO3 := 0;
      VAR_CODUSU := 0;
      VAR_NOME_X := '';
    end;
  end
  else
  if FOrigemPesquisa = fpc4 then
  begin
    if DmodGZ.QrSB4.RecordCount > 0 then
    begin
      VAR_CODIGO4 := DmodGZ.QrSB4Codigo.Value;
      VAR_TXTUSU := DmodGZ.QrSB4CodUsu.Value;
      VAR_NOME_X := DmodGZ.QrSB4Nome.Value;
    end else begin
      VAR_CODIGO4 := '';
      VAR_TXTUSU := '';
      VAR_NOME_X := '';
    end;
  end
  else
  if FOrigemPesquisa = fpc5 then
  begin
    if DmodGZ.QrSB5.RecordCount > 0 then
    begin
      VAR_CODIGO5 := DmodGZ.QrSB5Codigo.Value;
      //VAR_TXTUSU := DmodGZ.QrSB5CodUsu.Value;
      VAR_NOME_X := DmodGZ.QrSB5Nome.Value;
    end else begin
      VAR_CODIGO5 := 0;
      VAR_TXTUSU := '';
      VAR_NOME_X := '';
    end;
  end else
  begin
    Geral.MB_ERRO('Tipo de origem de pesquisa n�o implemetado!');
    VAR_CODIGO4 := '';
    VAR_TXTUSU := '';
    VAR_NOME_X := '';
  end;
end;

procedure TFmCuringaZ.DBGrid1CellClick(Column: TColumn);
begin
  VerificaOrigem;
end;

procedure TFmCuringaZ.DBGrid1DblClick(Sender: TObject);
begin
  VerificaOrigem;
  Close;
end;

procedure TFmCuringaZ.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCuringaZ.FormCreate(Sender: TObject);
begin
  FPodeReabrirPesquisa := False;
  FOrigemPesquisa      := fpcNenhum;
  FNomeDoCampoTipo     := '';
  FTipo1               := 0;
  FSohNum              := False;
  //
  ImgTipo.SQLType := stPsq;
  DBGrid1.DataSource := DmodGZ.DsSB;
end;

procedure TFmCuringaZ.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCuringaZ.ReabrePesquisa();
var
  //SQL,
  Erro: String;
  //I, N,
  K: Integer;
begin
  if not FPodeReabrirPesquisa then
    Exit;
  TZQuery(TDataSource(DBGrid1.DataSource).DataSet).Close;
  //
  K := Length(EdPesq.Text);
(*
  N := TBTam.Position;
  if N < 3 then
    N := 3;
*)
  if K >= 3 then
  begin
(*
    SQL := '';
    for I := 2 to K - N + 1 do
      SQL := SQL + 'OR (' + FFldNome + ' LIKE "%' + Copy(Texto, I, N) + '%")' + sLineBreak;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
    'SELECT ' + FFldID + ', ' + FFldNome,
    'FROM ' + TbCad.TableName,
    'WHERE (' + FFldNome + ' LIKE "%' + Copy(Texto, 1, N) + '%")',
    SQL,
    '']);
*)
    FTextoAPesquisar := GOTOz.ObtemSQLPesquisaText(RGMascara.ItemIndex, EdPesq.Text);
    //
    case FOrigemPesquisa of
      fpcTexto: GOTOz.ExecutouPesq01(FTextoAPesquisar, FNomeDoCampoCodigo,
      FNomeDoCampoNome1, FNomeDaTabela, FDataBase, FExtra, FLEFT_JOIN, Erro);
      //
      fpc2Nomes:
      begin
        VAR_NOME_S := FTextoAPesquisar;
        GOTOz.ExecutouPesq05(FNomeDoCampoCodigo, FNomeDoCampoNome1,
        FNomeDoCampoNome2, FNomeDoCampoNome3, FNomeDoCampoNome4, FNomeDaTabela,
        FNomeDoCampoTipo, FTipo1, FDataBase, FExtra, FSohNum, Erro)
      end;
      else Geral.MB_Erro('Origem de pesquisa n�o implementado!' + sLineBreak +
      'AVise a DERMATEK!');
    end;
  end;
end;

procedure TFmCuringaZ.RGMascaraClick(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmCuringaZ.TBTamChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmCuringaZ.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
  begin
    VerificaOrigem;
    Close;
  end;
end;

procedure TFmCuringaZ.EdPesqChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

end.
