unit PesqNomeZ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, dmkGeral, dmkEdit, dmkEditF7,
  DBCtrls, (*DBTables,*) dmkDBLookupComboBox, dmkDBGrid, dmkImage, UnDmkEnums,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, ZAbstractConnection,
  ZConnection, dmkDBGridZTO;

type
  TFmPesqNomeZ = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    DsPesq: TDataSource;
    DBGLista: TdmkDBGridZTO;
    Label1: TLabel;
    EdPesq: TdmkEdit;
    EdDoc: TdmkEdit;
    LaDoc: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    EdAntSelCod: TdmkEdit;
    EdAntSelNom: TdmkEdit;
    Label2: TLabel;
    EdTxtPesq: TdmkEdit;
    QrPesq: TZQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGListaDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdTxtPesqDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCB: TDBLookupCombobox;
    FF7: TdmkEdit;
    FDB: TZConnection;
    FND: Boolean;
    FNomeTabela, FNomeCampoNome, FNomeCampoCodigo, FTexto, FDefinedSQL: String;
    FAtivou: Boolean;
  end;

  var
  FmPesqNomeZ: TFmPesqNomeZ;

implementation

uses UnMyObjects, UnInternalConsts, Module, UnGrl_DmkDB;

{$R *.DFM}

procedure TFmPesqNomeZ.BtOKClick(Sender: TObject);
begin
  if QrPesq.State <> dsInactive then
  begin
(*
    VAR_CADASTRO := QrPesq.FieldByName('_Codigo').AsInteger;
    VAR_CAD_NOME := QrPesq.FieldByName('_Nome').AsString;
*)
    VAR_CADASTRO := QrPesq.Fields[0].AsInteger;
    VAR_CAD_NOME := QrPesq.Fields[1].AsString;
    //
    VAR_ANT_SEL_CADASTRO := VAR_CADASTRO;
    VAR_ANT_SEL_CAD_NOME := VAR_CAD_NOME;
    //
    Close;
  end;
end;

procedure TFmPesqNomeZ.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqNomeZ.DBGListaDblClick(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmPesqNomeZ.EdPesqChange(Sender: TObject);
var
  Texto, Doc, SQL_Text, Masc: String;
begin
  QrPesq.Close;
  if FDB <> nil then
    QrPesq.Connection := FDB;
  //
  Texto := EdPesq.ValueVariant;
  //
  if Texto <> '' then
    Texto := StringReplace(Texto, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
  //
  Doc := Geral.SoNumero_TT(EdDoc.Text);
  if (Length(Texto) >= 2) or (Length(Doc) > 10) then
  begin
    QrPesq.SQL.Clear;
    if Trim(FDefinedSQL) <> '' then
    begin
      SQL_Text := Format(FDEfinedSQL, ['"%' + Texto + '%"']);
      QrPesq.SQL.Text := SQL_Text;
    end else
    // Usar antes do entidades por causa do EntidadesImp (Rua, etc...)
    if (FCB <> nil) and (TdmkDBLookupComboBox(FCB).LocF7SQLText.Text <> '') then
    begin
      SQL_Text := TdmkDBLookupComboBox(FCB).LocF7SQLText.Text;
      Masc := TdmkDBLookupComboBox(FCB).LocF7SQLMasc;
      SQL_Text := Geral.Substitui(SQL_Text, Masc, Texto);
      QrPesq.SQL.Text := SQL_Text;
    end else
    if FNomeTabela = 'entidades' then
    begin
(*    2013-06-13
      QrPesq.SQL.Add('SELECT Codigo _Codigo, IF(Tipo=0,');
      QrPesq.SQL.Add('CONCAT(RazaoSocial, " ", Fantasia),');
      QrPesq.SQL.Add('CONCAT(Nome, " ", Apelido)) _Nome');
*)

(*    2017-06-24
      QrPesq.SQL.Add('SELECT Codigo _Codigo, CONCAT(');
      QrPesq.SQL.Add('IF(RazaoSocial <> "", CONCAT("{R} ", RazaoSocial, " "), ""),');
      QrPesq.SQL.Add('IF(Fantasia <> "", CONCAT("{F} ", Fantasia, " "), ""),');
      QrPesq.SQL.Add('IF(Nome <> "", CONCAT("{N} ", Nome, " "), ""),');
      QrPesq.SQL.Add('IF(Apelido <> "", CONCAT("{A} ", Apelido, " "), "")');
*)
      QrPesq.SQL.Add(' SELECT Codigo _Codigo, CONCAT(');
      QrPesq.SQL.Add('  IF(RazaoSocial <> "", CONCAT("{R} ", RazaoSocial, " "), ""),');
      QrPesq.SQL.Add('  IF(Fantasia <> "", CONCAT("{F} ", Fantasia, " "), ""),');
      QrPesq.SQL.Add('  IF(Nome <> "", CONCAT("{N} ", Nome, " "), ""),');
      QrPesq.SQL.Add('  IF(Apelido <> "", CONCAT("{A} ", Apelido, " "), ""),');
      QrPesq.SQL.Add('  " - ", IF(Tipo=0, ');
      QrPesq.SQL.Add('  CONCAT("CNPJ: ", MID(CNPJ,1,2), ".", MID(CNPJ,3,3), ".",');
      QrPesq.SQL.Add('  MID(CNPJ,6,3), "/", MID(CNPJ,9,4), "-", MID(CNPJ, 13,2)) ');
      QrPesq.SQL.Add('  , ');
      QrPesq.SQL.Add('  CONCAT("CPF: ", MID(CPF,1,3), ".", MID(CPF,4,3), ".",');
      QrPesq.SQL.Add('  MID(CPF,7,3), "-", MID(CPF,10,2)) ');
      QrPesq.SQL.Add('  )');
//    FIM 2017-06-24
      QrPesq.SQL.Add(') _Nome');
//    FIM 2013-06-13
      QrPesq.SQL.Add('FROM entidades');
      QrPesq.SQL.Add('WHERE (RazaoSocial LIKE "%' + Texto + '%"');
      QrPesq.SQL.Add('OR Nome LIKE "%' + Texto + '%"');
      QrPesq.SQL.Add('OR Fantasia LIKE "%' + Texto + '%"');
      QrPesq.SQL.Add('OR Apelido LIKE "%' + Texto + '%")');
      if EdDoc.Visible then
      begin
        if Doc <> '' then
          QrPesq.SQL.Add('AND ((CNPJ="' + Doc + '") OR (CPF = "' + Doc + '"))');
      end;
      //QrPesq.SQL.Add('ORDER BY _Nome');
      QrPesq.SQL.Add('ORDER BY IF(Tipo=0, RazaoSocial, Nome)');
    end else
    if Lowercase(FNomeTabela) = 'gragrux' then
    begin
      Grl_DmkDB.AbreSQLQuery0(QrPesq, QrPesq.Connection, [
      'SELECT ggx.Controle _Codigo,  ',
      'CONCAT(gg1.Nome,  ',
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
      '_Nome  ',
      'FROM gragrux ggx  ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
      'WHERE CONCAT(gg1.Nome,  ',
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
      'LIKE "%' + Texto + '%"  ',
      'ORDER BY _Nome  ',
      '']);
    end else
    if (Lowercase(FNomeTabela) = 'stqcenloc')
    or (Lowercase(FNomeCampoNome) = 'NO_LOC_CEN')
     then
    begin
      QrPesq.SQL.Add('SELECT scl.Controle, scl.CodUsu, ');
      QrPesq.SQL.Add('CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ');
      QrPesq.SQL.Add('FROM stqcenloc scl ');
      QrPesq.SQL.Add('LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ');
      QrPesq.SQL.Add('WHERE CONCAT(scl.Nome, " (", scc.Nome, ")") LIKE "%' + Texto + '%" ');
      QrPesq.SQL.Add('ORDER BY NO_LOC_CEN ');
    end else
    begin
      if (Trim(FNomeCampoCodigo) = '')
      or (Trim(FNomeCampoCodigo) = 'N�o') then
        QrPesq.SQL.Add('SELECT DISTINCT 999999999 _Codigo, ' + FNomeCampoNome +
        ' _Nome FROM ' + FNomeTabela + ' WHERE ' + FNomeCampoNome + ' LIKE "%' +
        Texto + '%"')
      else
        QrPesq.SQL.Add('SELECT ' + FNomeCampoCodigo + ' _Codigo, ' + FNomeCampoNome +
        ' _Nome FROM ' + FNomeTabela + ' WHERE ' + FNomeCampoNome + ' LIKE "%' +
        Texto + '%"');
    end;
    try
      // Usa nos aplicativos DermaXX
      //UnDmkDAC_PF.AbreQuery(QrPesq, 'TFmPesqNome.EdPesqChange()');
      Grl_DmkDB.AbreQuery(QrPesq, QrPesq.Connection);
      //Geral.MB_SQL(Self, QrPesq);
    except
      Grl_DmkDB.LeMeuSQL_Fixo_z(QrPesq, '', nil, True, True);
    end;
  end;
end;

procedure TFmPesqNomeZ.EdTxtPesqDblClick(Sender: TObject);
begin
  EdPesq.Text := '%' + EdTxtPesq.Text + '%';
end;

procedure TFmPesqNomeZ.FormActivate(Sender: TObject);
var
  Query: TZQuery;
  Texto: String;
  P, P10, P13: Integer;
begin
  MyObjects.CorIniComponente();
  if FCB <> nil then
  begin
    Query := TZQuery(FCB.ListSource.DataSet);
    if Query <> nil then
    begin
      Texto := Lowercase(Query.SQL.Text);
      P := pos('from', Texto);
      if P > 0 then
      begin
        Texto := Trim(Copy(Texto, P + 4));
        P := pos(' ', Texto);
        P10 := pos(#10, Texto);
        P13 := pos(#13, Texto);
        if (P13 < P) and (P13 > 0) then P := P13;
        if (P10 < P) and (P10 > 0) then P := P10;
        if P > 0 then
        begin
          FNomeTabela := Trim(Copy(Texto, 1, P));
          FNomeCampoNome   := FCB.ListField;
          FNomeCampoCodigo := FCB.KeyField;
          //
          if FCB is TdmkDBLookupComboBox then
          begin
            if TdmkDBLookupComboBox(FCB).LocF7TableName <> '' then
              FNometabela := TdmkDBLookupComboBox(FCB).LocF7TableName;
            if TdmkDBLookupComboBox(FCB).LocF7CodiFldName <> '' then
              FNomeCampoCodigo := TdmkDBLookupComboBox(FCB).LocF7CodiFldName;
            if TdmkDBLookupComboBox(FCB).LocF7NameFldName <> '' then
              FNomeCampoNome := TdmkDBLookupComboBox(FCB).LocF7NameFldName;
            if TdmkDBLookupComboBox(FCB).LocF7SQLText.Text <> '' then
            begin
              FNomeTabela := '';
            end;
          end;
        end;
      end;
    end;
  end
  else
  if FF7 is TdmkEditF7 then
  begin
    if TdmkEditF7(FF7).LocF7TableName <> '' then
      FNometabela := TdmkEditF7(FF7).LocF7TableName;

    if TdmkEditF7(FF7).LocF7CodiFldName <> '' then
      FNomeCampoCodigo := TdmkEditF7(FF7).LocF7CodiFldName;

    if TdmkEditF7(FF7).LocF7NameFldName <> '' then
      FNomeCampoNome := TdmkEditF7(FF7).LocF7NameFldName;
  end else
  if FND then
  begin
    //
  end;

  //

  if FNomeTabela = 'cunscad' then
    FNomeTabela := 'entidades';
  //

  if FNomeTabela = 'entidades' then
  begin
    LaDoc.Visible := True;
    EdDoc.Visible := True;
    EdPesq.Width  := 648;
  end;
  //
  if not FAtivou then
  begin
    FAtivou := True;
    EdPesq.Text := FTexto;
  end;
end;

procedure TFmPesqNomeZ.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  FND := False;
  QrPesq.Connection := Dmod.MyDB;
  FAtivou := False;
  FTexto := '';
  FDefinedSQL := '';
  EdAntSelCod.ValueVariant := VAR_ANT_SEL_CADASTRO;
  EdAntSelNom.ValueVariant := VAR_ANT_SEL_CAD_NOME;
end;

procedure TFmPesqNomeZ.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
