unit QuaisItens;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmQuaisItens = class(TForm)
    Panel1: TPanel;
    RGEscolha: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel3: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGEscolhaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FSelecionou: Boolean;
    FEscolha: TSelType;
  end;

  var
  FmQuaisItens: TFmQuaisItens;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmQuaisItens.BtSaidaClick(Sender: TObject);
begin
  FSelecionou := False;
  FEscolha := istNenhum;
  Close;
end;

procedure TFmQuaisItens.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmQuaisItens.BtOKClick(Sender: TObject);
begin
  FSelecionou := True;
  case RGEscolha.ItemIndex of
    1: FEscolha := istAtual;
    2: FEscolha := istSelecionados;
    3: FEscolha := istTodos;
    else FEscolha := istNenhum;
  end;
  Close;
end;

procedure TFmQuaisItens.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  FEscolha    := istNenhum;
  FSelecionou := False;
end;

procedure TFmQuaisItens.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmQuaisItens.RGEscolhaClick(Sender: TObject);
begin
  BtOK.Enabled := RGEscolha.ItemIndex > 0;
end;

end.
