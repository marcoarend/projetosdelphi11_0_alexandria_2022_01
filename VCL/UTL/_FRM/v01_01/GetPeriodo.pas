unit GetPeriodo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkEditDateTimePicker,
  dmkImage, UnDmkEnums;

type
  TFmGetPeriodo = class(TForm)
    Panel1: TPanel;
    TPIni: TdmkEditDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    TPFim: TdmkEditDateTimePicker;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    BtOK: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtOKClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGetPeriodo: TFmGetPeriodo;

implementation

uses UnMyObjects, UnInternalConsts, dmkGeral;

{$R *.DFM}

procedure TFmGetPeriodo.BtOKClick(Sender: TObject);
begin
  VAR_GETDATAI := TPIni.Date;
  VAR_GETDATAF := TPFim.Date;
  Close;
end;

procedure TFmGetPeriodo.BtCancelaClick(Sender: TObject);
begin
  VAR_GETDATAI := 0;
  VAR_GETDATAF := 0;
  Close;
end;

procedure TFmGetPeriodo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGetPeriodo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGetPeriodo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TPIni.Date := Date;
  TPFim.Date := Date;
end;

end.
