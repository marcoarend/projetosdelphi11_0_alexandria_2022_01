object FmPesqPrdTamCor: TFmPesqPrdTamCor
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-001 :: Pesquisa de Produtos pela Descri'#231#227'o'
  ClientHeight = 560
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 398
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 45
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaPrd: TLabel
        Left = 8
        Top = 4
        Width = 129
        Height = 13
        Caption = 'Produto (descri'#231#227'o parcial):'
      end
      object LaTam: TLabel
        Left = 240
        Top = 4
        Width = 137
        Height = 13
        Caption = 'Tamanho (descri'#231#227'o parcial):'
      end
      object LaCor: TLabel
        Left = 384
        Top = 4
        Width = 108
        Height = 13
        Caption = 'Cor (descri'#231#227'o parcial):'
      end
      object Label1: TLabel
        Left = 508
        Top = 4
        Width = 96
        Height = 13
        Caption = 'Sugest'#227'o pelo ERP:'
      end
      object EdPrd: TdmkEdit
        Left = 8
        Top = 20
        Width = 229
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPrdChange
      end
      object EdTam: TdmkEdit
        Left = 240
        Top = 20
        Width = 141
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPrdChange
      end
      object EdTxtPesq: TdmkEdit
        Left = 508
        Top = 20
        Width = 485
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPrdChange
        OnDblClick = EdTxtPesqDblClick
        OnEnter = EdTxtPesqEnter
      end
      object EdCor: TdmkEdit
        Left = 384
        Top = 20
        Width = 121
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPrdChange
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 45
      Width = 1008
      Height = 353
      Align = alClient
      TabOrder = 1
      object DBGPesquisa: TdmkDBGridZTO
        Left = 1
        Top = 1
        Width = 1006
        Height = 351
        Align = alClient
        DataSource = DsPesq
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        OnDblClick = DBGPesquisaDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Reduzido'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nivel1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 260
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TAM'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COR'
            Width = 112
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UnidMed'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TipoProd'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Referencia'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Patrimonio'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EAN13'
            Width = 90
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 454
        Height = 32
        Caption = 'Pesquisa de Produtos pela Descri'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 454
        Height = 32
        Caption = 'Pesquisa de Produtos pela Descri'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 454
        Height = 32
        Caption = 'Pesquisa de Produtos pela Descri'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 446
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 490
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 863
      Top = 15
      Width = 143
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 861
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 144
        Top = 8
        Width = 192
        Height = 13
        Caption = #218'ltimo selecionado em pesquisa anterior:'
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object EdAntSelCod: TdmkEdit
        Left = 144
        Top = 24
        Width = 80
        Height = 21
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdAntSelNom: TdmkEdit
        Left = 228
        Top = 24
        Width = 553
        Height = 21
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    SQL.Strings = (
      'SELECT ggx.Controle Reduzido, ggx.GraGru1 Nivel1, gg1.Nome, '
      'gti.Nome TAM, gcc.Nome COR, gg1.Referencia, gg1.Patrimonio,  '
      'ggx.EAN13, gg1.NCM, med.Sigla UnidMed, pgt.Nome TipoProd '
      'FROM gragrux    ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip  '
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed  '
      'WHERE ggx.Controle<>0 ')
    Left = 636
    Top = 156
    object QrPesqReduzido: TIntegerField
      FieldName = 'Reduzido'
      Required = True
    end
    object QrPesqNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrPesqTAM: TWideStringField
      FieldName = 'TAM'
      Size = 5
    end
    object QrPesqCOR: TWideStringField
      FieldName = 'COR'
      Size = 30
    end
    object QrPesqReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrPesqPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 25
    end
    object QrPesqEAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
    object QrPesqNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrPesqUnidMed: TWideStringField
      FieldName = 'UnidMed'
      Size = 6
    end
    object QrPesqTipoProd: TWideStringField
      FieldName = 'TipoProd'
      Size = 60
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 636
    Top = 204
  end
end
