object FmIPsNaRede: TFmIPsNaRede
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-000 :: IPs na Rede'
  ClientHeight = 689
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 150
        Height = 32
        Caption = 'IPs na Rede'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 150
        Height = 32
        Caption = 'IPs na Rede'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 150
        Height = 32
        Caption = 'IPs na Rede'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 527
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 527
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 527
        Align = alClient
        TabOrder = 0
        object ListBox1: TListBox
          Left = 2
          Top = 261
          Width = 808
          Height = 127
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
          OnClick = ListBox1Click
          OnDblClick = ListBox1DblClick
        end
        object MeResult_: TMemo
          Left = 2
          Top = 388
          Width = 808
          Height = 137
          Align = alBottom
          Color = clBlack
          Font.Charset = ANSI_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Lines.Strings = (
            'Tracert path:'
            ''
            'C:\Windows\System32')
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 50
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object Label1: TLabel
            Left = 4
            Top = 8
            Width = 37
            Height = 13
            Caption = 'Meu IP:'
          end
          object LaMeuIP: TLabel
            Left = 48
            Top = 8
            Width = 97
            Height = 13
            Caption = '000.000.000.000'
            Font.Charset = ANSI_CHARSET
            Font.Color = 270971
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label2: TLabel
            Left = 492
            Top = 8
            Width = 61
            Height = 13
            Caption = 'System path:'
          end
          object CkNames: TCheckBox
            Left = 4
            Top = 28
            Width = 233
            Height = 17
            Caption = 'Pesquisar somente nomes de computadores.'
            TabOrder = 0
          end
          object EdSystemPath: TEdit
            Left = 560
            Top = 4
            Width = 245
            Height = 21
            TabOrder = 1
            Text = 'C:\Windows\System32'
          end
        end
        object SGGeral: TdmkStringGrid
          Left = 2
          Top = 65
          Width = 808
          Height = 196
          Align = alTop
          DefaultColWidth = 28
          DefaultRowHeight = 21
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          OnDblClick = SGGeralDblClick
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 575
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 619
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BitBtn2: TBitBtn
        Tag = 18
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Parar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn2Click
      end
      object Button1: TButton
        Left = 352
        Top = 12
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 2
        OnClick = Button1Click
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object PMPesquisa: TPopupMenu
    Left = 132
    Top = 184
    object Rastreianomes1: TMenuItem
      Caption = 'Rastreia nomes'
      OnClick = Rastreianomes1Click
    end
    object IPseMACs1: TMenuItem
      Caption = 'IPs e MACs'
      OnClick = IPseMACs1Click
    end
  end
end
