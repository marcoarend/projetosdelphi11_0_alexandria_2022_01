unit Sel3RadioGroups;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmSel3RadioGroups = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    PnRGs: TPanel;
    RGItens1: TRadioGroup;
    RGItens2: TRadioGroup;
    RGItens3: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGItens1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PnRGsCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
  private
    { Private declarations }
    procedure VerificaDefinicao();
  public
    { Public declarations }
    FSelOnClick: Boolean;
    FAvisos: array of String;
    FAcoes: array of TSQLType;
    FValores: array of String; 
    FMyCods: array of Integer; 
  end;

  var
  FmSel3RadioGroups: TFmSel3RadioGroups;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmSel3RadioGroups.BtOKClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSel3RadioGroups.BtSaidaClick(Sender: TObject);
begin
  RGItens1.ItemIndex := -1;
  RGItens2.ItemIndex := -1;
  RGItens3.ItemIndex := -1;
  Close;
end;

procedure TFmSel3RadioGroups.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  FormResize(Sender);
end;

procedure TFmSel3RadioGroups.FormCreate(Sender: TObject);
begin
  FSelOnClick := False;
end;

procedure TFmSel3RadioGroups.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [(*LaAviso1, LaAviso2*)], Caption, True, taCenter, 2, 10, 20);
  //
end;

procedure TFmSel3RadioGroups.PnRGsCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
var
  H_RGs: Integer;
begin
  //H_RGs := (RGItens1.Height + RGItens2.Height + RGItens3.Height) div 3;
  H_RGs := PnRGs.Height div 3;
  RGItens1.Height := H_RGs;
  RGItens2.Height := H_RGs;
end;

procedure TFmSel3RadioGroups.RGItens1Click(Sender: TObject);
begin
  VerificaDefinicao();
end;

procedure TFmSel3RadioGroups.VerificaDefinicao();
var
  Definidos: Boolean;
begin
  BtOk.Enabled := False;
  Definidos := (RGItens1.ItemIndex > -1) and
               (RGItens2.ItemIndex > -1) and
               (RGItens3.ItemIndex > -1);
  //
  if Definidos then
  begin
    if FSelOnClick then
      BtOKClick(Self)
    else
      BtOk.Enabled := True;
  end;
end;

end.
