unit DataDef;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, UnInternalConsts, Db;

type
  TFmDataDef = class(TForm)
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    BtConfirma: TBitBtn;
    BitBtn1: TBitBtn;
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmDataDef: TFmDataDef;

implementation

uses UnMyObjects, UnMLAGeral;

{$R *.DFM}

procedure TFmDataDef.BtConfirmaClick(Sender: TObject);
begin
  VAR_DATA_INI_ESCOLHIDA := TPIni.Date;
  VAR_DATA_FIN_ESCOLHIDA := TPFim.Date;
  Close;
end;

procedure TFmDataDef.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDataDef.FormCreate(Sender: TObject);
begin
  VAR_DATA_INI_ESCOLHIDA := 0;
  VAR_DATA_FIN_ESCOLHIDA := 0;
  TPIni.Date := Date;
  TPFim.Date := Date;
end;

procedure TFmDataDef.BitBtn1Click(Sender: TObject);
begin
  VAR_DATA_INI_ESCOLHIDA := 0;
  VAR_DATA_FIN_ESCOLHIDA := 0;
  Close;
end;

end.
