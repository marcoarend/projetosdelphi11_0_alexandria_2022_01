unit GetDataEUser;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkEdit, dmkGeral,
  dmkEditDateTimePicker, dmkImage, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEditCB, UnDmkEnums;

type
  TFmGetDataEUser = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    TPData: TdmkEditDateTimePicker;
    LaHora: TLabel;
    EdHora: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    EdUsuario: TdmkEditCB;
    CBUsuario: TdmkDBLookupComboBox;
    Label2: TLabel;
    QrSenhas: TmySQLQuery;
    DsSenhas: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FSelecionou: Boolean;
    FMinData: TDateTime;
  end;

  var
  FmGetDataEUser: TFmGetDataEUser;

implementation

uses UnMyObjects, UnInternalConsts, DmkDAC_PF, Module;

{$R *.DFM}

procedure TFmGetDataEUser.BtSaidaClick(Sender: TObject);
begin
  FSelecionou := False;
  VAR_GETDATA := 0;
  VAR_GETUSER := 0;
  //
  Close;
end;

procedure TFmGetDataEUser.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  MyObjects.CorIniComponente();
end;

procedure TFmGetDataEUser.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGetDataEUser.FormCreate(Sender: TObject);
begin
  FSelecionou := False;
  VAR_GETDATA := 0;
  VAR_GETUSER := 0;
  TPData.MinDate := VAR_DATA_MINIMA;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSenhas, Dmod.MyDB, [
    'SELECT Numero, Login ',
    'FROM senhas ',
    'WHERE Numero NOT IN (-3, -1) ',
    'ORDER BY Login ',
    '']);
end;

procedure TFmGetDataEUser.BtOKClick(Sender: TObject);
begin
  FSelecionou := True;
  VAR_GETDATA := Int(TPData.Date) + EdHora.ValueVariant;
  VAR_GETUSER := EdUsuario.ValueVariant;
  //
  Close;
end;

end.
