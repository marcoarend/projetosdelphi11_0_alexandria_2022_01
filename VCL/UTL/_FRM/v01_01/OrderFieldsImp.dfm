object FmOrderFieldsImp: TFmOrderFieldsImp
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: Ordena'#231#227'o de tabelas'
  ClientHeight = 462
  ClientWidth = 504
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 504
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 456
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
      object BtDesfazOrdenacao: TBitBtn
        Tag = 329
        Left = 4
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesfazOrdenacaoClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 408
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 268
        Height = 32
        Caption = 'Ordena'#231#227'o de tabelas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 268
        Height = 32
        Caption = 'Ordena'#231#227'o de tabelas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 268
        Height = 32
        Caption = 'Ordena'#231#227'o de tabelas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 504
    Height = 300
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 504
      Height = 300
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 504
        Height = 300
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 252
          Top = 15
          Width = 250
          Height = 283
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object StaticText6: TStaticText
            Left = 0
            Top = 0
            Width = 250
            Height = 17
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Campos a ordenar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object LBFieldsOrdTab: TListBox
            Left = 0
            Top = 185
            Width = 250
            Height = 98
            Align = alBottom
            ItemHeight = 13
            TabOrder = 1
            Visible = False
          end
          object LBFieldsOrdTxt: TListBox
            Left = 0
            Top = 17
            Width = 200
            Height = 168
            Align = alClient
            ItemHeight = 13
            TabOrder = 2
            OnClick = LBFieldsOrdTxtClick
          end
          object Panel6: TPanel
            Left = 200
            Top = 17
            Width = 50
            Height = 168
            Align = alRight
            TabOrder = 3
            object BtDown: TBitBtn
              Tag = 31
              Left = 4
              Top = 46
              Width = 40
              Height = 40
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtDownClick
            end
            object BtUp: TBitBtn
              Tag = 32
              Left = 4
              Top = 3
              Width = 40
              Height = 40
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtUpClick
            end
          end
        end
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 250
          Height = 283
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object StaticText1: TStaticText
            Left = 0
            Top = 0
            Width = 250
            Height = 17
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Campos dispon'#237'veis'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object LBFieldsDisTab: TListBox
            Left = 0
            Top = 185
            Width = 250
            Height = 98
            Align = alBottom
            ItemHeight = 13
            TabOrder = 1
            Visible = False
          end
          object LBFieldsDisTxt: TListBox
            Left = 0
            Top = 17
            Width = 200
            Height = 168
            Align = alClient
            ItemHeight = 13
            TabOrder = 2
            OnClick = LBFieldsDisTxtClick
          end
          object Panel8: TPanel
            Left = 200
            Top = 17
            Width = 50
            Height = 168
            Align = alRight
            TabOrder = 3
            object BtLeft: TBitBtn
              Tag = 2
              Left = 4
              Top = 46
              Width = 40
              Height = 40
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtLeftClick
            end
            object BtRight: TBitBtn
              Tag = 3
              Left = 4
              Top = 3
              Width = 40
              Height = 40
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtRightClick
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 348
    Width = 504
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 500
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 392
    Width = 504
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 358
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 356
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
end
