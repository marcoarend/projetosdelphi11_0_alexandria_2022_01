unit GetMinMax;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkEdit, dmkGeral,
  dmkEditDateTimePicker, dmkImage, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEditCB, UnDmkEnums;

type
  TFmGetMinMax = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    LaResultMin: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaResultMax: TLabel;
    EdResultMin: TdmkEdit;
    EdResultMax: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FMinData: TDateTime;
  end;

  var
  FmGetMinMax: TFmGetMinMax;

implementation

uses UnMyObjects, UnInternalConsts, DmkDAC_PF;

{$R *.DFM}

procedure TFmGetMinMax.BtSaidaClick(Sender: TObject);
begin
  VAR_GETVALOR_GET := False;
  VAR_GETMINMAXDMK_Min := 0;
  VAR_GETMINMAXDMK_Max := 0;
  //
  Close;
end;

procedure TFmGetMinMax.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  MyObjects.CorIniComponente();
end;

procedure TFmGetMinMax.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGetMinMax.FormCreate(Sender: TObject);
begin
  VAR_GETVALOR_GET := False;
  VAR_GETMINMAXDMK_Min := 0;
  VAR_GETMINMAXDMK_Max := 0;
end;

procedure TFmGetMinMax.BtOKClick(Sender: TObject);
begin
  VAR_GETMINMAXDMK_Min := EdResultMin.ValueVariant;
  VAR_GETMINMAXDMK_Max := EdResultMax.ValueVariant;
  if VAR_GETMINMAXDMK_Max >= VAR_GETMINMAXDMK_Min then
  begin
    VAR_GETVALOR_GET := True;
    //
    Close;
  end else
  begin
    Geral.MB_Aviso(
    'O valor m�ximo deve ser igual ou maior que o valor m�nimo!');
    //
    VAR_GETMINMAXDMK_Min := 0;
    VAR_GETMINMAXDMK_Max := 0;
  end;
end;

end.
