unit ZForge;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, ZipForge, ComCtrls,
  dmkGeral, dmkImage, UnDMkEnums, dmkCompoStore, dmkPermissoes, dmkEdit;

type
  TzForgeTip = (zftArquivo=0, zftDiretorio=1);
  TFmZForge = class(TForm)
    Panel1: TPanel;
    pbProgress: TProgressBar;
    LaMsg: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    ZipForge1: TZipForge;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ZipForge1FileProgress(Sender: TObject; FileName: string;
      Progress: Double; Operation: TZFProcessOperation;
      ProgressPhase: TZFProgressPhase; var Cancel: Boolean);
    procedure ZipForge1OverallProgress(Sender: TObject; Progress: Double;
      Operation: TZFProcessOperation; ProgressPhase: TZFProgressPhase;
      var Cancel: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function ExtrairArquivo(): String; overload;
    function ExtrairArquivo(Arquivo, DirDestino: String): String; overload;
    function ZipaArquivo(Tipo: TzForgeTip; SaveDir, CamArq, NomeArq, Senha: String;
             Encryptar, Delete: Boolean; AlgorEncrypt:
             TZFCryptoAlgorithm = caAES_128; ValidaArquivo: Boolean = False): String;
  end;

  var
  FmZForge: TFmZForge;

implementation

uses UnMyObjects, UnDmkProcFunc, Principal, MyGlyfs;

{$R *.DFM}


procedure TFmZForge.BtSaidaClick(Sender: TObject);
begin
  if TFmZForge(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

function TFmZForge.ExtrairArquivo(): String;
var
  Arq, Dir: string;
  dmkEdit: TDmkEdit;
begin
  Result := '';
  //
  if MyObjects.FileOpenDialog(Self, '', '', 'Abrir arquivo',
    'Arquivos ZIP|*.ZIP;Arquivos zip|*.zip', [], Arq) then
  begin
    if Geral.MB_Pergunta('Deseja extrair o arquivo selecionado em um diret�rio diferente?') = ID_YES then
    begin
      dmkEdit := TdmkEdit.Create(nil);
      try
        dmkEdit.ValueVariant := ExtractFileDir(Arq);
        //
        MyObjects.DefineDiretorio(Self, dmkEdit);
        //
        Dir := dmkEdit.ValueVariant;
        //
      finally
        dmkEdit.Free;
      end;
    end else
      Dir := ExtractFileDir(Arq);
    //
    Result := ExtrairArquivo(Arq, Dir);
  end;
end;

function TFmZForge.ExtrairArquivo(Arquivo, DirDestino: String): String;
begin
  Result := '';
  //
  if MyObjects.FIC(Length(Arquivo) = 0, nil, 'Arquivo n�o definido!') then Exit;
  if MyObjects.FIC(not FileExists(Arquivo), nil, 'Arquivo n�o localizado!') then Exit;
  if MyObjects.FIC(not DirectoryExists(DirDestino), nil, 'Diret�rio destino n�o localizado!') then Exit;
  if MyObjects.FIC(UpperCase(ExtractFileExt(Arquivo)) <> '.ZIP', nil, 'O arquivo selecionado deve estar no formato ZIP!') then Exit;
  //
  try
    with ZipForge1 do
    begin
      FileName := Arquivo;
      //
      OpenArchive(fmOpenRead);
      //
      BaseDir := DirDestino;
      //
      ExtractFiles('*.*');
      CloseArchive();
      //
      Result := DirDestino;
    end;
  except
  on E: Exception do
    begin
      Writeln('Exception: ', E.Message);
      Readln;
    end;
  end;
end;

procedure TFmZForge.FormActivate(Sender: TObject);
begin
  if TFmZForge(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmZForge.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType     := stLok;
  pbProgress.Position := 0;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmZForge.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmZForge.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

function TFmZForge.ZipaArquivo(Tipo: TzForgeTip; SaveDir, CamArq, NomeArq,
  Senha: String; Encryptar, Delete: Boolean;
  AlgorEncrypt: TZFCryptoAlgorithm = caAES_128; ValidaArquivo: Boolean = False): String;
{ ********************* COMO USAR ******************************************* }
{ Tipo      = 0 - Arquivo (zftArquivo) 1 - Pasta (zftDiretorio)               }
{ SaveDir   = Diret�rio onde ser� salvo o arquivo zipado                      }
{ CamArq    = Caminho total do arquivo ou pasta a ser zipado                  }
{ NomeArq   = Novo nome para o arquivo zipado                                 }
{ Senha     = Senha do arquivo somente se a vari�vel encryptar estiver "True" }
{ Encryptar = Sim / N�o                                                       }
{ Delete    = Se "Sim" deleta o arquivo original (n�o zipado)                 }
{ *************************************************************************** }

  function ValidaArquivoZipado(Arquivo, Pass: String): String;
  begin
    try
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Validando arquivo zipado');
      //
      with ZipForge1 do
      begin
        if Pass <> '' then
        begin
          EncryptionMethod := AlgorEncrypt;
          Password         := AnsiString(Pass);
        end;
        FileName := Arquivo;
        OpenArchive(fmOpenReadWrite);
        TestFiles('*.*');
        CloseArchive();
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      end;
      //
      Result := Arquivo;
    except
      on E: Exception do
      begin
        Geral.MB_Erro('Erro: ' + E.Message);
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Erro: ' + E.Message);
        //
        Result := '';
      end;
    end;
  end;

var
  EZip: Boolean;
  NomeArqZip, ArqDir, Arq, Ext: String;
begin
  Result := '';
  //
  if MyObjects.FIC(Length(SaveDir) = 0, nil, 'Diret�rio destino n�o definido!') then Exit;
  if MyObjects.FIC(Length(CamArq) = 0, nil, 'Arquivo n�o definido!') then Exit;
  if Tipo = zftArquivo then
  begin
    if MyObjects.FIC(not FileExists(CamArq), nil, 'Arquivo n�o localizado!') then Exit;
  end else
  begin
    if MyObjects.FIC(not DirectoryExists(CamArq), nil, 'Diret�rio n�o localizado!') then Exit;
  end;
  if MyObjects.FIC(Length(NomeArq) = 0, nil, 'Nome do arquivo n�o definido!') then Exit;
  if MyObjects.FIC((Encryptar) and (Length(Senha) = 0), nil, 'Senha n�o definida!') then Exit;
  //
  {
  O WinZip n�o precisa estar instalado
  //Verifica se o WinZip est� instalado
  if Geral.ReadAppKeyLM('DisplayName',
    'Micsoft\Windows\CurrentVersion\Uninstall\WinZip', ktString, '0') = '' then
  begin
    Geral.MB_('WinZip n�o localizado!' + sLineBreak +
      'Intale o Winzip e tente novamente!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //Verifica se o WZCline est� instalado
  if Geral.ReadAppKeyLM('DisplayName',
    'Micsoft\Windows\CurrentVersion\Uninstall\WZCline', ktString, '0') = '' then
  begin
    Geral.MB_('WZCline n�o localizado!' + sLineBreak +
      'Avise a DERMATEK!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  }
  //Verifica se o arquivo � ZIP
  if (Tipo = zftArquivo) and (UpperCase(ExtractFileExt(CamArq)) = '.ZIP') then
    EZip := True
  else
    EZip := False;
  //
  if Tipo = zftArquivo then
    ArqDir := ExtractFileDir(CamArq)
  else
    ArqDir := CamArq;

  try
  with ZipForge1 do
  begin
    if EZip = True then
      NomeArqZip := SaveDir + '\' + NomeArq + '_zip.zip'
    else
      NomeArqZip := SaveDir + '\' + NomeArq + '.zip';
    //
    FileName := NomeArqZip;
    //
    if FileExists(FileName) then
    begin
      if Geral.MB_Pergunta(
        'O caminho selecionado j� exite! Deseja substitu�-lo?') <> ID_YES
      then
        Exit;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Zipando arquivos');
    //
    OpenArchive(fmCreate);
    BaseDir := ArqDir;
    if Encryptar then
    begin
      EncryptionMethod := AlgorEncrypt;
      Password         := AnsiString(Senha);
    end;
    case Tipo of
      zftArquivo:
      begin
        AddFiles(CamArq);
      end;
      zftDiretorio:
      begin
        if Copy(ArqDir, Length(ArqDir), 1) = '\' then
          AddFiles(ArqDir + '*.*')
        else
          AddFiles(ArqDir + '\*.*')
      end;
    end;
    CloseArchive;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    if Delete then
    begin
      case Tipo of
        zftArquivo:
        begin
          if not DeleteFile(CamArq) then
            Geral.MB_Aviso('N�o foi poss�vel excluir o arquivo tempor�rio!');
        end;
        zftDiretorio:
        begin
          if not dmkPF.DelDir(ArqDir) then
            Geral.MB_Aviso('N�o foi poss�vel excluir o diret�rio tempor�rio!');
        end;
      end;
    end;
  end;
  if ValidaArquivo = True then
    Result := ValidaArquivoZipado(NomeArqZip, Senha)
  else
    Result := NomeArqZip;
  except
  on E: Exception do
    begin
      Writeln('Erro: ' + E.Message);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Erro: ' + E.Message);
    end;
  end;
end;

procedure TFmZForge.ZipForge1FileProgress(Sender: TObject; FileName: string;
  Progress: Double; Operation: TZFProcessOperation;
  ProgressPhase: TZFProgressPhase; var Cancel: Boolean);
begin
  LaMsg.Caption := 'Zipando: ' + FileName;
  Application.ProcessMessages;
end;

procedure TFmZForge.ZipForge1OverallProgress(Sender: TObject; Progress: Double;
  Operation: TZFProcessOperation; ProgressPhase: TZFProgressPhase;
  var Cancel: Boolean);
begin
  pbProgress.Position := Trunc(Progress);
  pbProgress.Update;
  Application.ProcessMessages;
end;

end.
