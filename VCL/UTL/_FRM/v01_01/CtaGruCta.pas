unit CtaGruCta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmCtaGruCta = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    CBConta: TdmkDBLookupComboBox;
    DsExcelGru: TDataSource;
    EdConta: TdmkEditCB;
    QrExcelGru: TmySQLQuery;
    QrExcelGruCodigo: TIntegerField;
    QrExcelGruNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FExcelGru: Integer;
  end;

var
  FmCtaGruCta: TFmCtaGruCta;

implementation

uses UnMyObjects, Module, Entidades, DmkDAC_PF;

{$R *.DFM}

procedure TFmCtaGruCta.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtaGruCta.BtConfirmaClick(Sender: TObject);
begin
  FExcelGru := StrToInt(EdConta.Text);
  if FExcelGru = 0 then
  begin
    if Geral.MB_Pergunta('Confirma a anula��o da conta sazonal ' +
    'dos itens selecionados?') <>
    ID_YES then EdConta.SetFocus;
  end;
  Close;
end;

procedure TFmCtaGruCta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtaGruCta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCtaGruCta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FExcelGru := -1000;
  UnDmkDAC_PF.AbreQuery(QrExcelGru, Dmod.MyDB);
end;

end.
