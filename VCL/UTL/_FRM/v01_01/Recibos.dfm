object FmRecibos: TFmRecibos
  Left = 354
  Top = 177
  BorderStyle = bsSizeToolWin
  Caption = 'IMP-RECIB-002 :: Impress'#227'o de Recibos'
  ClientHeight = 702
  ClientWidth = 902
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  object Label8: TLabel
    Left = 20
    Top = 542
    Width = 58
    Height = 16
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Emitente: '
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 902
    Height = 647
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 902
      Height = 647
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alClient
      TabHeight = 25
      TabOrder = 0
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Recibo Pessoa F'#237'sica'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object LaResponsavelP: TLabel
          Left = 50
          Top = 558
          Width = 85
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Respons'#225'vel'
          Transparent = True
        end
        object Label1: TLabel
          Left = 44
          Top = 5
          Width = 51
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'N'#250'mero:'
          FocusControl = EdNumeroE
        end
        object Label2: TLabel
          Left = 74
          Top = 89
          Width = 63
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Recebi de'
          FocusControl = EdEPagador
        end
        object Label3: TLabel
          Left = 74
          Top = 123
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Endere'#231'o: '
        end
        object Label4: TLabel
          Left = 74
          Top = 197
          Width = 104
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'A import'#226'ncia de '
        end
        object Label5: TLabel
          Left = 74
          Top = 305
          Width = 62
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Referente:'
        end
        object Label6: TLabel
          Left = 74
          Top = 428
          Width = 58
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Emitente: '
        end
        object Label7: TLabel
          Left = 74
          Top = 458
          Width = 62
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Endere'#231'o:'
        end
        object Label10: TLabel
          Left = 625
          Top = 0
          Width = 35
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Valor:'
          FocusControl = EdValorE
        end
        object Label20: TLabel
          Left = 679
          Top = 423
          Width = 29
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'CPF:'
        end
        object Label21: TLabel
          Left = 679
          Top = 89
          Width = 29
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'CPF:'
        end
        object Label24: TLabel
          Left = 50
          Top = 527
          Width = 85
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Local e data:'
          Transparent = True
        end
        object EdPPagador: TdmkEdit
          Left = 143
          Top = 84
          Width = 488
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPCidade: TdmkEdit
          Left = 143
          Top = 156
          Width = 227
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPCEP: TdmkEdit
          Left = 463
          Top = 156
          Width = 80
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPPais: TdmkEdit
          Left = 566
          Top = 156
          Width = 134
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPTe1: TdmkEdit
          Left = 724
          Top = 156
          Width = 149
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 12
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPUF: TdmkEdit
          Left = 399
          Top = 156
          Width = 40
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPRua: TdmkEdit
          Left = 143
          Top = 121
          Width = 227
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPNumero: TdmkEdit
          Left = 394
          Top = 121
          Width = 65
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPCompl: TdmkEdit
          Left = 482
          Top = 121
          Width = 218
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPBairro: TdmkEdit
          Left = 724
          Top = 121
          Width = 149
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPCNPJC: TdmkEdit
          Left = 724
          Top = 84
          Width = 149
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEmitenteP: TdmkEdit
          Left = 143
          Top = 420
          Width = 498
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 15
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPRuaE: TdmkEdit
          Left = 143
          Top = 457
          Width = 224
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 17
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPCidadeE: TdmkEdit
          Left = 143
          Top = 492
          Width = 224
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 21
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPUFE: TdmkEdit
          Left = 399
          Top = 492
          Width = 39
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 22
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPNumeroE: TdmkEdit
          Left = 394
          Top = 457
          Width = 59
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 18
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPCEPE: TdmkEdit
          Left = 463
          Top = 492
          Width = 79
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 23
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPComplE: TdmkEdit
          Left = 482
          Top = 457
          Width = 218
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 19
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPPaisE: TdmkEdit
          Left = 566
          Top = 492
          Width = 139
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 24
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPTe1E: TdmkEdit
          Left = 724
          Top = 492
          Width = 149
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 25
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPBairroE: TdmkEdit
          Left = 724
          Top = 457
          Width = 149
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 20
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCNPJP: TdmkEdit
          Left = 724
          Top = 420
          Width = 149
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 16
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdValorP: TdmkEdit
          Left = 625
          Top = 20
          Width = 243
          Height = 35
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -28
          Font.Name = 'Lucida Console'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValorPChange
        end
        object EdNumeroP: TdmkEdit
          Left = 44
          Top = 25
          Width = 287
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Lucida Console'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object TextoP: TdmkMemo
          Left = 158
          Top = 305
          Width = 715
          Height = 95
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 14
          UpdType = utYes
        end
        object ExtensoP: TdmkMemo
          Left = 192
          Top = 197
          Width = 681
          Height = 90
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 13
          UpdType = utYes
        end
        object EdPResponsavel: TdmkEdit
          Left = 143
          Top = 554
          Width = 730
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 27
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPLocalData: TdmkEdit
          Left = 143
          Top = 523
          Width = 730
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 26
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Recibo Pessoa Jur'#237'dica'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label11: TLabel
          Left = 44
          Top = 5
          Width = 51
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'N'#250'mero:'
          FocusControl = EdNumeroE
        end
        object Label12: TLabel
          Left = 625
          Top = 0
          Width = 35
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Valor:'
          FocusControl = EdValorE
        end
        object Label13: TLabel
          Left = 74
          Top = 89
          Width = 63
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Recebi de'
          FocusControl = EdEPagador
        end
        object Label14: TLabel
          Left = 74
          Top = 123
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Endere'#231'o: '
        end
        object Label15: TLabel
          Left = 74
          Top = 197
          Width = 104
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'A import'#226'ncia de '
        end
        object Label16: TLabel
          Left = 74
          Top = 305
          Width = 62
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Referente:'
        end
        object Label9: TLabel
          Left = 74
          Top = 428
          Width = 58
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Emitente: '
        end
        object Label17: TLabel
          Left = 74
          Top = 458
          Width = 62
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Endere'#231'o:'
        end
        object Label18: TLabel
          Left = 679
          Top = 89
          Width = 38
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'CNPJ:'
        end
        object Label19: TLabel
          Left = 679
          Top = 423
          Width = 38
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'CNPJ:'
        end
        object LaResponsavelE: TLabel
          Left = 50
          Top = 558
          Width = 85
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Respons'#225'vel:'
          Transparent = True
        end
        object Label23: TLabel
          Left = 50
          Top = 527
          Width = 85
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Local e data:'
          Transparent = True
        end
        object EdNumeroE: TdmkEdit
          Left = 44
          Top = 25
          Width = 287
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Lucida Console'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdValorE: TdmkEdit
          Left = 625
          Top = 20
          Width = 243
          Height = 35
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -28
          Font.Name = 'Lucida Console'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValorEChange
        end
        object EdEPagador: TdmkEdit
          Left = 143
          Top = 84
          Width = 488
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdERuaC: TdmkEdit
          Left = 143
          Top = 121
          Width = 227
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdECidadeC: TdmkEdit
          Left = 143
          Top = 156
          Width = 227
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdENumeroC: TdmkEdit
          Left = 394
          Top = 121
          Width = 65
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEUFC: TdmkEdit
          Left = 399
          Top = 156
          Width = 40
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEComplC: TdmkEdit
          Left = 482
          Top = 121
          Width = 218
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdECEPC: TdmkEdit
          Left = 463
          Top = 156
          Width = 80
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEPaisC: TdmkEdit
          Left = 566
          Top = 156
          Width = 134
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdETe1C: TdmkEdit
          Left = 724
          Top = 156
          Width = 149
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 12
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEBairroC: TdmkEdit
          Left = 724
          Top = 121
          Width = 149
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdERuaE: TdmkEdit
          Left = 143
          Top = 457
          Width = 224
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 17
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdECidadeE: TdmkEdit
          Left = 143
          Top = 492
          Width = 224
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 21
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdENumeroE: TdmkEdit
          Left = 394
          Top = 457
          Width = 59
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 18
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEUFE: TdmkEdit
          Left = 399
          Top = 492
          Width = 39
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 22
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdECEPE: TdmkEdit
          Left = 463
          Top = 492
          Width = 79
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 23
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEComplE: TdmkEdit
          Left = 482
          Top = 457
          Width = 218
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 19
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEPaisE: TdmkEdit
          Left = 566
          Top = 492
          Width = 139
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 24
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEBairroE: TdmkEdit
          Left = 724
          Top = 457
          Width = 149
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 20
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdETe1E: TdmkEdit
          Left = 724
          Top = 492
          Width = 149
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 25
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEmitenteE: TdmkEdit
          Left = 138
          Top = 420
          Width = 498
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 15
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdECNPJE: TdmkEdit
          Left = 724
          Top = 420
          Width = 149
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 16
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdECNPJC: TdmkEdit
          Left = 724
          Top = 84
          Width = 149
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object ExtensoE: TdmkMemo
          Left = 192
          Top = 197
          Width = 681
          Height = 90
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 13
          UpdType = utYes
        end
        object TextoE: TdmkMemo
          Left = 158
          Top = 305
          Width = 715
          Height = 95
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 14
          UpdType = utYes
        end
        object EdEResponsavel: TdmkEdit
          Left = 143
          Top = 554
          Width = 730
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 27
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdELocalData: TdmkEdit
          Left = 143
          Top = 523
          Width = 730
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 26
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 647
    Width = 902
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object BtImprime: TBitBtn
      Tag = 5
      Left = 15
      Top = 2
      Width = 111
      Height = 50
      Hint = 'Preview'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Imprime'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtImprimeClick
    end
    object BtSaida: TBitBtn
      Tag = 15
      Left = 777
      Top = 2
      Width = 110
      Height = 50
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 308
    Top = 44
  end
  object frxRecibo1: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38009.439963981500000000
    ReportOptions.LastChange = 39098.923307291700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VARF_NOTCOLOR> = True then Shape8.Color := clWhite;'
      'end.')
    OnGetValue = frxRecibo1GetValue
    OnUserFunction = frxRecibo1UserFunction
    Left = 336
    Top = 44
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object Shape8: TfrxShapeView
        Left = 3.779530000000000000
        Width = 748.346940000000000000
        Height = 487.559370000000000000
        Fill.BackColor = 14342874
        Shape = skRoundRectangle
      end
      object Shape1: TfrxShapeView
        Left = 30.236240000000000000
        Top = 26.456710000000000000
        Width = 260.000000000000000000
        Height = 48.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo2: TfrxMemoView
        Left = 74.236240000000000000
        Top = 34.456710000000000000
        Width = 208.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[NUMERO_P]')
        ParentFont = False
      end
      object Memo28: TfrxMemoView
        Left = 34.236240000000000000
        Top = 26.456710000000000000
        Width = 24.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#186)
        ParentFont = False
      end
      object Shape2: TfrxShapeView
        Left = 470.236240000000000000
        Top = 26.456710000000000000
        Width = 260.000000000000000000
        Height = 48.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo29: TfrxMemoView
        Left = 514.236240000000000000
        Top = 38.456710000000000000
        Width = 208.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[VALOR_P]')
        ParentFont = False
      end
      object Memo30: TfrxMemoView
        Left = 474.236240000000000000
        Top = 30.456710000000000000
        Width = 36.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
      end
      object Shape3: TfrxShapeView
        Left = 30.236240000000000000
        Top = 78.456710000000000000
        Width = 700.000000000000000000
        Height = 112.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo4: TfrxMemoView
        Left = 38.236240000000000000
        Top = 80.456710000000000000
        Width = 496.000000000000000000
        Height = 30.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Recebemos de [NOME_PC]')
        ParentFont = False
      end
      object Memo3: TfrxMemoView
        Left = 126.236240000000000000
        Top = 110.456710000000000000
        Width = 188.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[RUA_PC]')
        ParentFont = False
      end
      object Memo1: TfrxMemoView
        Left = 598.236240000000000000
        Top = 82.456710000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[CNPJ_PC]')
        ParentFont = False
      end
      object Memo6: TfrxMemoView
        Left = 598.236240000000000000
        Top = 130.456710000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[TE1_PC]')
        ParentFont = False
      end
      object Memo7: TfrxMemoView
        Left = 330.236240000000000000
        Top = 110.456710000000000000
        Width = 56.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[NUMERO_PC]')
        ParentFont = False
      end
      object Memo8: TfrxMemoView
        Left = 402.236240000000000000
        Top = 110.456710000000000000
        Width = 184.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[COMPL_PC]')
        ParentFont = False
      end
      object Memo9: TfrxMemoView
        Left = 598.236240000000000000
        Top = 110.456710000000000000
        Width = 128.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[BAIRRO_PC]')
        ParentFont = False
      end
      object Memo10: TfrxMemoView
        Left = 126.236240000000000000
        Top = 130.456710000000000000
        Width = 188.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CIDADE_PC]')
        ParentFont = False
      end
      object Memo11: TfrxMemoView
        Left = 318.236240000000000000
        Top = 130.456710000000000000
        Width = 40.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[UF_PC]')
        ParentFont = False
      end
      object Memo12: TfrxMemoView
        Left = 362.236240000000000000
        Top = 130.456710000000000000
        Width = 104.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CEP_PC]')
        ParentFont = False
      end
      object Memo13: TfrxMemoView
        Left = 470.236240000000000000
        Top = 130.456710000000000000
        Width = 116.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[PAIS_PC]')
        ParentFont = False
      end
      object Memo14: TfrxMemoView
        Left = 122.236240000000000000
        Top = 150.456710000000000000
        Width = 600.000000000000000000
        Height = 38.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[EXTENSO_P]')
        ParentFont = False
      end
      object Memo31: TfrxMemoView
        Left = 534.236240000000000000
        Top = 82.456710000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'CNPJ / CPF: ')
        ParentFont = False
      end
      object Memo32: TfrxMemoView
        Left = 38.236240000000000000
        Top = 110.456710000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
      end
      object Memo33: TfrxMemoView
        Left = 38.236240000000000000
        Top = 150.456710000000000000
        Width = 80.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'A import'#226'ncia de:')
        ParentFont = False
      end
      object Shape4: TfrxShapeView
        Left = 30.236240000000000000
        Top = 194.456710000000000000
        Width = 700.000000000000000000
        Height = 68.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo16: TfrxMemoView
        Left = 42.236240000000000000
        Top = 198.456710000000000000
        Width = 684.000000000000000000
        Height = 62.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Referente a: [MOTIVO_P]')
        ParentFont = False
      end
      object Shape5: TfrxShapeView
        Left = 30.236240000000000000
        Top = 266.456710000000000000
        Width = 700.000000000000000000
        Height = 88.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo15: TfrxMemoView
        Left = 34.236240000000000000
        Top = 272.456710000000000000
        Width = 496.000000000000000000
        Height = 17.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Emitente: [EMITENTEP]')
        ParentFont = False
      end
      object Memo37: TfrxMemoView
        Left = 34.236240000000000000
        Top = 290.456710000000000000
        Width = 64.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
      end
      object Shape6: TfrxShapeView
        Left = 30.236240000000000000
        Top = 358.456710000000000000
        Width = 700.000000000000000000
        Height = 32.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo55: TfrxMemoView
        Left = 38.236240000000000000
        Top = 370.456710000000000000
        Width = 680.000000000000000000
        Height = 18.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[LOCAL_E_DATA_P]')
        ParentFont = False
      end
      object Shape7: TfrxShapeView
        Left = 30.236240000000000000
        Top = 394.456710000000000000
        Width = 700.000000000000000000
        Height = 56.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo27: TfrxMemoView
        Left = 358.236240000000000000
        Top = 430.456710000000000000
        Width = 368.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftTop]
        HAlign = haRight
        Memo.UTF8W = (
          '[RESPONSAVEL_P]')
        ParentFont = False
      end
      object Memo5: TfrxMemoView
        Left = 294.803340000000000000
        Top = 26.456710000000000000
        Width = 170.078850000000000000
        Height = 45.354360000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -35
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'RECIBO')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        Left = 124.724490000000000000
        Top = 300.126160000000000000
        Width = 188.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[RUA_PE]')
        ParentFont = False
      end
      object Memo18: TfrxMemoView
        Left = 596.724490000000000000
        Top = 272.126160000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[CNPJ_PE]')
        ParentFont = False
      end
      object Memo19: TfrxMemoView
        Left = 596.724490000000000000
        Top = 320.126160000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[TE1_PE]')
        ParentFont = False
      end
      object Memo20: TfrxMemoView
        Left = 400.724490000000000000
        Top = 300.126160000000000000
        Width = 184.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[COMPL_PE]')
        ParentFont = False
      end
      object Memo21: TfrxMemoView
        Left = 596.724490000000000000
        Top = 300.126160000000000000
        Width = 128.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[BAIRRO_PE]')
        ParentFont = False
      end
      object Memo22: TfrxMemoView
        Left = 124.724490000000000000
        Top = 320.126160000000000000
        Width = 188.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CIDADE_PE]')
        ParentFont = False
      end
      object Memo23: TfrxMemoView
        Left = 316.724490000000000000
        Top = 320.126160000000000000
        Width = 40.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[UF_PE]')
        ParentFont = False
      end
      object Memo24: TfrxMemoView
        Left = 360.724490000000000000
        Top = 320.126160000000000000
        Width = 104.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CEP_PE]')
        ParentFont = False
      end
      object Memo25: TfrxMemoView
        Left = 468.724490000000000000
        Top = 320.126160000000000000
        Width = 116.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[PAIS_PE]')
        ParentFont = False
      end
      object Memo26: TfrxMemoView
        Left = 532.724490000000000000
        Top = 272.126160000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'CNPJ / CPF: ')
        ParentFont = False
      end
    end
  end
  object frxRecibo2: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38009.439963981500000000
    ReportOptions.LastChange = 39098.923307291700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VARF_NOTCOLOR> = True then Shape8.Color := clWhite;'
      'end.')
    OnGetValue = frxRecibo1GetValue
    OnUserFunction = frxRecibo2UserFunction
    Left = 364
    Top = 44
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object Shape8: TfrxShapeView
        Left = 3.779530000000000000
        Width = 748.346940000000000000
        Height = 487.559370000000000000
        Fill.BackColor = 14342874
        Shape = skRoundRectangle
      end
      object Shape1: TfrxShapeView
        Left = 30.236240000000000000
        Top = 26.456710000000000000
        Width = 260.000000000000000000
        Height = 48.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo2: TfrxMemoView
        Left = 74.236240000000000000
        Top = 34.456710000000000000
        Width = 208.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[NUMERO_E]')
        ParentFont = False
      end
      object Memo28: TfrxMemoView
        Left = 34.236240000000000000
        Top = 26.456710000000000000
        Width = 24.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#186)
        ParentFont = False
      end
      object Shape2: TfrxShapeView
        Left = 470.236240000000000000
        Top = 26.456710000000000000
        Width = 260.000000000000000000
        Height = 48.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo29: TfrxMemoView
        Left = 514.236240000000000000
        Top = 38.456710000000000000
        Width = 208.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[VALOR_E]')
        ParentFont = False
      end
      object Memo30: TfrxMemoView
        Left = 474.236240000000000000
        Top = 30.456710000000000000
        Width = 36.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
      end
      object Shape3: TfrxShapeView
        Left = 30.236240000000000000
        Top = 78.456710000000000000
        Width = 700.000000000000000000
        Height = 112.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo4: TfrxMemoView
        Left = 38.236240000000000000
        Top = 80.456710000000000000
        Width = 496.000000000000000000
        Height = 30.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Recebemos de [NOME_EC]')
        ParentFont = False
      end
      object Memo3: TfrxMemoView
        Left = 126.236240000000000000
        Top = 110.456710000000000000
        Width = 188.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[RUA_EC]')
        ParentFont = False
      end
      object Memo1: TfrxMemoView
        Left = 598.236240000000000000
        Top = 82.456710000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[CNPJ_EC]')
        ParentFont = False
      end
      object Memo6: TfrxMemoView
        Left = 598.236240000000000000
        Top = 130.456710000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[TE1_EC]')
        ParentFont = False
      end
      object Memo7: TfrxMemoView
        Left = 330.236240000000000000
        Top = 110.456710000000000000
        Width = 56.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[NUMERO_EC]')
        ParentFont = False
      end
      object Memo8: TfrxMemoView
        Left = 402.236240000000000000
        Top = 110.456710000000000000
        Width = 184.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[COMPL_EC]')
        ParentFont = False
      end
      object Memo9: TfrxMemoView
        Left = 598.236240000000000000
        Top = 110.456710000000000000
        Width = 128.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[BAIRRO_EC]')
        ParentFont = False
      end
      object Memo10: TfrxMemoView
        Left = 126.236240000000000000
        Top = 130.456710000000000000
        Width = 188.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CIDADE_EC]')
        ParentFont = False
      end
      object Memo11: TfrxMemoView
        Left = 318.236240000000000000
        Top = 130.456710000000000000
        Width = 40.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[UF_EC]')
        ParentFont = False
      end
      object Memo12: TfrxMemoView
        Left = 362.236240000000000000
        Top = 130.456710000000000000
        Width = 104.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CEP_EC]')
        ParentFont = False
      end
      object Memo13: TfrxMemoView
        Left = 470.236240000000000000
        Top = 130.456710000000000000
        Width = 116.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[PAIS_EC]')
        ParentFont = False
      end
      object Memo14: TfrxMemoView
        Left = 122.236240000000000000
        Top = 150.456710000000000000
        Width = 600.000000000000000000
        Height = 38.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[EXTENSO_E]')
        ParentFont = False
      end
      object Memo31: TfrxMemoView
        Left = 534.236240000000000000
        Top = 82.456710000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'CNPJ / CPF: ')
        ParentFont = False
      end
      object Memo32: TfrxMemoView
        Left = 38.236240000000000000
        Top = 110.456710000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
      end
      object Memo33: TfrxMemoView
        Left = 38.236240000000000000
        Top = 150.456710000000000000
        Width = 80.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'A import'#226'ncia de:')
        ParentFont = False
      end
      object Shape4: TfrxShapeView
        Left = 30.236240000000000000
        Top = 194.456710000000000000
        Width = 700.000000000000000000
        Height = 68.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo16: TfrxMemoView
        Left = 42.236240000000000000
        Top = 198.456710000000000000
        Width = 684.000000000000000000
        Height = 62.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Referente a: [MOTIVO_E]')
        ParentFont = False
      end
      object Shape5: TfrxShapeView
        Left = 30.236240000000000000
        Top = 266.456710000000000000
        Width = 700.000000000000000000
        Height = 88.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo15: TfrxMemoView
        Left = 34.236240000000000000
        Top = 272.456710000000000000
        Width = 496.000000000000000000
        Height = 17.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Emitente: [EMITENTEP]')
        ParentFont = False
      end
      object Memo37: TfrxMemoView
        Left = 34.236240000000000000
        Top = 290.456710000000000000
        Width = 64.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
      end
      object Shape6: TfrxShapeView
        Left = 30.236240000000000000
        Top = 358.456710000000000000
        Width = 700.000000000000000000
        Height = 32.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo55: TfrxMemoView
        Left = 38.236240000000000000
        Top = 370.456710000000000000
        Width = 680.000000000000000000
        Height = 18.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[LOCAL_E_DATA_E]')
        ParentFont = False
      end
      object Shape7: TfrxShapeView
        Left = 30.236240000000000000
        Top = 394.456710000000000000
        Width = 700.000000000000000000
        Height = 56.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo27: TfrxMemoView
        Left = 358.236240000000000000
        Top = 430.456710000000000000
        Width = 368.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftTop]
        HAlign = haRight
        Memo.UTF8W = (
          '[RESPONSAVEL_E]')
        ParentFont = False
      end
      object Memo5: TfrxMemoView
        Left = 294.803340000000000000
        Top = 26.456710000000000000
        Width = 170.078850000000000000
        Height = 45.354360000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -35
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'RECIBO')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        Left = 124.724490000000000000
        Top = 300.126160000000000000
        Width = 188.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[RUA_EE]')
        ParentFont = False
      end
      object Memo18: TfrxMemoView
        Left = 596.724490000000000000
        Top = 272.126160000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[CNPJ_EE]')
        ParentFont = False
      end
      object Memo19: TfrxMemoView
        Left = 596.724490000000000000
        Top = 320.126160000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[TE1_EE]')
        ParentFont = False
      end
      object Memo20: TfrxMemoView
        Left = 400.724490000000000000
        Top = 300.126160000000000000
        Width = 184.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[COMPL_EE]')
        ParentFont = False
      end
      object Memo21: TfrxMemoView
        Left = 596.724490000000000000
        Top = 300.126160000000000000
        Width = 128.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[BAIRRO_EE]')
        ParentFont = False
      end
      object Memo22: TfrxMemoView
        Left = 124.724490000000000000
        Top = 320.126160000000000000
        Width = 188.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CIDADE_EE]')
        ParentFont = False
      end
      object Memo23: TfrxMemoView
        Left = 316.724490000000000000
        Top = 320.126160000000000000
        Width = 40.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[UF_EE]')
        ParentFont = False
      end
      object Memo24: TfrxMemoView
        Left = 360.724490000000000000
        Top = 320.126160000000000000
        Width = 104.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CEP_EE]')
        ParentFont = False
      end
      object Memo25: TfrxMemoView
        Left = 468.724490000000000000
        Top = 320.126160000000000000
        Width = 116.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[PAIS_EE]')
        ParentFont = False
      end
      object Memo26: TfrxMemoView
        Left = 532.724490000000000000
        Top = 272.126160000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'CNPJ / CPF: ')
        ParentFont = False
      end
    end
  end
  object frx2Recibos: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38009.439963981500000000
    ReportOptions.LastChange = 39098.923307291700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VARF_NOTCOLOR> = True then Shape8.Color := clWhite;'
      'end.')
    OnGetValue = frxRecibo1GetValue
    OnUserFunction = frx2RecibosUserFunction
    Left = 392
    Top = 44
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object Shape8: TfrxShapeView
        Left = 3.779530000000000000
        Width = 748.346940000000000000
        Height = 487.559370000000000000
        Fill.BackColor = 14342874
        Shape = skRoundRectangle
      end
      object Shape1: TfrxShapeView
        Left = 30.236240000000000000
        Top = 26.456710000000000000
        Width = 260.000000000000000000
        Height = 48.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo2: TfrxMemoView
        Left = 74.236240000000000000
        Top = 34.456710000000000000
        Width = 208.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[NUMERO_P]')
        ParentFont = False
      end
      object Memo28: TfrxMemoView
        Left = 34.236240000000000000
        Top = 26.456710000000000000
        Width = 24.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#186)
        ParentFont = False
      end
      object Shape2: TfrxShapeView
        Left = 470.236240000000000000
        Top = 26.456710000000000000
        Width = 260.000000000000000000
        Height = 48.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo29: TfrxMemoView
        Left = 514.236240000000000000
        Top = 38.456710000000000000
        Width = 208.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[VALOR_P]')
        ParentFont = False
      end
      object Memo30: TfrxMemoView
        Left = 474.236240000000000000
        Top = 30.456710000000000000
        Width = 36.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
      end
      object Shape3: TfrxShapeView
        Left = 30.236240000000000000
        Top = 78.456710000000000000
        Width = 700.000000000000000000
        Height = 112.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo4: TfrxMemoView
        Left = 38.236240000000000000
        Top = 80.456710000000000000
        Width = 496.000000000000000000
        Height = 30.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Recebemos de [NOME_PC]')
        ParentFont = False
      end
      object Memo3: TfrxMemoView
        Left = 126.236240000000000000
        Top = 110.456710000000000000
        Width = 188.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[RUA_PC]')
        ParentFont = False
      end
      object Memo1: TfrxMemoView
        Left = 598.236240000000000000
        Top = 82.456710000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[CNPJ_PC]')
        ParentFont = False
      end
      object Memo6: TfrxMemoView
        Left = 598.236240000000000000
        Top = 130.456710000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[TE1_PC]')
        ParentFont = False
      end
      object Memo7: TfrxMemoView
        Left = 330.236240000000000000
        Top = 110.456710000000000000
        Width = 56.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[NUMERO_PC]')
        ParentFont = False
      end
      object Memo8: TfrxMemoView
        Left = 402.236240000000000000
        Top = 110.456710000000000000
        Width = 184.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[COMPL_PC]')
        ParentFont = False
      end
      object Memo9: TfrxMemoView
        Left = 598.236240000000000000
        Top = 110.456710000000000000
        Width = 128.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[BAIRRO_PC]')
        ParentFont = False
      end
      object Memo10: TfrxMemoView
        Left = 126.236240000000000000
        Top = 130.456710000000000000
        Width = 188.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CIDADE_PC]')
        ParentFont = False
      end
      object Memo11: TfrxMemoView
        Left = 318.236240000000000000
        Top = 130.456710000000000000
        Width = 40.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[UF_PC]')
        ParentFont = False
      end
      object Memo12: TfrxMemoView
        Left = 362.236240000000000000
        Top = 130.456710000000000000
        Width = 104.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CEP_PC]')
        ParentFont = False
      end
      object Memo13: TfrxMemoView
        Left = 470.236240000000000000
        Top = 130.456710000000000000
        Width = 116.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[PAIS_PC]')
        ParentFont = False
      end
      object Memo14: TfrxMemoView
        Left = 122.236240000000000000
        Top = 150.456710000000000000
        Width = 600.000000000000000000
        Height = 38.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[EXTENSO_P]')
        ParentFont = False
      end
      object Memo31: TfrxMemoView
        Left = 534.236240000000000000
        Top = 82.456710000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'CNPJ / CPF: ')
        ParentFont = False
      end
      object Memo32: TfrxMemoView
        Left = 38.236240000000000000
        Top = 110.456710000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
      end
      object Memo33: TfrxMemoView
        Left = 38.236240000000000000
        Top = 150.456710000000000000
        Width = 80.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'A import'#226'ncia de:')
        ParentFont = False
      end
      object Shape4: TfrxShapeView
        Left = 30.236240000000000000
        Top = 194.456710000000000000
        Width = 700.000000000000000000
        Height = 68.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo16: TfrxMemoView
        Left = 42.236240000000000000
        Top = 198.456710000000000000
        Width = 684.000000000000000000
        Height = 62.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Referente a: [MOTIVO_P]')
        ParentFont = False
      end
      object Shape5: TfrxShapeView
        Left = 30.236240000000000000
        Top = 266.456710000000000000
        Width = 700.000000000000000000
        Height = 88.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo15: TfrxMemoView
        Left = 34.236240000000000000
        Top = 272.456710000000000000
        Width = 496.000000000000000000
        Height = 17.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Emitente: [EMITENTEP]')
        ParentFont = False
      end
      object Memo37: TfrxMemoView
        Left = 34.236240000000000000
        Top = 290.456710000000000000
        Width = 64.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
      end
      object Shape6: TfrxShapeView
        Left = 30.236240000000000000
        Top = 358.456710000000000000
        Width = 700.000000000000000000
        Height = 32.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo55: TfrxMemoView
        Left = 38.236240000000000000
        Top = 370.456710000000000000
        Width = 680.000000000000000000
        Height = 18.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[LOCAL_E_DATA_P]')
        ParentFont = False
      end
      object Shape7: TfrxShapeView
        Left = 30.236240000000000000
        Top = 394.456710000000000000
        Width = 700.000000000000000000
        Height = 56.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo27: TfrxMemoView
        Left = 358.236240000000000000
        Top = 430.456710000000000000
        Width = 368.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftTop]
        HAlign = haRight
        Memo.UTF8W = (
          '[RESPONSAVEL_P]')
        ParentFont = False
      end
      object Memo5: TfrxMemoView
        Left = 294.803340000000000000
        Top = 26.456710000000000000
        Width = 170.078850000000000000
        Height = 45.354360000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -35
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'RECIBO')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        Left = 124.724490000000000000
        Top = 300.126160000000000000
        Width = 188.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[RUA_PE]')
        ParentFont = False
      end
      object Memo18: TfrxMemoView
        Left = 596.724490000000000000
        Top = 272.126160000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[CNPJ_PE]')
        ParentFont = False
      end
      object Memo19: TfrxMemoView
        Left = 596.724490000000000000
        Top = 320.126160000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[TE1_PE]')
        ParentFont = False
      end
      object Memo20: TfrxMemoView
        Left = 400.724490000000000000
        Top = 300.126160000000000000
        Width = 184.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[COMPL_PE]')
        ParentFont = False
      end
      object Memo21: TfrxMemoView
        Left = 596.724490000000000000
        Top = 300.126160000000000000
        Width = 128.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[BAIRRO_PE]')
        ParentFont = False
      end
      object Memo22: TfrxMemoView
        Left = 124.724490000000000000
        Top = 320.126160000000000000
        Width = 188.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CIDADE_PE]')
        ParentFont = False
      end
      object Memo23: TfrxMemoView
        Left = 316.724490000000000000
        Top = 320.126160000000000000
        Width = 40.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[UF_PE]')
        ParentFont = False
      end
      object Memo24: TfrxMemoView
        Left = 360.724490000000000000
        Top = 320.126160000000000000
        Width = 104.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CEP_PE]')
        ParentFont = False
      end
      object Memo25: TfrxMemoView
        Left = 468.724490000000000000
        Top = 320.126160000000000000
        Width = 116.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[PAIS_PE]')
        ParentFont = False
      end
      object Memo26: TfrxMemoView
        Left = 532.724490000000000000
        Top = 272.126160000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'CNPJ / CPF: ')
        ParentFont = False
      end
      object Shape9: TfrxShapeView
        Left = 3.779530000000000000
        Top = 563.149970000000000000
        Width = 748.346940000000000000
        Height = 487.559370000000000000
        Fill.BackColor = 14342874
        Shape = skRoundRectangle
      end
      object Shape10: TfrxShapeView
        Left = 30.236240000000000000
        Top = 589.606680000000000000
        Width = 260.000000000000000000
        Height = 48.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo34: TfrxMemoView
        Left = 74.236240000000000000
        Top = 597.606680000000000000
        Width = 208.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[NUMERO_E]')
        ParentFont = False
      end
      object Memo35: TfrxMemoView
        Left = 34.236240000000000000
        Top = 589.606680000000000000
        Width = 24.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#186)
        ParentFont = False
      end
      object Shape11: TfrxShapeView
        Left = 470.236240000000000000
        Top = 589.606680000000000000
        Width = 260.000000000000000000
        Height = 48.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo36: TfrxMemoView
        Left = 514.236240000000000000
        Top = 601.606680000000000000
        Width = 208.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[VALOR_E]')
        ParentFont = False
      end
      object Memo38: TfrxMemoView
        Left = 474.236240000000000000
        Top = 593.606680000000000000
        Width = 36.000000000000000000
        Height = 24.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
      end
      object Shape12: TfrxShapeView
        Left = 30.236240000000000000
        Top = 641.606680000000000000
        Width = 700.000000000000000000
        Height = 112.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo39: TfrxMemoView
        Left = 38.236240000000000000
        Top = 643.606680000000000000
        Width = 496.000000000000000000
        Height = 30.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Recebemos de [NOME_EC]')
        ParentFont = False
      end
      object Memo40: TfrxMemoView
        Left = 126.236240000000000000
        Top = 673.606680000000000000
        Width = 188.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[RUA_EC]')
        ParentFont = False
      end
      object Memo41: TfrxMemoView
        Left = 598.236240000000000000
        Top = 645.606680000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[CNPJ_EC]')
        ParentFont = False
      end
      object Memo42: TfrxMemoView
        Left = 598.236240000000000000
        Top = 693.606680000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[TE1_EC]')
        ParentFont = False
      end
      object Memo43: TfrxMemoView
        Left = 330.236240000000000000
        Top = 673.606680000000000000
        Width = 56.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[NUMERO_EC]')
        ParentFont = False
      end
      object Memo44: TfrxMemoView
        Left = 402.236240000000000000
        Top = 673.606680000000000000
        Width = 184.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[COMPL_EC]')
        ParentFont = False
      end
      object Memo45: TfrxMemoView
        Left = 598.236240000000000000
        Top = 673.606680000000000000
        Width = 128.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[BAIRRO_EC]')
        ParentFont = False
      end
      object Memo46: TfrxMemoView
        Left = 126.236240000000000000
        Top = 693.606680000000000000
        Width = 188.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CIDADE_EC]')
        ParentFont = False
      end
      object Memo47: TfrxMemoView
        Left = 318.236240000000000000
        Top = 693.606680000000000000
        Width = 40.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[UF_EC]')
        ParentFont = False
      end
      object Memo48: TfrxMemoView
        Left = 362.236240000000000000
        Top = 693.606680000000000000
        Width = 104.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CEP_EC]')
        ParentFont = False
      end
      object Memo49: TfrxMemoView
        Left = 470.236240000000000000
        Top = 693.606680000000000000
        Width = 116.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[PAIS_EC]')
        ParentFont = False
      end
      object Memo50: TfrxMemoView
        Left = 122.236240000000000000
        Top = 713.606680000000000000
        Width = 600.000000000000000000
        Height = 38.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[EXTENSO_E]')
        ParentFont = False
      end
      object Memo51: TfrxMemoView
        Left = 534.236240000000000000
        Top = 645.606680000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'CNPJ / CPF: ')
        ParentFont = False
      end
      object Memo52: TfrxMemoView
        Left = 38.236240000000000000
        Top = 673.606680000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
      end
      object Memo53: TfrxMemoView
        Left = 38.236240000000000000
        Top = 713.606680000000000000
        Width = 80.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'A import'#226'ncia de:')
        ParentFont = False
      end
      object Shape13: TfrxShapeView
        Left = 30.236240000000000000
        Top = 757.606680000000000000
        Width = 700.000000000000000000
        Height = 68.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo54: TfrxMemoView
        Left = 42.236240000000000000
        Top = 761.606680000000000000
        Width = 684.000000000000000000
        Height = 62.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Referente a: [MOTIVO_E]')
        ParentFont = False
      end
      object Shape14: TfrxShapeView
        Left = 30.236240000000000000
        Top = 829.606680000000000000
        Width = 700.000000000000000000
        Height = 88.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo56: TfrxMemoView
        Left = 34.236240000000000000
        Top = 835.606680000000000000
        Width = 496.000000000000000000
        Height = 17.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Emitente: [EMITENTEP]')
        ParentFont = False
      end
      object Memo57: TfrxMemoView
        Left = 34.236240000000000000
        Top = 853.606680000000000000
        Width = 64.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
      end
      object Shape15: TfrxShapeView
        Left = 30.236240000000000000
        Top = 921.606680000000000000
        Width = 700.000000000000000000
        Height = 32.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo58: TfrxMemoView
        Left = 38.236240000000000000
        Top = 933.606680000000000000
        Width = 680.000000000000000000
        Height = 18.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[LOCAL_E_DATA_E]')
        ParentFont = False
      end
      object Shape16: TfrxShapeView
        Left = 30.236240000000000000
        Top = 957.606680000000000000
        Width = 700.000000000000000000
        Height = 56.000000000000000000
        Fill.BackColor = clWhite
        Shape = skRoundRectangle
      end
      object Memo59: TfrxMemoView
        Left = 358.236240000000000000
        Top = 993.606680000000000000
        Width = 368.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftTop]
        HAlign = haRight
        Memo.UTF8W = (
          '[RESPONSAVEL_E]')
        ParentFont = False
      end
      object Memo60: TfrxMemoView
        Left = 294.803340000000000000
        Top = 589.606680000000000000
        Width = 170.078850000000000000
        Height = 45.354360000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -35
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'RECIBO')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo61: TfrxMemoView
        Left = 124.724490000000000000
        Top = 863.276130000000000000
        Width = 188.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[RUA_EE]')
        ParentFont = False
      end
      object Memo62: TfrxMemoView
        Left = 596.724490000000000000
        Top = 835.276130000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        StretchMode = smMaxHeight
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[CNPJ_EE]')
        ParentFont = False
      end
      object Memo63: TfrxMemoView
        Left = 596.724490000000000000
        Top = 883.276130000000000000
        Width = 128.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[TE1_EE]')
        ParentFont = False
      end
      object Memo64: TfrxMemoView
        Left = 400.724490000000000000
        Top = 863.276130000000000000
        Width = 184.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[COMPL_EE]')
        ParentFont = False
      end
      object Memo65: TfrxMemoView
        Left = 596.724490000000000000
        Top = 863.276130000000000000
        Width = 128.000000000000000000
        Height = 17.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[BAIRRO_EE]')
        ParentFont = False
      end
      object Memo66: TfrxMemoView
        Left = 124.724490000000000000
        Top = 883.276130000000000000
        Width = 188.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CIDADE_EE]')
        ParentFont = False
      end
      object Memo67: TfrxMemoView
        Left = 316.724490000000000000
        Top = 883.276130000000000000
        Width = 40.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[UF_EE]')
        ParentFont = False
      end
      object Memo68: TfrxMemoView
        Left = 360.724490000000000000
        Top = 883.276130000000000000
        Width = 104.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[CEP_EE]')
        ParentFont = False
      end
      object Memo69: TfrxMemoView
        Left = 468.724490000000000000
        Top = 883.276130000000000000
        Width = 116.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[PAIS_EE]')
        ParentFont = False
      end
      object Memo70: TfrxMemoView
        Left = 532.724490000000000000
        Top = 835.276130000000000000
        Width = 64.000000000000000000
        Height = 18.000000000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'CNPJ / CPF: ')
        ParentFont = False
      end
      object Line1: TfrxLineView
        Top = 540.472790000000000000
        Width = 755.906000000000000000
        Color = clBlack
        Frame.Style = fsDash
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
    end
  end
end
