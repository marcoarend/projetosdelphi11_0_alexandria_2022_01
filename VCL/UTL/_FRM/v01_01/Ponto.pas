unit Ponto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnInternalConsts, ExtCtrls, StdCtrls, Buttons, DBCtrls, Db, UnDmkProcFunc,
  mySQLDbTables, ComCtrls, Grids, DBGrids, dmkGeral, dmkImage, UnDmkEnums,
  DmkDAC_PF;

type
  TFmPonto = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    RadioButton7: TRadioButton;
    RadioButton8: TRadioButton;
    RadioButton9: TRadioButton;
    RadioButton10: TRadioButton;
    QrPonto: TmySQLQuery;
    DsPonto: TDataSource;
    QrPontoFunci: TIntegerField;
    QrPontoData: TDateField;
    QrPontoTraIni: TWideStringField;
    QrPontoDesIni: TWideStringField;
    QrPontoDesFim: TWideStringField;
    QrPontoTraFim: TWideStringField;
    QrPontoEx1Ini: TWideStringField;
    QrPontoEx1Fim: TWideStringField;
    QrPontoEx2Ini: TWideStringField;
    QrPontoEx2Fim: TWideStringField;
    QrPontoEx3Ini: TWideStringField;
    QrPontoEx3Fim: TWideStringField;
    QrPontoLk: TIntegerField;
    QrPontoDataCad: TDateField;
    QrPontoDataAlt: TDateField;
    QrPontoUserCad: TIntegerField;
    QrPontoUserAlt: TIntegerField;
    QrPontoNormal: TIntegerField;
    QrPontoExtra: TIntegerField;
    DBGrid1: TDBGrid;
    QrPontoTraIniTXT: TWideStringField;
    QrPontoDesIniTXT: TWideStringField;
    QrPontoDesFimTXT: TWideStringField;
    QrPontoTraFimTXT: TWideStringField;
    QrPontoEx1IniTXT: TWideStringField;
    QrPontoEx1FimTXT: TWideStringField;
    QrPontoEx2IniTXT: TWideStringField;
    QrPontoEx2FimTXT: TWideStringField;
    QrPontoEx3IniTXT: TWideStringField;
    QrPontoEx3FimTXT: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure QrPontoCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FFuncionario: Integer;
    FData, FHora: String;
  end;

var
  FmPonto: TFmPonto;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmPonto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  QrPonto.Close;
  QrPonto.Params[0].AsInteger := FFuncionario;
  QrPonto.Params[1].AsString  := FData;
  UnDmkDAC_PF.AbreQuery(QrPonto, Dmod.MyDB);
  //
end;

procedure TFmPonto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPonto.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPonto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPonto.BtConfirmaClick(Sender: TObject);
var
  Item: Integer;
  Campo: String;
begin
  Item := 0;
  if RadioButton1.Checked then Item := 1;
  if RadioButton2.Checked then Item := 2;
  if RadioButton3.Checked then Item := 3;
  if RadioButton4.Checked then Item := 4;
  if RadioButton5.Checked then Item := 5;
  if RadioButton6.Checked then Item := 6;
  if RadioButton7.Checked then Item := 7;
  if RadioButton8.Checked then Item := 8;
  if RadioButton9.Checked then Item := 9;
  if RadioButton10.Checked then Item := 10;
  if Item = 0 then
  begin
    Geral.MB_Aviso('Defina uma a��o!');
    Exit;
  end;
  case Item of
    1: Campo := 'TraIni';
    2: Campo := 'DesIni';
    3: Campo := 'DesFim';
    4: Campo := 'TraFim';
    5: Campo := 'Ex1Ini';
    6: Campo := 'Ex1Fim';
    7: Campo := 'Ex2Ini';
    8: Campo := 'Ex2Fim';
    9: Campo := 'Ex3Ini';
   10: Campo := 'Ex3Fim';
  end;
  Dmod.QrUpd.Sql.Clear;
  if QrPonto.RecordCount = 0 then
  begin
    Dmod.QrUpd.Sql.Add('INSERT INTO ponto SET');
    Dmod.QrUpd.Sql.Add(Campo+'=:P0, Funci=:P1, Data=:P2');
  end else begin
    Dmod.QrUpd.Sql.Add('UPDATE ponto SET');
    Dmod.QrUpd.Sql.Add(Campo+'=:P0 WHERE Funci=:P1 AND Data=:P2');
  end;
  Dmod.QrUpd.Params[0].AsString  := FHora;
  Dmod.QrUpd.Params[1].AsInteger := FFuncionario;
  Dmod.QrUpd.Params[2].AsString  := FData;
  Dmod.QrUpd.ExecSQL;
  Close;
end;

procedure TFmPonto.QrPontoCalcFields(DataSet: TDataSet);
begin
  if QrPontoTraIni.Value = '' then QrPontoTraIniTXT.Value := '' else
  QrPontoTraIniTXT.Value := dmkPF.THT(QrPontoTraIni.Value);
  if QrPontoDesIni.Value = '' then QrPontoDesIniTXT.Value := '' else
  QrPontoDesIniTXT.Value := dmkPF.THT(QrPontoDesIni.Value);
  if QrPontoDesFim.Value = '' then QrPontoDesFimTXT.Value := '' else
  QrPontoDesFimTXT.Value := dmkPF.THT(QrPontoDesFim.Value);
  if QrPontoTraFim.Value = '' then QrPontoTraFimTXT.Value := '' else
  QrPontoTraFimTXT.Value := dmkPF.THT(QrPontoTraFim.Value);
  if QrPontoEx1Ini.Value = '' then QrPontoEx1IniTXT.Value := '' else
  QrPontoEx1IniTXT.Value := dmkPF.THT(QrPontoEx1Ini.Value);
  if QrPontoEx1Fim.Value = '' then QrPontoEx1FimTXT.Value := '' else
  QrPontoEx1FimTXT.Value := dmkPF.THT(QrPontoEx1Fim.Value);
  if QrPontoEx2Ini.Value = '' then QrPontoEx2IniTXT.Value := '' else
  QrPontoEx2IniTXT.Value := dmkPF.THT(QrPontoEx2Ini.Value);
  if QrPontoEx2Fim.Value = '' then QrPontoEx2FimTXT.Value := '' else
  QrPontoEx2FimTXT.Value := dmkPF.THT(QrPontoEx2Fim.Value);
  if QrPontoEx3Ini.Value = '' then QrPontoEx3IniTXT.Value := '' else
  QrPontoEx3IniTXT.Value := dmkPF.THT(QrPontoEx3Ini.Value);
  if QrPontoEx3Fim.Value = '' then QrPontoEx3FimTXT.Value := '' else
  QrPontoEx3FimTXT.Value := dmkPF.THT(QrPontoEx3Fim.Value);
end;

end.
