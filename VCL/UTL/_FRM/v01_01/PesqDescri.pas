unit PesqDescri;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs, Grids,
  DBGrids, ZCF2, UnInternalConsts, Variants, dmkGeral, mySQLDbTables, dmkImage,
  UnDmkEnums;


type
  TFmPesqDescri = class(TForm)
    PainelDados: TPanel;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    RGMascara: TRadioGroup;
    Label1: TLabel;
    EdTxt: TEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    procedure BtSaidaClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTxtChange(Sender: TObject);
    procedure EdTxtExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure VerificaOrigem;
  public
    { Public declarations }
    FValor: Variant;
    FDataType: TAllformat;
  end;

var
  FmPesqDescri: TFmPesqDescri;

implementation

uses UnMyObjects, Module, ModuleGeral, dmkDAC_PF;

{$R *.DFM}

procedure TFmPesqDescri.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqDescri.VerificaOrigem;
begin
  if (DBGrid1.DataSource = DmodG.DsSB) then
  begin
    if (DmodG.QrSB.State <> dsInactive) then
    begin
      if DmodG.QrSB.RecordCount > 0 then
        FValor := DmodG.QrSBCodigo.Value
      else FValor := Null;
    end else FValor := Null;
  end else
  if (DBGrid1.DataSource = DmodG.DsSB2) then
  begin
    if (DmodG.QrSB2.State <> dsInactive) then
    begin
      if DmodG.QrSB2.RecordCount > 0 then
        FValor := DmodG.QrSB2Codigo.Value
      else FValor := Null;
    end else FValor := Null;
  end else
  if (DBGrid1.DataSource = DmodG.DsSB3) then
  begin
    if DmodG.QrSB3.State <> dsInactive then
    begin
      if DmodG.QrSB3.RecordCount > 0 then
        FValor := DmodG.QrSB3Codigo.Value
      else FValor := Null;
    end else FValor := Null;
  end else
  if (DBGrid1.DataSource = DmodG.DsSB4) then
  begin
    if DmodG.QrSB4.State <> dsInactive then
    begin
      if DmodG.QrSB4.RecordCount > 0 then
        VAR_CODIGO4 := DmodG.QrSB4Codigo.Value
      else FValor := Null;
    end else FValor := Null;
  end else FValor := Null;
end;

procedure TFmPesqDescri.DBGrid1CellClick(Column: TColumn);
begin
  VerificaOrigem;
end;

procedure TFmPesqDescri.DBGrid1DblClick(Sender: TObject);
begin
  VerificaOrigem;
  Close;
end;

procedure TFmPesqDescri.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPesqDescri.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
end;

procedure TFmPesqDescri.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesqDescri.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
  begin
    VerificaOrigem;
    Close;
  end;  
end;

procedure TFmPesqDescri.EdTxtChange(Sender: TObject);
var
  Texto: String;
  Query: TmySQLQuery;
begin
  Texto := '';
  if RGMascara.ItemIndex in ([0,1]) then Texto := '%';
  Texto := Texto + EdTxt.Text;
  if RGMascara.ItemIndex in ([0,2]) then Texto := Texto + '%';
  //
  case FDataType of
    dmktfInteger: Query := DmodG.QrSB;
    else begin
      Geral.MB_Erro('"DataType" n�o informado na function ' +
      '"FmPesqDescri.EdTxtChange"! AVISE A DERMATEK');
      Exit;
    end;
  end;
  Query.Close;
  Query.Params[0].AsString := Texto;
  // ini 2022-04-15
  if Query.Database <> nil then
    UnDmkDAC_PF.AbreQuery(Query, Query.Database)
  else
  // end 2022-04-15
    UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
end;

procedure TFmPesqDescri.EdTxtExit(Sender: TObject);
begin
  DBGrid1.SetFocus;
end;

end.
