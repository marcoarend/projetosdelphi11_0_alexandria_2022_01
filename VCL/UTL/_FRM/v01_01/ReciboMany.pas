unit ReciboMany;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, DBCtrls, dmkEdit, Menus, ComCtrls, jpeg,
  dmkMemo, UnInternalConsts, frxClass, frxBarcode, dmkImage, dmkGeral,
  UnDmkProcFunc, UnDmkEnums, Data.DB, mySQLDbTables, frxDBSet;

type
  TFmReciboMany = class(TForm)
    Panel2: TPanel;
    frxRecibo1: TfrxReport;
    frxBarCodeObject1: TfrxBarCodeObject;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrRecibos: TmySQLQuery;
    QrRecibosSeqID: TIntegerField;
    QrRecibosEmitente: TIntegerField;
    QrRecibosBeneficiario: TIntegerField;
    QrRecibosValor: TFloatField;
    QrRecibosValorP: TFloatField;
    QrRecibosValorE: TFloatField;
    QrRecibosNumRecibo: TWideStringField;
    QrRecibosReferente: TWideMemoField;
    QrRecibosReferenteP: TWideMemoField;
    QrRecibosReferenteE: TWideMemoField;
    QrRecibosData: TDateField;
    QrRecibosSit: TIntegerField;
    QrRecibosAtivo: TSmallintField;
    frxDsRecibos: TfrxDBDataset;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure fr2RecibosGetValue(const ParName: String;
      var ParValue: Variant);
    procedure FormActivate(Sender: TObject);
    procedure frRecibo1UserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure frxRecibo1GetValue(const VarName: String;
      var Value: Variant);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrRecibosAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    FEdNumero_Text,
    FEdValor_Text,
    FEdEmitente_Text,
    FEdECNPJ_Text,
    FEdERua_Text,
    FEdENumero_Text,
    FEdECompl_Text,
    FEdEBairro_Text,
    FEdECidade_Text,
    FEdEUF_Text,
    FEdECEP_Text,
    FEdEPais_Text,
    FEdETe1_Text,
    Texto_Text,
    //
    FEdBeneficiario_Text,
    FEdBCNPJ_Text,
    FEdBRua_Text,
    FEdBNumero_Text,
    FEdBCompl_Text,
    FEdBBairro_Text,
    FEdBCidade_Text,
    FEdBUF_Text,
    FEdBCEP_Text,
    FEdBPais_Text,
    FEdBTe1_Text,
    //
    FEdLocalEData_Text,
    FEdResponsavel_Text,
    FExtenso_Text: String;
    //
    function ItemDeEndereco(Item, Valor: String): String;
  public
    { Public declarations }
    FRecibos: String;
    //
    procedure ImprimeRecibos();
  end;

var
  FmReciboMany: TFmReciboMany;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmReciboMany.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReciboMany.fr2RecibosGetValue(const ParName: String;
  var ParValue: Variant);
begin
(*
  if ParName = 'NUMERO_P'      then ParValue := FEdNumero_Text;
  if ParName = 'VALOR_P'       then ParValue := String(Dmod.QrControle.FieldByName('Moeda').AsString) + ' '+ FEdValor_Text;
  if ParName = 'NOME_PC'       then ParValue := EdEmitente.Text;
  if ParName = 'RUA_PC'        then ParValue := EdERua.Text;
  if ParName = 'NUMERO_PC'     then ParValue := EdENumero.Text;
  if ParName = 'CNPJ_PC'       then ParValue := EdECNPJ.Text;
  if ParName = 'COMPL_PC'      then ParValue := EdECompl.Text;
  if ParName = 'BAIRRO_PC'     then ParValue := EdEBairro.Text;
  if ParName = 'CIDADE_PC'     then ParValue := EdECidade.Text;
  if ParName = 'UF_PC'         then ParValue := EdEUF.Text;
  if ParName = 'CEP_PC'        then ParValue := EdECEP.Text;
  if ParName = 'PAIS_PC'       then ParValue := EdEPais.Text;
  if ParName = 'TE1_PC'        then ParValue := EdETe1.Text;
  if ParName = 'EXTENSO_P'     then ParValue := Extenso.Text;
  if ParName = 'MOTIVO_P'      then ParValue := Texto.Text;
  if ParName = 'EMITENTEP'     then ParValue := EdBeneficiario.Text;
  if ParName = 'CNPJ_PE'       then ParValue := EdBCNPJ.Text;
  if ParName = 'RUA_PE'        then ParValue := EdBRua.Text;
  if ParName = 'NUMERO_PE'     then ParValue := EdBNumero.Text;
  if ParName = 'COMPL_PE'      then ParValue := EdBCompl.Text;
  if ParName = 'BAIRRO_PE'     then ParValue := EdBBairro.Text;
  if ParName = 'CIDADE_PE'     then ParValue := EdBCidade.Text;
  if ParName = 'UF_PE'         then ParValue := EdBUF.Text;
  if ParName = 'CEP_PE'        then ParValue := EdBCEP.Text;
  if ParName = 'PAIS_PE'       then ParValue := EdBPais.Text;
  if ParName = 'TE1_PE'        then ParValue := EdBTe1.Text;
  //
  if ParName = 'RESPONSAVEL_E' then ParValue := EdResponsavel.Text;
  if ParName = 'RESPONSAVEL_P' then ParValue := EdResponsavel.Text;
  if ParName = 'LOCAL_E_DATA'  then ParValue := EdLocalData.Text;
*)
end;

procedure TFmReciboMany.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmReciboMany.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FEdNumero_Text       := '';
  FEdValor_Text        := '';
  FExtenso_Text        := '';

  FEdEmitente_Text     := '';
  FEdECNPJ_Text        := '';
  FEdERua_Text         := '';
  FEdENumero_Text      := '';
  FEdECompl_Text       := '';
  FEdEBairro_Text      := '';
  FEdECidade_Text      := '';
  FEdEUF_Text          := '';
  FEdECEP_Text         := '';
  FEdEPais_Text        := '';
  FEdETe1_Text         := '';
  Texto_Text           := '';
  //
  FEdBeneficiario_Text := '';
  FEdBCNPJ_Text        := '';
  FEdBRua_Text         := '';
  FEdBNumero_Text      := '';
  FEdBCompl_Text       := '';
  FEdBBairro_Text      := '';
  FEdBCidade_Text      := '';
  FEdBUF_Text          := '';
  FEdBCEP_Text         := '';
  FEdBPais_Text        := '';
  FEdBTe1_Text         := '';
  //
  FEdLocalEData_Text   := '';
  FEdLocalEData_Text   := '';
  FEdResponsavel_Text  := '';
end;

procedure TFmReciboMany.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmReciboMany.frRecibo1UserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if Name = 'VARF_NOTCOLOR' then
    Val := not Geral.IntToBool_0(Dmod.QrControle.FieldByName('CorRecibo').AsInteger);
end;

procedure TFmReciboMany.BtImprimeClick(Sender: TObject);
begin
  ImprimeRecibos();
end;

procedure TFmReciboMany.frxRecibo1GetValue(const VarName: String;
  var Value: Variant);
  function Valor(): String;
  begin
    Result := String(Dmod.QrControle.FieldByName('Moeda').AsString);
    if Geral.DMV(FEdValor_Text) <> 0 then
      Result := Result + ' ' + FEdValor_Text;
  end;
begin
  if VarName = 'NUMERO_P'      then Value := FEdNumero_Text;
  if VarName = 'VALOR_P'       then Value := Valor();
  if VarName = 'NOME_PC'       then Value := FEdEmitente_Text;
  if VarName = 'RUA_PC'        then Value := FEdERua_Text;
  if VarName = 'NUMERO_PC'     then Value := FEdENumero_Text;
  if VarName = 'CNPJ_PC'       then Value := FEdECNPJ_Text;
  if VarName = 'COMPL_PC'      then Value := FEdECompl_Text;
  if VarName = 'BAIRRO_PC'     then Value := FEdEBairro_Text;
  if VarName = 'CIDADE_PC'     then Value := FEdECidade_Text;
  if VarName = 'UF_PC'         then Value := FEdEUF_Text;
  if VarName = 'CEP_PC'        then Value := FEdECEP_Text;
  if VarName = 'PAIS_PC'       then Value := FEdEPais_Text;
  if VarName = 'TE1_PC'        then Value := FEdETe1_Text;
  if VarName = 'EXTENSO_P'     then Value := FExtenso_Text;
  if VarName = 'MOTIVO_P'      then Value := Texto_Text;
  if VarName = 'EMITENTEP'     then Value := FEdBeneficiario_Text;
  //
  if VarName = 'CNPJ_PE'       then Value := FEdBCNPJ_Text;
  if VarName = 'RUA_PE'        then Value := FEdBRua_Text;
  if VarName = 'NUMERO_PE'     then Value := FEdBNumero_Text;
  if VarName = 'COMPL_PE'      then Value := FEdBCompl_Text;
  if VarName = 'BAIRRO_PE'     then Value := FEdBBairro_Text;
  if VarName = 'CIDADE_PE'     then Value := FEdBCidade_Text;
  if VarName = 'UF_PE'         then Value := FEdBUF_Text;
  if VarName = 'CEP_PE'        then Value := FEdBCEP_Text;
  if VarName = 'PAIS_PE'       then Value := FEdBPais_Text;
  if VarName = 'TE1_PE'        then Value := FEdBTe1_Text;
  //
  if VarName = 'RESPONSAVEL_E' then Value := FEdResponsavel_Text;
  if VarName = 'RESPONSAVEL_P' then Value := FEdResponsavel_Text;
  if VarName = 'LOCAL_E_DATA'  then Value := FEdLocalEData_Text;
  if VarName = 'VARF_BARCODE_BARC' then Value := FEdNumero_Text;
  //
end;

procedure TFmReciboMany.ImprimeRecibos();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRecibos, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FRecibos,
  '']);
  //
  frxRecibo1.Variables['VARF_NOTCOLOR'] :=
    not Geral.IntToBool_0(Dmod.QrControle.FieldByName('CorRecibo').AsInteger);
  MyObjects.frxMostra(frxRecibo1, 'Multiplos Recibos');
end;

function TFmReciboMany.ItemDeEndereco(Item, Valor: String): String;
begin
  if Item <> '' then Result := Item + ' ' + Valor
  else Result := Valor;
end;

procedure TFmReciboMany.QrRecibosAfterScroll(DataSet: TDataSet);
begin
  //EnderecoDeEntidade(Emitente, -1);
  DModG.ReopenEndereco(QrRecibosEmitente.Value);
  FEdNumero_Text    := QrRecibosNumRecibo.Value;
  FEdValor_Text     := Geral.FFT(QrRecibosValor.Value, 2, siPositivo);
  FExtenso_Text     := dmkPF.ExtensoMoney(Geral.TFT(FEdValor_Text, 2, siNegativo))+'.';

  FEdEmitente_Text  := DmodG.QrEnderecoNOME_ENT.Value;
  FEdECNPJ_Text     := ItemDeEndereco(DmodG.QrEnderecoNO_TIPO_DOC.Value + ': ', DmodG.QrEnderecoCNPJ_TXT.Value);
  FEdERua_Text      := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
  FEdENumero_Text   := ItemDeEndereco('N�', DmodG.QrEnderecoNUMERO_TXT.Value);
  FEdECompl_Text    := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
  FEdEBairro_Text   := ItemDeEndereco('Bairro: ', DmodG.QrEnderecoBAIRRO.Value);
  FEdECidade_Text   := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
  FEdEUF_Text       := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
  FEdECEP_Text      := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
  FEdEPais_Text     := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
  FEdETe1_Text      := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
  Texto_Text        := QrRecibosReferente.Value + '.';
  //
  DModG.ReopenEndereco(QrRecibosBeneficiario.Value);
  FEdBeneficiario_Text := DmodG.QrEnderecoNOME_ENT.Value;
  FEdBCNPJ_Text    := ItemDeEndereco(DmodG.QrEnderecoNO_TIPO_DOC.Value + ': ', DmodG.QrEnderecoCNPJ_TXT.Value);
  FEdBRua_Text     := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
  FEdBNumero_Text  := ItemDeEndereco('N�:', DmodG.QrEnderecoNUMERO_TXT.Value);
  FEdBCompl_Text   := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
  FEdBBairro_Text  := ItemDeEndereco('Bairro:', DmodG.QrEnderecoBAIRRO.Value);
  FEdBCidade_Text  := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
  FEdBUF_Text      := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
  FEdBCEP_Text     := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
  FEdBPais_Text    := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
  FEdBTe1_Text     := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
  //
  FEdLocalEData_Text := Geral.Maiusculas(
    FormatDateTime('dddd, dd" de "mmmm" de "yyyy', QrRecibosData.Value),
    Geral.EhMinusculas(DmodG.QrEnderecoCIDADE.Value, False));
  if DmodG.QrEnderecoCIDADE.Value <> '' then FEdLocalEData_Text :=
    DmodG.QrEnderecoCIDADE.Value + ', ' + FEdLocalEData_Text;
  FEdLocalEData_Text := FEdLocalEData_Text + '.';
  FEdResponsavel_Text := DmodG.QrEnderecoRespons1.Value;
end;

end.
