unit CalcPercent;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, dmkEdit, dmkGeral, dmkImage, UnDMkEnums;

type
  TFmCalcPercent = class(TForm)
    Panel1: TPanel;
    EdValor: TdmkEdit;
    Label2: TLabel;
    EdPercent: TdmkEdit;
    LaPorcent: TLabel;
    Label3: TLabel;
    EdResult: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTitulo: TLabel;
    procedure FormResize(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure EdPercentExit(Sender: TObject);
    procedure EdValorChange(Sender: TObject);
    procedure EdPercentChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaPorcent;
  public
    { Public declarations }
  end;

var
  FmCalcPercent: TFmCalcPercent;

implementation

uses UnMyObjects, UnInternalConsts;

{$R *.DFM}

procedure TFmCalcPercent.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCalcPercent.BtSaidaClick(Sender: TObject);
begin
  VAR_VALOR := Geral.DMV(EdResult.Text);
  Close;
end;

procedure TFmCalcPercent.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCalcPercent.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCalcPercent.EdValorExit(Sender: TObject);
begin
  CalculaPorcent;
end;

procedure TFmCalcPercent.EdPercentExit(Sender: TObject);
begin
  CalculaPorcent;
end;

procedure TFmCalcPercent.EdValorChange(Sender: TObject);
begin
  CalculaPorcent;
end;

procedure TFmCalcPercent.CalculaPorcent;
var
  Valor, Porcent, Resultado, Divisor: Double;
begin
  Divisor := 1;
  if LaPorcent.Caption = '% Multa'     then Divisor :=  1;
  if LaPorcent.Caption = '% Juros/m�s' then Divisor := 30;
  Valor := Geral.DMV(
    Geral.TFT(EdValor.Text, 2, siPositivo));
  Porcent := Geral.DMV(
    Geral.TFT(EdPercent.Text, 4, siPositivo));
  Resultado := Trunc((Porcent / Divisor) * Valor) / 100;
  //
  EdResult.ValueVariant := Resultado;
end;

procedure TFmCalcPercent.EdPercentChange(Sender: TObject);
begin
  CalculaPorcent;
end;

end.
