object FmVeiculos: TFmVeiculos
  Left = 368
  Top = 194
  Caption = 'VEI-CULOS-001 :: Cadastro de Ve'#237'culos'
  ClientHeight = 354
  ClientWidth = 754
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 754
    Height = 258
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 296
    ExplicitTop = 168
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 754
      Height = 169
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      ExplicitLeft = 264
      ExplicitTop = 16
      object Label1: TLabel
        Left = 20
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label5: TLabel
        Left = 80
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 20
        Top = 60
        Width = 30
        Height = 13
        Caption = 'Marca'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 20
        Top = 100
        Width = 30
        Height = 13
        Caption = 'Placa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label10: TLabel
        Left = 120
        Top = 100
        Width = 212
        Height = 13
        Caption = 'Motorista: (Tipo de entidade: Fornece outros)'
      end
      object Label11: TLabel
        Left = 244
        Top = 60
        Width = 38
        Height = 13
        Caption = 'Modelo:'
        Color = clBtnFace
        ParentColor = False
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 20
        Top = 36
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVeiculos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 80
        Top = 36
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVeiculos
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 20
        Top = 76
        Width = 56
        Height = 21
        DataField = 'Marca'
        DataSource = DsVeiculos
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 244
        Top = 76
        Width = 56
        Height = 21
        DataField = 'Modelo'
        DataSource = DsVeiculos
        TabOrder = 5
      end
      object DBEdit4: TDBEdit
        Left = 120
        Top = 116
        Width = 56
        Height = 21
        DataField = 'MotorisPadr'
        DataSource = DsVeiculos
        TabOrder = 7
      end
      object DBEdit5: TDBEdit
        Left = 136
        Top = 36
        Width = 409
        Height = 21
        DataField = 'NO_EMP'
        DataSource = DsVeiculos
        TabOrder = 2
      end
      object DBEdit6: TDBEdit
        Left = 80
        Top = 76
        Width = 161
        Height = 21
        DataField = 'NO_Marca'
        DataSource = DsVeiculos
        TabOrder = 4
      end
      object DBEdit7: TDBEdit
        Left = 300
        Top = 76
        Width = 245
        Height = 21
        DataField = 'NO_Modelo'
        DataSource = DsVeiculos
        TabOrder = 6
      end
      object DBEdit8: TDBEdit
        Left = 176
        Top = 116
        Width = 369
        Height = 21
        DataField = 'NO_MOT'
        DataSource = DsVeiculos
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 20
        Top = 116
        Width = 95
        Height = 21
        DataField = 'placa'
        DataSource = DsVeiculos
        TabOrder = 9
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 194
      Width = 754
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 232
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 754
    Height = 258
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 754
      Height = 149
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 240
        Top = 56
        Width = 38
        Height = 13
        Caption = 'Modelo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 30
        Height = 13
        Caption = 'Marca'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 16
        Top = 96
        Width = 30
        Height = 13
        Caption = 'Placa:'
        Color = clBtnFace
        ParentColor = False
      end
      object SbVeicMarcas: TSpeedButton
        Left = 216
        Top = 72
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SbVeicMarcasClick
      end
      object Label25: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label22: TLabel
        Left = 116
        Top = 96
        Width = 212
        Height = 13
        Caption = 'Motorista: (Tipo de entidade: Fornece outros)'
      end
      object SbMotorista: TSpeedButton
        Left = 520
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbMotoristaClick
      end
      object SbVeicModels: TSpeedButton
        Left = 520
        Top = 72
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SbVeicModelsClick
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMarca: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Marca'
        UpdCampo = 'Marca'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMarca
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMarca: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 145
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVeicMarcas
        TabOrder = 4
        dmkEditCB = EdMarca
        QryCampo = 'Marca'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPlaca: TdmkEdit
        Left = 16
        Top = 112
        Width = 95
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'placa'
        UpdCampo = 'placa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdCampo = 'Filial'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 409
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdMotorisPadr: TdmkEditCB
        Left = 116
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MotorisPadr'
        UpdCampo = 'MotorisPadr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMotorisPadr
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMotorisPadr: TdmkDBLookupComboBox
        Left = 172
        Top = 112
        Width = 349
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsMotorista
        TabOrder = 9
        dmkEditCB = EdMotorisPadr
        QryCampo = 'MotorisPadr'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdModelo: TdmkEditCB
        Left = 240
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Modelo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBModelo
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBModelo: TdmkDBLookupComboBox
        Left = 296
        Top = 72
        Width = 225
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVeicModels
        TabOrder = 6
        dmkEditCB = EdModelo
        QryCampo = 'Modelo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 195
      Width = 754
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 615
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 754
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 706
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 490
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 258
        Height = 32
        Caption = 'Cadastro de Ve'#237'culos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 258
        Height = 32
        Caption = 'Cadastro de Ve'#237'culos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 258
        Height = 32
        Caption = 'Cadastro de Ve'#237'culos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 754
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 750
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrVeiculos: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVeiculosBeforeOpen
    AfterOpen = QrVeiculosAfterOpen
    SQL.Strings = (
      'SELECT vei.*, '
      'vma.Nome NO_Marca, vmo.Nome NO_Modelo,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,'
      'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOT'
      'FROM veiculos vei'
      'LEFT JOIN entidades emp ON emp.Codigo=vei.Empresa'
      'LEFT JOIN entidades mot ON mot.Codigo=vei.MotorisPadr'
      'LEFT JOIN veicmarcas vma ON vma.Codigo=vei.Marca'
      'LEFT JOIN veicmodels vmo ON vmo.Codigo=vei.Modelo')
    Left = 388
    Top = 16
    object QrVeiculosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVeiculosEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVeiculosRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
    object QrVeiculosMarca: TIntegerField
      FieldName = 'Marca'
      Required = True
    end
    object QrVeiculosRENAVAM: TWideStringField
      FieldName = 'RENAVAM'
      Size = 11
    end
    object QrVeiculosplaca: TWideStringField
      FieldName = 'placa'
      Required = True
      Size = 7
    end
    object QrVeiculostara: TIntegerField
      FieldName = 'tara'
      Required = True
    end
    object QrVeiculoscapKG: TIntegerField
      FieldName = 'capKG'
      Required = True
    end
    object QrVeiculoscapM3: TIntegerField
      FieldName = 'capM3'
      Required = True
    end
    object QrVeiculostpProp: TWideStringField
      FieldName = 'tpProp'
      Required = True
      Size = 1
    end
    object QrVeiculostpVeic: TSmallintField
      FieldName = 'tpVeic'
      Required = True
    end
    object QrVeiculostpRod: TSmallintField
      FieldName = 'tpRod'
      Required = True
    end
    object QrVeiculostpCar: TSmallintField
      FieldName = 'tpCar'
      Required = True
    end
    object QrVeiculosUF: TWideStringField
      FieldName = 'UF'
      Required = True
      Size = 2
    end
    object QrVeiculosMotorisPadr: TIntegerField
      FieldName = 'MotorisPadr'
      Required = True
    end
    object QrVeiculosModelo: TIntegerField
      FieldName = 'Modelo'
    end
    object QrVeiculosAnoModel: TIntegerField
      FieldName = 'AnoModel'
    end
    object QrVeiculosAnoFabri: TIntegerField
      FieldName = 'AnoFabri'
    end
    object QrVeiculosAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrVeiculosNO_Marca: TWideStringField
      FieldName = 'NO_Marca'
      Size = 60
    end
    object QrVeiculosNO_Modelo: TWideStringField
      FieldName = 'NO_Modelo'
      Size = 60
    end
    object QrVeiculosNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrVeiculosNO_MOT: TWideStringField
      FieldName = 'NO_MOT'
      Size = 100
    end
  end
  object DsVeiculos: TDataSource
    DataSet = QrVeiculos
    Left = 388
    Top = 68
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrVeicMarcas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM veicmarcas'
      'ORDER BY Nome')
    Left = 484
    Top = 12
    object QrVeicMarcasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVeicMarcasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVeicMarcas: TDataSource
    DataSet = QrVeicMarcas
    Left = 484
    Top = 60
  end
  object QrMotorista: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 268
    Top = 40
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsMotorista: TDataSource
    DataSet = QrMotorista
    Left = 268
    Top = 88
  end
  object QrVeicModels: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM veicmodels'
      'ORDER BY Nome')
    Left = 196
    Top = 48
    object QrVeicModelsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVeicModelsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVeicModels: TDataSource
    DataSet = QrVeicModels
    Left = 196
    Top = 96
  end
end
