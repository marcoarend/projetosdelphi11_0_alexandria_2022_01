unit TextoDeLista;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmTextoDeLista = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Qry: TMySQLQuery;
    DsQry: TDataSource;
    Panel2: TPanel;
    DBGTextos: TdmkDBGridZTO;
    QryTEXTO: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGTextosDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure SelecionaItem();
  public
    { Public declarations }
    FDB: TmySQLDatabase;
    FSQL, FTextoSelected: String;
    FSelected: Boolean;
    //
    procedure ReabreTabela();
  end;

  var
  FmTextoDeLista: TFmTextoDeLista;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmTextoDeLista.BtOKClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmTextoDeLista.BtSaidaClick(Sender: TObject);
begin
  FSelected := False;
  FTextoSelected := EmptyStr;
  Close;
end;

procedure TFmTextoDeLista.DBGTextosDblClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmTextoDeLista.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTextoDeLista.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FDB       := nil;
  FSQL      := EmptyStr;
  FTextoSelected := EmptyStr;
  FSelected := False;
end;

procedure TFmTextoDeLista.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTextoDeLista.ReabreTabela;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, FDB, [FSQL]);
end;

procedure TFmTextoDeLista.SelecionaItem();
begin
  if (Qry.State <> dsInactive) and (Qry.RecordCount > 0) then
  begin
    FSelected := True;
    FTextoSelected := QryTEXTO.Value;
    Close;
  end;
end;

end.
