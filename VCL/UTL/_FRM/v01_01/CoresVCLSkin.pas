unit CoresVCLSkin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
{$IfDef cSkinRank}
  WinSkinData, WinSkinStore,
{$ENdIf}
  ExtCtrls, StdCtrls, Buttons;

type
  TFmCoresVCLSkin = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    Shape4: TShape;
    Shape5: TShape;
    Shape6: TShape;
    Shape7: TShape;
    Shape8: TShape;
    Shape9: TShape;
    Shape10: TShape;
    Shape11: TShape;
    Shape12: TShape;
    Shape13: TShape;
    Shape14: TShape;
    Shape15: TShape;
    Shape16: TShape;
    Shape17: TShape;
    Shape18: TShape;
    Shape19: TShape;
    Shape20: TShape;
    Shape21: TShape;
    Shape22: TShape;
    Shape23: TShape;
    Shape24: TShape;
    Shape25: TShape;
    Shape26: TShape;
    Shape27: TShape;
    Shape28: TShape;
    Shape29: TShape;
    Shape30: TShape;
    Shape31: TShape;
    Shape32: TShape;
    Shape33: TShape;
    Shape34: TShape;
    Shape35: TShape;
    Shape36: TShape;
    Shape37: TShape;
    Shape38: TShape;
    Shape39: TShape;
    Shape40: TShape;
    Shape41: TShape;
    Shape42: TShape;
    Shape43: TShape;
    Shape44: TShape;
    Shape45: TShape;
    Shape46: TShape;
    Shape47: TShape;
    Shape48: TShape;
    Shape49: TShape;
    Shape50: TShape;
    Shape51: TShape;
    Shape52: TShape;
    Shape53: TShape;
    Shape54: TShape;
    Shape55: TShape;
    Shape56: TShape;
    Shape57: TShape;
    Shape58: TShape;
    Shape59: TShape;
    Shape60: TShape;
    Shape61: TShape;
    Shape62: TShape;
    Shape63: TShape;
    Shape64: TShape;
    Shape65: TShape;
    Shape66: TShape;
    Shape67: TShape;
    Shape68: TShape;
    Shape69: TShape;
    Shape70: TShape;
    Shape71: TShape;
    Shape72: TShape;
    Edit1: TEdit;
    Label1: TLabel;
    Button1: TButton;
    Memo1: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Shape1MouseActivate(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y, HitTest: Integer;
      var MouseActivate: TMouseActivate);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure Label1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCor: Integer;
  end;

  var
  FmCoresVCLSkin: TFmCoresVCLSkin;
  ShemeColorName: array[0..19] of String = (
         'csText','csTitleTextActive','csTitleTextNoActive',
         'csButtonFace','csButtonText',
         'csButtonHilight','csButtonlight','csButtonShadow','csButtonDkshadow',
         'csSelectText','csSelectBg','csHilightText','csHilight',
         'csMenuBar','csMenuBarText','csMenuText','csMenubg','csCaption',
         'csScrollbar','csTextDisable');

implementation

uses UnMyObjects, Principal;

{$R *.DFM}

procedure TFmCoresVCLSkin.BtOKClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCoresVCLSkin.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCoresVCLSkin.Button1Click(Sender: TObject);
var
  I: Integer;
  Nome, Cor: String;
begin
  Memo1.Lines.Clear;
  for I := 1 to 72 do
  begin
    Nome := 'Shape' + FormatFloat('0', I);
    if (FindComponent(Nome) as TShape) <> nil then
    begin
      Cor := ColorToString(TShape(FindComponent(Nome) as TShape).Brush.Color);
      while Length(Nome) < 10 do
        Nome := Nome + ' ';
      Memo1.lines.Add(Nome + ': ' + Cor);
    end;
  end;
end;

procedure TFmCoresVCLSkin.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  {$IfDef cSkinRank} //Berlin
  if FmPrincipal.sd1.Active = True then
  begin
    Shape1.Brush.Color :=  FmPrincipal.sd1.Colors[csText];
    Shape2.Brush.Color :=  FmPrincipal.sd1.Colors[csTitleTextActive];
    Shape3.Brush.Color :=  FmPrincipal.sd1.Colors[csTitleTextNoActive];
    Shape4.Brush.Color :=  FmPrincipal.sd1.Colors[csButtonFace];
    Shape5.Brush.Color :=  FmPrincipal.sd1.Colors[csButtonText];
    Shape6.Brush.Color :=  FmPrincipal.sd1.Colors[csButtonHilight];
    Shape7.Brush.Color :=  FmPrincipal.sd1.Colors[csButtonlight];
    Shape8.Brush.Color :=  FmPrincipal.sd1.Colors[csButtonShadow];
    Shape9.Brush.Color :=  FmPrincipal.sd1.Colors[csButtonDkshadow];
    Shape10.Brush.Color := FmPrincipal.sd1.Colors[csSelectText];
    Shape11.Brush.Color := FmPrincipal.sd1.Colors[csSelectBg];
    Shape12.Brush.Color := FmPrincipal.sd1.Colors[csHilightText];
    Shape13.Brush.Color := FmPrincipal.sd1.Colors[csHilight];
    Shape14.Brush.Color := FmPrincipal.sd1.Colors[csMenuBar];
    Shape15.Brush.Color := FmPrincipal.sd1.Colors[csMenuBarText];
    Shape16.Brush.Color := FmPrincipal.sd1.Colors[csMenuText];
    Shape17.Brush.Color := FmPrincipal.sd1.Colors[csMenubg];
    Shape18.Brush.Color := FmPrincipal.sd1.Colors[csCaption];
    Shape19.Brush.Color := FmPrincipal.sd1.Colors[csScrollbar];
    Shape20.Brush.Color := FmPrincipal.sd1.Colors[csTextDisable];
  end;
  {$EndIf} //Berlin
  {$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active = True then
  begin
    //? TODO
    Shape1.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(0)];
    Shape2.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(1)];
    Shape3.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(2)];
    Shape4.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(3)];
    Shape5.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(4)];
    Shape6.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(5)];
    Shape7.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(6)];
    Shape8.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(7)];
    Shape9.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(8)];
    Shape10.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(9)];
    Shape11.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(10)];
    Shape12.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(11)];
    Shape13.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(12)];
    Shape14.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(13)];
    Shape15.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(14)];
    Shape16.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(15)];
    Shape17.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(16)];
    Shape18.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(17)];
    Shape19.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(18)];
    Shape20.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(19)];
    Shape21.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(20)];
    Shape22.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(21)];
    Shape23.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(22)];
    Shape24.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(23)];
    Shape25.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(24)];
    Shape26.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(25)];
    Shape27.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(26)];
    Shape28.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(27)];
    Shape29.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(28)];
    Shape30.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(29)];
    Shape31.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(30)];
    Shape32.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(31)];
    Shape33.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(32)];
    Shape34.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(33)];
    Shape35.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(34)];
    Shape36.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(35)];
    Shape37.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(36)];
    Shape38.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(37)];
    Shape39.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(38)];
    Shape40.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(39)];
    Shape41.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(40)];
    Shape42.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(41)];
    Shape43.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(42)];
    Shape44.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(43)];
    Shape45.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(44)];
    Shape46.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(45)];
    Shape47.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(46)];
    Shape48.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(47)];
    Shape49.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(48)];
    Shape50.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(49)];
    Shape51.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(50)];
    Shape52.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(51)];
    Shape53.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(52)];
    Shape54.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(53)];
    Shape55.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(54)];
    Shape56.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(55)];
    Shape57.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(56)];
    Shape58.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(57)];
    Shape59.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(58)];
    Shape60.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(59)];
    Shape61.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(60)];
    Shape62.Brush.Color :=  FmPrincipal.sSkinManager1.Palette[TacPaletteColors(61)];
  end;
{$EndIf}
end;

procedure TFmCoresVCLSkin.FormCreate(Sender: TObject);
begin
  FCor := 0;
end;

procedure TFmCoresVCLSkin.FormResize(Sender: TObject);
begin
{
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [(*LaAviso1, LaAviso2*)], Caption, True, taCenter, 2, 10, 20);
}
end;

procedure TFmCoresVCLSkin.Label1Click(Sender: TObject);
begin
  FCor := clNone;
end;

procedure TFmCoresVCLSkin.Label1DblClick(Sender: TObject);
begin
  FCor := clNone;
  Close;
end;

procedure TFmCoresVCLSkin.Shape1MouseActivate(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y, HitTest: Integer;
  var MouseActivate: TMouseActivate);
var
  Codigo: Integer;
begin
  FCor := TShape(Sender).Brush.Color;
  Codigo := StrToInt(Copy(TShape(Sender).Name, 6));
  if Codigo < 20 then
    Edit1.Text := ShemeColorName[Codigo-1]
  else
    Edit1.Text := ColorToString(FCor);
end;

end.
