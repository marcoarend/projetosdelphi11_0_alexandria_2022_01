object FmCoresVCLSkin: TFmCoresVCLSkin
  Left = 339
  Top = 185
  Caption = 'Cores VCLSkin'
  ClientHeight = 722
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 674
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Edit1: TEdit
      Left = 128
      Top = 12
      Width = 229
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
    object Button1: TButton
      Left = 392
      Top = 4
      Width = 141
      Height = 40
      Caption = 'Lista todos nomes'
      TabOrder = 3
      OnClick = Button1Click
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Cores VCLSkin'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 368
      ExplicitTop = 2
      ExplicitWidth = 422
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 537
    Align = alTop
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object Shape1: TShape
      Left = 0
      Top = 0
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape2: TShape
      Left = 64
      Top = 0
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape3: TShape
      Left = 128
      Top = 0
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape4: TShape
      Left = 192
      Top = 0
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape5: TShape
      Left = 256
      Top = 0
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape6: TShape
      Left = 320
      Top = 0
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape7: TShape
      Left = 384
      Top = 0
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape8: TShape
      Left = 448
      Top = 0
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape9: TShape
      Left = 512
      Top = 0
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape10: TShape
      Left = 576
      Top = 0
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape11: TShape
      Left = 640
      Top = 0
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape12: TShape
      Left = 704
      Top = 0
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape13: TShape
      Left = 0
      Top = 64
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape14: TShape
      Left = 64
      Top = 64
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape15: TShape
      Left = 128
      Top = 64
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape16: TShape
      Left = 192
      Top = 64
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape17: TShape
      Left = 256
      Top = 64
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape18: TShape
      Left = 320
      Top = 64
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape19: TShape
      Left = 384
      Top = 64
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape20: TShape
      Left = 448
      Top = 64
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape21: TShape
      Left = 512
      Top = 64
      Width = 65
      Height = 65
      Brush.Color = clBlack
      Pen.Color = clNone
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape22: TShape
      Left = 576
      Top = 64
      Width = 65
      Height = 65
      Brush.Color = clMaroon
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape23: TShape
      Left = 640
      Top = 64
      Width = 65
      Height = 65
      Brush.Color = clGreen
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape24: TShape
      Left = 704
      Top = 64
      Width = 65
      Height = 65
      Brush.Color = clOlive
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape25: TShape
      Left = 0
      Top = 128
      Width = 65
      Height = 65
      Brush.Color = clNavy
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape26: TShape
      Left = 64
      Top = 128
      Width = 65
      Height = 65
      Brush.Color = clPurple
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape27: TShape
      Left = 128
      Top = 128
      Width = 65
      Height = 65
      Brush.Color = clTeal
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape28: TShape
      Left = 192
      Top = 128
      Width = 65
      Height = 65
      Brush.Color = clGray
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape29: TShape
      Left = 256
      Top = 128
      Width = 65
      Height = 65
      Brush.Color = clSilver
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape30: TShape
      Left = 320
      Top = 128
      Width = 65
      Height = 65
      Brush.Color = clRed
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape31: TShape
      Left = 384
      Top = 128
      Width = 65
      Height = 65
      Brush.Color = clLime
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape32: TShape
      Left = 448
      Top = 128
      Width = 65
      Height = 65
      Brush.Color = clYellow
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape33: TShape
      Left = 512
      Top = 128
      Width = 65
      Height = 65
      Brush.Color = clBlue
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape34: TShape
      Left = 576
      Top = 128
      Width = 65
      Height = 65
      Brush.Color = clFuchsia
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape35: TShape
      Left = 640
      Top = 128
      Width = 65
      Height = 65
      Brush.Color = clAqua
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape36: TShape
      Left = 704
      Top = 128
      Width = 65
      Height = 65
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape37: TShape
      Left = 0
      Top = 192
      Width = 65
      Height = 65
      Brush.Color = clMoneyGreen
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape38: TShape
      Left = 64
      Top = 192
      Width = 65
      Height = 65
      Brush.Color = clSkyBlue
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape39: TShape
      Left = 128
      Top = 192
      Width = 65
      Height = 65
      Brush.Color = clCream
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape40: TShape
      Left = 192
      Top = 192
      Width = 65
      Height = 65
      Brush.Color = clMedGray
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape41: TShape
      Left = 256
      Top = 192
      Width = 65
      Height = 65
      Brush.Color = clActiveBorder
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape42: TShape
      Left = 320
      Top = 192
      Width = 65
      Height = 65
      Brush.Color = clActiveCaption
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape43: TShape
      Left = 384
      Top = 192
      Width = 65
      Height = 65
      Brush.Color = clAppWorkSpace
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape44: TShape
      Left = 448
      Top = 192
      Width = 65
      Height = 65
      Brush.Color = clBackground
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape45: TShape
      Left = 512
      Top = 192
      Width = 65
      Height = 65
      Brush.Color = clBtnFace
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape46: TShape
      Left = 576
      Top = 192
      Width = 65
      Height = 65
      Brush.Color = clBtnHighlight
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape47: TShape
      Left = 640
      Top = 192
      Width = 65
      Height = 65
      Brush.Color = clBtnShadow
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape48: TShape
      Left = 704
      Top = 192
      Width = 65
      Height = 65
      Brush.Color = clBtnText
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape49: TShape
      Left = 0
      Top = 256
      Width = 65
      Height = 65
      Brush.Color = clCaptionText
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape50: TShape
      Left = 64
      Top = 256
      Width = 65
      Height = 65
      Brush.Color = clDefault
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape51: TShape
      Left = 128
      Top = 256
      Width = 65
      Height = 65
      Brush.Color = clGradientActiveCaption
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape52: TShape
      Left = 192
      Top = 256
      Width = 65
      Height = 65
      Brush.Color = clGradientInactiveCaption
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape53: TShape
      Left = 256
      Top = 256
      Width = 65
      Height = 65
      Brush.Color = clGrayText
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape54: TShape
      Left = 320
      Top = 256
      Width = 65
      Height = 65
      Brush.Color = clHighlight
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape55: TShape
      Left = 384
      Top = 256
      Width = 65
      Height = 65
      Brush.Color = clHighlightText
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape56: TShape
      Left = 448
      Top = 256
      Width = 65
      Height = 65
      Brush.Color = clHotLight
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape57: TShape
      Left = 512
      Top = 256
      Width = 65
      Height = 65
      Brush.Color = clInactiveBorder
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape58: TShape
      Left = 576
      Top = 256
      Width = 65
      Height = 65
      Brush.Color = clInactiveCaption
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape59: TShape
      Left = 640
      Top = 256
      Width = 65
      Height = 65
      Brush.Color = clInactiveCaptionText
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape60: TShape
      Left = 704
      Top = 256
      Width = 65
      Height = 65
      Brush.Color = clInfoBk
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape61: TShape
      Left = 0
      Top = 320
      Width = 65
      Height = 65
      Brush.Color = clInfoText
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape62: TShape
      Left = 64
      Top = 320
      Width = 65
      Height = 65
      Brush.Color = clMenu
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape63: TShape
      Left = 128
      Top = 320
      Width = 65
      Height = 65
      Brush.Color = clMenuBar
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape64: TShape
      Left = 192
      Top = 320
      Width = 65
      Height = 65
      Brush.Color = clMenuHighlight
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape65: TShape
      Left = 256
      Top = 320
      Width = 65
      Height = 65
      Brush.Color = clMenuText
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape66: TShape
      Left = 320
      Top = 320
      Width = 65
      Height = 65
      Brush.Color = clNone
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape67: TShape
      Left = 384
      Top = 320
      Width = 65
      Height = 65
      Brush.Color = clScrollBar
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape68: TShape
      Left = 448
      Top = 320
      Width = 65
      Height = 65
      Brush.Color = cl3DDkShadow
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape69: TShape
      Left = 512
      Top = 320
      Width = 65
      Height = 65
      Brush.Color = cl3DLight
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape70: TShape
      Left = 576
      Top = 320
      Width = 65
      Height = 65
      Brush.Color = clWindow
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape71: TShape
      Left = 640
      Top = 320
      Width = 65
      Height = 65
      Brush.Color = clWindowFrame
      OnMouseActivate = Shape1MouseActivate
    end
    object Shape72: TShape
      Left = 704
      Top = 320
      Width = 65
      Height = 65
      Brush.Color = clWindowText
      Pen.Color = clNone
      OnMouseActivate = Shape1MouseActivate
    end
    object Label1: TLabel
      Left = 328
      Top = 392
      Width = 52
      Height = 13
      Caption = '[Nenhuma]'
      OnClick = Label1Click
      OnDblClick = Label1DblClick
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 585
    Width = 784
    Height = 89
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
end
