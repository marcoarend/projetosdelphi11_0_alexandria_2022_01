unit LogOff;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables;

type
  TFmLogOff = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    EdLogin: TEdit;
    EdSenha: TEdit;
    Label2: TLabel;
    Label4: TLabel;
    EdEmpresa: TEdit;
    Timer1: TTimer;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdLoginKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Timer1Timer(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    FConfirmou: Boolean;
    procedure AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa: TEdit; FmMLA,
              FmMain: TForm; Componente: TComponent; ForcaSenha: Boolean;
              LaAviso1, LaAviso2: TLabel; QrGeraSenha, DmodFinQrCarts: TmySQLQuery);
  public
    { Public declarations }
    FDataBases: array of TmySQLDatabase;
  end;

  var
  FmLogOff: TFmLogOff;

implementation

uses UnMyObjects, Principal, UnGOTOy, UMySQLDB;

{$R *.DFM}

procedure TFmLogOff.AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa: TEdit;
  FmMLA, FmMain: TForm; Componente: TComponent; ForcaSenha: Boolean; LaAviso1,
  LaAviso2: TLabel; QrGeraSenha, DmodFinQrCarts: TmySQLQuery);
begin
  Screen.Cursor := crHourGlass;
  try
    USQLDB.ExecutaPingServidor(FDataBases);
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
  end;
  FConfirmou := True;
  GOTOy.AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa, FmLogOff, FmPrincipal,
    BtOk, False(*ForcaSenha*), LaAviso1, LaAviso2, nil, nil, True);
end;

procedure TFmLogOff.BitBtn1Click(Sender: TObject);
begin
  FmPrincipal.BringToFront;
end;

procedure TFmLogOff.BtOKClick(Sender: TObject);
begin
  AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa, FmLogOff, FmPrincipal, BtOk,
    False(*ForcaSenha*), LaAviso1, LaAviso2, nil, nil);
end;

procedure TFmLogOff.BtSaidaClick(Sender: TObject);
begin
  FConfirmou    := False;
  VAR_TERMINATE := True;
  Close;
end;

procedure TFmLogOff.EdLoginKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
    AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa, FmLogOff, FmPrincipal,
      BtOk, False(*ForcaSenha*), LaAviso1, LaAviso2, nil, nil);
  if (Key = VK_ESCAPE) then
    Close;
end;

procedure TFmLogOff.EdSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
    AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa, FmLogOff, FmPrincipal,
      BtOk, False(*ForcaSenha*), LaAviso1, LaAviso2, nil, nil);
  if (Key = VK_ESCAPE) then
    Close;
end;

procedure TFmLogOff.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLogOff.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if VAR_TERMINATE = False then
  begin
    VAR_SHOW_MASTER_POPUP := True;
    //FmPrincipal.AcoesIniciaisDoAplicativo();
  end;
  if FConfirmou = False then
    FmPrincipal.Close;
end;

procedure TFmLogOff.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FConfirmou      := False;
  FDataBases      := nil;
end;

procedure TFmLogOff.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) or (Key = VK_RETURN) or (Key = VK_TAB) then
  begin
    FmLogOff.BringToFront;
  end;
end;

procedure TFmLogOff.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLogOff.FormShow(Sender: TObject);
begin
  EdLogin.SetFocus;
end;

procedure TFmLogOff.Timer1Timer(Sender: TObject);
begin
  if FmLogOff.Visible = False then
  begin
    FmLogOff.Visible := True;
    FmLogOff.BringToFront;
  end;
end;

end.
