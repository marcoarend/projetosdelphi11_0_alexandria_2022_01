unit PesqNome;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, mySQLDbTables,
  dmkGeral, dmkEdit, dmkEditF7, DBCtrls, (*DBTables,*) dmkDBLookupComboBox,
  dmkDBGrid, dmkImage, DmkDAC_PF, UnDmkEnums;

type
  TFmPesqNome = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    DBGLista: TdmkDBGrid;
    Label1: TLabel;
    EdPesq: TdmkEdit;
    EdDoc: TdmkEdit;
    LaDoc: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    EdAntSelCod: TdmkEdit;
    EdAntSelNom: TdmkEdit;
    Label2: TLabel;
    EdTxtPesq: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGListaDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdTxtPesqDblClick(Sender: TObject);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure EdTxtPesqEnter(Sender: TObject);
  private
    { Private declarations }
    procedure VerificaEan13();
  public
    { Public declarations }
    FCB: TDBLookupCombobox;
    FF7: TdmkEdit;
    FDB: TmySQLDatabase;
    FND: Boolean;
    FNomeTabela, FNomeCampoNome, FNomeCampoDoc, FNomeCampoCodigo, FTexto,
    FDefinedSQL, FSQLJoke_SQL, FSQLJoke_Jok: String;
    FAtivou: Boolean;
    FDS: TDataSource;
  end;

  var
  FmPesqNome: TFmPesqNome;

implementation

uses UnMyObjects, UnInternalConsts, Module;

{$R *.DFM}

procedure TFmPesqNome.BtOKClick(Sender: TObject);
const
  sProcName = 'TFmPesqNome.BtOKClick()';
var
  CodTxt: String;
begin
  if QrPesq.State <> dsInactive then
  begin
    //VAR_CADASTRO := QrPesq.Fields[0].AsInteger;
    //
(*
  ini 2022-04-12
*)
    try
      VAR_CADASTRO := QrPesq.Fields[0].AsInteger;
    except
      try
        CodTxt := QrPesq.Fields[0].AsString;
        CodTxt := '0' + Geral.SoNumero_TT(CodTxt);
        VAR_CADASTRO := Geral.IMV(CodTxt);
      except
        on E: Exception do
          Geral.MB_Erro('Erro ao definir c�digo em ' + sProcName + sLineBreak +
          E.Message);
      end;
    end;
// fim 2022-04-12
    VAR_CAD_NOME := QrPesq.Fields[1].AsString;
    VAR_ANT_SEL_CADASTRO := VAR_CADASTRO;
    VAR_ANT_SEL_CAD_NOME := VAR_CAD_NOME;
    //
    Close;
  end;
end;

procedure TFmPesqNome.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqNome.DBGListaDblClick(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmPesqNome.EdPesqChange(Sender: TObject);
var
  Texto, Doc, SQL_Text, Masc: String;
begin
  QrPesq.Close;
  if FDB <> nil then
    QrPesq.Database := FDB;
  //
  Texto := EdPesq.ValueVariant;
  //
  if Texto <> '' then
    Texto := StringReplace(Texto, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
  //
  Doc := Geral.SoNumero_TT(EdDoc.Text);
  if (Length(Texto) >= 2) or (Length(Doc) > 10) then
  begin
    QrPesq.SQL.Clear;
    // ini 2020-11-07
    if Trim(FSQLJoke_SQL) <> '' then
    begin
      Masc := FSQLJoke_Jok;
      if Masc = EmptyStr then
        Masc := CO_JOKE_SQL;
      SQL_Text := StringReplace(FSQLJoke_SQL, Masc, Texto, [rfReplaceAll]);
      QrPesq.SQL.Text := SQL_Text;
    end else
    // fim 2020-11-07
    if Trim(FDefinedSQL) <> '' then
    begin
      SQL_Text := Format(FDEfinedSQL, ['"%' + Texto + '%"']);
      QrPesq.SQL.Text := SQL_Text;
    end else
    // Usar antes do entidades por causa do EntidadesImp (Rua, etc...)
    if (FCB <> nil) and (FCB is TdmkDBLookupComboBox)
    and (TdmkDBLookupComboBox(FCB).LocF7SQLText.Text <> '') then
    begin
      SQL_Text := TdmkDBLookupComboBox(FCB).LocF7SQLText.Text;
      Masc := TdmkDBLookupComboBox(FCB).LocF7SQLMasc;
      // ini 2020-11-05
      //SQL_Text := Geral.Substitui(SQL_Text, Masc, Texto);
      SQL_Text := StringReplace(SQL_Text, Masc, Texto, [rfReplaceAll]);
      // fim 2020-11-05
      QrPesq.SQL.Text := SQL_Text;
      //Geral.MB_Teste(QrPesq.SQL.Text)
    end else
    begin
      if FNomeTabela = '' then
      begin
        if FDS <> nil then
          FNomeTabela := Lowercase(Copy(FDS.Name, 3));
        if FCB <> nil then
          FNomeTabela := Lowercase(Copy(FCB.Name, 3));

      end;
      if FNomeCampoCodigo = '' then
        FNomeCampoCodigo := 'CODIGO';
      if FNomeCampoNome = '' then
        FNomeCampoNome := 'NOME';
      //
      if FNomeTabela = 'entidades' then
      begin
  (*    2013-06-13
        QrPesq.SQL.Add('SELECT Codigo _Codigo, IF(Tipo=0,');
        QrPesq.SQL.Add('CONCAT(RazaoSocial, " ", Fantasia),');
        QrPesq.SQL.Add('CONCAT(Nome, " ", Apelido)) _Nome');
  *)

  (*    2017-06-24
        QrPesq.SQL.Add('SELECT Codigo _Codigo, CONCAT(');
        QrPesq.SQL.Add('IF(RazaoSocial <> "", CONCAT("{R} ", RazaoSocial, " "), ""),');
        QrPesq.SQL.Add('IF(Fantasia <> "", CONCAT("{F} ", Fantasia, " "), ""),');
        QrPesq.SQL.Add('IF(Nome <> "", CONCAT("{N} ", Nome, " "), ""),');
        QrPesq.SQL.Add('IF(Apelido <> "", CONCAT("{A} ", Apelido, " "), "")');
  *)


        QrPesq.SQL.Add(' SELECT Codigo _Codigo, CONCAT(');
        QrPesq.SQL.Add('  IF(RazaoSocial <> "", CONCAT("{R} ", RazaoSocial, " "), ""),');
        QrPesq.SQL.Add('  IF(Fantasia <> "", CONCAT("{F} ", Fantasia, " "), ""),');
        QrPesq.SQL.Add('  IF(Nome <> "", CONCAT("{N} ", Nome, " "), ""),');
        QrPesq.SQL.Add('  IF(Apelido <> "", CONCAT("{A} ", Apelido, " "), ""),');
        QrPesq.SQL.Add('  " - ", IF(Tipo=0, ');

        QrPesq.SQL.Add('IF(CNPJ IS NULL, "", ');

        QrPesq.SQL.Add('  CONCAT("CNPJ: ", MID(CNPJ,1,2), ".", MID(CNPJ,3,3), ".",');
        QrPesq.SQL.Add('  MID(CNPJ,6,3), "/", MID(CNPJ,9,4), "-", MID(CNPJ, 13,2)) ');

        QrPesq.SQL.Add('  )');

        QrPesq.SQL.Add('  , ');


        QrPesq.SQL.Add('IF (CPF IS NULL, "", ');


        QrPesq.SQL.Add('  CONCAT("CPF: ", MID(CPF,1,3), ".", MID(CPF,4,3), ".",');
        QrPesq.SQL.Add('  MID(CPF,7,3), "-", MID(CPF,10,2)) ');
        QrPesq.SQL.Add('  )');

        QrPesq.SQL.Add('  )');

  //    FIM 2017-06-24
        QrPesq.SQL.Add(') _Nome');
  //    FIM 2013-06-13
        QrPesq.SQL.Add('FROM entidades');
        QrPesq.SQL.Add('WHERE (RazaoSocial LIKE "%' + Texto + '%"');
        QrPesq.SQL.Add('OR Nome LIKE "%' + Texto + '%"');
        QrPesq.SQL.Add('OR Fantasia LIKE "%' + Texto + '%"');
        QrPesq.SQL.Add('OR Apelido LIKE "%' + Texto + '%")');
        if EdDoc.Visible then
        begin
          if Doc <> '' then
            QrPesq.SQL.Add('AND ((CNPJ="' + Doc + '") OR (CPF = "' + Doc + '"))');
        end;
        //QrPesq.SQL.Add('ORDER BY _Nome');
        QrPesq.SQL.Add('ORDER BY IF(Tipo=0, RazaoSocial, Nome)');
      end else
      if (Lowercase(FNomeTabela) = 'gragrux')
      or (Uppercase(FNomeCampoNome) = 'NO_PRD_TAM_COR')
      then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, QrPesq.Database, [
        'SELECT ggx.Controle _Codigo,  ',
        'CONCAT(gg1.Nome,  ',
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)))  ',
        '_Nome  ',
        'FROM gragrux ggx  ',
        'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
        'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
        'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
        'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
        'WHERE CONCAT(gg1.Nome,  ',
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
        'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
        'LIKE "%' + Texto + '%"  ',
        'ORDER BY _Nome  ',
        '']);
      end else
      if (Lowercase(FNomeTabela) = 'stqcenloc')
      or (Lowercase(FNomeCampoNome) = 'NO_LOC_CEN')
       then
      begin
        QrPesq.SQL.Add('SELECT scl.Controle, scl.CodUsu, ');
        QrPesq.SQL.Add('CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ');
        QrPesq.SQL.Add('FROM stqcenloc scl ');
        QrPesq.SQL.Add('LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ');
        QrPesq.SQL.Add('WHERE CONCAT(scl.Nome, " (", scc.Nome, ")") LIKE "%' + Texto + '%" ');
        QrPesq.SQL.Add('ORDER BY NO_LOC_CEN ');
      end else
      begin
        QrPesq.SQL.Add('SELECT ' + FNomeCampoCodigo + ' _Codigo, ' + FNomeCampoNome +
        ' _Nome FROM ' + FNomeTabela + ' WHERE ' + FNomeCampoNome + ' LIKE "%' +
        Texto + '%"');
      end;
    end;
    try
      // Usa nos aplicativos DermaXX
      //UnDmkDAC_PF.AbreQuery(QrPesq, 'TFmPesqNome.EdPesqChange()');
      UnDmkDAC_PF.AbreQueryApenas(QrPesq);
      //Geral.MB_Teste(QrPesq.SQL.Text);
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrPesq, '', nil, True, True);
    end;
  end;
end;

procedure TFmPesqNome.EdTxtPesqDblClick(Sender: TObject);
begin
  EdPesq.Text := '%' + EdTxtPesq.Text + '%';
end;

procedure TFmPesqNome.EdTxtPesqEnter(Sender: TObject);
begin
  VerificaEAN13();
end;

procedure TFmPesqNome.FormActivate(Sender: TObject);
var
  Query: TmySQLQuery;
  Texto: String;
  P, P10, P13: Integer;
  DS: TDataSource;
begin
  MyObjects.CorIniComponente();
  if FCB <> nil then
  begin
    DS := FCB.ListSource;
    if DS <> nil then
    begin
      Query := TmySQLQuery(DS.DataSet);
      if Query <> nil then
      begin
        Texto := Lowercase(Query.SQL.Text);
        P := pos('from', Texto);
        if P > 0 then
        begin
          Texto := Trim(Copy(Texto, P + 4));
          P := pos(' ', Texto);
          P10 := pos(#10, Texto);
          P13 := pos(#13, Texto);
          if (P13 < P) and (P13 > 0) then P := P13;
          if (P10 < P) and (P10 > 0) then P := P10;
          if P > 0 then
          begin
            FNomeTabela := Trim(Copy(Texto, 1, P));
            FNomeCampoNome   := FCB.ListField;
            FNomeCampoCodigo := FCB.KeyField;
            //
            if FCB is TdmkDBLookupComboBox then
            begin
              if TdmkDBLookupComboBox(FCB).LocF7TableName <> '' then
                FNometabela := TdmkDBLookupComboBox(FCB).LocF7TableName;
              if TdmkDBLookupComboBox(FCB).LocF7CodiFldName <> '' then
                FNomeCampoCodigo := TdmkDBLookupComboBox(FCB).LocF7CodiFldName;
              if TdmkDBLookupComboBox(FCB).LocF7NameFldName <> '' then
                FNomeCampoNome := TdmkDBLookupComboBox(FCB).LocF7NameFldName;
              if TdmkDBLookupComboBox(FCB).LocF7SQLText.Text <> '' then
              begin
                FNomeTabela := '';
              end;
            end;
          end;
        end;
      end;
    end;
  end
  else
  if FF7 is TdmkEditF7 then
  begin
    if TdmkEditF7(FF7).LocF7TableName <> '' then
      FNometabela := TdmkEditF7(FF7).LocF7TableName;

    if TdmkEditF7(FF7).LocF7CodiFldName <> '' then
      FNomeCampoCodigo := TdmkEditF7(FF7).LocF7CodiFldName;

    if TdmkEditF7(FF7).LocF7NameFldName <> '' then
      FNomeCampoNome := TdmkEditF7(FF7).LocF7NameFldName;
  end else
  if FND then
  begin
    //
  end;

  //

  if FNomeTabela = 'cunscad' then
    FNomeTabela := 'entidades';
  //

  if FNomeTabela = 'entidades' then
  begin
    LaDoc.Visible := True;
    EdDoc.Visible := True;
    EdPesq.Width  := 648;
  end;
  //
  if not FAtivou then
  begin
    FAtivou := True;
    EdPesq.Text := FTexto;
  end;
end;

procedure TFmPesqNome.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  FND := False;
  QrPesq.Database := Dmod.MyDB;
  FAtivou := False;
  FTexto := '';
  FDefinedSQL := '';
  FSQLJoke_SQL := '';
  FSQLJoke_Jok := '';
  EdAntSelCod.ValueVariant := VAR_ANT_SEL_CADASTRO;
  EdAntSelNom.ValueVariant := VAR_ANT_SEL_CAD_NOME;
end;

procedure TFmPesqNome.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesqNome.QrPesqAfterOpen(DataSet: TDataSet);
begin
  VerificaEan13();
end;

procedure TFmPesqNome.VerificaEan13;
begin
  // EAN13
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount = 1)
  and (Length(EdPesq.Text) >= 8) and EdTxtPesq.Focused then
  begin
    BtOKClick(Self);
  end;
end;

end.
