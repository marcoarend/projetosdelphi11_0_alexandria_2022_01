unit AtrDef;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMySQLCuringa, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu,
  dmkLabel, Mask, dmkDBEdit, dmkGeral, dmkImage, CfgAtributos, DmkDAC_PF,
  UnDmkEnums;

type
  TFmAtrDef = class(TForm)
    QrAtrCad: TmySQLQuery;
    DsAtrCad: TDataSource;
    QrAtrCadCodigo: TIntegerField;
    QrAtrCadCodUsu: TIntegerField;
    QrAtrCadNome: TWideStringField;
    QrAtrIts: TmySQLQuery;
    DsAtrIts: TDataSource;
    QrAtrItsControle: TIntegerField;
    QrAtrItsCodUsu: TIntegerField;
    QrAtrItsNome: TWideStringField;
    VUAtrCad: TdmkValUsu;
    VUAtrIts: TdmkValUsu;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    QrAtrCadAtrTyp: TIntegerField;
    Panel3: TPanel;
    Label3: TLabel;
    EdID_Sorc: TdmkEdit;
    Label4: TLabel;
    EdID_Item: TdmkEdit;
    Label1: TLabel;
    EdAtrCad: TdmkEditCB;
    CBAtrCad: TdmkDBLookupComboBox;
    SBAtrCad: TSpeedButton;
    PnAtrIts: TPanel;
    Label2: TLabel;
    EdAtrIts: TdmkEditCB;
    CBAtrIts: TdmkDBLookupComboBox;
    SBAtrIts: TSpeedButton;
    PnAtrTxt: TPanel;
    EdAtrTxt: TdmkEdit;
    Label5: TLabel;
    QrDup: TmySQLQuery;
    QrDupID_Item: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdAtrCadChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdAtrCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBAtrCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAtrItsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBAtrItsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBAtrCadClick(Sender: TObject);
    procedure SBAtrItsClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenAtrIts(Controle: Integer);
    procedure CadastraAtrCad(Qual, Codigo: Integer);
  public
    { Public declarations }
    FPermiteSelecionarAtrTyp: Boolean;
    FTipoTab: TTipoTab;
    FNomeTabDef, FNomeTabCad, FNomeTabIts, FNomeTabTxt: String;
    FQryDef, FQryTxt: TmySQLQuery;
    FEdCad, FEdIts: TdmkEdit;
    FCBCad, FCBIts: TdmkDBLookupCombobox;
    FQrCad, FQrIts: TmySQLQuery;
    //
    procedure AbreAtrCad(TabDef, TabCad, TabIts, TabTxt: String; TipoTab: TTipoTab);
  end;

  var
  FmAtrDef: TFmAtrDef;

implementation

uses UnMyObjects, Module, UMySQLModule, MyDBCheck, AtrCad, UnInternalConsts;

{$R *.DFM}

procedure TFmAtrDef.AbreAtrCad(TabDef, TabCad, TabIts, TabTxt: String; TipoTab: TTipoTab);
begin
  FNomeTabDef := TabDef;
  FNomeTabCad := TabCad;
  FNomeTabIts := TabIts;
  FNomeTabTxt := TabTxt;
  FTipoTab    := TipoTab;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtrCad, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome, AtrTyp ',
  'FROM ' + FNomeTabCad,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmAtrDef.BtOKClick(Sender: TObject);
var
  ID_Item: Integer;
var
  ID_Sorc, AtrCad, AtrIts: Integer;
  AtrTxt: String;
begin
  ID_Sorc        := EdID_Sorc.ValueVariant;;
  AtrCad         := VUAtrCad.ValueVariant;
  if PnAtrTxt.Visible then
  begin
    AtrIts         := 0;
    AtrTxt         := EdAtrTxt.Text;
  end else
  begin
    AtrIts         := VUAtrIts.ValueVariant;
    AtrTxt         := '';
  end;
  //
  if ImgTipo.SQLType = stIns then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDup, Dmod.MyDB, [
    'SELECT ID_Item',
    'FROM ' + FNomeTabDef,
    'WHERE ID_Sorc=' + Geral.FF0(ID_Sorc),
    'AND AtrCad=' + Geral.FF0(AtrCad),
    '']);
    if QrDup.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Inclus�o cancelada! ' + sLineBreak +
      'O atributo selecionado j� tem seu valor definido!');
      Exit;
    end;
  end;  
  //
  if FTipoTab = tiptbITS then
  begin
    if PnAtrIts.Visible then
    begin
      ID_Item := EdID_Item.ValueVariant;
      ID_Item := UMyMod.BPGS1I32(
        FNomeTabDef, 'ID_Item', '', '', tsDef, ImgTipo.SQLType, ID_Item);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, FNomeTabDef, False, [
      'ID_Sorc', 'AtrCad', 'AtrIts'], ['ID_Item'], [
      ID_Sorc, AtrCad, AtrIts], [ID_Item], True) then
      begin
        if FQryDef <> nil then
        begin
          FQryDef.Close;
          UnDmkDAC_PF.AbreQuery(FQryDef, Dmod.MyDB);
          FQryDef.Locate('ID_Item', ID_Item, []);
        end;
        Close;
      end;
    end else
    begin
      ID_Item := EdID_Item.ValueVariant;
      ID_Item := UMyMod.BPGS1I32(
        FNomeTabTxt, 'ID_Item', '', '', tsDef, ImgTipo.SQLType, ID_Item);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, FNomeTabTxt, False, [
      'ID_Sorc', 'AtrCad', 'AtrTxt'], ['ID_Item'], [
      ID_Sorc, AtrCad, AtrTxt], [ID_Item], True) then
      begin
        if FQryTxt <> nil then
        begin
          FQryTxt.Close;
          UnDmkDAC_PF.AbreQuery(FQryTxt, Dmod.MyDB);
          FQryTxt.Locate('ID_Item', ID_Item, []);
        end;
        Close;
      end;
    end;
  end else
  begin
    if FEdCad <> nil then
      FEdCad.ValueVariant := AtrCad;
    if FCBCad <> nil then
      FCBCad.KeyValue := AtrCad;
    if FEdIts <> nil then
      FEdIts.ValueVariant := AtrIts;
    if FCBIts <> nil then
      FCBIts.KeyValue := AtrIts;
    Close;
  end;
end;

procedure TFmAtrDef.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAtrDef.CBAtrCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
  begin
    CuringaLoc.Pesquisa('CodUsu', 'Nome', FNomeTabCad, Dmod.MyDB,
    ''(*Extra*), EdAtrCad, CBAtrCad, dmktfInteger);
    ReopenAtrIts(0);
  end;
end;

procedure TFmAtrDef.CBAtrItsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', FNomeTabIts, Dmod.MyDB,
    'AND Codigo = ' + FormatFloat('0', Geral.IMV(EdAtrCad.Text)),
    EdAtrIts, CBAtrIts, dmktfInteger);
end;

procedure TFmAtrDef.EdAtrCadChange(Sender: TObject);
begin
  ReopenAtrIts(0);
end;

procedure TFmAtrDef.EdAtrCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
  begin
    CuringaLoc.Pesquisa('CodUsu', 'Nome', FNomeTabCad, Dmod.MyDB,
    ''(*Extra*), EdAtrCad, CBAtrCad, dmktfInteger);
    ReopenAtrIts(0);
  end;
end;

procedure TFmAtrDef.EdAtrItsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', FNomeTabIts, Dmod.MyDB,
    'AND Codigo = ' + FormatFloat('0', Geral.IMV(EdAtrCad.Text)),
    EdAtrIts, CBAtrIts, dmktfInteger);
end;

procedure TFmAtrDef.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAtrDef.FormCreate(Sender: TObject);
begin
  FPermiteSelecionarAtrTyp := False;
  ImgTipo.SQLType := stLok;
  PnAtrIts.Align := alClient;
  PnAtrTxt.Align := alClient;
end;

procedure TFmAtrDef.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAtrDef.ReopenAtrIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtrIts, Dmod.MyDB, [
  'SELECT Controle, CodUsu, Nome ',
  'FROM ' + FNomeTabIts,
  'WHERE Codigo=' + Geral.FF0(QrAtrCadCodigo.Value),
  'ORDER BY Nome ',
  '']);
  if Controle <> 0 then
    QrAtrIts.Locate('Controle', Controle, []);
  //
  if (PnAtrIts.Visible) and (TTypTabAtr(QrAtrCadAtrTyp.Value) = ttaTxtFree) then
  begin
     PnAtrTxt.Visible := True;
     PnAtrIts.Visible := False;
  end
  else
  if (PnAtrTxt.Visible) and (TTypTabAtr(QrAtrCadAtrTyp.Value) = ttaPreDef) then
  begin
     PnAtrIts.Visible := True;
     PnAtrTxt.Visible := False;
  end
  else
  begin
     PnAtrIts.Visible := TTypTabAtr(QrAtrCadAtrTyp.Value) = ttaPreDef;
     PnAtrTxt.Visible := TTypTabAtr(QrAtrCadAtrTyp.Value) = ttaTxtFree;
  end
end;

procedure TFmAtrDef.SBAtrCadClick(Sender: TObject);
begin
  CadastraAtrCad(1, EdAtrCad.ValueVariant);
  EdAtrCad.SetFocus;
end;

procedure TFmAtrDef.SBAtrItsClick(Sender: TObject);
begin
  CadastraAtrCad(2, 0);
end;

procedure TFmAtrDef.CadastraAtrCad(Qual, Codigo: Integer);
begin
  VAR_CADASTRO := 0;
  VAR_CAD_ITEM := 0;
  if DBCheck.CriaFm(TFmAtrCad, FmAtrCad, afmoNegarComAviso) then
  begin
    FmAtrCad.FNomeTabCad := FNomeTabCad;
    FmAtrCad.FNomeTabIts := FNomeTabIts;
    FmAtrCad.FNomeTabTxt := FNomeTabTxt;
    FmAtrCad.FPermiteSelecionarAtrTyp := FPermiteSelecionarAtrTyp;
    //
    if Codigo <> 0 then
      FmAtrCad.LocCod(Codigo, Codigo);
    //
    FmAtrCad.ShowModal;
    FmAtrCad.Destroy;
    //
    if Qual = 1 then
    begin
      if VAR_CADASTRO <> 0 then
      begin
        QrAtrCad.Close;
        UnDmkDAC_PF.AbreQuery(QrAtrCad, Dmod.MyDB);
        //
        EdAtrCad.ValueVariant := VAR_CADASTRO;
        CBAtrCad.KeyValue     := VAR_CADASTRO;
        if FQrCad <> nil then
        begin
          FQrCad.Close;
          UnDmkDAC_PF.AbreQuery(FQrCad, Dmod.MyDB);
        end;
      end;
    end else begin
      if VAR_CAD_ITEM <> 0 then
      begin
        QrAtrIts.Close;
        UnDmkDAC_PF.AbreQuery(QrAtrIts, Dmod.MyDB);
        //
        EdAtrIts.ValueVariant := VAR_CAD_ITEM;
        CBAtrIts.KeyValue     := VAR_CAD_ITEM;
        if FQrIts <> nil then
        begin
          UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
        end;
      end;
    end;
  end;
end;

end.
