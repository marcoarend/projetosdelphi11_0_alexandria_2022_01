unit MudaTexto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums;

type
  TFmMudaTexto = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label1: TLabel;
    EdAtual: TEdit;
    Label2: TLabel;
    EdNovo: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FMuda: Boolean;
    FAtual, FNovo: String;
  end;

  var
  FmMudaTexto: TFmMudaTexto;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmMudaTexto.BtOKClick(Sender: TObject);
begin
  FMuda  := False;
  FAtual := EdAtual.Text;
  FNovo  := EdNovo.Text;
  if (FAtual <> '') or (FNovo <> '') then FMuda := True;
  Close;
end;

procedure TFmMudaTexto.BtSaidaClick(Sender: TObject);
begin
  FMuda  := False;
  FAtual := '';
  FNovo  := '';
  Close;
end;

procedure TFmMudaTexto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMudaTexto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FMuda := False;
end;

procedure TFmMudaTexto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
