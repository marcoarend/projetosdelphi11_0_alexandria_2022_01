unit MyDBGridReorder;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, DBGrids,
  Db, mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmMyDBGridReorder = class(TForm)
    Grid: TStringGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
    FMySQLQuery, FMySQLQueryUpd: TmySQLQuery;
    FDBGrid: TDBGrid;
    FCampoControle, FCampoDescricao, FCampoAOrdenar, FTabelaAOrdenar, FSQLExtra1: String;
    FTrocavel1, FTrocador1: String;
    procedure ConfiguraGrade;
  end;

  var
  FmMyDBGridReorder: TFmMyDBGridReorder;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmMyDBGridReorder.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMyDBGridReorder.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMyDBGridReorder.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMyDBGridReorder.ConfiguraGrade;
var
  i: Integer;
  Grade: TStringGrid;
  Campo: String;
begin
  try
    Screen.Cursor := crHourGlass;
    //
    if FDBGrid = nil then
    begin
      Grid.ColCount     := 2;
      Grid.RowCount     := FMySQLQuery.RecordCount + 1;
      Grid.ColWidths[0] := 110;
      Grid.Cells[0, 0]  := FCampoControle;
      Grid.ColWidths[1] := 350;
      Grid.Cells[1, 0]  := 'Descri��o';
      //
      i := 1;
      //
      FMySQLQuery.First;
      while not FMySQLQuery.Eof do
      begin
        Grid.Cells[0, i] := Geral.FF0(FMySQLQuery.FieldByName(FCampoControle).AsInteger);
        Grid.Cells[1, i] := FMySQLQuery.FieldByName(FCampoDescricao).AsString;
        //
        i := i + 1;
        //
        FMySQLQuery.Next;
      end;
    end else
    begin
      Grade                 := TStringGrid(FDBGrid);
      Grid.ColCount         := Grade.ColCount;
      Grid.DefaultColWidth  := Grade.DefaultColWidth;
      Grid.DefaultRowHeight := Grade.DefaultRowHeight;
      Grid.FixedCols        := Grade.FixedCols;
      Grid.Options          := Grade.Options;
      //////////////////////////////////////////////////////////////////////////////
      if FMySQLQuery = nil then
      begin
        Geral.MensagemBox('Tabela de referencia n�o foi definida!',
          'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
      Grid.RowCount := FMySQLQuery.RecordCount + 1; // 1 = Titulo
      //
      for i := 1 to Grade.ColCount -1 do
        Grid.ColWidths[i] := Grade.ColWidths[i];
      //
      FMySQLQuery.First;
      while not FMySQLQuery.Eof do
      begin
        for i := 0 to Grid.ColCount -2 do
        begin
          if FDBGrid.Columns[i].FieldName = FTrocavel1 then
            Campo := FTrocador1
          else
            Campo := FDBGrid.Columns[i].FieldName;
          //
          Grid.Cells[i+1, FMySQLQuery.RecNo] := FMySQLQuery.FieldByName(Campo).AsString;
        end;
        FMySQLQuery.Next;
      end;
      for i := 0 to FDBGrid.Columns.Count -1 do
        Grid.Cells[i+1,0] := FDBGrid.Columns[i].Title.Caption;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMyDBGridReorder.BtConfirmaClick(Sender: TObject);

  procedure AtualizaOrdemParaNegativo();
  var
    Controle, Ordem: Integer;
    Qry: TmySQLQuery;
  begin
    Qry := TmySQLQuery.Create(TDataModule(FMySQLQueryUpd.DataBase));
    //
    FMySQLQuery.DisableControls;
    try
      FMySQLQuery.First;
      //
      while not FMySQLQuery.EOF do
      begin
        Ordem    := FMySQLQuery.FieldByName(FCampoAOrdenar).AsInteger * -1;
        Controle := FMySQLQuery.FieldByName(FCampoControle).AsInteger;
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, FMySQLQueryUpd.DataBase, [
          'UPDATE ' + Lowercase(FTabelaAOrdenar) + ' SET ',
          FCampoAOrdenar + '="' + Geral.FF0(Ordem) + '" ',
          'WHERE ' + FCampoControle + '='+ Geral.FF0(Controle),
          '']);
        //
        FMySQLQuery.Next;
      end;
    finally
      FMySQLQuery.EnableControls;
      Qry.Free;
      //
      FMySQLQuery.Close;
      UnDmkDAC_PF.AbreQuery(FMySQLQuery, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    end;
  end;
var
  i, Controle: Integer;
begin
  //Para caso a ordem seja chave prim�ria
  AtualizaOrdemParaNegativo;
  //
  Controle := -1;
  //
  for i := 0 to Grid.ColCount - 1 do
  begin
    if Uppercase(Grid.Cells[i, 0]) = Uppercase(FCampoControle) then
      Controle := i;
  end;
  if Controle = -1 then
  begin
    Geral.MensagemBox('Campo de controle n�o localizado no DBGrid origem!',
      'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  FMySQLQueryUpd.SQL.Clear;
  FMySQLQueryUpd.SQL.Add('UPDATE ' + Lowercase(FTabelaAOrdenar) + ' SET ');
  FMySQLQueryUpd.SQL.Add(FCampoAOrdenar + '=:P0');
  FMySQLQueryUpd.SQL.Add('WHERE ' + FCampoControle + '=:P1');
  FMySQLQueryUpd.SQL.Add(FSQLExtra1);
  for i := 1 to Grid.RowCount-1 do
  begin
    FMySQLQueryUpd.Params[0].AsInteger := i;
    FMySQLQueryUpd.Params[1].AsInteger := Geral.IMV(Grid.Cells[Controle, i]);
    FMySQLQueryUpd.ExecSQL;
  end;
  Close;
end;

procedure TFmMyDBGridReorder.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType   := stLok;
  Grid.ColWidths[0] := 32;
end;

procedure TFmMyDBGridReorder.GridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
begin
  if ACol = 0 then
  begin
    OldAlign              := SetTextAlign(Grid.Canvas.Handle, TA_CENTER);
    Grid.Canvas.Font.Name := 'wingdings';
    Grid.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
      '��');
    SetTextAlign(Grid.Canvas.Handle, OldAlign);
  end;
end;

end.
