unit GetData;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkEdit, dmkGeral,
  dmkEditDateTimePicker, dmkImage, UnDmkEnums, Variants, dmkEditCB, Vcl.DBCtrls,
  dmkDBLookupComboBox, Data.DB, mySQLDbTables;

type
  TFmGetData = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    TPData: TdmkEditDateTimePicker;
    LaHora: TLabel;
    EdHora: TdmkEdit;
    LaMulta: TLabel;
    EdMulta: TdmkEdit;
    LaJuros: TLabel;
    EdJuros: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasTipo: TIntegerField;
    DsCarteiras: TDataSource;
    GBCarteira: TGroupBox;
    Label13: TLabel;
    CBCarteira: TdmkDBLookupComboBox;
    EdCarteira: TdmkEditCB;
    MeInfoDetalhado: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTitulo: String;
    //
    FSelecionou: Boolean;
    FMinData: TDateTime;
    procedure OcultaJurosEMulta();
    procedure Entitula();
  end;

  var
    FmGetData: TFmGetData;

implementation

uses UnMyObjects, UnInternalConsts;

{$R *.DFM}

procedure TFmGetData.BtSaidaClick(Sender: TObject);
begin
  FSelecionou  := False;
  VAR_GETDATA  := 0;
  VAR_CARTEIRA := 0;
  Close;
end;

procedure TFmGetData.Entitula();
var
  Titulo: String;
begin
  if FTitulo <> '' then
    Titulo := '???-?????-999 :: ' + FTitulo
  else
    Titulo := '???-?????-999 :: Informe a Data';
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Titulo, True, taCenter, 2, 10, 20);
end;

procedure TFmGetData.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  MyObjects.CorIniComponente();
end;

procedure TFmGetData.FormResize(Sender: TObject);
begin
  Entitula();
end;

procedure TFmGetData.OcultaJurosEMulta;
begin
  LaMulta.Visible := False;
  LaJuros.Visible := False;
  EdMulta.Visible := False;
  EdJuros.Visible := False;
end;

procedure TFmGetData.FormCreate(Sender: TObject);
begin
  FSelecionou    := False;
  VAR_GETDATA    := 0;
  VAR_CARTEIRA   := 0;
  TPData.MinDate := VAR_DATA_MINIMA;
  //
  MeInfoDetalhado.Visible := False;
  MeInfoDetalhado.text    := '';
  //
  GBCarteira.Visible    := False;
  CBCarteira.ListSource := DsCarteiras;
  QrCarteiras.Close;
end;

procedure TFmGetData.BtOKClick(Sender: TObject);
var
  Carteira: Integer;
begin
  if MyObjects.FIC(((GBCarteira.Visible = True) and
    (EdCarteira.ValueVariant = 0)), EdCarteira, 'Carteira n�o definida!')
  then
    Exit;
  //
  if GBCarteira.Visible = True then
    Carteira := EdCarteira.ValueVariant
  else
    Carteira := 0;
  //
  FSelecionou  := True;
  VAR_GETDATA  := Int(TPData.Date) + EdHora.ValueVariant;
  VAR_CARTEIRA := Carteira;
  Close;
end;

end.
