object FmVerificaConexoes: TFmVerificaConexoes
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Verifica Conex'#245'es'
  ClientHeight = 526
  ClientWidth = 879
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 879
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 831
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 783
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 217
        Height = 32
        Caption = 'Verifica Conex'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 217
        Height = 32
        Caption = 'Verifica Conex'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 217
        Height = 32
        Caption = 'Verifica Conex'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 879
    Height = 364
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 879
      Height = 364
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 879
        Height = 364
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 412
    Width = 879
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 875
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 456
    Width = 879
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 733
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 731
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 879
    Height = 364
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 4
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Verifica conex'#245'es'
      object ImgIP: TImage
        Left = 784
        Top = 140
        Width = 32
        Height = 32
        Transparent = True
      end
      object ImgPorta: TImage
        Left = 784
        Top = 188
        Width = 32
        Height = 32
        Transparent = True
        Visible = False
      end
      object LaIP: TLabel
        Left = 224
        Top = 148
        Width = 550
        Height = 20
        AutoSize = False
        Caption = 'LaIP'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LaPorta: TLabel
        Left = 224
        Top = 196
        Width = 550
        Height = 20
        AutoSize = False
        Caption = 'LaPorta'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object LaDB: TLabel
        Left = 224
        Top = 244
        Width = 550
        Height = 20
        AutoSize = False
        Caption = 'LaDB'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object ImgDB: TImage
        Left = 784
        Top = 236
        Width = 32
        Height = 32
        Transparent = True
      end
      object BtIP: TBitBtn
        Tag = 14
        Left = 8
        Top = 128
        Width = 210
        Height = 40
        Caption = '&IP do servidor'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtIPClick
      end
      object BtPorta: TBitBtn
        Tag = 14
        Left = 8
        Top = 176
        Width = 210
        Height = 40
        Caption = '&Verifica Porta'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = BtPortaClick
      end
      object GroupBox2: TGroupBox
        Left = 8
        Top = 276
        Width = 804
        Height = 50
        Caption = 'Informa'#231#245'es de suporte'
        TabOrder = 2
        object Label1: TLabel
          Left = 10
          Top = 27
          Width = 37
          Height = 13
          Caption = 'Meu IP:'
        end
        object Label2: TLabel
          Left = 163
          Top = 27
          Width = 100
          Height = 13
          Caption = 'Computador servidor:'
        end
        object Label3: TLabel
          Left = 343
          Top = 27
          Width = 99
          Height = 13
          Caption = 'Vers'#227'o do aplicativo:'
        end
        object LaMeuIP: TLabel
          Left = 53
          Top = 27
          Width = 55
          Height = 13
          Caption = '127.0.0.1'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LaServidor: TLabel
          Left = 268
          Top = 27
          Width = 27
          Height = 13
          Caption = 'N'#195'O'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LaVersao: TLabel
          Left = 448
          Top = 27
          Width = 83
          Height = 13
          Caption = '12.12.28.1101'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 570
          Top = 27
          Width = 66
          Height = 13
          Caption = 'Computaddor:'
        end
        object LaComputador: TLabel
          Left = 650
          Top = 27
          Width = 44
          Height = 13
          Caption = 'MEUPC'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object BtDB: TBitBtn
        Tag = 14
        Left = 8
        Top = 224
        Width = 210
        Height = 40
        Caption = '&Conex'#227'o com o banco de dados'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtDBClick
      end
      object RGDadosConexao: TRadioGroup
        Left = 8
        Top = 20
        Width = 849
        Height = 101
        Caption = ' Dados para conex'#227'o: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o definido'
          'Configura'#231#227'o atual PRINCIPAL'
          'Configura'#231#227'o PRINCIPAL da aba "Gerencia conex'#245'es"'
          'Configura'#231#227'o CEP da aba "Gerencia conex'#245'es"')
        TabOrder = 4
        OnClick = RGDadosConexaoClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Gerencia conex'#245'es'
      ImageIndex = 1
      object GroupBox3: TGroupBox
        Left = 4
        Top = 0
        Width = 861
        Height = 281
        Caption = 'Banco de dados'
        TabOrder = 0
        object Label5: TLabel
          Left = 45
          Top = 68
          Width = 3
          Height = 13
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 207
          Height = 264
          Align = alLeft
          ParentBackground = False
          TabOrder = 0
          object LaRegPorta: TLabel
            Left = 80
            Top = 20
            Width = 123
            Height = 13
            Caption = 'Porta do banco de dados:'
          end
          object Label4: TLabel
            Left = 4
            Top = 45
            Width = 196
            Height = 13
            Caption = 'Nome alternativo para o banco de dados:'
          end
          object Label7: TLabel
            Left = 48
            Top = 69
            Width = 153
            Height = 13
            Caption = 'Nome alternativo para o usu'#225'rio:'
          end
          object Label8: TLabel
            Left = 12
            Top = 93
            Width = 187
            Height = 13
            Caption = 'Senha alternativa para o usu'#225'rio acima:'
          end
          object LaRegIPServerAll: TLabel
            Left = 132
            Top = 118
            Width = 68
            Height = 13
            Caption = 'IP do servidor:'
          end
        end
        object Panel6: TPanel
          Left = 209
          Top = 15
          Width = 314
          Height = 264
          Align = alLeft
          TabOrder = 1
          object Label9: TLabel
            Left = 4
            Top = 0
            Width = 140
            Height = 13
            Caption = 'Banco de dados PRINCIPAL:'
          end
          object SbDatabaseApp: TSpeedButton
            Left = 284
            Top = 41
            Width = 23
            Height = 23
            Caption = '...'
            OnClick = SbDatabaseAppClick
          end
          object EdRegPortaApp: TdmkEdit
            Left = 6
            Top = 18
            Width = 300
            Height = 20
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdDatabaseApp: TdmkEdit
            Left = 6
            Top = 42
            Width = 275
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnEnter = EdDatabaseAppEnter
            OnExit = EdDatabaseAppExit
          end
          object EdOthUserApp: TdmkEdit
            Left = 6
            Top = 66
            Width = 300
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnEnter = EdOthUserAppEnter
            OnExit = EdOthUserAppExit
          end
          object EdOthSetConApp: TdmkEdit
            Left = 6
            Top = 90
            Width = 300
            Height = 21
            Font.Charset = SYMBOL_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            PasswordChar = 'l'
            OnEnter = EdOthSetConAppEnter
            OnExit = EdOthSetConAppExit
          end
          object EdRegIPServerApp: TdmkEdit
            Left = 7
            Top = 114
            Width = 300
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object RGRegServerApp: TRadioGroup
            Left = 6
            Top = 140
            Width = 297
            Height = 40
            Caption = 'Tipo de computador'
            Columns = 4
            Items.Strings = (
              'Nenhum'
              'Cliente'
              'Servidor'
              'Ambos')
            TabOrder = 5
            OnClick = RGRegServerAppClick
          end
          object AdvToolBar8: TPanel
            AlignWithMargins = True
            Left = 4
            Top = 184
            Width = 306
            Height = 76
            Align = alBottom
            TabOrder = 6
            object BtBackupApp: TBitBtn
              Left = 24
              Top = 2
              Width = 112
              Height = 46
              Caption = 'Backup'
              Glyph.Data = {
                360C0000424D360C000000000000360400002800000040000000200000000100
                08000000000000080000195C0000195C000000010000000100004E4E4E00C6C4
                C400496EBA0095908F0097B3C30039D7F6006D6D6D00E1E1E100BABABA009999
                9900847F7F0024A7F700D8DFE20078A0EE00A6A6A60039E5F0005D5D5D00597B
                7D00FFFFFF004EA4A90075DBEB00B8D4E600D9D3D100398FC5003BDFE90092DB
                E7005AB8F10055555500827A7A0082C4EC0055D9F200EFEFEF007D787800CDD2
                E40061616100347AFF003D7CF500B1AFAF00797979008E8E8E005DE9F10026A5
                F300A5CEE800ABA094003ED7F50066666600A6DDE8005C5555004B6CB20070BE
                EE0000000000C4CECF00C2C2C20094B2EA003777F3004BABB100746E6E00D7D7
                D70089DCEC00CCCCCC0055E9F200AEAEAE0041B0F40035F0FC00FBFBFB007B94
                9600BFDFE5005B777900B8C6E600655C5A006A65650059504A00615A590068DA
                F000A4A0A100C6DFE40046D8F400B6E8E8003EEBF50075757500EBEBEB00DFDF
                DF0092929200F8F8F800B6B6B600A5BCE800B6E3E300BECCE500868686005959
                59003979F300AEDDE7004E86F300ADC1E70074E7EE005E646B0045EAF400DBDB
                DB0066616100CCCCCC006E9AEF003099D900A2DDE90071778B00515151005681
                8400B3D4E6009E9E9E00AAAAAA00655655007171710082828200E7E7E70038E9
                F4002EB1FD0024AAFB00D2DFE3005CD9F100C2DBE30034F3FF00B3B3B3007975
                7500A3BBE900969696004CD8F3008B8B8B004471CC00BFBFBF00BBCAE6008AE6
                EC00C6DBE40058515100696969005E585800B0E4E700767171009AE6EA0031AB
                F500547A94004480F400C6C6C6005B4A4A00DDDCE200C0D5E500A6E5E8005A62
                6300554E4E0082E7ED0050EAF3005A54540039EBF600CEE0E300928E8E00AEBE
                E7008A8583005650500083A7ED0039EFF700B6B3B300695952007CDBED00A5A2
                A200A1E5E9007D7D7D00625C5C008077760041E7F300726D6D00A9CFE7003D79
                F300645E5E00817D7D0053535E00AAACB400CFD6E3006CDAEF009ADCEA00437F
                F5007EE7ED0048B2F300CAC8C800AAA7A7006862620038E7F100596A6B00B6DE
                E6005FB9F00064E8F00041D8F50094DCEB00B5C4E70064DAF0006A595800D2D2
                D2008782820058EBF30039F3FB005288F3005E474600BFE4E500729DEF002DAA
                F60021AAFE00B1DDE7006E68680048D8F40099959500587E800052A6AC003891
                C800786D6D00C4CEE50041EAF5009E9A9A003A7AF600DBD7D700A2A2A200A29F
                9F00DFDBDB00D1CFCF00B9B7B700AEAAAA005D515100DADFE200D6E3E2006C66
                66007872720062E8F100A8BEE800D2E3E30050D8F3008D89890035AAF300CEE3
                E300D3DAE30034F6FF00A8D2EA00695955004883F4005B4C4700524B4B0058D9
                F20071DAEF0075C0EE008FDCEB009692920096DBE700BBE5E600CADEE40039E7
                EF0074DBEF00AADEE800695D59005E5249005186F3004AEAF400070707070707
                070707070707070707070707C109E1956298B407070707070707070707070707
                070707070707070707070707C109841B2227B407070707070707070707070707
                0707070707070707070739CE46B63820E246B6E73B0707070707070707070707
                07070707070707070707397B842D6E264F2D2D7D3B0707070707070707070707
                07AE805D555555555DAD48A754501212121F010A954A07070707070707070707
                0739DBB48C8C8C8CB43D106E54501212121F8C6F1BD807070707070707AE550D
                C5D6363636363623028303531212121212121212542FF5070707070707618C78
                D87B7B7B7B7B7B096F1B5253121212121212121254597B07070707AEC8243636
                363636363636237EFD9A1212409EC279ABA15012125495B50707073925097B7B
                7B7B7B7B7B7B09581B5812124078584FA3A1501212541BB50707575C36363636
                36363636363636AC47701250A7F02F858592AA6112121C85C107C1D87B7B7B7B
                7B7B7B7B7B7B7B596870125006005959596822611212A310C1075C3636363636
                363636363623302BB4121207A1B6A4AAAAAA95874012DC830907D97B7B7B7B7B
                7B7B7B7B7B09A36BB4121207D82D102222221B4F4012081B0907B13636363636
                363636248B8B671212121212120A2FAAAAAAAA2F0712612F38C1D57B7B7B7B7B
                7B7B7B7B6B6BA31212121212126F592222222259071239596EC1B1363636EE64
                35BE218E0716484A121212129E85A4AAAAAAAA833B12DB95626B6B7B7B7B6B3D
                7FDB3951513910D812121212781022222222221B3B12DB1B2D6B3636C8440707
                0707826AEC0445929840123D9BA4AAAAAAAAAAAA6298A7A4A4877B7B25DB0707
                0707393B3B3D10685240123D1B2222222222222222270610224F9CD30707A8F3
                B3C90B0BCA179FAA9BA7C22FA4AAAAAAAAAAAA95C28792AAAAAADCC10707B425
                6B52272727A3102268065859222222222222221B584F68222222070731890B0B
                0B0B0B0BCAD19FA4CC984648AAAAAAAAAAAA9BB51212CE92A44607073D522727
                27272727276F101006522D10222222222222680E12127B6822848F3E0B0B0B0B
                0B0B0B0BCA654583B412C195AAAAAAAAAA853D121212126C85C2C1D527272727
                272727272758101BB412C11B2222222222593D121212126C10583E0B0B0B0B0B
                0B0B0B0B0B295FDEC1127085A4AAAAAA8520121212121212AB25092727272727
                2727272727272D1BC1127010222222225926121212121212A325890B0B0B0B0B
                0B0B0B0BC9728AEF2512122083AAAAAAAAAAD95112123BA1C207522727272727
                27272727277B4F68251212261B2222222222D95112123BA16F07890B0B0B891A
                1D2A8FEA5151B46D79121251B6922F8585F0CC70121F2F83DC0752272727520E
                548CC1610707B410261212512D68595959000670121F591BDC070B0BBAA80707
                0707E5C74D4D4D418D3D12121FB50A79C2255312122795AB070727270EB40707
                070761C1DBDBDB58683D12121FB56F266F25531212271BA3070731150707F781
                E36096969696969643C6251212121212121212400983A7C107073DDB0707DB08
                6C6B7B7B7B7B7B7B846825121212121212121240091B06C10707070781FF9696
                969696969696963FB711C61C011F12121250088783873B0707070707086B7B7B
                7B7B7B7B7B7B7B7B7B8400A3011F12121250084F1B4FDB070707E5BB96969696
                96969696969696963F3F1385C6C0D2A5D26DC648F56107070707616C7B7B7B7B
                7B7B7B7B7B7B7B7B7B7B265968224F266E1068105261070707075E9696969696
                96969696969696969696EB183769B89191CFD033070707070707257B7B7B7B7B
                7B7B7B7B7B7B7B7B7B7B0952A30622222206A3B4070707070707E39696969696
                969696969696D4D4D4969696EBEB710F717777E00707070707076C7B7B7B7B7B
                7B7B7B7B7B09090909097B0909097B7B7B090961070707070707E39696969628
                9390C7E5070707070707E9F7A2B2C39D969696E50707070707076C7B7B7B7B0E
                548CC16151070707070739C101540E097B7B7B61070707070707949628900707
                07DF4BCB66B0B0B0B02EB9F8070707E0883C96E9070707070707D87BB58C5151
                5151C13B01347F7F348CDB390707070734D87B39070707070707B286070C66AF
                7C0505050505050505050505051EA05B070790E5070707070707543B0707013D
                6B7B7B7B7B7B7B7B7B7B7B7B09D8783B0707B46107070707070707DFFA050505
                050505050505050505050505050505054CF407070707070707070707257B7B7B
                7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B6B7F0707070707070707CB7C05050505
                05050505050505050505050505050505050549070707070707073BD97B7B7B7B
                7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B6C07070707070707BC0505050505
                050505050505050505050505050505050505054B070707070707097B7B7B7B7B
                7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B390707070707075B7C05050505
                050505050505050505050505050505050505BF510707070707073B6B7B7B7B7B
                7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B6C510707070707070797F2050505
                050505050505050505050505050505054C3A070707070707070707393D7B7B7B
                7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B6B080707070707070707070707F8B0F2
                1E4C2C050505050505050505CD75A0FB0C070707070707070707070707393425
                D86B097B7B7B7B7B7B7B7B096B0E54B451070707070707070707070707070707
                07B9F449E6BC05054CF1FAB04207070707070707070707070707070707070707
                073B083DD809090909A12534C107070707070707070707070707}
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtBackupAppClick
            end
            object BtVerifiBDApp: TBitBtn
              Left = 140
              Top = 2
              Width = 112
              Height = 46
              Caption = 'Verifica BD'
              Glyph.Data = {
                360C0000424D360C000000000000360400002800000040000000200000000100
                08000000000000080000195C0000195C0000000100000001000000000000BABA
                BA003976EA00439BA00039D7F60040748E00769FEE0096DDEA0099999900D2DF
                DF0025A5F3005087F3008E8E8E007BDBEE00969696004D5A5C00A4A7A000CCCC
                CC007D7D7D003AD7E1004369A3003AEBF6006874760039DBE5004C6264009EB7
                E90055D7EF008199A50053B6F10027A2ED0066666600BAC8E600BADFE600338B
                BE00C6C6C60071BEEB00AEAEAE004AEAF400409AD000526A6C0054E7EE0085DC
                EC003D7CF5009E9E9E004CB3F200B5D7E8004E5A5500A3CEE8003DD7F500C6D2
                E300D7D7D70055E7EB0062BAF000466978009AB2E70099B5E9006695F00082E7
                ED003FCDD60040B2BA0061838600B2B2B2003BAEF40097A9B300BCC0C1006E6E
                6E0050D8F300535E5E006CE8EF00D4E0E3004F5153007AE7EE006EDAEF0045D8
                F4008787870059595900A6DCE00039AAF3003E6FC80051606200FFFFFF003EEB
                F5003879F6008DC7EB009292920085ACC2006ADAF000ACC3EA00CEE0E300D3C7
                C700AAAAAA0039E7F300A2A2A2009AD5EB00AAE4E80069E8F00031ABF6004882
                F4005A8EF2006FBFEF00D8DCE20083A7ED0035F6FF00DBDBDB0075757500B2D2
                E3007E888A00C9E4E40023A9FB0066DAF000B6B6B60042708800616E6F0065E8
                F000C6DFE4008BA7B600BABEBE003979F300C1CDE500D2D2D20092C9EA0087C5
                EC004C5E620097E5EA005F5F5F0089ABEC00AEBEE70088E6EC003D799A00A2E5
                E9004DD8F300515151004E59590042A5AC005E5E5E004DA3D700B7E4E6006DBE
                EF005CD9F1002BAFFD00CCCCCC00DADFE2005E90F10059B7F100555555005169
                840049636D00717171005E6C6D005CE9F100357AFF00437FF5003190C800A6A6
                A6006A6A6A008A8A8A004D5C5D006F7B7C003EBCC5003D79F300BEE4E50044B1
                F3006C99EF00AADEE800505A580061B6EF0083838300D4D9E3007979790066C2
                F40081DBED009CA3A400BDCBE50051E9F3008EDCEB00CED9E30035F7FF009EE5
                E9008EAEEB0041D8F500A0DDE90028A8F600A4DDE9005F7376005057540036EF
                FB00A9CFE800B2C4E7008EE6EB006592EF00548AF200B1D2E700CCCCCC00A6AC
                AD00C9C9C900D2D7D7004C85F3003DEBF70050555100A6E4E80087A9EC0055D9
                F2004E56560070C4F300A2BAE9004A61690034ACF500B3E4E700AEE5E700D7E2
                E20022ABFF0072E8EF006997F0004F4D4E00328DC200ADDDE70039EBF30024A7
                F700C2C2C200BEBEBE004C5D640049D8F4007CDBED004F5C5900407DF50041B0
                F40039AEF400C4E3E5004581F400BBC9E600C5CFE500B7D4E6003F76920092B0
                EB00555D5D0041EBF3004D5D590061D9F10052616400C9D2E400D2E3E30039EF
                F7006C767600A2B6E7005AD9F20053B8F6004EEAF3005AE9F2008ADCEC004D5D
                550064BBEF00AEC1E7004986F30071C2EF009AE3E7008AE3EB008D8D8D8D8D8D
                8D76E9D4C42A5252E061BE3865CCEF8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D
                8D77DB242B080E0E082B5C5A6E22328D8D8D8D8D8D8D8D8D8D8D8D8D8D8DBB7D
                38C42A52525252525252525252E0C438B2AC8D8D8D8D406AAB678D8D8D8D1101
                5A2B080E0E0E0E0E0E0E0E0E0E082B5ADB778D8D8D8DDBA62B678D8D65E45252
                525252525252525252525252525252525252BE378D114F9C9C168D8D012B0E0E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E5CDA8D114B909041EF8E52525252
                525252525252525252525252525252525252525238F29C9C9C7032990E0E0E0E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E5A939090901E625252525252
                5252525252525252525252525252525252525296142E9C9C9D32990E0E0E0E0E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E08939090906832975252525252
                525252525252525252525252525252525252964E2E9C9C4F118D080E0E0E0E0E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E08A69090904B118D975252525252
                0BA2C819FB1F76767676E5BBCCB20662E05202DCDFDFDFC18D8D080E0E0E0E0E
                5C2401DAC27777777777771122DB3D990808544B9090905A8D8D2A52523819EF
                8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D8DA757912E9C9C6A8D8D8D080E0E5ADA32
                8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D67119A909090A68D8D8D9706EF8D8D8D
                BF79FA2CE1E2E2E2E2E2A12C8F63532D8D10DFDFDF94678D8D8D083D328D8D8D
                11015A2B080E0E0E0E08082B9924DB118D5C9090901E678D8D8DA78D8D788F60
                D9D9D9D9D9D9D9D9D9D9D9D9D2D21DD926A49C9C43DA8D8D8D8D678D8DDB5C54
                0C0C0C0C0C0C0C0C0C0C0C0C0C0C9B9B4A90909090DA8D8D8D8D8D2FCED9D9D9
                D9D9D9D9D9D9D9D9D9D9D9D2D635DC92920F0F0F1B8D8D8D8D8D8D220E0C0C0C
                0C0C0C0C0C0C0C0C0C0C0C0CA87C4B4B4B909090548D8D8D8D8D78B5D9D9D9D9
                D9D9D9D9D9D9D9D9D9D9D221C6C60F0F0F0F0F058BBF8D8D8D8DDB0C0C0C0C0C
                0C0C0C0C0C0C0C0C0C0C0CA8838390909090901E0E118D8D8D8DCED9D9D9D9D9
                D9D9D9D9D9D9D9D9D9D9D992849CA4A49C9CB805D2FD8D8D8D8D0E0C0C0C0C0C
                0C0C0C0C0C0C0C0C0C0C0C4B909090909090901E0C3D8D8D8D8DCED9D9D9D9D9
                D9D9D9CEE12C1C1C1C1C870F0F0F6FE87A0F0FC698CB8D8D8D8D0E0C0C0C0C0C
                0C0C0C0E082B5C5C5C5C549090901E1E4B909090123D8D8D8D8D60D9D9D9CE34
                53BFAF8D8D8D8D8D8D8D590F0F0F735D554343B880CB8D8D8D8D540C0C0C0E5A
                0111328D8D8D8D8D8D8D119090902BDA5C4B90909A3D8D8D8D8DD9D934BA8D8D
                8D8DF0A0D0C7C7C7C7C74C27D59CC28D24EE9CB880A98D8D8D8D0C0C99C28D8D
                8D8D67771122222222C2DA86D54B118D244B90909A248D8D8D8D23E78D8D6BB1
                D3F61515151515151515B913853A28B70F0F0FCA3F2D8D8D8D8D24118D8D32DA
                242B0E0E0E0E0E0E0E0E0E0C689B2B9A909090835C118D8D8D8D8D8D81F71515
                1515151515151515151515B9B0B09E469C9C463C8D8D8D8D8D8D8D8D22990E0E
                0E0E0E0E0E0E0E0E0E0E0E080808A683909083688D8D8D8D8D8D8D4715151515
                15151515151515151515151515B91718D50F03F15FD18D8D8D8D8D3D0E0E0E0E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E0C4BD590930824678D8D8D8D7F1515151515
                1515151515151515151515151515B9133B1366B9B9398D8D8D8D010E0E0E0E0E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E080C120C08080E018D8D8D8D711515151515
                151515151515151515151515151515B966B9151515448D8D8D8D5A0E0E0E0E0E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0808080E0E0E248D8D8D8D711515151515
                15F744397B81D0D0D0D0D0B1BC4771251515151515448D8D8D8D5A0E0E0E0E0E
                085C2401DA22C2111111C2DA013D5A2B0E0E0E0E0E248D8D8D8D711515F63988
                8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D6BB15F15155F8D8D8D8D5A0E0E5C0111
                8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D32DA24080E248D8D8D8DAD95D08D8D8D
                D7F848F482493030303030DDC96DAAB4728D8DF039D38D8D8D8D5C99118D8D67
                C20124992B2B08080808082B5C5A6E22328D8D676E248D8D8D8DE38D8D07F404
                040404040404040404040404040404040482AA728D8D8D8D8D8D778D8DDA990E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E2B6E328D8D8D8D8D8D8DA349040404
                0404040404040404040404040404040404040404DE8D8D8D8D8D8DC22B0E0E0E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E6E8D8D8D8D8D290404040404
                040404040404040404040404040404040404040404DE8D8D8D8D010E0E0E0E0E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E6E8D8D8D8D820404040404
                0404040404040404040404040404040404040404048A8D8D8D8D2B0E0E0E0E0E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E998D8D8D8D585604040404
                0404040404040404040404040404040404040404B3208D8D8D8D32240E0E0E0E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E08778D8D8D8D8D8D07420404
                040404040404040404040404040404040404B30D588D8D8D8D8D8D8DDA5C0E0E
                0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E083D328D8D8D8D8D8D8D8D8D20AE
                56424930040404040404040404B382EDAAA3458D8D8D8D8D8D8D8D8D8D8D7701
                245C2B080E0E0E0E0E0E0E0E0E082B5A6EC2678D8D8D8D8D8D8D8D8D8D8D8D8D
                8D45D7AE48C9B304040482EDAAB6728D8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D
                8D671101245C08080E082B5A6E22328D8D8D8D8D8D8D8D8D8D8D}
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtVerifiBDAppClick
            end
            object Panel24: TPanel
              Left = 1
              Top = 51
              Width = 304
              Height = 24
              Align = alBottom
              Caption = 'Ferramentas'
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 2
            end
          end
        end
        object Panel7: TPanel
          Left = 523
          Top = 15
          Width = 336
          Height = 264
          Align = alLeft
          TabOrder = 2
          object Label10: TLabel
            Left = 4
            Top = 0
            Width = 172
            Height = 13
            Caption = 'Banco de dados de consulta a CEP:'
          end
          object EdRegPortaCEP: TdmkEdit
            Left = 6
            Top = 18
            Width = 300
            Height = 20
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdDatabaseCEP: TdmkEdit
            Left = 6
            Top = 42
            Width = 300
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnEnter = EdDatabaseAppEnter
            OnExit = EdDatabaseAppExit
          end
          object EdOthUserCEP: TdmkEdit
            Left = 6
            Top = 66
            Width = 300
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnEnter = EdOthUserAppEnter
            OnExit = EdOthUserAppExit
          end
          object EdOthSetConCEP: TdmkEdit
            Left = 6
            Top = 90
            Width = 300
            Height = 21
            Font.Charset = SYMBOL_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            PasswordChar = 'l'
            OnEnter = EdOthSetConAppEnter
            OnExit = EdOthSetConAppExit
          end
          object EdRegIPServerCEP: TdmkEdit
            Left = 7
            Top = 114
            Width = 300
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object RGRegServerCEP: TRadioGroup
            Left = 8
            Top = 140
            Width = 297
            Height = 40
            Caption = 'Tipo de computador'
            Columns = 4
            Items.Strings = (
              'Nenhum'
              'Cliente'
              'Servidor'
              'Ambos')
            TabOrder = 5
            OnClick = RGRegServerAppClick
          end
        end
      end
      object BtSalvar: TBitBtn
        Tag = 24
        Left = 8
        Top = 289
        Width = 120
        Height = 44
        Cursor = crHandPoint
        Caption = '&Salvar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtSalvarClick
      end
      object BtAuto: TBitBtn
        Tag = 1000025
        Left = 300
        Top = 289
        Width = 120
        Height = 44
        Cursor = crHandPoint
        Caption = '&Auto'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAutoClick
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Porta'
      ImageIndex = 2
      object MePorta: TMemo
        Left = 0
        Top = 0
        Width = 871
        Height = 336
        Align = alClient
        Lines.Strings = (
          'MePorta')
        TabOrder = 0
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Observa'#231#245'es'
      ImageIndex = 3
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 871
        Height = 336
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 220
        ExplicitTop = 88
        ExplicitWidth = 185
        ExplicitHeight = 89
      end
    end
  end
  object MyDB: TMySQLDatabase
    DatabaseName = 'dcontrol'
    DesignOptions = []
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=dcontrol')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 584
    Top = 215
  end
  object IdAntiFreeze1: TIdAntiFreeze
    Left = 692
    Top = 88
  end
  object DCP_twofish1: TDCP_twofish
    Id = 6
    Algorithm = 'Twofish'
    MaxKeySize = 256
    BlockSize = 128
    Left = 596
    Top = 137
  end
  object QrUpd: TMySQLQuery
    Database = MyDB
    Left = 584
    Top = 268
  end
  object PMVerificaBD: TPopupMenu
    Left = 516
    Top = 368
    object VerificaBDServidor1: TMenuItem
      Caption = 'Verifica BD &Servidor'
      OnClick = VerificaBDServidor1Click
    end
    object VerificaTabelasPublicas1: TMenuItem
      Caption = 'Verifica BD P'#250'blicas'
      OnClick = VerificaTabelasPublicas1Click
    end
  end
  object PMDatabaseApp: TPopupMenu
    OnPopup = PMDatabaseAppPopup
    Left = 373
    Top = 135
    object Verificarseexiste1: TMenuItem
      Caption = '&Verificar se existe'
      OnClick = Verificarseexiste1Click
    end
    object Listarexistentes1: TMenuItem
      Caption = '&Listar bancos de dados existentes'
      OnClick = Listarexistentes1Click
    end
  end
end
