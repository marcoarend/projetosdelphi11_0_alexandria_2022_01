unit SelRadioGroup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmSelRadioGroup = class(TForm)
    RGItens: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGItensClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FSelOnClick: Boolean;
    FAvisos: array of String;
    FAcoes: array of TSQLType;
    FValores: array of String; 
    FMyCods: array of Integer; 
  end;

  var
  FmSelRadioGroup: TFmSelRadioGroup;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmSelRadioGroup.BtOKClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSelRadioGroup.BtSaidaClick(Sender: TObject);
begin
  RGItens.ItemIndex := -1;
  Close;
end;

procedure TFmSelRadioGroup.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmSelRadioGroup.FormCreate(Sender: TObject);
begin
  FSelOnClick := False;
end;

procedure TFmSelRadioGroup.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [(*LaAviso1, LaAviso2*)], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSelRadioGroup.RGItensClick(Sender: TObject);
begin
  if FSelOnClick then
    BtOKClick(Self)
  else
    BtOk.Enabled := RGItens.ItemIndex > -1;
end;

end.
