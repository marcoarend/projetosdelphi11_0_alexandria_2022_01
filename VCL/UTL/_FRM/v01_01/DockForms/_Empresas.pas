unit _Empresas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB,
  DBGrids, dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, DmkDAC_PF;

type
  THackDBGrid = class(TDBGrid);
  TFm_Empresas = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    GroupBox1: TGroupBox;
    EdPesq: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisar(Todos: Boolean = False);
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    FFecha: Boolean;
    class function CreateDockForm(): TCustomForm;
  end;

var
  Fm_Empresas: TFm_Empresas;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule(*, TabePrcCab, PediVdaImp, ModPediVda*),
UnInternalConsts, UCreate, MyGlyfs, APagRec, Principal, MovFin;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFm_Empresas.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFm_Empresas.AtivaItens(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'UPDATE ' + TAB_TMP_EMPRESAS,
  'SET Ativo=' + FormatFloat('0', Status)]);
  DModG.Tb_Empresas.Refresh;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFm_Empresas.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DModG.Tb_Empresas.First;
  while not DModG.Tb_Empresas.Eof do
  begin
    if DModG.Tb_EmpresasAtivo.Value <> Status then
    begin
      DModG.Tb_Empresas.Edit;
      DModG.Tb_EmpresasAtivo.Value := Status;
      DModG.Tb_Empresas.Post;
    end;
    DModG.Tb_Empresas.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFm_Empresas.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFm_Empresas.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFm_Empresas.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFm_Empresas.CreateDockForm(): TCustomForm;
begin
  Result := TFm_Empresas.Create(Application);
  FmMyGlyfs.ConfiguraFormDock(Result);
  Result.Show;
end;

procedure TFm_Empresas.FormActivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl1.ActivePageIndex := 1;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFm_Empresas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'DROP TABLE ' + TAB_TMP_EMPRESAS + '; ']);
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFm_Empresas.FormShow(Sender: TObject);
begin
  Pesquisar(True);
  //
{$IfDef cSkinRank}
  if FmPrincipal.sd1.Active then
    FmPrincipal.sd1.skinform(Handle);
{$Else}
   // TODO AlphaSkin
{$EndIf}
end;

procedure TFm_Empresas.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
end;


procedure TFm_Empresas.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFm_Empresas.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFm_Empresas.Pesquisar(Todos: Boolean = False);
begin
  Screen.Cursor := crHourGlass;
  //
  if (Trim(EdPesq.Text) <> '') or (Todos = True) then
  begin
    DModG.Tb_Empresas.Filter   := 'Nome LIKE ''%' + EdPesq.Text + '%''';
    DModG.Tb_Empresas.FilterOptions := [foCaseInsensitive, foNoPartialCompare];
    DModG.Tb_Empresas.Filtered := True;
  end else
    DModG.Tb_Empresas.Filtered := False;
  //
  Screen.Cursor := crDefault;
end;

procedure TFm_Empresas.RGSelecaoClick(Sender: TObject);
begin
  DModG.Tb_Empresas.Filtered := False;
  //
  case RGSelecao.ItemIndex of
    0: DModG.Tb_Empresas.Filter := 'Ativo=0';
    1: DModG.Tb_Empresas.Filter := 'Ativo=1';
    2: DModG.Tb_Empresas.Filter := '';
  end;
  //
  if RGSelecao.ItemIndex < 2 then
    DModG.Tb_Empresas.Filtered := True;
end;

procedure TFm_Empresas.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DModG.Tb_Empresas.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DModG.Tb_Empresas.Edit;
    DModG.Tb_Empresas.FieldByName('Ativo').Value := Status;
    DModG.Tb_Empresas.Post;
  end;
  HabilitaItemUnico();
end;

procedure TFm_Empresas.HabilitaItemUnico();
var
  Habilita: Boolean;
  Qry: TmySQLQuery;
begin
  Qry:= TmySQLQuery.Create(DmodG);
  //Qry.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
  'SELECT Count(*)  Itens',
  'FROM ' + TAB_TMP_EMPRESAS,
  'WHERE Ativo=1']);
  Habilita :=  Qry.FieldByName('Itens').AsInteger = 0;
  //
  if FindWindow('TFmAPagRec', nil) > 0 then
  begin
    FmAPagRec.LaEmpresa.Enabled := Habilita;
    FmAPagRec.EdEmpresa.Enabled := Habilita;
    FmAPagRec.CBEmpresa.Enabled := Habilita;
    if not Habilita then
    begin
      FmAPagRec.EdEmpresa.ValueVariant := 0;
      FmAPagRec.CBEmpresa.KeyValue     := 0;
    end;
  end;
  if FindWindow('TFmMovFin', nil) > 0 then
  begin
    FmMovFin.LaEmpresa.Enabled := Habilita;
    FmMovFin.EdEmpresa.Enabled := Habilita;
    FmMovFin.CBEmpresa.Enabled := Habilita;
    if not Habilita then
    begin
      FmMovFin.EdEmpresa.ValueVariant := 0;
      FmMovFin.CBEmpresa.KeyValue     := 0;
    end;
  end;
end;


procedure TFm_Empresas.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFm_Empresas.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFm_Empresas.FormCreate(Sender: TObject);
begin
  DBGrid1.DataSource := DModG.Ds_Empresas;
  DBGrid2.DataSource := DModG.Ds_Empresas;
  //
  RGSelecao.ItemIndex          := 2;
  PageControl1.ActivePageIndex := 0;
  PageControl1.ActivePageIndex := 1;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFm_Empresas.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmAPagRec', nil) > 0 then
    FmAPagRec.AtivaBtConfirma();
  //
{
  if FindWindow('TFmMovFin', nil) > 0 then
    FmMovFin.AtivaBtConfirma();
}
end;

end.

