object Fm_Indi_Pag: TFm_Indi_Pag
  Left = 339
  Top = 185
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Indica'#231#245'es'
  ClientHeight = 375
  ClientWidth = 372
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  OnShow = FormShow
  OnStartDock = FormStartDock
  OnUnDock = FormUnDock
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 372
    Height = 375
    Align = alClient
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 326
      Width = 370
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 271
        Top = 4
        Width = 90
        Height = 40
        Hint = 'Desmarca todos itens'
        Caption = '&Nenhum'
        TabOrder = 0
        OnClick = BtNenhumClick
        NumGlyphs = 2
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 7
        Top = 4
        Width = 90
        Height = 40
        Hint = 'Marca todos itens'
        Caption = '&Todos'
        TabOrder = 1
        OnClick = BtTodosClick
        NumGlyphs = 2
      end
      object BtPesq: TBitBtn
        Tag = 241
        Left = 123
        Top = 4
        Width = 120
        Height = 40
        Hint = 'Marca todos itens'
        Caption = '&Pesquisados'
        TabOrder = 2
        OnClick = BtPesqClick
        NumGlyphs = 2
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 370
      Height = 325
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = ' Pesquisa '
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 362
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 362
            Height = 48
            Align = alClient
            Caption = ' Informe parte da descri'#231#227'o para pesquisar: '
            TabOrder = 0
            object EdPesq: TEdit
              Left = 8
              Top = 20
              Width = 325
              Height = 21
              TabOrder = 0
              OnChange = EdPesqChange
            end
          end
        end
        object DBGrid1: TdmkDBGrid
          Left = 0
          Top = 48
          Width = 362
          Height = 249
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'OK'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 240
              Visible = True
            end>
          Color = clWindow
          DataSource = DModG.Ds_Empresas
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGrid1CellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'OK'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 240
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Selecionados '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 362
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object RGSelecao: TRadioGroup
            Left = 0
            Top = 0
            Width = 362
            Height = 48
            Align = alClient
            Caption = ' Status dos itens: '
            Columns = 3
            Items.Strings = (
              'N'#227'o selecionados'
              'Selecinodos'
              'Todos')
            TabOrder = 0
            OnClick = RGSelecaoClick
            ExplicitLeft = 1
            ExplicitTop = 1
            ExplicitWidth = 360
            ExplicitHeight = 46
          end
        end
        object DBGrid2: TdmkDBGrid
          Left = 0
          Top = 48
          Width = 362
          Height = 249
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'OK'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 240
              Visible = True
            end>
          Color = clWindow
          DataSource = DModG.Ds_Empresas
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGrid1CellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'OK'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 240
              Visible = True
            end>
        end
      end
    end
  end
  object PMPesq: TPopupMenu
    Left = 172
    Top = 316
    object Ativa1: TMenuItem
      Caption = '&Ativa'
      OnClick = Ativa1Click
    end
    object Desativa1: TMenuItem
      Caption = '&Desativa'
      OnClick = Desativa1Click
    end
  end
end
