unit _Indi_Pags;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB,
  DBGrids, dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, DmkDAC_PF;

type
  THackDBGrid = class(TDBGrid);
  TFm_Indi_Pags = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    RGSelecao: TRadioGroup;
    DBGrid2: TdmkDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    FFecha: Boolean;
    class function CreateDockForm(): TCustomForm;
  end;

var
  Fm_Indi_Pags: TFm_Indi_Pags;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule(*, TabePrcCab, PediVdaImp, ModPediVda*),
UnInternalConsts, UCreate, MyGlyfs, APagRec, Principal;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFm_Indi_Pags.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFm_Indi_Pags.AtivaItens(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'UPDATE ' + TAB_TMP_INDIPAGS,
  'SET Ativo=' + FormatFloat('0', Status)]);
  DModG.Tb_Indi_Pags.Refresh;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFm_Indi_Pags.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DModG.Tb_Indi_Pags.First;
  while not DModG.Tb_Indi_Pags.Eof do
  begin
    if DModG.Tb_Indi_PagsAtivo.Value <> Status then
    begin
      DModG.Tb_Indi_Pags.Edit;
      DModG.Tb_Indi_PagsAtivo.Value := Status;
      DModG.Tb_Indi_Pags.Post;
    end;
    DModG.Tb_Indi_Pags.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFm_Indi_Pags.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFm_Indi_Pags.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFm_Indi_Pags.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFm_Indi_Pags.CreateDockForm(): TCustomForm;
begin
  Result := TFm_Indi_Pags.Create(Application);
  FmMyGlyfs.ConfiguraFormDock(Result);
  Result.Show;
end;

procedure TFm_Indi_Pags.FormActivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl1.ActivePageIndex := 1;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFm_Indi_Pags.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'DROP TABLE ' + TAB_TMP_INDIPAGS + '; ']);
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFm_Indi_Pags.FormShow(Sender: TObject);
begin
{$IfDef cSkinRank}
  if FmPrincipal.sd1.Active then
    FmPrincipal.sd1.skinform(Handle);
{$Else}
   // TODO AlphaSkin
{$EndIf}
end;

procedure TFm_Indi_Pags.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
end;


procedure TFm_Indi_Pags.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFm_Indi_Pags.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFm_Indi_Pags.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  if Trim(EdPesq.Text) <> '' then
  begin
    DModG.Tb_Indi_Pags.Filter   := 'Nome LIKE ''%' + EdPesq.Text + '%''';
    DModG.Tb_Indi_Pags.Filtered := True;
  end else
    DModG.Tb_Indi_Pags.Filtered := False;
  //
  Screen.Cursor := crDefault;
end;

procedure TFm_Indi_Pags.RGSelecaoClick(Sender: TObject);
begin
  DModG.Tb_Indi_Pags.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DModG.Tb_Indi_Pags.Filter := 'Ativo=0';
    1: DModG.Tb_Indi_Pags.Filter := 'Ativo=1';
    2: DModG.Tb_Indi_Pags.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DModG.Tb_Indi_Pags.Filtered := True;
end;

procedure TFm_Indi_Pags.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DModG.Tb_Indi_Pags.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DModG.Tb_Indi_Pags.Edit;
    DModG.Tb_Indi_Pags.FieldByName('Ativo').Value := Status;
    DModG.Tb_Indi_Pags.Post;
  end;
  HabilitaItemUnico();
end;

procedure TFm_Indi_Pags.HabilitaItemUnico();
var
  Habilita: Boolean;
  Qry: TmySQLQuery;
begin
  Qry:= TmySQLQuery.Create(DmodG);
  //Qry.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
  'SELECT Count(*)  Itens',
  'FROM ' + TAB_TMP_INDIPAGS,
  'WHERE Ativo=1']);
  Habilita :=  Qry.FieldByName('Itens').AsInteger = 0;
  //
  if FindWindow('TFmAPagRec', nil) > 0 then
  begin
    FmAPagRec.LaIndiPag.Enabled := Habilita;
    FmAPagRec.EdIndiPag.Enabled := Habilita;
    FmAPagRec.CBIndiPag.Enabled := Habilita;
    if not Habilita then
    begin
      FmAPagRec.EdIndiPag.ValueVariant := 0;
      FmAPagRec.CBIndiPag.KeyValue     := 0;
    end;
  end;
end;

procedure TFm_Indi_Pags.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFm_Indi_Pags.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFm_Indi_Pags.FormCreate(Sender: TObject);
begin
  DBGrid1.DataSource := DModG.Ds_Indi_Pags;
  DBGrid2.DataSource := DModG.Ds_Indi_Pags;
  //
  RGSelecao.ItemIndex := 2;
  PageControl1.ActivePageIndex := 0;
  PageControl1.ActivePageIndex := 1;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFm_Indi_Pags.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmAPagRec', nil) > 0 then
    FmAPAgRec.AtivaBtConfirma();
end;

end.

