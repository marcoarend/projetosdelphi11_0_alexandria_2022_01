object Fm_Pla_Ctas: TFm_Pla_Ctas
  Left = 339
  Top = 185
  Caption = 'Plano de Contas'
  ClientHeight = 375
  ClientWidth = 372
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  OnShow = FormShow
  OnStartDock = FormStartDock
  OnUnDock = FormUnDock
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 372
    Height = 375
    Align = alClient
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 326
      Width = 370
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 271
        Top = 4
        Width = 90
        Height = 40
        Hint = 'Desmarca todos itens'
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtNenhumClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 7
        Top = 4
        Width = 90
        Height = 40
        Hint = 'Marca todos itens'
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTodosClick
      end
    end
    object TVPlano: TTreeView
      Left = 1
      Top = 1
      Width = 370
      Height = 284
      Align = alClient
      Indent = 19
      MultiSelect = True
      TabOrder = 1
      OnClick = TVPlanoClick
      OnCollapsing = TVPlanoCollapsing
      OnKeyDown = TVPlanoKeyDown
      Items.NodeData = {
        03010000001E0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000
        00000000000100}
    end
    object Panel5: TPanel
      Left = 1
      Top = 285
      Width = 370
      Height = 41
      Align = alBottom
      TabOrder = 2
      object dmkEdit1: TdmkEdit
        Left = 84
        Top = 12
        Width = 29
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit2: TdmkEdit
        Left = 116
        Top = 12
        Width = 29
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object QrPlanos: TMySQLQuery
   
    SQL.Strings = (
      'SELECT * FROM _pla_ctas_'
      'WHERE cN1 >0'
      'OR cN2>0 OR cN3>0 or cN4>0 OR cN5>0'
      'ORDER BY cN5, cN4, cN3, cN2, cN1')
    Left = 188
    Top = 4
    object QrPlanosNivel: TSmallintField
      FieldName = 'Nivel'
    end
    object QrPlanoscN5: TIntegerField
      FieldName = 'cN5'
    end
    object QrPlanosnN5: TWideStringField
      FieldName = 'nN5'
      Size = 50
    end
    object QrPlanoscN4: TIntegerField
      FieldName = 'cN4'
    end
    object QrPlanosnN4: TWideStringField
      FieldName = 'nN4'
      Size = 50
    end
    object QrPlanoscN3: TIntegerField
      FieldName = 'cN3'
    end
    object QrPlanosnN3: TWideStringField
      FieldName = 'nN3'
      Size = 50
    end
    object QrPlanoscN2: TIntegerField
      FieldName = 'cN2'
    end
    object QrPlanosnN2: TWideStringField
      FieldName = 'nN2'
      Size = 50
    end
    object QrPlanoscN1: TIntegerField
      FieldName = 'cN1'
    end
    object QrPlanosnN1: TWideStringField
      FieldName = 'nN1'
      Size = 50
    end
    object QrPlanosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
end
