unit _SPED_EFD_REG;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DB, ABSMain, dmkGeral,
  DBGrids, dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, ImgList, dmkEdit;

type
  THackDBGrid = class(TDBGrid);
  TFm_SPED_EFD_REG = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    ImageList1: TImageList;
    QrBlcs: TmySQLQuery;
    QrBlcsNivel: TSmallintField;
    QrBlcscN5: TIntegerField;
    QrBlcsnN5: TWideStringField;
    QrBlcscN4: TIntegerField;
    QrBlcsnN4: TWideStringField;
    QrBlcscN3: TIntegerField;
    QrBlcsnN3: TWideStringField;
    QrBlcscN2: TIntegerField;
    QrBlcsnN2: TWideStringField;
    QrBlcscN1: TIntegerField;
    QrBlcsnN1: TWideStringField;
    QrBlcsAtivo: TSmallintField;
    TVPlano: TTreeView;
    Panel5: TPanel;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure TVPlanoClick(Sender: TObject);
    procedure TVPlanoCollapsing(Sender: TObject; Node: TTreeNode;
      var AllowCollapse: Boolean);
    procedure TVPlanoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    F_SPED_EFD_REG: String;
    procedure AtivaItens(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    FFecha: Boolean;
    class function CreateDockForm(): TCustomForm;
  end;

var
  Fm_SPED_EFD_REG: TFm_SPED_EFD_REG;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, UnInternalConsts, UCreate, MyGlyfs,
SPED_EFD_Exporta, UnMyObjects, Principal, ModuleFin;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFm_SPED_EFD_REG.AtivaItens(Status: Integer);
var
  I: Integer;
  Noh: TTreeNode;
begin
  Screen.Cursor := crHourGlass;
  for I := 0 to TVPlano.Items.Count -1 do
  begin
    Noh := TVPlano.Items[I];
    Noh.StateIndex := Status + Integer(cCBNot);
  end;
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'UPDATE ' + F_SPED_EFD_REG,
  'SET Ativo=' + FormatFloat('0', Status)]);
  DModG.Tb_Indi_Pags.Refresh;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFm_SPED_EFD_REG.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFm_SPED_EFD_REG.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFm_SPED_EFD_REG.CreateDockForm(): TCustomForm;
begin
  Result := TFm_SPED_EFD_REG.Create(Application);
  FmMyGlyfs.ConfiguraFormDock(Result);
  Result.Show;
end;

procedure TFm_SPED_EFD_REG.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'DROP TABLE ' + F_SPED_EFD_REG + '; ']);
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFm_SPED_EFD_REG.FormShow(Sender: TObject);
begin
  if FmPrincipal.sd1.Active then
    FmPrincipal.sd1.skinform(Handle);
end;

procedure TFm_SPED_EFD_REG.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
end;


procedure TFm_SPED_EFD_REG.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFm_SPED_EFD_REG.TVPlanoClick(Sender: TObject);
var
  P: TPoint;
  Nivel, Codigo, Ativa: Integer;
  cNx: String;
begin
  GetCursorPos(P);
  P := TVPlano.ScreenToClient(P);
  if (htOnStateIcon in TVPlano.GetHitTestInfoAt(P.X,P.Y)) then
  begin
    if TVPlano.Selected.StateIndex = 2 then
      Ativa := 1
    else
      Ativa := 0;
    //
    Nivel := 5 - MyObjects.ToggleTreeViewCheckBoxes_Child(TVPlano.Selected);
    Codigo := TVPlano.Selected.ImageIndex;
    dmkEdit1.ValueVariant := Nivel;
    dmkEdit2.ValueVariant := Codigo;
    //
    //Atualizar Tb__SPED_EFD_REG
    cNx := FormatFloat('0', Nivel);
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE _SPED_EFD_REG_ SET ');
    DModG.QrUpdPID1.SQL.Add('Ativo=' + FormatFloat('0', Ativa));
    DModG.QrUpdPID1.SQL.Add('WHERE Nivel<=' + FormatFloat('0', Nivel));
    DModG.QrUpdPID1.SQL.Add('AND cn' + cNx + '=' + FormatFloat('0', Codigo));
    DModG.QrUpdPID1.ExecSQL;
    //
    HabilitaItemUnico();
  end;
end;

procedure TFm_SPED_EFD_REG.TVPlanoCollapsing(Sender: TObject; Node: TTreeNode;
  var AllowCollapse: Boolean);
begin
  //AllowCollapse := False;
end;

procedure TFm_SPED_EFD_REG.TVPlanoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_SPACE) and Assigned(TVPlano.Selected) then
    MyObjects.ToggleTreeViewCheckBoxes_Child(TVPlano.Selected);
end;

procedure TFm_SPED_EFD_REG.HabilitaItemUnico();
var
  Habilita: Boolean;
  Qry: TmySQLQuery;
begin
  Qry:= TmySQLQuery.Create(DmodG);
  Qry.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery1(Qry, [
  'SELECT Count(*)  Itens',
  'FROM ' + F_SPED_EFD_REG,
  'WHERE Ativo=1']);
  Habilita :=  Qry.FieldByName('Itens').AsInteger = 0;
  //
{
  if FindWindow('TFmAPagRec', nil) > 0 then
  begin
    FmAPagRec.RGNivelSel.Enabled := Habilita;
    FmAPagRec.LaNivelSel.Enabled := Habilita;
    FmAPagRec.EdNivelSel.Enabled := Habilita;
    FmAPagRec.CBNivelSel.Enabled := Habilita;
    if not Habilita then
    begin
      FmAPagRec.RGNivelSel.ItemIndex    := 0;
      FmAPagRec.EdNivelSel.ValueVariant := 0;
      FmAPagRec.CBNivelSel.KeyValue     := 0;
    end;
  end;
}
end;


procedure TFm_SPED_EFD_REG.FormCreate(Sender: TObject);
var
  Idx1, Idx2, Idx3, Idx4, Idx5, Idx6: Integer;
  function CriaNoh(const Noh: TTreeNode; const nN: String; const cN: Integer): TTreeNode;
  begin
    if Noh = nil then
      Result := TvPlano.Items.Add(Noh, nN)
    else
      Result := TvPlano.Items.AddChild(Noh, nN);
    Result.ImageIndex := cN;
    Result.StateIndex := QrPlanosAtivo.Value + Integer(cCBNot);
  end;
var
  Noh, Noh1, Noh2, Noh3, Noh4, Noh5: TTreeNode;
  nN: String;
  cN: Integer;
begin
  TVPlano.Items.Clear;
  QrPlanos.Close;
  QrPlanos.Database := DmodG.MyPID_DB;
  QrPlanos.Open;
  Idx1 := 0;
  Idx2 := 0;
  Idx3 := 0;
  Idx4 := 0;
  Idx5 := 0;
  Noh5 := CriaNoh( nil, '(N�o definido)', 0);
  Noh4 := CriaNoh(Noh5, '(N�o definido)', 0);
  Noh3 := CriaNoh(Noh4, '(N�o definido)', 0);
  Noh2 := CriaNoh(Noh3, '(N�o definido)', 0);
  Noh1 := CriaNoh(Noh2, '(N�o definido)', 0);
  while not QrPlanos.Eof do
  begin
    nN := QrPlanos.FieldByName(
      'nN' + FormatFloat('0', QrPlanosNivel.Value)).AsString;
    cN := QrPlanos.FieldByName(
      'cN' + FormatFloat('0', QrPlanosNivel.Value)).AsInteger;
    case QrPlanosNivel.Value of
      1: Noh1 := CriaNoh(Noh2, nN, cN);
      2: Noh2 := CriaNoh(Noh3, nN, cN);
      3: Noh3 := CriaNoh(Noh4, nN, cN);
      4: Noh4 := CriaNoh(Noh5, nN, cN);
      5: Noh5 := CriaNoh( nil, nN, cN);
    end;
    QrPlanos.Next;
  end;
  //
  //
{
  F_SPED_EFD_REG := FmAPagRec.F_SPED_EFD_REG;
}
end;

procedure TFm_SPED_EFD_REG.FormDeactivate(Sender: TObject);
begin
{
  if FindWindow('TFmAPagRec', nil) > 0 then
    FmAPagRec.AtivaBtConfirma();
}
end;

end.

