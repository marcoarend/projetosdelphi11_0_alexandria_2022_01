unit _Indi_Pag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DB, ABSMain,
  DBGrids, dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls;

type
  THackDBGrid = class(TDBGrid);
  TFm_Indi_Pag = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    GroupBox1: TGroupBox;
    EdPesq: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    FIndi_Pag: String;
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    FFecha: Boolean;
    class function PermiteClose(Permite: Boolean): Boolean;
    class function CreateDockForm(): TCustomForm;
  end;

var
  Fm_Indi_Pag: TFm_Indi_Pag;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule(*, TabePrcCab, PediVdaImp, ModPediVda*),
UnInternalConsts, UCreate, MyGlyfs, APagRec;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFm_Indi_Pag.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFm_Indi_Pag.AtivaItens(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'UPDATE ' + FIndi_Pag,
  'SET Ativo=' + FormatFloat('0', Status)]);
  DModG.Tb_Indi_Pag.Refresh;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFm_Indi_Pag.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DModG.Tb_Indi_Pag.First;
  while not DModG.Tb_Indi_Pag.Eof do
  begin
    if DModG.Tb_Indi_PagAtivo.Value <> Status then
    begin
      DModG.Tb_Indi_Pag.Edit;
      DModG.Tb_Indi_PagAtivo.Value := Status;
      DModG.Tb_Indi_Pag.Post;
    end;
    DModG.Tb_Indi_Pag.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFm_Indi_Pag.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFm_Indi_Pag.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFm_Indi_Pag.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFm_Indi_Pag.CreateDockForm(): TCustomForm;
var
  hMenuHandle: Integer;
begin
  Result := TFm_Indi_Pag.Create(Application);
  FmMyGlyfs.ConfiguraFormDock(Result);
  TCustomForm(Result).BorderStyle := bsNone;
  hMenuHandle := GetSystemMenu(TCustomForm(Result).Handle, False);
  if (hMenuHandle <> 0) then
    DeleteMenu(hMenuHandle, SC_CLOSE, MF_BYCOMMAND);
  //hideclosebutton(handle);
{
  TForm(Result).BorderIcons:=[];
  //
}
  Result.Show;
end;

procedure TFm_Indi_Pag.FormActivate(Sender: TObject);
var
  hMenuHandle: Integer;
begin
{
  hMenuHandle := GetSystemMenu(Handle, False);
  if (hMenuHandle <> 0) then
    DeleteMenu(hMenuHandle, SC_CLOSE, MF_BYCOMMAND);
  //
  hMenuHandle := GetSubMenu(Handle, False);
  if (hMenuHandle <> 0) then
    DeleteMenu(hMenuHandle, SC_CLOSE, MF_BYCOMMAND);
  //
}end;

procedure TFm_Indi_Pag.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if VAR_FECHA_DOCK_FORM then
  begin
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'DROP TABLE ' + FIndi_Pag + '; ']);
    //
    ManualFloat(Rect(0, 0, 0, 0));
    Action := caFree;

  end else
  begin
    Action := TCloseAction(caNone);
    //Deactivate;
  end;
{
  if not VAR_FECHA_DOCK_FORM then
    FmAPagRec.RevisaDockForms();
}
end;

procedure TFm_Indi_Pag.FormShow(Sender: TObject);
begin
  //FmPediVdaImp.BtConfirma.Enabled := False;
end;

procedure TFm_Indi_Pag.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  //DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFm_Indi_Pag.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFm_Indi_Pag.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

class function TFm_Indi_Pag.PermiteClose(Permite: Boolean): Boolean;
begin
  //FFecha := Permite;
end;

procedure TFm_Indi_Pag.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  if Trim(EdPesq.Text) <> '' then
  begin
    DModG.Tb_Indi_Pag.Filter   := 'Nome LIKE ''%' + EdPesq.Text + '%''';
    DModG.Tb_Indi_Pag.Filtered := True;
  end else
    DModG.Tb_Indi_Pag.Filtered := False;
  //
  Screen.Cursor := crDefault;
end;

procedure TFm_Indi_Pag.RGSelecaoClick(Sender: TObject);
begin
  DModG.Tb_Indi_Pag.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DModG.Tb_Indi_Pag.Filter := 'Ativo=0';
    1: DModG.Tb_Indi_Pag.Filter := 'Ativo=1';
    2: DModG.Tb_Indi_Pag.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DModG.Tb_Indi_Pag.Filtered := True;
end;

procedure TFm_Indi_Pag.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DModG.Tb_Indi_Pag.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DModG.Tb_Indi_Pag.Edit;
    DModG.Tb_Indi_Pag.FieldByName('Ativo').Value := Status;
    DModG.Tb_Indi_Pag.Post;
  end;
  HabilitaItemUnico();
end;

procedure TFm_Indi_Pag.HabilitaItemUnico();
var
  Habilita: Boolean;
  Qry: TmySQLQuery;
begin
  Qry:= TmySQLQuery.Create(DmodG);
  Qry.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery1(Qry, [
  'SELECT Count(*)  Itens',
  'FROM ' + FIndi_Pag,
  'WHERE Ativo=1']);
  Habilita :=  Qry.FieldByName('Itens').AsInteger = 0;
{
  FmPediVdaImp.LaPVICli.Enabled := Habilita;
  FmPediVdaImp.EdPVICli.Enabled := Habilita;
  FmPediVdaImp.CBPVICli.Enabled := Habilita;
  if not Habilita then
  begin
    FmPediVdaImp.EdPVICli.ValueVariant := 0;
    FmPediVdaImp.CBPVICli.KeyValue     := 0;
  end;
}
end;


procedure TFm_Indi_Pag.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFm_Indi_Pag.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFm_Indi_Pag.FormCreate(Sender: TObject);
begin
  DBGrid1.DataSource := DModG.Ds_Indi_Pag;
  DBGrid2.DataSource := DModG.Ds_Indi_Pag;
  //
  DModG.Tb_Indi_Pag.Close;
  DModG.Tb_Indi_Pag.Database := DModG.MyPID_DB;
  FIndi_Pag := UCriar.RecriaTempTableNovo(ntrttPesqESel, DModG.QrUpdPID1,
    False, 1, '_Indi_Pag_');
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'DELETE FROM _Indi_Pag_;',
  'INSERT INTO _Indi_Pag_',
  'SELECT eci.CodCliInt Codigo,',
  'IF(Tipo=0, RazaoSocial, Nome) Nome,',
  '1 Ativo',
  'FROM ' + TMeuDB + '.enticliint eci',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=eci.CodEnti']);
  DModG.Tb_Indi_Pag.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFm_Indi_Pag.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmAPagRec', nil) > 0 then
  begin
    FmAPAgRec.AtivaBtConfirma();
    //FmAPagRec.RevisaDockForms();
  end;
end;

end.

