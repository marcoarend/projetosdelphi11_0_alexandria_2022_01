object FmReciboMany: TFmReciboMany
  Left = 349
  Top = 173
  BorderStyle = bsSizeToolWin
  Caption = 'IMP-RECIB-001 :: Impress'#227'o de Recibo'
  ClientHeight = 319
  ClientWidth = 426
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 426
    Height = 157
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 426
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 378
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 330
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 257
        Height = 32
        Caption = 'Impress'#227'o de Recibo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 257
        Height = 32
        Caption = 'Impress'#227'o de Recibo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 257
        Height = 32
        Caption = 'Impress'#227'o de Recibo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 249
    Width = 426
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 280
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 278
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImprime: TBitBtn
        Tag = 14
        Left = 12
        Top = 2
        Width = 120
        Height = 40
        Hint = 'Preview'
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImprimeClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 205
    Width = 426
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 422
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object frxRecibo1: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38009.439963981500000000
    ReportOptions.LastChange = 39098.923307291700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VARF_NOTCOLOR> = True then Shape8.Color := clWhite;'
      'end.')
    OnGetValue = frxRecibo1GetValue
    Left = 48
    Top = 52
    Datasets = <
      item
        DataSet = frxDsRecibos
        DataSetName = 'frxDsRecibos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 553.700782520000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        DataSet = frxDsRecibos
        DataSetName = 'frxDsRecibos'
        RowCount = 0
        object Shape8: TfrxShapeView
          Top = 34.015770000000000000
          Width = 699.213050000000000000
          Height = 487.559370000000000000
          Fill.BackColor = 14342874
        end
        object Shape1: TfrxShapeView
          Left = 11.338590000000000000
          Top = 60.472480000000000000
          Width = 260.000000000000000000
          Height = 48.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo2: TfrxMemoView
          Left = 40.220470000000000000
          Top = 60.913420000000000000
          Width = 226.897650000000000000
          Height = 39.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NUMERO_P]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 15.338590000000000000
          Top = 60.472480000000000000
          Width = 24.000000000000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Left = 428.661410000000000000
          Top = 60.472480000000000000
          Width = 260.000000000000000000
          Height = 48.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo29: TfrxMemoView
          Left = 472.661410000000000000
          Top = 64.913420000000000000
          Width = 208.000000000000000000
          Height = 39.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VALOR_P]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 432.661410000000000000
          Top = 64.472480000000000000
          Width = 36.000000000000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Valor:')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Left = 11.338590000000000000
          Top = 112.472480000000000000
          Width = 677.322820000000000000
          Height = 112.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo4: TfrxMemoView
          Left = 15.559060000000000000
          Top = 114.472480000000000000
          Width = 477.102350000000000000
          Height = 30.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Recebemos de [NOME_PC]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 84.661410000000000000
          Top = 144.472480000000000000
          Width = 188.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[RUA_PC]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 496.188930000000000000
          Top = 116.472480000000000000
          Width = 188.472480000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[CNPJ_PC]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 556.661410000000000000
          Top = 164.472480000000000000
          Width = 128.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TE1_PC]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 288.661410000000000000
          Top = 144.472480000000000000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[NUMERO_PC]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 360.661410000000000000
          Top = 144.472480000000000000
          Width = 184.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[COMPL_PC]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 556.661410000000000000
          Top = 144.472480000000000000
          Width = 128.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[BAIRRO_PC]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 84.661410000000000000
          Top = 164.472480000000000000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[CIDADE_PC]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 276.661410000000000000
          Top = 164.472480000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[UF_PC]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 320.661410000000000000
          Top = 164.472480000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[CEP_PC]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 428.661410000000000000
          Top = 164.472480000000000000
          Width = 116.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[PAIS_PC]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 95.779530000000000000
          Top = 184.472480000000000000
          Width = 584.881880000000000000
          Height = 38.000000000000000000
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[EXTENSO_P]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 15.559060000000000000
          Top = 144.472480000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 15.559060000000000000
          Top = 180.692950000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'A import'#226'ncia de:')
          ParentFont = False
        end
        object Shape4: TfrxShapeView
          Left = 11.338590000000000000
          Top = 228.472480000000000000
          Width = 677.322820000000000000
          Height = 68.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo16: TfrxMemoView
          Left = 15.779530000000000000
          Top = 232.472480000000000000
          Width = 668.881880000000000000
          Height = 62.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Referente a: [MOTIVO_P]')
          ParentFont = False
        end
        object Shape5: TfrxShapeView
          Left = 11.338590000000000000
          Top = 300.472480000000000000
          Width = 677.322820000000000000
          Height = 88.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo15: TfrxMemoView
          Left = 15.338590000000000000
          Top = 306.472480000000000000
          Width = 473.322820000000000000
          Height = 17.000000000000000000
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Emitente: [EMITENTEP]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 15.338590000000000000
          Top = 324.472480000000000000
          Width = 48.881880000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Shape6: TfrxShapeView
          Left = 11.338590000000000000
          Top = 392.472480000000000000
          Width = 677.322820000000000000
          Height = 32.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo55: TfrxMemoView
          Left = 15.559060000000000000
          Top = 404.472480000000000000
          Width = 661.102350000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[LOCAL_E_DATA]')
          ParentFont = False
        end
        object Shape7: TfrxShapeView
          Left = 11.338590000000000000
          Top = 428.472480000000000000
          Width = 677.322820000000000000
          Height = 56.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo27: TfrxMemoView
          Left = 316.661410000000000000
          Top = 464.472480000000000000
          Width = 368.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[RESPONSAVEL_E]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 275.905690000000000000
          Top = 60.472480000000000000
          Width = 147.401670000000000000
          Height = 45.354360000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -35
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RECIBO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 83.149660000000000000
          Top = 334.141930000000000000
          Width = 188.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[RUA_PE]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 494.677180000000000000
          Top = 306.141930000000000000
          Width = 188.472480000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[CNPJ_PE]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 555.149660000000000000
          Top = 354.141930000000000000
          Width = 128.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TE1_PE]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 359.149660000000000000
          Top = 334.141930000000000000
          Width = 184.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[COMPL_PE]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 555.149660000000000000
          Top = 334.141930000000000000
          Width = 128.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[BAIRRO_PE]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 83.149660000000000000
          Top = 354.141930000000000000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[CIDADE_PE]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 275.149660000000000000
          Top = 354.141930000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[UF_PE]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 319.149660000000000000
          Top = 354.141930000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[CEP_PE]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 427.149660000000000000
          Top = 354.141930000000000000
          Width = 116.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[PAIS_PE]')
          ParentFont = False
        end
        object BarCode1: TfrxBarCodeView
          Left = 37.795300000000000000
          Top = 438.425480000000000000
          Width = 129.000000000000000000
          Height = 34.015770000000000000
          BarType = bcCode39Extended
          Expression = '<VARF_BARCODE_BARC>'
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Line1: TfrxLineView
          Top = 553.700782520000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
    end
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 112
    Top = 48
  end
  object QrRecibos: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrRecibosAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM _recibos_')
    Left = 48
    Top = 96
    object QrRecibosSeqID: TIntegerField
      FieldName = 'SeqID'
    end
    object QrRecibosEmitente: TIntegerField
      FieldName = 'Emitente'
    end
    object QrRecibosBeneficiario: TIntegerField
      FieldName = 'Beneficiario'
    end
    object QrRecibosValor: TFloatField
      FieldName = 'Valor'
    end
    object QrRecibosValorP: TFloatField
      FieldName = 'ValorP'
    end
    object QrRecibosValorE: TFloatField
      FieldName = 'ValorE'
    end
    object QrRecibosNumRecibo: TWideStringField
      FieldName = 'NumRecibo'
      Size = 255
    end
    object QrRecibosReferente: TWideMemoField
      FieldName = 'Referente'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrRecibosReferenteP: TWideMemoField
      FieldName = 'ReferenteP'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrRecibosReferenteE: TWideMemoField
      FieldName = 'ReferenteE'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrRecibosData: TDateField
      FieldName = 'Data'
    end
    object QrRecibosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrRecibosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object frxDsRecibos: TfrxDBDataset
    UserName = 'frxDsRecibos'
    CloseDataSource = False
    DataSet = QrRecibos
    BCDToCurrency = False
    Left = 48
    Top = 144
  end
end
