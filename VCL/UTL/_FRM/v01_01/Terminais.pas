unit Terminais;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, Grids, DBGrids, dmkImage,
  dmkGeral, UnDMkEnums;

type
  TFmTerminais = class(TForm)
    PainelDados: TPanel;
    Panel1: TPanel;
    DsTerminais: TDataSource;
    DBGrid1: TDBGrid;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrTerminaisConfigurado: TWideStringField;
    QrTerminaisLicenca: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTerminaisCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmTerminais: TFmTerminais;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UnInternalConsts, DmkDAC_PF;

procedure TFmTerminais.BtOKClick(Sender: TObject);
begin
  VAR_IP_LICENCA := String(QrTerminaisIP.Value);
  Close;
end;

procedure TFmTerminais.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTerminais.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTerminais.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTerminais.QrTerminaisCalcFields(DataSet: TDataSet);
begin
  if QrTerminaisLicenca.Value <> '' then
    QrTerminaisConfigurado.Value := 'SIM'
  else
    QrTerminaisConfigurado.Value := 'N�O';
end;

procedure TFmTerminais.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  UnDmkDAC_PF.AbreQuery(QrTerminais, Dmod.MyDB);
end;

end.
