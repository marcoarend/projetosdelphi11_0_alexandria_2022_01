unit Opcoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ExtDlgs, ComCtrls, UnDmkProcFunc,
  DBCtrls, UnInternalConsts, Db, mySQLDbTables, Menus, Variants,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Grids, DBGrids, dmkGeral, dmkCheckBox,
  dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmOpcoes = class(TForm)
    Panel1: TPanel;
    Panel4: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    QrUFs: TmySQLQuery;
    DsUFs: TDataSource;
    QrUFsCodigo: TIntegerField;
    QrUFsNome: TWideStringField;
    TabSheet2: TTabSheet;
    QrCarteiras1: TmySQLQuery;
    DsCarteiras1: TDataSource;
    QrCarteiras1Codigo: TIntegerField;
    QrCarteiras1Nome: TWideStringField;
    QrCarteiras1Tipo: TIntegerField;
    TabSheet3: TTabSheet;
    Memo1: TMemo;
    PMImprime: TPopupMenu;
    Impressora1: TMenuItem;
    ArquivoMemoaolado1: TMenuItem;
    BtImprime: TBitBtn;
    Memo2: TMemo;
    QrPlaNiv5Jur: TMySQLQuery;
    DsPlaNiv5Jur: TDataSource;
    QrPlaNiv5JurCodigo: TIntegerField;
    QrPlaNiv5JurNome: TWideStringField;
    OpenPictureDialog1: TOpenPictureDialog;
    OpenPictureDialog2: TOpenPictureDialog;
    QrCarteiras2: TmySQLQuery;
    DsCarteiras2: TDataSource;
    QrCarteiras2Codigo: TIntegerField;
    QrCarteiras2Nome: TWideStringField;
    QrCarteiras2Tipo: TIntegerField;
    TabSheet4: TTabSheet;
    EdCNABCtaJur: TdmkEditCB;
    Label8: TLabel;
    CBCNABCtaJur: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdCNABCtaTar: TdmkEditCB;
    CBCNABCtaTar: TdmkDBLookupComboBox;
    EdCNABCtaMul: TdmkEditCB;
    Label16: TLabel;
    CBCNABCtaMul: TdmkDBLookupComboBox;
    Label17: TLabel;
    Label18: TLabel;
    QrCambioMda: TmySQLQuery;
    DsCambioMda: TDataSource;
    QrCambioMdaCodigo: TIntegerField;
    QrCambioMdaNome: TWideStringField;
    TabSheet5: TTabSheet;
    Panel3: TPanel;
    DBGCambioUsu: TDBGrid;
    BtUsuario: TBitBtn;
    QrCambioUsu: TmySQLQuery;
    DsCambioUsu: TDataSource;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label20: TLabel;
    EdMoeda: TdmkEdit;
    EdMoedaBr: TdmkEditCB;
    CBMoedaBr: TdmkDBLookupComboBox;
    PMUsuario: TPopupMenu;
    Adicionausurio1: TMenuItem;
    Retirausurio1: TMenuItem;
    QrCambioUsuLogin: TWideStringField;
    QrCambioUsuNumero: TIntegerField;
    QrCambioUsuFuncionario: TIntegerField;
    QrCambioUsuNOMEFUNCI: TWideStringField;
    QrListaLograd: TmySQLQuery;
    QrListaLogradCodigo: TIntegerField;
    QrListaLogradNome: TWideStringField;
    QrListaLogradTrato: TWideStringField;
    Panel5: TPanel;
    Panel6: TPanel;
    GroupBox5: TGroupBox;
    Label2: TLabel;
    Label1: TLabel;
    CBUF: TDBLookupComboBox;
    EdCidade: TdmkEdit;
    CkTravaCidade: TCheckBox;
    BtCorrige: TBitBtn;
    CkMaiusculas: TCheckBox;
    CkSolicitaSenha: TCheckBox;
    CkEntraSemValor: TCheckBox;
    GroupBox3: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    EdMoraDD: TdmkEdit;
    EdMulta: TdmkEdit;
    Label3: TLabel;
    EdMeuLogoPath: TdmkEdit;
    Label14: TLabel;
    EdLogo0Path: TdmkEdit;
    Label5: TLabel;
    EdLogoNF: TdmkEdit;
    Label19: TLabel;
    EdLogoBig1: TdmkEdit;
    CkCorRecibo: TCheckBox;
    Panel7: TPanel;
    GroupBox6: TGroupBox;
    RGImpreCheque: TRadioGroup;
    LaPortaImpCheque: TLabel;
    EdPortaImpCheque: TdmkEdit;
    LaEdImpreCh_IP: TLabel;
    EdImpreCh_IP: TdmkEdit;
    EdImpreChPorta: TdmkEdit;
    LaEdImpreChPorta: TLabel;
    Label21: TLabel;
    EdDmkNetPath: TdmkEdit;
    SBDmkNetPath: TSpeedButton;
    Panel9: TPanel;
    CkAvisaVendedor: TCheckBox;
    CkAvisaRepresent: TCheckBox;
    CkAvisaCliInt: TCheckBox;
    CkAvisaDescoPor: TCheckBox;
    CkMensalSempre: TCheckBox;
    CkDuplicLct: TCheckBox;
    GroupBox4: TGroupBox;
    Label11: TLabel;
    Label13: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    EdMyPgQtdP: TdmkEdit;
    RGMyPgPeri: TRadioGroup;
    EdMyPgDias: TdmkEdit;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    EdVendaCartPg: TdmkEditCB;
    CBVendaCartPg: TdmkDBLookupComboBox;
    EdVendaParcPg: TdmkEdit;
    RGVendaPeriPg: TRadioGroup;
    EdVendaDiasPg: TdmkEdit;
    Label48: TLabel;
    EdDescoConta: TdmkEditCB;
    CBDescoConta: TdmkDBLookupComboBox;
    RGVctAutDefU: TRadioGroup;
    TabSheet6: TTabSheet;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel8: TPanel;
    BtSalvar: TBitBtn;
    Panel10: TPanel;
    GroupBox7: TGroupBox;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    CkProxUse: TdmkCheckBox;
    CkProxBAut: TdmkCheckBox;
    EdProxUser: TdmkEdit;
    EdProxServ: TdmkEdit;
    EdProxPort: TdmkEdit;
    EdProxPass: TEdit;
    TabSheet7: TTabSheet;
    Panel11: TPanel;
    RGAbaIniApp: TdmkRadioGroup;
    DsPlaNiv5Tar: TDataSource;
    QrPlaNiv5Tar: TMySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    CkMyPathsFrx: TdmkCheckBox;
    GroupBox8: TGroupBox;
    Label12: TLabel;
    EdIdleMinutos: TdmkEdit;
    Label22: TLabel;
    EdIdlePingMin: TdmkEdit;
    Label23: TLabel;
    LaHora: TLabel;
    EdLastBckpIntrvl: TdmkEdit;
    CkUsaGenCtb: TCheckBox;
    Label24: TLabel;
    EdPwdLibFunc: TdmkEdit;
    EdLimiCredIni: TdmkEdit;
    Label25: TLabel;
    EdCNPJaToken: TdmkEdit;
    Label26: TLabel;
    CkExigeNumDocCNPJCPF: TCheckBox;
    CkConcatNivOrdCta: TCheckBox;
    CkUsarFinCtb: TCheckBox;
    CkUsarCtbEstqParaMP: TCheckBox;
    CkUsarCtbEstqParaUC: TCheckBox;
    CkUsarEntraFiscal: TCheckBox;
    Label27: TLabel;
    EdDifMaxPedNFe: TdmkEdit;
    procedure BtSalvarClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCambio1Change(Sender: TObject);
    procedure EdPathProdChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Impressora1Click(Sender: TObject);
    procedure ArquivoMemoaolado1Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure EdLogo0PathKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMeuLogoPathKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLogoNFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLogoBig1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtUsuarioClick(Sender: TObject);
    procedure Retirausurio1Click(Sender: TObject);
    procedure Adicionausurio1Click(Sender: TObject);
    procedure BtCorrigeClick(Sender: TObject);
    procedure CkMaiusculasClick(Sender: TObject);
    procedure RGImpreChequeClick(Sender: TObject);
    procedure EdPortaImpChequeChange(Sender: TObject);
    procedure EdImpreCh_IPChange(Sender: TObject);
    procedure EdImpreChPortaChange(Sender: TObject);
    procedure SBDmkNetPathClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FModified: Boolean;
    function  Salvar(ForceSave: Boolean): Boolean;
    procedure GeraCaracteres(Mostra: Integer);
  public
    { Public declarations }
    FAba: Integer;
    {$IfNDef SemCotacoes}
    procedure ReopenCambioUsu(Codigo: Integer);
    {$EndIf}
  end;

var
  FmOpcoes: TFmOpcoes;

implementation

uses UnMyObjects, Module, MyDBCheck, UnGrl_Consts,
{$IfNDef SemCotacoes} CambioUsu, {$EndIf}
//{$IfNDef NAO_USA_IMP_CHEQUE} UnMyPrinters, {$EndIf}
  UMySQLModule, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmOpcoes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoes.FormCreate(Sender: TObject);
var
  ProxServ, ProxUser, ProxPass, Cam: String;
  ProxBaAu, ProxEnab, ProxPort, I: Integer;
begin
  ImgTipo.SQLType := stLok;
  FAba            := -1;
  //
  {$IfNDef SemCotacoes}
  BtUsuario.Enabled := True;
  //
  UMyMod.AbreQuery(QrCambioMda, Dmod.MyDB);
  {$Else}
  BtUsuario.Enabled := False;
  {$EndIf}
  {$IfDef cAdvToolx} //Berlin
  if VAR_AdvToolBarPagerPrincipal <> nil then
  begin
    RGAbaIniApp.Items.Clear;
    for I := 0 to VAR_AdvToolBarPagerPrincipal.AdvPageCount - 1 do
    begin
      RGAbaIniApp.Items.Add(VAR_AdvToolBarPagerPrincipal.AdvPages[I].Caption);
    end;
  end else
  begin
    RGAbaIniApp.Items.Add('Indefinido');
  end;
  {$Else}
  if VAR_PageControlMenuPrincipal <> nil then
  begin
    RGAbaIniApp.Items.Clear;
    for I := 0 to VAR_PageControlMenuPrincipal.PageCount - 1 do
    begin
      RGAbaIniApp.Items.Add(VAR_PageControlMenuPrincipal.Pages[I].Caption);
    end;
  end else
  begin
    RGAbaIniApp.Items.Add('Indefinido');
  end;
  {$EndIf}
  //
  {$IfNDef   NO_FINANCEIRO}
  UMyMod.AbreQuery(QrPlaNiv5Jur, Dmod.MyDB);
  UMyMod.AbreQuery(QrPlaNiv5Tar, Dmod.MyDB);
  UMyMod.AbreQuery(QrCarteiras1, Dmod.MyDB);
  UMyMod.AbreQuery(QrCarteiras2, Dmod.MyDB);
  {$EndIf}
  //
  Dmod.ReopenControle();
  DModG.ReopenControle();
  //
  {$IfNDef NO_FINANCEIRO}
  EdMoraDD.Text := Geral.FFT(Dmod.QrControle.FieldByName('MoraDD').AsFloat, 2, siPositivo);
  EdMulta.Text := Geral.FFT(Dmod.QrControle.FieldByName('Multa').AsFloat, 2, siPositivo);
  CkMensalSempre.Checked  := Geral.IntToBool(Dmod.QrControle.FieldByName('MensalSempre').AsInteger);
  CkEntraSemValor.Checked := Geral.IntToBool(Dmod.QrControle.FieldByName('EntraSemValor').AsInteger);
  CkCorRecibo.Checked     := Geral.IntToBool(Dmod.QrControle.FieldByName('CorRecibo').AsInteger);
  {$EndIf}
  //
  Cam := Application.Title+'\Opcoes';
  UnDmkDAC_PF.AbreQuery(QrUFs, Dmod.MyDB);
  //
  if  VAR_AVISOSCXAEDIT in [01,03,05,07,09,11,13,15] then CkAvisaVendedor.Checked := True;
  if  VAR_AVISOSCXAEDIT in [02,03,06,07,10,11,14,15] then CkAvisaVendedor.Checked := True;
  if  VAR_AVISOSCXAEDIT in [04,05,06,07,12,13,14,15] then CkAvisaCliInt.Checked := True;
  if  VAR_AVISOSCXAEDIT in [08,09,10,11,12,13,14,15] then CkAvisaDescoPor.Checked := True;
  CkMaiusculas.Checked  := VAR_SOMAIUSCULAS;
  EdMoeda.Text          := VAR_MOEDA;
  CBUF.KeyValue         := VAR_UFPADRAO;
  EdCidade.Text         := VAR_CIDADEPADRAO;
  CkTravaCidade.Checked := Geral.IntToBool(VAR_TRAVACIDADE);
  if DmodG.QrMasterSolicitaSenha.Value = 1 then  CkSolicitaSenha.Checked := True
  else CkSolicitaSenha.Checked := False;
  //
  EdIdleMinutos.ValueVariant := Dmod.QrControle.FieldByName('IdleMinutos').AsInteger;
  EdIdlePingMin.ValueVariant := Geral.ReadAppKeyLM('IdlePingMin', Application.Title, ktInteger, 1);
  // ini 2022-03-01
  if EdIdlePingMin.ValueVariant = 1 then
    EdIdlePingMin.ValueVariant := Geral.ReadAppKeyLM('IdlePingMin', Application.Title, ktInteger, 10);
  // fim 2022-03-01
  //
  FModified := False;
  PageControl1.ActivePageIndex := 0;
  //
  EdCarteira.Text        := Geral.FF0(Dmod.QrControle.FieldByName('MyPagCar').AsInteger);
  CBCarteira.KeyValue    := Dmod.QrControle.FieldByName('MyPagCar').AsInteger;
  EdMyPgQtdP.Text        := Geral.FF0(Dmod.QrControle.FieldByName('MyPgQtdP').AsInteger);
  RGMyPgPeri.ItemIndex   := Dmod.QrControle.FieldByName('MyPgPeri').AsInteger;
  EdMyPgDias.Text        := Geral.FF0(Dmod.QrControle.FieldByName('MyPgDias').AsInteger);
  //
  EdVendaCartPg.Text     := Geral.FF0(Dmod.QrControle.FieldByName('VendaCartPg').AsInteger);
  CBVendaCartPg.KeyValue := Dmod.QrControle.FieldByName('VendaCartPg').AsInteger;
  EdVendaParcPg.Text     := Geral.FF0(Dmod.QrControle.FieldByName('VendaParcPg').AsInteger);
  RGMyPgPeri.ItemIndex   := Dmod.QrControle.FieldByName('VendaPeriPg').AsInteger;
  EdVendaDiasPg.Text     := Geral.FF0(Dmod.QrControle.FieldByName('VendaDiasPg').AsInteger);
  // CNAB
  EdCNABCtaJur.Text     := Geral.FF0(Dmod.QrControle.FieldByName('CNABCtaJur').AsInteger);
  CBCNABCtaJur.KeyValue := Dmod.QrControle.FieldByName('CNABCtaJur').AsInteger;
  EdCNABCtaMul.Text     := Geral.FF0(Dmod.QrControle.FieldByName('CNABCtaMul').AsInteger);
  CBCNABCtaMul.KeyValue := Dmod.QrControle.FieldByName('CNABCtaMul').AsInteger;
  EdCNABCtaTar.Text     := Geral.FF0(Dmod.QrControle.FieldByName('CNABCtaTar').AsInteger);
  CBCNABCtaTar.KeyValue := Dmod.QrControle.FieldByName('CNABCtaTar').AsInteger;
  //
  EdMeuLogoPath.Text     := Dmod.QrControle.FieldByName('MeuLogoPath').AsString;
  EdLogoNF.Text          := Dmod.QrControle.FieldByName('LogoNF').AsString;
  EdLogoBig1.Text        := Dmod.QrControle.FieldByName('LogoBig1').AsString;
  EdLogo0Path.Text       := Geral.ReadAppKeyLM('Logo0', Application.Title, ktString, '');
  // ini 2022-03-01
  if EdLogo0Path.Text = EmptyStr then
    EdLogo0Path.Text       := Geral.ReadAppKeyCU('Logo0', Application.Title, ktString, '');
  // fim 2022-03-01
  EdMoedaBr.ValueVariant := Dmod.QrControle.FieldByName('MoedaBr').AsInteger;
  CBMoedaBr.KeyValue     := Dmod.QrControle.FieldByName('MoedaBr').AsInteger;
  //
  // Local
  RGImpreCheque.ItemIndex     := VAR_IMPCHEQUE;
  EdPortaImpCheque.Text       := VAR_IMPCHEQUE_PORTA;
  // Servidor
  EdImpreCh_IP.ValueVariant   := VAR_IMPCH_IP;
  EdImpreChPorta.ValueVariant := VAR_IMPCHPORTA;
  //
  // CtrlGeral
  CkDuplicLct.Checked         := Geral.IntToBool(DModG.QrCtrlGeralDuplicLct.Value);
  EdDmkNetPath.Text           := DModG.QrCtrlGeralDmkNetPath.Value;
  RGVctAutDefU.ItemIndex      := DModG.QrCtrlGeralVctAutDefU.Value;
  //
  {$IfNDef SemCotacoes}
  ReopenCambioUsu(0);
  {$EndIf}
  RGAbaIniApp.ItemIndex       := DModG.QrCtrlGeralAbaIniApp.Value;
  CkMyPathsFrx.Checked        := Geral.IntToBool(DModG.QrCtrlGeralMyPathsFrx.Value);
  //
  CkConcatNivOrdCta.Checked   := Geral.IntToBool(DModG.QrCtrlGeralConcatNivOrdCta.Value);
  CkUsarFinCtb.Checked        := Geral.IntToBool(DModG.QrCtrlGeralUsarFinCtb.Value);
  CkUsarCtbEstqParaMP.Checked := Geral.IntToBool(DModG.QrCtrlGeralUsarCtbEstqParaMP.Value);
  CkUsarCtbEstqParaUC.Checked := Geral.IntToBool(DModG.QrCtrlGeralUsarCtbEstqParaUC.Value);
  CkUsarEntraFiscal.Checked   := Geral.IntToBool(DModG.QrCtrlGeralUsarEntraFiscal.Value);
  EdDifMaxPedNFe.ValueVariant := DModG.QrCtrlGeralDifMaxPedNFe.Value;
  //
  ProxUser := Geral.ReadAppKeyCU('ProxyUser', 'Dermatek', ktString, '');
  ProxPass := Geral.ReadAppKeyCU('ProxyPass', 'Dermatek', ktString, '');
  ProxEnab := Geral.ReadAppKeyCU('ProxyEnable', 'Dermatek', ktInteger, 0);
  ProxBaAu := Geral.ReadAppKeyCU('ProxyBasicAuth', 'Dermatek', ktInteger, 0);
  ProxServ := Geral.ReadAppKeyCU('ProxyServ', 'Dermatek', ktString, '');
  ProxPort := Geral.ReadAppKeyCU('ProxyPort', 'Dermatek', ktInteger, 0);
  //
  if (Length(ProxServ) = 0) and (ProxEnab = 0) and (ProxPort = 0) then
  begin
    ProxEnab := Geral.ReadKey('ProxyEnable',
      'Software\Microsoft\Windows\CurrentVersion\Internet Settings\', ktInteger,
      0, HKEY_CURRENT_USER);
    ProxBaAu := ProxEnab;
    ProxServ := Geral.ReadKey('ProxyServer',
      'Software\Microsoft\Windows\CurrentVersion\Internet Settings\', ktString,
      '', HKEY_CURRENT_USER);
    if Length(ProxServ) > 0 then
    begin
      ProxPort := Pos(':', ProxServ);
      if ProxPort > 0 then
        ProxPort := Geral.IMV(Copy(ProxServ, ProxPort + 1));
    end;
    if Pos(':', ProxServ) > 0 then
      ProxServ := Copy(ProxServ, 0, Pos(':', ProxServ) - 1);
  end;
  //
  CkProxUse.Checked       := Geral.IntToBool(ProxEnab);
  CkProxBAut.Checked      := Geral.IntToBool(ProxBaAu);
  EdProxServ.ValueVariant := ProxServ;
  EdProxPort.ValueVariant := ProxPort;
  EdProxUser.ValueVariant := dmkPF.PWDExDecode(ProxUser, CO_RandStrWeb01);
  EdProxPass.Text         := dmkPF.PWDExDecode(ProxPass, CO_RandStrWeb01);
  //
  EdDmkNetPath.Enabled := True;//Length(DModG.QrCtrlGeralDmkNetPath.Value) > 0;
  SBDmkNetPath.Enabled := True;//Length(DModG.QrCtrlGeralDmkNetPath.Value) > 0;
  //
  EdLastBckpIntrvl.ValueVariant     := DModG.QrCtrlGeralLastBckpIntrvl.Value;
  //
  CkUsaGenCtb.Checked               := Geral.IntToBool(DModG.QrCtrlGeralUsaGenCtb.Value);
  EdPwdLibFunc.Text                 := DModG.QrCtrlGeralPwdLibFunc.Value;
  EdLimiCredIni.ValueVariant        := DModG.QrCtrlGeralLimiCredIni.Value;
  EdCNPJaToken.ValueVariant         := DModG.QrCtrlGeralCNPJaToken.Value;
  CkExigeNumDocCNPJCPF.Checked      := Geral.IntToBool(DModG.QrCtrlGeralExigeNumDocCNPJCPF.Value);
end;

procedure TFmOpcoes.EdCambio1Change(Sender: TObject);
begin
  FModified := True;
end;

procedure TFmOpcoes.EdPathProdChange(Sender: TObject);
begin
  FModified := True;
end;

procedure TFmOpcoes.EdPortaImpChequeChange(Sender: TObject);
begin
  VAR_IMPCHEQUE_PORTA := EdPortaImpCheque.Text;
end;

procedure TFmOpcoes.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := Salvar(False);
end;

procedure TFmOpcoes.BtSalvarClick(Sender: TObject);
begin
  Salvar(True);
end;

function TFmOpcoes.Salvar(ForceSave: Boolean): Boolean;
var
  SolicitaSenha, UF, AvisosCk, MyPagDias, MyPagParc, AbaIniApp: Integer;
  Retorno, ProxUser, ProxPass: String;
  //
  DmkNetPath, LastBckpIntrvl, PwdLibFunc, CNPJaToken: String;
  DuplicLct, VctAutDefU, MyPathsFrx, UsaGenCtb, ExigeNumDocCnpjCpf: Integer;
  LimiCredIni, DifMaxPedNFe: Double;
  ConcatNivOrdCta, UsarFinCtb, UsarCtbEstqParaMP, UsarCtbEstqParaUC,
  UsarEntraFiscal: Integer;
begin
  if ForceSave then
  begin
    try
      /////////////////////////////////////////
      AvisosCk := 0;
      MyPagDias := Geral.IMV(EdMyPgDias.Text);
      MyPagParc := Geral.IMV(EdMyPgQtdP.Text);
      if CkAvisaVendedor.Checked then inc(AvisosCk, 1);
      if CkAvisaRepresent.Checked then inc(AvisosCk, 2);
      if CkAvisaCliInt.Checked then inc(AvisosCk, 4);
      if CkAvisaDescoPor.Checked then inc(AvisosCk, 8);
      VAR_AVISOSCXAEDIT := AvisosCk;
      /////////////////////////////////////////
      if CkMaiusculas.Checked then VAR_SOMAIUSCULAS := True
      else VAR_SOMAIUSCULAS := False;
      ExigeNumDocCnpjCpf := Geral.BoolToInt(CkExigeNumDocCNPJCPF.Checked);
      if CBUF.KeyValue <> Null then UF := CBUF.KeyValue else UF := 0;
      VAR_MOEDA        := EdMoeda.Text;
      VAR_UFPADRAO     := UF;
      VAR_CIDADEPADRAO := EdCidade.Text;
      VAR_TRAVACIDADE  := Geral.BoolToInt(CkTravaCidade.Checked);
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE controle SET SoMaiusculas=:P0, ');
      Dmod.QrUpd.SQL.Add('Moeda=:P1, UFPadrao=:P2, Cidade=:P3,');

      Dmod.QrUpd.SQL.Add('TravaCidade=:P4, AvisosCxaEdit=:P5, ');
      Dmod.QrUpd.SQL.Add('MoraDD=:P6, Multa=:P7, MensalSempre=:P8,');
      Dmod.QrUpd.SQL.Add('EntraSemValor=:P9, MyPagTip=:P10, MyPagCar=:P11,');
      Dmod.QrUpd.SQL.Add('IdleMinutos=:P12, CorRecibo=:P13, MyPgParc=:P14, ');
      Dmod.QrUpd.SQL.Add('MyPgQtdP=:P15, MyPgPeri=:P16, MyPgDias=:P17, ');
      Dmod.QrUpd.SQL.Add('MeuLogoPath=:P18, LogoNF=:P19, VendaCartPg=:P20, ');
      Dmod.QrUpd.SQL.Add('VendaParcPg=:P21, VendaPeriPg=:P22, VendaDiasPg=:P23,');
      Dmod.QrUpd.SQL.Add('CNABCtaJur=:P24, CNABCtaMul=:P25, CNABCtaTar=:P26, ');
      Dmod.QrUpd.SQL.Add('LogoBig1=:P27, MoedaBr=:P28 ');
      //
      Dmod.QrUpd.Params[00].AsString  := dmkPF.BoolToV_F(VAR_SOMAIUSCULAS);
      Dmod.QrUpd.Params[01].AsString  := VAR_MOEDA;
      Dmod.QrUpd.Params[02].AsInteger := VAR_UFPADRAO;
      Dmod.QrUpd.Params[03].AsString  := VAR_CIDADEPADRAO;
      Dmod.QrUpd.Params[04].AsInteger := VAR_TRAVACIDADE;
      Dmod.QrUpd.Params[05].AsInteger := VAR_AVISOSCXAEDIT;
      Dmod.QrUpd.Params[06].AsFloat   := Geral.DMV(EdMoraDD.Text);
      Dmod.QrUpd.Params[07].AsFloat   := Geral.DMV(EdMulta.Text);
      Dmod.QrUpd.Params[08].AsInteger := Geral.BoolToInt(CkMensalSempre.Checked);
      Dmod.QrUpd.Params[09].AsInteger := Geral.BoolToInt(CkEntraSemValor.Checked);
      Dmod.QrUpd.Params[10].AsInteger := QrCarteiras1Tipo.Value;
      Dmod.QrUpd.Params[11].AsInteger := Geral.IMV(EdCarteira.Text);
      Dmod.QrUpd.Params[12].AsInteger := Geral.IMV(EdIdleMinutos.Text);
      Dmod.QrUpd.Params[13].AsInteger := Geral.BoolToInt(CkCorRecibo.Checked);
      Dmod.QrUpd.Params[14].AsInteger := MyPagParc;
      Dmod.QrUpd.Params[15].AsInteger := Geral.IMV(EdMyPgQtdP.Text);
      Dmod.QrUpd.Params[16].AsInteger := RGMyPgPeri.ItemIndex;
      Dmod.QrUpd.Params[17].AsInteger := MyPagDias;
      Dmod.QrUpd.Params[18].AsString  := EdMeuLogoPath.Text;
      Dmod.QrUpd.Params[19].AsString  := EdLogoNF.Text;
      Dmod.QrUpd.Params[20].AsInteger := Geral.IMV(EdVendaCartPg.Text);
      Dmod.QrUpd.Params[21].AsInteger := Geral.IMV(EdVendaParcPg.Text);
      Dmod.QrUpd.Params[22].AsInteger := RGVendaPeriPg.ItemIndex;
      Dmod.QrUpd.Params[23].AsInteger := Geral.IMV(EdVendaDiasPg.Text);
      Dmod.QrUpd.Params[24].AsInteger := Geral.IMV(EdCNABCtaJur.Text);
      Dmod.QrUpd.Params[25].AsInteger := Geral.IMV(EdCNABCtaMul.Text);
      Dmod.QrUpd.Params[26].AsInteger := Geral.IMV(EdCNABCtaTar.Text);
      Dmod.QrUpd.Params[27].AsString  := EdLogoBig1.Text;//dmkPF.DuplicaBarras();
      Dmod.QrUpd.Params[28].AsInteger := Geral.IMV(EdMoedaBr.Text);
      //
      Dmod.QrUpd.ExecSQL;
      //
      //
      if CkSolicitaSenha.Checked then SolicitaSenha := 1 else SolicitaSenha := 0;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE master SET SolicitaSenha=:P0');
      Dmod.QrUpd.Params[0].AsInteger := SolicitaSenha;
      Dmod.QrUpd.ExecSQL;
      //
      DmodG.ReopenMaster(' TFmOpcoes.Salvar(543)');
      //
      // CtrlGeral
      DmkNetPath := (*dmkPF.DuplicaBarras(*)EdDmkNetPath.Text(*)*);
      DuplicLct  := Geral.BoolToInt(CkDuplicLct.Checked);
      VctAutDefU := RGVctAutDefU.Itemindex;
      AbaIniApp  := RGAbaIniApp.ItemIndex;
      MyPathsFrx := Geral.BoolToInt(CkMyPathsFrx.Checked);
      LastBckpIntrvl := EdLastBckpIntrvl.Text;
      UsaGenCtb      := Geral.BoolToInt(CkUsaGenCtb.Checked);
      PwdLibFunc     := EdPwdLibFunc.Text;
      LimiCredIni    := EdLimiCredIni.ValueVariant;
      CNPJaToken     := EdCNPJaToken.ValueVariant;
      ConcatNivOrdCta := Geral.BoolToInt(CkConcatNivOrdCta.Checked);
      UsarFinCtb        := Geral.BoolToInt(CkUsarFinCtb.Checked);
      UsarCtbEstqParaMP := Geral.BoolToInt(CkUsarCtbEstqParaMP.Checked);
      UsarCtbEstqParaUC := Geral.BoolToInt(CkUsarCtbEstqParaUC.Checked);
      UsarEntraFiscal   := Geral.BoolToInt(CkUsarEntraFiscal.Checked);
      DifMaxPedNFe      := EdDifMaxPedNFe.ValueVariant;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctrlgeral', False, [
      'Codigo', 'DmkNetPath', 'DuplicLct',
      'VctAutDefU', 'AbaIniApp', 'MyPathsFrx',
      'LastBckpIntrvl', 'UsaGenCtb', 'PwdLibFunc',
      'LimiCredIni', 'CNPJaToken', 'ExigeNumDocCnpjCpf',
      'ConcatNivOrdCta', 'UsarFinCtb', 'UsarEntraFiscal',
      'UsarCtbEstqParaMP', 'UsarCtbEstqParaUC',
      'DifMaxPedNFe'], [
      ], [
      1, DmkNetPath, DuplicLct,
      VctAutDefU, AbaIniApp, MyPathsFrx,
      LastBckpIntrvl, UsaGenCtb, PwdLibFunc,
      LimiCredIni, CNPJaToken, ExigeNumDocCnpjCpf,
      ConcatNivOrdCta, UsarFinCtb, UsarEntraFiscal,
      UsarCtbEstqParaMP, UsarCtbEstqParaUC,
      DifMaxPedNFe], [
      ], False) then
      begin
        DModG.QrCtrlGeral.Close;
        UnDmkDAC_PF.AbreQuery(DModG.QrCtrlGeral, Dmod.MyDB);
      end;
      // Local
      Geral.WriteAppKeyCU('ImpCheque', 'Dermatek', VAR_IMPCHEQUE, ktInteger);
      Geral.WriteAppKeyCU('ImpCheque_Porta', 'Dermatek', VAR_IMPCHEQUE_PORTA, ktString);
      // Servidor
      Geral.WriteAppKeyCU('ImpreCh_IP', 'Dermatek', VAR_IMPCH_IP, ktString);
      Geral.WriteAppKeyCU('ImpreChPorta', 'Dermatek', VAR_IMPCHPORTA, ktInteger);
      //
      //
      {$IfNDef NAO_USA_IMP_CHEQUE}
      //MyPrinters.VerificaImpressoraDeCheque(Retorno);
      if Retorno <> '' then
        Geral.MB_Aviso(Retorno);
      {$EndIf}
      //
      ProxUser := dmkPF.PWDExEncode(EdProxUser.ValueVariant, CO_RandStrWeb01);
      ProxPass := dmkPF.PWDExEncode(EdProxPass.Text, CO_RandStrWeb01);
      //
      Geral.WriteAppKeyCU('ProxyUser', 'Dermatek', ProxUser, ktString);
      Geral.WriteAppKeyCU('ProxyPass', 'Dermatek', ProxPass, ktString);
      Geral.WriteAppKeyCU('ProxyEnable', 'Dermatek', Geral.BoolToInt(CkProxUse.Checked), ktInteger);
      Geral.WriteAppKeyCU('ProxyBasicAuth', 'Dermatek', Geral.BoolToInt(CkProxBAut.Checked), ktInteger);
      Geral.WriteAppKeyCU('ProxyServ', 'Dermatek', EdProxServ.ValueVariant, ktString);
      Geral.WriteAppKeyCU('ProxyPort', 'Dermatek', EdProxPort.ValueVariant, ktInteger);
      //
      //
{
      // ini 2022-03-01
      try
        Geral.WriteAppKeyLM('Logo0', Application.Title, EdLogo0Path.Text, ktString);
        Geral.WriteAppKeyLM('IdlePingMin', Application.Title, EdIdlePingMin.ValueVariant, ktInteger);
      except
        // nada
      end;
}
      Geral.WriteAppKeyCU('Logo0', Application.Title, EdLogo0Path.Text, ktString);
      Geral.WriteAppKeyCU('IdlePingMin', Application.Title, EdIdlePingMin.ValueVariant, ktInteger);
      Geral.MB_Info('"Logo0" e "IdlePingMin" salvos no current user -> HKCU');
      // fim 2022-03-01
      //
      Close;
    except
      raise;
      Exit;
    end;
  end;
  Result := True;
end;

procedure TFmOpcoes.SBDmkNetPathClick(Sender: TObject);
begin
  EdDmkNetPath.Text := '\\' + Geral.IndyComputerName() + '\Dermatek\';
end;

procedure TFmOpcoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoes.FormShow(Sender: TObject);
begin
  if FAba > -1 then
    PageControl1.ActivePageIndex := 3;
end;

procedure TFmOpcoes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Dmod.ReopenControle();
end;

procedure TFmOpcoes.Impressora1Click(Sender: TObject);
begin
  GeraCaracteres(1);
end;

procedure TFmOpcoes.Retirausurio1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrCambioUsu, DBGCambioUsu, 'cambiousu',
  ['Numero'], ['Codigo'], istPergunta, '');
end;

procedure TFmOpcoes.RGImpreChequeClick(Sender: TObject);
begin
  VAR_IMPCHEQUE := RGImpreCheque.ItemIndex;
  EdPortaImpCheque.Enabled := RGImpreCheque.ItemIndex = 3;
  LaPortaImpCheque.Enabled := RGImpreCheque.ItemIndex = 3;
end;

procedure TFmOpcoes.Adicionausurio1Click(Sender: TObject);
begin
{$IfNDef SemCotacoes}
  if DBCheck.CriaFm(TFmCambioUsu, FmCambioUsu, afmoNegarComAviso) then
  begin
    FmCambioUsu.ShowModal;
    FmCambioUsu.Destroy;
  end;
{$EndIf}
end;

procedure TFmOpcoes.ArquivoMemoaolado1Click(Sender: TObject);
begin
  GeraCaracteres(2);
end;

procedure TFmOpcoes.GeraCaracteres(Mostra: Integer);
const
  Arquivo = 'C:\Dermatek\CarcteresUnicode.txt';
var
  FArqPrn: TextFile;
  i: Integer;
begin
  if Mostra = 1 then AssignFile(FArqPrn, 'LPT1:')
    else AssignFile(FArqPrn, Arquivo);
  ReWrite(FArqPrn);
  Write(FArqPrn, Char($1B)+Char($52)+Char($12)); // Seleciona tabela de caracteres sul americana
  Write(FArqPrn, Char($1B)+Char($74)+Char($01)); // Seleciona Caracteres Graficos extendidos
  Write(FArqPrn, Char($1B)+Char($06));           // Habilita caracteres imprimíveis
  Write(FArqPrn, Char($1B)+Char($78));           // Seleciona Fonte Draft (rápida)
  Write(FArqPrn, Char($1B)+Char($0F));           // Ativa   modo condensado
  //Write(FArqPrn, Char($12));                   // Cancela modo condensado
  Write(FArqPrn, Char($1B)+Char($50));           // Seleciona 10 CPI
  for i := 32 to 255 do
    WriteLn(FArqPrn, FormatFloat('000', i)+' - '+Char(i)+'    ');
  Write(FArqPrn, Char($1B)+Char($40));           // ESC @  (Reseta (inicializa) impressora)
  CloseFile(FArqPrn);
  //ShowMessage('Arquivo UNICODE "C:\CaracteresUnicode.Txt" gerado com sucesso!');
  if Mostra = 2 then
  begin
    Memo1.Lines.Clear;
    Memo1.Lines.LoadFromFile(Arquivo);
  end;
end;

procedure TFmOpcoes.BtUsuarioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMUsuario, BtUsuario);
end;

procedure TFmOpcoes.BtCorrigeClick(Sender: TObject);
var
  Nome, Trato: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE listalograd SET ');
    Dmod.QrUpd.SQL.Add('Nome=:P0, Trato=:P1 WHERE Codigo=:P2');
    //
    QrListaLograd.Close;
    UnDmkDAC_PF.AbreQuery(QrListaLograd, Dmod.MyDB);
    while not QrListaLograd.Eof do
    begin
      if CkMaiusculas.Checked then
      begin
        Nome  := Geral.Maiusculas(QrListaLogradNome.Value, False);
        Trato := Geral.Maiusculas(QrListaLogradTrato.Value, False);
      end else begin
        Nome  := Geral.Minusculas(QrListaLogradNome.Value, False);
        if Length(Nome) > 0 then
          Nome := Geral.Maiusculas(Nome[1], False) + Copy(Nome, 2);
        Trato := Geral.Minusculas(QrListaLogradTrato.Value, False);
        //if Length(Trato) > 0 then
          //Trato := Geral.Maiusculas(Trato[1], False) + Copy(Trato, 2);
      end;
      Dmod.QrUpd.Params[00].AsString  := Nome;
      Dmod.QrUpd.Params[01].AsString  := Trato;
      Dmod.QrUpd.Params[02].AsInteger := QrListaLogradCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      QrListaLograd.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOpcoes.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmOpcoes.EdImpreChPortaChange(Sender: TObject);
begin
  VAR_IMPCHPORTA := EdImpreChPorta.ValueVariant;
end;

procedure TFmOpcoes.EdImpreCh_IPChange(Sender: TObject);
begin
  VAR_IMPCH_IP := EdImpreCh_IP.ValueVariant;
end;

procedure TFmOpcoes.EdLogo0PathKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if OpenPictureDialog1.Execute then EdLogo0Path.Text :=
      OpenPictureDialog1.FileName;
  end;
end;

procedure TFmOpcoes.EdLogoBig1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if OpenPictureDialog1.Execute then EdLogoBig1.Text :=
      OpenPictureDialog1.FileName;
  end;
end;

procedure TFmOpcoes.EdMeuLogoPathKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if OpenPictureDialog2.Execute then EdMeuLogoPath.Text :=
      OpenPictureDialog2.FileName;
  end;
end;

procedure TFmOpcoes.EdLogoNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if OpenPictureDialog1.Execute then EdLogoNF.Text :=
      OpenPictureDialog1.FileName;
  end;
end;

procedure TFmOpcoes.CkMaiusculasClick(Sender: TObject);
begin
end;

{$IfNDef SemCotacoes}
procedure TFmOpcoes.ReopenCambioUsu(Codigo: Integer);
begin
  QrCambioUsu.Close;
  UnDmkDAC_PF.AbreQuery(QrCambioUsu, Dmod.MyDB);
  //
  QrCambioUsu.Locate('Numero', Codigo, []);
end;
{$EndIf}

end.



