unit Curinga;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  Grids, DBGrids, ZCF2, UnInternalConsts, dmkImage, dmkGeral, ComCtrls,
  UnMySQLCuringa, mySQLDbTables, UnDmkEnums;

type
  TFmCuringa = class(TForm)
    PainelDados: TPanel;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    PnPesquisa: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    TBTam: TTrackBar;
    RGMascara: TRadioGroup;
    BtSelecionado: TBitBtn;
    BtTodos: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure TBTamChange(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure RGMascaraClick(Sender: TObject);
    procedure BtSelecionadoClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReabrePesquisa();
    procedure VerificaOrigem();
  public
    { Public declarations }
    FPodeReabrirPesquisa: Boolean;
    FOrigemPesquisa: TFormaPesquisa; // = (fpcNenhum, fpcTexto, fpc2, fpc3, fpc4, fpc2Nomes);
    FTextoAPesquisar, FNomeDoCampoCodigo, FNomeDoCampoNome1, FNomeDoCampoNome2,
    FNomeDoCampoNome3, FNomeDoCampoNome4, FNomeDoCampoNome5, FNomeDoCampoNome6,
    FNomeDoCampoNome7, FNomeDoCampoNome8, FNomeDaTabela, FNomeDoCampoTipo: String;
    FTipo1: Integer;
    FSohNum: Boolean;
    FDataBase: TmySQLDatabase;
    FExtra, FLEFT_JOIN: string;
    //
    FSQL_Livre: String;
  end;

var
  FmCuringa: TFmCuringa;

implementation

uses UnMyObjects, Module, ModuleGeral;

{$R *.DFM}

procedure TFmCuringa.BtSelecionadoClick(Sender: TObject);
begin
  VerificaOrigem;
  Close;
end;

procedure TFmCuringa.BtTodosClick(Sender: TObject);
begin
  VAR_CORDA_CODIGOS := MyObjects.CordaDeQuery(TmySQLQuery(DBGrid1.DataSource.DataSet),
  DBGrid1.Columns[0].FieldName, EmptyStr);
  if VAR_CORDA_CODIGOS <> EmptyStr then
  begin
    VAR_EXEC_CURINGA := True;
    VAR_TextoPesq_CURINGA := EdPesq.Text;
    Close;
  end;
end;

procedure TFmCuringa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCuringa.VerificaOrigem();
begin
  if FOrigemPesquisa = fpcNenhum then
  begin
    if DmodG.QrSB.State <> dsInactive then
      FOrigemPesquisa := fpcTexto
    else
    if DmodG.QrSB2.State <> dsInactive then
      FOrigemPesquisa := fpc2
    else
    if DmodG.QrSB3.State <> dsInactive then
      FOrigemPesquisa := fpc3
    else
    if DmodG.QrSB4.State <> dsInactive then
      FOrigemPesquisa := fpc4
    else
    if DmodG.QrSB5.State <> dsInactive then
      FOrigemPesquisa := fpc5
  end;
  if (FOrigemPesquisa = fpcTexto)
  or (FOrigemPesquisa = fpc2Nomes) then
  begin
    if DmodG.QrSB.RecordCount > 0 then
    begin
      VAR_CODIGO := DmodG.QrSBCodigo.Value;
      VAR_CODUSU := DmodG.QrSBCodUsu.Value;
      VAR_NOME_X := DmodG.QrSBNome.Value;
    end else begin
      VAR_CODIGO := 0;
      VAR_CODUSU := 0;
      VAR_NOME_X := '';
    end;
  end
  else
  if FOrigemPesquisa = fpc2 then
  begin
    if DmodG.QrSB2.RecordCount > 0 then
    begin
      VAR_CODIGO2 := DmodG.QrSB2Codigo.Value;
      VAR_CODUSU := DmodG.QrSB2CodUsu.Value;
      VAR_NOME_X := DmodG.QrSB2Nome.Value;
    end else begin
      VAR_CODIGO2 := 0;
      VAR_CODUSU := 0;
      VAR_NOME_X := '';
    end;
  end
  else
  if FOrigemPesquisa = fpc3 then
  begin
    if DmodG.QrSB3.RecordCount > 0 then
    begin
      VAR_CODIGO3 := DmodG.QrSB3Codigo.Value;
      VAR_CODUSU := DmodG.QrSB3CodUsu.Value;
      VAR_NOME_X := DmodG.QrSB3Nome.Value;
    end else begin
      VAR_CODIGO3 := 0;
      VAR_CODUSU := 0;
      VAR_NOME_X := '';
    end;
  end
  else
  if FOrigemPesquisa = fpc4 then
  begin
    if DmodG.QrSB4.RecordCount > 0 then
    begin
      VAR_CODIGO4 := DmodG.QrSB4Codigo.Value;
      VAR_TXTUSU := DmodG.QrSB4CodUsu.Value;
      VAR_NOME_X := DmodG.QrSB4Nome.Value;
    end else begin
      VAR_CODIGO4 := '';
      VAR_TXTUSU := '';
      VAR_NOME_X := '';
    end;
  end
  else
  if FOrigemPesquisa = fpc5 then
  begin
    if DmodG.QrSB5.RecordCount > 0 then
    begin
      VAR_CODIGO5 := DmodG.QrSB5Codigo.Value;
      //VAR_TXTUSU := DmodG.QrSB5CodUsu.Value;
      VAR_NOME_X := DmodG.QrSB5Nome.Value;
    end else begin
      VAR_CODIGO5 := 0;
      VAR_TXTUSU := '';
      VAR_NOME_X := '';
    end;
  end else
  if FOrigemPesquisa = fpcSQL_Livre then
  begin
    if DmodG.QrSB.RecordCount > 0 then
    begin
      VAR_CODIGO := DmodG.QrSBCodigo.Value;
      //VAR_TXTUSU := DmodG.QrSB5CodUsu.Value;
      VAR_NOME_X := DmodG.QrSBNome.Value;
    end else begin
      VAR_CODIGO := 0;
      VAR_TXTUSU := '';
      VAR_NOME_X := '';
    end;
  end else
  begin
    Geral.MB_ERRO('Tipo de origem de pesquisa n�o implemetado!');
    VAR_CODIGO4 := '';
    VAR_TXTUSU := '';
    VAR_NOME_X := '';
  end;
  VAR_EXEC_CURINGA := True;
  VAR_TextoPesq_CURINGA := EdPesq.Text;
end;

procedure TFmCuringa.DBGrid1CellClick(Column: TColumn);
begin
  VerificaOrigem;
end;

procedure TFmCuringa.DBGrid1DblClick(Sender: TObject);
begin
  VerificaOrigem;
  Close;
end;

procedure TFmCuringa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCuringa.FormCreate(Sender: TObject);
begin
  FSQL_Livre           := EmptyStr;
  VAR_EXEC_CURINGA     := True;
  VAR_CORDA_CODIGOS    := '';
  FPodeReabrirPesquisa := False;
  FOrigemPesquisa      := fpcNenhum;
  FNomeDoCampoTipo     := '';
  FTipo1               := 0;
  FSohNum              := False;
  //
  ImgTipo.SQLType := stPsq;
  DBGrid1.DataSource := DModG.DsSB;
end;

procedure TFmCuringa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCuringa.ReabrePesquisa();
var
  //SQL,
  Erro: String;
  //I, N,
  K: Integer;
begin
  if not FPodeReabrirPesquisa then
    Exit;
  TmySQLQuery(TDataSource(DBGrid1.DataSource).DataSet).Close;
  //
  K := Length(EdPesq.Text);
(*
  N := TBTam.Position;
  if N < 3 then
    N := 3;
*)
  if K >= 3 then
  begin
(*
    SQL := '';
    for I := 2 to K - N + 1 do
      SQL := SQL + 'OR (' + FFldNome + ' LIKE "%' + Copy(Texto, I, N) + '%")' + sLineBreak;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
    'SELECT ' + FFldID + ', ' + FFldNome,
    'FROM ' + TbCad.TableName,
    'WHERE (' + FFldNome + ' LIKE "%' + Copy(Texto, 1, N) + '%")',
    SQL,
    '']);
*)
    FTextoAPesquisar := CuringaLoc.ObtemSQLPesquisaText(RGMascara.ItemIndex, EdPesq.Text);
    //
    case FOrigemPesquisa of
      fpcTexto: CuringaLoc.ExecutouPesq01(FTextoAPesquisar, FNomeDoCampoCodigo,
      FNomeDoCampoNome1, FNomeDaTabela, FDataBase, FExtra, FLEFT_JOIN, Erro);
      //
      fpc2Nomes:
      begin
        VAR_NOME_S := FTextoAPesquisar;
        CuringaLoc.ExecutouPesq05(FNomeDoCampoCodigo, FNomeDoCampoNome1,
        FNomeDoCampoNome2, FNomeDoCampoNome3, FNomeDoCampoNome4,
        FNomeDoCampoNome5, FNomeDoCampoNome6, FNomeDoCampoNome7,
        FNomeDoCampoNome8, FNomeDaTabela,
        FNomeDoCampoTipo, FTipo1, FDataBase, FExtra, FSohNum, Erro)
      end;
      fpcSQL_Livre:
      begin
        CuringaLoc.ExecutouPesqLivre(FTextoAPesquisar, FSQL_Livre, FDataBase, Erro);
      end
      else Geral.MB_Erro('Origem de pesquisa n�o implementado!' + sLineBreak +
      'Avise a DERMATEK!');
    end;
  end;  
end;

procedure TFmCuringa.RGMascaraClick(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmCuringa.TBTamChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmCuringa.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
  begin
    VerificaOrigem;
    Close;
  end;
end;

procedure TFmCuringa.EdPesqChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

end.
