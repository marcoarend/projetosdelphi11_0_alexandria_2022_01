unit PesqESel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, mySQLDbTables,
  dmkGeral, dmkEdit, dmkEditF7, DBCtrls, (*DBTables,*) dmkDBLookupComboBox,
  dmkDBGrid, dmkDBGridDAC, dmkImage, UnDmkEnums;

type
  TTpTab = (tptPesquisa, tptSeleciona);
  TFmPesqESel = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    EdPesq: TdmkEdit;
    EdDoc: TdmkEdit;
    LaDoc: TLabel;
    QrPsq: TmySQLQuery;
    DsPsq: TDataSource;
    QrSel: TmySQLQuery;
    DsSel: TDataSource;
    QrPsqCodigo: TIntegerField;
    QrPsqNome: TWideStringField;
    QrPsqAtivo: TSmallintField;
    QrSelCodigo: TIntegerField;
    QrSelNome: TWideStringField;
    QrSelAtivo: TSmallintField;
    Panel5: TPanel;
    DBGSel: TdmkDBGridDAC;
    Panel7: TPanel;
    DBGPsq: TdmkDBGridDAC;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    BtTodosP: TBitBtn;
    BtNenhumP: TBitBtn;
    BtOK: TBitBtn;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    BtTodosS: TBitBtn;
    BtNenhumS: TBitBtn;
    BitBtn1: TBitBtn;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosPClick(Sender: TObject);
    procedure BtNenhumPClick(Sender: TObject);
    procedure BtTodosSClick(Sender: TObject);
    procedure BtNenhumSClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    procedure AtivaItens(Status: Integer; TipoTab: TTpTab);
    procedure ReopenPsq(Codigo: Integer);
    procedure ReopenSel(Codigo: Integer);
  public
    { Public declarations }
    FCB: TDBLookupCombobox;
    FF7: TdmkEdit;
    FND: Boolean;
    FNomeTabela, FNomeCampoNome, FNomeCampoCodigo: String;
    //
    FPesqESel_Psq, FPesqESel_Sel: String;
  end;

  var
  FmPesqESel: TFmPesqESel;

implementation

uses UnMyObjects, UnInternalConsts, Module, ModuleGeral, UMySQLModule,
DmkDAC_PF;

{$R *.DFM}

procedure TFmPesqESel.AtivaItens(Status: Integer; TipoTab: TTpTab);
var
  Tabela: String;
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  case TipoTab of
    tptPesquisa:  Tabela := 'pesqesel_psq';
    tptSeleciona: Tabela := 'pesqesel_sel';
  end;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + Tabela);
  DModG.QrUpdPID1.SQL.Add('SET Ativo=' + FormatFloat('0', Status));
  DModG.QrUpdPID1.ExecSQL;
  //
  Codigo := 0;
  case TipoTab of
    tptPesquisa:  if QrPsq.State <> dsInactive then Codigo := QrPsqCodigo.Value;
    tptSeleciona: if QrSel.State <> dsInactive then Codigo := QrSelCodigo.Value;
  end;

  case TipoTab of
    tptPesquisa:  ReopenPsq(Codigo);
    tptSeleciona: ReopenSel(Codigo);
  end;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPesqESel.BitBtn1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add(DELETE_FROM + ' pesqesel_sel');
    DModG.QrUpdPID1.SQL.Add('WHERE Ativo=1');
    UMyMod.ExecutaQuery(DModG.QrUpdPID1);
    //
    if QrSel.RecordCount > 0 then
      Codigo := QrSelCodigo.Value
    else
      Codigo := 0;
    //
    ReopenSel(Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesqESel.BtNenhumPClick(Sender: TObject);
begin
  AtivaItens(0, tptPesquisa);
end;

procedure TFmPesqESel.BtNenhumSClick(Sender: TObject);
begin
  AtivaItens(0, tptSeleciona);
end;

procedure TFmPesqESel.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO  pesqesel_sel');
    DModG.QrUpdPID1.SQL.Add('SELECT Codigo, Nome, 0 Ativo');
    DModG.QrUpdPID1.SQL.Add('FROM pesqesel_psq');
    DModG.QrUpdPID1.SQL.Add('WHERE Ativo = 1');
    DModG.QrUpdPID1.SQL.Add('AND Codigo NOT IN (');
    DModG.QrUpdPID1.SQL.Add('  SELECT Codigo');
    DModG.QrUpdPID1.SQL.Add('  FROM pesqesel_sel');
    DModG.QrUpdPID1.SQL.Add(')');
    UMyMod.ExecutaQuery(DModG.QrUpdPID1);
    //
    if QrSel.RecordCount > 0 then
      Codigo := QrSelCodigo.Value
    else
      Codigo := 0;
    //
    ReopenSel(Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesqESel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqESel.BtTodosPClick(Sender: TObject);
begin
  AtivaItens(1, tptPesquisa);
end;

procedure TFmPesqESel.BtTodosSClick(Sender: TObject);
begin
  AtivaItens(1, tptSeleciona);
end;

procedure TFmPesqESel.EdPesqChange(Sender: TObject);
var
  Texto, Doc: String;
  //Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    DModG.QrUpdPID1.Close;
    Texto := EdPesq.ValueVariant;
    Doc := Geral.SoNumero_TT(EdDoc.Text);
    if (Length(Texto) > 2) or (Length(Doc) > 10) then
    begin
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add(DELETE_FROM + ' pesqesel_psq;');
      DModG.QrUpdPID1.SQL.Add('INSERT INTO  pesqesel_psq');
      if FNomeTabela = 'entidades' then
      begin
        DModG.QrUpdPID1.SQL.Add('SELECT Codigo , IF(Tipo=0,');
        DModG.QrUpdPID1.SQL.Add('CONCAT(RazaoSocial, " ", Fantasia),');
        DModG.QrUpdPID1.SQL.Add('CONCAT(Nome, " ", Apelido)) Nome, 0 Ativo');
        DModG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.entidades');
        DModG.QrUpdPID1.SQL.Add('WHERE (RazaoSocial LIKE "%' + Texto + '%"');
        DModG.QrUpdPID1.SQL.Add('OR Nome LIKE "%' + Texto + '%"');
        DModG.QrUpdPID1.SQL.Add('OR Fantasia LIKE "%' + Texto + '%"');
        DModG.QrUpdPID1.SQL.Add('OR Apelido LIKE "%' + Texto + '%");');
        if EdDoc.Visible then
        begin
          if Doc <> '' then
            DModG.QrUpdPID1.SQL.Add('AND ((CNPJ="' + Doc + '") OR (CPF = "' + Doc + '"))');
        end;
        DModG.QrUpdPID1.SQL.Add('ORDER BY _Nome');
      end else begin
        DModG.QrUpdPID1.SQL.Add('SELECT ' + FNomeCampoCodigo + ' Codigo, ' + FNomeCampoNome +
        ' Nome, 0 Ativo FROM ' + TMeuDB + '.' + FNomeTabela + ' WHERE ' + FNomeCampoNome +
        ' LIKE "%' + Texto + '%";');
      end;
      UMyMod.ExecutaQuery(DModG.QrUpdPID1);
      ReopenPsq(0);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesqESel.FormActivate(Sender: TObject);
var
  Query: TmySQLQuery;
  Texto: String;
  P, P10, P13: Integer;
begin
  MyObjects.CorIniComponente();
  if FCB <> nil then
  begin
    Query := TmySQLQuery(FCB.ListSource.DataSet);
    if Query <> nil then
    begin
      Texto := Lowercase(Query.SQL.Text);
      P := pos('from', Texto);
      if P > 0 then
      begin
        Texto := Trim(Copy(Texto, P + 4));
        P := pos(' ', Texto);
        P10 := pos(#10, Texto);
        P13 := pos(#13, Texto);
        if (P13 < P) and (P13 > 0) then P := P13;
        if (P10 < P) and (P10 > 0) then P := P10;
        if P > 0 then
        begin
          FNomeTabela := Trim(Copy(Texto, 1, P));
          FNomeCampoNome   := FCB.ListField;
          FNomeCampoCodigo := FCB.KeyField;
          //
          if FCB is TdmkDBLookupComboBox then
          begin
            if TdmkDBLookupComboBox(FCB).LocF7TableName <> '' then
              FNometabela := TdmkDBLookupComboBox(FCB).LocF7TableName;
            if TdmkDBLookupComboBox(FCB).LocF7CodiFldName <> '' then
              FNomeCampoCodigo := TdmkDBLookupComboBox(FCB).LocF7CodiFldName;
            if TdmkDBLookupComboBox(FCB).LocF7NameFldName <> '' then
              FNomeCampoNome := TdmkDBLookupComboBox(FCB).LocF7NameFldName;
          end;
        end;
      end;
    end;
  end
  else
  if FF7 is TdmkEditF7 then
  begin
    if TdmkEditF7(FF7).LocF7TableName <> '' then
      FNometabela := TdmkEditF7(FF7).LocF7TableName;

    if TdmkEditF7(FF7).LocF7CodiFldName <> '' then
      FNomeCampoCodigo := TdmkEditF7(FF7).LocF7CodiFldName;

    if TdmkEditF7(FF7).LocF7NameFldName <> '' then
      FNomeCampoNome := TdmkEditF7(FF7).LocF7NameFldName;
  end else
  if FND then
  begin
    //
  end;

  //

  if FNomeTabela = 'entidades' then
  begin
    LaDoc.Visible := True;
    EdDoc.Visible := True;
    EdPesq.Width  := 648;
  end;
end;

procedure TFmPesqESel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  FND := False;
  //
  QrPsq.Close;
  QrPsq.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreQuery(QrPsq, DModG.MyPID_DB);
  //
  QrSel.Close;
  QrSel.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreQuery(QrSel, DModG.MyPID_DB);
end;

procedure TFmPesqESel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesqESel.ReopenPsq(Codigo: Integer);
begin
  QrPsq.Close;
  UnDmkDAC_PF.AbreQuery(QrPsq, DModG.MyPID_DB);
  QrPsq.Locate('Codigo', Codigo, []);
end;

procedure TFmPesqESel.ReopenSel(Codigo: Integer);
begin
  QrSel.Close;
  UnDmkDAC_PF.AbreQuery(QrSel, DModG.MyPID_DB);
  QrSel.Locate('Codigo', Codigo, []);
end;

end.
