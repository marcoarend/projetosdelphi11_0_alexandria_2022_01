object FmPeriodo: TFmPeriodo
  Left = 440
  Top = 274
  Caption = 'Defini'#231#227'o de per'#237'odo'
  ClientHeight = 144
  ClientWidth = 307
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 307
    Height = 96
    Align = alClient
    TabOrder = 0
    object LaMes: TLabel
      Left = 16
      Top = 28
      Width = 23
      Height = 13
      Caption = 'M'#234's:'
    end
    object LaAno: TLabel
      Left = 208
      Top = 28
      Width = 22
      Height = 13
      Caption = 'Ano:'
    end
    object LaSelMes: TLabel
      Left = 16
      Top = 76
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object LaSelAno: TLabel
      Left = 208
      Top = 76
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object CBMes: TComboBox
      Left = 16
      Top = 45
      Width = 182
      Height = 21
      Color = clWhite
      DropDownCount = 12
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Text = 'CBMes'
    end
    object CBAno: TComboBox
      Left = 208
      Top = 45
      Width = 90
      Height = 21
      Color = clWhite
      DropDownCount = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      Text = 'CBAno'
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 96
    Width = 307
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 16
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 208
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
end
