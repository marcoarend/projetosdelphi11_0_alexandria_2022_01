object FmSenha: TFmSenha
  Left = 605
  Top = 310
  Caption = 'AD2-SENHA-001 :: Senha'
  ClientHeight = 317
  ClientWidth = 401
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 401
    Height = 155
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object LaValor: TLabel
      Left = 240
      Top = 72
      Width = 27
      Height = 13
      Caption = 'Valor:'
      Transparent = True
      Visible = False
    end
    object LaBase: TLabel
      Left = 216
      Top = 48
      Width = 21
      Height = 13
      Caption = '0,00'
      Visible = False
    end
    object LaPorcentagem: TLabel
      Left = 240
      Top = 28
      Width = 66
      Height = 13
      Caption = 'Porcentagem:'
      Transparent = True
      Visible = False
    end
    object LaSenha: TLabel
      Left = 60
      Top = 72
      Width = 34
      Height = 13
      Caption = 'Senha:'
      Transparent = True
    end
    object LaLogin: TLabel
      Left = 60
      Top = 24
      Width = 29
      Height = 13
      Caption = 'Login:'
      Transparent = True
      Visible = False
    end
    object LaData: TLabel
      Left = 240
      Top = 116
      Width = 26
      Height = 13
      Caption = 'Data:'
      Visible = False
    end
    object LaJanela: TLabel
      Left = 4
      Top = 4
      Width = 43
      Height = 13
      Caption = 'LaJanela'
      Visible = False
    end
    object LaSenhaExtra: TLabel
      Left = 136
      Top = 16
      Width = 55
      Height = 13
      Caption = 'SenhaExtra'
      Visible = False
    end
    object EdValor: TdmkEdit
      Left = 240
      Top = 88
      Width = 149
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdValorExit
    end
    object EdPorcentagem: TdmkEdit
      Left = 240
      Top = 44
      Width = 149
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdPorcentagemExit
    end
    object EdSenha: TEdit
      Left = 60
      Top = 88
      Width = 149
      Height = 22
      Color = clWhite
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 1
    end
    object EdLogin: TEdit
      Left = 60
      Top = 44
      Width = 149
      Height = 22
      Color = clWhite
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 0
      Visible = False
    end
    object TPData: TdmkEditDateTimePicker
      Left = 240
      Top = 132
      Width = 149
      Height = 21
      Date = 39018.000000000000000000
      Time = 0.457153136601846200
      TabOrder = 4
      Visible = False
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 401
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 353
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 305
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 78
        Height = 32
        Caption = 'Senha'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 78
        Height = 32
        Caption = 'Senha'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 78
        Height = 32
        Caption = 'Senha'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 203
    Width = 401
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 397
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 247
    Width = 401
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 269
      Top = 15
      Width = 130
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtCancela: TBitBtn
        Tag = 15
        Left = 4
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtCancelaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 267
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 4
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrSenhas1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM senhas'
      'WHERE Login=:P0'
      'AND Senha=:P1')
    Left = 4
    Top = 84
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhas1Login: TWideStringField
      FieldName = 'Login'
      Origin = 'DNEMPORIUM.senhas.Login'
      Size = 128
    end
    object QrSenhas1Numero: TIntegerField
      FieldName = 'Numero'
      Origin = 'DNEMPORIUM.senhas.Numero'
    end
    object QrSenhas1Senha: TWideStringField
      FieldName = 'Senha'
      Origin = 'DNEMPORIUM.senhas.Senha'
      Size = 128
    end
    object QrSenhas1Perfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'DNEMPORIUM.senhas.Perfil'
    end
    object QrSenhas1Lk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DNEMPORIUM.senhas.Lk'
    end
  end
  object QrPerfis1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM perfis'
      'WHERE Codigo=:P0')
    Left = 4
    Top = 112
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrSenha: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Perfil '
      'FROM senhas'
      'WHERE Login = :P0'
      'AND AES_DECRYPT(Senha, :P1)  = :P2')
    Left = 152
    Top = 135
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSenhaPerfil: TIntegerField
      FieldName = 'Perfil'
    end
  end
  object QrPerfisItsPerf: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM perfisits pit'
      'LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela '
      'WHERE pip.Codigo=:P0'
      'AND pip.Janela=:P1')
    Left = 152
    Top = 164
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPerfisItsPerfLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisItsPerfJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 255
    end
  end
end
