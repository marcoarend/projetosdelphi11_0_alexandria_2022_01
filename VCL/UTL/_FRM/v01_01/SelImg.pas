unit SelImg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, RSSVGCtrls,
  RSGdiPlusCtrls, UnDmkProcFunc;

type
  TFmSelImg = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label2: TLabel;
    EdDiretorio: TdmkEdit;
    SbVetorSVG: TSpeedButton;
    LBItens: TListBox;
    RSSVGImage1: TRSSVGImage;
    RSSVGDocument1: TRSSVGDocument;
    Image1: TImage;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbVetorSVGClick(Sender: TObject);
    procedure LBItensClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    //function GetFileList(const Path, Ext: String): TStringList;
    procedure CarregaListaArquivos(Diretorio: String);
    procedure SelecionaImagem(Index: Integer);
  public
    { Public declarations }
    FTravaDir: Boolean;
    FExt: TStringList;
    FArquivoSel, FArquivo: String;
  end;

  var
  FmSelImg: TFmSelImg;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

(*
function TFmSelSVG.GetFileList(const Path, Ext: String): TStringList;
var
  I: Integer;
  SearchRec: TSearchRec;
begin
  Result := TStringList.Create;
  try
    I := FindFirst(Path + '\*.*', 0, SearchRec);
    while I = 0 do
    begin
      if Ext <> '' then
      begin
        if LowerCase(ExtractFileExt(SearchRec.Name)) = LowerCase(Ext) then
          Result.Add(SearchRec.Name);
      end else
        Result.Add(SearchRec.Name);
      I := FindNext(SearchRec);
    end;
  except
    Result.Free;
    raise;
  end;
end;
*)

procedure TFmSelImg.BtOKClick(Sender: TObject);
begin
  if LBItens.ItemIndex > -1 then
    FArquivo := EdDiretorio.ValueVariant + '\' + LBItens.Items[LBItens.ItemIndex]
  else
    FArquivo := '';
  //
  Close;
end;

procedure TFmSelImg.BtSaidaClick(Sender: TObject);
begin
  FArquivo := '';
  Close;
end;

procedure TFmSelImg.CarregaListaArquivos(Diretorio: String);
var
  I: Integer;
  Ext: array of string;
begin
  if FExt.Count = 0 then
  begin
    Geral.MB_Aviso('Lista de extens�es v�lidas n�o definida!');
    Exit;
  end;
  SetLength(Ext, FExt.Count);
  //
  for I := 0 to FExt.Count - 1 do
    Ext[I] := FExt[I];
  //
  LBItens.Items.Clear;
  LBItens.Items := dmkPF.GetFileList(EdDiretorio.ValueVariant, Ext);
  //
  if FArquivoSel <> '' then
  begin
    I := LBItens.Items.IndexOf(FArquivoSel);
    //
    if I > -1 then
    begin
      LBItens.ItemIndex := I;
      SelecionaImagem(I);
    end;
  end;
end;

procedure TFmSelImg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSelImg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FTravaDir       := False;
end;

procedure TFmSelImg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSelImg.FormShow(Sender: TObject);
begin
  LBItens.Items.Clear;
  //
  if EdDiretorio.ValueVariant = '' then
    EdDiretorio.ValueVariant := CO_DIR_RAIZ_DMK + '\Imagens';
  //
  CarregaListaArquivos(EdDiretorio.ValueVariant);
  //
  EdDiretorio.Enabled := not FTravaDir;
  SbVetorSVG.Enabled  := not FTravaDir;
end;

procedure TFmSelImg.LBItensClick(Sender: TObject);
begin
  if (EdDiretorio.ValueVariant <> '') then
    SelecionaImagem(LBItens.ItemIndex);
end;

procedure TFmSelImg.SbVetorSVGClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDiretorio);
  //
  if EdDiretorio.ValueVariant <> '' then
    CarregaListaArquivos(EdDiretorio.ValueVariant)
end;

procedure TFmSelImg.SelecionaImagem(Index: Integer);
var
  Arquivo, Ext: String;
begin
  if Index < 0 then
    Exit;
  //
  Arquivo := EdDiretorio.ValueVariant + '\' + LBItens.Items[Index];
  Ext     := LowerCase(ExtractFileExt(Arquivo));
  //
  if Ext = '.svg' then
  begin
    Image1.Visible := False;
    //
    RSSVGImage1.Visible := True;
    RSSVGImage1.Align   := alClient;
    //
    RSSVGDocument1.SVG.Clear;
    RSSVGDocument1.SVG.LoadFromFile(Arquivo);
  end else
  begin
    RSSVGImage1.Visible := False;
    //
    Image1.Visible := True;
    Image1.Align   := alClient;
    //
    Image1.Picture.LoadFromFile(Arquivo);
  end;
end;

end.
