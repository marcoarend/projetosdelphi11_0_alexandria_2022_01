unit ZStepCodUni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, Grids, DBGrids, DB,
  mySQLDbTables, dmkGeral, dmkLabel, Menus, ComCtrls, dmkImage, UnDmkEnums;

type
  TFmZStepCodUni = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    EdTabela: TdmkEdit;
    PnEdita: TPanel;
    DBGrid1: TDBGrid;
    QrZStepCod: TmySQLQuery;
    DsZStepCod: TDataSource;
    QrZStepCodTabela: TWideStringField;
    QrZStepCodDescricao: TWideStringField;
    QrZStepCodCodIni: TIntegerField;
    QrZStepCodCodFim: TIntegerField;
    PnConfirma: TPanel;
    BitBtn1: TBitBtn;
    Panel6: TPanel;
    BitBtn2: TBitBtn;
    EdTabDescri: TdmkEdit;
    GroupBox4: TGroupBox;
    Label9: TLabel;
    EdDescricao: TdmkEdit;
    EdCodIni: TdmkEdit;
    EdCodFim: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    QrMinMax: TmySQLQuery;
    QrMinMaxMin: TIntegerField;
    QrMinMaxMax: TIntegerField;
    QrJaExiste: TmySQLQuery;
    QrJaExisteDescricao: TWideStringField;
    PMIntervalos: TPopupMenu;
    Incluinovointervalo1: TMenuItem;
    Alteraintervaloatual1: TMenuItem;
    Excluiintervalos1: TMenuItem;
    PMCodUsu: TPopupMenu;
    Prximo1: TMenuItem;
    Primeirodisponvel1: TMenuItem;
    EdResult: TdmkEdit;
    QrPesq: TmySQLQuery;
    QrPesqMaximo: TIntegerField;
    QrDisp: TmySQLQuery;
    QrDispCodUsu: TIntegerField;
    PB1: TProgressBar;
    QrPesqCadastros: TLargeintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnGerencia: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtCodUsu: TBitBtn;
    BtIntervalos: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIntervalosClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrZStepCodBeforeClose(DataSet: TDataSet);
    procedure QrZStepCodAfterOpen(DataSet: TDataSet);
    procedure Incluinovointervalo1Click(Sender: TObject);
    procedure Alteraintervaloatual1Click(Sender: TObject);
    procedure Excluiintervalos1Click(Sender: TObject);
    procedure BtCodUsuClick(Sender: TObject);
    procedure Prximo1Click(Sender: TObject);
    procedure PMCodUsuPopup(Sender: TObject);
    procedure Primeirodisponvel1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function ImpedePorDuplicidade(): Boolean;
  public
    { Public declarations }
    FResult: Integer;
    procedure ReopenZStepCod();
  end;

  var
  FmZStepCodUni: TFmZStepCodUni;

implementation

uses UnMyObjects, Module, UMySQLModule, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmZStepCodUni.Alteraintervaloatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrZStepCod, [PnGerencia],
  [PnEdita], EdDescricao, ImgTipo, 'zstepcod');
end;

procedure TFmZStepCodUni.BitBtn1Click(Sender: TObject);
var
  Nome: String;
begin
  Nome := EdDescricao.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  if ImpedePorDuplicidade() then Exit;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, Panel1,
    'ZStepCod', EdTabela.Text, Dmod.QrUpd, [PnEdita], [PnGerencia], ImgTipo, True) then
  begin
    ReopenZStepCod;
  end;
end;

procedure TFmZStepCodUni.BitBtn2Click(Sender: TObject);
begin
  UMyMod.TravaFmEmPanelInsUpd([PnEdita], [PnGerencia], ImgTipo);
end;

procedure TFmZStepCodUni.BtIntervalosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIntervalos, BtIntervalos);
end;

procedure TFmZStepCodUni.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmZStepCodUni.BtCodUsuClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  //
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT COUNT(CodUsu)+ :P0 Cadastros,');
  QrPesq.SQL.Add('MAX(CodUsu) Maximo');
  QrPesq.SQL.Add('FROM ' + Lowercase(EdTabela.Text));
  QrPesq.SQL.Add('WHERE CodUsu BETWEEN :P1 AND :P2');
  QrPesq.Params[00].AsInteger := QrZStepCodCodIni.Value-1;
  QrPesq.Params[01].AsInteger := QrZStepCodCodIni.Value;
  QrPesq.Params[02].AsInteger := QrZStepCodCodFim.Value;
  UnDmkDAC_PF.AbreQuery(QrPesq, Dmod.MyDB);
  //
  Screen.Cursor := crDefault;
  MyObjects.MostraPopUpDeBotao(PMCodUsu, BtCodUsu);
end;

procedure TFmZStepCodUni.Excluiintervalos1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrZStepCod, DBGrid1,
    'ZStepCod', ['Tabela', 'Descricao'], ['Tabela', 'Descricao'], istPergunta, '');
end;

procedure TFmZStepCodUni.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmZStepCodUni.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FResult := 0;
end;

procedure TFmZStepCodUni.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmZStepCodUni.ReopenZStepCod;
begin
  QrZStepCod.Close;
  QrZStepCod.Params[0].AsString := EdTabela.Text;
  UnDmkDAC_PF.AbreQuery(QrZStepCod, Dmod.MyDB);
end;

function TFmZStepCodUni.ImpedePorDuplicidade(): Boolean;
var
  i, Ini, Fim: Integer;
begin
  Result := False;
  if ImgTipo.SQLType <> stIns then Exit;
  Ini := EdCodIni.ValueVariant;
  Fim := EdCodFim.ValueVariant;
  QrMinMax.Close;
  QrMinMax.Params[0].AsString := EdTabela.Text;
  UnDmkDAC_PF.AbreQuery(QrMinMax, Dmod.MyDB);
  //
  if Ini < QrMinMaxMin.Value then
  begin
    if Fim < QrMinMaxMin.Value then
      Exit
    else Ini := QrMinMaxMin.Value;
  end;
  if Fim > QrMinMaxMax.Value then
  begin
    if Ini > QrMinMaxMax.Value then
      Exit
    else Fim := QrMinMaxMax.Value;
  end;
  //
  for i := Ini to Fim do
  begin
    QrJaExiste.Close;
    QrJaExiste.Params[00].AsString  := EdTabela.Text;
    QrJaExiste.Params[01].AsInteger := i;
    UnDmkDAC_PF.AbreQuery(QrJaExiste, Dmod.MyDB);
    //
    if QrJaExiste.RecordCount > 0 then
    begin
      Result := True;
      Geral.MB_Aviso('A faixa de numera��o est� em conflito com ' +
      'o cadastro "' + QrJaExisteDescricao.Value + '"!');
      Break;
    end;
  end;
end;

procedure TFmZStepCodUni.Incluinovointervalo1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrZStepCod, [PnGerencia],
  [PnEdita], EdDescricao, ImgTipo, 'zstepcod');
end;

procedure TFmZStepCodUni.PMCodUsuPopup(Sender: TObject);
begin
  Primeirodisponvel1.Enabled := QrPesqCadastros.Value < QrPesqMaximo.Value;
end;

procedure TFmZStepCodUni.Primeirodisponvel1Click(Sender: TObject);
var
  n: Integer;
begin
  Screen.Cursor := crHourGlass;
  QrDisp.Close;
  QrDisp.SQL.Clear;
  QrDisp.SQL.Add('SELECT CodUsu');
  QrDisp.SQL.Add('FROM ' + Lowercase(EdTabela.Text));
  QrDisp.SQL.Add('WHERE CodUsu BETWEEN :P0 AND :P1');
  QrDisp.SQL.Add('ORDER BY CodUsu');
  QrDisp.Params[00].AsInteger := QrZStepCodCodIni.Value;
  QrDisp.Params[01].AsInteger := QrZStepCodCodFim.Value;
  UnDmkDAC_PF.AbreQuery(QrDisp, Dmod.MyDB);
  PB1.Position := 0;
  PB1.Max := QrDisp.RecordCount;
  n := QrZStepCodCodIni.Value;
  while not QrDisp.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    if QrDispCodUsu.Value > n  then
    begin
      FResult := n;
      //
      Break;
    end else n := n + 1;
    //
    QrDisp.Next;
  end;
  Screen.Cursor := crDefault;
  if FResult = 0 then
    Geral.MB_Aviso(
    'Erro ao localizar n�mero dispon�vel na faixa selecionada!');
  Close;
end;

procedure TFmZStepCodUni.Prximo1Click(Sender: TObject);
begin
  if QrPesqCadastros.Value = 0 then
    FResult := QrZStepCodCodIni.Value
  else if QrZStepCodCodFim.Value > QrPesqMaximo.Value then
  begin
    if QrPesqMaximo.Value = 0 then
      FResult := QrZStepCodCodIni.Value
    else
      FResult := QrPesqMaximo.Value + 1;
  end else begin
    FResult := 0;
    Geral.MB_Aviso('N�o h� mais c�digos dispon�veis nesta faixa!');
  end;
  Close;
end;

procedure TFmZStepCodUni.QrZStepCodAfterOpen(DataSet: TDataSet);
begin
  BtCodUsu.Enabled := QrZStepCod.RecordCount > 0;
end;

procedure TFmZStepCodUni.QrZStepCodBeforeClose(DataSet: TDataSet);
begin
  BtCodUsu.Enabled := False;
end;

end.

