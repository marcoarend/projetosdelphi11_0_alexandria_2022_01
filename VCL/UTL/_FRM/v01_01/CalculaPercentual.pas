unit CalculaPercentual;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit;

type
  TFmCalculaPercentual = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    RGForma: TRadioGroup;
    EdValorIni: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdValorFim: TdmkEdit;
    Label3: TLabel;
    EdPorcento: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGFormaClick(Sender: TObject);
    procedure EdValorIniChange(Sender: TObject);
    procedure EdValorFimChange(Sender: TObject);
    procedure EdPorcentoChange(Sender: TObject);
  private
    { Private declarations }
    procedure Calcula(Sender: TObject);
  public
    { Public declarations }
  end;

  var
  FmCalculaPercentual: TFmCalculaPercentual;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmCalculaPercentual.BtOKClick(Sender: TObject);
begin
//  ModalResult := mrOK;
//  Close; << N�o pode!!!
end;

procedure TFmCalculaPercentual.BtSaidaClick(Sender: TObject);
begin
//  Close; << N�o pode!!!
end;

procedure TFmCalculaPercentual.Calcula(Sender: TObject);
var
  vI, vF, pV, Valor: Double;
  Zerado: Boolean;
begin
  Valor := 0;
  vI := EdValorIni.ValueVariant;
  vF := EdValorFim.ValueVariant;
  pV := EdPorcento.ValueVariant;
  //
  case RGForma.ItemIndex of
    0: Zerado := vI = 0;
    1: Zerado := vF = 0;
    2: Zerado := pV = 0;
  end;
  //
  if not Zerado then
  begin
    case RGForma.ItemIndex of
      0: pV := ((vI - vF) / vI) * 100;
      1: pV := ((vF - vI) / vF) * 100;
      2: vF := vI / (1 - (pV / 100));
    end;
  end;
  //
  case RGForma.ItemIndex of
    0,
    1: if TControl(Sender).Name <> 'EdPorcento' then EdPorcento.ValueVariant := pV;
    2: if TControl(Sender).Name <> 'EdValorfim' then EdValorFim.ValueVariant := vF;
  end;
  //
end;

procedure TFmCalculaPercentual.EdPorcentoChange(Sender: TObject);
begin
  Calcula(Sender);
end;

procedure TFmCalculaPercentual.EdValorFimChange(Sender: TObject);
begin
  Calcula(Sender);
end;

procedure TFmCalculaPercentual.EdValorIniChange(Sender: TObject);
begin
  Calcula(Sender);
end;

procedure TFmCalculaPercentual.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCalculaPercentual.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  Modalresult := mrCancel;
end;

procedure TFmCalculaPercentual.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCalculaPercentual.RGFormaClick(Sender: TObject);
begin
  Calcula(Sender);
end;

end.
