unit HorVerao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkEdit,
  Vcl.Mask, dmkEditDateTimePicker, dmkCheckBox;

type
  TFmHorVerao = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    LaTitulo1C: TLabel;
    QrParamsEmp: TmySQLQuery;
    DsParamsEmp: TDataSource;
    QrParamsEmpTZD_UTC_Str: TWideStringField;
    QrParamsEmpNO_EMP: TWideStringField;
    QrParamsEmpFilial: TIntegerField;
    QrParamsEmpTZD_UTC: TFloatField;
    QrParamsEmphVeraoAsk: TDateField;
    QrParamsEmphVeraoIni: TDateField;
    QrParamsEmphVeraoFim: TDateField;
    Memo1: TMemo;
    DBGParamsEmp: TDBGrid;
    PnEdita: TPanel;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel5: TPanel;
    BtDesiste: TBitBtn;
    QrParamsEmpCodigo: TIntegerField;
    Panel6: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    GroupBox43: TGroupBox;
    Panel64: TPanel;
    Label259: TLabel;
    Label260: TLabel;
    Label261: TLabel;
    Label262: TLabel;
    Label263: TLabel;
    SpeedButton21: TSpeedButton;
    SbTZD_UTC: TSpeedButton;
    EdTZD_UTC: TdmkEdit;
    EdTZD_UTC_TXT: TEdit;
    TPhVeraoAsk: TdmkEditDateTimePicker;
    TPhVeraoIni: TdmkEditDateTimePicker;
    TPhVeraoFim: TdmkEditDateTimePicker;
    Panel1: TPanel;
    BtAltera: TBitBtn;
    QrParamsEmphVeraoAsk_TXT: TWideStringField;
    QrParamsEmphVeraoIni_TXT: TWideStringField;
    QrParamsEmphVeraoFim_TXT: TWideStringField;
    Label277: TLabel;
    CkTZD_UTC_Auto: TdmkCheckBox;
    QrParamsEmpTZD_UTC_Auto_UsuarioAceite: TIntegerField;
    QrParamsEmpTZD_UTC_Auto_TermoAceite: TIntegerField;
    QrParamsEmpTZD_UTC_Auto_DataHoraAceite: TDateTimeField;
    QrParamsEmpTZD_UTC_Auto: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrParamsEmpCalcFields(DataSet: TDataSet);
    procedure EdTZD_UTCChange(Sender: TObject);
    procedure SbTZD_UTCClick(Sender: TObject);
    procedure SpeedButton21Click(Sender: TObject);
    procedure DBGParamsEmpDblClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure Label277Click(Sender: TObject);
    procedure CkTZD_UTC_AutoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FMostraTermo: Boolean;
    FTZD_UTC_Auto_UsuarioAceite, FTZD_UTC_Auto_TermoAceite: Integer;
    FTZD_UTC_Auto_DataHoraAceite: TDateTime;
    procedure MostraInicio();
    procedure AlteraDados();
    procedure ConfiguraCamposTZ(Auto: Boolean);
  public
    { Public declarations }
    procedure ReopenParamsEmp(Codigo: Integer);
  end;

  var
  FmHorVerao: TFmHorVerao;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkProcFunc, UMySQLModule, UnDmkWeb,
  ModuleGeral, UnWAceites;

{$R *.DFM}

procedure TFmHorVerao.AlteraDados;
begin
  if (QrParamsEmp.State <> dsInactive) and (QrParamsEmp.RecordCount > 0) then
  begin
    FMostraTermo := False;
    //
    BtAltera.Visible       := False;
    BtSaida.Visible        := False;
    DBGParamsEmp.Enabled   := False;
    //
    EdTZD_UTC.ValueVariant := QrParamsEmpTZD_UTC.Value;
    TPhVeraoAsk.Date       := QrParamsEmphVeraoAsk.Value;
    TPhVeraoIni.Date       := QrParamsEmphVeraoIni.Value;
    TPhVeraoFim.Date       := QrParamsEmphVeraoFim.Value;
    //
    PnEdita.Visible        := True;
    //
    ConfiguraCamposTZ(Geral.IntToBool(QrParamsEmpTZD_UTC_Auto.Value));
    //
    FMostraTermo := True;
  end;
end;

procedure TFmHorVerao.ConfiguraCamposTZ(Auto: Boolean);
begin
  if Auto then
  begin
    EdTZD_UTC.Enabled   := False;
    SBTZD_UTC.Enabled   := False;
    TPhVeraoAsk.Enabled := False;
    TPhVeraoIni.Enabled := False;
    TPhVeraoFim.Enabled := False;
  end else
  begin
    EdTZD_UTC.Enabled   := True;
    SBTZD_UTC.Enabled   := True;
    TPhVeraoAsk.Enabled := True;
    TPhVeraoIni.Enabled := True;
    TPhVeraoFim.Enabled := True;
  end;
  if (QrParamsEmp.State <> dsInactive) and (QrParamsEmp.RecordCount > 0) then
  begin
    FTZD_UTC_Auto_UsuarioAceite  := 0;
    FTZD_UTC_Auto_TermoAceite    := 0;
    FTZD_UTC_Auto_DataHoraAceite := 0;
  end else
  begin
    FTZD_UTC_Auto_UsuarioAceite  := QrParamsEmpTZD_UTC_Auto_UsuarioAceite.Value;
    FTZD_UTC_Auto_TermoAceite    := QrParamsEmpTZD_UTC_Auto_TermoAceite.Value;
    FTZD_UTC_Auto_DataHoraAceite := QrParamsEmpTZD_UTC_Auto_DataHoraAceite.Value;
  end;
end;

procedure TFmHorVerao.BtAlteraClick(Sender: TObject);
begin
  AlteraDados;
end;

procedure TFmHorVerao.BtConfirmaClick(Sender: TObject);
var
  hVeraoAsk, hVeraoIni, hVeraoFim: String;
  TZD_UTC: Double;
  Codigo, TZD_UTC_Auto: Integer;
begin
  Codigo       := QrParamsEmpCodigo.Value;
  TZD_UTC      := EdTZD_UTC.ValueVariant;
  hVeraoAsk    := Geral.FDT(TPhVeraoAsk.Date, 1);
  hVeraoIni    := Geral.FDT(TPhVeraoIni.Date, 1);
  hVeraoFim    := Geral.FDT(TPhVeraoFim.Date, 1);
  TZD_UTC_Auto := Geral.BoolToInt(CkTZD_UTC_Auto.Checked);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'paramsemp', False, [
  'TZD_UTC',
  'hVeraoAsk', 'hVeraoIni', 'hVeraoFim'], [
  'Codigo'], [
  TZD_UTC,
  hVeraoAsk, hVeraoIni, hVeraoFim], [
  Codigo], True) then
  begin
    //Atualiza aceite
    if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'paramsemp', False,
      ['TZD_UTC_Auto_TermoAceite', 'TZD_UTC_Auto_DataHoraAceite',
      'TZD_UTC_Auto_UsuarioAceite', 'TZD_UTC_Auto'], ['Codigo'],
      [FTZD_UTC_Auto_TermoAceite, Geral.FDT(FTZD_UTC_Auto_DataHoraAceite, 109),
      FTZD_UTC_Auto_UsuarioAceite, TZD_UTC_Auto], [Codigo], True)
    then
      Geral.MB_Erro('Falha ao atualizar aceite!');
    //
    ReopenParamsEmp(Codigo);
    MostraInicio();
  end;
end;

procedure TFmHorVerao.BtDesisteClick(Sender: TObject);
begin
  MostraInicio();
end;

procedure TFmHorVerao.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmHorVerao.CkTZD_UTC_AutoClick(Sender: TObject);
var
  DtaHora: TDateTime;
  Usuario, Termo: Integer;
begin
  ConfiguraCamposTZ(CkTZD_UTC_Auto.Checked);
  //
  if (CkTZD_UTC_Auto.Checked) and (FMostraTermo = True) then
  begin
    if not UWAceites.AceitaTermos(istTZD_UTC_Auto, Termo, Usuario, DtaHora) then
    begin
      FTZD_UTC_Auto_UsuarioAceite  := 0;
      FTZD_UTC_Auto_TermoAceite    := 0;
      FTZD_UTC_Auto_DataHoraAceite := 0;
      //
      CkTZD_UTC_Auto.Checked := False;
    end else
    begin
      FTZD_UTC_Auto_UsuarioAceite  := Usuario;
      FTZD_UTC_Auto_TermoAceite    := Termo;
      FTZD_UTC_Auto_DataHoraAceite := DtaHora;
      //
      EdTZD_UTC.ValueVariant := DModG.ObtemFusoHorarioServidor;
    end;
  end;
end;

procedure TFmHorVerao.DBGParamsEmpDblClick(Sender: TObject);
begin
  AlteraDados;
end;

procedure TFmHorVerao.EdTZD_UTCChange(Sender: TObject);
begin
  EdTZD_UTC_TXT.Text := dmkPF.TZD_UTC_FloatToSignedStr(EdTZD_UTC.ValueVariant);
end;

procedure TFmHorVerao.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmHorVerao.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenParamsEmp(0);
end;

procedure TFmHorVerao.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmHorVerao.FormShow(Sender: TObject);
begin
  Memo1.WordWrap := True;
end;

procedure TFmHorVerao.Label277Click(Sender: TObject);
begin
  DmkWeb.MostraWebBrowser('http://pcdsh01.on.br/verao1.html', True, False, 0, 0);
end;

procedure TFmHorVerao.MostraInicio;
begin
  BtAltera.Visible     := True;
  BtSaida.Visible      := True;
  DBGParamsEmp.Enabled := True;
  //
  PnEdita.Visible      := False;
end;

procedure TFmHorVerao.QrParamsEmpCalcFields(DataSet: TDataSet);
begin
  if QrParamsEmpTZD_UTC.Value < 0 then
    QrParamsEmpTZD_UTC_Str.Value := '-' + Geral.FDT(QrParamsEmpTZD_UTC.Value, 102)
  else
    QrParamsEmpTZD_UTC_Str.Value := '+' + Geral.FDT(QrParamsEmpTZD_UTC.Value, 102);
end;

procedure TFmHorVerao.ReopenParamsEmp(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrParamsEmp, Dmod.MyDB, [
    // ini 2020-10-23
    //'SELECT *, ',
    'SELECT emp.TZD_UTC_Auto_UsuarioAceite, emp.TZD_UTC_Auto_TermoAceite, ',
    'emp.TZD_UTC_Auto_DataHoraAceite, emp.TZD_UTC_Auto, ',
    // fim 2020-10-23
    'IF(ent.Tipo=0, RazaoSocial, Nome) NO_EMP, Filial, emp.Codigo, ',
    'emp.TZD_UTC, emp.hVeraoAsk, emp.hVeraoIni, emp.hVeraoFim, ',
    'IF(emp.hVeraoAsk <= "1899-12-30", "", DATE_FORMAT(emp.hVeraoAsk, "%d/%m/%Y")) hVeraoAsk_TXT, ',
    'IF(emp.hVeraoIni <= "1899-12-30", "", DATE_FORMAT(emp.hVeraoIni, "%d/%m/%Y")) hVeraoIni_TXT, ',
    'IF(emp.hVeraoFim <= "1899-12-30", "", DATE_FORMAT(emp.hVeraoFim, "%d/%m/%Y")) hVeraoFim_TXT ',
    'FROM paramsemp emp ',
    'LEFT JOIN entidades ent ON ent.Codigo=emp.Codigo ',
    'WHERE emp.Codigo=' + VAR_LIB_Empresas,
    'AND emp.TZD_UTC_Auto = 0 ',
    'ORDER BY NO_EMP ',
    '']);
  //
  if Codigo <> 0 then
    QrParamsEmp.Locate('Codigo', Codigo, []);
end;

procedure TFmHorVerao.SbTZD_UTCClick(Sender: TObject);
begin
  EdTZD_UTC.ValueVariant := dmkPF.TZD_UTC_Define(EdTZD_UTC.ValueVariant);
end;

procedure TFmHorVerao.SpeedButton21Click(Sender: TObject);
begin
  dmkPF.TZD_UTC_InfoHelp();
end;

end.
