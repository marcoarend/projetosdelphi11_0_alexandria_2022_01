unit Feriados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, ComCtrls,
  DB, mySQLDbTables, dmkEdit, dmkEditDateTimePicker, dmkGeral, dmkImage,
  UnDmkEnums, dmkDBGrid;

type
  TFmFeriados = class(TForm)
    PainelDados: TPanel;
    PainelEdita: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    QrFeriados: TmySQLQuery;
    QrFeriadosData: TDateField;
    QrFeriadosMotivo: TWideStringField;
    QrFeriadosLk: TIntegerField;
    QrFeriadosDataCad: TDateField;
    QrFeriadosDataAlt: TDateField;
    QrFeriadosUserCad: TIntegerField;
    QrFeriadosUserAlt: TIntegerField;
    DsFeriados: TDataSource;
    TPData: TdmkEditDateTimePicker;
    EdNome: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel3: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtInclui: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    QrFeriadosAnual: TSmallintField;
    CkAnual: TCheckBox;
    BtAltera: TBitBtn;
    QrFeriadosData_Txt: TWideStringField;
    DBGrid1: TdmkDBGrid;
    QrFeriadosData_Ordena: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFeriadosAfterOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtAlteraClick(Sender: TObject);
  private
    { Private declarations }
    procedure CriaOForm;
    procedure DefParams;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure ReabreFeriados(Data: TDate);
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
  public
    { Public declarations }
  end;

  var
  FmFeriados: TFmFeriados;

implementation

uses UnMyObjects, Module, UnInternalConsts, UnGotoy, UMySQLModule, MyDBCheck,
  DmkDAC_PF, ModuleGeral;

{$R *.DFM}

procedure TFmFeriados.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
end;

procedure TFmFeriados.DefineONomeDoForm;
begin
end;

procedure TFmFeriados.DefParams;
begin
  VAR_GOTOTABELA := 'feriados';
  VAR_GOTOMYSQLTABLE := QrFeriados;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *, ');
  VAR_SQLx.Add('IF(Anual=1, DATE_FORMAT(Data, "%d/%m"), DATE_FORMAT(Data, "%d/%m/%Y")) Data_TXT, ');
  VAR_SQLx.Add('IF(Anual=1, CONCAT(DATE_FORMAT(Now(), "%Y"), DATE_FORMAT(Data, "%m%d")), DATE_FORMAT(Data, "%Y%m%d")) Data_Ordena ');
  VAR_SQLx.Add('FROM feriados');
  VAR_SQLx.Add('ORDER BY Data_Ordena DESC');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('');
  //
  VAR_SQLa.Add('');
  //
end;

procedure TFmFeriados.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(1, stUpd, 0);
end;

procedure TFmFeriados.BtConfirmaClick(Sender: TObject);
var
  Continua: Boolean;
  Anual: Integer;
  DataCod, Data, Nome: String;
begin
  Nome  := EdNome.ValueVariant;
  Anual := Geral.BoolToInt(CkAnual.Checked);
  Data  := Geral.FDT(TPData.Date, 1);
  //
  if MyObjects.FIC(TPData.Date = 0, TPData, 'Data n�o definida!') then Exit;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Motivo n�o definido!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    DataCod := Geral.FDT(TPData.Date, 1)
  else
    DataCod := Geral.FDT(QrFeriadosData.Value, 1);
  //
  //2016-01-04 => Feito assim pois a chave prim�ria � a Data
  if ImgTipo.SQLType = stIns then
  begin
    Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'feriados', False,
                  ['Motivo', 'Anual'], ['Data'],
                  [Nome, Anual], [DataCod], True);
  end else
  begin
    Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'feriados', False,
                  ['Motivo', 'Anual', 'Data'], ['Data'],
                  [Nome, Anual, Data], [DataCod], True);
  end;
  if Continua = True then  
  begin
    ReabreFeriados(TPData.Date);
    MostraEdicao(0, stLok, 0);
  end;  
end;

procedure TFmFeriados.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmFeriados.BtExcluiClick(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do feriado selecionado?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + ' feriados');
    Dmod.QrUpd.SQL.Add('WHERE Data=:P0 AND Motivo=:P1');
    Dmod.QrUpd.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, QrFeriadosData.Value);
    Dmod.QrUpd.Params[1].AsString := QrFeriadosMotivo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReabreFeriados(0);
  end;
end;

procedure TFmFeriados.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmFeriados.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFeriados.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFeriados.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFeriados.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType   := stLok;
  PainelEdita.Align := alClient;
  DBGrid1.Align     := alClient;
  CriaOForm;
  ReabreFeriados(0);
  //
  ImgTipo.SQLType := stLok;
end;

procedure TFmFeriados.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFeriados.MostraEdicao(Mostra: Integer; SQLType: TSQLType;
  Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      //
      if SQLType = stIns then
      begin
        TPData.Date         := Date;
        EdNome.ValueVariant := '';
        CkAnual.Checked     := False;
      end else begin
        TPData.Date         := QrFeriadosData.Value;
        EdNome.ValueVariant := QrFeriadosMotivo.Value;
        CkAnual.Checked     := Geral.IntToBool(QrFeriadosAnual.Value);
      end;
      TPData.SetFocus;
    end;
    else Geral.MB_Erro('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFeriados.QrFeriadosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFeriados.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFeriados.ReabreFeriados(Data: TDate);
begin
  UnDmkDAC_PF.AbreQuery(QrFeriados, Dmod.MyDB);
  //
  if Data > 0 then
    QrFeriados.Locate('Data', Data, []);
end;

end.
