object FmAbout: TFmAbout
  Left = 200
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Sobre'
  ClientHeight = 419
  ClientWidth = 401
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 10
    Top = 10
    Width = 380
    Height = 347
    TabOrder = 0
    object ProgramIcon: TImage
      Left = 174
      Top = 10
      Width = 32
      Height = 32
      Center = True
      Proportional = True
      Stretch = True
      IsControl = True
    end
    object LaLink: TLabel
      Left = 82
      Top = 298
      Width = 224
      Height = 13
      Cursor = crHandPoint
      Caption = 'Atualizar informa'#231#245'es referentes ao computador'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = LaLinkClick
    end
    object LaMsgs: TLabel
      Left = 114
      Top = 318
      Width = 154
      Height = 13
      Cursor = crHandPoint
      Caption = 'Mensagens de erros conhecidos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = LaMsgsClick
    end
    object Panel1: TPanel
      Left = 8
      Top = 52
      Width = 365
      Height = 241
      BevelOuter = bvLowered
      TabOrder = 0
      object LaAplicativo: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 10
        Width = 357
        Height = 13
        Margins.Top = 9
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Aplicativo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        IsControl = True
        ExplicitLeft = -15
        ExplicitTop = -7
        ExplicitWidth = 183
      end
      object LaInternalName: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 29
        Width = 357
        Height = 13
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Nome interno:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        IsControl = True
        ExplicitLeft = -175
        ExplicitTop = 79
        ExplicitWidth = 360
      end
      object LaVersao: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 48
        Width = 357
        Height = 13
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Vers'#227'o site: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 1461225
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        IsControl = True
        ExplicitLeft = -175
        ExplicitTop = 78
        ExplicitWidth = 360
      end
      object LaVerMCW: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 67
        Width = 357
        Height = 13
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Vers'#227'o beta W: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        IsControl = True
        ExplicitLeft = -175
        ExplicitTop = 98
        ExplicitWidth = 360
      end
      object LaVerMLA: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 105
        Width = 357
        Height = 13
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Vers'#227'o beta A: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        IsControl = True
        ExplicitLeft = 1
        ExplicitTop = 69
        ExplicitWidth = 363
      end
      object LaIpLocal: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 86
        Width = 357
        Height = 13
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Meu IP (local):'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        IsControl = True
        ExplicitLeft = -175
        ExplicitTop = 133
        ExplicitWidth = 360
      end
      object LaIPServidor: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 124
        Width = 357
        Height = 13
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'IP do servidor (local):'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        IsControl = True
        ExplicitLeft = -175
        ExplicitTop = 156
        ExplicitWidth = 360
      end
      object LaSO: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 143
        Width = 357
        Height = 13
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Sistema operacional:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        IsControl = True
        ExplicitLeft = -175
        ExplicitTop = 172
        ExplicitWidth = 360
      end
      object LaComputador: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 162
        Width = 357
        Height = 13
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Computador:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        IsControl = True
        ExplicitLeft = -175
        ExplicitTop = 172
        ExplicitWidth = 360
      end
      object LaDescriPC: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 181
        Width = 357
        Height = 13
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Descri'#231#227'o do computador:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        IsControl = True
        ExplicitLeft = -175
        ExplicitTop = 172
        ExplicitWidth = 360
      end
      object LaPerfil: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 200
        Width = 357
        Height = 13
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Perfil de senha:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        IsControl = True
        ExplicitLeft = -175
        ExplicitTop = 172
        ExplicitWidth = 360
      end
      object LaCopy: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 219
        Width = 357
        Height = 13
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Copy'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        IsControl = True
        ExplicitLeft = -175
        ExplicitTop = 172
        ExplicitWidth = 360
      end
    end
  end
  object Panel2: TPanel
    Left = 12
    Top = 364
    Width = 377
    Height = 41
    TabOrder = 1
    object OKButton: TButton
      AlignWithMargins = True
      Left = 121
      Top = 4
      Width = 135
      Height = 33
      Margins.Left = 120
      Margins.Right = 120
      Align = alClient
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = OKButtonClick
    end
  end
end
