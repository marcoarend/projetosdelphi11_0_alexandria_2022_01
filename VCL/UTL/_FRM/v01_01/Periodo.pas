unit Periodo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DBCtrls, UnGOTOy, UnInternalConsts, UnMsgInt, StdCtrls,
  Db, (*DBTables,*) Buttons, mySQLDbTables;

type
  TFmPeriodo = class(TForm)
    Panel1: TPanel;
    LaMes: TLabel;
    LaAno: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    LaSelMes: TLabel;
    LaSelAno: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmPeriodo: TFmPeriodo;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmPeriodo.FormCreate(Sender: TObject);
begin
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
end;

procedure TFmPeriodo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPeriodo.BtConfirmaClick(Sender: TObject);
begin
  LaSelAno.Caption := CBAno.Text;
  LaSelMes.Caption := IntToStr(CBMes.ItemIndex + 1);
  Close;
end;

procedure TFmPeriodo.BtDesisteClick(Sender: TObject);
begin
  LaSelAno.Caption := '0';
  LaSelMes.Caption := '0';
  Close;
end;

end.
