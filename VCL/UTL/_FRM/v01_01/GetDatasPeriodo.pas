unit GetDatasPeriodo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkEdit, dmkGeral,
  dmkEditDateTimePicker, dmkImage, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEditCB, UnDmkEnums;

type
  TFmGetDatasPeriodo = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    EdHoraIni: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    Label2: TLabel;
    TPDataFim: TdmkEditDateTimePicker;
    EdHoraFim: TdmkEdit;
    LaHoraIni: TLabel;
    LaHoraFim: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FSelecionou: Boolean;
    FMinData: TDateTime;
  end;

  var
  FmGetDatasPeriodo: TFmGetDatasPeriodo;

implementation

uses UnMyObjects, UnInternalConsts, DmkDAC_PF;

{$R *.DFM}

procedure TFmGetDatasPeriodo.BtSaidaClick(Sender: TObject);
begin
  FSelecionou := False;
  VAR_GETPERIODO_Ini := 0;
  VAR_GETPERIODO_Fim := 0;
  //
  Close;
end;

procedure TFmGetDatasPeriodo.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  MyObjects.CorIniComponente();
end;

procedure TFmGetDatasPeriodo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGetDatasPeriodo.FormCreate(Sender: TObject);
{
var
  Agora: TDateTime;
begin
  Agora := Now(); //Dmod.ObtemAgora();
  FSelecionou := False;
  VAR_GETPERIODO_Ini := 0;
  VAR_GETPERIODO_Fim := 0;
  TPDataIni.Date := 0;
  EdHoraIni.ValueVariant := 0;
  TPDataFim.Date := Int(Agora);
  EdHoraFim.ValueVariant := Agora;
  //
}
begin
  FSelecionou := False;
  VAR_GETPERIODO_Ini := 0;
  VAR_GETPERIODO_Fim := 0;
end;

procedure TFmGetDatasPeriodo.BtOKClick(Sender: TObject);
begin
  VAR_GETPERIODO_Ini := Int(TPDataIni.Date) + EdHoraIni.ValueVariant;
  VAR_GETPERIODO_Fim := Int(TPDataFim.Date) + EdHoraFim.ValueVariant;
  if VAR_GETPERIODO_Fim >= VAR_GETPERIODO_Ini then
  begin
    FSelecionou := True;
    //
    Close;
  end else
  begin
    Geral.MB_Aviso(
    'A data/hora final deve ser igual ou posterior a data/hora inicial!');
    //
    VAR_GETPERIODO_Ini := 0;
    VAR_GETPERIODO_Fim := 0;
  end;
end;

end.
