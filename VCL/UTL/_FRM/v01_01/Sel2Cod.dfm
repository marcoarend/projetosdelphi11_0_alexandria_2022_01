object FmSel2Cod: TFmSel2Cod
  Left = 450
  Top = 286
  Caption = 'XXX-XXXXX-002 :: Seleciona ?'
  ClientHeight = 261
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 99
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 624
    object LaPrompt1: TLabel
      Left = 12
      Top = 4
      Width = 45
      Height = 13
      Caption = 'Entidade:'
    end
    object LaOrdem1: TLabel
      Left = 708
      Top = 4
      Width = 34
      Height = 13
      Caption = 'Ordem:'
      Visible = False
    end
    object SBCadastro1: TSpeedButton
      Left = 684
      Top = 20
      Width = 23
      Height = 22
      Caption = '...'
      Visible = False
      OnClick = SBCadastro1Click
    end
    object LaPrompt2: TLabel
      Left = 12
      Top = 48
      Width = 45
      Height = 13
      Caption = 'Entidade:'
    end
    object SBCadastro2: TSpeedButton
      Left = 684
      Top = 64
      Width = 23
      Height = 22
      Caption = '...'
      Visible = False
    end
    object LaOrdem: TLabel
      Left = 708
      Top = 48
      Width = 34
      Height = 13
      Caption = 'Ordem:'
      Visible = False
    end
    object CBSel1: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 601
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Descricao'
      ListSource = DsSel1
      TabOrder = 1
      dmkEditCB = EdSel1
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdSel1: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBSel1
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object dmkEdOrdem1: TdmkEdit
      Left = 708
      Top = 20
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      Visible = False
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSel2: TdmkEditCB
      Left = 12
      Top = 64
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBSel2
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBSel2: TdmkDBLookupComboBox
      Left = 80
      Top = 64
      Width = 601
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Descricao'
      ListSource = DsSel2
      TabOrder = 4
      dmkEditCB = EdSel2
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object dmkEdOrdem2: TdmkEdit
      Left = 708
      Top = 64
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      Visible = False
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 624
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 576
      object ImgTipo: TdmkImage
        Left = 8
        Top = 7
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 528
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 142
        Height = 32
        Caption = 'Seleciona ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 142
        Height = 32
        Caption = 'Seleciona ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 142
        Height = 32
        Caption = 'Seleciona ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 147
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 624
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 620
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 191
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 624
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 478
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 476
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkContinua: TCheckBox
        Left = 140
        Top = 16
        Width = 117
        Height = 17
        Caption = 'Continua inserindo.'
        TabOrder = 1
        Visible = False
      end
    end
  end
  object DsSel1: TDataSource
    DataSet = QrSel1
    Left = 308
    Top = 70
  end
  object QrSel1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END Descricao, Codigo'
      'FROM entidades '
      'ORDER BY Descricao')
    Left = 308
    Top = 22
    object QrSel1Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrSel1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSel2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END Descricao, Codigo'
      'FROM entidades '
      'ORDER BY Descricao')
    Left = 372
    Top = 22
    object QrSel2Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrSel2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsSel2: TDataSource
    DataSet = QrSel2
    Left = 372
    Top = 70
  end
end
