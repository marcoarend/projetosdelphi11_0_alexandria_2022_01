unit NovaVersao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ZipForge, ShellApi,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  IdExplicitTLSClientServerBase, IdFTP, dmkEdit, UnDmkProcFunc, dmkImage,
  UnDMkEnums, DmkDAC_PF, IdSSLOpenSSL, UnProjGroup_Consts;

type
  TFmNovaVersao = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    STAviso: TStaticText;
    StVersao: TStaticText;
    Archiver: TZipForge;
    STAppArqName: TStaticText;
    STAppArqTitle: TStaticText;
    RGTipo: TRadioGroup;
    IdHTTP1: TIdHTTP;
    Panel1: TPanel;
    Label1: TLabel;
    ProgressBar1: TProgressBar;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    LaTamTot: TLabel;
    LaTamRec: TLabel;
    EdTamTot: TdmkEdit;
    EdTamRec: TdmkEdit;
    GroupBox3: TGroupBox;
    Label9: TLabel;
    EdPerRec: TdmkEdit;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EdTmpTot: TdmkEdit;
    EdTmpRun: TdmkEdit;
    EdTmpFal: TdmkEdit;
    LaLink: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BitBtn2: TBitBtn;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    //XMLTransform1: TXMLTransform;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure LaLinkClick(Sender: TObject);
    procedure LaLinkMouseEnter(Sender: TObject);
    procedure LaLinkMouseLeave(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure IdHTTP1Work(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Int64);
    procedure IdHTTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Int64);
    procedure IdHTTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
  private
    { Private declarations }
    FWorkCount, FWorkCountMax: Integer;
    FTimeIni: TDateTime;
    function DesZipaArquivo(ArqNome, DirToExtract: String): Boolean;
    function DownloadFileHTTP(Fonte, Destino: String): Boolean;
    //function DownloadFileFTP(Fonte, Destino: String): Boolean;
    {$IfNDef NO_USE_MYSQLMODULE}
    procedure AtualizaVersaoTerminal();
    {$EndIf}
  public
    { Public declarations }
    FNaoEncerra: Boolean;
    FAplic, FVersao: Int64;
    FURLArq: String;
  end;

  var
  FmNovaVersao: TFmNovaVersao;

implementation

uses UnInternalConsts, MyListas, Principal, dmkGeral, UnMyObjects,
{$IfNDef NO_USE_MYSQLMODULE} Module, ModuleGeral,  UnDmkWeb, {$EndIf}
UnGrl_Vars;
{$R *.DFM}

procedure TFmNovaVersao.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNovaVersao.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNovaVersao.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FNaoEncerra     := False; //Por causa do DermaApps
  VAR_DOWNLOAD_OK := False;
end;

procedure TFmNovaVersao.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNovaVersao.IdHTTP1Work(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Int64);
var
  PerNum, Tam: Double;
  Niv: Integer;
  Nom: String;
  TimeAtu, TimeTot: TDateTime;
begin
  PerNum     := 0;
  TimeAtu    := Now() - FTimeIni;
  FWorkCount := AWorkCount;
  if FWorkCountMax > 0 then
    PerNum := FWorkCount / FWorkCountMax;
  if PerNum > 0 then
    TimeTot := TimeAtu / PerNum
  else
    TimeTot := 0;
  PerNum := PerNum * 100;
  ProgressBar1.Position := AWorkCount;
  STAviso.Caption := 'Download em andamento...';
  Geral.AdequaMedida(grandezaBytes, FWorkCount, 0, Tam, Niv, Nom);
  EdTamRec.DecimalSize  := Niv;
  EdTamRec.ValueVariant := Tam;
  LaTamRec.Caption      := Nom;
  EdPerRec.ValueVariant := PerNum;
  EdTmpRun.ValueVariant := TimeAtu;
  EdTmpTot.ValueVariant := TimeTot;
  EdTmpFal.ValueVariant := TimeTot - TimeAtu;
  Update;
  Application.ProcessMessages;
end;

procedure TFmNovaVersao.IdHTTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCountMax: Int64);
var
  Tam: Double;
  Niv: Integer;
  Nom: String;
begin
  FTimeIni := Now();
  ProgressBar1.Position := 0;
  STAviso.Caption := 'Download sendo iniciado! Aguarde...';
  FWorkCount := 0;
  FWorkCountMax := AWorkCountMax;
  ProgressBar1.Max := FWorkCountMax;
  Geral.AdequaMedida(grandezaBytes, FWorkCountMax, 0, Tam, Niv, Nom);
  EdTamTot.DecimalSize  := Niv;
  EdTamTot.ValueVariant := Tam;
  LaTamTot.Caption := Nom;
  Update;
  Application.ProcessMessages;
end;

procedure TFmNovaVersao.IdHTTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
begin
  ProgressBar1.Position := ProgressBar1.Max;
  STAviso.Caption := 'Download Conclu�do! Aguarde a descompacta��o...';
end;

procedure TFmNovaVersao.LaLinkClick(Sender: TObject);
var
  Link: String;
begin
{$IfNDef NO_USE_MYSQLMODULE}
  Link := 'http://www.dermatek.net.br/?page=hisalt&aplicativo=' +
    Geral.FF0(FAplic) + '&versao=' + Geral.FF0(FVersao) + '&logid='+ VAR_WEB_IDLOGIN;
  //
  DmkWeb.MostraWebBrowser(Link, True, False, 0, 0);
{$EndIf}
end;

procedure TFmNovaVersao.LaLinkMouseEnter(Sender: TObject);
begin
  LaLink.Font.Color := clBlue;
  LaLink.Font.Style := [fsBold, fsUnderline];
end;

procedure TFmNovaVersao.LaLinkMouseLeave(Sender: TObject);
begin
  LaLink.Font.Color := clBlue;
  LaLink.Font.Style := [fsBold];
end;

procedure TFmNovaVersao.BtOKClick(Sender: TObject);
(*var
  ArqZip, Caminho, ArqExe: String;*)
begin
  (*VAR_BREAKDOWNLOAD := False;
  ArqZip := CO_DIR_RAIZ_DMK + '\BackUps\'+STAppArqName.Caption+'.zip';
  ArqExe := CO_DIR_RAIZ_DMK + '\BackUps\'+STAppArqName.Caption+'.exe';
  if FileExists(ArqZip) then DeleteFile(ArqZip);
  Label1.Caption := 'Conectando � Dermatek.com.br para baixar o arquivo '+
    STAppArqName.Caption+'.zip';
  Update;
  Application.ProcessMessages;
  if ????.FTPDownloadFile('ftp.dermatek.com.br', 'apps@dermatek.com.br',
    {'renewapp'}'Aplicativos', 21, '/', STAppArqName.Caption+'.zip', CO_DIR_RAIZ_DMK + '\Backups\'+STAppArqName.Caption+
    '.zip', ProgressBar1, Label1) then
  begin
    if VAR_BREAKDOWNLOAD then Exit;
    Caminho := ExtractFilePath(Application.ExeName);
    ????.RenameAppOlder;
    DesZipaArquivo(CO_DIR_RAIZ_DMK + '\Backups\'+STAppArqName.Caption+'.zip',
      ExtractFilePath(Application.ExeName));
    if VAR_FORMMUTEX <> nil then
    begin
      VAR_FORMMUTEX.Close;
      WinExec(PChar(Application.ExeName), SW_SHOWNORMAL);
      Application.Terminate;
    end;
  end else Geral.Aviso('N�o foi poss�vel fazer o download da nova '+
    'vers�o do aplicativo!);*)
end;

{$IfNDef NO_USE_MYSQLMODULE}
procedure TFmNovaVersao.AtualizaVersaoTerminal;
var
  SerialKey, SerialNum, VersaoMin, VersaoMax: String;
begin
  if CO_DMKID_APP <> 17 then //DControl
  begin
    DModG.ReopenTerminal(True);
    //
    if DModG.QrTerminal.RecordCount > 0 then
    begin
      VersaoMin := Geral.FF0(CO_VERSAO);
      VersaoMax := Geral.FF0(FVersao);
      SerialKey := DModG.QrTerminal.FieldByName('SerialKey').AsString;
      SerialNum := DModG.QrTerminal.FieldByName('SerialNum').AsString;
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE terminais ',
        'SET VersaoMin="' + VersaoMin + '", VersaoMax="' + VersaoMax + '" ',
        'WHERE SerialKey="' + SerialKey + '" AND SerialNum="' + SerialNum + '" ',
        '']);
    end;
  end;
end;
{$EndIf}

procedure TFmNovaVersao.BitBtn1Click(Sender: TObject);
begin
  VAR_BREAKDOWNLOAD := True;
end;

function TFmNovaVersao.DesZipaArquivo(ArqNome, DirToExtract: String): Boolean;
begin
  try
    with Archiver do begin
      FileName := ArqNome;
      OpenArchive(fmOpenRead + fmShareDenyWrite);
      BaseDir := DirToExtract;
      ExtractFiles('*.*');
      CloseArchive;
      ProgressBar1.Position := 0;
    end;
    Result := True;
  finally
     ;
  end;
end;

{
function TFmNovaVersao.DownloadFileFTP(Fonte, Destino: String): Boolean;
var
  MyFile: TFileStream;
begin
  if FTP.Connected then
    FTP.Disconnect;

  IdFTP1.Host     := '';
  IdFTP1.Username := '';
  IdFTP1.Password := '';
  IdFTP1.Connect;

  FWorkCount := 0;
  FWorkCountMax := 0;
  MyFile := TFileStream.Create(Destino, fmCreate); // local no hd e nome do arquivo com a extens�o, onde vai salvar.
  try
    //IdHTTP1.Get('http://www.arquivojuridico.com/'+arquivo, MyFile); // fazendo o download do arquivo
    IdFTP1.Get(Fonte, MyFile); // fazendo o download do arquivo
    Result := True;
  finally
    MyFile.Free;
  end;
  Result := True;
end;
}

function TFmNovaVersao.DownloadFileHTTP(Fonte, Destino: String): Boolean;
var
  MyFile: TFileStream;
  SSL: TIdSSLIOHandlerSocketOpenSSL;
begin
  //Result := False;
  FWorkCount := 0;
  FWorkCountMax := 0;
  MyFile := TFileStream.Create(Destino, fmCreate); // local no hd e nome do arquivo com a extens�o, onde vai salvar.
  try
    //IdHTTP1.Get('http://www.arquivojuridico.com/'+arquivo, MyFile); // fazendo o download do arquivo
    //Obtem configura��es do PROXY
    //
    DmkWeb.ConfiguraHTTP(IdHTTP1);
    //
    if Pos('https', LowerCase(Fonte)) > 0 then
    begin
      SSL := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
      //SSL.SSLOptions.Method      := sslvSSLv3;
      //SSL.SSLOptions.Mode        := sslmClient;
      //SSL.SSLOptions.SSLVersions := [sslvSSLv3];
      //
      IdHTTP1.IOHandler := SSL;
    end;
    //
    IdHTTP1.Get(Fonte, MyFile); // fazendo o download do arquivo
    Result := True;
  finally
    MyFile.Free;
  end;
end;

procedure TFmNovaVersao.BitBtn2Click(Sender: TObject);
var
  Destino, Fonte, Arq, DestFim: String;
  Renomear: Boolean;
begin
  Renomear := True;
  Fonte    := FURLArq;
  //
  if RGTipo.ItemIndex = 2 then
    Destino := CO_DIR_RAIZ_DMK + '\Imagens\'
  else
    Destino := CO_DIR_RAIZ_DMK + '\';
  //
  ForceDirectories(Destino);
  //
  if Pos('.zip', STAppArqName.Caption) = 0 then
   Destino := Destino + STAppArqName.Caption + '.zip'
  else
    Destino := Destino + STAppArqName.Caption;
  //
  //NovoArq := DestFile+STAppArqName.Caption+'.exe';
  if FileExists(Destino) then DeleteFile(Destino);
  STAviso.Caption := 'Aguarde... Baixando arquivo!';
  Application.ProcessMessages;
  //if ????.DownloadFile(Fonte, Destino) then
  if DownloadFileHTTP(Fonte, Destino) then
  begin
    STAviso.Caption := 'Aguarde... Renomeando arquivo!';
    Application.ProcessMessages;
    //
    Arq := ExtractFilePath(Application.ExeName) + STAppArqName.Caption +
             ExtractFileExt(Application.ExeName);

    if STAppArqName.Caption <> ExtractFileName(Application.ExeName) then
    begin
      Renomear := FileExists(Arq);
    end;
    if Renomear then
    begin
      if not dmkPF.RenameAppOlder(Arq) then
      begin
        STAviso.Caption := 'Erro... N�o foi poss�vel renomear o arquivo!';
        Application.ProcessMessages;
        Exit;
      end;
    end;
    STAviso.Caption := 'Aguarde... Descompactando arquivo!';
    Application.ProcessMessages;
    //
    if RGTipo.ItemIndex in ([1, 2, 3, 5]) then
      DestFim := ExtractFilePath(Destino)
    else
      DestFim := ExtractFilePath(Application.ExeName);
    //
    if DesZipaArquivo(Destino, DestFim) then
    begin
      STAviso.Caption := 'OK... Download conclu�do!';
      Application.ProcessMessages;
      Geral.MB_Info('OK... Download conclu�do!');
      case RGTipo.ItemIndex of
        0, 4, 5: // Executavel
        begin
          {$IfNDef NO_USE_MYSQLMODULE}
          AtualizaVersaoTerminal;
          {$EndIf}
          //
          DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtConfigura);
          //
          if STAppArqName.Caption = ExtractFileName(Application.ExeName) then
          begin
            if not FNaoEncerra then
              Application.Terminate;
          end else
          begin
            //terminar aplicativo e come�ar novo
            (*HandleJan := FindWindow(PChar(STAppArqName.Caption), nil);
            if HandleJan <> 0 then
              SendMessage(HandleJan, WM_CLOSE, 0, 0);*)
            // Qual dos dois � ? Acima ou abaixo ?
            if not FNaoEncerra then
              dmkPF.FecharOutraAplicacao(STAppArqName.Caption, Handle);
            dmkPF.WinExecAndWaitOrNot(Arq, 0, nil, False);
            // For�ar encerramento
            if not FNaoEncerra then
              Halt(0);
          end;
        end;
        1: ;// Tabelas
        2: ; // Imagens
      end;
      VAR_DOWNLOAD_OK := True;
    end else
    begin
      dmkPF.RenameAppOlder_Cancel;
      STAviso.Caption := 'Erro ao descompactar arquivo!';
      Application.ProcessMessages;
    end;
  end
  else STAviso.Caption := 'Erro durante o download de ' + FURLArq;
  //
  if FNaoEncerra then
    Close;
end;

end.

