unit ConfJanela;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, DBCtrls, Menus, UnGrl_Consts,
  UMySQLModule, ComCtrls, Variants, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmConfJanela = class(TForm)
    Panel1: TPanel;
    Label4: TLabel;
    CBConfig: TDBLookupComboBox;
    QrConfJanela: TmySQLQuery;
    DsConfJanela: TDataSource;
    PMSalva: TPopupMenu;
    Salvar1: TMenuItem;
    SalvarComo1: TMenuItem;
    QrLoc: TmySQLQuery;
    QrLocNome: TWideStringField;
    QrConfJanelaNome: TWideStringField;
    QrDados: TmySQLQuery;
    QrDadosCodigo: TIntegerField;
    QrDadosNome: TWideStringField;
    QrDadosJanela: TWideStringField;
    QrDadosCompo: TWideStringField;
    QrDadosLk: TIntegerField;
    QrDadosDataCad: TDateField;
    QrDadosDataAlt: TDateField;
    QrDadosUserCad: TIntegerField;
    QrDadosUserAlt: TIntegerField;
    QrDadosValTxt: TWideStringField;
    QrDadosValInt: TIntegerField;
    QrDadosValDat: TDateField;
    QrDadosValDou: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtAplica: TBitBtn;
    BtSalva: TBitBtn;
    BtExclui: TBitBtn;
    procedure BtAplicaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SalvarComo1Click(Sender: TObject);
    procedure BtSalvaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure CBConfigClick(Sender: TObject);
    procedure QrConfJanelaAfterScroll(DataSet: TDataSet);
    procedure PMSalvaPopup(Sender: TObject);
    procedure Salvar1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FNovoNome: String;
    procedure ObtemDadosCompo(const Compo: TComponent; var Nome,
              ValTxt: String; var ValInt: Integer; var ValDou: Double;
              var ValDat: TDateTime);
    procedure SetaDadosCompo(const Compo,
              ValTxt: String; const ValInt: Integer; const ValDou: Double;
              const ValDat: TDateTime);
    procedure ExcluiConfig(Nome: String; Reabre: Boolean);
  public
    { Public declarations }
    FJanela: String;
    FListaCWC: TConfWinControl;
    procedure ReopenConfJanela(Nome: String);
    procedure ConfiguraBotoes;
    procedure SalvaConfig(NomeConfig: String);
  end;

  var
  FmConfJanela: TFmConfJanela;

implementation

{$R *.DFM}

uses UnMyObjects, Module, DmkDAC_PF;


procedure TFmConfJanela.BtAplicaClick(Sender: TObject);
begin
  QrDados.Close;
  QrDados.Params[0].AsString := FJanela;
  QrDados.Params[1].AsString := CBConfig.Text;
  UnDmkDAC_PF.AbreQuery(QrDados, Dmod.MyDB);
  //
  while not QrDados.Eof do
  begin
    SetaDadosCompo(QrDadosCompo.Value, QrDadosValTxt.Value, QrDadosValInt.Value,
      QrDadosValDou.Value, QrDadosValDat.Value);
    QrDados.Next;
  end;
  Close;
end;

procedure TFmConfJanela.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmConfJanela.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmConfJanela.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
end;

procedure TFmConfJanela.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmConfJanela.SalvarComo1Click(Sender: TObject);
begin
  FNovoNome := '';
  if InputQuery('Salvar como...', 'Informe o nome da configura��o:', FNovoNome) then
  begin
    QrLoc.Close;
    QrLoc.Params[0].AsString := FJanela;
    QrLoc.Params[1].AsString := FNovoNome;
    UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
    if QrLoc.RecordCount > 0 then
      Geral.MB_Erro('Salvamento cancelado, nome j� existe!')
    else begin
      SalvaConfig(FNovoNome);
    end;
  end;
end;

procedure TFmConfJanela.ObtemDadosCompo(const Compo: TComponent; var Nome,
  ValTxt: String; var ValInt: Integer; var ValDou: Double; var ValDat: TDateTime);
begin
  Nome   := '';
  ValTxt := '';
  ValInt := 0;
  ValDou := 0;
  ValDat := 0;
  if Compo is TEdit then
  begin
    Nome   := TEdit(Compo).Name;
    ValTxt := TEdit(Compo).Text;
  {
  end else if Compo is T L M D Edit then
  begin
    Nome   := T L M D Edit(Compo).Name;
    ValTxt := T L M D Edit(Compo).Text;
  }
  end else if Compo is TDBLookupComboBox then
  begin
    Nome   := TDBLookupComboBox(Compo).Name;
    if TDBLookupComboBox(Compo).KeyValue <> Null then
    begin
      try
        ValInt := TDBLookupComboBox(Compo).KeyValue;
      except
        ValTxt := TDBLookupComboBox(Compo).KeyValue;
      end;
    end;
  end else if Compo is TDateTimePicker then
  begin
    Nome   := TDateTimePicker(Compo).Name;
    ValDat := TDateTimePicker(Compo).Date;
  end else if Compo is TRadioGroup then
  begin
    Nome   := TRadioGroup(Compo).Name;
    ValInt := TRadioGroup(Compo).ItemIndex;
  end else if Compo is TRadioButton then
  begin
    Nome   := TRadioButton(Compo).Name;
    ValInt := Geral.BoolToInt(TRadioButton(Compo).Checked);
  end else if Compo is TCheckBox then
  begin
    Nome   := TCheckBox(Compo).Name;
    ValInt := Geral.BoolToInt(TCheckBox(Compo).Checked);
  end else ShowMessage('Componente n�o habilitado "'+Compo.ClassName+'".');
end;

procedure TFmConfJanela.SetaDadosCompo(const Compo, ValTxt: String;
const ValInt: Integer; const ValDou: Double; const ValDat: TDateTime);
var
  i: Integer;
begin
  for i := 0 to TMaxConfWinControl do
  begin
    if FListaCWC[i] <> nil then
    begin
      if FListaCWC[i] is TEdit then
      begin
        if TEdit(FListaCWC[i]).Name = Compo then TEdit(FListaCWC[i]).Text := ValTxt;
      {
      end else if FListaCWC[i] is T L M D Edit then
      begin
        if T L M D Edit(FListaCWC[i]).Name = Compo then T L M D Edit(FListaCWC[i]).Text := ValTxt;
      }
      end else if FListaCWC[i] is TDBLookupComboBox then
      begin
        if TDBLookupComboBox(FListaCWC[i]).Name = Compo then
        try
          TDBLookupComboBox(FListaCWC[i]).KeyValue := ValInt;
        except
          TDBLookupComboBox(FListaCWC[i]).KeyValue := ValTxt;
        end;
      end else if FListaCWC[i] is TDateTimePicker then
      begin
        if TDateTimePicker(FListaCWC[i]).Name = Compo then
          TDateTimePicker(FListaCWC[i]).Date := ValDat;
      end else if FListaCWC[i] is TRadioGroup then
      begin
        if TRadioGroup(FListaCWC[i]).Name = Compo then
        TRadioGroup(FListaCWC[i]).ItemIndex := ValInt;
      end else if FListaCWC[i] is TRadioButton then
      begin
        if TRadioButton(FListaCWC[i]).Name = Compo then
          TRadioButton(FListaCWC[i]).Checked := Geral.IntToBool(ValInt);
      end else if FListaCWC[i] is TCheckBox then
      begin
        if TCheckBox(FListaCWC[i]).Name = Compo then
          TCheckBox(FListaCWC[i]).Checked := Geral.IntToBool(ValInt);
      end else ShowMessage('FListaCWC[i]nente n�o habilitado "'+FListaCWC[i].ClassName+'".');
    end;
  end;
end;

procedure TFmConfJanela.BtSalvaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSalva, BtSalva);
  ReopenConfJanela(FNovoNome);
end;

procedure TFmConfJanela.ReopenConfJanela(Nome: String);
begin
  QrConfJanela.Close;
  QrConfJanela.Params[0].AsString := FJanela;
  UnDmkDAC_PF.AbreQuery(QrConfJanela, Dmod.MyDB);
  //
  if Nome <> '' then QrConfJanela.Locate('Nome', Nome, [loCaseInsensitive]);
  
end;

procedure TFmConfJanela.BtExcluiClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da configura��o "' +
  CBConfig.Text + '"?') = ID_YES then
  begin
    ExcluiConfig(CBConfig.Text, True);
  end;
end;

procedure TFmConfJanela.ConfiguraBotoes;
begin
  if CBConfig.Text = '' then
  begin
    BtAplica.Enabled := False;
    BtExclui.Enabled := False;
  end else begin
    BtAplica.Enabled := True;
    BtExclui.Enabled := True;
  end;
end;

procedure TFmConfJanela.CBConfigClick(Sender: TObject);
begin
  ConfiguraBotoes;
end;

procedure TFmConfJanela.QrConfJanelaAfterScroll(DataSet: TDataSet);
begin
  ConfiguraBotoes;
end;

procedure TFmConfJanela.PMSalvaPopup(Sender: TObject);
begin
  if CBConfig.Text <> '' then Salvar1.Enabled := True
  else Salvar1.Enabled := False;
end;

procedure TFmConfJanela.ExcluiConfig(Nome: String; Reabre: Boolean);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add(DELETE_FROM + ' confjanela WHERE Nome = :P0 ');
  Dmod.QrUpd.SQL.Add('AND Janela = :P1');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.Params[00].AsString  := Nome;//CBConfig.Text;
  Dmod.QrUpd.Params[01].AsString  := FJanela;
  Dmod.QrUpd.ExecSQL;
  //
  if Reabre then
  begin
    CBConfig.KeyValue := Null;
    ReopenConfJanela('');
  end;
end;

procedure TFmConfJanela.Salvar1Click(Sender: TObject);
var
  Nome: String;
begin
  Nome := CBConfig.Text;
  ExcluiConfig(Nome, False);
  SalvaConfig(Nome);
end;

procedure TFmConfJanela.SalvaConfig(NomeConfig: String);
var
  ValTxt, Nome: String;
  ValInt, i, Codigo: Integer;
  ValDou: Double;
  ValDat: TDateTime;
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO confjanela SET ');
  Dmod.QrUpd.SQL.Add('Nome=:P0, Janela=:P1, Compo=:P2, ');
  Dmod.QrUpd.SQL.Add('ValTxt=:P3, ValInt=:P4, ValDou=:P5, ValDat=:P6');
  //
  Dmod.QrUpd.SQL.Add(', Codigo=:Pa');
  //
  for i := 0 to TMaxConfWinControl do
  begin
    if FListaCWC[i] <> nil then
    begin
      ObtemDadosCompo(FListaCWC[i], Nome, ValTxt, ValInt, ValDou, ValDat);
      if Nome <> '' then
      begin
        Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'ConfJanela', 'ConfJanela', 'Codigo');
        Dmod.QrUpd.Params[00].AsString  := NomeConfig;
        Dmod.QrUpd.Params[01].AsString  := FJanela;
        Dmod.QrUpd.Params[02].AsString  := Nome;
        Dmod.QrUpd.Params[03].AsString  := ValTxt;
        Dmod.QrUpd.Params[04].AsInteger := ValInt;
        Dmod.QrUpd.Params[05].AsFloat   := ValDou;
        Dmod.QrUpd.Params[06].AsString  := Geral.FDT(ValDat, 1);
        //
        Dmod.QrUpd.Params[07].AsInteger := Codigo;
        Dmod.QrUpd.ExecSQL;
      end;
    end;
  end;
end;

end.

