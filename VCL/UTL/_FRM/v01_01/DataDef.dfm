object FmDataDef: TFmDataDef
  Left = 445
  Top = 275
  BorderStyle = bsDialog
  Caption = 'Escolha de datas'
  ClientHeight = 139
  ClientWidth = 286
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 28
    Top = 16
    Width = 55
    Height = 13
    Caption = 'Data inicial:'
  end
  object Label2: TLabel
    Left = 148
    Top = 16
    Width = 45
    Height = 13
    Caption = 'DataFinal'
  end
  object TPIni: TDateTimePicker
    Left = 27
    Top = 32
    Width = 115
    Height = 21
    Date = 37279.190219560200000000
    Time = 37279.190219560200000000
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 5263440
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object TPFim: TDateTimePicker
    Left = 147
    Top = 32
    Width = 115
    Height = 21
    Date = 37279.190257638900000000
    Time = 37279.190257638900000000
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 5263440
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object BtConfirma: TBitBtn
    Tag = 14
    Left = 28
    Top = 83
    Width = 90
    Height = 40
    Cursor = crHandPoint
    Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
    Caption = '&Confirma'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    NumGlyphs = 2
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnClick = BtConfirmaClick
  end
  object BitBtn1: TBitBtn
    Tag = 15
    Left = 168
    Top = 83
    Width = 90
    Height = 40
    Cursor = crHandPoint
    Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
    Caption = '&Desiste'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    NumGlyphs = 2
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnClick = BitBtn1Click
  end
end
