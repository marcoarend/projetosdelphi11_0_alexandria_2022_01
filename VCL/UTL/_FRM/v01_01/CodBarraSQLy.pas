unit CodBarraSQLy;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, ComCtrls, dmkDBGrid, MyDBCheck, DBCtrls, UnDmkProcFunc,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, dmkEditDateTimePicker, TypInfo,
  stdctrls, UnMyVCLref, dmkGeral, dmkLabel, dmkRadioGroup, dmkMemo, dmkdbEdit,
  dmkCheckBox, DBGrids, UnDmkEnums;

type
  TUnCodBarraSQLy = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function DecifraTipoDeCodigoDeBarras(Leitura: String; Direcao:
             Integer;
             LaAviso1, LaAviso2, LaLast1, LaLast2: TLabel): Boolean;
  end;
var
  UnCodBarraSQLy: TUnCodBarraSQLy;

implementation

uses ModuleGeral, UnMyObjects, UMySQLModule, Module, UnInternalConsts,
CodBarraRead;

{ TUnCodBarraSQLy }

function TUnCodBarraSQLy.DecifraTipoDeCodigoDeBarras(Leitura: String; Direcao:
  Integer;
  LaAviso1, LaAviso2, LaLast1, LaLast2: TLabel): Boolean;
////////////////////////////////////////////////////////////////////////////////
//****************************************************************************//
//  CODIFICAÇÕES IMPLEMENTADAS                                                //
//  ==========================                                                //
//  ??/??/2012   >I<   : Item de lote de protocolo                            //
//  ??/??/2012   >L<   : Lote de protocolo                                    //
//  06/02/2013   -C<   : Cartão AR de carta de cobrança                       //
//  08/09/2013   OS1L  : Lote de Ordens de serviço Bgstrl                     //
//  08/09/2013   OS1I  : Item de Ordem de serviço Bgstrl                      //
//****************************************************************************//
////////////////////////////////////////////////////////////////////////////////
var
  Agora: TDateTime;
  //
  procedure AtualizaLote(Controle: Integer);
  var
    Reconhece: Boolean;
    FldA, FldB, DataHora: String;
  begin
    Reconhece := True;
    case Direcao of
      1: begin FldA := 'Saiu'; FldB := 'DataSai'; end;
      2: begin FldA := 'Recebeu'; FldB := 'DataRec'; end;
      3: begin FldA := 'Retornou'; FldB := 'DataRet'; end;
      else Reconhece := FmCodBarraRead.AvisaDirecaoInvalida();
    end;
    if Reconhece then
    begin
      DataHora := Geral.FDT(Agora, 109);
      Result := Reconhece;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
      FldA, FldB], ['Controle'], [VAR_USUARIO, DataHora], [Controle], True) then
      begin
        MyObjects.Informa2(LaLast1, LaLast2, False,
          LaLast1.Caption + ' >> ' + FldA + ' : ' + Geral.FDT(Agora, 0));
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      end;
    end;
  end;
  //
  procedure AtualizaItem(Conta: Integer);
  var
    Reconhece: Boolean;
    FldA, FldB, DataHora: String;
  begin
    Reconhece := True;
    case Direcao of
      1: begin FldA := 'Saiu'; FldB := 'DataSai'; end;
      2: begin FldA := 'Recebeu'; FldB := 'DataRec'; end;
      3: begin FldA := 'Retornou'; FldB := 'DataRet'; end;
      else Reconhece := FmCodBarraRead.AvisaDirecaoInvalida();
    end;
    if Reconhece then
    begin
      Result := Reconhece;
      DataHora := Geral.FDT(Agora, 109);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
      FldA, FldB], ['Conta'], [VAR_USUARIO, DataHora], [Conta], True) then
      begin
        MyObjects.Informa2(LaLast1, LaLast2, False, LaLast1.Caption + ' >> ' + FldA + ' : ' + Geral.FDT(Agora, 0));
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      end;
    end;
  end;
var
  FldA, FldB, Texto: String;
  Tam, Conta, Controle, P: Integer;
  Reconhece: Boolean;
begin
  Texto := Uppercase(Leitura);
  // 2014-02-26 - Erro na leitura - AnsiString????  Retornando >  [C1OS1L0000000070 para lote 70 e  [C1OS1I0000000125 para OS 125
  if Copy(Texto, 1, 3) = '[C1' then
    Texto := Copy(Texto, 4);
  if Copy(Texto, 1, 3) = ']C1' then
    Texto := Copy(Texto, 4);
  //
  Result := False;
  (*
  Reconhece := False;
  *)
  Agora := DModG.ObtemAgora();
  Tam := Length(Leitura);
  case Tam of
    14:
    begin
      // Protocolo (Item de lote > concatenação de '>I<' + FFN(protpakits.Conta, 11) = Tamanho 14
      if Copy(Leitura, 1, 3) = '>I<' then
      begin
        Conta := Geral.IMV(Copy(Leitura, 4));
        {
        Reconhece := True;
        case Direcao of
          1: begin FldA := 'Saiu'; FldB := 'DataSai'; end;
          2: begin FldA := 'Recebeu'; FldB := 'DataRec'; end;
          3: begin FldA := 'Retornou'; FldB := 'DataRet'; end;
          else Reconhece := FmCodBarraRead.AvisaDirecaoInvalida();
        end;
        if Reconhece then
        begin
          Result := Reconhece;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
          FldA, FldB], ['Conta'], [VAR_USUARIO, Agora], [Conta], True) then
          begin
            MyObjects.Informa2(LaLast1, LaLast2, False, LaLast1.Caption + ' >> ' + FldA + ' : ' + Geral.FDT(Agora, 0));
            MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
          end;
        end;
        }
        AtualizaItem(Conta);
      end else
      // Protocolo (Todo lote > concatenação de '>L<' + FFN(protocopak.Controle, 11) = Tamanho 14
      if Copy(Leitura, 1, 3) = '>L<' then
      begin
        Controle := Geral.IMV(Copy(Leitura, 4));
        {
        Reconhece := True;
        case Direcao of
          1: begin FldA := 'Saiu'; FldB := 'DataSai'; end;
          2: begin FldA := 'Recebeu'; FldB := 'DataRec'; end;
          3: begin FldA := 'Retornou'; FldB := 'DataRet'; end;
          else Reconhece := FmCodBarraRead.AvisaDirecaoInvalida();
        end;
        if Reconhece then
        begin
          Result := Reconhece;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
          FldA, FldB], ['Controle'], [VAR_USUARIO, Agora], [Controle], True) then
          begin
            MyObjects.Informa2(LaLast1, LaLast2, False, LaLast1.Caption + ' >> ' + FldA + ' : ' + Geral.FDT(Agora, 0));
            MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
          end;
        end;
        }
        AtualizaLote(Controle);
      end else
      // Protocolo (Item de lote de OS (1)> concatenação de 'OS1I' + FFN(protpakits.Conta, 10) = Tamanho 14
      if Copy(Leitura, 1, 4) = 'OS1I' then
      begin
        Conta := Geral.IMV(Copy(Leitura, 5));
        AtualizaItem(Conta);
      end else
      // Protocolo (Todo lote > concatenação de 'OS1L' + FFN(protocopak.Controle, 10) = Tamanho 14
      if Copy(Leitura, 1, 4) = 'OS1L' then
      begin
        Controle := Geral.IMV(Copy(Leitura, 5));
        AtualizaLote(Controle);
      //...
      end;
    end;
  end;
end;

end.
