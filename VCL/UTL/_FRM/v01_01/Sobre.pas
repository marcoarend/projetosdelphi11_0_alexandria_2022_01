unit Sobre;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ShellApi;

type
  TFmSobre = class(TForm)
    OKButton: TButton;
    GroupBox1: TGroupBox;
    ImgIconeApp: TImage;
    LaAppVersao: TLabel;
    LaOSDescri: TLabel;
    LaApp: TLabel;
    LaDevice: TLabel;
    LaDeviceNome: TLabel;
    LaIMEINumero: TLabel;
    LaOSNome: TLabel;
    LaOSVersao: TLabel;
    LaAppNome: TLabel;
    LaIMEI: TLabel;
    LaOS: TLabel;
    LaTela: TLabel;
    LaTelaLarg: TLabel;
    LaTelaAlt: TLabel;
    LaCopy: TLabel;
    procedure OKButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure CarregaIconeApp;
  public
    { Public declarations }
  end;

var
  FmSobre: TFmSobre;

implementation

uses dmkGeral, MyListas, UnGrl_AllOS;

{$R *.dfm}

procedure TFmSobre.CarregaIconeApp;
var
  AppDir: String;
  IconeLar, IconeSmal: HICON;
  Icone: TIcon;
begin
  AppDir := Application.ExeName;
  //
  if ExtractIconEx(PChar(AppDir), 0, IconeLar, IconeSmal, 1 ) = 2 then
  begin
    Icone := TIcon.Create;
    try
      Icone.Handle             := IconeLar;
      ImgIconeApp.Picture.Icon := Icone;
    finally
      Icone.Free;
    end;
  end
  else
  begin
    DestroyIcon(IconeLar);
    DestroyIcon(IconeSmal);
  end;
end;

procedure TFmSobre.FormCreate(Sender: TObject);
var
  App, Versao, DeviceName, DeviceID, OSName, OSNickName, OSVersion, DvcScreenH,
  DvcScreenW, Copy: String;
begin
  CarregaIconeApp;
  Grl_AllOS.ConfiguraDadosDispositivo(DeviceID, DeviceName, DvcScreenH,
    DvcScreenW, OSName, OSNickName, OSVersion);
  //
  App    := Application.Title;
  Versao := Geral.VersaoTxt2006(CO_VERSAO);
  Copy   := '� ' + Geral.FDT(Date, 25) + ' Dermatek';
  //
  LaAppNome.Caption   := 'Aplicativo: ' + App;
  LaAppVersao.Caption := 'Vers�o: ' + Versao;
  //
  LaDeviceNome.Caption := 'Nome: ' + DeviceName;
  //
  LaIMEINumero.Caption := 'N�mero: ' + DeviceID;
  //
  LaOSNome.Caption   := 'Nome: ' + OSName;
  LaOSDescri.Caption := 'Descri��o: ' + OSNickName;
  LaOSVersao.Caption := 'Vers�o: ' + OSVersion;
  //
  LaTelaLarg.Caption := 'Largura: ' + DvcScreenW;
  LaTelaAlt.Caption  := 'Altura: ' + DvcScreenH;
  //
  LaCopy.Caption       := Copy;
end;

procedure TFmSobre.OKButtonClick(Sender: TObject);
begin
  Close;
end;

end.

