object FmHorVerao: TFmHorVerao
  Left = 339
  Top = 185
  Caption = 'HOR-VERAO-001 :: Hor'#225'rio de Ver'#227'o'
  ClientHeight = 561
  ClientWidth = 814
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 814
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 1004
    object GB_R: TGroupBox
      Left = 762
      Top = 0
      Width = 52
      Height = 48
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 952
      ExplicitHeight = 47
      object ImgTipo: TdmkImage
        Left = 10
        Top = 6
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 714
      Height = 48
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      ExplicitLeft = 59
      ExplicitWidth = 686
      ExplicitHeight = 47
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 204
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Hor'#225'rio de Ver'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 204
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Hor'#225'rio de Ver'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 204
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Hor'#225'rio de Ver'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 814
    Height = 417
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 47
    ExplicitWidth = 805
    ExplicitHeight = 460
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 814
      Height = 417
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 805
      ExplicitHeight = 460
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 814
        Height = 417
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 805
        ExplicitHeight = 460
        object Memo1: TMemo
          Left = 2
          Top = 15
          Width = 810
          Height = 78
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Lines.Strings = (
            
              'O in'#237'cio do hor'#225'rio de ver'#227'o ser'#225' sempre no terceiro domingo de ' +
              'outubro e o t'#233'rmino do hor'#225'rio no terceiro domingo de fevereiro ' +
              'com exce'#231#227'o se a data coincidir com o carnaval e ai o t'#233'rmino se' +
              'r'#225' no domingo seguinte, o que ocorrer'#225' em 2015 e, por isso, o te' +
              'rmino ser'#225' dia 22 de fevereiro.'
            
              'O hor'#225'rio de ver'#227'o come'#231'ou a ser vigorado a partir de um decreto' +
              ' da Presid'#234'ncia da Rep'#250'blica, fundamentado em informa'#231#245'es dadas ' +
              'ao Minist'#233'rio das Minas e Energia que adotam o hor'#225'rio de ver'#227'o ' +
              'a partir de estudos t'#233'cnicos e indicando que Estados dever'#227'o ado' +
              'ta este hor'#225'rio de ver'#227'o. Este hor'#225'rio foi devidamente regulado ' +
              'em 2008 pela Casa Civil da Presid'#234'ncia da Rep'#250'blica pelo decreto' +
              ' n'#176' 6558 que inclusive definiu a data de in'#237'cio e t'#233'rmino do hor' +
              #225'rio de ver'#227'o.'
            '')
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 0
          WantReturns = False
          WordWrap = False
        end
        object DBGParamsEmp: TDBGrid
          Left = 2
          Top = 93
          Width = 810
          Height = 80
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          DataSource = DsParamsEmp
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGParamsEmpDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Filial'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EMP'
              Title.Caption = 'Nome'
              Width = 360
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'hVeraoAsk_TXT'
              Title.Caption = 'Perguntar'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'hVeraoIni_TXT'
              Title.Caption = 'In'#237'cio'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'hVeraoFim_TXT'
              Title.Caption = 'Final'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TZD_UTC_Str'
              Title.Caption = 'TZD UTC normal'
              Visible = True
            end>
        end
        object PnEdita: TPanel
          Left = 2
          Top = 173
          Width = 810
          Height = 242
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBackground = False
          ParentFont = False
          TabOrder = 2
          Visible = False
          ExplicitTop = 157
          ExplicitHeight = 282
          object GBEdita: TGroupBox
            Left = 0
            Top = 60
            Width = 810
            Height = 113
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Color = clBtnFace
            ParentBackground = False
            ParentColor = False
            TabOrder = 0
            ExplicitWidth = 1000
            object GroupBox43: TGroupBox
              Left = 10
              Top = 5
              Width = 932
              Height = 98
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' TZD UTC e altera'#231#245'es de hor'#225'rio de ver'#227'o: '
              TabOrder = 0
              object Panel64: TPanel
                Left = 2
                Top = 15
                Width = 928
                Height = 81
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitTop = 18
                ExplicitHeight = 78
                object Label259: TLabel
                  Left = 576
                  Top = 5
                  Width = 41
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'T'#233'rmino:'
                end
                object Label260: TLabel
                  Left = 462
                  Top = 5
                  Width = 30
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'In'#237'cio:'
                end
                object Label261: TLabel
                  Left = 310
                  Top = 5
                  Width = 144
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Perguntar de novo  a partir de:'
                end
                object Label262: TLabel
                  Left = 5
                  Top = 5
                  Width = 50
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'TZD UTC:'
                end
                object Label263: TLabel
                  Left = 222
                  Top = 30
                  Width = 82
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Hor'#225'rio de ver'#227'o:'
                end
                object SpeedButton21: TSpeedButton
                  Left = 116
                  Top = 25
                  Width = 21
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '?'
                  OnClick = SpeedButton21Click
                end
                object SbTZD_UTC: TSpeedButton
                  Left = 95
                  Top = 25
                  Width = 21
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '...'
                  OnClick = SbTZD_UTCClick
                end
                object Label277: TLabel
                  Left = 218
                  Top = 53
                  Width = 261
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Obter informa'#231#245'es sobre o hor'#225'rio de ver'#227'o'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -15
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsUnderline]
                  ParentFont = False
                  OnClick = Label277Click
                end
                object EdTZD_UTC: TdmkEdit
                  Left = 5
                  Top = 25
                  Width = 88
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 10
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000000000'
                  QryCampo = 'TZD_UTC'
                  UpdCampo = 'TZD_UTC'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdTZD_UTCChange
                end
                object EdTZD_UTC_TXT: TEdit
                  Left = 141
                  Top = 25
                  Width = 75
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ReadOnly = True
                  TabOrder = 1
                  Text = '00:00'
                end
                object TPhVeraoAsk: TdmkEditDateTimePicker
                  Left = 310
                  Top = 25
                  Width = 147
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 41927.000000000000000000
                  Time = 0.390190972218988500
                  TabOrder = 2
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  QryCampo = 'hVeraoAsk'
                  UpdCampo = 'hVeraoAsk'
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TPhVeraoIni: TdmkEditDateTimePicker
                  Left = 462
                  Top = 25
                  Width = 112
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 41927.000000000000000000
                  Time = 0.390190972218988500
                  TabOrder = 3
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  QryCampo = 'hVeraoIni'
                  UpdCampo = 'hVeraoIni'
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TPhVeraoFim: TdmkEditDateTimePicker
                  Left = 576
                  Top = 25
                  Width = 112
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 41927.000000000000000000
                  Time = 0.390190972218988500
                  TabOrder = 4
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  QryCampo = 'hVeraoFim'
                  UpdCampo = 'hVeraoFim'
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object CkTZD_UTC_Auto: TdmkCheckBox
                  Left = 5
                  Top = 53
                  Width = 210
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Obter fuso hor'#225'rio do servidor'
                  TabOrder = 5
                  OnClick = CkTZD_UTC_AutoClick
                  QryCampo = 'TZD_UTC_Auto'
                  UpdCampo = 'TZD_UTC_Auto'
                  UpdType = utYes
                  ValCheck = '1'
                  ValUncheck = '0'
                  OldValor = #0
                end
              end
            end
          end
          object GBConfirma: TGroupBox
            Left = 0
            Top = 180
            Width = 810
            Height = 62
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            TabOrder = 1
            ExplicitTop = 254
            ExplicitWidth = 1000
            object BtConfirma: TBitBtn
              Tag = 14
              Left = 15
              Top = 16
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Confirma'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtConfirmaClick
            end
            object Panel5: TPanel
              Left = 665
              Top = 15
              Width = 143
              Height = 45
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              ExplicitLeft = 656
              ExplicitHeight = 61
              object BtDesiste: TBitBtn
                Tag = 15
                Left = 9
                Top = 0
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Desiste'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtDesisteClick
              end
            end
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 810
            Height = 60
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            Enabled = False
            TabOrder = 2
            ExplicitWidth = 801
            object Label1: TLabel
              Left = 98
              Top = 5
              Width = 33
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Codigo'
              FocusControl = DBEdit1
            end
            object Label2: TLabel
              Left = 10
              Top = 5
              Width = 20
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Filial'
              FocusControl = DBEdit2
            end
            object Label3: TLabel
              Left = 187
              Top = 5
              Width = 45
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'NO_EMP'
              FocusControl = DBEdit3
            end
            object DBEdit1: TDBEdit
              Left = 98
              Top = 25
              Width = 84
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Codigo'
              DataSource = DsParamsEmp
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 14
              Top = 25
              Width = 84
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Filial'
              DataSource = DsParamsEmp
              TabOrder = 1
            end
            object DBEdit3: TDBEdit
              Left = 187
              Top = 25
              Width = 798
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NO_EMP'
              DataSource = DsParamsEmp
              TabOrder = 2
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 465
    Width = 814
    Height = 34
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 516
    ExplicitWidth = 1004
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 810
      Height = 17
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 801
      ExplicitHeight = 26
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 499
    Width = 814
    Height = 62
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 557
    ExplicitWidth = 1004
    object PnSaiDesis: TPanel
      Left = 661
      Top = 15
      Width = 151
      Height = 45
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 652
      ExplicitHeight = 52
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 0
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 659
      Height = 45
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 624
      ExplicitHeight = 52
      object BtAltera: TBitBtn
        Tag = 11
        Left = 15
        Top = 0
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAlteraClick
      end
    end
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrParamsEmpCalcFields
    SQL.Strings = (
      
        'SELECT *, IF(ent.Tipo=0, RazaoSocial, Nome) NO_EMP, Filial, emp.' +
        'Codigo,'
      'emp.TZD_UTC, emp.hVeraoAsk, emp.hVeraoIni, emp.hVeraoFim,'
      
        'IF(emp.hVeraoAsk <= "1899-12-30", "", DATE_FORMAT(emp.hVeraoAsk,' +
        ' "%d/%m/%Y")) hVeraoAsk_TXT, '
      
        'IF(emp.hVeraoIni <= "1899-12-30", "", DATE_FORMAT(emp.hVeraoIni,' +
        ' "%d/%m/%Y")) hVeraoIni_TXT, '
      
        'IF(emp.hVeraoFim <= "1899-12-30", "", DATE_FORMAT(emp.hVeraoFim,' +
        ' "%d/%m/%Y")) hVeraoFim_TXT'
      'FROM paramsemp emp'
      'LEFT JOIN entidades ent ON ent.Codigo=emp.Codigo'
      'WHERE TZD_UTC_Auto = 0'
      'ORDER BY NO_EMP')
    Left = 328
    Top = 140
    object QrParamsEmpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrParamsEmpFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrParamsEmpNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrParamsEmpTZD_UTC_Str: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TZD_UTC_Str'
      Size = 6
      Calculated = True
    end
    object QrParamsEmpTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrParamsEmphVeraoAsk: TDateField
      FieldName = 'hVeraoAsk'
    end
    object QrParamsEmphVeraoIni: TDateField
      FieldName = 'hVeraoIni'
    end
    object QrParamsEmphVeraoFim: TDateField
      FieldName = 'hVeraoFim'
    end
    object QrParamsEmphVeraoFim_TXT: TWideStringField
      FieldName = 'hVeraoFim_TXT'
      Size = 10
    end
    object QrParamsEmphVeraoIni_TXT: TWideStringField
      FieldName = 'hVeraoIni_TXT'
      Size = 10
    end
    object QrParamsEmphVeraoAsk_TXT: TWideStringField
      FieldName = 'hVeraoAsk_TXT'
      Size = 10
    end
    object QrParamsEmpTZD_UTC_Auto_UsuarioAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_UsuarioAceite'
    end
    object QrParamsEmpTZD_UTC_Auto_TermoAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_TermoAceite'
    end
    object QrParamsEmpTZD_UTC_Auto_DataHoraAceite: TDateTimeField
      FieldName = 'TZD_UTC_Auto_DataHoraAceite'
    end
    object QrParamsEmpTZD_UTC_Auto: TSmallintField
      FieldName = 'TZD_UTC_Auto'
    end
  end
  object DsParamsEmp: TDataSource
    DataSet = QrParamsEmp
    Left = 328
    Top = 188
  end
end
