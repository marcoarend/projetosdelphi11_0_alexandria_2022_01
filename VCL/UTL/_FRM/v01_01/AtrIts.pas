unit AtrIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, Mask,
  dmkDBEdit, MyDBCheck, UnDmkProcFunc, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmAtrIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdCodigo: TdmkDBEdit;
    EdNome: TdmkEdit;
    CkContinuar: TCheckBox;
    EdCodUsu: TdmkEdit;
    DBEdNome: TdmkDBEdit;
    EdControle: TdmkEdit;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Label11: TLabel;
    CBCorPie: TColorBox;
    RGEfeito: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCodUsuChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenAtrIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FNomeTabIts: String;
    FControle: Integer;
  end;

  var
  FmAtrIts: TFmAtrIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, dmkGeral,
DmkDAC_PF;

{$R *.DFM}

procedure TFmAtrIts.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, CodUsu, CorPie, Efeito: Integer;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := FControle;
  CodUsu         := EdCodUsu.ValueVariant;
  Nome           := EdNome.Text;
  CorPie         := CBCorPie.Selected;
  Efeito         := RGEfeito.ItemIndex;
  //
  Controle := UMyMod.BPGS1I32(FNomeTabIts,
    'Controle', '', '', tsDef, ImgTipo.SQLType, FControle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, FNomeTabIts, False, [
  'Codigo', 'CodUsu', 'Nome',
  'CorPie', 'Efeito'], [
  'Controle'], [
  Codigo, CodUsu, Nome,
  CorPie, Efeito], [
  Controle], True) then
  begin
    ReopenAtrIts(Controle);
    //
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType         := stIns;
      EdControle.ValueVariant := 0;
      EdCodUsu.ValueVariant   := 0;
      EdNome.Text             := '';
      CBCorPie.Selected       := 0;
      //
      EdCodUsu.SetFocus;
    end else Close;
  end;
end;

procedure TFmAtrIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAtrIts.EdCodUsuChange(Sender: TObject);
begin
  BtOK.Enabled := EdCodUsu.ValueVariant <> 0;
end;

procedure TFmAtrIts.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, FNomeTabIts, 'CodUsu', ['Codigo'],
      [Geral.IMV(DBEdCodigo.Text)], stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmAtrIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource   := FDsCab;
end;

procedure TFmAtrIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAtrIts.ReopenAtrIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
