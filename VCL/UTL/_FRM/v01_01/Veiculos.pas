unit Veiculos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkDBLookupComboBox, dmkEditCB, Variants;

type
  TFmVeiculos = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrVeiculos: TMySQLQuery;
    DsVeiculos: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    EdMarca: TdmkEditCB;
    CBMarca: TdmkDBLookupComboBox;
    Label3: TLabel;
    QrVeicMarcas: TMySQLQuery;
    EdPlaca: TdmkEdit;
    Label4: TLabel;
    DsVeicMarcas: TDataSource;
    QrVeicMarcasCodigo: TIntegerField;
    QrVeicMarcasNome: TWideStringField;
    SbVeicMarcas: TSpeedButton;
    EdEmpresa: TdmkEditCB;
    Label25: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label22: TLabel;
    EdMotorisPadr: TdmkEditCB;
    CBMotorisPadr: TdmkDBLookupComboBox;
    SbMotorista: TSpeedButton;
    QrMotorista: TMySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    EdModelo: TdmkEditCB;
    CBModelo: TdmkDBLookupComboBox;
    SbVeicModels: TSpeedButton;
    QrVeicModels: TMySQLQuery;
    QrVeicModelsCodigo: TIntegerField;
    QrVeicModelsNome: TWideStringField;
    DsVeicModels: TDataSource;
    QrVeiculosCodigo: TIntegerField;
    QrVeiculosEmpresa: TIntegerField;
    QrVeiculosRNTRC: TWideStringField;
    QrVeiculosMarca: TIntegerField;
    QrVeiculosRENAVAM: TWideStringField;
    QrVeiculosplaca: TWideStringField;
    QrVeiculostara: TIntegerField;
    QrVeiculoscapKG: TIntegerField;
    QrVeiculoscapM3: TIntegerField;
    QrVeiculostpProp: TWideStringField;
    QrVeiculostpVeic: TSmallintField;
    QrVeiculostpRod: TSmallintField;
    QrVeiculostpCar: TSmallintField;
    QrVeiculosUF: TWideStringField;
    QrVeiculosMotorisPadr: TIntegerField;
    QrVeiculosModelo: TIntegerField;
    QrVeiculosAnoModel: TIntegerField;
    QrVeiculosAnoFabri: TIntegerField;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    QrVeiculosAtivo: TSmallintField;
    QrVeiculosNO_Marca: TWideStringField;
    QrVeiculosNO_Modelo: TWideStringField;
    QrVeiculosNO_EMP: TWideStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    QrVeiculosNO_MOT: TWideStringField;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVeiculosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVeiculosBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbVeicMarcasClick(Sender: TObject);
    procedure SbMotoristaClick(Sender: TObject);
    procedure SbVeicModelsClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenVeicMarcas();
    procedure ReopenVeicModels();

  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmVeiculos: TFmVeiculos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, CfgCadLista, UnAppPF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVeiculos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVeiculos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVeiculosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVeiculos.DefParams;
begin
  VAR_GOTOTABELA := 'veiculos';
  VAR_GOTOMYSQLTABLE := QrVeiculos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := 'placa'; // CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vei.*, ');
  VAR_SQLx.Add('vma.Nome NO_Marca, vmo.Nome NO_Modelo,');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,');
  VAR_SQLx.Add('IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOT');
  VAR_SQLx.Add('FROM veiculos vei');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=vei.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades mot ON mot.Codigo=vei.MotorisPadr');
  VAR_SQLx.Add('LEFT JOIN veicmarcas vma ON vma.Codigo=vei.Marca');
  VAR_SQLx.Add('LEFT JOIN veicmodels vmo ON vmo.Codigo=vei.Modelo');
  VAR_SQLx.Add('WHERE vei.Codigo > 0');
  //
  VAR_SQL1.Add('AND vei.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vei.Placa Like :P0');
  //
end;

procedure TFmVeiculos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVeiculos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVeiculos.ReopenVeicMarcas();
begin
  UnDmkDAC_PF.AbreQuery(QrVeicMarcas, Dmod.MyDB);
end;

procedure TFmVeiculos.ReopenVeicModels();
begin
  UnDmkDAC_PF.AbreQuery(QrVeicModels, Dmod.MyDB);
end;

procedure TFmVeiculos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVeiculos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVeiculos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVeiculos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVeiculos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVeiculos.SbVeicMarcasClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'VeicMarcas', 60,
  ncGerlSeq1, 'Cadastro de Marcas de Ve�culos',
  [], False, Null, [], [], False);
  ReopenVeicMarcas();
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdMarca, CBMarca, QrVeicMarcas, VAR_CADASTRO);
end;

procedure TFmVeiculos.SbVeicModelsClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'VeicModels', 60,
  ncGerlSeq1, 'Cadastro de Modelos de Ve�culos',
  [], False, Null, [], [], False);
  ReopenVeicModels();
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdModelo, CBModelo, QrVeicModels, VAR_CADASTRO);
end;

procedure TFmVeiculos.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVeiculos.BtAlteraClick(Sender: TObject);
var
  Filial: Integer;
begin
  if (QrVeiculos.State <> dsInactive) and (QrVeiculos.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVeiculos, [PnDados],
      [PnEdita], EdEmpresa, ImgTipo, 'veiculos');
    Filial := DModG.ObtemFilialDeEntidade(QrVeiculosEmpresa.Value);
    EdEmpresa.ValueVariant := Filial;
    CBEmpresa.KeyValue     := Filial;
  end;
end;

procedure TFmVeiculos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVeiculosCodigo.Value;
  Close;
end;

procedure TFmVeiculos.BtConfirmaClick(Sender: TObject);
var
  RNTRC, Modelo,RENAVAM, placa, tpProp, UF: String;
  Filial, Codigo, Empresa, Marca,  AnoModel, AnoFabri, tara, capKG, capM3,
  tpVeic, tpRod, tpCar, MotorisPadr: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Filial         := EdEmpresa.ValueVariant;
  Empresa        := DmodG.QrEmpresasCodigo.Value;
  RNTRC          := '';
  Marca          := EdMarca.ValueVariant;
  Modelo         := EdModelo.ValueVariant;
  AnoModel       := 0;
  AnoFabri       := 0;
  RENAVAM        := '';
  placa          := Geral.SoNumeroELetra_TT(EdPlaca.ValueVariant);
  tara           := 0;
  capKG          := 0;
  capM3          := 0;
  tpProp         := '?';
  tpVeic         := 0;
  tpRod          := 0;
  tpCar          := 0;
  UF             := '??';
  MotorisPadr    := EdMotorisPadr.ValueVariant;
  //
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Informe a empresa') then Exit;
  if MyObjects.FIC(Length(Modelo) = 0, EdModelo, 'Defina o modelo!') then Exit;
  if MyObjects.FIC(Length(placa) = 0, EdPlaca, 'Defina a placa!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('veiculos', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'veiculos', False, [
  'Empresa', 'RNTRC', 'Marca',
  'Modelo', 'AnoModel', 'AnoFabri',
  'RENAVAM', 'placa', 'tara',
  'capKG', 'capM3', 'tpProp',
  'tpVeic', 'tpRod', 'tpCar',
  'UF', 'MotorisPadr'], [
  'Codigo'], [
  Empresa, RNTRC, Marca,
  Modelo, AnoModel, AnoFabri,
  RENAVAM, placa, tara,
  capKG, capM3, tpProp,
  tpVeic, tpRod, tpCar,
  UF, MotorisPadr], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVeiculos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'veiculos', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmVeiculos.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVeiculos, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'veiculos');
end;

procedure TFmVeiculos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa, DModG.QrEmpresas);
  //
  ReopenVeicMarcas();
  ReopenVeicModels();
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
end;

procedure TFmVeiculos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVeiculosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVeiculos.SbMotoristaClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdMotorisPadr.Text := IntToStr(VAR_ENTIDADE);
    CBMotorisPadr.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVeiculos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVeiculos.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVeiculosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVeiculos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVeiculos.QrVeiculosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVeiculos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVeiculos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVeiculosCodigo.Value,
    CuringaLoc.CriaFormLivre(AppPF.SQLPesqVeiculo(), Dmod.MyDB));
end;

procedure TFmVeiculos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVeiculos.QrVeiculosBeforeOpen(DataSet: TDataSet);
begin
  QrVeiculosCodigo.DisplayFormat := FFormatFloat;
end;

end.

