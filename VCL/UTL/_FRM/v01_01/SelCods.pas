unit SelCods;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, dmkDBGridDAC, MyDBCheck, UnDmkEnums;

type
  TFmSelCods = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBPrompt: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBGSel: TdmkDBGridDAC;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    LaTotal: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure AtivarTodos(Ativo: Integer);
    procedure ReopenSel(FFldUnique: String; Codigo: Integer);
    procedure ExecutaInsercaoTipoA();
  public
    { Public declarations }
    FSelCodsExecution: TSelCodsExecution;
    FSelected: Boolean;
    FTempTab, FFldAtivo, FFldUnique: String;
    // Inserir selecionados
    FDestMaster: array of String;
    FDestTab, FDestDetail, FDestSelec1: String;
    FValrMaster: array of Variant;
    FQrDetail, FQrExecSQL: TmySQLQuery;
    FExcluiAnteriores: Boolean;
  end;

  var
  FmSelCods: TFmSelCods;

implementation

uses UnMyObjects, ModuleGeral, DmkDAC_PF, UMySQLModule, Module;

{$R *.DFM}

procedure TFmSelCods.AtivarTodos(Ativo: Integer);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Codigo := DModG.QrSelCods.FieldByName(FFldUnique).AsInteger;
    UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DmodG.MyPID_DB, [
    'UPDATE ' + FTempTab + ' SET ',
    FFldAtivo + '=' + Geral.FF0(Ativo),
    '']);
    //
    ReopenSel(FFldUnique, Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelCods.BtNenhumClick(Sender: TObject);
begin
  AtivarTodos(0);
end;

procedure TFmSelCods.BtOKClick(Sender: TObject);
begin
  FSelected := True;
  case FSelCodsExecution of
    sceNone: ; // nada!
    sceExecA: ExecutaInsercaoTipoA();
    else Geral.MB_Aviso('Tipo de inser��o n�o definida!');
  end;
  Close;
end;

procedure TFmSelCods.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSelCods.BtTodosClick(Sender: TObject);
begin
  AtivarTodos(1);
end;

procedure TFmSelCods.ExecutaInsercaoTipoA();
var
  Selecio1, ValrDetail, I: Integer;
  Campos: array of string;
  Valores: array of Variant;
begin
  ValrDetail := 0;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo selecionados');
    //
    if FExcluiAnteriores then
    begin
      UMyMod.ExecutaMySQLQuery1(FQrExecSQL, [
      'INSERT INTO livre2 (Tabela, Campo, Codigo, Vezes, AlterWeb, Ativo) ',
      'SELECT "' + FDestTab + '" Tabela, "' + FDestDetail + '" Campo, ',
      FDestDetail + ' Codigo, 1 Vezes, 0 AlterWeb, 0 Ativo ',
      'FROM ' + FDestTab,
      'WHERE ' + FDestMaster[Length(FDestMaster)-1] + '=' +
      Geral.FF0(FValrMaster[Length(FDestMaster)-1]),
      ';',
      DELETE_FROM + ' ' + FDestTab,
      'WHERE ' + FDestMaster[Length(FDestMaster)-1] +
      '=' + Geral.FF0(FValrMaster[Length(FDestMaster)-1]),
      '']);
    end;
    //
    DModG.QrSelCods.First;
    while not DModG.QrSelCods.Eof do
    begin
      if DModG.QrSelCodsAtivo.Value = 1 then
      begin
        if FQrExecSQL.Database <> DmodG.MyPID_DB then
          ValrDetail := UMyMod.BPGS1I32_Reaproveita(FDestTab, FDestDetail, '', '', tsDef, stIns, 0)
        else
          ValrDetail := 0;
        Selecio1 := DModG.QrSelCods.FieldByName(FFldUnique).AsInteger;
        SetLength(Campos, Length(FDestMaster) + 1);
        SetLength(Valores, Length(FValrMaster) + 1);
        for I := 0 to Length(FDestMaster) -1 do
        begin
          Campos[I] := FDestMaster[I];
          Valores[I] := FValrMaster[I];
        end;
        Campos[Length(FDestMaster)] := FDestSelec1;
        Valores[Length(FDestMaster)] := Selecio1;
        //
        UMyMod.SQLInsUpd(FQrExecSQL, stIns, FDestTab, False, Campos, [
        FDestDetail],
        Valores, [
        ValrDetail], True);
      end;
      DModG.QrSelCods.Next;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    if FQrDetail <> nil then
    begin
      FQrDetail.Close;
      UnDmkDAC_PF.AbreQuery(FQrDetail, FQrDetail.DataBase);
      FQrDetail.Locate(FDestDetail, ValrDetail, []);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelCods.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSelCods.FormCreate(Sender: TObject);
begin
  FSelCodsExecution := sceNone;
  FExcluiAnteriores := True;
  FSelected := False;
  ImgTipo.SQLType := stLok;
  DBGSel.DataSource := DmodG.DsSelCods;
end;

procedure TFmSelCods.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSelCods.ReopenSel(FFldUnique: String; Codigo: Integer);
begin
  UnDmkDAC_PF.AbreQuery(DModG.QrSelCods, DModG.QrSelCods.Database);
  DModG.QrSelCods.Locate(FFldUnique, Codigo, []);
end;

end.
