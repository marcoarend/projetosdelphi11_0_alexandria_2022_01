object FmMudaTexto: TFmMudaTexto
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-012 :: Mudar Texto'
  ClientHeight = 262
  ClientWidth = 441
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 441
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 393
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 345
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 148
        Height = 32
        Caption = 'Mudar Texto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 148
        Height = 32
        Caption = 'Mudar Texto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 148
        Height = 32
        Caption = 'Mudar Texto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 441
    Height = 100
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 441
      Height = 100
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      ExplicitHeight = 467
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 441
        Height = 100
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 812
        ExplicitHeight = 467
        object Label1: TLabel
          Left = 16
          Top = 8
          Width = 151
          Height = 13
          Caption = 'Texto a ser substitu'#237'do (parcial):'
        end
        object Label2: TLabel
          Left = 16
          Top = 52
          Width = 95
          Height = 13
          Caption = 'Novo texto (parcial):'
        end
        object EdAtual: TEdit
          Left = 16
          Top = 24
          Width = 409
          Height = 21
          TabOrder = 0
        end
        object EdNovo: TEdit
          Left = 16
          Top = 68
          Width = 409
          Height = 21
          TabOrder = 1
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 148
    Width = 441
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 437
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 192
    Width = 441
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 295
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 293
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
end
