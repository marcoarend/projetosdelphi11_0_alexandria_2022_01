unit GetPercent;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmGetPercent = class(TForm)
    PainelDados: TPanel;
    EdPercent: TdmkEdit;
    LaPercent: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    procedure BtOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPercentExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCasasPercent: Integer;
    FSinal: TSinal;
  end;

  var
  FmGetPercent: TFmGetPercent;

implementation

{$R *.DFM}

uses UnMyObjects, UnInternalConsts;

procedure TFmGetPercent.BtOKClick(Sender: TObject);
begin
  VAR_PORCENTAGEM := Geral.DMV(EdPercent.Text);
  Close;
end;

procedure TFmGetPercent.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGetPercent.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGetPercent.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGetPercent.EdPercentExit(Sender: TObject);
begin
  EdPercent.DecimalSize := FCasasPercent;
end;

procedure TFmGetPercent.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCasasPercent := 6;
  FSinal := siNegativo;
end;

end.
