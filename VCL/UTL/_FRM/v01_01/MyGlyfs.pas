unit MyGlyfs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, UnInternalConsts2, UnInternalConsts, Grids,
  DBGrids, ImgList, StdCtrls, Menus, Buttons,
  ExtCtrls, dmkGeral, System.ImageList, Vcl.ComCtrls,
  UnDmkProcFunc, Vcl.ExtDlgs,
  System.NetEncoding, UnProjGroup_Consts;

const
  MaxBcos = 999;

type

  THackDBGrid = class(TDBGrid);
  TFmMyGlyfs = class(TForm)
    PnInativo: TPanel;
    Label1: TLabel;
    EdNome: TEdit;
    Label2: TLabel;
    EdCPF: TEdit;
    PnImprime: TPanel;
    ImgChkTV: TImageList;
    Timer1: TTimer;
    ListaChecks: TImageList;
    ListaCkPerc01: TImageList;
    ListaCheck3: TImageList;
    ImageList1: TImageList;
    Img_Bco_001: TImage;
    Img_Bco_104: TImage;
    Img_Bco_341: TImage;
    Img_Bco_399: TImage;
    Img_Bco_008: TImage;
    Img_Bco_237: TImage;
    Img_Bco_356: TImage;
    Img_Bco_409: TImage;
    Img_Bco_748: TImage;
    Img_Bco_756: TImage;
    Lista_32x64A: TImageList;
    ListaChecks2: TImageList;
    ListaChecks5: TImageList;
    ListaChecks4: TImageList;
    Shape1: TShape;
    Label3: TLabel;
    Lista_32X32_Textos: TImageList;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    ListaPesqGrid: TImageList;
    Image1: TImage;
    ProgressBar1: TProgressBar;
    ImageToB64: TImage;
    dlgopen: TOpenPictureDialog;
    Memo: TMemo;
    Lista_32x64B: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    LogosBcos: array[000..MaxBcos] of Integer;
  public
    { Public declarations }
    FForm: TForm;
{ Desabilitado 2011-07-01
    function  SolicitaSenha: Boolean;
    function  MasSenha: String;
    function  MasLogin: String;
}
    function  NomeClie: String;    function  Document: String;

    // Procedures Logo Banco
    function  LogoBancoExiste(Banco: Integer): Boolean;
    function  CaminhoLogoBanco(Banco: Integer): String;
    procedure PreparaLogoBanco(Banco: Integer);
    // Fim procedures Logo Banco
    //==========================================================================
    // Procedurea functions de VCL Skin

    // Fim procedures e functions de VCL Skin
    //

    procedure DefineGlyphBtnTag(BitBtn: TBitBtn);
    procedure HintDeBitBtn(BitBtn: TBitBtn);
    procedure DefineGlyfs(ScreenActiveForm: TForm);
{$IfDef cSkinRank} //Berlin
    procedure DefineGlyfsTDI(sd1: TSkinData; Sender: TObject);
{$EndIf} //Berlin
{$IfDef cAlphaSkin} //Berlin
    //procedure DefineGlyfsTDI2(sm1: TsSkinManager; Sender: TObject);
{$EndIf} //Berlin
    function  ConfiguraFormDock(Form: TCustomForm): Boolean;
    procedure ObtemBitmapDeGlyph(Index: Integer; BitBtn: TBitBtn);
    function  DefineIdTxtBmp(): Integer;
    procedure PreparaChecks(Lista: Integer);
    //
    function  CalculaCanvasTextWidth(Texto, FontName: String; FontSize:
              Integer): Integer;
    procedure ExportaGlyfs(ImageList: TImageList; PB1: TProgressBar;
              LaAviso1, LaAviso2: TLabel);
    procedure DrawGrid4(Grade: TStringGrid; const Rect: TRect; FixedCols, Valor: Integer);
    procedure CarregaPNGdoBD(ImgPngB64: TImage; TxtPngB64: String);
    function  SalvaPNGDeArquivoNoBD(const Form: TForm; const MaxWidth,
              MaxHeight: Integer; var TxtB64, Path: String): Boolean;

  end;

const
  FLogoBancoDir = CO_DIR_RAIZ_DMK + '\Logos\Bancos\';
  FLogoListaChk = CO_DIR_RAIZ_DMK + '\Logos\Checks\';

var

  FmMyGlyfs: TFmMyGlyfs;

implementation

uses UnMyObjects, Principal, MyListas,
{$IfNDef NAO_USA_DB_GERAL}UMySQLModule,  {$EndIf}
Module;

{$R *.DFM}

procedure TFmMyGlyfs.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  //VAR_LISTA_BITMAPS_TITLE_GRID := ListaPesqGrid;
  for I := 0 to MaxBcos do
    LogosBcos[I] := 0;
  LogosBcos[001] := 001; // BB - Banco do Brasil
  LogosBcos[008] := 008; // Santander
  LogosBcos[033] := 008; // Santander
  LogosBcos[353] := 008; // Santander
  LogosBcos[104] := 104; // CEF - Caixa Econ�mica Federal
  LogosBcos[237] := 237; // Bradesco
  LogosBcos[341] := 341; // Ita�
  LogosBcos[356] := 356; // Banco Real (Santader)
  LogosBcos[399] := 399; // HSBC
  LogosBcos[409] := 409; // Unibanco (Ita�)
  LogosBcos[748] := 748; // Sicredi
  LogosBcos[756] := 756; // Sicoob
  //
  if ListaChecks4.Count <> 12 then
    Geral.MB_Aviso(
    'UPDATE na ListaChecks4 sem UPDATE em "function IndiceCheck4"');
  if ListaChecks5.Count <> 11 then
    Geral.MB_Aviso(
    'UPDATE na ListaChecks5 sem UPDATE em "function IndiceCheck5"');
  //
(* TODO TAdv*)
  if (VAR_COR_TIT_A = 0) and (VAR_COR_TIT_B = 0) and (VAR_COR_TIT_C = 0) then
  begin
    VAR_COR_TIT_A := $00FEF6F0; //AdvOfficePagerOfficeStyler.TabAppearance.ColorSelected;
    VAR_COR_TIT_B := clNone;//$00FAF1E9; //AdvOfficePagerOfficeStyler.TabAppearance.ColorSelectedTo;
    VAR_COR_TIT_C := clNone; //$008B4215; //AdvOfficePagerOfficeStyler.TabAppearance.TextColor;
    VAR_COR_AVISO_C := clNone; //$008B4215;  //AdvOfficePagerOfficeStyler.TabAppearance.TextColorSelected;
    VAR_COR_AVISO_A := $00E8C7AE; //AdvOfficePagerOfficeStyler.TabAppearance.ShadowColor;
  end;
(**)
end;

procedure TFmMyGlyfs.Button2Click(Sender: TObject);
begin
end;

{ Desabilitado 2011-07-01
function TFmMyGlyfs.SolicitaSenha: Boolean;
begin
  Result := Uppercase(Grade.Cells[01,04]) = 'SIM';
end;

function TFmMyGlyfs.MasLogin: String;
begin
  Result := Uppercase(Grade.Cells[01,05]);
end;

function TFmMyGlyfs.MasSenha: String;
begin
  Result := Uppercase(Grade.Cells[01,06]);
end;

}

function TFmMyGlyfs.NomeClie: String;
begin
  Result := EdNome.Text;
end;

procedure TFmMyGlyfs.ObtemBitmapDeGlyph(Index: Integer; BitBtn: TBitBtn);
{
var
  Bmp: TBitmap;
}
begin
{  Result := nil;
  Bmp := TBitmap.Create;
  try
    Bmp.Width := 64;
    Bmp.Height := 32;
}
  BitBtn.Glyph := nil;
  if Index > 500 then
    Lista_32x64B.GetBitmap(Index-501, BitBtn.Glyph)
  else
    Lista_32x64A.GetBitmap(Index, BitBtn.Glyph);
{
    if Bmp <> nil then
      Result := Bmp;
  finally
    Bmp.Free
  end;
}
end;

function TFmMyGlyfs.Document: String;
begin
  Result := EdCPF.Text;
end;

procedure TFmMyGlyfs.DrawGrid4(Grade: TStringGrid; const Rect: TRect; FixedCols,
  Valor: Integer);
var
  W1,H1,Glyph: Integer;
  Pos: MyArrayI02;
  Bmp: TBitmap;
begin
  if Valor < 0 then Valor := 0;
  if Valor > 2 then Valor := 2;
  with Grade.Canvas do
  begin
    W1 := FmMyGlyfs.ListaCheck3.Width;
    H1 := FmMyGlyfs.ListaCheck3.Height;
    Pos := dmkPF.CoordenadasIniciais(Rect.Left,Rect.Top,Rect.Right,Rect.Bottom,W1,H1);
    FillRect(Rect);
    //TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    Glyph := {(VAR_IDTXTBMP*2)+}Valor;
    Bmp := TBitmap.Create;
    try
      Bmp.Width := W1;
      Bmp.Height := H1;
      FmMyGlyfs.ListaCheck3.GetBitmap(Glyph, Bmp);
      Draw(Pos[1], Pos[2], Bmp);
    finally
      Bmp.Free;
    end;
  end;
  THackDBGrid(Grade).FixedCols := FixedCols;
end;

procedure TFmMyGlyfs.ExportaGlyfs(ImageList: TImageList; PB1: TProgressBar;
LaAviso1, LaAviso2: TLabel);
const
  BaseCam = 'C:\Dermatek\Glyfs\';
var
  I, N: Integer;
  Caminho, Arquivo: String;
begin
  N := 0;
  Caminho := BaseCam + ImageList.Name + '\';
  ForceDirectories(Caminho);
  if PB1 <> nil then
  begin
    PB1.Position := 0;
    PB1.Max      := ImageList.Count;
  end;
  for I := 0 to ImageList.Count - 1 do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    Arquivo := Caminho + Geral.FFN(I, 4) + '.bmp';
    //Lista_32x64.GetBitmap(I, Image1.Picture.Bitmap);
    if I > 500 then
      Lista_32x64B.GetBitmap(I-501, Image1.Picture.Bitmap)
    else
      Lista_32x64A.GetBitmap(I, Image1.Picture.Bitmap);
    if Image1.Picture.Bitmap <> nil then
    begin
      Image1.Picture.Bitmap.SaveToFile(Arquivo);
      N := N + 1;
    end;
  end;
  Geral.MB_Info(Geral.FF0(N) + ' bitmaps foram salvos em "' + Caminho + '"');
end;

procedure TFmMyGlyfs.PreparaChecks(Lista: Integer);
var
  //B, Itens: Integer;
  I, Fez: Integer;
  //Logo: TImage;
  //Nome,
  Dir, Arq, Chk: String;
  ListaChecks: TImageList;
  Bmp: TBitMap;
begin
  Fez := 0;
{ N�o precisei usar}
  Bmp := TBitmap.Create;
  try
    case Lista of
      4: ListaChecks := ListaChecks4;
      5: ListaChecks := ListaChecks5;
      else
      begin
        Geral.MB_Aviso('ListaChecks n�o implementada:' + Geral.FF0(Lista));
        Exit;
      end;
    end;
    //
    for I := 0 to ListaChecks.Count - 1 do
    begin
      Fez := 0;
      Chk := Geral.FFn(I, 4);
      Dir := FLogoListaChk + 'ListaChecks' + Geral.FFn(Lista, 3) + '\';
      Arq := Dir + Chk + '.bmp';
      if not FileExists(Arq) then
      begin
        ForceDirectories(Dir);
        ListaChecks.GetBitmap(I, Bmp);
        Bmp.SaveToFile(Arq);
        Fez := Fez + 1;
      end;
    end;
    if Fez > 0 then Geral.MB_Aviso(Geral.FF0(Fez) +
    ' bitmaps de check foram exportados para arquivo!' + sLineBreak +
    'Reinicie o aplicativo e tente novamente.' + sLineBreak +
    'Se esta mensagem for exibida novamente para o mesmo banco, avise a DERMATEK!');
  finally
    Bmp.Free;
  end;
end;

procedure TFmMyGlyfs.PreparaLogoBanco(Banco: Integer);
var
  Tem: Boolean;
  B: Integer;
  Logo: TImage;
  Arq, Bco, Nome: String;
begin
  Tem := False;
  Bco := FormatFloat('000', Banco);
  Arq := FLogoBancoDir + Bco + '.bmp';
  if not FileExists(Arq) then
  begin
    ForceDirectories(FLogoBancoDir);
    B := LogosBcos[Banco];
    if B <> 0 then
    begin
      //Logo := nil;
      Nome := 'Img_Bco_' + FormatFloat('000', B);
      if FmMyGlyfs.FindComponent(Nome) <> nil then
      begin
        try
          Logo := TImage(FmMyGlyfs.FindComponent(Nome));
          if FileExists(Arq) then
            DeleteFile(Arq);
          Logo.Picture.SaveToFile(Arq);
          Logo.Free;
          Tem := True;
        except
          Geral.MB_Erro('Componente "' + Nome + '" n�o � TImage');
        end;
      end;
    end;
  end else Tem := True;
  if not Tem then Geral.MB_Aviso('O banco ' + Bco +
  ' n�o possui logotipo cadastrado!' + sLineBreak +
  'Reinicie o aplicativo e tente novamente.' + sLineBreak +
  'Se esta mensagem for exibida novamente para o mesmo banco, avise a DERMATEK!');
end;

function TFmMyGlyfs.SalvaPNGDeArquivoNoBD(const Form: TForm; const MaxWidth,
              MaxHeight: Integer; var TxtB64, Path: String): Boolean;
const
  Titulo = 'Defini��o Imagem de bot�o';
var
  LInput : TMemoryStream;
  LOutput: TMemoryStream;
begin
  Result := False;
  TxtB64 := '';
  // show open file dialog
  if
    FmMyGlyfs.dlgOpen.Execute then
    begin
    // load image into image component (TPicture)
    FmMyGlyfs.ImageToB64.Picture.LoadFromFile(FmMyGlyfs.dlgOpen.FileName);
    LInput := TMemoryStream.Create;
    LOutput := TMemoryStream.Create;
    // write picture to stream and encode
    try
      FmMyGlyfs.ImageToB64.Picture.SaveToStream(LInput);
      LInput.Position := 0;
      TNetEncoding.Base64.Encode( LInput, LOutput );
      LOutput.Position := 0;
      FmMyGlyfs.Memo.Lines.LoadFromStream( LOutput );
      //tXTb64.LoADfROMsTREAM(
      //
      //SetString(TxtB64, PChar(LOutput.Memory), (LOutput.Size div SizeOf(Char)));
      TxtB64 := FmMyGlyfs.Memo.Lines.Text;
      Path := FmMyGlyfs.dlgOpen.FileName;
      Result := True;
    finally
      LInput.Free;
      LOutput.Free;
    end;
  end;
end;

procedure TFmMyGlyfs.Timer1Timer(Sender: TObject);
var
  H, W : Integer;
begin
  Timer1.Enabled := False;
  if FForm = nil then
  begin
    //Result := False;
    Exit;
  end;
  // 2011-09-23
  FForm.Visible := False;
  FForm.Update;
  Application.ProcessMessages();
  // fim 2011-09-23
  H := FForm.Height;
  W := FForm.Width;
  if H > 600 then H := 600;
  if W > 800 then W := 800;
  FForm.Constraints.MinHeight := H;
  FForm.Constraints.MinWidth  := W;
  if FForm.WindowState = wsMaximized then
  FForm.Top := 0;
  if (H > 530) and (Screen.Height <= 600) then FForm.Top := 0;
  //
  FmMyGlyfs.DefineGlyfs(Screen.ActiveForm);
  //
  //?.DefineCompoCor();
  //
  MyObjects.ControleCor(Self);
  //
  MyObjects.ControleStrings(Self);
  //
  // 2011-09-21
  FmPrincipal.ReCaptionComponentesDeForm(FForm);
  //
  // 2011-09-23
  FForm.Visible := True;
  FForm.Update;
  Application.ProcessMessages();
  // fim 2011-09-23
  //
  //Result := True;
end;

function TFmMyGlyfs.LogoBancoExiste(Banco: Integer): Boolean;
var
  Arq, Bco: String;
begin
  Bco := FormatFloat('000', Banco);
  Arq := FLogoBancoDir + Bco + '.bmp';
  Result := FileExists(Arq);
  if not Result then
  begin
    PreparaLogoBanco(Banco);
    Result := FileExists(Arq);
  end;
end;

function TFmMyGlyfs.CalculaCanvasTextWidth(Texto, FontName: String; FontSize:
  Integer): Integer;
begin
  Result := 9;
  Canvas.Font.Name := FontName;
  Canvas.Font.Size := FontSize;
  Result := Canvas.TextWidth(Texto);
end;

function TFmMyGlyfs.CaminhoLogoBanco(Banco: Integer): String;
var
  Bco, Arq: String;
begin
  Bco := FormatFloat('000', Banco);
  Arq := FLogoBancoDir + Bco + '.bmp';
  Result := Arq;
end;

procedure TFmMyGlyfs.CarregaPNGdoBD(ImgPngB64: TImage; TxtPngB64: String);
var
   LInput: TMemoryStream;
  LOutput: TMemoryStream;
begin
  if TxtPngB64 <> EmptyStr then
  begin
(*
  if dlgOpen.Execute then
  begin
    Memo.Lines.LoadFromFile(dlgOpen.FileName);
*)
      Memo.Lines.Text := TxtPngB64;
      LInput := TMemoryStream.Create;
      LOutput := TMemoryStream.Create;
      try
        Memo.Lines.SaveToStream(LInput);
        LInput.Position := 0;
        TNetEncoding.Base64.Decode( LInput, LOutput );
        LOutput.Position := 0;
        ImgPngB64.Picture.LoadFromStream(LOutput);
        ImgPngB64.Visible := True;
      finally
        LInput.Free;
        LOutput.Free;
      end;
(*
    end;
*)
  end else
    ImgPngB64.Visible := False;
end;

procedure TFmMyGlyfs.DefineGlyphBtnTag(BitBtn: TBitBtn);
begin
  case BitBtn.Tag of
    001: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(004, BitBtn); //SpeedButton1
    002: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(005, BitBtn); //SpeedButton2
    003: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(006, BitBtn); //SpeedButton3
    004: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(007, BitBtn); //SpeedButton4
    005: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(000, BitBtn); //SbImprime
    006: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(026, BitBtn); //SbNovo
    007: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(011, BitBtn); //SbNumero
    008: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(012, BitBtn); //SbNome
    009: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(013, BitBtn); //SbQuery
    010: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(129, BitBtn); //BtInclui
    011: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(194, BitBtn); //BtAltera
    012: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(192, BitBtn); //BtExclui
    013: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(190, BitBtn); //BtSaida
    014: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(191, BitBtn); //BtConfirma
    015: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(010, BitBtn); //BtDesiste
    018: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(020, BitBtn); //BtRefresh
    019: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(198, BitBtn); //BtImportar
    020: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(176, BitBtn); //BtPagamento
    021: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(199, BitBtn); //BtCheque
    022: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(002, BitBtn); //BtPesquisa
    023: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(037, BitBtn); //BtNivelAcima
    024: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(034, BitBtn); //BtSalvar
    025: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(038, BitBtn); //BtAbrir
    026: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(038, BitBtn); //BtAbrir
    027: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(040, BitBtn); //BtPreview
    028: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(038, BitBtn); //BtAbrir
    029: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(018, BitBtn); 
    030:
    begin
      case CO_DMKID_APP of
        2:  //Bluederm
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(447, BitBtn);//
        6:  //Planning
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(437, BitBtn);//
        24: //Bugstrol
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(522, BitBtn);//
        28: //Toolrent
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(497, BitBtn);//
        else
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(115, BitBtn);//
      end;
    end;
    031: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(031, BitBtn);
    032: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(032, BitBtn);
    //
    034: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(034, BitBtn);
    035: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(035, BitBtn);
    //
    038: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(038, BitBtn);
    039: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(039, BitBtn);
    040: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(040, BitBtn);
    041: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(041, BitBtn);
    //
    047: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(047, BitBtn);
    048: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(048, BitBtn);
    //
    056: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(056, BitBtn);
    //
    058: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(058, BitBtn);
    //
    059: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(059, BitBtn);
    //
    066: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(066, BitBtn); //
    067: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(067, BitBtn); //
    068: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(068, BitBtn); //
    //
    073: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(073, BitBtn); //
    075: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(075, BitBtn); //
    //
    079: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(079, BitBtn); //
    //
    086: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(086, BitBtn); //
    //
    089: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(089, BitBtn); //
    //
    092: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(092, BitBtn); //
    //
    094: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(094, BitBtn); //
    095: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(095, BitBtn); //
    //
    100: (*BitBtn.Glyph := *)ObtemBitmapDeGlyph(100, BitBtn); 
    101: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(150, BitBtn); //BtEntidades2
    102: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(033, BitBtn); //BtImagem BtImagem1
    103: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(023, BitBtn); //BtCEP
    104: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(024, BitBtn); //BtCopiar
    105: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(129, BitBtn);
    106: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(192, BitBtn);
    107: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(196, BitBtn); //BtCalendario
    108: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(215, BitBtn); //BtCheckImp
    //109: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(211, BitBtn); //BtSinaleira
    110: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(150, BitBtn); //
    111: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(155, BitBtn); //
    112: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(169, BitBtn); //
    113: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(177, BitBtn); //
    114: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(177, BitBtn); //
    115: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(198, BitBtn); //
    116: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(236, BitBtn); //BtEuro
    117: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(210, BitBtn); //BtChequeDeposito
    118: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(074, BitBtn); //
    119: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(216, BitBtn); //BtNavegar
    120: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(202, BitBtn);
    121: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(088, BitBtn);
    122: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(150, BitBtn);
    123: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(099, BitBtn);
    124: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(178, BitBtn);
    125: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(076, BitBtn);
    126: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(138, BitBtn);
    127: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(029, BitBtn);
    128: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(030, BitBtn);
    129: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(217, BitBtn);
    130: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(218, BitBtn);
    131: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(211, BitBtn);
    132: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(150, BitBtn);
    133: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(159, BitBtn);
    134: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(163, BitBtn);
    135: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(200, BitBtn);// BtChequeDev
    136: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(200, BitBtn);//
    137: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(174, BitBtn);// BtRefazExclui
    138: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(201, BitBtn);// BtChequeOcorr
    139: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(213, BitBtn);// BtChequePror
    140: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(199, BitBtn);// BtCheque
    141: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(154, BitBtn);// BtProtecao
    142: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(208, BitBtn);// BtEuroExport
    143: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(136, BitBtn);//
    144: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(207, BitBtn);//
    145: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(206, BitBtn);//
    146: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(199, BitBtn);//
    147: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(198, BitBtn);//
    148: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(204, BitBtn);//
    149: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(209, BitBtn);//
    150: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(203, BitBtn);//
    151: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(155, BitBtn);//
    152: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(180, BitBtn);//
    153: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(027, BitBtn);//
    154: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(214, BitBtn);//
    155: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(041, BitBtn);//
    156: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(212, BitBtn);//
    157: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(160, BitBtn);//
    158: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(033, BitBtn);//
    159: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(205, BitBtn);//
    160: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(158, BitBtn);//
    161: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(219, BitBtn);//
    162: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(220, BitBtn);//
    163: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(184, BitBtn);//
    164: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(221, BitBtn);//
    165: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(222, BitBtn);//
    166: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(223, BitBtn);//
    167: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(224, BitBtn);//
    168: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(225, BitBtn);//
    169: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(068, BitBtn);//
    170: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(068, BitBtn);//
    171: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(226, BitBtn);//
    172: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(226, BitBtn);//
    173: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(227, BitBtn);//
    174: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(067, BitBtn);//
    175: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(139, BitBtn);//
    176: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(228, BitBtn);//
    177: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(042, BitBtn);//
    178: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(136, BitBtn);// Receitas
    179: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(131, BitBtn);// BtOS
    180: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(153, BitBtn);// BtCalc
    181: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(116, BitBtn);// BtCadastro
    182: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(068, BitBtn);
    183: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(229, BitBtn);
    184: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(106, BitBtn);// BitBtn1
    185: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(231, BitBtn);//
    186: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(232, BitBtn);//
    187: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(135, BitBtn);// BtPagtos
    188: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(233, BitBtn);// BtAdia
    189: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(234, BitBtn);//
    190: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(235, BitBtn);//
    191: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(196, BitBtn);//
    192: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(198, BitBtn);//
    193: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(180, BitBtn);//
    194: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(179, BitBtn);//
    195: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(141, BitBtn);//
    197: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(197, BitBtn);//
    199: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(199, BitBtn);//
    200: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(240, BitBtn);//
    201: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(241, BitBtn);//
    202: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(242, BitBtn);//
    203: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(243, BitBtn);//
    204: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(244, BitBtn);//
    205: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(245, BitBtn);//
    206: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(246, BitBtn);//
    207: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(247, BitBtn);//
    208: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(248, BitBtn);//
    209: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(249, BitBtn);//
    210: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(250, BitBtn);//
    211: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(251, BitBtn);//
    212: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(252, BitBtn);//
    213: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(253, BitBtn);//
    214: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(254, BitBtn);//
    215: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(255, BitBtn);//
    216: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(256, BitBtn);//
    217: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(257, BitBtn);//
    218: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(258, BitBtn);//
    219: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(259, BitBtn);//
    220: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(260, BitBtn);//
    221: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(261, BitBtn);//
    222: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(262, BitBtn);//
    223: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(130, BitBtn);//
    224: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(263, BitBtn);//
    225: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(136, BitBtn);//
    226: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(274, BitBtn);//
    227: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(033, BitBtn);//
    228: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(264, BitBtn);//
    229: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(265, BitBtn);//
    230: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(266, BitBtn);//
    231: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(267, BitBtn);//
    232: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(268, BitBtn);//
    233: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(269, BitBtn);//
    234: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(270, BitBtn);//
    235: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(271, BitBtn);//
    236: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(077, BitBtn);//
    237: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(031, BitBtn);//
    238: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(180, BitBtn);//
    239: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(178, BitBtn);//
    240: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(272, BitBtn);//
    241: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(134, BitBtn);//
    242: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(099, BitBtn);//
    243: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(183, BitBtn);//
    244: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(066, BitBtn);//
    245: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(219, BitBtn);//
    246: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(068, BitBtn);//
    247: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(050, BitBtn);//
    248: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(051, BitBtn);//
    249: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(052, BitBtn);//
    250: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(053, BitBtn);//
    251: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(054, BitBtn);//
    252: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(055, BitBtn);//
    253: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(041, BitBtn);//
    254: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(153, BitBtn);//
    255: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(058, BitBtn);//
    256: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(057, BitBtn);//
    257: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(162, BitBtn);//
    258: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(273, BitBtn);//
    259: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(277, BitBtn);//
    260: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(278, BitBtn);//
    261: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(151, BitBtn);//
    262: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(162, BitBtn);//
    263: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(076, BitBtn);//
    264: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(279, BitBtn);// Carteira
    265: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(279, BitBtn);// Carteira
    266: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(280, BitBtn);// Carteira
    267: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(281, BitBtn);//
    268: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(282, BitBtn);//
    269: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(283, BitBtn);//
    270: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(284, BitBtn);//
    271: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(028, BitBtn);//
    272: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(285, BitBtn);//
    273: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(286, BitBtn);//
    274: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(287, BitBtn);//
    275: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(288, BitBtn);//
    276: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(289, BitBtn);//
    277: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(291, BitBtn);//
    278: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(290, BitBtn);//
    279: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(187, BitBtn);//
    280: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(292, BitBtn);//
    281: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(293, BitBtn);//
    282: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(294, BitBtn);//
    283: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(178, BitBtn);//
    284: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(295, BitBtn);//
    285: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(298, BitBtn);//
    286: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(158, BitBtn);//
    287: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(297, BitBtn);//
    288: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(299, BitBtn);//
    289: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(156, BitBtn);//
    290: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(101, BitBtn);//
    291: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(117, BitBtn);//
    292: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(134, BitBtn);//
    293: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(295, BitBtn);//
    294: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(067, BitBtn);//
    //
    295: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(460, BitBtn);//
    296: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(296, BitBtn);//
    //
    300: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(300, BitBtn);//
    301: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(301, BitBtn);//
    302: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(302, BitBtn);//
    303: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(303, BitBtn);//
    304: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(304, BitBtn);//
    305: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(305, BitBtn);//
    306: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(306, BitBtn);//
    307: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(307, BitBtn);//
    308: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(308, BitBtn);//
    309: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(309, BitBtn);//
    310: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(310, BitBtn);//
    311: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(311, BitBtn);//
    316: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(316, BitBtn);//
    318: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(318, BitBtn);//
    319: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(319, BitBtn);//
    320: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(320, BitBtn);//
    321: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(321, BitBtn);//
    322: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(322, BitBtn);//
    323: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(323, BitBtn);//
    324: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(324, BitBtn);//
    325: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(325, BitBtn);//
    326: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(326, BitBtn);//
    327: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(327, BitBtn);//
    328: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(328, BitBtn);//
    329: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(329, BitBtn);//
    330: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(330, BitBtn);//
    331: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(331, BitBtn);//
    332: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(332, BitBtn);//
    333: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(333, BitBtn);//
    334: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(334, BitBtn);//
    335: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(335, BitBtn);//
    336: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(336, BitBtn);//
    337: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(337, BitBtn);//
    338: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(338, BitBtn);//
    339: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(339, BitBtn);//
    340: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(340, BitBtn);//
    //
    348: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(348, BitBtn);//
    //
    350: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(350, BitBtn);//
    351: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(351, BitBtn);//
    352: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(352, BitBtn);//
    353: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(353, BitBtn);//
    354: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(354, BitBtn);//
    //
    358: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(358, BitBtn);//
    359: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(359, BitBtn);//
    360: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(360, BitBtn);//
    //
    363: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(363, BitBtn);//
    364: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(364, BitBtn);//
    //
    366: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(366, BitBtn);//
    367: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(367, BitBtn);//
    //
    378: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(378, BitBtn);//
    //
    387: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(387, BitBtn);//
    //
    391: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(391, BitBtn);//
    //
    393: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(393, BitBtn);//
    394: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(394, BitBtn);//
    //
    396: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(396, BitBtn);//
    //
    398: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(398, BitBtn);//
    399: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(399, BitBtn);//
    //
    406: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(406, BitBtn);//
    407: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(407, BitBtn);//
    408: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(408, BitBtn);//
    //
    410: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(410, BitBtn);//
    414: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(414, BitBtn);//
    //
    418: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(418, BitBtn);//
    //
    420: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(420, BitBtn);//
    421: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(421, BitBtn);//
    //
    423: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(423, BitBtn);//
    //
    425: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(425, BitBtn);//
    426: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(426, BitBtn);//
    //
    430: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(430, BitBtn);//
    //
    435:
    begin
      case CO_DMKID_APP of
        2:  //Bluederm
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(421, BitBtn);//
        6:  //Planning
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(435, BitBtn);//
        24: //Bugstrol
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(521, BitBtn);//
        else
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(161, BitBtn);//
      end;
    end;
    //
    438: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(438, BitBtn);//
    //
    441: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(441, BitBtn);//
    //
    447: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(447, BitBtn);//
    //
    450: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(450, BitBtn);//
    //
    455: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(455, BitBtn);//
    456: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(456, BitBtn);//
    //
    471: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(471, BitBtn);//
    472: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(472, BitBtn);//
    473: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(473, BitBtn);//
    474: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(474, BitBtn);//
    //
    490: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(490, BitBtn);//
    //
    492: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(492, BitBtn);//
    493: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(493, BitBtn);//
    494: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(495, BitBtn);//
    495: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(189, BitBtn);//
    496: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(496, BitBtn);//
    497: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(497, BitBtn);//
    498: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(498, BitBtn);//
    499: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(499, BitBtn);//
    //
    502: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(502, BitBtn);//
    //
    503:
    begin
      case CO_DMKID_APP of
        2:  //Bluederm
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(447, BitBtn);//
        6:  //Planning
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(503, BitBtn);//
        24: //Bugstrol
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(522, BitBtn);//
        28: //Toolrent
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(497, BitBtn);//
        else
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(115, BitBtn);//
      end;
    end;
    504: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(504, BitBtn);//
    505: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(505, BitBtn);//
    506: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(506, BitBtn);//
    507: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(507, BitBtn);//
    //
    511: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(511, BitBtn);//
    512: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(512, BitBtn);//
    //
    518: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(518, BitBtn);//
    519: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(519, BitBtn);//
    //
    521: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(521, BitBtn);//
    //
    523: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(523, BitBtn);//
    524: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(524, BitBtn);//
    //
    525:
    begin
      case CO_DMKID_APP of
        2:  //Bluederm
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(447, BitBtn);//
        6:  //Planning
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(437, BitBtn);//
        24: //Bugstrol
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(522, BitBtn);//
        28: //Toolrent
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(497, BitBtn);//
        else
          (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(115, BitBtn);//
      end;
    end;
    //
    526: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(526, BitBtn);//
    //
    528: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(528, BitBtn);//
    //
    530: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(530, BitBtn);//
    531: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(531, BitBtn);//
    532: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(532, BitBtn);//
    //
    533: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(533, BitBtn);//
    //
    539: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(539, BitBtn);//
    540: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(540, BitBtn);//
    541: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(541, BitBtn);//
    542: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(542, BitBtn);//
    543: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(543, BitBtn);//
    //
    546: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(546, BitBtn);//
    547: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(547, BitBtn);//
    548: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(548, BitBtn);//
    //
    550: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(550, BitBtn);//
    551: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(551, BitBtn);//
    552: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(552, BitBtn);//
    //
    553: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(488, BitBtn);//
    //
    555: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(555, BitBtn);//
    556: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(556, BitBtn);//
    //
    559: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(559, BitBtn);//
    560: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(560, BitBtn);//
    //
    561: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(191, BitBtn);//
    562: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(189, BitBtn);//
    //
    568: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(568, BitBtn);//
    569: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(569, BitBtn);//
  	570: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(386, BitBtn);//
    //
  	575: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(575, BitBtn);//
    //
  	578: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(578, BitBtn);//
    //
   	588: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(588, BitBtn);//
   	589: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(589, BitBtn);//
   	590: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(590, BitBtn);//
    //
    600: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(600, BitBtn);//
    601: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(601, BitBtn);//
    //
    605: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(605, BitBtn);//
    //
    611: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(611, BitBtn);//
    612: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(612, BitBtn);//
    613: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(613, BitBtn);//
    614: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(614, BitBtn);//
    //
    615: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(443, BitBtn);//
    616: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(360, BitBtn);//
    617: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(615, BitBtn);//
    618: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(436, BitBtn);//
    619: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(436, BitBtn);//
    620: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(189, BitBtn);//
    //
    629: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(506, BitBtn);//
    630: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(384, BitBtn);//
    //
  10001: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(025, BitBtn);//
  10002: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(001, BitBtn);//
  10003: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(307, BitBtn);//
  10004: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(180, BitBtn);//
  10005: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(308, BitBtn);//
  10006: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(151, BitBtn);//
  10007: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(160, BitBtn);//
  10008: (*BitBtn.Glyph := nil*);//
  10009: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(040, BitBtn);//
  10010: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(067, BitBtn);//
  10011: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(042, BitBtn);//
  10012: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(307, BitBtn);//
  10013: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(108, BitBtn);//
  10014: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(135, BitBtn);//
  10015: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(167, BitBtn);//
  10016: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(302, BitBtn);//
  10017: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(305, BitBtn);//
  10018: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(306, BitBtn);//
  10019: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(312, BitBtn);//
  10020: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(313, BitBtn);//
  10021: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(314, BitBtn);//
  10022: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(234, BitBtn);//
  10023: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(315, BitBtn);//
  10024: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(236, BitBtn);//
  10025: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(141, BitBtn);//
  10026: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(134, BitBtn);//
  10027: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(099, BitBtn);//
  10028: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(088, BitBtn);//
  10029: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(229, BitBtn);//
  10030: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(301, BitBtn);//
  10031: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(262, BitBtn);//
  10032: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(231, BitBtn);//
  10033: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(317, BitBtn);//
  10034: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(184, BitBtn);//
  10035: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(219, BitBtn);//
  10036: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(066, BitBtn);//
  10037: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(043, BitBtn);//
  10038: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(178, BitBtn);//
  10039: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(306, BitBtn);//
  10040: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(318, BitBtn);//
  10041: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(231, BitBtn);//
  10042: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(156, BitBtn);//
  10043: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(159, BitBtn);//
  10044: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(272, BitBtn);//
  10045: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(234, BitBtn);//
  10046: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(236, BitBtn);//
  10047: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(150, BitBtn);//
  10048: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(209, BitBtn);//
  10049: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(393, BitBtn);//
  10050: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(262, BitBtn);//
  10051: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(179, BitBtn);//
  10052: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(078, BitBtn);//
  10053: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(206, BitBtn);//
  10054: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(058, BitBtn);//
  10055: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(228, BitBtn);//
  10056: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(029, BitBtn);//
  10057: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(100, BitBtn);//
  10058: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(073, BitBtn);//
  10059: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(179, BitBtn);//
  10060: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(100, BitBtn);//
  10061: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(158, BitBtn);//
  10062: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(159, BitBtn);//
  10063: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(167, BitBtn);//
  10064: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(073, BitBtn);//
  10065: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(027, BitBtn);//
  10066: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(273, BitBtn);//
  10067: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(068, BitBtn);//
  10068: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(316, BitBtn);//
  10069: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(135, BitBtn);//
  10070: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(341, BitBtn);//
  10071: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(342, BitBtn);//
  10072: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(187, BitBtn);//
  10073: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(198, BitBtn);//
  10074: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(188, BitBtn);//
  10075: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(343, BitBtn);//
  10076: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(228, BitBtn);//
  10077: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(345, BitBtn);//
  10078: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(054, BitBtn);//
  10079: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(326, BitBtn);//
  10080: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(315, BitBtn);//
  10081: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(178, BitBtn);//
  10082: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(231, BitBtn);//
  10083: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(217, BitBtn);//
  10084: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(230, BitBtn);//
  10085: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(114, BitBtn);//
  10086: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(065, BitBtn);//
  10087: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(056, BitBtn);//
  10088: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(409, BitBtn);//
  10089: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(378, BitBtn);//
  10090: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(105, BitBtn);//
  10091: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(214, BitBtn);//
  10092: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(150, BitBtn);//
  10093: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(099, BitBtn);//
  10094: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(252, BitBtn);//
  10095: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(307, BitBtn);//
  10096: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(206, BitBtn);//
  10097: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(273, BitBtn);//
  10098: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(210, BitBtn);//
  10099: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(227, BitBtn);//
  10100: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(274, BitBtn);//
  10101: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(146, BitBtn);//
  10102: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(117, BitBtn);//
  10103: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(114, BitBtn);//
  10104: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(502, BitBtn);//
  10105: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(428, BitBtn);//
  10106: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(041, BitBtn);//
  10107: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(368, BitBtn);//
  10108: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(133, BitBtn);//
  10109: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(179, BitBtn);//
  10110: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(116, BitBtn);//
  10111: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(334, BitBtn);//
  10112: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(146, BitBtn);//
  10113: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(359, BitBtn);//
  10114: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(153, BitBtn);//
  10115: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(224, BitBtn);//
  10116: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(223, BitBtn);//
  10117: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(134, BitBtn);//
  10118: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(067, BitBtn);//
  10119: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(141, BitBtn);//
  10120: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(119, BitBtn);//
  10121: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(184, BitBtn);//
  10122: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(075, BitBtn);//
  10123: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(393, BitBtn);//
  10124: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(266, BitBtn);//
  10125: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(376, BitBtn);//
  10126: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(076, BitBtn);//
  10127: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(315, BitBtn);//
  10128: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(104, BitBtn);//
  10129: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(232, BitBtn);//
  10130: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(338, BitBtn);//
  10131: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(052, BitBtn);//
  10132: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(178, BitBtn);//
  10133: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(319, BitBtn);//
  10134: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(114, BitBtn);//
  10135: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(105, BitBtn);//
  10136: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(155, BitBtn);//
  10200: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(409, BitBtn);//
  10201: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(334, BitBtn);//
  10202: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(332, BitBtn);//
  10203: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(017, BitBtn);//
  10204: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(088, BitBtn);//
  10205: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(029, BitBtn);//
  10206: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(009, BitBtn);//
  10207: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(140, BitBtn);//
  10208: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(189, BitBtn);//
  10209: (*BitBtn.Glyph :=*) ObtemBitmapDeGlyph(191, BitBtn);//
  else
  begin         //  1.000.000
    if BitBtn.Tag > 1000000 then
      ObtemBitmapDeGlyph(BitBtn.Tag - 1000000, BitBtn);
  end;
  end;
end;

function TFmMyGlyfs.DefineIdTxtBmp(): Integer;
var
  Arq: String;
begin
  Arq := Uppercase(ExtractFileName(VAR_CAMINHOTXTBMP));
  if Arq = 'ITUNES.SKN'         then Result := 01
  else if Arq = 'MACOS.SKN'     then Result := 01
  else if Arq = 'MXP1.SKN'      then Result := 02
  else if Arq = 'MXP2.SKN'      then Result := 03
  else if Arq = 'MXP3.SKN'      then Result := 04
  else if Arq = 'MXP05.SKN'     then Result := 05
  else if Arq = 'MXSKIN2.SKN'   then Result := 06
  else if Arq = 'MXSKIN03.SKN'  then Result := 07
  else if Arq = 'MXSKIN8.SKN'   then Result := 08
  else if Arq = 'MXSKIN9.SKN'   then Result := 09
  else if Arq = 'MXSKIN10.SKN'  then Result := 10
  else if Arq = 'MXSKIN11.SKN'  then Result := 11
  else if Arq = 'MXSKIN12.SKN'  then Result := 12
  else if Arq = 'MXSKIN13.SKN'  then Result := 13
  else if Arq = 'MXSKIN14.SKN'  then Result := 14
  else if Arq = 'MXSKIN15.SKN'  then Result := 15
  else if Arq = 'MXSKIN17.SKN'  then Result := 16
  else if Arq = 'MXSKIN18.SKN'  then Result := 17
  else if Arq = 'MXSKIN19.SKN'  then Result := 18
  else if Arq = 'MXSKIN20.SKN'  then Result := 19
  else if Arq = 'MXSKIN21.SKN'  then Result := 20
  else if Arq = 'MXSKIN22.SKN'  then Result := 21
  else if Arq = 'MXSKIN23.SKN'  then Result := 22
  else if Arq = 'MXSKIN24.SKN'  then Result := 23
  else if Arq = 'MXSKIN25.SKN'  then Result := 24
  else if Arq = 'MXSKIN26.SKN'  then Result := 25
  else if Arq = 'MXSKIN27.SKN'  then Result := 26
  else if Arq = 'MXSKIN28.SKN'  then Result := 27
  else if Arq = 'MXSKIN29.SKN'  then Result := 28
  else if Arq = 'MXSKIN30.SKN'  then Result := 29
  else if Arq = 'MXSKIN31.SKN'  then Result := 30
  else if Arq = 'MXSKIN32.SKN'  then Result := 31
  else if Arq = 'MXSKIN33.SKN'  then Result := 32
  else if Arq = 'MXSKIN34.SKN'  then Result := 33
  else if Arq = 'MXSKIN35.SKN'  then Result := 34
  else if Arq = 'MXSKIN36.SKN'  then Result := 35
  else if Arq = 'MXSKIN37.SKN'  then Result := 36
  else if Arq = 'MXSKIN38.SKN'  then Result := 37
  else if Arq = 'MXSKIN39.SKN'  then Result := 38
  else if Arq = 'MXSKIN40.SKN'  then Result := 39
  else if Arq = 'MXSKIN41.SKN'  then Result := 40
  else if Arq = 'MXSKIN42.SKN'  then Result := 41
  else if Arq = 'MXSKIN43.SKN'  then Result := 42
  else if Arq = 'MXSKIN44.SKN'  then Result := 43
  else if Arq = 'MXSKIN45.SKN'  then Result := 44
  else if Arq = 'MXSKIN46.SKN'  then Result := 45
  else if Arq = 'MXSKIN47.SKN'  then Result := 46
  else if Arq = 'MXSKIN48.SKN'  then Result := 47
  else if Arq = 'MXSKIN49.SKN'  then Result := 48
  else if Arq = 'MXSKIN50.SKN'  then Result := 49
  else if Arq = 'MXSKIN51.SKN'  then Result := 50
  else if Arq = 'MXSKIN52.SKN'  then Result := 51
  else if Arq = 'MXSKIN53.SKN'  then Result := 52
  else Result := 0;
  if (ListaChecks.Count < (Result*2)+1) then Result := 0; //49
end;

procedure TFmMyGlyfs.HintDeBitBtn(BitBtn: TBitBtn);
begin
  case BitBtn.Tag of
    001: BitBtn.Hint := 'Primeiro|Localiza o primeiro registro da tabela';//SpeedButton1
    002: BitBtn.Hint := 'Anterior|Localiza o registro anterior ao atual';//SpeedButton2
    003: BitBtn.Hint := 'Pr�ximo|Localiza o pr�ximo registro da tabela'; //SpeedButton3
    004: BitBtn.Hint := '�ltimo|Localiza o �ltimo registro da tabela'; //SpeedButton4
    005: BitBtn.Hint := 'Imprimir|Gera relat�rio(s) para vizualiza��o/impress�o'; //SbImprime
    006: BitBtn.Hint := 'Em branco|Vizualiza/Imprime formul�rio em branco'; //SbNovo
    007: BitBtn.Hint := 'Localiza pelo c�digo / n�mero|Localiza registro pelo c�digo / N�mero'; //SbNumero
    008: BitBtn.Hint := 'Localiza pelo nome|Localiza registro pelo nome / descri��o'; //SbNome
    009: BitBtn.Hint := 'Localiza avan�ada pelo nome|Localiza registro pelo nome / descri��o com mais op��es'; //SbQuery
    010: BitBtn.Hint := 'Novo|Inclui novo registro'; //BtInclui
    011: BitBtn.Hint := 'Altera|Altera dados do registro atual'; //BtAltera
    012: BitBtn.Hint := 'Exclui|Exclui o registro atual'; //BtExclui
    013: BitBtn.Hint := 'Fecha a janela|Fecha a janela atual.';//BtSair
    014: BitBtn.Hint := 'Confirma a a��o|Confirma a a��o selecionada.';//BtConfirma
    015: BitBtn.Hint := 'Desiste|Desiste da a��o em curso'; //BtDesiste
    018: BitBtn.Hint := 'Refresh|Recalcula dados / reabre tabela'; //BtRefresh
    019: BitBtn.Hint := 'Importa|Importa dados externos'; //BtImportar
    020: BitBtn.Hint := 'Pagamento|Pagamento'; //BtPagamento
    021: BitBtn.Hint := 'Cheque|Emite cheque'; //BtCheque
    022: BitBtn.Hint := 'Pesquisa|Pesquisa dados'; //BtPesquisa
    023: BitBtn.Hint := 'N�vel acima|Cadastro de agrupador deste cadastro'; //BtNivelAcima
    024: BitBtn.Hint := 'Grava dados|Salva as altera��es dos dados'; //BtSalvar
    025: BitBtn.Hint := 'Abrir|Abre arquivo de dados para importacao'; //BtAbrir
    026: BitBtn.Hint := 'Abrir|Abre arquivo para selecionar caminho';  //BtAbrir
    027: BitBtn.Hint := 'Preview de impress�o|Mostra como ficar� a impress�o';  //BtPreview
    028: BitBtn.Hint := 'Abrir|Abre arquivo';  //BtAbrir
    029: BitBtn.Hint := 'Cores|Cores';
    030: BitBtn.Hint := 'Mercadoria|Mercadoria';
    031: BitBtn.Hint := 'Abaixo|Abaixo';
    032: BitBtn.Hint := 'Acima|Acima';
    //
    034: BitBtn.Hint := 'Salva dados|Slava dados (grava)';
    035: BitBtn.Hint := 'Estrutura|Estrutura do banco de dados';
    //
    038: BitBtn.Hint := 'Abrir|Abrir arquivo';
    039: BitBtn.Hint := 'Importa|Importa';
    040: BitBtn.Hint := 'Pesquisa|Pesquisa';
    041: BitBtn.Hint := 'Gera|Gera texto/c�digo';
    //
    047: BitBtn.Hint := 'Empresta|Empresta';
    048: BitBtn.Hint := 'Cancela|Cancela documento';
    //
    056: BitBtn.Hint := 'Duplica|Duplica';
    //
    058: BitBtn.Hint := 'Grupo (Grade)|Grupo (Grade)';
    //
    059: BitBtn.Hint := 'Backup|Gera Backup';
    //
    066: BitBtn.Hint := 'E-mails|Cadastro de e-mails';
    067: BitBtn.Hint := 'Encerramento de pedido|Encerramento de pedido';
    068: BitBtn.Hint := 'Limpeza|Limpeza';
    //
    073: BitBtn.Hint := 'Reordena|Reordena';
    075: BitBtn.Hint := 'Op��es aplicativo|Op��es aplicativo';
    //
    079: BitBtn.Hint := 'Compras|Compras';
    //
    086: BitBtn.Hint := 'Conversa|Conversa';
    //
    089: BitBtn.Hint := 'Ratifica|Ratifica';
    //
    092: BitBtn.Hint := 'Telefones|Cadastro de telefones';
    //
    094: BitBtn.Hint := 'Localiza item|Localiza item';
    095: BitBtn.Hint := 'Revisa/corrige|Revisa/corrige';
    //
    //0: BitBtn.Hint := '|'; //
    100: BitBtn.Hint := 'Grupo de pessoas';
    101: BitBtn.Hint := 'Transfer�ncia|Transfere movimentos da entidade atual para outra'; //BtEntidade2
    102: BitBtn.Hint := 'Logo|Escolha de Logo'; //BtImagem
    103: BitBtn.Hint := 'CEP|Escolha ou confer�ncia de C�digo de endere�amento Postal (CEP)'; //BtCEP
    104: BitBtn.Hint := 'Copiar de entidade|Copias dados de outra entidade'; //
    105: BitBtn.Hint := 'Inclui transportadora|Adiciona transportadora � lista'; //
    106: BitBtn.Hint := 'Exclui transportadora|Retira transportadora da lista'; //
    107: BitBtn.Hint := 'Calcula dias|Calcula n�mero de dias entre duas datas'; //
    108: BitBtn.Hint := 'M�ltiplas impress�es|Seleciona v�rios relat�rios e os imprime'; //
    //109: BitBtn.Hint := 'Risco do cliente|Mostra risco do cliente selecionado'; //
    110: BitBtn.Hint := 'Cadastra Emitente(s)/Sacado(s)|Cadastra Emitente(s) Sacado(s) selecionado(s)'; //
    111: BitBtn.Hint := 'Igonora diferen�as de cadastro|Ignora as diferen�as entre o cadastro e os dados do emitente/sacado'; //
    112: BitBtn.Hint := 'Altera cadastro do emitente/sacado|Altera os dados do cadastro do emitente/sacado gravando os dados enviados pelo cliente'; //
    113: BitBtn.Hint := 'Remove duplicata|Cancela importa��o da duplicata selecionada'; //
    114: BitBtn.Hint := 'Remove cheque|Cancela importa��o do cheque selecionado'; //
    115: BitBtn.Hint := 'Importa do SuitCash|Importa cheques / duplicatas de um arquivo gerado pelo aplicativo Suit Cash'; //
    116: BitBtn.Hint := 'Gerencia Border�s|Cria gerencia e imprime border�s'; //
    117: BitBtn.Hint := 'Dep�sito de cheques|Relat�rio configur�vel de dep�sitos de cheques'; //
    118: BitBtn.Hint := 'L� o cheque|L� e retorna dados e CMC7 do cheque'; //
    119: BitBtn.Hint := 'Navega no site|Navega no site selecionado'; //
    120: BitBtn.Hint := 'Adiciona border�|Adiciona border� a esta NF'; //
    121: BitBtn.Hint := 'Edita texto|Edita texto do registro selecionado'; //
    122: BitBtn.Hint := 'Cadastra/edita cliente|Cadastra e/ou edita cliente'; //
    123: BitBtn.Hint := 'Gerencia s�cios|Cadastra/edita/adiciona/retira s�cio deste cliente'; //
    124: BitBtn.Hint := 'Adiciona/retira taxas|Adiciona/retira taxas deste cliente'; //
    125: BitBtn.Hint := 'Atualiza BD|Verifica e atualiza o banco de dados pela vers�o do aplicativo'; //
    126: BitBtn.Hint := 'P�ra a��o|P�ra a realiza��o da a��o em andamento'; //
    127: BitBtn.Hint := 'Seleciona todos|Seleciona todos itens'; //
    128: BitBtn.Hint := 'Desmarca todos|Desmarca todos itens'; //
    129: BitBtn.Hint := 'Ativa o servi�o indicado|Ativa o servi�o indicado'; //
    130: BitBtn.Hint := 'Destiva o servi�o indicado|Destiva o servi�o indicado'; //
    131: BitBtn.Hint := 'Status do servi�o indicado|Mostra o status do servi�o indicado'; //
    132: BitBtn.Hint := 'Gerencia clientes|Cadastra clientes seus s�cios e suas taxas'; //
    133: BitBtn.Hint := 'Gerenciamento do risco de clientes|Gerenciamento do risco de clientes'; //
    134: BitBtn.Hint := 'Gerenciamento do risco de sacados e emitentes|Gerenciamento do risco de sacados e emitentes'; //
    135: BitBtn.Hint := 'Gerenciamento de cheques devolvidos|Devolu��o e pagamento de cheques'; //
    136: BitBtn.Hint := 'Pesquisa cheque a devolver|Pesquisa e/ou adiciona cheque a lista de cheques devolvidos'; //
    137: BitBtn.Hint := 'Devolve cheque|Adiciona cheque ao cadastro de cheques devolvidos'; //
    138: BitBtn.Hint := 'Ocorr�ncias em cheques|Inclui/altera/exclui ocorr�ncias em cheques'; //
    139: BitBtn.Hint := 'Prorroga��es de cheques|Inclui/exclui prorroga��es de cheques'; //
    140: BitBtn.Hint := 'Gerenciamento de cheques|Prorroga��es, ocorr�ncias de devolu��es de cheques'; //
    141: BitBtn.Hint := 'Backup|Backup das tabelas do banco de dados'; //
    142: BitBtn.Hint := 'Repasse de cheques|Repasse de cheques a Coligadas'; //
    143: BitBtn.Hint := 'Cria novo repasse|Cria novo lote de repasse de cheques'; //
    144: BitBtn.Hint := 'Imprime lote de repasse|Imprime lote de repasse'; //
    145: BitBtn.Hint := 'Imprime outros|Imprime outros relat�rios de pesquisa'; //
    146: BitBtn.Hint := 'Seleciona cheque|Pesquisa cheque avulso e inclui ao lote'; //
    147: BitBtn.Hint := 'Importa border�|Importa itens de border� para dentro do lote selecionado'; //
    148: BitBtn.Hint := 'Ocorr�ncias em duplicatas|Inclui/altera/exclui ocorr�ncias em duplicatas'; //
    149: BitBtn.Hint := 'Ocorr�ncias em clientes|Inclui/altera/exclui ocorr�ncias em clientes'; //
    150: BitBtn.Hint := 'Gerenciamento de duplicatas|Prorroga��es, ocorr�ncias de devolu��es de duplicatas'; //
    151: BitBtn.Hint := 'Status da duplicata|Novo stutus da duplicata'; //
    152: BitBtn.Hint := 'Pagamento da duplicata|Novo pagamento da duplicata'; //
    153: BitBtn.Hint := 'Novo status + prorroga��o para a duplicata|Novo status + prorroga��o para a duplicata na mesma a��o'; //
    154: BitBtn.Hint := 'Prorroga��es da duplicata|Inclui/exclui prorroga��es da duplicata selecionada'; //
    155: BitBtn.Hint := 'L� diret�rio/Arquivo|L� diret�rio/Arquivo'; //
    156: BitBtn.Hint := 'Carta|Imprime carta por emiss�o manual'; //
    157: BitBtn.Hint := 'Novo arquivo|Cria novo arquivo'; //
    158: BitBtn.Hint := 'Skin|Escolha de Skin'; //BtSkin
    159: BitBtn.Hint := 'Imprime|Imprime'; //BtImpNovo
    160: BitBtn.Hint := 'Dados de dentifica��o|Dados de identifica��o cadastral'; //BtImpNovo
    161: BitBtn.Hint := 'Salva arquivo|Salva modifica��es do arquivo atual'; //BtSalva
    162: BitBtn.Hint := 'Salvar como ...|Salva dados atuais em arquivo novo'; //BtSalvaComo
    163: BitBtn.Hint := 'Gera relat�rio|Gera relat�rio criando tabela tempor�ria'; //BtGera
    164: BitBtn.Hint := 'Risco do cliente|Mostra risco do cliente selecionado'; //
    165: BitBtn.Hint := 'Risco do sacado/emitente|Mostra risco do sacado/emitente selecionado'; //
    166: BitBtn.Hint := 'Ratifica chegada de malote|Ratifica chegada de malote'; //
    167: BitBtn.Hint := 'Desfaz ratifica��o|Desfaz ratifica��o de chegada de malote'; //
    168: BitBtn.Hint := 'Confere chegada de cheques|Confer�ncia de malote de cheques'; //
    169: BitBtn.Hint := 'Limpa tabelas tempor�rias|Esvazia dados da tabela que verifica chegada de cheques'; //
    170: BitBtn.Hint := 'Limpa confer�ncia|Define cheques pesquisados como n�o conferido'; //
    171: BitBtn.Hint := 'Quita��o|Quita��o total do cheque'; //
    172: BitBtn.Hint := 'Quita��o|Quita��o total da duplicata'; //
    173: BitBtn.Hint := 'Ocorr�ncia|Cadastro de ocorr�ncia'; //
    174: BitBtn.Hint := 'Pagamento r�pido|Pagamento r�pido do saldo'; //
    175: BitBtn.Hint := 'Produtos Qu�micos|Gerenciamento de produtos qu�micos'; //
    176: BitBtn.Hint := 'Pre�o padr�o|Define este pre�o como padr�o do cliente interno'; //
    177: BitBtn.Hint := 'Duplica dados|Duplica dados atuais'; //
    178: BitBtn.Hint := 'Cadastro de Receitas|Gerencia receitas qu�micas'; //
    179: BitBtn.Hint := 'Ordem de Servi�o|Ordem de Servi�o'; //
    180: BitBtn.Hint := 'Calculadora|Calculadora'; //
    181: BitBtn.Hint := 'Cadastro|Cadastro'; //
    182: BitBtn.Hint := 'Zera balan�o|Zera balan�o'; //
    183: BitBtn.Hint := 'Contratos|Gerencia contratos'; //
    184: BitBtn.Hint := '?|Indefinido'; //
    185: BitBtn.Hint := 'Contas Mensais|Contas Mensais'; //
    186: BitBtn.Hint := 'Resultados di�rios|Resultados di�rios'; //
    187: BitBtn.Hint := 'Pagamentos|Inclus�o, (altera��o), exclus�o de pagamentos'; //
    188: BitBtn.Hint := 'Adiamento|Adiamento de pagamento'; //
    189: BitBtn.Hint := 'Resultados|Resultado Financeiro'; //
    190: BitBtn.Hint := 'Inadimpl�ncias|Inadimpl�ncias'; //
    191: BitBtn.Hint := 'Per�odo|Escolha de per�odo'; //
    192: BitBtn.Hint := 'Download|Atualiza��o do aplicativo'; //
    193: BitBtn.Hint := 'Cota��o|Cota��o de moedas'; //
    194: BitBtn.Hint := 'Emite pesagem|Emite e imprime receita com pesagem'; //
    195: BitBtn.Hint := 'Relat�rios PQ|Relat�rios de insumos qu�micos'; //
    197: BitBtn.Hint := 'ID|ID';
    199: BitBtn.Hint := 'Recibo|Recibo';
    200: BitBtn.Hint := 'Tecla 0|Tecla 0'; //
    201: BitBtn.Hint := 'Tecla 1|Tecla 1'; //
    202: BitBtn.Hint := 'Tecla 2|Tecla 2'; //
    203: BitBtn.Hint := 'Tecla 3|Tecla 3'; //
    204: BitBtn.Hint := 'Tecla 4|Tecla 4'; //
    205: BitBtn.Hint := 'Tecla 5|Tecla 5'; //
    206: BitBtn.Hint := 'Tecla 6|Tecla 6'; //
    207: BitBtn.Hint := 'Tecla 7|Tecla 7'; //
    208: BitBtn.Hint := 'Tecla 8|Tecla 8'; //
    209: BitBtn.Hint := 'Tecla 9|Tecla 9'; //
    210: BitBtn.Hint := 'Dividir|Sinal de dividir'; //
    211: BitBtn.Hint := 'Subtrair|Sinal de subtra��o'; //
    212: BitBtn.Hint := 'Somar|Sinal de soma'; //
    213: BitBtn.Hint := 'Resultado|Sinal de resultado do c�lculo'; //
    214: BitBtn.Hint := 'V�rgula|V�rgula'; //
    215: BitBtn.Hint := 'Multiplicxa��o|Sinal de multiplica��o'; //
    216: BitBtn.Hint := 'Mudan�a de sinal|Mudar o sinal'; //
    217: BitBtn.Hint := 'Zeramento|Zera o valor em cache'; //
    218: BitBtn.Hint := 'Zeramento da mem�ria|Zera o valor na mem�ria'; //
    219: BitBtn.Hint := 'Valor em mem�ria|Mostra valor em mem�ria'; //
    220: BitBtn.Hint := 'Substitui mem�ria|Substitui valor da mem�ria'; //
    221: BitBtn.Hint := 'Adiciona valor � mem�ria|Adiciona valor � mem�ria'; //
    222: BitBtn.Hint := 'Porcentagem|Calcula porcentagem'; //
    223: BitBtn.Hint := 'Pallets|Pallets de couros'; //
    224: BitBtn.Hint := 'Couros|Couros'; //
    225: BitBtn.Hint := 'Selecionar passagem|Selecionar passagem b�blica'; //
    226: BitBtn.Hint := 'Gerar p�ginas|Gerar p�ginas para leiura b�blica'; //
    227: BitBtn.Hint := 'Imagem de fundo|Escolha de imagem de fundo'; //
    228: BitBtn.Hint := 'M�sica (T�tulo)|M�sica (T�tulo)'; //
    229: BitBtn.Hint := 'Letra da m�sica|Letra da M�sica'; //
    230: BitBtn.Hint := 'Preview tela cheia|Preview tela cheia'; //
    231: BitBtn.Hint := 'Liturgias|Liturgias'; //
    232: BitBtn.Hint := 'Hin�rios|Hin�rios'; //
    233: BitBtn.Hint := 'Itens de liturgias|Cadastro de itens de liturgia'; //
    234: BitBtn.Hint := 'Palestra|Palestra'; //
    235: BitBtn.Hint := 'Slides|Slides'; //
    236: BitBtn.Hint := 'Reordena|Reordena'; //
    237: BitBtn.Hint := 'Menu de op��es|Menu de op��es'; //
    238: BitBtn.Hint := 'Saldo aqui|Verifica saldo at� o lan�amento atual'; //
    239: BitBtn.Hint := 'Dinheiro em caixa|Informe de dinheiro em caixa'; //
    240: BitBtn.Hint := 'Desconto|Desconto'; //
    241: BitBtn.Hint := 'Sele��o de texto|Sele��o de texto'; //
    242: BitBtn.Hint := 'Chefe de fam�lia|Chefe de fam�lia'; //
    243: BitBtn.Hint := 'Membros da fam�lia|Membros de fam�lia'; //
    244: BitBtn.Hint := 'Envia por e-mail|Envia por e-mail'; //
    245: BitBtn.Hint := 'Salva configura��o atual|Salva configura��o atual'; //
    246: BitBtn.Hint := 'Limpa configura��es|Limpa configura��es'; //
    247: BitBtn.Hint := 'Tamanho de fonte = 6|Define tamanho da fonte = 6'; //
    248: BitBtn.Hint := 'Tamanho de fonte = 8|Define tamanho da fonte = 8'; //
    249: BitBtn.Hint := 'Tamanho de fonte = 10|Define tamanho da fonte = 10'; //
    250: BitBtn.Hint := 'Tamanho de fonte = 12|Define tamanho da fonte = 12'; //
    251: BitBtn.Hint := 'Tamanho de fonte = 14|Define tamanho da fonte = 14'; //
    252: BitBtn.Hint := 'Libera para edi��o|Libera para edi��o do texto'; //
    253: BitBtn.Hint := 'Mostra c�digo|Mostra c�digo'; //
    254: BitBtn.Hint := 'Soma|Soma'; //
    255: BitBtn.Hint := 'Visualiza grade de Resultados|Visualiza grade de Resultados'; //
    256: BitBtn.Hint := 'Configura impress�o|Configura impress�o'; //
    257: BitBtn.Hint := 'Grupos|Grupos'; //
    258: BitBtn.Hint := 'Cultos|Cultos'; //
    259: BitBtn.Hint := 'Hist�rico de entidades|Hist�rico de entidades'; //
    260: BitBtn.Hint := 'Custos b�sicos|Planilha de custos b�sicos'; //
    261: BitBtn.Hint := 'Or�amentos|Or�amentos'; //
    262: BitBtn.Hint := 'Gr�fico|Atualiza gr�fico'; //
    263: BitBtn.Hint := 'Configura��o|Configura��o'; //
    264: BitBtn.Hint := 'Carteira|Carteira'; //
    265: BitBtn.Hint := 'Lote|Lote'; //
    266: BitBtn.Hint := 'Cobran�a Banc�ria|Cobran�a Banc�ria de t�tulos'; //
    267: BitBtn.Hint := 'Envia / recebe arquivos|Envia recebe arquivos por FTP'; //
    268: BitBtn.Hint := 'MSN Messenger|MSN Messenger'; //
    269: BitBtn.Hint := 'Exporta arquivos|Exporta arquivos'; //
    270: BitBtn.Hint := 'Importa arquivos|Importa arquivos'; //
    271: BitBtn.Hint := 'Menu de op��es|Baixa menu de op��es'; //
    272: BitBtn.Hint := 'Mais zoom|Aproxima a vis�o do documento'; //
    273: BitBtn.Hint := 'Menos zoom|Afasta a vis�o do documento'; //
    274: BitBtn.Hint := 'Abrir|Abrir documento'; //
    275: BitBtn.Hint := 'Tela cheia|Tela cheia'; //
    276: BitBtn.Hint := 'Configurar|Configurar impress�o'; //
    277: BitBtn.Hint := 'Localizar registro|Localizar registro'; //
    278: BitBtn.Hint := 'Miniaturas|Miniaturas das p�ginas'; //
    279: BitBtn.Hint := 'Im�vel|Im�vel'; //
    280: BitBtn.Hint := 'Im�veis|Im�veis'; //
    281: BitBtn.Hint := 'Renova contrato|Renova contrato'; //
    282: BitBtn.Hint := 'C�modos|C�modos'; //
    283: BitBtn.Hint := 'Gerencia alugu�is|Gerenciamento de alugu�is'; //
    284: BitBtn.Hint := 'Juros e/ou multas|Juros e/ou multas'; //
    285: BitBtn.Hint := 'Pedidos|Pedidos de couros'; //
    286: BitBtn.Hint := 'Cart�o ponto|Cart�o ponto'; //
    287: BitBtn.Hint := 'Status pedidos|Status pedidos de couros'; //
    288: BitBtn.Hint := 'Relat�rios pedidos|Relat�rios pedidos de couros'; //
    289: BitBtn.Hint := 'Emite duplicata|Emite duplicata'; //
    290: BitBtn.Hint := 'Itens de mercadoria|Itens de mercadoria'; //
    291: BitBtn.Hint := 'Compras|Entrada por compras'; //
    292: BitBtn.Hint := 'Aceita pre�o antigo|Aceita pre�o antigo (enviado pelo cliente)'; //
    293: BitBtn.Hint := 'Sobras|Sobras de valores'; //
    294: BitBtn.Hint := 'Executa|Executa a��o'; //
    //
    295: BitBtn.Hint := 'Balan�o|Balan�o'; //
    296: BitBtn.Hint := 'Grupo de artigos|Grupo de artigos'; //
    //
    300: BitBtn.Hint := 'Tamanho|Tamanho'; //
    301: BitBtn.Hint := 'Dep�sito|Dep�sito banc�rio'; //
    302: BitBtn.Hint := 'Compras|Compras'; //
    303: BitBtn.Hint := 'Vendas|Vendas'; //

    310: BitBtn.Hint := 'Per�odo atual|Per�odo atual'; //
    311: BitBtn.Hint := 'Linkar|Linkar extrato com lan�amentos'; //
    316: BitBtn.Hint := 'Lista de confer�ncia de pagamentos|Lista de confer�ncia de pagamentos'; //
    318: BitBtn.Hint := 'Confer�ncia de pagamentos|Confer�ncia de pagamentos'; //
    319: BitBtn.Hint := 'Pesquisa de pagamentos|Pesquisa de pagamentos'; //
    320: BitBtn.Hint := 'Pesquisa UHs / Cond�minos|Pesquisa UHs / Cond�minos'; //
    321: BitBtn.Hint := 'Pagamentos m�ltiplos|Pagamentos m�ltiplos'; //
    //
    326: BitBtn.Hint := 'Saldos|Consulta e impres�o se saldos'; //
    //
    328: BitBtn.Hint := 'Consulta ao SPC/SERASA|Consulta ao SPC - servi�o de prote��o ao cr�dito e/ou ao SERASA'; //
    329: BitBtn.Hint := 'Cancela ordena��o|Cancela ordena��o'; //
    330: BitBtn.Hint := 'Transfer�ncia entre contas|Transfer�ncia entre contas (Plano de contas)'; //
    331: BitBtn.Hint := 'Receitas de ribeira|Receitas de ribeira'; //
    332: BitBtn.Hint := 'Pesagem de ribeira|Pesagem de ribeira'; //
    333: BitBtn.Hint := 'Receitas de acabamento|Receitas de acabamento'; //
    334: BitBtn.Hint := 'Pesagem de acabamento|Pesagem de acabamento'; //
    335: BitBtn.Hint := 'Arrecada��o base|Arrecada��o base'; //
    336: BitBtn.Hint := 'Agendamento de provis�o|Agendamento de provis�o'; //
    337: BitBtn.Hint := 'Pesquisa agendamento de provis�o efetivado|Pesquisa agendamento de provis�o efetivado'; //
    338: BitBtn.Hint := 'Inverte posi��o de layout|Inverte posi��o de layout de visualiza��o'; //
    339: BitBtn.Hint := 'Importa��o CNAB|Importa��o CNAB'; //
    340: BitBtn.Hint := 'Exporta��o CNAB|Exporta��o CNAB'; //
    //
    348: BitBtn.Hint := 'Op��es|Op��es'; //
    //
    350: BitBtn.Hint := 'N�vel 1|Planos'; //
    351: BitBtn.Hint := 'N�vel 2|Conjuntos'; //
    352: BitBtn.Hint := 'N�vel 3|Grupos'; //
    353: BitBtn.Hint := 'N�vel 4|Sub-grupos'; //
    354: BitBtn.Hint := 'N�vel 5|Contas'; //
    //
    359: BitBtn.Hint := 'Contrato|Contrato'; //
    360: BitBtn.Hint := 'Balancete|Balancete'; //
    //
    363: BitBtn.Hint := 'Lista|Lista'; //
    364: BitBtn.Hint := 'N�veis|N�veis'; //
    //
    366: BitBtn.Hint := 'Extrato|Extrato'; //
    367: BitBtn.Hint := 'Item de configura��o|Item de configura��o'; //
    //
    378: BitBtn.Hint := 'Configura��o|Configura��o'; //
    //
    387: BitBtn.Hint := 'Gerencia NF-e(s)|Gerencia NF-e(s)'; //
    //
    391: BitBtn.Hint := 'CFOP|CFOP'; //
    //
    393: BitBtn.Hint := 'Filial|Filial'; //
    394: BitBtn.Hint := 'NFe|NFe'; //
    //
    396: BitBtn.Hint := 'Lote de couro|Lote de couro'; //
    //
    398: BitBtn.Hint := 'Gerencia o di�rio|Gerencia o di�rio'; //
    399: BitBtn.Hint := 'Escreve no di�rio|Escreve no di�rio'; //
    //
    406: BitBtn.Hint := 'Texto email|Texto email (pr�-email)'; //
    407: BitBtn.Hint := 'Condi��o de pagamento|Condi��o de pagamento'; //
    408: BitBtn.Hint := 'Configura��o de filial|Configura��o de filial'; //
    //
    410: BitBtn.Hint := 'Exporta��o TED Cheque|Exporta��o TED Cheque'; //
    //
    414: BitBtn.Hint := 'Faturamenro|Faturamento'; //
    //
    418: BitBtn.Hint := 'Configura��o de faturamenro|Configura��o de faturamento'; //
    //
    420: BitBtn.Hint := 'Pele In Natura|Pele In Natura';
    421: BitBtn.Hint := 'Couro curtido|Couro curtido';
    //
    423: BitBtn.Hint := 'Insumo|Insumo';
    //
    425: BitBtn.Hint := 'Couro|Couro'; // V�rios usos!?
    426: BitBtn.Hint := 'Banco|Dados banc�rios'; //
    //
    430: BitBtn.Hint := 'Provid�ncias|Pedido de Provid�ncias'; //
    //
    435: BitBtn.Hint := 'Tipos de produtos|Tipos de produtos'; //
    //
    438: BitBtn.Hint := 'Centros de estoque|Centro de estoque'; //
    //
    441: BitBtn.Hint := 'NF-e|NF-e'; //
    //
    447: BitBtn.Hint := 'Novo artigo de couro|Novo artigo de couro'; //
    //
    450: BitBtn.Hint := 'Insumos qu�micos|Insumos qu�micos'; //
    //
    455: BitBtn.Hint := 'Cancela NF-e|Cancela NF-e'; //
    456: BitBtn.Hint := 'Informa��es NF-e|Informa��es NF-e'; //
    //
    471: BitBtn.Hint := 'Mapa|Google Maps'; //
    472: BitBtn.Hint := 'Imp.Custom.Salva|Salva Impress�o Customizada'; //
    473: BitBtn.Hint := 'Imp.Custom.Impr.|Imprime Impress�o Customizada'; //
    474: BitBtn.Hint := 'Imp.Custom.Conf.|Desenha Impress�o Customizada'; //
    //
    490: BitBtn.Hint := 'Reparcelar Bloquetos|Reparcelamento de Bloquetos (Local)'; //
    //
    492: BitBtn.Hint := 'Bloquetos|Configura bloquetos'; //
    493: BitBtn.Hint := 'Tags|Tags de mala direta'; //
    494: BitBtn.Hint := 'IGPM|Fator / �ndice IGPM'; //
    495: BitBtn.Hint := 'Desconectar|Desconectar base de dados WEB'; //
    496: BitBtn.Hint := 'Minimizar|Minimizar aplicativo'; //
    497: BitBtn.Hint := 'Patrim�nio|Patrim�nio'; //
    498: BitBtn.Hint := 'Acess�rios|Acess�rios'; //
    499: BitBtn.Hint := 'Loca��o|Loca��o'; //

    502: BitBtn.Hint := 'Fiscal|Fiscal'; //
    503: BitBtn.Hint := 'Produtos|Produtos em grade'; //
    504: BitBtn.Hint := 'Dados de produto|Dados de produto'; //
    505: BitBtn.Hint := 'Marca|Marca'; //
    506: BitBtn.Hint := 'Encerra|Encerra'; //
    507: BitBtn.Hint := 'CNAE|CNAE'; //
    //
    511: BitBtn.Hint := 'Armadilhas / Armadilhas'; //
    512: BitBtn.Hint := 'XML / BD|XML / BD'; //
    //
    518: BitBtn.Hint := 'SMS|SMS'; //
    519: BitBtn.Hint := 'Pragas|Pragas'; //
    //
    521: BitBtn.Hint := 'Outros movimentos|Outros movimentos'; //
    //
    523: BitBtn.Hint := 'Item de produto|Item de produto'; //
    524: BitBtn.Hint := 'Produtos aplica��o|Produtos aplica��o'; //
    525: BitBtn.Hint := 'Produtos|Produtos Grade'; //
    526: BitBtn.Hint := 'Evento NF-e|Evento NF-e'; //
    //
    528: BitBtn.Hint := 'Consulta NF-e|Consulta NF-e'; //
    //
    530: BitBtn.Hint := 'Servi�os|Servi�os LC 116/03'; //
    531: BitBtn.Hint := 'Erros e alertas NFS-e|Erros e Alertas NFS-e'; //
    532: BitBtn.Hint := 'Cidades e Paises|Cidades e Paises'; //
    //
    533: BitBtn.Hint := 'Gerencia NFS-e|Gerencia NFS-e'; //
    //
    539: BitBtn.Hint := 'Ordem de servi�o|Ordem de servi�o'; //
    540: BitBtn.Hint := 'Grupos de pragas|Grupos de pragas'; //
    541: BitBtn.Hint := 'Substitui��o de NFSe|Substitui��o de NFSe'; //
    542: BitBtn.Hint := 'RPS|RPS - Recibo provis�rio de Servi�o'; //
    543: BitBtn.Hint := 'DPS|DPS - Declara��o de presta��o de servi�o'; //
    //
    546: BitBtn.Hint := 'Malote de RPS|Malote de RPS'; //
    547: BitBtn.Hint := 'Servi�os prote��o|Servi�os prote��o'; //
    548: BitBtn.Hint := 'Composi��o qu�mica|Composi��o qu�mica'; // Princ�pio ativo
    550: BitBtn.Hint := 'Cobran�a judicial|Cobran�a judicial'; //
    551: BitBtn.Hint := 'Config. envio msg|Configura��o de envio de msg'; //
    552: BitBtn.Hint := 'Item config envio msg|Item de configura��o de envio de msg'; //
    //
    553: BitBtn.Hint := 'Uploads|Upload de arquivos'; //
    //
    555: BitBtn.Hint := 'Atributos|Cadastro de Atributos'; //
    556: BitBtn.Hint := 'Itens de Atributo|Itens de Atributo'; //
    //
    559: BitBtn.Hint := 'Pr� cadastro de p�s venda|Pr� cadastro de p�s venda'; //
    560: BitBtn.Hint := 'Manifesta��o Destinat�rio|Manifesta��o Destinat�rio'; //
    561: BitBtn.Hint := 'Conectar|Conectar';
    562: BitBtn.Hint := 'Desconectar|Desconectar';
    //
    568: BitBtn.Hint := 'Retorno de insumo|Retorno de insumo';
    569: BitBtn.Hint := 'Grupos de emiss�es|Grupos de emiss�es';
    //
    570: BitBtn.Hint := 'Lotes NF-e|Lotes NF-e';
    //
    575: BitBtn.Hint := 'Volumes da NF-e|Volumes da NF-e';
    //
    578: BitBtn.Hint := 'Conf. In Natura|Conf. In Natura';
    //
    589: BitBtn.Hint := 'IME-I|Identificador de movimenta��o de estoque - item';
    590: BitBtn.Hint := 'Couro|Couro';
    //
    600: BitBtn.Hint := 'Classificar|Classificar';
    //
    605: BitBtn.Hint := 'Sub Produto|Sub Produto';
    //
    611: BitBtn.Hint := 'IME-I|IME-I';
    612: BitBtn.Hint := 'Servi�o|Servi�o';
    613: BitBtn.Hint := 'Ordem de Servi�o|Ordem de Servi�o';
    614: BitBtn.Hint := 'CT-e|CT-e';
    //
    615: BitBtn.Hint := 'Ajuda|Tutorias de Ajuda';
    616: BitBtn.Hint := 'Encerramento|Fluxo de Caixa - Di�rio';
    617: BitBtn.Hint := 'Sped|Sped';
    618: BitBtn.Hint := 'Turmas|Turmas';
    619: BitBtn.Hint := 'Gerencia Professores|Gerencia Professores';
    620: BitBtn.Hint := 'Encerrar conex�o|Encerrar conex�o'; //
    //
    629: BitBtn.Hint := 'Restri��es|Restri��es'; //
    630: BitBtn.Hint := 'Assinatura|Assinatura'; //
    //
  10001: BitBtn.Hint := 'Banco de dados|Banco de Dados'; //
  10002: BitBtn.Hint := 'Rejeita|Rejeita'; //
  10003: BitBtn.Hint := 'Condom�nios|Condom�nios'; //
  10004: BitBtn.Hint := 'Previs�o base|Previs�o base'; //
  10005: BitBtn.Hint := 'Condom�nios clientes|Condom�nios clientes'; //
  10006: BitBtn.Hint := 'Or�amento|Cria or�amento'; //
  10007: BitBtn.Hint := 'Bloquetos|Cria bloquetos'; //
  10008: BitBtn.Hint := 'Ativa / desativa item|Ativa / desativa item'; //
  10009: BitBtn.Hint := 'Ver itens|Ver itens salvos'; //
  10010: BitBtn.Hint := 'Cadastro|Cadastro r�pido'; //
  10011: BitBtn.Hint := 'Conciliar|Concilia��o banc�ria'; //
  10012: BitBtn.Hint := 'Cadastro de Banco|Cadastro de institui��o banc�ria'; //
  10013: BitBtn.Hint := 'Volta|Volta � janela anterior'; //
    //
  10014: BitBtn.Hint := 'Arrecada��o|Arrecada��es de provis�es'; //
  10015: BitBtn.Hint := 'Leituras|Leituras de consumos'; //
  10016: BitBtn.Hint := 'Provis�o|Provis�es de despesas'; //
  10017: BitBtn.Hint := 'Abertos|Itens abertos'; //
  10018: BitBtn.Hint := 'CNAB|Retorno CNAB'; //
  10019: BitBtn.Hint := 'C�pia de cheque|C�pia de cheque'; //
  10020: BitBtn.Hint := 'Pr�-bloqueto|Pr�-bloqueto'; //
  10021: BitBtn.Hint := 'Im�vel|Im�vel'; //
  10022: BitBtn.Hint := 'Provis�es bases|Provis�es bases'; //
  10023: BitBtn.Hint := 'Arrecada��es bases|Arrecada��es bases'; //
  10024: BitBtn.Hint := 'Pagar / Receber / Quitar / Rolar|Pagar / Receber / Quitar / Rolar'; //
  10025: BitBtn.Hint := 'Fluxo de caixa|Fluxo de caixa'; //
  10026: BitBtn.Hint := 'Analisa contrato|Analisa contrato (Aprova ou reprova)'; //
  10027: BitBtn.Hint := 'Corretores|Corretores'; //
  10028: BitBtn.Hint := 'Status|Status do contrato (posi��o atual)'; //
  10029: BitBtn.Hint := 'Contratos|Contratos'; //
  10030: BitBtn.Hint := 'Institui��o financeira|Institui��o financeira'; //
  10031: BitBtn.Hint := 'Comiss�es|Comiss�es escalonadas por taxas aplicadas'; //
  10032: BitBtn.Hint := 'Agenda cobran�a de diferen�a de juros|Agenda cobran�a de diferen�a de juros'; //
  10033: BitBtn.Hint := 'Retrocede|Retrocede'; //
  10034: BitBtn.Hint := 'Avan�a|Avan�a'; //
  10035: BitBtn.Hint := 'Salva dados|Salva dados'; //
  10036: BitBtn.Hint := 'Exporta|Exporta para outros formatos'; //
  10037: BitBtn.Hint := 'Mostra janela|Mostra janela'; //
  10038: BitBtn.Hint := 'Gerencia lan�amento|Gerencia lan�amento'; //
  10039: BitBtn.Hint := 'Bloqueto(s)|Bloqueto(s)'; //
  10040: BitBtn.Hint := 'OSs em aberto|OSs em Aberto'; //
  10041: BitBtn.Hint := 'Parcelamento|Parcelamento'; //
  10042: BitBtn.Hint := 'Emite cheque|Emite cheque'; //
  10043: BitBtn.Hint := 'Gerencia clientes|Gerencia clientes'; //
  10044: BitBtn.Hint := 'Despesas|Despesas'; //
  10045: BitBtn.Hint := 'Finan�as|Gerenciamento financeiro'; //
  10046: BitBtn.Hint := 'Empr�timos|Pagar / receber empr�stimos'; //
  10047: BitBtn.Hint := 'Cadastros de usu�rios|Cadastros de usu�rios'; //
  10048: BitBtn.Hint := 'Cadastros de perfis|Cadastros de perfis de usu�rios'; //
  10049: BitBtn.Hint := 'Empresa|Dados da empresa'; //
  10050: BitBtn.Hint := 'Recalcula percentuais|Recalcula percentuais proporcionais'; //
  10051: BitBtn.Hint := 'Protocolos|Protocolos'; //
  10052: BitBtn.Hint := '�ltimo condom�nio selecionado|�ltimo condom�nio selecionado'; //
  10053: BitBtn.Hint := 'Impress�o (configurar antes)|Impress�o (configurar antes)'; //
  10054: BitBtn.Hint := 'Sele��o de itens|Sele��o de itens'; //
  10055: BitBtn.Hint := 'Customiza��o|Customiza��o do produto'; //
  10056: BitBtn.Hint := 'Multiplas op��es|Multiplas op��es'; //
  10057: BitBtn.Hint := 'Rol de membros|Gerenciamento do Cadstro de Membrol (Rol de Membros)'; //
  10058: BitBtn.Hint := 'Renova|Renova Contrato'; //
  10059: BitBtn.Hint := 'Chamada|Lista de chamada'; //
  10060: BitBtn.Hint := 'Turmas|Turmas'; //
  10061: BitBtn.Hint := 'Matr�cula|Matr�cula'; //
  10062: BitBtn.Hint := 'Entidades|Cadastro de Entidades'; //
  10063: BitBtn.Hint := 'Hor�rios|Gerenciamento de hor�rios'; //
  10064: BitBtn.Hint := 'Recalcula e atualiza|Recalcula e atualiza'; //
  10065: BitBtn.Hint := 'Defini��o / sele��o|Defini��o / sele��o'; //
  10066: BitBtn.Hint := 'Slides|Imagens de slides'; //
  10067: BitBtn.Hint := 'Limpa texto|Limpa texto'; //
  10068: BitBtn.Hint := 'Item|Item'; //
  10069: BitBtn.Hint := 'Finan�as|Gerenciamento financeiro'; //
  10070: BitBtn.Hint := 'Mover uma pasta acima'; //
  10071: BitBtn.Hint := 'Voltar'; //
  10072: BitBtn.Hint := 'In�cio'; //
  10073: BitBtn.Hint := 'Download'; //
  10074: BitBtn.Hint := 'Excel'; //
  10075: BitBtn.Hint := 'Upload'; //
  10076: BitBtn.Hint := 'Favoritos'; //
  10077: BitBtn.Hint := 'Inclui novo diret�rio|Inclui novo diret�rio'; //
  10078: BitBtn.Hint := 'Intervalo de apura��o|Intervalo de apura��o'; //
  10079: BitBtn.Hint := 'Valores do intervalo de apura��o|Valores do intervalo de apura��o'; //
  10080: BitBtn.Hint := 'Ajuste/Benef�cio/Incentivo da Apura��o|Ajuste/Benef�cio/Incentivo da Apura��o'; //
  10081: BitBtn.Hint := 'Valores declarat�rios|Valores declarat�rios'; //
  10082: BitBtn.Hint := 'Obriga��es do ICMS a recolher|Obriga��es do ICMS a recolher'; //
  10083: BitBtn.Hint := 'Energia el�trica / �gua / g�s|Energia el�trica / �gua / g�s'; //
  10084: BitBtn.Hint := 'Comunica��o e telecomunica��o|Comunica��o e telecomunica��o'; //
  10085: BitBtn.Hint := 'NF 01 / 1B / 04 / 55|NF (01) / NF Avulsa (1B) / NF Produtor (04) / NF-e (55)'; //
  10086: BitBtn.Hint := 'Calcula|Calcula'; //
  10087: BitBtn.Hint := 'Inclui autom�tico|inclui autom�tico'; //
  10088: BitBtn.Hint := 'Layout|Layout'; //
  10089: BitBtn.Hint := 'Item de layout|Item de layout'; //
  10090: BitBtn.Hint := 'Repara|Repara tabelas de banco de dados'; //
  10091: BitBtn.Hint := 'Carta de corre��o|CCe NF-e (carta de corre��o eletr�nica)'; //
  10092: BitBtn.Hint := 'Contatos|Cadastro de contatos'; //
  10093: BitBtn.Hint := 'Respons�veis|Cadastro de respons�veis'; //
  10094: BitBtn.Hint := 'Lan�amentos por documento|Inclus�o de lan�amentos por documento'; //
  10095: BitBtn.Hint := 'Obra|Obra'; //
  10096: BitBtn.Hint := 'Impress�es diversas|Impress�es diversas'; //
  10097: BitBtn.Hint := 'Eventos|Eventos'; //
  10098: BitBtn.Hint := 'Itens prazo|Itens condi��o de pagamento'; //
  10099: BitBtn.Hint := 'Atributos|Atributos'; //
  10100: BitBtn.Hint := 'Janelas|Janelas'; //
  10101: BitBtn.Hint := 'CTB|CTB'; //
  10102: BitBtn.Hint := 'Faturamento|Faturamento'; //
  10103: BitBtn.Hint := 'Volumes|Volumes'; //
  10104: BitBtn.Hint := 'Regra fiscal|Regra fiscal'; //
  10105: BitBtn.Hint := 'Emitente / destinat�rio|Emitente / destinat�rio'; //
  10106: BitBtn.Hint := 'Corrige c�digo|Corrige c�digo'; //
  10107: BitBtn.Hint := 'Tabelas de Pre�os|Tabelas de Pre�os'; //
  10108: BitBtn.Hint := 'Abrang�ncia|Abrang�ncia'; //
  10109: BitBtn.Hint := 'Itens de f�rmula|Itens de f�rmula'; //
  10110: BitBtn.Hint := 'Grupos de Tags|Grupos de Tags'; //
  10111: BitBtn.Hint := 'Lista de Perguntas|Lista de Perguntas'; //
  10112: BitBtn.Hint := 'Item de Pergunta|Item de Pergunta'; //
  10113: BitBtn.Hint := 'Certid�o|Certid�o'; //
  10114: BitBtn.Hint := 'Recorrige|Recorrige'; //
  10115: BitBtn.Hint := 'Ratifica (-)|Ratifica (-)'; //
  10116: BitBtn.Hint := 'Ratifica (+)|Ratifica (+)'; //
  10117: BitBtn.Hint := 'Reconfirma|Reconfirma'; //
  10118: BitBtn.Hint := 'Seleciona todo ano|Seleciona todo ano'; //
  10119: BitBtn.Hint := 'Lista de espera|Lista de espera'; //
  10120: BitBtn.Hint := 'Item de pr� cadastro de p�s venda|Item de pr� cadastro de p�s venda'; //
  10121: BitBtn.Hint := 'Fluxo|Fluxo de produ��o'; //
  10122: BitBtn.Hint := 'Opera��o|Opera��o'; //
  10123: BitBtn.Hint := 'Setor|Setor'; //
  10124: BitBtn.Hint := 'Configura��o|Configura��o'; //
  10125: BitBtn.Hint := 'Prepara gera��o|Prepara gera��o'; //
  10126: BitBtn.Hint := 'Sugest�es|Sugest�es'; //
  10127: BitBtn.Hint := 'Custo / Pre�o|Custo / Pre�o'; //
  10128: BitBtn.Hint := 'Banco de dados|Banco de dados'; //
  10129: BitBtn.Hint := 'Faixas|Faixas'; //
  10130: BitBtn.Hint := 'Ordenar|Ordenar campos'; //
  10131: BitBtn.Hint := 'Hoje|Data de hoje'; //
  10132: BitBtn.Hint := 'Centro de Custo|Centro de Custo'; //
  10133: BitBtn.Hint := 'Pesquisa financeira|Pesquisa financeira'; //
  10134: BitBtn.Hint := 'Remover Pallet|Remover Pallet'; //
  10135: BitBtn.Hint := 'Item de OS de servi�o|Item de OS de servi�o'; //
  //
  10136: BitBtn.Hint := 'Sobre|Sobre'; //
  //
  10200: BitBtn.Hint := 'Configura��o CNAB|Configura��o CNAB';
  10201: BitBtn.Hint := 'Prot. Padr�o Bol.|Protocolo padr�o de boletos';
  10202: BitBtn.Hint := 'Geren. Protocolos|Gerenciamento de protocolos';
  10203: BitBtn.Hint := 'Aplicativos|Aplicativos';
  10204: BitBtn.Hint := 'Observa��es|Observa��es';
  10205: BitBtn.Hint := 'M�dulos|M�dulos de aplicativos';
  10206: BitBtn.Hint := 'Croqui|Croqui';
  //: BitBtn.Hint := '|'; //
    else BitBtn.Hint := '|Hint n�o definido. Contate a Dermatek';
  end;
end;

function TFmMyGlyfs.ConfiguraFormDock(Form: TCustomForm): Boolean;
var
  i: Integer;
  Compo: TComponent;
begin
  for i := 0 to TCustomForm(Form).ComponentCount -1 do
  begin
    Compo := TCustomForm(Form).Components[i];
    if TCustomForm(Form).Components[i] is TBitBtn then
    begin
      FmMyGlyfs.DefineGlyphBtnTag(TBitBtn(Compo));
      TBitBtn(Compo).ShowHint := True;
      HintDeBitBtn(TBitBtn(Compo));
    end;
  end;
  Result := True;
end;

procedure TFmMyGlyfs.DefineGlyfs(ScreenActiveForm: TForm);
var
  i: Integer;
  //i, H, W : Integer;
  Compo : TComponent;
  //cor : TColor;
begin
  for i := 0 to ScreenActiveForm.ComponentCount -1 do
  begin
    Compo := ScreenActiveForm.Components[i];
    if ScreenActiveForm.Components[i] is TBitBtn then
    begin
      //TBitBtn(Compo).Hint := '';
      if TBitBtn(Compo).Hint = 'Inclui novo banco' then
        TBitBtn(Compo).Hint := 'Inclui novo registro'
      else if TBitBtn(Compo).Hint = 'Altera banco atual' then
        TBitBtn(Compo).Hint := 'Altera registro atual'
      else if TBitBtn(Compo).Hint = 'Exclui banco atual' then
        TBitBtn(Compo).Hint := 'Exclui registro atual'
      else if TBitBtn(Compo).Hint = 'Altera banco atual' then
        TBitBtn(Compo).Hint := 'Altera registro atual'
      else if TBitBtn(Compo).Hint = 'ltimo registro' then
        TBitBtn(Compo).Hint := '�ltimo registro';

      if VAR_NAO_USA_TAG_NEM_NOME then
      begin
        // n�o faz nada
      end else if VAR_USA_TAG_BITBTN then
      begin
         DefineGlyphBtnTag(TBitBtn(Compo));
         // 2011-09-26
         if TBitBtn(Compo).NumGlyphs <> 2 then
           TBitBtn(Compo).NumGlyphs := 2;
         // Fim 2011-09-26
         TBitBtn(Compo).ShowHint := True;
         HintDeBitBtn(TBitBtn(Compo));
{
      end else if (TBitBtn(Compo).Name = 'SbImprime') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(0)
      else if (TBitBtn(Compo).Name = 'BtImprime') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(0)
      else if (TBitBtn(Compo).Name = 'BtImprime1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(0)
      else if (TBitBtn(Compo).Name = 'BtImprime2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(0)
      else if (TBitBtn(Compo).Name = 'BtImprime3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(0)
      else if (TBitBtn(Compo).Name = 'BtImprime4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(0)
      else if (TBitBtn(Compo).Name = 'BtImprime5') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(0)
      else if (TBitBtn(Compo).Name = 'BtImprime6') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(0)
      else if (TBitBtn(Compo).Name = 'BtImprimir') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(0)
      else if (TBitBtn(Compo).Name = 'BtListagem') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(0)
      else if (TBitBtn(Compo).Name = 'BtExclui_Antigo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(1)
      else if (TBitBtn(Compo).Name = 'BtLocaliza1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(2)
      else if (TBitBtn(Compo).Name = 'BtLocaliza2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(2)
      else if (TBitBtn(Compo).Name = 'BtProcura1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(2)
      else if (TBitBtn(Compo).Name = 'BtProcura2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(2)
      else if (TBitBtn(Compo).Name = 'BtPesquisa') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(2)
      else if (TBitBtn(Compo).Name = 'BtPesquisa1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(2)
      else if (TBitBtn(Compo).Name = 'BtPesquisa2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(2)
      else if (TBitBtn(Compo).Name = 'BtVerifica') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(2)
      else if (TBitBtn(Compo).Name = 'BtExplodir') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(3)
      else if (TBitBtn(Compo).Name = 'SpeedButton1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(4)
      else if (TBitBtn(Compo).Name = 'SpeedButton2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(5)
      else if (TBitBtn(Compo).Name = 'SpeedButton3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(6)
      else if (TBitBtn(Compo).Name = 'SpeedButton4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(7)
      //
      else if (TBitBtn(Compo).Name = 'BtIncluiAntigo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(8)
      else if (TBitBtn(Compo).Name = 'BtAlteraAntigo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(9)

      else if (TBitBtn(Compo).Name = 'BtDesiste') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(10)
      else if (TBitBtn(Compo).Name = 'BtDesiste0') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(10)
      else if (TBitBtn(Compo).Name = 'BtDesiste1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(10)
      else if (TBitBtn(Compo).Name = 'BtDesiste2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(10)
      else if (TBitBtn(Compo).Name = 'BtDesiste3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(10)
      else if (TBitBtn(Compo).Name = 'BtDesiste4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(10)
      else if (TBitBtn(Compo).Name = 'BtDesiste5') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(10)
      else if (TBitBtn(Compo).Name = 'BtDesiste6') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(10)
      else if (TBitBtn(Compo).Name = 'BtDesiste7') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(10)
      else if (TBitBtn(Compo).Name = 'BtCancela') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(10)
      //
      else if (TBitBtn(Compo).Name = 'SbNumero') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(11)
      else if (TBitBtn(Compo).Name = 'SbNome') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(12)
      else if (TBitBtn(Compo).Name = 'SbQuery') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(13)
      //
      else if (TBitBtn(Compo).Name = 'BtOK_Antigo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(14)
      //
      else if (TBitBtn(Compo).Name = 'BtEntidades') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(15)
      else if (TBitBtn(Compo).Name = 'SbEntidades') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(15)
      else if (TBitBtn(Compo).Name = 'BtClientes') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(15)
      else if (TBitBtn(Compo).Name = 'BtFornecedores') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(15)
      else if (TBitBtn(Compo).Name = 'BtReceptores') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(15)
      else if (TBitBtn(Compo).Name = 'BtVendedores') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(15)
      else if (TBitBtn(Compo).Name = 'BtTransportadores') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(15)
      //
      else if (TBitBtn(Compo).Name = 'BtCaixas') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(16)
      else if (TBitBtn(Compo).Name = 'BtArquivoMorto') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(16)
      //
      else if (TBitBtn(Compo).Name = 'BtInstall') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(17)
      else if (TBitBtn(Compo).Name = 'BtCor1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(18)
      else if (TBitBtn(Compo).Name = 'BtCor2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(18)
      else if (TBitBtn(Compo).Name = 'BtCor3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(18)
      else if (TBitBtn(Compo).Name = 'BtCor4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(18)
      else if (TBitBtn(Compo).Name = 'BtCores') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(18)
      else if (TBitBtn(Compo).Name = 'BtCores1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(18)
      else if (TBitBtn(Compo).Name = 'BtCores2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(18)
      else if (TBitBtn(Compo).Name = 'BtTamanhos') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(19)
      else if (TBitBtn(Compo).Name = 'SbFilmes') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(19)
      //
      else if (TBitBtn(Compo).Name = 'BtRefresh') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(20)
      else if (TBitBtn(Compo).Name = 'BtRefresh1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(20)
      else if (TBitBtn(Compo).Name = 'BtRefresh2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(20)
      else if (TBitBtn(Compo).Name = 'BtRefresh3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(20)
      else if (TBitBtn(Compo).Name = 'BtRefresh4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(20)
      else if (TBitBtn(Compo).Name = 'BtAtualiza') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(20)
      //
      else if (TBitBtn(Compo).Name = 'BtEmprestimo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(22)
      //
      else if (TBitBtn(Compo).Name = 'BtCEP1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(23)
      else if (TBitBtn(Compo).Name = 'BtCEP2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(23)
      else if (TBitBtn(Compo).Name = 'BtCEP3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(23)
      else if (TBitBtn(Compo).Name = 'BtCEP4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(23)
      //
      else if (TBitBtn(Compo).Name = 'BtCopiar') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(24)
      //
      else if (TBitBtn(Compo).Name = 'BtDinheiro1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(25)
      else if (TBitBtn(Compo).Name = 'BtTeste1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(25)
      else if (TBitBtn(Compo).Name = 'BtTeste3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(25)
      else if (TBitBtn(Compo).Name = 'BtContarDinheiro') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(25)
      else if (TBitBtn(Compo).Name = 'BtComissoes') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(25)
      //
      else if (TBitBtn(Compo).Name = 'BtTeste2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(26)
      else if (TBitBtn(Compo).Name = 'SbNovo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(26)
      //
      else if (TBitBtn(Compo).Name = 'BtZAZ') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(27)
      //
      else if (TBitBtn(Compo).Name = 'BtMenu') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(28)
      else if (TBitBtn(Compo).Name = 'BtMenu1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(28)
      //
      else if (TBitBtn(Compo).Name = 'BtTodos') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(29)
      else if (TBitBtn(Compo).Name = 'BtTudo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(29)
      else if (TBitBtn(Compo).Name = 'BtNenhum') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(30)
      //
      else if (TBitBtn(Compo).Name = 'BtDown') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(31)
      else if (TBitBtn(Compo).Name = 'BtUp') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(32)
      //
      else if (TBitBtn(Compo).Name = 'BtImagem') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(33)
      else if (TBitBtn(Compo).Name = 'BtImagem1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(33)
      else if (TBitBtn(Compo).Name = 'BtSkin') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(33)
      //
      else if (TBitBtn(Compo).Name = 'BtSalvar') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(34)
      else if (TBitBtn(Compo).Name = 'BtGravar') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(34)
      else if (TBitBtn(Compo).Name = 'BtSalvarComo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(35)
      else if (TBitBtn(Compo).Name = 'BtEstrutura') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(35)
      //
      else if (TBitBtn(Compo).Name = 'BtCartao') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(36)
      //
      else if (TBitBtn(Compo).Name = 'BtNivelAcima') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(37)
      //
      else if (TBitBtn(Compo).Name = 'BtNivelAcima1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(37)
      //
      else if (TBitBtn(Compo).Name = 'BtNivelAcima2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(37)
      //
      else if (TBitBtn(Compo).Name = 'BtNivelAcima3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(37)
      //
      else if (TBitBtn(Compo).Name = 'BtNivelAcima4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(37)
      //
      //
      else if (TBitBtn(Compo).Name = 'BtAbrir') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(38)
      else if (TBitBtn(Compo).Name = 'BtAbrir1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(38)
      else if (TBitBtn(Compo).Name = 'BtAbrir2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(38)
      else if (TBitBtn(Compo).Name = 'BtAbrir3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(38)
      else if (TBitBtn(Compo).Name = 'BtAbrir4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(38)
      //
      else if (TBitBtn(Compo).Name = 'BtImportarAntigo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(39)
      //
      else if (TBitBtn(Compo).Name = 'BtPreview') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(40)
      else if (TBitBtn(Compo).Name = 'BtPreview1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(40)
      //
      else if (TBitBtn(Compo).Name = 'BtASCII') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(41)
      //
      else if (TBitBtn(Compo).Name = 'BtConcilia') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(42)
      else if (TBitBtn(Compo).Name = 'BtDuplica') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(42)
      //
      else if (TBitBtn(Compo).Name = 'BtDireto') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(43)
      //
      else if (TBitBtn(Compo).Name = 'BtEmitido') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(44)
      else if (TBitBtn(Compo).Name = 'BtTransf') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(44)
      //
      else if (TBitBtn(Compo).Name = 'BtFatura') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(45)
      //
      else if (TBitBtn(Compo).Name = 'BtConsigna') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(46)
      else if (TBitBtn(Compo).Name = 'SbLocadores') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(46)
      //
      else if (TBitBtn(Compo).Name = 'BtDivida') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(47)
      //
      else if (TBitBtn(Compo).Name = 'BtErro') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(48)
      //
      else if (TBitBtn(Compo).Name = 'BtEmpresa') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(49)
      //
      else if (TBitBtn(Compo).Name = 'Bt06') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(50)
      else if (TBitBtn(Compo).Name = 'Bt08') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(51)
      else if (TBitBtn(Compo).Name = 'Bt10') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(52)
      else if (TBitBtn(Compo).Name = 'Bt12') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(53)
      else if (TBitBtn(Compo).Name = 'BtPeriodo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(53)
      else if (TBitBtn(Compo).Name = 'SbVaiPara') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(53)
      else if (TBitBtn(Compo).Name = 'Bt14') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(54)
      //
      else if (TBitBtn(Compo).Name = 'BtLibera') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(55)
      //
      else if (TBitBtn(Compo).Name = 'BtSoma') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(56)
      else if (TBitBtn(Compo).Name = 'BtAdiciona') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(56)
      else if (TBitBtn(Compo).Name = 'BtQuita') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(56)
      else if (TBitBtn(Compo).Name = 'BtQuita2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(56)
      else if (TBitBtn(Compo).Name = 'BtSomaCheque') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(56)
      else if (TBitBtn(Compo).Name = 'BtSomaCheque1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(56)
      //
      else if (TBitBtn(Compo).Name = 'BtImpConfig1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(57)
      else if (TBitBtn(Compo).Name = 'BtImpConfig2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(57)
      //
      else if (TBitBtn(Compo).Name = 'BtGrade') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(58)
      else if (TBitBtn(Compo).Name = 'BtGrade1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(58)
      else if (TBitBtn(Compo).Name = 'BtGrade2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(58)
      //
      else if (TBitBtn(Compo).Name = 'BtBackup') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(59)
      //
      else if (TBitBtn(Compo).Name = 'BtDriveA') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(60)
      //
      else if (TBitBtn(Compo).Name = 'BtDriveC') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(61)
      //
      else if (TBitBtn(Compo).Name = 'BtCorta') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(62)
      //
      else if (TBitBtn(Compo).Name = 'BtAnalogo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(63)
      //
      else if (TBitBtn(Compo).Name = 'BtExporta') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(64)
      //
      else if (TBitBtn(Compo).Name = 'BtCalcula') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(65)
      //
      else if (TBitBtn(Compo).Name = 'BtEMail') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(66)
      //
      else if (TBitBtn(Compo).Name = 'BtRaio') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(67)
      else if (TBitBtn(Compo).Name = 'BtZAZ') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(67)
      else if (TBitBtn(Compo).Name = 'BtTeste') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(67)
      //
      else if (TBitBtn(Compo).Name = 'BtLimpa') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(68)
      else if (TBitBtn(Compo).Name = 'BtLimpa1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(68)
      else if (TBitBtn(Compo).Name = 'BtLimpa2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(68)
      else if (TBitBtn(Compo).Name = 'BtLimpa3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(68)
      else if (TBitBtn(Compo).Name = 'BtLimpa4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(68)
      else if (TBitBtn(Compo).Name = 'BtLimpa5') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(68)
      else if (TBitBtn(Compo).Name = 'BtLimpa6') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(68)
      //
      else if (TBitBtn(Compo).Name = 'BtNovoE') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(69)
      else if (TBitBtn(Compo).Name = 'BtEditaE') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(70)
      else if (TBitBtn(Compo).Name = 'BtNovoS') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(71)
      else if (TBitBtn(Compo).Name = 'BtEditaS') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(72)
      //
      else if (TBitBtn(Compo).Name = 'BtTroca') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(73)
      else if (TBitBtn(Compo).Name = 'BtOrdem') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(73)
      else if (TBitBtn(Compo).Name = 'BtRestore') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(73)
      //
      else if (TBitBtn(Compo).Name = 'BtInfo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(74)
      //
      else if (TBitBtn(Compo).Name = 'BtOperacoes') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(75)
      //
      else if (TBitBtn(Compo).Name = 'BtAcoes') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(76)
      //
      else if (TBitBtn(Compo).Name = 'BtClassifica') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(77)
      //
      else if (TBitBtn(Compo).Name = 'BtMercadorias') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(78)
      //
      else if (TBitBtn(Compo).Name = 'BtMercadoria1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(79)
      //
      else if (TBitBtn(Compo).Name = 'BtCDVai') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(80)
      //
      else if (TBitBtn(Compo).Name = 'BtCDVem') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(81)
      //
      else if (TBitBtn(Compo).Name = 'BtTickets') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(82)
      else if (TBitBtn(Compo).Name = 'SbEspectadores') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(83)
      else if (TBitBtn(Compo).Name = 'SbTelevisao') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(84)
      else if (TBitBtn(Compo).Name = 'BtEntiPesq') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(85)
      else if (TBitBtn(Compo).Name = 'SbEntiPesq') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(85)
      else if (TBitBtn(Compo).Name = 'SbPreviewFast') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(86)
      else if (TBitBtn(Compo).Name = 'SbPesq3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(87)
      else if (TBitBtn(Compo).Name = 'SbReserva') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(88)
      else if (TBitBtn(Compo).Name = 'BtItens') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(88)
      else if (TBitBtn(Compo).Name = 'SbAlegre') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(89)
      else if (TBitBtn(Compo).Name = 'SbEspeciais') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(90)
      else if (TBitBtn(Compo).Name = 'SbRadar') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(91)
      else if (TBitBtn(Compo).Name = 'SbTelefone') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(92)
      else if (TBitBtn(Compo).Name = 'SbRefresh5') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(93)
      else if (TBitBtn(Compo).Name = 'BtArquiva') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(94)
      else if (TBitBtn(Compo).Name = 'BtAjustar') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(94)
      else if (TBitBtn(Compo).Name = 'BtProcessos') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(095)
      else if (TBitBtn(Compo).Name = 'BtEntradaCouro') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(096)
      else if (TBitBtn(Compo).Name = 'BtDesconto') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(97)
      else if (TBitBtn(Compo).Name = 'BtRecovery') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(98)
      else if (TBitBtn(Compo).Name = 'BtGestor') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(99)
      else if (TBitBtn(Compo).Name = 'BtTerceiro') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(99)
      else if (TBitBtn(Compo).Name = 'BtGrupo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(100)
      else if (TBitBtn(Compo).Name = 'BtRoupa') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(101)
      else if (TBitBtn(Compo).Name = 'BtMochila') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(102)
      else if (TBitBtn(Compo).Name = 'BtTrio') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(103)
      else if (TBitBtn(Compo).Name = 'BtPagtos') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(104)
      else if (TBitBtn(Compo).Name = 'BtDespesas') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(104)
      else if (TBitBtn(Compo).Name = 'BtServicos') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(105)
      else if (TBitBtn(Compo).Name = 'SbServicos') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(105)
      else if (TBitBtn(Compo).Name = 'BitBtn1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(106)
      else if (TBitBtn(Compo).Name = 'BitBtn2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(106)
      else if (TBitBtn(Compo).Name = 'BitBtn3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(106)
      else if (TBitBtn(Compo).Name = 'BitBtn4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(106)
      else if (TBitBtn(Compo).Name = 'BitBtn5') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(106)
      else if (TBitBtn(Compo).Name = 'BitBtn6') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(106)
      else if (TBitBtn(Compo).Name = 'BitBtn7') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(106)
      else if (TBitBtn(Compo).Name = 'BitBtn8') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(106)
      else if (TBitBtn(Compo).Name = 'BitBtn9') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(106)
      else if (TBitBtn(Compo).Name = 'BitBtn10') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(106)
      else if (TBitBtn(Compo).Name = 'BitBtn11') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(106)
      else if (TBitBtn(Compo).Name = 'BitBtn12') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(106)
      else if (TBitBtn(Compo).Name = 'BtPlacaDetran') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(107)
      //
      else if (TBitBtn(Compo).Name = 'BtReabre') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(108)
      else if (TBitBtn(Compo).Name = 'BtReabre1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(108)
      else if (TBitBtn(Compo).Name = 'BtReabre2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(108)
      else if (TBitBtn(Compo).Name = 'BtReabre3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(108)
      else if (TBitBtn(Compo).Name = 'BtVendaCouro') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(109)
      else if (TBitBtn(Compo).Name = 'BtNotaMusical') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(110)
      else if (TBitBtn(Compo).Name = 'BtSmailei') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(111)
      //
      else if (TBitBtn(Compo).Name = 'BtImpressora') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(112)
      else if (TBitBtn(Compo).Name = 'BtLiturgias') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(113)
      else if (TBitBtn(Compo).Name = 'BtTransportadora') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(114)
      else if (TBitBtn(Compo).Name = 'BtPack') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(114)
      else if (TBitBtn(Compo).Name = 'BtMercadoria') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(115)
      else if (TBitBtn(Compo).Name = 'BtCadastros') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(116)
      else if (TBitBtn(Compo).Name = 'BtVendas') then   //135
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(117)
      else if (TBitBtn(Compo).Name = 'BtCompras') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(118)
      else if (TBitBtn(Compo).Name = 'BtCombinar') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(119)
      else if (TBitBtn(Compo).Name = 'BtPlanilha') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(120)
      else if (TBitBtn(Compo).Name = 'BtPrintCal') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(121)
      else if (TBitBtn(Compo).Name = 'BtPrintCur') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(122)
      else if (TBitBtn(Compo).Name = 'BtPrintRec') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(123)
      else if (TBitBtn(Compo).Name = 'BtPrintAca') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(124)
      else if (TBitBtn(Compo).Name = 'BtPesaCal') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(125)
      else if (TBitBtn(Compo).Name = 'BtPesaCur') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(126)
      else if (TBitBtn(Compo).Name = 'BtPesaRec') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(127)
      else if (TBitBtn(Compo).Name = 'BtPesaAca') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(128)
      else if (TBitBtn(Compo).Name = 'BtInclui') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(129)
      else if (TBitBtn(Compo).Name = 'BtIncluiIts') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(129)
      else if (TBitBtn(Compo).Name = 'BtInclui1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(129)
      else if (TBitBtn(Compo).Name = 'BtInclui2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(129)
      else if (TBitBtn(Compo).Name = 'BtInclui5') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(129)
      else if (TBitBtn(Compo).Name = 'BtInsere') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(129)
      else if (TBitBtn(Compo).Name = 'BtInsereIts') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(129)
      else if (TBitBtn(Compo).Name = 'BtInserePgs') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(129)
      else if (TBitBtn(Compo).Name = 'BtEntrada') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(129)
      else if (TBitBtn(Compo).Name = 'BtLotes') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(130)
      else if (TBitBtn(Compo).Name = 'BtOS') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(131)
      else if (TBitBtn(Compo).Name = 'BtEtiqueta') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(132)
      else if (TBitBtn(Compo).Name = 'BtFluxos') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(133)
      else if (TBitBtn(Compo).Name = 'BtDireciona') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(133)
      else if (TBitBtn(Compo).Name = 'BtFichaEstoque') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(134)
      else if (TBitBtn(Compo).Name = 'BtFaturar') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(135)
      else if (TBitBtn(Compo).Name = 'BtFaturar1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(135)
      else if (TBitBtn(Compo).Name = 'BtReceitas') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(136)
      else if (TBitBtn(Compo).Name = 'BtPedidosCP') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(137)
      else if (TBitBtn(Compo).Name = 'BtParar') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(138)
      else if (TBitBtn(Compo).Name = 'BtInsumo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(139)
      else if (TBitBtn(Compo).Name = 'BtPQCad') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(139)
      else if (TBitBtn(Compo).Name = 'BtPQEntrada') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(140)
      else if (TBitBtn(Compo).Name = 'BtPQPed') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(141)
      else if (TBitBtn(Compo).Name = 'BtMount') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(141)
      else if (TBitBtn(Compo).Name = 'BtRatiCal') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(142)
      else if (TBitBtn(Compo).Name = 'BtRatiCur') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(143)
      else if (TBitBtn(Compo).Name = 'BtRatiRec') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(144)
      else if (TBitBtn(Compo).Name = 'BtRatiAca') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(145)
      else if (TBitBtn(Compo).Name = 'BtPesaPQExtra') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(146)
      else if (TBitBtn(Compo).Name = 'BtSubProd') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(147)
      else if (TBitBtn(Compo).Name = 'BtMostraFundo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(148)
      else if (TBitBtn(Compo).Name = 'BtImprimeNF') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(149)
      else if (TBitBtn(Compo).Name = 'BtEntidades2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(150)
      else if (TBitBtn(Compo).Name = 'BtEntidades2_1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(150)
      else if (TBitBtn(Compo).Name = 'BtRascunho') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(151)
      else if (TBitBtn(Compo).Name = 'BtReciclo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(152)
      else if (TBitBtn(Compo).Name = 'BtSomaCh') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(153)
      else if (TBitBtn(Compo).Name = 'BtProtecao') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(154)
      else if (TBitBtn(Compo).Name = 'BtCuidado') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(155)
      else if (TBitBtn(Compo).Name = 'BtChequeEmite') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(156)
      else if (TBitBtn(Compo).Name = 'BtDuplicata1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(156)
      else if (TBitBtn(Compo).Name = 'BtCheques') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(157)
      else if (TBitBtn(Compo).Name = 'BtMatricula') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(158)
      else if (TBitBtn(Compo).Name = 'BtMatricula2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(158)
      else if (TBitBtn(Compo).Name = 'BtMatricula3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(158)
      else if (TBitBtn(Compo).Name = 'BtMatricula4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(158)
      else if (TBitBtn(Compo).Name = 'BtEntidades3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(159)
      else if (TBitBtn(Compo).Name = 'BtNovo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(160)
      else if (TBitBtn(Compo).Name = 'BtRelatorios') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(160)
      else if (TBitBtn(Compo).Name = 'BtLista1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(160)
      else if (TBitBtn(Compo).Name = 'BtLista2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(160)
      else if (TBitBtn(Compo).Name = 'BtProdRecicl') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(161)
      else if (TBitBtn(Compo).Name = 'BtChart2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(162)
      else if (TBitBtn(Compo).Name = 'BtGrupos') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(162)
      else if (TBitBtn(Compo).Name = 'BtEntidades4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(163)
      else if (TBitBtn(Compo).Name = 'BtChamada') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(164)
      else if (TBitBtn(Compo).Name = 'BtChamadas') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(164)
      else if (TBitBtn(Compo).Name = 'BtDiploma1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(165)
      else if (TBitBtn(Compo).Name = 'BtDiploma2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(165)
      else if (TBitBtn(Compo).Name = 'BtDiploma3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(165)
      else if (TBitBtn(Compo).Name = 'BtRefaz') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(166)
      else if (TBitBtn(Compo).Name = 'BtRelogioInclui') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(167)
      else if (TBitBtn(Compo).Name = 'BtRelogioExclui') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(168)
      else if (TBitBtn(Compo).Name = 'BtTurmasInclui') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(169)
      else if (TBitBtn(Compo).Name = 'BtEntiTroca1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(169)
      else if (TBitBtn(Compo).Name = 'BtEntiTroca2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(169)
      else if (TBitBtn(Compo).Name = 'BtTurmasExclui') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(170)
      else if (TBitBtn(Compo).Name = 'BtAtividadesInclui') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(171)
      else if (TBitBtn(Compo).Name = 'BtAtividadesExclui') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(172)
      else if (TBitBtn(Compo).Name = 'BtRefazInclui') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(173)
      else if (TBitBtn(Compo).Name = 'BtRefazExclui') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(174)
      else if (TBitBtn(Compo).Name = 'BtRefazAltera') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(175)
      else if (TBitBtn(Compo).Name = 'BtDinheiro') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(176)
      else if (TBitBtn(Compo).Name = 'BtPagtosInclui') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(176)
      else if (TBitBtn(Compo).Name = 'BtPagtosInclui8') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(176)
      else if (TBitBtn(Compo).Name = 'BtPagtosInclui9') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(176)
      else if (TBitBtn(Compo).Name = 'BtPagamento') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(176)
      else if (TBitBtn(Compo).Name = 'BtPagamento2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(176)
      else if (TBitBtn(Compo).Name = 'BtPagamento6') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(176)
      else if (TBitBtn(Compo).Name = 'BtPagamento7') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(176)
      else if (TBitBtn(Compo).Name = 'BtPagtosExclui') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(177)
      else if (TBitBtn(Compo).Name = 'BtPagtosExclui2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(177)
      else if (TBitBtn(Compo).Name = 'BtPagtosExclui6') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(177)
      else if (TBitBtn(Compo).Name = 'BtPagtosExclui8') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(177)
      else if (TBitBtn(Compo).Name = 'BtPagtosExclui9') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(177)
      else if (TBitBtn(Compo).Name = 'BtPagtosAltera') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(178)
      else if (TBitBtn(Compo).Name = 'BtPagtosAltera2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(178)
      else if (TBitBtn(Compo).Name = 'BtPagtosAltera6') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(178)
      else if (TBitBtn(Compo).Name = 'BtPagtosAltera8') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(178)
      else if (TBitBtn(Compo).Name = 'BtTaxas') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(178)
      else if (TBitBtn(Compo).Name = 'BtChamada2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(179)
      else if (TBitBtn(Compo).Name = 'BtRelPedidos') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(179)
      else if (TBitBtn(Compo).Name = 'BtPagtoDuvida') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(180)
      else if (TBitBtn(Compo).Name = 'BtLocaSai') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(181)
      else if (TBitBtn(Compo).Name = 'BtLocaVolta') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(182)
      else if (TBitBtn(Compo).Name = 'BtLocaRoupa') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(183)
      else if (TBitBtn(Compo).Name = 'BtFamilia') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(183)
      else if (TBitBtn(Compo).Name = 'BtGera') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(184)
      else if (TBitBtn(Compo).Name = 'BtCheio') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(185)
      else if (TBitBtn(Compo).Name = 'BtCheioNAO') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(186)
      else if (TBitBtn(Compo).Name = 'BtFilial') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(187)
      else if (TBitBtn(Compo).Name = 'BtExcel') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(188)
      else if (TBitBtn(Compo).Name = 'BtPrivativo') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(189)
      else if (TBitBtn(Compo).Name = 'BtFechar') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(190)
      else if (TBitBtn(Compo).Name = 'BtSair') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(190)
      else if (TBitBtn(Compo).Name = 'BtSair2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(190)
      else if (TBitBtn(Compo).Name = 'BtSair4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(190)
      else if (TBitBtn(Compo).Name = 'BtSaida') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(190)
      else if (TBitBtn(Compo).Name = 'BtOK') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtOK_A') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtOK_C') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtConfirma') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtConfirma0') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtConfirma1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtConfirma2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtConfirma3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtConfirma4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtConfirma5') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtConfirma6') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtConfirma7') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtTrava') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(191)
      else if (TBitBtn(Compo).Name = 'BtExclui') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(192)
      else if (TBitBtn(Compo).Name = 'BtExclui1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(192)
      else if (TBitBtn(Compo).Name = 'BtExclui2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(192)
      else if (TBitBtn(Compo).Name = 'BtExclui3') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(192)
      else if (TBitBtn(Compo).Name = 'BtExclui4') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(192)
      else if (TBitBtn(Compo).Name = 'BtExclui5') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(192)
      else if (TBitBtn(Compo).Name = 'BtExcluiIts') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(192)
      else if (TBitBtn(Compo).Name = 'BtExcluiPgs') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(192)
      else if (TBitBtn(Compo).Name = 'BtEdita') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(194)
      else if (TBitBtn(Compo).Name = 'BtEditaIts') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(194)
      else if (TBitBtn(Compo).Name = 'BtEditaPgs') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(194)
      else if (TBitBtn(Compo).Name = 'BtAltera') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(194)
      else if (TBitBtn(Compo).Name = 'BtAltera1') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(194)
      else if (TBitBtn(Compo).Name = 'BtAltera2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(194)
      else if (TBitBtn(Compo).Name = 'BtAltera5') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(194)
      else if (TBitBtn(Compo).Name = 'BtAlteraIts') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(194)
      else if (TBitBtn(Compo).Name = 'BtBD') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(195)
      else if (TBitBtn(Compo).Name = 'BtCalendario') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(196)
      else if (TBitBtn(Compo).Name = 'BtCalendario2') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(196)
      else if (TBitBtn(Compo).Name = 'BtServico') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(197)
      else if (TBitBtn(Compo).Name = 'BtImportar') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(198)
      else if (TBitBtn(Compo).Name = 'BtCheque') then
         TBitBtn(Compo).Glyph := FmMyGlyfs.ObtemBitmapDeGlyph(199)
      //
}
    end;
    end;
  end;
end;

{$IfDef cSkinRank} //Berlin
procedure TFmMyGlyfs.DefineGlyfsTDI(sd1: TSkinData; Sender: TObject);
begin
  if sd1.active then
    sd1.SkinForm(TForm(Sender).Handle);
  //
  // ERRO! Oculta dmkEdits!
  //MyObjects.CorIniComponente(THandle(Sender));
  DefineGlyfs(TForm(Sender));
end;
{$EndIf}

{$IfDef cAlphaSkin} //Berlin
procedure TFmMyGlyfs.DefineGlyfsTDI2(sm1: TsSkinManager; Sender: TObject);
begin
  //TODO AlphaSkin
  //N�o existe a procedure/function "sm1.SkinForm(". Ver se h� compativel e se
  // realmente necessita!
  //if sm1.active then
    //sm1.SkinForm(TForm(Sender).Handle);
  //
  // ERRO! Oculta dmkEdits!
  //MyObjects.CorIniComponente(THandle(Sender));
  DefineGlyfs(TForm(Sender));
end;
{$EndIf} //Berlin

end.
