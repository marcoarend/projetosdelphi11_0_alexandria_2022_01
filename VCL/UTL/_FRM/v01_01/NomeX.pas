unit NomeX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  Mask, mySQLDbTables, dmkImage, dmkGeral, UnDmkEnums;

type
  TFmNomeX = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    EdTxt: TEdit;
    RGMascara: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    function ObtemSQLPesquisaText(Mascara: Integer; Texto: String): String;
  public
    { Public declarations }
    FOrigi, FTexto: String;
    FContinua: Boolean;
  end;

var
  FmNomeX: TFmNomeX;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmNomeX.BtOKClick(Sender: TObject);
var
  Texto: String;
begin
  FContinua := True;
  FTexto    := '';
  FOrigi    := EdTxt.Text;
  Texto     := EdTxt.Text;
  //
  FTexto := ObtemSQLPesquisaText(RGMascara.ItemIndex, Texto);
  //
  Close;
end;

procedure TFmNomeX.BtSaidaClick(Sender: TObject);
begin
  FContinua := False;
  Close;
end;

procedure TFmNomeX.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmNomeX.ObtemSQLPesquisaText(Mascara: Integer; Texto: String): String;
var
  Txt, Res: String;
begin
  Txt := Texto;
  //
  if Mascara in ([0, 3]) then
    Txt := StringReplace(Txt, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
  //
  Res := '';
  //
  if Mascara in ([0, 1, 2]) then
    Res := '%';
  //
  Res := Res + Txt;
  //
  if Mascara in ([0, 1, 4]) then
    Res := Res + '%';
  //
  Result := Res;
end;

procedure TFmNomeX.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNomeX.FormCreate(Sender: TObject);
begin
  VAR_CORDA_CODIGOS     := '';
  ImgTipo.SQLType := stLok;
end;

end.
