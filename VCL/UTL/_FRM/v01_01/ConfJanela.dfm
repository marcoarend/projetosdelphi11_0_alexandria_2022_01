object FmConfJanela: TFmConfJanela
  Left = 409
  Top = 187
  Caption = 'XXX-XXXXX-999 :: Configura'#231#227'o de Janela'
  ClientHeight = 226
  ClientWidth = 520
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 520
    Height = 64
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label4: TLabel
      Left = 28
      Top = 8
      Width = 66
      Height = 13
      Caption = 'Configura'#231#227'o:'
    end
    object CBConfig: TDBLookupComboBox
      Left = 28
      Top = 24
      Width = 465
      Height = 21
      KeyField = 'Nome'
      ListField = 'Nome'
      ListSource = DsConfJanela
      TabOrder = 0
      OnClick = CBConfigClick
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 520
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 472
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 424
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 286
        Height = 32
        Caption = 'Configura'#231#227'o de Janela'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 286
        Height = 32
        Caption = 'Configura'#231#227'o de Janela'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 286
        Height = 32
        Caption = 'Configura'#231#227'o de Janela'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 112
    Width = 520
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 516
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 156
    Width = 520
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 384
      Top = 15
      Width = 134
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 4
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 382
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAplica: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Aplica'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAplicaClick
      end
      object BtSalva: TBitBtn
        Tag = 161
        Left = 134
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Sal&va'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtSalvaClick
      end
      object BtExclui: TBitBtn
        Tag = 15
        Left = 258
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtExcluiClick
      end
    end
  end
  object QrConfJanela: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrConfJanelaAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT Nome'
      'FROM confjanela'
      'WHERE Janela=:P0'
      'ORDER BY Janela')
    Left = 228
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConfJanelaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsConfJanela: TDataSource
    DataSet = QrConfJanela
    Left = 256
    Top = 64
  end
  object PMSalva: TPopupMenu
    OnPopup = PMSalvaPopup
    Left = 4
    Top = 12
    object Salvar1: TMenuItem
      Caption = '&Salvar'
      Enabled = False
      OnClick = Salvar1Click
    end
    object SalvarComo1: TMenuItem
      Caption = 'Salvar &Como...'
      OnClick = SalvarComo1Click
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome'
      'FROM confjanela'
      'WHERE Janela=:P0'
      'AND Nome=:P1')
    Left = 316
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrDados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM confjanela'
      'WHERE Janela=:P0'
      'AND Nome=:P1')
    Left = 288
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDadosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDadosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrDadosJanela: TWideStringField
      FieldName = 'Janela'
      Size = 30
    end
    object QrDadosCompo: TWideStringField
      FieldName = 'Compo'
      Size = 30
    end
    object QrDadosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDadosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDadosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDadosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDadosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDadosValTxt: TWideStringField
      FieldName = 'ValTxt'
      Size = 255
    end
    object QrDadosValInt: TIntegerField
      FieldName = 'ValInt'
    end
    object QrDadosValDat: TDateField
      FieldName = 'ValDat'
    end
    object QrDadosValDou: TFloatField
      FieldName = 'ValDou'
    end
  end
end
