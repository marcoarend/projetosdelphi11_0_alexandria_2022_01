object FmAgendaTarefa: TFmAgendaTarefa
  Left = 0
  Top = 0
  Caption = 'Agendar'
  ClientHeight = 130
  ClientWidth = 423
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label25: TLabel
    Left = 16
    Top = 16
    Width = 220
    Height = 13
    Caption = 'Usu'#225'rio administrador do sistema operacional:'
  end
  object Label26: TLabel
    Left = 16
    Top = 44
    Width = 214
    Height = 13
    Caption = 'Senha administrador do sistema operacional:'
  end
  object SbUserPwd: TSpeedButton
    Left = 388
    Top = 40
    Width = 23
    Height = 22
    OnMouseDown = SbUserPwdMouseDown
    OnMouseUp = SbUserPwdMouseUp
  end
  object EdSTUserName: TdmkEdit
    Left = 264
    Top = 12
    Width = 120
    Height = 21
    TabOrder = 0
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
    ValWarn = False
  end
  object EdSTUserPwd: TdmkEdit
    Left = 264
    Top = 40
    Width = 120
    Height = 21
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
    ValWarn = False
    PasswordChar = 'l'
  end
  object BtOK: TBitBtn
    Left = 152
    Top = 76
    Width = 120
    Height = 40
    Caption = 'Confirmar'
    TabOrder = 2
    OnClick = BtOKClick
  end
end
