object FmSel3RadioGroups: TFmSel3RadioGroups
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: T'#237'tulo'
  ClientHeight = 519
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 522
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 474
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 528
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 426
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 66
        Height = 32
        Caption = 'T'#237'tulo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 66
        Height = 32
        Caption = 'T'#237'tulo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 66
        Height = 32
        Caption = 'T'#237'tulo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 449
    Width = 624
    Height = 70
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 241
    ExplicitWidth = 522
    object PnSaiDesis: TPanel
      Left = 478
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 376
      object BtDesiste: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 476
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 374
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object PnRGs: TPanel
    Left = 0
    Top = 48
    Width = 624
    Height = 401
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    OnCanResize = PnRGsCanResize
    ExplicitLeft = 148
    ExplicitTop = 172
    ExplicitWidth = 185
    ExplicitHeight = 41
    object RGItens1: TRadioGroup
      Left = 0
      Top = 0
      Width = 624
      Height = 137
      Align = alTop
      Caption = ' Itens: '
      TabOrder = 0
      OnClick = RGItens1Click
      ExplicitTop = 48
      ExplicitWidth = 522
    end
    object RGItens2: TRadioGroup
      Left = 0
      Top = 137
      Width = 624
      Height = 132
      Align = alTop
      Caption = ' Itens: '
      TabOrder = 1
      OnClick = RGItens1Click
      ExplicitTop = 185
      ExplicitWidth = 522
    end
    object RGItens3: TRadioGroup
      Left = 0
      Top = 269
      Width = 624
      Height = 132
      Align = alClient
      Caption = ' Itens: '
      TabOrder = 2
      OnClick = RGItens1Click
      ExplicitLeft = 52
      ExplicitTop = 105
      ExplicitWidth = 522
    end
  end
end
