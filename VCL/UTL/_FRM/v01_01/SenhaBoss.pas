unit SenhaBoss;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  UnInternalConsts, UnInternalConsts2, ResIntStrings, dmkGeral,
  dmkEdit, dmkImage, UnDmkEnums;

type
  TFmSenhaBoss = class(TForm)
    Panel2: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    LaLogin: TLabel;
    EdLogin: TdmkEdit;
    LaSenha: TLabel;
    Label1: TLabel;
    Panel3: TPanel;
    Label4: TLabel;
    Label6: TLabel;
    EdEmpresa: TdmkEdit;
    EdCNPJ: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    EdSenha1: TEdit;
    EdSenha2: TEdit;
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdCNPJExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmSenhaBoss: TFmSenhaBoss;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmSenhaBoss.BtOKClick(Sender: TObject);
begin
  if Trim(EdSenha1.Text) = CO_VAZIO then
  begin
    Geral.MB_Aviso('Defina uma senha!');
    EdSenha1.SetFocus;
    Exit;
  end;
  if Trim(EdLogin.Text) = CO_VAZIO then
  begin
    Geral.MB_Aviso('Defina um Login!');
    EdLogin.SetFocus;
    Exit;
  end;
  if EdSenha1.Text <> EdSenha2.Text then
  begin
    Geral.MB_Aviso('Confirmação difere da senha!');
    EdSenha2.SetFocus;
    Exit;
  end;
  VAR_BOSS        := EdLogin.Text;
  VAR_SENHATXT    := EdSenha1.Text;
  VAR_NOMEEMPRESA := EdEmpresa.Text;
  VAR_CNPJEMPRESA := String(Geral.FormataCNPJ_TFT(AnsiString(EdCNPJ.Text)));
  Close;
end;

procedure TFmSenhaBoss.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSenhaBoss.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  VAR_TERMINATE := True;
end;

procedure TFmSenhaBoss.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  EdLogin.Text   := VAR_BOSS;
  EdSenha1.Text  := VAR_SENHATXT;
  EdEmpresa.Text := VAR_NOMEEMPRESA;
  EdCNPJ.Text    := VAR_CNPJEMPRESA;
end;

procedure TFmSenhaBoss.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSenhaBoss.EdCNPJExit(Sender: TObject);
var
  Num : AnsiString;
  CNPJ : AnsiString;
begin
  CNPJ := AnsiString(Geral.SoNumero_TT(EdCNPJ.Text));
  if CNPJ <> CO_VAZIO then
    begin
    Num := Geral.CalculaCNPJCPF(CNPJ);
    if Geral.FormataCNPJ_TFT(CNPJ) <> Num then
    begin
      Geral.MB_Aviso(SMLA_NUMEROINVALIDO2);
      EdCNPJ.SetFocus;
    end else EdCNPJ.Text := String(Geral.FormataCNPJ_TT(CNPJ));
  end else EdCNPJ.Text := CO_VAZIO;
end;

procedure TFmSenhaBoss.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
