object FmMenuGeral: TFmMenuGeral
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: Menu Principal'
  ClientHeight = 502
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Menu Principal'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 406
    Align = alClient
    TabOrder = 0
    object AdvToolBarPager1: TAdvToolBarPager
      Left = 1
      Top = 1
      Width = 1006
      Height = 149
      CaptionButtons = []
      TabGroups = <>
      TabSettings.EndMargin = 0
      Persistence.Location = plRegistry
      Persistence.Enabled = False
      PageLeftMargin = 0
      PageRightMargin = 0
      TabOrder = 0
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 454
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 2
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Fechar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
end
