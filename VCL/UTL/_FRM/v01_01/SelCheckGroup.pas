unit SelCheckGroup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmSelCheckGroup = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    CGItens: TdmkCheckGroup;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CGItensClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //FSelOnClick: Boolean;
    FAvisos: array of String;
    FAcoes: array of TSQLType;
    FValores: array of String; 
    FMyCods: array of Integer; 
  end;

  var
  FmSelCheckGroup: TFmSelCheckGroup;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmSelCheckGroup.BtNenhumClick(Sender: TObject);
begin
  CGItens.Value := 0;
end;

procedure TFmSelCheckGroup.BtOKClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSelCheckGroup.BtSaidaClick(Sender: TObject);
begin
  CGItens.ItemIndex := -1;
  Close;
end;

procedure TFmSelCheckGroup.BtTodosClick(Sender: TObject);
begin
  CGItens.Value := CGItens.MaxValue;
end;

procedure TFmSelCheckGroup.CGItensClick(Sender: TObject);
begin
(*
  if FSelOnClick then
    BtOKClick(Self)
  else
    BtOk.Enabled := CGItens.Value > -1;
*)
end;

procedure TFmSelCheckGroup.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmSelCheckGroup.FormCreate(Sender: TObject);
begin
  //FSelOnClick := False;
end;

procedure TFmSelCheckGroup.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [(*LaAviso1, LaAviso2*)], Caption, True, taCenter, 2, 10, 20);
end;

end.
