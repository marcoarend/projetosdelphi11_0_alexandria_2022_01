unit GerlShowGrid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditCB,
  dmkDBLookupComboBox, dmkEdit, mySQLDbTables, dmkDBGridZTO;

type
  TFmGerlShowGrid = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBGSel: TdmkDBGridZTO;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    MeAvisos: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGSelDblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure MeAvisosChange(Sender: TObject);
  private
    { Private declarations }
    procedure Selecionou();
    procedure SelecinarTodos(Selecionar: Boolean);
  public
    { Public declarations }
    FSelecionou: Boolean;
    FTitulo: String;
    //
    procedure Informa(Texto: String);
  end;

  var
  FmGerlShowGrid: TFmGerlShowGrid;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmGerlShowGrid.BtNenhumClick(Sender: TObject);
begin
  SelecinarTodos(False);
end;

procedure TFmGerlShowGrid.BtOKClick(Sender: TObject);
begin
  Selecionou();
end;

procedure TFmGerlShowGrid.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerlShowGrid.BtTodosClick(Sender: TObject);
begin
  SelecinarTodos(True);
end;

procedure TFmGerlShowGrid.DBGSelDblClick(Sender: TObject);
begin
  Selecionou();
end;

procedure TFmGerlShowGrid.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGerlShowGrid.FormCreate(Sender: TObject);
begin
  FTitulo := 'XXX-XXXXX-014 :: Mostra Dados em Grade';
  MeAvisos.Lines.Clear;
  ImgTipo.SQLType := stLok;
  FSelecionou := False;
end;

procedure TFmGerlShowGrid.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], FTitulo, True, taCenter, 2, 10, 20);
end;

procedure TFmGerlShowGrid.Informa(Texto: String);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
end;

procedure TFmGerlShowGrid.MeAvisosChange(Sender: TObject);
begin
  MeAvisos.Visible := MeAvisos.Text <> EmptyStr;
end;

procedure TFmGerlShowGrid.SelecinarTodos(Selecionar: Boolean);
begin
  if DBGSel.DataSource.DataSet.RecordCount > 0 then
  begin
    try
      Screen.Cursor     := crHourGlass;
      DBGSel.Enabled := False;
      //
      DBGSel.DataSource.DataSet.First;
      while not DBGSel.DataSource.DataSet.Eof do
      begin
        DBGSel.SelectedRows.CurrentRowSelected := Selecionar;
        DBGSel.DataSource.DataSet.Next;
      end;
    finally
      DBGSel.Enabled := True;
      Screen.Cursor  := crDefault;
    end;
  end;
end;

procedure TFmGerlShowGrid.Selecionou();
begin
  FSelecionou := True;
  Close;
end;

end.
