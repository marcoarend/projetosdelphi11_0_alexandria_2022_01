object FmPesqESel: TFmPesqESel
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-003 :: Pesquisa e Seleciona pela Descri'#231#227'o'
  ClientHeight = 542
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 1008
    Height = 450
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 85
        Height = 13
        Caption = 'Descri'#231#227'o parcial:'
      end
      object LaDoc: TLabel
        Left = 660
        Top = 4
        Width = 61
        Height = 13
        Caption = 'CNPJ / CPF:'
        Visible = False
      end
      object EdPesq: TdmkEdit
        Left = 8
        Top = 20
        Width = 648
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPesqChange
      end
      object EdDoc: TdmkEdit
        Left = 660
        Top = 20
        Width = 112
        Height = 21
        TabOrder = 1
        Visible = False
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPesqChange
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 53
      Width = 1008
      Height = 397
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object Panel5: TPanel
        Left = 496
        Top = 0
        Width = 512
        Height = 397
        Align = alRight
        TabOrder = 0
        object DBGSel: TdmkDBGridDAC
          Left = 1
          Top = 1
          Width = 510
          Height = 325
          SQLFieldsToChange.Strings = (
            'Ativo')
          SQLIndexesOnUpdate.Strings = (
            'Codigo')
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 384
              Visible = True
            end>
          Color = clWindow
          DataSource = DsSel
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          SQLTable = 'pesqesel_sel'
          EditForceNextYear = False
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 384
              Visible = True
            end>
        end
        object GroupBox1: TGroupBox
          Left = 1
          Top = 326
          Width = 510
          Height = 70
          Align = alBottom
          TabOrder = 1
          object Panel2: TPanel
            Left = 2
            Top = 15
            Width = 506
            Height = 53
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object BtTodosS: TBitBtn
              Tag = 127
              Left = 8
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Todos'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtTodosSClick
            end
            object BtNenhumS: TBitBtn
              Tag = 128
              Left = 130
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Nenhum'
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtNenhumSClick
            end
            object BitBtn1: TBitBtn
              Tag = 12
              Left = 252
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Retira'
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BitBtn1Click
            end
            object Panel6: TPanel
              Left = 376
              Top = 0
              Width = 130
              Height = 53
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 3
              object BtSaida: TBitBtn
                Tag = 13
                Left = 2
                Top = 3
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Hint = 'Sai da janela atual'
                Caption = '&Sa'#237'da'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaidaClick
              end
            end
          end
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 397
        Align = alClient
        TabOrder = 1
        object DBGPsq: TdmkDBGridDAC
          Left = 1
          Top = 1
          Width = 494
          Height = 325
          SQLFieldsToChange.Strings = (
            'Ativo')
          SQLIndexesOnUpdate.Strings = (
            'Codigo')
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 375
              Visible = True
            end>
          Color = clWindow
          DataSource = DsPsq
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          SQLTable = 'pesqesel_psq'
          EditForceNextYear = False
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 375
              Visible = True
            end>
        end
        object GBRodaPe: TGroupBox
          Left = 1
          Top = 326
          Width = 494
          Height = 70
          Align = alBottom
          TabOrder = 1
          object Panel9: TPanel
            Left = 2
            Top = 15
            Width = 490
            Height = 53
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object BtTodosP: TBitBtn
              Tag = 127
              Left = 4
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Todos'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtTodosPClick
            end
            object BtNenhumP: TBitBtn
              Tag = 128
              Left = 130
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Nenhum'
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtNenhumPClick
            end
            object BtOK: TBitBtn
              Tag = 14
              Left = 256
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Adiciona'
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BtOKClick
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 450
        Height = 32
        Caption = 'Pesquisa e Seleciona pela Descri'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 450
        Height = 32
        Caption = 'Pesquisa e Seleciona pela Descri'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 450
        Height = 32
        Caption = 'Pesquisa e Seleciona pela Descri'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrPsq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM pesqesel_psq')
    Left = 160
    Top = 124
    object QrPsqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPsqNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrPsqAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsPsq: TDataSource
    DataSet = QrPsq
    Left = 188
    Top = 124
  end
  object QrSel: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM pesqesel_sel')
    Left = 160
    Top = 152
    object QrSelCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSelNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSelAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsSel: TDataSource
    DataSet = QrSel
    Left = 188
    Top = 152
  end
end
