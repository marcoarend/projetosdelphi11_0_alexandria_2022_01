unit CalcMod10e11;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit, ComCtrls, DBCtrls,
  Db, mySQLDbTables, Grids, frxClass, dmkGeral, dmkDBLookupComboBox, dmkEditCB,
  UnDmkProcFunc, dmkImage, UnDmkEnums, UnInternalConsts;

type
  TFmCalcMod10e11 = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    RGCalculo: TRadioGroup;
    RGTipo: TRadioGroup;
    TabSheet2: TTabSheet;
    Label2: TLabel;
    QrBancos: TmySQLQuery;
    DsBancos: TDataSource;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    Panel3: TPanel;
    EdBanco: TdmkEditCB;
    Label4: TLabel;
    CBBanco: TdmkDBLookupComboBox;
    Label10: TLabel;
    dmkEdBloqueto: TdmkEdit;
    dmkEdCarteira: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    dmkEdIDCobranca: TdmkEdit;
    dmkEdCodCedente: TdmkEdit;
    Label13: TLabel;
    dmkEdAgencia: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    dmkEdDVAg: TdmkEdit;
    dmkEdPosto: TdmkEdit;
    Label7: TLabel;
    Label8: TLabel;
    dmkEdConta: TdmkEdit;
    dmkEdDVCta: TdmkEdit;
    Label9: TLabel;
    TPVencto: TDateTimePicker;
    Label16: TLabel;
    dmkEdValor: TdmkEdit;
    Label15: TLabel;
    Label14: TLabel;
    dmkEdOperCodi: TdmkEdit;
    Panel4: TPanel;
    Label3: TLabel;
    Edit3: TEdit;
    Label17: TLabel;
    Edit4: TEdit;
    Edit5: TEdit;
    Label18: TLabel;
    Grade: TStringGrid;
    BitBtn1: TBitBtn;
    dmkEdParcTit: TdmkEdit;
    LaParcTit: TLabel;
    Edit6: TEdit;
    SpeedButton1: TSpeedButton;
    Edit7: TEdit;
    frxCodBarras: TfrxReport;
    TabSheet3: TTabSheet;
    Panel5: TPanel;
    Panel6: TPanel;
    StaticText1: TStaticText;
    Memo1: TMemo;
    Splitter1: TSplitter;
    StaticText2: TStaticText;
    Memo2: TMemo;
    RGModalCobr: TRadioGroup;
    RGTipoCobranca: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel7: TPanel;
    BtOK: TBitBtn;
    Button1: TButton;
    Edit8: TEdit;
    CkZerado: TCheckBox;
    EdEspecieDoc: TdmkEdit;
    Label19: TLabel;
    RGCNAB: TRadioGroup;
    Label20: TLabel;
    EdCtaCooper: TdmkEdit;
    Label82: TLabel;
    EdLayoutRem: TdmkEdit;
    GroupBox3: TGroupBox;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    EdCorresBco: TdmkEditCB;
    EdCorresAge: TdmkEdit;
    EdCorresCto: TdmkEdit;
    dmkEdCartImp: TdmkEdit;
    Label24: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdBancoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure frxCodBarrasGetValue(const VarName: String; var Value: Variant);
    procedure Button1Click(Sender: TObject);
    procedure EdLayoutRemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCalcMod10e11: TFmCalcMod10e11;

implementation

uses UnMyObjects, UnBancos, Module, UnBco_Rem, UnGrl_Consts;

{$R *.DFM}

procedure TFmCalcMod10e11.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCalcMod10e11.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCalcMod10e11.BtOKClick(Sender: TObject);
var
  Conta, Carteira, CartImp, IDCobranca, CodCedente, DVAg, NossoNumero, DVCta,
  OperCodi, EspecieDoc, CtaCooper, LayoutRem, CorresCto, NossoNumero_Rem: String;
  Banco, Agencia, Posto, TipoCobranca, TipoCarteira, ModalCobr,
  ParcTit, CNAB, CorresBco, CorresAge: Integer;
  Bloqueto: Double;
  Vencto: TDateTime;
  Valor: Double;
begin
  case PageControl1.ActivePageIndex of
    0:
    begin
      case RGCalculo.ItemIndex of
        0: Edit2.Text := Geral.Modulo10_1e2_9_Back(Edit1.Text);
        1: Edit2.Text := Geral.Modulo10_2e1_X_Back(Edit1.Text, RGTipo.ItemIndex);
        2: Edit2.Text := Geral.Modulo11_2a9_Back(Edit1.Text, RGTipo.ItemIndex);
        3: Edit2.Text := Geral.Modulo11_2a7_Back(Edit1.Text, RGTipo.ItemIndex);
        4: Edit2.Text := Geral.ModuloEAN13(Edit1.Text);
      end;
    end;
    1:
    begin
      Banco        := Geral.IMV(EdBanco.Text);
      Agencia      := dmkEdAgencia.ValueVariant;
      CorresBco    := EdCorresBco.ValueVariant;
      CorresAge    := EdCorresAge.ValueVariant;
      DVAg         := dmkEdDVAg.ValueVariant;
      CorresCto    := EdCorresCto.ValueVariant;
      Posto        := dmkEdPosto.ValueVariant;
      Bloqueto     := dmkEdBloqueto.ValueVariant;
      Conta        := dmkEdConta.Text;
      DVCta        := dmkEdDVCta.ValueVariant;
      Carteira     := dmkEdCarteira.Text;
      CartImp      := dmkEdCartImp.Text;
      IDCobranca   := dmkEdIDCobranca.Text;
      CodCedente   := dmkEdCodCedente.Text;
      OperCodi     := dmkEdOperCodi.Text;
      TipoCobranca := RGTipoCobranca.ItemIndex;
      TipoCarteira := 0;
      Vencto       := TPVencto.Date;
      Valor        := dmkEdValor.ValueVariant;
      ModalCobr    := RGModalCobr.ItemIndex;
      ParcTit      := dmkEdParcTit.ValueVariant;
      EspecieDoc   := EdEspecieDoc.Text;
      CNAB         := Geral.IMV(RGCNAB.Items[RGCNAB.ItemIndex]);
      CtaCooper    := EdCtaCooper.Text;
      LayoutRem    := EdLayoutRem.ValueVariant;
      //CodCeden esta apenas com '11111'

      UBancos.GeraNossoNumero(ModalCobr, Banco, Agencia, Posto, Bloqueto,
        Conta, Carteira, IDCobranca, CodCedente, Vencto, TipoCobranca,
        EspecieDoc, CNAB, CtaCooper, LayoutRem, NossoNumero, NossoNumero_Rem);

      Edit5.Text := NossoNumero;
      Edit3.Text := UBancos.CodigoDeBarra_BoletoDeCobranca(Banco, Agencia,
                      CorresBco, CorresAge, DVAg, Posto, Conta, DVCta,
                      CorresCto, 9, TipoCobranca, TipoCarteira, NossoNumero,
                      CodCedente, Carteira, CartImp, IDCobranca, OperCodi, Vencto, Valor,
                      ParcTit, 0, not CkZerado.Checked, ModalCobr, LayoutRem);
      Edit4.Text := UBancos.LinhaDigitavel_BoletoDeCobranca(Edit3.Text);
    end;
    2:
    begin
      Memo2.Text := dmkPF.PWDExDecode(Memo1.Text, CO_RandStrWeb01);
    end;
  end;
end;

procedure TFmCalcMod10e11.EdBancoChange(Sender: TObject);
begin
  BitBtn1.Enabled := Geral.IMV(EdBanco.Text) > 0;
end;

procedure TFmCalcMod10e11.EdLayoutRemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  CNAB: Integer;
begin
  if Key = VK_F4 then
  begin
    case RGCNAB.ItemIndex of
        1: CNAB := 240;
        2: CNAB := 400;
      else CNAB := 0;
    end;
    if MyObjects.FIC(EdBanco.ValueVariant = 0, EdBanco, 'Banco n�o definido!') then Exit;
    if MyObjects.FIC(CNAB = 0, nil, 'CNAB n�o definido!') then Exit;
    //
    UBco_Rem.LayoutsRemessa(EdBanco.ValueVariant, CNAB);
  end;
end;

procedure TFmCalcMod10e11.FormCreate(Sender: TObject);
var
  Banco: Integer;
  t, Data: String;
begin
  TPVencto.Date := Date + 30;
  Data := Geral.FDT(Date, 2);
  QrBancos.Open;
  (*Grade.Cells[00,00] := 'Seq.';
  Grade.Cells[01,00] := 'Banco';
  Grade.Cells[02,00] := 'Agen.';
  Grade.Cells[03,00] := 'DV';*)
  t := Application.Title + '\CalcMod10e11';
  Banco := Geral.ReadAppKey('Banco', t, ktInteger, 0, HKEY_LOCAL_MACHINE);
  EdBanco.Text := IntToStr(Banco);
  CBBanco.KeyValue := Banco;
  //
  dmkEdAgencia.ValueVariant :=
    Geral.ReadAppKey('Agenc', t, ktInteger, 0, HKEY_LOCAL_MACHINE);
  //
  dmkEdDVAg.ValueVariant :=
    Geral.ReadAppKey('AgeDV', t, ktInteger, 0, HKEY_LOCAL_MACHINE);
  //
  dmkEdPosto.ValueVariant :=
    Geral.ReadAppKey('Posto', t, ktInteger, 0, HKEY_LOCAL_MACHINE);
  //
  dmkEdConta.ValueVariant :=
    Geral.ReadAppKey('Conta', t, ktString, '', HKEY_LOCAL_MACHINE);
  //
  dmkEdDVCta.ValueVariant :=
    Geral.ReadAppKey('ConDV', t, ktInteger, 0, HKEY_LOCAL_MACHINE);
  //
  dmkEdBloqueto.Text :=
    Geral.ReadAppKey('Bloqu', t, ktString , '', HKEY_LOCAL_MACHINE);
  //
  dmkEdCarteira.ValueVariant :=
    Geral.ReadAppKey('Carte', t, ktString, '', HKEY_LOCAL_MACHINE);
  //
  dmkEdIDCobranca.ValueVariant :=
    Geral.ReadAppKey('IDCon', t, ktString, '', HKEY_LOCAL_MACHINE);
  //
  dmkEdCodCedente.ValueVariant :=
    Geral.ReadAppKey('Ceden', t, ktString, '', HKEY_LOCAL_MACHINE);
  //
  dmkEdOperCodi.ValueVariant :=
    Geral.ReadAppKey('OpeCo', t, ktString, '', HKEY_LOCAL_MACHINE);
  //
  dmkEdValor.ValueVariant :=
    (Geral.ReadAppKey('Valor', t, ktInteger, 0, HKEY_LOCAL_MACHINE)) / 100;
  //
  TPVencto.Date := Geral.ValidaDataSimples(
    Geral.ReadAppKey('Vecto', t, ktString, Data, HKEY_LOCAL_MACHINE), True);
  //
  dmkEdParcTit.ValueVariant :=
    Geral.ReadAppKey('ParcT', t, ktInteger, 0, HKEY_LOCAL_MACHINE);
  //
  RGModalCobr.ItemIndex :=
    Geral.ReadAppKey('ModCo', t, ktInteger, 0, HKEY_LOCAL_MACHINE);
  //
  RGTipoCobranca.ItemIndex :=
    Geral.ReadAppKey('TpCob', t, ktInteger, 0, HKEY_LOCAL_MACHINE);
  //
  EdEspecieDoc.Text :=
    Geral.ReadAppKey('EsDoc', t, ktString, '', HKEY_LOCAL_MACHINE);
  //
  MyObjects.ConfiguraTipoCobranca(RGTipoCobranca, 2, 0);
end;

procedure TFmCalcMod10e11.BtSaidaClick(Sender: TObject);
var
  Banco, Agenc, Posto, AgeDV, ConDV, ModCo, TpCob: Integer;
  Bloqu, Vecto, t, Conta, Carte, IDCon, Ceden, OpeCo, ParcT, EsDoc: String;
  Valor: Double;
begin
  t := Application.Title + '\CalcMod10e11';
  Banco := Geral.IMV(EdBanco.Text);
  Geral.WriteAppKey('Banco', t, Banco, ktInteger, HKEY_LOCAL_MACHINE);
  //
  Agenc := Geral.IMV(dmkEdAgencia.ValueVariant);
  Geral.WriteAppKey('Agenc', t, Agenc, ktInteger, HKEY_LOCAL_MACHINE);
  //
  AgeDV := Geral.IMV(dmkEdDVAg.ValueVariant);
  Geral.WriteAppKey('AgeDV', t, AgeDV, ktInteger, HKEY_LOCAL_MACHINE);
  //
  Posto := Geral.IMV(dmkEdPosto.ValueVariant);
  Geral.WriteAppKey('Posto', t, Posto, ktInteger, HKEY_LOCAL_MACHINE);
  //
  Conta := dmkEdConta.ValueVariant;
  Geral.WriteAppKey('Conta', t, Conta, ktString, HKEY_LOCAL_MACHINE);
  //
  ConDV := Geral.IMV(dmkEdDVCta.ValueVariant);
  Geral.WriteAppKey('ConDV', t, ConDV, ktInteger, HKEY_LOCAL_MACHINE);
  //
  Bloqu := dmkEdBloqueto.Text;
  Geral.WriteAppKey('Bloqu', t, Bloqu, ktString, HKEY_LOCAL_MACHINE);
  //
  Carte := dmkEdCarteira.ValueVariant;
  Geral.WriteAppKey('Carte', t, Carte, ktString, HKEY_LOCAL_MACHINE);
  //
  IDCon := dmkEdIDCobranca.ValueVariant;
  Geral.WriteAppKey('IDCon', t, IDCon, ktString, HKEY_LOCAL_MACHINE);
  //
  Ceden := dmkEdCodCedente.ValueVariant;
  Geral.WriteAppKey('Ceden', t, Ceden, ktString, HKEY_LOCAL_MACHINE);
  //
  OpeCo := dmkEdOperCodi.ValueVariant;
  Geral.WriteAppKey('OpeCo', t, OpeCo, ktString, HKEY_LOCAL_MACHINE);
  //
  Valor := dmkEdValor.ValueVariant * 100;
  Geral.WriteAppKey('Valor', t, Valor, ktInteger, HKEY_LOCAL_MACHINE);
  //
  Vecto := Geral.FDT(Int(TPVencto.Date), 2);
  Geral.WriteAppKey('Vecto', t, Vecto, ktString, HKEY_LOCAL_MACHINE);
  //
  ParcT := dmkEdParcTit.ValueVariant;
  Geral.WriteAppKey('ParcT', t, ParcT, ktInteger, HKEY_LOCAL_MACHINE);
  //
  ModCo := RGModalCobr.ItemIndex;
  Geral.WriteAppKey('ModCo', t, ModCo, ktInteger, HKEY_LOCAL_MACHINE);
  //
  TpCob := RGTipoCobranca.ItemIndex;
  Geral.WriteAppKey('TpCob', t, TpCob, ktInteger, HKEY_LOCAL_MACHINE);
  //
  EsDoc := EdEspecieDoc.Text;
  Geral.WriteAppKey('EsDoc', t, EsDoc, ktString, HKEY_LOCAL_MACHINE);
  //


  //


  Close;
end;

procedure TFmCalcMod10e11.BitBtn1Click(Sender: TObject);
var
  Bloqueto: Double;
begin
  UBancos.GeraBloquetoNumero(Geral.IMV(EdBanco.Text),
    dmkEdBloqueto.ValueVariant, True,
    False, 0, 0, // Vaer o que e se fazer - Ita�
    Bloqueto, EdLayoutRem.ValueVariant);
  dmkEdBloqueto.Text := FormatFloat('0', Bloqueto);
end;

procedure TFmCalcMod10e11.SpeedButton1Click(Sender: TObject);
begin
  Edit7.Text := Format('%x', [Geral.IMV(Edit6.Text)]);
end;

procedure TFmCalcMod10e11.frxCodBarrasGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_CODIGOBARRAS') = 0 then
    Value := Edit3.Text;
end;

procedure TFmCalcMod10e11.Button1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxCodBarras, 'C�digo de barras');
end;

end.
