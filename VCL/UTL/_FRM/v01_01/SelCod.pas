unit SelCod;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkGeral, dmkImage, Variants, UnDmkEnums;

type
  TFmSelCod = class(TForm)
    PainelDados: TPanel;
    LaPrompt: TLabel;
    CBSel: TdmkDBLookupComboBox;
    DsSel: TDataSource;
    EdSel: TdmkEditCB;
    LaOrdem: TLabel;
    QrSel: TmySQLQuery;
    QrSelDescricao: TWideStringField;
    QrSelCodigo: TIntegerField;
    dmkEdOrdem: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    SBCadastro: TSpeedButton;
    CkContinua: TCheckBox;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure SBCadastroClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FSelected, FContinua: Boolean;
    FIDThis, FTabela: String;
    FNovoCodigo: TNovoCodigo;
    FPermiteSelZero: Boolean;
    //
    procedure Informa(Texto: String);
  end;

var
  FmSelCod: TFmSelCod;

implementation

uses {$IfNDef SemCfgCadLista} CfgCadLista,{$EndIf}
  UnMyObjects, Module;

{$R *.DFM}

procedure TFmSelCod.BtOKClick(Sender: TObject);
begin
  VAR_SELCOD := Geral.IMV(EdSel.Text);
  VAR_SELNOM := CBSel.Text;
  if (VAR_SELCOD = 0) and (FPermiteSelZero = False) then
  begin
    VAR_SELNOM := '';
    Geral.MB_Erro('Nenhuma escolha foi realizada!');
    Exit;
  end;
  FSelected  := True;
  FContinua  := CkContinua.Checked;
  Close;
end;

procedure TFmSelCod.BtSaidaClick(Sender: TObject);
begin
  VAR_SELCOD := 0;
  Close;
end;

procedure TFmSelCod.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSelCod.Informa(Texto: String);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
end;

procedure TFmSelCod.SBCadastroClick(Sender: TObject);
begin
{$IfNDef SemCfgCadLista}
  VAR_CADASTRO := 0;
  //
  if FTabela = EmptyStr then
  begin
    Geral.MB_Erro('Tabela n�o definida para cadastro! ' + sLineBreak +
    'SBCadastroClick > UnCfgCadLista.MostraCadLista()');
    Exit;
  end;
  if FNovoCodigo = TNovoCodigo.ncIdefinido then
  begin
    Geral.MB_Erro('Tipo de obten��o de novo c�digo indefinido!' + sLineBreak +
    'SBCadastroClick > UnCfgCadLista.MostraCadLista()');
    Exit;
  end;
  //
  UnCfgCadLista.MostraCadLista(QrSel.Database, FTabela, QrSelDescricao.Size,
    FNovoCodigo, 'Cadastro de "' +
    Geral.Substitui(LaPrompt.Caption, ':', #0) + '"',
    [], False, Null, [], [], False, EdSel.ValueVariant);
  //
  UMyMod.SetaCodigoPesquisado(EdSel, CBSel, QrSel, VAR_CADASTRO);
  EdSel.SetFocus;
{$EndIf}
end;

procedure TFmSelCod.FormActivate(Sender: TObject);
var
  IDForm: String;
begin
  MyObjects.CorIniComponente();
  IDForm := Copy(Caption, 1, 13);
  if Uppercase(IDForm) <> FIDThis then
  begin
    Caption := FIDThis + ' :: ' + Caption;
    MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  end;
end;

procedure TFmSelCod.FormCreate(Sender: TObject);
begin
  FIDThis := 'XXX-XXXXX-002';
  FPermiteSelZero := False;
  ImgTipo.SQLType := stPsq;
  //
  FSelected := False;
  FContinua := False;
  VAR_SELCOD := 0;
end;

end.
