unit PesqPrdTamCor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, mySQLDbTables,
  dmkGeral, dmkEdit, dmkEditF7, DBCtrls, (*DBTables,*) dmkDBLookupComboBox,
  dmkDBGrid, dmkImage, DmkDAC_PF, UnDmkEnums, dmkDBGridZTO;

type
  TFmPesqPrdTamCor = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    LaPrd: TLabel;
    EdPrd: TdmkEdit;
    EdTam: TdmkEdit;
    LaTam: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    EdAntSelCod: TdmkEdit;
    EdAntSelNom: TdmkEdit;
    Label2: TLabel;
    EdTxtPesq: TdmkEdit;
    EdCor: TdmkEdit;
    LaCor: TLabel;
    DBGPesquisa: TdmkDBGridZTO;
    QrPesqReduzido: TIntegerField;
    QrPesqNivel1: TIntegerField;
    QrPesqNome: TWideStringField;
    QrPesqTAM: TWideStringField;
    QrPesqCOR: TWideStringField;
    QrPesqReferencia: TWideStringField;
    QrPesqPatrimonio: TWideStringField;
    QrPesqEAN13: TWideStringField;
    QrPesqNCM: TWideStringField;
    QrPesqUnidMed: TWideStringField;
    QrPesqTipoProd: TWideStringField;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPrdChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGListaDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdTxtPesqDblClick(Sender: TObject);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure EdTxtPesqEnter(Sender: TObject);
    procedure DBGPesquisaDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure VerificaEan13();
  public
    { Public declarations }
    FControle: TControl;
    FCB: TDBLookupCombobox;
    FF7: TdmkEdit;
    FLS: TDataSource;
    FQr: TmySQLQuery;
    FDB: TmySQLDatabase;
    FSQLBase, FNomeTabela, FNomeCampoNome, FNomeCampoCodigo: String;
(*
    FND: Boolean;
    FNomeCampoDoc, FTexto,
    FDefinedSQL, FSQLJoke_SQL, FSQLJoke_Jok: String;
    FAtivou: Boolean;
    FDS: TDataSource;
*)
  end;

  var
  FmPesqPrdTamCor: TFmPesqPrdTamCor;

implementation

uses UnMyObjects, UnInternalConsts, Module;

{$R *.DFM}

procedure TFmPesqPrdTamCor.BtOKClick(Sender: TObject);
begin
  if QrPesq.State <> dsInactive then
  begin
    VAR_CADASTRO  := QrPesq.Fields[0].AsInteger;
    VAR_CAD_NOME  := QrPesq.Fields[1].AsString;
    VAR_CADASTRO2 := QrPesq.Fields[2].AsInteger;
    //
    VAR_ANT_SEL_CADASTRO := VAR_CADASTRO;
    VAR_ANT_SEL_CAD_NOME := VAR_CAD_NOME;
    //
    Close;
  end;
end;

procedure TFmPesqPrdTamCor.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqPrdTamCor.DBGListaDblClick(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmPesqPrdTamCor.DBGPesquisaDblClick(Sender: TObject);
begin
  if FNomeCampoCodigo <> EmptyStr then
  begin
    try
      VAR_CADASTRO := QrPesq.FieldByName(FNomeCampoCodigo).AsInteger;
    except
      VAR_CADASTRO := QrPesqReduzido.Value;
    end;
  end else
    VAR_CADASTRO := QrPesqReduzido.Value;
  //
  VAR_CODIGO   := QrPesqReduzido.Value;
  VAR_CODIGO2  := QrPesqNivel1.Value;
  //
  Close;
end;

procedure TFmPesqPrdTamCor.EdPrdChange(Sender: TObject);
var
  sPrd, sTam, sCor, SQL_Text, Concatenados: String;
  //_OR: String;
begin
  //_OR := EmptyStr;
  QrPesq.Close;
  if FDB <> nil then
    QrPesq.Database := FDB;
  //
  sPrd := EdPrd.ValueVariant;
  sTam := EdTam.ValueVariant;
  sCor := EdCor.ValueVariant;
  //
  if sPrd <> '' then
    sPrd := StringReplace(sPrd, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
  //
  Concatenados := sPrd + sTam + sCor;
  if (Length(Concatenados) >= 2) then
  begin
    SQL_Text := '';
    //
    if (FNomeTabela <> EmptyStr) and (FNomeCampoNome <> EmptyStr) then
    begin
      SQL_Text := Geral.ATS([
      'SELECT ggx.Controle Reduzido, ggx.GraGru1 Nivel1, gg1.Nome, ',
      'gti.Nome TAM, gcc.Nome COR, gg1.Referencia, gg1.Patrimonio,  ',
      'ggx.EAN13, gg1.NCM, med.Sigla UnidMed, pgt.Nome TipoProd ',
      'FROM ' + Lowercase(FNomeTabela) + ' its ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=its.' + Lowercase(FNomeCampoNome),
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
      'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo  ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip  ',
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed  ',
      'WHERE ggx.Controle<>0 ',
      'AND ggx.Ativo=1',
      FSQLBase,
      '']);
    end else
    if FSQLBase <> EmptyStr then
    begin
      SQL_Text := FSQLBase
    end else
    begin
      SQL_Text := Geral.ATS([
      'SELECT ggx.Controle Reduzido, ggx.GraGru1 Nivel1, gg1.Nome, ',
      'gti.Nome TAM, gcc.Nome COR, gg1.Referencia, gg1.Patrimonio,  ',
      'ggx.EAN13, gg1.NCM, med.Sigla UnidMed, pgt.Nome TipoProd ',
      'FROM gragrux ggx  ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip  ',
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed  ',
      'WHERE ggx.Controle<>0 ',
      'AND ggx.Ativo=1',
      '']);
    end;
    //
    if sPrd <> EmptyStr then
    begin
      SQL_Text := SQL_Text + Geral.ATS([
      'AND ( ',
      '  gg1.Nome LIKE "%' + sPrd + '%" ',
      '  OR gg1.Referencia LIKE "%' + sPrd + '%" ',
      '  OR gg1.Patrimonio LIKE "%' + sPrd + '%" ',
      ') ']);
      //_OR := ' OR';
    end;
    //
    if sTam <> EmptyStr then
    begin
      SQL_Text := SQL_Text + Geral.ATS([
      'AND ( ',
      '  gti.Nome = "' + sTam + '" ',
      ') ']);
      //_OR := ' OR';
    end;
    //
    if sCor <> EmptyStr then
    begin
      SQL_Text := SQL_Text + Geral.ATS([
      'AND ( ',
      '  gcc.Nome LIKE "%' + sCor + '%" ',
      ') ']);
      //_OR := ' OR';
    end;
    //
    QrPesq.SQL.Text := SQL_Text;
    //
    try
      // Usa nos aplicativos DermaXX
      //UnDmkDAC_PF.AbreQuery(QrPesq, 'TFmPesqNome.EdPesqChange()');
      UnDmkDAC_PF.AbreQueryApenas(QrPesq);
      //Geral.MB_SQL(Self, QrPesq);
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrPesq, '', nil, True, True);
    end;
  end;
end;

procedure TFmPesqPrdTamCor.EdTxtPesqDblClick(Sender: TObject);
begin
  EdPrd.Text := '%' + EdTxtPesq.Text + '%';
end;

procedure TFmPesqPrdTamCor.EdTxtPesqEnter(Sender: TObject);
begin
  VerificaEAN13();
end;

procedure TFmPesqPrdTamCor.FormActivate(Sender: TObject);
var
  Query: TmySQLQuery;
  Texto: String;
  P, P10, P13: Integer;
  DS: TDataSource;
begin
{
  MyObjects.CorIniComponente();
  if FCB <> nil then
  begin
    DS := FCB.ListSource;
    if DS <> nil then
    begin
      Query := TmySQLQuery(DS.DataSet);
      if Query <> nil then
      begin
        Texto := Lowercase(Query.SQL.Text);
        P := pos('from', Texto);
        if P > 0 then
        begin
          Texto := Trim(Copy(Texto, P + 4));
          P := pos(' ', Texto);
          P10 := pos(#10, Texto);
          P13 := pos(#13, Texto);
          if (P13 < P) and (P13 > 0) then P := P13;
          if (P10 < P) and (P10 > 0) then P := P10;
          if P > 0 then
          begin
            FNomeTabela := Trim(Copy(Texto, 1, P));
            FNomeCampoNome   := FCB.ListField;
            FNomeCampoCodigo := FCB.KeyField;
            //
            if FCB is TdmkDBLookupComboBox then
            begin
              if TdmkDBLookupComboBox(FCB).LocF7TableName <> '' then
                FNometabela := TdmkDBLookupComboBox(FCB).LocF7TableName;
              if TdmkDBLookupComboBox(FCB).LocF7CodiFldName <> '' then
                FNomeCampoCodigo := TdmkDBLookupComboBox(FCB).LocF7CodiFldName;
              if TdmkDBLookupComboBox(FCB).LocF7NameFldName <> '' then
                FNomeCampoNome := TdmkDBLookupComboBox(FCB).LocF7NameFldName;
              if TdmkDBLookupComboBox(FCB).LocF7SQLText.Text <> '' then
              begin
                FNomeTabela := '';
              end;
            end;
          end;
        end;
      end;
    end;
  end
  else
  if FF7 is TdmkEditF7 then
  begin
    if TdmkEditF7(FF7).LocF7TableName <> '' then
      FNometabela := TdmkEditF7(FF7).LocF7TableName;

    if TdmkEditF7(FF7).LocF7CodiFldName <> '' then
      FNomeCampoCodigo := TdmkEditF7(FF7).LocF7CodiFldName;

    if TdmkEditF7(FF7).LocF7NameFldName <> '' then
      FNomeCampoNome := TdmkEditF7(FF7).LocF7NameFldName;
  end else
  if FND then
  begin
    //
  end;

  //

  if FNomeTabela = 'cunscad' then
    FNomeTabela := 'entidades';
  //

  if FNomeTabela = 'entidades' then
  begin
    LaDoc.Visible := True;
    EdDoc.Visible := True;
    EdPesq.Width  := 648;
  end;
  //
  if not FAtivou then
  begin
    FAtivou := True;
    EdPesq.Text := FTexto;
  end;
}
end;

procedure TFmPesqPrdTamCor.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  QrPesq.Database := Dmod.MyDB;
(*
  FND := False;
  FAtivou := False;
  FTexto := '';
  FDefinedSQL := '';
  FSQLJoke_SQL := '';
  FSQLJoke_Jok := '';
*)
  EdAntSelCod.ValueVariant := VAR_ANT_SEL_CADASTRO;
  EdAntSelNom.ValueVariant := VAR_ANT_SEL_CAD_NOME;
end;

procedure TFmPesqPrdTamCor.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesqPrdTamCor.QrPesqAfterOpen(DataSet: TDataSet);
begin
  VerificaEan13();
end;

procedure TFmPesqPrdTamCor.VerificaEan13;
begin
  // EAN13
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount = 1)
  and (Length(EdPrd.Text) >= 8) and EdTxtPesq.Focused then
  begin
    BtOKClick(Self);
  end;
end;

end.

