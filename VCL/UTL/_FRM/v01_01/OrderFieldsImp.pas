unit OrderFieldsImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums;

type
  TFmOrderFieldsImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    StaticText6: TStaticText;
    LBFieldsOrdTab: TListBox;
    LBFieldsOrdTxt: TListBox;
    Panel6: TPanel;
    BtDown: TBitBtn;
    BtUp: TBitBtn;
    Panel7: TPanel;
    StaticText1: TStaticText;
    LBFieldsDisTab: TListBox;
    LBFieldsDisTxt: TListBox;
    Panel8: TPanel;
    BtLeft: TBitBtn;
    BtRight: TBitBtn;
    BtDesfazOrdenacao: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtUpClick(Sender: TObject);
    procedure BtDownClick(Sender: TObject);
    procedure BtRightClick(Sender: TObject);
    procedure BtLeftClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtDesfazOrdenacaoClick(Sender: TObject);
    procedure LBFieldsDisTxtClick(Sender: TObject);
    procedure LBFieldsOrdTxtClick(Sender: TObject);
  private
    { Private declarations }
    procedure MoveItensListBox(ListBox: TListBox; Cima: Boolean);
    procedure MoveLeftRightListBox(Origem1, Origem2, Destino1, Destino2: TListBox);
  public
    { Public declarations }
    FOrdFld_Def, FOrdTxt_Def, FOrdFld_Sel, FOrdTxt_Sel, FSeparador: String;
  end;

  var
    FmOrderFieldsImp: TFmOrderFieldsImp;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmOrderFieldsImp.BtDesfazOrdenacaoClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Deseja escluir esta configura��o e restaurar a ordena��o padr�o?') = ID_YES then
  begin
    FOrdFld_Sel := FOrdFld_Def;
    FOrdTxt_Sel := FOrdTxt_Def;
    //
    Close;
  end;
end;

procedure TFmOrderFieldsImp.BtDownClick(Sender: TObject);
begin
  MoveItensListBox(LBFieldsOrdTxt, False);
  MoveItensListBox(LBFieldsOrdTab, False);
end;

procedure TFmOrderFieldsImp.MoveLeftRightListBox(Origem1, Origem2, Destino1,
  Destino2: TListBox);
var
  Selected, Modo: Integer;
  CompFld, CompTxt: String;
begin
  Selected := Origem1.ItemIndex;
  //
  if Selected > -1 then
  begin
    if Origem1.Name = 'LBFieldsDisTxt' then
    begin
      Modo := MyObjects.SelRadioGroup('Selecione o tipo de ordena��o', '', ['Crescente', 'Decrescente'], 2);
      //
      case Modo of
          0:
          begin
            CompFld := ' ASC';
            CompTxt := ' (Crescente)';
          end;
          1:
          begin
            CompFld := ' DESC';
            CompTxt := ' (Decrescente)';
          end
        else Exit;
      end;
    end;
    //
    if Origem1.Name = 'LBFieldsDisTxt' then
      Destino1.Items.Add(Origem1.Items[Selected] + CompTxt)
    else
    begin
      CompTxt := Origem1.Items[Selected];
      CompTxt := StringReplace(CompTxt, '(Decrescente)', '', [rfReplaceAll, rfIgnoreCase]);
      CompTxt := StringReplace(CompTxt, '(Crescente)', '', [rfReplaceAll, rfIgnoreCase]);
      CompTxt := Trim(CompTxt);
      //
      Destino1.Items.Add(CompTxt);
    end;
    //
    Origem1.DeleteSelected;
    //
    if Origem2.Name = 'LBFieldsDisTab' then
      Destino2.Items.Add(Origem2.Items[Selected] + CompFld)
    else
    begin
      CompFld := Origem2.Items[Selected];
      CompFld := StringReplace(CompFld, 'DESC', '', [rfReplaceAll, rfIgnoreCase]);
      CompFld := StringReplace(CompFld, 'ASC', '', [rfReplaceAll, rfIgnoreCase]);
      CompFld := Trim(CompFld);
      //
      Destino2.Items.Add(CompFld);
    end;
    //
    Origem2.Selected[Selected];
    Origem2.DeleteSelected;
  end;
end;

procedure TFmOrderFieldsImp.BtLeftClick(Sender: TObject);
begin
  MoveLeftRightListBox(LBFieldsOrdTxt, LBFieldsOrdTab, LBFieldsDisTxt,
    LBFieldsDisTab);
end;

procedure TFmOrderFieldsImp.BtOKClick(Sender: TObject);
  function InsereTextoOrder(Lista: TListBox; Separ: String): String;
  var
    i: Integer;
    Txt: String;
  begin
    Txt := '';
    //
    for i := 0 to Lista.Count - 1 do
    begin
      if i <> Lista.Count - 1 then
        Txt := Txt + Lista.Items[i] + Separ
      else
        Txt := Txt + Lista.Items[i];
    end;
    Result := Txt;
  end;
var
  Descri, Fields: String;
begin
  if MyObjects.FIC(FSeparador = '', nil, 'Falha ao salvar! Separador n�o definido!') then Exit;
  if MyObjects.FIC(LBFieldsOrdTxt.Items.Count = 0, nil, 'Voc� deve selecionar pelo menos um campo!') then Exit;
  if MyObjects.FIC(LBFieldsOrdTxt.Items.Count > 5, nil, 'Voc� deve selecionar no m�ximo 5 campos!') then Exit;
  //
  Descri := InsereTextoOrder(LBFieldsOrdTxt, FSeparador);
  Fields := InsereTextoOrder(LBFieldsOrdTab, FSeparador);
  //
  if (Descri <> '') and (Fields <> '') then
  begin
    FOrdTxt_Sel := Descri;
    FOrdFld_Sel := Fields;
    //
    Close;
  end;
end;

procedure TFmOrderFieldsImp.BtRightClick(Sender: TObject);
begin
  MoveLeftRightListBox(LBFieldsDisTxt, LBFieldsDisTab, LBFieldsOrdTxt,
    LBFieldsOrdTab);
end;

procedure TFmOrderFieldsImp.BtSaidaClick(Sender: TObject);
begin
  FOrdTxt_Sel := '';
  FOrdFld_Sel := '';
  //
  Close;
end;

procedure TFmOrderFieldsImp.MoveItensListBox(ListBox: TListBox; Cima: Boolean);
var
  NovoIndex: Integer;
  Executa: Boolean;
begin
  if Cima then
    Executa := ListBox.ItemIndex > 0
  else
    Executa := ListBox.ItemIndex < ListBox.Items.Count - 1;
  if Executa then
  begin
    if Cima then
      NovoIndex := ListBox.ItemIndex - 1
    else
      NovoIndex := ListBox.ItemIndex + 1;
    ListBox.Items.Move(ListBox.ItemIndex, NovoIndex);
    ListBox.ItemIndex := NovoIndex;
    if ListBox.Visible = True then
      ListBox.SetFocus;
  end;
end;

procedure TFmOrderFieldsImp.BtUpClick(Sender: TObject);
begin
  MoveItensListBox(LBFieldsOrdTxt, True);
  MoveItensListBox(LBFieldsOrdTab, True);
end;

procedure TFmOrderFieldsImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOrderFieldsImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOrderFieldsImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOrderFieldsImp.LBFieldsDisTxtClick(Sender: TObject);
begin
  LBFieldsDisTab.ItemIndex := LBFieldsDisTxt.ItemIndex;
end;

procedure TFmOrderFieldsImp.LBFieldsOrdTxtClick(Sender: TObject);
begin
  LBFieldsOrdTab.ItemIndex := LBFieldsOrdTxt.ItemIndex;
end;

end.
