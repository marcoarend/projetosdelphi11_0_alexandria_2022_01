unit UnTreeView;

interface

uses dmkGeral, dmkImage, Forms, Controls, Windows, SysUtils, ComCtrls, Classes,
  UnDmkProcFunc, ExtCtrls, UnInternalConsts, mySQLDbTables, Dialogs, UnDmkEnums,
  Variants, DB, DmkDAC_PF;

type
  TUnTreeView = class(TObject)
  private
  public
    function  ReordenaNodes(TreeView: TTreeView; Expand: Boolean): Boolean;
    function  CriaNode(Node: TTreeNode; Nome: String; Id: Integer;
                TreeView: TTreeView; ImgIndex: Integer = -1;
                Child: Boolean = False): TTreeNode;
    function  ExcluiNode(TreeView: TTreeView; Msg: String): Boolean;
    procedure EditaNode(TreeView: TTreeView; Node: TTreeNode; Nome: String;
                Id: Integer; ImgIndex: Integer = -1);
    // B A N C O   D E   D A D O S
    procedure CarregaDBItensTreeView(Query: TmySQLQuery; ColId, ColNome,
                ColNivel: String; TreeView: TTreeView; Expand: Boolean);
    procedure AtualizaDBNodesOrdem(DataBase: TmySQLDatabase;
                Query: TmySQLQuery; TreeView: TTreeView; Tabela, ColNivel,
                ColOrdem, ColId, ColParentId: String);
    // O U T R O S
    function  TreeView_VerificaCaracteresEspeciais(const NewDir,
                Separador: String; var Msg: String): Boolean;
    function  TreeView_VerificaSeDiretorioExiste(NewCaminhoDir, CampoNome,
                Tabela, SQLCompl: String; QueryLoc: TmySQLQuery;
                DataBase: TmySQLDatabase; var Msg: String): Boolean;
    procedure TreeView_CarregaItems(TreeView: TTreeView; QueryItens: TmySQLQuery;
                CampoNome, CampoCodigo, Separador: String);
    procedure LocalizaNodeByStateIndex(TreeView: TTreeView; StateIndex: Integer);
    function  LocalizaNodeInArrayNode(Nome: String; StateIndex, Level: Integer;
                TreeView: TTreeView): TTreeNode;
  end;

var
  UTreeView: TUnTreeView;

implementation

uses UMySQLModule, UnMyObjects, MyDBCheck, MyTreeViewReorder;

{ TUnTreeView }

function TUnTreeView.TreeView_VerificaCaracteresEspeciais(const NewDir,
  Separador: String; var Msg: String): Boolean;
begin
  if Pos(Separador, NewDir) > 0 then
  begin
    Msg    := 'O nome do diret�rio n�o pode conter "."!';
    Result := False;
  end else
  begin
    Msg    := '';
    Result := True;
  end;
end;

function TUnTreeView.TreeView_VerificaSeDiretorioExiste(NewCaminhoDir,
  CampoNome, Tabela, SQLCompl: String; QueryLoc: TmySQLQuery;
  DataBase: TmySQLDatabase; var Msg: String): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, DataBase, [
    'SELECT ' + CampoNome,
    'FROM ' + Tabela,
    'WHERE ' + CampoNome + ' = "' + NewCaminhoDir + '" ',
    SQLCompl,
    '']);
  if QueryLoc.RecordCount > 0 then
  begin
    Msg    := 'Este diret�rio j� foi cadastrado!';
    Result := False;
  end else
  begin
    Msg    := '';
    Result := True;
  end;
  //
  QueryLoc.Close;
end;

procedure TUnTreeView.LocalizaNodeByStateIndex(TreeView: TTreeView; StateIndex: Integer);
var
  I, Idx: Integer;
begin
  for i := 0 to TreeView.Items.Count - 1 do
  begin
    Idx := TreeView.Items[I].StateIndex;
    //
    if StateIndex = Idx then
    begin
      TreeView.Items[I].Selected := True;
      Exit;
    end;
  end;
end;

function TUnTreeView.LocalizaNodeInArrayNode(Nome: String; StateIndex,
  Level: Integer; TreeView: TTreeView): TTreeNode;
var
  i, Itens, Nivel, Index: Integer;
  Diretorio: String;
begin
  Result := nil;
  Itens  := TreeView.Items.Count;
  //
  if Itens = 0 then
    Exit;
  for i := 0 to Itens - 1 do
  begin
    Diretorio := TreeView.Items[i].Text;
    Nivel     := TreeView.Items[i].Level;
    Index     := TreeView.Items[i].StateIndex;
    //
    (*
    if StateIndex = 0 then
    begin
      if(Diretorio = Nome) and (Nivel = Level) then
      begin
        Result := TreeView.Items[i];
        Exit;
      end;
    end else
    *)
    begin
      if(StateIndex = Index) and (Nivel = Level) then
      begin
        Result := TreeView.Items[i];
        Exit;
      end;
    end;
  end;
end;

function TUnTreeView.ReordenaNodes(TreeView: TTreeView; Expand: Boolean): Boolean;
begin
  Result := False;
  //
  if TreeView.Items.Count > 0 then
  begin
    if DBCheck.CriaFm(TFmMyTreeViewReorder, FmMyTreeViewReorder, afmoNegarComAviso) then
    begin
      FmMyTreeViewReorder.TreeView1.Items.BeginUpdate;
      try
        FmMyTreeViewReorder.TreeView1.Items.Clear;
        FmMyTreeViewReorder.TreeView1.Items := TreeView.Items;
        FmMyTreeViewReorder.TreeView1.FullExpand;
      finally
        FmMyTreeViewReorder.TreeView1.Items.EndUpdate;
      end;
      FmMyTreeViewReorder.ShowModal;
      //
      if FmMyTreeViewReorder.FTreeView <> nil then
      begin
        TreeView.Items.BeginUpdate;
        try
          TreeView.Items.Clear;
          TreeView.Items := FmMyTreeViewReorder.FTreeView.Items;
        finally
          TreeView.Items.EndUpdate;
          //
          if Expand then
            TreeView.FullExpand
          else
            TreeView.FullCollapse;
        end;
        Result := True;
      end;
      //
      FmMyTreeViewReorder.Destroy;
    end;
  end else
    Result := True;
end;

procedure TUnTreeView.TreeView_CarregaItems(TreeView: TTreeView;
  QueryItens: TmySQLQuery; CampoNome, CampoCodigo, Separador: String);
var
  DirListExplode: TStringList;
  j, CodPasta: Integer;
  Linha, PastaAnt, Pasta: String;
  LocNode, LastNode: TTreeNode;
begin
  TreeView.Items.Clear;
  //
  try
    Screen.Cursor := crHourGlass;
    //
    while not QueryItens.Eof do
    begin
      Linha    := QueryItens.FieldByName(CampoNome).AsString;
      CodPasta := QueryItens.FieldByName(CampoCodigo).AsInteger;

      DirListExplode := Geral.Explode(Linha, Separador, 0);
      //
      try
        for j := 0 to DirListExplode.Count - 1 do
        begin
          Pasta    := DirListExplode[j];
          PastaAnt := '';
          //
          if j = 0 then
          begin
            if LocalizaNodeInArrayNode(Pasta, 0, j, TreeView) = nil then
              CriaNode(nil, Pasta, CodPasta, TreeView);
          end else
          begin
            PastaAnt := DirListExplode[j - 1];
            LastNode := LocalizaNodeInArrayNode(PastaAnt, 0, j -1, TreeView);
            //
            if LocalizaNodeInArrayNode(Pasta, 0, j, TreeView) = nil then
              CriaNode(LastNode, Pasta, CodPasta, TreeView);
          end;
        end;
      finally
        DirListExplode.Free;
      end;
      QueryItens.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  TreeView.FullExpand;
end;

function TUnTreeView.ExcluiNode(TreeView: TTreeView; Msg: String): Boolean;
var
  Node: TTreeNode;
  SelCod, Respos: Integer;
begin
  Result := False;
  //
  Node := TreeView.Selected;
  //
  if Node <> nil then
  begin
    TreeView.Items.BeginUpdate;
    try
      if Msg <> '' then
        Respos := Geral.MB_Pergunta(Msg)
      else
        Respos := ID_YES;
      //
      if Respos = ID_YES then
      begin
        if Node.getFirstChild = nil then
        begin
          Node.Delete;
          Result := True;
        end else
          Geral.MB_Aviso('Este item n�o pode ser removido!' + sLineBreak +
            'Motivo: Este item possui subitens!');
      end;
    finally
      TreeView.Items.EndUpdate;
    end;
  end;
end;

procedure TUnTreeView.EditaNode(TreeView: TTreeView; Node: TTreeNode;
  Nome: String; Id: Integer; ImgIndex: Integer = -1);
begin
  if Node <> nil then
  begin
    TreeView.Items.BeginUpdate;
    try
      Node.Text       := Nome;
      Node.StateIndex := Id;
      Node.ImageIndex := ImgIndex;
      Node.Selected   := True;
    finally
      TreeView.Items.EndUpdate;
    end;
  end;
end;

procedure TUnTreeView.AtualizaDBNodesOrdem(DataBase: TmySQLDatabase;
  Query: TmySQLQuery; TreeView: TTreeView; Tabela, ColNivel, ColOrdem,
  ColId, ColParentId: String);
var
  I: Integer;
  Id, ParentId, Nivel, Ordem: Integer;
begin
  if TreeView.Items.Count > 0 then
  begin
    Screen.Cursor := crHourGlass;
    try
      for I := 0 to TreeView.Items.Count - 1 do
      begin
        Id    := TreeView.Items[I].StateIndex;
        Nivel := TreeView.Items[I].Level;
        Ordem := TreeView.Items[I].AbsoluteIndex;
        //
        if TreeView.Items[I].Parent <> nil then
          ParentId := TreeView.Items[I].Parent.StateIndex
        else
          ParentId := 0;
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DataBase, [
          'UPDATE ' + Tabela + ' SET ',
          ColNivel + '=' + Geral.FF0(Nivel) + ', ',
          ColOrdem + '=' + Geral.FF0(Ordem) + ', ',
          ColParentId + '=' + Geral.FF0(ParentId),
          'WHERE ' + ColId + '=' + Geral.FF0(Id),
          '']);
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TUnTreeView.CarregaDBItensTreeView(Query: TmySQLQuery; ColId,
  ColNome, ColNivel: String; TreeView: TTreeView; Expand: Boolean);
var
  ANode, NextNode: TTreeNode;
  Nome: String;
  Controle, Nivel: Integer;
begin
  TreeView.Items.Clear;
  //
  if Query.RecordCount > 0 then
  begin
    try
      TreeView.Items.BeginUpdate;
      Query.First;
      //
      ANode := nil;
      //
      while not Query.Eof do
      begin
        Controle := Query.FieldByName(ColId).AsInteger;
        Nome     := Query.FieldByName(ColNome).AsString;
        Nivel    := Query.FieldByName(ColNivel).AsInteger;
        //
        if ANode = nil then
          ANode := TreeView.Items.AddChild(nil, Nome)
        else if ANode.Level = Nivel then
          ANode := TreeView.Items.AddChild(ANode.Parent, Nome)
        else if ANode.Level = (Nivel - 1) then
          ANode := TreeView.Items.AddChild(ANode, Nome)
        else if ANode.Level > Nivel then
        begin
          NextNode := ANode.Parent;
          while NextNode.Level > Nivel do
            NextNode := NextNode.Parent;
          ANode := TreeView.Items.AddChild(NextNode.Parent, Nome);
        end;
        //
        if ANode <> nil then
          ANode.StateIndex := Controle;
        //
        Query.Next;
      end;
    finally
      TreeView.Items.EndUpdate;
      //
      if Expand then
        TreeView.FullExpand
      else
        TreeView.FullCollapse;
    end;
  end;
end;

function TUnTreeView.CriaNode(Node: TTreeNode; Nome: String;
  Id: Integer; TreeView: TTreeView; ImgIndex: Integer = -1;
  Child: Boolean = False): TTreeNode;
var
  TreeNode: TTreeNode;
begin
  try
    TreeView.Items.BeginUpdate;
    //
    if not Child then
      TreeNode := TreeView.Items.Add(Node, Nome)
    else
      TreeNode := TreeView.Items.AddChild(Node, Nome);
    //
    if TreeNode <> nil then
    begin
      TreeNode.StateIndex := Id;
      TreeNode.ImageIndex := ImgIndex;
      TreeNode.Selected   := True;
      //
      Result := TreeNode;
    end;
    TreeView.Items.EndUpdate;
  except
    Result := nil;
  end;
end;

end.
