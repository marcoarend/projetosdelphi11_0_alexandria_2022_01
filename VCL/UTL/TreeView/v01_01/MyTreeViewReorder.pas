unit MyTreeViewReorder;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, DBGrids,
  Db, mySQLDbTables, dmkImage, UnDmkEnums, Vcl.ComCtrls;

type
  TFmMyTreeViewReorder = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    TreeView1: TTreeView;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TreeView1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure TreeView1DragDrop(Sender, Source: TObject; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    FTreeView: TTreeView;
  end;

  var
  FmMyTreeViewReorder: TFmMyTreeViewReorder;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmMyTreeViewReorder.BtSaidaClick(Sender: TObject);
begin
  FTreeView := nil;
  Close;
end;

procedure TFmMyTreeViewReorder.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMyTreeViewReorder.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMyTreeViewReorder.TreeView1DragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  AnItem: TTreeNode;
  AttachMode: TNodeAttachMode;
  HT: THitTests;
begin
  if TreeView1.Selected = nil then
    Exit;
  //
  try
    HT     := TreeView1.GetHitTestInfoAt(X, Y);
    AnItem := TreeView1.GetNodeAt(X, Y);
    //
    (*
    if AnItem = nil then
      Exit;
    *)
    //
    if (HT - [htOnItem, htOnIcon, htNowhere, htOnIndent] <> HT) then
    begin
      if (htOnItem in HT) or (htOnIcon in HT) then
        AttachMode := naAddChild
      else if htNowhere in HT then
        AttachMode := naAdd
      else if htOnIndent in HT then
        AttachMode := naInsert;
      //
      if AttachMode in [naAddChild, naAdd, naInsert] then
        TreeView1.Selected.MoveTo(AnItem, AttachMode);
    end;
  except
    TreeView1.ClearSelection;
  end;
end;

procedure TFmMyTreeViewReorder.TreeView1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := Source is TTreeView;
end;

procedure TFmMyTreeViewReorder.BtConfirmaClick(Sender: TObject);
begin
  FTreeView := TreeView1;
  Close;
end;

procedure TFmMyTreeViewReorder.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FTreeView       := nil;
end;

end.
