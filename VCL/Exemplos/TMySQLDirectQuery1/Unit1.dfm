object Form1: TForm1
  Left = 192
  Top = 103
  BorderStyle = bsSingle
  Caption = 'TMySQLDirectQuery VS TmySQLQuery example'
  ClientHeight = 390
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 4
    Width = 381
    Height = 13
    Caption = 
      'This example uses DBDEMOS SQL script database which can be obtai' +
      'ned here:'
  end
  object lDownloadsLink: TLabel
    Left = 4
    Top = 20
    Width = 312
    Height = 13
    Cursor = crHandPoint
    Caption = 'http://microolap.com/products/connectivity/mysqldac/download/'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = lDownloadsLinkClick
  end
  object Label2: TLabel
    Left = 8
    Top = 76
    Width = 65
    Height = 13
    Caption = 'Repeat count'
  end
  object Label3: TLabel
    Left = 164
    Top = 40
    Width = 221
    Height = 85
    AutoSize = False
    Caption = 
      'This example selects values from `AmountPaid` column of `orders`' +
      ' table, goes through dataset and calculates total amount.'
    WordWrap = True
  end
  object bConnect: TButton
    Left = 4
    Top = 40
    Width = 153
    Height = 25
    Caption = 'Connect to database'
    TabOrder = 0
    OnClick = bConnectClick
  end
  object bRun: TButton
    Left = 4
    Top = 100
    Width = 153
    Height = 25
    Caption = 'Run tests'
    TabOrder = 1
    OnClick = bRunClick
  end
  object seRepeat: TSpinEdit
    Left = 80
    Top = 72
    Width = 77
    Height = 22
    MaxValue = 10000
    MinValue = 1
    TabOrder = 2
    Value = 500
  end
  object mResult: TMemo
    Left = 4
    Top = 132
    Width = 381
    Height = 225
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object bAbout: TBitBtn
    Left = 232
    Top = 360
    Width = 75
    Height = 25
    Caption = 'About...'
    TabOrder = 4
    OnClick = bAboutClick
    Kind = bkHelp
  end
  object bExit: TBitBtn
    Left = 312
    Top = 360
    Width = 75
    Height = 25
    Caption = 'Exit'
    TabOrder = 5
    OnClick = bExitClick
    Kind = bkClose
  end
  object MySQLDirectQuery1: TmySQLDirectQuery
    Database = mySQLDatabase1
    SQL.Strings = (
      'SELECT AmountPaid FROM orders;')
    Left = 72
    Top = 8
  end
  object mySQLDatabase1: TmySQLDatabase
    DatabaseName = 'dbdemos'
    UserName = 'root'
    Host = 'localhost'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=dbdemos'
      'UID=root'
      'Host=localhost')
    DatasetOptions = []
    Left = 8
    Top = 8
  end
  object mySQLQuery1: TmySQLQuery
    Database = mySQLDatabase1
    SQL.Strings = (
      'SELECT AmountPaid FROM orders;')
    Left = 40
    Top = 8
  end
end
