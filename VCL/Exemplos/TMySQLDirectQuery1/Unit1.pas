unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, mySQLDirectQuery, StdCtrls, Spin, Buttons;

type
  TForm1 = class(TForm)
    MySQLDirectQuery1: TMySQLDirectQuery;
    mySQLDatabase1: TmySQLDatabase;
    mySQLQuery1: TmySQLQuery;
    Label1: TLabel;
    lDownloadsLink: TLabel;
    bConnect: TButton;
    bRun: TButton;
    Label2: TLabel;
    seRepeat: TSpinEdit;
    mResult: TMemo;
    Label3: TLabel;
    bAbout: TBitBtn;
    bExit: TBitBtn;
    procedure bConnectClick(Sender: TObject);
    procedure bRunClick(Sender: TObject);
    procedure bExitClick(Sender: TObject);
    procedure bAboutClick(Sender: TObject);
    procedure lDownloadsLinkClick(Sender: TObject);
  private
    { Private declarations }
    function RunWithQuery(): double;
    function RunWithDirectQuery(): double;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

uses ShellAPI, mySQLConnFrm, mySQLAboutFrm;

{ TForm1 }

function TForm1.RunWithDirectQuery: double;
begin
  Result := 0;

  MySQLDirectQuery1.Open();
  MySQLDirectQuery1.First();
  while not MySQLDirectQuery1.Eof do
  begin
    Result := Result + StrToFloat(MySQLDirectQuery1.FieldValues[0]);

    MySQLDirectQuery1.Next();
  end;
  MySQLDirectQuery1.Close();
end;

function TForm1.RunWithQuery : double;
begin
  Result := 0;

  MySQLQuery1.Open();
  MySQLQuery1.First();
  while not MySQLQuery1.Eof do
  begin
    Result := Result + MySQLQuery1.Fields[0].AsFloat;

    MySQLQuery1.Next();
  end;
  MySQLQuery1.Close();
end;

procedure TForm1.bConnectClick(Sender: TObject);
begin
  if EditDatabase(mySQLDatabase1) then
  begin
    mySQLDatabase1.Connect;
    mResult.Lines.Add('Succesfully connected to ' + mySQLDatabase1.GetServerInfo());
  end;
end;

procedure TForm1.bRunClick(Sender: TObject);
var
  i : integer;
  Total : double;
  a, b, t1, t2 : Cardinal;
begin
  Total := 0;

  mResult.Clear();
  Application.ProcessMessages();

  if not mySQLDatabase1.Connected then
  begin
    ShowMessage('Please connect to database first!');
    exit;
  end;

  mResult.Lines.Add('Running TmySQLQuery test ' + IntToStr(seRepeat.Value) + ' times...');
  a := GetTickCount();
  for i := 1 to seRepeat.Value do
  begin
    Total := RunWithQuery();
  end;
  b := GetTickCount();
  t1 := b - a;
  mResult.Lines.Add('Total time: ' + FloatToStr(t1 / 1000) + 's (Sum=' + FloatToStr(Total) + ')');
  mResult.Lines.Add('---------------------');

  mResult.Lines.Add('Running TmySQLDirectQuery test ' + IntToStr(seRepeat.Value) + ' times...');
  a := GetTickCount();
  for i := 1 to seRepeat.Value do
  begin
    Total := RunWithDirectQuery();
  end;
  b := GetTickCount();
  t2 := b - a;
  mResult.Lines.Add('Total time: ' + FloatToStr(t2 / 1000) + 's (Sum=' + FloatToStr(Total) + ')');
  mResult.Lines.Add('---------------------');
  mResult.Lines.Add('It seems that TmySQLDirectQuery ' + FloatToStr(t1 / t2) + ' times faster than TmySQLQuery');
end;

procedure TForm1.bExitClick(Sender: TObject);
begin
  Close();
end;

procedure TForm1.bAboutClick(Sender: TObject);
begin
  dacformysqlShowAbout();
end;

procedure TForm1.lDownloadsLinkClick(Sender: TObject);
begin
  {Go to web}
	ShellExecute(0,'Open',PChar(lDownloadsLink.Caption),nil,nil,SW_SHOW); 
end;

end.
