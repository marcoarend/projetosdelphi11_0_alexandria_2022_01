unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, UnInternalConsts, 
  mySQLDbTables, Winsock, MySQLBatch, frxClass, frxDBSet, dmkGeral;

type
  TDmod = class(TDataModule)
    QrFields: TMySQLQuery;
    QrMaster: TMySQLQuery;
    QrLivreY_D: TmySQLQuery;
    QrRecCountX: TMySQLQuery;
    QrRecCountXRecord: TIntegerField;
    QrUser: TMySQLQuery;
    QrSelX: TMySQLQuery;
    QrSelXLk: TIntegerField;
    QrSB: TMySQLQuery;
    QrSBCodigo: TIntegerField;
    QrSBNome: TStringField;
    DsSB: TDataSource;
    QrSB2: TMySQLQuery;
    QrSB2Codigo: TFloatField;
    QrSB2Nome: TStringField;
    DsSB2: TDataSource;
    QrSB3: TMySQLQuery;
    QrSB3Codigo: TDateField;
    QrSB3Nome: TStringField;
    DsSB3: TDataSource;
    QrDuplicIntX: TMySQLQuery;
    QrDuplicIntXINTEIRO1: TIntegerField;
    QrDuplicIntXINTEIRO2: TIntegerField;
    QrDuplicIntXCODIGO: TIntegerField;
    QrDuplicStrX: TMySQLQuery;
    QrDuplicStrXNOME: TStringField;
    QrDuplicStrXCODIGO: TIntegerField;
    QrDuplicStrXANTERIOR: TIntegerField;
    QrInsLogX: TMySQLQuery;
    QrDelLogX: TMySQLQuery;
    QrDataBalY: TmySQLQuery;
    QrDataBalYData: TDateField;
    QrSenha: TMySQLQuery;
    QrSenhaLogin: TStringField;
    QrSenhaNumero: TIntegerField;
    QrSenhaSenha: TStringField;
    QrSenhaPerfil: TIntegerField;
    QrSenhaLk: TIntegerField;
    QrUserLogin: TStringField;
    QrMaster2: TMySQLQuery;
    QrSomaM: TMySQLQuery;
    QrSomaMValor: TFloatField;
    QrMasterCNPJ_TXT: TStringField;
    QrMasterTE1_TXT: TStringField;
    QrMasterCEP_TXT: TStringField;
    QrProduto: TMySQLQuery;
    QrVendas: TMySQLQuery;
    QrEntrada: TMySQLQuery;
    QrEntradaPecas: TFloatField;
    QrEntradaValor: TFloatField;
    QrVendasPecas: TFloatField;
    QrVendasValor: TFloatField;
    QrUpdM: TmySQLQuery;
    QrUpdU: TmySQLQuery;
    QrUpdL: TmySQLQuery;
    QrUpdY: TmySQLQuery;
    QrLivreY: TmySQLQuery;
    QrLivreYCodigo: TIntegerField;
    QrMaster2Em: TStringField;
    QrMaster2CNPJ: TStringField;
    QrMaster2Monitorar: TSmallintField;
    QrMaster2Distorcao: TIntegerField;
    QrMaster2DataI: TDateField;
    QrMaster2DataF: TDateField;
    QrMaster2Hoje: TDateField;
    QrMaster2Hora: TTimeField;
    QrMaster2MasLogin: TStringField;
    QrMaster2MasSenha: TStringField;
    QrMaster2MasAtivar: TStringField;
    QrMasterEm: TStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TStringField;
    QrMasterIE: TStringField;
    QrMasterECidade: TStringField;
    QrMasterNOMEUF: TStringField;
    QrMasterEFax: TStringField;
    QrMasterERua: TStringField;
    QrMasterEBairro: TStringField;
    QrMasterECompl: TStringField;
    QrMasterEContato: TStringField;
    QrMasterECel: TStringField;
    QrMasterETe1: TStringField;
    QrMasterETe2: TStringField;
    QrMasterETe3: TStringField;
    QrMasterEPais: TStringField;
    QrMasterRespons1: TStringField;
    QrMasterRespons2: TStringField;
    QrCountY: TmySQLQuery;
    QrCountYRecord: TIntegerField;
    QrLocY: TmySQLQuery;
    QrLocYRecord: TIntegerField;
    QrLocDataY: TmySQLQuery;
    QrLocDataYRecord: TDateField;
    QrLivreY_DCodigo: TLargeintField;
    QrMasterECEP: TIntegerField;
    QrIdx: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrSQL: TmySQLQuery;
    MyDB: TmySQLDatabase;
    MyLocDatabase: TmySQLDatabase;
    QrSB4: TmySQLQuery;
    DsSB4: TDataSource;
    QrSB4Codigo: TStringField;
    QrSB4Nome: TStringField;
    QrPriorNext: TmySQLQuery;
    QrAuxL: TmySQLQuery;
    QrSoma1: TmySQLQuery;
    QrSoma1TOTAL: TFloatField;
    QrSoma1Qtde: TFloatField;
    QrControle: TmySQLQuery;
    QrControleSoMaiusculas: TStringField;
    QrControleMoeda: TStringField;
    QrControleUFPadrao: TIntegerField;
    QrControleCidade: TStringField;
    QrMasterLogo2: TBlobField;
    QrEstoque: TmySQLQuery;
    QrEstoqueTipo: TSmallintField;
    QrEstoqueQtde: TFloatField;
    QrEstoqueValorCus: TFloatField;
    QrPeriodoBal: TmySQLQuery;
    QrPeriodoBalPeriodo: TIntegerField;
    QrMin: TmySQLQuery;
    QrMinPeriodo: TIntegerField;
    QrBalancosIts: TmySQLQuery;
    QrBalancosItsEstqQ: TFloatField;
    QrBalancosItsEstqV: TFloatField;
    QrEstqperiodo: TmySQLQuery;
    QrBalancosItsEstqQ_G: TFloatField;
    QrBalancosItsEstqV_G: TFloatField;
    QrEstqperiodoTipo: TSmallintField;
    QrEstqperiodoQtde: TFloatField;
    QrEstqperiodoValorCus: TFloatField;
    QrControleCartVen: TIntegerField;
    QrControleCartCom: TIntegerField;
    QrControleCartDeS: TIntegerField;
    QrControleCartReS: TIntegerField;
    QrControleCartDeG: TIntegerField;
    QrControleCartReG: TIntegerField;
    QrControleCartCoE: TIntegerField;
    QrControleCartCoC: TIntegerField;
    QrControleCartEmD: TIntegerField;
    QrControleCartEmA: TIntegerField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TStringField;
    QrTerminalTerminal: TIntegerField;
    QrControleContraSenha: TStringField;
    QrProdutoCodigo: TIntegerField;
    QrProdutoControla: TStringField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrMasterLimite: TSmallintField;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    ChDB: TmySQLDatabase;
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TStringField;
    QrControleTravaCidade: TSmallintField;
    QrMasterSolicitaSenha: TIntegerField;
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleMensalSempre: TIntegerField;
    QrControleEntraSemValor: TIntegerField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroNOMEpUF: TStringField;
    QrTerceiroNOMEeUF: TStringField;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TStringField;
    QrTerceiroFantasia: TStringField;
    QrTerceiroRespons1: TStringField;
    QrTerceiroRespons2: TStringField;
    QrTerceiroPai: TStringField;
    QrTerceiroMae: TStringField;
    QrTerceiroCNPJ: TStringField;
    QrTerceiroIE: TStringField;
    QrTerceiroIEST: TStringField;
    QrTerceiroNome: TStringField;
    QrTerceiroApelido: TStringField;
    QrTerceiroCPF: TStringField;
    QrTerceiroRG: TStringField;
    QrTerceiroELograd: TSmallintField;
    QrTerceiroERua: TStringField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroECompl: TStringField;
    QrTerceiroEBairro: TStringField;
    QrTerceiroECidade: TStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TStringField;
    QrTerceiroETe1: TStringField;
    QrTerceiroEte2: TStringField;
    QrTerceiroEte3: TStringField;
    QrTerceiroECel: TStringField;
    QrTerceiroEFax: TStringField;
    QrTerceiroEEmail: TStringField;
    QrTerceiroEContato: TStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPLograd: TSmallintField;
    QrTerceiroPRua: TStringField;
    QrTerceiroPNumero: TIntegerField;
    QrTerceiroPCompl: TStringField;
    QrTerceiroPBairro: TStringField;
    QrTerceiroPCidade: TStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TStringField;
    QrTerceiroPTe1: TStringField;
    QrTerceiroPte2: TStringField;
    QrTerceiroPte3: TStringField;
    QrTerceiroPCel: TStringField;
    QrTerceiroPFax: TStringField;
    QrTerceiroPEmail: TStringField;
    QrTerceiroPContato: TStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TStringField;
    QrTerceiroResponsavel: TStringField;
    QrTerceiroProfissao: TStringField;
    QrTerceiroCargo: TStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TStringField;
    QrTerceiroCliente2: TStringField;
    QrTerceiroFornece1: TStringField;
    QrTerceiroFornece2: TStringField;
    QrTerceiroFornece3: TStringField;
    QrTerceiroFornece4: TStringField;
    QrTerceiroTerceiro: TStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TStringField;
    QrTerceiroObservacoes: TMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroCLograd: TSmallintField;
    QrTerceiroCRua: TStringField;
    QrTerceiroCNumero: TIntegerField;
    QrTerceiroCCompl: TStringField;
    QrTerceiroCBairro: TStringField;
    QrTerceiroCCidade: TStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TStringField;
    QrTerceiroCTel: TStringField;
    QrTerceiroCCel: TStringField;
    QrTerceiroCFax: TStringField;
    QrTerceiroCContato: TStringField;
    QrTerceiroLLograd: TSmallintField;
    QrTerceiroLRua: TStringField;
    QrTerceiroLNumero: TIntegerField;
    QrTerceiroLCompl: TStringField;
    QrTerceiroLBairro: TStringField;
    QrTerceiroLCidade: TStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TStringField;
    QrTerceiroLTel: TStringField;
    QrTerceiroLCel: TStringField;
    QrTerceiroLFax: TStringField;
    QrTerceiroLContato: TStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TStringField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroAccount: TIntegerField;
    QrTerceiroLogo2: TBlobField;
    QrTerceiroConjugeNome: TStringField;
    QrTerceiroConjugeNatal: TDateField;
    QrTerceiroNome1: TStringField;
    QrTerceiroNatal1: TDateField;
    QrTerceiroNome2: TStringField;
    QrTerceiroNatal2: TDateField;
    QrTerceiroNome3: TStringField;
    QrTerceiroNatal3: TDateField;
    QrTerceiroNome4: TStringField;
    QrTerceiroNatal4: TDateField;
    QrTerceiroCreditosI: TIntegerField;
    QrTerceiroCreditosL: TIntegerField;
    QrTerceiroCreditosF2: TFloatField;
    QrTerceiroCreditosD: TDateField;
    QrTerceiroCreditosU: TDateField;
    QrTerceiroCreditosV: TDateField;
    QrTerceiroMotivo: TIntegerField;
    QrTerceiroQuantI1: TIntegerField;
    QrTerceiroQuantI2: TIntegerField;
    QrTerceiroQuantI3: TIntegerField;
    QrTerceiroQuantI4: TIntegerField;
    QrTerceiroQuantN1: TFloatField;
    QrTerceiroQuantN2: TFloatField;
    QrTerceiroAgenda: TStringField;
    QrTerceiroSenhaQuer: TStringField;
    QrTerceiroSenha1: TStringField;
    QrTerceiroLimiCred: TFloatField;
    QrTerceiroDesco: TFloatField;
    QrTerceiroCasasApliDesco: TSmallintField;
    QrTerceiroTempD: TFloatField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TIntegerField;
    QrTerceiroUserAlt: TIntegerField;
    QrTerceiroCPF_Pai: TStringField;
    QrTerceiroSSP: TStringField;
    QrTerceiroCidadeNatal: TStringField;
    QrTerceiroUFNatal: TSmallintField;
    QlLocal: TMySQLBatchExecute;
    QrControleMyPagTip: TIntegerField;
    QrControleMyPagCar: TIntegerField;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrControleDono: TIntegerField;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TStringField;
    QrTerminaisTerminal: TIntegerField;
    QrEndereco: TmySQLQuery;
    QrEnderecoCodigo: TIntegerField;
    QrEnderecoCadastro: TDateField;
    QrEnderecoNOMEDONO: TStringField;
    QrEnderecoCNPJ_CPF: TStringField;
    QrEnderecoIE_RG: TStringField;
    QrEnderecoNIRE_: TStringField;
    QrEnderecoRUA: TStringField;
    QrEnderecoNUMERO: TLargeintField;
    QrEnderecoCOMPL: TStringField;
    QrEnderecoBAIRRO: TStringField;
    QrEnderecoCIDADE: TStringField;
    QrEnderecoNOMELOGRAD: TStringField;
    QrEnderecoNOMEUF: TStringField;
    QrEnderecoPais: TStringField;
    QrEnderecoLograd: TLargeintField;
    QrEnderecoTipo: TSmallintField;
    QrEnderecoCEP: TLargeintField;
    QrEnderecoTE1: TStringField;
    QrEnderecoFAX: TStringField;
    QrEnderecoENatal: TDateField;
    QrEnderecoPNatal: TDateField;
    QrEnderecoECEP_TXT: TStringField;
    QrEnderecoNUMERO_TXT: TStringField;
    QrEnderecoE_ALL: TStringField;
    QrEnderecoCNPJ_TXT: TStringField;
    QrEnderecoFAX_TXT: TStringField;
    QrEnderecoTE1_TXT: TStringField;
    QrEnderecoNATAL_TXT: TStringField;
    QrControleCorRecibo: TIntegerField;
    QrControleIdleMinutos: TIntegerField;
    QrEnderecoRespons1: TStringField;
    QrAgoraAGORA: TDateTimeField;
    QrControleMyPgParc: TSmallintField;
    QrControleMyPgQtdP: TSmallintField;
    QrControleMyPgPeri: TSmallintField;
    QrControleMyPgDias: TSmallintField;
    QrControleMeuLogoPath: TStringField;
    QrControleLogoNF: TStringField;
    QrMasterENumero: TFloatField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrMaster2Limite: TSmallintField;
    QrMaster2Licenca: TStringField;
    QrTerminaisLicenca: TStringField;
    QrTransf: TmySQLQuery;
    QrTransfData: TDateField;
    QrTransfTipo: TSmallintField;
    QrTransfCarteira: TIntegerField;
    QrTransfControle: TIntegerField;
    QrTransfGenero: TIntegerField;
    QrTransfDebito: TFloatField;
    QrTransfCredito: TFloatField;
    QrTransfDocumento: TFloatField;
    DsCarteiras: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TStringField;
    QrCarteirasFatura: TStringField;
    QrCarteirasID_Fat: TStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasNOMEDOBANCO: TStringField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasDIFERENCA: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTIPOPRAZO: TStringField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TSmallintField;
    QrCarteirasUserAlt: TSmallintField;
    QrCarteirasNOMEPAGREC: TStringField;
    QrCarteirasPagRec: TSmallintField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasNome2: TStringField;
    QrCarteirasNOMEFORNECEI: TStringField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasContato1: TStringField;
    QrLanctos: TmySQLQuery;
    QrLanctosData: TDateField;
    QrLanctosTipo: TSmallintField;
    QrLanctosCarteira: TIntegerField;
    QrLanctosAutorizacao: TIntegerField;
    QrLanctosGenero: TIntegerField;
    QrLanctosDescricao: TStringField;
    QrLanctosNotaFiscal: TIntegerField;
    QrLanctosDebito: TFloatField;
    QrLanctosCredito: TFloatField;
    QrLanctosCompensado: TDateField;
    QrLanctosDocumento: TFloatField;
    QrLanctosSit: TIntegerField;
    QrLanctosVencimento: TDateField;
    QrLanctosLk: TIntegerField;
    QrLanctosFatID: TIntegerField;
    QrLanctosFatNum: TIntegerField;
    QrLanctosFatParcela: TIntegerField;
    QrLanctosCONTA: TIntegerField;
    QrLanctosNOMECONTA: TStringField;
    QrLanctosNOMEEMPRESA: TStringField;
    QrLanctosNOMESUBGRUPO: TStringField;
    QrLanctosNOMEGRUPO: TStringField;
    QrLanctosNOMECONJUNTO: TStringField;
    QrLanctosNOMESIT: TStringField;
    QrLanctosAno: TFloatField;
    QrLanctosMENSAL: TStringField;
    QrLanctosMENSAL2: TStringField;
    QrLanctosBanco: TIntegerField;
    QrLanctosLocal: TIntegerField;
    QrLanctosFatura: TStringField;
    QrLanctosSub: TSmallintField;
    QrLanctosCartao: TIntegerField;
    QrLanctosLinha: TIntegerField;
    QrLanctosPago: TFloatField;
    QrLanctosSALDO: TFloatField;
    QrLanctosID_Sub: TSmallintField;
    QrLanctosMez: TIntegerField;
    QrLanctosFornecedor: TIntegerField;
    QrLanctoscliente: TIntegerField;
    QrLanctosMoraDia: TFloatField;
    QrLanctosNOMECLIENTE: TStringField;
    QrLanctosNOMEFORNECEDOR: TStringField;
    QrLanctosTIPOEM: TStringField;
    QrLanctosNOMERELACIONADO: TStringField;
    QrLanctosOperCount: TIntegerField;
    QrLanctosLancto: TIntegerField;
    QrLanctosMulta: TFloatField;
    QrLanctosATRASO: TFloatField;
    QrLanctosJUROS: TFloatField;
    QrLanctosDataDoc: TDateField;
    QrLanctosNivel: TIntegerField;
    QrLanctosVendedor: TIntegerField;
    QrLanctosAccount: TIntegerField;
    QrLanctosMes2: TLargeintField;
    QrLanctosProtesto: TDateField;
    QrLanctosDataCad: TDateField;
    QrLanctosDataAlt: TDateField;
    QrLanctosUserCad: TSmallintField;
    QrLanctosUserAlt: TSmallintField;
    QrLanctosControle: TIntegerField;
    QrLanctosID_Pgto: TIntegerField;
    QrLanctosCtrlIni: TIntegerField;
    QrLanctosFatID_Sub: TIntegerField;
    QrLanctosICMS_P: TFloatField;
    QrLanctosICMS_V: TFloatField;
    QrLanctosDuplicata: TStringField;
    QrLanctosCOMPENSADO_TXT: TStringField;
    QrLanctosCliInt: TIntegerField;
    QrLanctosDepto: TIntegerField;
    QrLanctosDescoPor: TIntegerField;
    QrLanctosForneceI: TIntegerField;
    QrLanctosQtde: TFloatField;
    DsLanctos: TDataSource;
    QrEstqR: TmySQLQuery;
    QrEstqRQtd: TFloatField;
    QrEstqRVal: TFloatField;
    QrEstqP: TmySQLQuery;
    QrEstqPQtd: TFloatField;
    QrSel: TmySQLQuery;
    QrSelMovix: TIntegerField;
    QrControleVerBcoTabs: TIntegerField;
    QrControleLk: TIntegerField;
    QrControleDataCad: TDateField;
    QrControleDataAlt: TDateField;
    QrControleUserCad: TIntegerField;
    QrControleUserAlt: TIntegerField;
    QrControleCNAB_Cfg: TIntegerField;
    QrControleCNAB_Rem: TIntegerField;
    QrControleCNAB_Rem_I: TIntegerField;
    QrControlePreEmMsgIm: TIntegerField;
    QrControlePreEmMsg: TIntegerField;
    QrControlePreEmail: TIntegerField;
    QrControleContasMes: TIntegerField;
    QrControleMultiPgto: TIntegerField;
    QrControleImpObs: TIntegerField;
    QrControleProLaINSSr: TFloatField;
    QrControleProLaINSSp: TFloatField;
    QrControleFPPontoHor: TIntegerField;
    QrControleFPHrExt: TIntegerField;
    QrControleVerSalTabs: TIntegerField;
    QrControleCNAB_CaD: TIntegerField;
    QrControleCNAB_CaG: TIntegerField;
    QrControleFPFunciFer: TIntegerField;
    QrControleFPFunciHor: TIntegerField;
    QrControleAtzCritic: TIntegerField;
    QrControleFPFunciEve: TIntegerField;
    QrControleFPRacas: TIntegerField;
    QrControleFPCivil: TIntegerField;
    QrControleFPNacio: TIntegerField;
    QrControleFPFolhaCal: TIntegerField;
    QrControleFPCateg: TIntegerField;
    QrControleFPEvent: TIntegerField;
    QrControleFPVaria: TIntegerField;
    QrControleFPVincu: TIntegerField;
    QrControleContasTrf: TIntegerField;
    QrControleSomaIts: TIntegerField;
    QrControleContasAgr: TIntegerField;
    QrControleConfJanela: TIntegerField;
    QrControleDataPesqAuto: TSmallintField;
    QrControleAtividad: TSmallintField;
    QrControleCidades: TSmallintField;
    QrControleChequez: TSmallintField;
    QrControlePaises: TSmallintField;
    QrControleCambiosData: TDateField;
    QrControleCambiosUsuario: TIntegerField;
    QrControlereg10: TSmallintField;
    QrControlereg11: TSmallintField;
    QrControlereg50: TSmallintField;
    QrControlereg51: TSmallintField;
    QrControlereg53: TSmallintField;
    QrControlereg54: TSmallintField;
    QrControlereg56: TSmallintField;
    QrControlereg60: TSmallintField;
    QrControlereg75: TSmallintField;
    QrControlereg88: TSmallintField;
    QrControlereg90: TSmallintField;
    QrControleNumSerieNF: TSmallintField;
    QrControleSerieNF: TSmallintField;
    QrControleModeloNF: TSmallintField;
    QrControleControlaNeg: TSmallintField;
    QrControleFamilias: TSmallintField;
    QrControleFamiliasIts: TSmallintField;
    QrControleAskNFOrca: TSmallintField;
    QrControlePreviewNF: TSmallintField;
    QrControleOrcaRapido: TSmallintField;
    QrControleDistriDescoItens: TSmallintField;
    QrControleBalType: TSmallintField;
    QrControleOrcaOrdem: TSmallintField;
    QrControleOrcaLinhas: TSmallintField;
    QrControleOrcaLFeed: TIntegerField;
    QrControleOrcaModelo: TSmallintField;
    QrControleOrcaRodaPos: TSmallintField;
    QrControleOrcaRodape: TSmallintField;
    QrControleOrcaCabecalho: TSmallintField;
    QrControleCoresRel: TSmallintField;
    QrControleCFiscalPadr: TStringField;
    QrControleSitTribPadr: TStringField;
    QrControleCFOPPadr: TStringField;
    QrControleAvisosCxaEdit: TSmallintField;
    QrControleChConfCab: TIntegerField;
    QrControleImpDOS: TIntegerField;
    QrControleUnidadePadrao: TIntegerField;
    QrControleProdutosV: TIntegerField;
    QrControleCartDespesas: TIntegerField;
    QrControleReserva: TSmallintField;
    QrControleCNPJ: TStringField;
    QrControleVersao: TIntegerField;
    QrControleVerWeb: TIntegerField;
    QrControleErroHora: TIntegerField;
    QrControleSenhas: TIntegerField;
    QrControleSalarios: TIntegerField;
    QrControleEntidades: TIntegerField;
    QrControleUFs: TIntegerField;
    QrControleListaECivil: TIntegerField;
    QrControlePerfis: TIntegerField;
    QrControleUsuario: TIntegerField;
    QrControleContas: TIntegerField;
    QrControleCentroCusto: TIntegerField;
    QrControleDepartamentos: TIntegerField;
    QrControleDividas: TIntegerField;
    QrControleDividasIts: TIntegerField;
    QrControleDividasPgs: TIntegerField;
    QrControleCarteiras: TIntegerField;
    QrControleCartaG: TIntegerField;
    QrControleCartas: TIntegerField;
    QrControleConsignacao: TIntegerField;
    QrControleGrupo: TIntegerField;
    QrControleSubGrupo: TIntegerField;
    QrControleConjunto: TIntegerField;
    QrControlePlano: TIntegerField;
    QrControleInflacao: TIntegerField;
    QrControlekm: TIntegerField;
    QrControlekmMedia: TIntegerField;
    QrControlekmIts: TIntegerField;
    QrControleFatura: TIntegerField;
    QrControleLanctos: TLargeintField;
    QrControleEntiGrupos: TIntegerField;
    QrControleAparencias: TIntegerField;
    QrControlePages: TIntegerField;
    QrControleMultiEtq: TIntegerField;
    QrControleEntiTransp: TIntegerField;
    QrControleExcelGru: TIntegerField;
    QrControleExcelGruImp: TIntegerField;
    QrControleMediaCH: TIntegerField;
    QrControleImprime: TIntegerField;
    QrControleImprimeBand: TIntegerField;
    QrControleImprimeView: TIntegerField;
    QrControleComProdPerc: TIntegerField;
    QrControleComProdEdit: TIntegerField;
    QrControleComServPerc: TIntegerField;
    QrControleComServEdit: TIntegerField;
    QrControlePadrPlacaCar: TStringField;
    QrControleServSMTP: TStringField;
    QrControleNomeMailOC: TStringField;
    QrControleDonoMailOC: TStringField;
    QrControleMailOC: TStringField;
    QrControleCorpoMailOC: TMemoField;
    QrControleConexaoDialUp: TStringField;
    QrControleMailCCCega: TStringField;
    QrControleMoedaVal: TFloatField;
    QrControleTela1: TIntegerField;
    QrControleChamarPgtoServ: TIntegerField;
    QrControleFormUsaTam: TIntegerField;
    QrControleFormHeight: TIntegerField;
    QrControleFormWidth: TIntegerField;
    QrControleFormPixEsq: TIntegerField;
    QrControleFormPixDir: TIntegerField;
    QrControleFormPixTop: TIntegerField;
    QrControleFormPixBot: TIntegerField;
    QrControleFormFoAlt: TIntegerField;
    QrControleFormFoPro: TFloatField;
    QrControleFormUsaPro: TIntegerField;
    QrControleFormSlides: TIntegerField;
    QrControleFormNeg: TIntegerField;
    QrControleFormIta: TIntegerField;
    QrControleFormSub: TIntegerField;
    QrControleFormExt: TIntegerField;
    QrControleFormFundoTipo: TIntegerField;
    QrControleFormFundoBMP: TStringField;
    QrControleServInterv: TIntegerField;
    QrControleServAntecip: TIntegerField;
    QrControleAdiLancto: TIntegerField;
    QrControleContaSal: TIntegerField;
    QrControleContaVal: TIntegerField;
    QrControlePronomeE: TStringField;
    QrControlePronomeM: TStringField;
    QrControlePronomeF: TStringField;
    QrControlePronomeA: TStringField;
    QrControleSaudacaoE: TStringField;
    QrControleSaudacaoM: TStringField;
    QrControleSaudacaoF: TStringField;
    QrControleSaudacaoA: TStringField;
    QrControleNiver: TSmallintField;
    QrControleNiverddA: TSmallintField;
    QrControleNiverddD: TSmallintField;
    QrControleLastPassD: TDateTimeField;
    QrControleMultiPass: TIntegerField;
    QrControleCodigo: TIntegerField;
    QrControleAlterWeb: TSmallintField;
    QrControleAtivo: TSmallintField;
    QrControleCores: TIntegerField;
    QrControleGradeG: TIntegerField;
    QrControleGrades: TIntegerField;
    QrControleGradesCors: TIntegerField;
    QrControleGradesTams: TIntegerField;
    QrControleTamanhos: TIntegerField;
    QrControleProdutos: TIntegerField;
    QrControleMoviC: TIntegerField;
    QrControleLastBco: TIntegerField;
    QrUpdW: TmySQLQuery;
    QrControleMovix: TIntegerField;
    frxDsDono: TfrxDBDataset;
    QrControleMyPerJuros: TFloatField;
    QrControleMyPerMulta: TFloatField;
    QrDono: TmySQLQuery;
    QrDonoCodigo: TIntegerField;
    QrDonoCadastro: TDateField;
    QrDonoNOMEDONO: TStringField;
    QrDonoCNPJ_CPF: TStringField;
    QrDonoIE_RG: TStringField;
    QrDonoNIRE_: TStringField;
    QrDonoRUA: TStringField;
    QrDonoCOMPL: TStringField;
    QrDonoBAIRRO: TStringField;
    QrDonoCIDADE: TStringField;
    QrDonoNOMELOGRAD: TStringField;
    QrDonoNOMEUF: TStringField;
    QrDonoPais: TStringField;
    QrDonoLograd: TLargeintField;
    QrDonoTipo: TSmallintField;
    QrDonoCEP: TLargeintField;
    QrDonoTE1: TStringField;
    QrDonoTE2: TStringField;
    QrDonoFAX: TStringField;
    QrDonoENatal: TDateField;
    QrDonoPNatal: TDateField;
    QrDonoECEP_TXT: TStringField;
    QrDonoNUMERO_TXT: TStringField;
    QrDonoCNPJ_TXT: TStringField;
    QrDonoFAX_TXT: TStringField;
    QrDonoTE1_TXT: TStringField;
    QrDonoTE2_TXT: TStringField;
    QrDonoNATAL_TXT: TStringField;
    QrDonoE_LNR: TStringField;
    QrDonoE_CUC: TStringField;
    QrDonoEContato: TStringField;
    QrDonoECel: TStringField;
    QrDonoCEL_TXT: TStringField;
    QrDonoE_LN2: TStringField;
    QrDonoAPELIDO: TStringField;
    QrDonoNUMERO: TFloatField;
    QrDonoRespons1: TStringField;
    QrDonoRespons2: TStringField;
    QrDonoFONES: TStringField;
    QrBoss: TmySQLQuery;
    QrBossMasSenha: TStringField;
    QrBossMasLogin: TStringField;
    QrBossEm: TStringField;
    QrBossCNPJ: TStringField;
    QrBossMasZero: TStringField;
    QrBSit: TmySQLQuery;
    QrBSitSitSenha: TSmallintField;
    QrSenhas: TmySQLQuery;
    QrSenhasSenhaNew: TStringField;
    QrSenhasSenhaOld: TStringField;
    QrSenhaslogin: TStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasFuncionario: TIntegerField;
    QrSSit: TmySQLQuery;
    QrSSitSitSenha: TSmallintField;
    frxDsMaster: TfrxDBDataset;
    QrControleLogoBig1: TStringField;
    QrSenhasIP_Default: TStringField;
    QrEnderecoNO_TIPO_DOC: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure TbMovCalcFields(DataSet: TDataSet);
    procedure TbMovBeforePost(DataSet: TDataSet);
    procedure TbMovBeforeDelete(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QrEnderecoCalcFields(DataSet: TDataSet);
    procedure QrDonoCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FProdDelete: Integer;
    function Privilegios(Usuario : Integer) : Boolean;
    procedure RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
    procedure VerificaSenha(Index: Integer; Login, Senha: String);

    function ConcatenaRegistros(Matricula: Integer): String;
    function ObtemIP(Retorno: Integer): String;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    function BuscaProximoMovix: Integer;
  end;

var
  Dmod: TDmod;
  FVerifi: Boolean;
  //
  QvEntra3: TmySQLQuery;
  QvVenda3: TmySQLQuery;
  QvDevol3: TmySQLQuery;
  QvRecom3: TmySQLQuery;

implementation

uses UnMyObjects, Principal;

{$R *.DFM}

procedure TDmod.DataModuleCreate(Sender: TObject);
//var
  //Versao, VerZero, Resp: Integer;
  //BD: String;
  //Verifica: Boolean;
begin
  if MyDB.Connected then
    Application.MessageBox('MyDB est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  if MyLocDataBase.Connected then
    Application.MessageBox('MyLocDataBase est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  VAR_BDSENHA := 'wkljweryhvbirt';
  {
  if not GOTOy.OMySQLEstaInstalado(
  FmBase_MLA.StaticText1, FmBase_MLA.ProgressBar1) then
  begin
    raise EAbort.Create('');
    Exit;
  end;
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
  ChDB.UserPassword := VAR_BDSENHA;
  try
    ChDB.Connected := True;
  except
    ChDB.UserPassword := '852456';
    try
      ChDB.Connected := True;
      QrUpd.Database := ChDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE user SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then ShowMessage('Banco de dados teste n�o se conecta!');
    end;
  end;
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Application.MessageBox(PChar('M�quina cliente sem rede.'), 'Erro', MB_OK+MB_ICONERROR);
        //Application.Terminate;
      end;
    end;
  except
    Application.MessageBox(PChar('Teste'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  }
  Geral.DefineFormatacoes;
  //
  {
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Host := VAR_IP;
  MyDB.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES');
  QrAux.Open;
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  MyDB.Close;
  MyDB.DataBaseName := BD;
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Application.MessageBox(PChar('O banco de dados '+TMeuDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?'), 'Banco de Dados',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      MyDB.DatabaseName := TMeuDB;
    end else if Resp = ID_CANCEL then
    begin
      Application.MessageBox('O aplicativo ser� encerrado!', 'Banco de Dados',
      MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  //
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  QrAuxL.Open;
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Application.MessageBox(PChar('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?'),
      'Banco de Dados Local', MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Application.MessageBox('O aplicativo ser� encerrado!',
      'Banco de Dados Local', MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
  VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    0, HKEY_LOCAL_MACHINE);
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  Verifica := False;
  if Versao < CO_Versao then Verifica := True;
  if VerZero = 0 then
  begin
    Resp := Application.MessageBox('N�o h� informa��o de vers�o no registro, '+
    'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
    'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
    'banco de dados. Confirma a Verifica��o?',
    'Aus�ncia de Informa��o de Vers�o no Registro',
    MB_YESNOCANCEL+MB_ICONWARNING);
    if Resp = ID_YES then Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  if Verifica then
  begin
    Application.CreateForm(TFmVerifiDB, FmVerifiDB);
    with FmVerifiDb do
    begin
      BtSair.Enabled := False;
      FVerifi := True;
      ShowModal;
      FVerifi := False;
      Destroy;
    end;
  end;
  //
  try
    QrMaster.Open;
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
      Application.CreateForm(TFmVerifiDB, FmVerifiDB);
      with FmVerifiDb do
      begin
        BtSair.Enabled := False;
        FVerifi := True;
        ShowModal;
        FVerifi := False;
        Destroy;
      end;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;
  if VAR_SERVIDOR = 2 then GOTOy.LiberaUso;
  VAR_MyBDFINANCAS := MyDB;  // MyPagtos : CASHIER
  //FmPrincipal.VerificaTerminal;
  MLAGeral.ConfiguracoesIniciais(1, MyDB.DatabaseName);
  }
  MyDB.Connect;

end;

procedure TDmod.RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
var
  Saldo: Double;
begin
  if Tipo < 2 then
  begin
    QrSomaM.Close;
    QrSomaM.SQL.Clear;
    QrSomaM.SQL.Add('SELECT Inicial Valor FROM carteiras');
    QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Codigo=:P1');
    QrSomaM.Params[0].AsInteger := Tipo;
    QrSomaM.Params[1].AsInteger := Carteira;
    QrSomaM.Open;
    Saldo := QrSomaMValor.Value;
  end else Saldo := 0;
  QrSomaM.Close;
  QrSomaM.SQL.Clear;
  if Tipo = 2 then begin
    QrSomaM.SQL.Add('SELECT SUM(IF(Sit=0, (Credito-Debito),');
    QrSomaM.SQL.Add('IF(Sit=1, (Credito-Debito+Pago), 0))) Valor FROM lanctos');
  end else
    QrSomaM.SQL.Add('SELECT (SUM(Credito) - SUM(Debito)) Valor FROM lanctos');
  QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
  QrSomaM.Params[0].AsInteger := Tipo;
  QrSomaM.Params[1].AsInteger := Carteira;
  QrSomaM.Open;
  Saldo := Saldo + QrSomaMValor.Value;
  QrSomaM.Close;

  QrUpdM.Close;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE carteiras SET Saldo=:P0');
  QrUpdM.SQL.Add('WHERE Tipo=:P1 AND Codigo=:P2');
  QrUpdM.Params[0].AsFloat := Saldo;
  QrUpdM.Params[1].AsInteger := Tipo;
  QrUpdM.Params[2].AsInteger := Carteira;
  QrUpdM.ExecSQL;
(*  if Localiza = 3 then
  begin
    if Dmod.QrLanctos.State in [dsBrowse] then
    if (Dmod.QrLanctosTipo.Value = Tipo)
    and (Dmod.QrLanctosCarteira.Value = Carteira) then
    Localiza := 1;
    Dmod.DefParams;
  end;
  if Localiza = 1 then GOTOx.LocalizaCodigo(Carteira, Carteira);*)
end;

procedure TDmod.VerificaSenha(Index: Integer; Login, Senha: String);
begin
  if (Senha = VAR_BOSS) or (Senha = CO_MASTER) then VAR_SENHARESULT := 2
  else
  begin
    if Index = 7 then
    begin
(*      QrSenha.Close;
      QrSenha.Params[0].AsString := Login;
      QrSenha.Params[1].AsString := Senha;
      QrSenha.Open;
      if QrSenha.RecordCount > 0 then
      begin
        QrPerfis.Close;
        QrPerfis.Params[0].AsInteger := QrSenhaPerfilW.Value;
        QrPerfis.Open;
        if QrPerfisVendas.Value = 'V' then VAR_SENHARESULT := 2
        else MessageBox(0,'Acesso negado. Senha Inv�lida',
        'Permiss�o por senha', MB_OK+MB_ICONEXCLAMATION);
        QrPerfis.Close;
      end else ShowMessage('Login inv�lido.');
      QrSenha.Close;                      *)
    end else ShowMessage('Em constru��o');
  end;
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
  ShowMessage('CNPJ n�o definido ou Empresa n�o definida. '+Chr(13)+Chr(10)+
  'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.');
  FmPrincipal.Caption := Application.Title+'  ::  '+QrMasterEm.Value+' # '+
  QrMasterCNPJ.Value;
  //
  QrControle.Close;
  QrControle.Open;
  if QrControleSoMaiusculas.Value = 'V' then VAR_SOMAIUSCULAS := True;
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_CIDADEPADRAO    := QrControleCidade.Value;
  VAR_TRAVACIDADE     := QrControleTravaCidade.Value;
  VAR_MOEDA           := QrControleMoeda.Value;
  //
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
  //
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
(*  case QrMasterTipo.Value of
    0: QrMasterEm.Value := QrMasterRazaoSocial.Value;
    1: QrMasterEm.Value := QrMasterNome.Value;
    else QrMasterEm.Value := QrMasterRazaoSocial.Value;
  end;*)
  QrMasterTE1_TXT.Value := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value :=Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

function TDmod.ConcatenaRegistros(Matricula: Integer): String;
(*var
  Registros, Conta: Integer;
  Liga: String;*)
begin
(*  Qr??.Close;
  Qr??.Params[0].AsInteger := Matricula;
  Qr??.Open;
  //
  Registros := MLAGeral.Registros(Qr??);
  if Registros > 1 then Result := 'Pagamento de '
  else if Registros = 1 then Result := 'Pagamento de ';
  Result := Result + Qr??NOMECURSO.Value;
  Qr??.Next;
  Conta := 1;
  Liga := ', ';
  while not Qr??.Eof do
  begin
    Conta := Conta + 1;
    if Conta = Registros then Liga := ' e ';
    Result := Result + Liga + Qr??NOMECURSO.Value;
    Qr?
    ?.Next;
  end;*)
end;


procedure TDmod.TbMovCalcFields(DataSet: TDataSet);
begin
(*  TbMovSUBT.Value := TbMovQtde.Value * TbMovPreco.Value;
  TbMovTOTA.Value := TbMovSUBT.Value - TbMovDesco.Value;
  case TbMovTipo.Value of
    -1: TbMovNOMETIPO.Value := 'Venda';
     1: TbMovNOMETIPO.Value := 'Compra';
    else TbMovNOMETIPO.Value := 'INDEFINIDO';
  end;
  case TbMovENTITIPO.Value of
    0: TbMovNOMEENTIDADE.Value := TbMovENTIRAZAO.Value;
    1: TbMovNOMEENTIDADE.Value := TbMovENTINOME.Value;
    else TbMovNOMEENTIDADE.Value := 'INDEFINIDO';
  end;
  if TbMovESTQQ.Value <> 0 then
  TbMovCUSTOPROD.Value := TbMovESTQV.Value / TbMovESTQQ.Value else
  TbMovCUSTOPROD.Value := 0;*)
end;

procedure TDmod.TbMovBeforePost(DataSet: TDataSet);
begin
(*  case TbMovTipo.Value of
   -1:
    begin
      TbMovTotalV.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
      TbMovTotalC.Value := TbMovQtde.Value * TbMovCUSTOPROD.Value;
    end;
    1:
    begin
      TbMovTotalV.Value := 0;
      TbMovTotalC.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
    end;
    else begin
      //
    end;
  end;*)
end;

procedure TDmod.TbMovBeforeDelete(DataSet: TDataSet);
begin
  //FProdDelete := TbMovProduto.Value;
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  WSACleanup;
end;

function TDmod.ObtemIP(Retorno: Integer): String;
var
  p: PHostEnt;
  s: array[0..128] of char;
  p2: PChar;
begin
  GetHostName(@s, 128);
  p := GetHostByName(@s);
  p2 := iNet_ntoa(PInAddr(p^.h_addr_list^)^);
  case Retorno of
    0: Result := p^.h_Name;
    1: Result := p2;
    else Result := '';
  end;
end;

function TDMod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  FM_MASTER := 'F';
  Dmod.QrPerfis.Params[0].AsInteger := Usuario;
  Dmod.QrPerfis.Open;
  if Dmod.QrPerfis.RecordCount > 0 then
  begin
    Result := True;
    //FM_EQUIPAMENTOS             := Dmod.QrPerfisEquipamentos.Value;
    //FM_MARCAS          := Dmod.QrPerfisMarcas.Value;
    //FM_MODELOS                := Dmod.QrPerfisModelos.Value;
    //FM_SERVICOS            := Dmod.QrPerfisServicos.Value;
    //FM_PRODUTOS            := Dmod.QrPerfisProdutos.Value;
  end;
  Dmod.QrPerfis.Close;
end;

procedure TDmod.QrEnderecoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrEnderecoTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEnderecoTe1.Value);
  QrEnderecoFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEnderecoFax.Value);
  QrEnderecoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEnderecoCNPJ_CPF.Value);
  //
  QrEnderecoNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEnderecoRua.Value, QrEnderecoNumero.Value, False);
  QrEnderecoE_ALL.Value := QrEnderecoNOMELOGRAD.Value;
  if Trim(QrEnderecoE_ALL.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ';
  QrEnderecoE_ALL.Value := QrEnderecoE_ALL.Value + QrEnderecoRua.Value;
  if Trim(QrEnderecoRua.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', ' + QrEnderecoNUMERO_TXT.Value;
  if Trim(QrEnderecoCompl.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ' + QrEnderecoCompl.Value;
  if Trim(QrEnderecoBairro.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoBairro.Value;
  if QrEnderecoCEP.Value > 0 then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  if Trim(QrEnderecoCidade.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoCidade.Value;
  //
  QrEnderecoECEP_TXT.Value :=Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  //
  if QrEnderecoTipo.Value = 0 then
  begin
    Natal := QrEnderecoENatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CNPJ';
  end else begin
    Natal := QrEnderecoPNatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CPF';
  end;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

procedure TDmod.DefParams;
begin
  //FmPrincipal.DefParams;
end;

procedure TDmod.LocCod(Atual, Codigo: Integer);
begin
  //FmPrincipal.LocCod(Atual, Codigo);
end;

function TDmod.BuscaProximoMovix: Integer;
begin
  QrSel.Close;
  QrSel.Open;
  Result := QrSelMovix.Value;
end;

procedure TDmod.QrDonoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrDonoTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoTe1.Value);
  QrDonoTE2_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoTe2.Value);
  QrDonoFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoFax.Value);
  QrDonoCEL_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoECel.Value);
  QrDonoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrDonoCNPJ_CPF.Value);
  //
  QrDonoNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrDonoRua.Value, Trunc(QrDonoNumero.Value), False);
  QrDonoE_LNR.Value := QrDonoNOMELOGRAD.Value;
  if Trim(QrDonoE_LNR.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ';
  QrDonoE_LNR.Value := QrDonoE_LNR.Value + QrDonoRua.Value;
  if Trim(QrDonoRua.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ', ' + QrDonoNUMERO_TXT.Value;
  if Trim(QrDonoCompl.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ' + QrDonoCompl.Value;
  if Trim(QrDonoBairro.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' - ' + QrDonoBairro.Value;
  //
  QrDonoE_LN2.Value := '';
  if Trim(QrDonoCidade.Value) <>  '' then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + ' - '+QrDonoCIDADE.Value;
  QrDonoE_LN2.Value := QrDonoE_LN2.Value + ' - '+QrDonoNOMEUF.Value;
  if QrDonoCEP.Value > 0 then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + '  CEP ' +Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  QrDonoE_CUC.Value := QrDonoE_LNR.Value + QrDonoE_LN2.Value;
  //
  QrDonoECEP_TXT.Value :=Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  if QrDonoTipo.Value = 0 then Natal := QrDonoENatal.Value
  else Natal := QrDonoPNatal.Value;
  if Natal < 2 then QrDonoNATAL_TXT.Value := ''
  else QrDonoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
  //
  if Trim(QrDonoTE1.Value) <> '' then
    QrDonoFONES.Value := QrDonoTE1_TXT.Value;
  if Trim(QrDonoTe2.Value) <> '' then
    QrDonoFONES.Value := QrDonoFONES.Value + ' - ' + QrDonoTE2_TXT.Value;
end;

end.

