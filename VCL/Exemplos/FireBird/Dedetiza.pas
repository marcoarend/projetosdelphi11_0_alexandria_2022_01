unit Dedetiza;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnDmkProcFunc;

type
  TFmDedetiza = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    QrTabelas: TmySQLQuery;
    DsTabelas: TDataSource;
    DBGrid1: TDBGrid;
    DBAnt: TmySQLDatabase;
    QrTabelasTabela: TStringField;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    QrDados: TmySQLQuery;
    DsDados: TDataSource;
    DBGrid2: TDBGrid;
    Splitter1: TSplitter;
    Panel5: TPanel;
    Splitter2: TSplitter;
    Panel6: TPanel;
    Label1: TLabel;
    EdCampo: TEdit;
    DBMemo1: TDBMemo;
    GroupBox2: TGroupBox;
    CkCliente: TCheckBox;
    CkFornece: TCheckBox;
    CkFuncion: TCheckBox;
    Panel7: TPanel;
    BtImporta: TBitBtn;
    QrClientesIB: TmySQLQuery;
    DsClientesIB: TDataSource;
    QrClientesIBAUTOINC: TIntegerField;
    QrClientesIBCODCLI: TIntegerField;
    QrClientesIBNOMECLI: TStringField;
    QrClientesIBTPCLI: TStringField;
    QrClientesIBENDERECO: TStringField;
    QrClientesIBBAIRRO: TStringField;
    QrClientesIBID_CIDADE: TIntegerField;
    QrClientesIBCIDADE: TStringField;
    QrClientesIBESTADO: TStringField;
    QrClientesIBCEP: TStringField;
    QrClientesIBRG: TStringField;
    QrClientesIBCPF: TStringField;
    QrClientesIBFONE: TStringField;
    QrClientesIBFAX: TStringField;
    QrClientesIBCELULAR: TStringField;
    QrClientesIBCONTATOS: TStringField;
    QrClientesIBEMAIL: TStringField;
    QrClientesIBHOMEPAGE: TStringField;
    QrClientesIBOBS: TMemoField;
    QrClientesIBDATACON: TDateField;
    QrClientesIBREFERENCIA: TStringField;
    QrClientesIBCAIXA: TStringField;
    QrClientesIBAREA: TStringField;
    QrClientesIBINTERNO: TMemoField;
    QrClientesIBID_VEND: TIntegerField;
    QrClientesIBEXTERNO: TMemoField;
    QrClientesIBEND_COBR: TStringField;
    QrClientesIBBAIRRO_COBR: TStringField;
    QrClientesIBCIDADE_COBR: TStringField;
    QrClientesIBESTADO_COBR: TStringField;
    QrClientesIBCEP_COBR: TStringField;
    QrClientesIBPROPA: TIntegerField;
    QrClientesIBASP1: TIntegerField;
    QrClientesIBASP2: TIntegerField;
    QrClientesIBASP3: TIntegerField;
    QrClientesIBASP4: TIntegerField;
    QrClientesIBASP5: TIntegerField;
    QrClientesIBASP6: TIntegerField;
    QrClientesIBASP7: TIntegerField;
    QrClientesIBASP7_OUTROS: TStringField;
    QrClientesIBCOND1: TIntegerField;
    QrClientesIBCOND2: TIntegerField;
    QrClientesIBCOND3: TIntegerField;
    QrClientesIBCOND4: TIntegerField;
    QrClientesIBCOND5: TIntegerField;
    QrClientesIBCOND6: TIntegerField;
    QrClientesIBCOND6_OUTROS: TStringField;
    QrClientesIBID_EMPRESA: TIntegerField;
    QrClientesIBFANTASIA: TStringField;
    QrClientesIBNUMERO: TStringField;
    QrClientesIBCOMPLEMENTO: TStringField;
    QrClientesIBIBGE: TIntegerField;
    QrClientesIBID_CARTFIDEL: TIntegerField;
    QrClientesIBSATISFACAO: TStringField;
    QrClientesIBOBSSATISFACAO: TMemoField;
    QrClientesIBRESTRICAO: TStringField;
    QrClientesIBID_COLETALIXO: TIntegerField;
    QrExiste: TmySQLQuery;
    QrExisteCodigo: TIntegerField;
    CkAltera: TCheckBox;
    PB1: TProgressBar;
    QrAnt: TmySQLQuery;
    QrClientesIBCOD_CIDADE: TIntegerField;
    RGArea: TRadioGroup;
    QrFornecedIB: TmySQLQuery;
    DsFornecedIB: TDataSource;
    QrFornecedIBCOD_CIDADE: TIntegerField;
    QrFornecedIBAUTOINC: TIntegerField;
    QrFornecedIBCODFORN: TIntegerField;
    QrFornecedIBRAZFORN: TStringField;
    QrFornecedIBENDFORN: TStringField;
    QrFornecedIBCONTATO: TStringField;
    QrFornecedIBBAIRRO: TStringField;
    QrFornecedIBID_CIDADE: TIntegerField;
    QrFornecedIBCIDADE: TStringField;
    QrFornecedIBESTADO: TStringField;
    QrFornecedIBINSCR: TStringField;
    QrFornecedIBCNPJ: TStringField;
    QrFornecedIBCEP: TStringField;
    QrFornecedIBFONE: TStringField;
    QrFornecedIBFAX: TStringField;
    QrFornecedIBCELULAR: TStringField;
    QrFornecedIBEMAIL: TStringField;
    QrFornecedIBHOMEPAGE: TStringField;
    QrFornecedIBOBS: TMemoField;
    QrFornecedIBID_EMPRESA: TIntegerField;
    QrFuncionarios: TmySQLQuery;
    DsFuncionarios: TDataSource;
    QrFuncionariosAUTOINC: TIntegerField;
    QrFuncionariosID_FUNC: TIntegerField;
    QrFuncionariosNOME: TStringField;
    QrFuncionariosFONE: TStringField;
    QrFuncionariosDTA_CONTR: TDateField;
    QrFuncionariosDTA_DEMISS: TDateField;
    QrFuncionariosCARGO: TStringField;
    QrFuncionariosSETOR: TStringField;
    QrFuncionariosAJ_CUSTO: TFloatField;
    QrFuncionariosCOMISSAO: TFloatField;
    QrFuncionariosVL_TRANSP: TIntegerField;
    QrFuncionariosVL_ALIM: TIntegerField;
    QrFuncionariosCONVENIO: TIntegerField;
    QrFuncionariosCESTA_BAS: TIntegerField;
    QrFuncionariosSALARIO: TFloatField;
    QrFuncionariosID_EMPRESA: TIntegerField;
    QrFuncionariosECIVIL: TStringField;
    QrFuncionariosDTA_NASC: TDateField;
    QrFuncionariosCARTPROF: TStringField;
    QrFuncionariosRG: TStringField;
    QrFuncionariosID_DEMISS: TIntegerField;
    QrFuncionariosCPF: TStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTabelasBeforeClose(DataSet: TDataSet);
    procedure QrTabelasAfterScroll(DataSet: TDataSet);
    procedure QrDadosBeforeClose(DataSet: TDataSet);
    procedure EdCampoExit(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    //FTipoFone, FTipoFax, FTipoCelular: Integer;
    //
    procedure ImportaClientes();
    procedure ImportaFornecedores();
    procedure ImportaFuncionarios();
    //
    procedure IncluiAlteraFone(const Codigo, Controle: Integer; const Telefone,
              Ramal, Nome: String(*; var CodRes: Integer*));
    procedure IncluiAlteraMail(const Codigo, Controle: Integer; const EMail,
              Nome: String(*; var CodRes: Integer*));
  public
    { Public declarations }
  end;

  var
  FmDedetiza: TFmDedetiza;

implementation

uses UnMyObjects, MyMySQL, Module;


{$R *.DFM}

const
  NomeSYS = '[* IMPORTADO *]';

procedure TFmDedetiza.BtImportaClick(Sender: TObject);
begin
  //
  LaAviso1.Font.Color := clSilver;
  LaAviso2.Font.Color := clRed;
  //
  if CkCliente.Checked and (RGArea.ItemIndex < 0) then
  begin
    Geral.MensagemBox('Informe o tipo de �rea no cadastro do cliente!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if CkCliente.Checked then ImportaClientes();
  //
  if CkFornece.Checked then ImportaFornecedores();
  //
  if CkFuncion.Checked then ImportaFuncionarios();
  //
    Geral.MensagemBox('Importa��o finalizada!',
    'Mensagem', MB_OK+MB_ICONINFORMATION);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmDedetiza.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDedetiza.EdCampoExit(Sender: TObject);
begin
  if QrDados.State <> dsInactive then
  begin
    DBMemo1.DataSource := DsDados;
    DBMemo1.DataField := EdCampo.Text;
  end;
end;

procedure TFmDedetiza.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmDedetiza.FormCreate(Sender: TObject);
begin
  QrExiste.Database := Dmod.MyDB;
  (*
  FTipoFone := 0;
  FTipoFax := 0;
  FTipoCelular := 0;
  *)
end;

procedure TFmDedetiza.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDedetiza.ImportaClientes();
var
  I: Integer;
  Antigo, CNPJ_CPF, CNPJ, CPF, RazaoSocial, Nome, Fantasia, Apelido, IE, RG,
  xLogr, nLogr, xRua, Atividade, URL, Num, CEP, Observacoes, Cliente1,
  ERua, ECompl, EBairro, ECidade, EPAIS, ETe1, EFax, ECel, EEmail, EContato,
  PRua, PCompl, PBairro, PCidade, PPAIS, PTe1, PFax, PCel, PEmail, PContato,
  EEndeRef, PEndeRef, CRua, CBairro, CCidade, CPais, Te1, Fax, Cel: String;
  Tipo, ELograd, ENumero, PLograd, PNumero, EUF, PUF, ECEP, PCEP, CUF, CCEP,
  Codigo, CodUsu, ECodMunici, ECodiPais, PCodMunici, PCodiPais, COD_CIDADE,
  PAtividad, EAtividad: Integer;
  SQLType: TSQLType;
  //
  DataCon, Contatos: String;
  Controle, Cargo, Conta, EntTipCto, AtivPrinc, HowFind, Account: Integer;
  //
  SRua, SCompl, SCompl2, SBairro, SCidade, SUF, SPais, SEndeRef, Mail, STe1: String;
  Cliente, SLograd, SNumero, SCEP, UF, SCodMunici, SCodiPais: Integer;
  M2Constru, M2NaoBuild, M2Terreno, M2Total: Double;
  //
  SiapImaTer, Objeto, Finalidade, TpConstru: Integer;
begin
  if Geral.MensagemBox(
  'Deseja excluir os cadastros das tabelas envolvidas antes de importar os clientes?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo dados!');
    Dmod.MyDB.Execute('DELETE FROM entitipcto WHERE Codigo > 0');
    Dmod.MyDB.Execute('DELETE FROM entitel WHERE Codigo > 0');
    Dmod.MyDB.Execute('DELETE FROM entimail WHERE Codigo > 0');
    Dmod.MyDB.Execute('DELETE FROM enticontat WHERE Codigo > 0');
    Dmod.MyDB.Execute('DELETE FROM entidades WHERE Codigo > 0');
    Dmod.MyDB.Execute('DELETE FROM howfounded WHERE Codigo > 0');
    Dmod.MyDB.Execute('DELETE FROM siaptercad WHERE Codigo > 0');
    Dmod.MyDB.Execute('DELETE FROM siapimacad WHERE Codigo > 0');
    //
    Dmod.MyDB.Execute('UPDATE controle SET entitipcto=0, entidades=0, ' +
    'entitel=0, entimail=0, enticontat=0');
    Dmod.MyDB.Execute('DELETE FROM gerlseq1 WHERE Tabela="howfounded"');
    Dmod.MyDB.Execute('DELETE FROM gerlseq1 WHERE Tabela="siaptercad"');
    Dmod.MyDB.Execute('DELETE FROM gerlseq1 WHERE Tabela="siapimacad"');
    //
    UnMyMySQL.AbreMySQLQuery1(QrAnt, [
    'SELECT MAX(PROPA) PROPA ',
    'FROM clientesib',
    '']);
    //N := 0;
    for I := 0 to QrAnt.FieldByName('PROPA').AsInteger do
    begin
     Codigo := UnMyMySQL.BPGS1I32('howfounded', 'Codigo', '', '', tsPos, stIns, 0);
     //N := N + 1;
      Nome := '[Cad. Propa.] ' + Geral.FFN(Codigo, 3);
      UnMyMySQL.SQLInsUpd(Dmod.QrUpd, stIns, 'howfounded', False, [
      'Codigo', 'Nome'], [], [Codigo, Nome], [], True);
    end;
    // FIM EXCUS�ES
    //Codigo := 0;
  end;
  //
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela (origem) de clientes');
  QrClientesIB.Close;
  QrClientesIB.Open;
  PB1.Max := QrClientesIB.RecordCount;
  //
  while not QrClientesIB.Eof do
  begin
    Antigo := 'CLI' + Geral.FFN(QrClientesIBCODCLI.Value, 8);
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Verificando cliente: ' + Antigo);

    CNPJ_CPF := Geral.SoNumero_TT(QrClientesIBCPF.Value);
    if Length(CNPJ_CPF) = 0 then
    begin
      if UPPERCASE(QrClientesIBTPCli.Value) = 'RESIDENCIAL' then
        Tipo := 1
      else
        Tipo := 0;
    end else
    begin
      if Length(CNPJ_CPF) > 11 then
        Tipo := 0
      else Tipo := 1;
    end;

    Geral.SeparaLogradouro(0, QrClientesIBENDERECO.Value,  xLogr, nLogr, xRua, nil, nil);
    //
    //ERRO! nLogr := Geral.SoNumero_TT(nLogr);
    if nLogr = '' then
      nLogr := '0';
    //
    Num := Geral.SoNumero_TT(QrClientesIBNUMERO.Value);
    if Num = '' then
      Num := '0';
    //
    CEP := Geral.SoNumero_TT(QrClientesIBCEP.Value);
    if CEP = '' then
      CEP := '0';

    Te1 := Geral.SoNumero_TT(QrClientesIBFONE.Value);
    if Length(Te1) < 3 then
      Te1 := '';
    //
    Fax := Geral.SoNumero_TT(QrClientesIBFAX.Value);
    if Length(Fax) < 3 then
      Fax := '';
    //
    Cel := Geral.SoNumero_TT(QrClientesIBCELULAR.Value);
    if Length(Cel) < 3 then
      Cel := '';
    //
    RazaoSocial    := '';
    Fantasia       := '';
    CNPJ           := '';
    IE             := '';
    //
    ELograd        := 0;
    ERua           := '';
    ENumero        := 0;
    ECompl         := '';
    EBairro        := '';
    ECidade        := '';
    EUF            := 0;
    ECEP           := 0;
    EPAIS          := '';
    ETe1           := '';
    EFax           := '';
    ECel           := '';
    EEmail         := '';
    EContato       := '';
    EEndeRef       := '';
    EAtividad      := 0;
    ECodMunici     := 0;
    ECodiPais      := 0;

    /////////////////////

    Nome           := '';
    Apelido        := '';
    CPF            := '';
    RG             := '';
    //
    PLograd        := 0;
    PRua           := '';
    PNumero        := 0;
    PCompl         := '';
    PBairro        := '';
    PCidade        := '';
    PUF            := 0;
    PCEP           := 0;
    PPAIS          := '';
    PTe1           := '';
    PFax           := '';
    PCel           := '';
    PEmail         := '';
    PContato       := '';
    PEndeRef       := '';
    PAtividad      := 0;
    PCodMunici     := 0;
    PCodiPais      := 0;
    //
    COD_CIDADE := QrClientesIBCOD_CIDADE.Value;
    if (COD_CIDADE < 3000000) or (COD_CIDADE >= 5000000) then
      COD_CIDADE := 0;
   // QrClientesIB.Value;
    Mail := LowerCase(Trim(QrClientesIBEMAIL.Value));
    if Tipo = 0 then
    begin
      RazaoSocial    := QrClientesIBNOMECLI.Value;
      Fantasia       := QrClientesIBFANTASIA.Value;
      CNPJ           := CNPJ_CPF;
      IE             := Geral.SoNumero_TT(QrClientesIBRG.Value);
      //
      ELograd        := Geral.IMV(nLogr);
      ERua           := xRua;
      ENumero        := Geral.IMV(Num);
      ECompl         := QrClientesIBCOMPLEMENTO.Value;
      EBairro        := QrClientesIBBAIRRO.Value;
      ECidade        := QrClientesIBCIDADE.Value;
      EUF            := Geral.GetCodigoUF_da_SiglaUF(QrClientesIBESTADO.Value);
      ECEP           := Geral.IMV(CEP);
      EPAIS          := 'BRASIL';
      ETe1           := Te1;
      EFax           := Fax;
      ECel           := Cel;
      EEmail         := Mail;
      EContato       := QrClientesIBCONTATOS.Value;
      EEndeRef       := QrClientesIBREFERENCIA.Value;
      ECodMunici     := COD_CIDADE;
      ECodiPais      := 1058;

    end else
    begin
      Nome           := QrClientesIBNOMECLI.Value;
      Apelido        := QrClientesIBFANTASIA.Value;
      CPF            := CNPJ_CPF;
      RG             := Geral.SoNumero_TT(QrClientesIBRG.Value);
      //
      PLograd        := Geral.IMV(nLogr);
      PRua           := xRua;
      PNumero        := Geral.IMV(Num);
      PCompl         := QrClientesIBCOMPLEMENTO.Value;
      PBairro        := QrClientesIBBAIRRO.Value;
      PCidade        := QrClientesIBCIDADE.Value;
      PUF            := Geral.GetCodigoUF_da_SiglaUF(QrClientesIBESTADO.Value);
      PCEP           := Geral.IMV(CEP);
      PPAIS          := 'BRASIL';
      PTe1           := Te1;
      PFax           := Fax;
      PCel           := Cel;
      PEmail         := Mail;
      PContato       := QrClientesIBCONTATOS.Value;
      PEndeRef       := QrClientesIBREFERENCIA.Value;
      PCodMunici     := COD_CIDADE;
      PCodiPais      := 1058;
    end;

    Atividade   := QrClientesIBTPCLI.Value;
    URL         := LowerCase(QrClientesIBHOMEPAGE.Value);
    Observacoes := QrClientesIBOBS.Value;
    CRua        := QrClientesIBEND_COBR.Value;
    CBairro     := QrClientesIBBAIRRO_COBR.Value;
    CCidade     := QrClientesIBCIDADE_COBR.Value;
    CUF         := Geral.GetCodigoUF_da_SiglaUF(QrClientesIBESTADO_COBR.Value);
    CCEP        := Geral.IMV(Geral.SoNumero_TT(QrClientesIBCEP_COBR.Value));
    Cliente1    := 'V';
    CPais       := 'BRASIL';
    //
    UnMyMySQL.AbreMySQLQuery1(QrExiste, [
    'SELECT Codigo ',
    'FROM entidades ',
    'WHERE Antigo="' + Antigo + '"',
    '']);
    //
    if QrExiste.RecordCount = 1 then
    begin
      SQLType := stUpd;
      Codigo := QrExisteCodigo.Value;
      CodUsu := QrExisteCodigo.Value;
    end else
    begin
      SQLType := stIns;
      Codigo := UnMyMySQL.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
      CodUsu := Codigo;
    end;
    //
    if (SQLType = stIns) or (CkAltera.Checked) then
    begin
      if UnMyMySQL.SQLInsUpd(Dmod.QrUpd, SQLType, 'entidades', False, [
      'CodUsu', 'RazaoSocial', 'Fantasia',
      (*'Respons1', 'Respons2', 'Pai',
      'Mae',*) 'CNPJ', 'IE',
      (*'NIRE', 'FormaSociet', 'Simples',
      'IEST',*) 'Atividade', 'Nome',
      'Apelido', 'CPF', (*'CPF_Pai',
      'CPF_Conjuge',*) 'RG', (*'SSP',
      'DataRG', 'CidadeNatal', 'EstCivil',
      'UFNatal', 'Nacionalid',*) 'ELograd',
      'ERua', 'ENumero', 'ECompl',
      'EBairro', 'ECidade', 'EUF',
      'ECEP', 'EPais', 'ETe1',
      (*'Ete2', 'Ete3',*) 'ECel',
      'EFax', 'EEmail', 'EContato',
      (*'ENatal',*) 'PLograd', 'PRua',
      'PNumero', 'PCompl', 'PBairro',
      'PCidade', 'PUF', 'PCEP',
      'PPais', 'PTe1', (*'Pte2',
      'Pte3',*) 'PCel', 'PFax',
      'PEmail', 'PContato', (*'PNatal',
      'Sexo', 'Responsavel', 'Profissao',
      'Cargo', 'Recibo', 'DiaRecibo',
      'AjudaEmpV', 'AjudaEmpP',*) 'Cliente1',
      (*'Cliente2', 'Cliente3', 'Cliente4',
      'Fornece1', 'Fornece2', 'Fornece3',
      'Fornece4', 'Fornece5', 'Fornece6',
      'Fornece7', 'Fornece8', 'Terceiro',
      'Cadastro', 'Informacoes', 'Logo',
      'Veiculo', 'Mensal',*) 'Observacoes',
      'Tipo', (*'CLograd',*) 'CRua',
      (*'CNumero', 'CCompl',*) 'CBairro',
      'CCidade', 'CUF', 'CCEP',
      'CPais', (*'CTel', 'CCel',
      'CFax', 'CContato', 'LLograd',
      'LRua', 'LNumero', 'LCompl',
      'LBairro', 'LCidade', 'LUF',
      'LCEP', 'LPais', 'LTel',
      'LCel', 'LFax', 'LContato',
      'Comissao', 'Situacao', 'Nivel',
      'Grupo', 'Account', 'Logo2',
      'ConjugeNome', 'ConjugeNatal', 'Nome1',
      'Natal1', 'Nome2', 'Natal2',
      'Nome3', 'Natal3', 'Nome4',
      'Natal4', 'CreditosI', 'CreditosL',
      'CreditosF2', 'CreditosD', 'CreditosU',
      'CreditosV', 'Motivo', 'QuantI1',
      'QuantI2', 'QuantI3', 'QuantI4',
      'QuantN1', 'QuantN2', 'QuantN3',
      'Agenda', 'SenhaQuer', 'Senha1',
      'LimiCred', 'Desco', 'CasasApliDesco',
      'TempA', 'TempD', 'Banco',
      'Agencia', 'ContaCorrente', 'FatorCompra',
      'AdValorem', 'DMaisC', 'DMaisD',
      'Empresa', 'CBE', 'SCB',*)
      'PAtividad', 'EAtividad', (*'PCidadeCod',
      'ECidadeCod', 'PPaisCod', 'EPaisCod',*)
      'Antigo', (*'CUF2', 'Contab',
      'MSN1', 'PastaTxtFTP', 'PastaPwdFTP',
      'Protestar', 'MultaCodi', 'MultaDias',
      'MultaValr', 'MultaPerc', 'MultaTiVe',
      'JuroSacado', 'CPMF', 'Corrido',
      'CPF_Resp1', 'CliInt', 'AltDtPlaCt',
      'CartPref', 'RolComis', 'Filial',*)
      'EEndeRef', 'PEndeRef', (*'CEndeRef',
      'LEndeRef',*) 'ECodMunici', 'PCodMunici',
      (*'CCodMunici', 'LCodMunici', 'CNAE',
      'SUFRAMA',*) 'ECodiPais', 'PCodiPais',
      (*'L_CNPJ', 'L_Ativo', 'CNAE20',*)
      'URL'(*, 'CRT', 'COD_PART'*)], [
      'Codigo'], [
      CodUsu, RazaoSocial, Fantasia,
      (*Respons1, Respons2, Pai,
      Mae,*) CNPJ, IE,
      (*NIRE, FormaSociet, Simples,
      IEST,*) Atividade, Nome,
      Apelido, CPF, (*CPF_Pai,
      CPF_Conjuge,*) RG, (*SSP,
      DataRG, CidadeNatal, EstCivil,
      UFNatal, Nacionalid,*) ELograd,
      ERua, ENumero, ECompl,
      EBairro, ECidade, EUF,
      ECEP, EPais, ETe1,
      (*Ete2, Ete3,*) ECel,
      EFax, EEmail, EContato,
      (*ENatal,*) PLograd, PRua,
      PNumero, PCompl, PBairro,
      PCidade, PUF, PCEP,
      PPais, PTe1, (*Pte2,
      Pte3,*) PCel, PFax,
      PEmail, PContato, (*PNatal,
      Sexo, Responsavel, Profissao,
      Cargo, Recibo, DiaRecibo,
      AjudaEmpV, AjudaEmpP,*) Cliente1,
      (*Cliente2, Cliente3, Cliente4,
      Fornece1, Fornece2, Fornece3,
      Fornece4, Fornece5, Fornece6,
      Fornece7, Fornece8, Terceiro,
      Cadastro, Informacoes, Logo,
      Veiculo, Mensal,*) Observacoes,
      Tipo, (*CLograd,*) CRua,
      (*CNumero, CCompl,*) CBairro,
      CCidade, CUF, CCEP,
      CPais, (*CTel, CCel,
      CFax, CContato, LLograd,
      LRua, LNumero, LCompl,
      LBairro, LCidade, LUF,
      LCEP, LPais, LTel,
      LCel, LFax, LContato,
      Comissao, Situacao, Nivel,
      Grupo, Account, Logo2,
      ConjugeNome, ConjugeNatal, Nome1,
      Natal1, Nome2, Natal2,
      Nome3, Natal3, Nome4,
      Natal4, CreditosI, CreditosL,
      CreditosF2, CreditosD, CreditosU,
      CreditosV, Motivo, QuantI1,
      QuantI2, QuantI3, QuantI4,
      QuantN1, QuantN2, QuantN3,
      Agenda, SenhaQuer, Senha1,
      LimiCred, Desco, CasasApliDesco,
      TempA, TempD, Banco,
      Agencia, ContaCorrente, FatorCompra,
      AdValorem, DMaisC, DMaisD,
      Empresa, CBE, SCB,*)
      PAtividad, EAtividad, (*PCidadeCod,
      ECidadeCod, PPaisCod, EPaisCod,*)
      Antigo, (*CUF2, Contab,
      MSN1, PastaTxtFTP, PastaPwdFTP,
      Protestar, MultaCodi, MultaDias,
      MultaValr, MultaPerc, MultaTiVe,
      JuroSacado, CPMF, Corrido,
      CPF_Resp1, CliInt, AltDtPlaCt,
      CartPref, RolComis, Filial,*)
      EEndeRef, PEndeRef, (*CEndeRef,
      LEndeRef,*) ECodMunici, PCodMunici,
      (*CCodMunici, LCodMunici, CNAE,
      SUFRAMA,*) ECodiPais, PCodiPais,
      (*L_CNPJ, L_Ativo, CNAE20,*)
      URL(*, CRT, COD_PART*)], [
      Codigo], True) then
      begin
        if (Te1 <> '') or (Fax <> '') or (Cel <> '') or (Mail <> '') then
        begin
          //
          UnMyMySQL.AbreMySQLQuery1(QrExiste, [
          'SELECT Controle Codigo ',
          'FROM enticontat ',
          'WHERE Codigo=' + Geral.FF0(Codigo),
          'AND Nome="' + NomeSYS + '"',
          '']);
          if QrExisteCodigo.Value > 0 then
          begin
            SQLType := stUpd;
            Controle := QrExisteCodigo.Value
          end else
          begin
            SQLType := stIns;
            Controle := UnMyMySQL.BuscaEmLivreY_Def('enticontat', 'Controle', stIns, 0);
          end;
          Nome := NomeSYS;
          Cargo := 0;
          if UnMyMySQL.SQLInsUpd(Dmod.QrUpd, SQLType, 'enticontat', False, [
          'Codigo', 'Nome', 'Cargo'], [
          'Controle'], [
          Codigo, Nome, Cargo], [
          Controle], True) then
          begin
            if (Te1 <> '') then
              IncluiAlteraFone(Codigo, Controle, Te1, '', 'Fone [IMPORTADO]');
            if (Fax <> '') then
              IncluiAlteraFone(Codigo, Controle, Fax, '', 'Fax [IMPORTADO]');
            if (Cel <> '') then
              IncluiAlteraFone(Codigo, Controle, Cel, '', 'Celular [IMPORTADO]');
            if (Mail <> '') then
              IncluiAlteraMail(Codigo, Controle, Mail, '[IMPORTADO]');
          end;
        end;
        //
        Contatos := Geral.SeparaNomes(Trim(QrClientesIBCONTATOS.Value));
        while Contatos <> '' do
        begin
          if Geral.SeparaPrimeiraOcorrenciaDeTexto('|', Contatos, Nome, Contatos) then
          begin
            Nome := trim(Nome);
            if Nome <> '' then
            begin
              UnMyMySQL.AbreMySQLQuery1(QrExiste, [
              'SELECT Controle Codigo ',
              'FROM enticontat ',
              'WHERE Codigo=' + Geral.FF0(Codigo),
              'AND Nome="' + Nome + '"',
              '']);
              if QrExisteCodigo.Value > 0 then
              begin
                SQLType := stUpd;
                Controle := QrExisteCodigo.Value
              end else
              begin
                SQLType := stIns;
                Controle := UnMyMySQL.BuscaEmLivreY_Def('enticontat', 'Controle', stIns, 0);
              end;
              UnMyMySQL.SQLInsUpd(Dmod.QrUpd, SQLType, 'enticontat', False, [
              'Codigo', 'Nome', 'Cargo'], [
              'Controle'], [
              Codigo, Nome, Cargo], [
              Controle], True);
            end;
          end;
        end;
        //
        // Dados espec�ficos do cliente
        UnMyMySQL.AbreMySQLQuery1(QrExiste, [
        'SELECT Codigo ',
        'FROM cunscad ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
        if QrExisteCodigo.Value > 0 then
        begin
          SQLType := stUpd;
        end else
        begin
          SQLType := stIns;
        end;
        //
        AtivPrinc := 0;
        HowFind := QrClientesIBPROPA.Value;
{ TODO : ATUALIZAR CAMPO AP�S IMPORTAR FUNCION�RIOS! }        
        Account := -QrClientesIBID_VEND.Value;  // para poder mudar depois!
        DataCon := Geral.FDT(QrClientesIBDATACON.Value, 1);
        UnMyMySQL.SQLInsUpd(Dmod.QrUpd, SQLType, 'cunscad', False, [
        'AtivPrinc', 'HowFind', 'Account',
        'DataCon'], [
        'Codigo'], [
        AtivPrinc, HowFind, Account,
        DataCon], [
        Codigo], True);
        //
        // Cadastro do Terreno
        Nome           := '[* PADR�O *]';
        SLograd        := dmkPF.EscolhaDe2Int(Tipo = 0, ELograd    , PLograd    );
        SRua           := dmkPF.EscolhaDe2Str(Tipo = 0, ERua       , PRua       );
        SNumero        := dmkPF.EscolhaDe2Int(Tipo = 0, ENumero    , PNumero    );
        SCompl         := dmkPF.EscolhaDe2Str(Tipo = 0, ECompl     , PCompl     );
        SBairro        := dmkPF.EscolhaDe2Str(Tipo = 0, EBairro    , PBairro    );
        SCidade        := dmkPF.EscolhaDe2Str(Tipo = 0, ECidade    , PCidade    );
        UF             := dmkPF.EscolhaDe2Int(Tipo = 0, EUF        , PUF        );
        SCEP           := dmkPF.EscolhaDe2Int(Tipo = 0, ECEP       , PCEP       );
        SPais          := dmkPF.EscolhaDe2Str(Tipo = 0, EPais      , PPais      );
        SEndeRef       := dmkPF.EscolhaDe2Str(Tipo = 0, EEndeRef   , PEndeRef   );
        SCodMunici     := dmkPF.EscolhaDe2Int(Tipo = 0, ECodMunici , PCodMunici );
        STe1           := dmkPF.EscolhaDe2Str(Tipo = 0, ETe1       , PTe1       );
        SCodiPais      := 1058;
        M2Constru      := 0;
        M2NaoBuild     := 0;
        M2Terreno      := 0;
        M2Total        := 0;
        case RGArea.ItemIndex of
          0: ;//
          1: M2Constru  := Geral.DMV(QrClientesIBAREA.Value);
          2: M2NaoBuild := Geral.DMV(QrClientesIBAREA.Value);
          3: M2Terreno  := Geral.DMV(QrClientesIBAREA.Value);
        end;
        SUF := Geral.GetSiglaUF_do_CodigoUF(UF);
        //
        Cliente := Codigo;
        Codigo := UnMyMySQL.BPGS1I32('siaptercad', 'Codigo', '', '', tsPos, stIns, 0);
        if UnMyMySQL.SQLInsUpd(Dmod.QrUpd, stIns, 'siaptercad', False, [
        'Nome', 'Cliente', 'SLograd',
        'SRua', 'SNumero', 'SCompl',
        'SBairro', 'SCidade', 'SUF',
        'SCEP', 'SPais', 'SEndeRef',
        'SCodMunici', 'SCodiPais', 'STe1',
        'M2Constru', 'M2NaoBuild', 'M2Terreno', 'M2Total'], [
        'Codigo'], [
        Nome, Cliente, SLograd,
        SRua, SNumero, SCompl,
        SBairro, SCidade, SUF,
        SCEP, SPais, SEndeRef,
        SCodMunici, SCodiPais, STe1,
        M2Constru, M2NaoBuild, M2Terreno, M2Total], [
        Codigo], True) then ;
        //
        // Cadastro do im�vel
(*
var
  SCompl: String;
  Codigo, SiapImaTer, Objeto, Finalidade, TpConstru: Integer;
  M2Constru, M2NaoBuild, M2Terreno, M2Total: Double;
begin
*)
  //
        SiapImaTer := Codigo;
        Objeto := 0;
        { TODO : VER COM O ESLAUCO! }
        Finalidade := 0;//ObtemCodigoDaFinalidade(QrClientesIBTPCLI.Value);
        TpConstru := 0;
        SCompl2 := dmkPF.EscolhaDe2Str(Tipo = 0, ECompl, PCompl);
        //
        Codigo := UnMyMySQL.BPGS1I32('siapimacad', 'Codigo', '', '', tsPos, stIns, 0);
        if UnMyMySQL.SQLInsUpd(Dmod.QrUpd, stIns, 'siapimacad', False, [
        'SiapImaTer', 'Objeto', 'Finalidade',
        'TpConstru', 'SCompl2', 'M2Constru',
        'M2NaoBuild', 'M2Terreno', 'M2Total'], [
        'Codigo'], [
        SiapImaTer, Objeto, Finalidade,
        TpConstru, SCompl2, M2Constru,
        M2NaoBuild, M2Terreno, M2Total], [
        Codigo], True) then ;

  {
  -TPCLI
  -DATACON
  -CAIXA
  -AREA
  -INTERNO
  -ID_VEND
  -EXTERNO
  PROPA
  ASP1
  ASP2
  ASP3
  ASP4
  ASP5
  ASP6
  ASP7
  ASP7_OUTROS
  COND1
  COND2
  COND3
  COND4
  COND5
  COND6
  COND6_OUTROS
  ID_CARTFIDEL
  SATISFACAO
  OBSSATISFACAO
  RESTRICAO
  ID_COLETALIXO
  }
        //
      end;
    end;
    //
    QrClientesIB.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Importa��o de clientes finalizada!');
end;


procedure TFmDedetiza.ImportaFornecedores();
var
  //I: Integer;
  //, CPF, RG, Apelido, Atividade,
  Num, Nome, CNPJ_CPF, CNPJ, Antigo, RazaoSocial, Fantasia, IE,
  xLogr, nLogr, xRua, URL, CEP, Observacoes, Fornece1,
  ERua, ECompl, EBairro, ECidade, EPAIS, ETe1, EFax, ECel, EEmail, EContato,
  (*PRua, PCompl, PBairro, PCidade, PPAIS, PTe1, PFax, PCel, PEmail, PContato,
  PEndeRef, CRua, CBairro, CCidade, CPais, *)
  EEndeRef, Te1, Fax, Cel: String;
  (*PLograd, PNumero, PCEP, PUF, PCodMunici, PCodiPais*)
  //CUF, CCEP,
  Tipo, ELograd, ENumero, EUF, ECEP,
  Codigo, CodUsu, ECodMunici, ECodiPais, COD_CIDADE: Integer;
  SQLType: TSQLType;
  //
  //DataCon,
  Contatos: String;
  //Conta, AtivPrinc, EntTipCto, HowFind, Account,
  Controle, Cargo: Integer;
  //
(*
  SRua, SCompl, SBairro, SCidade, SUF, SPais, SEndeRef: String;
  Cliente, SLograd, SNumero, SCEP, UF, SCodMunici, SCodiPais: Integer;
  M2Constru, M2NaoBuild, M2Terreno, M2Total: Double;
  //
  SiapImaTer, Objeto, Finalidade, TpConstru: Integer;
*)
begin
(*
  if Geral.MensagemBox(
  'Deseja excluir os cadastros das tabelas envolvidas antes de importar os ??,
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo dados!');
    Dmod.MyDB.Execute('DELETE FROM entitipcto WHERE Codigo > 0');
    Dmod.MyDB.Execute('DELETE FROM entitel WHERE Codigo > 0');
    Dmod.MyDB.Execute('DELETE FROM enticontat WHERE Codigo > 0');
    Dmod.MyDB.Execute('DELETE FROM entidades WHERE Codigo > 0');
    Dmod.MyDB.Execute('DELETE FROM howfounded WHERE Codigo > 0');
    //
    Dmod.MyDB.Execute('UPDATE controle SET entitipcto=0, entidades=0, ' +
    'entitel=0, enticontat=0');
    Dmod.MyDB.Execute('UPDATE gerlseq1 SET BigIntPos=0, BigintNeg=0 ' +
      ' WHERE Tabela="howfounded" AND Campo="Codigo"');
    //
    UnMyMySQL.AbreMySQLQuery1(QrAnt, [
    'SELECT MAX(PROPA) PROPA ',
    'FROM fornecedib',
    '']);
    //N := 0;
    for I := 0 to QrAnt.FieldByName('PROPA').AsInteger do
    begin
     Codigo := UnMyMySQL.BPGS1I32('howfounded', 'Codigo', '', '', tsPos, stIns, 0);
     //N := N + 1;
      Nome := '[Cad. Propa.] ' + Geral.FFN(Codigo, 3);
      UnMyMySQL.SQLInsUpd(Dmod.QrUpd, stIns, 'howfounded', False, [
      'Codigo', 'Nome'], [], [Codigo, Nome], [], True);
    end;
    // FIM EXCUS�ES
    Codigo := 0;
  end;
  //
*)
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela (origem) de fornecedores');
  QrFornecedIB.Close;
  QrFornecedIB.Open;
  PB1.Max := QrFornecedIB.RecordCount;
  //
  while not QrFornecedIB.Eof do
  begin
    Antigo := 'FRN' + Geral.FFN(QrFornecedIBCODFORN.Value, 8);
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Verificando fornecedor: ' + Antigo);

    CNPJ_CPF := Geral.SoNumero_TT(QrFornecedIBCNPJ.Value);
    Tipo := 0;
    {
    if Length(CNPJ_CPF) = 0 then
    begin
      if UPPERCASE(QrFornecedIBi.Value) = 'RESIDENCIAL' then
        Tipo := 1
      else
        Tipo := 0;
    end else
    begin
      if Length(CNPJ_CPF) > 11 then
        Tipo := 0
      else Tipo := 1;
    end;
    }

    Geral.SeparaEndereco(0, QrFornecedIBENDFORN.Value, Num, ECompl,
      EBairro, xLogr, nLogr, xRua, False, nil, nil);
    //Geral.SeparaLogradouro(0, QrFornecedIBENDFORN.Value,  xLogr, nLogr, xRua, nil, nil);
    //
    //ERRO! nLogr := Geral.SoNumero_TT(nLogr);
    if nLogr = '' then
      nLogr := '0';
    //
    //Num := Geral.SoNumero_TT(QrFornecedIBNUMERO.Value);
    if Num = '' then
      Num := '0';
    //
    CEP := Geral.SoNumero_TT(QrFornecedIBCEP.Value);
    if CEP = '' then
      CEP := '0';

    Te1 := Geral.SoNumero_TT(QrFornecedIBFONE.Value);
    if Length(Te1) < 3 then
      Te1 := '';
    //
    Fax := Geral.SoNumero_TT(QrFornecedIBFAX.Value);
    if Length(Fax) < 3 then
      Fax := '';
    //
    Cel := Geral.SoNumero_TT(QrFornecedIBCELULAR.Value);
    if Length(Cel) < 3 then
      Cel := '';
    //
(*
    RazaoSocial    := '';
    Fantasia       := '';
    CNPJ           := '';
    IE             := '';
    //
    ELograd        := 0;
    ERua           := '';
    ENumero        := 0;
    ECompl         := '';
    EBairro        := '';
    ECidade        := '';
    EUF            := 0;
    ECEP           := 0;
    EPAIS          := '';
    ETe1           := '';
    EFax           := '';
    ECel           := '';
    EEmail         := '';
    EContato       := '';
    EEndeRef       := '';
    ECodMunici     := 0;
    ECodiPais      := 0;

    /////////////////////

    Nome           := '';
    Apelido        := '';
    CPF            := '';
    RG             := '';
    //
    PLograd        := 0;
    PRua           := '';
    PNumero        := 0;
    PCompl         := '';
    PBairro        := '';
    PCidade        := '';
    PUF            := 0;
    PCEP           := 0;
    PPAIS          := '';
    PTe1           := '';
    PFax           := '';
    PCel           := '';
    PEmail         := '';
    PContato       := '';
    PEndeRef       := '';
    PCodMunici     := 0;
    PCodiPais      := 0;
*)
    //
    COD_CIDADE := QrFornecedIBCOD_CIDADE.Value;
    if (COD_CIDADE < 3000000) or (COD_CIDADE >= 5000000) then
      COD_CIDADE := 0;

(*
    if Tipo = 0 then
    begin
*)
      RazaoSocial    := QrFornecedIBRAZFORN.Value;
      Fantasia       := RazaoSocial;
      CNPJ           := CNPJ_CPF;
      IE             := Geral.SoNumero_TT(QrFornecedIBINSCR.Value);
      //
      ELograd        := Geral.IMV(nLogr);
      ERua           := xRua;
      ENumero        := Geral.IMV(Num);
      //ECompl         := QrFornecedIBCOMPLEMENTO.Value;
      EBairro        := QrFornecedIBBAIRRO.Value;
      ECidade        := QrFornecedIBCIDADE.Value;
      EUF            := Geral.GetCodigoUF_da_SiglaUF(QrFornecedIBESTADO.Value);
      ECEP           := Geral.IMV(CEP);
      EPAIS          := 'BRASIL';
      ETe1           := Te1;
      EFax           := Fax;
      ECel           := Cel;
      EEmail         := LowerCase(QrFornecedIBEMAIL.Value);
      EContato       := QrFornecedIBCONTATO.Value;
      EEndeRef       := '';//QrFornecedIBREFERENCIA.Value;
      ECodMunici     := COD_CIDADE;
      ECodiPais      := 1058;

(*
    end else
    begin
      Nome           := QrFornecedIBNOMECLI.Value;
      Apelido        := QrFornecedIBFANTASIA.Value;
      CPF            := CNPJ_CPF;
      RG             := Geral.SoNumero_TT(QrFornecedIBRG.Value);
      //
      PLograd        := Geral.IMV(nLogr);
      PRua           := xRua;
      PNumero        := Geral.IMV(Num);
      PCompl         := QrFornecedIBCOMPLEMENTO.Value;
      PBairro        := QrFornecedIBBAIRRO.Value;
      PCidade        := QrFornecedIBCIDADE.Value;
      PUF            := Geral.GetCodigoUF_da_SiglaUF(QrFornecedIBESTADO.Value);
      PCEP           := Geral.IMV(CEP);
      PPAIS          := 'BRASIL';
      PTe1           := Te1;
      PFax           := Fax;
      PCel           := Cel;
      PEmail         := LowerCase(QrFornecedIBEMAIL.Value);
      PContato       := QrFornecedIBCONTATOS.Value;
      PEndeRef       := QrFornecedIBREFERENCIA.Value;
      PCodMunici     := COD_CIDADE;
      PCodiPais      := 1058;
    end;
*)

    //Atividade   := QrFornecedIBTPCLI.Value;
    URL         := LowerCase(QrFornecedIBHOMEPAGE.Value);
    Observacoes := QrFornecedIBOBS.Value;
    Fornece1    := 'V';
    //
    UnMyMySQL.AbreMySQLQuery1(QrExiste, [
    'SELECT Codigo ',
    'FROM entidades ',
    'WHERE Antigo="' + Antigo + '"',
    '']);
    //
    if QrExiste.RecordCount = 1 then
    begin
      SQLType := stUpd;
      Codigo := QrExisteCodigo.Value;
      CodUsu := QrExisteCodigo.Value;
    end else
    begin
      SQLType := stIns;
      Codigo := UnMyMySQL.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
      CodUsu := Codigo;
    end;
    //
    if (SQLType = stIns) or (CkAltera.Checked) then
    begin
      if UnMyMySQL.SQLInsUpd(Dmod.QrUpd, SQLType, 'entidades', False, [
      'CodUsu', 'RazaoSocial', 'Fantasia',
      (*'Respons1', 'Respons2', 'Pai',
      'Mae',*) 'CNPJ', 'IE',
      (*'NIRE', 'FormaSociet', 'Simples',
      'IEST', 'Atividade', 'Nome',
      'Apelido', 'CPF', 'CPF_Pai',
      'CPF_Conjuge', 'RG', 'SSP',
      'DataRG', 'CidadeNatal', 'EstCivil',
      'UFNatal', 'Nacionalid',*) 'ELograd',
      'ERua', 'ENumero', 'ECompl',
      'EBairro', 'ECidade', 'EUF',
      'ECEP', 'EPais', 'ETe1',
      (*'Ete2', 'Ete3',*) 'ECel',
      'EFax', 'EEmail', 'EContato',
      (*'ENatal', 'PLograd', 'PRua',
      'PNumero', 'PCompl', 'PBairro',
      'PCidade', 'PUF', 'PCEP',
      'PPais', 'PTe1', 'Pte2',
      'Pte3', 'PCel', 'PFax',
      'PEmail', 'PContato', 'PNatal',
      'Sexo', 'Responsavel', 'Profissao',
      'Cargo', 'Recibo', 'DiaRecibo',
      'AjudaEmpV', 'AjudaEmpP', 'Cliente1',
      'Cliente2', 'Cliente3', 'Cliente4',*)
      'Fornece1', (*'Fornece2', 'Fornece3',
      'Fornece4', 'Fornece5', 'Fornece6',
      'Fornece7', 'Fornece8', 'Terceiro',
      'Cadastro', 'Informacoes', 'Logo',
      'Veiculo', 'Mensal',*) 'Observacoes',
      'Tipo', (*'CLograd', 'CRua',
      'CNumero', 'CCompl', 'CBairro',
      'CCidade', 'CUF', 'CCEP',
      'CPais', 'CTel', 'CCel',
      'CFax', 'CContato', 'LLograd',
      'LRua', 'LNumero', 'LCompl',
      'LBairro', 'LCidade', 'LUF',
      'LCEP', 'LPais', 'LTel',
      'LCel', 'LFax', 'LContato',
      'Comissao', 'Situacao', 'Nivel',
      'Grupo', 'Account', 'Logo2',
      'ConjugeNome', 'ConjugeNatal', 'Nome1',
      'Natal1', 'Nome2', 'Natal2',
      'Nome3', 'Natal3', 'Nome4',
      'Natal4', 'CreditosI', 'CreditosL',
      'CreditosF2', 'CreditosD', 'CreditosU',
      'CreditosV', 'Motivo', 'QuantI1',
      'QuantI2', 'QuantI3', 'QuantI4',
      'QuantN1', 'QuantN2', 'QuantN3',
      'Agenda', 'SenhaQuer', 'Senha1',
      'LimiCred', 'Desco', 'CasasApliDesco',
      'TempA', 'TempD', 'Banco',
      'Agencia', 'ContaCorrente', 'FatorCompra',
      'AdValorem', 'DMaisC', 'DMaisD',
      'Empresa', 'CBE', 'SCB',
      'PAtividad', 'EAtividad', 'PCidadeCod',
      'ECidadeCod', 'PPaisCod', 'EPaisCod',*)
      'Antigo', (*'CUF2', 'Contab',
      'MSN1', 'PastaTxtFTP', 'PastaPwdFTP',
      'Protestar', 'MultaCodi', 'MultaDias',
      'MultaValr', 'MultaPerc', 'MultaTiVe',
      'JuroSacado', 'CPMF', 'Corrido',
      'CPF_Resp1', 'CliInt', 'AltDtPlaCt',
      'CartPref', 'RolComis', 'Filial',*)
      'EEndeRef', (*'PEndeRef', 'CEndeRef',
      'LEndeRef',*) 'ECodMunici', (*'PCodMunici',
      'CCodMunici', 'LCodMunici', 'CNAE',
      'SUFRAMA',*) 'ECodiPais', (*'PCodiPais',
      'L_CNPJ', 'L_Ativo', 'CNAE20',*)
      'URL'(*, 'CRT', 'COD_PART'*)], [
      'Codigo'], [
      CodUsu, RazaoSocial, Fantasia,
      (*Respons1, Respons2, Pai,
      Mae,*) CNPJ, IE,
      (*NIRE, FormaSociet, Simples,
      IEST, Atividade, Nome,
      Apelido, CPF, CPF_Pai,
      CPF_Conjuge, RG, SSP,
      DataRG, CidadeNatal, EstCivil,
      UFNatal, Nacionalid,*) ELograd,
      ERua, ENumero, ECompl,
      EBairro, ECidade, EUF,
      ECEP, EPais, ETe1,
      (*Ete2, Ete3,*) ECel,
      EFax, EEmail, EContato,
      (*ENatal, PLograd, PRua,
      PNumero, PCompl, PBairro,
      PCidade, PUF, PCEP,
      PPais, PTe1, Pte2,
      Pte3, PCel, PFax,
      PEmail, PContato, PNatal,
      Sexo, Responsavel, Profissao,
      Cargo, Recibo, DiaRecibo,
      AjudaEmpV, AjudaEmpP, Cliente1,
      Cliente2, Cliente3, Cliente4,*)
      Fornece1, (*Fornece2, Fornece3,
      Fornece4, Fornece5, Fornece6,
      Fornece7, Fornece8, Terceiro,
      Cadastro, Informacoes, Logo,
      Veiculo, Mensal,*) Observacoes,
      Tipo, (*CLograd, CRua,
      CNumero, CCompl, CBairro,
      CCidade, CUF, CCEP,
      CPais, CTel, CCel,
      CFax, CContato, LLograd,
      LRua, LNumero, LCompl,
      LBairro, LCidade, LUF,
      LCEP, LPais, LTel,
      LCel, LFax, LContato,
      Comissao, Situacao, Nivel,
      Grupo, Account, Logo2,
      ConjugeNome, ConjugeNatal, Nome1,
      Natal1, Nome2, Natal2,
      Nome3, Natal3, Nome4,
      Natal4, CreditosI, CreditosL,
      CreditosF2, CreditosD, CreditosU,
      CreditosV, Motivo, QuantI1,
      QuantI2, QuantI3, QuantI4,
      QuantN1, QuantN2, QuantN3,
      Agenda, SenhaQuer, Senha1,
      LimiCred, Desco, CasasApliDesco,
      TempA, TempD, Banco,
      Agencia, ContaCorrente, FatorCompra,
      AdValorem, DMaisC, DMaisD,
      Empresa, CBE, SCB,
      PAtividad, EAtividad, PCidadeCod,
      ECidadeCod, PPaisCod, EPaisCod,*)
      Antigo, (*CUF2, Contab,
      MSN1, PastaTxtFTP, PastaPwdFTP,
      Protestar, MultaCodi, MultaDias,
      MultaValr, MultaPerc, MultaTiVe,
      JuroSacado, CPMF, Corrido,
      CPF_Resp1, CliInt, AltDtPlaCt,
      CartPref, RolComis, Filial,*)
      EEndeRef, (*PEndeRef, CEndeRef,
      LEndeRef,*) ECodMunici, (*PCodMunici,
      CCodMunici, LCodMunici, CNAE,
      SUFRAMA,*) ECodiPais, (*PCodiPais,
      L_CNPJ, L_Ativo, CNAE20,*)
      URL(*, CRT, COD_PART*)], [
      Codigo], True) then
      begin
        if (Te1 <> '') or (Fax <> '') or (Cel <> '') then
        begin
          //
          UnMyMySQL.AbreMySQLQuery1(QrExiste, [
          'SELECT Controle Codigo ',
          'FROM enticontat ',
          'WHERE Codigo=' + Geral.FF0(Codigo),
          'AND Nome="' + NomeSYS + '"',
          '']);
          if QrExisteCodigo.Value > 0 then
          begin
            SQLType := stUpd;
            Controle := QrExisteCodigo.Value
          end else
          begin
            SQLType := stIns;
            Controle := UnMyMySQL.BuscaEmLivreY_Def('enticontat', 'Controle', stIns, 0);
          end;
          Nome := NomeSYS;
          Cargo := 0;
          if UnMyMySQL.SQLInsUpd(Dmod.QrUpd, SQLType, 'enticontat', False, [
          'Codigo', 'Nome', 'Cargo'], [
          'Controle'], [
          Codigo, Nome, Cargo], [
          Controle], True) then
          begin
            if (Te1 <> '') then
              IncluiAlteraFone(Codigo, Controle, Te1, '', 'Fone [IMPORTADO]');
            if (Fax <> '') then
              IncluiAlteraFone(Codigo, Controle, Fax, '', 'Fax [IMPORTADO]');
            if (Cel <> '') then
              IncluiAlteraFone(Codigo, Controle, Cel, '', 'Celular [IMPORTADO]');
            if (EEMail <> '') then
              IncluiAlteraMail(Codigo, Controle, EEMail, '[IMPORTADO]');
          end;
        end;
        //
        Contatos := Geral.SeparaNomes(Trim(QrFornecedIBCONTATO.Value));
        while Contatos <> '' do
        begin
          if Geral.SeparaPrimeiraOcorrenciaDeTexto('|', Contatos, Nome, Contatos) then
          begin
            Nome := trim(Nome);
            if Nome <> '' then
            begin
              UnMyMySQL.AbreMySQLQuery1(QrExiste, [
              'SELECT Controle Codigo ',
              'FROM enticontat ',
              'WHERE Codigo=' + Geral.FF0(Codigo),
              'AND Nome="' + Nome + '"',
              '']);
              if QrExisteCodigo.Value > 0 then
              begin
                SQLType := stUpd;
                Controle := QrExisteCodigo.Value
              end else
              begin
                SQLType := stIns;
                Controle := UnMyMySQL.BuscaEmLivreY_Def('enticontat', 'Controle', stIns, 0);
              end;
              Cargo := 0;
              UnMyMySQL.SQLInsUpd(Dmod.QrUpd, SQLType, 'enticontat', False, [
              'Codigo', 'Nome', 'Cargo'], [
              'Controle'], [
              Codigo, Nome, Cargo], [
              Controle], True);
            end;
          end;
        end;
      end;
    end;
    QrFornecedIB.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Importa��o de fornecedores finalizada!');
end;

procedure TFmDedetiza.ImportaFuncionarios();
var
  CNPJ_CPF, CPF, Antigo, Nome, Apelido, RG, Te1, PTe1, Fornece2, PNatal: String;
  (*xLogr, nLogr, xRua, URL, CEP, Observacoes,
  (*ERua, ECompl, EBairro, ECidade, EPAIS, ETe1, EFax, ECel, EEmail, EContato,
  (*PRua, PCompl, PBairro, PCidade, PPAIS, PTe1, PFax, PCel, PEmail, PContato,
  PEndeRef, CRua, CBairro, CCidade, CPais,
  EEndeRef, Te1, Fax, Cel: String;
  (*PLograd, PNumero, PCEP, PUF, PCodMunici, PCodiPais*)
  //CUF, CCEP,
  Tipo, (*ELograd, ENumero, EUF, ECEP,*)
  Codigo, CodUsu (*, ECodMunici, ECodiPais, COD_CIDADE*): Integer;
  SQLType: TSQLType;
  //
  //DataCon,
  //Contatos: String;
  //Conta, AtivPrinc, EntTipCto, HowFind, Account,
  Controle, Cargo: Integer;
  //
(*
  SRua, SCompl, SBairro, SCidade, SUF, SPais, SEndeRef: String;
  Cliente, SLograd, SNumero, SCEP, UF, SCodMunici, SCodiPais: Integer;
  M2Constru, M2NaoBuild, M2Terreno, M2Total: Double;
  //
  SiapImaTer, Objeto, Finalidade, TpConstru: Integer;
*)
begin
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela (origem) de funcion�rios');
  QrFuncionarios.Close;
  QrFuncionarios.Open;
  PB1.Max := QrFuncionarios.RecordCount;
  //
  while not QrFuncionarios.Eof do
  begin
    Antigo := 'FUN' + Geral.FFN(QrFuncionariosID_FUNC.Value, 8);
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Verificando funccion�rio: ' + Antigo);

    CNPJ_CPF := Geral.SoNumero_TT(QrFuncionariosCPF.Value);
    Tipo := 1;
    {
    if Length(CNPJ_CPF) = 0 then
    begin
      if UPPERCASE(QrFuncionariosi.Value) = 'RESIDENCIAL' then
        Tipo := 1
      else
        Tipo := 0;
    end else
    begin
      if Length(CNPJ_CPF) > 11 then
        Tipo := 0
      else Tipo := 1;
    end;
    }

(*
    Geral.SeparaEndereco(0, QrFuncionariosENDFORN.Value, Num, ECompl,
      EBairro, xLogr, nLogr, xRua, False, nil, nil);
    //Geral.SeparaLogradouro(0, QrFuncionariosENDFORN.Value,  xLogr, nLogr, xRua, nil, nil);
    //
    //ERRO! nLogr := Geral.SoNumero_TT(nLogr);
    if nLogr = '' then
      nLogr := '0';
    //
    //Num := Geral.SoNumero_TT(QrFuncionariosNUMERO.Value);
    if Num = '' then
      Num := '0';
    //
    CEP := Geral.SoNumero_TT(QrFuncionariosCEP.Value);
    if CEP = '' then
      CEP := '0';
*)
    Te1 := Geral.SoNumero_TT(QrFuncionariosFONE.Value);
    if Length(Te1) < 3 then
      Te1 := '';
    //
(*    Fax := Geral.SoNumero_TT(QrFuncionariosFAX.Value);
    if Length(Fax) < 3 then
      Fax := '';
    //
    Cel := Geral.SoNumero_TT(QrFuncionariosCELULAR.Value);
    if Length(Cel) < 3 then
      Cel := '';
*)
    //
(*
    RazaoSocial    := '';
    Fantasia       := '';
    CNPJ           := '';
    IE             := '';
    //
    ELograd        := 0;
    ERua           := '';
    ENumero        := 0;
    ECompl         := '';
    EBairro        := '';
    ECidade        := '';
    EUF            := 0;
    ECEP           := 0;
    EPAIS          := '';
    ETe1           := '';
    EFax           := '';
    ECel           := '';
    EEmail         := '';
    EContato       := '';
    EEndeRef       := '';
    ECodMunici     := 0;
    ECodiPais      := 0;

    /////////////////////

    Nome           := '';
    Apelido        := '';
    CPF            := '';
    RG             := '';
    //
    PLograd        := 0;
    PRua           := '';
    PNumero        := 0;
    PCompl         := '';
    PBairro        := '';
    PCidade        := '';
    PUF            := 0;
    PCEP           := 0;
    PPAIS          := '';
    PTe1           := '';
    PFax           := '';
    PCel           := '';
    PEmail         := '';
    PContato       := '';
    PEndeRef       := '';
    PCodMunici     := 0;
    PCodiPais      := 0;
    //
    COD_CIDADE := QrFuncionariosCOD_CIDADE.Value;
    if (COD_CIDADE < 3000000) or (COD_CIDADE >= 5000000) then
      COD_CIDADE := 0;

    if Tipo = 0 then
    begin
      RazaoSocial    := QrFuncionariosRAZFORN.Value;
      Fantasia       := RazaoSocial;
      CNPJ           := CNPJ_CPF;
      IE             := Geral.SoNumero_TT(QrFuncionariosINSCR.Value);
      //
      ELograd        := Geral.IMV(nLogr);
      ERua           := xRua;
      ENumero        := Geral.IMV(Num);
      //ECompl         := QrFuncionariosCOMPLEMENTO.Value;
      EBairro        := QrFuncionariosBAIRRO.Value;
      ECidade        := QrFuncionariosCIDADE.Value;
      EUF            := Geral.GetCodigoUF_da_SiglaUF(QrFuncionariosESTADO.Value);
      ECEP           := Geral.IMV(CEP);
      EPAIS          := 'BRASIL';
      ETe1           := Te1;
      EFax           := Fax;
      ECel           := Cel;
      EEmail         := LowerCase(QrFuncionariosEMAIL.Value);
      EContato       := QrFuncionariosCONTATO.Value;
      EEndeRef       := '';//QrFuncionariosREFERENCIA.Value;
      ECodMunici     := COD_CIDADE;
      ECodiPais      := 1058;

    end else
    begin
*)
      Nome           := QrFuncionariosNOME.Value;
      Apelido        := Nome;
      CPF            := CNPJ_CPF;
      RG             := Geral.SoNumero_TT(QrFuncionariosRG.Value);
      //
(*
      PLograd        := Geral.IMV(nLogr);
      PRua           := xRua;
      PNumero        := Geral.IMV(Num);
      PCompl         := QrFuncionariosCOMPLEMENTO.Value;
      PBairro        := QrFuncionariosBAIRRO.Value;
      PCidade        := QrFuncionariosCIDADE.Value;
      PUF            := Geral.GetCodigoUF_da_SiglaUF(QrFuncionariosESTADO.Value);
      PCEP           := Geral.IMV(CEP);
      PPAIS          := 'BRASIL';
*)
      PTe1           := Te1;
(*
      PFax           := Fax;
      PCel           := Cel;
      PEmail         := LowerCase(QrFuncionariosEMAIL.Value);
      PContato       := QrFuncionariosCONTATOS.Value;
      PEndeRef       := QrFuncionariosREFERENCIA.Value;
      PCodMunici     := COD_CIDADE;
      PCodiPais      := 1058;
    end;

    //Atividade   := QrFuncionariosTPCLI.Value;
    URL         := LowerCase(QrFuncionariosHOMEPAGE.Value);
    Observacoes := QrFuncionariosOBS.Value;
*)
    Fornece2    := 'V';
    //
    PNatal := Geral.FDT(QrFuncionariosDTA_NASC.Value, 1);
    //
    UnMyMySQL.AbreMySQLQuery1(QrExiste, [
    'SELECT Codigo ',
    'FROM entidades ',
    'WHERE Antigo="' + Antigo + '"',
    '']);
    //
    if QrExiste.RecordCount = 1 then
    begin
      SQLType := stUpd;
      Codigo := QrExisteCodigo.Value;
      CodUsu := QrExisteCodigo.Value;
    end else
    begin
      SQLType := stIns;
      Codigo := UnMyMySQL.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
      CodUsu := Codigo;
    end;
    //
    if (SQLType = stIns) or (CkAltera.Checked) then
    begin
      if UnMyMySQL.SQLInsUpd(Dmod.QrUpd, SQLType, 'entidades', False, [
      'CodUsu', (*'RazaoSocial', 'Fantasia',
      'Respons1', 'Respons2', 'Pai',
      'Mae', 'CNPJ', 'IE',
      'NIRE', 'FormaSociet', 'Simples',
      'IEST', 'Atividade',*) 'Nome',
      'Apelido', 'CPF', (*'CPF_Pai',
      'CPF_Conjuge',*) 'RG', (*'SSP',
      'DataRG', 'CidadeNatal', 'EstCivil',
      'UFNatal', 'Nacionalid', 'ELograd',
      'ERua', 'ENumero', 'ECompl',
      'EBairro', 'ECidade', 'EUF',
      'ECEP', 'EPais', 'ETe1',
      'Ete2', 'Ete3', 'ECel',
      'EFax', 'EEmail', 'EContato',
      'ENatal', 'PLograd', 'PRua',
      'PNumero', 'PCompl', 'PBairro',
      'PCidade', 'PUF', 'PCEP',
      'PPais',*) 'PTe1', (*'Pte2',
      'Pte3', 'PCel', 'PFax',
      'PEmail', 'PContato',*) 'PNatal',
      (*'Sexo', 'Responsavel', 'Profissao',
      'Cargo', 'Recibo', 'DiaRecibo',
      'AjudaEmpV', 'AjudaEmpP', 'Cliente1',
      'Cliente2', 'Cliente3', 'Cliente4',
      'Fornece1',*) 'Fornece2', (*'Fornece3',
      'Fornece4', 'Fornece5', 'Fornece6',
      'Fornece7', 'Fornece8', 'Terceiro',
      'Cadastro', 'Informacoes', 'Logo',
      'Veiculo', 'Mensal', 'Observacoes',*)
      'Tipo', (*'CLograd', 'CRua',
      'CNumero', 'CCompl', 'CBairro',
      'CCidade', 'CUF', 'CCEP',
      'CPais', 'CTel', 'CCel',
      'CFax', 'CContato', 'LLograd',
      'LRua', 'LNumero', 'LCompl',
      'LBairro', 'LCidade', 'LUF',
      'LCEP', 'LPais', 'LTel',
      'LCel', 'LFax', 'LContato',
      'Comissao', 'Situacao', 'Nivel',
      'Grupo', 'Account', 'Logo2',
      'ConjugeNome', 'ConjugeNatal', 'Nome1',
      'Natal1', 'Nome2', 'Natal2',
      'Nome3', 'Natal3', 'Nome4',
      'Natal4', 'CreditosI', 'CreditosL',
      'CreditosF2', 'CreditosD', 'CreditosU',
      'CreditosV', 'Motivo', 'QuantI1',
      'QuantI2', 'QuantI3', 'QuantI4',
      'QuantN1', 'QuantN2', 'QuantN3',
      'Agenda', 'SenhaQuer', 'Senha1',
      'LimiCred', 'Desco', 'CasasApliDesco',
      'TempA', 'TempD', 'Banco',
      'Agencia', 'ContaCorrente', 'FatorCompra',
      'AdValorem', 'DMaisC', 'DMaisD',
      'Empresa', 'CBE', 'SCB',
      'PAtividad', 'EAtividad', 'PCidadeCod',
      'ECidadeCod', 'PPaisCod', 'EPaisCod',*)
      'Antigo'(*, 'CUF2', 'Contab',
      'MSN1', 'PastaTxtFTP', 'PastaPwdFTP',
      'Protestar', 'MultaCodi', 'MultaDias',
      'MultaValr', 'MultaPerc', 'MultaTiVe',
      'JuroSacado', 'CPMF', 'Corrido',
      'CPF_Resp1', 'CliInt', 'AltDtPlaCt',
      'CartPref', 'RolComis', 'Filial',
      'EEndeRef', 'PEndeRef', 'CEndeRef',
      'LEndeRef', 'ECodMunici', 'PCodMunici',
      'CCodMunici', 'LCodMunici', 'CNAE',
      'SUFRAMA', 'ECodiPais', 'PCodiPais',
      'L_CNPJ', 'L_Ativo', 'CNAE20',
      'URL', 'CRT', 'COD_PART'*)], [
      'Codigo'], [
      CodUsu, (*RazaoSocial, Fantasia,
      Respons1, Respons2, Pai,
      Mae, CNPJ, IE,
      (*NIRE, FormaSociet, Simples,
      IEST, Atividade,*) Nome,
      Apelido, CPF, (*CPF_Pai,
      CPF_Conjuge,*) RG, (*SSP,
      DataRG, CidadeNatal, EstCivil,
      UFNatal, Nacionalid, ELograd,
      ERua, ENumero, ECompl,
      EBairro, ECidade, EUF,
      ECEP, EPais, ETe1,
      (*Ete2, Ete3, ECel,
      EFax, EEmail, EContato,
      (*ENatal, PLograd, PRua,
      PNumero, PCompl, PBairro,
      PCidade, PUF, PCEP,
      PPais,*) PTe1, (*Pte2,
      Pte3, PCel, PFax,
      PEmail, PContato,*) PNatal,
      (*Sexo, Responsavel, Profissao,
      Cargo, Recibo, DiaRecibo,
      AjudaEmpV, AjudaEmpP, Cliente1,
      Cliente2, Cliente3, Cliente4,
      Fornece1, *)Fornece2, (*Fornece3,
      Fornece4, Fornece5, Fornece6,
      Fornece7, Fornece8, Terceiro,
      Cadastro, Informacoes, Logo,
      Veiculo, Mensal, Observacoes,*)
      Tipo, (*CLograd, CRua,
      CNumero, CCompl, CBairro,
      CCidade, CUF, CCEP,
      CPais, CTel, CCel,
      CFax, CContato, LLograd,
      LRua, LNumero, LCompl,
      LBairro, LCidade, LUF,
      LCEP, LPais, LTel,
      LCel, LFax, LContato,
      Comissao, Situacao, Nivel,
      Grupo, Account, Logo2,
      ConjugeNome, ConjugeNatal, Nome1,
      Natal1, Nome2, Natal2,
      Nome3, Natal3, Nome4,
      Natal4, CreditosI, CreditosL,
      CreditosF2, CreditosD, CreditosU,
      CreditosV, Motivo, QuantI1,
      QuantI2, QuantI3, QuantI4,
      QuantN1, QuantN2, QuantN3,
      Agenda, SenhaQuer, Senha1,
      LimiCred, Desco, CasasApliDesco,
      TempA, TempD, Banco,
      Agencia, ContaCorrente, FatorCompra,
      AdValorem, DMaisC, DMaisD,
      Empresa, CBE, SCB,
      PAtividad, EAtividad, PCidadeCod,
      ECidadeCod, PPaisCod, EPaisCod,*)
      Antigo(*, CUF2, Contab,
      MSN1, PastaTxtFTP, PastaPwdFTP,
      Protestar, MultaCodi, MultaDias,
      MultaValr, MultaPerc, MultaTiVe,
      JuroSacado, CPMF, Corrido,
      CPF_Resp1, CliInt, AltDtPlaCt,
      CartPref, RolComis, Filial,
      EEndeRef, PEndeRef, CEndeRef,
      LEndeRef, ECodMunici, PCodMunici,
      CCodMunici, LCodMunici, CNAE,
      SUFRAMA, ECodiPais, PCodiPais,
      L_CNPJ, L_Ativo, CNAE20,
      URL, CRT, COD_PART*)], [
      Codigo], True) then
      begin
        if (Te1 <> '') (*or (Fax <> '') or (Cel <> '')*) then
        begin
          //
          UnMyMySQL.AbreMySQLQuery1(QrExiste, [
          'SELECT Controle Codigo ',
          'FROM enticontat ',
          'WHERE Codigo=' + Geral.FF0(Codigo),
          'AND Nome="' + NomeSYS + '"',
          '']);
          if QrExisteCodigo.Value > 0 then
          begin
            SQLType := stUpd;
            Controle := QrExisteCodigo.Value
          end else
          begin
            SQLType := stIns;
            Controle := UnMyMySQL.BuscaEmLivreY_Def('enticontat', 'Controle', stIns, 0);
          end;
          Nome := NomeSYS;
          Cargo := 0;
          if UnMyMySQL.SQLInsUpd(Dmod.QrUpd, SQLType, 'enticontat', False, [
          'Codigo', 'Nome', 'Cargo'], [
          'Controle'], [
          Codigo, Nome, Cargo], [
          Controle], True) then
          begin
            if (Te1 <> '') then
              IncluiAlteraFone(Codigo, Controle, Te1, '', 'Fone [IMPORTADO]');
            (*
            if (Fax <> '') then
              IncluiAlteraFone(Codigo, Controle, Fax, '', 'Fax');
            if (Cel <> '') then
              IncluiAlteraFone(Codigo, Controle, Cel, '', 'Celular');
            *)
          end;
        end;
        //
        (*
        Contatos := Geral.SeparaNomes(Trim(QrFuncionariosCONTATO.Value));
        while Contatos <> '' do
        begin
          if Geral.SeparaPrimeiraOcorrenciaDeTexto('|', Contatos, Nome, Contatos) then
          begin
            Nome := trim(Nome);
            if Nome <> '' then
            begin
              UnMyMySQL.AbreMySQLQuery1(QrExiste, [
              'SELECT Controle Codigo ',
              'FROM enticontat ',
              'WHERE Codigo=' + Geral.FF0(Codigo),
              'AND Nome="' + Nome + '"',
              '']);
              if QrExisteCodigo.Value > 0 then
              begin
                SQLType := stUpd;
                Controle := QrExisteCodigo.Value
              end else
              begin
                SQLType := stIns;
                Controle := UnMyMySQL.BuscaEmLivreY_Def('enticontat', 'Controle', stIns, 0);
              end;
              Cargo := 0;
              UnMyMySQL.SQLInsUpd(Dmod.QrUpd, SQLType, 'enticontat', False, [
              'Codigo', 'Nome', 'Cargo'], [
              'Controle'], [
              Codigo, Nome, Cargo], [
              Controle], True);
            end;
          end;
        end;
        *)
      end;
    end;
    QrFuncionarios.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Importa��o de funcion�rios finalizada!');
end;

procedure TFmDedetiza.IncluiAlteraFone(const Codigo, Controle: Integer;
  const Telefone, Ramal, Nome: String(*; var CodRes: Integer*));
var
  Conta, EntiTipCto: Integer;
  SQLType: TSQLType;
begin
  UnMyMySQL.AbreMySQLQuery1(QrExiste, [
  'SELECT Conta Codigo ',
  'FROM entitel ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND Controle=' + Geral.FF0(Controle),
  'AND Telefone="' + Telefone + '"',
  '']);
  if QrExisteCodigo.Value > 0 then
  begin
    SQLType := stUpd;
    Conta := QrExisteCodigo.Value
  end else
  begin
    SQLType := stIns;
    Conta := UnMyMySQL.BuscaEmLivreY_Def('entitel', 'Conta', stIns, 0);
  end;
  UnMyMySQL.AbreMySQLQuery1(QrExiste, [
  'SELECT Codigo ',
  'FROM entitipcto ',
  'WHERE Nome="' + Nome + '"',
  '']);
  if QrExisteCodigo.Value <> 0 then
    EntiTipCto := QrExisteCodigo.Value
  else
  begin
    EntiTipCto := UnMyMySQL.BuscaEmLivreY_Def('entitipcto', 'Codigo', stIns, 0);
    UnMyMySQL.SQLInsUpd(Dmod.QrUpd, stIns, 'entitipcto', False, [
    'CodUsu', 'Nome'], [
    'Codigo'], [
    EntiTipCto, Nome], [
    EntiTipCto], True);
  end;
  UnMyMySQL.SQLInsUpd(Dmod.QrUpd, SQLType, 'entitel', False, [
  'Codigo', 'Controle', 'Telefone',
  'EntiTipCto', 'Ramal'], [
  'Conta'], [
  Codigo, Controle, Telefone,
  EntiTipCto, Ramal], [
  Conta], True);
end;

procedure TFmDedetiza.IncluiAlteraMail(const Codigo, Controle: Integer;
  const EMail, Nome: String);
var
  Conta, EntiTipCto: Integer;
  SQLType: TSQLType;
begin
  UnMyMySQL.AbreMySQLQuery1(QrExiste, [
  'SELECT Conta Codigo ',
  'FROM entimail ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND Controle=' + Geral.FF0(Controle),
  'AND EMail="' + EMail + '"',
  '']);
  if QrExisteCodigo.Value > 0 then
  begin
    SQLType := stUpd;
    Conta := QrExisteCodigo.Value
  end else
  begin
    SQLType := stIns;
    Conta := UnMyMySQL.BuscaEmLivreY_Def('entimail', 'Conta', stIns, 0);
  end;
  UnMyMySQL.AbreMySQLQuery1(QrExiste, [
  'SELECT Codigo ',
  'FROM entitipcto ',
  'WHERE Nome="' + Nome + '"',
  '']);
  if QrExisteCodigo.Value <> 0 then
    EntiTipCto := QrExisteCodigo.Value
  else
  begin
    EntiTipCto := UnMyMySQL.BuscaEmLivreY_Def('entitipcto', 'Codigo', stIns, 0);
    UnMyMySQL.SQLInsUpd(Dmod.QrUpd, stIns, 'entitipcto', False, [
    'CodUsu', 'Nome'], [
    'Codigo'], [
    EntiTipCto, Nome], [
    EntiTipCto], True);
  end;
  UnMyMySQL.SQLInsUpd(Dmod.QrUpd, SQLType, 'entimail', False, [
  'Codigo', 'Controle', 'EMail',
  'EntiTipCto', 'Ordem'], [
  'Conta'], [
  Codigo, Controle, EMail,
  EntiTipCto, 999], [
  Conta], True);
end;

procedure TFmDedetiza.QrDadosBeforeClose(DataSet: TDataSet);
begin
  EdCampo.Text := '';
  DBMemo1.DataField := '';
end;

procedure TFmDedetiza.QrTabelasAfterScroll(DataSet: TDataSet);
begin
  UnMyMySQL.AbreMySQLQuery1(QrDados, [
  'SELECT * FROM ' + QrTabelasTabela.Value,
  '']);
end;

procedure TFmDedetiza.QrTabelasBeforeClose(DataSet: TDataSet);
begin
  QrDados.Close;
end;

end.
