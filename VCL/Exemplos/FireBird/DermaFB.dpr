program DermaFB;

uses
  Forms,
  Principal in 'Principal.pas' {FmPrincipal},
  UnDmkProcFunc in 'C:\Geral\Units\UnDmkProcFunc.pas',
  MeuFrx in 'C:\Outros\Print\Geral\MeuFrx.pas' {FmMeuFrx},
  MeuDBUses in 'MeuDBUses.pas' {FmMeuDBUSes},
  MyMySQL in 'C:\Outros\MySQL\MyMySQL.pas',
  Dedetiza in 'Dedetiza.pas' {FmDedetiza},
  GModule in 'C:\Outros\MySQL\GModule.pas' {GMod: TDataModule},
  Module in 'Module.pas' {Dmod: TDataModule},
  dmkDAC_PF in 'C:\Outros\MySQL\dmkDAC_PF.pas',
  Textos2 in 'C:\Outros\Textos\Textos2.pas' {FmTextos2},
  reinit in 'C:\Outros\Richedit\reinit.pas',
  Calculadora in 'C:\Outros\Geral\Forms\Calculadora.pas' {FmCalculadora},
  SelRadioGroup in 'C:\_Compilers\projetosdelphi10_2_2_tokyo_2020_06\VCL\UTL\_FRM\v01_01\SelRadioGroup.pas' {FmSelRadioGroup};

{$R *.res}

begin
  //MyGlyfs in '..\..\..\Outros\Geral\Forms\MyGlyfs.pas' {FmMyGlyfs},
  //SelRadioGroup in '..\..\..\Geral\SelRadioGroup.pas' {FmSelRadioGroup},
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TGMod, GMod);
  Application.CreateForm(TDmod, Dmod);
  Application.Run;
end.
