object FmDedetiza: TFmDedetiza
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: Importa'#231#227'o de Dados Dedetiza'
  ClientHeight = 492
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 376
        Height = 32
        Caption = 'Importa'#231#227'o de Dados Dedetiza'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 376
        Height = 32
        Caption = 'Importa'#231#227'o de Dados Dedetiza'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 376
        Height = 32
        Caption = 'Importa'#231#227'o de Dados Dedetiza'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 311
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 311
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 311
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 125
          Top = 15
          Height = 294
          ExplicitLeft = 468
          ExplicitTop = 240
          ExplicitHeight = 100
        end
        object Splitter2: TSplitter
          Left = 516
          Top = 15
          Height = 294
          Align = alRight
          ExplicitLeft = 876
          ExplicitTop = 23
          ExplicitHeight = 313
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 123
          Height = 294
          Align = alLeft
          DataSource = DsTabelas
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Tabela'
              Width = 87
              Visible = True
            end>
        end
        object DBGrid2: TDBGrid
          Left = 128
          Top = 15
          Width = 388
          Height = 294
          Align = alClient
          DataSource = DsDados
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object Panel5: TPanel
          Left = 519
          Top = 15
          Width = 302
          Height = 294
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 2
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 302
            Height = 25
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 4
              Width = 36
              Height = 13
              Caption = 'Campo:'
            end
            object EdCampo: TEdit
              Left = 48
              Top = 0
              Width = 225
              Height = 21
              TabOrder = 0
              OnExit = EdCampoExit
            end
          end
          object DBMemo1: TDBMemo
            Left = 0
            Top = 25
            Width = 302
            Height = 215
            Align = alClient
            DataSource = DsDados
            TabOrder = 1
          end
          object RGArea: TRadioGroup
            Left = 0
            Top = 240
            Width = 302
            Height = 54
            Align = alBottom
            Caption = ' '#193'rea informada no cadastro de cliente '#233'...: '
            Columns = 2
            Items.Strings = (
              'N'#227'o importar'
              'Constru'#237'da'
              'N'#227'o constru'#237'da'
              'Do terreno')
            TabOrder = 2
          end
        end
        object GroupBox2: TGroupBox
          Left = 821
          Top = 15
          Width = 185
          Height = 294
          Align = alRight
          Caption = ' Importa'#231#245'es:'
          TabOrder = 3
          object CkCliente: TCheckBox
            Left = 12
            Top = 20
            Width = 65
            Height = 17
            Caption = 'Clientes'
            TabOrder = 0
          end
          object CkFornece: TCheckBox
            Left = 12
            Top = 36
            Width = 89
            Height = 17
            Caption = 'Fornecedores'
            TabOrder = 1
          end
          object CkFuncion: TCheckBox
            Left = 12
            Top = 52
            Width = 97
            Height = 17
            Caption = 'Funcion'#225'rios'
            TabOrder = 2
          end
          object Panel7: TPanel
            Left = 2
            Top = 221
            Width = 181
            Height = 71
            Align = alBottom
            TabOrder = 3
            object BtImporta: TBitBtn
              Tag = 14
              Left = 36
              Top = 24
              Width = 120
              Height = 40
              Caption = '&Importa'
              TabOrder = 0
              OnClick = BtImportaClick
              NumGlyphs = 2
            end
            object CkAltera: TCheckBox
              Left = 28
              Top = 4
              Width = 137
              Height = 17
              Caption = 'Alterar os j'#225' importados.'
              TabOrder = 1
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 359
    Width = 1008
    Height = 63
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 46
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 29
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        Visible = False
        NumGlyphs = 2
      end
    end
  end
  object QrTabelas: TmySQLQuery
    Database = DBAnt
    BeforeClose = QrTabelasBeforeClose
    AfterScroll = QrTabelasAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT Tabela '
      'FROM _tab_tabs_'
      'ORDER BY Tabela')
    Left = 8
    Top = 132
    object QrTabelasTabela: TStringField
      FieldName = 'Tabela'
      Size = 60
    end
  end
  object DsTabelas: TDataSource
    DataSet = QrTabelas
    Left = 36
    Top = 132
  end
  object DBAnt: TmySQLDatabase
    Connected = True
    DatabaseName = 'zzz_ib_db_ant'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=zzz_ib_db_ant'
      'PWD=wkljweryhvbirt'
      'UID=root')
    Left = 8
    Top = 104
  end
  object QrDados: TmySQLQuery
    Database = DBAnt
    BeforeClose = QrDadosBeforeClose
    Left = 8
    Top = 160
  end
  object DsDados: TDataSource
    DataSet = QrDados
    Left = 36
    Top = 160
  end
  object QrClientesIB: TmySQLQuery
    Database = DBAnt
    BeforeClose = QrDadosBeforeClose
    SQL.Strings = (
      'SELECT dtb.Codigo COD_CIDADE, cib.* '
      'FROM zzz_ib_db_ant.clientesib cib'
      'LEFT JOIN bugstrol.dtb_munici dtb '
      '     ON cib.Cidade=dtb.Nome'
      'ORDER BY CODCLI')
    Left = 8
    Top = 196
    object QrClientesIBAUTOINC: TIntegerField
      FieldName = 'AUTOINC'
    end
    object QrClientesIBCODCLI: TIntegerField
      FieldName = 'CODCLI'
    end
    object QrClientesIBNOMECLI: TStringField
      FieldName = 'NOMECLI'
      Size = 65
    end
    object QrClientesIBTPCLI: TStringField
      FieldName = 'TPCLI'
      Size = 25
    end
    object QrClientesIBENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 65
    end
    object QrClientesIBBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrClientesIBID_CIDADE: TIntegerField
      FieldName = 'ID_CIDADE'
    end
    object QrClientesIBCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 40
    end
    object QrClientesIBESTADO: TStringField
      FieldName = 'ESTADO'
      Size = 2
    end
    object QrClientesIBCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object QrClientesIBRG: TStringField
      FieldName = 'RG'
      Size = 18
    end
    object QrClientesIBCPF: TStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrClientesIBFONE: TStringField
      FieldName = 'FONE'
      Size = 14
    end
    object QrClientesIBFAX: TStringField
      FieldName = 'FAX'
      Size = 14
    end
    object QrClientesIBCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 14
    end
    object QrClientesIBCONTATOS: TStringField
      FieldName = 'CONTATOS'
      Size = 30
    end
    object QrClientesIBEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object QrClientesIBHOMEPAGE: TStringField
      FieldName = 'HOMEPAGE'
      Size = 50
    end
    object QrClientesIBOBS: TMemoField
      FieldName = 'OBS'
      BlobType = ftMemo
      Size = 4
    end
    object QrClientesIBDATACON: TDateField
      FieldName = 'DATACON'
    end
    object QrClientesIBREFERENCIA: TStringField
      FieldName = 'REFERENCIA'
      Size = 100
    end
    object QrClientesIBCAIXA: TStringField
      FieldName = 'CAIXA'
      Size = 15
    end
    object QrClientesIBAREA: TStringField
      FieldName = 'AREA'
      Size = 15
    end
    object QrClientesIBINTERNO: TMemoField
      FieldName = 'INTERNO'
      BlobType = ftMemo
      Size = 4
    end
    object QrClientesIBID_VEND: TIntegerField
      FieldName = 'ID_VEND'
    end
    object QrClientesIBEXTERNO: TMemoField
      FieldName = 'EXTERNO'
      BlobType = ftMemo
      Size = 4
    end
    object QrClientesIBEND_COBR: TStringField
      FieldName = 'END_COBR'
      Size = 65
    end
    object QrClientesIBBAIRRO_COBR: TStringField
      FieldName = 'BAIRRO_COBR'
      Size = 30
    end
    object QrClientesIBCIDADE_COBR: TStringField
      FieldName = 'CIDADE_COBR'
      Size = 40
    end
    object QrClientesIBESTADO_COBR: TStringField
      FieldName = 'ESTADO_COBR'
      Size = 2
    end
    object QrClientesIBCEP_COBR: TStringField
      FieldName = 'CEP_COBR'
      Size = 9
    end
    object QrClientesIBPROPA: TIntegerField
      FieldName = 'PROPA'
    end
    object QrClientesIBASP1: TIntegerField
      FieldName = 'ASP1'
    end
    object QrClientesIBASP2: TIntegerField
      FieldName = 'ASP2'
    end
    object QrClientesIBASP3: TIntegerField
      FieldName = 'ASP3'
    end
    object QrClientesIBASP4: TIntegerField
      FieldName = 'ASP4'
    end
    object QrClientesIBASP5: TIntegerField
      FieldName = 'ASP5'
    end
    object QrClientesIBASP6: TIntegerField
      FieldName = 'ASP6'
    end
    object QrClientesIBASP7: TIntegerField
      FieldName = 'ASP7'
    end
    object QrClientesIBASP7_OUTROS: TStringField
      FieldName = 'ASP7_OUTROS'
      Size = 30
    end
    object QrClientesIBCOND1: TIntegerField
      FieldName = 'COND1'
    end
    object QrClientesIBCOND2: TIntegerField
      FieldName = 'COND2'
    end
    object QrClientesIBCOND3: TIntegerField
      FieldName = 'COND3'
    end
    object QrClientesIBCOND4: TIntegerField
      FieldName = 'COND4'
    end
    object QrClientesIBCOND5: TIntegerField
      FieldName = 'COND5'
    end
    object QrClientesIBCOND6: TIntegerField
      FieldName = 'COND6'
    end
    object QrClientesIBCOND6_OUTROS: TStringField
      FieldName = 'COND6_OUTROS'
      Size = 30
    end
    object QrClientesIBID_EMPRESA: TIntegerField
      FieldName = 'ID_EMPRESA'
    end
    object QrClientesIBFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 65
    end
    object QrClientesIBNUMERO: TStringField
      FieldName = 'NUMERO'
      Size = 10
    end
    object QrClientesIBCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 40
    end
    object QrClientesIBIBGE: TIntegerField
      FieldName = 'IBGE'
    end
    object QrClientesIBID_CARTFIDEL: TIntegerField
      FieldName = 'ID_CARTFIDEL'
    end
    object QrClientesIBSATISFACAO: TStringField
      FieldName = 'SATISFACAO'
      Size = 35
    end
    object QrClientesIBOBSSATISFACAO: TMemoField
      FieldName = 'OBSSATISFACAO'
      BlobType = ftMemo
      Size = 4
    end
    object QrClientesIBRESTRICAO: TStringField
      FieldName = 'RESTRICAO'
      Size = 40
    end
    object QrClientesIBID_COLETALIXO: TIntegerField
      FieldName = 'ID_COLETALIXO'
    end
    object QrClientesIBCOD_CIDADE: TIntegerField
      FieldName = 'COD_CIDADE'
    end
  end
  object DsClientesIB: TDataSource
    DataSet = QrClientesIB
    Left = 36
    Top = 196
  end
  object QrExiste: TmySQLQuery
    Database = DBAnt
    SQL.Strings = (
      'SELECT Codigo '
      'FROM Entidades'
      'WHERE Antigo=:P0')
    Left = 168
    Top = 108
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrExisteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrAnt: TmySQLQuery
    Database = DBAnt
    Left = 36
    Top = 104
  end
  object QrFornecedIB: TmySQLQuery
    Database = DBAnt
    BeforeClose = QrDadosBeforeClose
    SQL.Strings = (
      'SELECT dtb.Codigo COD_CIDADE, cib.* '
      'FROM zzz_ib_db_ant.fornecedib cib'
      'LEFT JOIN bugstrol.dtb_munici dtb '
      '     ON cib.Cidade=dtb.Nome'
      'ORDER BY CODFORN'
      '')
    Left = 8
    Top = 224
    object QrFornecedIBCOD_CIDADE: TIntegerField
      FieldName = 'COD_CIDADE'
    end
    object QrFornecedIBAUTOINC: TIntegerField
      FieldName = 'AUTOINC'
    end
    object QrFornecedIBCODFORN: TIntegerField
      FieldName = 'CODFORN'
    end
    object QrFornecedIBRAZFORN: TStringField
      FieldName = 'RAZFORN'
      Size = 65
    end
    object QrFornecedIBENDFORN: TStringField
      FieldName = 'ENDFORN'
      Size = 65
    end
    object QrFornecedIBCONTATO: TStringField
      FieldName = 'CONTATO'
      Size = 30
    end
    object QrFornecedIBBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrFornecedIBID_CIDADE: TIntegerField
      FieldName = 'ID_CIDADE'
    end
    object QrFornecedIBCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 40
    end
    object QrFornecedIBESTADO: TStringField
      FieldName = 'ESTADO'
      Size = 2
    end
    object QrFornecedIBINSCR: TStringField
      FieldName = 'INSCR'
      Size = 15
    end
    object QrFornecedIBCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrFornecedIBCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object QrFornecedIBFONE: TStringField
      FieldName = 'FONE'
      Size = 14
    end
    object QrFornecedIBFAX: TStringField
      FieldName = 'FAX'
      Size = 14
    end
    object QrFornecedIBCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 14
    end
    object QrFornecedIBEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object QrFornecedIBHOMEPAGE: TStringField
      FieldName = 'HOMEPAGE'
      Size = 50
    end
    object QrFornecedIBOBS: TMemoField
      FieldName = 'OBS'
      BlobType = ftMemo
      Size = 4
    end
    object QrFornecedIBID_EMPRESA: TIntegerField
      FieldName = 'ID_EMPRESA'
    end
  end
  object DsFornecedIB: TDataSource
    DataSet = QrFornecedIB
    Left = 36
    Top = 224
  end
  object QrFuncionarios: TmySQLQuery
    Database = DBAnt
    BeforeClose = QrDadosBeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM funcionarios')
    Left = 8
    Top = 252
    object QrFuncionariosAUTOINC: TIntegerField
      FieldName = 'AUTOINC'
    end
    object QrFuncionariosID_FUNC: TIntegerField
      FieldName = 'ID_FUNC'
    end
    object QrFuncionariosNOME: TStringField
      FieldName = 'NOME'
      Size = 60
    end
    object QrFuncionariosFONE: TStringField
      FieldName = 'FONE'
      Size = 14
    end
    object QrFuncionariosDTA_CONTR: TDateField
      FieldName = 'DTA_CONTR'
    end
    object QrFuncionariosDTA_DEMISS: TDateField
      FieldName = 'DTA_DEMISS'
    end
    object QrFuncionariosCARGO: TStringField
      FieldName = 'CARGO'
      Size = 40
    end
    object QrFuncionariosSETOR: TStringField
      FieldName = 'SETOR'
      Size = 25
    end
    object QrFuncionariosAJ_CUSTO: TFloatField
      FieldName = 'AJ_CUSTO'
    end
    object QrFuncionariosCOMISSAO: TFloatField
      FieldName = 'COMISSAO'
    end
    object QrFuncionariosVL_TRANSP: TIntegerField
      FieldName = 'VL_TRANSP'
    end
    object QrFuncionariosVL_ALIM: TIntegerField
      FieldName = 'VL_ALIM'
    end
    object QrFuncionariosCONVENIO: TIntegerField
      FieldName = 'CONVENIO'
    end
    object QrFuncionariosCESTA_BAS: TIntegerField
      FieldName = 'CESTA_BAS'
    end
    object QrFuncionariosSALARIO: TFloatField
      FieldName = 'SALARIO'
    end
    object QrFuncionariosID_EMPRESA: TIntegerField
      FieldName = 'ID_EMPRESA'
    end
    object QrFuncionariosECIVIL: TStringField
      FieldName = 'ECIVIL'
      Size = 35
    end
    object QrFuncionariosDTA_NASC: TDateField
      FieldName = 'DTA_NASC'
    end
    object QrFuncionariosCARTPROF: TStringField
      FieldName = 'CARTPROF'
      Size = 35
    end
    object QrFuncionariosRG: TStringField
      FieldName = 'RG'
    end
    object QrFuncionariosID_DEMISS: TIntegerField
      FieldName = 'ID_DEMISS'
    end
    object QrFuncionariosCPF: TStringField
      FieldName = 'CPF'
    end
  end
  object DsFuncionarios: TDataSource
    DataSet = QrFuncionarios
    Left = 36
    Top = 252
  end
end
