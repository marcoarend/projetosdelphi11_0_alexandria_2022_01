unit IB_BD_Ant;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker,
  dmkImage, WideStrings, DBClient, SimpleDS, SqlExpr;

type
  TFmIB_BD_Ant = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBFB: TSQLConnection;
    SDFields: TSimpleDataSet;
    DsFields: TDataSource;
    DBGrid3: TDBGrid;
    Panel5: TPanel;
    Label39: TLabel;
    EdIB_DB: TdmkEdit;
    SbIB_DB: TSpeedButton;
    BtIB_DB: TButton;
    Label1: TLabel;
    EdDBNome: TdmkEdit;
    BtErrado: TButton;
    DBAnt: TmySQLDatabase;
    QrUpd: TmySQLQuery;
    Label2: TLabel;
    EdIPServer: TdmkEdit;
    Label3: TLabel;
    EdPorta: TdmkEdit;
    QrAux: TmySQLQuery;
    Memo1: TMemo;
    GroupBox2: TGroupBox;
    Panel13: TPanel;
    Panel7: TPanel;
    LaAvisoG1: TLabel;
    LaAvisoB1: TLabel;
    LaAvisoG2: TLabel;
    LaAvisoB2: TLabel;
    LaAvisoR1: TLabel;
    LaAvisoR2: TLabel;
    Panel14: TPanel;
    Panel6: TPanel;
    LaTempoI: TLabel;
    LaTempoF: TLabel;
    LaTempoT: TLabel;
    Panel8: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    SDFieldsTABELA: TStringField;
    SDFieldsCAMPO: TStringField;
    SDFieldsTIPO_CAMPO: TStringField;
    SDFieldsTAMANHO: TSmallintField;
    SDFieldsESCALA: TFMTBCDField;
    SDFieldsPRIMARY_KEY: TStringField;
    SDFieldsNOT_NULL: TStringField;
    SDFieldsFOREIGN_KEY: TStringField;
    SDFieldsINDICE_CHAVE: TStringField;
    SDFieldsTABELA_CHAVE: TStringField;
    SDFieldsCAMPO_CHAVE: TStringField;
    SDFieldsREGRA_UPDATE: TStringField;
    SDFieldsREGRA_DELETE: TStringField;
    QrTabelas: TmySQLQuery;
    QrTabelasTabela: TStringField;
    QrCampos: TmySQLQuery;
    QrCamposTabela: TStringField;
    QrCamposCampo: TStringField;
    QrCamposDataType: TStringField;
    QrCamposTamanho: TIntegerField;
    QrCamposEscala: TIntegerField;
    QrCamposPrimaryKey: TStringField;
    QrCamposNotNull: TStringField;
    QrCamposForeignKey: TStringField;
    QrCamposIndiceChave: TStringField;
    QrCamposTabelaChave: TStringField;
    QrCamposCampoChave: TStringField;
    QrCamposRegraUpdate: TStringField;
    QrCamposRegraDelete: TStringField;
    SDDados: TSimpleDataSet;
    SDTabelas: TSimpleDataSet;
    DsTabelas: TDataSource;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbIB_DBClick(Sender: TObject);
    procedure BtIB_DBClick(Sender: TObject);
    procedure BtErradoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FTabTabs: String;
    function  Conectou(DBNome: String): Boolean;
    function  DefineDataType(DataType: String; Tamanho, Escala: Integer): String;
    function  ExcluiTabela(QryAux: TMySQLQuery; DB, Tabela: String): Boolean;
    procedure ImportarRegistrosTabelas();
    function  TabelaExiste(QryAux: TMySQLQuery; DB, Tabela: String): Boolean;
    procedure VerificaEstruturaTabelas();
    //
    procedure EstruturaBDs();
    procedure EstruturaTabela(NomeTab: String; SD: TSimpleDataset);

  public
    { Public declarations }
  end;

  var
  FmIB_BD_Ant: TFmIB_BD_Ant;

implementation

uses UnMyObjects, MyMySQL;

{$R *.DFM}

procedure TFmIB_BD_Ant.BtIB_DBClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    DBFB.Connected := True;
    //
    SDFields.Active := False;
    SDFields.Active := True;
    //
    //BtErrado.Enabled := SDFields.Active;
    //
    BtOK.Enabled := SDFields.Active;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmIB_BD_Ant.BtOKClick(Sender: TObject);
var
  //Txt,
  SQLa, SQLb, Tb, Campo, Extra, Temp: String;
  //K,
  I, N, Tam, J: Integer;
  Valor: WideString;
  //
  Campos: array of String;
  Mostra: Boolean;
  //
  Comeca: Boolean;
begin
  //
  LaAvisoR1.Font.Color := clSilver;
  LaAvisoR2.Font.Color := clRed;
  //
  EstruturaBDs();
  //Comeca := False;
  UnMyMySQL.AbreMySQLQuery1(QrTabelas, [
  'SELECT DISTINCT Tabela ',
  'FROM ' + FTabTabs,
  'ORDER BY Tabela ',
  '']);
  PB1.Max := QrTabelas.RecordCount;
  PB1.Position := 0;
  QrTabelas.First;
  //QtdCampos := 0;
  while not QrTabelas.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
    MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, '...');
    MyObjects.Informa2(LaAvisoB1, LaAvisoB2, False, '...');
    //
    Tam := 0;
    SetLength(Campos, Tam);
    Tb := QrTabelasTabela.Value;
    //if (LowerCase(Tb) = 'modelos') or (LowerCase(Tb) = 'orcamentosib') then
    //if (LowerCase(Tb) = 'pragas') then
      Comeca := True;
    if Comeca then
    begin
      MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, 'Tabela "' + Tb + '". ');
      //
      UnMyMySQL.AbreMySQLQuery1(QrCampos, [
      'SELECT * ',
      'FROM ' + FTabTabs,
      'WHERE Tabela="' + Tb + '" ',
      '']);
      //SQLa := 'INSERT INTO ' + Tb + ' VALUES ';
      SQLa := 'INSERT INTO ' + Tb + ' SET ';
      //
      MyObjects.Informa2(LaAvisoB1, LaAvisoB2, True, 'Abrindo tabela no FireBird. ');
      SDDados.Close;
      SDDados.DataSet.CommandText := 'SELECT * FROM ' + Tb;
      SDDados.Open;

      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Criando tabela no MySQL.');
      EstruturaTabela(Tb, SDDados);
      QrUpd.SQL.Text := Memo1.Text;
      UnMyMySQL.ExecutaQuery(QrUpd);

      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
      UnMyMySQL.ExecutaMySQLQuery1(QrUpd, [
      'DELETE FROM ' + tb,
      '']);

      MyObjects.Informa2(LaAvisoB1, LaAvisoB2, True, 'Importando. ');

      SDDados.First;
      while not SDDados.Eof do
      begin
        try
          MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
            Geral.FF0(SDDados.RecNo) + ' de ' + Geral.FF0(SDDados.RecordCount));
          SQLb := '';
          N := 0;
          Mostra := False;
          for I := 0 to SDDados.FieldCount - 1 do
          begin
            if SDDados.Fields[I].Value <> Null then
            begin
              //SDDados.FieldByName('aa').Value
              if N > 0 then
                SQLb := SQLb + ', ';
              //
              Extra := '';
              Campo := SDDados.Fields[I].FieldName;
              {
              if Lowercase(Campo) = 'orctexto' then
                ShowMessage(Campo);
              }
              if SDDados.Fields[I].DataType = ftFMTBcd then
                Valor := Geral.VariavelToString(TFMTBCDField(SDDados.Fields[I]).AsVariant)
              else
                if (SDDados.Fields[I].DataType in ([ftBlob])) then
                begin
                  if pos('{\rtf', SDDados.Fields[I].Value) = 1 then
                  begin
                    Temp := SDDados.Fields[I].AsString;
                    Valor := '';
                    for J := 1 to Length(Temp) do
                    begin
                      if Ord(Temp[J]) <> 0 then
                        Valor := Valor + Temp[J];
                    end;
                    Valor := '"' + Valor + '"';
                  end else
                  if pos('BM', SDDados.Fields[I].Value) = 1 then
                  begin
                    // n�o faz nada!parei aqui
                  end else
                    Valor := Geral.VariavelToString(SDDados.Fields[I].Value);
                end
                else
                if (SDDados.Fields[I].DataType in ([ftMemo])) then
                begin
                  if pos('{\rtf', SDDados.Fields[I].Value) = 1 then
                  begin
                    Temp := SDDados.Fields[I].AsString;
                    Valor := '';
                    for J := 1 to Length(Temp) do
                    begin
                      if Ord(Temp[J]) <> 0 then
                        Valor := Valor + Temp[J];
                    end;
                    Valor := '"' + Valor + '"';
                  end else
                    Valor := Geral.VariavelToString(SDDados.Fields[I].Value);
                end
                else
                  Valor := Geral.VariavelToString(SDDados.Fields[I].Value);
              if Valor = 'Null' then
                Valor := '';
              SQLb := SQLb + Campo + '='(*+ ''''*) + Extra + Valor + Extra(*+ ''''*);
              N := N + 1;
            end;
          end;
          //
          //SQLb := SQLa + '(' + SQLb + ')';
          SQLb := SQLa + SQLb;
          if Mostra then
            Geral.MensagemBox(SQLb, 'SQL a ser Executada', MB_OK+MB_ICONINFORMATION);
          //
          QrUpd.SQL.Text := SQLb;

          UnMyMySQL.ExecutaQuery(QrUpd);
          //
  {
          if Tam > 0 then
          begin
            if (LowerCase(Tb) = 'modelos') then
            begin
              QrUpd.SQL.Clear;
              QrUpd.SQL.Add('UPDATE ' + Tb + ' SET TEXTO=:P0 WHERE ID_MODELO=:P1');
              QrUpd.Params[0].AsString := SDDados.FieldByName('TEXTO').AsString;
              QrUpd.Params[1].AsInteger := SDDados.FieldByName('ID_MODELO').AsInteger;
              QrUpd.ExecSQL;
            end
            else
            if (LowerCase(Tb) = 'orcamentoib') then
            begin
              QrUpd.SQL.Clear;
              QrUpd.SQL.Add('UPDATE ' + Tb + ' SET ORCTEXTO=:P0 WHERE ORCNUMERO=:P1');
              QrUpd.Params[0].AsString := SDDados.FieldByName('ORCTEXTO').AsString;
              QrUpd.Params[1].AsInteger := SDDados.FieldByName('ORCNUMERO').AsInteger;
              QrUpd.ExecSQL;
            end
            else
            //if (LowerCase(Tb) = '') then
            begin
              Geral.MensagemBox(Tb, 'Aviso', MB_OK+MB_ICONINFORMATION);
              (*
              QrUpd.SQL.Clear;
              QrUpd.SQL.Add('UPDATE orcamentoib SET ORCTEXTO=:P0 WHERE ORCNUMERO=:P1');
              QrUpd.Params[0].AsString := SDDados.FieldByName('ORCTEXTO').AsString;
              QrUpd.Params[1].AsInteger := SDDados.FieldByName('ORCNUMERO').AsInteger;
              QrUpd.ExecSQL;
              *)
            end;
          end;
  }
          //
          except
            ;
          end;
          SDDados.Next;
        end;
        // Parei aqui!
        //
      end;
    QrTabelas.Next;
  end;
  //
  Geral.MensagemBox('Cria��o finalizada!', 'Mensagem', MB_OK+MB_ICONINFORMATION);
  MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, '...');
  MyObjects.Informa2(LaAvisoB1, LaAvisoB2, False, '...');
  PB1.Position := 0;
  //
end;

procedure TFmIB_BD_Ant.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmIB_BD_Ant.Conectou(DBNome: String): Boolean;
begin
  DBAnt.Disconnect;
  DBAnt.DatabaseName := DBNome;
  DBAnt.Host := EdIPServer.Text;
  DBAnt.Port := EdPorta.ValueVariant;
  DBAnt.UserName := 'root';
  DBAnt.UserPassword := 'wkljwery' + 'hvbirt';
  DBAnt.Connect;
  Result := DBAnt.Connected;
end;

function TFmIB_BD_Ant.DefineDataType(DataType: String; Tamanho,
  Escala: Integer): String;
var
  Tam, Casas: Integer;
begin
  Result := DataType;
  Casas := 0;
  Tam := 0;
  if DataType = 'VARCHAR' then
  begin
    Result := DataType + '(' + Geral.FF0(Tamanho) + ')';
  end else
  if DataType = 'INTEGER' then
  begin
    if Tamanho = 0 then
      Tam := 11;
    Result := DataType + '(' + Geral.FF0(Tam) + ')';
  end else
  if DataType = 'SMALLINT' then
  begin
    if Tamanho = 0 then
      Tam := 4; // 3 ???
    Result := DataType + '(' + Geral.FF0(Tam) + ')';
  end else
  if DataType = 'DOUBLE' then
  begin
    if Tamanho = 0 then
      Tam := 15;
    if Escala = 0 then
      Casas := 6;
    Result := DataType + '(' + Geral.FF0(Tam) + ',' + Geral.FF0(Casas) + ')';
  end else
  if DataType = 'FLOAT' then
  begin
    Result := DataType; // + '(' + Geral.FF0(Tamanho) + ')';
  end else
  (*if DataType = 'NUMERIC' then
  begin
    Result := DataType + '(' + Geral.FF0(Tamanho) + ')';
  end else*)
  if DataType = 'CHAR' then
  begin
    Result := DataType + '(' + Geral.FF0(Tamanho) + ')';
  end else
  if DataType = 'BLOB' then
  begin
    Result := 'TEXT';
  end else
  if DataType = 'DATE' then
  begin
    Result := DataType;
  end else
  if DataType = 'TIMESTAMP' then
  begin
    Result := DataType;
  end else
  begin
    Result := '#ERRO#';
    Geral.MensagemBox('"Data Type" n�o implementado: "' + DataType + '"',
    'ERRO', MB_OK+MB_ICONERROR);
  end;
end;

procedure TFmIB_BD_Ant.BtErradoClick(Sender: TObject);
var
  DB: String;
  FLCampos   : TList;
  FLIndices  : TList;
var
  Tabela, Campo, DataType, PrimaryKey, NotNull, ForeignKey, IndiceChave, TabelaChave, CampoChave, RegraUpdate, RegraDelete: String;
  Tamanho, Escala: Integer;
begin
  MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, 'Verificando e conectando ao BD ' + DB);
  DB := LowerCase(EdDBNome.Text);
  if Conectou('mysql') then
  begin
    try
      FLCampos  := TList.Create;
      FLIndices := TList.Create;
      //
      UnMyMySQL.AbreMySQLQuery1(QrAux, [
      'SHOW DATABASES LIKE "' + DB + '" ',
      '']);
      if QrAux.RecordCount = 0 then
      begin
        UnMyMySQL.ExecutaMySQLQuery1(QrUpd, [
        'CREATE DATABASE ' + DB,
        '']);
      end;
      if not Conectou(DB) then Exit;
      //
      MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, 'Verificando a tabela de tabelas');
      //if not TabelaExiste(QrAux, DB, FTabTabs) then
      if ExcluiTabela(QrAux, DB, FTabTabs) then
      begin
        FLCampos.Clear;
        FLIndices.Clear;
        UnMyMySQL.AdicionaCampo(FLCampos, 'Tabela'       , 'varchar(60)', '?');
        UnMyMySQL.AdicionaCampo(FLCampos, 'Campo'        , 'varchar(60)', '?');
        UnMyMySQL.AdicionaCampo(FLCampos, 'DataType'     , 'varchar(30)', '?');
        UnMyMySQL.AdicionaCampo(FLCampos, 'Tamanho'      , 'int(11)'    , '0');
        UnMyMySQL.AdicionaCampo(FLCampos, 'Escala'       , 'int(11)'    , '0');
        UnMyMySQL.AdicionaCampo(FLCampos, 'PrimaryKey'   , 'char(1)'    , 'o');
        UnMyMySQL.AdicionaCampo(FLCampos, 'NotNull'      , 'char(1)'    , 'o');
        UnMyMySQL.AdicionaCampo(FLCampos, 'ForeignKey'   , 'char(1)'    , 'o');
        UnMyMySQL.AdicionaCampo(FLCampos, 'IndiceChave'  , 'varchar(60)', '' );
        UnMyMySQL.AdicionaCampo(FLCampos, 'TabelaChave'  , 'varchar(60)', '' );
        UnMyMySQL.AdicionaCampo(FLCampos, 'CampoChave'   , 'varchar(60)', '' );
        UnMyMySQL.AdicionaCampo(FLCampos, 'RegraUpdate'  , 'varchar(30)', '?');
        UnMyMySQL.AdicionaCampo(FLCampos, 'RegraDelete'  , 'varchar(30)', '?');
        //
        UnMyMySQL.CriaTabela(QrUpd, FTabTabs, FTabTabs, Memo1, actCreate,
        LaAvisoB1, LaAvisoB2, LaAvisoG1, LaAvisoG2, FLCampos, FLIndices);
      end;
      //
      SDFields.First;
      while not SDFields.Eof do
      begin
        MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True,
          'Inserindo informa��es das tabelas na tabela das tabelas ' +
        Geral.FF0(SDFields.RecNo) + ' de ' + Geral.FF0(SDFields.RecordCount));
        Tabela         := SDFieldsTABELA.Value;
        Campo          := SDFieldsCAMPO.Value;
        DataType       := SDFieldsTIPO_CAMPO.Value;
        Tamanho        := SDFieldsTAMANHO.Value;
        Escala         := SDFieldsESCALA.AsInteger;
        PrimaryKey     := SDFieldsPRIMARY_KEY.Value;
        NotNull        := SDFieldsNOT_NULL.Value;
        ForeignKey     := SDFieldsFOREIGN_KEY.Value;
        IndiceChave    := SDFieldsINDICE_CHAVE.Value;
        TabelaChave    := SDFieldsTABELA_CHAVE.Value;
        CampoChave     := SDFieldsCAMPO_CHAVE.Value;
        RegraUpdate    := SDFieldsREGRA_UPDATE.Value;
        RegraDelete    := SDFieldsREGRA_DELETE.Value;
        //
        //if
        //if (Tabela= 'BOLETO_PAGAR') and (Campo = '' then

        UnMyMySQL.SQLInsUpd(QrUpd, stIns, '_tab_tabs_', False, [
        'Tabela', 'Campo', 'DataType',
        'Tamanho', 'Escala', 'PrimaryKey',
        'NotNull', 'ForeignKey', 'IndiceChave',
        'TabelaChave', 'CampoChave', 'RegraUpdate',
        'RegraDelete'], [
        ], [
        Tabela, Campo, DataType,
        Tamanho, Escala, PrimaryKey,
        NotNull, ForeignKey, IndiceChave,
        TabelaChave, CampoChave, RegraUpdate,
        RegraDelete], [
        ], False);
        //
        SDFields.Next;
      end;
      //
      VerificaEstruturaTabelas();
      //
      ImportarRegistrosTabelas();
      //...
      Geral.MensagemBox('Verifica��o finalizada!', 'Mensagem', MB_OK+MB_ICONINFORMATION);
      BtOK.Enabled := True;
      //
      MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
    finally
{
      if FLCampos <> nil then
        FLCampos := nil;
      if FLIndices <> nil then
        FLIndices := nil;
}
    end;
  end;
end;

procedure TFmIB_BD_Ant.EstruturaBDs();
var
  DB: String;
  FLCampos   : TList;
  FLIndices  : TList;
var
  Tabela, Campo, DataType, PrimaryKey, NotNull, ForeignKey, IndiceChave, TabelaChave, CampoChave, RegraUpdate, RegraDelete: String;
  Tamanho, Escala: Integer;
begin
  MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, 'Verificando e conectando ao BD ' + DB);
  DB := LowerCase(EdDBNome.Text);
  if Conectou('mysql') then
  begin
    try
      FLCampos  := TList.Create;
      FLIndices := TList.Create;
      //
      UnMyMySQL.AbreMySQLQuery1(QrAux, [
      'SHOW DATABASES LIKE "' + DB + '" ',
      '']);
      if QrAux.RecordCount = 0 then
      begin
        UnMyMySQL.ExecutaMySQLQuery1(QrUpd, [
        'CREATE DATABASE ' + DB,
        '']);
      end;
      if not Conectou(DB) then Exit;
      //
      MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, 'Verificando a tabela de tabelas');
      //if not TabelaExiste(QrAux, DB, FTabTabs) then
      if ExcluiTabela(QrAux, DB, FTabTabs) then
      begin
        FLCampos.Clear;
        FLIndices.Clear;
        UnMyMySQL.AdicionaCampo(FLCampos, 'Tabela'       , 'varchar(60)', '?');
        UnMyMySQL.AdicionaCampo(FLCampos, 'Campo'        , 'varchar(60)', '?');
        UnMyMySQL.AdicionaCampo(FLCampos, 'DataType'     , 'varchar(30)', '?');
        UnMyMySQL.AdicionaCampo(FLCampos, 'Tamanho'      , 'int(11)'    , '0');
        UnMyMySQL.AdicionaCampo(FLCampos, 'Escala'       , 'int(11)'    , '0');
        UnMyMySQL.AdicionaCampo(FLCampos, 'PrimaryKey'   , 'char(1)'    , 'o');
        UnMyMySQL.AdicionaCampo(FLCampos, 'NotNull'      , 'char(1)'    , 'o');
        UnMyMySQL.AdicionaCampo(FLCampos, 'ForeignKey'   , 'char(1)'    , 'o');
        UnMyMySQL.AdicionaCampo(FLCampos, 'IndiceChave'  , 'varchar(60)', '' );
        UnMyMySQL.AdicionaCampo(FLCampos, 'TabelaChave'  , 'varchar(60)', '' );
        UnMyMySQL.AdicionaCampo(FLCampos, 'CampoChave'   , 'varchar(60)', '' );
        UnMyMySQL.AdicionaCampo(FLCampos, 'RegraUpdate'  , 'varchar(30)', '?');
        UnMyMySQL.AdicionaCampo(FLCampos, 'RegraDelete'  , 'varchar(30)', '?');
        //
        UnMyMySQL.CriaTabela(QrUpd, FTabTabs, FTabTabs, Memo1, actCreate,
        LaAvisoB1, LaAvisoB2, LaAvisoG1, LaAvisoG2, FLCampos, FLIndices);
      end;
      //
      PB1.Max := SDFields.RecordCount;
      PB1.Position := SDFields.RecordCount;
      SDFields.First;
      while not SDFields.Eof do
      begin
        PB1.Position := PB1.Position - 1;
        MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True,
          'Inserindo informa��es das tabelas na tabela das tabelas ' +
        Geral.FF0(SDFields.RecNo) + ' de ' + Geral.FF0(SDFields.RecordCount));
        Tabela         := SDFieldsTABELA.Value;
        Campo          := SDFieldsCAMPO.Value;
        DataType       := SDFieldsTIPO_CAMPO.Value;
        Tamanho        := SDFieldsTAMANHO.Value;
        Escala         := SDFieldsESCALA.AsInteger;
        PrimaryKey     := SDFieldsPRIMARY_KEY.Value;
        NotNull        := SDFieldsNOT_NULL.Value;
        ForeignKey     := SDFieldsFOREIGN_KEY.Value;
        IndiceChave    := SDFieldsINDICE_CHAVE.Value;
        TabelaChave    := SDFieldsTABELA_CHAVE.Value;
        CampoChave     := SDFieldsCAMPO_CHAVE.Value;
        RegraUpdate    := SDFieldsREGRA_UPDATE.Value;
        RegraDelete    := SDFieldsREGRA_DELETE.Value;
        //
        //if
        //if (Tabela= 'BOLETO_PAGAR') and (Campo = '' then

        UnMyMySQL.SQLInsUpd(QrUpd, stIns, '_tab_tabs_', False, [
        'Tabela', 'Campo', 'DataType',
        'Tamanho', 'Escala', 'PrimaryKey',
        'NotNull', 'ForeignKey', 'IndiceChave',
        'TabelaChave', 'CampoChave', 'RegraUpdate',
        'RegraDelete'], [
        ], [
        Tabela, Campo, DataType,
        Tamanho, Escala, PrimaryKey,
        NotNull, ForeignKey, IndiceChave,
        TabelaChave, CampoChave, RegraUpdate,
        RegraDelete], [
        ], False);
        //
        SDFields.Next;
      end;
      //
{
      VerificaEstruturaTabelas();
      //
      ImportarRegistrosTabelas();
      //...
      Geral.MensagemBox('Verifica��o finalizada!', 'Mensagem', MB_OK+MB_ICONINFORMATION);
}
      BtOK.Enabled := True;
      //
      MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
    finally
{
      if FLCampos <> nil then
        FLCampos := nil;
      if FLIndices <> nil then
        FLIndices := nil;
}
    end;
  end;
end;

procedure TFmIB_BD_Ant.EstruturaTabela(NomeTab: String; SD: TSimpleDataset);
  function DC(Texto: String; Tamanho: Integer): String;
  begin
    Result := Texto;
    while length(Result) < Tamanho do
      Result := Result + ' ';
  end;
(*
const
  NomeTab = 'NomeTabela';
*)
var
  Memo: TMemo;
  FIndice,
  Linha, Virgula, V1, V2, V3, V5: String;
  //
  //J,
  I, T, P: Integer;
begin
  FIndice := '';
  Memo := Memo1;
  Memo.Lines.Clear;

{
  if Nome = Lowercase('CNAB_Rem') then begin
    Qry.SQL.Add('CREATE TABLE CNAB_Rem (');
    Qry.SQL.Add('  Item      int(11)    AUTO_INCREMENT, ');
    Qry.SQL.Add('  Linha     int(11)    NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  Codigo    int(11)    NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  TipoDado  tinyint(3) NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  DadoI     int(11)             DEFAULT NULL, ');
    Qry.SQL.Add('  DadoF     double              DEFAULT NULL, ');
    Qry.SQL.Add('  DadoD     date                DEFAULT NULL, ');
    Qry.SQL.Add('  DadoH     time                DEFAULT NULL, ');
    Qry.SQL.Add('  DadoT     varchar(255)        DEFAULT "", ');
    Qry.SQL.Add('  PRIMARY KEY (Item)');
    Qry.SQL.Add(') TYPE=MyISAM');
    Qry.ExecSQL;
  end else
}
  //Memo.Lines.Add('  if Nome = Lowercase('''+ NomeTab +''') then begin');
  Memo.Lines.Add('DROP TABLE IF EXISTS ' + NomeTab +';');
  Memo.Lines.Add('CREATE TABLE ' + NomeTab +' (');
  for I := 0 to SD.Fields.Count - 1 do
  begin
    V1 := SD.Fields[I].FullName;
    case SD.Fields[I].DataType of
      ftUnknown:        V2 := 'Unknown';
      ftString:         V2 := 'Varchar'; // diferente
      ftSmallint:       V2 := 'Smallint';
      ftInteger:        V2 := 'Integer';
      ftWord:           V2 := 'Word';
      ftBoolean:        V2 := 'tinyint(1)'; // 'Boolean'; Diferente? n�o testado
      ftFloat:          V2 := 'Float';
      ftCurrency:       V2 := 'Currency';
      ftBCD:            V2 := 'BCD';
      ftDate:           V2 := 'Date';
      ftTime:           V2 := 'Time';
      ftDateTime:       V2 := 'DateTime';
      ftBytes:          V2 := 'Bytes';
      ftVarBytes:       V2 := 'VarBytes';
      ftAutoInc:        V2 := 'AutoInc';
      ftMemo:           V2 := 'Text';  // diferente
      ftGraphic:        V2 := 'Graphic';
      ftFmtMemo:        V2 := 'FmtMemo';
      ftParadoxOle:     V2 := 'ParadoxOle';
      ftDBaseOle:       V2 := 'DBaseOle';
      ftTypedBinary:    V2 := 'TypedBinary';
      ftCursor:         V2 := 'Cursor';
      ftFixedChar:      V2 := 'FixedChar';
      ftWideString:     V2 := 'WideString';
      ftLargeint:       V2 := 'Largeint';
      ftADT:            V2 := 'ADT';
      ftArray:          V2 := 'Array';
      ftReference:      V2 := 'Reference';
      ftDataSet:        V2 := 'DataSet';
      ftOraBlob:        V2 := 'OraBlob';
      ftOraClob:        V2 := 'OraClob';
      ftVariant:        V2 := 'Variant';
      ftInterface:      V2 := 'Interface';
      ftIDispatch:      V2 := 'IDispatch';
      ftGuid:           V2 := 'Guid';
      ftTimeStamp:      V2 := 'TimeStamp';
      ftFMTBcd:         V2 := 'Double'; //'FMTBcd';
      ftFixedWideChar:  V2 := 'FixedWideChar';
      ftWideMemo:       V2 := 'WideMemo';
      ftOraTimeStamp:   V2 := 'OraTimeStamp';
      ftOraInterval:    V2 := 'OraInterval';
      ftBlob:
      begin
        V2 := '';
        SD.First;
        while not SD.Eof do
        begin
          if pos('BM[', SD.Fields[I].Value) = 1 then
          begin
            V2 := 'blob';
            SD.Last;
          end;
          SD.Next;
        end;
        SD.First;
        if V2 = '' then
          V2 := 'Text';
      end;
    end;
    if not (SD.Fields[I].DataType in ([ftBlob, ftMemo])) then
    begin
      if SD.Fields[I].DataType = ftFMTBcd then
        T := TFMTBcdField(SD.Fields[I]).Precision
      else
        T := SD.Fields[I].Size;
      if T > 0 then
      begin
        V2 := V2 + '(' + FormatFloat('0', T);
        case SD.Fields[I].DataType of
          ftFloat:   P := TFloatField(SD.Fields[I]).Precision;
          ftBCD:     P := TBCDField(SD.Fields[I]).Precision;
          ftFMTBcd:  P := SD.Fields[I].Size;
          else P := 0;
        end;
        if P > 0 then
          V2 := V2 + ',' + FormatFloat('0', P);
        V2 := V2 + ')';
      end;
    end;
    //
    if SD.Fields[I].Required then
      V3 := 'NOT NULL'
    else
      V3 := '';
    if SD.Fields[I].IsIndexField then
    begin
      FIndice := FIndice + V1 + ',';
      //V4 := 'PRI';
    end;
    V5 := ''; // N�o tem como saber o valor default?
    //Linha := '    Qry.SQL.Add(''  '+
    Linha := '    ' +
      DC(V1,20)+' '+
      DC(V2,12)+' '+
      DC(V3,08)+' '+
      //DC(V4,00)+' '+
      DC(V5,20);
    if (I + 1 < SD.Fields.Count) or (FIndice <> '') then
      Virgula := ','
    else Virgula := '';
    Linha := Linha + Virgula;
    //Linha := Linha + ''');';
    //
    Memo.Lines.Add(Linha);
  end;
  if FIndice <> '' then
  begin
    FIndice := Copy(FIndice, 1, Length(FIndice)-2);
    //Memo.Lines.Add('    Qry.SQL.Add(''  PRIMARY KEY (' + FIndice + ')'');');
    Memo.Lines.Add('    PRIMARY KEY (' + FIndice + ')');
  end;
  Memo.Lines.Add(') TYPE=MyISAM;');
  //Memo.Lines.Add('    Qry.SQL.Add('') TYPE=MyISAM'');');
  //Memo.Lines.Add('    Qry.ExecSQL;');
  //Memo.Lines.Add('  end else');
end;

function TFmIB_BD_Ant.ExcluiTabela(QryAux: TMySQLQuery; DB,
  Tabela: String): Boolean;
begin
  UnMyMySQL.ExecutaMySQLQuery1(QryAux, [
  'DROP TABLE IF EXISTS ' + Tabela,
  '']);
  Result := not TabelaExiste(QryAux, DB, Tabela);
end;

procedure TFmIB_BD_Ant.FormActivate(Sender: TObject);
var
  Arq: String;
begin
  ImgTipo.SQLType := stPsq;
  MyObjects.CorIniComponente();
  Arq := EdIB_DB.Text;
  BtIB_DB.Enabled := FileExists(Arq);
  //
  LaAvisoR1.Color := clSilver;
  LaAvisoR2.Color := clRed;
end;

procedure TFmIB_BD_Ant.FormCreate(Sender: TObject);
begin
  FTabTabs := '_tab_tabs_';
end;

procedure TFmIB_BD_Ant.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAvisoR1, LaAvisoR2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmIB_BD_Ant.ImportarRegistrosTabelas();
var
  Txt, SQLa, SQLb, Tb, Valor, Campo: String;
  I, N: Integer;
begin
  UnMyMySQL.AbreMySQLQuery1(QrTabelas, [
  'SELECT DISTINCT Tabela ',
  'FROM ' + FTabTabs,
  'ORDER BY Tabela ',
  '']);
  QrTabelas.First;
  while not QrTabelas.Eof do
  begin
    Tb := QrTabelasTabela.Value;
    //if (Tb <> 'BOLETO_PAGAR') and (TB <> 'CHEQUEPAG') and (TB <> 'CHEQUE_REC') and
       //(Tb <> 'CONCILIA') and (TB <> 'CONTA') (*and (TB <> 'CHEQUE_REC')*) then
      //ShowMessage('BOLETO_PAGAR');
    begin
      MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, 'Abrindo tabela "' + Tb + '". ');
      //
      UnMyMySQL.AbreMySQLQuery1(QrCampos, [
      'SELECT * ',
      'FROM ' + FTabTabs,
      'WHERE Tabela="' + tb + '" ',
      '']);
      //SQLa := 'INSERT INTO ' + Tb + ' VALUES ';
      SQLa := 'INSERT INTO ' + Tb + ' SET ';
      //
      SDDados.Close;
      SDDados.DataSet.CommandText := 'SELECT * FROM ' + Tb;
      SDDados.Open;

      MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, 'Exclu�ndo dados da tabela "' + Tb + '". ');
      UnMyMySQL.ExecutaMySQLQuery1(QrUpd, [
      'DELETE FROM ' + tb,
      '']);

      Txt := 'Importando dados da tabela "' + Tb + '". ';
      MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, Txt);

      SDDados.First;
      while not SDDados.Eof do
      begin
        MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, Txt + ' Registro ' +
          Geral.FF0(SDDados.RecNo) + ' de ' + Geral.FF0(SDDados.RecordCount));
        SQLb := '';
        N := 0;
        for I := 0 to SDDados.FieldCount - 1 do
        begin
          if SDDados.Fields[I].Value <> Null then
          begin
            //SDDados.FieldByName('aa').Value
            if N > 0 then
              SQLb := SQLb + ', ';
            //
            Campo := SDDados.Fields[I].FieldName;
            if SDDados.Fields[I].DataType = ftFMTBcd then
              Valor := Geral.VariavelToString(TFMTBCDField(SDDados.Fields[I]).AsVariant)
            else
              Valor := Geral.VariavelToString(SDDados.Fields[I].Value);
            if Valor <> 'Null' then
              //Valor := '';
            SQLb := SQLb +Campo + '='(*+ ''''*) + Valor (*+ ''''*);
            N := N + 1;
          end;
        end;
        //
        //SQLb := SQLa + '(' + SQLb + ')';
        SQLb := SQLa + SQLb;
        //
        QrUpd.SQL.Text := SQLb;

        //try
          UnMyMySQL.ExecutaQuery(QrUpd);
        //except
          ;
        //end;
  {
        QrCampos.First;
        while not QrCampos.Eof do
        begin
          //
          //
          QrCampos.Next;
        end;
  }
        //
        SDDados.Next;
      end;

      // Parei aqui!

      //
    end;  
    QrTabelas.Next;
  end;
  //
  MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
end;

procedure TFmIB_BD_Ant.SbIB_DBClick(Sender: TObject);
var
  Arq: String;
begin
  MyObjects.DefineArquivo2(Self, EdIB_DB,
    'C:\Dermatek\SPED\DownLoadTabsEFD', 'tb6.txt', ppAtual_Edit);
  Arq := EdIB_DB.Text;
  BtIB_DB.Enabled := FileExists(Arq);
end;

function TFmIB_BD_Ant.TabelaExiste(QryAux: TMySQLQuery; DB, Tabela: String): Boolean;
begin
  UnMyMySQL.AbreMySQLQuery1(QryAux, [
  'SHOW TABLES FROM ' + DB + ' LIKE "' + Tabela + '" ',
  '']);
  //
  Result := QryAux.RecordCount > 0;
end;

procedure TFmIB_BD_Ant.VerificaEstruturaTabelas();
var
  Txt, DB, Tb: String;
  LCampos   : TList;
  LIndices  : TList;
  //
  Campo, DataType: String;
begin
  DB := LowerCase(EdDBNome.Text);
  UnMyMySQL.AbreMySQLQuery1(QrTabelas, [
  'SELECT DISTINCT Tabela ',
  'FROM ' + FTabTabs,
  'ORDER BY Tabela ',
  '']);
  QrTabelas.First;
  while not QrTabelas.Eof do
  begin
    Tb := QrTabelasTabela.Value;
    Txt := 'Verificando tabela "' + Tb + '". ';
    MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, Txt);
    // Parei aqui!
    if ExcluiTabela(QrAux, DB, Tb) then
    begin
      //
      UnMyMySQL.AbreMySQLQuery1(QrCampos, [
      'SELECT * ',
      'FROM ' + FTabTabs,
      'WHERE Tabela="' + tb + '" ',
      '']);
      //
      LCampos  := TList.Create;
      LIndices := TList.Create;
      try
        QrCampos.First;
        if Uppercase(tb) = 'BOLETO_PAGAR' then
          ShowMessage(tb);
        while not QrCampos.Eof do
        begin
          Campo :=  QrCamposCampo.Value;
          DataType := DefineDataType(
            QrCamposDataType.Value, QrCamposTamanho.Value, QrCamposEscala.Value);
          UnMyMySQL.AdicionaCampo(LCampos, Campo          , DataType, '');
          //
          QrCampos.Next;
        end;
        //
        UnMyMySQL.CriaTabela(QrUpd, tb, tb, Memo1, actCreate,
        LaAvisoB1, LaAvisoB2, LaAvisoG1, LaAvisoG2, LCampos, LIndices);
      finally
        //ver o que fazer! est� destriundo no criatabela?
        //LCampos.Free;
        //LIndices.Free;
      end;
    end;
    //
    QrTabelas.Next;
  end;
end;

end.
