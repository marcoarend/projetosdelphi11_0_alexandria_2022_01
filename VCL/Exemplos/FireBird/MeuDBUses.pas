unit MeuDBUses;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids;

type
  TFmMeuDBUSes = class(TForm)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ImprimeStringGrid(StringGrid: TStringGrid; Titulo: String);
    procedure ImprimeDBGrid(DBGrid: TDBGrid; Titulo: String);
    procedure VirtualKey_F7(Texto: String);
  end;

var
  FmMeuDBUSes: TFmMeuDBUSes;

implementation

{$R *.dfm}

{ TFmMeuDBUSes }

procedure TFmMeuDBUSes.ImprimeDBGrid(DBGrid: TDBGrid; Titulo: String);
{
var
  Query: TABSQuery;
}
begin
{ TODO : Impress�o DBGrid }
{
  Query := TABSQuery(DBGrid.DataSource.DataSet);
  frxDsPG.DataSet := Query;
  //
  FTipoGrade := 1; // Grade com origem de DataAware. DBGrid ...
  FTitulo := Titulo;
  FDBGrid := DBGrid;
  FGrade := TStringGrid(DBGrid);
  MyObjects.MostraPopUpNoCentro(PMImpressao);
}
end;


procedure TFmMeuDBUSes.ImprimeStringGrid(StringGrid: TStringGrid;
  Titulo: String);
(*
var
  i, j, c, r: Integer;
  Xt: array of String;
  Xv: array of String;
*)
begin
{ TODO : ImprimeStringGrid }
{
  FTipoGrade := 0; // // Grade com origem de Texto. TStringGrid...
  c := StringGrid.ColCount;
  r := StringGrid.RowCount;
  FPrintGrid := UCriar.RecriaMemTable('PrintGrid', DmodG.QrUpdPID1, False, c);
  SetLength(Xt, StringGrid.ColCount);
  SetLength(Xv, StringGrid.ColCount);
  for i := 0 to c - 1 do
    Xt[i] := 'Col' + FormatFloat('000', i);
  for i := 0 to r - 1 do
  begin
    for j := 0 to c - 1 do
      Xv[j] := StringGrid.Cells[j,i];
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO printgrid SET ');
    for j := 0 to c - 2 do
      DmodG.QrUpdPID1.SQL.Add(Xt[j] + '="' + dmkPF.DuplicaAspasDuplas(Xv[j]) + '",');
    DmodG.QrUpdPID1.SQL.Add(Xt[c-1] + '="' + dmkPF.DuplicaAspasDuplas(Xv[c-1]) + '"');

    UMyMod.ExecutaQuery(DmodG.QrUpdPID1);//.ExecSQL;
  end;
  FGrade  := StringGrid;
  FDBGrid := nil;
  FTitulo := Titulo;
  QrPg.Database   := DmodG.MyPID_DB;
  frxDsPG.DataSet := QrPG;
  MyObjects.MostraPopUpNoCentro(PMImpressao);
}
end;

procedure TFmMeuDBUSes.VirtualKey_F7(Texto: String);
begin
// 
end;

end.
