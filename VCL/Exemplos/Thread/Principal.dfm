object FMPrincipal: TFMPrincipal
  Left = 0
  Top = 0
  Caption = 'FMPrincipal'
  ClientHeight = 321
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 635
    Height = 133
    Align = alTop
    Lines.Strings = (
      'ResultsMemo')
    TabOrder = 0
    ExplicitLeft = 4
  end
  object Panel1: TPanel
    Left = 0
    Top = 280
    Width = 635
    Height = 41
    Align = alBottom
    TabOrder = 1
    ExplicitLeft = 68
    ExplicitTop = 252
    ExplicitWidth = 185
    object BtIni: TButton
      Left = 12
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Come'#231'a'
      TabOrder = 0
      OnClick = BtIniClick
    end
    object BtFim: TButton
      Left = 108
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Para'
      Enabled = False
      TabOrder = 1
      OnClick = BtFimClick
    end
  end
  object Memo2: TMemo
    Left = 0
    Top = 133
    Width = 635
    Height = 133
    Align = alTop
    Lines.Strings = (
      'ResultsMemo')
    TabOrder = 2
    ExplicitTop = 4
  end
end
