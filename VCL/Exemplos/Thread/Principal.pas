unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFMPrincipal = class(TForm)
    Memo1: TMemo;
    Panel1: TPanel;
    BtIni: TButton;
    BtFim: TButton;
    Memo2: TMemo;
    procedure BtIniClick(Sender: TObject);
    procedure BtFimClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMPrincipal: TFMPrincipal;

implementation

{$R *.dfm}

uses UnPalmForm;

procedure TFMPrincipal.BtIniClick(Sender: TObject);
begin
  Application.Createform(TPalFrm, PalFrm1);
  PalFrm1.FResultsMemo := Memo1;
  PalFrm1.StartButtonClick(Self);

  Application.Createform(TPalFrm, PalFrm2);
  PalFrm2.FResultsMemo := Memo2;
  PalFrm2.StartButtonClick(Self);

  BtFim.Enabled := True;
end;

procedure TFMPrincipal.BtFimClick(Sender: TObject);
begin
  //PalFrm1.Show;
  PalFrm1.StopButtonClick(Self);
  PalFrm1.Destroy;
  //
  //PalFrm2.Show;
  PalFrm2.StopButtonClick(Self);
  PalFrm2.Destroy;
  //
end;

end.
