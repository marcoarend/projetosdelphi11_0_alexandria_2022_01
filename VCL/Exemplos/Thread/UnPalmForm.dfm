object PalFrm: TPalFrm
  Left = 0
  Top = 0
  Caption = 'PalFrm'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object ResultsMemo: TMemo
    Left = 0
    Top = 0
    Width = 635
    Height = 258
    Align = alClient
    Lines.Strings = (
      'ResultsMemo')
    TabOrder = 0
    ExplicitLeft = 188
    ExplicitTop = 68
    ExplicitWidth = 185
    ExplicitHeight = 89
  end
  object Panel1: TPanel
    Left = 0
    Top = 258
    Width = 635
    Height = 41
    Align = alBottom
    TabOrder = 1
    ExplicitLeft = 444
    ExplicitTop = 228
    ExplicitWidth = 185
    object StopButton: TBitBtn
      Left = 84
      Top = 8
      Width = 75
      Height = 25
      Caption = 'StopButton'
      TabOrder = 0
      OnClick = StopButtonClick
    end
    object StartButton: TBitBtn
      Left = 4
      Top = 8
      Width = 75
      Height = 25
      Caption = 'StartButton'
      TabOrder = 1
      OnClick = StartButtonClick
    end
  end
end
