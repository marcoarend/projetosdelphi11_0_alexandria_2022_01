program PrimeNums;

uses
  Vcl.Forms,
  PrimeThreads in 'PrimeThreads.pas',
  BoundedBuf in 'BoundedBuf.pas',
  UnPalmForm in 'UnPalmForm.pas' {PalFrm},
  Principal in 'Principal.pas' {FMPrincipal};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFMPrincipal, FMPrincipal);
  Application.CreateForm(TPalFrm, PalFrm);
  Application.Run;
end.
