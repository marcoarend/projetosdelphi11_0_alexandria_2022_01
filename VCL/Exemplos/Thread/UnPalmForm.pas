unit UnPalmForm;


interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

const
  WM_DATA_IN_BUF = WM_APP + 1000;
  MaxMemoLines = 20;

type
  TPalFrm = class(TForm)
    ResultsMemo: TMemo;
    Panel1: TPanel;
    StopButton: TBitBtn;
    StartButton: TBitBtn;
    procedure StartButtonClick(Sender: TObject);
    procedure StopButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FStringSectInit: boolean;
    FStringBuf: TStringList;
    StringSection: TRTLCriticalSection;
    InstrcsFin: Integer;
    procedure UpdateButtons;
    procedure HandleNewData(var Message: TMessage); message WM_DATA_IN_BUF;
  public
    { Public declarations }
    FResultsMemo: TMemo;
  end;

var
  PalFrm: TPalFrm;
  PalFrm1: TPalFrm;
  PalFrm2: TPalFrm;

implementation

uses PrimeThreads, Principal;

{$R *.DFM}

procedure TPalFrm.UpdateButtons;
begin
  StopButton.Enabled := FStringSectInit;
  StartButton.Enabled := not FStringSectInit;
end;

procedure TPalFrm.StartButtonClick(Sender: TObject);
begin
  if not FStringSectInit then
  begin
    InitializeCriticalSection(StringSection);
    FStringBuf := TStringList.Create;
    FStringSectInit := true;
    StartThreads(Self, @StringSection, WM_DATA_IN_BUF, FStringBuf);
  end;
  UpdateButtons;
end;

procedure TPalFrm.StopButtonClick(Sender: TObject);
begin
  try
    if FStringSectInit then
    begin
      InstrcsFin := InstrcsFin + 1;
      FStringSectInit := false;
      ResultsMemo.Lines.Add('Please wait...');
      StopThreads;
      ResultsMemo.Lines.Add('Done!');
      FStringBuf.Free;
      FStringBuf := nil;
      DeleteCriticalSection(StringSection);
      //FStringSectInit := false;
    end;
    UpdateButtons;
  except
    //Close;
  end;
end;

procedure TPalFrm.FormCreate(Sender: TObject);
begin
  InstrcsFin := 0;
end;

procedure TPalFrm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_Delete then
    StopButtonClick(Self);
end;

procedure TPalFrm.HandleNewData(var Message: TMessage);
begin
  if FStringSectInit then
  begin
    EnterCriticalSection(StringSection);
    ResultsMemo.Lines.Add(FStringBuf.Strings[0]);
    FResultsMemo.Lines.Add(FStringBuf.Strings[0]);
    FStringBuf.Delete(0);
    LeaveCriticalSection(StringSection);
    if ResultsMemo.Lines.Count > MaxMemoLines then
      ResultsMemo.Lines.Delete(0);
  end;
end;

procedure TPalFrm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  StopButtonClick(Self);
end;

end.

