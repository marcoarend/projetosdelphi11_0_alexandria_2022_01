unit UnitGavetaCMD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Registry;

type
  TForm1 = class(TForm)
    Button1: TButton;
    SerialCombo: TComboBox;
    Button2: TButton;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  sPorta, cBuffer, Status : string;
  F : TextFile;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  F : TextFile;
begin
  AssignFile(F,SerialCombo.Text);
  Rewrite(F);
  Writeln(F,#27+#118+#140);
  CloseFile(F);
end;

procedure TForm1.FormCreate(Sender: TObject);
var i:integer;
begin
 with TRegistry.create do
  try
   rootkey:=HKEY_LOCAL_MACHINE;
   if keyexists('HARDWARE') then begin
    if openkey('HARDWARE',false)then begin
     if keyexists('DEVICEMAP') then begin
      if openkey('DEVICEMAP',false)then begin
       if keyexists('SERIALCOMM') then begin
        if openkey('SERIALCOMM',false)then begin
         GetValueNames(SerialCombo.items);
         for i:=0 to SerialCombo.items.count-1 do
          begin
           SerialCombo.items[i]:=ReadString(SerialCombo.items[i]);
          end;
         end
        end;
       end;
      end;
     end;
   end;
  finally free;
end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
Close;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  F : TextFile;
begin
  AssignFile(F,SerialCombo.Text);
  Rewrite(F);

  Writeln(F,'Teste de impressao - Linha 0');
  Writeln(F,'Teste de impressao - Linha 1');
  Writeln(F,#27#15+'Teste de Impress�o - Linha 2');
  Writeln(F,'Teste de impressao - Linha 3');
  Writeln(F,#27#18+'Teste de Impress�o - Linha 4');
  Writeln(F,'Teste de impressao - Linha 5');
  Writeln(F,#12); // Ejeta a p�gina
  CloseFile(F);
end;

end.
