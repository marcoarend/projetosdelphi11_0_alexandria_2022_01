object Form1: TForm1
  Left = 192
  Top = 152
  Width = 244
  Height = 268
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 32
    Top = 112
    Width = 161
    Height = 25
    Caption = 'Aciona Gaveta'
    TabOrder = 0
    OnClick = Button1Click
  end
  object SerialCombo: TComboBox
    Left = 40
    Top = 32
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    Text = 'Porta da impressora'
  end
  object Button2: TButton
    Left = 32
    Top = 72
    Width = 161
    Height = 25
    Caption = 'Imprime Texto'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 72
    Top = 184
    Width = 75
    Height = 25
    Caption = 'Sair'
    TabOrder = 3
    OnClick = Button3Click
  end
end
