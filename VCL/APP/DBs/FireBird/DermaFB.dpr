program DermaFB;

uses
  Forms,
  Principal in 'Principal.pas' {FmPrincipal},
  MeuDBUses in 'MeuDBUses.pas' {FmMeuDBUSes},
  Module in 'Module.pas' {Dmod: TDataModule},
  SelRadioGroup in '..\..\..\UTL\_FRM\v01_01\SelRadioGroup.pas' {FmSelRadioGroup},
  MeuFrx in '..\..\..\UTL\Print\v01_01\MeuFrx.pas' {FmMeuFrx},
  ZCF2 in '..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnDmkProcFunc in '..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  UnDmkEnums in '..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnMyObjects in '..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  UnGrl_Vars in '..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  MyMySQL in '..\..\..\UTL\MySQL\v01_01\MyMySQL.pas',
  dmkDAC_PF in '..\..\..\UTL\MySQL\v01_01\dmkDAC_PF.pas',
  UMySQLDB in '..\..\..\UTL\MySQL\v01_01\UMySQLDB.pas',
  UnGrl_Consts in '..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnGrl_Geral in '..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  VerificaConexoes in '..\..\..\UTL\_FRM\v01_01\VerificaConexoes.pas' {FmVerificaConexoes},
  UnInternalConsts in '..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  GModule in '..\..\..\UTL\MySQL\v01_01\GModule.pas' {GMod: TDataModule},
  FB_BD_Ant in 'FB_BD_Ant.pas' {FmFB_BD_Ant},
  Dedetiza in 'Dedetiza.pas' {FmDedetiza},
  UnInternalConsts3 in '..\..\..\UTL\_UNT\v01_01\UnInternalConsts3.pas';

{$R *.res}

begin
  //UnDmkProcFunc in 'C:\Geral\Units\UnDmkProcFunc.pas',
 // MyMySQL in 'C:\Outros\MySQL\MyMySQL.pas',
//  Calculadora in 'C:\Outros\Geral\Forms\Calculadora.pas' {FmCalculadora},
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TGMod, GMod);
  Application.CreateForm(TDmod, Dmod);
  Application.Run;
end.
