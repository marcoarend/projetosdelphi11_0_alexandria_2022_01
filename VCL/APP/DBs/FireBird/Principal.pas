unit Principal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, WideStrings, FMTBcd, DB, SqlExpr, Grids, DBGrids, StdCtrls, Buttons,
  IBDatabase, DBClient, SimpleDS, ExtCtrls, DBCtrls, ComCtrls, dmkGeral,
  Data.DBXFirebird, dmkEdit;

type
  TFmPrincipal = class(TForm)
    DBFB: TSQLConnection;
    DsTabelas: TDataSource;
    SDTabelas: TSimpleDataSet;
    Panel1: TPanel;
    SDFields: TSimpleDataSet;
    DsFields: TDataSource;
    SDDados: TSimpleDataSet;
    DsDados: TDataSource;
    SDFieldsTABELA: TStringField;
    SDFieldsCAMPO: TStringField;
    SDFieldsTIPO_CAMPO: TStringField;
    SDFieldsTAMANHO: TSmallintField;
    SDFieldsESCALA: TFMTBCDField;
    SDFieldsPRIMARY_KEY: TStringField;
    SDFieldsNOT_NULL: TStringField;
    SDFieldsFOREIGN_KEY: TStringField;
    SDFieldsINDICE_CHAVE: TStringField;
    SDFieldsTABELA_CHAVE: TStringField;
    SDFieldsCAMPO_CHAVE: TStringField;
    SDFieldsREGRA_UPDATE: TStringField;
    SDFieldsREGRA_DELETE: TStringField;
    Button4: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid2: TDBGrid;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    Button3: TButton;
    DBGrid3: TDBGrid;
    Button2: TButton;
    Button1: TButton;
    TabSheet3: TTabSheet;
    Panel4: TPanel;
    DBMemo1: TDBMemo;
    Campo: TLabel;
    EdCampo: TEdit;
    Button5: TButton;
    Button6: TButton;
    CkLimit: TCheckBox;
    EdSkip: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdFirst: TdmkEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure SDTabelasAfterScroll(DataSet: TDataSet);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure EdCampoExit(Sender: TObject);
    procedure SDDadosBeforeClose(DataSet: TDataSet);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReCaptionComponentesDeForm(Form: TForm);
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses FB_BD_Ant(*, Dedetiza*), Dedetiza;

{$R *.dfm}

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
begin
  SDTabelas.Active := False;
  SDTabelas.DataSet.CommandText :=
  'select rdb$relation_name ' +
  'from rdb$relations ' +
  'where rdb$view_blr is null '+
  'and (rdb$system_flag is null or rdb$system_flag = 0);';
  //
  SDTabelas.Active := True;
end;

procedure TFmPrincipal.Button1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Application.CreateForm(TFmDedetiza, FmDedetiza);
  try
    FmDedetiza.DBAnt.Connect;
    if FmDedetiza.DBAnt.Connected then
    begin
      FmDedetiza.QrTabelas.Open;
      Screen.Cursor := crDefault;
      FmDedetiza.ShowModal;
    end
    else Geral.MensagemBox('N�o foi poss�vel conectar ao BD!',
      'Aviso', MB_OK+MB_ICONWARNING);
  finally
    FmDedetiza.Destroy;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.Button2Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    SDFields.Active := False;
    SDFields.Active := True;
  finally
  Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.Button3Click(Sender: TObject);
var
  SQL: String;
begin
  SQL := 'SELECT ';
  if CkLimit.Checked then
  begin
    if EdFirst.ValueVariant > 0 then
    begin
      SQL := SQL + ' FIRST ' + Geral.FF0(EdFirst.ValueVariant);
      if EdSkip.ValueVariant > 0 then
        SQL := SQL + ' SKIP ' + Geral.FF0(EdSkip.ValueVariant);
    end;
  end;
  SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
  SDDados.Active := False;
  SDDados.DataSet.CommandText := SQL;
 // 'select FIRST 100 * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
  //+ ' LIMIT 0, 100';
  SDDados.Active := True;
end;

procedure TFmPrincipal.Button4Click(Sender: TObject);
begin
  Application.CreateForm(TFmFB_BD_Ant, FmFB_BD_Ant);
  FmFB_BD_Ant.ShowModal;
  FmFB_BD_Ant.Destroy;
end;

procedure TFmPrincipal.Button5Click(Sender: TObject);
begin
  DBFB.Connected := True;
end;

procedure TFmPrincipal.Button6Click(Sender: TObject);
var
  I: Integer;
begin
(*
  for I := 0 to SDDados.FieldCount - 1 do
  begin
    SDDados.FieldByName()
*)
end;

procedure TFmPrincipal.EdCampoExit(Sender: TObject);
begin
  if SDDados.State <> dsInactive then
  begin
    DBMemo1.DataSource := DsDados;
    DBMemo1.DataField := EdCampo.Text;
  end;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa
end;

procedure TFmPrincipal.SDDadosBeforeClose(DataSet: TDataSet);
begin
  EdCampo.Text := '';
  DBMemo1.DataField := '';
end;

procedure TFmPrincipal.SDTabelasAfterScroll(DataSet: TDataSet);
begin
 // SDDados.Active := False;
end;

end.
