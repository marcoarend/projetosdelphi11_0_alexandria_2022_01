object Dmod: TDmod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 505
  Width = 814
  object QrFields: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SHOW FIELDS FROM :p0')
    Left = 428
    Top = 159
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrMaster: TMySQLQuery
    Database = MyDB
    AfterOpen = QrMasterAfterOpen
    OnCalcFields = QrMasterCalcFields
    SQL.Strings = (
      'SELECT ma.Em, te.Tipo, te.Logo,  te.Logo2,cm.Dono, cm.Versao, '
      'cm.CNPJ, te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax,'
      
        'te.ERua, (te.ENumero+0.000) ENumero, te.EBairro, te.ECEP, te.ECo' +
        'mpl,'
      'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais,'
      'te.Respons1, te.Respons2, ma.Limite, ma.SolicitaSenha'
      'FROM entidades te, Controle cm, Ufs uf, Master ma'
      'WHERE te.Codigo=cm.Dono'
      
        'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.T' +
        'ipo=1))'
      'AND uf.Codigo=te.EUF'
      '')
    Left = 12
    Top = 143
    object QrMasterCNPJ_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterTE1_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterCEP_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrMasterEm: TStringField
      FieldName = 'Em'
      Required = True
      Size = 100
    end
    object QrMasterTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrMasterLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Required = True
    end
    object QrMasterVersao: TIntegerField
      FieldName = 'Versao'
      Required = True
    end
    object QrMasterCNPJ: TStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 18
    end
    object QrMasterIE: TStringField
      FieldName = 'IE'
      Size = 15
    end
    object QrMasterECidade: TStringField
      FieldName = 'ECidade'
      Size = 15
    end
    object QrMasterNOMEUF: TStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrMasterEFax: TStringField
      FieldName = 'EFax'
    end
    object QrMasterERua: TStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrMasterEBairro: TStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrMasterECompl: TStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrMasterEContato: TStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrMasterECel: TStringField
      FieldName = 'ECel'
    end
    object QrMasterETe1: TStringField
      FieldName = 'ETe1'
    end
    object QrMasterETe2: TStringField
      FieldName = 'ETe2'
    end
    object QrMasterETe3: TStringField
      FieldName = 'ETe3'
    end
    object QrMasterEPais: TStringField
      FieldName = 'EPais'
    end
    object QrMasterRespons1: TStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrMasterRespons2: TStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrMasterECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrMasterLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrMasterLimite: TSmallintField
      FieldName = 'Limite'
      Required = True
    end
    object QrMasterSolicitaSenha: TIntegerField
      FieldName = 'SolicitaSenha'
    end
    object QrMasterENumero: TFloatField
      FieldName = 'ENumero'
    end
  end
  object QrLivreY_D: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SELECT Controle Codigo FROM lanctos')
    Left = 320
    Top = 59
    object QrLivreY_DCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrRecCountX: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  ArtigosGrupos')
    Left = 392
    Top = 247
    object QrRecCountXRecord: TIntegerField
      FieldName = 'Record'
    end
  end
  object QrUser: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Login FROM senhas'
      'WHERE Numero=:Usuario')
    Left = 12
    Top = 95
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Usuario'
        ParamType = ptUnknown
      end>
    object QrUserLogin: TStringField
      FieldName = 'Login'
      Origin = 'DNTEACH.senhas.Login'
      Size = 128
    end
  end
  object QrSelX: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SELECT Lk FROM lanctos')
    Left = 272
    Top = 55
    object QrSelXLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.lanctos.Lk'
    end
  end
  object QrSB: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 204
    Top = 7
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSBCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrSBNome: TStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.pq.Nome'
      Size = 41
    end
  end
  object DsSB: TDataSource
    DataSet = QrSB
    Left = 232
    Top = 7
  end
  object QrSB2: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 204
    Top = 51
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSB2Codigo: TFloatField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.lanctos.Controle'
    end
    object QrSB2Nome: TStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.lanctos.Descricao'
      Size = 128
    end
  end
  object DsSB2: TDataSource
    DataSet = QrSB2
    Left = 232
    Top = 51
  end
  object QrSB3: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SELECT fa.Data Codigo, ca.Nome Nome'
      'FROM faturas fa, Carteiras ca'
      'WHERE ca.Codigo=fa.Emissao'
      'AND fa.Emissao=15')
    Left = 204
    Top = 95
    object QrSB3Codigo: TDateField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.faturas.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSB3Nome: TStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
  end
  object DsSB3: TDataSource
    DataSet = QrSB3
    Left = 232
    Top = 95
  end
  object QrDuplicIntX: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SELECT NF INTEIRO1, Cliente INTEIRO2, Codigo CODIGO'
      'FROM pqpse'
      'WHERE NF=1'
      'AND Cliente=1')
    Left = 392
    Top = 159
    object QrDuplicIntXINTEIRO1: TIntegerField
      FieldName = 'INTEIRO1'
      Origin = 'DBMBWET.pqpse.NF'
    end
    object QrDuplicIntXINTEIRO2: TIntegerField
      FieldName = 'INTEIRO2'
      Origin = 'DBMBWET.pqpse.Cliente'
    end
    object QrDuplicIntXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pqpse.Codigo'
    end
  end
  object QrDuplicStrX: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SELECT Nome NOME, Codigo CODIGO, IDCodigo ANTERIOR'
      'FROM pq'
      'WHERE Nome=:Nome')
    Left = 364
    Top = 159
    ParamData = <
      item
        DataType = ftString
        Name = 'Nome'
        ParamType = ptUnknown
      end>
    object QrDuplicStrXNOME: TStringField
      FieldName = 'NOME'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
    object QrDuplicStrXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrDuplicStrXANTERIOR: TIntegerField
      FieldName = 'ANTERIOR'
      Origin = 'DBMBWET.pq.IDCodigo'
    end
  end
  object QrInsLogX: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'INSERT INTO logs SET'
      'Data=NOW(),'
      'Tipo=:P0,'
      'Usuario=:P1,'
      'ID=:P2')
    Left = 392
    Top = 203
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDelLogX: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'DELETE FROM logs'
      'WHERE Tipo=:P0'
      'AND Usuario=:P1'
      'AND ID=:P2')
    Left = 364
    Top = 203
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDataBalY: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SELECT MAX(Data) Data FROM pesagem'
      'WHERE Tipo=4')
    Left = 272
    Top = 7
    object QrDataBalYData: TDateField
      FieldName = 'Data'
      Origin = 'DBMBWET.pesagem.Data'
    end
  end
  object QrSenha: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM senhas'
      'WHERE Login=:P0'
      'AND Senha=:P1')
    Left = 8
    Top = 239
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhaLogin: TStringField
      FieldName = 'Login'
      Origin = 'DNTEACH.senhas.Login'
      Size = 128
    end
    object QrSenhaNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'DNTEACH.senhas.Numero'
    end
    object QrSenhaSenha: TStringField
      FieldName = 'Senha'
      Origin = 'DNTEACH.senhas.Senha'
      Size = 128
    end
    object QrSenhaPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'DNTEACH.senhas.Perfil'
    end
    object QrSenhaLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DNTEACH.senhas.Lk'
    end
  end
  object QrMaster2: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM master')
    Left = 8
    Top = 195
    object QrMaster2Limite: TSmallintField
      FieldName = 'Limite'
    end
    object QrMaster2Em: TStringField
      FieldName = 'Em'
      Required = True
      Size = 100
    end
    object QrMaster2CNPJ: TStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 30
    end
    object QrMaster2Monitorar: TSmallintField
      FieldName = 'Monitorar'
      Required = True
    end
    object QrMaster2Licenca: TStringField
      FieldName = 'Licenca'
      Size = 50
    end
    object QrMaster2Distorcao: TIntegerField
      FieldName = 'Distorcao'
      Required = True
    end
    object QrMaster2DataI: TDateField
      FieldName = 'DataI'
    end
    object QrMaster2DataF: TDateField
      FieldName = 'DataF'
    end
    object QrMaster2Hoje: TDateField
      FieldName = 'Hoje'
    end
    object QrMaster2Hora: TTimeField
      FieldName = 'Hora'
    end
    object QrMaster2MasLogin: TStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrMaster2MasSenha: TStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrMaster2MasAtivar: TStringField
      FieldName = 'MasAtivar'
      Size = 1
    end
  end
  object QrSomaM: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT (SUM(Credito) - SUM(Debito)) Valor'
      'FROM lanctos')
    Left = 448
    Top = 91
    object QrSomaMValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
  end
  object QrProduto: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo, Controla'
      'FROM produtos'
      'WHERE Codigo=:P0')
    Left = 120
    Top = 287
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdutoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutoControla: TStringField
      FieldName = 'Controla'
      Size = 1
    end
  end
  object QrVendas: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Custo) Valor'
      'FROM produtossits'
      'WHERE Produto=:P0')
    Left = 92
    Top = 287
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVendasPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'DNSTORE001.produtossits.Pecas'
    end
    object QrVendasValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNSTORE001.produtossits.Total'
    end
  end
  object QrEntrada: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Custo) Valor'
      'FROM produtoscits'
      'WHERE Produto=:P0'
      '')
    Left = 64
    Top = 287
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntradaPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'DNSTORE001.produtoscits.Pecas'
    end
    object QrEntradaValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNSTORE001.produtoscits.Total'
    end
  end
  object QrUpdM: TMySQLQuery
    Database = MyDB
    Left = 448
    Top = 47
  end
  object QrUpdU: TMySQLQuery
    Database = MyDB
    Left = 12
    Top = 47
  end
  object QrUpdL: TMySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 95
  end
  object QrUpdY: TMySQLQuery
    Database = MyDB
    Left = 64
    Top = 239
  end
  object QrLivreY: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo FROM livres')
    Left = 60
    Top = 191
    object QrLivreYCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrCountY: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  Entidades')
    Left = 464
    Top = 247
    object QrCountYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrLocY: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SELECT MIN(Codigo) Record FROM carteiras'
      '')
    Left = 464
    Top = 203
    object QrLocYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrLocDataY: TMySQLQuery
    Database = ChDB
    SQL.Strings = (
      'SELECT MIN(Data) Record FROM lanctos'
      '')
    Left = 320
    Top = 7
    object QrLocDataYRecord: TDateField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrIdx: TMySQLQuery
    Database = MyDB
    Left = 120
    Top = 320
  end
  object QrMas: TMySQLQuery
    Database = MyDB
    Left = 148
    Top = 320
  end
  object QrUpd: TMySQLQuery
    Database = MyDB
    Left = 176
    Top = 320
  end
  object QrAux: TMySQLQuery
    Database = MyDB
    Left = 176
    Top = 364
  end
  object QrSQL: TMySQLQuery
    Database = MyDB
    Left = 120
    Top = 364
  end
  object MyDB: TMySQLDatabase
    DatabaseName = 'toolrent'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=toolrent'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 12
    Top = 3
  end
  object MyLocDatabase: TMySQLDatabase
    DatabaseName = 'LocLeSew'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=LocLeSew'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 156
    Top = 7
  end
  object QrSB4: TMySQLQuery
    Database = MyLocDatabase
    Left = 200
    Top = 143
    object QrSB4Codigo: TStringField
      FieldName = 'Codigo'
      Size = 255
    end
    object QrSB4Nome: TStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsSB4: TDataSource
    DataSet = QrSB4
    Left = 232
    Top = 143
  end
  object QrPriorNext: TMySQLQuery
    Database = MyDB
    Left = 148
    Top = 364
  end
  object QrAuxL: TMySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 51
  end
  object QrSoma1: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(TotalC*Tipo) TOTAL, '
      'SUM(Qtde*Tipo) Qtde '
      'FROM mov'
      'WHERE Produto=:P0')
    Left = 260
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSoma1TOTAL: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'TOTAL'
    end
    object QrSoma1Qtde: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Qtde'
    end
  end
  object QrControle: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM controle')
    Left = 12
    Top = 287
    object QrControleLastBco: TIntegerField
      FieldName = 'LastBco'
    end
    object QrControleSoMaiusculas: TStringField
      FieldName = 'SoMaiusculas'
      Size = 1
    end
    object QrControleMoeda: TStringField
      FieldName = 'Moeda'
      Size = 4
    end
    object QrControleUFPadrao: TIntegerField
      FieldName = 'UFPadrao'
    end
    object QrControleCidade: TStringField
      FieldName = 'Cidade'
      Size = 100
    end
    object QrControleCartVen: TIntegerField
      FieldName = 'CartVen'
    end
    object QrControleCartCom: TIntegerField
      FieldName = 'CartCom'
    end
    object QrControleCartDeS: TIntegerField
      FieldName = 'CartDeS'
    end
    object QrControleCartReS: TIntegerField
      FieldName = 'CartReS'
    end
    object QrControleCartDeG: TIntegerField
      FieldName = 'CartDeG'
    end
    object QrControleCartReG: TIntegerField
      FieldName = 'CartReG'
    end
    object QrControleCartCoE: TIntegerField
      FieldName = 'CartCoE'
    end
    object QrControleCartCoC: TIntegerField
      FieldName = 'CartCoC'
    end
    object QrControleCartEmD: TIntegerField
      FieldName = 'CartEmD'
    end
    object QrControleCartEmA: TIntegerField
      FieldName = 'CartEmA'
    end
    object QrControleContraSenha: TStringField
      FieldName = 'ContraSenha'
      Size = 50
    end
    object QrControlePaperTop: TIntegerField
      FieldName = 'PaperTop'
    end
    object QrControlePaperLef: TIntegerField
      FieldName = 'PaperLef'
    end
    object QrControlePaperWid: TIntegerField
      FieldName = 'PaperWid'
    end
    object QrControlePaperHei: TIntegerField
      FieldName = 'PaperHei'
    end
    object QrControlePaperFcl: TIntegerField
      FieldName = 'PaperFcl'
    end
    object QrControleTravaCidade: TSmallintField
      FieldName = 'TravaCidade'
    end
    object QrControleMoraDD: TFloatField
      FieldName = 'MoraDD'
    end
    object QrControleMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrControleMensalSempre: TIntegerField
      FieldName = 'MensalSempre'
    end
    object QrControleEntraSemValor: TIntegerField
      FieldName = 'EntraSemValor'
    end
    object QrControleMyPagTip: TIntegerField
      FieldName = 'MyPagTip'
    end
    object QrControleMyPagCar: TIntegerField
      FieldName = 'MyPagCar'
    end
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
    end
    object QrControleCorRecibo: TIntegerField
      FieldName = 'CorRecibo'
    end
    object QrControleIdleMinutos: TIntegerField
      FieldName = 'IdleMinutos'
    end
    object QrControleMyPgParc: TSmallintField
      FieldName = 'MyPgParc'
    end
    object QrControleMyPgQtdP: TSmallintField
      FieldName = 'MyPgQtdP'
    end
    object QrControleMyPgPeri: TSmallintField
      FieldName = 'MyPgPeri'
    end
    object QrControleMyPgDias: TSmallintField
      FieldName = 'MyPgDias'
    end
    object QrControleMeuLogoPath: TStringField
      FieldName = 'MeuLogoPath'
      Size = 255
    end
    object QrControleLogoNF: TStringField
      FieldName = 'LogoNF'
      Size = 255
    end
    object QrControleVendaDiasPg: TIntegerField
      FieldName = 'VendaDiasPg'
    end
    object QrControleVendaPeriPg: TIntegerField
      FieldName = 'VendaPeriPg'
    end
    object QrControleVendaParcPg: TIntegerField
      FieldName = 'VendaParcPg'
    end
    object QrControleVendaCartPg: TIntegerField
      FieldName = 'VendaCartPg'
    end
    object QrControleCNABCtaTar: TIntegerField
      FieldName = 'CNABCtaTar'
    end
    object QrControleCNABCtaMul: TIntegerField
      FieldName = 'CNABCtaMul'
    end
    object QrControleCNABCtaJur: TIntegerField
      FieldName = 'CNABCtaJur'
    end
    object QrControleVerBcoTabs: TIntegerField
      FieldName = 'VerBcoTabs'
    end
    object QrControleLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrControleDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrControleDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrControleUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrControleUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrControleCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrControleCNAB_Rem: TIntegerField
      FieldName = 'CNAB_Rem'
    end
    object QrControleCNAB_Rem_I: TIntegerField
      FieldName = 'CNAB_Rem_I'
    end
    object QrControlePreEmMsgIm: TIntegerField
      FieldName = 'PreEmMsgIm'
    end
    object QrControlePreEmMsg: TIntegerField
      FieldName = 'PreEmMsg'
    end
    object QrControlePreEmail: TIntegerField
      FieldName = 'PreEmail'
    end
    object QrControleContasMes: TIntegerField
      FieldName = 'ContasMes'
    end
    object QrControleMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrControleImpObs: TIntegerField
      FieldName = 'ImpObs'
    end
    object QrControleProLaINSSr: TFloatField
      FieldName = 'ProLaINSSr'
    end
    object QrControleProLaINSSp: TFloatField
      FieldName = 'ProLaINSSp'
    end
    object QrControleFPPontoHor: TIntegerField
      FieldName = 'FPPontoHor'
    end
    object QrControleFPHrExt: TIntegerField
      FieldName = 'FPHrExt'
    end
    object QrControleVerSalTabs: TIntegerField
      FieldName = 'VerSalTabs'
    end
    object QrControleCNAB_CaD: TIntegerField
      FieldName = 'CNAB_CaD'
    end
    object QrControleCNAB_CaG: TIntegerField
      FieldName = 'CNAB_CaG'
    end
    object QrControleFPFunciFer: TIntegerField
      FieldName = 'FPFunciFer'
    end
    object QrControleFPFunciHor: TIntegerField
      FieldName = 'FPFunciHor'
    end
    object QrControleAtzCritic: TIntegerField
      FieldName = 'AtzCritic'
    end
    object QrControleFPFunciEve: TIntegerField
      FieldName = 'FPFunciEve'
    end
    object QrControleFPRacas: TIntegerField
      FieldName = 'FPRacas'
    end
    object QrControleFPCivil: TIntegerField
      FieldName = 'FPCivil'
    end
    object QrControleFPNacio: TIntegerField
      FieldName = 'FPNacio'
    end
    object QrControleFPFolhaCal: TIntegerField
      FieldName = 'FPFolhaCal'
    end
    object QrControleFPCateg: TIntegerField
      FieldName = 'FPCateg'
    end
    object QrControleFPEvent: TIntegerField
      FieldName = 'FPEvent'
    end
    object QrControleFPVaria: TIntegerField
      FieldName = 'FPVaria'
    end
    object QrControleFPVincu: TIntegerField
      FieldName = 'FPVincu'
    end
    object QrControleContasTrf: TIntegerField
      FieldName = 'ContasTrf'
    end
    object QrControleSomaIts: TIntegerField
      FieldName = 'SomaIts'
    end
    object QrControleContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object QrControleConfJanela: TIntegerField
      FieldName = 'ConfJanela'
    end
    object QrControleDataPesqAuto: TSmallintField
      FieldName = 'DataPesqAuto'
    end
    object QrControleAtividad: TSmallintField
      FieldName = 'Atividad'
    end
    object QrControleCidades: TSmallintField
      FieldName = 'Cidades'
    end
    object QrControleChequez: TSmallintField
      FieldName = 'Chequez'
    end
    object QrControlePaises: TSmallintField
      FieldName = 'Paises'
    end
    object QrControleCambiosData: TDateField
      FieldName = 'CambiosData'
    end
    object QrControleCambiosUsuario: TIntegerField
      FieldName = 'CambiosUsuario'
    end
    object QrControlereg10: TSmallintField
      FieldName = 'reg10'
    end
    object QrControlereg11: TSmallintField
      FieldName = 'reg11'
    end
    object QrControlereg50: TSmallintField
      FieldName = 'reg50'
    end
    object QrControlereg51: TSmallintField
      FieldName = 'reg51'
    end
    object QrControlereg53: TSmallintField
      FieldName = 'reg53'
    end
    object QrControlereg54: TSmallintField
      FieldName = 'reg54'
    end
    object QrControlereg56: TSmallintField
      FieldName = 'reg56'
    end
    object QrControlereg60: TSmallintField
      FieldName = 'reg60'
    end
    object QrControlereg75: TSmallintField
      FieldName = 'reg75'
    end
    object QrControlereg88: TSmallintField
      FieldName = 'reg88'
    end
    object QrControlereg90: TSmallintField
      FieldName = 'reg90'
    end
    object QrControleNumSerieNF: TSmallintField
      FieldName = 'NumSerieNF'
    end
    object QrControleSerieNF: TSmallintField
      FieldName = 'SerieNF'
    end
    object QrControleModeloNF: TSmallintField
      FieldName = 'ModeloNF'
    end
    object QrControleControlaNeg: TSmallintField
      FieldName = 'ControlaNeg'
    end
    object QrControleFamilias: TSmallintField
      FieldName = 'Familias'
    end
    object QrControleFamiliasIts: TSmallintField
      FieldName = 'FamiliasIts'
    end
    object QrControleAskNFOrca: TSmallintField
      FieldName = 'AskNFOrca'
    end
    object QrControlePreviewNF: TSmallintField
      FieldName = 'PreviewNF'
    end
    object QrControleOrcaRapido: TSmallintField
      FieldName = 'OrcaRapido'
    end
    object QrControleDistriDescoItens: TSmallintField
      FieldName = 'DistriDescoItens'
    end
    object QrControleBalType: TSmallintField
      FieldName = 'BalType'
    end
    object QrControleOrcaOrdem: TSmallintField
      FieldName = 'OrcaOrdem'
    end
    object QrControleOrcaLinhas: TSmallintField
      FieldName = 'OrcaLinhas'
    end
    object QrControleOrcaLFeed: TIntegerField
      FieldName = 'OrcaLFeed'
    end
    object QrControleOrcaModelo: TSmallintField
      FieldName = 'OrcaModelo'
    end
    object QrControleOrcaRodaPos: TSmallintField
      FieldName = 'OrcaRodaPos'
    end
    object QrControleOrcaRodape: TSmallintField
      FieldName = 'OrcaRodape'
    end
    object QrControleOrcaCabecalho: TSmallintField
      FieldName = 'OrcaCabecalho'
    end
    object QrControleCoresRel: TSmallintField
      FieldName = 'CoresRel'
    end
    object QrControleCFiscalPadr: TStringField
      FieldName = 'CFiscalPadr'
    end
    object QrControleSitTribPadr: TStringField
      FieldName = 'SitTribPadr'
    end
    object QrControleCFOPPadr: TStringField
      FieldName = 'CFOPPadr'
    end
    object QrControleAvisosCxaEdit: TSmallintField
      FieldName = 'AvisosCxaEdit'
    end
    object QrControleChConfCab: TIntegerField
      FieldName = 'ChConfCab'
    end
    object QrControleImpDOS: TIntegerField
      FieldName = 'ImpDOS'
    end
    object QrControleUnidadePadrao: TIntegerField
      FieldName = 'UnidadePadrao'
    end
    object QrControleProdutosV: TIntegerField
      FieldName = 'ProdutosV'
    end
    object QrControleCartDespesas: TIntegerField
      FieldName = 'CartDespesas'
    end
    object QrControleReserva: TSmallintField
      FieldName = 'Reserva'
    end
    object QrControleCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrControleVersao: TIntegerField
      FieldName = 'Versao'
    end
    object QrControleVerWeb: TIntegerField
      FieldName = 'VerWeb'
    end
    object QrControleErroHora: TIntegerField
      FieldName = 'ErroHora'
    end
    object QrControleSenhas: TIntegerField
      FieldName = 'Senhas'
    end
    object QrControleSalarios: TIntegerField
      FieldName = 'Salarios'
    end
    object QrControleEntidades: TIntegerField
      FieldName = 'Entidades'
    end
    object QrControleUFs: TIntegerField
      FieldName = 'UFs'
    end
    object QrControleListaECivil: TIntegerField
      FieldName = 'ListaECivil'
    end
    object QrControlePerfis: TIntegerField
      FieldName = 'Perfis'
    end
    object QrControleUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrControleContas: TIntegerField
      FieldName = 'Contas'
    end
    object QrControleCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrControleDepartamentos: TIntegerField
      FieldName = 'Departamentos'
    end
    object QrControleDividas: TIntegerField
      FieldName = 'Dividas'
    end
    object QrControleDividasIts: TIntegerField
      FieldName = 'DividasIts'
    end
    object QrControleDividasPgs: TIntegerField
      FieldName = 'DividasPgs'
    end
    object QrControleCarteiras: TIntegerField
      FieldName = 'Carteiras'
    end
    object QrControleCartaG: TIntegerField
      FieldName = 'CartaG'
    end
    object QrControleCartas: TIntegerField
      FieldName = 'Cartas'
    end
    object QrControleConsignacao: TIntegerField
      FieldName = 'Consignacao'
    end
    object QrControleGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrControleSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
    end
    object QrControleConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrControlePlano: TIntegerField
      FieldName = 'Plano'
    end
    object QrControleInflacao: TIntegerField
      FieldName = 'Inflacao'
    end
    object QrControlekm: TIntegerField
      FieldName = 'km'
    end
    object QrControlekmMedia: TIntegerField
      FieldName = 'kmMedia'
    end
    object QrControlekmIts: TIntegerField
      FieldName = 'kmIts'
    end
    object QrControleFatura: TIntegerField
      FieldName = 'Fatura'
    end
    object QrControleLanctos: TLargeintField
      FieldName = 'Lanctos'
    end
    object QrControleEntiGrupos: TIntegerField
      FieldName = 'EntiGrupos'
    end
    object QrControleAparencias: TIntegerField
      FieldName = 'Aparencias'
    end
    object QrControlePages: TIntegerField
      FieldName = 'Pages'
    end
    object QrControleMultiEtq: TIntegerField
      FieldName = 'MultiEtq'
    end
    object QrControleEntiTransp: TIntegerField
      FieldName = 'EntiTransp'
    end
    object QrControleExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrControleExcelGruImp: TIntegerField
      FieldName = 'ExcelGruImp'
    end
    object QrControleMediaCH: TIntegerField
      FieldName = 'MediaCH'
    end
    object QrControleImprime: TIntegerField
      FieldName = 'Imprime'
    end
    object QrControleImprimeBand: TIntegerField
      FieldName = 'ImprimeBand'
    end
    object QrControleImprimeView: TIntegerField
      FieldName = 'ImprimeView'
    end
    object QrControleComProdPerc: TIntegerField
      FieldName = 'ComProdPerc'
    end
    object QrControleComProdEdit: TIntegerField
      FieldName = 'ComProdEdit'
    end
    object QrControleComServPerc: TIntegerField
      FieldName = 'ComServPerc'
    end
    object QrControleComServEdit: TIntegerField
      FieldName = 'ComServEdit'
    end
    object QrControlePadrPlacaCar: TStringField
      FieldName = 'PadrPlacaCar'
      Size = 100
    end
    object QrControleServSMTP: TStringField
      FieldName = 'ServSMTP'
      Size = 50
    end
    object QrControleNomeMailOC: TStringField
      FieldName = 'NomeMailOC'
      Size = 50
    end
    object QrControleDonoMailOC: TStringField
      FieldName = 'DonoMailOC'
      Size = 50
    end
    object QrControleMailOC: TStringField
      FieldName = 'MailOC'
      Size = 80
    end
    object QrControleCorpoMailOC: TMemoField
      FieldName = 'CorpoMailOC'
      BlobType = ftMemo
      Size = 4
    end
    object QrControleConexaoDialUp: TStringField
      FieldName = 'ConexaoDialUp'
      Size = 50
    end
    object QrControleMailCCCega: TStringField
      FieldName = 'MailCCCega'
      Size = 80
    end
    object QrControleMoedaVal: TFloatField
      FieldName = 'MoedaVal'
    end
    object QrControleTela1: TIntegerField
      FieldName = 'Tela1'
    end
    object QrControleChamarPgtoServ: TIntegerField
      FieldName = 'ChamarPgtoServ'
    end
    object QrControleFormUsaTam: TIntegerField
      FieldName = 'FormUsaTam'
    end
    object QrControleFormHeight: TIntegerField
      FieldName = 'FormHeight'
    end
    object QrControleFormWidth: TIntegerField
      FieldName = 'FormWidth'
    end
    object QrControleFormPixEsq: TIntegerField
      FieldName = 'FormPixEsq'
    end
    object QrControleFormPixDir: TIntegerField
      FieldName = 'FormPixDir'
    end
    object QrControleFormPixTop: TIntegerField
      FieldName = 'FormPixTop'
    end
    object QrControleFormPixBot: TIntegerField
      FieldName = 'FormPixBot'
    end
    object QrControleFormFoAlt: TIntegerField
      FieldName = 'FormFoAlt'
    end
    object QrControleFormFoPro: TFloatField
      FieldName = 'FormFoPro'
    end
    object QrControleFormUsaPro: TIntegerField
      FieldName = 'FormUsaPro'
    end
    object QrControleFormSlides: TIntegerField
      FieldName = 'FormSlides'
    end
    object QrControleFormNeg: TIntegerField
      FieldName = 'FormNeg'
    end
    object QrControleFormIta: TIntegerField
      FieldName = 'FormIta'
    end
    object QrControleFormSub: TIntegerField
      FieldName = 'FormSub'
    end
    object QrControleFormExt: TIntegerField
      FieldName = 'FormExt'
    end
    object QrControleFormFundoTipo: TIntegerField
      FieldName = 'FormFundoTipo'
    end
    object QrControleFormFundoBMP: TStringField
      FieldName = 'FormFundoBMP'
      Size = 255
    end
    object QrControleServInterv: TIntegerField
      FieldName = 'ServInterv'
    end
    object QrControleServAntecip: TIntegerField
      FieldName = 'ServAntecip'
    end
    object QrControleAdiLancto: TIntegerField
      FieldName = 'AdiLancto'
    end
    object QrControleContaSal: TIntegerField
      FieldName = 'ContaSal'
    end
    object QrControleContaVal: TIntegerField
      FieldName = 'ContaVal'
    end
    object QrControlePronomeE: TStringField
      FieldName = 'PronomeE'
    end
    object QrControlePronomeM: TStringField
      FieldName = 'PronomeM'
    end
    object QrControlePronomeF: TStringField
      FieldName = 'PronomeF'
    end
    object QrControlePronomeA: TStringField
      FieldName = 'PronomeA'
    end
    object QrControleSaudacaoE: TStringField
      FieldName = 'SaudacaoE'
      Size = 50
    end
    object QrControleSaudacaoM: TStringField
      FieldName = 'SaudacaoM'
      Size = 50
    end
    object QrControleSaudacaoF: TStringField
      FieldName = 'SaudacaoF'
      Size = 50
    end
    object QrControleSaudacaoA: TStringField
      FieldName = 'SaudacaoA'
      Size = 50
    end
    object QrControleNiver: TSmallintField
      FieldName = 'Niver'
    end
    object QrControleNiverddA: TSmallintField
      FieldName = 'NiverddA'
    end
    object QrControleNiverddD: TSmallintField
      FieldName = 'NiverddD'
    end
    object QrControleLastPassD: TDateTimeField
      FieldName = 'LastPassD'
    end
    object QrControleMultiPass: TIntegerField
      FieldName = 'MultiPass'
    end
    object QrControleCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrControleAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrControleAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrControleCores: TIntegerField
      FieldName = 'Cores'
    end
    object QrControleGradeG: TIntegerField
      FieldName = 'GradeG'
    end
    object QrControleGrades: TIntegerField
      FieldName = 'Grades'
    end
    object QrControleGradesCors: TIntegerField
      FieldName = 'GradesCors'
    end
    object QrControleGradesTams: TIntegerField
      FieldName = 'GradesTams'
    end
    object QrControleTamanhos: TIntegerField
      FieldName = 'Tamanhos'
    end
    object QrControleProdutos: TIntegerField
      FieldName = 'Produtos'
    end
    object QrControleMoviC: TIntegerField
      FieldName = 'MoviC'
    end
    object QrControleMovix: TIntegerField
      FieldName = 'Movix'
    end
    object QrControleMyPerJuros: TFloatField
      FieldName = 'MyPerJuros'
    end
    object QrControleMyPerMulta: TFloatField
      FieldName = 'MyPerMulta'
    end
    object QrControleLogoBig1: TStringField
      FieldName = 'LogoBig1'
      Size = 255
    end
  end
  object QrEstoque: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pm.Tipo, SUM(pmi.Qtde) Qtde,'
      'SUM(pmi.ValorCus) ValorCus'
      'FROM produtosm pm, ProdutosMIts pmi'
      'WHERE pm.Codigo=pmi.Codigo'
      'AND pmi.Produto=:P0'
      'AND pm.DataM>=:P1'
      'GROUP BY pm.Tipo')
    Left = 428
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEstoqueTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstoqueValorCus: TFloatField
      FieldName = 'ValorCus'
    end
  end
  object QrPeriodoBal: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM balancos')
    Left = 344
    Top = 324
    object QrPeriodoBalPeriodo: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Periodo'
    end
  end
  object QrMin: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Min(Periodo) Periodo'
      'FROM balancos')
    Left = 372
    Top = 325
    object QrMinPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'DBMBWET.balancos.Periodo'
    end
  end
  object QrBalancosIts: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(EstqQ) EstqQ, SUM(EstqV) EstqV,'
      'SUM(EstqQ_G) EstqQ_G, SUM(EstqV_G) EstqV_G'
      'FROM balancosits'
      'WHERE Produto=:P0'
      'AND Periodo=:P1'
      '')
    Left = 400
    Top = 324
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBalancosItsEstqQ: TFloatField
      FieldName = 'EstqQ'
      Origin = 'DBMBWET.balancosits.EstqQ'
    end
    object QrBalancosItsEstqV: TFloatField
      FieldName = 'EstqV'
      Origin = 'DBMBWET.balancosits.EstqV'
    end
    object QrBalancosItsEstqQ_G: TFloatField
      FieldName = 'EstqQ_G'
    end
    object QrBalancosItsEstqV_G: TFloatField
      FieldName = 'EstqV_G'
    end
  end
  object QrEstqperiodo: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pm.Tipo, SUM(pmi.Qtde) Qtde,'
      'SUM(pmi.ValorCus) ValorCus'
      'FROM produtosm pm, ProdutosMIts pmi'
      'WHERE pm.Codigo=pmi.Codigo'
      'AND pmi.Produto=:P0'
      'AND pm.DataM BETWEEN :P1 AND :P2'
      'GROUP BY pm.Tipo')
    Left = 456
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEstqperiodoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstqperiodoQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstqperiodoValorCus: TFloatField
      FieldName = 'ValorCus'
    end
  end
  object QrTerminal: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM terminais'
      'WHERE IP=:P0')
    Left = 292
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrNTV: TMySQLQuery
    Database = MyLocDatabase
    Left = 60
    Top = 4
  end
  object QrNTI: TMySQLQuery
    Database = MyLocDatabase
    Left = 60
    Top = 48
  end
  object ChDB: TMySQLDatabase
    DatabaseName = 'LeSew'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=LeSew'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 96
    Top = 3
  end
  object QrPerfis: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM perfisits pit'
      
        'LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0')
    Left = 300
    Top = 292
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfisLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisJanela: TStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrTerceiro: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT ufp.Nome NOMEpUF, ufe.Nome NOMEeUF, en.* '
      'FROM entidades en'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'WHERE en.Codigo=:P0')
    Left = 196
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerceiroNOMEpUF: TStringField
      FieldName = 'NOMEpUF'
      Required = True
      Size = 2
    end
    object QrTerceiroNOMEeUF: TStringField
      FieldName = 'NOMEeUF'
      Required = True
      Size = 2
    end
    object QrTerceiroCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTerceiroRazaoSocial: TStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrTerceiroFantasia: TStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons1: TStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons2: TStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrTerceiroPai: TStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrTerceiroMae: TStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrTerceiroCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTerceiroIE: TStringField
      FieldName = 'IE'
    end
    object QrTerceiroIEST: TStringField
      FieldName = 'IEST'
    end
    object QrTerceiroNome: TStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrTerceiroApelido: TStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrTerceiroCPF: TStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTerceiroRG: TStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrTerceiroELograd: TSmallintField
      FieldName = 'ELograd'
      Required = True
    end
    object QrTerceiroERua: TStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrTerceiroENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrTerceiroECompl: TStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrTerceiroEBairro: TStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrTerceiroECidade: TStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrTerceiroEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrTerceiroECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrTerceiroEPais: TStringField
      FieldName = 'EPais'
    end
    object QrTerceiroETe1: TStringField
      FieldName = 'ETe1'
    end
    object QrTerceiroEte2: TStringField
      FieldName = 'Ete2'
    end
    object QrTerceiroEte3: TStringField
      FieldName = 'Ete3'
    end
    object QrTerceiroECel: TStringField
      FieldName = 'ECel'
    end
    object QrTerceiroEFax: TStringField
      FieldName = 'EFax'
    end
    object QrTerceiroEEmail: TStringField
      FieldName = 'EEmail'
      Size = 100
    end
    object QrTerceiroEContato: TStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrTerceiroENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrTerceiroPLograd: TSmallintField
      FieldName = 'PLograd'
      Required = True
    end
    object QrTerceiroPRua: TStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrTerceiroPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrTerceiroPCompl: TStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrTerceiroPBairro: TStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrTerceiroPCidade: TStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrTerceiroPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrTerceiroPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrTerceiroPPais: TStringField
      FieldName = 'PPais'
    end
    object QrTerceiroPTe1: TStringField
      FieldName = 'PTe1'
    end
    object QrTerceiroPte2: TStringField
      FieldName = 'Pte2'
    end
    object QrTerceiroPte3: TStringField
      FieldName = 'Pte3'
    end
    object QrTerceiroPCel: TStringField
      FieldName = 'PCel'
    end
    object QrTerceiroPFax: TStringField
      FieldName = 'PFax'
    end
    object QrTerceiroPEmail: TStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrTerceiroPContato: TStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrTerceiroPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrTerceiroSexo: TStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrTerceiroResponsavel: TStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrTerceiroProfissao: TStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrTerceiroCargo: TStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrTerceiroRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrTerceiroDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrTerceiroAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrTerceiroAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrTerceiroCliente1: TStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrTerceiroCliente2: TStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrTerceiroFornece1: TStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrTerceiroFornece2: TStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrTerceiroFornece3: TStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrTerceiroFornece4: TStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrTerceiroTerceiro: TStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrTerceiroCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrTerceiroInformacoes: TStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrTerceiroLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrTerceiroVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrTerceiroMensal: TStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrTerceiroObservacoes: TMemoField
      FieldName = 'Observacoes'
      BlobType = ftMemo
      Size = 4
    end
    object QrTerceiroTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTerceiroCLograd: TSmallintField
      FieldName = 'CLograd'
      Required = True
    end
    object QrTerceiroCRua: TStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrTerceiroCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrTerceiroCCompl: TStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrTerceiroCBairro: TStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrTerceiroCCidade: TStringField
      FieldName = 'CCidade'
      Size = 25
    end
    object QrTerceiroCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrTerceiroCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrTerceiroCPais: TStringField
      FieldName = 'CPais'
    end
    object QrTerceiroCTel: TStringField
      FieldName = 'CTel'
    end
    object QrTerceiroCCel: TStringField
      FieldName = 'CCel'
    end
    object QrTerceiroCFax: TStringField
      FieldName = 'CFax'
    end
    object QrTerceiroCContato: TStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrTerceiroLLograd: TSmallintField
      FieldName = 'LLograd'
      Required = True
    end
    object QrTerceiroLRua: TStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrTerceiroLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrTerceiroLCompl: TStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrTerceiroLBairro: TStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrTerceiroLCidade: TStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrTerceiroLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrTerceiroLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrTerceiroLPais: TStringField
      FieldName = 'LPais'
    end
    object QrTerceiroLTel: TStringField
      FieldName = 'LTel'
    end
    object QrTerceiroLCel: TStringField
      FieldName = 'LCel'
    end
    object QrTerceiroLFax: TStringField
      FieldName = 'LFax'
    end
    object QrTerceiroLContato: TStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrTerceiroComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrTerceiroSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrTerceiroNivel: TStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrTerceiroGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrTerceiroAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrTerceiroLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrTerceiroConjugeNome: TStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrTerceiroConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
    end
    object QrTerceiroNome1: TStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrTerceiroNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrTerceiroNome2: TStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrTerceiroNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrTerceiroNome3: TStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrTerceiroNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrTerceiroNome4: TStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrTerceiroNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrTerceiroCreditosI: TIntegerField
      FieldName = 'CreditosI'
    end
    object QrTerceiroCreditosL: TIntegerField
      FieldName = 'CreditosL'
    end
    object QrTerceiroCreditosF2: TFloatField
      FieldName = 'CreditosF2'
    end
    object QrTerceiroCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrTerceiroCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrTerceiroCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrTerceiroMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrTerceiroQuantI1: TIntegerField
      FieldName = 'QuantI1'
    end
    object QrTerceiroQuantI2: TIntegerField
      FieldName = 'QuantI2'
    end
    object QrTerceiroQuantI3: TIntegerField
      FieldName = 'QuantI3'
    end
    object QrTerceiroQuantI4: TIntegerField
      FieldName = 'QuantI4'
    end
    object QrTerceiroQuantN1: TFloatField
      FieldName = 'QuantN1'
    end
    object QrTerceiroQuantN2: TFloatField
      FieldName = 'QuantN2'
    end
    object QrTerceiroAgenda: TStringField
      FieldName = 'Agenda'
      Required = True
      Size = 1
    end
    object QrTerceiroSenhaQuer: TStringField
      FieldName = 'SenhaQuer'
      Required = True
      Size = 1
    end
    object QrTerceiroSenha1: TStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrTerceiroLimiCred: TFloatField
      FieldName = 'LimiCred'
      Required = True
    end
    object QrTerceiroDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrTerceiroCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Required = True
    end
    object QrTerceiroTempD: TFloatField
      FieldName = 'TempD'
    end
    object QrTerceiroLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTerceiroDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTerceiroDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTerceiroUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTerceiroUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTerceiroCPF_Pai: TStringField
      FieldName = 'CPF_Pai'
      Size = 18
    end
    object QrTerceiroSSP: TStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrTerceiroCidadeNatal: TStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrTerceiroUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Required = True
    end
  end
  object QlLocal: TMySQLBatchExecute
    Action = baContinue
    Database = MyLocDatabase
    Delimiter = ';'
    Left = 368
    Top = 4
  end
  object QrAgora: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 60
    Top = 95
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrTerminais: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM terminais')
    Left = 292
    Top = 228
    object QrTerminaisIP: TStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminaisLicenca: TStringField
      FieldName = 'Licenca'
      Size = 50
    end
    object QrTerminaisTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrEndereco: TMySQLQuery
    Database = MyDB
    OnCalcFields = QrEnderecoCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END NUME' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Logr' +
        'ad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 60
    Top = 342
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnderecoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEnderecoCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEnderecoNOMEDONO: TStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrEnderecoCNPJ_CPF: TStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEnderecoIE_RG: TStringField
      FieldName = 'IE_RG'
    end
    object QrEnderecoNIRE_: TStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrEnderecoRUA: TStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEnderecoNUMERO: TLargeintField
      FieldName = 'NUMERO'
    end
    object QrEnderecoCOMPL: TStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEnderecoBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEnderecoCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEnderecoNOMELOGRAD: TStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEnderecoNOMEUF: TStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEnderecoPais: TStringField
      FieldName = 'Pais'
    end
    object QrEnderecoLograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrEnderecoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEnderecoCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrEnderecoTE1: TStringField
      FieldName = 'TE1'
    end
    object QrEnderecoFAX: TStringField
      FieldName = 'FAX'
    end
    object QrEnderecoENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEnderecoPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEnderecoECEP_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrEnderecoNUMERO_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrEnderecoE_ALL: TStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrEnderecoCNPJ_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEnderecoFAX_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEnderecoTE1_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEnderecoNATAL_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEnderecoRespons1: TStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrEnderecoNO_TIPO_DOC: TStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO_DOC'
      Calculated = True
    end
  end
  object QrTransf: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      
        'SELECT Data, Tipo, Carteira, Controle, Genero, Debito, Credito, ' +
        'Documento FROM lanctos'
      'WHERE Controle=:P0'
      '')
    Left = 72
    Top = 144
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTransfData: TDateField
      FieldName = 'Data'
    end
    object QrTransfTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTransfCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrTransfControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTransfGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrTransfDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrTransfCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrTransfDocumento: TFloatField
      FieldName = 'Documento'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 544
    Top = 41
  end
  object QrCarteiras: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT DISTINCT ca.*, '
      'CASE WHEN ba.Codigo=0 THEN "" ELSE ba.Nome END NOMEDOBANCO, '
      
        'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END NOMEFOR' +
        'NECEI'
      'FROM carteiras ca'
      'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco'
      'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI'
      'WHERE ca.Codigo>0')
    Left = 516
    Top = 41
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'carteiras.Codigo'
      DisplayFormat = '000'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'carteiras.Tipo'
    end
    object QrCarteirasNome: TStringField
      FieldName = 'Nome'
      Origin = 'carteiras.Nome'
      Size = 128
    end
    object QrCarteirasInicial: TFloatField
      FieldName = 'Inicial'
      Origin = 'carteiras.Inicial'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'carteiras.Banco'
    end
    object QrCarteirasID: TStringField
      FieldName = 'ID'
      Origin = 'carteiras.ID'
      Size = 128
    end
    object QrCarteirasFatura: TStringField
      FieldName = 'Fatura'
      Origin = 'carteiras.Fatura'
      FixedChar = True
      Size = 128
    end
    object QrCarteirasID_Fat: TStringField
      FieldName = 'ID_Fat'
      Origin = 'carteiras.ID_Fat'
      Size = 128
    end
    object QrCarteirasSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'carteiras.Saldo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'carteiras.Lk'
    end
    object QrCarteirasNOMEDOBANCO: TStringField
      FieldName = 'NOMEDOBANCO'
      Origin = 'carteiras.Nome'
      FixedChar = True
      Size = 100
    end
    object QrCarteirasEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      Origin = 'carteiras.EmCaixa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasDIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIFERENCA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
      Origin = 'carteiras.Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrCarteirasTIPOPRAZO: TStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOPRAZO'
      Size = 1
      Calculated = True
    end
    object QrCarteirasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'carteiras.DataCad'
    end
    object QrCarteirasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'carteiras.DataAlt'
    end
    object QrCarteirasUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'carteiras.UserCad'
    end
    object QrCarteirasUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'carteiras.UserAlt'
    end
    object QrCarteirasNOMEPAGREC: TStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEPAGREC'
      Size = 50
      Calculated = True
    end
    object QrCarteirasPagRec: TSmallintField
      FieldName = 'PagRec'
      Origin = 'carteiras.PagRec'
    end
    object QrCarteirasDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
      Required = True
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
      Required = True
    end
    object QrCarteirasNome2: TStringField
      FieldName = 'Nome2'
      Required = True
      Size = 100
    end
    object QrCarteirasNOMEFORNECEI: TStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
      Required = True
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
      DisplayFormat = '000'
    end
    object QrCarteirasAgencia1: TIntegerField
      FieldName = 'Agencia1'
      DisplayFormat = '0000'
    end
    object QrCarteirasConta1: TStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCarteirasCheque1: TIntegerField
      FieldName = 'Cheque1'
      DisplayFormat = '000000'
    end
    object QrCarteirasContato1: TStringField
      FieldName = 'Contato1'
      Required = True
      Size = 100
    end
  end
  object QrLanctos: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR'
      'FROM lanctos la'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'ORDER BY la.Data, la.Controle')
    Left = 518
    Top = 88
    object QrLanctosData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLanctosTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrLanctosCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrLanctosAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLanctosGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrLanctosDescricao: TStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrLanctosNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLanctosDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLanctosCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLanctosCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLanctosDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLanctosSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrLanctosVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLanctosLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrLanctosFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrLanctosFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'lanctos.FatNum'
    end
    object QrLanctosFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrLanctosCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrLanctosNOMECONTA: TStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrLanctosNOMEEMPRESA: TStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'contas.Nome'
      Size = 128
    end
    object QrLanctosNOMESUBGRUPO: TStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 4
    end
    object QrLanctosNOMEGRUPO: TStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 4
    end
    object QrLanctosNOMECONJUNTO: TStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 4
    end
    object QrLanctosNOMESIT: TStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLanctosAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLanctosMENSAL: TStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLanctosMENSAL2: TStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLanctosBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLanctosLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLanctosFatura: TStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLanctosSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLanctosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLanctosLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLanctosPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLanctosSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLanctosID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLanctosMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLanctosFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLanctoscliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLanctosMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLanctosNOMECLIENTE: TStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLanctosNOMEFORNECEDOR: TStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLanctosTIPOEM: TStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLanctosNOMERELACIONADO: TStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLanctosOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLanctosLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLanctosMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLanctosATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLanctosJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLanctosDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLanctosNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLanctosVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLanctosAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLanctosMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLanctosProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLanctosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLanctosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLanctosUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLanctosUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLanctosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLanctosID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLanctosCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLanctosFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLanctosICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLanctosICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLanctosDuplicata: TStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLanctosCOMPENSADO_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLanctosCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLanctosDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLanctosDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLanctosForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLanctosQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object DsLanctos: TDataSource
    DataSet = QrLanctos
    Left = 546
    Top = 88
  end
  object QrEstqR: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Qtd) Qtd, SUM(Val) Val'
      'FROM movim'
      'WHERE Grade= :P0'
      'AND Cor= :P1'
      'AND Tam= :P2'
      'AND DataReal>= :P3')
    Left = 516
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrEstqRQtd: TFloatField
      FieldName = 'Qtd'
    end
    object QrEstqRVal: TFloatField
      FieldName = 'Val'
    end
  end
  object QrEstqP: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Qtd) Qtd'
      'FROM movim'
      'WHERE Grade= :P0'
      'AND Cor= :P1'
      'AND Tam= :P2'
      'AND DataPedi >= :P3 '
      'AND DataReal < 2'
      '')
    Left = 544
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrEstqPQtd: TFloatField
      FieldName = 'Qtd'
    end
  end
  object QrSel: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'UNLOCK TABLES;'
      'UPDATE controle SET Movix=Movix+1;'
      'LOCK TABLES Controle READ;'
      'SELECT Movix FROM controle;'
      'UNLOCK TABLES;')
    Left = 236
    Top = 292
    object QrSelMovix: TIntegerField
      FieldName = 'Movix'
    end
  end
  object QrUpdW: TMySQLQuery
    Database = MyDB
    Left = 484
    Top = 7
  end
  object frxDsDono: TfrxDBDataset
    UserName = 'frxDsDono'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 12
    Top = 381
  end
  object QrDono: TMySQLQuery
    Database = MyDB
    OnCalcFields = QrDonoCalcFields
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, EContato, E' +
        'Cel,'
      'Respons1, Respons2,  '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O, '
      
        'CASE WHEN en.Tipo=0 THEN en.Fantasia   ELSE en.Apelido END APELI' +
        'DO, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero+0.000     ELSE en.PNumero+0.' +
        '000 END NUMERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Logr' +
        'ad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe2        ELSE en.PTe2    END TE2,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX '
      'FROM controle co'
      'LEFT JOIN entidades en ON en.Codigo=co.Dono'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd')
    Left = 12
    Top = 334
    object QrDonoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDonoCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrDonoNOMEDONO: TStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrDonoCNPJ_CPF: TStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrDonoIE_RG: TStringField
      FieldName = 'IE_RG'
    end
    object QrDonoNIRE_: TStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrDonoRUA: TStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrDonoCOMPL: TStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrDonoBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrDonoCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrDonoNOMELOGRAD: TStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrDonoNOMEUF: TStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrDonoPais: TStringField
      FieldName = 'Pais'
    end
    object QrDonoLograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrDonoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDonoCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrDonoTE1: TStringField
      FieldName = 'TE1'
    end
    object QrDonoTE2: TStringField
      FieldName = 'TE2'
    end
    object QrDonoFAX: TStringField
      FieldName = 'FAX'
    end
    object QrDonoENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrDonoPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrDonoECEP_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrDonoNUMERO_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrDonoCNPJ_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoFAX_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoTE1_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoTE2_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'TE2_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoNATAL_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoE_LNR: TStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrDonoE_CUC: TStringField
      FieldKind = fkCalculated
      FieldName = 'E_CUC'
      Size = 255
      Calculated = True
    end
    object QrDonoEContato: TStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrDonoECel: TStringField
      FieldName = 'ECel'
    end
    object QrDonoCEL_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoE_LN2: TStringField
      FieldKind = fkCalculated
      FieldName = 'E_LN2'
      Size = 255
      Calculated = True
    end
    object QrDonoAPELIDO: TStringField
      FieldName = 'APELIDO'
      Size = 60
    end
    object QrDonoNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrDonoRespons1: TStringField
      FieldName = 'Respons1'
      Size = 60
    end
    object QrDonoRespons2: TStringField
      FieldName = 'Respons2'
      Size = 60
    end
    object QrDonoFONES: TStringField
      FieldKind = fkCalculated
      FieldName = 'FONES'
      Size = 255
      Calculated = True
    end
  end
  object QrBoss: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MasSenha MasZero, '
      'AES_DECRYPT(MasSenha, :P0) MasSenha, MasLogin, Em, CNPJ'
      'FROM master')
    Left = 340
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBossMasSenha: TStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrBossMasLogin: TStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrBossEm: TStringField
      FieldName = 'Em'
      Size = 100
    end
    object QrBossCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 30
    end
    object QrBossMasZero: TStringField
      FieldName = 'MasZero'
      Size = 30
    end
  end
  object QrBSit: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM master')
    Left = 368
    Top = 376
    object QrBSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrSenhas: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT AES_DECRYPT(se.Senha, :P0) SenhaNew, se.Senha SenhaOld, '
      'se.login, se.Numero, se.Perfil, se.Funcionario, se.IP_Default'
      'FROM senhas se'
      'WHERE se.Login=:P1')
    Left = 396
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhasSenhaNew: TStringField
      FieldName = 'SenhaNew'
      Size = 30
    end
    object QrSenhasSenhaOld: TStringField
      FieldName = 'SenhaOld'
      Size = 30
    end
    object QrSenhaslogin: TStringField
      FieldName = 'login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
    end
    object QrSenhasFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrSenhasIP_Default: TStringField
      FieldName = 'IP_Default'
      Size = 50
    end
  end
  object QrSSit: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM senhas'
      'WHERE login=:P0'
      '')
    Left = 424
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object frxDsMaster: TfrxDBDataset
    UserName = 'frxDsMaster'
    CloseDataSource = False
    DataSet = QrMaster
    BCDToCurrency = False
    Left = 44
    Top = 144
  end
end
