object FmIB_BD_Ant: TFmIB_BD_Ant
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Cria'#231#227'o do BD Anterior'
  ClientHeight = 642
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 280
        Height = 32
        Caption = 'Cria'#231#227'o do BD Anterior'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 280
        Height = 32
        Caption = 'Cria'#231#227'o do BD Anterior'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 280
        Height = 32
        Caption = 'Cria'#231#227'o do BD Anterior'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 524
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 524
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 524
        Align = alClient
        TabOrder = 0
        object DBGrid3: TDBGrid
          Left = 2
          Top = 117
          Width = 780
          Height = 215
          Align = alClient
          DataSource = DsFields
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 102
          Align = alTop
          TabOrder = 1
          object Label39: TLabel
            Left = 8
            Top = 4
            Width = 111
            Height = 13
            Caption = 'Arquivo dp BD (*.GDB):'
          end
          object SbIB_DB: TSpeedButton
            Left = 671
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbIB_DBClick
          end
          object Label1: TLabel
            Left = 12
            Top = 44
            Width = 255
            Height = 13
            Caption = 'Nome do BD de importa'#231#227'o simples e direta (espelho):'
            Enabled = False
          end
          object Label2: TLabel
            Left = 276
            Top = 44
            Width = 70
            Height = 13
            Caption = 'IP do Servidor:'
          end
          object Label3: TLabel
            Left = 372
            Top = 44
            Width = 28
            Height = 13
            Caption = 'Porta:'
          end
          object EdIB_DB: TdmkEdit
            Left = 8
            Top = 20
            Width = 660
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'C:\_MLArend\Clientes\DDTudo\BUGSTROL\FIREBIRD\dedetiza.gdb'
            QryCampo = 'DirPedCan'
            UpdCampo = 'DirPedCan'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'C:\_MLArend\Clientes\DDTudo\BUGSTROL\FIREBIRD\dedetiza.gdb'
            ValWarn = False
          end
          object BtIB_DB: TButton
            Left = 696
            Top = 20
            Width = 75
            Height = 21
            Caption = 'Conecta'
            Enabled = False
            TabOrder = 1
            OnClick = BtIB_DBClick
          end
          object EdDBNome: TdmkEdit
            Left = 12
            Top = 60
            Width = 261
            Height = 21
            Enabled = False
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'ZZZ_IB_DB_ANT'
            QryCampo = 'DirPedCan'
            UpdCampo = 'DirPedCan'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'ZZZ_IB_DB_ANT'
            ValWarn = False
          end
          object BtErrado: TButton
            Left = 600
            Top = 56
            Width = 169
            Height = 21
            Caption = 'Teste 1 (n'#227'o deu certo)'
            Enabled = False
            TabOrder = 3
            Visible = False
            OnClick = BtErradoClick
          end
          object EdIPServer: TdmkEdit
            Left = 276
            Top = 60
            Width = 93
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '127.0.0.1'
            QryCampo = 'DirPedCan'
            UpdCampo = 'DirPedCan'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '127.0.0.1'
            ValWarn = False
          end
          object EdPorta: TdmkEdit
            Left = 372
            Top = 60
            Width = 37
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '3306'
            QryCampo = 'DirPedCan'
            UpdCampo = 'DirPedCan'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 3306
            ValWarn = False
          end
        end
        object Memo1: TMemo
          Left = 2
          Top = 433
          Width = 780
          Height = 89
          Align = alBottom
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 332
          Width = 780
          Height = 101
          Align = alBottom
          Caption = ' Avisos: '
          TabOrder = 3
          object Panel13: TPanel
            Left = 2
            Top = 15
            Width = 776
            Height = 84
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 650
              Height = 84
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object LaAvisoG1: TLabel
                Left = 13
                Top = 34
                Width = 120
                Height = 16
                Caption = '..............................'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clSilver
                Font.Height = -13
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object LaAvisoB1: TLabel
                Left = 13
                Top = 18
                Width = 120
                Height = 16
                Caption = '..............................'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clSilver
                Font.Height = -13
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object LaAvisoG2: TLabel
                Left = 12
                Top = 33
                Width = 120
                Height = 16
                Caption = '..............................'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -13
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object LaAvisoB2: TLabel
                Left = 12
                Top = 17
                Width = 120
                Height = 16
                Caption = '..............................'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object LaAvisoR1: TLabel
                Left = 13
                Top = 2
                Width = 120
                Height = 16
                Caption = '..............................'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clSilver
                Font.Height = -13
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object LaAvisoR2: TLabel
                Left = 12
                Top = 1
                Width = 120
                Height = 16
                Caption = '..............................'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -13
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object PB1: TProgressBar
                Left = 0
                Top = 67
                Width = 650
                Height = 17
                Align = alBottom
                TabOrder = 0
              end
            end
            object Panel14: TPanel
              Left = 762
              Top = 0
              Width = 14
              Height = 84
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
            end
            object Panel6: TPanel
              Left = 694
              Top = 0
              Width = 68
              Height = 84
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object LaTempoI: TLabel
                Left = 0
                Top = 0
                Width = 68
                Height = 14
                Align = alTop
                Alignment = taRightJustify
                AutoSize = False
                Caption = '...'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitLeft = 1
                ExplicitTop = 1
                ExplicitWidth = 9
              end
              object LaTempoF: TLabel
                Left = 0
                Top = 14
                Width = 68
                Height = 14
                Align = alTop
                Alignment = taRightJustify
                AutoSize = False
                Caption = '...'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -11
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitLeft = 1
                ExplicitTop = 15
                ExplicitWidth = 9
              end
              object LaTempoT: TLabel
                Left = 0
                Top = 28
                Width = 68
                Height = 14
                Align = alTop
                Alignment = taRightJustify
                AutoSize = False
                Caption = '...'
                Color = clBtnFace
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentColor = False
                ParentFont = False
                Transparent = True
                ExplicitLeft = -19
                ExplicitTop = 63
                ExplicitWidth = 339
              end
            end
            object Panel8: TPanel
              Left = 650
              Top = 0
              Width = 44
              Height = 84
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 3
              object Label4: TLabel
                Left = 0
                Top = 0
                Width = 44
                Height = 14
                Align = alTop
                AutoSize = False
                Caption = 'In'#237'cio:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                ExplicitLeft = 1
                ExplicitTop = 1
                ExplicitWidth = 9
              end
              object Label5: TLabel
                Left = 0
                Top = 14
                Width = 44
                Height = 14
                Align = alTop
                AutoSize = False
                Caption = 'T'#233'rmino:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                ExplicitLeft = 1
                ExplicitTop = 15
                ExplicitWidth = 9
              end
              object Label6: TLabel
                Left = 0
                Top = 28
                Width = 44
                Height = 14
                Align = alTop
                AutoSize = False
                Caption = 'Tempo:'
                Color = clBtnFace
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Arial'
                Font.Style = []
                ParentColor = False
                ParentFont = False
                Transparent = True
                ExplicitLeft = -19
                ExplicitTop = 63
                ExplicitWidth = 339
              end
            end
          end
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 572
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DBFB: TSQLConnection
    ConnectionName = 'IBConnection'
    DriverName = 'Interbase'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Interbase'
      
        'Database=C:/_MLArend/Clientes/DDTudo/BUGSTROL/FIREBIRD/dedetiza.' +
        'gdb'
      'RoleName=RoleName'
      'User_Name=sysdba'
      'Password=masterkey'
      'ServerCharSet='
      'SQLDialect=3'
      'ErrorResourceFile='
      'LocaleCode=0000'
      'BlobSize=-1'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'Interbase TransIsolation=ReadCommited'
      'Trim Char=False')
    Left = 172
    Top = 232
  end
  object SDFields: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 
      'SELECT      RRF.RDB$RELATION_NAME AS TABELA,  '#13#10'            RRF.' +
      'RDB$FIELD_NAME AS CAMPO,  '#13#10'            CASE  '#13#10'              RT' +
      'P.RDB$TYPE_NAME  '#13#10'                WHEN '#39'VARYING'#39'  THEN  '#39'VARCHA' +
      'R'#39'  '#13#10'                WHEN '#39'LONG'#39'     THEN  '#39'INTEGER'#39'  '#13#10'       ' +
      '         WHEN '#39'SHORT'#39'    THEN  '#39'SMALLINT'#39'  '#13#10'                WHE' +
      'N '#39'DOUBLE'#39'   THEN  '#39'DOUBLE'#39'  '#13#10'                WHEN '#39'FLOAT'#39'    T' +
      'HEN  '#39'FLOAT'#39'  '#13#10'                WHEN '#39'INT64'#39'    THEN  '#39'NUMERIC'#39' ' +
      ' '#13#10'                WHEN '#39'TEXT'#39'     THEN  '#39'CHAR'#39'  '#13#10'             ' +
      '   ELSE RTP.RDB$TYPE_NAME  '#13#10'            END TIPO_CAMPO,  '#13#10'    ' +
      '        CASE  '#13#10'              RTP.RDB$TYPE_NAME  '#13#10'             ' +
      '   WHEN  '#39'VARYING'#39' THEN RFL.RDB$FIELD_LENGTH  '#13#10'                ' +
      'ELSE  RFL.RDB$FIELD_PRECISION  '#13#10'            END AS TAMANHO,  '#13#10 +
      '            (RFL.RDB$FIELD_SCALE * -1) AS ESCALA,  '#13#10'           ' +
      ' IIF(  EXISTS(   SELECT      FIRST 1 1  '#13#10'                      ' +
      '      FROM        RDB$RELATION_CONSTRAINTS  RCN  '#13#10'             ' +
      '               INNER JOIN  RDB$INDEX_SEGMENTS        ISG     ON ' +
      '   RCN.RDB$INDEX_NAME = ISG.RDB$INDEX_NAME AND  '#13#10'              ' +
      '                                                                ' +
      '  ISG.RDB$FIELD_NAME = RRF.RDB$FIELD_NAME  '#13#10'                   ' +
      '         WHERE       RCN.RDB$CONSTRAINT_TYPE = '#39'PRIMARY KEY'#39' AND' +
      '  '#13#10'                                        RCN.RDB$RELATION_NAM' +
      'E =  RRF.RDB$RELATION_NAME),  '#13#10'                  '#39'X'#39',  '#13#10'      ' +
      '            '#39'O'#39')  AS  PRIMARY_KEY,  '#13#10'            IIF(  EXISTS( ' +
      '  SELECT      FIRST 1 1  '#13#10'                            FROM     ' +
      '   RDB$RELATION_CONSTRAINTS  RCN  '#13#10'                            ' +
      'INNER JOIN  RDB$CHECK_CONSTRAINTS     CCN     ON    RCN.RDB$CONS' +
      'TRAINT_NAME = CCN.RDB$CONSTRAINT_NAME AND  '#13#10'                   ' +
      '                                                             CCN' +
      '.RDB$TRIGGER_NAME = RRF.RDB$FIELD_NAME  '#13#10'                      ' +
      '       WHERE      RCN.RDB$RELATION_NAME =  RRF.RDB$RELATION_NAME' +
      ' ),  '#13#10'                  '#39'X'#39',  '#13#10'                  '#39'O'#39')  AS  NOT' +
      '_NULL,  '#13#10'            IIF(  RRC.RDB$RELATION_NAME IS NOT NULL,  ' +
      ' '#13#10'                  '#39'X'#39',  '#13#10'                  '#39'O'#39')  AS  FOREIGN' +
      '_KEY,  '#13#10'            RFC.RDB$CONST_NAME_UQ AS  INDICE_CHAVE,  '#13#10 +
      '            RRC.RDB$RELATION_NAME AS  TABELA_CHAVE,  '#13#10'         ' +
      '   RIS2.RDB$FIELD_NAME   AS  CAMPO_CHAVE,  '#13#10'            RFC.RDB' +
      '$UPDATE_RULE   AS  REGRA_UPDATE,  '#13#10'            RFC.RDB$DELETE_R' +
      'ULE   AS  REGRA_DELETE  '#13#10'FROM        RDB$RELATION_FIELDS   RRF ' +
      ' '#13#10'INNER JOIN  RDB$FIELDS            RFL     ON    RFL.RDB$FIELD' +
      '_NAME = RRF.RDB$FIELD_SOURCE  '#13#10'INNER JOIN  RDB$TYPES           ' +
      '  RTP     ON    RTP.RDB$TYPE = RFL.RDB$FIELD_TYPE AND  '#13#10'       ' +
      '                                         RTP.RDB$FIELD_NAME = '#39'R' +
      'DB$FIELD_TYPE'#39'  '#13#10'LEFT JOIN   RDB$INDEX_SEGMENTS    RIS     ON  ' +
      '  RIS.RDB$FIELD_NAME = RRF.RDB$FIELD_NAME AND  '#13#10'               ' +
      '                                 EXISTS (  SELECT      FIRST 1 1' +
      '  '#13#10'                                                          FR' +
      'OM        RDB$INDICES   IND  '#13#10'                                 ' +
      '                         INNER JOIN  RDB$REF_CONSTRAINTS   RFC  ' +
      ' ON    RFC.RDB$CONSTRAINT_NAME = IND.RDB$INDEX_NAME  '#13#10'         ' +
      '                                                 WHERE       IND' +
      '.RDB$INDEX_NAME = RIS.RDB$INDEX_NAME AND  '#13#10'                    ' +
      '                                                  IND.RDB$RELATI' +
      'ON_NAME = RRF.RDB$RELATION_NAME)  '#13#10'LEFT JOIN   RDB$REF_CONSTRAI' +
      'NTS   RFC     ON    RFC.RDB$CONSTRAINT_NAME = RIS.RDB$INDEX_NAME' +
      '  '#13#10'LEFT JOIN   RDB$INDEX_SEGMENTS    RIS2    ON    RIS2.RDB$IND' +
      'EX_NAME = RFC.RDB$CONST_NAME_UQ AND  '#13#10'                         ' +
      '                       RIS2.RDB$FIELD_POSITION = RIS.RDB$FIELD_P' +
      'OSITION  '#13#10'LEFT  JOIN  RDB$RELATION_CONSTRAINTS RRC  ON    RFC.R' +
      'DB$CONST_NAME_UQ = RRC.RDB$CONSTRAINT_NAME AND  '#13#10'              ' +
      '                                  RRC.RDB$CONSTRAINT_TYPE = '#39'PRI' +
      'MARY KEY'#39'  '#13#10'WHERE       RRF.RDB$RELATION_NAME NOT STARTING WITH' +
      ' '#39'RDB$'#39'  '#13#10'ORDER BY    RRF.RDB$RELATION_NAME'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 200
    Top = 232
    object SDFieldsTABELA: TStringField
      FieldName = 'TABELA'
      FixedChar = True
      Size = 93
    end
    object SDFieldsCAMPO: TStringField
      FieldName = 'CAMPO'
      FixedChar = True
      Size = 93
    end
    object SDFieldsTIPO_CAMPO: TStringField
      FieldName = 'TIPO_CAMPO'
      FixedChar = True
      Size = 93
    end
    object SDFieldsTAMANHO: TSmallintField
      FieldName = 'TAMANHO'
    end
    object SDFieldsESCALA: TFMTBCDField
      FieldName = 'ESCALA'
      Precision = 15
      Size = 0
    end
    object SDFieldsPRIMARY_KEY: TStringField
      FieldName = 'PRIMARY_KEY'
      Required = True
      FixedChar = True
      Size = 1
    end
    object SDFieldsNOT_NULL: TStringField
      FieldName = 'NOT_NULL'
      Required = True
      FixedChar = True
      Size = 1
    end
    object SDFieldsFOREIGN_KEY: TStringField
      FieldName = 'FOREIGN_KEY'
      Required = True
      FixedChar = True
      Size = 1
    end
    object SDFieldsINDICE_CHAVE: TStringField
      FieldName = 'INDICE_CHAVE'
      FixedChar = True
      Size = 93
    end
    object SDFieldsTABELA_CHAVE: TStringField
      FieldName = 'TABELA_CHAVE'
      FixedChar = True
      Size = 93
    end
    object SDFieldsCAMPO_CHAVE: TStringField
      FieldName = 'CAMPO_CHAVE'
      FixedChar = True
      Size = 93
    end
    object SDFieldsREGRA_UPDATE: TStringField
      FieldName = 'REGRA_UPDATE'
      FixedChar = True
      Size = 11
    end
    object SDFieldsREGRA_DELETE: TStringField
      FieldName = 'REGRA_DELETE'
      FixedChar = True
      Size = 11
    end
  end
  object DsFields: TDataSource
    DataSet = SDFields
    Left = 228
    Top = 232
  end
  object DBAnt: TMySQLDatabase
    DatabaseName = 'zzz_ib_db_ant'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=zzz_ib_db_ant'
      'PWD=wkljweryhvbirt'
      'UID=root')
    DatasetOptions = []
    Left = 172
    Top = 260
  end
  object QrUpd: TMySQLQuery
    Database = DBAnt
    Left = 200
    Top = 260
  end
  object QrAux: TMySQLQuery
    Database = DBAnt
    Left = 228
    Top = 260
  end
  object QrTabelas: TMySQLQuery
    Database = DBAnt
    SQL.Strings = (
      'SELECT DISTINCT Tabela '
      'FROM _tab_tabs_'
      'ORDER BY Tabela'
      '')
    Left = 172
    Top = 288
    object QrTabelasTabela: TStringField
      FieldName = 'Tabela'
      Size = 60
    end
  end
  object QrCampos: TMySQLQuery
    Database = DBAnt
    SQL.Strings = (
      'SELECT * '
      'FROM _tab_tabs_'
      'WHERE Tabela="ABASTECIB"'
      ''
      '')
    Left = 200
    Top = 288
    object QrCamposTabela: TStringField
      FieldName = 'Tabela'
      Size = 60
    end
    object QrCamposCampo: TStringField
      FieldName = 'Campo'
      Size = 60
    end
    object QrCamposDataType: TStringField
      FieldName = 'DataType'
      Size = 30
    end
    object QrCamposTamanho: TIntegerField
      FieldName = 'Tamanho'
    end
    object QrCamposEscala: TIntegerField
      FieldName = 'Escala'
    end
    object QrCamposPrimaryKey: TStringField
      FieldName = 'PrimaryKey'
      Size = 1
    end
    object QrCamposNotNull: TStringField
      FieldName = 'NotNull'
      Size = 1
    end
    object QrCamposForeignKey: TStringField
      FieldName = 'ForeignKey'
      Size = 1
    end
    object QrCamposIndiceChave: TStringField
      FieldName = 'IndiceChave'
      Size = 60
    end
    object QrCamposTabelaChave: TStringField
      FieldName = 'TabelaChave'
      Size = 60
    end
    object QrCamposCampoChave: TStringField
      FieldName = 'CampoChave'
      Size = 60
    end
    object QrCamposRegraUpdate: TStringField
      FieldName = 'RegraUpdate'
      Size = 30
    end
    object QrCamposRegraDelete: TStringField
      FieldName = 'RegraDelete'
      Size = 30
    end
  end
  object SDDados: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'SELECT * FROM ?'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 256
    Top = 232
  end
  object SDTabelas: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 200
    Top = 204
  end
  object DsTabelas: TDataSource
    DataSet = SDTabelas
    Left = 228
    Top = 204
  end
end
