object FmPrincipal: TFmPrincipal
  Left = 0
  Top = 0
  Caption = 'FmPrincipal'
  ClientHeight = 576
  ClientWidth = 800
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 800
    Height = 41
    Align = alTop
    TabOrder = 0
    object Button4: TButton
      Left = 572
      Top = 8
      Width = 103
      Height = 25
      Caption = '1. Cria Base MySQL'
      TabOrder = 0
      OnClick = Button4Click
    end
    object Button1: TButton
      Left = 676
      Top = 8
      Width = 103
      Height = 25
      Caption = '2. Importa Dados'
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button5: TButton
      Left = 8
      Top = 8
      Width = 103
      Height = 25
      Caption = '0. Conectar'
      TabOrder = 2
      OnClick = Button5Click
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 41
    Width = 800
    Height = 535
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = ' Dados '
      object DBGrid2: TDBGrid
        Left = 229
        Top = 33
        Width = 563
        Height = 474
        Align = alClient
        DataSource = DsDados
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 33
        Width = 229
        Height = 474
        Align = alLeft
        DataSource = DsTabelas
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 33
        Align = alTop
        ParentBackground = False
        TabOrder = 2
        object Label1: TLabel
          Left = 404
          Top = 12
          Width = 28
          Height = 13
          Caption = 'Pular:'
        end
        object Label2: TLabel
          Left = 544
          Top = 12
          Width = 49
          Height = 13
          Caption = 'Registros:'
        end
        object BitBtn1: TBitBtn
          Left = 4
          Top = 4
          Width = 75
          Height = 25
          Caption = 'Tabelas'
          TabOrder = 0
          OnClick = BitBtn1Click
        end
        object Button3: TButton
          Left = 80
          Top = 4
          Width = 75
          Height = 25
          Caption = 'Dados'
          TabOrder = 1
          OnClick = Button3Click
        end
        object Button6: TButton
          Left = 156
          Top = 4
          Width = 75
          Height = 25
          Caption = 'Estrutura'
          TabOrder = 2
          OnClick = Button6Click
        end
        object CkLimit: TCheckBox
          Left = 304
          Top = 12
          Width = 97
          Height = 17
          Caption = 'Limitar dados:'
          TabOrder = 3
        end
        object EdSkip: TdmkEdit
          Left = 440
          Top = 8
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdFirst: TdmkEdit
          Left = 600
          Top = 8
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '100'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 100
          ValWarn = False
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = ' Estrutura '
      ImageIndex = 1
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 41
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        object Button2: TButton
          Left = 4
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Fields'
          TabOrder = 0
          OnClick = Button2Click
        end
      end
      object DBGrid3: TDBGrid
        Left = 0
        Top = 41
        Width = 792
        Height = 466
        Align = alClient
        DataSource = DsFields
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 29
        Align = alTop
        TabOrder = 0
        object Campo: TLabel
          Left = 8
          Top = 4
          Width = 37
          Height = 13
          Caption = 'Campo:'
        end
        object EdCampo: TEdit
          Left = 52
          Top = 0
          Width = 121
          Height = 21
          TabOrder = 0
          OnExit = EdCampoExit
        end
      end
      object DBMemo1: TDBMemo
        Left = 0
        Top = 29
        Width = 792
        Height = 478
        Align = alClient
        DataSource = DsDados
        TabOrder = 1
      end
    end
  end
  object DBFB: TSQLConnection
    ConnectionName = 'FBConnection'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Firebird'
      'Database=C:\Teste\teste5.fdb'
      'RoleName=RoleName'
      'User_Name=sysdba'
      'Password=masterkey'
      'ServerCharSet='
      'SQLDialect=3'
      'ErrorResourceFile='
      'LocaleCode=0000'
      'BlobSize=-1'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'IsolationLevel=ReadCommitted'
      'Trim Char=False')
    Left = 44
    Top = 156
  end
  object DsTabelas: TDataSource
    DataSet = SDTabelas
    Left = 44
    Top = 252
  end
  object SDTabelas: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    AfterScroll = SDTabelasAfterScroll
    Left = 44
    Top = 204
  end
  object SDFields: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 
      'SELECT      RRF.RDB$RELATION_NAME AS TABELA,  '#13#10'            RRF.' +
      'RDB$FIELD_NAME AS CAMPO,  '#13#10'            CASE  '#13#10'              RT' +
      'P.RDB$TYPE_NAME  '#13#10'                WHEN '#39'VARYING'#39'  THEN  '#39'VARCHA' +
      'R'#39'  '#13#10'                WHEN '#39'LONG'#39'     THEN  '#39'INTEGER'#39'  '#13#10'       ' +
      '         WHEN '#39'SHORT'#39'    THEN  '#39'SMALLINT'#39'  '#13#10'                WHE' +
      'N '#39'DOUBLE'#39'   THEN  '#39'DOUBLE'#39'  '#13#10'                WHEN '#39'FLOAT'#39'    T' +
      'HEN  '#39'FLOAT'#39'  '#13#10'                WHEN '#39'INT64'#39'    THEN  '#39'NUMERIC'#39' ' +
      ' '#13#10'                WHEN '#39'TEXT'#39'     THEN  '#39'CHAR'#39'  '#13#10'             ' +
      '   ELSE RTP.RDB$TYPE_NAME  '#13#10'            END TIPO_CAMPO,  '#13#10'    ' +
      '        CASE  '#13#10'              RTP.RDB$TYPE_NAME  '#13#10'             ' +
      '   WHEN  '#39'VARYING'#39' THEN RFL.RDB$FIELD_LENGTH  '#13#10'                ' +
      'ELSE  RFL.RDB$FIELD_PRECISION  '#13#10'            END AS TAMANHO,  '#13#10 +
      '            (RFL.RDB$FIELD_SCALE * -1) AS ESCALA,  '#13#10'           ' +
      ' IIF(  EXISTS(   SELECT      FIRST 1 1  '#13#10'                      ' +
      '      FROM        RDB$RELATION_CONSTRAINTS  RCN  '#13#10'             ' +
      '               INNER JOIN  RDB$INDEX_SEGMENTS        ISG     ON ' +
      '   RCN.RDB$INDEX_NAME = ISG.RDB$INDEX_NAME AND  '#13#10'              ' +
      '                                                                ' +
      '  ISG.RDB$FIELD_NAME = RRF.RDB$FIELD_NAME  '#13#10'                   ' +
      '         WHERE       RCN.RDB$CONSTRAINT_TYPE = '#39'PRIMARY KEY'#39' AND' +
      '  '#13#10'                                        RCN.RDB$RELATION_NAM' +
      'E =  RRF.RDB$RELATION_NAME),  '#13#10'                  '#39'X'#39',  '#13#10'      ' +
      '            '#39'O'#39')  AS  PRIMARY_KEY,  '#13#10'            IIF(  EXISTS( ' +
      '  SELECT      FIRST 1 1  '#13#10'                            FROM     ' +
      '   RDB$RELATION_CONSTRAINTS  RCN  '#13#10'                            ' +
      'INNER JOIN  RDB$CHECK_CONSTRAINTS     CCN     ON    RCN.RDB$CONS' +
      'TRAINT_NAME = CCN.RDB$CONSTRAINT_NAME AND  '#13#10'                   ' +
      '                                                             CCN' +
      '.RDB$TRIGGER_NAME = RRF.RDB$FIELD_NAME  '#13#10'                      ' +
      '       WHERE      RCN.RDB$RELATION_NAME =  RRF.RDB$RELATION_NAME' +
      ' ),  '#13#10'                  '#39'X'#39',  '#13#10'                  '#39'O'#39')  AS  NOT' +
      '_NULL,  '#13#10'            IIF(  RRC.RDB$RELATION_NAME IS NOT NULL,  ' +
      ' '#13#10'                  '#39'X'#39',  '#13#10'                  '#39'O'#39')  AS  FOREIGN' +
      '_KEY,  '#13#10'            RFC.RDB$CONST_NAME_UQ AS  INDICE_CHAVE,  '#13#10 +
      '            RRC.RDB$RELATION_NAME AS  TABELA_CHAVE,  '#13#10'         ' +
      '   RIS2.RDB$FIELD_NAME   AS  CAMPO_CHAVE,  '#13#10'            RFC.RDB' +
      '$UPDATE_RULE   AS  REGRA_UPDATE,  '#13#10'            RFC.RDB$DELETE_R' +
      'ULE   AS  REGRA_DELETE  '#13#10'FROM        RDB$RELATION_FIELDS   RRF ' +
      ' '#13#10'INNER JOIN  RDB$FIELDS            RFL     ON    RFL.RDB$FIELD' +
      '_NAME = RRF.RDB$FIELD_SOURCE  '#13#10'INNER JOIN  RDB$TYPES           ' +
      '  RTP     ON    RTP.RDB$TYPE = RFL.RDB$FIELD_TYPE AND  '#13#10'       ' +
      '                                         RTP.RDB$FIELD_NAME = '#39'R' +
      'DB$FIELD_TYPE'#39'  '#13#10'LEFT JOIN   RDB$INDEX_SEGMENTS    RIS     ON  ' +
      '  RIS.RDB$FIELD_NAME = RRF.RDB$FIELD_NAME AND  '#13#10'               ' +
      '                                 EXISTS (  SELECT      FIRST 1 1' +
      '  '#13#10'                                                          FR' +
      'OM        RDB$INDICES   IND  '#13#10'                                 ' +
      '                         INNER JOIN  RDB$REF_CONSTRAINTS   RFC  ' +
      ' ON    RFC.RDB$CONSTRAINT_NAME = IND.RDB$INDEX_NAME  '#13#10'         ' +
      '                                                 WHERE       IND' +
      '.RDB$INDEX_NAME = RIS.RDB$INDEX_NAME AND  '#13#10'                    ' +
      '                                                  IND.RDB$RELATI' +
      'ON_NAME = RRF.RDB$RELATION_NAME)  '#13#10'LEFT JOIN   RDB$REF_CONSTRAI' +
      'NTS   RFC     ON    RFC.RDB$CONSTRAINT_NAME = RIS.RDB$INDEX_NAME' +
      '  '#13#10'LEFT JOIN   RDB$INDEX_SEGMENTS    RIS2    ON    RIS2.RDB$IND' +
      'EX_NAME = RFC.RDB$CONST_NAME_UQ AND  '#13#10'                         ' +
      '                       RIS2.RDB$FIELD_POSITION = RIS.RDB$FIELD_P' +
      'OSITION  '#13#10'LEFT  JOIN  RDB$RELATION_CONSTRAINTS RRC  ON    RFC.R' +
      'DB$CONST_NAME_UQ = RRC.RDB$CONSTRAINT_NAME AND  '#13#10'              ' +
      '                                  RRC.RDB$CONSTRAINT_TYPE = '#39'PRI' +
      'MARY KEY'#39'  '#13#10'WHERE       RRF.RDB$RELATION_NAME NOT STARTING WITH' +
      ' '#39'RDB$'#39'  '#13#10'ORDER BY    RRF.RDB$RELATION_NAME'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    AfterScroll = SDTabelasAfterScroll
    Left = 44
    Top = 300
    object SDFieldsTABELA: TStringField
      FieldName = 'TABELA'
      FixedChar = True
      Size = 93
    end
    object SDFieldsCAMPO: TStringField
      FieldName = 'CAMPO'
      FixedChar = True
      Size = 93
    end
    object SDFieldsTIPO_CAMPO: TStringField
      FieldName = 'TIPO_CAMPO'
      FixedChar = True
      Size = 93
    end
    object SDFieldsTAMANHO: TSmallintField
      FieldName = 'TAMANHO'
    end
    object SDFieldsESCALA: TFMTBCDField
      FieldName = 'ESCALA'
      Precision = 15
      Size = 0
    end
    object SDFieldsPRIMARY_KEY: TStringField
      FieldName = 'PRIMARY_KEY'
      Required = True
      FixedChar = True
      Size = 1
    end
    object SDFieldsNOT_NULL: TStringField
      FieldName = 'NOT_NULL'
      Required = True
      FixedChar = True
      Size = 1
    end
    object SDFieldsFOREIGN_KEY: TStringField
      FieldName = 'FOREIGN_KEY'
      Required = True
      FixedChar = True
      Size = 1
    end
    object SDFieldsINDICE_CHAVE: TStringField
      FieldName = 'INDICE_CHAVE'
      FixedChar = True
      Size = 93
    end
    object SDFieldsTABELA_CHAVE: TStringField
      FieldName = 'TABELA_CHAVE'
      FixedChar = True
      Size = 93
    end
    object SDFieldsCAMPO_CHAVE: TStringField
      FieldName = 'CAMPO_CHAVE'
      FixedChar = True
      Size = 93
    end
    object SDFieldsREGRA_UPDATE: TStringField
      FieldName = 'REGRA_UPDATE'
      FixedChar = True
      Size = 11
    end
    object SDFieldsREGRA_DELETE: TStringField
      FieldName = 'REGRA_DELETE'
      FixedChar = True
      Size = 11
    end
  end
  object DsFields: TDataSource
    DataSet = SDFields
    Left = 44
    Top = 344
  end
  object SDDados: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    BeforeClose = SDDadosBeforeClose
    AfterScroll = SDTabelasAfterScroll
    Left = 140
    Top = 156
  end
  object DsDados: TDataSource
    DataSet = SDDados
    Left = 140
    Top = 204
  end
end
