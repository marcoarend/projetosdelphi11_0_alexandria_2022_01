unit UnApp_PF;

interface

uses
  Winapi.Windows, System.SysUtils, System.Classes,
  dmkGeral, UnDMkEnums;

type
  TSvcMsgKind = (smkUnknown=0, smkRunOK=1, smkGetErr=2);
  TUnApp_PF = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    function  FiltroDadosReleventesAlheios(): String;
    procedure InformaLoadCSV(Texto: String);
    //procedure AtualizaLogFile(Texto: String);
    procedure SalvaEmArquivoLog(SvcMsgKind: TSvcMsgKind; Msg: String);
  end;

var
  App_PF: TUnApp_PF;

implementation

uses UnOVS_ProjGroupVars, //UnApp_Vars,
  Module, ModuleGeral;

{ TUnApp_PF }

(*
procedure TUnApp_PF.AtualizaLogFile(Texto: String);
const
  Caminho = 'C:\Dermatek\OverSeerSvc\Logs\';
var
  StrList: TStringList;
  FileName: String;
  Agora: TDateTime;
begin
  try
    if not DirectoryExists(Caminho) then
      CreateDir(Caminho);
    //
    StrList := TStringList.Create;
    try
      Agora := Now();
      FileName := Caminho + 'Log_' + FormatDateTime('YYYY_MM') + '.log';
      if FileExists(FileName) then
        strList.LoadFromFile(FileName);

      strList.Add('My new line');

      strList.SaveToFile(FileName);
    finally
      strList.Free;
    end;
  except
    Geral.MB_Erro('N�o foi poss�vel gravar o arquivo de log do OverSeerSvc!');
  end;
end;
*)

function TUnApp_PF.FiltroDadosReleventesAlheios(): String;
var
  SQL_NrLote,
  SQL_Empresas,
  SQL_Externo,
  SQL_TipoOP,
  SQL_NrSituacaoOP,
  SQL_TipLoclz,
  SQL_TipProdOP: String;
begin
  SQL_NrLote       := '';
  SQL_Empresas     := '';
  SQL_Externo      := '';
  SQL_TipoOP       := '';
  SQL_NrSituacaoOP := '';
  SQL_TipLoclz     := '';
  SQL_TipProdOP    := '';
  case Dmod.QrOpcoesAppLoadCSVSoLote.Value of
    //N/D
    //0: SQL_NrLote := 'WHERE fop.NrLote > -999999999';
    //Com n�mero de lote
    1: SQL_NrLote := 'WHERE fop.NrLote > 0' + sLineBreak;
    //Sem n�mero de lote
    2: SQL_NrLote := 'WHERE fop.NrLote = 0' + sLineBreak;
    //Qualquer situa��o
    3: SQL_NrLote := 'WHERE fop.NrLote > -999999999' + sLineBreak;
    else
    begin
      SQL_NrLote := 'WHERE fop.NrLote > -999999999' + sLineBreak;
      Geral.MB_Aviso(
      'Par�metro "Lote" n�o definido para em "Importa��o de dados" nas op��es espec�ficas.');
    end;
  end;
  //
  if Dmod.QrOpcoesAppLoadCSVEmpresas.Value <> '' then
    if Dmod.QrOpcoesAppLoadCSVEmpresas.Value <> '0' then
      SQL_Empresas := 'AND fop.Empresa IN (' +
      Dmod.QrOpcoesAppLoadCSVEmpresas.Value + ')'  + sLineBreak;
  //
  case Dmod.QrOpcoesAppLoadCSVIntExt.Value of
    //N/D
    //0:
    //Intermos
    1: SQL_Externo := 'AND dlo.Externo="NAO" ' + sLineBreak;
    //Externos
    2: SQL_Externo := 'AND dlo.Externo="SIM" ' + sLineBreak;
    //Ambos
    3: SQL_Externo := '';
    else
    begin
      SQL_Externo := '';
      Geral.MB_Aviso(
      'Par�metro "Locais" n�o definido para em "Importa��o de dados" nas op��es espec�ficas.');
    end;
  end;
  //
  if Dmod.QrOpcoesAppLoadCSVTipOP.Value <> '' then
    SQL_TipoOP := 'AND fop.TipoOP IN (' +
      Dmod.QrOpcoesAppLoadCSVTipOP.Value + ')' + sLineBreak;
  //
  if Dmod.QrOpcoesAppLoadCSVSitOP.Value <> '' then
    SQL_TipoOP := 'AND fop.NrSituacaoOP IN (' +
      Dmod.QrOpcoesAppLoadCSVSitOP.Value + ')' + sLineBreak;
  //
  if Dmod.QrOpcoesAppLoadCSVTipLoclz.Value <> '' then
    SQL_TipLoclz := 'AND fop.TipoLocalizacao IN (' +
      Dmod.QrOpcoesAppLoadCSVTipLoclz.Value + ')' + sLineBreak;
  //
  if Dmod.QrOpcoesAppLoadCSVTipProdOP.Value <> '' then
    SQL_TipProdOP := 'AND fop.NrTipoProducaoOP IN (' +
      Dmod.QrOpcoesAppLoadCSVTipProdOP.Value + ')' + sLineBreak;
  //

  Result :=
    SQL_NrLote +
    SQL_Empresas +
    SQL_Externo +
    SQL_TipoOP +
    SQL_NrSituacaoOP +
    SQL_TipLoclz +
    SQL_TipProdOP;
end;

procedure TUnApp_PF.InformaLoadCSV(Texto: String);
begin
  if VAR_LOAD_CSV_Memo <> nil then
    VAR_LOAD_CSV_Memo.Text := Geral.FDT(DModG.ObtemAgora(), 2) + ' ' + Texto +
    sLineBreak + VAR_LOAD_CSV_Memo.Text;

end;

procedure TUnApp_PF.SalvaEmArquivoLog(SvcMsgKind: TSvcMsgKind; Msg: String);
const
  Dir = 'C:\Dermatek\OverSeerSvc\Logs\';
var
  ArqLog, aamm, PreTxt: String;
  Lista: TStringList;
begin
  if not DirectoryExists(Dir) then
    ForceDirectories(Dir);
  //
  PreTxt := '[' + Geral.FDT(Now(), 109) + '][' + VAR_RANDM_STR_ONLY_ONE_ACESS + ']:';
  aamm := FormatDateTime('yyyy_mm', Now());
  ArqLog := Dir + 'Log_' + aamm + '.log';
  try
    // Cria lista
    Lista := TStringList.Create;
    try
      // se o arquivo existe, carrega ele;
      if FileExists(ArqLog) then
        Lista.LoadFromFile(ArqLog);
      // adiciona a nova string
      Lista.Add(PreTxt + Msg);
    except
      on E: Exception do
        Lista.Add(PreTxt + E.Message);
    end;
  finally
    // Atualiza log
    Lista.SaveToFile(ArqLog);
    // Libera lista
    Lista.Free;
  end;
end;

end.
