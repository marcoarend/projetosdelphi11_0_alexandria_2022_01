unit OpcoesApp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, System.DateUtils, System.JSON,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, dmkEdit, Vcl.ExtCtrls,
  Vcl.ComCtrls, dmkEditDateTimePicker, Vcl.Buttons, IdBaseComponent,
  IdComponent, IdUDPBase, IdUDPClient, IdSNTP, dmkCheckBox,
  System.IOUtils, Winapi.ShlObj;

type
  TFmOpcoesApp = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    EdCPF_CNPJ: TdmkEdit;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    EdPosicao: TdmkEdit;
    Label2: TLabel;
    EdCodigoPIN: TdmkEdit;
    Label3: TLabel;
    EdAppUserPwd: TdmkEdit;
    Label4: TLabel;
    CkExigeSenhaLoginApp: TdmkCheckBox;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    function Licencia(): Boolean;
  public
    { Public declarations }
    FAtualizadoEm,
    FSerialHD,
    FCPUID,
    FSerialKey: String;
    FLiberado: Boolean;
  end;

var
  FmOpcoesApp: TFmOpcoesApp;

implementation

uses dmkGeral, UnProjGroup_Vars, UnProjGroup_PF, Principal, UnLicVendaApp_Dmk, MyListas,
  UnGrl_Vars;

{$R *.dfm}

procedure TFmOpcoesApp.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesApp.BtOKClick(Sender: TObject);
begin
  if Salva() then
    Licencia();
end;

function TFmOpcoesApp.Licencia: Boolean;
var
  //CNPJ, SerialHD, CPUID, SerialKey: String;
  CodAplic, Versao: Integer;
  CodigoPin: String;
begin
  VAR_CPF_CNPJ := Geral.SoNumero_TT(EdCPF_CNPJ.ValueVariant);
  if LicVendaApp_Dmk.ObtemCodigoPinDeCnpj(
    EdCPF_CNPJ.Text, // CNPJ do cliente
    EdPosicao.ValueVariant, // Posi��o de 1 a 10. Come�ar com 1 e ir incrementando conforme o cliente solicita
    CodigoPIN) then // CodigoPin - ser� fornecido ao cliente via WhatsApp, email ou outro meio seguro
  begin
    if CodigoPin <> EdCodigoPIN.Text then
    begin
      Geral.MB_Info('C�digo PIN inv�lido!');
      Exit;
    end;
  end else Exit;
  //
  CodAplic  := CO_DMKID_APP;
  Versao    := CO_VERSAO;
  //DataVenda := TPDataVenda.Date;
  //
 if LicVendaApp_Dmk.LiberaUso(VAR_CPF_CNPJ, CodigoPIN, CodAplic, Versao, FAtualizadoEm, FSerialHD,
  FCPUID, FSerialKey) then
  begin
    FLiberado := True;
    Close;
  end else
   Halt(0);
end;

function TFmOpcoesApp.Salva(): Boolean;
var
  lJsonObj: TJSONObject;
  TxtCript, Arquivo: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  try
    lJsonObj      := TJSONObject.Create;
    //
    VAR_SENHA_BOSS            := EdAppUserPwd.Text;
    VAR_CPF_CNPJ              := Geral.SoNumero_TT(EdCPF_CNPJ.ValueVariant);
    VAR_Posicao               := EdPosicao.ValueVariant;
    VAR_CodigoPIN             := EdCodigoPIN.Text;
    VAR_ExigeSenhaLoginApp    := CkExigeSenhaLoginApp.Checked;

    lJsonObj.AddPair('SENHA_BOSS', Geral.JsonText(VAR_SENHA_BOSS));
    lJsonObj.AddPair('CPF_CNPJ', Geral.JsonText(VAR_CPF_CNPJ));
    lJsonObj.AddPair('Posicao', Geral.JsonText(Geral.FF0(VAR_Posicao)));
    lJsonObj.AddPair('CodigoPIN', Geral.JsonText(VAR_CodigoPIN));
    lJsonObj.AddPair('ExigeSenhaLoginApp',  Geral.JsonText(Geral.BoolToNumStr(VAR_ExigeSenhaLoginApp)));

    TxtCript := ProjGroup_PF.EnCryptJSON(lJsonObj.ToString, FmPrincipal.DCP_twofish1);
    Arquivo := ProjGroup_PF.ObtemNomeArquivoOpcoes();
    ForceDirectories(ExtractFilePath(ProjGroup_PF.ObtemNomeArquivoOpcoes()));
    TFile.WriteAllText(Arquivo, TxtCript, TEncoding.ANSI);

    VAR_SENHA_BOSS := EdAppUserPwd.Text;
    //
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
