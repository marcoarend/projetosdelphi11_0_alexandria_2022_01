unit Dmk_DoleOut;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, dmkEdit,
  Vcl.ExtCtrls;

type
  TFmDmk_DoleOut = class(TForm)
    Panel1: TPanel;
    Label4: TLabel;
    EdAppUserPwd: TdmkEdit;
    Panel2: TPanel;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    SpeedButton1: TSpeedButton;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SpeedButton1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    FTentativas: Integer;
  public
    { Public declarations }
    FResult: Boolean;
  end;

var
  FmDmk_DoleOut: TFmDmk_DoleOut;

implementation

uses DmkGeral, UnProjGroup_Vars;

{$R *.dfm}

procedure TFmDmk_DoleOut.BtDesisteClick(Sender: TObject);
begin
  Halt(0);
end;

procedure TFmDmk_DoleOut.BtOKClick(Sender: TObject);
begin
  if AnsiUppercase(EdAppUserPwd.Text) = AnsiUppercase(VAR_SENHA_BOSS) then
  begin
    FResult := True;
    Close;
  end else
  begin
    Geral.MB_Aviso('Senha inv�lida!');
    FTentativas := FTentativas + 1;
    if FTentativas >= 3 then
      Halt(0);
  end;
end;

procedure TFmDmk_DoleOut.FormCreate(Sender: TObject);
begin
  FResult     := False;
  FTentativas := 0;
end;

procedure TFmDmk_DoleOut.SpeedButton1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  EdAppUserPwd.Font.Charset := DEFAULT_CHARSET;
  EdAppUserPwd.PasswordChar := #0;
end;

procedure TFmDmk_DoleOut.SpeedButton1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  EdAppUserPwd.Font.Charset := SYMBOL_CHARSET;
  EdAppUserPwd.PasswordChar := 'l';
end;

end.
