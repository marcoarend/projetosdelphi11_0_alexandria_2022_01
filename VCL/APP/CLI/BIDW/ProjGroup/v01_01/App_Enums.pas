unit App_Enums;

interface

uses  System.SysUtils, System.Types {$IfDef MSWINDOWS}, Winapi.Windows {$EndIf};

const
  Max_BWLiciOrdem = 64;

type
  TBWLiciOrdem = (bwloNenhum=0,
bwloNO_BWStatus=1,
bwloReabertuDt_TXT=2,
bwloReabertuHr_TXT=3,
bwloAberturaDt=4,
bwloAberturaHr=5,
bwloLimCadPpDt=6,
bwloLimCadPpHr=7,
bwloNO_Cliente=8,
bwloUF=9,
bwloPregao=10,
bwloLote=11,
bwloItemLt=12,
bwloNO_Produto=13,
bwloNO_SIGLA=14,
bwloQtdeItem=15,
bwloValrUnit=16,
bwloValItem=17,
bwloNO_BWPortal=18,
bwloNrProcesso=19,
bwloColocacao=20,
bwloEstah=21,
bwloNO_PrzAmostras=22,
bwloNO_PrzEntregas=23,
bwloNO_Responsav1=24,
bwloAmstrConvocDt_TXT=25,
bwloAmstrConvocHr=26,
bwloAmstrLimEntrDt_TXT=27,
bwloAmstrLimEntrHr=28,
bwloAmstrEntrReaDt_TXT=29,
bwloAmstrEntrReaHr=30,
bwloNO_Responsav2=31,
bwloNO_Responsav3=32,
bwloNO_Empresa=33,
bwloNO_GraFabCad=34,
bwloNo_GraFabMCd=35,
bwloNO_Segmento=36,
bwloNO_Representn=37,
bwloValrPregao=38,
bwloValrVenced=39,
bwloClassifcac=40,
bwloQtdeLote=41,
bwloValLote=42,
bwloQtdeTotal=43,
bwloValTotal=44,
bwloEntidade=45,
bwloCliente=46,
bwloBWStatus=47,
bwloCodigo=48,
bwloControle=49,
bwloConta=50,
bwloProduto=51,
bwloBWPortal=52,
bwloPrzAmostras=53,
bwloPrzEntregas=54,
bwloResponsav1=55,
bwloResponsav2=56,
bwloResponsav3=57,
bwloUnidade=58,
bwloEmpresa=59,
bwloVenceFabri=60,
bwloNome=61,
bwloVenceMarca=62,
bwloSegmento=63,
bwloRepresentn=64);

  TUnDmkEnums = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ObtemBW_FieldName(BWLiciOrdem: Integer): String;
    function  ObtemBW_DisplName(BWLiciOrdem: Integer): String;
  end;
var
  DmkEnums: TUnDmkEnums;


implementation

{ TUnDmkEnums }

{
function TUnDmkEnums.ObtemStr_LogActnID(LogActnID: TLogActnID): String;
begin
  if Integer(High(TLogActnID)) > 1 then
    raise Exception.Create('"ObtemStr_LogActnID" aqu�m do m�ximo!');
  case LogActnID of
    laiOSWCab: LowerCase('OSWCab');
    else (*laiNone:*) Result := '???';
  end;
end;
}

{ TUnDmkEnums }

function TUnDmkEnums.ObtemBW_DisplName(BWLiciOrdem: Integer): String;
begin
  case BWLiciOrdem of
    00: Result := '';
    01: Result := 'Descri��o do Status';
    02: Result := 'Reabertura Data';
    03: Result := 'Reabertura Hora';
    04: Result := 'Abertura Data';
    05: Result := 'Abertura Hora';
    06: Result := 'Limite de Cadadastro da Proposta Data';
    07: Result := 'Limite de Cadadastro da Proposta Hora';
    08: Result := 'Nome do Cliente';
    09: Result := 'UF';
    10: Result := 'Preg�o';
    11: Result := 'Lote';
    12: Result := 'Item do Lote';
    13: Result := 'Nome do Produto';
    14: Result := 'Sigla';
    15: Result := 'Quantidade Item';
    16: Result := 'Valor Unitario';
    17: Result := 'Valor do Item';
    18: Result := 'Nome do Portal';
    19: Result := 'N�mero do Processo';
    20: Result := 'Coloca��o';
    21: Result := 'Est�';
    22: Result := 'Nome prazo Amostras';
    23: Result := 'Nome prazo Entregas';
    24: Result := 'Nome do Responsave1';
    25: Result := 'Amostra Convoca��o Data';
    26: Result := 'Amostra Convoca��o Hora';
    27: Result := 'Amostra Limite de Entrega Data';
    28: Result := 'Amostra Limite de Entrega Hora';
    29: Result := 'Amostra Entrega Realizada Data';
    30: Result := 'Amostra Entrega Realizada Hora';
    31: Result := 'Nome Respons�vel 2';
    32: Result := 'Nome Respons�vel 3';
    33: Result := 'Nome da Empresa';
    34: Result := 'Nome do Fabricane';
    35: Result := 'Nome da Marca do Fabricante';
    36: Result := 'Nome do Segmento';
    37: Result := 'Nome do Representante';
    38: Result := 'Valor do Preg�o';
    39: Result := 'Valor Vencedor';
    40: Result := 'Classifca��o';
    41: Result := 'Quantidade no Lote';
    42: Result := 'Valor do Lote';
    43: Result := 'Quantidade Total';
    44: Result := 'Valor Total';
    45: Result := 'ID da Entidade';
    46: Result := 'ID do Cliente';
    47: Result := 'ID do Status';
    48: Result := 'ID da Licita��o';
    49: Result := 'ID do lote';
    50: Result := 'ID do Item';
    51: Result := 'ID do Produto';
    52: Result := 'ID do Portal';
    53: Result := 'ID do Prazo das Amostras';
    54: Result := 'ID do Prazo das Entregas';
    55: Result := 'ID do Respons�ve1 1';
    56: Result := 'ID do Respons�ve1 2';
    57: Result := 'ID do Respons�ve1 3';
    58: Result := 'ID da Unidade';
    59: Result := 'ID da Empresa';
    60: Result := 'ID do Fabricante Vencedor';
    61: Result := 'Descri��o Licita��o';
    62: Result := 'ID da Marca do Vencedor';
    63: Result := 'ID do Segmento';
    64: Result := 'ID do Representante';
    else Result := '???';
  end
end;

function TUnDmkEnums.ObtemBW_FieldName(BWLiciOrdem: Integer): String;
begin
  case BWLiciOrdem of
    00: Result := ''                    ;
    01: Result := 'NO_BWStatus'         ;
    02: Result := 'ReabertuDt_TXT'      ;
    03: Result := 'ReabertuHr_TXT'      ;
    04: Result := 'AberturaDt'          ;
    05: Result := 'AberturaHr'          ;
    06: Result := 'LimCadPpDt'          ;
    07: Result := 'LimCadPpHr'          ;
    08: Result := 'NO_Cliente'          ;
    09: Result := 'UF'                  ;
    10: Result := 'Pregao'              ;
    11: Result := 'Lote'                ;
    12: Result := 'ItemLt'              ;
    13: Result := 'NO_Produto'          ;
    14: Result := 'NO_SIGLA'            ;
    15: Result := 'QtdeItem'            ;
    16: Result := 'ValrUnit'            ;
    17: Result := 'ValItem'             ;
    18: Result := 'NO_BWPortal'         ;
    19: Result := 'NrProcesso'          ;
    20: Result := 'Colocacao'           ;
    21: Result := 'Estah'               ;
    22: Result := 'NO_PrzAmostras'      ;
    23: Result := 'NO_PrzEntregas'      ;
    24: Result := 'NO_Responsav1'       ;
    25: Result := 'AmstrConvocDt_TXT'   ;
    26: Result := 'AmstrConvocHr'       ;
    27: Result := 'AmstrLimEntrDt_TXT'  ;
    28: Result := 'AmstrLimEntrHr'      ;
    29: Result := 'AmstrEntrReaDt_TXT'  ;
    30: Result := 'AmstrEntrReaHr'      ;
    31: Result := 'NO_Responsav2'       ;
    32: Result := 'NO_Responsav3'       ;
    33: Result := 'NO_Empresa'          ;
    34: Result := 'NO_GraFabCad'        ;
    35: Result := 'No_GraFabMCd'        ;
    36: Result := 'NO_Segmento'         ;
    37: Result := 'NO_Representn'       ;
    38: Result := 'ValrPregao'          ;
    39: Result := 'ValrVenced'          ;
    40: Result := 'Classifcac'          ;
    41: Result := 'QtdeLote'            ;
    42: Result := 'ValLote'             ;
    43: Result := 'QtdeTotal'           ;
    44: Result := 'ValTotal'            ;
    45: Result := 'Entidade'            ;
    46: Result := 'Cliente'             ;
    47: Result := 'BWStatus'            ;
    48: Result := 'Codigo'              ;
    49: Result := 'Controle'            ;
    50: Result := 'Conta'               ;
    51: Result := 'Produto'             ;
    52: Result := 'BWPortal'            ;
    53: Result := 'PrzAmostras'         ;
    54: Result := 'PrzEntregas'         ;
    55: Result := 'Responsav1'          ;
    56: Result := 'Responsav2'          ;
    57: Result := 'Responsav3'          ;
    58: Result := 'Unidade'             ;
    59: Result := 'Empresa'             ;
    60: Result := 'VenceFabri'          ;
    61: Result := 'Nome'                ;
    62: Result := 'VenceMarca'          ;
    63: Result := 'Segmento'            ;
    64: Result := 'Representn'          ;
    else Result := '???'                ;
  end;
end;

end.
