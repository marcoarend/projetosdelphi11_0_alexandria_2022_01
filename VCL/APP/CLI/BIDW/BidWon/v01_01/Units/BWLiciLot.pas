unit BWLiciLot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Menus, Vcl.ComCtrls,
  dmkEditDateTimePicker;

type
  TFiltroPesquisaSQL = (fpsIndef=0, fpsTodos=1, fpsUnico=2, fpsInverte=3);
  TFmBWLiciLot = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrBWPortais: TMySQLQuery;
    DsBWPortais: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label7: TLabel;
    EdLote: TdmkEdit;
    Label1: TLabel;
    EdBWPortal: TdmkEditCB;
    CBBWPortal: TdmkDBLookupComboBox;
    SbPortal: TSpeedButton;
    QrBWPortaisCodigo: TIntegerField;
    QrBWPortaisNome: TWideStringField;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdNrProcesso: TdmkEdit;
    EdColocacao: TdmkEdit;
    EdEstah: TdmkEdit;
    Label5: TLabel;
    EdPrzAmostras: TdmkEditCB;
    CBPrzAmostras: TdmkDBLookupComboBox;
    SbPrzAmostras: TSpeedButton;
    Label8: TLabel;
    EdPrzEntregas: TdmkEditCB;
    CBPrzEntregas: TdmkDBLookupComboBox;
    SbPrzEntregas: TSpeedButton;
    QrPrzAmostras: TMySQLQuery;
    QrPrzAmostrasCodigo: TIntegerField;
    QrPrzAmostrasNome: TWideStringField;
    DsPrzAmostras: TDataSource;
    QrPrzEntregas: TMySQLQuery;
    QrPrzEntregasCodigo: TIntegerField;
    QrPrzEntregasNome: TWideStringField;
    DsPrzEntregas: TDataSource;
    QrBWRespnsvs1: TMySQLQuery;
    QrBWRespnsvs1Codigo: TIntegerField;
    QrBWRespnsvs1Nome: TWideStringField;
    DsBWRespnsvs1: TDataSource;
    QrBWRespnsvs2: TMySQLQuery;
    QrBWRespnsvs2Codigo: TIntegerField;
    QrBWRespnsvs2Nome: TWideStringField;
    DsBWRespnsvs2: TDataSource;
    QrBWRespnsvs3: TMySQLQuery;
    QrBWRespnsvs3Codigo: TIntegerField;
    QrBWRespnsvs3Nome: TWideStringField;
    DsBWRespnsvs3: TDataSource;
    EdResponsav1: TdmkEditCB;
    Label9: TLabel;
    CBResponsav1: TdmkDBLookupComboBox;
    SBResponsav1: TSpeedButton;
    Label10: TLabel;
    EdResponsav2: TdmkEditCB;
    CBResponsav2: TdmkDBLookupComboBox;
    SBResponsav2: TSpeedButton;
    Label11: TLabel;
    EdResponsav3: TdmkEditCB;
    CBResponsav3: TdmkDBLookupComboBox;
    SBResponsav3: TSpeedButton;
    Label12: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    SbEmpresa: TSpeedButton;
    EdVenceFabri: TdmkEditCB;
    CBVenceFabri: TdmkDBLookupComboBox;
    Label13: TLabel;
    SbVenceFabri: TSpeedButton;
    EdVenceMarca: TdmkEditCB;
    Label14: TLabel;
    CBVenceMarca: TdmkDBLookupComboBox;
    SbVenceMarca: TSpeedButton;
    QrBWEmpresas: TMySQLQuery;
    DsBWEmpresas: TDataSource;
    QrGraFabCad: TMySQLQuery;
    QrGraFabCadCodigo: TIntegerField;
    QrGraFabCadNome: TWideStringField;
    DsGraFabCad: TDataSource;
    QrBWEmpresasCodigo: TIntegerField;
    QrBWEmpresasNome: TWideStringField;
    QrGraFabMar: TMySQLQuery;
    QrGraFabMarNO_GraFabMar: TWideStringField;
    DsGraFabMar: TDataSource;
    Label15: TLabel;
    EdSegmento: TdmkEditCB;
    CBSegmento: TdmkDBLookupComboBox;
    SbSegmento: TSpeedButton;
    Label16: TLabel;
    EdRepresentn: TdmkEditCB;
    CBRepresentn: TdmkDBLookupComboBox;
    SbRepresentn: TSpeedButton;
    QrBWSegmento: TMySQLQuery;
    QrBWSegmentoCodigo: TIntegerField;
    QrBWSegmentoNome: TWideStringField;
    DsBWSegmento: TDataSource;
    QrBWRepresnt: TMySQLQuery;
    QrBWRepresntCodigo: TIntegerField;
    QrBWRepresntNome: TWideStringField;
    DsBWRepresnt: TDataSource;
    EdValrPregao: TdmkEdit;
    Label17: TLabel;
    Label18: TLabel;
    EdValrVenced: TdmkEdit;
    Label19: TLabel;
    EdClassifcac: TdmkEdit;
    QrGraFabMarCodigo: TIntegerField;
    PMVenceMarca: TPopupMenu;
    Pelofabricante1: TMenuItem;
    Diretodasmarcas1: TMenuItem;
    GBAmostras: TGroupBox;
    Panel5: TPanel;
    Label20: TLabel;
    TPAmstrConvocDt: TdmkEditDateTimePicker;
    EdAmstrConvocHr: TdmkEdit;
    Label21: TLabel;
    TPAmstrLimEntrDt: TdmkEditDateTimePicker;
    EDAmstrLimEntrHr: TdmkEdit;
    Label22: TLabel;
    TPAmstrEntrReaDt: TdmkEditDateTimePicker;
    EdAmstrEntrReaHr: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbPortalClick(Sender: TObject);
    procedure SbPrzAmostrasClick(Sender: TObject);
    procedure SbPrzEntregasClick(Sender: TObject);
    procedure SBResponsav1Click(Sender: TObject);
    procedure SBResponsav2Click(Sender: TObject);
    procedure SBResponsav3Click(Sender: TObject);
    procedure SbEmpresaClick(Sender: TObject);
    procedure SbVenceFabriClick(Sender: TObject);
    procedure SbSegmentoClick(Sender: TObject);
    procedure EdVenceFabriRedefinido(Sender: TObject);
    procedure EdVenceMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbRepresentnClick(Sender: TObject);
    procedure CBVenceMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Pelofabricante1Click(Sender: TObject);
    procedure Diretodasmarcas1Click(Sender: TObject);
    procedure SbVenceMarcaClick(Sender: TObject);
  private
    { Private declarations }
    FFiltroPesquisaSQL: TFiltroPesquisaSQL;
    //
    procedure ReopenBWLiciLot(Controle: Integer);
    procedure InsereBWLiHEsta(Codigo, Controle, EstahAtu: Integer);
  public
    { Public declarations }
    FCodigo, FEstahAnt: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    procedure ReopenGraFabMar(FiltroPesquisaSQL: TFiltroPesquisaSQL);
  end;

  var
  FmBWLiciLot: TFmBWLiciLot;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  BWPortais, UnApp_Jan, ModuleGeral, CfgCadLista;

{$R *.DFM}

procedure TFmBWLiciLot.BtOKClick(Sender: TObject);
var
  Lote, NrProcesso: String;
  //CtrlMarca,
  Codigo, Controle, BWPortal, Colocacao, Estah, PrzAmostras, PrzEntregas,
  Responsav1, Responsav2, Responsav3, Empresa, VenceFabri, VenceMarca, Segmento,
  Representn, Classifcac: Integer;
  ValrPregao, ValrVenced(*, QtdeLote, ValLote*): Double;
  SQLType: TSQLType;
  AmstrConvocDt, AmstrConvocHr, AmstrLimEntrDt,
  AmstrLimEntrHr, AmstrEntrReaDt, AmstrEntrReaHr: String;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  Controle       := EdControle.ValueVariant;
  Lote           := EdLote.ValueVariant;
  BWPortal       := EdBWPortal.ValueVariant;
  NrProcesso     := EdNrProcesso.ValueVariant;
  Colocacao      := EdColocacao.ValueVariant;
  Estah          := EdEstah.ValueVariant;
  PrzAmostras    := EdPrzAmostras.ValueVariant;
  PrzEntregas    := EdPrzEntregas.ValueVariant;
  Responsav1     := EdResponsav1.ValueVariant;
  Responsav2     := EdResponsav2.ValueVariant;
  Responsav3     := EdResponsav3.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  VenceFabri     := EdVenceFabri.ValueVariant;
  VenceMarca     := EdVenceMarca.ValueVariant;
  Segmento       := EdSegmento.ValueVariant;
  Representn     := EdRepresentn.ValueVariant;
  ValrPregao     := EdValrPregao.ValueVariant;
  ValrVenced     := EdValrVenced.ValueVariant;
  Classifcac     := EdClassifcac.ValueVariant;
  AmstrConvocDt  := Geral.FDT(TPAmstrConvocDt.Date, 1);
  AmstrConvocHr  := EdAmstrConvocHr.Text;
  AmstrLimEntrDt := Geral.FDT(TPAmstrLimEntrDt.Date, 1);
  AmstrLimEntrHr := EDAmstrLimEntrHr.Text;
  AmstrEntrReaDt := Geral.FDT(TPAmstrEntrReaDt.Date, 1);
  AmstrEntrReaHr := EdAmstrEntrReaHr.Text;
  //QtdeLote       :=
  //ValLote        :=
(*
  CtrlMarca      := EdVenceMarca.ValueVariant;
  if CtrlMarca <> 0 then
    VenceMarca := QrGraFabMarCodigo.Value
  else
    VenceMarca := 0;
*)
  //
  if MyObjects.FIC(Trim(Lote) = EmptyStr, EdLote, 'Informe o Lote') then Exit;
  if MyObjects.FIC(Trim(NrProcesso) = EmptyStr, EdNrProcesso, 'Informe o N�mero do Processo') then Exit;
  if MyObjects.FIC(PrzAmostras = 0, EdPrzAmostras, 'Informe o Prazo das Amostras') then Exit;
  if MyObjects.FIC(PrzEntregas = 0, EdPrzEntregas, 'Informe o Prazo das Entregas') then Exit;
  if MyObjects.FIC(Responsav1 = 0, EdResponsav1, 'Informe o Respons�vel 1') then Exit;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a Empresa Participante') then Exit;
  if MyObjects.FIC(Segmento = 0, EdSegmento, 'Informe o Segmento') then Exit;
  if MyObjects.FIC(Representn = 0, EdRepresentn, 'Informe o Representante') then Exit;
  //
  Controle := UMyMod.BPGS1I32('bwlicilot', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwlicilot', False, [
  'Codigo', 'Lote', 'BWPortal',
  'NrProcesso', 'Colocacao', 'Estah',
  'PrzAmostras', 'PrzEntregas', 'Responsav1',
  'Responsav2', 'Responsav3', 'Empresa',
  'VenceFabri', 'VenceMarca', 'Segmento',
  'Representn', 'ValrPregao', 'ValrVenced',
  'Classifcac',(*, 'QtdeLote', 'ValLote'*)
  'AmstrConvocDt', 'AmstrConvocHr', 'AmstrLimEntrDt',
  'AmstrLimEntrHr', 'AmstrEntrReaDt', 'AmstrEntrReaHr'], [
  'Controle'], [
  Codigo, Lote, BWPortal,
  NrProcesso, Colocacao, Estah,
  PrzAmostras, PrzEntregas, Responsav1,
  Responsav2, Responsav3, Empresa,
  VenceFabri, VenceMarca, Segmento,
  Representn, ValrPregao, ValrVenced,
  Classifcac(*, QtdeLote, ValLote*),
  AmstrConvocDt, AmstrConvocHr, AmstrLimEntrDt,
  AmstrLimEntrHr, AmstrEntrReaDt, AmstrEntrReaHr], [
  Controle], True) then
  begin
    InsereBWLiHEsta(Codigo, Controle, Estah);
    //
    ReopenBWLiciLot(Controle);
(*
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      ....
      Ed?.SetFocus;
    end else
*)
      Close;
  end;
end;

procedure TFmBWLiciLot.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBWLiciLot.CBVenceMarcaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    ReopenGraFabMar(fpsInverte);
end;

procedure TFmBWLiciLot.Diretodasmarcas1Click(Sender: TObject);
  procedure MostraFormGraFabMCd();
  begin
    UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'GraFabMCd', 255, ncGerlSeq1,
    'Marcas',
    [], False, Null(*Maximo*), [], [], False);
  end;
begin
  VAR_CADASTRO := 0;
  MostraFormGraFabMCd();
  ReopenGraFabMar(fpsTodos);
  UMyMod.SetaCodigoPesquisado(EdVenceMarca, CBVenceMarca, QrGraFabMar, VAR_CADASTRO);
end;

procedure TFmBWLiciLot.EdVenceFabriRedefinido(Sender: TObject);
begin
  if EdVenceMarca.ValueVariant = 0 then
    ReopenGraFabMar(fpsTodos)
  else
    ReopenGraFabMar(fpsUnico)
end;

procedure TFmBWLiciLot.EdVenceMarcaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //if EdVenceMarca.ValueVariant = 0 then
  if Key = VK_F3 then
    ReopenGraFabMar(fpsInverte);
end;

procedure TFmBWLiciLot.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBWLiciLot.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FEstahAnt          := 0;
  FFiltroPesquisaSQL := TFiltroPesquisaSQL.fpsTodos;
  TPAmstrConvocDt.Date  := 0;
  TPAmstrLimEntrDt.Date := 0;
  TPAmstrEntrReaDt.Date := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrBWPortais, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrzAmostras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrzEntregas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBWRespnsvs1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBWRespnsvs2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBWRespnsvs3, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBWEmpresas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraFabCad, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrGraFabMar, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBWSegmento, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBWRepresnt, Dmod.MyDB);
end;

procedure TFmBWLiciLot.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBWLiciLot.InsereBWLiHEsta(Codigo, Controle, EstahAtu: Integer);
var
  DataHora: String;
  Nivel4: Integer;
  SQLType: TSQLType;
begin
  if (ImgTipo.SQLType = stIns) or (FEstahAnt <> EstahAtu) then
  begin
    SQLType        := stIns;
    //Codigo         := ;
    //Controle       := ;
    Nivel4         := 0;
    DataHora       := Geral.FDT(DModG.ObtemAgora(), 109);
    //Estah          := ;
    //
    Nivel4 := UMyMod.BPGS1I32('bwlihesta', 'Nivel4', '', '', tsPos, SQLType, Nivel4);
    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwlihesta', False, [
    'Codigo', 'Controle', 'DataHora',
    'EstahAnt', 'EstahAtu'], [
    'Nivel4'], [
    Codigo, Controle, DataHora,
    FEstahAnt, EstahAtu], [
    Nivel4], True);
  end;
end;

procedure TFmBWLiciLot.Pelofabricante1Click(Sender: TObject);
begin
  VAR_CADASTRO := EdVenceFabri.ValueVariant;
  VAR_CADASTRO2 := EdVenceMarca.ValueVariant;
  App_Jan.MostraFormGraFabCad(VAR_CADASTRO, VAR_CADASTRO2);
  ReopenGraFabMar(fpsTodos);
  UMyMod.SetaCodigoPesquisado(EdVenceMarca, CBVenceMarca, QrGraFabMar, VAR_CADASTRO2);
end;

procedure TFmBWLiciLot.ReopenBWLiciLot(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmBWLiciLot.ReopenGraFabMar(FiltroPesquisaSQL: TFiltroPesquisaSQL);
var
  SQL: String;
  GraFabMar: Integer;
begin
  case FiltroPesquisaSQL of
    TFiltroPesquisaSQL.fpsIndef,
    TFiltroPesquisaSQL.fpsTodos:
      SQL := '';
    TFiltroPesquisaSQL.fpsUnico: SQL :=
      'WHERE mar.Codigo=' + Geral.FF0(EdVenceFabri.ValueVariant);
    TFiltroPesquisaSQL.fpsInverte:
    begin
      if FFiltroPesquisaSQL = TFiltroPesquisaSQL.fpsTodos then
        FFiltroPesquisaSQL := TFiltroPesquisaSQL.fpsUnico
      else
        FFiltroPesquisaSQL := TFiltroPesquisaSQL.fpsTodos;
      case FFiltroPesquisaSQL of
        TFiltroPesquisaSQL.fpsTodos:
          SQL := '';
        TFiltroPesquisaSQL.fpsUnico: SQL :=
          'WHERE mar.Codigo=' + Geral.FF0(EdVenceFabri.ValueVariant);
      end;
    end;
  end;
  //
  GraFabMar := EdVenceMarca.ValueVariant;
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraFabMar, Dmod.MyDB, [
  'SELECT DISTINCT mcd.Codigo, ',
  //'CONCAT(mcd.Nome, " (", cad.Nome, ")") NO_GraFabMar  ',
  'mcd.Nome NO_GraFabMar  ',
  'FROM grafabmar mar ',
  'LEFT JOIN grafabcad cad ON cad.Codigo=mar.Codigo ',
  'LEFT JOIN grafabmcd mcd ON mcd.Codigo=mar.GraFabMCd ',
  SQL,
  'ORDER BY NO_GraFabMar  ',
  '']);
  if GraFabMar <> 0 then
  begin
    if QrGraFabMar.Locate('Codigo', GraFabMar, []) then
    begin
      EdVenceMarca.ValueVariant := GraFabMar;
      CBVenceMarca.KeyValue     := GraFabMar;
    end else
    begin
      EdVenceMarca.ValueVariant := 0;
      CBVenceMarca.KeyValue     := 0;
    end;
  end;
  if FiltroPesquisaSQL <> TFiltroPesquisaSQL.fpsInverte then
    FFiltroPesquisaSQL := FiltroPesquisaSQL;
end;

procedure TFmBWLiciLot.SbEmpresaClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormBWEmpresas();
  UMyMod.SetaCodigoPesquisado(EdEmpresa, CBEMpresa, QrBWEmpresas, VAR_CADASTRO);
end;

procedure TFmBWLiciLot.SbPortalClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormBWPortais(EdBWPortal.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdBWPortal, CBBWPortal, QrBWPortais, VAR_CADASTRO);
end;

procedure TFmBWLiciLot.SbPrzAmostrasClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan. MostraFormPrzEntrgCab(EdPrzAmostras.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdPrzAmostras, CBPrzAmostras, QrPrzAmostras, VAR_CADASTRO);
end;

procedure TFmBWLiciLot.SbPrzEntregasClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan. MostraFormPrzEntrgCab(EdPrzEntregas.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdPrzEntregas, CBPrzEntregas, QrPrzEntregas, VAR_CADASTRO);
end;

procedure TFmBWLiciLot.SbRepresentnClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormGraFabCad(0,0);
  UMyMod.SetaCodigoPesquisado(EdVenceFabri, CBVenceFabri, QrGraFabCad, VAR_CADASTRO);
end;

procedure TFmBWLiciLot.SBResponsav1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormResponsaveis();
  UMyMod.SetaCodigoPesquisado(EdResponsav1, CBResponsav1, QrBWRespnsvs1, VAR_CADASTRO);
end;

procedure TFmBWLiciLot.SBResponsav2Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormResponsaveis();
  UMyMod.SetaCodigoPesquisado(EdResponsav2, CBResponsav2, QrBWRespnsvs2, VAR_CADASTRO);
end;

procedure TFmBWLiciLot.SBResponsav3Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormResponsaveis();
  UMyMod.SetaCodigoPesquisado(EdResponsav3, CBResponsav3, QrBWRespnsvs3, VAR_CADASTRO);
end;

procedure TFmBWLiciLot.SbSegmentoClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormBWSegmento();
  UMyMod.SetaCodigoPesquisado(EdSegmento, CBSegmento, QrBWSegmento, VAR_CADASTRO);
end;

procedure TFmBWLiciLot.SbVenceFabriClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormGraFabCad(EdVenceFabri.ValueVariant, EdVenceMarca.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdVenceFabri, CBVenceFabri, QrGraFabCad, VAR_CADASTRO);
end;

procedure TFmBWLiciLot.SbVenceMarcaClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMVenceMarca, SbVenceMarca);
end;

end.
