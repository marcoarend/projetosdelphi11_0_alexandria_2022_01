object FmBWLiciLot: TFmBWLiciLot
  Left = 339
  Top = 185
  Caption = 'LIC-TACAO-002 :: Lote de Licita'#231#227'o'
  ClientHeight = 468
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 323
    Width = 1008
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 207
        Height = 32
        Caption = 'Lote de Licita'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 207
        Height = 32
        Caption = 'Lote de Licita'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 207
        Height = 32
        Caption = 'Lote de Licita'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 354
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 398
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 275
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label6: TLabel
      Left = 8
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label7: TLabel
      Left = 92
      Top = 4
      Width = 24
      Height = 13
      Caption = 'Lote:'
    end
    object Label1: TLabel
      Left = 168
      Top = 4
      Width = 30
      Height = 13
      Caption = 'Portal:'
    end
    object SbPortal: TSpeedButton
      Left = 632
      Top = 20
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbPortalClick
    end
    object Label2: TLabel
      Left = 656
      Top = 4
      Width = 76
      Height = 13
      Caption = 'N'#176' do processo:'
    end
    object Label3: TLabel
      Left = 880
      Top = 4
      Width = 54
      Height = 13
      Caption = 'Coloca'#231#227'o:'
    end
    object Label4: TLabel
      Left = 940
      Top = 4
      Width = 24
      Height = 13
      Caption = 'Est'#225':'
    end
    object Label5: TLabel
      Left = 8
      Top = 44
      Width = 144
      Height = 13
      Caption = 'Prazo de entrega de amostras:'
    end
    object SbPrzAmostras: TSpeedButton
      Left = 472
      Top = 60
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbPrzAmostrasClick
    end
    object Label8: TLabel
      Left = 496
      Top = 44
      Width = 198
      Height = 13
      Caption = 'Prazo de entrega dos produtos acabados:'
    end
    object SbPrzEntregas: TSpeedButton
      Left = 972
      Top = 60
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbPrzEntregasClick
    end
    object Label9: TLabel
      Left = 8
      Top = 84
      Width = 74
      Height = 13
      Caption = 'Respons'#225'vel 1:'
    end
    object SBResponsav1: TSpeedButton
      Left = 316
      Top = 100
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBResponsav1Click
    end
    object Label10: TLabel
      Left = 340
      Top = 84
      Width = 74
      Height = 13
      Caption = 'Respons'#225'vel 2:'
    end
    object SBResponsav2: TSpeedButton
      Left = 648
      Top = 100
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBResponsav2Click
    end
    object Label11: TLabel
      Left = 672
      Top = 84
      Width = 74
      Height = 13
      Caption = 'Respons'#225'vel 3:'
    end
    object SBResponsav3: TSpeedButton
      Left = 972
      Top = 100
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBResponsav3Click
    end
    object Label12: TLabel
      Left = 8
      Top = 164
      Width = 102
      Height = 13
      Caption = 'Empresa participante:'
    end
    object SbEmpresa: TSpeedButton
      Left = 232
      Top = 180
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbEmpresaClick
    end
    object Label13: TLabel
      Left = 8
      Top = 124
      Width = 49
      Height = 13
      Caption = 'Vencedor:'
    end
    object SbVenceFabri: TSpeedButton
      Left = 472
      Top = 140
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbVenceFabriClick
    end
    object Label14: TLabel
      Left = 496
      Top = 124
      Width = 214
      Height = 13
      Caption = 'Marca do vencedor [F3] Todos <> Vencedor:'
    end
    object SbVenceMarca: TSpeedButton
      Left = 972
      Top = 140
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbVenceMarcaClick
    end
    object Label15: TLabel
      Left = 256
      Top = 164
      Width = 51
      Height = 13
      Caption = 'Segmento:'
    end
    object SbSegmento: TSpeedButton
      Left = 468
      Top = 180
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbSegmentoClick
    end
    object Label16: TLabel
      Left = 492
      Top = 164
      Width = 73
      Height = 13
      Caption = 'Representante:'
    end
    object SbRepresentn: TSpeedButton
      Left = 708
      Top = 180
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbRepresentnClick
    end
    object Label17: TLabel
      Left = 732
      Top = 164
      Width = 46
      Height = 13
      Caption = '$ Preg'#227'o:'
    end
    object Label18: TLabel
      Left = 828
      Top = 164
      Width = 58
      Height = 13
      Caption = '$ Vencedor:'
    end
    object Label19: TLabel
      Left = 924
      Top = 164
      Width = 65
      Height = 13
      Caption = 'Classifica'#231#227'o:'
    end
    object EdControle: TdmkEdit
      Left = 8
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdLote: TdmkEdit
      Left = 92
      Top = 20
      Width = 73
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBWPortal: TdmkEditCB
      Left = 168
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBBWPortal
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBBWPortal: TdmkDBLookupComboBox
      Left = 224
      Top = 20
      Width = 407
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBWPortais
      TabOrder = 3
      dmkEditCB = EdBWPortal
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdNrProcesso: TdmkEdit
      Left = 656
      Top = 20
      Width = 217
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdColocacao: TdmkEdit
      Left = 880
      Top = 20
      Width = 54
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdEstah: TdmkEdit
      Left = 940
      Top = 20
      Width = 54
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdPrzAmostras: TdmkEditCB
      Left = 8
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPrzAmostras
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPrzAmostras: TdmkDBLookupComboBox
      Left = 64
      Top = 60
      Width = 407
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPrzAmostras
      TabOrder = 8
      dmkEditCB = EdPrzAmostras
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdPrzEntregas: TdmkEditCB
      Left = 496
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPrzEntregas
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPrzEntregas: TdmkDBLookupComboBox
      Left = 552
      Top = 60
      Width = 420
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPrzEntregas
      TabOrder = 10
      dmkEditCB = EdPrzEntregas
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdResponsav1: TdmkEditCB
      Left = 8
      Top = 100
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBResponsav1
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBResponsav1: TdmkDBLookupComboBox
      Left = 64
      Top = 100
      Width = 252
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBWRespnsvs1
      TabOrder = 12
      dmkEditCB = EdResponsav1
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdResponsav2: TdmkEditCB
      Left = 340
      Top = 100
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBResponsav2
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBResponsav2: TdmkDBLookupComboBox
      Left = 396
      Top = 100
      Width = 252
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBWRespnsvs2
      TabOrder = 14
      dmkEditCB = EdResponsav2
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdResponsav3: TdmkEditCB
      Left = 672
      Top = 100
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 15
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBResponsav3
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBResponsav3: TdmkDBLookupComboBox
      Left = 728
      Top = 100
      Width = 244
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBWRespnsvs3
      TabOrder = 16
      dmkEditCB = EdResponsav3
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdEmpresa: TdmkEditCB
      Left = 8
      Top = 180
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 21
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 64
      Top = 180
      Width = 165
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBWEmpresas
      TabOrder = 22
      dmkEditCB = EdEmpresa
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdVenceFabri: TdmkEditCB
      Left = 8
      Top = 140
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 17
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdVenceFabriRedefinido
      DBLookupComboBox = CBVenceFabri
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBVenceFabri: TdmkDBLookupComboBox
      Left = 64
      Top = 140
      Width = 405
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsGraFabCad
      TabOrder = 18
      dmkEditCB = EdVenceFabri
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdVenceMarca: TdmkEditCB
      Left = 496
      Top = 140
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 19
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdVenceMarcaKeyDown
      DBLookupComboBox = CBVenceMarca
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBVenceMarca: TdmkDBLookupComboBox
      Left = 556
      Top = 140
      Width = 415
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NO_GraFabMar'
      ListSource = DsGraFabMar
      TabOrder = 20
      OnKeyDown = CBVenceMarcaKeyDown
      dmkEditCB = EdVenceMarca
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLText.Strings = (
        'SELECT Codigo _CODIGO, Nome _NOME'
        'FROM grafabmcd'
        'WHERE Nome LIKE "$#"'
        'ORDER BY Nome')
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdSegmento: TdmkEditCB
      Left = 256
      Top = 180
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 23
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBSegmento
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBSegmento: TdmkDBLookupComboBox
      Left = 312
      Top = 180
      Width = 153
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBWSegmento
      TabOrder = 24
      dmkEditCB = EdSegmento
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdRepresentn: TdmkEditCB
      Left = 492
      Top = 180
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 25
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBRepresentn
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBRepresentn: TdmkDBLookupComboBox
      Left = 548
      Top = 180
      Width = 157
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBWRepresnt
      TabOrder = 26
      dmkEditCB = EdRepresentn
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdValrPregao: TdmkEdit
      Left = 732
      Top = 180
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 27
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdValrVenced: TdmkEdit
      Left = 828
      Top = 180
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 28
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdClassifcac: TdmkEdit
      Left = 924
      Top = 180
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 29
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object GBAmostras: TGroupBox
      Left = 8
      Top = 204
      Width = 989
      Height = 69
      Caption = ' Amostras: '
      TabOrder = 30
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 985
        Height = 52
        Align = alClient
        TabOrder = 0
        object Label20: TLabel
          Left = 12
          Top = 4
          Width = 64
          Height = 13
          Caption = 'Convoca'#231#227'o:'
        end
        object Label21: TLabel
          Left = 192
          Top = 4
          Width = 84
          Height = 13
          Caption = 'Limite da entrega:'
        end
        object Label22: TLabel
          Left = 364
          Top = 4
          Width = 85
          Height = 13
          Caption = 'Entrega realizada:'
        end
        object TPAmstrConvocDt: TdmkEditDateTimePicker
          Left = 12
          Top = 20
          Width = 112
          Height = 21
          Date = 44195.487580057870000000
          Time = 44195.487580057870000000
          ShowCheckbox = True
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'ReabertuDt'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdAmstrConvocHr: TdmkEdit
          Left = 128
          Top = 20
          Width = 45
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'ReabertuHr'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object TPAmstrLimEntrDt: TdmkEditDateTimePicker
          Left = 192
          Top = 20
          Width = 112
          Height = 21
          Date = 44195.487580092590000000
          Time = 44195.487580092590000000
          ShowCheckbox = True
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'ReabertuDt'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EDAmstrLimEntrHr: TdmkEdit
          Left = 308
          Top = 20
          Width = 45
          Height = 21
          TabOrder = 3
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'ReabertuHr'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object TPAmstrEntrReaDt: TdmkEditDateTimePicker
          Left = 364
          Top = 20
          Width = 112
          Height = 21
          Date = 44195.487580127320000000
          Time = 44195.487580127320000000
          ShowCheckbox = True
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'ReabertuDt'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdAmstrEntrReaHr: TdmkEdit
          Left = 480
          Top = 20
          Width = 45
          Height = 21
          TabOrder = 5
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'ReabertuHr'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
  end
  object QrBWPortais: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM bwportais'
      'ORDER BY Nome')
    Left = 284
    Top = 8
    object QrBWPortaisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWPortaisNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsBWPortais: TDataSource
    DataSet = QrBWPortais
    Left = 284
    Top = 56
  end
  object QrPrzAmostras: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM przentrgcab'
      'ORDER BY Nome')
    Left = 364
    Top = 8
    object QrPrzAmostrasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrzAmostrasNome: TWideStringField
      DisplayWidth = 120
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object DsPrzAmostras: TDataSource
    DataSet = QrPrzAmostras
    Left = 364
    Top = 56
  end
  object QrPrzEntregas: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM przentrgcab'
      'ORDER BY Nome')
    Left = 448
    Top = 8
    object QrPrzEntregasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrzEntregasNome: TWideStringField
      DisplayWidth = 120
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object DsPrzEntregas: TDataSource
    DataSet = QrPrzEntregas
    Left = 448
    Top = 56
  end
  object QrBWRespnsvs1: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM bwrespnsvs'
      'ORDER BY Nome')
    Left = 532
    Top = 8
    object QrBWRespnsvs1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWRespnsvs1Nome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsBWRespnsvs1: TDataSource
    DataSet = QrBWRespnsvs1
    Left = 532
    Top = 56
  end
  object QrBWRespnsvs2: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM bwrespnsvs'
      'ORDER BY Nome')
    Left = 620
    Top = 8
    object QrBWRespnsvs2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWRespnsvs2Nome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsBWRespnsvs2: TDataSource
    DataSet = QrBWRespnsvs2
    Left = 620
    Top = 56
  end
  object QrBWRespnsvs3: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM bwrespnsvs'
      'ORDER BY Nome')
    Left = 712
    Top = 4
    object QrBWRespnsvs3Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWRespnsvs3Nome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsBWRespnsvs3: TDataSource
    DataSet = QrBWRespnsvs3
    Left = 712
    Top = 52
  end
  object QrBWEmpresas: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM bwempresas'
      'ORDER BY Nome')
    Left = 792
    Top = 4
    object QrBWEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWEmpresasNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsBWEmpresas: TDataSource
    DataSet = QrBWEmpresas
    Left = 792
    Top = 52
  end
  object QrGraFabCad: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM grafabcad'
      'ORDER BY Nome')
    Left = 172
    Top = 252
    object QrGraFabCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraFabCadNome: TWideStringField
      DisplayWidth = 120
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object DsGraFabCad: TDataSource
    DataSet = QrGraFabCad
    Left = 172
    Top = 300
  end
  object QrGraFabMar: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SELECT DISTINCT mcd.Codigo, mcd.Nome NO_GraFabMar'
      'FROM grafabmar mar'
      'LEFT JOIN grafabcad cad ON cad.Codigo=mar.Codigo'
      'LEFT JOIN grafabmcd mcd ON mcd.Codigo=mar.GraFabMCd'
      'WHERE mar.Codigo>0'
      'ORDER BY NO_GraFabMar')
    Left = 240
    Top = 256
    object QrGraFabMarNO_GraFabMar: TWideStringField
      FieldName = 'NO_GraFabMar'
      Required = True
      Size = 100
    end
    object QrGraFabMarCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsGraFabMar: TDataSource
    DataSet = QrGraFabMar
    Left = 240
    Top = 304
  end
  object QrBWSegmento: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM bwsegmento'
      'ORDER BY Nome')
    Left = 320
    Top = 256
    object QrBWSegmentoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWSegmentoNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsBWSegmento: TDataSource
    DataSet = QrBWSegmento
    Left = 320
    Top = 304
  end
  object QrBWRepresnt: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM bwrepresnt'
      'ORDER BY Nome')
    Left = 404
    Top = 260
    object QrBWRepresntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWRepresntNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsBWRepresnt: TDataSource
    DataSet = QrBWRepresnt
    Left = 404
    Top = 308
  end
  object PMVenceMarca: TPopupMenu
    Left = 844
    Top = 282
    object Pelofabricante1: TMenuItem
      Caption = '&Pela Empresa dona da marca'
      OnClick = Pelofabricante1Click
    end
    object Diretodasmarcas1: TMenuItem
      Caption = '&Direto das marcas'
      OnClick = Diretodasmarcas1Click
    end
  end
end
