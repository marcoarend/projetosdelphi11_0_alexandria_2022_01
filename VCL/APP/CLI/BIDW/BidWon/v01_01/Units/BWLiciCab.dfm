object FmBWLiciCab: TFmBWLiciCab
  Left = 368
  Top = 194
  Caption = 'LIC-TACAO-001 :: Cadastro de Licita'#231#227'o'
  ClientHeight = 691
  ClientWidth = 1264
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1264
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1264
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 12
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label9: TLabel
        Left = 72
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object Label15: TLabel
        Left = 272
        Top = 16
        Width = 32
        Height = 13
        Caption = 'Cliente'
      end
      object Label16: TLabel
        Left = 644
        Top = 16
        Width = 45
        Height = 13
        Caption = 'Entidade:'
      end
      object Label17: TLabel
        Left = 700
        Top = 16
        Width = 56
        Height = 13
        Caption = 'Reabertura:'
      end
      object Label18: TLabel
        Left = 852
        Top = 16
        Width = 43
        Height = 13
        Caption = 'Abertura:'
      end
      object Label19: TLabel
        Left = 12
        Top = 56
        Width = 111
        Height = 13
        Caption = 'Data limite do cadastro:'
      end
      object Label20: TLabel
        Left = 168
        Top = 56
        Width = 17
        Height = 13
        Caption = 'UF:'
      end
      object Label21: TLabel
        Left = 200
        Top = 56
        Width = 37
        Height = 13
        Caption = 'Preg'#227'o:'
      end
      object Label24: TLabel
        Left = 404
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object SBCliente: TSpeedButton
        Left = 624
        Top = 32
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBClienteClick
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdBWStatus: TdmkEditCB
        Left = 72
        Top = 32
        Width = 41
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'BWStatus'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBBWStatus
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBBWStatus: TdmkDBLookupComboBox
        Left = 116
        Top = 32
        Width = 153
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsBWStatus
        TabOrder = 2
        dmkEditCB = EdBWStatus
        QryCampo = 'BWStatus'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPReabertuDt: TdmkEditDateTimePicker
        Left = 700
        Top = 32
        Width = 109
        Height = 21
        Date = 40656.000000000000000000
        Time = 40656.000000000000000000
        ShowCheckbox = True
        TabOrder = 6
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'ReabertuDt'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdReabertuHr: TdmkEdit
        Left = 812
        Top = 32
        Width = 36
        Height = 21
        TabOrder = 7
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'ReabertuHr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPAberturaDt: TdmkEditDateTimePicker
        Left = 852
        Top = 32
        Width = 109
        Height = 21
        Date = 40656.000000000000000000
        Time = 40656.000000000000000000
        ShowCheckbox = True
        TabOrder = 8
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'AberturaDt'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdAberturaHr: TdmkEdit
        Left = 964
        Top = 32
        Width = 36
        Height = 21
        TabOrder = 9
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'AberturaHr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPLimCadPpDt: TdmkEditDateTimePicker
        Left = 12
        Top = 72
        Width = 109
        Height = 21
        Date = 40656.000000000000000000
        Time = 40656.000000000000000000
        ShowCheckbox = True
        TabOrder = 10
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'LimCadPpDt'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdLimCadPpHr: TdmkEdit
        Left = 124
        Top = 72
        Width = 40
        Height = 21
        TabOrder = 11
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'LimCadPpHr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 272
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 328
        Top = 32
        Width = 297
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsBWClientes
        TabOrder = 4
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEntidade: TdmkEdit
        Left = 644
        Top = 32
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Entidade'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdUF: TdmkEdit
        Left = 168
        Top = 72
        Width = 29
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'UF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPregao: TdmkEdit
        Left = 200
        Top = 72
        Width = 201
        Height = 21
        TabOrder = 13
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Pregao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 404
        Top = 72
        Width = 596
        Height = 21
        TabOrder = 14
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 532
      Width = 1264
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1124
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1264
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 531
      Width = 1264
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 118
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 292
        Top = 15
        Width = 970
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 837
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 6
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Preg'#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCabClick
        end
        object BtLot: TBitBtn
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtLotClick
        end
        object BtIts: TBitBtn
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtStatus: TBitBtn
          Left = 500
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Status'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtStatusClick
        end
        object BtFila: TBitBtn
          Left = 624
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fila'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtFilaClick
        end
        object BtAtrLiciDef: TBitBtn
          Tag = 10099
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Atributos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtAtrLiciDefClick
        end
      end
    end
    object PnDadosDB: TPanel
      Left = 0
      Top = 0
      Width = 1005
      Height = 497
      Caption = 'PnDadosDB'
      TabOrder = 1
      object Splitter1: TSplitter
        Left = 1
        Top = 305
        Width = 1003
        Height = 5
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 149
        ExplicitWidth = 1235
      end
      object Splitter3: TSplitter
        Left = 1
        Top = 389
        Width = 1003
        Height = 5
        Cursor = crVSplit
        Align = alTop
      end
      object DBGIts: TDBGrid
        Left = 1
        Top = 310
        Width = 1003
        Height = 79
        Align = alTop
        DataSource = DsBWLiciIts
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Conta'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ItemLt'
            Title.Caption = 'Item'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Produto'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Produto'
            Title.Caption = 'Descri'#231#227'o do Produto'
            Width = 466
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SIGLA_UNIDADE'
            Title.Caption = 'Unidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtdeItem'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrUnit'
            Title.Caption = 'Valor Unit.'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValItem'
            Title.Caption = 'Valor Total'
            Width = 109
            Visible = True
          end>
      end
      object DBGLot: TDBGrid
        Left = 1
        Top = 157
        Width = 1003
        Height = 148
        Align = alTop
        DataSource = DsBWLiciLot
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Lote'
            Width = 29
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_BWPortal'
            Title.Caption = 'Portal'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NrProcesso'
            Title.Caption = 'N'#176' Processo'
            Width = 131
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Colocacao'
            Title.Caption = 'Coloca'#231#227'o'
            Width = 26
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Estah'
            Title.Caption = 'Est'#225
            Width = 26
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PrzAmostras'
            Title.Caption = 'Prazo amostras'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PrzEntregas'
            Title.Caption = 'Prazo entregas'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Responsav1'
            Title.Caption = 'Respons'#225'vel principal'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Empresa'
            Title.Caption = 'Empresa'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_VenceFabri'
            Title.Caption = 'Empresa vencedora'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'No_GraFabMCd'
            Title.Caption = 'Marca vencedora'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Segmento'
            Title.Caption = 'Segmento'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Representn'
            Title.Caption = 'Representante'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrPregao'
            Title.Caption = '$ Preg'#227'o'
            Width = 66
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrVenced'
            Title.Caption = '$ Vencedor'
            Width = 66
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Classifcac'
            Title.Caption = 'Classifca'#231#227'o'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtdeLote'
            Title.Caption = 'Qtd.Lote'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValLote'
            Title.Caption = '$ Lote'
            Width = 72
            Visible = True
          end>
      end
      object GBDados: TGroupBox
        Left = 1
        Top = 1
        Width = 1003
        Height = 156
        Align = alTop
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 564
          Top = 56
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label3: TLabel
          Left = 748
          Top = 16
          Width = 56
          Height = 13
          Caption = 'Reabertura:'
          FocusControl = DBEdit1
        end
        object Label5: TLabel
          Left = 876
          Top = 16
          Width = 43
          Height = 13
          Caption = 'Abertura:'
          FocusControl = DBEdit3
        end
        object Label8: TLabel
          Left = 288
          Top = 16
          Width = 32
          Height = 13
          Caption = 'Cliente'
          FocusControl = DBEdit5
        end
        object Label11: TLabel
          Left = 68
          Top = 16
          Width = 33
          Height = 13
          Caption = 'Status:'
          FocusControl = DBEdit7
        end
        object Label4: TLabel
          Left = 8
          Top = 56
          Width = 111
          Height = 13
          Caption = 'Data limite do cadastro:'
          FocusControl = DBEdit9
        end
        object Label6: TLabel
          Left = 136
          Top = 56
          Width = 17
          Height = 13
          Caption = 'UF:'
          FocusControl = DBEdit11
        end
        object Label10: TLabel
          Left = 168
          Top = 56
          Width = 37
          Height = 13
          Caption = 'Preg'#227'o:'
          FocusControl = DBEdit12
        end
        object Label12: TLabel
          Left = 384
          Top = 56
          Width = 66
          Height = 13
          Caption = 'Quantid. total:'
          FocusControl = DBEdit13
        end
        object Label13: TLabel
          Left = 468
          Top = 56
          Width = 50
          Height = 13
          Caption = 'Valor total:'
          FocusControl = DBEdit14
        end
        object Label14: TLabel
          Left = 692
          Top = 16
          Width = 45
          Height = 13
          Caption = 'Entidade:'
          FocusControl = DBEdit15
        end
        object dmkLabelRotate1: TdmkLabelRotate
          Left = 4
          Top = 96
          Width = 21
          Height = 57
          Angle = ag90
          Caption = 'Preg'#227'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Microsoft Sans Serif'
          Font.Style = []
        end
        object dmkLabelRotate2: TdmkLabelRotate
          Left = 504
          Top = 95
          Width = 21
          Height = 57
          Angle = ag90
          Caption = 'Lote:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Microsoft Sans Serif'
          Font.Style = []
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 8
          Top = 32
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsBWLiciCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 564
          Top = 72
          Width = 437
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsBWLiciCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 9
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit1: TDBEdit
          Left = 748
          Top = 32
          Width = 82
          Height = 21
          DataField = 'ReabertuDt_TXT'
          DataSource = DsBWLiciCab
          TabOrder = 5
        end
        object DBEdit2: TDBEdit
          Left = 832
          Top = 32
          Width = 40
          Height = 21
          DataField = 'ReabertuHr_TXT'
          DataSource = DsBWLiciCab
          TabOrder = 6
        end
        object DBEdit3: TDBEdit
          Left = 876
          Top = 32
          Width = 82
          Height = 21
          DataField = 'AberturaDt'
          DataSource = DsBWLiciCab
          TabOrder = 7
        end
        object DBEdit4: TDBEdit
          Left = 960
          Top = 32
          Width = 40
          Height = 21
          DataField = 'AberturaHr'
          DataSource = DsBWLiciCab
          TabOrder = 8
        end
        object DBEdit5: TDBEdit
          Left = 288
          Top = 32
          Width = 55
          Height = 21
          DataField = 'Cliente'
          DataSource = DsBWLiciCab
          TabOrder = 3
        end
        object DBEdit6: TDBEdit
          Left = 344
          Top = 32
          Width = 345
          Height = 21
          DataField = 'NO_Cliente'
          DataSource = DsBWLiciCab
          TabOrder = 4
        end
        object DBEdit7: TDBEdit
          Left = 68
          Top = 32
          Width = 35
          Height = 21
          DataField = 'BWStatus'
          DataSource = DsBWLiciCab
          TabOrder = 1
        end
        object DBEdit8: TDBEdit
          Left = 104
          Top = 32
          Width = 180
          Height = 21
          DataField = 'NO_BWStatus'
          DataSource = DsBWLiciCab
          TabOrder = 2
        end
        object DBEdit9: TDBEdit
          Left = 8
          Top = 72
          Width = 66
          Height = 21
          DataField = 'LimCadPpDt'
          DataSource = DsBWLiciCab
          TabOrder = 10
        end
        object DBEdit10: TDBEdit
          Left = 76
          Top = 72
          Width = 56
          Height = 21
          DataField = 'LimCadPpHr'
          DataSource = DsBWLiciCab
          TabOrder = 11
        end
        object DBEdit11: TDBEdit
          Left = 136
          Top = 72
          Width = 30
          Height = 21
          DataField = 'UF'
          DataSource = DsBWLiciCab
          TabOrder = 12
        end
        object DBEdit12: TDBEdit
          Left = 168
          Top = 72
          Width = 213
          Height = 21
          DataField = 'Pregao'
          DataSource = DsBWLiciCab
          TabOrder = 13
        end
        object DBEdit13: TDBEdit
          Left = 384
          Top = 72
          Width = 80
          Height = 21
          DataField = 'QtdeTotal'
          DataSource = DsBWLiciCab
          TabOrder = 14
        end
        object DBEdit14: TDBEdit
          Left = 468
          Top = 72
          Width = 92
          Height = 21
          DataField = 'ValTotal'
          DataSource = DsBWLiciCab
          TabOrder = 15
        end
        object DBEdit15: TDBEdit
          Left = 692
          Top = 32
          Width = 54
          Height = 21
          DataField = 'Entidade'
          DataSource = DsBWLiciCab
          TabOrder = 16
        end
        object DBMemo1: TDBMemo
          Left = 24
          Top = 96
          Width = 480
          Height = 57
          DataField = 'Historico'
          DataSource = DsBWLiHFCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          ParentFont = False
          TabOrder = 17
        end
        object DBMemo2: TDBMemo
          Left = 524
          Top = 95
          Width = 476
          Height = 57
          DataField = 'Historico'
          DataSource = DsBWLiHFLot
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          ParentFont = False
          TabOrder = 18
        end
      end
      object DBGAtr: TDBGrid
        Left = 1
        Top = 394
        Width = 1003
        Height = 102
        Align = alClient
        DataSource = DsAtrLiciDef
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CU_CAD'
            Title.Caption = 'C'#243'digo'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_CAD'
            Title.Caption = 'Atributo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CU_ITS'
            Title.Caption = 'C'#243'digo'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_ITS'
            Title.Caption = 'Descri'#231#227'o do item de atributo'
            Width = 600
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_Item'
            Title.Caption = 'ID do Item'
            Width = 56
            Visible = True
          end>
      end
    end
    object Panel7: TPanel
      Left = 1004
      Top = 0
      Width = 260
      Height = 531
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object Splitter2: TSplitter
        Left = 0
        Top = 209
        Width = 260
        Height = 5
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 120
      end
      object DBGHStat: TDBGrid
        Left = 0
        Top = 0
        Width = 260
        Height = 209
        Align = alTop
        DataSource = DsBWLiHStat
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'DataHora'
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_StatusAnt'
            Title.Caption = 'Status antigo'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_StatusAtu'
            Title.Caption = 'Novo status'
            Width = 70
            Visible = True
          end>
      end
      object DBGHEsta: TDBGrid
        Left = 0
        Top = 214
        Width = 260
        Height = 317
        Align = alClient
        DataSource = DsBWLiHEsta
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'DataHora'
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EstahAnt'
            Title.Caption = 'Pos.ant.'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EstahAtu'
            Title.Caption = 'Pos.Nova'
            Width = 52
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1216
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 261
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object SbEntiDocs: TBitBtn
        Tag = 160
        Left = 214
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 5
        OnClick = SbEntiDocsClick
      end
    end
    object GB_M: TGroupBox
      Left = 261
      Top = 0
      Width = 955
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 265
        Height = 32
        Caption = 'Cadastro de Licita'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 265
        Height = 32
        Caption = 'Cadastro de Licita'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 265
        Height = 32
        Caption = 'Cadastro de Licita'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1264
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1260
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrBWLiciCab: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrBWLiciCabBeforeOpen
    AfterOpen = QrBWLiciCabAfterOpen
    BeforeClose = QrBWLiciCabBeforeClose
    AfterScroll = QrBWLiciCabAfterScroll
    SQL.Strings = (
      'SELECT sta.Nome NO_BWStatus, cli.Nome NO_Cliente,'
      'cab.* '
      'FROM bwlicicab cab'
      'LEFT JOIN bwstatus sta ON sta.Codigo=cab.BWStatus'
      'LEFT JOIN bwclientes cli ON cli.Codigo=cab.Cliente'
      'WHERE cab.Codigo > 0')
    Left = 96
    Top = 233
    object QrBWLiciCabNO_BWStatus: TWideStringField
      FieldName = 'NO_BWStatus'
      Size = 60
    end
    object QrBWLiciCabNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 255
    end
    object QrBWLiciCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWLiciCabBWStatus: TIntegerField
      FieldName = 'BWStatus'
      Required = True
    end
    object QrBWLiciCabAberturaDt: TDateField
      FieldName = 'AberturaDt'
      Required = True
    end
    object QrBWLiciCabAberturaHr: TTimeField
      FieldName = 'AberturaHr'
      Required = True
      DisplayFormat = 'hh:nn'
    end
    object QrBWLiciCabReabertuDt: TDateField
      FieldName = 'ReabertuDt'
      Required = True
    end
    object QrBWLiciCabReabertuHr: TTimeField
      FieldName = 'ReabertuHr'
      Required = True
      DisplayFormat = 'hh:nn'
    end
    object QrBWLiciCabLimCadPpDt: TDateField
      FieldName = 'LimCadPpDt'
      Required = True
    end
    object QrBWLiciCabLimCadPpHr: TTimeField
      FieldName = 'LimCadPpHr'
      Required = True
      DisplayFormat = 'hh:nn'
    end
    object QrBWLiciCabEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrBWLiciCabCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrBWLiciCabUF: TWideStringField
      FieldName = 'UF'
      Required = True
      Size = 2
    end
    object QrBWLiciCabPregao: TWideStringField
      FieldName = 'Pregao'
      Size = 60
    end
    object QrBWLiciCabQtdeTotal: TFloatField
      FieldName = 'QtdeTotal'
      Required = True
    end
    object QrBWLiciCabValTotal: TFloatField
      FieldName = 'ValTotal'
      Required = True
    end
    object QrBWLiciCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 511
    end
    object QrBWLiciCabReabertuHr_TXT: TWideStringField
      FieldName = 'ReabertuHr_TXT'
      Size = 5
    end
    object QrBWLiciCabReabertuDt_TXT: TWideStringField
      FieldName = 'ReabertuDt_TXT'
      Size = 10
    end
  end
  object DsBWLiciCab: TDataSource
    DataSet = QrBWLiciCab
    Left = 92
    Top = 281
  end
  object QrBWLiciLot: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrBWLiciLotBeforeClose
    AfterScroll = QrBWLiciLotAfterScroll
    SQL.Strings = (
      'SELECT url.Nome NO_BWPortal, bem.Nome NO_Empresa,'
      'amo.Nome NO_PrzAmostras, amo.Nome NO_PrzEntregas,'
      're1.Nome NO_Responsav1, re2.Nome NO_Responsav2,'
      're3.Nome NO_Responsav3, gfd.Nome No_GraFabMCd,'
      'sgm.Nome NO_Segmento, rpr.Nome NO_Representn, '
      'LPAD(Lote, 11, '#39'0'#39') LoteORD, lot.*'
      'FROM bwlicilot lot'
      'LEFT JOIN bwportais   url ON url.Codigo=lot.BWPortal'
      'LEFT JOIN przentrgcab amo ON amo.Codigo=lot.PrzAmostras'
      'LEFT JOIN przentrgcab etr ON etr.Codigo=lot.PrzEntregas'
      'LEFT JOIN bwrespnsvs  re1 ON re1.Codigo=lot.Responsav1'
      'LEFT JOIN bwrespnsvs  re2 ON re2.Codigo=lot.Responsav2'
      'LEFT JOIN bwrespnsvs  re3 ON re3.Codigo=lot.Responsav3'
      'LEFT JOIN bwempresas  bem ON bem.Codigo=lot.Empresa'
      'LEFT JOIN grafabcad   gfc ON gfc.Codigo=lot.VenceFabri'
      'LEFT JOIN grafabmar   gfm ON gfm.Controle=lot.VenceMarca'
      'LEFT JOIN grafabmcd   gfd ON gfd.Codigo=gfm.GraFabMCd'
      'LEFT JOIN bwsegmento  sgm ON sgm.Codigo=lot.Segmento'
      'LEFT JOIN bwrepresnt  rpr ON rpr.Codigo=lot.Representn')
    Left = 188
    Top = 237
    object QrBWLiciLotNO_BWPortal: TWideStringField
      FieldName = 'NO_BWPortal'
      Size = 100
    end
    object QrBWLiciLotNO_PrzAmostras: TWideStringField
      FieldName = 'NO_PrzAmostras'
      Size = 120
    end
    object QrBWLiciLotNO_PrzEntregas: TWideStringField
      FieldName = 'NO_PrzEntregas'
      Size = 120
    end
    object QrBWLiciLotNO_Responsav1: TWideStringField
      FieldName = 'NO_Responsav1'
      Size = 60
    end
    object QrBWLiciLotNO_Responsav2: TWideStringField
      FieldName = 'NO_Responsav2'
      Size = 60
    end
    object QrBWLiciLotNO_Responsav3: TWideStringField
      FieldName = 'NO_Responsav3'
      Size = 60
    end
    object QrBWLiciLotNo_GraFabMCd: TWideStringField
      FieldName = 'No_GraFabMCd'
      Size = 60
    end
    object QrBWLiciLotNO_Segmento: TWideStringField
      FieldName = 'NO_Segmento'
      Size = 60
    end
    object QrBWLiciLotNO_Representn: TWideStringField
      FieldName = 'NO_Representn'
      Size = 60
    end
    object QrBWLiciLotCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWLiciLotControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBWLiciLotLote: TWideStringField
      FieldName = 'Lote'
      Size = 30
    end
    object QrBWLiciLotBWPortal: TIntegerField
      FieldName = 'BWPortal'
      Required = True
    end
    object QrBWLiciLotNrProcesso: TWideStringField
      FieldName = 'NrProcesso'
      Size = 60
    end
    object QrBWLiciLotColocacao: TIntegerField
      FieldName = 'Colocacao'
      Required = True
    end
    object QrBWLiciLotEstah: TIntegerField
      FieldName = 'Estah'
      Required = True
    end
    object QrBWLiciLotPrzAmostras: TIntegerField
      FieldName = 'PrzAmostras'
      Required = True
    end
    object QrBWLiciLotPrzEntregas: TIntegerField
      FieldName = 'PrzEntregas'
      Required = True
    end
    object QrBWLiciLotResponsav1: TIntegerField
      FieldName = 'Responsav1'
      Required = True
    end
    object QrBWLiciLotResponsav2: TIntegerField
      FieldName = 'Responsav2'
      Required = True
    end
    object QrBWLiciLotResponsav3: TIntegerField
      FieldName = 'Responsav3'
      Required = True
    end
    object QrBWLiciLotEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrBWLiciLotVenceFabri: TIntegerField
      FieldName = 'VenceFabri'
      Required = True
    end
    object QrBWLiciLotVenceMarca: TIntegerField
      FieldName = 'VenceMarca'
      Required = True
    end
    object QrBWLiciLotSegmento: TIntegerField
      FieldName = 'Segmento'
      Required = True
    end
    object QrBWLiciLotRepresentn: TIntegerField
      FieldName = 'Representn'
      Required = True
    end
    object QrBWLiciLotValrPregao: TFloatField
      FieldName = 'ValrPregao'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBWLiciLotValrVenced: TFloatField
      FieldName = 'ValrVenced'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBWLiciLotClassifcac: TIntegerField
      FieldName = 'Classifcac'
      Required = True
    end
    object QrBWLiciLotLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrBWLiciLotDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBWLiciLotDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBWLiciLotUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrBWLiciLotUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrBWLiciLotAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrBWLiciLotAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrBWLiciLotAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrBWLiciLotAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrBWLiciLotQtdeLote: TFloatField
      FieldName = 'QtdeLote'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrBWLiciLotValLote: TFloatField
      FieldName = 'ValLote'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBWLiciLotNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 60
    end
    object QrBWLiciLotNO_VenceFabri: TWideStringField
      FieldName = 'NO_VenceFabri'
      Size = 120
    end
    object QrBWLiciLotLoteORD: TWideStringField
      FieldName = 'LoteORD'
    end
    object QrBWLiciLotAmstrConvocDt: TDateField
      FieldName = 'AmstrConvocDt'
    end
    object QrBWLiciLotAmstrConvocHr: TTimeField
      FieldName = 'AmstrConvocHr'
    end
    object QrBWLiciLotAmstrLimEntrDt: TDateField
      FieldName = 'AmstrLimEntrDt'
    end
    object QrBWLiciLotAmstrLimEntrHr: TTimeField
      FieldName = 'AmstrLimEntrHr'
    end
    object QrBWLiciLotAmstrEntrReaDt: TDateField
      FieldName = 'AmstrEntrReaDt'
    end
    object QrBWLiciLotAmstrEntrReaHr: TTimeField
      FieldName = 'AmstrEntrReaHr'
    end
  end
  object DsBWLiciLot: TDataSource
    DataSet = QrBWLiciLot
    Left = 188
    Top = 281
  end
  object PMLot: TPopupMenu
    OnPopup = PMLotPopup
    Left = 464
    Top = 508
    object LotInclui1: TMenuItem
      Caption = '&Inclui novo lote'
      Enabled = False
      OnClick = LotInclui1Click
    end
    object LotAltera1: TMenuItem
      Caption = '&Altera o lote selecionado'
      Enabled = False
      OnClick = LotAltera1Click
    end
    object CConcluiLote1: TMenuItem
      Caption = '&Conclui o lote selecionado'
      OnClick = CConcluiLote1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object LotExclui1: TMenuItem
      Caption = '&Exclui o lote selecionado'
      Enabled = False
      OnClick = LotExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 344
    Top = 512
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object ConcluiCab1: TMenuItem
      Caption = '&Conclui'
      OnClick = ConcluiCab1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrBWStatus: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM bwstatus'
      'ORDER BY Nome')
    Left = 508
    Top = 8
    object QrBWStatusCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWStatusNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsBWStatus: TDataSource
    DataSet = QrBWStatus
    Left = 508
    Top = 56
  end
  object QrBWClientes: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM bwclientes'
      'ORDER BY Nome')
    Left = 580
    Top = 12
    object QrBWClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWClientesNome: TWideStringField
      DisplayWidth = 255
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
  end
  object DsBWClientes: TDataSource
    DataSet = QrBWClientes
    Left = 580
    Top = 60
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 588
    Top = 512
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona item'
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita item'
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove item'
      OnClick = ItsExclui1Click
    end
  end
  object QrBWLiciIts: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT prd.Nome NO_Produto, its.*'
      'FROM bwliciits its'
      'LEFT JOIN bwprodutos prd ON prd.Codigo=its.Produto'
      'WHERE its.Controle>0'
      'ORDER BY its.ItemLt, its.Controle')
    Left = 276
    Top = 237
    object QrBWLiciItsNO_Produto: TWideStringField
      FieldName = 'NO_Produto'
      Size = 255
    end
    object QrBWLiciItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWLiciItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBWLiciItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrBWLiciItsItemLt: TWideStringField
      FieldName = 'ItemLt'
      Required = True
      Size = 11
    end
    object QrBWLiciItsProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrBWLiciItsUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrBWLiciItsQtdeItem: TFloatField
      FieldName = 'QtdeItem'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrBWLiciItsValrUnit: TFloatField
      FieldName = 'ValrUnit'
      Required = True
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrBWLiciItsValItem: TFloatField
      FieldName = 'ValItem'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBWLiciItsLinha: TIntegerField
      FieldName = 'Linha'
      Required = True
    end
    object QrBWLiciItsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrBWLiciItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBWLiciItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBWLiciItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrBWLiciItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrBWLiciItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrBWLiciItsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrBWLiciItsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrBWLiciItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrBWLiciItsSIGLA_UNIDADE: TWideStringField
      FieldName = 'SIGLA_UNIDADE'
      Size = 6
    end
  end
  object DsBWLiciIts: TDataSource
    DataSet = QrBWLiciIts
    Left = 276
    Top = 281
  end
  object QrBWLiHStat: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT ant.Nome NO_StatusAnt,  '
      'atu.Nome NO_StatusAtu, lhs.*  '
      'FROM bwlihstat lhs '
      'LEFT JOIN bwstatus ant ON ant.Codigo=lhs.StatusAnt '
      'LEFT JOIN bwstatus atu ON atu.Codigo=lhs.StatusAtu '
      'WHERE lhs.Codigo>0 '
      'ORDER BY DataHora ')
    Left = 540
    Top = 237
    object QrBWLiHStatNO_StatusAnt: TWideStringField
      FieldName = 'NO_StatusAnt'
      Size = 60
    end
    object QrBWLiHStatNO_StatusAtu: TWideStringField
      FieldName = 'NO_StatusAtu'
      Size = 60
    end
    object QrBWLiHStatCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWLiHStatNivel4: TIntegerField
      FieldName = 'Nivel4'
      Required = True
    end
    object QrBWLiHStatDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrBWLiHStatStatusAnt: TIntegerField
      FieldName = 'StatusAnt'
      Required = True
    end
    object QrBWLiHStatStatusAtu: TIntegerField
      FieldName = 'StatusAtu'
      Required = True
    end
    object QrBWLiHStatNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrBWLiHStatHistorico: TWideMemoField
      FieldName = 'Historico'
      BlobType = ftWideMemo
    end
  end
  object DsBWLiHStat: TDataSource
    DataSet = QrBWLiHStat
    Left = 540
    Top = 281
  end
  object QrBWLiHEsta: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT lhe.*  '
      'FROM bwlihesta lhe '
      'WHERE lhe.Codigo>0 '
      'ORDER BY DataHora ')
    Left = 636
    Top = 237
    object QrBWLiHEstaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWLiHEstaControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBWLiHEstaNivel4: TIntegerField
      FieldName = 'Nivel4'
      Required = True
    end
    object QrBWLiHEstaDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrBWLiHEstaEstahAnt: TIntegerField
      FieldName = 'EstahAnt'
      Required = True
    end
    object QrBWLiHEstaEstahAtu: TIntegerField
      FieldName = 'EstahAtu'
      Required = True
    end
    object QrBWLiHEstaNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrBWLiHEstaHistorico: TWideMemoField
      FieldName = 'Historico'
      BlobType = ftWideMemo
    end
  end
  object DsBWLiHEsta: TDataSource
    DataSet = QrBWLiHEsta
    Left = 636
    Top = 285
  end
  object PMStatus: TPopupMenu
    OnPopup = PMStatusPopup
    Left = 712
    Top = 508
    object IncluiStatus1: TMenuItem
      Caption = '&Inclui novo status'
      OnClick = IncluiStatus1Click
    end
    object AlteraStatus1: TMenuItem
      Caption = '&Altera o status atual'
      OnClick = AlteraStatus1Click
    end
    object ExcluiStatus1: TMenuItem
      Caption = '&Exclui o status atual'
      OnClick = ExcluiStatus1Click
    end
  end
  object QrBWLiHFCab: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT hfc.*  '
      'FROM bwlihfcab hfc '
      'WHERE lhs.Codigo>0 ')
    Left = 360
    Top = 237
    object QrBWLiHFCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWLiHFCabHistorico: TWideMemoField
      FieldName = 'Historico'
      BlobType = ftWideMemo
    end
  end
  object DsBWLiHFCab: TDataSource
    DataSet = QrBWLiHFCab
    Left = 360
    Top = 281
  end
  object QrBWLiHFLot: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT ant.Nome NO_StatusAnt,  '
      'atu.Nome NO_StatusAtu, lhs.*  '
      'FROM bwlihstat lhs '
      'LEFT JOIN bwstatus ant ON ant.Codigo=lhs.StatusAnt '
      'LEFT JOIN bwstatus atu ON atu.Codigo=lhs.StatusAtu '
      'WHERE lhs.Codigo>0 '
      'ORDER BY DataHora ')
    Left = 452
    Top = 237
    object QrBWLiHFLotCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWLiHFLotControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBWLiHFLotHistorico: TWideMemoField
      FieldName = 'Historico'
      BlobType = ftWideMemo
    end
  end
  object DsBWLiHFLot: TDataSource
    DataSet = QrBWLiHFLot
    Left = 452
    Top = 281
  end
  object PMFila: TPopupMenu
    OnPopup = PMFilaPopup
    Left = 832
    Top = 504
    object IncluiFila1: TMenuItem
      Caption = '&Inclui nova posi'#231#227'o na fila'
      OnClick = IncluiFila1Click
    end
    object AlteraFila1: TMenuItem
      Caption = '&Altera a posi'#231#227'o selecionada da fila'
      OnClick = AlteraFila1Click
    end
    object ExcluiFila1: TMenuItem
      Caption = '&Exclui a posi'#231#227'o selecionada da fila'
      OnClick = ExcluiFila1Click
    end
  end
  object frxLIC_TACAO_001_01: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.407328206020000000
    ReportOptions.LastChange = 39949.407328206020000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxLIC_TACAO_001_01GetValue
    Left = 748
    Top = 284
    Datasets = <
      item
        DataSet = frxDsBWLiciCab
        DataSetName = 'frxDsBWLiciCab'
      end
      item
        DataSet = frxDsBWLiciIts
        DataSetName = 'frxDsBWLiciIts'
      end
      item
        DataSet = frxDsBWLiciLot
        DataSetName = 'frxDsBWLiciLot'
      end
      item
        DataSet = frxDsBWLiHEsta
        DataSetName = 'frxDsBWLiHEsta'
      end
      item
        DataSet = frxDsBWLiHFCab
        DataSetName = 'frxDsBWLiHFCab'
      end
      item
        DataSet = frxDsBWLiHFLot
        DataSetName = 'frxDsBWLiHFLot'
      end
      item
        DataSet = frxDsBWLiHStat
        DataSetName = 'frxDsBWLiHStat'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader5: TfrxPageHeader
        FillType = ftBrush
        Height = 120.944950240000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Top = 71.811070000000000000
          Width = 718.110236220000000000
          Height = 45.354360000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Shape4: TfrxShapeView
          Width = 718.110700000000000000
          Height = 45.354360000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo104: TfrxMemoView
          Left = 7.559060000000000000
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo105: TfrxMemoView
          Left = 170.078850000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Participa'#231#227'o de Licita'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo106: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 162.519790000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date] [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 162.519790000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          Top = 45.354360000000000000
          Width = 718.110700000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBWLiciCab."NO_Cliente"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 7.559060000000000000
          Top = 75.590600000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Status:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 52.913420000000000000
          Top = 75.590600000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciCab."NO_BWStatus"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 166.299320000000000000
          Top = 75.590600000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Abertura:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 226.771800000000000000
          Top = 75.590600000000000000
          Width = 71.811023620000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciCab."AberturaDt"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 340.157700000000000000
          Top = 75.590600000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Reabertura:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 415.748300000000000000
          Top = 75.590600000000000000
          Width = 71.811030940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciCab."ReabertuDt_TXT"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 529.134200000000000000
          Top = 75.590600000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Lim.Prop.:')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 600.945270000000000000
          Top = 75.590600000000000000
          Width = 71.811023620000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciCab."LimCadPpDt"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 7.559060000000000000
          Top = 98.267780000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'UF:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 34.015770000000000000
          Top = 98.267780000000000000
          Width = 30.236240000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciCab."UF"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 64.252010000000000000
          Top = 98.267780000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Preg'#227'o:')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 117.165430000000000000
          Top = 98.267780000000000000
          Width = 222.992270000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciCab."Pregao"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 343.937230000000000000
          Top = 98.267780000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Qtde. Total:')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 419.527830000000000000
          Top = 98.267780000000000000
          Width = 94.488210940000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciCab."QtdeTotal"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 514.016080000000000000
          Top = 98.267780000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ Total:')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 566.929500000000000000
          Top = 98.267780000000000000
          Width = 143.622100940000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBWLiciCab."ValTotal"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 298.582870000000000000
          Top = 75.590600000000000000
          Width = 41.574803150000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = 'hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciCab."AberturaHr"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 487.559370000000000000
          Top = 75.590600000000000000
          Width = 41.574803150000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciCab."ReabertuHr_TXT"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 672.756340000000000000
          Top = 75.590600000000000000
          Width = 41.574803150000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = 'hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciCab."LimCadPpHr"]')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Height = 45.354350240000000000
        Top = 200.315090000000000000
        Width = 718.110700000000000000
        DataSet = frxDsBWLiciLot
        DataSetName = 'frxDsBWLiciLot'
        RowCount = 0
        object Memo11: TfrxMemoView
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Top = 30.236240000000000000
          Width = 52.913383390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Processo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 64.252010000000000000
          Width = 26.456710000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Est'#225)
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 449.764070000000000000
          Width = 64.251970940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde. Lote')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 514.016080000000000000
          Width = 102.047270940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Lote')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 207.874150000000000000
          Top = 30.236240000000000000
          Width = 37.795270710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Portal:')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 30.236240000000000000
          Width = 34.015738270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Coloc.')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          Left = 90.708720000000000000
          Width = 60.472436060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Respons.')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          Left = 151.181200000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 204.094620000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencedor')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          Left = 279.685220000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 336.378170000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Segmento')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          Left = 393.071120000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Represent.')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          Top = 15.118120000000000000
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          DataField = 'Lote'
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBWLiciLot."Lote"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 245.669450000000000000
          Top = 30.236240000000000000
          Width = 321.260013390000000000
          Height = 15.118110240000000000
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciLot."NrProcesso"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 64.252010000000000000
          Top = 15.118120000000000000
          Width = 26.456710000000000000
          Height = 15.118110240000000000
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBWLiciLot."Estah"]'#176)
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 449.764070000000000000
          Top = 15.118120000000000000
          Width = 64.251970940000000000
          Height = 15.118110240000000000
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBWLiciLot."QtdeLote"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 514.016080000000000000
          Top = 15.118120000000000000
          Width = 102.047270940000000000
          Height = 15.118110240000000000
          DataField = 'ValLote'
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBWLiciLot."ValLote"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 52.913420000000000000
          Top = 30.236240000000000000
          Width = 154.960700710000000000
          Height = 15.118110240000000000
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciLot."NO_BWPortal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Left = 30.236240000000000000
          Top = 15.118120000000000000
          Width = 34.015738270000000000
          Height = 15.118110240000000000
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBWLiciLot."Colocacao"]'#176)
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          Left = 90.708720000000000000
          Top = 15.118120000000000000
          Width = 60.472436060000000000
          Height = 15.118110240000000000
          DataField = 'NO_Responsav1'
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciLot."NO_Responsav1"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 151.181200000000000000
          Top = 15.118120000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'NO_Empresa'
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciLot."NO_Empresa"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          Left = 204.094620000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'NO_VenceFabri'
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciLot."NO_VenceFabri"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          Left = 279.685220000000000000
          Top = 15.118120000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'No_GraFabMCd'
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciLot."No_GraFabMCd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          Left = 336.378170000000000000
          Top = 15.118120000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'NO_Segmento'
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciLot."NO_Segmento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          Left = 393.071120000000000000
          Top = 15.118120000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'NO_Representn'
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciLot."NO_Representn"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 616.063390000000000000
          Width = 102.047270940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Preg'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 616.063390000000000000
          Top = 15.118120000000000000
          Width = 102.047270940000000000
          Height = 15.118110240000000000
          DataField = 'ValrPregao'
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBWLiciLot."ValrPregao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 566.929500000000000000
          Top = 30.236240000000000000
          Width = 49.133850940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Venc.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 616.063390000000000000
          Top = 30.236240000000000000
          Width = 102.047270940000000000
          Height = 15.118110240000000000
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBWLiciLot."ValrVenced"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 907.087200000000000000
        Width = 718.110700000000000000
        object Memo120: TfrxMemoView
          Width = 718.110700000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 846.614720000000000000
        Width = 718.110700000000000000
        Stretched = True
        object Memo85: TfrxMemoView
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conclus'#227'o Geral da Licita'#231#227'o:')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiHFCab."Historico"]')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 39.685046690000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        DataSet = frxDsBWLiciIts
        DataSetName = 'frxDsBWLiciIts'
        RowCount = 0
        object Memo61: TfrxMemoView
          Width = 30.236210710000000000
          Height = 39.685046690000000000
          DataField = 'ItemLt'
          DataSet = frxDsBWLiciIts
          DataSetName = 'frxDsBWLiciIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBWLiciIts."ItemLt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 551.811380000000000000
          Width = 64.251970940000000000
          Height = 39.685039370078740000
          DataField = 'QtdeItem'
          DataSet = frxDsBWLiciIts
          DataSetName = 'frxDsBWLiciIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBWLiciIts."QtdeItem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 480.000310000000000000
          Width = 71.811023620000000000
          Height = 39.685039370078740000
          DataField = 'ValrUnit'
          DataSet = frxDsBWLiciIts
          DataSetName = 'frxDsBWLiciIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBWLiciIts."ValrUnit"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 30.236240000000000000
          Width = 393.071088270000000000
          Height = 39.685046690000000000
          DataSet = frxDsBWLiciIts
          DataSetName = 'frxDsBWLiciIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciIts."NO_Produto"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 423.307360000000000000
          Width = 56.692913390000000000
          Height = 39.685039370078740000
          DataField = 'SIGLA_UNIDADE'
          DataSet = frxDsBWLiciIts
          DataSetName = 'frxDsBWLiciIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciIts."SIGLA_UNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 616.063390000000000000
          Width = 102.047270940000000000
          Height = 39.685039370078740000
          DataField = 'ValItem'
          DataSet = frxDsBWLiciIts
          DataSetName = 'frxDsBWLiciIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBWLiciIts."ValItem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        PrintChildIfInvisible = True
        object Memo27: TfrxMemoView
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Item')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 551.811380000000000000
          Width = 64.251970940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde. Item')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 480.000310000000000000
          Width = 71.811023620000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Unit'#225'rio')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 30.236240000000000000
          Width = 393.071088270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          Left = 423.307360000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          Left = 616.063390000000000000
          Width = 102.047270940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Total item')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 370.393940000000000000
        Width = 718.110700000000000000
        PrintChildIfInvisible = True
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 457.323130000000000000
        Width = 718.110700000000000000
        Child = frxLIC_TACAO_001_01.ChHStat
        DataSet = frxDsBWLiHStat
        DataSetName = 'frxDsBWLiHStat'
        RowCount = 0
        object Memo37: TfrxMemoView
          Width = 90.708690710000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsBWLiHStat
          DataSetName = 'frxDsBWLiHStat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiHStat."DataHora"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          Left = 90.708720000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'NO_StatusAtu'
          DataSet = frxDsBWLiHStat
          DataSetName = 'frxDsBWLiHStat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiHStat."NO_StatusAtu"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          Left = 166.299320000000000000
          Width = 551.811343390000000000
          Height = 15.118110240000000000
          DataField = 'Nome'
          DataSet = frxDsBWLiHStat
          DataSetName = 'frxDsBWLiHStat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiHStat."Nome"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 396.850650000000000000
        Width = 718.110700000000000000
        object Memo28: TfrxMemoView
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hist'#243'rico de Altera'#231#245'es de Status da Licita'#231#227'o')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Top = 22.677180000000000000
          Width = 90.708690710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 90.708720000000000000
          Top = 22.677180000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Status')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          Left = 166.299320000000000000
          Top = 22.677180000000000000
          Width = 551.811343390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        Height = 7.559060000000000000
        Top = 540.472790000000000000
        Width = 718.110700000000000000
      end
      object ChHStat: TfrxChild
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 495.118430000000000000
        Width = 718.110700000000000000
        KeepChild = True
        Stretched = True
        object Memo66: TfrxMemoView
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiHStat."Historico"]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 631.181510000000000000
        Width = 718.110700000000000000
        DataSet = frxDsBWLiciLot
        DataSetName = 'frxDsBWLiciLot'
        RowCount = 0
        object Memo78: TfrxMemoView
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lote:')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          Left = 30.236240000000000000
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBWLiciLot."Lote"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo80: TfrxMemoView
          Left = 60.472480000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Est'#225':')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          Left = 90.708720000000000000
          Width = 26.456710000000000000
          Height = 15.118110240000000000
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBWLiciLot."Estah"]'#176)
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          Left = 117.165430000000000000
          Width = 52.913383390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Processo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          Left = 170.078850000000000000
          Width = 548.031820710000000000
          Height = 15.118110240000000000
          DataSet = frxDsBWLiciLot
          DataSetName = 'frxDsBWLiciLot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiciLot."NO_BWPortal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Header3: TfrxHeader
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 570.709030000000000000
        Width = 718.110700000000000000
        object Memo70: TfrxMemoView
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hist'#243'rico de Altera'#231#245'es de Posi'#231#227'o na Fila')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Top = 22.677180000000000000
          Width = 90.708690710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          Left = 90.708720000000000000
          Top = 22.677180000000000000
          Width = 45.354311180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Posi'#231#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          Left = 136.063080000000000000
          Top = 22.677180000000000000
          Width = 582.047583390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer3: TfrxFooter
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 668.976810000000000000
        Width = 718.110700000000000000
        Stretched = True
        object Memo84: TfrxMemoView
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiHFLot."Historico"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo87: TfrxMemoView
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conclus'#227'o relativa ao Lote [frxDsBWLiciLot."Lote"]')
          ParentFont = False
        end
      end
      object ChHEsta: TfrxChild
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 767.244590000000000000
        Width = 718.110700000000000000
        KeepChild = True
        Stretched = True
        object Memo77: TfrxMemoView
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiHEsta."Historico"]')
          ParentFont = False
        end
      end
      object DetailData2: TfrxDetailData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 729.449290000000000000
        Width = 718.110700000000000000
        Child = frxLIC_TACAO_001_01.ChHEsta
        DataSet = frxDsBWLiHEsta
        DataSetName = 'frxDsBWLiHEsta'
        RowCount = 0
        object Memo67: TfrxMemoView
          Width = 90.708690710000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsBWLiHEsta
          DataSetName = 'frxDsBWLiHEsta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiHEsta."DataHora"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          Left = 90.708720000000000000
          Width = 45.354311180000000000
          Height = 15.118110240000000000
          DataField = 'EstahAtu'
          DataSet = frxDsBWLiHEsta
          DataSetName = 'frxDsBWLiHEsta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiHEsta."EstahAtu"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          Left = 136.063080000000000000
          Width = 582.047583390000000000
          Height = 15.118110240000000000
          DataField = 'Nome'
          DataSet = frxDsBWLiHEsta
          DataSetName = 'frxDsBWLiHEsta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsBWLiHEsta."Nome"]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsBWLiciCab: TfrxDBDataset
    UserName = 'frxDsBWLiciCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_BWStatus=NO_BWStatus'
      'NO_Cliente=NO_Cliente'
      'Codigo=Codigo'
      'BWStatus=BWStatus'
      'AberturaDt=AberturaDt'
      'AberturaHr=AberturaHr'
      'ReabertuDt=ReabertuDt'
      'ReabertuHr=ReabertuHr'
      'LimCadPpDt=LimCadPpDt'
      'LimCadPpHr=LimCadPpHr'
      'Entidade=Entidade'
      'Cliente=Cliente'
      'UF=UF'
      'Pregao=Pregao'
      'QtdeTotal=QtdeTotal'
      'ValTotal=ValTotal'
      'Nome=Nome'
      'ReabertuHr_TXT=ReabertuHr_TXT'
      'ReabertuDt_TXT=ReabertuDt_TXT')
    DataSet = QrBWLiciCab
    BCDToCurrency = False
    Left = 92
    Top = 328
  end
  object frxDsBWLiciLot: TfrxDBDataset
    UserName = 'frxDsBWLiciLot'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_BWPortal=NO_BWPortal'
      'NO_PrzAmostras=NO_PrzAmostras'
      'NO_PrzEntregas=NO_PrzEntregas'
      'NO_Responsav1=NO_Responsav1'
      'NO_Responsav2=NO_Responsav2'
      'NO_Responsav3=NO_Responsav3'
      'No_GraFabMCd=No_GraFabMCd'
      'NO_Segmento=NO_Segmento'
      'NO_Representn=NO_Representn'
      'Codigo=Codigo'
      'Controle=Controle'
      'Lote=Lote'
      'BWPortal=BWPortal'
      'NrProcesso=NrProcesso'
      'Colocacao=Colocacao'
      'Estah=Estah'
      'PrzAmostras=PrzAmostras'
      'PrzEntregas=PrzEntregas'
      'Responsav1=Responsav1'
      'Responsav2=Responsav2'
      'Responsav3=Responsav3'
      'Empresa=Empresa'
      'VenceFabri=VenceFabri'
      'VenceMarca=VenceMarca'
      'Segmento=Segmento'
      'Representn=Representn'
      'ValrPregao=ValrPregao'
      'ValrVenced=ValrVenced'
      'Classifcac=Classifcac'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'QtdeLote=QtdeLote'
      'ValLote=ValLote'
      'NO_Empresa=NO_Empresa'
      'NO_VenceFabri=NO_VenceFabri')
    DataSet = QrBWLiciLot
    BCDToCurrency = False
    Left = 188
    Top = 332
  end
  object frxDsBWLiciIts: TfrxDBDataset
    UserName = 'frxDsBWLiciIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_Produto=NO_Produto'
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'ItemLt=ItemLt'
      'Produto=Produto'
      'Unidade=Unidade'
      'QtdeItem=QtdeItem'
      'ValrUnit=ValrUnit'
      'ValItem=ValItem'
      'Linha=Linha'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'SIGLA_UNIDADE=SIGLA_UNIDADE')
    DataSet = QrBWLiciIts
    BCDToCurrency = False
    Left = 276
    Top = 332
  end
  object frxDsBWLiHFCab: TfrxDBDataset
    UserName = 'frxDsBWLiHFCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Historico=Historico')
    DataSet = QrBWLiHFCab
    BCDToCurrency = False
    Left = 360
    Top = 332
  end
  object frxDsBWLiHFLot: TfrxDBDataset
    UserName = 'frxDsBWLiHFLot'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Historico=Historico')
    DataSet = QrBWLiHFLot
    BCDToCurrency = False
    Left = 452
    Top = 328
  end
  object frxDsBWLiHStat: TfrxDBDataset
    UserName = 'frxDsBWLiHStat'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_StatusAnt=NO_StatusAnt'
      'NO_StatusAtu=NO_StatusAtu'
      'Codigo=Codigo'
      'Nivel4=Nivel4'
      'DataHora=DataHora'
      'StatusAnt=StatusAnt'
      'StatusAtu=StatusAtu'
      'Nome=Nome'
      'Historico=Historico')
    DataSet = QrBWLiHStat
    BCDToCurrency = False
    Left = 540
    Top = 328
  end
  object frxDsBWLiHEsta: TfrxDBDataset
    UserName = 'frxDsBWLiHEsta'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Nivel4=Nivel4'
      'DataHora=DataHora'
      'EstahAnt=EstahAnt'
      'EstahAtu=EstahAtu'
      'Nome=Nome'
      'Historico=Historico')
    DataSet = QrBWLiHEsta
    BCDToCurrency = False
    Left = 640
    Top = 332
  end
  object QrAtrLiciDef: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      
        'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ' +
        'ATRITS,'
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,'
      'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp'
      'FROM atrlicidef def'
      'LEFT JOIN atrliciits its ON its.Controle=def.AtrIts'
      'LEFT JOIN atrlicicad cad ON cad.Codigo=def.AtrCad'
      'WHERE def.ID_Sorc>0'
      ''
      'UNION'
      ''
      'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS,'
      'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,'
      'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp'
      'FROM atrlicitxt def'
      'LEFT JOIN atrlicicad cad ON cad.Codigo=def.AtrCad'
      'WHERE def.ID_Sorc>0'
      ''
      'ORDER BY NO_CAD, NO_ITS')
    Left = 168
    Top = 464
    object QrAtrLiciDefID_Item: TIntegerField
      FieldName = 'ID_Item'
      Required = True
    end
    object QrAtrLiciDefID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
      Required = True
    end
    object QrAtrLiciDefAtrCad: TIntegerField
      FieldName = 'AtrCad'
      Required = True
    end
    object QrAtrLiciDefATRITS: TFloatField
      FieldName = 'ATRITS'
      Required = True
    end
    object QrAtrLiciDefCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
    end
    object QrAtrLiciDefCU_ITS: TLargeintField
      FieldName = 'CU_ITS'
    end
    object QrAtrLiciDefNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrAtrLiciDefNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 255
    end
    object QrAtrLiciDefAtrTyp: TSmallintField
      FieldName = 'AtrTyp'
    end
    object QrAtrLiciDefAtrTxt: TWideStringField
      FieldName = 'AtrTxt'
      Size = 255
    end
  end
  object DsAtrLiciDef: TDataSource
    DataSet = QrAtrLiciDef
    Left = 168
    Top = 516
  end
  object PMAtrLiciDef: TPopupMenu
    OnPopup = PMAtrLiciDefPopup
    Left = 736
    Top = 576
    object IncluiAtributo1: TMenuItem
      Caption = '&Inclui atributo'
      OnClick = IncluiAtributo1Click
    end
    object AlteraAtributo1: TMenuItem
      Caption = '&Altera atributo'
      OnClick = AlteraAtributo1Click
    end
    object ExcluiAtributo1: TMenuItem
      Caption = '&Exclui atributo'
      OnClick = ExcluiAtributo1Click
    end
  end
end
