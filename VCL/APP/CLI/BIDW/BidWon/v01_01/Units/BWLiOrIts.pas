unit BWLiOrIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmBWLiOrIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrBWLiOrFld: TMySQLQuery;
    QrBWLiOrFldCodigo: TIntegerField;
    QrBWLiOrFldNome: TWideStringField;
    DsBWLiOrFld: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label1: TLabel;
    EdCampo: TdmkEditCB;
    CBCampo: TdmkDBLookupComboBox;
    EdOrdem: TdmkEdit;
    Label2: TLabel;
    RGDirecao: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenBWLiOrCab(Controle: Integer);
  public
    { Public declarations }
    FCodigo, FOrdem: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmBWLiOrIts: TFmBWLiOrIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  BWLiOrCab;

{$R *.DFM}

procedure TFmBWLiOrIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Campo, Ordem, Direcao: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  Controle       := EdControle.ValueVariant;
  Campo          := EdCampo.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  Direcao        := RGDirecao.ItemIndex;
  //
  if MyObjects.FIC(Campo < 1, EdCampo, 'Informe uma o campo!') then
    Exit;
  if MyObjects.FIC(Ordem < 1, EdOrdem, 'Informe a ordem!') then
    Exit;
  if MyObjects.FIC(Direcao < 1, RGDirecao, 'Informe uma a dire��o!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('bwliorits', 'Controle', '', '', tsPos, SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwliorits', False, [
  'Codigo', 'Campo', 'Ordem',
  'Direcao'], [
  'Controle'], [
  Codigo, Campo, Ordem,
  Direcao], [
  Controle], True) then
  begin
    FmBWLiOrCab.ReordenaBWLiOrIts(Controle, FOrdem, Ordem);
    ReopenBWLiOrCab(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdCampo.ValueVariant     := 0;
      CBCampo.KeyValue         := Null;
      EdOrdem.ValueVariant     := EdOrdem.ValueVariant + 1;
      RGDirecao.ItemIndex      := 0;
      //
      EdCampo.SetFocus;
    end else Close;
  end;
end;

procedure TFmBWLiOrIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBWLiOrIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBWLiOrIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FOrdem := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrBWLiOrFld, Dmod.MyDB);
end;

procedure TFmBWLiOrIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBWLiOrIts.ReopenBWLiOrCab(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
