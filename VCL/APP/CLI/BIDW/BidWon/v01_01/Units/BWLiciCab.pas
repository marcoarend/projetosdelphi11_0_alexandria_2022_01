unit BWLiciCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, Vcl.ComCtrls,
  dmkEditDateTimePicker, dmkLabelRotate, frxClass, frxDBSet;

type
  TFmBWLiciCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    EdCodigo: TdmkEdit;
    QrBWLiciCab: TMySQLQuery;
    DsBWLiciCab: TDataSource;
    QrBWLiciLot: TMySQLQuery;
    DsBWLiciLot: TDataSource;
    PMLot: TPopupMenu;
    LotInclui1: TMenuItem;
    LotExclui1: TMenuItem;
    LotAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtLot: TBitBtn;
    QrBWLiciCabNO_BWStatus: TWideStringField;
    QrBWLiciCabNO_Cliente: TWideStringField;
    QrBWLiciCabCodigo: TIntegerField;
    QrBWLiciCabBWStatus: TIntegerField;
    QrBWLiciCabAberturaDt: TDateField;
    QrBWLiciCabAberturaHr: TTimeField;
    QrBWLiciCabReabertuDt: TDateField;
    QrBWLiciCabReabertuHr: TTimeField;
    QrBWLiciCabLimCadPpDt: TDateField;
    QrBWLiciCabLimCadPpHr: TTimeField;
    QrBWLiciCabEntidade: TIntegerField;
    QrBWLiciCabCliente: TIntegerField;
    QrBWLiciCabUF: TWideStringField;
    QrBWLiciCabPregao: TWideStringField;
    QrBWLiciCabQtdeTotal: TFloatField;
    QrBWLiciCabValTotal: TFloatField;
    QrBWLiciCabNome: TWideStringField;
    QrBWLiciLotNO_PrzAmostras: TWideStringField;
    QrBWLiciLotNO_PrzEntregas: TWideStringField;
    QrBWLiciLotNO_Responsav1: TWideStringField;
    QrBWLiciLotNO_Responsav2: TWideStringField;
    QrBWLiciLotNO_Responsav3: TWideStringField;
    QrBWLiciLotNo_GraFabMCd: TWideStringField;
    QrBWLiciLotNO_Segmento: TWideStringField;
    QrBWLiciLotNO_Representn: TWideStringField;
    QrBWLiciLotCodigo: TIntegerField;
    QrBWLiciLotControle: TIntegerField;
    QrBWLiciLotLote: TWideStringField;
    QrBWLiciLotBWPortal: TIntegerField;
    QrBWLiciLotNrProcesso: TWideStringField;
    QrBWLiciLotColocacao: TIntegerField;
    QrBWLiciLotEstah: TIntegerField;
    QrBWLiciLotPrzAmostras: TIntegerField;
    QrBWLiciLotPrzEntregas: TIntegerField;
    QrBWLiciLotResponsav1: TIntegerField;
    QrBWLiciLotResponsav2: TIntegerField;
    QrBWLiciLotResponsav3: TIntegerField;
    QrBWLiciLotEmpresa: TIntegerField;
    QrBWLiciLotVenceFabri: TIntegerField;
    QrBWLiciLotVenceMarca: TIntegerField;
    QrBWLiciLotSegmento: TIntegerField;
    QrBWLiciLotRepresentn: TIntegerField;
    QrBWLiciLotValrPregao: TFloatField;
    QrBWLiciLotValrVenced: TFloatField;
    QrBWLiciLotClassifcac: TIntegerField;
    QrBWLiciLotLk: TIntegerField;
    QrBWLiciLotDataCad: TDateField;
    QrBWLiciLotDataAlt: TDateField;
    QrBWLiciLotUserCad: TIntegerField;
    QrBWLiciLotUserAlt: TIntegerField;
    QrBWLiciLotAlterWeb: TSmallintField;
    QrBWLiciLotAWServerID: TIntegerField;
    QrBWLiciLotAWStatSinc: TSmallintField;
    QrBWLiciLotAtivo: TSmallintField;
    QrBWLiciLotQtdeLote: TFloatField;
    QrBWLiciLotValLote: TFloatField;
    Label7: TLabel;
    Label9: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label24: TLabel;
    EdBWStatus: TdmkEditCB;
    CBBWStatus: TdmkDBLookupComboBox;
    QrBWStatus: TMySQLQuery;
    QrBWStatusCodigo: TIntegerField;
    QrBWStatusNome: TWideStringField;
    DsBWStatus: TDataSource;
    QrBWClientes: TMySQLQuery;
    QrBWClientesCodigo: TIntegerField;
    QrBWClientesNome: TWideStringField;
    DsBWClientes: TDataSource;
    TPReabertuDt: TdmkEditDateTimePicker;
    EdReabertuHr: TdmkEdit;
    TPAberturaDt: TdmkEditDateTimePicker;
    EdAberturaHr: TdmkEdit;
    TPLimCadPpDt: TdmkEditDateTimePicker;
    EdLimCadPpHr: TdmkEdit;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdEntidade: TdmkEdit;
    EdUF: TdmkEdit;
    EdPregao: TdmkEdit;
    EdNome: TdmkEdit;
    QrBWLiciLotNO_BWPortal: TWideStringField;
    QrBWLiciLotNO_Empresa: TWideStringField;
    QrBWLiciLotNO_VenceFabri: TWideStringField;
    BtIts: TBitBtn;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    ItsExclui1: TMenuItem;
    QrBWLiciIts: TMySQLQuery;
    DsBWLiciIts: TDataSource;
    QrBWLiciItsNO_Produto: TWideStringField;
    QrBWLiciItsCodigo: TIntegerField;
    QrBWLiciItsControle: TIntegerField;
    QrBWLiciItsConta: TIntegerField;
    QrBWLiciItsItemLt: TWideStringField;
    QrBWLiciItsProduto: TIntegerField;
    QrBWLiciItsUnidade: TIntegerField;
    QrBWLiciItsQtdeItem: TFloatField;
    QrBWLiciItsValrUnit: TFloatField;
    QrBWLiciItsValItem: TFloatField;
    QrBWLiciItsLinha: TIntegerField;
    QrBWLiciItsLk: TIntegerField;
    QrBWLiciItsDataCad: TDateField;
    QrBWLiciItsDataAlt: TDateField;
    QrBWLiciItsUserCad: TIntegerField;
    QrBWLiciItsUserAlt: TIntegerField;
    QrBWLiciItsAlterWeb: TSmallintField;
    QrBWLiciItsAWServerID: TIntegerField;
    QrBWLiciItsAWStatSinc: TSmallintField;
    QrBWLiciItsAtivo: TSmallintField;
    QrBWLiciItsSIGLA_UNIDADE: TWideStringField;
    PnDadosDB: TPanel;
    DBGIts: TDBGrid;
    DBGLot: TDBGrid;
    Splitter1: TSplitter;
    GBDados: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Panel7: TPanel;
    QrBWLiHStat: TMySQLQuery;
    DsBWLiHStat: TDataSource;
    QrBWLiHEsta: TMySQLQuery;
    DsBWLiHEsta: TDataSource;
    DBGHStat: TDBGrid;
    DBGHEsta: TDBGrid;
    Splitter2: TSplitter;
    QrBWLiHStatNO_StatusAnt: TWideStringField;
    QrBWLiHStatNO_StatusAtu: TWideStringField;
    QrBWLiHStatCodigo: TIntegerField;
    QrBWLiHStatNivel4: TIntegerField;
    QrBWLiHStatDataHora: TDateTimeField;
    QrBWLiHStatStatusAnt: TIntegerField;
    QrBWLiHStatStatusAtu: TIntegerField;
    QrBWLiHEstaCodigo: TIntegerField;
    QrBWLiHEstaControle: TIntegerField;
    QrBWLiHEstaNivel4: TIntegerField;
    QrBWLiHEstaDataHora: TDateTimeField;
    QrBWLiHEstaEstahAnt: TIntegerField;
    QrBWLiHEstaEstahAtu: TIntegerField;
    BtStatus: TBitBtn;
    BtFila: TBitBtn;
    PMStatus: TPopupMenu;
    IncluiStatus1: TMenuItem;
    AlteraStatus1: TMenuItem;
    ExcluiStatus1: TMenuItem;
    QrBWLiHStatNome: TWideStringField;
    QrBWLiHStatHistorico: TWideMemoField;
    N1: TMenuItem;
    ConcluiCab1: TMenuItem;
    QrBWLiHFCab: TMySQLQuery;
    DsBWLiHFCab: TDataSource;
    DBMemo1: TDBMemo;
    dmkLabelRotate1: TdmkLabelRotate;
    QrBWLiHFCabCodigo: TIntegerField;
    QrBWLiHFCabHistorico: TWideMemoField;
    QrBWLiHFLot: TMySQLQuery;
    QrBWLiHFLotCodigo: TIntegerField;
    QrBWLiHFLotControle: TIntegerField;
    QrBWLiHFLotHistorico: TWideMemoField;
    DsBWLiHFLot: TDataSource;
    N2: TMenuItem;
    CConcluiLote1: TMenuItem;
    dmkLabelRotate2: TdmkLabelRotate;
    DBMemo2: TDBMemo;
    PMFila: TPopupMenu;
    IncluiFila1: TMenuItem;
    AlteraFila1: TMenuItem;
    ExcluiFila1: TMenuItem;
    QrBWLiHEstaNome: TWideStringField;
    QrBWLiHEstaHistorico: TWideMemoField;
    frxLIC_TACAO_001_01: TfrxReport;
    frxDsBWLiciCab: TfrxDBDataset;
    frxDsBWLiciLot: TfrxDBDataset;
    frxDsBWLiciIts: TfrxDBDataset;
    frxDsBWLiHFCab: TfrxDBDataset;
    frxDsBWLiHFLot: TfrxDBDataset;
    frxDsBWLiHStat: TfrxDBDataset;
    frxDsBWLiHEsta: TfrxDBDataset;
    QrBWLiciCabReabertuDt_TXT: TWideStringField;
    QrBWLiciCabReabertuHr_TXT: TWideStringField;
    QrBWLiciLotLoteORD: TWideStringField;
    SbEntiDocs: TBitBtn;
    Splitter3: TSplitter;
    DBGAtr: TDBGrid;
    QrAtrLiciDef: TMySQLQuery;
    DsAtrLiciDef: TDataSource;
    BtAtrLiciDef: TBitBtn;
    PMAtrLiciDef: TPopupMenu;
    IncluiAtributo1: TMenuItem;
    AlteraAtributo1: TMenuItem;
    ExcluiAtributo1: TMenuItem;
    QrAtrLiciDefID_Item: TIntegerField;
    QrAtrLiciDefID_Sorc: TIntegerField;
    QrAtrLiciDefAtrCad: TIntegerField;
    QrAtrLiciDefATRITS: TFloatField;
    QrAtrLiciDefCU_CAD: TIntegerField;
    QrAtrLiciDefCU_ITS: TLargeintField;
    QrAtrLiciDefNO_CAD: TWideStringField;
    QrAtrLiciDefNO_ITS: TWideStringField;
    QrAtrLiciDefAtrTyp: TSmallintField;
    QrAtrLiciDefAtrTxt: TWideStringField;
    SBCliente: TSpeedButton;
    QrBWLiciLotAmstrConvocDt: TDateField;
    QrBWLiciLotAmstrConvocHr: TTimeField;
    QrBWLiciLotAmstrLimEntrDt: TDateField;
    QrBWLiciLotAmstrLimEntrHr: TTimeField;
    QrBWLiciLotAmstrEntrReaDt: TDateField;
    QrBWLiciLotAmstrEntrReaHr: TTimeField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrBWLiciCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrBWLiciCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrBWLiciCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtLotClick(Sender: TObject);
    procedure LotInclui1Click(Sender: TObject);
    procedure LotExclui1Click(Sender: TObject);
    procedure LotAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMLotPopup(Sender: TObject);
    procedure QrBWLiciCabBeforeClose(DataSet: TDataSet);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure QrBWLiciLotBeforeClose(DataSet: TDataSet);
    procedure QrBWLiciLotAfterScroll(DataSet: TDataSet);
    procedure ItsAltera1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure IncluiStatus1Click(Sender: TObject);
    procedure AlteraStatus1Click(Sender: TObject);
    procedure ExcluiStatus1Click(Sender: TObject);
    procedure PMStatusPopup(Sender: TObject);
    procedure BtStatusClick(Sender: TObject);
    procedure ConcluiCab1Click(Sender: TObject);
    procedure CConcluiLote1Click(Sender: TObject);
    procedure IncluiFila1Click(Sender: TObject);
    procedure AlteraFila1Click(Sender: TObject);
    procedure ExcluiFila1Click(Sender: TObject);
    procedure PMFilaPopup(Sender: TObject);
    procedure BtFilaClick(Sender: TObject);
    procedure frxLIC_TACAO_001_01GetValue(const VarName: string; var Value: Variant);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbEntiDocsClick(Sender: TObject);
    procedure ExcluiAtributo1Click(Sender: TObject);
    procedure PMAtrLiciDefPopup(Sender: TObject);
    procedure IncluiAtributo1Click(Sender: TObject);
    procedure AlteraAtributo1Click(Sender: TObject);
    procedure BtAtrLiciDefClick(Sender: TObject);
    procedure SBClienteClick(Sender: TObject);
  private
    FStatusAnt: Integer;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormAtrLiciDef(SQLType: TSQLType);
    procedure MostraBWLiciLot(SQLType: TSQLType);
    procedure MostraBWLiciIts(SQLType: TSQLType);
    procedure InsereBWLiHStat(Codigo, StatusAtu: Integer);
    procedure ReopenAtrLiciDef(ID_Item: Integer);
    procedure ReopenBWLiHEsta(Nivel4: Integer);
    procedure ReopenBWLiHStat(Nivel4: Integer);
    procedure ReopenBWLiHFCab();
    procedure ReopenBWLiHFlot();

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    // FApenasBWLiciCab, FApenasBWLiciLot
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure MostraFormBWLiHEsta(SQLTYpe: TSQLType);
    procedure MostraFormBWLiHStat(SQLTYpe: TSQLType);
    procedure ReopenBWLiciLot(Controle: Integer);
    procedure ReopenBWLiciIts(Conta: Integer);

  end;

var
  FmBWLiciCab: TFmBWLiciCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, BWLiciLot, BWLiciIts, UnApp_PF,
  ModuleGeral, BWLiHStat, BWLiHEsta, GetTexto, UnEntities, CfgAtributos,
  UnApp_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmBWLiciCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmBWLiciCab.MostraBWLiciIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmBWLiciIts, FmBWLiciIts, afmoNegarComAviso) then
  begin
    FmBWLiciIts.ImgTipo.SQLType := SQLType;
    FmBWLiciIts.FCodigo   := QrBWLiciCabCodigo.Value;
    FmBWLiciIts.FControle := QrBWLiciLotControle.Value;
    //
    FmBWLiciIts.FDsCab := DsBWLiciCab;
    FmBWLiciIts.FDsLot := DsBWLiciLot;
    FmBWLiciIts.FQrCab := QrBWLiciCab;
    FmBWLiciIts.FQrLot := QrBWLiciLot;
    FmBWLiciIts.FQrIts := QrBWLiciIts;
    if SQLType = stIns then
      //
    else
    begin
      FmBWLiciIts.EdConta.ValueVariant       := QrBWLiciItsConta.Value;
      //
      FmBWLiciIts.EdLinha.ValueVariant       := QrBWLiciItsLinha.Value;
      FmBWLiciIts.EdItemLt.ValueVariant      := QrBWLiciItsItemLt.Value;
      FmBWLiciIts.EdProduto.ValueVariant     := QrBWLiciItsProduto.Value;
      FmBWLiciIts.CBProduto.KeyValue         := QrBWLiciItsProduto.Value;
      FmBWLiciIts.EdUnidade.ValueVariant     := QrBWLiciItsUnidade.Value;
      FmBWLiciIts.CBUnidade.KeyValue         := QrBWLiciItsUnidade.Value;
      FmBWLiciIts.EdQtdeItem.ValueVariant    := QrBWLiciItsQtdeItem.Value;
      FmBWLiciIts.EdValrUnit.ValueVariant    := QrBWLiciItsValrUnit.Value;
      FmBWLiciIts.EdValItem.ValueVariant     := QrBWLiciItsValItem.Value;
      //
    end;
    FmBWLiciIts.ShowModal;
    FmBWLiciIts.Destroy;
  end;
end;

procedure TFmBWLiciCab.MostraBWLiciLot(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmBWLiciLot, FmBWLiciLot, afmoNegarComAviso) then
  begin
    FmBWLiciLot.ImgTipo.SQLType := SQLType;
    FmBWLiciLot.FCodigo := QrBWLiciCabCodigo.Value;
    FmBWLiciLot.FEstahAnt := QrBWLiciLotEstah.Value;
    //
    FmBWLiciLot.FQrCab := QrBWLiciCab;
    FmBWLiciLot.FDsCab := DsBWLiciCab;
    FmBWLiciLot.FQrIts := QrBWLiciLot;
    if SQLType = stIns then
      //
    else
    begin
      FmBWLiciLot.EdControle.ValueVariant    := QrBWLiciLotControle.Value;
      //
      FmBWLiciLot.EdLote.ValueVariant        := QrBWLiciLotLote.Value;
      FmBWLiciLot.EdBWPortal.ValueVariant    := QrBWLiciLotBWPortal.Value;
      FmBWLiciLot.CBBWPortal.KeyValue        := QrBWLiciLotBWPortal.Value;
      FmBWLiciLot.EdNrProcesso.ValueVariant  := QrBWLiciLotNrProcesso.Value;
      FmBWLiciLot.EdColocacao.ValueVariant   := QrBWLiciLotColocacao.Value;
      FmBWLiciLot.EdEstah.ValueVariant       := QrBWLiciLotEstah.Value;
      FmBWLiciLot.EdPrzAmostras.ValueVariant := QrBWLiciLotPrzAmostras.Value;
      FmBWLiciLot.CBPrzAmostras.KeyValue     := QrBWLiciLotPrzAmostras.Value;
      FmBWLiciLot.EdPrzEntregas.ValueVariant := QrBWLiciLotPrzEntregas.Value;
      FmBWLiciLot.CBPrzEntregas.KeyValue     := QrBWLiciLotPrzEntregas.Value;
      FmBWLiciLot.EdResponsav1.ValueVariant  := QrBWLiciLotResponsav1.Value;
      FmBWLiciLot.CBResponsav1.KeyValue      := QrBWLiciLotResponsav1.Value;
      FmBWLiciLot.EdResponsav2.ValueVariant  := QrBWLiciLotResponsav2.Value;
      FmBWLiciLot.CBResponsav2.KeyValue      := QrBWLiciLotResponsav2.Value;
      FmBWLiciLot.EdResponsav3.ValueVariant  := QrBWLiciLotResponsav3.Value;
      FmBWLiciLot.CBResponsav3.KeyValue      := QrBWLiciLotResponsav3.Value;
      FmBWLiciLot.EdEmpresa.ValueVariant     := QrBWLiciLotEmpresa.Value;
      FmBWLiciLot.CBEmpresa.KeyValue         := QrBWLiciLotEmpresa.Value;
      FmBWLiciLot.EdVenceFabri.ValueVariant  := QrBWLiciLotVenceFabri.Value;
      FmBWLiciLot.CBVenceFabri.KeyValue      := QrBWLiciLotVenceFabri.Value;
      //
      if QrBWLiciLotVenceMarca.Value <> 0 then
      begin
        FmBWLiciLot.ReopenGraFabMar(TFiltroPesquisaSQL.fpsTodos);
(*
        if FmBWLiciLot.QrGraFabMar.Locate('Codigo', QrBWLiciLotVenceMarca.Value, []) then
        begin
          FmBWLiciLot.EdVenceMarca.ValueVariant  := FmBWLiciLot.QrGraFabMarControle.Value;
          FmBWLiciLot.CBVenceMarca.KeyValue      := FmBWLiciLot.QrGraFabMarControle.Value;
        end;
*)
        FmBWLiciLot.EdVenceMarca.ValueVariant   := QrBWLiciLotVenceMarca.Value;
        FmBWLiciLot.CBVenceMarca.KeyValue       := QrBWLiciLotVenceMarca.Value;
      end;
      FmBWLiciLot.EdSegmento.ValueVariant       := QrBWLiciLotSegmento.Value;
      FmBWLiciLot.CBSegmento.KeyValue           := QrBWLiciLotSegmento.Value;
      FmBWLiciLot.EdRepresentn.ValueVariant     := QrBWLiciLotRepresentn.Value;
      FmBWLiciLot.CBRepresentn.KeyValue         := QrBWLiciLotRepresentn.Value;
      FmBWLiciLot.EdValrPregao.ValueVariant     := QrBWLiciLotValrPregao.Value;
      FmBWLiciLot.EdValrVenced.ValueVariant     := QrBWLiciLotValrVenced.Value;
      FmBWLiciLot.EdClassifcac.ValueVariant     := QrBWLiciLotClassifcac.Value;
      //
      FmBWLiciLot.TPAmstrConvocDt.Date          := QrBWLiciLotAmstrConvocDt.Value;
      FmBWLiciLot.EdAmstrConvocHr.ValueVariant  := QrBWLiciLotAmstrConvocHr.Value;
      FmBWLiciLot.TPAmstrLimEntrDt.Date         := QrBWLiciLotAmstrLimEntrDt.Value;
      FmBWLiciLot.EDAmstrLimEntrHr.ValueVariant := QrBWLiciLotAmstrLimEntrHr.Value;
      FmBWLiciLot.TPAmstrEntrReaDt.Date         := QrBWLiciLotAmstrEntrReaDt.Value;
      FmBWLiciLot.EdAmstrEntrReaHr.ValueVariant := QrBWLiciLotAmstrEntrReaHr.Value;
    end;
    FmBWLiciLot.ShowModal;
    FmBWLiciLot.Destroy;
  end;
end;

procedure TFmBWLiciCab.MostraFormAtrLiciDef(SQLType: TSQLType);
begin
  UnCfgAtributos.InsAltAtrDef_ITS(QrBWLiciCabCodigo.Value,
    QrAtrliciDefID_Item.Value, 'atrlicidef', 'atrlicicad', 'atrliciits',
    'atrlicitxt', SQLType,  QrAtrliciDefAtrCad.Value,
    Trunc(QrAtrLiciDefAtrIts.Value), QrAtrliciDefAtrTxt.Value,
    QrAtrliciDef, QrAtrliciDef, True);
end;

procedure TFmBWLiciCab.MostraFormBWLiHEsta(SQLTYpe: TSQLType);
var
  Agora: TDateTime;
begin
  if DBCheck.CriaFm(TFmBWLiHEsta, FmBWLiHEsta, afmoNegarComAviso) then
  begin
    FmBWLiHEsta.ImgTipo.SQLType := SQLType;
    FmBWLiHEsta.FCodigo   := QrBWLiciCabCodigo.Value;
    FmBWLiHEsta.FControle := QrBWLiciLotControle.Value;
    FmBWLiHEsta.FEstahAnt := QrBWLiciLotEstah.Value;
    //
    FmBWLiHEsta.FQrCab := QrBWLiciCab;
    FmBWLiHEsta.FDsCab := DsBWLiciCab;
    FmBWLiHEsta.FQrIts := QrBWLiHEsta;
    if SQLType = stIns then
    begin
      Agora := DMOd.ObtemAgora();
      FmBWLiHEsta.TPDataHora.Date := Agora;
      FmBWLiHEsta.EdDataHora.ValueVariant := Agora;
    end else
    begin
      FmBWLiHEsta.FCodigo                  := QrBWLiciCabCodigo.Value;
      FmBWLiHEsta.EdNivel4.ValueVariant    := QrBWLiHEstaNivel4.Value;
      //
      FmBWLiHEsta.TPDataHora.Date          := QrBWLiHEstaDataHora.Value;
      FmBWLiHEsta.EdDataHora.ValueVariant  := QrBWLiHEstaDataHora.Value;
      FmBWLiHEsta.EdEstahAnt.ValueVariant  := QrBWLiHEstaEstahAnt.Value;
      FmBWLiHEsta.EdEstahAtu.ValueVariant  := QrBWLiHEstaEstahAtu.Value;
      FmBWLiHEsta.EdNome.ValueVariant      := QrBWLiHEstaNome.Value;
      FmBWLiHEsta.MeHistorico.Text         := QrBWLiHEstaHistorico.Value;
    end;
    FmBWLiHEsta.ShowModal;
    FmBWLiHEsta.Destroy;
  end;
end;

procedure TFmBWLiciCab.MostraFormBWLiHStat(SQLTYpe: TSQLType);
var
  Agora: TDateTime;
begin
  if DBCheck.CriaFm(TFmBWLiHStat, FmBWLiHStat, afmoNegarComAviso) then
  begin
    FmBWLiHStat.ImgTipo.SQLType := SQLType;
    FmBWLiHStat.FCodigo := QrBWLiciCabCodigo.Value;
    FmBWLiHStat.FStatusAnt := QrBWLiciLotEstah.Value;
    //
    FmBWLiHStat.FQrCab := QrBWLiciCab;
    FmBWLiHStat.FDsCab := DsBWLiciCab;
    FmBWLiHStat.FQrIts := QrBWLiHStat;
    if SQLType = stIns then
    begin
      Agora := DMOd.ObtemAgora();
      FmBWLiHStat.TPDataHora.Date := Agora;
      FmBWLiHStat.EdDataHora.ValueVariant := Agora;
    end else
    begin
      FmBWLiHStat.FCodigo                  := QrBWLiciCabCodigo.Value;
      FmBWLiHStat.EdNivel4.ValueVariant    := QrBWLiHStatNivel4.Value;
      //
      FmBWLiHStat.TPDataHora.Date          := QrBWLiHStatDataHora.Value;
      FmBWLiHStat.EdDataHora.ValueVariant  := QrBWLiHStatDataHora.Value;
      FmBWLiHStat.EdStatusAnt.ValueVariant := QrBWLiHStatStatusAnt.Value;
      FmBWLiHStat.CBStatusAnt.KeyValue     := QrBWLiHStatStatusAnt.Value;
      FmBWLiHStat.EdStatusAtu.ValueVariant := QrBWLiHStatStatusAtu.Value;
      FmBWLiHStat.CBStatusAtu.KeyValue     := QrBWLiHStatStatusAtu.Value;
      FmBWLiHStat.EdNome.ValueVariant      := QrBWLiHStatNome.Value;
      FmBWLiHStat.MeHistorico.Text         := QrBWLiHStatHistorico.Value;
    end;
    FmBWLiHStat.ShowModal;
    FmBWLiHStat.Destroy;
  end;
end;

procedure TFmBWLiciCab.PMAtrLiciDefPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiAtributo1, QrBWLiciCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtributo1, QrAtrLiciDef);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtributo1, QrAtrLiciDef);
end;

procedure TFmBWLiciCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrBWLiciCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrBWLiciCab, QrBWLiciLot);
end;

procedure TFmBWLiciCab.PMFilaPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiFila1, QrBWLiciLot);
  MyObjects.HabilitaMenuItemItsUpd(AlteraFila1, QrBWLiHEsta);
  MyObjects.HabilitaMenuItemItsDel(ExcluiFila1, QrBWLiHEsta);
end;

procedure TFmBWLiciCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrBWLiciLot);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrBWLiciIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrBWLiciIts);
end;

procedure TFmBWLiciCab.PMLotPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(LotInclui1, QrBWLiciCab);
  MyObjects.HabilitaMenuItemItsUpd(LotAltera1, QrBWLiciLot);
  MyObjects.HabilitaMenuItemcabDel(LotExclui1, QrBWLiciLot, QrBWLiciIts);
end;

procedure TFmBWLiciCab.PMStatusPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiStatus1, QrBWLiciCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraStatus1, QrBWLiHStat);
  MyObjects.HabilitaMenuItemItsDel(ExcluiStatus1, QrBWLiHStat);
end;

procedure TFmBWLiciCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrBWLiciCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmBWLiciCab.DefParams;
begin
  VAR_GOTOTABELA := 'bwlicicab';
  VAR_GOTOMYSQLTABLE := QrBWLiciCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT sta.Nome NO_BWStatus, cli.Nome NO_Cliente,');
  VAR_SQLx.Add('IF(cab.ReabertuDt < "1900-01-01", "", ');
  VAR_SQLx.Add('DATE_FORMAT(cab.ReabertuDt, "%d/%m/%Y")) ReabertuDt_TXT, ');
  VAR_SQLx.Add('IF(cab.ReabertuDt < "1900-01-01", "", ');
  VAR_SQLx.Add('DATE_FORMAT(cab.ReabertuHr, "%H:%i")) ReabertuHr_TXT, ');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('cab.* ');
  VAR_SQLx.Add('FROM bwlicicab cab');
  VAR_SQLx.Add('LEFT JOIN bwstatus sta ON sta.Codigo=cab.BWStatus');
  VAR_SQLx.Add('LEFT JOIN bwclientes cli ON cli.Codigo=cab.Cliente');
  VAR_SQLx.Add('WHERE cab.Codigo > 0');
  //
  VAR_SQL1.Add('AND cab.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND cab.Nome Like :P0');
  //
end;

procedure TFmBWLiciCab.ExcluiAtributo1Click(Sender: TObject);
var
  ID_Item: Integer;
  Tabela: String;
begin
  case TTypTabAtr(QrAtrLiciDefAtrTyp.Value) of
    ttaPreDef: Tabela := 'atrlicidef';
    ttaTxtFree: Tabela := 'atrlicitxt';
    else Tabela := '????';
  end;
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do atributo selecionado?',
  Tabela, 'ID_Item', QrAtrliciDefID_Item.Value, Dmod.MyDB) = ID_YES then
  begin
    ID_Item := GOTOy.LocalizaPriorNextIntQr(QrAtrLiciDef, QrAtrliciDefID_Item,
    QrAtrliciDefID_Item.Value);
    ReopenAtrliciDef(ID_Item);
  end;
end;

procedure TFmBWLiciCab.ExcluiFila1Click(Sender: TObject);
var
  Nivel4: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item de Posi��o na Fila selecionado?',
  'BWLiHEsta', 'Nivel4', QrBWLiHEstaNivel4.Value, Dmod.MyDB) = ID_YES then
  begin
    Nivel4 := GOTOy.LocalizaPriorNextIntQr(QrBWLiHEsta,
      QrBWLiHEstaNivel4, QrBWLiHEstaNivel4.Value);
    ReopenBWLiciLot(Nivel4);
  end;
end;

procedure TFmBWLiciCab.ExcluiStatus1Click(Sender: TObject);
var
  Nivel4: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item de Hist�rico de Status selecionado?',
  'BWLiHStat', 'Nivel4', QrBWLiHStatNivel4.Value, Dmod.MyDB) = ID_YES then
  begin
    Nivel4 := GOTOy.LocalizaPriorNextIntQr(QrBWLiHStat,
      QrBWLiHStatNivel4, QrBWLiHStatNivel4.Value);
    ReopenBWLiciLot(Nivel4);
  end;
end;

procedure TFmBWLiciCab.IncluiAtributo1Click(Sender: TObject);
begin
  MostraFormAtrLiciDef(stIns);
end;

procedure TFmBWLiciCab.IncluiFila1Click(Sender: TObject);
begin
  MostraFormBWLiHEsta(stIns);
end;

procedure TFmBWLiciCab.IncluiStatus1Click(Sender: TObject);
begin
  MostraFormBWLiHStat(stIns);
end;

procedure TFmBWLiciCab.InsereBWLiHStat(Codigo, StatusAtu: Integer);
var
  DataHora: String;
  Nivel4: Integer;
  SQLType: TSQLType;
begin
  if (ImgTipo.SQLType = stIns) or (FStatusAnt <> StatusAtu) then
  begin
    SQLType        := stIns;
    //Codigo         := ;
    Nivel4         := 0;
    DataHora       := Geral.FDT(DModG.ObtemAgora(), 109);
    //Status         := ;

    //
    Nivel4 := UMyMod.BPGS1I32('bwlihstat', 'Nivel4', '', '', tsPos, stIns, Nivel4);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwlihstat', False, [
    'Codigo', 'DataHora',
    'StatusAnt', 'StatusAtu'], [
    'Nivel4'], [
    Codigo, DataHora,
    FStatusAnt, StatusAtu], [
    Nivel4], True);
  end;
end;

procedure TFmBWLiciCab.ItsAltera1Click(Sender: TObject);
begin
  MostraBWLiciIts(stUpd);
end;

procedure TFmBWLiciCab.LotAltera1Click(Sender: TObject);
begin
  MostraBWLiciLot(stUpd);
end;

procedure TFmBWLiciCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o desta licita��o?',
  'BWLiciCab', 'Codigo', QrBWLiciCabCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrBWLiciCab,
      QrBWLiciCabCodigo, QrBWLiciCabCodigo.Value);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmBWLiciCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmBWLiciCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmBWLiciCab.LotExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do lote selecionado?',
  'BWLiciLot', 'Controle', QrBWLiciLotControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrBWLiciLot,
      QrBWLiciLotControle, QrBWLiciLotControle.Value);
    ReopenBWLiciLot(Controle);
  end;
end;

procedure TFmBWLiciCab.ItsExclui1Click(Sender: TObject);
var
  Controle, Codigo, Conta: Integer;
begin
  Controle := QrBWLiciLotControle.Value;
  Codigo   := QrBWLiciCabCodigo.Value;
  //
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'BWLiciIts', 'Conta', QrBWLiciItsConta.Value, Dmod.MyDB) = ID_YES then
  begin
    App_PF.CalculaTotaisLiciLot(Controle, Codigo);
    App_PF.ReopenAllAfterUpd(QrBWLiciCab, QrBWLiciLot, QrBWLiciIts,
      Codigo, Controle, Conta);
    //
    Conta := GOTOy.LocalizaPriorNextIntQr(QrBWLiciIts,
      QrBWLiciItsConta, QrBWLiciItsConta.Value);
    ReopenBWLiciIts(Conta);
  end;
end;

procedure TFmBWLiciCab.ReopenAtrLiciDef(ID_Item: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtrLiciDef, Dmod.MyDB, [
    'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ATRITS, ',
    'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,',
    'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp ',
    'FROM atrlicidef def ',
    'LEFT JOIN atrliciits its ON its.Controle=def.AtrIts ',
    'LEFT JOIN atrlicicad cad ON cad.Codigo=def.AtrCad ',
    'WHERE def.ID_Sorc=' + Geral.FF0(QrBWLiciCabCodigo.Value),
    ' ',
    'UNION  ',
    ' ',
    'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, ',
    'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt,',
    'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp ',
    'FROM atrlicitxt def ',
    'LEFT JOIN atrlicicad cad ON cad.Codigo=def.AtrCad ',
    'WHERE def.ID_Sorc=' + Geral.FF0(QrBWLiciCabCodigo.Value),
    ' ',
    'ORDER BY NO_CAD, NO_ITS ',
    '',
    '']);
  QrAtrliciDef.Locate('ID_Item', ID_Item, []);
end;

procedure TFmBWLiciCab.ReopenBWLiciIts(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrBWLiciIts, Dmod.MyDB, [
  'SELECT prd.Nome NO_Produto, med.Sigla SIGLA_UNIDADE, its.* ',
  'FROM bwliciits its ',
  'LEFT JOIN bwprodutos prd ON prd.Codigo=its.Produto ',
  'LEFT JOIN unidmed    med ON med.Codigo=its.Unidade ',
  'WHERE its.Controle=' + Geral.FF0(QrBWLiciLotControle.Value),
  'ORDER BY its.ItemLt, its.Controle ',
  '']);
end;

procedure TFmBWLiciCab.ReopenBWLiciLot(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrBWLiciLot, Dmod.MyDB, [
  'SELECT url.Nome NO_BWPortal, bem.Nome NO_Empresa, ',
  'amo.Nome NO_PrzAmostras, etr.Nome NO_PrzEntregas, ',
  're1.Nome NO_Responsav1, re2.Nome NO_Responsav2, ',
  're3.Nome NO_Responsav3, gfd.Nome NO_GraFabMCd, ',
  'sgm.Nome NO_Segmento, rpr.Nome NO_Representn, ',
  'gfc.Nome NO_VenceFabri, ',
  'LPAD(Lote, 11, "0") LoteORD, ',
  'lot.* ',
  'FROM bwlicilot lot ',
  'LEFT JOIN bwportais   url ON url.Codigo=lot.BWPortal ',
  'LEFT JOIN przentrgcab amo ON amo.Codigo=lot.PrzAmostras ',
  'LEFT JOIN przentrgcab etr ON etr.Codigo=lot.PrzEntregas ',
  'LEFT JOIN bwrespnsvs  re1 ON re1.Codigo=lot.Responsav1 ',
  'LEFT JOIN bwrespnsvs  re2 ON re2.Codigo=lot.Responsav2 ',
  'LEFT JOIN bwrespnsvs  re3 ON re3.Codigo=lot.Responsav3 ',
  'LEFT JOIN bwempresas  bem ON bem.Codigo=lot.Empresa ',
  'LEFT JOIN grafabcad   gfc ON gfc.Codigo=lot.VenceFabri ',
  '/*LEFT JOIN grafabmar gfm ON gfm.Controle=lot.VenceMarca ',
  'LEFT JOIN grafabmcd   gfd ON gfd.Codigo=gfm.GraFabMCd */',
  'LEFT JOIN grafabmcd   gfd ON gfd.Codigo=lot.VenceMarca',
  'LEFT JOIN bwsegmento  sgm ON sgm.Codigo=lot.Segmento ',
  'LEFT JOIN bwrepresnt  rpr ON rpr.Codigo=lot.Representn ',
  'WHERE lot.Codigo=' + Geral.FF0(QrBWLiciCabCodigo.Value),
  'ORDER BY LoteORD, lot.Controle ',
  '']);
  //
  QrBWLiciLot.Locate('Controle', Controle, []);
end;

procedure TFmBWLiciCab.ReopenBWLiHEsta(Nivel4: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrBWLiHEsta, Dmod.MyDB, [
  'SELECT lhe.*  ',
  'FROM bwlihesta lhe ',
  'WHERE lhe.Controle=' + Geral.FF0(QrBWLiciLotControle.Value),
  'ORDER BY DataHora',
  '']);
  //
  if Nivel4 <> 0 then
    QrBWLiHEsta.Locate('Nivel4', Nivel4, [])
  else
  QrBWLiHEsta.Last;
end;

procedure TFmBWLiciCab.ReopenBWLiHFCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrBWLiHFCab, Dmod.MyDB, [
  'SELECT hfc.* ',
  'FROM bwlihfcab hfc ',
  'WHERE hfc.Codigo=' + Geral.FF0(QrBWLiciCabCodigo.Value),
  '']);
end;

procedure TFmBWLiciCab.ReopenBWLiHFlot();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrBWLiHFLot, Dmod.MyDB, [
  'SELECT hfl.* ',
  'FROM bwlihflot hfl ',
  'WHERE hfl.Controle=' + Geral.FF0(QrBWLiciLotControle.Value),
  '']);
end;

procedure TFmBWLiciCab.ReopenBWLiHStat(Nivel4: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrBWLiHStat, Dmod.MyDB, [
  'SELECT ant.Nome NO_StatusAnt,  ',
  'atu.Nome NO_StatusAtu, lhs.*  ',
  'FROM bwlihstat lhs ',
  'LEFT JOIN bwstatus ant ON ant.Codigo=lhs.StatusAnt ',
  'LEFT JOIN bwstatus atu ON atu.Codigo=lhs.StatusAtu ',
  'WHERE lhs.Codigo=' + Geral.FF0(QrBWLiciCabCodigo.Value),
  'ORDER BY DataHora',
  '']);
  //
  if Nivel4 <> 0 then
    QrBWLiHStat.Locate('Nivel4', Nivel4, [])
  else
    QrBWLiHStat.Last;
end;

procedure TFmBWLiciCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmBWLiciCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmBWLiciCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmBWLiciCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmBWLiciCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmBWLiciCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBWLiciCab.BtStatusClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMStatus, BtStatus);
end;

procedure TFmBWLiciCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrBWLiciCabCodigo.Value;
  Close;
end;

procedure TFmBWLiciCab.LotInclui1Click(Sender: TObject);
begin
  MostraBWLiciLot(stIns);
end;

procedure TFmBWLiciCab.CabAltera1Click(Sender: TObject);
begin
  FStatusAnt := QrBWLiciCabBWStatus.Value;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrBWLiciCab, [PnDados],
  [PnEdita], EdBWStatus, ImgTipo, 'bwlicicab');
end;

procedure TFmBWLiciCab.BtConfirmaClick(Sender: TObject);
var
  AberturaDt, AberturaHr, ReabertuDt, ReabertuHr, LimCadPpDt, LimCadPpHr, UF,
  Pregao, Nome: String;
  Codigo, BWStatus, Entidade, Cliente: Integer;
  //QtdeTotal, ValTotal: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  BWStatus       := EdBWStatus.ValueVariant;
  AberturaDt     := Geral.FDT(TPAberturaDt.Date, 1);
  AberturaHr     := EdAberturaHr.Text;
  ReabertuDt     := Geral.FDT(TPReabertuDt.Date, 1);
  ReabertuHr     := EdReabertuHr.Text;
  LimCadPpDt     := Geral.FDT(TPLimCadPpDt.Date, 1);
  LimCadPpHr     := EdLimCadPpHr.Text;
  Entidade       := EdEntidade.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  UF             := EdUF.ValueVariant;
  Pregao         := EdPregao.ValueVariant;
  //QtdeTotal      := EdQtdeTotal.ValueVariant;
  //ValTotal       := EdValTotal.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //
  if MyObjects.FIC(BWStatus = 0, EdBWStatus, 'Defina o Status!') then Exit;
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Defina o Cliente!') then Exit;
  if MyObjects.FIC(TPAberturaDt.Date < 2, TPAberturaDt, 'Defina a data de abertura!') then Exit;
  if MyObjects.FIC(TPLimCadPpDt.Date < 2, TPLimCadPpDt, 'Defina a data limite de cadastro!') then Exit;
  if MyObjects.FIC(Geral.SiglaUFValida(UF) = False, EdUF, 'Defina uma UF v�lida!') then Exit;
  if MyObjects.FIC(Length(Pregao) = 0, EdPregao, 'Defina o preg�o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('bwlicicab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwlicicab', False, [
  'BWStatus', 'AberturaDt', 'AberturaHr',
  'ReabertuDt', 'ReabertuHr', 'LimCadPpDt',
  'LimCadPpHr', 'Entidade', 'Cliente',
  'UF', 'Pregao', (*'QtdeTotal',
  'ValTotal',*) 'Nome'], [
  'Codigo'], [
  BWStatus, AberturaDt, AberturaHr,
  ReabertuDt, ReabertuHr, LimCadPpDt,
  LimCadPpHr, Entidade, Cliente,
  UF, Pregao, (*QtdeTotal,
  ValTotal,*) Nome], [
  Codigo], True) then
  begin
    InsereBWLiHStat(Codigo, BWStatus);
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmBWLiciCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'bwlicicab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'bwlicicab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmBWLiciCab.BtFilaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFila, BtFila);
end;

procedure TFmBWLiciCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmBWLiciCab.BtLotClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLot, BtLot);
end;

procedure TFmBWLiciCab.ItsInclui1Click(Sender: TObject);
begin
  MostraBWLiciIts(stIns);
end;

procedure TFmBWLiciCab.AlteraAtributo1Click(Sender: TObject);
begin
  MostraFormAtrLiciDef(stUpd);
end;

procedure TFmBWLiciCab.AlteraFila1Click(Sender: TObject);
begin
  MostraFormBWLiHEsta(stUpd);
end;

procedure TFmBWLiciCab.AlteraStatus1Click(Sender: TObject);
begin
  MostraFormBWLiHStat(stUpd);
end;

procedure TFmBWLiciCab.BtAtrLiciDefClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAtrLiciDef, BtAtrLiciDef);
end;

procedure TFmBWLiciCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmBWLiciCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType  := stLok;
  //FApenasBWLiciCab := False;
  //FApenasBWLiciLot := False;
  GBEdita.Align    := alClient;
  PnDadosDB.Align  := alClient;
  CriaOForm;
  FSeq := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrBWStatus, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBWCLientes, Dmod.MyDB);
end;

procedure TFmBWLiciCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrBWLiciCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmBWLiciCab.SBClienteClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormBWClientes();
  UMyMod.SetaCodigoPesquisado(EdCliente, CBCliente, QrBWClientes, VAR_CADASTRO);
end;

procedure TFmBWLiciCab.SbEntiDocsClick(Sender: TObject);
begin
  Entities.MostraFormEntiDocs(CONST_TIPO_EntiDocs2_PREGAO,
  QrBWLiciCabCodigo.Value, QrBWLiciCabPregao.Value);
end;

procedure TFmBWLiciCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxLIC_TACAO_001_01, [
    frxDsBWLiciCab,
    frxDsBWLiciLot,
    frxDsBWLiciIts,
    frxDsBWLiHFCab,
    frxDsBWLiHFLot,
    frxDsBWLiHStat,
    frxDsBWLiHEsta
  ]);
  //
  MyObjects.frxMostra(frxLIC_TACAO_001_01, 'Relat�rio de Licita��o');
end;

procedure TFmBWLiciCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmBWLiciCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrBWLiciCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmBWLiciCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmBWLiciCab.QrBWLiciCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmBWLiciCab.QrBWLiciCabAfterScroll(DataSet: TDataSet);
begin
(*
  if  FApenasBWLiciCab then
    FApenasBWLiciCab := False
  else
*)
  begin
    ReopenBWLiciLot(0);
    ReopenBWLiHStat(0);
    ReopenBWLiHFCab();
    ReopenAtrLiciDef(0);
  end;
end;

procedure TFmBWLiciCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrBWLiciCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmBWLiciCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrBWLiciCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'bwlicicab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmBWLiciCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBWLiciCab.frxLIC_TACAO_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_NO_EMPRESA' then
    Value := VAR_NOMEDONO
  else
end;

procedure TFmBWLiciCab.CabInclui1Click(Sender: TObject);
begin
  FStatusAnt := 0;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrBWLiciCab, [PnDados],
  [PnEdita], EdBWStatus, ImgTipo, 'bwlicicab');
end;

procedure TFmBWLiciCab.CConcluiLote1Click(Sender: TObject);
const
  Titulo = 'Conclus�o do Lote';
var
  Texto: String;
  Codigo, Controle: Integer;
  SQLType: TSQLType;
begin
  if (QrBWLiciLot.State <> dsInactive ) and (QrBWLiciLot.RecordCount > 0) then
  begin
    Codigo   := QrBWLiciLotCodigo.Value;
    Controle := QrBWLiciLotControle.Value;
    Texto    := QrBWLiHFLotHistorico.Value;
    if QrBWLiHFLotControle.Value <> 0 then
      SQLType := stUpd
    else
      SQLType := stIns;
    //
    if MyObjects.GetTexto(TFmGetTexto, FmGetTexto, Titulo, Titulo, Texto) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwlihflot', False, [
      'Codigo', 'Historico'], ['Controle'], [
      Codigo, Texto], [Controle], True) then
      begin
        //FApenasBWLiciLot := True;
        ReopenBWLiciLot(Controle);
      end;
    end;
  end;
end;

procedure TFmBWLiciCab.ConcluiCab1Click(Sender: TObject);
const
  Titulo = 'Conclus�o da Licita��o';
var
  Texto: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  if (QrBWLiciCab.State <> dsInactive ) and (QrBWLiciCab.RecordCount > 0) then
  begin
    Codigo := QrBWLiciCabCodigo.Value;
    Texto  := QrBWLiHFCabHistorico.Value;
    if QrBWLiHFCabCodigo.Value <> 0 then
      SQLType := stUpd
    else
      SQLType := stIns;
    //
    if MyObjects.GetTexto(TFmGetTexto, FmGetTexto, Titulo, Titulo, Texto) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwlihfcab', False, [
      'Historico'], ['Codigo'], [Texto], [Codigo], True) then
      begin
        //FApenasBWLiciCab := True;
        LocCod(Codigo, Codigo);
      end;
    end;
  end;
end;

procedure TFmBWLiciCab.QrBWLiciCabBeforeClose(
  DataSet: TDataSet);
begin
  QrBWLiHFCab.Close;
  QrBWLiciLot.Close;
  QrBWLiHStat.Close;
  QrAtrLiciDef.Close;
end;

procedure TFmBWLiciCab.QrBWLiciCabBeforeOpen(DataSet: TDataSet);
begin
  QrBWLiciCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmBWLiciCab.QrBWLiciLotAfterScroll(DataSet: TDataSet);
begin
  ReopenBWLiciIts(0);
  ReopenBWLiHEsta(0);
  ReopenBWLiHFLot();
end;

procedure TFmBWLiciCab.QrBWLiciLotBeforeClose(DataSet: TDataSet);
begin
  QrBWLiHEsta.Close;
  QrBWLiciIts.Close;
  QrBWLiHFlot.Close;
end;

(*
object EdQtdeTotal: TdmkEdit
  Left = 404
  Top = 72
  Width = 81
  Height = 21
  Alignment = taRightJustify
  TabOrder = 14
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 3
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,000'
  QryCampo = 'QtdeTotal'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
  ValWarn = False
end
object Label22: TLabel
  Left = 404
  Top = 56
  Width = 66
  Height = 13
  Caption = 'Quantid. total:'
  FocusControl = DBEdit13
end
object Label23: TLabel
  Left = 488
  Top = 56
  Width = 50
  Height = 13
  Caption = 'Valor total:'
  FocusControl = DBEdit14
end
object EdValTotal: TdmkEdit
  Left = 488
  Top = 72
  Width = 93
  Height = 21
  Alignment = taRightJustify
  TabOrder = 15
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  QryCampo = 'ValTotal'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
  ValWarn = False
end
*)
end.

