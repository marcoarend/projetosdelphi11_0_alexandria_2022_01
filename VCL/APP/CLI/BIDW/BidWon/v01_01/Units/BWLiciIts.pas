unit BWLiciIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmBWLiciIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    EdItemLt: TdmkEdit;
    Label7: TLabel;
    CBProduto: TdmkDBLookupComboBox;
    EdProduto: TdmkEditCB;
    Label1: TLabel;
    SbProduto: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrBWProdutos: TMySQLQuery;
    QrBWProdutosCodigo: TIntegerField;
    QrBWProdutosNome: TWideStringField;
    DsBWProdutos: TDataSource;
    Label2: TLabel;
    DBEdControle: TdmkDBEdit;
    DBEdCtrlNome: TDBEdit;
    Label4: TLabel;
    QrUnidMed: TMySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    EdUnidade: TdmkEditCB;
    Label9: TLabel;
    EdSigla: TdmkEdit;
    CBUnidade: TdmkDBLookupComboBox;
    SBUnidMed: TSpeedButton;
    EdQtdeItem: TdmkEdit;
    Label8: TLabel;
    Label10: TLabel;
    EdValrUnit: TdmkEdit;
    Label11: TLabel;
    EdValItem: TdmkEdit;
    Label12: TLabel;
    EdLinha: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbProdutoClick(Sender: TObject);
    procedure EdUnidadeChange(Sender: TObject);
    procedure EdUnidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBUnidMedClick(Sender: TObject);
    procedure EdQtdeItemChange(Sender: TObject);
    procedure EdValrUnitChange(Sender: TObject);
    procedure EdValItemChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenBWProdutos();
    procedure RecalculaValorTotal();
  public
    { Public declarations }
    FQrCab, FQRLot, FQrIts: TmySQLQuery;
    FDsCab, FDsLot: TDataSource;
    FCodigo, FControle: Integer;
  end;

  var
  FmBWLiciIts: TFmBWLiciIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, DmkDAC_PF,
  UnMySQLCuringa, UnUMedi_PF, UnApp_Jan, UnApp_PF;

{$R *.DFM}

procedure TFmBWLiciIts.BtOKClick(Sender: TObject);
var
  ItemLt: String;
  Codigo, Controle, Conta, Produto, Unidade, Linha: Integer;
  QtdeItem, ValrUnit, ValItem: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  Controle       := FControle;
  Conta          := EdConta.ValueVariant;
  ItemLt         := EdItemLt.ValueVariant;
  Produto        := EdProduto.ValueVariant;
  Unidade        := EdUnidade.ValueVariant;
  QtdeItem       := EdQtdeItem.ValueVariant;
  ValrUnit       := EdValrUnit.ValueVariant;
  ValItem        := EdValItem.ValueVariant;
  Linha          := EdLinha.ValueVariant;
  //
  Conta := UMyMod.BPGS1I32('bwliciits', 'Conta', '', '', tsPos, SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwliciits', False, [
  'Codigo', 'Controle', 'ItemLt',
  'Produto', 'Unidade', 'QtdeItem',
  'ValrUnit', 'ValItem', 'Linha'], [
  'Conta'], [
  Codigo, Controle, ItemLt,
  Produto, Unidade, QtdeItem,
  ValrUnit, ValItem, Linha], [
  Conta], True) then
  begin
    App_PF.CalculaTotaisLiciLot(Controle, Codigo);
    App_PF.ReopenAllAfterUpd(FQrCab, FQrLot, FQrIts, Codigo, Controle, Conta);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdConta.ValueVariant      := 0;
      EdLinha.ValueVariant      := 0;
      EdItemLt.ValueVariant     := '';
      EdProduto.ValueVariant    := 0;
      CBProduto.KeyValue        := Null;
      EdUnidade.ValueVariant    := 0;
      EdSigla.ValueVariant      := '';
      CBUnidade.KeyValue        := Null;
      EdQtdeItem.ValueVariant   := 0;
      EdValrUnit.ValueVariant   := 0;
      EdValItem.ValueVariant    := 0;
      //
      EdLinha.SetFocus;
    end else Close;
  end;
end;

procedure TFmBWLiciIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBWLiciIts.CBUnidadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidade, CBUnidade, dmktfInteger);
end;

procedure TFmBWLiciIts.EdQtdeItemChange(Sender: TObject);
begin
  if EdQtdeItem.Focused then
    RecalculaValorTotal();
end;

procedure TFmBWLiciIts.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    UMedi_PF.PesquisaPorSigla(False, EdSigla, EdUnidade, CBUnidade);
end;

procedure TFmBWLiciIts.EdSiglaExit(Sender: TObject);
begin
  UMedi_PF.PesquisaPorSigla(True, EdSigla, EdUnidade, CBUnidade);
end;

procedure TFmBWLiciIts.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidade, CBUnidade, dmktfInteger)
end;

procedure TFmBWLiciIts.EdUnidadeChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    UMedi_PF.PesquisaPorCodigo(EdUnidade.ValueVariant, EdSigla);
end;

procedure TFmBWLiciIts.EdUnidadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidade, CBUnidade, dmktfInteger);
end;

procedure TFmBWLiciIts.EdValrUnitChange(Sender: TObject);
begin
  if EdValrUnit.Focused then
    RecalculaValorTotal();
end;

procedure TFmBWLiciIts.EdValItemChange(Sender: TObject);
var
  ValItem, ValrUnit, QtdeItem: Double;
begin
  if EdValItem.Focused then
  begin
    ValItem    := EdValItem.ValueVariant;
    QtdeItem   := EdQtdeItem.ValueVariant;
    if QtdeItem > 0 then
      ValrUnit := ValItem / QtdeItem
    else
      ValrUnit := 0;
    EdValrUnit.ValueVariant := ValrUnit;
  end;
end;

procedure TFmBWLiciIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource   := FDsCab;
  DBEdNome.DataSource     := FDsCab;
  //
  DBEdControle.DataSource := FDsLot;
  DBEdCtrlNome.DataSource := FDsLot;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmBWLiciIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenBWProdutos();
  UnDMkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
end;

procedure TFmBWLiciIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBWLiciIts.RecalculaValorTotal;
var
  ValItem, ValrUnit, QtdeItem: Double;
begin
  ValrUnit    := EdValrUnit.ValueVariant;
  QtdeItem    := EdQtdeItem.ValueVariant;
  ValItem     := ValrUnit * QtdeItem;
  EdValItem.ValueVariant := ValItem;
end;

procedure TFmBWLiciIts.ReopenBWProdutos();
begin
  UnDMkDAC_PF.AbreQuery(QrBWProdutos, DMod.MyDB);
end;

procedure TFmBWLiciIts.SbProdutoClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormBWProdutos();
  UMyMod.SetaCodigoPesquisado(EdProduto, CBProduto, QrBWProdutos, VAR_CADASTRO);
end;

procedure TFmBWLiciIts.SBUnidMedClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := EdUnidade.ValueVariant;
  UMedi_PF.CriaEEscolheUnidMed(Codigo, EdUnidade, CBUnidade, QrUnidMed);
end;

end.
