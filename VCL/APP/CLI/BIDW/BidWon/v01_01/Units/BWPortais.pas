unit BWPortais;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, Variants, ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums;

type
  TFmBWPortais = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrBWPortais: TmySQLQuery;
    DsBWPortais: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrBWPortaisCodigo: TIntegerField;
    QrBWPortaisNome: TWideStringField;
    QrBWPortaisSigla: TWideStringField;
    QrBWPortaisURL: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdSigla: TdmkEdit;
    EdURL: TdmkEdit;
    BtCarrega: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrBWPortaisAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrBWPortaisBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmBWPortais: TFmBWPortais;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, CfgCadLista;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmBWPortais.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmBWPortais.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrBWPortaisCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmBWPortais.DefParams;
begin
  VAR_GOTOTABELA := 'bwportais';
  VAR_GOTOMYSQLTABLE := QrBWPortais;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM bwportais');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmBWPortais.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmBWPortais.QueryPrincipalAfterOpen;
begin
end;

procedure TFmBWPortais.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmBWPortais.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmBWPortais.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmBWPortais.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmBWPortais.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmBWPortais.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBWPortais.BtAlteraClick(Sender: TObject);
begin
  if (QrBWPortais.State <> dsInactive) and (QrBWPortais.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrBWPortais, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'bwportais');
  end;
end;

procedure TFmBWPortais.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrBWPortaisCodigo.Value;
  Close;
end;

procedure TFmBWPortais.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla, URL: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Sigla          := EdSigla.ValueVariant;;
  URL            := EdURL.ValueVariant;;

  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  if MyObjects.FIC(Length(Sigla) = 0, EdSigla, 'Defina uma Sigla!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('bwportais', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwportais', False, [
  'Nome', 'Sigla', 'URL'], [
  'Codigo'], [
  Nome, Sigla, URL], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmBWPortais.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'bwportais', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmBWPortais.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrBWPortais, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'bwportais');
end;

procedure TFmBWPortais.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmBWPortais.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrBWPortaisCodigo.Value, LaRegistro.Caption);
end;

procedure TFmBWPortais.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmBWPortais.SbNovoClick(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'BWPortais', 100, ncGerlSeq1,
  'Portais da WEB',
  [], False, Null(*Maximo*), [], [], False);
end;

procedure TFmBWPortais.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmBWPortais.QrBWPortaisAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmBWPortais.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBWPortais.SbQueryClick(Sender: TObject);
begin
  LocCod(QrBWPortaisCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'bwportais', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmBWPortais.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBWPortais.QrBWPortaisBeforeOpen(DataSet: TDataSet);
begin
  QrBWPortaisCodigo.DisplayFormat := FFormatFloat;
end;

end.

