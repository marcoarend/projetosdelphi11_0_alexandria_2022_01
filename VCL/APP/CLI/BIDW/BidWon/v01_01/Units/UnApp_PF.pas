unit UnApp_PF;

interface

uses
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, MaskUtils, Registry, ShellAPI, TypInfo,
  UnMsgInt, mySQLDbTables, DB, Vcl.DBCtrls, System.Rtti, dmkEdit, dmkEditCB,
  mySQLExceptions, dmkGeral, UnInternalConsts, dmkDBGrid, UnDmkProcFunc,
  UnDmkEnums, UnGrl_Vars, mySQLDirectQuery, UnGrl_Consts;

type
  TUnApp_PF = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    procedure CalculaTotaisLiciCab(Codigo: Integer);
    procedure CalculaTotaisLiciLot(Controle, Codigo: Integer);
    procedure ExportaRegistrosEntreDBs2(TabOrig, TabDest, CondicaoSQL:
              String; BaseOrig, BaseDest: TmySQLDatabase; RichEdit: TRichEdit;
              PB: TProgressBar);
    procedure ImportaEntidadesSourceConnect1(GBAvisos: TGroupBox; PB:
              TProgressBar; LaAviso1, LaAviso2: TLabel);
    procedure ImportaEntidadesSourceConnect2(RichEdit: TRichEdit; PB: TProgressBar);
    procedure ObtemDadosSourceConnect(var Porta, Server: Integer; var IPServer,
              DataBase, OthUser, OthSetCon: String);
    procedure ReopenAllAfterUpd(QrCab, QrLot, QrIts: TmySqlQuery;
              Codigo, Controle, Conta: Integer);
  end;

var
  App_PF: TUnApp_PF;

implementation

uses
  UnMyObjects, UMySQLDB, Module, UMySQLModule, DmkDAC_PF, MyDBCheck;

{ TUnApp_PF }

procedure TUnApp_PF.CalculaTotaisLiciCab(Codigo: Integer);
var
  sCodigo, sQtdeTotal, sValTotal: String;
begin
  sCodigo := Geral.FF0(Codigo);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrSum, Dmod.MyDB, [
  'SELECT SUM(QtdeLote) Quantidade, ',
  'SUM(ValLote) ValTotal  ',
  'FROM bwlicilot ',
  'WHERE Codigo=' + sCodigo,
  '']);
  //
  sQtdeTotal := Geral.FFT_Dot(Dmod.QrSumQuantidade.Value, 3, siNegativo);
  sValTotal  := Geral.FFT_Dot(Dmod.QrSumValTotal.Value, 2, siNegativo);
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE bwlicicab SET ' +
  '  QtdeTotal=' + sQtdeTotal +
  ', ValTotal=' + sValTotal +
  '  WHERE Codigo=' + sCodigo);
end;

procedure TUnApp_PF.CalculaTotaisLiciLot(Controle, Codigo: Integer);
var
  sControle, sQuantidade, sValTotal: String;
begin
  sControle := Geral.FF0(Controle);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrSum, Dmod.MyDB, [
  'SELECT SUM(QtdeItem) Quantidade, ',
  'SUM(ValItem) ValTotal  ',
  'FROM bwliciits ',
  'WHERE Controle=' + sControle,
  '']);
  //
  sQuantidade := Geral.FFT_Dot(Dmod.QrSumQuantidade.Value, 3, siNegativo);
  sValTotal   := Geral.FFT_Dot(Dmod.QrSumValTotal.Value, 2, siNegativo);
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE bwlicilot SET ' +
  '  QtdeLote=' + sQuantidade +
  ', ValLote=' + sValTotal +
  '  WHERE Controle=' + sControle);
  //
  CalculaTotaisLiciCab(Codigo);
end;

procedure TUnApp_PF.ExportaRegistrosEntreDBs2(TabOrig, TabDest,
  CondicaoSQL: String; BaseOrig, BaseDest: TmySQLDatabase; RichEdit: TRichEdit;
  PB: TProgressBar);
  procedure Info(RichEdit: TRichEdit; TabDest, Texto: String);
  begin
    if RichEdit <> nil then
    begin
      RichEdit.SelAttributes.Color := clBlue;
      RichEdit.Text := FormatDateTime('hh:nn:ss:zzz', now()) + ' > ' +
        Uppercase(lowercase(TabDest)) + Texto + sLineBreak + RichEdit.Text;
      RichEdit.Update;
      Application.ProcessMessages;
    end;
  end;
var
  Pasta: String;
  //F: TextFile;
  //S,
  Arquivo, Campos, Prefix, DataDir, Barra: String;
  //QrL,
  Query, Qr1, Qr2, Qr3: TmySQLQuery;
  ArrCampos: TUMyArray_Str;
  ArrValues: TUMyArray_Var;
  QtdFlds: Integer;
begin
  Query := TmySQLQuery.Create(Dmod);
  Query.Close;
  Query.Database := BaseOrig;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM ' + lowercase(TabOrig));
  Query.SQL.Add(CondicaoSQL);
  UnDmkDAC_PF.AbreQuery(Query, BaseOrig);
  //
{
  QrL := TmySQLQuery.Create(Dmod);
  QrL.Close;
  QrL.Database := BaseLocl;
  QrL.SQL.Clear;
}
  //
  Qr1 := TmySQLQuery.Create(Dmod);
  Qr1.Close;
  Qr1.Database := BaseOrig;
  Qr1.SQL.Clear;
  //
  Qr2 := TmySQLQuery.Create(Dmod);
  Qr2.Close;
  Qr2.Database := BaseDest;
  Qr2.SQL.Clear;
  //
  Qr3 := TmySQLQuery.Create(Dmod);
  Qr3.Close;
  Qr3.Database := BaseDest;
  Qr3.SQL.Clear;
  //
  //
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SHOW VARIABLES LIKE "datadir"');
  UnDmkDAC_PF.AbreQuery(Qr1, BaseOrig);
  DataDir := Qr1.FieldByName('Value').AsString;
  //
  if DataDir = '' then
    Barra := '\'
  else
  if pos('\', DataDir) > 0 then
    Barra := '\'
  else
    Barra := '/';
  //
  Pasta := 'C:' +
    Barra + 'Dermatek' +
    Barra + 'Web' +
    Barra + Application.Title +
    Barra + 'Data' +
    Barra;
  Arquivo :=  Pasta + 'SQL_' + lowercase(TabDest) + '.txt';
  if not ForceDirectories(Pasta) then
  begin
    Geral.MB_Aviso('N�o foi poss�vel criar o diret�rio "' + Pasta +
    '". Tente cri�-lo manualmente!');
    Exit;
  end;
{
  if not ForceDirectories(Dir2) then
  begin
    Geral.MB_Erro('N�o foi poss�vel criar o diret�rio "' + Dir2 +
    '". Tente cri�-lo manualmente!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
}
  //
  Campos := '';
  Prefix := '';
  Info(RichEdit, lowercase(TabDest), ' - Obtendo campos da tabela de destino...');
  Campos := UMyMod.ObtemCamposDeTabelaIdentica(BaseDest, lowercase(TabDest), Prefix, QtdFlds);
  ArrCampos := UMyMod.ArrayDeTabelaIdentica_Campos(BaseDest, lowercase(TabDest));

  //

  Info(RichEdit, lowercase(TabDest), ' - Obtendo dados da tabela de origem...');
  if FileExists(Arquivo) then
    DeleteFile(Arquivo);
  Arquivo := dmkPF.DuplicaBarras(Arquivo);
  if FileExists(Arquivo) then
    DeleteFile(Arquivo);
  Application.ProcessMessages;

  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT '+Campos);
  Qr1.SQL.Add('FROM '+ lowercase(TabOrig));
  Qr1.SQL.Add( CondicaoSQL );
  // mudado 2011-10-31 s� funcionava no servidor
  //Qr1.SQL.Add('INTO OUTFILE "' + Arquivo + '"');
  //Qr1.ExecSQL;
  UnDmkDAC_PF.AbreQuery(Qr1, Qr1.Database);
  //

  Info(RichEdit, lowercase(TabDest), ' - Excluindo registros duplicados no destino...');
  Qr3.SQL.Clear;
  Qr3.SQL.Add(DELETE_FROM + lowercase(TabDest) );
  Qr3.SQL.Add( CondicaoSQL );
  Application.ProcessMessages;
  Qr3.ExecSQL;
  //

  Qr1.First;
  Qr2.SQL.Clear;
  Info(RichEdit, lowercase(TabDest), ' - Inserindo dados no destino...');
  PB.Position := 0;
  PB.Max := Qr1.RecordCount;
  while not Qr1.Eof do
  begin
    MyObjects.UpdPB(PB, nil, nil);
    ArrValues := UMyMod.ArrayDeTabelaIdentica_Values(Qr1);
    //
    UMyMod.SQLInsUpd(Qr2, stIns, lowercase(TabDest), False,
      ArrCampos, [], ArrValues, [], False);
    //
    Qr1.Next;
  end;

{
  Info(RichEdit, lowercase(TabDest), ' - Importando de arquivo tempor�rio...');
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arquivo + '"');
  Qr2.SQL.Add('INTO Table '+lowercase(TabDest));
  Qr2.ExecSQL;
}
  //
  Info(RichEdit, lowercase(TabDest), ' - Finalizando ...');
  Qr1.SQL.Clear;
  Qr1.SQL.Add('UPDATE '+lowercase(taborig)+' SET AlterWeb=0');
  Qr1.SQL.Add( CondicaoSQL );
  Qr1.ExecSQL;
  //
  Info(RichEdit, lowercase(TabDest), ' - Transfer�ncia de dados finalizada!');
  Info(RichEdit, '', '===============================================');
end;

procedure TUnApp_PF.ImportaEntidadesSourceConnect1(GBAvisos: TGroupBox; PB:
  TProgressBar; LaAviso1, LaAviso2: TLabel);
const
  cStep = 100;
  cLimit = True;
  //
  sProcName = 'TUnApp_PF.ImportaEntidadesSourceConnect1()';
  TabelaSrc = 'entidades';
  TabelaDst = 'entidades';
var
  CamposSrc, CamposDst, Campos: String;
  QtdFldsSrc, QtdFldsDst: Integer;
var
  Porta, Server: Integer;
  IPServer, DataBase, OthUser, OthSetCon, UnDmkProcFunc: String;
  DB: TmySQLDatabase;
  Qr1: TmySQLQuery;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Codigo, Controle: Integer;
  Nome: String;
  //
  I: Integer;
const
  OriTab = 'entidades';
  DstTab = 'entidades';
  //
  procedure PV_N(I: Integer; Texto: String);
  begin
    if I = 0 then
      SQLRec := SQLRec + Texto
    else
      SQLRec := SQLRec + ', ' + Texto;
  end;
begin
  if Geral.MB_Pergunta(
  'Deseja realmente importar as Entidades?') = ID_YES then
  begin
    GBAvisos.Visible := True;
    ObtemDadosSourceConnect(
      Porta, Server, IPServer, DataBase, OthUser, OthSetCon);
    //
    DB := TmySQLDatabase.Create(Dmod);
    try
      try
        UnDmkDAC_PF.ConfiguracaoDB(DB);
        //
        DB.DatabaseName   := Database;
        DB.Host           := IPServer;
        DB.Port           := Porta;
        DB.UserName       := OthUser;
        DB.UserPassword   := OthSetCon;
        DB.KeepConnection := True;
      except
        Geral.MB_Erro(
        'N�o foi poss�vel conectar o "SourceConnect" para configura��o do mysql:' +
        sLineBreak + sLineBreak+
        'Database: ' + DB.DatabaseName + sLineBreak +
        'Host: ' + DB.Host + sLineBreak +
        'Porta: ' + FormatFloat('0', VAR_PORTA));
      end;
      //
      CamposSrc := UMyMod.ObtemCamposDeTabelaIdentica(DB, TabelaSrc, '', QtdFldsSrc);
      CamposDst := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabelaDst, '', QtdFldsDst);
      //
      // Evitar erros de mudan�a n�o sincrona de tabelas!
      if QtdFldsSrc < QtdFldsDst then
        Campos := CamposSrc
      else
        Campos := CamposDst;
      //
      Qr1 := TmySQLQuery.Create(Dmod);
      Qr1.Close;
      Qr1.Database := DB;
      Qr1.SQL.Text := 'SELECT COUNT(*) FROM ' + TabelaSrc;
      Qr1.Open;
      Total := Qr1.Fields[0].AsInteger;
      Qr1.Close;

      //




  //procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Marca;
    Dmod.MySQLBatchExecute1.Database := Dmod.MyDB;
    SQL_FIELDS := Geral.ATS(['INSERT INTO ' + DstTab + ' ( ', Campos, ') VALUES ']);
    //

    ItsOK := 0;

    //

    //
    Step :=  cStep; //EdFirst.ValueVariant;
    PB.Position := 0;
    PB.Max := (Total div Step) + 1;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Registro ' +
    Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo dados. ');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
    //
    //QrUpd.SQL.Text := '';
    while Total > ItsOK do
    begin
      MyObjects.UpdPB(PB, nil, nil);
      SQL := 'SELECT ' + Campos + ' FROM ' + TabelaSrc + sLineBreak;
      //if CkLimit.Checked then
      if cLimit then
      begin
        //if EdFirst.ValueVariant > 0 then
        begin
          (*
          SQL := SQL + ' FIRST ' + Geral.FF0(Step);
          if ItsOK > 0 then
            SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
          *)
          SQL := SQL + ' Limit ' + Geral.FF0(ItsOK) + ',' + Geral.FF0(Step);
        end;
      end;
      //
      Qr1.Close;
      Qr1.SQL.Text := SQL;
      Qr1.Open;
      Continua := Qr1.RecordCount > 0;
      if Continua then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Registro ' +
          Geral.FF0(Qr1.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
        SQL_VALUES := '';
        Qr1.First;
        SQLRec    := '';
        while not Qr1.Eof do
        begin
          //
          if Qr1.RecNo = 1 then
            SQLRec    := ' ('
          else
            SQLRec    := ', (';
          //
          for I := 0 to Qr1.Fields.Count -1 do
          begin
            case Qr1.Fields[I].DataType of
              ftString,
              ftWideString,
              ftMemo,
              ftWideMemo,
              ftBlob: PV_N(I, Geral.VariavelToString(Qr1.Fields[I].AsString));
              ftSmallint,
              ftInteger,
              ftLargeInt,
              ftWord,
              ftShortint,
              ftLongWord,
              ftSingle: PV_N(I, Geral.FF0(Qr1.Fields[I].AsInteger));
              ftFloat,
              ftCurrency,
              ftExtended: PV_N(I, Geral.FFT_Dot(Qr1.Fields[I].AsFloat, 10, siNegativo));
              ftDate,
              ftDateTime: PV_N(I, '"' + Geral.FDT(Qr1.Fields[I].AsDateTime, 109) + '"');
              ftTime: PV_N(I, '"' + Geral.FDT(Qr1.Fields[I].AsDateTime, 100) + '"');
              else
              begin
                Geral.MB_Erro('Vari�vel n�o implementada em ' + sProcName + sLineBreak +
                GetEnumName(TypeInfo(TFieldType), Integer(Qr1.Fields[I].DataType)));
                PV_N(I, '#Erro');
                EXIT;
              end;
            end;
          end;
          SQLRec := SQLRec + ') ';
                //
          SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
          Qr1.Next;
        end;

        if SQL_VALUES <> EmptyStr then
        begin
          //DBAnt.Execute(SQLc);
          Dmod.MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
          try
            Dmod.MySQLBatchExecute1.ExecSQL;
          except
            on E: Exception do
            begin
              Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
              E.Message + sLineBreak +
              SQL_FIELDS + SQL_VALUES);
              //
              EXIT;
            end;
          end;
        end;
        SQL_VALUES := EmptyStr;

        //
        ItsOK := ItsOK + Step;
      end;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Registro ' +
      Geral.FF0(Qr1.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));




    //
    finally
      DB.Free;
      if Qr1 <> nil then
        Qr1.Free;
      GBAvisos.Visible := False;
    end;
  end;
end;

procedure TUnApp_PF.ImportaEntidadesSourceConnect2(RichEdit: TRichEdit;
PB: TProgressBar);
const
  sProcName = 'TUnApp_PF.ImportaEntidadesSourceConnect2()';
  TabOrig = 'entidades';
  TabDest = 'entidades';
  CondicaoSQL = '';
var
  BaseOrig, BaseDest: TmySQLDatabase;
var
  Porta, Server: Integer;
  IPServer, DataBaseName, OthUser, OthSetCon, UnDmkProcFunc: String;
  DB: TmySQLDatabase;
begin
  ObtemDadosSourceConnect(
    Porta, Server, IPServer, DataBaseName, OthUser, OthSetCon);
  //
  BaseDest := Dmod.MyDB;
  BaseOrig := TmySQLDatabase.Create(Dmod);
  try
    try
      //DB.Connect;
      //USQLDB.ConectaDB(BaseOrig, sProcName);
      UnDmkDAC_PF.ConfiguracaoDB(BaseOrig);
      //
      BaseOrig.DatabaseName   := DatabaseName;
      BaseOrig.Host           := IPServer;
      BaseOrig.Port           := Porta;
      BaseOrig.UserName       := OthUser;
      BaseOrig.UserPassword   := OthSetCon;
      BaseOrig.KeepConnection := True;
    except
      Geral.MB_Erro(
      'N�o foi poss�vel conectar o "SourceConnect" para configura��o do mysql:' +
      sLineBreak + sLineBreak+
      'Database: ' + BaseOrig.DatabaseName + sLineBreak +
      'Host: ' + BaseOrig.Host + sLineBreak +
      'Porta: ' + FormatFloat('0', VAR_PORTA));
    end;
    //
    //
    ExportaRegistrosEntreDBs2(TabOrig, TabDest, CondicaoSQL,
    BaseOrig, BaseDest, RichEdit, PB);
  finally
    BaseOrig.Free;
  end;

end;

procedure TUnApp_PF.ObtemDadosSourceConnect(var Porta, Server: Integer; var
  IPServer, DataBase, OthUser, OthSetCon: String);
begin
  Porta     := Geral.ReadAppKeyCU('Porta', Application.Title + CO_SourceConnect, ktInteger, 3306);
  Server    := Geral.ReadAppKeyCU('Server', Application.Title + CO_SourceConnect, ktInteger, 0);
  IPServer  := Geral.ReadAppKeyCU('IPServer', Application.Title + CO_SourceConnect, ktString, '127.0.0.1');
  ////
  DataBase  := Geral.ReadAppKeyCU('Database', Application.Title + CO_SourceConnect, ktString, 'overseer');
  OthUser   := Geral.ReadAppKeyCU('OthUser', Application.Title + CO_SourceConnect, ktString, 'root');
  OthSetCon := Geral.ReadAppKeyCU('OthSetCon', Application.Title + CO_SourceConnect, ktString, '');
  if OthSetCon = EmptyStr then
    OthSetCon := CO_USERSPNOW
  else
    OthSetCon := dmkPF.Criptografia(OthSetCon, CO_LLM_SecureStr);
end;

procedure TUnApp_PF.ReopenAllAfterUpd(QrCab, QrLot, QrIts: TmySqlQuery; Codigo,
  Controle, Conta: Integer);
begin
  if QrCab <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(QrCab, Dmod.MyDB);
    if QrCab.Locate('Codigo', Codigo, []) then
    begin
      if QrLot <> nil then
      begin
        UnDmkDAC_PF.AbreQuery(QrLot, Dmod.MyDB);
        if QrLot.Locate('Controle', Controle, []) then
        begin
          if QrIts <> nil then
          begin
            UnDmkDAC_PF.AbreQuery(QrIts, DMod.MyDB);
            if Conta <> 0 then
              QrIts.Locate('Conta', Conta, []);
          end;
        end;
      end;
    end;
  end;
end;

end.
