unit BWLiHEsta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmBWLiHEsta = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label6: TLabel;
    EdNivel4: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    Label9: TLabel;
    Label18: TLabel;
    TPDataHora: TdmkEditDateTimePicker;
    EdDataHora: TdmkEdit;
    MeHistorico: TMemo;
    EdEstahAnt: TdmkEdit;
    EdEstahAtu: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenBWLiHEsta(Nivel4: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    FCodigo, FControle, FEstahAnt: Integer;
  end;

  var
  FmBWLiHEsta: TFmBWLiHEsta;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmBWLiHEsta.BtOKClick(Sender: TObject);
var
  DataHora, Nome, Historico: String;
  Codigo, Controle, Nivel4, EstahAnt, EstahAtu: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  Controle       := FControle;
  Nivel4         := EdNivel4.ValueVariant;
  DataHora       := Geral.FDT(TPDataHora.Date, 1) + ' ' + EdDataHora.Text;
  EstahAnt       := EdEstahAnt.ValueVariant;
  EstahAtu       := EdEstahAtu.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Historico      := MeHistorico.Text;

  //
  Nivel4 := UMyMod.BPGS1I32('bwlihesta', 'Nivel4', '', '', tsPos, SQLType, Nivel4);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwlihesta', False, [
  'Codigo', 'Controle', 'DataHora',
  'EstahAnt', 'EstahAtu', 'Nome',
  'Historico'], [
  'Nivel4'], [
  Codigo, Controle, DataHora,
  EstahAnt, EstahAtu, Nome,
  Historico], [
  Nivel4], True) then
  begin
    ReopenBWLiHEsta(Nivel4);
(*
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdCargo.ValueVariant     := 0;
      CBCargo.KeyValue         := Null;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else*)
    Close;
  end;
end;

procedure TFmBWLiHEsta.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBWLiHEsta.FormActivate(Sender: TObject);
begin
  //DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmBWLiHEsta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MeHistorico.Align := alClient;
end;

procedure TFmBWLiHEsta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBWLiHEsta.ReopenBWLiHEsta(Nivel4: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Nivel4 <> 0 then
      FQrIts.Locate('Nivel4', Nivel4, []);
  end;
end;

end.
