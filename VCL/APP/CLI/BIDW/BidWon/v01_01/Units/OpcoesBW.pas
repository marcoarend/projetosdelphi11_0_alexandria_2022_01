unit OpcoesBW;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw;

type
  TFmOpcoesBW = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel9: TPanel;
    Label14: TLabel;
    SbDirDocEnti: TSpeedButton;
    Label15: TLabel;
    SBDirDocsApOS: TSpeedButton;
    EdDirDocsEnti: TdmkEdit;
    EdDirDocsApOS: TdmkEdit;
    GroupBox4: TGroupBox;
    Label3: TLabel;
    EdSloganFoot: TdmkEdit;
    LaHora: TLabel;
    EdLastBckpIntrvl: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbDirDocEntiClick(Sender: TObject);
    procedure SBDirDocsApOSClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmOpcoesBW: TFmOpcoesBW;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule;

{$R *.DFM}

procedure TFmOpcoesBW.BtOKClick(Sender: TObject);
const
  Codigo = 1;
var
(*
  DefContrat, GraNivPatr, GraCodPatr, GraNivOutr, GraCodOutr, TipCodPatr,
  TipCodOutr, TipCodServ, TipCodVend, FormaCobrLoca, LocArredMinut,
  PermLocSemEstq, StatIniPadrLoc, StatIniPadrSvc, StatIniPadrVen,
  LocRegrFisNFe: Integer;
  HrLimPosDiaNaoUtil, LocArredHrIniIni, LocArredHrIniFim,
  LocPeriodoExtenso, TxtDevolLimpeza, TxtDevolConserva
*)
  SloganFoot, DirDocsEnti, DirDocsApOS, LastBckpIntrvl: String;
  //DdValidOrcaLoc, DdValidOrcaVen: Integer;
begin
(*
  TipCodPatr := EdTipCodPatr.ValueVariant;
  TipCodOutr := EdTipCodOutr.ValueVariant;
  TipCodServ := EdTipCodServ.ValueVariant;
  TipCodVend := EdTipCodVend.ValueVariant;
  DefContrat := EdDefContrat.ValueVariant;
  GraNivPatr := RGGraNivPatr.ItemIndex;
  GraCodPatr := EdGraCodPatr.ValueVariant;
  GraNivOutr := RGGraNivOutr.ItemIndex;
  GraCodOutr := EdGraCodOutr.ValueVariant;
  FormaCobrLoca      := RGFormaCobrLoca.ItemIndex;
  HrLimPosDiaNaoUtil := EdHrLimPosDiaNaoUtil.Text;
  LocArredMinut      := EdLocArredMinut.ValueVariant;
  LocArredHrIniIni   := EdLocArredHrIniIni.Text;
  LocArredHrIniFim   := EdLocArredHrIniFim.Text;
  PermLocSemEstq     := Geral.BoolToInt(CkPermLocSemEstq.Checked);
  StatIniPadrLoc     := RGStatIniPadrLoc.ItemIndex;
  StatIniPadrSvc     := RGStatIniPadrSvc.ItemIndex;
  StatIniPadrVen     := RGStatIniPadrVen.ItemIndex;
  LocRegrFisNFe      := EdLocRegrFisNFe.ValueVariant;
  LocPeriodoExtenso  := EdLocPeriodoExtenso.Text;
*)
  DirDocsEnti        := EdDirDocsEnti.ValueVariant;
  DirDocsApOS        := EdDirDocsApOS.ValueVariant;
(*
  DdValidOrcaLoc     := EdDdValidOrcaLoc.ValueVariant;
  DdValidOrcaVen     := EdDdValidOrcaVen.ValueVariant;
  TxtDevolLimpeza    := EdTxtDevolLimpeza.ValueVariant;
  TxtDevolConserva   := EdTxtDevolConserva.ValueVariant;
  //
  if MyObjects.FIC(FormaCobrLoca < 1, RGFormaCobrLoca,
  'Informe a forma de cobran�a da loca��o!') then
  begin
    PageControl1.ActivePageIndex := 2;
    RGFormaCobrLoca.SetFocus;
    Exit;
  end;
*)
  //
(*
 if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'opcoestren', False, [
  'DefContrat', 'GraNivPatr', 'GraCodPatr',
  'GraNivOutr', 'GraCodOutr', 'BloPrdSPer',
  'TipCodPatr', 'TipCodOutr',
  'TipCodServ', 'TipCodVend',
  'FormaCobrLoca', 'HrLimPosDiaNaoUtil', 'LocArredMinut',
  'LocArredHrIniIni', 'LocArredHrIniFim', 'PermLocSemEstq',
  'StatIniPadrLoc', 'StatIniPadrSvc', 'StatIniPadrVen',
  'LocRegrFisNFe', 'LocPeriodoExtenso',
  'DdValidOrcaLoc', 'DdValidOrcaVen',
  'TxtDevolLimpeza', 'TxtDevolConserva'], [
  'Codigo'], [
  DefContrat, GraNivPatr, GraCodPatr,
  GraNivOutr, GraCodOutr, CkBloPrdSPer.Checked,
  TipCodPatr, TipCodOutr,
  TipCodServ, TipCodVend,
  FormaCobrLoca, HrLimPosDiaNaoUtil, LocArredMinut,
  LocArredHrIniIni, LocArredHrIniFim, PermLocSemEstq,
  StatIniPadrLoc, StatIniPadrSvc, StatIniPadrVen,
  LocRegrFisNFe, LocPeriodoExtenso,
  DdValidOrcaLoc, DdValidOrcaVen,
  TxtDevolLimpeza, TxtDevolConserva], [
  Codigo], True) then
  begin
*)
    SloganFoot := EdSloganFoot.Text;
    LastBckpIntrvl := EdLastBckpIntrvl.Text;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'opcoesgerl', False, [
    'SloganFoot', 'DirDocsEnti', 'DirDocsApOS'], [
    'Codigo'], [
    SloganFoot, DirDocsEnti, DirDocsApOS], [
    Codigo], False) then
    begin
      (*
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'controle', False, [
        'CasasProd'], [], [RGCasasProd.ItemIndex], [], False) then
      begin
      *)
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctrlgeral', False, [
      'LastBckpIntrvl'], [
      'Codigo'], [
      LastBckpIntrvl], [
      1], False) then
      begin
        DModG.ReopenOpcoesGerl();
        DModG.ReopenCtrlGeral();
        //Dmod.ReopenOpcoesTRen();
        Close;
      end;
    end;
  //end;
end;

procedure TFmOpcoesBW.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesBW.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesBW.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
  //
  DModG.ReopenOpcoesGerl();
//  Dmod.ReopenOpcoesTRen();
  Dmod.ReopenControle;
  //
(*
  EdDefContrat.ValueVariant := Dmod.QrOpcoesTRenDefContrat.Value;
  CBDefContrat.KeyValue     := Dmod.QrOpcoesTRenDefContrat.Value;
  RGGraNivPatr.ItemIndex    := Dmod.QrOpcoesTRenGraNivPatr.Value;
  EdGraCodPatr.ValueVariant := Dmod.QrOpcoesTRenGraCodPatr.Value;
  RGGraNivOutr.ItemIndex    := Dmod.QrOpcoesTRenGraNivOutr.Value;
  EdGraCodOutr.ValueVariant := Dmod.QrOpcoesTRenGraCodOutr.Value;
  CkBloPrdSPer.Checked      := Geral.IntToBool(Dmod.QrOpcoesTRenBloPrdSPer.Value);
  //
  EdTipCodPatr.ValueVariant := Dmod.QrOpcoesTRenTipCodPatr.Value;
  CBTipCodPatr.KeyValue     := Dmod.QrOpcoesTRenTipCodPatr.Value;
  EdTipCodOutr.ValueVariant := Dmod.QrOpcoesTRenTipCodOutr.Value;
  CBTipCodOutr.KeyValue     := Dmod.QrOpcoesTRenTipCodOutr.Value;
  EdTipCodServ.ValueVariant := Dmod.QrOpcoesTRenTipCodServ.Value;
  CBTipCodServ.KeyValue     := Dmod.QrOpcoesTRenTipCodServ.Value;
  EdTipCodVend.ValueVariant := Dmod.QrOpcoesTRenTipCodVend.Value;
  CBTipCodVend.KeyValue     := Dmod.QrOpcoesTRenTipCodVend.Value;
  //
  RGCasasProd.ItemIndex     := Dmod.QrControleCasasProd.Value;
  //
  RGFormaCobrLoca.ItemIndex := Dmod.QrOpcoesTRenFormaCobrLoca.Value;
  //
  EdHrLimPosDiaNaoUtil.ValueVariant := Dmod.QrOpcoesTRenHrLimPosDiaNaoUtil.Value;
  EdLocArredMinut.ValueVariant      := Dmod.QrOpcoesTRenLocArredMinut.Value;
  EdLocArredHrIniIni.ValueVariant   := Dmod.QrOpcoesTRenLocArredHrIniIni.Value;
  EdLocArredHrIniFim.ValueVariant   := Dmod.QrOpcoesTRenLocArredHrIniFim.Value;
  CkPermLocSemEstq.Checked          := Geral.IntToBool(Dmod.QrOpcoesTRenPermLocSemEstq.Value);
  //
  RGStatIniPadrLoc.ItemIndex        := Dmod.QrOpcoesTRenStatIniPadrLoc.Value;
  RGStatIniPadrSvc.ItemIndex        := Dmod.QrOpcoesTRenStatIniPadrSvc.Value;
  RGStatIniPadrVen.ItemIndex        := Dmod.QrOpcoesTRenStatIniPadrVen.Value;
  EdLocRegrFisNFe.ValueVariant      := Dmod.QrOpcoesTRenLocRegrFisNFe.Value;
  CBLocRegrFisNFe.KeyValue          := Dmod.QrOpcoesTRenLocRegrFisNFe.Value;
  //
  EdLocPeriodoExtenso.Text          := Dmod.QrOpcoesTRenLocPeriodoExtenso.Value;
*)
  //
  EdDirDocsEnti.Text                := DModG.QrOpcoesGerl.FieldByName('DirDocsEnti').AsString;
  EdDirDocsApOS.Text                := DModG.QrOpcoesGerl.FieldByName('DirDocsApOS').AsString;
  EdLastBckpIntrvl.ValueVariant     := DModG.QrCtrlGeralLastBckpIntrvl.Value;
  //
(*
  EdDdValidOrcaLoc.ValueVariant     := Dmod.QrOpcoesTRenDdValidOrcaLoc.Value;
  EdDdValidOrcaVen.ValueVariant     := Dmod.QrOpcoesTRenDdValidOrcaVen.Value;
  EdTxtDevolLimpeza.Text            := Dmod.QrOpcoesTRenTxtDevolLimpeza.Value;
  EDTxtDevolConserva.Text           := Dmod.QrOpcoesTRenTxtDevolConserva.Value;
  //
*)
  {
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartaG, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM cartag ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
  }
(*
  UnDmkDAC_PF.AbreQuery(QrTipCodPatr, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTipCodOutr, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTipCodServ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTipCodVend, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratos, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM contratos ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
*)
  //
  EdSloganFoot.ValueVariant := DModG.QrOpcoesGerl.FieldByName('SloganFoot').AsString;
  //
  //PageControl1.ActivePageIndex := 0;
end;

procedure TFmOpcoesBW.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesBW.SbDirDocEntiClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirDocsEnti);
end;

procedure TFmOpcoesBW.SBDirDocsApOSClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirDocsApOS);
end;

end.
