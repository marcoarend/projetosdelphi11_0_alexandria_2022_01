unit LoadCSV_01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TCamposTab = (camtabIndef=0, camtabCodNom=1, camtabCodUsuNom=2, camtabCodUsuSglNom=3);
  TFormaIns = (formaiIndef=0, formaiSimples=1, formaiMulNom=2);
  TMyArrString = array of String;
  TFmLoadCSV_01 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Query: TMySQLQuery;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    SbLoadCSVOthDir: TSpeedButton;
    PB1: TProgressBar;
    EdLoadCSVArq: TdmkEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;

    MeAvisos: TMemo;
    TabSheet2: TTabSheet;
    Grade1: TStringGrid;
    TabSheet3: TTabSheet;
    Grade2: TStringGrid;
    BtInsereB: TBitBtn;
    BtAtrela: TBitBtn;
    BtVerifCadastr: TBitBtn;
    TabSheet4: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrCadVerif: TMySQLQuery;
    QrCadVerifCampo: TWideStringField;
    QrCadVerifCodigo: TIntegerField;
    QrCadVerifLinha: TIntegerField;
    QrCadVerifTexto: TWideStringField;
    DsCadVerif: TDataSource;
    BtGeraBWLici: TBitBtn;
    QrBWCsvLoad: TMySQLQuery;
    QrLote: TMySQLQuery;
    QrLoteCodigo: TIntegerField;
    QrLoteLinha: TIntegerField;
    QrLoteBWStatus: TIntegerField;
    QrLoteNO_BWStatus: TWideStringField;
    QrLoteReabertura: TDateTimeField;
    QrLoteData: TDateField;
    QrLoteHora: TTimeField;
    QrLoteCadastroDaProposta: TWideStringField;
    QrLoteEntidade: TIntegerField;
    QrLoteCodCliente: TIntegerField;
    QrLoteCliente: TWideStringField;
    QrLoteEstado: TWideStringField;
    QrLotePregao: TWideStringField;
    QrLoteLote: TWideStringField;
    QrLoteCodPortal: TIntegerField;
    QrLotePortal: TWideStringField;
    QrLoteNProcesso: TWideStringField;
    QrLoteColocacao: TWideStringField;
    QrLoteEstah: TWideStringField;
    QrLoteCodPrzAmostra: TIntegerField;
    QrLoteAmostras: TWideStringField;
    QrLoteCodPrzEntrega: TIntegerField;
    QrLotePrazoEntrega: TWideStringField;
    QrLoteCodResponsa1: TIntegerField;
    QrLoteCodResponsa2: TIntegerField;
    QrLoteCodResponsa3: TIntegerField;
    QrLoteResponsaveis: TWideStringField;
    QrLoteCodEmpresa: TIntegerField;
    QrLoteEmpresa: TWideStringField;
    QrLoteCodEmpresaGanhadora: TIntegerField;
    QrLoteEmpresaGanhadora: TWideStringField;
    QrLoteCodMarcaDoGanhador: TIntegerField;
    QrLoteMarcaDoGanhador: TWideStringField;
    QrLoteCodSegmento: TIntegerField;
    QrLoteSegmento: TWideStringField;
    QrLoteCodRepresentante: TIntegerField;
    QrLoteRepresentante: TWideStringField;
    QrLoteValorNoPregao: TFloatField;
    QrLoteValorGanhador: TFloatField;
    QrLoteClass_: TIntegerField;
    QrBWCsvLoadCodigo: TIntegerField;
    QrBWCsvLoadLinha: TIntegerField;
    QrBWCsvLoadBWStatus: TIntegerField;
    QrBWCsvLoadNO_BWStatus: TWideStringField;
    QrBWCsvLoadReabertura: TDateTimeField;
    QrBWCsvLoadData: TDateField;
    QrBWCsvLoadHora: TTimeField;
    QrBWCsvLoadCadastroDaProposta: TWideStringField;
    QrBWCsvLoadEntidade: TIntegerField;
    QrBWCsvLoadCodCliente: TIntegerField;
    QrBWCsvLoadCliente: TWideStringField;
    QrBWCsvLoadEstado: TWideStringField;
    QrBWCsvLoadPregao: TWideStringField;
    QrBWCsvLoadLote: TWideStringField;
    QrBWCsvLoadItemLt: TWideStringField;
    QrBWCsvLoadCodProduto: TIntegerField;
    QrBWCsvLoadProduto: TWideStringField;
    QrBWCsvLoadCodUnidade: TIntegerField;
    QrBWCsvLoadUnidade: TWideStringField;
    QrBWCsvLoadQuantidade: TFloatField;
    QrBWCsvLoadEstimativa: TFloatField;
    QrBWCsvLoadEstimativa2: TFloatField;
    QrBWCsvLoadCodPortal: TIntegerField;
    QrBWCsvLoadPortal: TWideStringField;
    QrBWCsvLoadNProcesso: TWideStringField;
    QrBWCsvLoadColocacao: TWideStringField;
    QrBWCsvLoadEstah: TWideStringField;
    QrBWCsvLoadCodPrzAmostra: TIntegerField;
    QrBWCsvLoadAmostras: TWideStringField;
    QrBWCsvLoadCodPrzEntrega: TIntegerField;
    QrBWCsvLoadPrazoEntrega: TWideStringField;
    QrBWCsvLoadCodResponsa1: TIntegerField;
    QrBWCsvLoadCodResponsa2: TIntegerField;
    QrBWCsvLoadCodResponsa3: TIntegerField;
    QrBWCsvLoadResponsaveis: TWideStringField;
    QrBWCsvLoadCodEmpresa: TIntegerField;
    QrBWCsvLoadEmpresa: TWideStringField;
    QrBWCsvLoadCodEmpresaGanhadora: TIntegerField;
    QrBWCsvLoadEmpresaGanhadora: TWideStringField;
    QrBWCsvLoadCodMarcaDoGanhador: TIntegerField;
    QrBWCsvLoadMarcaDoGanhador: TWideStringField;
    QrBWCsvLoadCodSegmento: TIntegerField;
    QrBWCsvLoadSegmento: TWideStringField;
    QrBWCsvLoadCodRepresentante: TIntegerField;
    QrBWCsvLoadRepresentante: TWideStringField;
    QrBWCsvLoadValorNoPregao: TFloatField;
    QrBWCsvLoadValorGanhador: TFloatField;
    QrBWCsvLoadClass_: TIntegerField;
    QrBWCsvLoadMes: TWideStringField;
    QrBWCsvLoadAno: TWideStringField;
    QrBWCsvLoadLk: TIntegerField;
    QrBWCsvLoadDataCad: TDateField;
    QrBWCsvLoadDataAlt: TDateField;
    QrBWCsvLoadUserCad: TIntegerField;
    QrBWCsvLoadUserAlt: TIntegerField;
    QrBWCsvLoadAlterWeb: TSmallintField;
    QrBWCsvLoadAWServerID: TIntegerField;
    QrBWCsvLoadAWStatSinc: TSmallintField;
    QrBWCsvLoadAtivo: TSmallintField;
    BtDropTable: TBitBtn;
    BtGeraCads: TBitBtn;
    QrPsq2: TMySQLQuery;
    QrPsq2Codigo: TIntegerField;
    QrPsq2Nome: TWideStringField;
    QrPesq: TMySQLQuery;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbLoadCSVOthDirClick(Sender: TObject);
    procedure BtInsereBClick(Sender: TObject);
    procedure BtAtrelaClick(Sender: TObject);
    procedure BtVerifCadastrClick(Sender: TObject);
    procedure BtGeraBWLiciClick(Sender: TObject);
    procedure BtDropTableClick(Sender: TObject);
    procedure BtGeraCadsClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    FSQLCad: String;
    FFldID: String;
    FFldNome: String;
    //FDBGInsert: Boolean;
    FTabela, FSQLInsExtra: String;
    FLstLin1: TStringList;
    //
    procedure AtualizaLog(Status: Integer; Nome, Campo: String; ValInt: Integer);
    procedure Informa(Texto: String);
    procedure CarregaAtual_SL(Codigo, Linha: Integer; Lista: TStringList);
    procedure CarregaAtual_SG(Codigo, Lin: Integer; Grade: TStringGrid);
    function  ObtemNomesResponsaveis(const Texto: String; var Responsav1,
              Responsav2, Responsav3: String): Boolean;
    //
    procedure IncluiBWLiciCab();
    procedure IncluiBWLiciLot();
    procedure IncluiBWLiciIts();

    function  CarregaAtual_Qr(Texto, Tabela: String; NovoCodigo: TNovoCodigo): Boolean;
    procedure DivideMulNomes(Texto: String; MyArr: TMyArrString);
    procedure CarregaFabricante_x_Marca();
  public

    { Public declarations }
    FCodigo, FControle, FConta: Integer;
  end;

  var
  FmLoadCSV_01: TFmLoadCSV_01;

implementation

uses UnMyObjects, UMySQLModule, Module, DmkDAC_PF, MyDBCheck, UnALL_Jan,
  UnDmkProcFunc;

{$R *.DFM}

const
  cCabecalho: array [0..30] of string = ('STATUS',	'REABERTURA',	'DATA',	'HORA',
  'CADASTRO DA PROPOSTA', 'CLIENTE', 'ESTADO', 'PREG�O', 'LOTE', 'PRODUTO',
  'UNID', 'QUANTIDADE', 'ESTIMATIVA', 'ESTIMATIVA2', 'PORTAL', 'N� PROCESSO',
  'COLOCA��O', 'EST�', 'AMOSTRAS', 'PRAZO ENTREGA', 'RESPONS�VEIS',
  'EMPRESA', 'EMPRESA GANHADORA', 'MARCA DO GANHADOR', 'SEGMENTO',
  'REPRESENTANTE', 'VALOR NO PREG�O', 'VALOR GANHADOR', 'CLASS.', 'M�S',
  'ANO');

procedure TFmLoadCSV_01.AtualizaLog(Status: Integer; Nome, Campo: String;
  ValInt: Integer);
begin
//
end;

procedure TFmLoadCSV_01.BitBtn1Click(Sender: TObject);
begin
  CarregaFabricante_x_Marca();
end;

procedure TFmLoadCSV_01.BtAtrelaClick(Sender: TObject);
var
 Nome: String;
 Codigo: Integer;
 //
 aCodiResponsaveis: array of Integer;
 aNomeResponsaveis: array of String;
 Nome1, Nome2, Nome3: String;
 I, Q, Regs, CodResponsa1, CodResponsa2, CodResponsa3: Integer;
begin
(*
UPDATE bwcsvload SET BWStatus=0;
UPDATE bwcsvload SET Entidade=0;
UPDATE bwcsvload SET CodCliente=0;
UPDATE bwcsvload SET CodProduto=0;
UPDATE bwcsvload SET CodUnidade=0;
UPDATE bwcsvload SET CodPortal=0;
*)

////////////////////////////////////////////////////////////////////////////////
////////////////////////////   STATUS   ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Status');
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM bwstatus ',
  '']);
  Query.First;
  while not Query.Eof do
  begin
    Nome   := Query.FieldByName('Nome').AsString;
    Codigo := Query.FieldByName('Codigo').AsInteger;
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE bwcsvload SET BWStatus=' + Geral.FF0(Codigo) +
    ' WHERE UPPER(NO_BWStatus) = UPPER("' + Nome + '")');
    //
    Query.Next;
  end;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Status');
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
  'UPDATE bwcsvload',
  '',
  'INNER JOIN',
  '  bwstatus ON bwcsvload.NO_BWStatus = bwstatus.Nome ',
  'SET ',
  '  bwcsvload.bwstatus = bwstatus.Codigo;',
  '']);
//
////////////////////////////////////////////////////////////////////////////////
////////////////////////////   CLIENTE   ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
DROP TABLE IF EXISTS _NO_Cliente_;
CREATE TABLE _NO_Cliente_
SELECT DISTINCT Cliente
FROM bidwon.bwcsvload;
/**/
SELECT ent.Codigo, cli.Cliente
FROM _NO_Cliente_ cli
LEFT JOIN Entidades ent
  ON ent.RazaoSocial=cli.Cliente
ORDER BY Codigo DESC, Cliente
INTO OUTFILE "C:/Teste/Nayr/cliente.csv"
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY ''
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Clientes');
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
  'UPDATE bwcsvload',
  '',
  'INNER JOIN',
  '  bwclientes ON bwcsvload.Cliente = bwclientes.Nome ',
  'SET ',
  '  bwcsvload.CodCliente = bwclientes.Codigo;',
  '']);
//
//
////////////////////////////////////////////////////////////////////////////////
////////////////////////////   PRODUTO   ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
SELECT DISTINCT Produto
FROM bwcsvload
ORDER BY Produto
INTO OUTFILE "C:/Teste/Nayr/produto.csv"
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY ''
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Produtos');
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
  'UPDATE bwcsvload',
  '',
  'INNER JOIN',
  '  bwprodutos ON bwcsvload.Produto = bwprodutos.Nome ',
  'SET ',
  '  bwcsvload.CodProduto = bwprodutos.Codigo;',
  '']);
//
////////////////////////////////////////////////////////////////////////////////
////////////////////////////   UNIDADE   ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
SELECT DISTINCT Unidade
FROM bwcsvload
ORDER BY Unidade
INTO OUTFILE "C:/Teste/Nayr/Unidade.csv"
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY ''
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Unidades de Medida');
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
  'UPDATE bwcsvload',
  '',
  'INNER JOIN',
  '  unidmed ON bwcsvload.Unidade = unidmed.Nome',
  'SET ',
  '  bwcsvload.CodUnidade = unidmed.Codigo;',
  '']);
//
////////////////////////////////////////////////////////////////////////////////
////////////////////////////    PORTAL   ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
SELECT DISTINCT Portal
FROM bwcsvload
ORDER BY Portal
INTO OUTFILE "C:/Teste/Nayr/Portal.csv"
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY ''
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Portais');
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
  'UPDATE bwcsvload',
  '',
  'INNER JOIN',
  '  bwportais ON bwcsvload.Portal = bwportais.Nome',
  'SET ',
  '  bwcsvload.CodPortal = bwportais.Codigo;',
  '']);

//
////////////////////////////////////////////////////////////////////////////////
////////////////////////////   AMOSTRAS   ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
SELECT DISTINCT Amostras
FROM bwcsvload
ORDER BY Amostras
INTO OUTFILE "C:/Teste/Nayr/Amostras.csv"
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY ''
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Prazos de Amostra');
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
  'UPDATE bwcsvload',
  '',
  'INNER JOIN',
  '  przentrgcab ON bwcsvload.Amostras = przentrgcab.Nome',
  'SET ',
  '  bwcsvload.CodPrzAmostra = przentrgcab.Codigo;',
  '']);
//
//
////////////////////////////////////////////////////////////////////////////////
//////////////////////////   PRAZOENTREGA   ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
SELECT DISTINCT PrazoEntrega
FROM bwcsvload
ORDER BY PrazoEntrega
INTO OUTFILE "C:/Teste/Nayr/PrazoEntrega.csv"
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY ''
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Prazos de Entrega');
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
  'UPDATE bwcsvload',
  '',
  'INNER JOIN',
  '  przentrgcab ON bwcsvload.PrazoEntrega = przentrgcab.Nome',
  'SET ',
  '  bwcsvload.CodPrzEntrega = przentrgcab.Codigo;',
  '']);
//
////////////////////////////////////////////////////////////////////////////////
//////////////////////////   RESPONSAVEIS   ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
SELECT DISTINCT Responsaveis
FROM bwcsvload
ORDER BY Responsaveis
INTO OUTFILE "C:/Teste/Nayr/Responsaveis.csv"
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY ''
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Respons�veis');
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM bwrespnsvs ',
  '']);
  Regs := Query.RecordCount;
  SetLength(aCodiResponsaveis, Regs + 1);
  SetLength(aNomeResponsaveis, Regs + 1);
  aCodiResponsaveis[0] := 0;
  aNomeResponsaveis[0] := EmptyStr;
  Query.First;
  while not Query.Eof do
  begin
    aCodiResponsaveis[Query.RecNo + 1] := Query.FieldByName('Codigo').AsInteger;
    aNomeResponsaveis[Query.RecNo + 1] := Query.FieldByName('Nome').AsString;
    //
    Query.Next;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT Codigo, Responsaveis ',
  'FROM bwcsvload ',
  '']);
  Query.First;
  PB1.Position := 0;
  PB1.Max := (Query.RecordCount div 100) + 1;
  Q := 0;
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Atualizando Respons�veis ' + Geral.FF0(Q) +
    ' de ' + Geral.FF0(Query.RecordCount));
  while not Query.Eof do
  begin
    Q := Q + 1;
    if Q / 100 = Q div 100 then
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Atualizando Respons�veis ' + Geral.FF0(Q) +
        ' de ' + Geral.FF0(Query.RecordCount));
    if ObtemNomesResponsaveis(Query.FieldByName('Responsaveis').AsString, Nome1,
    Nome2, Nome3) then
    begin
      if Nome1 <> EmptyStr then
      begin
        for I := 0 to Regs do
        begin
          if AnsiLowercase(aNomeResponsaveis[I]) = AnsiLowerCase(Nome1) then
          begin
            CodResponsa1 := aCodiResponsaveis[I];
            Break;
          end;
        end;
        //
        for I := 0 to Regs do
        begin
          if AnsiLowercase(aNomeResponsaveis[I]) = AnsiLowerCase(Nome2) then
          begin
            CodResponsa2 := aCodiResponsaveis[I];
            Break;
          end;
        end;
        //
        for I := 0 to Regs do
        begin
          if AnsiLowercase(aNomeResponsaveis[I]) = AnsiLowerCase(Nome3) then
          begin
            CodResponsa3 := aCodiResponsaveis[I];
            Break;
          end;
        end;
        //
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE bwcsvload SET ' +
        '  CodResponsa1=' + Geral.FF0(CodResponsa1) +
        ', CodResponsa2=' + Geral.FF0(CodResponsa2) +
        ', CodResponsa3=' + Geral.FF0(CodResponsa3) +
        ' WHERE Codigo=' + Geral.FF0(Query.FieldByName('Codigo').AsInteger));
        //
      end;
      //
    end;
    //
    Query.Next;
  end;
  Query.Close;
  //
    //
//
////////////////////////////////////////////////////////////////////////////////
////////////////////////////   EMPRESA   ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
SELECT DISTINCT Empresa
FROM bwcsvload
ORDER BY Empresa
INTO OUTFILE "C:/Teste/Nayr/Empresa.csv"
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY ''
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Empresas');
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
  'UPDATE bwcsvload',
  '',
  'INNER JOIN',
  '  bwempresas ON bwcsvload.Empresa = bwempresas.Nome',
  'SET ',
  '  bwcsvload.CodEmpresa = bwempresas.Codigo;',
  '']);
//
////////////////////////////////////////////////////////////////////////////////
////////////////////////   EmpresaGanhadora   //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
SELECT DISTINCT EmpresaGanhadora
FROM bwcsvload
ORDER BY EmpresaGanhadora
INTO OUTFILE "C:/Teste/Nayr/EmpresaGanhadora.csv"
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY ''
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Empresas Ganhadoras');
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
  'UPDATE bwcsvload',
  '',
  'INNER JOIN',
//  '  bwwinners ON bwcsvload.EmpresaGanhadora = bwwinners.Nome',
  '  grafabcad ON bwcsvload.EmpresaGanhadora = grafabcad.Nome',
  'SET ',
//  '  bwcsvload.CodEmpresaGanhadora = bwwinners.Codigo;',
  '  bwcsvload.CodEmpresaGanhadora = grafabcad.Codigo;',
  '']);

////////////////////////////////////////////////////////////////////////////////
/////////////////////////   MarcaDoGanhador   //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
SELECT DISTINCT MarcaDoGanhador
FROM bwcsvload
ORDER BY MarcaDoGanhador
INTO OUTFILE "C:/Teste/Nayr/MarcaDoGanhador.csv"
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY ''
*)
//
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Marcas Ganhadoras');
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
  'UPDATE bwcsvload',
  '',
  'INNER JOIN',
  '  grafabmcd ON bwcsvload.MarcaDoGanhador = grafabmcd.Nome',
  'SET ',
  '  bwcsvload.CodMarcaDoGanhador = grafabmcd.Codigo;',
  '']);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////   Segmento   ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
SELECT DISTINCT Segmento
FROM bwcsvload
ORDER BY Segmento
INTO OUTFILE "C:/Teste/Nayr/Segmento.csv"
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY ''
*)
//
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Segmentos');
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
  'UPDATE bwcsvload',
  '',
  'INNER JOIN',
  '  bwsegmento ON bwcsvload.Segmento = bwsegmento.Nome',
  'SET ',
  '  bwcsvload.CodSegmento = bwsegmento.Codigo;',
  '']);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////   Representante   ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
(*
SELECT DISTINCT Representante
FROM bwcsvload
ORDER BY Representante
INTO OUTFILE "C:/Teste/Nayr/Representante.csv"
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY ''
*)
//
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Representantes');
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
  'UPDATE bwcsvload',
  '',
  'INNER JOIN',
  '  bwrepresnt ON bwcsvload.Representante = bwrepresnt.Nome',
  'SET ',
  '  bwcsvload.CodRepresentante = bwrepresnt.Codigo;',
  '']);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Atualiza��o Finalizada!');
end;

procedure TFmLoadCSV_01.BtDropTableClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  if Geral.MB_Pergunta('Deseja realmente excluir todas tabelas?') = ID_YES then
  begin
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWCsvLoad');
    //UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE ');
    //UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE //////////');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWStatus');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWClientes');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWProdutos');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWPortais');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWRespnsvs');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWEmpresas');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWWinners');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWSegmento');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWRepresnt');
    //UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE ');
    //UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE ');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE GraFabCad');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE GraFabMCd');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE GraFabMar');
    //UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE ');
    //UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE ');
    //UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE ');
    //UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE //////////');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWLiciCab');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWLiciLot');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DROP TABLE BWLiciIts');
    //
    ALL_Jan.MostraFormVerifiDB(False);
  end;
end;

procedure TFmLoadCSV_01.BtGeraBWLiciClick(Sender: TObject);
(*
  'SELECT Codigo,',
  'Linha, BWStatus, NO_BWStatus, ',
  'Reabertura, Data, Hora, ',
  'CadastroDaProposta, Entidade, CodCliente, ',
  'Cliente, Estado, Pregao, ',
  'Lote, ',
  '/*',
  'ItemLt, CodProduto, ',
  'Produto, CodUnidade, Unidade, ',
  'Quantidade, Estimativa, Estimativa2, ',
  '*/',
  'CodPortal, Portal, NProcesso, ',
  'Colocacao, Estah, CodPrzAmostra, ',
  'Amostras, CodPrzEntrega, PrazoEntrega, ',
  'CodResponsa1, CodResponsa2, CodResponsa3, ',
  'Responsaveis, CodEmpresa, Empresa, ',
  'CodEmpresaGanhadora, EmpresaGanhadora, CodMarcaDoGanhador, ',
  'MarcaDoGanhador, CodSegmento, Segmento, ',
  'CodRepresentante, Representante, ValorNoPregao, ',
  'ValorGanhador, Class_  ',
  'FROM bwcsvload',
  'GROUP BY Data, Cliente, Pregao, Lote',
    //

*)
var
  Data: TDateTime;
  Cliente, Pregao, Lote: String;
  NovoCab: Boolean;
  QtdeLote, ValLote, QtdeTotal, ValTotal: Double;
begin
  FCodigo   := 0;
  FControle := 0;
  FConta    := 0;
  //
  QtdeLote  := 0;
  ValLote   := 0;
  QtdeTotal := 0;
  ValTotal  := 0;
  //
  if Geral.MB_Pergunta('TODOS lan�amentos de licita��es ser�o exclu�dos!' +
  sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Data    := 0;
      Cliente := EmptyStr;
      Pregao  := EmptyStr;
      Lote    := '-999999999';
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo lan�amentos atuais');
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      ' DELETE FROM bwlicicab;' +
      ' DELETE FROM bwlicilot;' +
      ' DELETE FROM bwliciits;');
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela tempor�ria');
      UnDmkDAC_PF.AbreMySQLQuery0(QrBWCsvLoad, Dmod.MyDB, [
      'SELECT *  ',
      'FROM bwcsvload ',
      'ORDER BY Linha ',
      '']);
      PB1.Position := 0;
      PB1.Max := QrBWCsvLoad.RecordCount;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando lan�amentos');
      QrBWCsvLoad.First;
      while not QrBWCsvLoad.Eof do
      begin
        NovoCab := False;
        // Parei Aqui! Fazer!
        if (QrBWCsvLoadData.Value <> Data)
        or (AnsiUppercase(QrBWCsvLoadCliente.Value) <> AnsiUppercase(Cliente))
        or (AnsiUppercase(QrBWCsvLoadPregao.Value) <> AnsiUppercase(Pregao)) then
        begin
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'bwlicicab', False, [
          'QtdeTotal', 'ValTotal'], ['Codigo'], [
          QtdeTotal, ValTotal], [FCodigo], False) then
          begin
            QtdeTotal := 0;
            ValTotal  := 0;
          end;
          //
          IncluiBWLiciCab();
          //
          Data    := QrBWCsvLoadData.Value;
          Cliente := QrBWCsvLoadCliente.Value;
          Pregao  := QrBWCsvLoadPregao.Value;
          //
          NovoCab := True;
        end;
        if NovoCab or (QrBWCsvLoadLote.Value <> Lote) then
        begin
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'bwlicilot', False, [
          'QtdeLote', 'ValLote'], ['Controle'], [
          QtdeLote, ValLote], [FControle], False) then
          begin
            QtdeLote  := 0;
            ValLote   := 0;
          end;
          //
          IncluiBWLiciLot();
          Lote := QrBWCsvLoadLote.Value;
        end;
        //
        MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
        //
        IncluiBWLiciIts();
        QtdeLote  := QtdeLote  + QrBWCsvLoadQuantidade.Value;
        ValLote   := ValLote   + QrBWCsvLoadEstimativa2.Value;
        QtdeTotal := QtdeTotal + QrBWCsvLoadQuantidade.Value;
        ValTotal  := ValTotal  + QrBWCsvLoadEstimativa2.Value;
        //
        QrBWCsvLoad.Next;
      end;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'bwlicicab', False, [
      'QtdeTotal', 'ValTotal'], ['Codigo'], [
      QtdeTotal, ValTotal], [FCodigo], False);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'bwlicilot', False, [
      'QtdeLote', 'ValLote'], ['Controle'], [
      QtdeLote, ValLote], [FControle], False);
      //
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmLoadCSV_01.BtGeraCadsClick(Sender: TObject);
const
  sProcName = 'TFmLoadCSV_01.BtGeraCadsClick()';
  Step = 25;
var
  N: Integer;
  sMax, Nome: String;
  Nomes: TMyArrString;
  //
  procedure GeraCads(Titulo, Tabela, CampoOri: String; CamposTab: TCamposTab;
  FormaIns: TFormaIns);
  var
    J: Integer;
  begin
    N := 0;
    PB1.Position := N;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando ' + Titulo);
    if CampoOri <> EmptyStr then
      UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
      'SELECT DISTINCT ' + CampoOri + ' Nome ',
      'FROM bwcsvload ',
      'WHERE ' + CampoOri + ' <> ""',
      ''])
    else
      UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
      FSQLCad,
      '']);
    if Query.RecordCount > 1 then
    begin
      PB1.Max := (Query.RecordCount div Step) + 1;
      sMax := Geral.FF0(Query.RecordCount);
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM gerlseq1 WHERE Tabela="' + Tabela + '"');
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + Tabela + ' WHERE Codigo > 0');
      //
      Query.First;
      while not Query.Eof do
      begin
        Nome := Geral.VariavelToString(Query.FieldByName('Nome').AsString);
        N := N + 1;
        if (N div Step) = (N / Step) then
          MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
          'Atualizando ' + Titulo + ' ' + Geral.FF0(N) + ' de ' + sMax);
        //
        if FormaIns = formaiSimples then
        begin
          case CamposTab of
            TCamposTab.camtabCodNom:
              UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'INSERT INTO ' + Tabela +
              ' SET Codigo=' + Geral.FF0(N) + ', Nome=' +
              Geral.VariavelToString(Query.FieldByName('Nome').AsString));
            TCamposTab.camtabCodUsuNom:
              UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'INSERT INTO ' + Tabela +
              ' SET Codigo=' + Geral.FF0(N) + ', CodUsu=' + Geral.FF0(N) +
              ', Nome=' + Nome);
            TCamposTab.camtabCodUsuSglNom:
              UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'INSERT INTO ' + Tabela +
              ' SET Codigo=' + Geral.FF0(N) + ', CodUsu=' + Geral.FF0(N) +
              ', Nome=' + Nome + ', Sigla="' + DmkPF.GeraSigla(Nome, 6) + '"');
            else Geral.MB_Erro('"CamposTab" n�o implementado em ' + sProcName);
          end;
        end else
        begin
          DivideMulNomes(Nome, Nomes);
          for J := Low(Nomes) to High(Nomes) do
            CarregaAtual_Qr(Nomes[J], Tabela, TNovoCodigo.ncGerlSeq1);
        end;
        //
        Query.Next;
      end;
    end;
  end;
begin
  FLstLin1 := TStringList.Create;
  try
  //
  GeraCads('Clientes', 'bwclientes', 'Cliente', TCamposTab.camtabCodNom, TFormaIns.formaiSimples);
  GeraCads('Produtos', 'bwprodutos', 'Produto', TCamposTab.camtabCodNom, TFormaIns.formaiSimples);
  GeraCads('Unidades', 'unidmed', 'Unidade', TCamposTab.camtabCodUsuSglNom, TFormaIns.formaiSimples);
  GeraCads('Portais', 'bwportais', 'Portal', TCamposTab.camtabCodNom, TFormaIns.formaiSimples);
  FSQLCad := Geral.ATS([
  'SELECT DISTINCT Amostras Nome',
  'FROM bwcsvload ',
  'WHERE Amostras <> ""',
  '',
  'UNION',
  '',
  'SELECT DISTINCT PrazoEntrega Nome',
  'FROM bwcsvload ',
  'WHERE PrazoEntrega <> ""',
  '']);
  GeraCads('Amostras e Prazo de Entrega', 'przentrgcab', EmptyStr, TCamposTab.camtabCodNom, TFormaIns.formaiSimples);
  //
  GeraCads('Respons�veis', 'bwrespnsvs', 'Responsaveis', TCamposTab.camtabCodNom, TFormaIns.formaiSimples); //TFormaIns.formaiMulNom);
  GeraCads('Empresas', 'bwempresas', 'Empresa', TCamposTab.camtabCodNom, TFormaIns.formaiSimples);
  GeraCads('Empresas Ganhadoras', 'grafabcad', 'EmpresaGanhadora', TCamposTab.camtabCodUsuNom, TFormaIns.formaiSimples);
  GeraCads('Marca do Ganhador', 'grafabmcd', 'MarcaDoGanhador', TCamposTab.camtabCodNom, TFormaIns.formaiSimples);
  CarregaFabricante_x_Marca();
  GeraCads('Segmentos', 'bwsegmento', 'Segmento', TCamposTab.camtabCodNom, TFormaIns.formaiSimples);
  GeraCads('Representantes', 'bwrepresnt', 'Representante', TCamposTab.camtabCodNom, TFormaIns.formaiSimples);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Atualiza��o finalizada!');
  //
  finally
    FLstLin1.Free;
  end;
end;

procedure TFmLoadCSV_01.BtInsereBClick(Sender: TObject);
var
  Col, Row, I, Erros, Codigo: Integer;
  TitEsperado, TitEncontrado: String;
  Erro: Boolean;
begin
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM bwcsvload');
  //
  Erros := 0;
  for I := 1 to Grade2.ColCount - 1 do
  begin
    TitEsperado   := Uppercase(Trim(cCabecalho[I-1]));
    TitEncontrado := Uppercase(Trim(Grade2.Cells[I, 0]));
    //
    if (TitEsperado <> TitEncontrado) then
    begin
      Erro := (I < Length(cCabecalho)) or (TitEncontrado <> EmptyStr);
      if Erro then
      begin
        Erros := Erros + 1;
        Geral.MB_Erro(
        'T�tulo de coluna esperado: "' + TitEsperado + '"' +
        sLineBreak +
        'T�tulo encontrado: "' + TitEncontrado +  '"' +
        sLineBreak + '�ndice: ' + Geral.FF0(I + 1));
      end;
    end;
  end;
  if Erros > 0 then Exit;
  //
  PB1.Position := 0;
  PB1.Max := Grade2.RowCount - 1;
  Codigo := 0;
  for I := 1 to Grade2.RowCount - 1 do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    Codigo := Codigo + 1;
    CarregaAtual_SG(Codigo, I, Grade2);
 end;

end;

procedure TFmLoadCSV_01.BtOKClick(Sender: TObject);
const
  sProcName = 'TFmLoadCSV_01.BtOKClick()';
  //
  procedure CarregaArquivoParaLayout(const Arquivo: String);
  var
    N, I, Controle, Conta, Coluna, Codigo: Integer;
    Linha, CodTxt, Nome, DataIni, DataFim, NomeEsq, NomeArq, NomeTab, NomeFld,
    Campo, DataType: String;
    LstArq, LstLin1, LstLin2, LstLin3: TStringList;
    Continua: Boolean;
    TitEsperado, TitEncontrado, Texto, sLimpo: AnsiString;
    Erros: Integer;
    TxtArq: String;
  begin
    //AtualizaLog(_STATUS_100, '', EmptyStr, 0);
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM bwcsvload');
    //
    if FileExists(Arquivo) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo: ' + Arquivo + '.');
      LstArq := TStringList.Create;
      try
        LstLin1 := TStringList.Create;
        try
          LstLin2 := TStringList.Create;
          try
            LstLin3 := TStringList.Create;
            try
              LstArq.Delimiter :=';'; //comma delimiter
              LstArq.QuoteChar := '"'; //single quote around strings
              //
              LstArq.LoadFromFile(Arquivo);
              if lstArq.Count > 0 then
              begin
                PB1.Position := 0;
                PB1.Max := lstArq.Count;
                Linha := ';' + LstArq[0] + ';';
                //
                LstLin1 := Geral.Explode3(Linha, ';');
                if LstLin1.Count > 0 then
                begin
                  Erros := 0;
                  for I := 0 to LstLin1.Count - 1 do
                  begin
                    TitEsperado   := Uppercase(Trim(cCabecalho[I]));
                    TitEncontrado := Uppercase(Trim(LstLin1[I]));
                    if TitEsperado <> TitEncontrado then
                    begin
                      Erros := Erros + 1;
                      Geral.MB_Erro(
                      'T�tulo de coluna eperado: "' + TitEsperado + '"' +
                      sLineBreak +
                      'T�tulo encontrado: "' + TitEncontrado +  '"' +
                      sLineBreak + '�ndice: ' + Geral.FF0(I + 1));
                    end;
                  end;
                  if Erros > 0 then Exit;
                  //
                  Codigo := 0;
                  for N := 1 to lstArq.Count - 1 do
                  begin
                    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
                    Linha := ';' + LstArq[N] + ';';
                    LstLin1 := Geral.Explode3(Linha, ';');
                    if LstLin1.Count > 0 then
                    begin
                      Texto := Uppercase(Trim(LstLin1[0]));
                      if Texto = EmptyStr then
                      begin
(*
                        Texto := EmptyStr;
                        for I := 0 to LstLin1.Count - 1 do
                        begin
                          Texto := Texto + Uppercase(Trim(LstLin1[I]));
                        end;
                        sLimpo := Texto;
                        sLimpo := StringReplace(sLimpo, 'R$ -JANEIRO1900', '',[rfReplaceAll]);
                        sLimpo := StringReplace(sLimpo, 'JANEIRO1900', '',[rfReplaceAll]);
                        if Trim(sLimpo) <> EmptyStr then
                          Geral.MB_Info(sLimpo + sLineBreak + Texto + sLineBreak +
                          'Linha ' + IntToStr(N));
*)
                      end else
                      begin
                        Codigo := Codigo + 1;
                        if LstLin1.Count < 31 then
                          Geral.MB_Erro('Erro na linha:' + sLineBreak + Linha);
                        //
                        CarregaAtual_SL(Codigo, N, LstLin1);
                      end;
                    end;
                  end;
(*
                  Linha := ';' + LstArq[1] + ';';
                  LstLin2 := Geral.Explode3(Linha, ';');
                  if LstLin2.Count > 0 then
                  begin
                    if LstLin1.Count <> LstLin2.Count then
                    begin
                      Informa('[CAPL] Quantidade de Colunas difere (2): ' +
                        sLineBreak + Arquivo);
                      Exit;
                    end;
                    Linha := ';' + LstArq[2] + ';';
                    LstLin3 := Geral.Explode3(Linha, ';');
                    if LstLin3.Count > 0 then
                    begin
                      if LstLin1.Count <> LstLin3.Count then
                      begin
                        Informa('[CAPL] Quantidade de Colunas difere (3): ' +
                          sLineBreak + Arquivo);
                        Exit;
                      end;
                      NomeEsq  := '';
                      Controle := 0;
                      NomeArq  := ExtractFileName(Arquivo);
                      NomeTab  := '';
                      if CodLayout = 0 then
                        Continua := InsereTabelaOVPLayEsq(NomeEsq, CodLayout)
                      else
                        Continua := True;
                      if Continua then
                      begin
                        if InsereTabelaOVPLayTab(CodLayout, Controle, NomeArq, NomeTab) then
                        begin
                          for I := 0 to LstLin1.Count - 1 do
                          begin
                            Conta    := 0;
                            Coluna   := I + 1;
                            NomeFld  := LstLin1[I];
                            //NomeFld  := UTF8ToWIdeString(LstLin1[I]);
                            Campo    := LstLin2[I];
                            DataType := LstLin3[I];
                            InsereTabelaOVPLayFld(CodLayout, Controle, Conta, Campo,
                              NomeFld, DataType, Coluna);
                          end;
                        end;
                      end;
                    end;
                  end;
*)
                end;
              end;
              //
              MyObjects.Informa2(LaAviso1, LaAviso2, False, Geral.FDT(
                Dmod.DModG_ObtemAgora(), 2) + ' Obten��o layout finalizado do arquivo: ' +
                Arquivo);
              PB1.Position := 0;
            finally
              if LstLin3 <> nil then
                LstLin3.Free;
            end;
          finally
            if LstLin2 <> nil then
              LstLin2.Free;
          end;
        finally
          if LstLin1 <> nil then
            LstLin1.Free;
        end;
      finally
        if LstArq <> nil then
          LstArq.Free;
      end;
    end;
  end;
var
  Arquivo: String;
begin
  Arquivo := EdLoadCSVArq.Text;
  case PageControl1.ActivePageIndex of
(*
    0: CarregaArquivoParaLayout(Arquivo);
    1: MyObjects.Xls_To_StringGrid(Grade1, Arquivo, PB1, LaAviso1, LaAviso2);
*)
    2:
    begin
      MeAvisos.Lines.Clear;
      //
      MyObjects.Xls_To_StringGrid_Faster(Grade2, Arquivo, 'NOVA PLANILHA', PB1,
      LaAviso1, LaAviso2, MeAvisos, 1, 0);
      //
      BtInsereB.Enabled := True;
      //
      if MeAvisos.Text <> EmptyStr then
        if Geral.MB_Pergunta(
        'Existem erros de importa��o! Deseja visualiz�-los?') = ID_YES then
          PageControl1.ActivePageIndex := 0;
    end;
    else Geral.MB_Info('Carregamento n� implementado para "' +
    PageControl1.Pages[PageControl1.ActivePageIndex].Caption + '" em ' + sProcName);
  end;
end;

procedure TFmLoadCSV_01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLoadCSV_01.BtVerifCadastrClick(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCadVerif, Dmod.MyDB, [
  'SELECT "Status" Campo, Codigo, Linha, NO_BWStatus Texto ',
  'FROM bwcsvload',
  'WHERE NO_BWStatus <> ""',
  'AND BWStatus=0',
  '',
  'UNION ',
  '',
  'SELECT "Cliente" Campo, Codigo, Linha, Cliente Texto ',
  'FROM bwcsvload',
  'WHERE Cliente <> "" ',
  'AND CodCliente=0',
  '',
  'UNION ',
  '',
  'SELECT "Produto" Campo, Codigo, Linha, Produto Texto ',
  'FROM bwcsvload',
  'WHERE Produto <> "" ',
  'AND CodProduto=0',
  '',
  'UNION ',
  '',
  'SELECT "Unidade" Campo, Codigo, Linha, Unidade Texto ',
  'FROM bwcsvload',
  'WHERE Unidade <> "" ',
  'AND CodUnidade=0',
  '',
  'UNION ',
  '',
  'SELECT "Portal" Campo, Codigo, Linha, Portal Texto ',
  'FROM bwcsvload',
  'WHERE Portal <> "" ',
  'AND CodPortal=0',
  '',
  'UNION ',
  '',
  'SELECT "Amostras" Campo, Codigo, Linha, Amostras Texto ',
  'FROM bwcsvload',
  'WHERE Amostras <> "" ',
  'AND CodPrzAmostra=0',
  '',
  'UNION ',
  '',
  'SELECT "PrazoEntrega" Campo, Codigo, Linha, PrazoEntrega Texto ',
  'FROM bwcsvload',
  'WHERE PrazoEntrega <> "" ',
  'AND CodPrzEntrega=0',
  '',
  'UNION ',
  '',
  'SELECT "PrazoEntrega" Campo, Codigo, Linha, PrazoEntrega Texto ',
  'FROM bwcsvload',
  'WHERE PrazoEntrega <> "" ',
  'AND CodPrzEntrega=0',
  '',
  'UNION ',
  '',
  'SELECT "Responsaveis" Campo, Codigo, Linha, Responsaveis Texto ',
  'FROM bwcsvload',
  'WHERE Responsaveis <> "" ',
  'AND CodResponsa1=0',
  '',
  'UNION ',
  '',
  'SELECT "Empresa" Campo, Codigo, Linha, Empresa Texto ',
  'FROM bwcsvload',
  'WHERE Empresa <> "" ',
  'AND CodEmpresa=0',
  '',
  'UNION ',
  '',
  'SELECT "EmpresaGanhadora" Campo, Codigo, Linha, EmpresaGanhadora Texto ',
  'FROM bwcsvload',
  'WHERE EmpresaGanhadora <> "" ',
  'AND CodEmpresaGanhadora=0',
  '',
  'UNION ',
  '',
  'SELECT "MarcaDoGanhador" Campo, Codigo, Linha, MarcaDoGanhador Texto ',
  'FROM bwcsvload',
  'WHERE MarcaDoGanhador <> "" ',
  'AND CodMarcaDoGanhador=0',
  '',
  'UNION ',
  '',
  'SELECT "Segmento" Campo, Codigo, Linha, Segmento Texto ',
  'FROM bwcsvload',
  'WHERE Segmento <> "" ',
  'AND CodSegmento=0',
  '',
  'UNION ',
  '',
  'SELECT "Representante" Campo, Codigo, Linha, Representante Texto ',
  'FROM bwcsvload',
  'WHERE Representante <> "" ',
  'AND CodRepresentante=0',
  '']);
  //
  if QrCadVerif.RecordCount > 0 then
    PageControl1.ActivePageIndex := 3;
end;

function TFmLoadCSV_01.CarregaAtual_Qr(Texto, Tabela: String; NovoCodigo: TNovoCodigo): Boolean;
const
  FixCol = 1;
var
  //
  SQLType: TSQLType;
  //
  I, O, Codigo: Integer;
  Nome: String;
  Campos: array of String;
  Valores: array of String;
begin
  Result  := False;
  SQLType := stIns;
  Nome    := EmptyStr;
  for I := 1 to Length(Texto) do
  begin
    O := Ord(Texto[I]);
    if (O > 31) and (O < 255) then
      Nome := Nome + Texto[I];
  end;
  if Nome = EmptyStr then
  begin
    Result := True;
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq2, Dmod.MyDB, [
  'SELECT ' + FFldID + ', ' + FFldNome,
  'FROM ' + Tabela,
  'WHERE ' + FFldNome + ' ="' + Nome + '"',
  '']);
  if QrPsq2.RecordCount = 0 then
  begin
    case NovoCodigo of
      ncControle: Codigo := UMyMod.BuscaNovoCodigo_Int(
        Dmod.QrAux, Tabela, FFldID, [], [], stIns, 0, siPositivo, nil);
      ncGerlSeq1: Codigo := UMyMod.BPGS1I32(
        Tabela, FFldID, '', '', tsPos, stIns, 0);
      else (*ncIdefinido: CtrlGeral: ;*)
      begin
        Geral.MB_Erro('Tipo de obten��o de novo c�digo indefinido!');
        Halt(0);
        Exit;
      end;
    end;

    (*
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, FTabela, False, [
    'Nome', FCamposExtras], [
    'Codigo'], [
    Nome, FValorsExtras], [
    Codigo], True);
    *)
    Dmod.MyDB.Execute(Geral.ATS([
      'INSERT INTO ' + FTabela,
      'SET Codigo=' + Geral.FF0(Codigo),
      ', Nome="' + Nome + '"',
      FSQLInsExtra,
      '']));
    Result := True;
  end else
    Result := True;
end;

procedure TFmLoadCSV_01.CarregaAtual_SG(Codigo, Lin: Integer;
  Grade: TStringGrid);
const
  FixCol = 1;
var
  NO_BWStatus, Reabertura, Data, Hora, CadastroDaProposta, Cliente, Estado,
  Pregao, Lote, ItemLt, Produto, Unidade, Portal, NProcesso, Colocacao, Estah,
  Amostras, PrazoEntrega, Responsaveis, Empresa, EmpresaGanhadora, MarcaDoGanhador,
  Segmento, Representante, Mes, Ano: String;
  //
  BWStatus, Entidade, CodPortal, CodEmpresa, CodEmpresaGanhadora,
  CodMarcaDoGanhador, CodSegmento, CodRepresentante, Class_: Integer;
  //
  Quantidade, Estimativa, Estimativa2, ValorNoPregao, ValorGanhador: Double;
  //
  SQLType: TSQLType;
  //
  x1, x2, x3: String;
  p, I, Q, cUF: Integer;
  Texto: String;
  EhLote, MostraErros: Boolean;
  Linha: Integer;
begin
  SQLType        := stIns;
  MostraErros    := False;
  //Codigo         := ;
  //Lin          := ;
  BWStatus       := 0;
  NO_BWStatus    := Grade.Cells[FixCol + 00, Lin];
  if NO_BWStatus <> EmptyStr then
  begin
    x1             := Trim(Grade.Cells[FixCol + 01, Lin]);
    p              := pos('-', x1);
    if p > 0 then
    begin
      x2             := Trim(Copy(x1, 1, p-1));
      x3             := Trim(Copy(x1, p + 1));
      Reabertura     := Trim(Geral.FDT(Geral.ValidaDataBr(x2, True, True), 1) + ' ' + x3);
    end else
      Reabertura := '0000-00-00 00:00:00';
    //
    x1             := Grade.Cells[FixCol + 02, Lin];
    if Geral.SoNumero_TT(x1) = x1 then
      Data           := Geral.FDT(Geral.IMV(x1), 1)
    else
      Data           := Geral.FDT(Geral.ValidaDataBr(x1, True, True), 1);
    Hora           := Geral.FDT(Geral.DMV(Trim(Grade.Cells[FixCol + 03, Lin])), 100);
    if Hora = EmptyStr then
      Hora := '00:00:00';
    CadastroDaProposta:= Trim(Grade.Cells[FixCol + 04, Lin]);
    Entidade       := 0;
    Cliente        := Trim(Grade.Cells[FixCol + 05, Lin]);
    Estado         := Uppercase(Trim(Grade.Cells[FixCol + 06, Lin]));
    if Estado <> EmptyStr then
    begin
      cUF := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(Estado);
      if cUF = 0 then
      begin
        Geral.MB_Aviso('UF inv�lida: ' + Estado);
        Exit;
      end;
    end;
    Pregao         := Trim(Grade.Cells[FixCol + 07, Lin]);
    Texto          := Trim(Grade.Cells[FixCol + 08, Lin]);
    //if Lin = ? then
    begin
      Lote   := '';
      ItemLt := '';
      EhLote := True;
      Q := 0;
      for I := 1 to Length(Texto) do
      begin
        if CharInSet(Texto[I], ['0'..'9']) then
        begin
          if EhLote then
            Lote := Lote + Texto[I]
          else
            ItemLt := ItemLt + Texto[I];
        end else
        begin
          EhLote := False;
          if (CharInSet(Texto[I], (['a'..'z'])))
            or (CharInSet(Texto[i], (['A'..'Z'])))
              or (CharInSet(Texto[i], (['0'..'9']))) then
                  ItemLt := ItemLt + Texto[I];
        end;
      end;
    end;
    Produto        := Trim(Grade.Cells[FixCol + 09, Lin]);
    Unidade        := Trim(Grade.Cells[FixCol + 10, Lin]);
    Quantidade     := Geral.DMV(Trim(Grade.Cells[FixCol + 11, Lin]));
    Estimativa     := Geral.DMV(Trim(Grade.Cells[FixCol + 12, Lin]));
    Estimativa2    := Geral.DMV(Trim(Grade.Cells[FixCol + 13, Lin]));
    CodPortal      := 0;
    Portal         := Trim(Grade.Cells[FixCol + 14, Lin]);
    NProcesso      := Trim(Grade.Cells[FixCol + 15, Lin]);
    Colocacao      := Trim(Grade.Cells[FixCol + 16, Lin]);
    Estah          := Trim(Grade.Cells[FixCol + 17, Lin]);
    Amostras       := Trim(Grade.Cells[FixCol + 18, Lin]);
    PrazoEntrega   := Trim(Grade.Cells[FixCol + 19, Lin]);
    Responsaveis   := Trim(Grade.Cells[FixCol + 20, Lin]);
    CodEmpresa     := 0;
    Empresa        := Trim(Grade.Cells[FixCol + 21, Lin]);
    CodEmpresaGanhadora:= 0;
    EmpresaGanhadora:= StringReplace(Trim(Grade.Cells[FixCol + 22, Lin]), '?', '', [rfReplaceAll]);
    //if Lin = 1289 then
    begin
      Texto := '';
      Q := 0;
      for I := 1 to length(EmpresaGanhadora) do
      begin
        if Ord(EmpresaGanhadora[I]) < 255 then
          Texto := Texto + EmpresaGanhadora[I]
        else
          Q := Q + 1;
      end;
      if Q > 0 then
      begin
(*
        Geral.MB_Info(EmpresaGanhadora + sLineBreak + Texto + sLineBreak +
        'Erros: ' + Geral.FF0(Q));
*)
        MeAvisos.Lines.Add(EmpresaGanhadora + sLineBreak + Texto + sLineBreak +
        'Erros: ' + Geral.FF0(Q));
        MostraErros := True;
      end;
      EmpresaGanhadora := Texto;
    end;
    CodMarcaDoGanhador:= 0;
    MarcaDoGanhador:= Trim(Grade.Cells[FixCol + 23, Lin]);
    CodSegmento    := 0;
    Segmento       := Trim(Grade.Cells[FixCol + 24, Lin]);
    CodRepresentante:= 0;
    Representante  := Trim(Grade.Cells[FixCol + 25, Lin]);
    ValorNoPregao  := Geral.DMV(Trim(Grade.Cells[FixCol + 26, Lin]));
    ValorGanhador  := Geral.DMV(Trim(Grade.Cells[FixCol + 27, Lin]));
    Class_         := Geral.IMV(Trim(Grade.Cells[FixCol + 28, Lin]));
    Mes            := Trim(Grade.Cells[FixCol + 29, Lin]);
    Ano            := Trim(Grade.Cells[FixCol + 30, Lin]);
    //
    //if
    Linha := Lin + 1;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwcsvload', False, [
    'Linha', 'BWStatus', 'NO_BWStatus',
    'Reabertura', 'Data', 'Hora',
    'CadastroDaProposta', 'Entidade', 'Cliente',
    'Estado', 'Pregao', 'Lote', 'ItemLt',
    'Produto', 'Unidade', 'Quantidade',
    'Estimativa', 'Estimativa2', 'CodPortal',
    'Portal', 'NProcesso', 'Colocacao',
    'Estah', 'Amostras', 'PrazoEntrega',
    'Responsaveis', 'CodEmpresa', 'Empresa',
    'CodEmpresaGanhadora', 'EmpresaGanhadora', 'CodMarcaDoGanhador',
    'MarcaDoGanhador', 'CodSegmento', 'Segmento',
    'CodRepresentante', 'Representante', 'ValorNoPregao',
    'ValorGanhador', 'Class_', 'Mes',
    'Ano'], [
    'Codigo'], [
    Linha, BWStatus, NO_BWStatus,
    Reabertura, Data, Hora,
    CadastroDaProposta, Entidade, Cliente,
    Estado, Pregao, Lote, ItemLt,
    Produto, Unidade, Quantidade,
    Estimativa, Estimativa2, CodPortal,
    Portal, NProcesso, Colocacao,
    Estah, Amostras, PrazoEntrega,
    Responsaveis, CodEmpresa, Empresa,
    CodEmpresaGanhadora, EmpresaGanhadora, CodMarcaDoGanhador,
    MarcaDoGanhador, CodSegmento, Segmento,
    CodRepresentante, Representante, ValorNoPregao,
    ValorGanhador, Class_, Mes,
    Ano], [
    Codigo], True);
  end;
  if MostraErros then
    PageControl1.ActivePageIndex := 0;
end;

procedure TFmLoadCSV_01.CarregaAtual_SL(Codigo, Linha: Integer; Lista: TStringList);
var
  NO_BWStatus, Reabertura, Data, Hora, CadastroDaProposta, Cliente, Estado,
  Pregao, Lote, Produto, Unidade, Portal, NProcesso, Colocacao, Estah, Amostras,
  PrazoEntrega, Responsaveis, Empresa, EmpresaGanhadora, MarcaDoGanhador,
  Segmento, Representante, Mes, Ano: String;
  //
  BWStatus, Entidade, CodPortal, CodEmpresa, CodEmpresaGanhadora,
  CodMarcaDoGanhador, CodSegmento, CodRepresentante, Class_: Integer;
  //
  Quantidade, Estimativa, Estimativa2, ValorNoPregao, ValorGanhador: Double;
  //
  SQLType: TSQLType;
  //
  x1, x2, x3: String;
  p: Integer;
begin
  SQLType        := stIns;
  //Codigo         := ;
  //Linha          := ;
  BWStatus       := 0;
  NO_BWStatus    := Lista[00];
  x1             := Trim(Lista[01]);
  p              := pos('-', x1);
  if p > 0 then
  begin
    x2             := Trim(Copy(x1, 1, p-1));
    x3             := Trim(Copy(x1, p + 1));
    Reabertura     := Trim(Geral.FDT(Geral.ValidaDataBr(x2, True, True), 1) + ' ' + x3);
  end else
    Reabertura := '0000-00-00 00:00:00';
  Data           := Geral.FDT(Geral.ValidaDataBr(Lista[02], True, True), 1);
  Hora           := Trim(Lista[03]);
  if Hora = EmptyStr then
    Hora := '00:00:00';
  CadastroDaProposta:= Trim(Lista[04]);
  Entidade       := 0;
  Cliente        := Trim(Lista[05]);
  Estado         := Trim(Lista[06]);
  Pregao         := Trim(Lista[07]);
  Lote           := Trim(Lista[08]);
  Produto        := Trim(Lista[09]);
  Unidade        := Trim(Lista[10]);
  Quantidade     := Geral.DMV(Trim(Lista[11]));
  Estimativa     := Geral.DMV(Trim(Lista[12]));
  Estimativa2    := Geral.DMV(Trim(Lista[13]));
  CodPortal      := 0;
  Portal         := Trim(Lista[14]);
  NProcesso      := Trim(Lista[15]);
  Colocacao      := Trim(Lista[16]);
  Estah          := Trim(Lista[17]);
  Amostras       := Trim(Lista[18]);
  PrazoEntrega   := Trim(Lista[19]);
  Responsaveis   := Trim(Lista[20]);
  CodEmpresa     := 0;
  Empresa        := Trim(Lista[21]);
  CodEmpresaGanhadora:= 0;
  EmpresaGanhadora:= Trim(Lista[22]);
  CodMarcaDoGanhador:= 0;
  MarcaDoGanhador:= Trim(Lista[23]);
  CodSegmento    := 0;
  Segmento       := Trim(Lista[24]);
  CodRepresentante:= 0;
  Representante  := Trim(Lista[25]);
  ValorNoPregao  := Geral.DMV(Trim(Lista[26]));
  ValorGanhador  := Geral.DMV(Trim(Lista[27]));
  Class_         := Geral.IMV(Trim(Lista[28]));
  Mes            := Trim(Lista[29]);
  Ano            := Trim(Lista[30]);
  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwcsvload', False, [
  'Linha', 'BWStatus', 'NO_BWStatus',
  'Reabertura', 'Data', 'Hora',
  'CadastroDaProposta', 'Entidade', 'Cliente',
  'Estado', 'Pregao', 'Lote',
  'Produto', 'Unidade', 'Quantidade',
  'Estimativa', 'Estimativa2', 'CodPortal',
  'Portal', 'NProcesso', 'Colocacao',
  'Estah', 'Amostras', 'PrazoEntrega',
  'Responsaveis', 'CodEmpresa', 'Empresa',
  'CodEmpresaGanhadora', 'EmpresaGanhadora', 'CodMarcaDoGanhador',
  'MarcaDoGanhador', 'CodSegmento', 'Segmento',
  'CodRepresentante', 'Representante', 'ValorNoPregao',
  'ValorGanhador', 'Class_', 'Mes',
  'Ano'], [
  'Codigo'], [
  Linha, BWStatus, NO_BWStatus,
  Reabertura, Data, Hora,
  CadastroDaProposta, Entidade, Cliente,
  Estado, Pregao, Lote,
  Produto, Unidade, Quantidade,
  Estimativa, Estimativa2, CodPortal,
  Portal, NProcesso, Colocacao,
  Estah, Amostras, PrazoEntrega,
  Responsaveis, CodEmpresa, Empresa,
  CodEmpresaGanhadora, EmpresaGanhadora, CodMarcaDoGanhador,
  MarcaDoGanhador, CodSegmento, Segmento,
  CodRepresentante, Representante, ValorNoPregao,
  ValorGanhador, Class_, Mes,
  Ano], [
  Codigo], True);
end;

procedure TFmLoadCSV_01.CarregaFabricante_x_Marca();
var
  Col, Row, I, Codigo, Controle, GraFabMCd: Integer;
  sGraFabCad, sGraFabMCd, Nome: String;
begin
  Nome := '';
  PB1.Position := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT DISTINCT EmpresaGanhadora, MarcaDoGanhador',
  'FROM bwcsvload',
  'WHERE MarcaDoGanhador <> ""',
  'AND MarcaDoGanhador <> "0"',
  'ORDER BY EmpresaGanhadora, MarcaDoGanhador',
  '']);
  PB1.Max := Query.RecordCount;
  while not Query.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    sGraFabCad := Query.FieldByName('EmpresaGanhadora').AsString;
    sGraFabMCd := Query.FieldByName('MarcaDoGanhador').AsString;
    //
    if (sGraFabCad <> EmptyStr) and (sGraFabMCd <> EmptyStr) and (sGraFabCad <> '0') then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
      'SELECT Codigo, Nome  ',
      'FROM grafabcad ',
      'WHERE Nome = "' + sGraFabCad + '"',
      '']);
      Codigo := QrPesq.FieldByName('Codigo').AsInteger;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrPsq2, Dmod.MyDB, [
      'SELECT Codigo, Nome  ',
      'FROM grafabmcd ',
      'WHERE Nome = "' + sGraFabMCd + '"',
      '']);
      GraFabMCd := QrPsq2.FieldByName('Codigo').AsInteger;
      //
      Controle := UMyMod.BuscaEmLivreY_Def('grafabmar', 'Controle', stIns, Controle);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'grafabmar', False, [
      'Codigo', 'Nome', 'GraFabMCd'], [
      'Controle'], [
      Codigo, Nome, GraFabMCd], [
      Controle], True) then ;
      //
    end;
    //
    Query.Next;
  end;
end;

procedure TFmLoadCSV_01.DivideMulNomes(Texto: String; MyArr: TMyArrString);
var
  I, N, J: Integer;
  Linha, Divisor: String;
  //
  Nome: String;
begin
  J := 0;
  SetLength(MyArr, J);
  Divisor := '/';
  //
  if Divisor <> EmptyStr then
  begin
    FLstLin1 := Geral.Explode3(Texto, Divisor);
    //
    for N := 0 to FLstLin1.Count - 1 do
    begin
      Nome  := Trim(FLstLin1[N]);
      if Nome <> EmptyStr then
      begin
        J := J + 1;
        SetLength(MyArr, J);
        MyArr[J-1] := Nome;
      end;
    end;
  end;
end;

procedure TFmLoadCSV_01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLoadCSV_01.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 2;
  FFldID := 'Codigo';
  FFldNome := 'Nome';
  FTabela := '';
  FSQLInsExtra := '';
end;

procedure TFmLoadCSV_01.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLoadCSV_01.IncluiBWLiciCab();
var
  AberturaDt, AberturaHr, ReabertuDt, ReabertuHr, LimCadPpDt, LimCadPpHr, UF, Pregao: String;
  Codigo, BWStatus, Entidade, Cliente, Linha: Integer;
  SQLType: TSQLType;
  //
  Texto: String;
begin
  SQLType        := stIns; //ImgTipo.SQLType;
  //
  FCodigo        := FCodigo + 1;
  Codigo         := FCodigo;
  BWStatus       := QrBWCsvLoadBWStatus.Value;
  AberturaDt     := Geral.FDT(QrBWCsvLoadData.Value, 1);
  AberturaHr     := Geral.FDT(QrBWCsvLoadHora.Value, 100);
  if AberturaHr = EmptyStr then
    AberturaHr := '00:00:00';
  ReabertuDt     := Geral.FDT(QrBWCsvLoadReabertura.Value, 1);
  ReabertuHr     := Geral.FDT(QrBWCsvLoadReabertura.Value, 100);
  if ReabertuHr = EmptyStr then
    ReabertuHr := '00:00:00';
  Texto          := QrBWCsvLoadCadastroDaProposta.Value;
  Texto          := Geral.SoNumeroEData_TT(Texto);
  if Texto <> EmptyStr then
  begin
    LimCadPpDt     := Copy(Texto, 1, 10);
    LimCadPpDt     := Geral.FDT(Geral.ValidaDataBR(LimCadPpDt, True, False), 1);
    LimCadPpHr     := Copy(Texto, 11);
    if LimCadPpHr = EmptyStr then
      LimCadPpHr := '00:00:00';
  end else
  begin
    LimCadPpDt     := '0000-00-00';
    LimCadPpHr     := '00:00:00';
  end;
  Entidade       := QrBWCsvLoadEntidade.Value;
  Cliente        := QrBWCsvLoadCodCliente.Value;
  UF             := QrBWCsvLoadEstado.Value;
  Pregao         := QrBWCsvLoadPregao.Value;
  //
  Linha          := QrBWCsvLoadLinha.Value;
  //Result :=
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwlicicab', False, [
  'BWStatus', 'AberturaDt', 'AberturaHr',
  'ReabertuDt', 'ReabertuHr', 'LimCadPpDt',
  'LimCadPpHr', 'Entidade', 'Cliente',
  'UF', 'Pregao', 'Linha'], [
  'Codigo'], [
  BWStatus, AberturaDt, AberturaHr,
  ReabertuDt, ReabertuHr, LimCadPpDt,
  LimCadPpHr, Entidade, Cliente,
  UF, Pregao, Linha], [
  Codigo], True);
end;

procedure TFmLoadCSV_01.IncluiBWLiciIts();
var
  ItemLt: String;
  Codigo, Controle, Conta, Produto, Unidade, Linha: Integer;
  QtdeItem, ValrUnit, ValItem: Double;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := FCodigo;
  Controle       := FControle;
  FConta := FConta + 1;
  Conta          := FConta;
  ItemLt         := QrBWCsvLoadItemLt.Value;
  Produto        := QrBWCsvLoadCodProduto.Value;
  Unidade        := QrBWCsvLoadCodUnidade.Value;
  QtdeItem       := QrBWCsvLoadQuantidade.Value;
  ValrUnit       := QrBWCsvLoadEstimativa.Value;
  ValItem        := QrBWCsvLoadEstimativa2.Value;
  Linha          := QrBWCsvLoadLinha.Value;
  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwliciits', False, [
  'Codigo', 'Controle', 'ItemLt',
  'Produto', 'Unidade', 'QtdeItem',
  'ValrUnit', 'ValItem', 'Linha'], [
  'Conta'], [
  Codigo, Controle, ItemLt,
  Produto, Unidade, QtdeItem,
  ValrUnit, ValItem, Linha], [
  Conta], True);
end;

procedure TFmLoadCSV_01.IncluiBWLiciLot();
var
  Lote, NrProcesso: String;
  Codigo, Controle, BWPortal, Colocacao, Estah, PrzAmostras, PrzEntregas,
  Responsav1, Responsav2, Responsav3, Empresa, VenceFabri, VenceMarca, Segmento,
  Representn, Classifcac, Linha: Integer;
  ValrPregao, ValrVenced(*, QtdeLote, ValLote*): Double;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := FCodigo;
  FControle      := FControle + 1;
  Controle       := FControle;
  Lote           := QrBWCsvLoadLote.Value;
  BWPortal       := QrBWCsvLoadCodPortal.Value;
  NrProcesso     := QrBWCsvLoadNProcesso.Value;
  Colocacao      := Geral.IMV(Geral.SoNumero_TT(QrBWCsvLoadColocacao.Value));
  Estah          := Geral.IMV(Geral.SoNumero_TT(QrBWCsvLoadEstah.Value));
  PrzAmostras    := QrBWCsvLoadCodPrzAmostra.Value;
  PrzEntregas    := QrBWCsvLoadCodPrzEntrega.Value;
  Responsav1     := QrBWCsvLoadCodResponsa1.Value;
  Responsav2     := QrBWCsvLoadCodResponsa2.Value;
  Responsav3     := QrBWCsvLoadCodResponsa3.Value;
  Empresa        := QrBWCsvLoadCodEmpresa.Value;
  VenceFabri     := QrBWCsvLoadCodEmpresaGanhadora.Value;
  VenceMarca     := QrBWCsvLoadCodMarcaDoGanhador.Value;
  Segmento       := QrBWCsvLoadCodSegmento.Value;
  Representn     := QrBWCsvLoadCodRepresentante.Value;
  ValrPregao     := QrBWCsvLoadValorNoPregao.Value;
  ValrVenced     := QrBWCsvLoadValorGanhador.Value;
  Classifcac     := QrBWCsvLoadClass_.Value;
  Linha          := QrBWCsvLoadLinha.Value;
  //QtdeLote       := ;
  //ValLote        := ;

  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwlicilot', False, [
  'Codigo', 'Lote', 'BWPortal',
  'NrProcesso', 'Colocacao', 'Estah',
  'PrzAmostras', 'PrzEntregas', 'Responsav1',
  'Responsav2', 'Responsav3', 'Empresa',
  'VenceFabri', 'VenceMarca', 'Segmento',
  'Representn', 'ValrPregao', 'ValrVenced',
  'Classifcac', 'Linha'(*, 'QtdeLote', 'ValLote'*)], [
  'Controle'], [
  Codigo, Lote, BWPortal,
  NrProcesso, Colocacao, Estah,
  PrzAmostras, PrzEntregas, Responsav1,
  Responsav2, Responsav3, Empresa,
  VenceFabri, VenceMarca, Segmento,
  Representn, ValrPregao, ValrVenced,
  Classifcac, Linha(*, QtdeLote, ValLote*)], [
  Controle], True);
end;

procedure TFmLoadCSV_01.Informa(Texto: String);
begin
  MeAvisos.Text := Geral.FDT(Dmod.DModG_ObtemAgora(), 2) + ' ' + Texto +
    sLineBreak + MeAvisos.Text;
end;

function TFmLoadCSV_01.ObtemNomesResponsaveis(const Texto: String;
  var Responsav1, Responsav2, Responsav3: String): Boolean;
var
  LstLin1: TStringList;
  I, N: Integer;
  Linha, Divisor: String;
  //
  Nome: String;
begin
  Result := False;
  Responsav1 := EmptyStr;
  Responsav2 := EmptyStr;
  Responsav3 := EmptyStr;
  //
  Divisor := '/';
  //
  Linha := Divisor + Texto + Divisor;
  //
  LstLin1 := TStringList.Create;
  try
    LstLin1 := Geral.Explode3(Linha, Divisor);
    //
    I := 0;
    for N := 0 to LstLin1.Count - 1 do
    begin
      Nome  := Trim(LstLin1[N]);
      if Nome <> EmptyStr then
      begin
        I := I + 1;
        case I of
          1: Responsav1 := Nome;
          2: Responsav2 := Nome;
          3: Responsav3 := Nome;
        end;
      end;
    end;
    Result := True;
  finally
    LstLin1.Free;
  end;
end;

procedure TFmLoadCSV_01.SbLoadCSVOthDirClick(Sender: TObject);
var
  Pasta, Arquivo: String;
begin
  Pasta   := 'C:\Dermatek\CSV\';
  Arquivo := 'Planilha de Concorr�ncias - Nova.csv';
  //
  MyObjects.DefineArquivo2(Self, EdLoadCSVArq, Pasta, Arquivo,
    TPriorityPath.ppAtual_Edit);
end;

end.
