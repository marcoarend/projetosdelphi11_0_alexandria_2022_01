unit BWLiHStat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmBWLiHStat = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label6: TLabel;
    EdNivel4: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    EdStatusAnt: TdmkEditCB;
    CBStatusAnt: TdmkDBLookupComboBox;
    QrBWStatusAnt: TMySQLQuery;
    QrBWStatusAntCodigo: TIntegerField;
    QrBWStatusAntNome: TWideStringField;
    DsBWStatusAnt: TDataSource;
    QrBWStatusAtu: TMySQLQuery;
    QrBWStatusAtuCodigo: TIntegerField;
    QrBWStatusAtuNome: TWideStringField;
    DsBWStatusAtu: TDataSource;
    Label9: TLabel;
    EdStatusAtu: TdmkEditCB;
    CBStatusAtu: TdmkDBLookupComboBox;
    Label18: TLabel;
    TPDataHora: TdmkEditDateTimePicker;
    EdDataHora: TdmkEdit;
    MeHistorico: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenBWLiHStat(Nivel4: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    FCodigo, FStatusAnt: Integer;
  end;

  var
  FmBWLiHStat: TFmBWLiHStat;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmBWLiHStat.BtOKClick(Sender: TObject);
var
  DataHora, Nome, Historico: String;
  Codigo, Nivel4, StatusAnt, StatusAtu: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  Nivel4         := EdNivel4.ValueVariant;
  DataHora       := Geral.FDT(TPDataHora.Date, 1) + ' ' + EdDataHora.Text;
  StatusAnt      := EdStatusAnt.ValueVariant;
  StatusAtu      := EdStatusAtu.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Historico      := MeHistorico.Text;
  //
  Nivel4 := UMyMod.BPGS1I32('bwlihstat', 'Nivel4', '', '', tsPos, SQLType, Nivel4);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwlihstat', False, [
  'Codigo', 'DataHora', 'StatusAnt',
  'StatusAtu', 'Nome', 'Historico'], [
  'Nivel4'], [
  Codigo, DataHora, StatusAnt,
  StatusAtu, Nome, Historico], [
  Nivel4], True) then
  begin
    ReopenBWLiHStat(Nivel4);
(*
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdCargo.ValueVariant     := 0;
      CBCargo.KeyValue         := Null;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else*)
    Close;
  end;
end;

procedure TFmBWLiHStat.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBWLiHStat.FormActivate(Sender: TObject);
begin
  //DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmBWLiHStat.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MeHistorico.Align := alClient;
  //
  UnDmkDAC_PF.AbreQuery(QrBWStatusAnt, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBWStatusAtu, Dmod.MyDB);
end;

procedure TFmBWLiHStat.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBWLiHStat.ReopenBWLiHStat(Nivel4: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Nivel4 <> 0 then
      FQrIts.Locate('Nivel4', Nivel4, []);
  end;
end;

end.
