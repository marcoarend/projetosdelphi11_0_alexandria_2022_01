unit UnApp_Tabs;
// App - Custos e Resultados Operacionais
{ Colocar no MyListas:

Uses UnApp_Tabs;


//
function TMyListas.CriaListaTabelas:
      App_Tabs.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
    App_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:

    App_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      App_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      App_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    App_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  App_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms,
  UnInternalConsts, dmkGeral, UnDmkEnums;

type
  TUnApp_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
  end;

//const

var
  App_Tabs: TUnApp_Tabs;

implementation

function TUnApp_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
//////////////////////////// tempor�rio!? //////////////////////////////////////
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWCsvLoad'), '');

///////////////////////////  CADASTROS  ////////////////////////////////////////
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWStatus'), '_lst_sample');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWClientes'), '_lst_sample255');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWProdutos'), '_lst_sample255');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWPortais'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWRespnsvs'), '_lst_sample');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWEmpresas'), '_lst_sample');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWWinners'), '_lst_sample');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWSegmento'), '_lst_sample');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWRepresnt'), '_lst_sample');
    //
    // da Grade:
    MyLinguas.AdTbLst(Lista, False, Lowercase('GraFabCad'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('GraFabMCd'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('GraFabMar'), '');
    // Fim grade!
    //

  /////////////////////////  MOVIMENTO  ////////////////////////////////////////
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWLiciCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWLiciLot'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWLiciIts'), '');
    //
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWLiOrCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWLiOrIts'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWLiOrFld'), '');
    //
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWLiHFCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWLiHFLot'), '');
    //
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWLiHStat'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('BWLiHEsta'), '');
    //

    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnApp_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
{
separador = "|"
}
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('CtrlGeral') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end else
  if Uppercase(Tabela) = Uppercase('GraFabCad') then // Marcas
  begin
    if FListaSQL.Count = 0 then  // evitar erro no blue derm! 2012-01-16
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|""');
  end else
  if Uppercase(Tabela) = Uppercase('GraFabMCd') then // Marcas
  begin
    if FListaSQL.Count = 0 then  // evitar erro no blue derm! 2012-01-16
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|""');
  end else
  if Uppercase(Tabela) = Uppercase('GraFabMar') then // Marcas
  begin
    if FListaSQL.Count = 0 then  // evitar erro no blue derm! 2012-01-16
    FListaSQL.Add('Codigo|Controle|GraFabMCd');
    FListaSQL.Add('0|0|0');
  end else
end;

function TUnApp_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('BWStatus') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"ND"');
    FListaSQL.Add( '1024|"A Realizar"');
    FListaSQL.Add( '2048|"Pendente"');
    FListaSQL.Add( '3072|"Realizada"');
    FListaSQL.Add( '4096|"Declinada"');
    FListaSQL.Add( '5120|"Suspensa"');
    FListaSQL.Add( '6144|"Anulada"');
    FListaSQL.Add( '7168|"Cancelada"');
    FListaSQL.Add( '8192|"Fracassada"');
    FListaSQL.Add( '9216|"Revogada"');
    FListaSQL.Add('10240|"Finalizada"');
  end else
  if Uppercase(Tabela) = Uppercase('BWLiOrFld') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|Campo|Nome');
    FListaSQL.Add( '0|""|""');
    FListaSQL.Add( '1|"NO_BWStatus"|"Descri��o do Status"');
    FListaSQL.Add( '2|"ReabertuDt"|"Reabertura Data"');
    FListaSQL.Add( '3|"ReabertuHr"|"Reabertura Hora"');
    FListaSQL.Add( '4|"AberturaDt"|"Abertura Data"');
    FListaSQL.Add( '5|"AberturaHr"|"Abertura Hora"');
    FListaSQL.Add( '6|"LimCadPpDt"|"Limite de Cadadastro da Proposta Data"');
    FListaSQL.Add( '7|"LimCadPpHr"|"Limite de Cadadastro da Proposta Hora"');
    FListaSQL.Add( '8|"NO_Cliente"|"Nome do Cliente"');
    FListaSQL.Add( '9|"UF"|"UF"');
    FListaSQL.Add('10|"Pregao"|"Preg�o"');
    FListaSQL.Add('11|"Lote"|"Lote"');
    FListaSQL.Add('12|"ItemLt"|"Item do Lote"');
    FListaSQL.Add('13|"NO_Produto"|"Nome do Produto"');
    FListaSQL.Add('14|"NO_SIGLA"|"Sigla"');
    FListaSQL.Add('15|"QtdeItem"|"Quantidade Item"');
    FListaSQL.Add('16|"ValrUnit"|"Valor Unitario"');
    FListaSQL.Add('17|"ValItem"|"Valor do Item"');
    FListaSQL.Add('18|"NO_BWPortal"|"Nome do Portal"');
    FListaSQL.Add('19|"NrProcesso"|"N�mero do Processo"');
    FListaSQL.Add('20|"Colocacao"|"Coloca��o"');
    FListaSQL.Add('21|"Estah"|"Est�"');
    FListaSQL.Add('22|"NO_PrzAmostras"|"Nome prazo Amostras"');
    FListaSQL.Add('23|"NO_PrzEntregas"|"Nome prazo Entregas"');
    FListaSQL.Add('24|"NO_Responsav1"|"Nome do Responsave1"');
    FListaSQL.Add('25|"AmstrConvocDt_TXT"|"Amostra Convoca��o Data"');
    FListaSQL.Add('26|"AmstrConvocHr"|"Amostra Convoca��o Hora"');
    FListaSQL.Add('27|"AmstrLimEntrDt_TXT"|"Amostra Limite de Entrega Data"');
    FListaSQL.Add('28|"AmstrLimEntrHr"|"Amostra Limite de Entrega Hora"');
    FListaSQL.Add('29|"AmstrEntrReaDt_TXT"|"Amostra Entrega Realizada Data"');
    FListaSQL.Add('30|"AmstrEntrReaHr"|"Amostra Entrega Realizada Hora"');
    FListaSQL.Add('31|"NO_Responsav2"|"Nome Respons�vel 2"');
    FListaSQL.Add('32|"NO_Responsav3"|"Nome Respons�vel 3"');
    FListaSQL.Add('33|"NO_Empresa"|"Nome da Empresa"');
    FListaSQL.Add('34|"NO_GraFabCad"|"Nome do Fabricane"');
    FListaSQL.Add('35|"No_GraFabMCd"|"Nome da Marca do Fabricante"');
    FListaSQL.Add('36|"NO_Segmento"|"Nome do Segmento"');
    FListaSQL.Add('37|"NO_Representn"|"Nome do Representante"');
    FListaSQL.Add('38|"ValrPregao"|"Valor do Preg�o"');
    FListaSQL.Add('39|"ValrVenced"|"Valor Vencedor"');
    FListaSQL.Add('40|"Classifcac"|"Classifca��o"');
    FListaSQL.Add('41|"QtdeLote"|"Quantidade no Lote"');
    FListaSQL.Add('42|"ValLote"|"Valor do Lote"');
    FListaSQL.Add('43|"QtdeTotal"|"Quantidade Total"');
    FListaSQL.Add('44|"ValTotal"|"Valor Total"');
    FListaSQL.Add('45|"Entidade"|"ID da Entidade"');
    FListaSQL.Add('46|"Cliente"|"ID do Cliente"');
    FListaSQL.Add('47|"BWStatus"|"ID do Status"');
    FListaSQL.Add('48|"Codigo"|"ID da Licita��o"');
    FListaSQL.Add('49|"Controle"|"ID do lote"');
    FListaSQL.Add('50|"Conta"|"ID do Item"');
    FListaSQL.Add('51|"Produto"|"ID do Produto"');
    FListaSQL.Add('52|"BWPortal"|"ID do Portal"');
    FListaSQL.Add('53|"PrzAmostras"|"ID do Prazo das Amostras"');
    FListaSQL.Add('54|"PrzEntregas"|"ID do Prazo das Entregas"');
    FListaSQL.Add('55|"Responsav1"|"ID do Respons�ve1 1"');
    FListaSQL.Add('56|"Responsav2"|"ID do Respons�ve1 2"');
    FListaSQL.Add('57|"Responsav3"|"ID do Respons�ve1 3"');
    FListaSQL.Add('58|"Unidade"|"ID da Unidade"');
    FListaSQL.Add('59|"Empresa"|"ID da Empresa"');
    FListaSQL.Add('60|"VenceFabri"|"ID do Fabricante Vencedor"');
    FListaSQL.Add('61|"Nome"|"Descri��o Licita��o"');
    FListaSQL.Add('62|"VenceMarca"|"ID da Marca do Vencedor"');
    FListaSQL.Add('63|"Segmento"|"ID do Segmento"');
    FListaSQL.Add('64|"Representn"|"ID do Representante"');
  end;
end;


function TUnApp_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('BWCsvLoad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Linha';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('BWLiciCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('BWLiciLot') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('BWLiciIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('BWLiOrCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('BWLiOrIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('BWLiOrFld') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('BWLiHFCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('BWLiHFLot') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('BWLiHStat') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Nivel4';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('BWLiHEsta') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Nivel4';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('BWPortais') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('GraFabCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'CodUsu';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('GraFabMCd') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Nome';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('GraFabMar') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
end;

function TUnApp_Tabs.CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TUnApp_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('BWCsvLoad') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Linha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BWStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NO_BWStatus';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Reabertura';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Hora';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CadastroDaProposta';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodCliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estado';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pregao';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Lote';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItemLt';
      FRCampos.Tipo       := 'varchar(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodProduto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Produto';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUnidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Unidade';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Quantidade';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estimativa';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estimativa2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodPortal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Portal';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NProcesso';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Colocacao';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estah';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodPrzAmostra';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Amostras';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodPrzEntrega';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrazoEntrega';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodResponsa1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodResponsa2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodResponsa3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Responsaveis';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodEmpresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodEmpresaGanhadora';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmpresaGanhadora';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodMarcaDoGanhador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MarcaDoGanhador';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodSegmento';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Segmento';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodRepresentante';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Representante';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorNoPregao';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorGanhador';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Class_';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Mes';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ano';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('BWLiciCab') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BWStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AberturaDt';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AberturaHr';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReabertuDt';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReabertuHr';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimCadPpDt';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimCadPpHr';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UF';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pregao';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdeTotal';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValTotal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome'; // aqui de prop�sito
      FRCampos.Tipo       := 'varchar(512)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      New(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Linha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('BWLiciLot') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Lote';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BWPortal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NrProcesso';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Colocacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estah';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrzAmostras';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrzEntregas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Responsav1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Responsav2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Responsav3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VenceFabri';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VenceMarca';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Segmento';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Representn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValrPregao';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValrVenced';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Classifcac';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdeLote';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValLote';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Linha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AmstrConvocDt';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AmstrConvocHr';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AmstrLimEntrDt';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AmstrLimEntrHr';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AmstrEntrReaDt';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AmstrEntrReaHr';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('BWLiciIts') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItemLt';
      FRCampos.Tipo       := 'varchar(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Produto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Unidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdeItem';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValrUnit';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValItem';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Linha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('BWLiOrCab') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('BWLiOrIts') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Campo';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Direcao';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  //  0 Indefinido, 1 DESC e 2 ASC
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('BWLiOrFld') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Campo';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('BWLiHFCab') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Historico';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('BWLiHFLot') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Historico';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('BWLiHStat') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'Nivel4';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StatusAnt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StatusAtu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Historico';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('BWLiHEsta') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
(*
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'Nivel4';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstahAnt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstahAtu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Historico';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('BWPortais') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'URL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('GraFabCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('GraFabMCd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('GraFabMar') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraFabMCd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
  except
    raise;
    Result := False;
  end;
end;

function TUnApp_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleSim;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'GraFabCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraFabMar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
  except
    raise;
    Result := False;
  end;
end;

function TUnApp_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  // TABELA : perfjan
  // LIC-TACAO-001 :: Cadastro de Licita��o
  New(FRJanelas);
  FRJanelas.ID        := 'LIC-TACAO-001';
  FRJanelas.Nome      := 'FmBWLiciCab';
  FRJanelas.Descricao := 'Cadastro de Licita��o';
  FLJanelas.Add(FRJanelas);
  //
  // LIC-TACAO-002 :: Lote de Licita��o
  New(FRJanelas);
  FRJanelas.ID        := 'LIC-TACAO-002';
  FRJanelas.Nome      := 'FmBWLiciLot';
  FRJanelas.Descricao := 'Lote de Licita��o';
  FLJanelas.Add(FRJanelas);
  //
  // LIC-TACAO-003 :: Item de Licita��o
  New(FRJanelas);
  FRJanelas.ID        := 'LIC-TACAO-003';
  FRJanelas.Nome      := 'FmBWLiciIts';
  FRJanelas.Descricao := 'Item de Licita��o';
  FLJanelas.Add(FRJanelas);
  //
  // LIC-TACAO-004 :: Panorama de Licita��es
  New(FRJanelas);
  FRJanelas.ID        := 'LIC-TACAO-004';
  FRJanelas.Nome      := 'FmBWLiciPan';
  FRJanelas.Descricao := 'Panorama de Licita��es';
  FLJanelas.Add(FRJanelas);
  //
  // LIC-TACAO-005 :: Hist�rico de Status
  New(FRJanelas);
  FRJanelas.ID        := 'LIC-TACAO-005';
  FRJanelas.Nome      := 'FmBWLiHStat';
  FRJanelas.Descricao := 'Hist�rico de Status';
  FLJanelas.Add(FRJanelas);
  //
  // LIC-TACAO-006 :: Hist�rico de Posi��o
  New(FRJanelas);
  FRJanelas.ID        := 'LIC-TACAO-006';
  FRJanelas.Nome      := 'FmBWLiHEsta';
  FRJanelas.Descricao := 'Hist�rico de Posi��o';
  FLJanelas.Add(FRJanelas);
  //
  // LIC-TACAO-007 :: Conclus�o do Preg�o
  New(FRJanelas);
  FRJanelas.ID        := 'LIC-TACAO-007';
  FRJanelas.Nome      := 'FmBWLiHFCab';
  FRJanelas.Descricao := 'Conclus�o do Preg�o';
  FLJanelas.Add(FRJanelas);
  //
  // LIC-TACAO-008 :: Conclus�o do Lote
  New(FRJanelas);
  FRJanelas.ID        := 'LIC-TACAO-008';
  FRJanelas.Nome      := 'FmBWLiHFLot';
  FRJanelas.Descricao := 'Conclus�o do Lote';
  FLJanelas.Add(FRJanelas);
  //
  // LIC-TACAO-009 :: Cadastro de Ordena��o do Panorama
  New(FRJanelas);
  FRJanelas.ID        := 'LIC-TACAO-009';
  FRJanelas.Nome      := 'FmBWLiOrCab';
  FRJanelas.Descricao := 'Cadastro de Ordena��o do Panorama';
  FLJanelas.Add(FRJanelas);
  //
  // LIC-TACAO-010 :: Item de Ordena��o do Panorama
  New(FRJanelas);
  FRJanelas.ID        := 'LIC-TACAO-010';
  FRJanelas.Nome      := 'FmBWLiOrIts';
  FRJanelas.Descricao := 'Item de Ordena��o do Panorama';
  FLJanelas.Add(FRJanelas);
  //
  // LIC-TACAO-011 :: Campos de Ordena��o do Panorama
  New(FRJanelas);
  FRJanelas.ID        := 'LIC-TACAO-011';
  FRJanelas.Nome      := 'FmBWLiOrFld';
  FRJanelas.Descricao := 'Campos de Ordena��o do Panorama';
  FLJanelas.Add(FRJanelas);
  //
  // da Grade!
  // PRD-GERAL-003 :: Cadastro de Fabricantes
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-GERAL-003';
  FRJanelas.Nome      := 'FmGraFabCad';
  FRJanelas.Descricao := 'Cadastro de Fabricantes';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-GERAL-004 :: Cadastro de Marca
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-GERAL-004';
  FRJanelas.Nome      := 'FmGraFabMar';
  FRJanelas.Descricao := 'Cadastro de Marca';
  FLJanelas.Add(FRJanelas);
  // Fim Grade
  //
  // LOA-CSV01-001 :: Carrega Arquivo CSV
  New(FRJanelas);
  FRJanelas.ID        := 'LOA-CSV01-001';
  FRJanelas.Nome      := 'FmLoadCSV_01';
  FRJanelas.Descricao := 'Carrega Arquivo CSV';
  FLJanelas.Add(FRJanelas);
  //
  // URL-PORTA-001 :: Cadastro de Portais WEB
  New(FRJanelas);
  FRJanelas.ID        := 'URL-PORTA-001';
  FRJanelas.Nome      := 'FmBWPortais';
  FRJanelas.Descricao := 'Cadastro de Portais WEB';
  FLJanelas.Add(FRJanelas);
  //
  // FER-OPCAO-002 :: Op��es Espec�ficas do Aplicativo
  New(FRJanelas);
  FRJanelas.ID        := 'FER-OPCAO-002';
  FRJanelas.Nome      := 'FmOpcoesBW';
  FRJanelas.Descricao := 'Op��es Espec�ficas do Aplicativo';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
