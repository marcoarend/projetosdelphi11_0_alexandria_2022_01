unit BWLiciPan;

interface

uses
  Winapi.Windows, Winapi.Messages, Math,
  System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, mySQLDbTables, Vcl.Buttons, Vcl.StdCtrls, Data.DB,  Vcl.ComCtrls,
  dmkCheckGroup, dmkGeral, UnInternalConsts, dmkCompoStore, UnGrl_Vars,
  UnDmkEnums, Vcl.DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  //TDBgrid=Class(DBGrids.TDBGrid)
  THackDBGrid = class(Vcl.DBGrids.TDBGrid);
  //THackEditor = class(Vcl.Mask.TCustomMaskEdit);
  TDBGrid = class(Vcl.DBGrids.TDBGrid)
  private
     FColResize: TNotifyEvent;
     procedure ColWidthsChanged; override;
  protected
     property OnColResize: TNotifyEvent read FColResize Write FColResize;
  end;
  TFmBWLiciPan = class(TForm)
    Panel1: TPanel;
    DBGPan: TDBGrid;
    QrBWLiciPan: TMySQLQuery;
    DsBWLiciPan: TDataSource;
    QrBWLiciPanNO_Produto: TWideStringField;
    QrBWLiciPanCodigo: TIntegerField;
    QrBWLiciPanControle: TIntegerField;
    QrBWLiciPanConta: TIntegerField;
    QrBWLiciPanItemLt: TWideStringField;
    QrBWLiciPanProduto: TIntegerField;
    QrBWLiciPanUnidade: TIntegerField;
    QrBWLiciPanQtdeItem: TFloatField;
    QrBWLiciPanValrUnit: TFloatField;
    QrBWLiciPanValItem: TFloatField;
    QrBWLiciPanLinha: TIntegerField;
    QrBWLiciPanNO_BWPortal: TWideStringField;
    QrBWLiciPanNO_Empresa: TWideStringField;
    QrBWLiciPanNO_PrzAmostras: TWideStringField;
    QrBWLiciPanNO_PrzEntregas: TWideStringField;
    QrBWLiciPanNO_Responsav1: TWideStringField;
    QrBWLiciPanNO_Responsav2: TWideStringField;
    QrBWLiciPanNO_Responsav3: TWideStringField;
    QrBWLiciPanNo_GraFabMCd: TWideStringField;
    QrBWLiciPanNO_Segmento: TWideStringField;
    QrBWLiciPanNO_Representn: TWideStringField;
    QrBWLiciPanLote: TWideStringField;
    QrBWLiciPanBWPortal: TIntegerField;
    QrBWLiciPanNrProcesso: TWideStringField;
    QrBWLiciPanColocacao: TIntegerField;
    QrBWLiciPanEstah: TIntegerField;
    QrBWLiciPanPrzAmostras: TIntegerField;
    QrBWLiciPanPrzEntregas: TIntegerField;
    QrBWLiciPanResponsav1: TIntegerField;
    QrBWLiciPanResponsav2: TIntegerField;
    QrBWLiciPanResponsav3: TIntegerField;
    QrBWLiciPanEmpresa: TIntegerField;
    QrBWLiciPanVenceFabri: TIntegerField;
    QrBWLiciPanVenceMarca: TIntegerField;
    QrBWLiciPanSegmento: TIntegerField;
    QrBWLiciPanRepresentn: TIntegerField;
    QrBWLiciPanValrPregao: TFloatField;
    QrBWLiciPanValrVenced: TFloatField;
    QrBWLiciPanClassifcac: TIntegerField;
    QrBWLiciPanQtdeLote: TFloatField;
    QrBWLiciPanValLote: TFloatField;
    QrBWLiciPanNO_BWStatus: TWideStringField;
    QrBWLiciPanNO_Cliente: TWideStringField;
    QrBWLiciPanBWStatus: TIntegerField;
    QrBWLiciPanAberturaDt: TDateField;
    QrBWLiciPanAberturaHr: TTimeField;
    QrBWLiciPanReabertuDt: TDateField;
    QrBWLiciPanReabertuHr: TTimeField;
    QrBWLiciPanLimCadPpDt: TDateField;
    QrBWLiciPanLimCadPpHr: TTimeField;
    QrBWLiciPanEntidade: TIntegerField;
    QrBWLiciPanCliente: TIntegerField;
    QrBWLiciPanUF: TWideStringField;
    QrBWLiciPanPregao: TWideStringField;
    QrBWLiciPanQtdeTotal: TFloatField;
    QrBWLiciPanValTotal: TFloatField;
    QrBWLiciPanNome: TWideStringField;
    QrBWLiciPanNO_SIGLA: TWideStringField;
    PnGerencia: TPanel;
    SbBWStatus: TSpeedButton;
    Qry: TMySQLQuery;
    QrBWLiciPanReabertuDt_TXT: TWideStringField;
    QrBWLiciPanReabertuHr_TXT: TWideStringField;
    SbReabertuDtHr: TSpeedButton;
    SbLimCadPpDtHr: TSpeedButton;
    SbAberturaDtHr: TSpeedButton;
    SbCliente: TSpeedButton;
    SBUF: TSpeedButton;
    SbPregao: TSpeedButton;
    SbProduto: TSpeedButton;
    SbPortal: TSpeedButton;
    SbNrProcesso: TSpeedButton;
    SbNO_Responsav1: TSpeedButton;
    SbNO_Responsav2: TSpeedButton;
    SbNO_Responsav3: TSpeedButton;
    SbNO_Empresa: TSpeedButton;
    SbNO_GraFabMCd: TSpeedButton;
    SbNO_Representn: TSpeedButton;
    SbNO_Segmento: TSpeedButton;
    SbLimpa: TSpeedButton;
    SpeedButton2: TSpeedButton;
    CSTabSheetChamou: TdmkCompoStore;
    QrBWLiciPanLoteORD: TWideStringField;
    QrBWLiciPanNO_GraFabCad: TWideStringField;
    SbEstah: TSpeedButton;
    SbColocacao: TSpeedButton;
    QrBWLiciPanAmstrConvocDt_TXT: TWideStringField;
    QrBWLiciPanAmstrLimEntrDt_TXT: TWideStringField;
    QrBWLiciPanAmstrEntrReaDt_TXT: TWideStringField;
    QrBWLiciPanAmstrConvocHr: TTimeField;
    QrBWLiciPanAmstrLimEntrHr: TTimeField;
    QrBWLiciPanAmstrEntrReaHr: TTimeField;
    SbAmstrConvocDtHr: TSpeedButton;
    SbAmstrLimEntrDtHr: TSpeedButton;
    SbAmstrEntrReaDtHr: TSpeedButton;
    SbOrdem: TSpeedButton;
    Label1: TLabel;
    EdBWLiOrCab: TdmkEditCB;
    CBBWLiOrCab: TdmkDBLookupComboBox;
    QrBWLiOrCab: TMySQLQuery;
    QrBWLiOrCabCodigo: TIntegerField;
    QrBWLiOrCabNome: TWideStringField;
    DsBWLiOrCab: TDataSource;
    QrBWLiOrIts: TMySQLQuery;
    QrBWLiOrItsNO_Campo: TWideStringField;
    QrBWLiOrItsCodigo: TIntegerField;
    QrBWLiOrItsControle: TIntegerField;
    QrBWLiOrItsCampo: TSmallintField;
    QrBWLiOrItsOrdem: TIntegerField;
    QrBWLiOrItsDirecao: TSmallintField;
    QrBWLiOrItsNO_Direcao: TWideStringField;
    QrBWLiOrItsFLD_Name: TWideStringField;
    SbImprime: TSpeedButton;
    DBGrid1_: TDBGrid;
    Label2: TLabel;
    EdPesqDBG: TdmkEdit;
    SbPsqNext: TSpeedButton;
    Label3: TLabel;
    EdIntrvColIni: TdmkEdit;
    EdIntrvColFim: TdmkEdit;
    CkCaseSensitive: TCheckBox;
    CkTotalCase: TCheckBox;
    SbPsqPrior: TSpeedButton;
    PBPesqDBG: TProgressBar;
    QrBWLiciPanLimCadPpDt_TXT: TWideStringField;
    QrBWLiciPanLimCadPpHr_TXT: TWideStringField;
    procedure FormCreate(Sender: TObject);
    procedure SbBWStatusClick(Sender: TObject);
    procedure DBGPanDblClick(Sender: TObject);
    procedure DBGPanColumnMoved(Sender: TObject; FromIndex, ToIndex: Integer);
    procedure SbReabertuDtHrClick(Sender: TObject);
    procedure SbAberturaDtHrClick(Sender: TObject);
    procedure SbLimCadPpDtHrClick(Sender: TObject);
    procedure SbClienteClick(Sender: TObject);
    procedure SBUFClick(Sender: TObject);
    procedure DBGPanTitleClick(Column: TColumn);
    procedure SbPregaoClick(Sender: TObject);
    procedure SbProdutoClick(Sender: TObject);
    procedure DBGPanDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure SbPortalClick(Sender: TObject);
    procedure SbNrProcessoClick(Sender: TObject);
    procedure SbNO_Responsav1Click(Sender: TObject);
    procedure SbNO_Responsav2Click(Sender: TObject);
    procedure SbNO_Responsav3Click(Sender: TObject);
    procedure SbNO_EmpresaClick(Sender: TObject);
    procedure SbNO_GraFabMCdClick(Sender: TObject);
    procedure SbNO_SegmentoClick(Sender: TObject);
    procedure SbNO_RepresentnClick(Sender: TObject);
    procedure SbLimpaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SbColocacaoClick(Sender: TObject);
    procedure SbEstahClick(Sender: TObject);
    procedure SbAmstrConvocDtHrClick(Sender: TObject);
    procedure SbAmstrLimEntrDtHrClick(Sender: TObject);
    procedure SbAmstrEntrReaDtHrClick(Sender: TObject);
    procedure SbOrdemClick(Sender: TObject);
    procedure EdBWLiOrCabRedefinido(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbPsqNextClick(Sender: TObject);
    procedure EdIntrvColFimKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIntrvColIniKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbPsqPriorClick(Sender: TObject);
    procedure EdPesqDBGRedefinido(Sender: TObject);
    procedure EdIntrvColIniRedefinido(Sender: TObject);
    procedure EdIntrvColFimRedefinido(Sender: TObject);
  private
    { Private declarations }
    FTickCount: Cardinal;
    FBWStatus: Integer;

    FReabertuDtIni, FReabertuDtFim, FAberturaDtIni, FAberturaDtFim,
    FLimCadPpDtIni, FLimCadPpDtFim, FAmstrConvocDtIni, FAmstrConvocDtFim,
    FAmstrLimEntrDtIni, FAmstrLimEntrDtFim, FAmstrEntrReaDtIni,
    FAmstrEntrReaDtFim: TDateTime;

    FReabertuHrIni, FReabertuHrFim, FAberturaHrIni, FAberturaHrFim,
    FLimCadPpHrIni, FLimCadPpHrFim, FAmstrConvocHrIni, FAmstrConvocHrFim,
    FAmstrLimEntrHrIni, FAmstrLimEntrHrFim, FAmstrEntrReaHrIni,
    FAmstrEntrReaHrFim: TTime;

    FTextoPesqCli, FTextoPesqUF, FTextoPesqPregao, FTextoPesqProd,
    FTextoPesqProduto, FTextoPesqPortal, FTextoPesqNrProcesso,
    FTextoPesqResponsav1, FTextoPesqResponsav2, FTextoPesqResponsav3,
    FTextoPesqEmpresa, FTextoPesqGraFabMCd, FTextoPesqSegmento,
    FTextoPesqRepresentn: String;

    FSQL_ORDEM,
    FSQL_BWStatus, FSQL_Reabertu, FSQL_Abertura, FSQL_LimCadPp, FSQL_CLiente,
    FSQL_UF, FSQL_Pregao, FSQL_Produto, FSQL_Portal, FSQL_NrProcesso,
    FSQL_Responsav1, FSQL_Responsav2, FSQL_Responsav3, FSQL_Empresa,
    FSQL_GraFabMCd, FSQL_Segmento, FSQL_Representn, FSQL_Colocacao, FSQL_Estah,
    FSQL_AmstrConvocDtHr, FSQL_AmstrLimEntrDtHr, FSQL_AmstrEntrReaDtHr: String;

    FColocacaoMin, FColocacaoMax, FEstahMin, FEstahMax: Integer;
    //
    FLocGridFound: Boolean;
    //
    procedure ColResize(Sender: TObject);
    //
    function  ContaDoRegistroSel(): Integer;
    procedure DefinePeriodoPesquisa(const Titulo, StrSQL: String; var _FSQL:
              String; const SpeedButton: TSpeedButton; var DtIni, DtFim:
              TDateTime; var HrIni, HrFim: TTime);
    procedure DefineIntervaloInt(const Titulo, Campo: String; var _FSQL: String;
              const SpeedButton: TSpeedButton; var ValMin, ValMax: Integer);
    procedure ReopenBWLiciPan(Conta: Integer);
    procedure PosicionaBotao(NomeCampo: String; SpeedButton: TSpeedButton);
    procedure DefineGlyph(Index: Integer; SpeedButton: TSpeedButton);
    procedure ReposicionaBotoes();
    procedure PesquisaSimplesTabela(const Tabela, FldPsq: String; var _TextoPesq,
              _FSQL: String; const SpeedButton: TSpeedButton);
    procedure PesquisaSimplesInput(const Titulo, Label_Caption, FldPsq: String;
              var _TextoPesq, _FSQL: String; const SpeedButton: TSpeedButton);
    procedure LimpaVariaveisTexto();
    procedure ReopenBWLiOrCab();
    procedure ProcuraTextoNoGrid(Direcao: TSinal);

  public
    { Public declarations }
  end;

var
  FmBWLiciPan: TFmBWLiciPan;

implementation

uses
  Module, DmkDAC_PF, UnMyObjects, UnDmkProcFunc, UnApp_Jan, ModuleGeral, MyGlyfs,
  UnMySQLCuringa, NomeX, GetMinMax, MeuDBUses;

{$R *.dfm}

{ TFmBWLiciPan }

const
  FColsPsq = 21;
  //
  FCampos: array[0..FColsPsq] of String = ('NO_BWStatus', 'ReabertuDt_TXT',
    'AberturaDt', 'LimCadPpDt_TXT', 'NO_Cliente', 'UF', 'Pregao', 'NO_Produto',
    'NO_BWPortal', 'NrProcesso', 'NO_Responsav1', 'NO_Responsav2',
    'NO_Responsav3', 'NO_Empresa', 'NO_GraFabMCd', 'NO_Segmento',
    'NO_Representn', 'Colocacao', 'Estah', 'AmstrConvocDt_TXT',
    'AmstrLimEntrDt_TXT', 'AmstrEntrReaDt_TXT');
  //
  sOrdemInicial = 'ORDER BY BWSTatus, ReabertuDt DESC, AberturaDt DESC, LoteORD, its.ItemLt, its.Controle ';

var
  FSBs: array[0..FColsPsq] of TSpeedButton;


procedure TFmBWLiciPan.ColResize(Sender: TObject);
begin
  ReposicionaBotoes();
end;

function TFmBWLiciPan.ContaDoRegistroSel(): Integer;
begin
  if QrBWLiciPan.State <> dsInactive then
    Result := QrBWLiciPanConta.Value
  else
    Result := 0;
end;

procedure TFmBWLiciPan.DBGPanColumnMoved(Sender: TObject; FromIndex,
  ToIndex: Integer);
var
  Campo: String;
  I: Integer;
begin
  Campo := Uppercase(DBGPan.Columns[ToIndex].FieldName);
  for I := Low(FCampos) to High(FCampos) do
  begin
    if Campo = Uppercase(FCampos[I]) then
      PosicionaBotao(Campo, FSBs[I]);
  end;
end;

procedure TFmBWLiciPan.DBGPanDblClick(Sender: TObject);
var
  TickCount, TickDiff: Cardinal;
begin
  TickCount := GetTickCount();
  TickDiff  := TickCount - FTickCount;
  if TickDiff < 300 then Exit;
  //
  if QrBWLiciPan.State <> dsInactive then
    App_Jan.MostraFormBWLiciCab(
      QrBWLiciPanCodigo.Value, QrBWLiciPanControle.Value, QrBWLiciPanConta.Value);
end;

procedure TFmBWLiciPan.DBGPanDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Rect.Top < 20 then
  begin
    //Memo1.Text := Geral.FF0(Rect.Top) + ' - ' + Geral.FF0(Rect.Left) + sLineBreak + Memo1.Text ;
    ReposicionaBotoes();
  end;
end;

procedure TFmBWLiciPan.DBGPanTitleClick(Column: TColumn);
begin
  FTickCount := GetTickCount;
end;

procedure TFmBWLiciPan.DefineGlyph(Index: Integer; SpeedButton: TSpeedButton);
begin
  SpeedButton.Glyph := nil;
  FmMyGlyfs.ListaPesqGrid.GetBitmap(Index, SpeedButton.Glyph);
end;

procedure TFmBWLiciPan.DefineIntervaloInt(const Titulo, Campo: String;
  var _FSQL: String; const SpeedButton: TSpeedButton; var ValMin, ValMax: Integer);
const
  Casas     = 0;
  LeftZeros = 0;
  ValMinMin = '';
  ValMaxMin = '';
  ValMinMax = '';
  ValMaxMax = '';
  Obrigatorio = False;
  ValMinCaption = 'M�nimo';
  ValMaxCaption = 'M�ximo';
  WidthVal = 72;
var
  DefaultMin, DefaultMax, ResultadoMin, ResultadoMax: Variant;
  Conta: Integer;
begin
  Conta := ContaDoRegistroSel;
  //
  DefaultMin := ValMin;
  DefaultMax := ValMax;
  if MyObjects.GetMinMaxDmk(TFmGetMinMax, FmGetMinMax, TAllFormat.dmktfInteger,
  DefaultMin, DefaultMax, Casas, LeftZeros, ValMinMin, ValMaxMin, ValMinMax, ValMaxMax,
  Obrigatorio, Titulo, ValMinCaption, ValMaxCaption, WidthVal,
  ResultadoMin, ResultadoMax) then
  begin
    ValMin := ResultadoMin;
    ValMax := ResultadoMax;
    //
    _FSQL := EmptyStr;
    if ValMax > ValMin then
    begin
      _FSQL := ' AND ' + Campo + ' BETWEEN ' + Geral.FF0(ValMin) +
        ' AND ' + Geral.FF0(ValMax);
      //
      DefineGlyph(1, SpeedButton);
    end else
    begin
      DefineGlyph(0, SpeedButton);
    end;
    ReopenBWLiciPan(Conta);
  end;
end;

procedure TFmBWLiciPan.DefinePeriodoPesquisa(const Titulo, StrSQL: String; var
  _FSQL: String; const SpeedButton: TSpeedButton; var DtIni, DtFim: TDateTime;
  var HrIni, HrFim: TTime);
const
  HabilitaHora = False;
  //Titulo = 'Per�odo da Reabertura';
  InfoDetalhado = '';
var
  DtDefaultIni, DtDefaultFim, SelectIni, SelectFim: TDateTime;
  HrDefaultIni, HrDefaultFim: TTime; // = 1 - (1/24/60/60);
  Conta: Integer;
begin
  Conta := ContaDoRegistroSel;
  //
  DtDefaultIni := DtIni;
  DtDefaultFim := DtFim;
  SelectIni    := 0;
  SelectFim    := 0;
  HrDefaultIni := HrIni;
  HrDefaultFim := HrFim;
  //
  if MyObjects.ObtemDatasPeriodo(DtDefaultIni, DtDefaultFim, SelectIni, SelectFim,
  HabilitaHora, HrDefaultIni, HrDefaultFim, Titulo, InfoDetalhado) then
  begin
    DtIni := Int(SelectIni);
    HrIni := SelectIni - DtIni;
    //
    DtFim := Int(SelectFim);
    HrFim := SelectFim - DtFim;
    //
    _FSQL := EmptyStr;
    if SelectFim > SelectIni then
    begin
      _FSQL := dmkPF.SQL_Periodo(StrSQL, SelectIni,
      SelectFim, True, True);
      DefineGlyph(1, SpeedButton);
    end else
    begin
      DefineGlyph(0, SpeedButton);
    end;
    ReopenBWLiciPan(Conta);
  end;
end;

procedure TFmBWLiciPan.EdBWLiOrCabRedefinido(Sender: TObject);
var
  Conta: Integer;
  Ordem: String;
begin
  Conta := ContaDoRegistroSel;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBWLiOrIts, Dmod.MyDB, [
  'SELECT fld.Nome NO_Campo, fld.Campo FLD_Name, ',
  'ELT(its.Direcao, "Descendente", "Ascendente", "??????") NO_Direcao, ',
  'its.* ',
  'FROM bwliorits its ',
  'LEFT JOIN bwliorfld fld ON fld.Codigo=its.Campo ',
  'WHERE its.Codigo=' + Geral.FF0(QrBWLiOrCabCodigo.Value),
  'ORDER BY Ordem, Controle ',
  '']);
  Ordem := '';
  if QrBWLiOrIts.RecordCount > 0 then
  begin
    QrBWLiOrIts.First;
    //
    Ordem := QrBWLiOrItsFLD_Name.Value;
    if QrBWLiOrItsDirecao.Value = 1 then
      Ordem := Ordem + ' DESC';
    QrBWLiOrIts.Next;
    //
    while not QrBWLiOrIts.Eof do
    begin
      Ordem := Ordem + ', ' + QrBWLiOrItsFLD_Name.Value;
      if QrBWLiOrItsDirecao.Value = 1 then
        Ordem := Ordem + ' DESC';
      //
      QrBWLiOrIts.Next;
    end;
  end;
  if Ordem <> '' then
    FSQL_ORDEM := 'ORDER BY ' + Ordem
  else
    FSQL_ORDEM := sOrdemInicial;
  //
  ReopenBWLiciPan(Conta);
end;

procedure TFmBWLiciPan.EdIntrvColFimKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdIntrvColFim.ValueVariant := DBGPan.Col;
end;

procedure TFmBWLiciPan.EdIntrvColFimRedefinido(Sender: TObject);
begin
  FLocGridFound := False;
end;

procedure TFmBWLiciPan.EdIntrvColIniKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdIntrvColIni.ValueVariant := DBGPan.Col;
end;

procedure TFmBWLiciPan.EdIntrvColIniRedefinido(Sender: TObject);
begin
  FLocGridFound := False;
end;

procedure TFmBWLiciPan.EdPesqDBGRedefinido(Sender: TObject);
begin
  FLocGridFound := False;
end;

procedure TFmBWLiciPan.FormCreate(Sender: TObject);
begin
  FLocGridFound := False;
  FSQL_ORDEM := sOrdemInicial;
  //
  EdIntrvColIni.ValMax := Geral.FF0(DBGPan.ColCount - 1);
  EdIntrvColFim.ValMax := EdIntrvColIni.ValMax;
  EdIntrvColIni.ValueVariant := 1;
  EdIntrvColFim.ValueVariant := DBGPan.ColCount - 1;
  //
  FSBs[00] := SbBWStatus;
  FSBs[01] := SbReabertuDtHr;
  FSBs[02] := SbAberturaDtHr;
  FSBs[03] := SbLimCadPpDtHr;
  FSBs[04] := SbCliente;
  FSBs[05] := SbUF;
  FSBs[06] := SbPregao;
  FSBs[07] := SbProduto;
  FSBs[08] := SbPortal;
  (**)
  FSBs[09] := SbNrProcesso;
  FSBs[10] := SbNO_Responsav1;
  FSBs[11] := SbNO_Responsav2;
  FSBs[12] := SbNO_Responsav3;
  FSBs[13] := SbNO_Empresa;
  FSBs[14] := SbNO_GraFabMCd;
  FSBs[15] := SbNO_Segmento;
  FSBs[16] := SbNO_Representn;
  (**)
  FSBs[17] := SbColocacao;
  FSBs[18] := SbEstah;
  (**)
  FSBs[19] := SbAmstrConvocDtHr;
  FSBs[20] := SbAmstrLimEntrDtHr;
  FSBs[21] := SbAmstrEntrReaDtHr;
  (**)
  //
  FReabertuHrIni := 0;
  FReabertuHrFim := 1 - (1/24/60/60);
  FAberturaHrIni := 0;
  FAberturaHrFim := 1 - (1/24/60/60);
  FLimCadPpHrIni := 0;
  FLimCadPpHrFim := 1 - (1/24/60/60);
  //
  FAmstrConvocHrIni := 0;
  FAmstrConvocHrFim := 1 - (1/24/60/60);
  FAmstrLimEntrHrIni := 0;
  FAmstrLimEntrHrFim := 1 - (1/24/60/60);
  FAmstrEntrReaHrIni := 0;
  FAmstrEntrReaHrFim := 1 - (1/24/60/60);
  //
  LimpaVariaveisTexto();
  //
  DBGPan.OnColResize := ColResize;
  //
  ReopenBWLiciPan(0);
  //
  ReposicionaBotoes();
  FColocacaoMin := 0;
  FColocacaoMax := 1000;
  FEstahMin     := 0;
  FEstahMax     := 1000;
  //
  ReopenBWLiOrCab();
  //
end;

procedure TFmBWLiciPan.FormShow(Sender: TObject);
begin
  //PnGerencia.Visible := True;
end;

procedure TFmBWLiciPan.LimpaVariaveisTexto();
begin
  FTextoPesqCli          := '';
  FTextoPesqUF           := '';
  FTextoPesqPregao       := '';
  FTextoPesqProd         := '';
  FTextoPesqProduto      := '';
  FTextoPesqPortal       := '';
  FTextoPesqNrProcesso   := '';
  FTextoPesqResponsav1   := '';
  FTextoPesqResponsav2   := '';
  FTextoPesqResponsav3   := '';
  FTextoPesqEmpresa      := '';
  FTextoPesqGraFabMCd    := '';
  FTextoPesqSegmento     := '';
  FTextoPesqRepresentn   := '';
  FColocacaoMin          := 0;
  FColocacaoMax          := 100;
  //
  FSQL_BWStatus          := '';
  FSQL_Reabertu          := '';
  FSQL_Abertura          := '';
  FSQL_LimCadPp          := '';
  FSQL_CLiente           := '';
  FSQL_UF                := '';
  FSQL_Pregao            := '';
  FSQL_Produto           := '';
  FSQL_Portal            := '';
  FSQL_NrProcesso        := '';
  FSQL_Responsav1        := '';
  FSQL_Responsav2        := '';
  FSQL_Responsav3        := '';
  FSQL_Empresa           := '';
  FSQL_GraFabMCd         := '';
  FSQL_Segmento          := '';
  FSQL_Representn        := '';
  FSQL_Colocacao         := '';
  FSQL_Estah             := '';
  FSQL_AmstrConvocDtHr   := '';
  FSQL_AmstrLimEntrDtHr  := '';
  FSQL_AmstrEntrReaDtHr  := '';
  //
end;

procedure TFmBWLiciPan.PesquisaSimplesInput(const Titulo, Label_Caption, FldPsq:
 String; var _TextoPesq, _FSQL: String; const SpeedButton: TSpeedButton);
const
  ReturnCodUsu = False;
  LEFT_JOIN = '';
var
  Codigo: Integer;
  Conta: Integer;
  Compare: String;
begin
  Conta := ContaDoRegistroSel;
  //
  if InputQuery(Titulo, Label_Caption, _TextoPesq) and (_TextoPesq <> EmptyStr)then
  begin
   if pos('%', _TextoPesq) > 0 then
     Compare := ' LIKE '
   else
     Compare := '=';
   //
    _FSQL := ' AND ' + FldPsq + Compare + '"' + _TextoPesq + '"';
    DefineGlyph(1, SpeedButton);
  end else
  begin
    _FSQL := EmptyStr;
    DefineGlyph(0, SpeedButton);
  end;
  ReopenBWLiciPan(Conta);
end;

procedure TFmBWLiciPan.PesquisaSimplesTabela(const Tabela, FldPsq: String; var
 _TextoPesq, _FSQL: String; const SpeedButton: TSpeedButton);
const
  ReturnCodUsu = False;
  LEFT_JOIN = '';
var
  Codigo: Integer;
  Conta: Integer;
begin
  Conta := ContaDoRegistroSel;
  //
  Codigo := CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, Tabela, Dmod.MyDB,
  EmptyStr, ReturnCodUsu, LEFT_JOIN, _TextoPesq);
  //
  if VAR_EXEC_CURINGA then
  begin
    _TextoPesq := VAR_TextoPesq_CURINGA;
    if VAR_CORDA_CODIGOS <> EmptyStr then
    begin
      _FSQL := ' AND ' + FldPsq + ' IN (' + VAR_CORDA_CODIGOS + ') ';
      DefineGlyph(1, SpeedButton);
    end else
    if Codigo <> 0 then
    begin
      _FSQL := ' AND ' + FldPsq + '=' + Geral.FF0(Codigo);
      DefineGlyph(1, SpeedButton);
    end else
    begin
      _FSQL := EmptyStr;
      DefineGlyph(0, SpeedButton);
    end;
    //
    ReopenBWLiciPan(Conta);
  end;
end;

procedure TFmBWLiciPan.PosicionaBotao(NomeCampo: String; SpeedButton: TSpeedButton);
var
  DataRect: TRect;
  I, K: Integer;
begin
  if SpeedButton = nil then Exit;
  // Place the button in the first column.
  //if (Column.Index = 0) then
  begin
    K := -1;
    for I := 0 to DBGPan.Columns.Count - 1 do
    begin
      if Uppercase(DBGPan.Columns[I].FieldName) = Uppercase(NomeCampo) then
      begin
        K := I;
        Break;
      end;
    end;
    if K = -1 then Exit;
    with TStringGrid(DBGPan) do
    begin
       DataRect := CellRect(K + 1(*Column.Index*), 0(*Row*));
    end;
    // Assign the button's parent to the grid.
    if SpeedButton.Parent <> DBGPan then
      SpeedButton.Parent := DBGPan ;
    // Set the button's coordinates.
    // In this case, right justify the button.
    if SpeedButton.Left <> (DataRect.Right - SpeedButton.Width) then
      SpeedButton.Left := (DataRect.Right - SpeedButton.Width) ;
    if (SpeedButton.Top <> DataRect.Top) then
      SpeedButton.Top := DataRect.Top ;

    // Make sure the button's height fits in row.
    if (SpeedButton.Height <> (DataRect.Bottom-DataRect.Top)) then
      SpeedButton.Height := (DataRect.Bottom-DataRect.Top);
  end;
end;

procedure TFmBWLiciPan.ProcuraTextoNoGrid(Direcao: TSinal);
const
  sProcName= 'TFmBWLiciPan.SbPsqNextClick()';
var
  //Lin,
  Col, ColIni, StartCol, StartRow, IntrvColIni, IntrvColFim: Integer;
  //sAtual, Nome:,
  sToLoc, Texto: String;
  Campos: array of String;
  VoltaAoInicio, Continua, AskLocAll: Boolean;
  BMStart: TBookmark;
  //
  function Localiza(Coluna: Integer): Boolean;
  begin
    if CkTotalCase.Checked then
    begin
      if CkCaseSensitive.Checked then
        Result := sToLoc = QrBWLiciPan.FieldByName(Campos[Coluna-1]).DisplayText
      else
        Result :=
          sToLoc = Lowercase(QrBWLiciPan.FieldByName(Campos[Coluna-1]).DisplayText);
    end else
    begin
      if CkCaseSensitive.Checked then
        Result :=
          Pos(sToLoc, QrBWLiciPan.FieldByName(Campos[Coluna-1]).DisplayText) > 0
      else
        Result :=
          Pos(sToLoc, Lowercase(QrBWLiciPan.FieldByName(Campos[Coluna-1]).DisplayText)) > 0
    end;
    if Result then
      FLocGridFound := True;
  end;
  //
  function NotLocate(): String;
  begin
    Result := 'O texto "' + Texto + '" n�o foi localizado!';
  end;
begin
  try
  PBPesqDBG.Visible := True;
  Texto    := EdPesqDBG.ValueVariant;
  StartRow := DBGPan.Row;
  BMStart  := QrBWLiciPan.GetBookMark;
  //
  if CkCaseSensitive.Checked then
    sToLoc := Texto
  else
    sToLoc := Lowercase(Texto);
  //
  if MyObjects.FIC(sToLoc = EMptyStr,  EdPesqDBG,
    'Informe o texto a ser pesquisado!') then Exit;
  //
  IntrvColIni := EdIntrvColIni.ValueVariant;
  IntrvColFim := EdIntrvColFim.ValueVariant;
  //
  if MyObjects.FIC(IntrvColIni > IntrvColFim, EdIntrvColIni,
    'A coluna inicial do intervalo de colunas n�o pode ser maior que a coluna final!')
    then Exit;
  //
  StartCol := DBGPan.Col;
  if StartCol < IntrvColIni then
    StartCol := IntrvColIni;
  if StartCol > IntrvColFim then
    StartCol := IntrvColFim;
  //
  //
  if (StartCol > IntrvColIni) or (StartRow > 1) then
  begin
    if Direcao = siPositivo then
      VoltaAoInicio := True
    else
      VoltaAoInicio := False;
  end else
    VoltaAoInicio := False;

  if Direcao = siPositivo then
  begin
    ColIni := DBGPan.Col + 1;
    if ColIni < IntrvColIni then
      ColIni := IntrvColIni;
    //if ColIni >= DBGPan.ColCount then
    if ColIni >= IntrvColFim then
    begin
      if QrBWLiciPan.Eof then
        QrBWLiciPan.First
      else
        QrBWLiciPan.Next;
      ColIni := IntrvColIni; //1;
    end;
  end else
  begin
    ColIni := DBGPan.Col - 1;
    if ColIni < IntrvColIni then
    begin
      if QrBWLiciPan.Bof then
        QrBWLiciPan.Last
      else
        QrBWLiciPan.Prior;
      ColIni := IntrvColFim;
    end;
  end;
  QrBWLiciPan.DisableControls;
  Screen.Cursor := crHourGlass;
  try
    SetLength(Campos, DBGPan.ColCount);
    for Col := 1 to DBGPan.ColCount - 1 do
      Campos[Col-1] := DBGPan.Columns[Col-1].FieldName;
    //
    if Direcao = siPositivo then
    begin
      PBPesqDBG.Position := 0;
      PBPesqDBG.Max := QrBWLiciPan.RecordCount - QrBWLiciPan.RecNo + 1;
      while not QrBWLiciPan.Eof do
      begin
        MyObjects.UpdPBOnly(PBPesqDBG);
        //for Col := ColIni to DBGPan.ColCount - 1 do
        for Col := ColIni to IntrvColFim do
        begin
          if Localiza(Col) then
          begin
            DBGPan.Col := Col;
            DBGPan.SetFocus;
            Exit;
          end;
        end;
        //
        ColIni := IntrvColIni; //1;
        QrBWLiciPan.Next;
      end;
    end else
    begin
      PBPesqDBG.Position := 0;
      PBPesqDBG.Max := QrBWLiciPan.RecNo + 1;
      while not QrBWLiciPan.Bof do
      begin
        MyObjects.UpdPBOnly(PBPesqDBG);
        for Col := ColIni downto IntrvColIni do
        begin
          if Localiza(Col) then
          begin
            DBGPan.Col := Col;
            DBGPan.SetFocus;
            Exit;
          end;
        end;
        //
        ColIni := IntrvColIni; //1;
        QrBWLiciPan.Prior;
      end;
    end;
    //
    if VoltaAoInicio then
      Continua := Geral.MB_Pergunta(NotLocate() + sLineBreak +
      'Deseja recome�ar a pesquisa do �n�cio?') = ID_YES
    else
    begin
      Continua   := False;
      DBGPan.Col := StartCol;
      DBGPan.Row := StartRow;
      QrBWLiciPan.GotoBookMark(BMStart);
      Geral.MB_Info(NotLocate());
      AskLocAll := True;
    end;
    if Continua then
    begin
      QrBWLiciPan.First;
      PBPesqDBG.Position := 0;
      PBPesqDBG.Max := QrBWLiciPan.RecordCount;
      //
      while not QrBWLiciPan.Eof do
      begin
        MyObjects.UpdPBOnly(PBPesqDBG);
        DBGPan.Col := IntrvColIni;
        if (QrBWLiciPan.RecNo >= StartRow) and (DBGPan.Col >= StartCol) then
        begin
          if not Localiza(DBGPan.Col) then
          begin
            Geral.MB_Info(NotLocate());
            AskLocAll := True;
          end;
          Exit;
        end;
        //for Col := ColIni to DBGPan.ColCount - 1 do
        for Col := IntrvColIni to IntrvColFim do
        begin
          if Localiza(Col) then
          begin
            DBGPan.Col := Col;
            DBGPan.SetFocus;
            Exit;
          end;
        end;
        //
        ColIni := 1;
        QrBWLiciPan.Next;
      end;
    end else
    begin
      DBGPan.Col := StartCol;
      DBGPan.Row := StartRow;
      QrBWLiciPan.GotoBookMark(BMStart);
    end;
  finally
    QrBWLiciPan.FreeBookMark(BMStart);
    QrBWLiciPan.EnableControls;
    Screen.Cursor := crDefault;
    if AskLocAll and (FLocGridFound = False) then
    begin
      if (IntrvColIni > 1) or (IntrvColFim < DBGPan.ColCount - 1) then
      begin
        if Geral.MB_Pergunta('Deseja pesquisar em todas colunas?') = ID_YES then
        begin
          EdIntrvColIni.ValueVariant := 1;
          EdIntrvColFim.ValueVariant := DBGPan.ColCount - 1;
          //
          ProcuraTextoNoGrid(siPositivo);
        end;
      end;
    end;
  end;
  DBGPan.SetFocus;
  finally
    PBPesqDBG.Visible := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBWLiciPan.ReopenBWLiciPan(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrBWLiciPan, Dmod.MyDB, [
  'SELECT prd.Nome NO_Produto, med.Sigla NO_Sigla, ',
  'its.Codigo, its.Controle, ',
  'its.Conta, its.ItemLt, its.Produto, its.Unidade, ',
  'its.QtdeItem, its.ValrUnit, its.ValItem, its.Linha, ',
  ' ',
  'url.Nome NO_BWPortal, bem.Nome NO_Empresa, ',
  'amo.Nome NO_PrzAmostras, amo.Nome NO_PrzEntregas, ',
  're1.Nome NO_Responsav1, re2.Nome NO_Responsav2, ',
  're3.Nome NO_Responsav3, gfc.Nome NO_GraFabCad, gfd.Nome No_GraFabMCd, ',
  'sgm.Nome NO_Segmento, rpr.Nome NO_Representn, ',
  ' ',
  'lot.Lote, lot.BWPortal, lot.NrProcesso, ',
  'lot.Colocacao, lot.Estah, lot.PrzAmostras, ',
  'lot.PrzEntregas, lot.Responsav1, lot.Responsav2, ',
  'lot.Responsav3, lot.Empresa, lot.VenceFabri, ',
  'lot.VenceMarca, lot.Segmento, lot.Representn, ',
  'lot.ValrPregao, lot.ValrVenced, lot.Classifcac, ',
  'lot.QtdeLote, lot.ValLote, /*lot.Linha,*/ ',
  'LPAD(lot.Lote, 11, "0") LoteORD, ',
  'IF(lot.AmstrConvocDt < "1900-01-01", "",  ',
  'DATE_FORMAT(lot.AmstrConvocDt, "%d/%m/%Y")) AmstrConvocDt_TXT,  ',
  'IF(lot.AmstrLimEntrDt < "1900-01-01", "",  ',
  'DATE_FORMAT(lot.AmstrLimEntrDt, "%d/%m/%Y")) AmstrLimEntrDt_TXT,  ',
  'IF(lot.AmstrEntrReaDt < "1900-01-01", "",  ',
  'DATE_FORMAT(lot.AmstrEntrReaDt, "%d/%m/%Y")) AmstrEntrReaDt_TXT,  ',
  'lot.AmstrConvocHr, AmstrLimEntrHr, AmstrEntrReaHr, ',
  ' ',
  'IF(cab.ReabertuDt < "1900-01-01", "", ',
  'DATE_FORMAT(cab.ReabertuDt, "%d/%m/%Y")) ReabertuDt_TXT, ',
  'IF(cab.ReabertuDt < "1900-01-01", "", ',
  'DATE_FORMAT(cab.ReabertuHr, "%H:%i")) ReabertuHr_TXT, ',

  'IF(cab.LimCadPpDt < "1900-01-01", "", ',
  'DATE_FORMAT(cab.LimCadPpDt, "%d/%m/%Y")) LimCadPpDt_TXT, ',
  'IF(cab.LimCadPpDt < "1900-01-01", "", ',
  'DATE_FORMAT(cab.LimCadPpHr, "%H:%i")) LimCadPpHr_TXT, ',

  'sta.Nome NO_BWStatus, cli.Nome NO_Cliente, ',
  'cab.BWStatus, cab.AberturaDt, cab.AberturaHr, ',
  'cab.ReabertuDt, cab.ReabertuHr, cab.LimCadPpDt, ',
  'cab.LimCadPpHr, cab.Entidade, cab.Cliente, ',
  'cab.UF, cab.Pregao, cab.QtdeTotal, ',
  'cab.ValTotal, cab.Nome ',
  ' ',
  ' ',
  'FROM bwliciits its ',
  'LEFT JOIN bwprodutos prd ON prd.Codigo=its.Produto ',
  'LEFT JOIN unidmed    med ON med.Codigo=its.Unidade ',
  ' ',
  'LEFT JOIN bwlicilot   lot ON lot.Controle=its.Controle ',
  'LEFT JOIN bwportais   url ON url.Codigo=lot.BWPortal ',
  'LEFT JOIN przentrgcab amo ON amo.Codigo=lot.PrzAmostras ',
  'LEFT JOIN przentrgcab etr ON etr.Codigo=lot.PrzEntregas ',
  'LEFT JOIN bwrespnsvs  re1 ON re1.Codigo=lot.Responsav1 ',
  'LEFT JOIN bwrespnsvs  re2 ON re2.Codigo=lot.Responsav2 ',
  'LEFT JOIN bwrespnsvs  re3 ON re3.Codigo=lot.Responsav3 ',
  'LEFT JOIN bwempresas  bem ON bem.Codigo=lot.Empresa ',
  'LEFT JOIN grafabcad   gfc ON gfc.Codigo=lot.VenceFabri ',
  //'LEFT JOIN grafabmar   gfm ON gfm.Controle=lot.VenceMarca ',
  'LEFT JOIN grafabmcd   gfd ON gfd.Codigo=lot.VenceMarca ', //gfm.GraFabMCd ',
  'LEFT JOIN bwsegmento  sgm ON sgm.Codigo=lot.Segmento ',
  'LEFT JOIN bwrepresnt  rpr ON rpr.Codigo=lot.Representn ',
  ' ',
  'LEFT JOIN bwlicicab cab ON cab.Codigo=its.Codigo ',
  'LEFT JOIN bwstatus sta ON sta.Codigo=cab.BWStatus ',
  'LEFT JOIN bwclientes cli ON cli.Codigo=cab.Cliente ',
  ' ',
  'WHERE its.Controle>0 ',
  FSQL_BWStatus,
  FSQL_Reabertu,
  FSQL_Abertura,
  FSQL_LimCadPp,
  FSQL_Cliente,
  FSQL_UF,
  FSQL_Pregao,
  FSQL_Produto,
  FSQL_Portal,
  FSQL_NrProcesso,
  FSQL_Responsav1,
  FSQL_Responsav2,
  FSQL_Responsav3,
  FSQL_Empresa,
  FSQL_GraFabMCd,
  FSQL_Segmento,
  FSQL_Representn,
  FSQL_Colocacao,
  FSQL_Estah,
  FSQL_AmstrConvocDtHr,
  FSQL_AmstrLimEntrDtHr,
  FSQL_AmstrEntrREaDtHr,
  //
  FSQL_ORDEM,
  '']);
  //
  //Geral.MB_Teste(QrBWLiciPan.SQL.Text);
  //
  if Conta <> 0 then
    QrBWLiciPan.Locate('Conta', Conta, []);
  //
  //Geral.MB_Teste(QrBWLiciPan.SQL.Text);
end;

procedure TFmBWLiciPan.ReopenBWLiOrCab;
var
  BWLiOrCab: Integer;
begin
  BWLiOrCab := EdBWLiOrCab.ValueVariant;
  //
  UnDmkDAC_PF.AbreQuery(QrBWLiOrCab, Dmod.MyDB);
  if QrBWLiOrCab.Locate('Codigo', BWLiOrCab, []) then
  begin
    EdBWLiOrCab.ValueVariant := BWLiOrCab;
    CBBWLiOrCab.KeyValue     := BWLiOrCab;
  end;
end;

procedure TFmBWLiciPan.ReposicionaBotoes();
var
  I: Integer;
begin
  for I := Low(FCampos) to High(FCampos) do
  begin
    //if Campo = Uppercase(FCampos[I]) then
      PosicionaBotao(FCampos[I], FSBs[I]);
  end;
(*
  PosicionaBotao('NO_BWStatus', SbBWStatus);
  PosicionaBotao('ReabertuDt_TXT', SbReabertuDtHr);
  PosicionaBotao('AberturaDt', SbAberturaDtHr);
  PosicionaBotao('LimCadPpDt', SbLimCadPpDtHr);
  PosicionaBotao('NO_Cliente', SbCliente);
  PosicionaBotao('UF', SbUF);
  PosicionaBotao('Pregao', SbPregao);
  PosicionaBotao('NO_Produto', SbProduto);
*)
end;

procedure TFmBWLiciPan.SbAberturaDtHrClick(Sender: TObject);
const
  Titulo = 'Per�odo da Abertura';
  StrSQL = ' AND AberturaDt ';
begin
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_Abertura, SbAberturaDtHr,
  FAberturaDtIni, FAberturaDtFim, FAberturaHrIni, FAberturaHrFim);
end;

procedure TFmBWLiciPan.SbAmstrConvocDtHrClick(Sender: TObject);
const
  Titulo = 'Per�odo da Convoca��o de Amostra';
  StrSQL = ' AND AmstrConvocDt ';
begin
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_AmstrConvocDtHr, SbAmstrConvocDtHr,
  FAmstrConvocDtIni, FAmstrConvocDtFim, FAmstrConvocHrIni, FAmstrConvocHrFim);
end;

procedure TFmBWLiciPan.SbAmstrEntrReaDtHrClick(Sender: TObject);
const
  Titulo = 'Per�odo da Entrega da Amostra';
  StrSQL = ' AND AmstrEntrReaDt ';
begin
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_AmstrEntrReaDtHr, SbAmstrEntrReaDtHr,
  FAmstrEntrReaDtIni, FAmstrEntrReaDtFim, FAmstrEntrReaHrIni, FAmstrEntrReaHrFim);
end;

procedure TFmBWLiciPan.SbAmstrLimEntrDtHrClick(Sender: TObject);
const
  Titulo = 'Per�odo da Data Limite de Entrega da Amostra';
  StrSQL = ' AND AmstrLimEntrDt ';
begin
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_AmstrLimEntrDtHr, SbAmstrLimEntrDtHr,
  FAmstrLimEntrDtIni, FAmstrLimEntrDtFim, FAmstrLimEntrHrIni, FAmstrLimEntrHrFim);
end;

procedure TFmBWLiciPan.SbBWStatusClick(Sender: TObject);
const
  CaptionFm = 'Status';
  CaptionCG = 'Selecione os Status';
var
  Codigos: array of Integer;
  Nomes: array of String;
  I, N, Conta: Integer;
  P, K: Single;
begin
  Conta := ContaDoRegistroSel();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM BWStatus',
  'ORDER BY Codigo',
  '']);
  //
  N := Qry.RecordCount;
  SetLength(Codigos, N);
  SetLength(Nomes, N);
  Qry.First;
  while not Qry.Eof do
  begin
    Codigos[Qry.RecNo - 1] := Qry.FieldByName('Codigo').AsInteger;
    Nomes[Qry.RecNo - 1] := Qry.FieldByName('Nome').AsString;
    //
    Qry.Next;
  end;
  FBWStatus := MyObjects.SelCheckGroup(CaptionFm, CaptionCG, Nomes, 2, FBWStatus);
  FSQL_BWStatus := '';
  for I := 0 to N - 1 do
  begin
    K := Power(2, Single(I));
    if dmkPF.IntInConjunto2(Trunc(K), FBWStatus) then
      FSQL_BWStatus := FSQL_BWStatus + ', ' + Geral.FF0(Codigos[I]);
  end;
  if Length(FSQL_BWStatus) > 2 then
  begin
    FSQL_BWStatus := ' AND BWStatus IN (' + Copy(FSQL_BWStatus, 3) + ') ';
    DefineGlyph(1, SbBWStatus);
  end else
  begin
    DefineGlyph(0, SbBWStatus);
  end;
  //
  ReopenBWLiciPan(Conta);
end;

procedure TFmBWLiciPan.SbClienteClick(Sender: TObject);
begin
  PesquisaSimplesTabela('bwclientes', 'cab.Cliente', FTextoPesqCLi, FSQL_CLiente, SbCliente);
end;

procedure TFmBWLiciPan.SbColocacaoClick(Sender: TObject);
begin
  DefineIntervaloInt('Coloca��o', 'Colocacao', FSQL_Colocacao,  SbColocacao,
    FColocacaoMin, FColocacaoMax);
end;

procedure TFmBWLiciPan.SbEstahClick(Sender: TObject);
begin
  DefineIntervaloInt('Est�', 'Estah', FSQL_Estah,  SbEstah, FEstahMin, FEstahMax);
end;

procedure TFmBWLiciPan.SbImprimeClick(Sender: TObject);
var
  I, K: Integer;
var
  Query: TmySQLQuery;
  Titulo: String;
begin
(*
  DBGrid1.Columns.Clear;
  for I := 1 to DBGPan.Col do
  begin
    K := I-1;
    DBGrid1.Columns.Add;
    DBGrid1.Columns[K].Title := DBGPan.Columns[K].Title;
    DBGrid1.Columns[K].Width := DBGPan.Columns[K].Width;
    DBGrid1.Columns[K].FieldName := DBGPan.Columns[K].FieldName;
    //
  end;
  DBGrid1.DataSource := DBGPan.DataSource;
  FmMeuDBUses.FDBGrid := DBGrid1;
*)
  // ini 2022-01-24 (Alexandria)
  //FmMeuDBUses.FOriDBGrid := MeuDBUses.TDBGrid(DBGPan);
  FmMeuDBUses.FOriDBGrid := TDBGrid(DBGPan);
  // fim 2022-01-24 (Alexandria)
  FmMeuDBUses.PnVisivel.Align := alTop;
  FmMeuDBUses.FTitulo := 'Panorama de Licita��es';
  FmMeuDBUses.Height :=
    FmMeuDBUses.PnVisivel.Height +
    FmMeuDBUses.PnConfirma.Height +
    (FmMeuDBUses.Height - FmMeuDBUses.ClientHeight);
  FmMeuDBUses.PnVisivel.Align := alClient;
  FmMeuDBUses.EdColIni.ValueVariant := 1;
  FmMeuDBUses.EdColFim.ValueVariant := DBGPan.Col;
  FmMeuDBUses.PnInativo.Visible := False;
  FmMeuDBUses.PnImprime.Visible := False;
  FmMeuDBUses.DBGrid1.Visible   := False;
  FmMeuDBUses.Show;
{
var
  I, K: Integer;
var
  Query: TmySQLQuery;
  Titulo: String;
begin
  DBGrid1.Columns.Clear;
  for I := 1 to DBGPan.Col do
  begin
    K := I-1;
    DBGrid1.Columns.Add;
    DBGrid1.Columns[K].Title := DBGPan.Columns[K].Title;
    DBGrid1.Columns[K].Width := DBGPan.Columns[K].Width;
    DBGrid1.Columns[K].FieldName := DBGPan.Columns[K].FieldName;
    //
  end;
  DBGrid1.DataSource := DBGPan.DataSource;
  FmMeuDBUses.ImprimeDBGrid(TDBGrid(DBGrid1), 'Panorama de Licita��es')
}
end;

procedure TFmBWLiciPan.SbLimCadPpDtHrClick(Sender: TObject);
const
  Titulo = 'Per�odo do Limite de Cadastro da Proposta';
  StrSQL = ' AND LimCadPpDt ';
begin
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_LimCadPp, SbLimCadPpDtHr,
  FLimCadPpDtIni, FLimCadPpDtFim, FLimCadPpHrIni, FLimCadPpHrFim);
end;

procedure TFmBWLiciPan.SbNO_EmpresaClick(Sender: TObject);
begin
  PesquisaSimplesTabela('bwempresas', 'lot.Empresa', FTextoPesqEmpresa,
  FSQL_Empresa, SbNO_Empresa);
end;

procedure TFmBWLiciPan.SbNO_GraFabMCdClick(Sender: TObject);
begin
  PesquisaSimplesTabela('grafabmcd', 'lot.VenceMarca', FTextoPesqGraFabMCd,
  FSQL_GraFabMCd, SbNO_GraFabMCd);
end;

procedure TFmBWLiciPan.SbNO_RepresentnClick(Sender: TObject);
begin
  PesquisaSimplesTabela('bwrepresnt', 'lot.Representn', FTextoPesqRepresentn,
  FSQL_Representn, SbNO_Representn);
end;

procedure TFmBWLiciPan.SbNO_Responsav1Click(Sender: TObject);
begin
  PesquisaSimplesTabela('bwrespnsvs', 'lot.Responsav1', FTextoPesqResponsav1,
  FSQL_Responsav1, SbNO_Responsav1);
end;

procedure TFmBWLiciPan.SbNO_Responsav2Click(Sender: TObject);
begin
  PesquisaSimplesTabela('bwrespnsvs', 'lot.Responsav2', FTextoPesqResponsav2,
  FSQL_Responsav2, SbNO_Responsav2);
end;

procedure TFmBWLiciPan.SbNO_Responsav3Click(Sender: TObject);
begin
  PesquisaSimplesTabela('bwrespnsvs', 'lot.Responsav3', FTextoPesqResponsav3,
  FSQL_Responsav3, SbNO_Responsav3);
end;

procedure TFmBWLiciPan.SbNO_SegmentoClick(Sender: TObject);
begin
  PesquisaSimplesTabela('bwsegmento', 'lot.Segmento', FTextoPesqSegmento,
  FSQL_Segmento, SbNO_Segmento);
end;

procedure TFmBWLiciPan.SbNrProcessoClick(Sender: TObject);
begin
  PesquisaSimplesInput('N�mero do processo', 'Texto Parcial (use %%)',
  'lot.NrProcesso', FTextoPesqNrProcesso, FSQL_NrProcesso, SbNrProcesso);
end;

procedure TFmBWLiciPan.SbOrdemClick(Sender: TObject);
begin
  App_Jan.MostraFormBWLiOrCab(0);
  ReopenBWLiOrCab();
end;

procedure TFmBWLiciPan.SbPortalClick(Sender: TObject);
begin
  PesquisaSimplesTabela('bwportais', 'lot.BWPortal', FTextoPesqPortal, FSQL_Portal, SbPortal);
end;

procedure TFmBWLiciPan.SbPregaoClick(Sender: TObject);
begin
  //PesquisaSimplesTabela('bwlicicab', 'Pregao', FTextoPesqPrg, FSQL_Pregao, SbPregao);
  PesquisaSimplesInput('Pregao', 'Texto Parcial (use %%)', 'cab.Pregao',
    FTextoPesqPregao, FSQL_Pregao, SbPregao);
end;

procedure TFmBWLiciPan.SbProdutoClick(Sender: TObject);
begin
  PesquisaSimplesTabela('bwprodutos', 'its.Produto', FTextoPesqProd, FSQL_Produto, SbProduto);
end;

procedure TFmBWLiciPan.SbPsqNextClick(Sender: TObject);
begin
  ProcuraTextoNoGrid(siPositivo);
end;

procedure TFmBWLiciPan.SbPsqPriorClick(Sender: TObject);
begin
  ProcuraTextoNoGrid(siNegativo);
end;

procedure TFmBWLiciPan.SbReabertuDtHrClick(Sender: TObject);
const
  Titulo = 'Per�odo da Reabertura';
  StrSQL = ' AND ReabertuDt ';
begin
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_Reabertu, SbReabertuDtHr,
  FReabertuDtIni, FReabertuDtFim, FReabertuHrIni, FReabertuHrFim);
end;

procedure TFmBWLiciPan.SBUFClick(Sender: TObject);
begin
  PesquisaSimplesInput('UF', 'Informe a UF', 'cab.UF', FTextoPesqUF, FSQL_UF, SbUF);
end;

procedure TFmBWLiciPan.SbLimpaClick(Sender: TObject);
var
  I, Conta: Integer;
begin
  Conta := ContaDoRegistroSel();
  //
  LimpaVariaveisTexto();
  //
  for I := Low(FSbs) to High(FSBs) do
      DefineGlyph(0, FSBs[I]);
  ReopenBWLiciPan(Conta);
end;

procedure TFmBWLiciPan.SpeedButton2Click(Sender: TObject);
begin
  //VAR_CADASTRO := Qr.Value;
  //
  if TFmBWLiciPan(Self).Owner is TApplication then
    Close
  else
  try
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
    VAR_AdvToolBarPagerNovo.Visible := True;
  except
    ; // nada
  end;
end;

{ TDBgrid }

procedure TDBgrid.ColWidthsChanged;
begin
  inherited;
  if Assigned(FColResize) then  FColResize(self);
end;

end.
