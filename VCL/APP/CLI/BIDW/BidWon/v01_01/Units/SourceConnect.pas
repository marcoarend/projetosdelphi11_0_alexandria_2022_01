unit SourceConnect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, UnGrl_Consts, UnApp_PF;

type
  TFmSourceConnect = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    GroupBox3: TGroupBox;
    LaRegPorta: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EdRegPorta: TdmkEdit;
    EdDatabase: TdmkEdit;
    EdOthUser: TdmkEdit;
    EdOthSetCon: TdmkEdit;
    LaRegIPServer: TLabel;
    EdRegIPServer: TdmkEdit;
    RGRegServer: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmSourceConnect: TFmSourceConnect;

implementation

uses UnMyObjects, UnDmkProcFunc;

{$R *.DFM}

procedure TFmSourceConnect.BtOKClick(Sender: TObject);
var
  Porta, Server: Integer;
  IPServer, DataBase, OthUser, OthSetCon, UnDmkProcFunc: String;
begin
  Porta     := EdRegPorta.ValueVariant;
  Server    := RGRegServer.ItemIndex + 1;
  IPServer  := EdRegIPServer.ValueVariant;
  DataBase  := EdDataBase.ValueVariant;
  OthUser   := EdOthUser.ValueVariant;
  OthSetCon := EdOthSetCon.ValueVariant;
  OthSetCon := dmkPF.Criptografia(OthSetCon, CO_LLM_SecureStr);
  //
  if MyObjects.FIC(Porta = 0, EdRegPorta, 'Porta do banco de dados n�o definida!') then Exit;
  if MyObjects.FIC(Server < 0, RGRegServer, 'Tipo de computador n�o definido!') then Exit;
  //
  if Server = 1 then
  begin
    if MyObjects.FIC(IPServer = '', EdRegIPServer, 'IP do servidor n�o definido!') then Exit;
  end else
    IPServer := '127.0.0.1';
  try
    Geral.WriteAppKeyCU('Porta', Application.Title + CO_SourceConnect, Porta, ktInteger);
    Geral.WriteAppKeyCU('Server', Application.Title + CO_SourceConnect, Server, ktInteger);
    Geral.WriteAppKeyCU('IPServer', Application.Title + CO_SourceConnect, IPServer, ktString);
    //
    Geral.WriteAppKeyCU('Database', Application.Title + CO_SourceConnect, DataBase, ktString);
    Geral.WriteAppKeyCU('OthUser', Application.Title + CO_SourceConnect, OthUser, ktString);
    Geral.WriteAppKeyCU('OthSetCon', Application.Title + CO_SourceConnect, OthSetCon, ktString);
  except
    Geral.MB_Aviso('Falha ao salvar dados!' + sLineBreak +
      'Certifique-se que voc� est� logado no sistema operacional com um usu�rio do tipo administrador!');
  end;
  VAR_TERMINATE := True;
  VAR_APLICATION_TERMINATE := True;
  Application.Terminate;
end;

procedure TFmSourceConnect.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSourceConnect.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSourceConnect.FormCreate(Sender: TObject);
var
  Porta, Server: Integer;
  IPServer, DataBase, OthUser, OthSetCon, UnDmkProcFunc: String;
begin
  ImgTipo.SQLType := stLok;
  //
  App_PF.ObtemDadosSourceConnect(
    Porta, Server, IPServer, DataBase, OthUser, OthSetCon);
  //
  EdRegPorta.ValueVariant      := Porta;
  RGRegServer.ItemIndex        := Server;
  EdRegIPServer.ValueVariant   := IPServer;
  //
  EdDataBase.ValueVariant      := DataBase;
  EdOthUser.ValueVariant       := OthUser;
  EdOthSetCon.ValueVariant     := OthSetCon;
  //
end;

procedure TFmSourceConnect.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
