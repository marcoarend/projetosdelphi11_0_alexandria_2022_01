unit UnApp_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  UnInternalConsts, Variants, Controls, dmkPageControl, UnProjGroup_Vars;

type
  TUnApp_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormBWClientes();
    procedure MostraFormBWEmpresas();
    procedure MostraFormBWLiciCab(Codigo, Controle, Conta: Integer);
    procedure MostraFormBWLiciPan(AbrirEmAba: Boolean; InOwner: TWincontrol;
              AdvToolBarPager: TdmkPageControl; Codigo: Integer);
    procedure MostraFormBWLiOrCab(Codigo: Integer);
    procedure MostraFormBWPortais(Codigo: Integer);
    procedure MostraFormBWProdutos();
    procedure MostraFormBWRepresnt();
    procedure MostraFormBWSegmento();
    procedure MostraFormOpcoesBW();
    procedure MostraFormOpcoesGrl();
    procedure MostraFormResponsaveis();
    procedure MostraFormGraFabCad(Codigo, Marca: Integer);
    procedure MostraFormLoadCSV_01();
    procedure MostraFormPrzEntrgCab(Codigo: Integer);
    procedure MostraFormSourceConnect();
  end;

var
  App_Jan: TUnApp_Jan;


implementation

uses
  MyDBCheck, Module, SourceConnect, LoadCSV_01, BWPortais, PrzEntrgCab,
  GraFabCad, BWLiciCab, CfgCadLista, BWLiciPan, UnMyObjects, OpcoesBW,
  BWLiOrCab, Opcoes;

{ TUnApp_Jan }

procedure TUnApp_Jan.MostraFormBWClientes();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'BWClientes', 255, ncGerlSeq1,
  'Clientes',
  [], False, Null(*Maximo*), [], [], False);
end;

procedure TUnApp_Jan.MostraFormBWEmpresas();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'bwempresas', 60, ncGerlSeq1,
  'Empresas',
  [], False, Null(*Maximo*), [], [], False);
end;

procedure TUnApp_Jan.MostraFormBWLiciCab(Codigo, Controle, Conta: Integer);
begin
  if DBCheck.CriaFm(TFmBWLiciCab, FmBWLiciCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmBWLiciCab.LocCod(Codigo, Codigo);
      FmBWLiciCab.QrBWLiciLot.Locate('Controle', Controle, []);
      FmBWLiciCab.QrBWLiciIts.Locate('Conta', Conta, []);
    end;
    FmBWLiciCab.ShowModal;
    FmBWLiciCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormBWLiciPan(AbrirEmAba: Boolean; InOwner:
TWincontrol; AdvToolBarPager: TdmkPageControl; Codigo: Integer);
begin
  if AbrirEmAba then
  begin
    AdvToolBarPager.Visible := False;
    if FmBWLiciPan = nil then
    begin
      VAR_FmBWLiciPan := MyObjects.FormTDICria(TFmBWLiciPan, InOwner, AdvToolBarPager, True, True);
      //TFmBWLiciPan(VAR_FmBWLiciPan).QrBWLici?.Locate('Codigo', Codigo, []);
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmBWLiciPan, FmBWLiciPan, afmoNegarComAviso) then
    begin
      FmBWLiciPan.ShowModal;
      FmBWLiciPan.Destroy;
      //
      FmBWLiciPan := nil;
    end;
  end;
end;

procedure TUnApp_Jan.MostraFormBWLiOrCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmBWLiOrCab, FmBWLiOrCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmBWLiOrCab.LocCod(Codigo, Codigo);
    FmBWLiOrCab.ShowModal;
    FmBWLiOrCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormBWPortais(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmBWPortais, FmBWPortais, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmBWPortais.LocCod(Codigo, Codigo);
    FmBWPortais.ShowModal;
    FmBWPortais.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormBWProdutos();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'BWProdutos', 255, ncGerlSeq1,
  'Produtos',
  [], False, Null(*Maximo*), [], [], False);
end;

procedure TUnApp_Jan.MostraFormBWRepresnt();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'BWRepresnt', 60, ncGerlSeq1,
  'Representantes',
  [], False, Null(*Maximo*), [], [], False);
end;

procedure TUnApp_Jan.MostraFormBWSegmento();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'BWSegmento', 60, ncGerlSeq1,
  'Segmentos',
  [], False, Null(*Maximo*), [], [], False);
end;

procedure TUnApp_Jan.MostraFormGraFabCad(Codigo, Marca: Integer);
begin
  if DBCheck.CriaFm(TFmGraFabCad, FmGraFabCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmGraFabCad.LocCod(Codigo, Codigo);
      FmGraFabCad.QrGraFabMar.Locate('Codigo', Marca, []);
    end else
    begin
      FmGraFabCad.QrGraFabMar.Close;
    end;
    //
    FmGraFabCad.ShowModal;
    FmGraFabCad.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormLoadCSV_01();
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  if DBCheck.CriaFm(TFmLoadCSV_01, FmLoadCSV_01, afmoSoBoss) then
  begin
    FmLoadCSV_01.ShowModal;
    FmLoadCSV_01.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormOpcoesBW();
begin
  if DBCheck.CriaFm(TFmOpcoesBW, FmOpcoesBW, afmoSoBoss) then
  begin
    FmOpcoesBW.ShowModal;
    FmOpcoesBW.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormOpcoesGrl;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoSoBoss) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormPrzEntrgCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPrzEntrgCab, FmPrzEntrgCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPrzEntrgCab.LocCod(Codigo, Codigo);
    FmPrzEntrgCab.ShowModal;
    FmPrzEntrgCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormResponsaveis();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'BWRespnsvs', 60, ncGerlSeq1,
  'Responsáveis',
  [], False, Null(*Maximo*), [], [], False);
end;

procedure TUnApp_Jan.MostraFormSourceConnect();
begin
  if DBCheck.CriaFm(TFmSourceConnect, FmSourceConnect, afmoSoBoss) then
  begin
    FmSourceConnect.ShowModal;
    FmSourceConnect.Destroy;
  end;
end;

end.
