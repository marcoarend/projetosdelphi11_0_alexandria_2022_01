unit BWLiOrCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmBWLiOrCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrBWLiOrCab: TMySQLQuery;
    DsBWLiOrCab: TDataSource;
    QrBWLiOrIts: TMySQLQuery;
    DsBWLiOrIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrBWLiOrCabCodigo: TIntegerField;
    QrBWLiOrCabNome: TWideStringField;
    QrBWLiOrItsNO_Campo: TWideStringField;
    QrBWLiOrItsCodigo: TIntegerField;
    QrBWLiOrItsControle: TIntegerField;
    QrBWLiOrItsCampo: TSmallintField;
    QrBWLiOrItsOrdem: TIntegerField;
    QrBWLiOrItsDirecao: TSmallintField;
    QrOrdem: TMySQLQuery;
    QrOrdemControle: TIntegerField;
    QrOrdemOrdem: TIntegerField;
    QrBWLiOrItsNO_Direcao: TWideStringField;
    QrBWLiOrItsFLD_Name: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrBWLiOrCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrBWLiOrCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrBWLiOrCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrBWLiOrCabBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormBWLiOrIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenBWLiOrIts(Controle: Integer);
    procedure ReordenaBWLiOrIts(_Controle, OrdemAnt, OrdemNew: Integer);

  end;

var
  FmBWLiOrCab: TFmBWLiOrCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, BWLiOrIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmBWLiOrCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmBWLiOrCab.MostraFormBWLiOrIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmBWLiOrIts, FmBWLiOrIts, afmoNegarComAviso) then
  begin
    FmBWLiOrIts.ImgTipo.SQLType := SQLType;
    FmBWLiOrIts.FQrCab := QrBWLiOrCab;
    FmBWLiOrIts.FDsCab := DsBWLiOrCab;
    FmBWLiOrIts.FQrIts := QrBWLiOrIts;
    FmBWLiOrIts.FCodigo := QrBWLiOrCabCodigo.Value;
    if SQLType = stIns then
      FmBWLiOrIts.FOrdem := 0
      //
    else
    begin
      FmBWLiOrIts.EdControle.ValueVariant := QrBWLiOrItsControle.Value;
      FmBWLiOrIts.FOrdem                  := QrBWLiOrItsOrdem.Value;
      //
      FmBWLiOrIts.EdCampo.ValueVariant := QrBWLiOrItsCampo.Value;
      FmBWLiOrIts.CBCampo.KeyValue     := QrBWLiOrItsCampo.Value;
      FmBWLiOrIts.EdOrdem.ValueVariant := QrBWLiOrItsOrdem.Value;
      FmBWLiOrIts.RGDirecao.ItemIndex  := QrBWLiOrItsDirecao.Value;
    end;
    FmBWLiOrIts.ShowModal;
    FmBWLiOrIts.Destroy;
  end;
end;

procedure TFmBWLiOrCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrBWLiOrCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrBWLiOrCab, QrBWLiOrIts);
end;

procedure TFmBWLiOrCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrBWLiOrCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrBWLiOrIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrBWLiOrIts);
end;

procedure TFmBWLiOrCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrBWLiOrCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmBWLiOrCab.DefParams;
begin
  VAR_GOTOTABELA := 'bwliorcab';
  VAR_GOTOMYSQLTABLE := QrBWLiOrCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM bwliorcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmBWLiOrCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormBWLiOrIts(stUpd);
end;

procedure TFmBWLiOrCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmBWLiOrCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmBWLiOrCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmBWLiOrCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'BWLiOrIts', 'Controle', QrBWLiOrItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrBWLiOrIts,
      QrBWLiOrItsControle, QrBWLiOrItsControle.Value);
    ReopenBWLiOrIts(Controle);
  end;
end;

procedure TFmBWLiOrCab.ReopenBWLiOrIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrBWLiOrIts, Dmod.MyDB, [
  'SELECT fld.Nome NO_Campo, fld.Campo FLD_Name, ',
  'ELT(its.Direcao, "Descendente", "Ascendente", "??????") NO_Direcao, ',
  'its.* ',
  'FROM bwliorits its ',
  'LEFT JOIN bwliorfld fld ON fld.Codigo=its.Campo ',
  'WHERE its.Codigo=' + Geral.FF0(QrBWLiOrCabCodigo.Value),
  'ORDER BY Ordem, Controle ',
  '']);
  //
  QrBWLiOrIts.Locate('Controle', Controle, []);
end;


procedure TFmBWLiOrCab.ReordenaBWLiOrIts(_Controle, OrdemAnt, OrdemNew: Integer);
var
  Controle, Ordem: Integer;
  SQLType: TSQLType;
begin
  SQLType := stUpd;
  //
  Screen.Cursor := crHourGlass;
  try
    if OrdemAnt = 0 then
    begin
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      ' UPDATE bwliorits SET Ordem=Ordem + 1 ' +
      ' WHERE Codigo = ' + Geral.FF0(QrBWLiOrCabCodigo.Value) +
      ' AND Ordem >= ' + Geral.FF0(OrdemNew)
      );
    end else
    if OrdemNew > OrdemAnt then
    begin
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      ' UPDATE bwliorits SET Ordem=Ordem - 1 ' +
      ' WHERE Codigo = ' + Geral.FF0(QrBWLiOrCabCodigo.Value) +
      ' AND ( ' +
      '   Ordem > ' + Geral.FF0(OrdemAnt) +
      '   AND ' +
      '   Ordem <= ' + Geral.FF0(OrdemNew) +
      ' ) ' +
      ' AND Controle <> ' + Geral.FF0(_Controle)
      );
    end else
    if OrdemNew < OrdemAnt then
    begin
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      ' UPDATE bwliorits SET Ordem=Ordem + 1 ' +
      ' WHERE Codigo = ' + Geral.FF0(QrBWLiOrCabCodigo.Value) +
      ' AND ( ' +
      '   Ordem >= ' + Geral.FF0(OrdemNew) +
      '   AND ' +
      '   Ordem < ' + Geral.FF0(OrdemAnt) +
      ' ) ' +
      ' AND Controle <> ' + Geral.FF0(_Controle)
      );
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrOrdem, Dmod.MyDB, [
    'SELECT its.Ordem, its.Controle  ',
    'FROM bwliorits its ',
    'WHERE its.Codigo=' + Geral.FF0(QrBWLiOrCabCodigo.Value),
    'ORDER BY its.Ordem, its.Controle ',
    '']);
    //
    QrOrdem.First;
    while not QrOrdem.Eof do
    begin
      Controle := QrOrdemControle.Value;
      Ordem := QrOrdem.RecNo;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwliorits', False, [
      'Ordem'], ['Controle'], [
      Ordem], [Controle], True);
      //
      QrOrdem.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBWLiOrCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmBWLiOrCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmBWLiOrCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmBWLiOrCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmBWLiOrCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmBWLiOrCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBWLiOrCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrBWLiOrCabCodigo.Value;
  Close;
end;

procedure TFmBWLiOrCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormBWLiOrIts(stIns);
end;

procedure TFmBWLiOrCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrBWLiOrCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'bwliorcab');
end;

procedure TFmBWLiOrCab.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('bwliorcab', 'Codigo', '', '', tsPos, stIns, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'bwliorcab', False, [
  'Nome'], [
  'Codigo'], [
  Nome], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmBWLiOrCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'bwliorcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'bwliorcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmBWLiOrCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmBWLiOrCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmBWLiOrCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmBWLiOrCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrBWLiOrCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmBWLiOrCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmBWLiOrCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrBWLiOrCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmBWLiOrCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmBWLiOrCab.QrBWLiOrCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmBWLiOrCab.QrBWLiOrCabAfterScroll(DataSet: TDataSet);
begin
  ReopenBWLiOrIts(0);
end;

procedure TFmBWLiOrCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrBWLiOrCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmBWLiOrCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrBWLiOrCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'bwliorcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmBWLiOrCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBWLiOrCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrBWLiOrCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'bwliorcab');
end;

procedure TFmBWLiOrCab.QrBWLiOrCabBeforeClose(
  DataSet: TDataSet);
begin
  QrBWLiOrIts.Close;
end;

procedure TFmBWLiOrCab.QrBWLiOrCabBeforeOpen(DataSet: TDataSet);
begin
  QrBWLiOrCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

