object FmBWLiHStat: TFmBWLiHStat
  Left = 339
  Top = 185
  Caption = 'LIC-TACAO-005 :: Hist'#243'rico de Status'
  ClientHeight = 549
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 404
    Width = 1008
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 230
        Height = 32
        Caption = 'Hist'#243'rico de Status'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 230
        Height = 32
        Caption = 'Hist'#243'rico de Status'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 230
        Height = 32
        Caption = 'Hist'#243'rico de Status'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 435
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 479
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 89
    Align = alTop
    TabOrder = 0
    object Label6: TLabel
      Left = 8
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label7: TLabel
      Left = 92
      Top = 4
      Width = 28
      Height = 13
      Caption = 'Nome'
    end
    object Label1: TLabel
      Left = 164
      Top = 44
      Width = 71
      Height = 13
      Caption = 'Status anterior:'
    end
    object Label9: TLabel
      Left = 384
      Top = 44
      Width = 60
      Height = 13
      Caption = 'Novo status:'
    end
    object Label18: TLabel
      Left = 8
      Top = 44
      Width = 101
      Height = 13
      Caption = 'Data hora do evento:'
    end
    object EdNivel4: TdmkEdit
      Left = 8
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 92
      Top = 20
      Width = 509
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdStatusAnt: TdmkEditCB
      Left = 164
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBStatusAnt
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBStatusAnt: TdmkDBLookupComboBox
      Left = 220
      Top = 60
      Width = 160
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBWStatusAnt
      TabOrder = 3
      dmkEditCB = EdStatusAnt
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdStatusAtu: TdmkEditCB
      Left = 384
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'BWStatus'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBStatusAtu
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBStatusAtu: TdmkDBLookupComboBox
      Left = 440
      Top = 60
      Width = 160
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBWStatusAtu
      TabOrder = 5
      dmkEditCB = EdStatusAtu
      QryCampo = 'BWStatus'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object TPDataHora: TdmkEditDateTimePicker
      Left = 8
      Top = 60
      Width = 109
      Height = 21
      Date = 40656.000000000000000000
      Time = 40656.000000000000000000
      ShowCheckbox = True
      TabOrder = 6
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'AberturaDt'
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdDataHora: TdmkEdit
      Left = 120
      Top = 60
      Width = 40
      Height = 21
      TabOrder = 7
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00:00'
      QryCampo = 'AberturaHr'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object MeHistorico: TMemo
    Left = 0
    Top = 137
    Width = 1008
    Height = 89
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object QrBWStatusAnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM bwstatus'
      'ORDER BY Nome')
    Left = 296
    Top = 4
    object QrBWStatusAntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWStatusAntNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsBWStatusAnt: TDataSource
    DataSet = QrBWStatusAnt
    Left = 296
    Top = 52
  end
  object QrBWStatusAtu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM bwstatus'
      'ORDER BY Nome')
    Left = 404
    Top = 4
    object QrBWStatusAtuCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWStatusAtuNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsBWStatusAtu: TDataSource
    DataSet = QrBWStatusAtu
    Left = 404
    Top = 52
  end
end
