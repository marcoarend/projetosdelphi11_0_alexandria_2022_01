program BidWon;

uses
  Vcl.Forms,
  Vcl.Themes,
  Vcl.Styles,
  Winapi.Windows,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  Module in '..\Units\Module.pas' {Dmod: TDataModule},
  dmkGeral in '..\..\..\..\..\..\..\dmkComp\dmkGeral.pas',
  UnMyObjects in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  UnGrl_Vars in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnDmkEnums in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnDmkProcFunc in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  ZCF2 in '..\..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnInternalConsts in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnInternalConsts2 in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts2.pas',
  UnMyJSON in '..\..\..\..\..\..\UTL\JSON\UnMyJSON.pas',
  uLkJSON in '..\..\..\..\..\..\UTL\JSON\uLkJSON.pas',
  UnLicVendaApp_Dmk in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnLicVendaApp_Dmk.pas',
  UnGrl_DmkREST in '..\..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_DmkREST.pas',
  UnDmkHTML2 in '..\..\..\..\..\..\UTL\WEB\HTML\v01_01\UnDmkHTML2.pas',
  WBFuncs in '..\..\..\..\..\..\UTL\WEB\HTML\v01_01\WBFuncs.pas',
  UnGrl_Geral in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  Windows_FMX_ProcFunc in '..\..\..\..\..\..\..\FMX\UTL\_UNT\v01_01\Windows\Windows_FMX_ProcFunc.pas',
  LicDados in '..\..\..\ProjGroup\v01_01\LicDados.pas' {FmLicDados},
  ABOUT in '..\..\..\..\..\..\UTL\_FRM\v01_01\ABOUT.pas' {FmAbout},
  UnMyVclEvents in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnMyVclEvents.pas',
  UnProjGroup_Consts in '..\..\..\ProjGroup\v01_01\UnProjGroup_Consts.pas',
  MyListas in '..\..\..\ProjGroup\v01_01\MyListas.pas',
  UnProjGroup_Vars in '..\..\..\ProjGroup\v01_01\UnProjGroup_Vars.pas',
  dmkLed in '..\..\..\..\..\..\..\dmkComp\dmkLed.pas',
  AgendaTarefa in '..\..\..\..\..\..\UTL\_FRM\v01_01\AgendaTarefa.pas' {FmAgendaTarefa},
  MyDBCheck in '..\..\..\..\..\..\UTL\MySQL\v01_01\MyDBCheck.pas',
  UnGOTOy in '..\..\..\..\..\..\UTL\MySQL\v01_01\UnGOTOy.pas',
  dmkDAC_PF in '..\..\..\..\..\..\UTL\MySQL\v01_01\dmkDAC_PF.pas',
  UnInternalConsts3 in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts3.pas',
  UMySQLDB in '..\..\..\..\..\..\UTL\MySQL\v01_01\UMySQLDB.pas',
  UnGrl_Consts in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  VerificaConexoes in '..\..\..\..\..\..\UTL\_FRM\v01_01\VerificaConexoes.pas' {FmVerificaConexoes},
  UnLock_MLA in '..\..\..\..\..\DMK\UnLock\v01_01\UnLock_MLA.pas' {FmUnLock_MLA},
  Senha in '..\..\..\..\..\..\UTL\_FRM\v01_01\Senha.pas' {FmSenha},
  SelCod in '..\..\..\..\..\..\UTL\_FRM\v01_01\SelCod.pas' {FmSelCod},
  SelCods in '..\..\..\..\..\..\UTL\_FRM\v01_01\SelCods.pas' {FmSelCods},
  SelStr in '..\..\..\..\..\..\UTL\_FRM\v01_01\SelStr.pas' {FmSelStr},
  OrderFieldsImp in '..\..\..\..\..\..\UTL\_FRM\v01_01\OrderFieldsImp.pas' {FmOrderFieldsImp},
  ZStepCodUni in '..\..\..\..\..\..\UTL\_FRM\v01_01\ZStepCodUni.pas' {FmZStepCodUni},
  GerlShowGrid in '..\..\..\..\..\..\UTL\_FRM\v01_01\GerlShowGrid.pas' {FmGerlShowGrid},
  AtivaCod in '..\..\..\..\..\..\UTL\_FRM\v01_01\AtivaCod.pas' {FmAtivaCod},
  VerifiDBTerceiros in '..\..\..\..\..\..\UTL\MySQL\v01_01\VerifiDBTerceiros.pas' {FmVerifiDBTerceiros},
  UnDmkWeb in '..\..\..\..\..\..\MDL\WEB\v01_01\UnDmkWeb.pas',
  UnALL_Jan in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnALL_Jan.pas',
  dmkSOAP in '..\..\..\..\..\..\MDL\WEB\v01_01\WebService\dmkSOAP.pas',
  LogOff in '..\..\..\..\..\..\UTL\_FRM\v01_01\LogOff.pas' {FmLogOff},
  ModuleGeral in '..\..\..\..\..\..\UTL\MySQL\v01_01\ModuleGeral.pas' {DModG: TDataModule},
  UnEntities in '..\..\..\..\..\..\MDL\ENT\v02_01\UnEntities.pas',
  Descanso in '..\..\..\..\..\..\UTL\_FRM\v01_01\Descanso.pas' {FmDescanso},
  UnEmpresas_Jan in '..\..\..\..\..\..\MDL\EMP\v01_01\UnEmpresas_Jan.pas',
  CfgCadLista in '..\..\..\..\..\..\UTL\MySQL\v01_01\CfgCadLista.pas',
  UMySQLModule in '..\..\..\..\..\..\UTL\MySQL\v01_01\UMySQLModule.pas',
  GModule in '..\..\..\..\..\..\UTL\MySQL\v01_01\GModule.pas' {GMod: TDataModule},
  SenhaEspecial in '..\..\..\..\..\..\UTL\_FRM\v01_01\SenhaEspecial.pas' {FmSenhaEspecial},
  ConfJanela in '..\..\..\..\..\..\UTL\_FRM\v01_01\ConfJanela.pas' {FmConfJanela},
  UnMyVCLref in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnMyVCLref.pas',
  PesqESel in '..\..\..\..\..\..\UTL\_FRM\v01_01\PesqESel.pas' {FmPesqESel},
  Terminais in '..\..\..\..\..\..\UTL\_FRM\v01_01\Terminais.pas' {FmTerminais},
  Entidade2 in '..\..\..\..\..\..\MDL\ENT\v02_01\Entidade2.pas' {FmEntidade2},
  UnMySQLCuringa in '..\..\..\..\..\..\UTL\MySQL\v01_01\UnMySQLCuringa.pas',
  NomeX in '..\..\..\..\..\..\UTL\_FRM\v01_01\NomeX.pas' {FmNomeX},
  UnCEP in '..\..\..\..\..\..\MDL\ENT\v02_01\CEP\UnCEP.pas',
  Curinga in '..\..\..\..\..\..\UTL\_FRM\v01_01\Curinga.pas' {FmCuringa},
  ConsultaCEP in '..\..\..\..\..\..\UTL\CEP\delphi-consulta-cep-master\ConsultaCEP.pas',
  EntiCEP2 in '..\..\..\..\..\..\MDL\ENT\v02_01\CEP\EntiCEP2.pas' {FmEntiCEP2},
  CEP_Result in '..\..\..\..\..\..\MDL\ENT\v02_01\CEP\CEP_Result.pas' {FmCEP_Result},
  MyGlyfs in '..\..\..\..\..\..\UTL\_FRM\v01_01\MyGlyfs.pas' {FmMyGlyfs},
  Entidade2Imp in '..\..\..\..\..\..\MDL\ENT\v02_01\Entidade2Imp.pas' {FmEntidade2Imp},
  Opcoes in '..\..\..\..\..\..\UTL\_FRM\v01_01\Opcoes.pas' {FmOpcoes},
  Entidade2ImpOne in '..\..\..\..\..\..\MDL\ENT\v02_01\Entidade2ImpOne.pas' {FmEntidade2ImpOne},
  PesqDescri in '..\..\..\..\..\..\UTL\_FRM\v01_01\PesqDescri.pas' {FmPesqDescri},
  UnReordena in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnReordena.pas',
  MyDBGridReorder in '..\..\..\..\..\..\UTL\_FRM\v01_01\MyDBGridReorder.pas' {FmMyDBGridReorder},
  ParamsEmp in '..\..\..\..\..\..\MDL\EMP\v01_01\ParamsEmp.pas' {FmParamsEmp},
  UnLock_Dmk in '..\..\..\..\..\DMK\UnLock\v01_01\UnLock_Dmk.pas' {FmUnLock_Dmk},
  GetValor in '..\..\..\..\..\..\UTL\_FRM\v01_01\GetValor.pas' {FmGetValor},
  UnWAceites in '..\..\..\..\..\..\MDL\TEXT\v01_01\Geral\UnWAceites.pas',
  VerifiDB in '..\..\..\..\..\..\UTL\MySQL\v01_01\VerifiDB.pas' {FmVerifiDB},
  GetBiosInformation in '..\..\..\..\..\..\UTL\_UNT\v01_01\GetBiosInformation.pas',
  WAceite in '..\..\..\..\..\..\MDL\TEXT\v01_01\Geral\WAceite.pas' {FmWAceite},
  Matriz in '..\..\..\..\..\..\MDL\EMP\v01_01\Matriz.pas' {FmMatriz},
  EntiCliInt in '..\..\..\..\..\..\MDL\EMP\v01_01\EntiCliInt.pas' {FmEntiCliInt},
  CadLista in '..\..\..\..\..\..\UTL\MySQL\v01_01\CadLista.pas' {FmCadLista},
  CadItemGrupo in '..\..\..\..\..\..\UTL\MySQL\v01_01\CadItemGrupo.pas' {FmCadItemGrupo},
  CadRegistroSimples in '..\..\..\..\..\..\UTL\MySQL\v01_01\CadRegistroSimples.pas' {FmCadRegistroSimples},
  CadListaOrdem in '..\..\..\..\..\..\UTL\MySQL\v01_01\CadListaOrdem.pas' {FmCadListaOrdem},
  FilialUsrs in '..\..\..\..\..\..\MDL\EMP\v01_01\FilialUsrs.pas' {FmFilialUsrs},
  UnitMD5 in '..\..\..\..\..\..\UTL\Encrypt\v01_01\UnitMD5.pas',
  BidWon_Dmk in '..\Units\BidWon_Dmk.pas' {FmBidWon_Dmk},
  UnLic_Dmk in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnLic_Dmk.pas',
  UCreate in '..\..\..\..\..\..\UTL\MySQL\v01_01\UCreate.pas',
  MeuDBUses in '..\..\..\..\..\..\UTL\MySQL\v01_01\MeuDBUses.pas' {FmMeuDBUses},
  UCreateEDrop in '..\..\..\..\..\..\UTL\_UNT\v01_01\UCreateEDrop.pas',
  PesqNome in '..\..\..\..\..\..\UTL\_FRM\v01_01\PesqNome.pas' {FmPesqNome},
  UnFTP in '..\..\..\..\..\..\MDL\ARQ\v01_01\Geral\UnFTP.pas',
  UnConsultasWeb in '..\..\..\..\..\..\MDL\WEB\v01_01\UnConsultasWeb.pas',
  UnDmkImg in '..\..\..\..\..\..\UTL\Imagem\v01_01\UnDmkImg.pas',
  FTPUploads in '..\..\..\..\..\..\MDL\ARQ\v01_01\Geral\FTPUploads.pas' {FmFTPUploads},
  ApplicationConfiguration in '..\..\..\..\..\..\MDL\ARQ\v01_01\Geral\ApplicationConfiguration.pas',
  FTPSiteInfo in '..\..\..\..\..\..\MDL\ARQ\v01_01\Geral\FTPSiteInfo.pas',
  NovaVersao in '..\..\..\..\..\..\UTL\_FRM\v01_01\NovaVersao.pas' {FmNovaVersao},
  MeuFrx in '..\..\..\..\..\..\UTL\Print\v01_01\MeuFrx.pas' {FmMeuFrx},
  CustomFR3Imp in '..\..\..\..\..\..\UTL\Print\v01_01\CustomFR3Imp.pas' {FmCustomFR3Imp},
  CustomFR3 in '..\..\..\..\..\..\UTL\Print\v01_01\CustomFR3.pas' {FmCustomFR3},
  AtrIts in '..\..\..\..\..\..\UTL\_FRM\v01_01\AtrIts.pas' {FmAtrIts},
  SenhaBoss in '..\..\..\..\..\..\UTL\_FRM\v01_01\SenhaBoss.pas' {FmSenhaBoss},
  ServerConnect in '..\..\..\..\..\..\UTL\MySQL\v01_01\ServerConnect.pas' {FmServerConnect},
  ServiceManager in '..\..\..\..\..\..\UTL\MySQL\v01_01\ServiceManager.pas',
  ServicoManager in '..\..\..\..\..\..\UTL\MySQL\v01_01\ServicoManager.pas' {FmServicoManager},
  Servidor in '..\..\..\..\..\..\UTL\MySQL\v01_01\Servidor.pas' {FmServidor},
  MyInis in '..\..\..\..\..\..\UTL\MySQL\v01_01\MyInis.pas' {FmMyInis},
  Restaura2 in '..\..\..\..\..\..\UTL\MySQL\v01_01\Restaura2.pas' {FmRestaura2},
  Backup3 in '..\..\..\..\..\..\UTL\MySQL\v01_01\Backup3.pas' {FmBackup3},
  ZForge in '..\..\..\..\..\..\UTL\_FRM\v01_01\ZForge.pas' {FmZForge},
  UnGrlUsuarios in '..\..\..\..\..\..\..\MultiOS\MDL\USR\v02_01\UnGrlUsuarios.pas',
  UnGrl_DmkDB in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_DmkDB.pas',
  UnGrl_REST in '..\..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_REST.pas',
  UnGrlTemp in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrlTemp.pas',
  OAuth in '..\..\..\..\..\..\UTL\WEB\HTML\v01_01\OAuth.pas' {FmOAuth},
  UnGrlEnt in '..\..\..\..\..\..\..\MultiOS\MDL\ENT\v02_01\UnGrlEnt.pas',
  UnEnti_Tabs in '..\..\..\..\..\..\MDL\ENT\v02_01\UnEnti_Tabs.pas',
  UnPerfJan_Tabs in '..\..\..\..\..\..\..\MultiOS\AllOS\Listas\UnPerfJan_Tabs.pas',
  UnALL_Tabs in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnALL_Tabs.pas',
  UnIBGE_DTB_Tabs in '..\..\..\..\..\..\MDL\ENT\v02_01\IBGE\UnIBGE_DTB_Tabs.pas',
  UnEmpresas_Tabs in '..\..\..\..\..\..\MDL\EMP\v01_01\UnEmpresas_Tabs.pas',
  Geral_TbTerc in '..\..\..\..\..\..\UTL\_UNT\v01_01\Geral_TbTerc.pas',
  SourceConnect in '..\Units\SourceConnect.pas' {FmSourceConnect},
  UnApp_Jan in '..\Units\UnApp_Jan.pas',
  UnApp_PF in '..\Units\UnApp_PF.pas',
  LoadCSV_01 in '..\Units\LoadCSV_01.pas' {FmLoadCSV_01},
  UnApp_Tabs in '..\Units\UnApp_Tabs.pas',
  UnPrzEntrg_Tabs in '..\..\..\..\..\..\MDL\PrzEntrg\v01_01\UnPrzEntrg_Tabs.pas',
  UnUMedi_Tabs in '..\..\..\..\..\..\MDL\UMed\v01_01\UnUMedi_Tabs.pas',
  BWPortais in '..\Units\BWPortais.pas' {FmBWPortais},
  PrzEntrgCab in '..\..\..\..\..\..\MDL\Praz\v01_01\PrzEntrgCab.pas' {FmPrzEntrgCab},
  UnUMedi_PF in '..\..\..\..\..\..\MDL\UMed\v01_01\UnUMedi_PF.pas',
  UnidMed in '..\..\..\..\..\..\MDL\UMed\v01_01\UnidMed.pas' {FmUnidMed},
  SPED_Listas in '..\..\..\..\..\..\MDL\SPED\v00_01\SPED_Listas.pas',
  GraFabMar in '..\..\..\..\..\..\MDL\GRAD\v01_01\GraFabMar.pas' {FmGraFabMar},
  GraFabCad in '..\..\..\..\..\..\MDL\GRAD\v01_01\GraFabCad.pas' {FmGraFabCad},
  Data.DB in 'c:\program files (x86)\embarcadero\studio\19.0\source\data\Data.DB.pas',
  WSuporte in '..\..\..\..\..\..\MDL\HELP\v01_01\Geral\WSuporte.pas' {FmWSuporte},
  WLogin in '..\..\..\..\..\..\MDL\WEB\v01_01\WLogin.pas' {FmWLogin},
  WOrdSerIncRap in '..\..\..\..\..\..\MDL\HELP\v01_01\Geral\WOrdSerIncRap.pas' {FmWOrdSerIncRap},
  WebBrowser in '..\..\..\..\..\..\MDL\WEB\v01_01\WebBrowser.pas' {FmWebBrowser},
  SelRadioGroup in '..\..\..\..\..\..\UTL\_FRM\v01_01\SelRadioGroup.pas' {FmSelRadioGroup},
  Sel2RadioGroups in '..\..\..\..\..\..\UTL\_FRM\v01_01\Sel2RadioGroups.pas' {FmSel2RadioGroups},
  Sel3RadioGroups in '..\..\..\..\..\..\UTL\_FRM\v01_01\Sel3RadioGroups.pas' {FmSel3RadioGroups},
  SelCheckGroup in '..\..\..\..\..\..\UTL\_FRM\v01_01\SelCheckGroup.pas' {FmSelCheckGroup},
  QuaisItens in '..\..\..\..\..\..\UTL\_FRM\v01_01\QuaisItens.pas' {FmQuaisItens},
  GetData in '..\..\..\..\..\..\UTL\_FRM\v01_01\GetData.pas' {FmGetData},
  GetDataEUser in '..\..\..\..\..\..\UTL\_FRM\v01_01\GetDataEUser.pas' {FmGetDataEUser},
  GetDatasPeriodo in '..\..\..\..\..\..\UTL\_FRM\v01_01\GetDatasPeriodo.pas' {FmGetDatasPeriodo},
  GetMinMax in '..\..\..\..\..\..\UTL\_FRM\v01_01\GetMinMax.pas' {FmGetMinMax},
  BWLiciCab in '..\Units\BWLiciCab.pas' {FmBWLiciCab},
  BWLiciLot in '..\Units\BWLiciLot.pas' {FmBWLiciLot},
  BWLiciIts in '..\Units\BWLiciIts.pas' {FmBWLiciIts},
  BWLiciPan in '..\Units\BWLiciPan.pas' {FmBWLiciPan},
  BWLiHStat in '..\Units\BWLiHStat.pas' {FmBWLiHStat},
  BWLiHEsta in '..\Units\BWLiHEsta.pas' {FmBWLiHEsta},
  GetTexto in '..\..\..\..\..\..\UTL\_FRM\v01_01\GetTexto.pas' {FmGetTexto},
  EntiDocs2 in '..\..\..\..\..\..\MDL\ENT\v02_01\EntiDocs2.pas' {FmEntiDocs2},
  ObtemFoto4 in '..\..\..\..\..\..\UTL\Imagem\v01_01\Camera\ObtemFoto4.pas' {FmObtemFoto4},
  VFrames in '..\..\..\..\..\..\UTL\Imagem\v01_01\Camera\Common\VFrames.pas',
  GdiPlus in '..\..\..\..\..\..\UTL\Imagem\v01_01\Camera\Common\GdiPlus\Lib\GdiPlus.pas',
  VSample in '..\..\..\..\..\..\UTL\Imagem\v01_01\Camera\Common\VSample.pas',
  OpcoesBW in '..\Units\OpcoesBW.pas' {FmOpcoesBW},
  EntiDocLoad in '..\..\..\..\..\..\MDL\ENT\v02_01\EntiDocLoad.pas' {FmEntiDocLoad},
  Mail_Tabs in '..\..\..\..\..\..\MDL\MAIL\v01_01\Mail_Tabs.pas',
  cfgAtributos in '..\..\..\..\..\..\UTL\_FRM\v01_01\cfgAtributos.pas',
  AtrDef in '..\..\..\..\..\..\UTL\_FRM\v01_01\AtrDef.pas' {FmAtrDef},
  AtrCad in '..\..\..\..\..\..\UTL\_FRM\v01_01\AtrCad.pas' {FmAtrCad},
  UnServices in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\Comp\UnServices.pas',
  Sel2Cod in '..\..\..\..\..\..\UTL\_FRM\v01_01\Sel2Cod.pas' {FmSel2Cod},
  App_Enums in '..\..\..\ProjGroup\v01_01\App_Enums.pas',
  BWLiOrIts in '..\Units\BWLiOrIts.pas' {FmBWLiOrIts},
  BWLiOrCab in '..\Units\BWLiOrCab.pas' {FmBWLiOrCab},
  UnDmkPing in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnDmkPing.pas',
  CNPJa in '..\..\..\..\..\..\UTL\_UNT\CNPJa.pas',
  CNPJaConsulta in '..\..\..\..\..\..\UTL\_UNT\ConsultaCNPJ\CNPJaConsulta.pas' {FmCNPJaConsulta},
  Pkg.Json.DTO in '..\..\..\..\..\..\UTL\_UNT\ConsultaCNPJ\Pkg.Json.DTO.pas',
  RootUnit in '..\..\..\..\..\..\UTL\_UNT\ConsultaCNPJ\RootUnit.pas',
  GetHora in '..\..\..\..\..\..\UTL\_FRM\v01_01\GetHora.pas' {FmGetHora};

{$R *.res}

const
  NomeForm = 'BidWon';
var
  Form: PChar;
  StyleName: String;
begin
  Form := PChar(NomeForm);
  if OpenMutex(MUTEX_ALL_ACCESS, False, Form) = 0 then
  begin
    CreateMutex(nil, False, Form);
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    //
    try
      StyleName := Geral.ReadAppKeyCU('StyleName', Application.Title, ktString, 'Turquoise Gray');
      TStyleManager.TrySetStyle(StyleName);
      //
    except
      //
    end;
    //TStyleManager.TrySetStyle(StyleName);
    //
    Application.Title := 'BidWon';
    Application.Name  := CO_APPLICATION_NAME_BIDWON; // N�o mudar!!! usa para definir diret�rio de dados!!!
    if CO_VERMCW > CO_VERMLA then
      CO_VERSAO := CO_VERMCW
    else
      CO_VERSAO := CO_VERMLA;


  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.CreateForm(TFmMeuDBUses, FmMeuDBUses);
  Application.CreateForm(TGMod, GMod);
  //Application.CreateForm(TDmOVS, DmOVS);
  Application.CreateForm(TFmBidWon_Dmk, FmBidWon_Dmk);

  Application.Run;
  end
  else
  begin
    Geral.MB_Aviso('O Aplicativo j� esta em uso.');
    SetForeGroundWindow(FindWindow('TFmBlueDerm_dmk', Form));
  end;

end.
