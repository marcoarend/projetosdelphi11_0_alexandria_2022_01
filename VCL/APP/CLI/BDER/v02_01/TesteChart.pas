unit TesteChart;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  dmkGeral,
  //
  frxClass, frxChart, TypInfo, Vcl.ExtCtrls,
  VCLTee.TeeProcs, VCLTee.TeEngine, VCLTee.Chart, VCLTee.Series, VCLTee.TeCanvas
  //TeeProcs, TeEngine, Chart, Series, TeCanvas
 {$IFDEF TeeChartPro}, VCLTEE.TeeEdit{$IFNDEF TeeChart4}, VCLTEE.TeeEditCha{$ENDIF} {$ENDIF}
 {$IFDEF TeeChartPro}, TeeEdit{$IFNDEF TeeChart4}, TeeEditCha{$ENDIF} {$ENDIF}
;


type
  TFmTesteChart = class(TForm)
    frxReport: TfrxReport;
    Shape1: TShape;
    procedure frxReportGetValue(const VarName: string; var Value: Variant);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FSeries: Integer;

    FYCount: Integer;
    FYValues: array of array of String;
    FTitles: array of String;
    FXSource: String;
(*
    FNResp: Boolean;
    FXValues: array of String;
    FCores: array of TColor;
*)
  public
    { Public declarations }
    procedure MostrafrxChart();
  end;

var
  FmTesteChart: TFmTesteChart;

implementation

uses UnMyObjects;

var
  FSeries: Integer = 0;
  FYCount: Integer = 0;
  //FNResp: Boolean;
  FXSource: String;
  FXValues: array of String;
  FYValues: array of array of String;
  FTitles: array of String;
  FCores: array of TColor;

{$R *.dfm}

procedure TFmTesteChart.FormShow(Sender: TObject);
begin
  MostrafrxChart();
end;

procedure TFmTesteChart.frxReportGetValue(const VarName: string;
  var Value: Variant);
var
  Chart1: TfrxChartView;
  //
  YSource: String;
  I, J: Integer;
begin
  if VarName = 'VAR_LINE_TEE_006' then
  begin
////////////////////////////////////////////////////////////////////////////////
    FSeries := 4;
    FYCount := 3;
    SetLength(FYValues, FSeries);
    SetLength(FTitles, FSeries);
    //SetLength(FXSource, FYCount);
    //
    FYValues[0][0] := '31.50';
    FYValues[0][1] := '28.54';
    FYValues[0][2] := '31.58';
    FTitles[0] := 'A';
    //
    FYValues[1][0] := '41.50';
    FYValues[1][1] := '38.54';
    FYValues[1][2] := '41.58';
    FTitles[1] := 'B';
    //
    FYValues[2][0] := '51.50';
    FYValues[2][1] := '48.54';
    FYValues[2][2] := '51.58';
    FTitles[2] := 'C';
    //
    FYValues[3][0] := '61.50';
    FYValues[3][1] := '58.54';
    FYValues[3][2] := '61.58';
    FTitles[3] := 'D';
    //
    FXSource := 'Valor 1;Valor 2; Valor3';
    //
////////////////////////////////////////////////////////////////////////////////
    Value := True;
    //
    Chart1 := frxReport.FindObject('Chart006A') as TfrxChartView;
    while Chart1.SeriesData.Count > 0 do
      Chart1.SeriesData[0].Free;
    while Chart1.Chart.SeriesCount > 0 do
      Chart1.Chart.Series[0].Free;
    //
    for I := 0 to FSeries - 1 do
    begin
      YSource := '';
      for J := 0 to FYCount - 1 do
      begin
        YSource := YSource + ';' + FYValues[I][J];
      end;
      if Length(YSource) > 0 then
        YSource := Copy(YSource, 2);

      Chart1.AddSeries(TfrxChartSeries.csLine);

      // do VCLTee.Chart.CustomChart:
      Chart1.Chart.Series[I].Pen.Width := 4;
      Chart1.Chart.Series[I].LegendTitle := FTitles[I];
      //Chart1.Chart.Series[I].Color := FCores[I];

      // do frxChat
      Chart1.SeriesData[I].DataType := dtFixedData;
      Chart1.SeriesData[I].XSource := FXSource; //'value1; value2; value3';
      Chart1.SeriesData[I].YSource := YSource; //'31.5;28.54;31.58';
      Chart1.SeriesData[I].SortOrder := soNone;
      Chart1.SeriesData[I].TopN := 0;
      Chart1.SeriesData[I].XType := xtText;
    end;
  end;
end;

procedure TFmTesteChart.MostrafrxChart;
  procedure SVP_Primit(Compo: TComponent; Propriedade: String; Valor: Variant);
  var
    PropInfo: PPropInfo;
  begin
    if Compo <> nil then
    begin
      PropInfo := GetPropInfo(Compo, Propriedade);
      if PropInfo <> nil then
        SetPropValue(Compo, Propriedade, Valor)
    end;
  end;
{
  function  SVP_Comp(Compo: TComponent; Propriedade: String): PropType;
  var
    PropInfo: PPropInfo;
    PropTy
  begin
    if Compo <> nil then
    begin
      PropInfo := GetPropInfo(Compo, Propriedade);
      if PropInfo <> nil then
        Result := PropInfo.PropType;
    end;
  end;
}
  procedure SVP_N(Compo: TComponent; Propriedade: array of String;
  Valor: Variant);
  var
    PropInfo: PPropInfo;
    I, K: Integer;
    Continua: Boolean;
  begin
    if Compo <> nil then
    begin
      for I := 0 to K - 1 do
      begin
        PropInfo := GetPropInfo(Compo, Propriedade[I]);
        if PropInfo <> nil then
        begin
          if I = K - 1 then
            SetPropValue(Compo, Propriedade[I], Valor)
          else
            Continua := True;
        end;
      end;
    end;
  end;
  //
  function SVP_O(Compo: TObject; Propriede: String; Valor: Variant): Boolean;
  var
    PropInfo: PPropInfo;
  begin
    Result := False;
    if Compo <> nil then
    begin
      PropInfo := GetPropInfo(Compo, Propriede);
      if PropInfo <> nil then
      begin
        SetPropValue(Compo, Propriede, Valor);
        Result := Valor;
      end;
    end;
  end;
  //
  function  ObtemObjeto(const AObject: TObject; const Nome: String; const
  Classe: TClass; var Objeto: TObject): Boolean;
  //
  var
    I, Count: Integer;
    PropInfo: PPropInfo;
    TempList: PPropList;
    LObject: TObject;
  begin
    Result := False;
    Objeto := nil;
    Count := GetPropList(AObject, TempList);
    if Count > 0 then
    try
      for I := 0 to Count - 1 do
      begin
        PropInfo := TempList^[I];
        if GetPropName(PropInfo) = Nome then
        begin
          //Geral.MB_Info('Nome PropInfo: ' + GetPropName(PropInfo));
          if (PropInfo^.PropType^.Kind = tkClass) and
          Assigned(PropInfo^.GetProc) and
          Assigned(PropInfo^.SetProc) then
          begin
            LObject := GetObjectProp(AObject, PropInfo);
            if LObject <> nil then
            begin
              Objeto := LObject;
              Result := True;
              //
              Exit;
            end;
          end;
        end;
      end;
    finally
      FreeMem(TempList);
    end;
  end;
  //
  function  SVP_Objeto(const AObject: TObject; const Nome: String; const
  Classe: TClass; const Propriedade: String; const Valor: Variant): Boolean;
  var
    Objeto: TObject;
  begin
    Result := False;
    if ObtemObjeto(AObject, Nome, Classe, Objeto) then
      Result := SVP_O(Objeto, Propriedade, Valor);
  end;
  //
{
  function  SVP_Objeto(const AObject: TObject; const Nome: String; const
  Classe: TClass; const Propriedade: String; const Valor: Variant): Boolean;
  //
  function SVP(Compo: TObject; Propried: String; Valr: Variant): Boolean;
  var
    PI: PPropInfo;
  begin
    Result := False;
    if Compo <> nil then
    begin
      PI := GetPropInfo(Compo, Propried);
      if PI <> nil then
      begin
        SetPropValue(Compo, Propried, Valr);
        Result := Valr;
      end;
    end;
  end;
  //
  var
    I, Count: Integer;
    PropInfo: PPropInfo;
    TempList: PPropList;
    LObject: TObject;
  begin
    Result := False;
    //Objeto := nil;
    Count := GetPropList(AObject, TempList);
    if Count > 0 then
    try
      for I := 0 to Count - 1 do
      begin
        PropInfo := TempList^[I];
        if GetPropName(PropInfo) = Nome then
        begin
          //Geral.MB_Info('Nome PropInfo: ' + GetPropName(PropInfo));
          if (PropInfo^.PropType^.Kind = tkClass) and
          Assigned(PropInfo^.GetProc) and
          Assigned(PropInfo^.SetProc) then
          begin
            LObject := GetObjectProp(AObject, PropInfo);
            if LObject <> nil then
            begin
              if (LObject.ClassType = Classe) then
              begin
                Result := SVP(LObject, Propriedade, Valor);
                (*
                Objeto := LObject;
                Result := True;
                //
                *)
                Exit;
              end;
            end;
          end;
        end;
      end;
    finally
      FreeMem(TempList);
    end;
  end;
}
  //
const
  A_AreaLinEx = $00AAA8A6; // 166-168-170
  B_AreaAreas = $00C0BEBC; // 188-190-192
  C_AreaLinVI = $00B3B1AF; // 175-177-179
  D_AreaLinHI = $00BBB9B7; // 183-185-187
  E_AreaLinHE = $00EDEDEC; // 236-237-237
var
  Chart1: TfrxChartView;
  //AreaSeries: TfrxAreaChartView;
  ChartPen: TChartPen;
  I, J: Integer;
  YSource: String;
  Objeto: TObject;
  iSerie: TChartSeries;
begin
  Chart1 := frxReport.FindObject('Chart01A') as TfrxChartView;
(*
  //Changing colors:
  // Changing the color of a serie:
  //TBarSeries(Chart1.Series[0]).SeriesColor := clRed;
  // Changing the color of the axis' title:
  TTeePen(Chart1.Chart.LeftAxis.Title.Font).Color := clRed;
  // Changing the color of an axis:
  TPen(Chart1.Chart.LeftAxis.Axis).Color := clRed;

  Changing values:
  CODE
  // Changing the datasource of a serie:
  Chart1.SeriesData[0].Source2 := '<Dataset1."Field1">';

  // Changing the title of a serie (visible in the ledgend):
  TBarSeries(Chart1.Series[0]).Title := 'My Series Title';

  // Changing the title of an axis:
  Chart1.Chart.LeftAxis.Title.Caption := 'Temperature / �C';

  LeftAxis := Ca as TfrxChartView;
*)
  Chart1.Chart.LeftAxis.Axis.Color := clSilver;
  Chart1.Chart.LeftAxis.Axis.Width := 2;
  Chart1.Chart.BottomAxis.Axis.Color := clSilver;
  Chart1.Chart.BottomAxis.Axis.Width := 2;
  //
  while Chart1.SeriesData.Count > 0 do
    Chart1.SeriesData[0].Free;
  while Chart1.Chart.SeriesCount > 0 do
    Chart1.Chart.Series[0].Free;
  //
  //Chart1.Chart.Legend.LegendStyle := lsSerie;
  I := 1;
  FSeries := I;
  SetLength(FTitles, I);
  FTitles[I] := 'Titulo I1';
  for I := 0 to FSeries - 1 do
  begin
    YSource := '';
    for J := 0 to FYCount - 1 do
    begin
      YSource := YSource + ';' + FYValues[I][J];
    end;
    if Length(YSource) > 0 then
      YSource := Copy(YSource, 2);
    //
    //Chart1.AddSeries(TfrxChartSeries.csArea);
    Chart1.AddSeries(TfrxChartSeries.csArea);

    // do VCLTee.Chart.CustomChart:
    Chart1.Chart.Series[I].Pen.Width := 4;
    Chart1.Chart.Series[I].LegendTitle := FTitles[I];
    //Chart1.Chart.Series[I].Color := FCores[I];

    // do frxChat
    Chart1.SeriesData[I].DataType := dtFixedData;
    Chart1.SeriesData[I].XSource := (*FXSource; //*)'value0; value1; value2; value3';
    Chart1.SeriesData[I].YSource := (*YSource; //*)'0;31.5;28.54;31.58';
    Chart1.SeriesData[I].SortOrder := soNone;
    Chart1.SeriesData[I].TopN := 0;
    Chart1.SeriesData[I].XType := xtText;
  end;
  // Gr�fico em si (sem nenhuma s�rie - fundo vazio)
  Chart1.Chart.LeftAxis.Axis.Color := clSilver;
  Chart1.Chart.LeftAxis.Axis.Width := 1;
  Chart1.Chart.BottomAxis.Axis.Color := clSilver;
  Chart1.Chart.BottomAxis.Axis.Width := 1;
  //
  Chart1.Chart.LeftAxis.Grid.Color     := E_AreaLinHE;
  Chart1.Chart.LeftAxis.Grid.DrawEvery := 3; //
  Chart1.Chart.LeftAxis.Grid.Style     := TPenStyle.psSolid;


  //
  iSerie := Chart1.Chart.Series[0];
  // (A) - Linha do gr�fico de �rea
  SVP_Objeto(iSerie,  'LinePen', TChartPen, 'Color', A_AreaLinEx);
  SVP_Objeto(iSerie,  'LinePen', TChartPen, 'Width', 2);
  // (B) - Area do gr�fico �rea
  SVP_Primit(iSerie, 'AreaColor', B_AreaAreas);
  // (C) - Linhas verticais dentro da �rea ocupada do gr�fico �rea
  SVP_Objeto(iSerie,  'AreaLinesPen', TChartPen, 'Color', C_AreaLinVI);
  SVP_Objeto(iSerie,  'AreaLinesPen', TChartPen, 'Width', 1);
  //
(*
  ObtemObjeto(Chart1.Chart.Series[0],  'AreaLinesPen', TChartPen, 'Color', clGreen);
  //Objeto := nil;
  if ObtemObjeto(Chart1.Chart.Series[0],  'LinePen', TChartPen, Objeto) then
  begin
    TChartPen(Objeto).Color := clpurple;
    TChartPen(Objeto).Width := 2;
  end;
*)
  //ChartPen := TChartPen(GetObjectPropClass(Chart1.Chart.Series[0], 'AreaLinesPen'));
  //ChartPen.Color := clGreen;
  //ChartBrush := TChartBrush(GetPropValue(Chart1.Chart.Series[0], 'AreaLinesPen'));
  //ChartBrush.
  ///
  //SVP(Chart1.Chart.Series[0], ['AreaLinesPen', 'Color'], A_AreaLinEx);
  //SVP(Chart1.Chart.Series[0], ['AreaLinesPen', 'Width'], 2);
  //
  //Chart1.Chart.Series[0].Color := clYellow;

  //Chart1.Chart.Series[I] as TfrxSeriesItem(Chart1.Chart.Series[I]).
  //
  MyObjects.frxMostra(frxReport, 'Teste Chart');
end;


(*
  Interactive Chart in FastReport:
  https://www.fast-report.com/en/blog/181/show/
*)
end.
