object FmTesteChart: TFmTesteChart
  Left = 0
  Top = 0
  Caption = 'FmTesteChart'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Shape1: TShape
    Left = 112
    Top = 148
    Width = 65
    Height = 65
    Brush.Color = 15592940
  end
  object frxReport: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '              '
      'end.')
    Left = 272
    Top = 92
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object Chart01A: TfrxChartView
        Top = 15.118120000000000000
        Width = 680.315400000000000000
        Height = 268.346630000000000000
        HighlightColor = clBlack
        Chart = {
          5450463006544368617274054368617274044C656674020003546F7002000557
          696474680390010648656967687403FA000E4261636B57616C6C2E436F6C6F72
          0706636C4C696D65144261636B57616C6C2E50656E2E56697369626C6508124C
          6567656E642E4672616D652E436F6C6F720708636C53696C766572164C656765
          6E642E4672616D652E536D616C6C446F747309154C6567656E642E536861646F
          772E56697369626C65080D4672616D652E56697369626C65081450616E6E696E
          672E496E73696465426F756E6473090656696577334408165669657733444F70
          74696F6E732E526F746174696F6E02000A426576656C4F75746572070662764E
          6F6E6505436F6C6F720707636C576869746511436F6C6F7250616C6574746549
          6E646578020D000B54417265615365726965730753657269657333134D61726B
          732E4172726F772E56697369626C6509194D61726B732E43616C6C6F75742E42
          727573682E436F6C6F720707636C426C61636B1B4D61726B732E43616C6C6F75
          742E4172726F772E56697369626C65090D4D61726B732E56697369626C650808
          44726177417265610916506F696E7465722E496E666C6174654D617267696E73
          090D506F696E7465722E5374796C65070B707352656374616E676C650F506F69
          6E7465722E56697369626C65080C5856616C7565732E4E616D650601580D5856
          616C7565732E4F72646572070B6C6F417363656E64696E670C5956616C756573
          2E4E616D650601590D5956616C7565732E4F7264657207066C6F4E6F6E650000
          00}
        ChartElevation = 345
        SeriesData = <
          item
            InheritedName = 'TfrxSeriesItem4'
            DataType = dtFixedData
            SortOrder = soNone
            TopN = 0
            XType = xtText
            Source2 = '0;21.5;68.54;11.58'
            Source3 = 'value0; value1; value2; value3'
            YSource = '0;21.5;68.54;11.58'
          end>
      end
    end
  end
end
