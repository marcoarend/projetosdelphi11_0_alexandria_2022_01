  object QrSomaM: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT (SUM(Credito) - SUM(Debito)) Valor'
      'FROM lanctos')
  object QrLct: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR'
      'FROM lanctos la'

        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'ORDER BY la.Data, la.Controle')
  object QrTransf: TmySQLQuery
    Database = MyDB
    SQL.Strings = (

        'SELECT Data, Tipo, Carteira, Controle, Genero, Debito, Credito, ' +
        'Documento FROM lanctos'
      'WHERE Controle=:P0'
      '')
  object QrErrQuit: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT lan.Controle Controle1, la2.Controle Controle2, '
      'la2.Data, lan.Sit, lan.Descricao'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN lanctos   la2 ON la2.ID_pgto=lan.Controle'
      ''
      'WHERE lan.Tipo=2'
      'AND lan.Sit=2'
      'AND lan.Credito-lan.Pago = 0'
      'AND lan.Controle<>la2.Controle'
      'AND lan.ID_Quit=0'
      '')
  object QrAPL: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito, SUM(Debito) Debito, '
      'SUM(MoraVal) MoraVal, SUM(MultaVal) MultaVal,'
      'MAX(Data) Data '
      'FROM lanctos WHERE ID_Pgto=:P0')
  object QrVLA: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Credito, Debito '
      'FROM lanctos '
      'WHERE Sub=0 '
      'AND Controle=:P0')


arthur thomas 806
