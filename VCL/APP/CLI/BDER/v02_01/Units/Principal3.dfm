object FmPrincipal3: TFmPrincipal3
  Left = 0
  Top = 0
  Caption = 'FmPrincipal3'
  ClientHeight = 548
  ClientWidth = 1014
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Ribbon1: TRibbon
    Left = 0
    Top = 0
    Width = 1014
    Height = 143
    Caption = 'Ribbon1'
    Tabs = <
      item
        Caption = 'Cadastros de coisas longas'
        Page = RibbonPage1
      end
      item
        Caption = 'RibbonPage2'
        Page = RibbonPage2
      end
      item
        Caption = 'RibbonPage3'
        Page = RibbonPage3
      end>
    TabIndex = 2
    DesignSize = (
      1014
      143)
    StyleName = 'Ribbon - Luna'
    object RibbonPage1: TRibbonPage
      Left = 0
      Top = 50
      Width = 1013
      Height = 93
      Caption = 'Cadastros de coisas longas'
      Index = 0
    end
    object RibbonPage2: TRibbonPage
      Left = 0
      Top = 50
      Width = 1013
      Height = 93
      Caption = 'RibbonPage2'
      Index = 1
    end
    object RibbonPage3: TRibbonPage
      Left = 0
      Top = 50
      Width = 1013
      Height = 93
      Caption = 'RibbonPage3'
      Index = 2
      object RibbonGroup1: TRibbonGroup
        Left = 4
        Top = 3
        Width = 205
        Height = 86
        Caption = 'RibbonGroup1'
        GroupIndex = 0
        object BitBtn1: TBitBtn
          Tag = 15
          Left = 4
          Top = 2
          Width = 101
          Height = 34
          Caption = 'BitBtn1'
          NumGlyphs = 2
          TabOrder = 0
        end
        object BitBtn2: TBitBtn
          Tag = 22
          Left = 4
          Top = 35
          Width = 113
          Height = 34
          Caption = 'BitBtn2'
          NumGlyphs = 2
          TabOrder = 1
        end
      end
    end
  end
end
