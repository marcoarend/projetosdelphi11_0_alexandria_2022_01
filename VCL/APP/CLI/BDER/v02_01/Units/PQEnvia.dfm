object FmPQEnvia: TFmPQEnvia
  Left = 201
  Top = 154
  Caption = 'QUI-ENTRA-006 :: Envio de Pedido de Insumos por E-mail'
  ClientHeight = 686
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 530
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 8
      Width = 90
      Height = 13
      Caption = 'Servidor de SMTP:'
    end
    object Label2: TLabel
      Left = 12
      Top = 48
      Width = 76
      Height = 13
      Caption = 'Nome da conta:'
    end
    object Label4: TLabel
      Left = 12
      Top = 88
      Width = 31
      Height = 13
      Caption = 'E-mail:'
    end
    object Label5: TLabel
      Left = 12
      Top = 128
      Width = 74
      Height = 13
      Caption = 'Dono da conta:'
    end
    object Label9: TLabel
      Left = 12
      Top = 168
      Width = 41
      Height = 13
      Caption = 'Assunto:'
    end
    object Label8: TLabel
      Left = 12
      Top = 284
      Width = 25
      Height = 13
      Caption = 'Para:'
    end
    object Label10: TLabel
      Left = 244
      Top = 284
      Width = 45
      Height = 13
      Caption = 'CC Cega:'
    end
    object Label11: TLabel
      Left = 244
      Top = 8
      Width = 100
      Height = 13
      Caption = 'Corpo da mensagem:'
    end
    object Label12: TLabel
      Left = 12
      Top = 348
      Width = 17
      Height = 13
      Caption = 'CC:'
    end
    object Label13: TLabel
      Left = 572
      Top = 284
      Width = 104
      Height = 13
      Caption = 'Mensagens de status:'
    end
    object Label14: TLabel
      Left = 12
      Top = 208
      Width = 110
      Height = 13
      Caption = 'Anexos (insert - delete):'
    end
    object Label15: TLabel
      Left = 244
      Top = 340
      Width = 103
      Height = 13
      Caption = 'Conex'#227'o rede dial-up:'
    end
    object EdSMTP: TEdit
      Left = 12
      Top = 24
      Width = 220
      Height = 21
      Color = clWhite
      TabOrder = 0
    end
    object EdConta: TEdit
      Left = 12
      Top = 64
      Width = 220
      Height = 21
      Color = clWhite
      TabOrder = 1
    end
    object EdEMail: TEdit
      Left = 12
      Top = 104
      Width = 220
      Height = 21
      Color = clWhite
      TabOrder = 2
    end
    object EdDono: TEdit
      Left = 12
      Top = 144
      Width = 220
      Height = 21
      Color = clWhite
      TabOrder = 3
    end
    object EdAssunto: TEdit
      Left = 12
      Top = 184
      Width = 220
      Height = 21
      Color = clWhite
      TabOrder = 4
    end
    object MePara: TMemo
      Left = 12
      Top = 300
      Width = 221
      Height = 45
      Color = clWhite
      TabOrder = 5
    end
    object MeCega: TMemo
      Left = 244
      Top = 300
      Width = 217
      Height = 37
      Color = clWhite
      TabOrder = 6
    end
    object MeCC: TMemo
      Left = 12
      Top = 364
      Width = 221
      Height = 57
      Color = clWhite
      TabOrder = 7
    end
    object MeCorpo: TMemo
      Left = 244
      Top = 24
      Width = 537
      Height = 257
      Color = clWhite
      TabOrder = 8
    end
    object Memo5: TMemo
      Left = 572
      Top = 300
      Width = 209
      Height = 121
      TabStop = False
      ReadOnly = True
      TabOrder = 9
    end
    object LBAnexos: TListBox
      Left = 12
      Top = 224
      Width = 221
      Height = 57
      Color = clWhite
      ItemHeight = 13
      TabOrder = 10
      OnKeyDown = LBAnexosKeyDown
    end
    object CheckBox1: TCheckBox
      Left = 432
      Top = 356
      Width = 133
      Height = 17
      Caption = 'Setar valor de limpeza.'
      TabOrder = 11
    end
    object RadioGroup1: TRadioGroup
      Left = 468
      Top = 284
      Width = 97
      Height = 53
      Caption = ' Encode: '
      ItemIndex = 0
      Items.Strings = (
        'MIME'
        'UUEncode')
      TabOrder = 12
    end
    object EdDialUp: TEdit
      Left = 244
      Top = 356
      Width = 177
      Height = 21
      Color = clWhite
      TabOrder = 13
    end
    object BtLimpa: TBitBtn
      Left = 244
      Top = 380
      Width = 133
      Height = 40
      Caption = '&Limpar par'#226'metros'
      NumGlyphs = 2
      TabOrder = 14
      OnClick = BtLimpaClick
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 549
        Height = 32
        Caption = 'Envio de pedido de PQ por servidor de SMTP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 549
        Height = 32
        Caption = 'Envio de pedido de PQ por servidor de SMTP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 549
        Height = 32
        Caption = 'Envio de pedido de PQ por servidor de SMTP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 578
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 622
    Width = 792
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 644
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela de cadastro de senhas'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtEMail: TBitBtn
        Tag = 66
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Direto'
        TabOrder = 1
        OnClick = BtEMailClick
      end
      object BitBtn1: TBitBtn
        Tag = 66
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Outlook express'
        TabOrder = 2
        OnClick = BtEMailClick
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 404
    Top = 180
  end
  object Query1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 224
    Top = 144
  end
end
