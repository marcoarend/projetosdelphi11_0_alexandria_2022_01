unit PQxExcl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Mask, DBCtrls, Db,
  mySQLDbTables, ComCtrls, Grids, DBGrids, Menus, dmkDBLookupComboBox,
  dmkEditCB, dmkEdit, dmkLabel, dmkImage, UnDMkProcFunc, AppListas,
  UnProjGroup_Consts, UnDmkEnums, dmkEditDateTimePicker, UnAppEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmPQxExcl = class(TForm)
    Panel1: TPanel;
    Label3: TLabel;
    EdPesagem: TdmkEdit;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit6: TDBEdit;
    QrEmit: TmySQLQuery;
    QrEmitCodigo: TIntegerField;
    QrEmitDataEmis: TDateTimeField;
    QrEmitStatus: TSmallintField;
    QrEmitNumero: TIntegerField;
    QrEmitNOMECI: TWideStringField;
    QrEmitNOMESETOR: TWideStringField;
    QrEmitTecnico: TWideStringField;
    QrEmitNOME: TWideStringField;
    QrEmitClienteI: TIntegerField;
    QrEmitTipificacao: TSmallintField;
    QrEmitTempoR: TSmallintField;
    QrEmitTempoP: TSmallintField;
    QrEmitSetor: TSmallintField;
    QrEmitEspessura: TWideStringField;
    QrEmitPeso: TFloatField;
    QrEmitQtde: TFloatField;
    QrEmitLk: TIntegerField;
    QrEmitDataCad: TDateField;
    QrEmitDataAlt: TDateField;
    QrEmitUserCad: TIntegerField;
    QrEmitUserAlt: TIntegerField;
    QrEmitAreaM2: TFloatField;
    QrEmitFulao: TWideStringField;
    QrEmitDefPeca: TWideStringField;
    QrEmitCusto: TFloatField;
    DsEmit: TDataSource;
    QrMPIn: TmySQLQuery;
    QrMPInMPIn: TIntegerField;
    QrPQx: TmySQLQuery;
    QrPQxInsumo: TIntegerField;
    QrPQxCliOrig: TIntegerField;
    ProgressBar1: TProgressBar;
    DsPQx: TDataSource;
    PMExclui: TPopupMenu;
    Itemdepesagem1: TMenuItem;
    Todapesagem1: TMenuItem;
    QrPQxDataX: TDateField;
    QrPQxTipo: TSmallintField;
    QrPQxCliDest: TIntegerField;
    QrPQxPeso: TFloatField;
    QrPQxValor: TFloatField;
    QrPQxOrigemCodi: TIntegerField;
    QrPQxOrigemCtrl: TIntegerField;
    QrEmitObs: TWideStringField;
    QrPQxNOMECLI: TWideStringField;
    QrPQxNOMEPQ: TWideStringField;
    QrPQxCUSTOMEDIO: TFloatField;
    QrPQ: TmySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsCI: TDataSource;
    QrCI: TmySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    DsPQ: TDataSource;
    QrSaldo: TmySQLQuery;
    QrSaldoPeso: TFloatField;
    QrSaldoValor: TFloatField;
    QrSaldoCUSTO: TFloatField;
    DsSaldo: TDataSource;
    QrSaldoCI: TIntegerField;
    QrSaldoPQ: TIntegerField;
    QrSumEmit: TmySQLQuery;
    QrSumEmitCUSTO: TFloatField;
    QrPesq: TmySQLQuery;
    DsEmitCusBH: TDataSource;
    Label15: TLabel;
    DBEdit10: TDBEdit;
    QrEmitCusBH: TmySQLQuery;
    QrEmitCusBHMarca: TWideStringField;
    QrEmitCusBHDataMPIn: TDateField;
    QrEmitCusBHPecas: TFloatField;
    QrEmitCusBHPeso: TFloatField;
    QrEmitCusBHCusto: TFloatField;
    QrEmitCusBHMPIn: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBConfirma: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    BtConfirma2: TBitBtn;
    BtDesiste2: TBitBtn;
    GBControle: TGroupBox;
    Panel6: TPanel;
    Panel7: TPanel;
    BtImprime: TBitBtn;
    BtPesquisa: TBitBtn;
    BitBtn1: TBitBtn;
    BtAdiciona: TBitBtn;
    BtExclui: TBitBtn;
    BtCancela: TBitBtn;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    PnInsumos: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    EdPesoAdd: TdmkEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    EdSaldoFut: TdmkEdit;
    QrEmitAlterWeb: TSmallintField;
    QrEmitTipific: TSmallintField;
    QrEmitAtivo: TSmallintField;
    QrEmitSetrEmi: TSmallintField;
    QrEmitCod_Espess: TIntegerField;
    QrEmitCodDefPeca: TIntegerField;
    QrEmitSourcMP: TSmallintField;
    PmManyGrids: TPanel;
    DBGEmitCusBH: TDBGrid;
    DBGWBMovIts: TDBGrid;
    QrWBMovIts: TmySQLQuery;
    QrWBMovItsCodigo: TIntegerField;
    QrWBMovItsControle: TIntegerField;
    QrWBMovItsMovimCod: TIntegerField;
    QrWBMovItsEmpresa: TIntegerField;
    QrWBMovItsMovimID: TIntegerField;
    QrWBMovItsGraGruX: TIntegerField;
    QrWBMovItsPecas: TFloatField;
    QrWBMovItsPesoKg: TFloatField;
    QrWBMovItsAreaM2: TFloatField;
    QrWBMovItsAreaP2: TFloatField;
    QrWBMovItsLk: TIntegerField;
    QrWBMovItsDataCad: TDateField;
    QrWBMovItsDataAlt: TDateField;
    QrWBMovItsUserCad: TIntegerField;
    QrWBMovItsUserAlt: TIntegerField;
    QrWBMovItsAlterWeb: TSmallintField;
    QrWBMovItsAtivo: TSmallintField;
    QrWBMovItsNO_PRD_TAM_COR: TWideStringField;
    QrWBMovItsSrcMovID: TIntegerField;
    QrWBMovItsSrcNivel1: TIntegerField;
    QrWBMovItsSrcNivel2: TIntegerField;
    QrWBMovItsPallet: TIntegerField;
    QrWBMovItsNO_PALLET: TWideStringField;
    QrWBMovItsSdoVrtArM2: TFloatField;
    QrWBMovItsSdoVrtPeca: TFloatField;
    QrWBMovItsObserv: TWideStringField;
    QrWBMovItsValorT: TFloatField;
    DsWBMovIts: TDataSource;
    DBGEmitCusWE: TDBGrid;
    QrEmitCusWE: TmySQLQuery;
    QrEmitCusWETextoECor: TWideStringField;
    QrEmitCusWENOME_CLI: TWideStringField;
    QrEmitCusWECodigo: TIntegerField;
    QrEmitCusWEControle: TIntegerField;
    QrEmitCusWEMPIn: TIntegerField;
    QrEmitCusWEFormula: TIntegerField;
    QrEmitCusWEDataEmis: TDateTimeField;
    QrEmitCusWEPeso: TFloatField;
    QrEmitCusWECusto: TFloatField;
    QrEmitCusWEPecas: TFloatField;
    QrEmitCusWEAtivo: TSmallintField;
    QrEmitCusWEMPVIts: TIntegerField;
    QrEmitCusWEAreaM2: TFloatField;
    QrEmitCusWEAreaP2: TFloatField;
    DsEmitCusWE: TDataSource;
    Splitter1: TSplitter;
    Pesagem1: TMenuItem;
    Lotedesemiacabado1: TMenuItem;
    Matriaprima1: TMenuItem;
    Produto1: TMenuItem;
    Lotedecurtimento1: TMenuItem;
    Recurtimento1: TMenuItem;
    Panel3: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    QrPesqCodigo: TIntegerField;
    SbImprime: TBitBtn;
    PMImprime: TPopupMenu;
    Estoquedematriaprimadesemiacabadomostraroutrajanela1: TMenuItem;
    QrMPInMPVIts: TIntegerField;
    QrEmitCustoTo: TFloatField;
    QrEmitCustoKg: TFloatField;
    QrEmitCustoM2: TFloatField;
    QrEmitCusUSM2: TFloatField;
    QrEmitEmitGru: TIntegerField;
    QrEmitRetrabalho: TSmallintField;
    QrEmitNO_EmitGru: TWideStringField;
    QrEmitSemiAreaM2: TFloatField;
    QrEmitSemiRendim: TFloatField;
    QrEmitBRL_USD: TFloatField;
    QrEmitBRL_EUR: TFloatField;
    QrEmitDtaCambio: TDateField;
    QrLotes: TmySQLQuery;
    QrLotesControle: TIntegerField;
    QrLotesVSMovIts: TIntegerField;
    QrLotesPeso: TFloatField;
    QrLotesPecas: TFloatField;
    QrLotesSerieFch: TIntegerField;
    QrLotesFicha: TIntegerField;
    QrLotesMarca: TWideStringField;
    QrLotesNO_SerieFch: TWideStringField;
    DsLotes: TDataSource;
    DBGEmitCusVS_BH: TDBGrid;
    QrLotesCusto: TFloatField;
    DBGEmitCusVS_WE: TDBGrid;
    QrLotesOP: TIntegerField;
    PMAdiciona: TPopupMenu;
    Insumonapessagem1: TMenuItem;
    MateriaPrima1: TMenuItem;
    QrEmitVSMovCod: TIntegerField;
    Zerapreos1: TMenuItem;
    Label14: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label16: TLabel;
    Label17: TLabel;
    Label22: TLabel;
    QrEmitGraCorCad: TIntegerField;
    QrEmitNoGraCorCad: TWideStringField;
    QrEmitLinhas: TWideStringField;
    QrEmitRebLin: TWideStringField;
    QrEmitSemiCodPeca: TIntegerField;
    QrEmitSemiTxtPeca: TWideStringField;
    QrEmitSemiPeso: TFloatField;
    QrEmitSemiQtde: TFloatField;
    QrEmitSemiCodEspe: TIntegerField;
    QrEmitSemiTxtEspe: TWideStringField;
    QrEmitHoraIni: TTimeField;
    QrEmitSemiCodEspReb: TIntegerField;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    BtAltera: TBitBtn;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Label21: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrPQxEmpresa: TIntegerField;
    DBEdit19: TDBEdit;
    QrEmitNumCODIF: TWideStringField;
    QrEmitSemiTxtEspReb: TWideStringField;
    QrEmitDtaBaixa: TDateField;
    QrEmitDtCorrApo: TDateTimeField;
    QrEmitEmpresa: TIntegerField;
    QrEmitAWServerID: TIntegerField;
    QrEmitAWStatSinc: TSmallintField;
    QrEmitSbtCouIntros: TFloatField;
    QrEmitSbtAreaM2: TFloatField;
    QrEmitSbtAreaP2: TFloatField;
    QrEmitSbtRendEsper: TFloatField;
    QrEmitOperPosStatus: TSmallintField;
    QrEmitOperPosGrandz: TIntegerField;
    QrEmitOperPosQtdDon: TFloatField;
    QrEmitOperPosDthIni: TDateTimeField;
    QrEmitOperPosDthFim: TDateTimeField;
    QrEmitVersao: TIntegerField;
    Label18: TLabel;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    QrEmitTpReceita: TSmallintField;
    DBEdit22: TDBEdit;
    Label19: TLabel;
    procedure BtExcluiClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPesagemExit(Sender: TObject);
    procedure QrEmitAfterOpen(DataSet: TDataSet);
    procedure Todapesagem1Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
    procedure Itemdepesagem1Click(Sender: TObject);
    procedure EdPQChange(Sender: TObject);
    procedure EdCIChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAdicionaClick(Sender: TObject);
    procedure EdPesoAddExit(Sender: TObject);
    procedure EdPesagemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrEmitCusWEAfterScroll(DataSet: TDataSet);
    procedure QrEmitCusWEBeforeClose(DataSet: TDataSet);
    procedure QrEmitAfterScroll(DataSet: TDataSet);
    procedure QrWBMovItsAfterOpen(DataSet: TDataSet);
    procedure PMExcluiPopup(Sender: TObject);
    procedure QrEmitBeforeClose(DataSet: TDataSet);
    procedure Recurtimento1Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Estoquedematriaprimadesemiacabadomostraroutrajanela1Click(
      Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Lotedesemiacabado1Click(Sender: TObject);
    procedure Lotedecurtimento1Click(Sender: TObject);
    procedure Insumonapessagem1Click(Sender: TObject);
    procedure MateriaPrima1Click(Sender: TObject);
    procedure Matriaprima1Click(Sender: TObject);
    procedure PMImprimePopup(Sender: TObject);
    procedure Zerapreos1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure CkDtCorrApoClick(Sender: TObject);
    procedure QrEmitCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FPQx: Integer;
    procedure AtualizaLotes();
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    function  Pertence: Boolean;
    procedure CalculaSaldoFuturo;
    procedure AtualizaEmit;
    procedure ReopenEmitCusBH();
    procedure ReopenEmitCusWE(ReopenMovIts: Boolean);
    procedure ReopenEmitCusXX();
    procedure ReopenWBMovIts(Controle: Integer);
    procedure ReopenLotes();
    //
    procedure Registro(VaiPara: TVaiPara);
  public
    { Public declarations }
    procedure AtualizaCustosEmit();
    procedure ReopenEmit(Avisa: Boolean);
  end;

  var
  FmPQxExcl: TFmPQxExcl;

implementation

uses UnMyObjects, Module, PQx, FormulasImpShow, BlueDermConsts, UnInternalConsts,
  UMySQLModule, Principal, PQExclPesq, MyDBCheck, ModEmit, DmkDAC_PF, WBMovImp,
  FormulasImpShowNew, ModVS, UnVS_PF, UnPQ_PF, ModuleGeral;

{$R *.DFM}

procedure TFmPQxExcl.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmPQxExcl.BitBtn1Click(Sender: TObject);
begin
  AtualizaCustosEmit();
end;

procedure TFmPQxExcl.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQxExcl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQxExcl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQxExcl.EdPesagemExit(Sender: TObject);
begin
  ReopenEmit(False);
end;

procedure TFmPQxExcl.Recurtimento1Click(Sender: TObject);
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrWBMovItsCodigo.Value;
  MovimCod := QrWBMovItsMovimCod.Value;
  //
  if Dmod.ExcluiControleWBMovIts(QrWBMovIts, QrWBMovItsControle,
  QrWBMovItsControle.Value, QrWBMovItsSrcNivel2.Value) then
  begin
    //Dmod.AtualizaTotaisWBXxxCab('wbinncab', MovimCod);
    ReopenWBMovIts(0);
  end;
end;

procedure TFmPQxExcl.Registro(VaiPara: TVaiPara);
var
  SQL1, SQL2, Atual: String;
begin
  Atual := Geral.FF0(EdPesagem.ValueVariant);
  case VaiPara of
    vpFirst: SQL1 := 'MIN(Codigo)';
    vpPrior: SQL1 := 'MAX(Codigo)';
    vpNext:  SQL1 := 'MIN(Codigo)';
    vpLast:  SQL1 := 'MAX(Codigo)';
    else SQL1 := '????';
  end;
  case VaiPara of
    vpFirst: SQL2 := '';
    vpPrior: SQL2 := 'WHERE Codigo < ' + Atual;
    vpNext:  SQL2 := 'WHERE Codigo > ' + Atual;
    vpLast:  SQL2 := '';
    else SQL1 := '????';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
  'SELECT ' + SQL1 + ' Codigo ',
  'FROM emit ',
  SQL2,
  '']);
  EdPesagem.Text := IntToStr(QrPesqCodigo.Value);
  //
  ReopenEmit(True);
end;

procedure TFmPQxExcl.ReopenEmit(Avisa: Boolean);
var
  Pesagem: Integer;
begin
  Pesagem := EdPesagem.ValueVariant;
  QrEmit.Close;
  if Pesagem > 0 then
  begin
//    QrEmit.Params[0].AsInteger := Pesagem;
//    UnDmkDAC_PF.AbreQuery(QrEmit, Dmod.MyDB);
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
  'SELECT gru.Nome NO_EmitGru, gcc.Nome NoGraCorCad, ',
  'esp.Linhas, reb.Linhas RebLin, emi.*, ',
  'lis.TpReceita ',
  'FROM emit emi ',
  'LEFT JOIN emitgru gru ON gru.Codigo=emi.EmitGru ',
  'LEFT JOIN gracorcad gcc ON gcc.Codigo=emi.GraCorCad ',
  'LEFT JOIN espessuras esp ON esp.Codigo=emi.Espessura',
  'LEFT JOIN espessuras reb ON reb.Codigo=emi.SemiCodEspReb',
  'LEFT JOIN listasetores lis ON lis.Codigo=emi.Setor ',
  'WHERE emi.Codigo=' + Geral.FF0(Pesagem),
  '']);
  end else
  if Avisa then
    Geral.MB_Erro('Pesagem n�o localizada!');
end;

procedure TFmPQxExcl.QrEmitAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := True;
  QrPQx.Close;
  QrPQx.Params[0].AsInteger := QrEmitCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPQx, Dmod.MyDB);
  if FPQx > 0 then QrPQx.Locate('OrigemCtrl', FPqx, []);

end;

procedure TFmPQxExcl.QrEmitAfterScroll(DataSet: TDataSet);
begin
  ReopenEmitCusXX();
end;

procedure TFmPQxExcl.QrEmitBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
  QrEmitCusBH.Close;
  QrEmitCusWE.Close;
  QrLotes.Close;
end;

procedure TFmPQxExcl.Todapesagem1Click(Sender: TObject);
var
  N: Integer;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrEmitDtaBaixa.Value) then Exit;
  // 2023-09-09
  if PQ_PF.ExcluiPesagem(QrEmitCodigo.Value, VAR_FATID_0110, ProgressBar1) then
    ReopenEmit(False);
  //
(*
  N := 0;
  QrPQx.First;
  while not QrPQx.Eof do
  begin
    if Dmod.ImpedePeloRetornoPQ(
    QrPQxOrigemCodi.Value, QrPQxOrigemCtrl.Value, QrPQxTipo.Value, False) then
      N := N + 1;
    //
    QrPQx.Next;
  end;
  if N > 0 then
  begin
    Geral.MB_Aviso(Geral.FF0(N) +
    ' itens da baixa impedem a exclus�o de toda receita porque j� possuem retorno!');
    Exit;
  end;
  if QrEmitCodigo.Value <> 0 then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o de TODA pesagem?' + sLineBreak +
    'Obs.: A receita espelho ser� mantida!') = ID_YES then
    begin
      ProgressBar1.Visible := True;
      try
        if Pertence then Exit;
        ProgressBar1.Position := 0;
        ProgressBar1.Max := QrPQx.RecordCount + 5;
        //
        ProgressBar1.Position := ProgressBar1.Position + 1;
        ProgressBar1.Update;
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'DELETE FROM wbmovits ',
        'WHERE MovimID =' + Geral.FF0(Integer(emidIndsWE)),
        'AND LnkNivXtr1=' + Geral.FF0(QrEmitCodigo.Value),
        '']);
        //
        ProgressBar1.Position := ProgressBar1.Position + 1;
        ProgressBar1.Update;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM emitcus WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrEmitCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        ProgressBar1.Position := ProgressBar1.Position + 1;
        ProgressBar1.Update;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM emitits WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrEmitCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        ProgressBar1.Position := ProgressBar1.Position + 1;
        ProgressBar1.Update;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM p q x WHERE OrigemCodi=:P0 AND Tipo=110');
        Dmod.QrUpd.Params[0].AsInteger := QrEmitCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        QrPQx.First;
        while not QrPQx.Eof do
        begin
          ProgressBar1.Position := ProgressBar1.Position + 1;
          ProgressBar1.Update;
          Application.ProcessMessages;
          UnPQx.AtualizaEstoquePQ(QrPQxCliOrig.Value, QrPQxInsumo.Value, aeMsg, '');
          QrPQx.Next;
        end;
        //
        ProgressBar1.Position := ProgressBar1.Position + 1;
        ProgressBar1.Update;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM emit WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrEmitCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        ReopenEmit(False);
      except
        raise;
      end;
      Screen.Cursor := crDefault;
      ProgressBar1.Visible := False;
    end;
  end;
*)
end;

procedure TFmPQxExcl.Zerapreos1Click(Sender: TObject);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE pqx ',
  'SET Valor=0, ',
  'SdoValr=0 ',
  'WHERE CliDest>0 ',
  'AND CliDest=' + Geral.FF0(QrEmitClienteI.Value),
  '']);
end;

procedure TFmPQxExcl.BtImprimeClick(Sender: TObject);
var
  TpReceita: TReceitaTipoSetor;
begin
  case TReceitaTipoSetor(QrEmitTpReceita.Value) of
    //recsetrNenhum:
    rectipsetrRibeira:      TpReceita    := TReceitaTipoSetor.rectipsetrRibeira;
    rectipsetrRecurtimento: TpReceita    := TReceitaTipoSetor.rectipsetrRecurtimento;
    //recsetrAcabamento:
    else
    begin
      Geral.MB_Aviso(
        'Nenhum tipo de receita foi definido no cadastro do setor!' + sLineBreak +
        'Ser� utilizado o de ribeira!');
      TpReceita    := TReceitaTipoSetor.rectipsetrRecurtimento;
    end;
  end;
  PQ_PF.ReimprimePesagemNovo(QrEmitCodigo.Value, QrEmitSetrEmi.Value, TpReceita, ProgressBar1);
end;

procedure TFmPQxExcl.MateriaPrima1Click(Sender: TObject);
const
  Empresa = 0;
var
  MateriaPrima, Formula: Integer;
  BxaEstqVS: TBxaEstqVS;
  DataEmis: TDateTime;
  Qry: TmySQLQuery;
begin
  //N�o gerou custo!
  case TTipoSourceMP(QrEmitSourcMP.Value) of
    //dmktfrmSourcMP_Ribeira: PQ_PF.MostraFormFormulasImpBH();
    //dmktfrmSourcMP_WetSome: PQ_PF.MostraFormFormulasImpWE();
    dmktfrmSourcMP_Ribeira: MateriaPrima := 0;
    dmktfrmSourcMP_WetSome: MateriaPrima := 4;
    else
    begin
      MateriaPrima := -1;
      Geral.MB_Aviso('SourcMP n�o implementado! SourcMP: ' +
      Geral.FF0(QrEmitSetor.Value) + ' - ' + QrEmitNOMESETOR.Value);
    end;
  end;
  //
  if MateriaPrima > -1 then
  begin
    Formula   := QrEmitNumero.Value;
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT BxaEstqVS ',
      'FROM formulas ',
      'WHERE Numero=' + Geral.FF0(Formula),
      '']);
      BxaEstqVS := TBxaEstqVS(Qry.FieldByName('BxaEstqVS').AsInteger);
    finally
      Qry.Free;
    end;
    DataEmis  := QrEmitDataEmis.Value;
    VS_PF.MostraFormVSEmitCus(QrEmitCodigo.Value, Empresa, MateriaPrima,
      Formula, BxaEstqVS, DataEmis);
    ReopenLotes();
    // Inserir quantidades. Custo nao calculado ainda...
    AtualizaLotes();
    // .. calcular Custo.
    AtualizaCustosEmit();
  end;
end;

procedure TFmPQxExcl.Matriaprima1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrLotesControle.Value;
  //
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada da mat�ria-prima selecionada?',
  'emitcus', 'Controle', Controle, Dmod.MyDB) = ID_YES then
  begin
    //AtualizaLotes();
    AtualizaCustosEmit();
  end;
end;

procedure TFmPQxExcl.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      GBControle.Visible := True;
      GBConfirma.Visible := False;
      //PnImprime.Visible  := False;
      PnInsumos.Visible  := False;
      //
      EdPesagem.Enabled  := True;
    end;
(*
    1:
    begin
      //PnImprime.Visible  := True;
      GBConfirma.Visible := True;
      GBControle.Visible := False;
      //
      EdPesagem.Enabled  := False;
    end;
*)
    2:
    begin
      PnInsumos.Visible  := True;
      GBConfirma.Visible := True;
      GBControle.Visible := False;
      //
      EdPesagem.Enabled  := False;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  (*GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then
  begin
    FBaixaControle := Codigo;
    ReopenBaixa;
  end;*)
end;

procedure TFmPQxExcl.BtConfirma2Click(Sender: TObject);
const
  Unitario = True;
  TemIMEIMrt = 1;
var
  Controle: Integer;
  KG, RS, SF: Double;
  //
  InstanceClass: TComponentClass;
  Form: TForm;
  //
  DataX, DtCorrApo: String;
  OriCodi, OriCtrl, OriTipo, CliOrig, CliDest, Insumo, Empresa: Integer;
  Peso, Valor: Double;
begin
(*
  if PnImprime.Visible then
  begin
    BDC_APRESENTACAO_IMPRESSAO := RGImpRecRib.ItemIndex;
    VAR_SETOR    := '';
    VAR_SETORNUM := QrEmitSetor.Value;

    case Dmod.QrControleVerImprRecRib.Value of
      0:
      begin
        FrFormulasImpShow.FReImprime    := True;
        FrFormulasImpShow.FEmit         := QrEmitCodigo.Value;
        FrFormulasImpShow.FEmitGru      := QrEmitEmitGru.Value;
        FrFormulasImpShow.FVSMovCod     := QrEmitVSMovCod.Value;
        FrFormulasImpShow.FTemIMEIMrt   := TemIMEIMrt;
        FrFormulasImpShow.FEmitGru_TXT  := QrEmitNO_EmitGru.Value;
        FrFormulasImpShow.FRetrabalho   := QrEmitRetrabalho.Value;
        FrFormulasImpShow.FSourcMP      := QrEmitSourcMP.Value;
        FrFormulasImpShow.FDataP        := QrEmitDataEmis.Value;
        FrFormulasImpShow.FDtCorrApo    := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
        FrFormulasImpShow.FProgressBar1 := ProgressBar1;
        FrFormulasImpShow.FTipoPreco    := -1;// n�o deve usar
        FrFormulasImpShow.FCliIntCod    := QrEmitClienteI.Value;
        FrFormulasImpShow.FCliIntTxt    := QrEmitNOMECI.Value;
        FrFormulasImpShow.FFormula      := QrEmitNumero.Value;
        FrFormulasImpShow.FTipoImprime  := RGImprime.ItemIndex;
        FrFormulasImpShow.FPeso         := QrEmitPeso.Value;
        FrFormulasImpShow.FQtde         := QrEmitQtde.Value;
        FrFormulasImpShow.FArea          := QrEmitAreaM2.Value;
        FrFormulasImpShow.FLinhas       := QrEmitEspessura.Value;
        FrFormulasImpShow.FFulao        := QrEmitFulao.Value;
        FrFormulasImpShow.FDefPeca      := QrEmitDefPeca.Value;
        FrFormulasImpShow.FHHMM_P       := dmkPF.HorasMH(QrEmitTempoP.Value, False);
        FrFormulasImpShow.FHHMM_R       := dmkPF.HorasMH(QrEmitTempoR.Value, False);
        FrFormulasImpShow.FHHMM_T       := dmkPF.HorasMH(QrEmitTempoP.Value+QrEmitTempoR.Value, False);
        FrFormulasImpShow.FObs          := QrEmitObs.Value;
        FrFormulasImpShow.FSetor        := QrEmitSetor.Value;
        FrFormulasImpShow.FTipific      := QrEmitTipificacao.Value;
        FrFormulasImpShow.FCod_Espess   := QrEmitCod_Espess.Value;
        FrFormulasImpShow.FCodDefPeca   := QrEmitCodDefPeca.Value;
        FrFormulasImpShow.FSemiAreaM2   := QrEmitSemiAreaM2.Value;
        FrFormulasImpShow.FSemiRendim   := QrEmitSemiRendim.Value;
        FrFormulasImpShow.FBRL_USD      := QrEmitBRL_USD.Value;
        FrFormulasImpShow.FBRL_EUR      := QrEmitBRL_EUR.Value;
        FrFormulasImpShow.FDtaCambio    := QrEmitDtaCambio.Value;
        //
        if CkMatricial.Checked then FrFormulasImpShow.FMatricialNovo := True
        else FrFormulasImpShow.FMatricialNovo := False;
        //
        if RGImpRecRib.ItemIndex = 1 then FrFormulasImpShow.FCalcCustos := True
          else FrFormulasImpShow.FCalcCustos := False;
        if RGImpRecRib.ItemIndex = 2 then FrFormulasImpShow.FCalcPrecos := True
          else FrFormulasImpShow.FCalcPrecos := False;
        if RBkg.Checked then FrFormulasImpShow.FPesoCalc := 0.01
        else FrFormulasImpShow.FPesoCalc := 10;
        //
        if FrFormulasImpShow.FLinhas = '' then
          FrFormulasImpShow.FMedia := FrFormulasImpShow.FEMCM
        else begin
          if FrFormulasImpShow.FQtde > 0 then FrFormulasImpShow.FMedia := FrFormulasImpShow.FPeso / FrFormulasImpShow.FQtde
          else FrFormulasImpShow.FMedia := 0;
        end;
        //
        FrFormulasImpShow.CalculaReceita(nil);
      end;
      1:
      begin
        FrFormulasImpShowNew.FReImprime    := True;
        FrFormulasImpShowNew.FEmit         := QrEmitCodigo.Value;
        FrFormulasImpShowNew.FEmitGru      := QrEmitEmitGru.Value;
        FrFormulasImpShowNew.FVSMovCod     := QrEmitVSMovCod.Value;
        FrFormulasImpShowNew.FTemIMEIMrt   := TemIMEIMrt;
        FrFormulasImpShowNew.FEmitGru_TXT  := QrEmitNO_EmitGru.Value;
        FrFormulasImpShowNew.FRetrabalho   := QrEmitRetrabalho.Value;
        FrFormulasImpShowNew.FSourcMP      := QrEmitSourcMP.Value;
        FrFormulasImpShowNew.FDataP        := QrEmitDataEmis.Value;
        FrFormulasImpShowNew.FDtCorrApo    := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
        FrFormulasImpShowNew.FProgressBar1 := ProgressBar1;
        FrFormulasImpShowNew.FTipoPreco    := -1;// n�o deve usar
        FrFormulasImpShowNew.FCliIntCod    := QrEmitClienteI.Value;
        FrFormulasImpShowNew.FCliIntTxt    := QrEmitNOMECI.Value;
        FrFormulasImpShowNew.FFormula      := QrEmitNumero.Value;
        FrFormulasImpShowNew.FTipoImprime  := RGImprime.ItemIndex;
        FrFormulasImpShowNew.FPeso         := QrEmitPeso.Value;
        FrFormulasImpShowNew.FQtde         := QrEmitQtde.Value;
        FrFormulasImpShowNew.FArea          := QrEmitAreaM2.Value;
        FrFormulasImpShowNew.FLinhas       := QrEmitEspessura.Value;
        FrFormulasImpShowNew.FFulao        := QrEmitFulao.Value;
        FrFormulasImpShowNew.FDefPeca      := QrEmitDefPeca.Value;
        FrFormulasImpShowNew.FHHMM_P       := dmkPF.HorasMH(QrEmitTempoP.Value, False);
        FrFormulasImpShowNew.FHHMM_R       := dmkPF.HorasMH(QrEmitTempoR.Value, False);
        FrFormulasImpShowNew.FHHMM_T       := dmkPF.HorasMH(QrEmitTempoP.Value+QrEmitTempoR.Value, False);
        FrFormulasImpShowNew.FObs          := QrEmitObs.Value;
        FrFormulasImpShowNew.FSetor        := QrEmitSetor.Value;
        FrFormulasImpShowNew.FTipific      := QrEmitTipificacao.Value;
        FrFormulasImpShowNew.FCod_Espess   := QrEmitCod_Espess.Value;
        FrFormulasImpShowNew.FCodDefPeca   := QrEmitCodDefPeca.Value;
        FrFormulasImpShowNew.FSemiAreaM2   := QrEmitSemiAreaM2.Value;
        FrFormulasImpShowNew.FSemiRendim   := QrEmitSemiRendim.Value;
        FrFormulasImpShowNew.FBRL_USD      := QrEmitBRL_USD.Value;
        FrFormulasImpShowNew.FBRL_EUR      := QrEmitBRL_EUR.Value;
        FrFormulasImpShowNew.FDtaCambio    := QrEmitDtaCambio.Value;
        //
        if CkMatricial.Checked then FrFormulasImpShowNew.FMatricialNovo := True
        else FrFormulasImpShowNew.FMatricialNovo := False;
        //
        if RGImpRecRib.ItemIndex = 1 then FrFormulasImpShowNew.FCalcCustos := True
          else FrFormulasImpShowNew.FCalcCustos := False;
        if RGImpRecRib.ItemIndex = 2 then FrFormulasImpShowNew.FCalcPrecos := True
          else FrFormulasImpShowNew.FCalcPrecos := False;
        if RBkg.Checked then FrFormulasImpShowNew.FPesoCalc := 0.01
        else FrFormulasImpShowNew.FPesoCalc := 10;
        //
        if FrFormulasImpShowNew.FLinhas = '' then
          FrFormulasImpShowNew.FMedia := FrFormulasImpShowNew.FEMCM
        else begin
          if FrFormulasImpShowNew.FQtde > 0 then FrFormulasImpShowNew.FMedia := FrFormulasImpShowNew.FPeso / FrFormulasImpShowNew.FQtde
          else FrFormulasImpShowNew.FMedia := 0;
        end;
        //
        FrFormulasImpShowNew.CalculaReceita(nil);
      end;
      else
      begin
        Geral.MB_Erro('Vers�o de impress�o de receita de ribeira n�o implementada [4]!');
        Exit;
      end;
    end;
    Application.ProcessMessages;
    MostraEdicao(0, stLok, 0);
  end else
*)
  if PnInsumos.Visible then
  begin
    if QrSaldo.RecordCount = 0 then
    begin
      Geral.MB_Erro('Insumo / cliente interno n�o definido!');
      Exit;
    end;
    SF := Geral.DMV(EdSaldoFut.Text);
    if SF < 0 then
    begin
      Geral.MB_Erro('Quantidade insuficiente no estoque!');
      Exit;
    end;
    //
    //
    if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
    //
    //
    KG := Geral.DMV(EdPesoAdd.Text);
    RS := KG * QrSaldoCUSTO.Value;
    if MyObjects.FIC(KG <= 0, EdPesoAdd, 'Quantidade inv�lida!') then Exit;
    //
    Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'EmitIts',
      'EmitIts', 'Controle');
(*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO p q x SET Tipo=110, DataX=:P0, ');
    Dmod.QrUpd.SQL.Add('CliOrig=:P1, CliDest=:P2, Insumo=:P3, Peso=:P4, ');
    Dmod.QrUpd.SQL.Add('Valor=:P5, OrigemCodi=:P6, OrigemCtrl=:P7');
    Dmod.QrUpd.Params[00].AsString  := Geral.FDT(QrEmitDataEmis.Value, 1);
    Dmod.QrUpd.Params[01].AsInteger := QrSaldoCI.Value;
    Dmod.QrUpd.Params[02].AsInteger := QrEmitClienteI.Value;
    Dmod.QrUpd.Params[03].AsInteger := QrSaldoPQ.Value;
    Dmod.QrUpd.Params[04].AsFloat   := -KG;
    Dmod.QrUpd.Params[05].AsFloat   := -RS;
    Dmod.QrUpd.Params[06].AsInteger := QrEmitCodigo.Value;
    Dmod.QrUpd.Params[07].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
*)
    DataX      := Geral.FDT(QrEmitDataEmis.Value, 1);
    OriCodi    := QrEmitCodigo.Value;
    OriCtrl    := Controle;
    OriTipo    := VAR_FATID_0110;
    CliOrig    := QrSaldoCI.Value;
    CliDest    := QrEmitClienteI.Value;
    Insumo     := QrSaldoPQ.Value;
    Peso       := -KG;
    Valor      := -RS;
    DtCorrApo  := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
    //
    PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
      OriCtrl, OriTipo, Unitario, DtCorrApo, Empresa);
    //
    FPQx := Controle;
    UnPQx.AtualizaEstoquePQ(QrSaldoCI.Value, QrSaldoPQ.Value, Empresa, aeMsg, '');
    QrSaldo.Close;
    UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
    AtualizaCustosEmit();
  end;
end;

procedure TFmPQxExcl.BtDesiste2Click(Sender: TObject);
begin
(*
  if PnImprime.Visible then
    MostraEdicao(0, stLok, 0) else
*)
  begin
    ReopenEmit(True);
    MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmPQxExcl.Insumonapessagem1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmPQxExcl.Itemdepesagem1Click(Sender: TObject);
var
  OriCodi, OriCtrl, OriTipo, Insumo, CliInt, Empresa: Integer;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrEmitDtaBaixa.Value) then Exit;
  // 2023-09-09
  if Dmod.ImpedePeloRetornoPQ(
  QrPQxOrigemCodi.Value, QrPQxOrigemCtrl.Value, QrPQxTipo.Value, True) then
    Exit;
  //if Pertence then Exit;
  if Geral.MB_Pergunta('O item (insumo) "' + QrPQxNOMEPQ.Value +
  '" ser� excluido da pesagem, mas n�o da receita espelho.' + sLineBreak +
  'Confirma a exclus�o do insumo "'+ QrPQxNOMEPQ.Value +
  '" desta pesagem?') = ID_YES then
  begin
    Insumo  := QrPQxInsumo.Value;
    CliInt  := QrPQxCliOrig.Value;
    Empresa := QrPQxEmpresa.Value;
(*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM p q x WHERE Tipo=110 AND ');
    Dmod.QrUpd.SQL.Add('OrigemCodi=:P0 AND OrigemCtrl=:P1');
    Dmod.QrUpd.Params[0].AsInteger := QrPQxOrigemCodi.Value;
    Dmod.QrUpd.Params[1].AsInteger := QrPQxOrigemCtrl.Value;
    Dmod.QrUpd.ExecSQL;
    UnPQx.AtualizaEstoquePQ(CliInt, Insumo, aeMsg, '');
*)
    OriCodi := QrPQxOrigemCodi.Value;
    OriCtrl := QrPQxOrigemCtrl.Value;
    OriTipo := VAR_FATID_0110;
    PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
    //
    AtualizaCustosEmit();
    //
    FPQx := UMyMod.ProximoRegistro(QrPQx, 'OrigemCtrl', QrPQxOrigemCtrl.Value);
    ReopenEmit(False);
  end;
end;

procedure TFmPQxExcl.Lotedecurtimento1Click(Sender: TObject);
begin
  Geral.MB_Aviso('Procedimento n�o implementado. Solicite `a DERMATEK!');
end;

procedure TFmPQxExcl.Lotedesemiacabado1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada do item de custo selecionado de seu pedido?' + sLineBreak +
  'ATEN��O! a baixa continuar� existindo e n�o haver� mais ' + sLineBreak +
  'atrelamento do custo proporcional!',
  'emitcus', 'Controle', QrEmitCusWEControle.Value, Dmod.MyDB) = ID_YES then
    ReopenEmitCusWE(True);
end;

function TFmPQxExcl.Pertence: Boolean;
var
  OSs, Liga: String;
  OS: Integer;
begin
  Result := False;
////////////////////////////////////////////////////////////////////////////////
  EXIT;
////////////////////////////////////////////////////////////////////////////////
  QrMPIn.Close;
  QrMPIn.Params[0].AsInteger := QrEmitCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrMPIn, Dmod.MyDB);
  if QrMPIn.RecordCount > 0 then
  begin
    Result := True;
    if QrMPIn.RecordCount = 1 then
    begin
      if QrMPInMPIn.Value <> 0 then
        OS := QrMPInMPIn.Value
      else
        OS := QrMPInMPVIts.Value;
      Geral.MB_Aviso('Esta pesagem n�o pode ser exclu�da ou ' +
      'modificada porque j� pertence a OS n� ' +
      Geral.FF0(OS) + '.')
    end else
    begin
      QrMPIn.First;
      OSs := '';
      Liga := '';
      while not QrMPIn.Eof do
      begin
        if QrMPInMPIn.Value <> 0 then
          OS := QrMPInMPIn.Value
        else
          OS := QrMPInMPVIts.Value;
        OSs := OSs + Liga + FormatFloat('000', OS);
        Liga := ', ';
        QrMPIn.Next;
      end;
      Geral.MB_Aviso('Esta pesagem n�o pode ser exclu�da ou ' +
      'modificada porque j� pertence as OS n�s: ' + OSs + '.');
    end;
  end else Result := False;
end;

procedure TFmPQxExcl.PMExcluiPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsDel(Lotedecurtimento1, QrEmitCusBH);
  MyObjects.HabilitaMenuItemCabDel(Lotedesemiacabado1, QrEmitCusWE, QrWBMovIts);
  MyObjects.HabilitaMenuItemItsDel(Recurtimento1, QrWBMovIts);
end;

procedure TFmPQxExcl.PMImprimePopup(Sender: TObject);
begin
  Zerapreos1.Enabled := (QrEmit.State <> dsInactive) and (QrEmitClienteI.Value >0)
end;

procedure TFmPQxExcl.EdPQChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQxExcl.Estoquedematriaprimadesemiacabadomostraroutrajanela1Click(
  Sender: TObject);
begin
  if DBCheck.CriaFm(TFmWBMovImp, FmWBMovImp, afmoNegarComAviso) then
  begin
    FmWBMovImp.ShowModal;
    FmWBMovImp.Destroy;
  end;
end;

procedure TFmPQxExcl.EdCIChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQxExcl.CalculaSaldoFuturo;
var
  CI, PQ: Integer;
  PesoAdd, SaldoFut: Double;
begin
  CI := Geral.IMV(EdCI.Text);
  PQ := Geral.IMV(EdPQ.Text);
  QrSaldo.Close;
  QrSaldo.Params[0].AsInteger := CI;
  QrSaldo.Params[1].AsInteger := PQ;
  UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
  //
  PesoAdd := Geral.DMV(EdPesoAdd.Text);
  SaldoFut := QrSaldoPeso.Value - PesoAdd;
  EdSaldoFut.Text := Geral.FFT(SaldoFut, 3, siNegativo);
end;

procedure TFmPQxExcl.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmPQxExcl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //DBGWBMovIts.Height := (THackDBGrid(DBGWBMovIts).DefaultRowHeight + 2) * 4;
  DBGWBMovIts.Align := alBottom;
  DBGEmitCusBH.Align := alClient;
  DBGEmitCusWE.Align := alClient;
  DBGEmitCusVS_BH.Align := alClient;
  DBGEmitCusVS_WE.Align := alClient;
  //
(*
  MyObjects.PreencheComponente(RGImpRecRib, sListaRecImpApresenta, 5);
  RGImpRecRib.ItemIndex := Dmod.QrControleImpRecRib.Value;
*)
  //
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
end;

procedure TFmPQxExcl.BtAdicionaClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMAdiciona, BtAdiciona);
end;

procedure TFmPQxExcl.BtAlteraClick(Sender: TObject);
begin
  UnPQx.MostraFormEmitCab(stUpd, QrEmitCodigo.Value);
  ReopenEmit(True);
end;

procedure TFmPQxExcl.BtPesquisaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPQExclPesq, FmPQExclPesq, afmoNegarComAviso) then
  begin
    FmPQExclPesq.ShowModal;
    FmPQExclPesq.Destroy;
  end;
end;

procedure TFmPQxExcl.EdPesoAddExit(Sender: TObject);
begin
  EdPesoAdd.Text := Geral.TFT(EdPesoAdd.Text, 3, siPositivo);
  CalculaSaldoFuturo;
end;

procedure TFmPQxExcl.EdPesagemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    Registro(vpLast);
end;

procedure TFmPQxExcl.QrEmitCalcFields(DataSet: TDataSet);
begin
  QrEmitNumCODIF.Value := dmkPF.IntToCODIF_Receita(QrEmitNumero.Value);
end;

procedure TFmPQxExcl.QrEmitCusWEAfterScroll(DataSet: TDataSet);
begin
  ReopenWBMovIts(0);
end;

procedure TFmPQxExcl.QrEmitCusWEBeforeClose(DataSet: TDataSet);
begin
  QrWBMovIts.Close;
end;

procedure TFmPQxExcl.QrWBMovItsAfterOpen(DataSet: TDataSet);
var
  DRH, Altura, Itens: Integer;
begin
  DRH := THackDBGrid(DBGWBMovIts).DefaultRowHeight + 2;
  Itens := 3;
  if QrEmitCusBH.State <> dsInactive then
    Itens := Itens + QrEmitCusBH.RecordCount;
  if QrEmitCusWE.State <> dsInactive then
    Itens := Itens + QrEmitCusWE.RecordCount;
  //
  Altura := DRH * Itens;
  if Altura > (PmManyGrids.Height - DRH) then
    Altura := PmManyGrids.Height div 2
  else
    Altura := PmManyGrids.Height - Altura;
  //
  DBGWBMovIts.Height := Altura;
end;

procedure TFmPQxExcl.ReopenEmitCusXX();
begin
  DBGEmitCusBH.Visible := False;
  DBGEmitCusWE.Visible := False;
  DBGEmitCusVS_BH.Visible := False;
  DBGEmitCusVS_WE.Visible := False;
  DBGWBMovIts.Visible  := False;
  Splitter1.Visible    := False;
  Splitter1.Align      := alTop;
  //
  case TTipoReceitas(QrEmitSetrEmi.Value) of
    //dmktfrmSetrEmi_INDEFIN,
    dmktfrmSetrEmi_MOLHADO:
    begin
      case TTipoSourceMP(QrEmitSourcMP.Value) of
        //dmktfrmSourcMP_INDEFIN
        dmktfrmSourcMP_Ribeira: ReopenEmitCusBH();
        dmktfrmSourcMP_WetSome: ReopenEmitCusWE(True);
        dmktfrmSourcMP_VS_BH: ReopenEmitCusBH();
        else Geral.MB_Erro('"SourceMP" indefinido! [1]');
      end;
    end;
    dmktfrmSetrEmi_ACABATO: ReopenEmitCusWE(False);
    else Geral.MB_Erro('"SetrEmi" indefinido! [1]');
  end;
end;

procedure TFmPQxExcl.ReopenLotes();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotes, Dmod.MyDB, [
  'SELECT emc.Controle, emc.VSMovIts, emc.Peso, emc.Pecas, ',
  'vmi.SerieFch, vmi.Ficha, vmi.Marca, fch.Nome NO_SerieFch, ',
  'vmi.Codigo OP, emc.Custo ',
  'FROM emitcus emc ',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=emc.VSMovIts ',
  'LEFT JOIN vsserfch fch ON fch.Codigo=vmi.SerieFch ',
  'WHERE emc.Codigo=' + Geral.FF0(QrEmitCodigo.Value),
  '']);
end;

procedure TFmPQxExcl.ReopenEmitCusBH();
begin
(*
//  Desabilitado em 2016-03-26 para habilitar v s m o v i t s  abaixo!
  DBGEmitCusBH.Visible := True;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitCusBH, Dmod.MyDB, [
  'SELECT mpi.Marca, mpi.Data DataMPIn, ',
  'emc.Pecas, emc.Peso, emc.Custo, emc.MPIn ',
  'FROM emitcus emc ',
  'LEFT JOIN mpin mpi ON mpi.Controle=emc.MPIn ',
  'WHERE emc.Codigo=' + Geral.FF0(QrEmitCodigo.Value),
  '']);
*)
  // 2016-03-26
  DBGEmitCusVS_BH.Visible := True;
  ReopenLotes();
end;

procedure TFmPQxExcl.ReopenEmitCusWE(ReopenMovIts: Boolean);
begin
  DBGWBMovIts.Visible := ReopenMovIts;
  Splitter1.Align := alBottom;
  Splitter1.Visible := DBGWBMovIts.Visible;
  //
  DBGEmitCusWE.Visible := True;
  //
(*
  UnDMkDAC_PF.AbreMySQLQuery0(QrEmitCusWE, Dmod.MyDB, [
  'SELECT CONCAT(mpi.Texto, " ", mpi.CorTxt) TextoECor,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_CLI,',
  'ecu.* ',
  'FROM emitcus ecu',
  'LEFT JOIN mpvits mpi ON mpi.Controle=ecu.MPVIts',
  'LEFT JOIN mpp mpp ON mpp.Codigo=mpi.Pedido ',
  'LEFT JOIN entidades ent ON ent.Codigo=mpp.Cliente ',
  'WHERE ecu.Codigo=' + Geral.FF0(QrEmitCodigo.Value),
  '']);
*)
  // 2016-03-27
  DBGEmitCusVS_WE.Visible := True;
  ReopenLotes();
end;

procedure TFmPQxExcl.ReopenWBMovIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBMovIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'WHERE wmi.MovimID=' + Geral.FF0(Integer(emidIndsWE)),
  'AND wmi.Codigo=' + Geral.FF0(QrEmitCusWEMPVIts.Value),
  'AND wmi.LnkNivXtr1=' + Geral.FF0(QrEmitCusWECodigo.Value),
  'AND wmi.LnkNivXtr2=' + Geral.FF0(QrEmitCusWEControle.Value),
  //'ORDER BY NO_Pallet, wmi.Controle ',
  'ORDER BY wmi.Controle ',
  '']);
  //
  QrWBMovIts.Locate('Controle', Controle, []);
end;

procedure TFmPQxExcl.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPQxExcl.SpeedButton1Click(Sender: TObject);
begin
  Registro(vpFirst);
end;

procedure TFmPQxExcl.SpeedButton2Click(Sender: TObject);
begin
  Registro(vpPrior);
end;

procedure TFmPQxExcl.SpeedButton3Click(Sender: TObject);
begin
  Registro(vpNext);
end;

procedure TFmPQxExcl.SpeedButton4Click(Sender: TObject);
begin
  Registro(vpLast);
end;

procedure TFmPQxExcl.AtualizaEmit;
begin
  QrSumEmit.Close;
  QrSumEmit.Params[0].AsInteger := QrEmitCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSumEmit, Dmod.MyDB);
  //
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE emit SET Custo=:P0, Status=2 WHERE Codigo=:P1');
  Dmod.QrUpdM.Params[0].AsFloat   := QrSumEmitCUSTO.Value;
  Dmod.QrUpdM.Params[1].AsInteger := QrEmitCodigo.Value;
  Dmod.QrUpdM.ExecSQL;
end;

  // 2016-03-26
procedure TFmPQxExcl.AtualizaLotes();
begin
  QrLotes.First;
  while not QrLotes.Eof do
  begin
    DmModVS.AtualizaVSMovIts_CusEmit(QrLotesVSMovIts.Value);
    //
    QrLotes.Next;
  end;
end;

procedure TFmPQxExcl.AtualizaCustosEmit();
var
  //MPIn: Integer;
  Qry: TmySQLQuery;
begin
  AtualizaEmit;
  if QrEmit.State <> dsInactive then
  begin
    //MPIn := QrEmitCusBHMPIn.Value;
    FPQx := QrPQxOrigemCtrl.Value;
    //
    Dmod.AtualizaCustosEmit_1(QrEmitCodigo.Value);
    QrEmit.Close;
    UnDmkDAC_PF.AbreQuery(QrEmit, Dmod.MyDB);
    (*
    Qry := TmySQLQuery.Create(Self);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT MPIn ',
      'FROM emitcus ',
      'WHERE Codigo=' + Geral.FF0(QrEmitCodigo.Value),
      '']);
      Qry.First;
      while not Qry.Eof do
      begin
        Dmod.AtualizaMPIn(Qry.FieldByName('MPIn').AsInteger);
        Qry.Next;
      end;
      Qry.Locate('MPIn', MPIn, []);
    finally
      Qry.Free;
    end;
    *)
  end;
  AtualizaLotes();
end;

(*
object PnImprime: TPanel
  Left = 0
  Top = 362
  Width = 1016
  Height = 96
  Align = alBottom
  BevelOuter = bvNone
  TabOrder = 2
  Visible = False
  object RGImpRecRib: TRadioGroup
    Left = 6
    Top = 4
    Width = 528
    Height = 65
    Caption = ' Apresenta'#231#227'o: '
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'Procure em:'
      ''
      'FmPrincipal.VAR_RecImpApresentaCol'
      'FmPrincipal.VAR_RecImpApresentaRol')
    TabOrder = 0
  end
  object RGImprime: TRadioGroup
    Left = 532
    Top = 4
    Width = 160
    Height = 65
    Caption = ' Op'#231#245'es: '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Visualizar'
      'Imprimir'
      'Matricial'
      'Arquivo')
    TabOrder = 1
  end
  object GBkgTon: TGroupBox
    Left = 690
    Top = 4
    Width = 95
    Height = 65
    Caption = ' Grandeza: '
    TabOrder = 2
    object RBTon: TRadioButton
      Left = 8
      Top = 16
      Width = 50
      Height = 17
      Caption = 'Ton'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object RBkg: TRadioButton
      Left = 8
      Top = 36
      Width = 50
      Height = 17
      Caption = 'kg'
      TabOrder = 1
    end
  end
  object CkMatricial: TCheckBox
    Left = 8
    Top = 72
    Width = 117
    Height = 17
    Caption = 'Novo (Matricial)'
    TabOrder = 3
  end
end
*)

end.

