unit MPSubProd;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, Variants, DmkDAC_PF,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkDBLookupComboBox,
  dmkEditCB, dmkRadioGroup, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmMPSubProd = class(TForm)
    PainelDados: TPanel;
    DsMPSubProd: TDataSource;
    QrMPSubProd: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    PainelData: TPanel;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    QrMPSubProdCodigo: TIntegerField;
    QrMPSubProdNome: TWideStringField;
    QrMPSubProdGGX_Ori: TIntegerField;
    QrMPSubProdGGX_Ger: TIntegerField;
    QrMPSubProdGrndzaBase: TSmallintField;
    EdGGX_Ori: TdmkEditCB;
    CBGGX_Ori: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrGGX_Ori: TmySQLQuery;
    DsGGX_Ori: TDataSource;
    QrGGX_Ger: TmySQLQuery;
    DsGGX_Ger: TDataSource;
    QrGGX_OriControle: TIntegerField;
    QrGGX_OriNO_PRD_TAM_COR: TWideStringField;
    QrGGX_GerControle: TIntegerField;
    QrGGX_GerNO_PRD_TAM_COR: TWideStringField;
    Label4: TLabel;
    EdGGX_Ger: TdmkEditCB;
    CBGGX_Ger: TdmkDBLookupComboBox;
    RGGrndzaBase: TdmkRadioGroup;
    EdFator: TdmkEdit;
    Label5: TLabel;
    QrMPSubProdFator: TFloatField;
    QrGGX_GerGerBxaEstq: TSmallintField;
    Label6: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    LaOriGer_Qry: TLabel;
    QrShowOri: TmySQLQuery;
    DsShowOri: TDataSource;
    DBEdit1: TDBEdit;
    QrShowOriNO_PRD_TAM_COR: TWideStringField;
    DBEdit2: TDBEdit;
    QrShowGer: TmySQLQuery;
    DsShowGer: TDataSource;
    QrShowGerNO_PRD_TAM_COR: TWideStringField;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit5: TDBEdit;
    QrShowGerGerBxaEstq: TSmallintField;
    LaOriGer_Edit: TLabel;
    Label11: TLabel;
    EdArredonda: TdmkEdit;
    QrMPSubProdArredonda: TFloatField;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    LaArred_Edit: TLabel;
    LaArred_Qry: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMPSubProdAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMPSubProdBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure RGGrndzaBaseClick(Sender: TObject);
    procedure EdGGX_OriChange(Sender: TObject);
    procedure EdGGX_GerChange(Sender: TObject);
    procedure QrMPSubProdAfterScroll(DataSet: TDataSet);
    procedure QrMPSubProdBeforeClose(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    function  DefineLaOriGer(GGX_Ger, GerBxaEstq, GrndzaBase: Integer): String;
    procedure DefineLaEdits();
  public
    { Public declarations }
  end;

var
  FmMPSubProd: TFmMPSubProd;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModProd;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMPSubProd.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMPSubProd.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMPSubProdCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMPSubProd.DefParams;
begin
  VAR_GOTOTABELA := 'mpsubprod';
  VAR_GOTOMYSQLTABLE := QrMPSubProd;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM mpsubprod');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmMPSubProd.EdGGX_GerChange(Sender: TObject);
begin
  DefineLaEdits();
end;

procedure TFmMPSubProd.EdGGX_OriChange(Sender: TObject);
begin
  DefineLaEdits();
end;

procedure TFmMPSubProd.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmMPSubProd.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmMPSubProd.QueryPrincipalAfterOpen;
begin
end;

procedure TFmMPSubProd.RGGrndzaBaseClick(Sender: TObject);
begin
  DefineLaEdits();
end;

procedure TFmMPSubProd.DefineLaEdits();
begin
  if (EdGGX_Ger.ValueVariant <> Null) and (RGGrndzaBase.ItemIndex > -1) then
  begin
    LaOriGer_Edit.Caption := DefineLaOriGer(EdGGX_Ger.ValueVariant,
      QrGGX_GerGerBxaEstq.Value, RGGrndzaBase.ItemIndex);
    LaArred_Edit.Caption :=
      DmProd.DefineDescriGerBxaEstq(QrGGX_GerGerBxaEstq.Value);
  end;
end;

function TFmMPSubProd.DefineLaOriGer(GGX_Ger, GerBxaEstq, GrndzaBase: Integer): String;
var
  Descri: String;
begin
  if GGX_Ger = 0 then Descri := '?' else
  begin
{
    case GerBxaEstq of
      0: Descri := '? ? ?';
      1: Descri := 'Pe�a';
      2: Descri := 'm�';
      3: Descri := 'kg';
      4: Descri := 't (ton)';
      5: Descri := 'ft�';
      else Descri := '#ERRO#';
    end;
}
    Descri := DmProd.DefineDescriGerBxaEstq(GerBxaEstq);
  end;
  Result := Descri + ' / ' + RGGrndzaBase.Items[GrndzaBase];
end;

procedure TFmMPSubProd.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMPSubProd.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMPSubProd.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMPSubProd.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMPSubProd.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMPSubProd.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrMPSubProd, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'mpsubprod');
end;

procedure TFmMPSubProd.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrMPSubProdCodigo.Value;
  Close;
end;

procedure TFmMPSubProd.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('mpsubprod', 'Codigo', ImgTipo.SQLType,
    QrMPSubProdCodigo.Value);
  EdCodigo.ValueVariant := Codigo;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmMPSubProd, PainelEdit,
    'mpsubprod', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmMPSubProd.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'mpsubprod', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'mpsubprod', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'mpsubprod', 'Codigo');
end;

procedure TFmMPSubProd.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrMPSubProd, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'mpsubprod');
end;

procedure TFmMPSubProd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrGGX_Ori, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGGX_Ger, Dmod.MyDB);
end;

procedure TFmMPSubProd.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMPSubProdCodigo.Value, LaRegistro.Caption);
end;

procedure TFmMPSubProd.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmMPSubProd.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMPSubProd.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmMPSubProd.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMPSubProd.QrMPSubProdAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmMPSubProd.QrMPSubProdAfterScroll(DataSet: TDataSet);
begin
  QrShowOri.Close;
  QrShowOri.Params[0].AsInteger := QrMPSubProdGGX_Ori.Value;
  UnDmkDAC_PF.AbreQuery(QrShowOri, Dmod.MyDB);
  //
  QrShowGer.Close;
  QrShowGer.Params[0].AsInteger := QrMPSubProdGGX_Ger.Value;
  UnDmkDAC_PF.AbreQuery(QrShowGer, Dmod.MyDB);
  //
  LaOriGer_Qry.Caption := DefineLaOriGer(QrMPSubProdGGX_Ger.Value,
    QrShowGerGerBxaEstq.Value, QrMPSubProdGrndzaBase.Value);
  LaArred_Qry.Caption :=
    DmProd.DefineDescriGerBxaEstq(QrShowGerGerBxaEstq.Value);
end;

procedure TFmMPSubProd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPSubProd.SbQueryClick(Sender: TObject);
begin
  LocCod(QrMPSubProdCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'mpsubprod', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmMPSubProd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPSubProd.QrMPSubProdBeforeClose(DataSet: TDataSet);
begin
  LaOriGer_Qry.Caption := '? / ?';
  LaArred_Qry.Caption := '?';
  QrShowOri.Close;
  QrShowGer.Close;
end;

procedure TFmMPSubProd.QrMPSubProdBeforeOpen(DataSet: TDataSet);
begin
  QrMPSubProdCodigo.DisplayFormat := FFormatFloat;
end;

end.

