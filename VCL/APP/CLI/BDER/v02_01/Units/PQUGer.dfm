object FmPQUGer: TFmPQUGer
  Left = 339
  Top = 185
  Caption = 'QUI-INSUM-009 :: Previs'#227'o de Consumo de Insumos'
  ClientHeight = 656
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 415
        Height = 32
        Caption = 'Previs'#227'o de Consumo de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 415
        Height = 32
        Caption = 'Previs'#227'o de Consumo de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 415
        Height = 32
        Caption = 'Previs'#227'o de Consumo de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 538
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 538
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 538
        Align = alClient
        TabOrder = 0
        object PCPrevisao: TPageControl
          Left = 2
          Top = 61
          Width = 1004
          Height = 475
          ActivePage = TabSheet13
          Align = alClient
          TabOrder = 0
          Visible = False
          object TabSheet12: TTabSheet
            Caption = ' Produ'#231#227'o di'#225'ria'
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 447
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object GradePrev: TdmkDBGridZTO
                Left = 293
                Top = 0
                Width = 703
                Height = 447
                Align = alClient
                DataSource = DsPQUFrm1
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 2
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Receita'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMERECEITA'
                    Title.Caption = 'Nome da receita'
                    Width = 193
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas1'
                    Title.Caption = 'Qtde 1'
                    Width = 52
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas2'
                    Title.Caption = 'Qtde 2'
                    Width = 52
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DMais2'
                    Title.Caption = 'D+ 2'
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas3'
                    Title.Caption = 'Qtde 3'
                    Width = 52
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DMais3'
                    Title.Caption = 'D+ 3'
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas4'
                    Title.Caption = 'Qtde 4'
                    Width = 52
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DMais4'
                    Title.Caption = 'D+ 4'
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'UNIDADE'
                    Title.Caption = 'Unidade'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Media'
                    Title.Caption = 'M'#233'dia'
                    Width = 60
                    Visible = True
                  end>
              end
              object PnPrevAdd: TPanel
                Left = 293
                Top = 0
                Width = 703
                Height = 447
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                Visible = False
                object Label6: TLabel
                  Left = 4
                  Top = 4
                  Width = 40
                  Height = 13
                  Caption = 'Receita:'
                end
                object Label7: TLabel
                  Left = 516
                  Top = 8
                  Width = 43
                  Height = 13
                  Caption = 'Unidade:'
                end
                object Label9: TLabel
                  Left = 4
                  Top = 84
                  Width = 67
                  Height = 13
                  Caption = 'Quantidade 1:'
                end
                object Label10: TLabel
                  Left = 404
                  Top = 84
                  Width = 66
                  Height = 13
                  Caption = 'M'#233'dia kg/Un:'
                end
                object Label11: TLabel
                  Left = 80
                  Top = 84
                  Width = 67
                  Height = 13
                  Caption = 'Quantidade 2:'
                end
                object Label12: TLabel
                  Left = 188
                  Top = 84
                  Width = 67
                  Height = 13
                  Caption = 'Quantidade 3:'
                end
                object Label13: TLabel
                  Left = 296
                  Top = 84
                  Width = 67
                  Height = 13
                  Caption = 'Quantidade 4:'
                end
                object Label14: TLabel
                  Left = 4
                  Top = 44
                  Width = 70
                  Height = 13
                  Caption = 'Cliente interno:'
                  Visible = False
                end
                object Label1: TLabel
                  Left = 156
                  Top = 84
                  Width = 26
                  Height = 13
                  Caption = 'D+ 2:'
                end
                object Label2: TLabel
                  Left = 264
                  Top = 84
                  Width = 26
                  Height = 13
                  Caption = 'D+ 3:'
                end
                object Label3: TLabel
                  Left = 372
                  Top = 84
                  Width = 26
                  Height = 13
                  Caption = 'D+ 4:'
                end
                object EdReceita1: TdmkEditCB
                  Left = 4
                  Top = 20
                  Width = 65
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBReceita1
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBReceita1: TdmkDBLookupComboBox
                  Left = 72
                  Top = 20
                  Width = 437
                  Height = 21
                  KeyField = 'Numero'
                  ListField = 'Nome'
                  ListSource = DsFormulas
                  TabOrder = 1
                  dmkEditCB = EdReceita1
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object EdUnidade: TdmkEditCB
                  Left = 516
                  Top = 24
                  Width = 33
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBUnidade
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBUnidade: TdmkDBLookupComboBox
                  Left = 552
                  Top = 24
                  Width = 169
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsDefPecas
                  TabOrder = 3
                  dmkEditCB = EdUnidade
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object EdQtde1: TdmkEdit
                  Left = 4
                  Top = 100
                  Width = 73
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 6
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdMedia: TdmkEdit
                  Left = 404
                  Top = 100
                  Width = 105
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 13
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdQtde2: TdmkEdit
                  Left = 80
                  Top = 100
                  Width = 73
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 7
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdQtde3: TdmkEdit
                  Left = 188
                  Top = 100
                  Width = 73
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 9
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdQtde4: TdmkEdit
                  Left = 296
                  Top = 100
                  Width = 73
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 11
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCliInt: TdmkEditCB
                  Left = 4
                  Top = 60
                  Width = 65
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  Visible = False
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBCliInt
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBCliInt: TdmkDBLookupComboBox
                  Left = 72
                  Top = 60
                  Width = 437
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'NOMECI'
                  ListSource = DsCI2A
                  TabOrder = 5
                  Visible = False
                  dmkEditCB = EdCliInt
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object StaticText1: TStaticText
                  Left = 516
                  Top = 48
                  Width = 205
                  Height = 77
                  Alignment = taCenter
                  AutoSize = False
                  BevelInner = bvSpace
                  BevelOuter = bvNone
                  BorderStyle = sbsSunken
                  Caption = 
                    #13#10#201' poss'#237'vel simular at'#233' 4 previs'#245'es de produ'#231#227'o, informando dif' +
                    'erentes quantidades di'#225'rias e / ou dias a mais de estoque (D+).'
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBackground
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentColor = False
                  ParentFont = False
                  TabOrder = 14
                end
                object GBConf1: TGroupBox
                  Left = 0
                  Top = 383
                  Width = 703
                  Height = 64
                  Align = alBottom
                  TabOrder = 15
                  Visible = False
                  object Panel19: TPanel
                    Left = 2
                    Top = 15
                    Width = 699
                    Height = 47
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Panel20: TPanel
                      Left = 555
                      Top = 0
                      Width = 144
                      Height = 47
                      Align = alRight
                      BevelOuter = bvNone
                      TabOrder = 0
                      object BtDesiste1: TBitBtn
                        Tag = 15
                        Left = 20
                        Top = 4
                        Width = 120
                        Height = 40
                        Caption = '&Desiste'
                        NumGlyphs = 2
                        TabOrder = 0
                        OnClick = BtDesiste1Click
                      end
                    end
                    object BtConf1: TBitBtn
                      Tag = 14
                      Left = 20
                      Top = 4
                      Width = 120
                      Height = 40
                      Caption = '&Confirma'
                      NumGlyphs = 2
                      TabOrder = 1
                      OnClick = BtConf1Click
                    end
                  end
                end
                object EdDMais2: TdmkEdit
                  Left = 156
                  Top = 100
                  Width = 29
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 8
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdDMais3: TdmkEdit
                  Left = 264
                  Top = 100
                  Width = 29
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 10
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdDMais4: TdmkEdit
                  Left = 372
                  Top = 100
                  Width = 29
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 12
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object Panel21: TPanel
                Left = 0
                Top = 0
                Width = 293
                Height = 447
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 1
                object Panel28: TPanel
                  Left = 0
                  Top = 390
                  Width = 293
                  Height = 57
                  Align = alBottom
                  TabOrder = 0
                  object BitBtn1: TBitBtn
                    Tag = 350
                    Left = 24
                    Top = 8
                    Width = 120
                    Height = 40
                    Caption = '&Grupo'
                    NumGlyphs = 2
                    TabOrder = 0
                    OnClick = BitBtn1Click
                  end
                  object BitBtn2: TBitBtn
                    Tag = 351
                    Left = 144
                    Top = 8
                    Width = 120
                    Height = 40
                    Caption = '&Item'
                    NumGlyphs = 2
                    TabOrder = 1
                    OnClick = BitBtn2Click
                  end
                end
                object dmkDBGridZTO1: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 293
                  Height = 390
                  Align = alClient
                  DataSource = DsPQUCab
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 1
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Title.Caption = 'C'#243'digo'
                      Width = 45
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Nome'
                      Title.Caption = 'Descri'#231#227'o'
                      Width = 200
                      Visible = True
                    end>
                end
              end
            end
          end
          object TabSheet13: TTabSheet
            Caption = ' Pedido em aberto '
            ImageIndex = 1
            object Panel27: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 289
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Splitter1: TSplitter
                Left = 522
                Top = 0
                Height = 289
                ExplicitLeft = 600
                ExplicitHeight = 241
              end
              object GradePedOpn: TdmkDBGridZTO
                Left = 525
                Top = 0
                Width = 471
                Height = 289
                Align = alClient
                DataSource = DsPQUFrm2
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                OnDblClick = GradePedOpnDblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Receita'
                    Width = 50
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMERECEITA'
                    Title.Caption = 'Nome da receita'
                    Width = 193
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_Grandeza'
                    Title.Caption = 'Grandeza'
                    Width = 52
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PedAreaM2'
                    Title.Caption = 'A produzir m'#178
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PedAreaP2'
                    Title.Caption = 'A produzir ft'#178
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PedPesoKg'
                    Title.Caption = 'A produzir Kg'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PedLinhasCul'
                    Title.Caption = 'Reb.Lin.cul'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PedLinhasCab'
                    Title.Caption = 'Reb.Lin.cab'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PedPdArM2'
                    Title.Caption = 'Pedido m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PedPdArP2'
                    Title.Caption = 'Pedido ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PedPdPeKg'
                    Title.Caption = 'Pedido kg'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PedPrArM2'
                    Title.Caption = 'Produzido m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PedPrArP2'
                    Title.Caption = 'Produzido ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PedPrPeKg'
                    Title.Caption = 'Produzido kg'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'ID'
                    Visible = True
                  end>
              end
              object Panel31: TPanel
                Left = 0
                Top = 0
                Width = 522
                Height = 289
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 1
                object PnPrevPed: TPanel
                  Left = 0
                  Top = 232
                  Width = 522
                  Height = 57
                  Align = alBottom
                  TabOrder = 0
                  object BtPrevGrupo: TBitBtn
                    Tag = 569
                    Left = 4
                    Top = 8
                    Width = 102
                    Height = 40
                    Caption = '&Pedido'
                    NumGlyphs = 2
                    TabOrder = 0
                    OnClick = BtPrevGrupoClick
                  end
                  object BtItemGrupo: TBitBtn
                    Tag = 601
                    Left = 108
                    Top = 8
                    Width = 102
                    Height = 40
                    Caption = '&Item'
                    NumGlyphs = 2
                    TabOrder = 1
                    OnClick = BtItemGrupoClick
                  end
                  object BtAtzProduzidos: TBitBtn
                    Tag = 18
                    Left = 212
                    Top = 8
                    Width = 181
                    Height = 40
                    Caption = 'Atuali&za produ'#231#227'o realizada'
                    NumGlyphs = 2
                    TabOrder = 2
                    OnClick = BtAtzProduzidosClick
                  end
                  object GroupBox3: TGroupBox
                    Left = 396
                    Top = 1
                    Width = 125
                    Height = 55
                    Align = alRight
                    Caption = ' Dias:'
                    TabOrder = 3
                    object Label8: TLabel
                      Left = 4
                      Top = 14
                      Width = 47
                      Height = 13
                      Caption = 'Consumo:'
                    end
                    object Label15: TLabel
                      Left = 68
                      Top = 14
                      Width = 39
                      Height = 13
                      Caption = 'Compra:'
                    end
                    object EdDdConsumo: TdmkEdit
                      Left = 4
                      Top = 30
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '180'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 180
                      ValWarn = False
                    end
                    object EdDdCompra: TdmkEdit
                      Left = 64
                      Top = 30
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '45'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 45
                      ValWarn = False
                    end
                  end
                end
                object DBGPrevPed: TdmkDBGridZTO
                  Left = 0
                  Top = 37
                  Width = 525
                  Height = 195
                  Align = alLeft
                  DataSource = DsPQUCab
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 1
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  OnDblClick = DBGPrevPedDblClick
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Title.Caption = 'C'#243'digo'
                      Width = 45
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Nome'
                      Title.Caption = 'Pedido / descri'#231#227'o '
                      Width = 200
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DtPedido'
                      Title.Caption = 'Dt.Pedido'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DtIniProd'
                      Title.Caption = 'Dt.Ini.Prod'
                      Width = 92
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DtEntrega'
                      Title.Caption = 'Dt.Entrega'
                      Width = 94
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Ordem'
                      Width = 39
                      Visible = True
                    end>
                end
                object RGPQUCabSit: TRadioGroup
                  Left = 0
                  Top = 0
                  Width = 522
                  Height = 37
                  Align = alTop
                  Caption = ' Mostrar pedidos: '
                  Columns = 3
                  ItemIndex = 0
                  Items.Strings = (
                    'Abertos'
                    'Encerrados'
                    'Todos')
                  TabOrder = 2
                  OnClick = RGPQUCabSitClick
                end
              end
            end
            object Panel7: TPanel
              Left = 0
              Top = 289
              Width = 996
              Height = 158
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Splitter3: TSplitter
                Left = 817
                Top = 0
                Height = 104
                Align = alRight
                ExplicitLeft = 828
                ExplicitTop = 68
                ExplicitHeight = 100
              end
              object DBGPQURecCarga: TdmkDBGridZTO
                Left = 0
                Top = 0
                Width = 817
                Height = 104
                Align = alClient
                DataSource = DsPQURecCarga
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                OnDblClick = DBGPQURecCargaDblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Insumo'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NomePQ'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 180
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'KgLiqEmb'
                    Title.Caption = 'Emb.kg liq.'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoEstq'
                    Title.Caption = 'Estoque kg'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoUso'
                    Title.Caption = 'kg utilizar'#225
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoFut'
                    Title.Caption = 'kg futuro estoque'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoNeed'
                    Title.Caption = 'kg necess'#225'rio'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoCompra'
                    Title.Caption = 'kg comprar'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesComprPreDef'
                    Title.Caption = 'kg pr'#233'-definido'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CargaPercInc'
                    Title.Caption = '% incremento'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CargaKgUso'
                    Title.Caption = 'Carga kg uso'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CargaKgNeed'
                    Title.Caption = 'Carga kg necess'#225'rio'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CargaKgCompra'
                    Title.Caption = 'Carga kg compra'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CargaKgQtEmb'
                    Title.Caption = 'Carga embalagens'
                    Width = 62
                    Visible = True
                  end>
              end
              object Panel9: TPanel
                Left = 0
                Top = 104
                Width = 996
                Height = 54
                Align = alBottom
                TabOrder = 1
                object LaQtPedido: TLabel
                  Left = 281
                  Top = 8
                  Width = 105
                  Height = 13
                  Caption = 'Carga total liq. transp.:'
                end
                object Label4: TLabel
                  Left = 392
                  Top = 8
                  Width = 102
                  Height = 13
                  Caption = 'Fornecedor da carga:'
                end
                object Label5: TLabel
                  Left = 809
                  Top = 8
                  Width = 66
                  Height = 13
                  Caption = '% incremento:'
                end
                object EdCargaLiqTransp: TdmkEdit
                  Left = 282
                  Top = 24
                  Width = 107
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '30.500,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 30500.000000000000000000
                  ValWarn = False
                end
                object BtPedItem: TBitBtn
                  Left = 128
                  Top = 8
                  Width = 149
                  Height = 40
                  Caption = 'kg pr'#233'-definido item'
                  NumGlyphs = 2
                  TabOrder = 1
                  OnClick = BtPedItemClick
                end
                object BtCalculaCarga: TBitBtn
                  Left = 884
                  Top = 8
                  Width = 102
                  Height = 40
                  Caption = 'Calcula carga'
                  NumGlyphs = 2
                  TabOrder = 2
                  OnClick = BtCalculaCargaClick
                end
                object BtCalculaConsumo: TBitBtn
                  Left = 4
                  Top = 8
                  Width = 120
                  Height = 40
                  Caption = 'Calcula consumo'
                  NumGlyphs = 2
                  TabOrder = 3
                  OnClick = BtCalculaConsumoClick
                end
                object EdIQ: TdmkEditCB
                  Left = 392
                  Top = 24
                  Width = 65
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnRedefinido = EdCIRedefinido
                  DBLookupComboBox = CBIQ
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBIQ: TdmkDBLookupComboBox
                  Left = 456
                  Top = 24
                  Width = 349
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'NOME'
                  ListSource = DsFornecedores
                  TabOrder = 5
                  dmkEditCB = EdIQ
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object EdIncremento: TdmkEdit
                  Left = 810
                  Top = 24
                  Width = 71
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 6
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0,01'
                  ValMax = '100'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1.000000000000000000
                  ValWarn = False
                end
              end
              object dmkDBGridZTO4: TdmkDBGridZTO
                Left = 820
                Top = 0
                Width = 176
                Height = 104
                Align = alRight
                DataSource = DsUso
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 2
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                OnDblClick = dmkDBGridZTO4DblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Formula'
                    Title.Caption = 'Receita'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PorcentUso'
                    Title.Caption = '% uso'
                    Width = 83
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet14: TTabSheet
            Caption = ' Produ'#231#227'o '
            ImageIndex = 2
            object Panel22: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 447
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Splitter2: TSplitter
                Left = 409
                Top = 0
                Height = 447
                ExplicitLeft = 524
                ExplicitTop = 96
                ExplicitHeight = 100
              end
              object Panel29: TPanel
                Left = 0
                Top = 0
                Width = 409
                Height = 447
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                object GroupBox2: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 409
                  Height = 65
                  Align = alTop
                  Caption = ' Pesquisa de produ'#231#227'o realizada:'
                  TabOrder = 0
                  TabStop = True
                  object Panel30: TPanel
                    Left = 2
                    Top = 15
                    Width = 405
                    Height = 46
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    TabStop = True
                    object Label16: TLabel
                      Left = 8
                      Top = 4
                      Width = 124
                      Height = 13
                      Caption = 'Data inicial de produzidos:'
                    end
                    object Label17: TLabel
                      Left = 144
                      Top = 4
                      Width = 117
                      Height = 13
                      Caption = 'Data final de produzidos:'
                    end
                    object TPProdzIni: TdmkEditDateTimePicker
                      Left = 8
                      Top = 20
                      Width = 112
                      Height = 21
                      Date = 44054.000000000000000000
                      Time = 0.549949409716646200
                      TabOrder = 0
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      UpdType = utYes
                      DatePurpose = dmkdpNone
                    end
                    object TPProdzFim: TdmkEditDateTimePicker
                      Left = 144
                      Top = 20
                      Width = 112
                      Height = 21
                      Date = 44054.000000000000000000
                      Time = 0.549949409716646200
                      TabOrder = 1
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      UpdType = utYes
                      DatePurpose = dmkdpNone
                    end
                    object BtPesquisar: TBitBtn
                      Tag = 18
                      Left = 276
                      Top = 4
                      Width = 120
                      Height = 40
                      Caption = '&Pesquisar'
                      NumGlyphs = 2
                      TabOrder = 2
                      OnClick = BtPesquisarClick
                    end
                  end
                end
                object DBGProdz: TdmkDBGridZTO
                  Left = 0
                  Top = 65
                  Width = 409
                  Height = 382
                  Align = alClient
                  DataSource = DsProdz
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 1
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Numero'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Nome'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Peso'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Qtde'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaM2'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Min_DataEmis'
                      Title.Caption = 'Min.Data Emis'
                      Visible = True
                    end>
                end
              end
              object Panel32: TPanel
                Left = 412
                Top = 0
                Width = 584
                Height = 447
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object dmkDBGridZTO2: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 584
                  Height = 402
                  Align = alClient
                  DataSource = DsProducao
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Semana'
                      Width = 46
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Peso'
                      Title.Caption = 'Peso kg'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Qtde'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaM2'
                      Title.Caption = #193'rea m'#178
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MediaM2Couro'
                      Title.Caption = 'm'#178'/pe'#231'a'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtFuloes'
                      Title.Caption = 'Fulonadas'
                      Width = 61
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Media5'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DiaSemana'
                      Title.Caption = 'Dias'
                      Visible = True
                    end>
                end
                object dmkDBGridZTO3: TdmkDBGridZTO
                  Left = 0
                  Top = 402
                  Width = 584
                  Height = 45
                  Align = alBottom
                  DataSource = DsSumProd
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 1
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Peso'
                      Title.Caption = 'Peso kg'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Qtde'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaM2'
                      Title.Caption = #193'rea m'#178
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MediaM2Couro'
                      Title.Caption = 'm'#178'/pe'#231'a'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DtIni'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DtFim'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Semanas'
                      Width = 61
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Media5'
                      Visible = True
                    end>
                end
              end
            end
          end
          object TabSheet1: TTabSheet
            Caption = ' Configura'#231#245'es '
            ImageIndex = 3
            object CGTipoCad: TdmkCheckGroup
              Left = 0
              Top = 97
              Width = 996
              Height = 350
              Align = alClient
              Caption = ' Tipo de Cadastro: '
              Columns = 3
              Items.Strings = (
                'FmPrincipal.VAR_TipoCadPQ')
              TabOrder = 0
              OnClick = CGTipoCadClick
              OnDblClick = CGTipoCadDblClick
              OnExit = CGTipoCadExit
              UpdType = utYes
              Value = 0
              OldValor = 0
            end
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 97
              Align = alTop
              ParentBackground = False
              TabOrder = 1
              object BtTodosTipCad: TBitBtn
                Tag = 127
                Left = 9
                Top = 13
                Width = 114
                Height = 40
                Cursor = crHandPoint
                Caption = '&Todos'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtTodosTipCadClick
              end
              object BtNenhumTipCad: TBitBtn
                Tag = 128
                Left = 9
                Top = 53
                Width = 114
                Height = 40
                Cursor = crHandPoint
                Caption = '&Nenhum'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtNenhumTipCadClick
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = ' Avulsos '
            ImageIndex = 4
            object DBGAvulsos: TDBGrid
              Left = 0
              Top = 89
              Width = 996
              Height = 358
              Align = alClient
              DataSource = DsAvulsos
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Insumo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o do insumo'
                  Width = 180
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'KgEstq'
                  Title.Caption = 'kg Estq.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DiasEstq'
                  Title.Caption = 'dd Estq.'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'KgConsumo'
                  Title.Caption = 'kg consumo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Kg_dia'
                  Title.Caption = 'kg/dia'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DiasPeriod'
                  Title.Caption = 'dd per'#237'odo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NecesPeriod'
                  Title.Caption = 'Neces.per'#237'odo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'KgLiqEmb'
                  Title.Caption = 'kg l'#237'q. embal.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Comprar'
                  Title.Caption = 'kg Comprar'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IQ'
                  Title.Caption = 'Fornecedor'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NomeIQ'
                  Title.Caption = 'Nome do fornecedor'
                  Width = 180
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CI'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NomeCI'
                  Title.Caption = 'Nome do cliente interno'
                  Visible = True
                end>
            end
            object Memo1: TMemo
              Left = 0
              Top = 0
              Width = 996
              Height = 89
              Align = alTop
              TabOrder = 1
            end
          end
        end
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object LaCI: TLabel
            Left = 8
            Top = 4
            Width = 70
            Height = 13
            Caption = 'Cliente interno:'
          end
          object EdCI: TdmkEditCB
            Left = 8
            Top = 20
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdCIRedefinido
            DBLookupComboBox = CBCI
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCI: TdmkDBLookupComboBox
            Left = 72
            Top = 20
            Width = 337
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECI'
            ListSource = DsCI2A
            TabOrder = 1
            dmkEditCB = EdCI
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object MeAvisos: TMemo
            Left = 532
            Top = 0
            Width = 472
            Height = 46
            Align = alRight
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Lucida Console'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
          end
          object EdEmpresas: TdmkEdit
            Left = 412
            Top = 20
            Width = 117
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '140,183'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '140,183'
            ValWarn = False
          end
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 586
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object PB1: TProgressBar
        Left = 652
        Top = 0
        Width = 205
        Height = 17
        TabOrder = 1
      end
      object PB2: TProgressBar
        Left = 652
        Top = 16
        Width = 205
        Height = 17
        TabOrder = 2
      end
      object PB3: TProgressBar
        Left = 652
        Top = 32
        Width = 205
        Height = 17
        TabOrder = 3
      end
      object GBAvisos1: TGroupBox
        Left = 140
        Top = 3
        Width = 505
        Height = 44
        Caption = ' Avisos: '
        TabOrder = 4
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 501
          Height = 27
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1: TLabel
            Left = 13
            Top = 2
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 12
            Top = 1
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 7
  end
  object DsCI2A: TDataSource
    DataSet = QrCI
    Left = 924
    Top = 176
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 924
    Top = 128
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object DsDefPecas: TDataSource
    DataSet = QrDefPecas
    Left = 925
    Top = 274
  end
  object QrDefPecas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * From DefPecas'
      'ORDER BY Nome')
    Left = 925
    Top = 226
    object QrDefPecasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDefPecasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object QrPQURec: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT rec.Insumo, rec.NomePQ, rec.CliInt, '
      'rec.NomeCI, pqc.Peso KgEstq, '
      'pqc.peso - SUM(rec.PesoUso) KgSaldoFut,'
      'SUM(rec.PesoUso) PesoUso'
      'FROM pqurec rec'
      'LEFT JOIN pqcli pqc ON pqc.PQ=rec.Insumo '
      '          AND rec.CliInt=rec.CliInt'
      'WHERE Nivel=1'
      'GROUP BY Insumo, CliInt ')
    Left = 544
    Top = 128
    object QrPQURecInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPQURecNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Required = True
      Size = 100
    end
    object QrPQURecCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrPQURecNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Required = True
      Size = 100
    end
    object QrPQURecPesoUso: TFloatField
      FieldName = 'PesoUso'
    end
    object QrPQURecKgEstq: TFloatField
      FieldName = 'KgEstq'
    end
    object QrPQURecKgSaldoFut: TFloatField
      FieldName = 'KgSaldoFut'
    end
    object QrPQURecKgLiqEmb: TFloatField
      FieldName = 'KgLiqEmb'
    end
    object QrPQURecInsumM2PesoKg: TFloatField
      FieldName = 'InsumM2PesoKg'
    end
    object QrPQURecIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQURecNOMEIQ: TWideStringField
      Alignment = taRightJustify
      FieldName = 'NOMEIQ'
      Size = 100
    end
  end
  object QrPQUCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPQUCabBeforeClose
    AfterScroll = QrPQUCabAfterScroll
    OnCalcFields = QrPQUCabCalcFields
    SQL.Strings = (
      'SELECT rec.Nome NOMERECEITA, dpe.Nome UNIDADE, pqu.* '
      'FROM pqucab pqu'
      'LEFT JOIN formulas rec ON rec.Numero=pqu.Receita'
      'LEFT JOIN defpecas dpe ON dpe.Codigo=pqu.DefPeca'
      '')
    Left = 544
    Top = 176
    object QrPQUCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQUCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrPQUCabAplicacao: TSmallintField
      FieldName = 'Aplicacao'
    end
    object QrPQUCabOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPQUCabDtPedido: TDateField
      FieldName = 'DtPedido'
    end
    object QrPQUCabEmitGru: TSmallintField
      FieldName = 'EmitGru'
    end
    object QrPQUCabNO_EmitGru: TWideStringField
      FieldName = 'NO_EmitGru'
      Size = 60
    end
    object QrPQUCabCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPQUCabDtIniProd: TDateTimeField
      FieldName = 'DtIniProd'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrPQUCabDtEntrega: TDateTimeField
      FieldName = 'DtEntrega'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrPQUCabQtProdDia: TFloatField
      FieldName = 'QtProdDia'
    end
    object QrPQUCabPedQtPdGdz: TFloatField
      FieldName = 'PedQtPdGdz'
    end
    object QrPQUCabPedGrandeza: TIntegerField
      FieldName = 'PedGrandeza'
    end
  end
  object QrPQUFrm1: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPQUFrm1CalcFields
    SQL.Strings = (
      'SELECT " " NO_Grandeza,'
      'rec.Nome NOMERECEITA, dpe.Nome UNIDADE, pqu.* '
      'FROM pqufrm pqu'
      'LEFT JOIN formulas rec ON rec.Numero=pqu.Receita'
      'LEFT JOIN defpecas dpe ON dpe.Codigo=pqu.DefPeca'
      '')
    Left = 548
    Top = 324
    object QrPQUFrm1NO_Grandeza: TWideStringField
      FieldName = 'NO_Grandeza'
      Size = 100
    end
    object QrPQUFrm1NOMERECEITA: TWideStringField
      FieldName = 'NOMERECEITA'
      Size = 30
    end
    object QrPQUFrm1UNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 30
    end
    object QrPQUFrm1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQUFrm1Receita: TIntegerField
      FieldName = 'Receita'
    end
    object QrPQUFrm1DefPeca: TIntegerField
      FieldName = 'DefPeca'
    end
    object QrPQUFrm1Media: TFloatField
      FieldName = 'Media'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQUFrm1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQUFrm1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQUFrm1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQUFrm1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQUFrm1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQUFrm1Pecas1: TIntegerField
      FieldName = 'Pecas1'
      DisplayFormat = '#,###,##0'
    end
    object QrPQUFrm1Pecas2: TIntegerField
      FieldName = 'Pecas2'
      DisplayFormat = '#,###,##0'
    end
    object QrPQUFrm1Pecas3: TIntegerField
      FieldName = 'Pecas3'
      DisplayFormat = '#,###,##0'
    end
    object QrPQUFrm1Pecas4: TIntegerField
      FieldName = 'Pecas4'
      DisplayFormat = '#,###,##0'
    end
    object QrPQUFrm1Base1: TFloatField
      FieldName = 'Base1'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQUFrm1Base2: TFloatField
      FieldName = 'Base2'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQUFrm1Base3: TFloatField
      FieldName = 'Base3'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQUFrm1Base4: TFloatField
      FieldName = 'Base4'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQUFrm1KGT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'KGT'
      Calculated = True
    end
    object QrPQUFrm1DMais2: TIntegerField
      FieldName = 'DMais2'
      DisplayFormat = '0;-0; '
    end
    object QrPQUFrm1DMais3: TIntegerField
      FieldName = 'DMais3'
      DisplayFormat = '0;-0; '
    end
    object QrPQUFrm1DMais4: TIntegerField
      FieldName = 'DMais4'
      DisplayFormat = '0;-0; '
    end
    object QrPQUFrm1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPQUFrm1PedGrandeza: TIntegerField
      FieldName = 'PedGrandeza'
      Required = True
    end
    object QrPQUFrm1PedQtdeGdz: TFloatField
      FieldName = 'PedQtdeGdz'
      Required = True
    end
    object QrPQUFrm1PedLinhasCul: TFloatField
      FieldName = 'PedLinhasCul'
      Required = True
    end
    object QrPQUFrm1PedLinhasCab: TFloatField
      FieldName = 'PedLinhasCab'
      Required = True
    end
    object QrPQUFrm1PedAreaM2: TFloatField
      FieldName = 'PedAreaM2'
      Required = True
    end
    object QrPQUFrm1PedAreaP2: TFloatField
      FieldName = 'PedAreaP2'
      Required = True
    end
    object QrPQUFrm1PedPesoKg: TFloatField
      FieldName = 'PedPesoKg'
      Required = True
    end
    object QrPQUFrm1AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPQUFrm1AWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPQUFrm1AWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPQUFrm1Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object QrProdz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(emi.Peso) Peso,  '
      'SUM(emi.Qtde) Qtde,  '
      'SUM(emi.AreaM2) AreaM2,  '
      'rec.Nome, MIN(DataEmis) Min_DataEmis  '
      'FROM bluederm_noroeste.emit emi '
      
        'LEFT JOIN bluederm_noroeste.listasetores lse ON lse.Codigo=emi.S' +
        'etor '
      
        'LEFT JOIN bluederm_noroeste.formulas rec ON rec.Numero=emi.Numer' +
        'o '
      'WHERE lse.TpReceita=2 '
      'AND DataEmis BETWEEN "2020-06-29" AND "2020-08-29" '
      'GROUP BY emi.Numero '
      'ORDER BY Nome ')
    Left = 710
    Top = 127
    object QrProdzNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrProdzPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrProdzQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrProdzAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrProdzNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrProdzMin_DataEmis: TWideStringField
      FieldName = 'Min_DataEmis'
    end
  end
  object DsPQUCab: TDataSource
    DataSet = QrPQUCab
    Left = 544
    Top = 228
  end
  object DsPQUFrm1: TDataSource
    DataSet = QrPQUFrm1
    Left = 548
    Top = 372
  end
  object DsProdz: TDataSource
    DataSet = QrProdz
    Left = 710
    Top = 175
  end
  object QrPedPedEPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM pqurec rec'
      'WHERE Nivel=2')
    Left = 624
    Top = 128
    object QrPedPedEPQInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPedPedEPQPesoUso: TFloatField
      FieldName = 'PesoUso'
      Required = True
    end
    object QrPedPedEPQPedidoCod: TIntegerField
      FieldName = 'PedidoCod'
    end
    object QrPedPedEPQNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Required = True
      Size = 100
    end
    object QrPedPedEPQNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Required = True
      Size = 100
    end
    object QrPedPedEPQPedidoTxt: TWideStringField
      FieldName = 'PedidoTxt'
      Size = 255
    end
    object QrPedPedEPQCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPedPedEPQPedidoOrd: TIntegerField
      FieldName = 'PedidoOrd'
    end
    object QrPedPedEPQInsumM2PesoKg: TFloatField
      FieldName = 'InsumM2PesoKg'
    end
    object QrPedPedEPQIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPedPedEPQNOMEIQ: TWideStringField
      FieldName = 'NOMEIQ'
      Size = 100
    end
  end
  object QrPQUFrm2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT rec.Nome NOMERECEITA, dpe.Nome UNIDADE, pqu.* '
      'FROM pqufrm pqu'
      'LEFT JOIN formulas rec ON rec.Numero=pqu.Receita'
      'LEFT JOIN defpecas dpe ON dpe.Codigo=pqu.DefPeca'
      '')
    Left = 788
    Top = 324
    object QrPQUFrm2NOMERECEITA: TWideStringField
      FieldName = 'NOMERECEITA'
      Size = 30
    end
    object QrPQUFrm2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQUFrm2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPQUFrm2Receita: TIntegerField
      FieldName = 'Receita'
    end
    object QrPQUFrm2DefPeca: TIntegerField
      FieldName = 'DefPeca'
    end
    object QrPQUFrm2Media: TFloatField
      FieldName = 'Media'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQUFrm2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQUFrm2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQUFrm2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQUFrm2UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQUFrm2UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQUFrm2Pecas1: TIntegerField
      FieldName = 'Pecas1'
      DisplayFormat = '#,###,##0'
    end
    object QrPQUFrm2Pecas2: TIntegerField
      FieldName = 'Pecas2'
      DisplayFormat = '#,###,##0'
    end
    object QrPQUFrm2Pecas3: TIntegerField
      FieldName = 'Pecas3'
      DisplayFormat = '#,###,##0'
    end
    object QrPQUFrm2Pecas4: TIntegerField
      FieldName = 'Pecas4'
      DisplayFormat = '#,###,##0'
    end
    object QrPQUFrm2Base1: TFloatField
      FieldName = 'Base1'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQUFrm2Base2: TFloatField
      FieldName = 'Base2'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQUFrm2Base3: TFloatField
      FieldName = 'Base3'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQUFrm2Base4: TFloatField
      FieldName = 'Base4'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQUFrm2KGT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'KGT'
      Calculated = True
    end
    object QrPQUFrm2DMais2: TIntegerField
      FieldName = 'DMais2'
      DisplayFormat = '0;-0; '
    end
    object QrPQUFrm2DMais3: TIntegerField
      FieldName = 'DMais3'
      DisplayFormat = '0;-0; '
    end
    object QrPQUFrm2DMais4: TIntegerField
      FieldName = 'DMais4'
      DisplayFormat = '0;-0; '
    end
    object QrPQUFrm2PedGrandeza: TIntegerField
      FieldName = 'PedGrandeza'
      Required = True
    end
    object QrPQUFrm2PedQtdeGdz: TFloatField
      FieldName = 'PedQtdeGdz'
      Required = True
    end
    object QrPQUFrm2PedLinhasCul: TFloatField
      FieldName = 'PedLinhasCul'
      Required = True
    end
    object QrPQUFrm2PedLinhasCab: TFloatField
      FieldName = 'PedLinhasCab'
      Required = True
    end
    object QrPQUFrm2PedAreaM2: TFloatField
      FieldName = 'PedAreaM2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPQUFrm2PedAreaP2: TFloatField
      FieldName = 'PedAreaP2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPQUFrm2PedPesoKg: TFloatField
      FieldName = 'PedPesoKg'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQUFrm2AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPQUFrm2AWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPQUFrm2AWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPQUFrm2Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPQUFrm2NO_Grandeza: TWideStringField
      FieldName = 'NO_Grandeza'
      Size = 100
    end
    object QrPQUFrm2PedQtPdGdz: TFloatField
      FieldName = 'PedQtPdGdz'
    end
    object QrPQUFrm2PedQtPrGdz: TFloatField
      FieldName = 'PedQtPrGdz'
    end
    object QrPQUFrm2PedPdArM2: TFloatField
      FieldName = 'PedPdArM2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrPQUFrm2PedPdArP2: TFloatField
      FieldName = 'PedPdArP2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrPQUFrm2PedPdPeKg: TFloatField
      FieldName = 'PedPdPeKg'
      Required = True
      DisplayFormat = '#,###,###,##0.000;#,###,###,##0.000; '
    end
    object QrPQUFrm2PedPrArM2: TFloatField
      FieldName = 'PedPrArM2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrPQUFrm2PedPrArP2: TFloatField
      FieldName = 'PedPrArP2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrPQUFrm2PedPrPeKg: TFloatField
      FieldName = 'PedPrPeKg'
      Required = True
      DisplayFormat = '#,###,###,##0.000;#,###,###,##0.000; '
    end
    object QrPQUFrm2RendEsperado: TFloatField
      FieldName = 'RendEsperado'
      DisplayFormat = '0.00'
    end
    object QrPQUFrm2MargemDBI: TFloatField
      FieldName = 'MargemDBI'
    end
  end
  object frxPedOpen: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.488155740700000000
    ReportOptions.LastChange = 40119.835206539350000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxPrevisaoGetValue
    Left = 708
    Top = 224
    Datasets = <
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPedOpen
        DataSetName = 'frxDsPedOpen'
      end
      item
        DataSet = frxDsPQU
        DataSetName = 'frxDsPQU'
      end
      item
        DataSet = frxDsPQUCab
        DataSetName = 'frxDsPQUCab'
      end
      item
        DataSet = frxDsPQUFrm2
        DataSetName = 'frxDsPQUFrm2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.425170000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 512.425170000000000000
          Top = 8.440940000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 3.921150000000000000
          Top = 1.763760000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 36.000000000000000000
        Top = 506.457020000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 0.093999999999937240
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 55.795258500000000000
        Top = 71.811070000000000000
        Width = 680.315400000000000000
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 3.779525120000000000
          Width = 628.409400000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PREVIS'#195'O PARA COMPRA DE INSUMOS QU'#205'MICOS - POR PEDIDOS ABERTOS')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 30.236218030000020000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 30.236218030000000000
          Width = 394.551020000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 30.236240000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Grupo:')
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 528.661720000000000000
          Top = 30.236240000000000000
          Width = 644.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_GRUPO]')
          ParentFont = False
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 6.000000000000000000
        Top = 438.425480000000000000
        Width = 680.315400000000000000
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPedOpen
        DataSetName = 'frxDsPedOpen'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 377.952838900000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soUso">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 34.015770000000000000
          Width = 222.992191890000000000
          Height = 15.118110240000000000
          DataField = 'NomePQ'
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPedOpen."NomePQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480410160000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soEstq">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 619.669450000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soCompra">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 0.000041500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soFut">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 0.000041500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Re' +
              'pet">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedOpen."Insumo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 0.000041500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soNeed">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 0.000041500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Kg' +
              'LiqEmb">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPQUFrm2
        DataSetName = 'frxDsPQUFrm2'
        RowCount = 0
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 238.110228900000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxPQUFrm2."PedAre' +
              'aM2">)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 200.315011890000000000
          Height = 15.118110240000000000
          DataField = 'NOMERECEITA'
          DataSet = frxDsPQUFrm2
          DataSetName = 'frxDsPQUFrm2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQUFrm2."NOMERECEITA"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795221890000000000
          Height = 15.118110240000000000
          DataField = 'Receita'
          DataSet = frxDsPQUFrm2
          DataSetName = 'frxDsPQUFrm2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQUFrm2."Receita"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxPQUFrm2."PedAre' +
              'aP2">)]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxPQUFrm2."PedPes' +
              'oKg">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Width = 79.370051890000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUFrm2
          DataSetName = 'frxDsPQUFrm2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxPQUFrm2."PedLinhasCul"] - [frxPQUFrm2."PedLinhasCab"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPQU."KGT"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 238.110228900000000000
          Top = 3.779530000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ' '#193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 238.110311890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Top = 3.779530000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ' '#193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Top = 3.779530000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 3.779530000000000000
          Width = 79.370081180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Linhas Rebaixe')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 377.952838900000000000
          Top = 3.779488500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg Usar'#225)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779488500000000000
          Width = 257.007961890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480410160000000000
          Top = 3.779488500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Estq atual')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 619.669450000000000000
          Top = 3.779488500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Comprar')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo futuro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Repeti'#231#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Necess'#225'rio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Liq. Embalagem')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPedOpen."NomeCI"'
        object frxDsPQUItsNomeCI: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 801.260360000000000000
          Height = 18.897650000000000000
          DataField = 'NomeCI'
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPedOpen."NomeCI"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsPedOpen: TfrxDBDataset
    UserName = 'frxDsPedOpen'
    CloseDataSource = False
    FieldAliases.Strings = (
      'InsumM2PesoKg=InsumM2PesoKg'
      'Codigo=Codigo'
      'Controle=Controle'
      'Insumo=Insumo'
      'PesoEstq=PesoEstq'
      'PesoUso=PesoUso'
      'PesoFut=PesoFut'
      'PesoNeed=PesoNeed'
      'CliInt=CliInt'
      'Repet=Repet'
      'NomePQ=NomePQ'
      'NomeCI=NomeCI'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'Nivel=Nivel'
      'KgLiqEmb=KgLiqEmb'
      'PesoCompra=PesoCompra'
      'IQ=IQ'
      'NOMEIQ=NOMEIQ'
      'PesComprCarga=PesComprCarga')
    DataSet = QrPedOpen
    BCDToCurrency = False
    DataSetOptions = []
    Left = 708
    Top = 320
  end
  object QrPedComprar: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM pqurec rec'
      'WHERE Nivel=2'
      'ORDER BY NomeIQ, NOMEPQ')
    Left = 624
    Top = 178
    object QrPedComprarInsumM2PesoKg: TFloatField
      FieldName = 'InsumM2PesoKg'
    end
    object QrPedComprarCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPedComprarControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPedComprarInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPedComprarPesoEstq: TFloatField
      FieldName = 'PesoEstq'
      Required = True
    end
    object QrPedComprarPesoUso: TFloatField
      FieldName = 'PesoUso'
      Required = True
    end
    object QrPedComprarPesoFut: TFloatField
      FieldName = 'PesoFut'
      Required = True
    end
    object QrPedComprarPesoNeed: TFloatField
      FieldName = 'PesoNeed'
      Required = True
    end
    object QrPedComprarCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrPedComprarRepet: TFloatField
      FieldName = 'Repet'
      Required = True
    end
    object QrPedComprarNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Required = True
      Size = 100
    end
    object QrPedComprarNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Required = True
      Size = 100
    end
    object QrPedComprarLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrPedComprarDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPedComprarDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPedComprarUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrPedComprarUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrPedComprarAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPedComprarAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPedComprarAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPedComprarAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPedComprarNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPedComprarKgLiqEmb: TFloatField
      FieldName = 'KgLiqEmb'
    end
    object QrPedComprarPesoCompra: TFloatField
      FieldName = 'PesoCompra'
    end
    object QrPedComprarIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPedComprarNOMEIQ: TWideStringField
      FieldName = 'NOMEIQ'
      Size = 100
    end
    object QrPedComprarPesComprCarga: TFloatField
      FieldName = 'PesComprCarga'
    end
    object QrPedComprarPesoPedido: TFloatField
      FieldName = 'PesoPedido'
    end
  end
  object DsPQUFrm2: TDataSource
    DataSet = QrPQUFrm2
    Left = 788
    Top = 372
  end
  object QrPedOpen: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM pqurec rec'
      'WHERE Nivel=2')
    Left = 708
    Top = 272
    object QrPedOpenInsumM2PesoKg: TFloatField
      FieldName = 'InsumM2PesoKg'
    end
    object QrPedOpenCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPedOpenControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPedOpenInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPedOpenPesoEstq: TFloatField
      FieldName = 'PesoEstq'
      Required = True
    end
    object QrPedOpenPesoUso: TFloatField
      FieldName = 'PesoUso'
      Required = True
    end
    object QrPedOpenPesoFut: TFloatField
      FieldName = 'PesoFut'
      Required = True
    end
    object QrPedOpenPesoNeed: TFloatField
      FieldName = 'PesoNeed'
      Required = True
    end
    object QrPedOpenCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrPedOpenRepet: TFloatField
      FieldName = 'Repet'
      Required = True
    end
    object QrPedOpenNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Required = True
      Size = 100
    end
    object QrPedOpenNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Required = True
      Size = 100
    end
    object QrPedOpenLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrPedOpenDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPedOpenDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPedOpenUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrPedOpenUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrPedOpenAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPedOpenAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPedOpenAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPedOpenAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPedOpenNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPedOpenKgLiqEmb: TFloatField
      FieldName = 'KgLiqEmb'
    end
    object QrPedOpenPesoCompra: TFloatField
      FieldName = 'PesoCompra'
    end
    object QrPedOpenIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPedOpenNOMEIQ: TWideStringField
      FieldName = 'NOMEIQ'
      Size = 100
    end
    object QrPedOpenPesComprCarga: TFloatField
      FieldName = 'PesComprCarga'
    end
  end
  object frxDsPedComprar: TfrxDBDataset
    UserName = 'frxDsPedComprar'
    CloseDataSource = False
    FieldAliases.Strings = (
      'InsumM2PesoKg=InsumM2PesoKg'
      'Codigo=Codigo'
      'Controle=Controle'
      'Insumo=Insumo'
      'PesoEstq=PesoEstq'
      'PesoUso=PesoUso'
      'PesoFut=PesoFut'
      'PesoNeed=PesoNeed'
      'CliInt=CliInt'
      'Repet=Repet'
      'NomePQ=NOMEPQ'
      'NomeCI=NOMECI'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'Nivel=Nivel'
      'KgLiqEmb=KgLiqEmb'
      'PesoCompra=PesoCompra'
      'IQ=IQ'
      'NOMEIQ=NOMEIQ'
      'PesComprCarga=PesComprCarga'
      'PesoPedido=PesoPedido')
    DataSet = QrPedComprar
    BCDToCurrency = False
    DataSetOptions = []
    Left = 624
    Top = 228
  end
  object frxDsPQUFrm2: TfrxDBDataset
    UserName = 'frxDsPQUFrm2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMERECEITA=NOMERECEITA'
      'Codigo=Codigo'
      'Receita=Receita'
      'DefPeca=DefPeca'
      'Media=Media'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'Pecas1=Pecas1'
      'Pecas2=Pecas2'
      'Pecas3=Pecas3'
      'Pecas4=Pecas4'
      'Base1=Base1'
      'Base2=Base2'
      'Base3=Base3'
      'Base4=Base4'
      'KGT=KGT'
      'DMais2=DMais2'
      'DMais3=DMais3'
      'DMais4=DMais4'
      'Controle=Controle'
      'PedGrandeza=PedGrandeza'
      'PedQtdeGdz=PedQtdeGdz'
      'PedLinhasCul=PedLinhasCul'
      'PedLinhasCab=PedLinhasCab'
      'PedAreaM2=PedAreaM2'
      'PedAreaP2=PedAreaP2'
      'PedPesoKg=PedPesoKg'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'NO_Grandeza=NO_Grandeza'
      'PedQtPdGdz=PedQtPdGdz'
      'PedQtPrGdz=PedQtPrGdz'
      'PedPdArM2=PedPdArM2'
      'PedPdArP2=PedPdArP2'
      'PedPdPeKg=PedPdPeKg'
      'PedPrArM2=PedPrArM2'
      'PedPrArP2=PedPrArP2'
      'PedPrPeKg=PedPrPeKg')
    DataSet = QrPQUFrm2
    BCDToCurrency = False
    DataSetOptions = []
    Left = 788
    Top = 420
  end
  object frxPedSeq: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.488155740700000000
    ReportOptions.LastChange = 40119.835206539300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxPrevisaoGetValue
    Left = 708
    Top = 368
    Datasets = <
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPedComprar
        DataSetName = 'frxDsPedComprar'
      end
      item
        DataSet = frxDsPedOpen
        DataSetName = 'frxDsPedOpen'
      end
      item
        DataSet = frxDsPedSeq
        DataSetName = 'frxDsPedSeq'
      end
      item
        DataSet = frxDsPQU
        DataSetName = 'frxDsPQU'
      end
      item
        DataSet = frxDsPQUCab
        DataSetName = 'frxDsPQUCab'
      end
      item
        DataSet = frxDsPQUFrm2
        DataSetName = 'frxDsPQUFrm2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.425170000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 512.425170000000000000
          Top = 8.440940000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 3.921150000000000000
          Top = 1.763760000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.661410000000000000
        Top = 449.764070000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 0.094000000000000000
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 55.795258500000000000
        Top = 71.811070000000000000
        Width = 680.315400000000000000
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 3.779525120000000000
          Width = 673.763760000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'PREVIS'#195'O PARA COMPRA DE INSUMOS QU'#205'MICOS - POR PEDIDOS ABERTOS S' +
              'EQUENCIAIS')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 30.236218030000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 30.236218030000000000
          Width = 394.551020000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 30.236240000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Grupo:')
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 528.661720000000000000
          Top = 30.236240000000000000
          Width = 644.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_GRUPO]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015760240000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPQU."KGT"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527668900000000000
          Top = 18.897650000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ' '#193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 245.669371890000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Receita')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 18.897650000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ' '#193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Top = 18.897650000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 18.897650000000000000
          Width = 56.692901180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Linhas Rebaixe')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 18.897650000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ' '#193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598801100000000000
          Top = 18.897650000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ' '#193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 3.779530000000000000
          Width = 136.063001890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Top = 3.779530000000000000
          Width = 260.787491890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quanto do pedido falta produzir')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 0.000009760000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPQUCab
        DataSetName = 'frxDsPQUCab'
        RowCount = 0
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Width = 476.220780000000000000
          Height = 30.236240000000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Pedido: [frxDsPQUCab."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data entrega')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 15.118120000000000000
          Width = 68.031518030000000000
          Height = 15.118110240000000000
          DataField = 'DtEntrega'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQUCab."DtEntrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data ini. prod.')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Top = 15.118120000000000000
          Width = 68.031518030000000000
          Height = 15.118110240000000000
          DataField = 'DtIniProd'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQUCab."DtIniProd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data pedido')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Top = 15.118120000000000000
          Width = 68.031518030000000000
          Height = 15.118110240000000000
          DataField = 'DtPedido'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQUCab."DtPedido"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPQUFrm2
        DataSetName = 'frxDsPQUFrm2'
        RowCount = 0
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527668900000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsPQUFrm2."PedA' +
              'reaM2">)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 207.874071890000000000
          Height = 15.118110240000000000
          DataField = 'NOMERECEITA'
          DataSet = frxDsPQUFrm2
          DataSetName = 'frxDsPQUFrm2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQUFrm2."NOMERECEITA"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795221890000000000
          Height = 15.118110240000000000
          DataField = 'Receita'
          DataSet = frxDsPQUFrm2
          DataSetName = 'frxDsPQUFrm2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQUFrm2."Receita"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsPQUFrm2."PedA' +
              'reaP2">)]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsPQUFrm2."PedP' +
              'esoKg">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692871890000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUFrm2
          DataSetName = 'frxDsPQUFrm2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQUFrm2."PedLinhasCul"] - [frxDsPQUFrm2."PedLinhasCab"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsPQUFrm2."PedP' +
              'dArM2">)]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598801100000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsPQUFrm2."PedP' +
              'dArP2">)]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 385.512060000000000000
        Width = 680.315400000000000000
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader3: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 55.795258500000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779525120000000000
          Width = 680.314960629921300000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'PREVIS'#195'O PARA COMPRA DE INSUMOS QU'#205'MICOS - POR PEDIDOS ABERTOS S' +
              'EQUENCIAIS')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236218030000000000
          Width = 34.015748030000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 34.015748030000000000
          Top = 30.236218030000000000
          Width = 646.299385910000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPedOpen
        DataSetName = 'frxDsPedOpen'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630018900000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soUso">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 34.015770000000000000
          Width = 268.346551890000000000
          Height = 15.118110240000000000
          DataField = 'NomePQ'
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPedOpen."NomePQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157590160000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soEstq">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 619.669450000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soCompra">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 0.000041500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soFut">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Top = 0.000041500000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Re' +
              'pet">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedOpen."Insumo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 0.000041500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soNeed">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 0.000041500000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39', <frxDsPedOpen."KgLiqEmb">' +
              ')]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795290240000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPedOpen."NomeCI"'
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630018900000000000
          Top = 22.677138500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg Usar'#225)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677138500000000000
          Width = 302.362321890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157590160000000000
          Top = 22.677138500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Estq atual')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 619.669450000000000000
          Top = 22.677138500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Comprar')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo futuro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Top = 22.677180000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Repeti'#231#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Necess'#225'rio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 22.677180000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Liq. Embal.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object frxDsPQUItsNomeCI: TfrxMemoView
          AllowVectorExport = True
          Width = 680.314960630000000000
          Height = 18.897650000000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPedOpen."NomeCI"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Width = 559.370195910000000000
          Height = 18.897650000000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total da empresa: [frxDsPedOpen."NomeCI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Width = 120.944920940000000000
          Height = 18.897640240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', SUM(<frxDsPedOpen' +
              '."PesoCompra">,Band4))]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.661410000000000000
        Top = 400.630180000000000000
        Width = 680.315400000000000000
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 0.094000000000000000
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object GroupHeader5: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 196.535560000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPedOpen."NOMEIQ"'
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315155910000000000
          Height = 18.897650000000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor: [frxDsPedOpen."NOMEIQ"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter5: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Width = 559.370195910000000000
          Height = 18.897650000000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total do fornecedor: [frxDsPedOpen."NOMEIQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Width = 120.944920940000000000
          Height = 18.897640240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', SUM(<frxDsPedOpen' +
              '."PesoCompra">,Band4))]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
    object Page3: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader2: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 55.795258500000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779525120000000000
          Width = 680.314960629921300000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'PREVIS'#195'O PARA COMPRA DE INSUMOS QU'#205'MICOS - POR PEDIDOS ABERTOS S' +
              'EQUENCIAIS')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = -1.559060000000000000
          Top = 30.236218030000000000
          Width = 41.574803150000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574803150000000000
          Top = 30.236218030000000000
          Width = 366.614165910000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188976380000000000
          Top = 30.236240000000000000
          Width = 90.708671180000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Grupo:')
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897637800000000000
          Top = 30.236240000000000000
          Width = 181.417391180000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_GRUPO]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.874057250000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPedSeq
        DataSetName = 'frxDsPedSeq'
        RowCount = 0
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897798900000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          DataField = 'PesoUso'
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedSeq."PesoUso"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 275.905611890000000000
          Height = 15.874015750000000000
          DataField = 'NomePQ'
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPedSeq."NomePQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189130160000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          DataField = 'PesoEstq'
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedSeq."PesoEstq"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 0.000041500000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          DataField = 'PesoFut'
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedSeq."PesoFut"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 41.574808030000000000
          Height = 15.874015750000000000
          DataField = 'Insumo'
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedSeq."Insumo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480520000000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          DataField = 'InsumM2PesoKg'
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          DisplayFormat.FormatStr = '%2.6n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedSeq."InsumM2PesoKg"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 46.110297250000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPedSeq."PedidoCod"'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Width = 408.189240000000000000
          Height = 30.236240000000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pedido: [frxDsPedSeq."PedidoTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 90.708661417322830000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data entrega')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 15.118120000000000000
          Width = 90.708661417322830000
          Height = 15.118110240000000000
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          DisplayFormat.FormatStr = 'dd/mm/yyyy  hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPedSeq."DtEntrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 90.708661417322830000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data ini. prod.')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 15.118120000000000000
          Width = 90.708661417322830000
          Height = 15.118110240000000000
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          DisplayFormat.FormatStr = 'dd/mm/yyyy  hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPedSeq."DtIniProd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Width = 90.708661417322830000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data pedido')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 15.118120000000000000
          Width = 90.708661417322830000
          Height = 15.118110240000000000
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPedSeq."DtPedido"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897798900000000000
          Top = 30.236240000000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg Usar'#225)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 317.480441890000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189130160000000000
          Top = 30.236240000000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Estq atual')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 30.236281500000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo futuro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480520000000000000
          Top = 30.236240000000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Uso kg/m'#178)
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.661410000000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897642680000000000
          Top = 0.094000000000000000
          Width = 181.417317950000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
    end
    object Page4: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 55.795258500000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 3.779525120000000000
          Width = 673.763760000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'PREVIS'#195'O PARA COMPRA DE INSUMOS QU'#205'MICOS - POR PEDIDOS ABERTOS S' +
              'EQUENCIAIS')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPedComprar
        DataSetName = 'frxDsPedComprar'
        RowCount = 0
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Width = 566.929421890000000000
          Height = 18.897637800000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsPedComprar
          DataSetName = 'frxDsPedComprar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPedComprar."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 566.756030000000000000
          Width = 113.385860940000000000
          Height = 18.897637800000000000
          DataSet = frxDsPedComprar
          DataSetName = 'frxDsPedComprar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedComprar."PesoPedido"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574776300000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPedComprar."IQ"'
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677138500000000000
          Width = 566.929421890000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 566.756030000000000000
          Top = 22.677138500000000000
          Width = 113.385860940000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Comprar')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Width = 676.535625910000000000
          Height = 18.897650000000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPedComprar."NOMEIQ"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236227800000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 566.929421890000000000
          Height = 18.897637800000000000
          DataSet = frxDsPedComprar
          DataSetName = 'frxDsPedComprar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL: [frxDsPedComprar."NOMEIQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 566.756030000000000000
          Top = 7.559060000000000000
          Width = 113.385860940000000000
          Height = 18.897637800000000000
          DataSet = frxDsPedComprar
          DataSetName = 'frxDsPedComprar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPedComprar."PesoPedido">,MasterData1)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter4: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.661410000000000000
        Top = 332.598640000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrFormulas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 925
    Top = 321
    object QrFormulasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object QrPedSeq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM pqurec rec'
      'WHERE Nivel=2')
    Left = 708
    Top = 417
    object QrPedSeqCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPedSeqControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPedSeqInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPedSeqPesoEstq: TFloatField
      FieldName = 'PesoEstq'
      Required = True
    end
    object QrPedSeqPesoUso: TFloatField
      FieldName = 'PesoUso'
      Required = True
    end
    object QrPedSeqPesoFut: TFloatField
      FieldName = 'PesoFut'
      Required = True
    end
    object QrPedSeqPesoNeed: TFloatField
      FieldName = 'PesoNeed'
      Required = True
    end
    object QrPedSeqCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrPedSeqRepet: TFloatField
      FieldName = 'Repet'
      Required = True
    end
    object QrPedSeqNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Required = True
      Size = 100
    end
    object QrPedSeqNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Required = True
      Size = 100
    end
    object QrPedSeqLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrPedSeqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPedSeqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPedSeqUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrPedSeqUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrPedSeqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPedSeqAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPedSeqAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPedSeqAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPedSeqNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPedSeqKgLiqEmb: TFloatField
      FieldName = 'KgLiqEmb'
    end
    object QrPedSeqPesoCompra: TFloatField
      FieldName = 'PesoCompra'
    end
    object QrPedSeqPedidoCod: TIntegerField
      FieldName = 'PedidoCod'
    end
    object QrPedSeqPedidoTxt: TWideStringField
      FieldName = 'PedidoTxt'
      Size = 100
    end
    object QrPedSeqPedidoOrd: TIntegerField
      FieldName = 'PedidoOrd'
    end
    object QrPedSeqDtPedido: TDateField
      FieldName = 'DtPedido'
    end
    object QrPedSeqInsumM2PercRetr: TFloatField
      FieldName = 'InsumM2PercRetr'
    end
    object QrPedSeqInsumM2PedKgM2: TFloatField
      FieldName = 'InsumM2PedKgM2'
    end
    object QrPedSeqInsumM2PesoKg: TFloatField
      FieldName = 'InsumM2PesoKg'
    end
    object QrPedSeqDtIniProd: TDateTimeField
      FieldName = 'DtIniProd'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrPedSeqDtEntrega: TDateTimeField
      FieldName = 'DtEntrega'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrPedSeqPesComprCarga: TFloatField
      FieldName = 'PesComprCarga'
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 925
    Top = 369
  end
  object frxDsPedSeq: TfrxDBDataset
    UserName = 'frxDsPedSeq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Insumo=Insumo'
      'PesoEstq=PesoEstq'
      'PesoUso=PesoUso'
      'PesoFut=PesoFut'
      'PesoNeed=PesoNeed'
      'CliInt=CliInt'
      'Repet=Repet'
      'NomePQ=NomePQ'
      'NomeCI=NomeCI'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'Nivel=Nivel'
      'KgLiqEmb=KgLiqEmb'
      'PesoCompra=PesoCompra'
      'PedidoCod=PedidoCod'
      'PedidoTxt=PedidoTxt'
      'PedidoOrd=PedidoOrd'
      'DtPedido=DtPedido'
      'InsumM2PercRetr=InsumM2PercRetr'
      'InsumM2PedKgM2=InsumM2PedKgM2'
      'InsumM2PesoKg=InsumM2PesoKg'
      'DtIniProd=DtIniProd'
      'DtEntrega=DtEntrega'
      'PesComprCarga=PesComprCarga')
    DataSet = QrPedSeq
    BCDToCurrency = False
    DataSetOptions = []
    Left = 708
    Top = 465
  end
  object PMPrevGrupo: TPopupMenu
    Left = 72
    Top = 388
    object Incluinovogrupo1: TMenuItem
      Caption = '&Inclui novo grupo'
      object Manual1: TMenuItem
        Caption = '&Pedido '
        OnClick = Manual1Click
      end
      object Incluicomplementodeproduo1: TMenuItem
        Caption = 'Provis'#227'o por &Hist'#243'rico'
        OnClick = Incluicomplementodeproduo1Click
      end
    end
    object Alteragruposelecionado1: TMenuItem
      Caption = '&Altera grupo selecionado'
      OnClick = Alteragruposelecionado1Click
    end
    object Excluigruposelecionado1: TMenuItem
      Caption = '&Exclui grupo selecionado'
      OnClick = Excluigruposelecionado1Click
    end
    object TMenuItem
      Caption = '-'
    end
    object Excluigrupoetodosseusitens1: TMenuItem
      Caption = 'Exclui grupo e todos seus itens'
      OnClick = Excluigrupoetodosseusitens1Click
    end
  end
  object PMItemGrupo: TPopupMenu
    Left = 160
    Top = 384
    object Incluinovoitem1: TMenuItem
      Caption = '&Inclui novo item '
      OnClick = Incluinovoitem1Click
    end
    object Alteraitemselecionado1: TMenuItem
      Caption = '&Altera item selecionado'
      OnClick = Alteraitemselecionado1Click
    end
    object Excluiitemselecionado1: TMenuItem
      Caption = '&Exclui item selecionado'
      OnClick = Excluiitemselecionado1Click
    end
  end
  object QrRec: TMySQLQuery
    Database = Dmod.MyDB
    Left = 628
    Top = 373
    object QrRecClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrRecProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrRecPorcent: TFloatField
      FieldName = 'Porcent'
    end
    object QrRecCI: TIntegerField
      FieldName = 'CI'
    end
    object QrRecEstSegur: TIntegerField
      FieldName = 'EstSegur'
    end
    object QrRecPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrRecNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrRecNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrRecIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrRecNOMEIQ: TWideStringField
      FieldName = 'NOMEIQ'
      Size = 100
    end
  end
  object frxDsPQUCab: TfrxDBDataset
    UserName = 'frxDsPQUCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'Aplicacao=Aplicacao'
      'Ordem=Ordem'
      'DtPedido=DtPedido'
      'DtIniProd=DtIniProd'
      'DtEntrega=DtEntrega')
    DataSet = QrPQUCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 544
    Top = 277
  end
  object QrPQUClc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pui.Insumo, pui.CliInt, pui.ddSegur, pui.Peso,'
      'pui.NomePQ, pui.NomeCI, ddSegur, ddSegur2, ddSegur3, '
      'ddSegur4,'
      'SUM(pui.Peso1) Peso1,'
      'SUM(pui.Peso2) Peso2,'
      'SUM(pui.Peso3) Peso3,'
      'SUM(pui.Peso4) Peso4,'
      ''
      'pui.Peso / SUM(pui.Peso1) Dias1,'
      'pui.Peso / SUM(pui.Peso2) Dias2,'
      'pui.Peso / SUM(pui.Peso3) Dias3,'
      'pui.Peso / SUM(pui.Peso4) Dias4,'
      ''
      'IF((SUM(pui.Peso1) * pui.ddSegur ) < pui.Peso, 0,'
      '(SUM(pui.Peso1) * pui.ddSegur ) - pui.Peso) Need1,'
      ''
      'IF((SUM(pui.Peso2) * pui.ddSegur2 ) < pui.Peso, 0,'
      '(SUM(pui.Peso2) * pui.ddSegur2) - pui.Peso) Need2,'
      ''
      'IF((SUM(pui.Peso3) * pui.ddSegur3 ) < pui.Peso, 0,'
      '(SUM(pui.Peso3) * pui.ddSegur3) - pui.Peso) Need3,'
      ''
      'IF((SUM(pui.Peso4) * pui.ddSegur4 ) < pui.Peso, 0,'
      '(SUM(pui.Peso4) * pui.ddSegur4) - pui.Peso) Need4'
      ''
      'FROM pquclc pui'
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pui.Insumo'
      'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI=-11'
      'GROUP BY pui.Insumo, pui.CliInt'
      'ORDER BY pui.NomeCI, pui.NOMEPQ')
    Left = 789
    Top = 129
    object QrPQUClcInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQUClcCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPQUClcddSegur: TIntegerField
      FieldName = 'ddSegur'
    end
    object QrPQUClcPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPQUClcNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Size = 100
    end
    object QrPQUClcNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Size = 100
    end
    object QrPQUClcddSegur_1: TIntegerField
      FieldName = 'ddSegur_1'
    end
    object QrPQUClcddSegur2: TIntegerField
      FieldName = 'ddSegur2'
    end
    object QrPQUClcddSegur3: TIntegerField
      FieldName = 'ddSegur3'
    end
    object QrPQUClcddSegur4: TIntegerField
      FieldName = 'ddSegur4'
    end
    object QrPQUClcPeso1: TFloatField
      FieldName = 'Peso1'
    end
    object QrPQUClcPeso2: TFloatField
      FieldName = 'Peso2'
    end
    object QrPQUClcPeso3: TFloatField
      FieldName = 'Peso3'
    end
    object QrPQUClcPeso4: TFloatField
      FieldName = 'Peso4'
    end
    object QrPQUClcNeed1: TFloatField
      FieldName = 'Need1'
    end
    object QrPQUClcNeed2: TFloatField
      FieldName = 'Need2'
    end
    object QrPQUClcNeed3: TFloatField
      FieldName = 'Need3'
    end
    object QrPQUClcNeed4: TFloatField
      FieldName = 'Need4'
    end
    object QrPQUClcDias1: TFloatField
      FieldName = 'Dias1'
    end
    object QrPQUClcDias2: TFloatField
      FieldName = 'Dias2'
    end
    object QrPQUClcDias3: TFloatField
      FieldName = 'Dias3'
    end
    object QrPQUClcDias4: TFloatField
      FieldName = 'Dias4'
    end
  end
  object frxDsPQUIts: TfrxDBDataset
    UserName = 'frxDsPQUIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Insumo=Insumo'
      'CliInt=CliInt'
      'ddSegur=ddSegur'
      'Peso=Peso'
      'NomePQ=NomePQ'
      'NomeCI=NomeCI'
      'ddSegur_1=ddSegur_1'
      'ddSegur2=ddSegur2'
      'ddSegur3=ddSegur3'
      'ddSegur4=ddSegur4'
      'Peso1=Peso1'
      'Peso2=Peso2'
      'Peso3=Peso3'
      'Peso4=Peso4'
      'Need1=Need1'
      'Need2=Need2'
      'Need3=Need3'
      'Need4=Need4'
      'Dias1=Dias1'
      'Dias2=Dias2'
      'Dias3=Dias3'
      'Dias4=Dias4')
    DataSet = QrPQUClc
    BCDToCurrency = False
    DataSetOptions = []
    Left = 788
    Top = 176
  end
  object frxPrevisao: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.488155740700000000
    ReportOptions.LastChange = 40119.835206539350000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxPrevisaoGetValue
    Left = 788
    Top = 224
    Datasets = <
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPQU
        DataSetName = 'frxDsPQU'
      end
      item
        DataSet = frxDsPQUIts
        DataSetName = 'frxDsPQUIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.425170000000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 803.448980000000000000
          Top = 4.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 3.921150000000000000
          Top = 1.763760000000001000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 36.000000000000000000
        Top = 506.457020000000000000
        Width = 971.339210000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 0.093999999999937240
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 55.795258500000000000
        Top = 71.811070000000000000
        Width = 971.339210000000000000
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 7.559055119999996000
          Width = 957.228510000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PREVIS'#195'O PARA COMPRA DE INSUMOS QU'#205'MICOS')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 30.236218030000020000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 30.236218030000020000
          Width = 644.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 714.331170000000000000
          Top = 30.236239999999990000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Grupo:')
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 774.331170000000000000
          Top = 30.236239999999990000
          Width = 644.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_GRUPO]')
          ParentFont = False
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 6.000000000000000000
        Top = 438.425480000000000000
        Width = 971.339210000000000000
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 355.275820000000000000
        Width = 971.339210000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPQUIts
        DataSetName = 'frxDsPQUIts'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 264.566938900000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."Pes' +
              'o1">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700777640000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."DIA' +
              'S1">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 34.015770000000010000
          Width = 147.401591890000000000
          Height = 15.118110240000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQUIts."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417330160000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."Pes' +
              'o">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 445.811070000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."Pes' +
              'o2">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677226380000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."DIA' +
              'S2">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 634.960727560000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."Pes' +
              'o3">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 691.653628740000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."DIA' +
              'S3">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 816.378091890000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."Pes' +
              'o4">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 873.070993070000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."DIA' +
              'S4">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 238.110390000000000000
          Top = 0.000041500000008909
          Width = 26.456688030000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQUIts."ddSegur"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 0.000041500000008909
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."Nee' +
              'd1">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000100000
          Top = 0.000041500000008909
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."Nee' +
              'd2">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 733.228820000000000000
          Top = 0.000041500000008909
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."Nee' +
              'd3">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 914.646260000000000000
          Top = 0.000041500000008909
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQUIts."Nee' +
              'd4">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 789.921770000000000000
          Width = 26.456688030000000000
          Height = 15.118110240000000000
          DataField = 'ddSegur4'
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQUIts."ddSegur4"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Width = 26.456688030000000000
          Height = 15.118110240000000000
          DataField = 'ddSegur3'
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQUIts."ddSegur3"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Width = 26.456688030000000000
          Height = 15.118110240000000000
          DataField = 'ddSegur2'
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQUIts."ddSegur2"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQUIts."Insumo"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 230.551330000000000000
        Width = 971.339210000000000000
        DataSet = frxDsPQU
        DataSetName = 'frxDsPQU'
        RowCount = 0
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 328.818948900000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQU."Base1"' +
              '>)]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464537640000000000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39', <frxDsPQU."Pecas1">)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 200.315011890000000000
          Height = 15.118110240000000000
          DataField = 'NOMERECEITA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQU."NOMERECEITA"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 238.110280160000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQU."UNIDADE"]/dia')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882241260000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQU."Base2"' +
              '>)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39', <frxDsPQU."Pecas2">)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 653.858741260000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQU."Base3"' +
              '>)]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39', <frxDsPQU."Pecas3">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 835.276181260000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPQU."Base4"' +
              '>)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 789.921770000000000000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39', <frxDsPQU."Pecas4">)]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795221890000000000
          Height = 15.118110240000000000
          DataField = 'Receita'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQU."Receita"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 188.976500000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsPQU."KGT"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 328.818948900000000000
          Top = 3.779529999999994000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg base 1')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464537640000000000
          Top = 3.779529999999994000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as 1')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 238.110311890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 238.110280160000000000
          Top = 3.779529999999994000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882241260000000000
          Top = 3.779529999999994000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg base 2')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Top = 3.779529999999994000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as 2')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 653.858741260000000000
          Top = 3.779529999999994000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg base 3')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 3.779529999999994000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as 3')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 835.276181260000000000
          Top = 3.779529999999994000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg base 4')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 789.921770000000000000
          Top = 3.779529999999994000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as 4')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897598740000000000
        Top = 268.346630000000000000
        Width = 971.339210000000000000
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 264.566938900000000000
          Top = 3.779488500000013000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Previs'#227'o 1')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700777640000000000
          Top = 3.779488500000013000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Dias')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779488500000013000
          Width = 181.417361890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417330160000000000
          Top = 3.779488500000013000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Estq atual')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 445.811070000000000000
          Top = 3.779488500000013000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Previs'#227'o 2')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677226380000000000
          Top = 3.779488500000013000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'd Prod.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 634.960727560000000000
          Top = 3.779488500000013000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Previs'#227'o 3')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 691.653628740000000000
          Top = 3.779488500000013000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'd Prod.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 816.378091890000000000
          Top = 3.779488500000013000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Previs'#227'o 4')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 873.070993070000000000
          Top = 3.779488500000013000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'd Prod.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 238.110390000000000000
          Top = 3.779530000000022000
          Width = 26.456688030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D.E.S.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Necess'#225'rio 1')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000100000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Necess'#225'rio 2')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 733.228820000000000000
          Top = 3.779530000000022000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Necess'#225'rio 3')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 914.646260000000000000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Necess'#225'rio 4')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Top = 3.779530000000022000
          Width = 26.456688030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D.E.S.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 3.779530000000022000
          Width = 26.456688030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D.E.S.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 789.921770000000000000
          Top = 3.779530000000022000
          Width = 26.456688030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D.E.S.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 309.921460000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsPQUIts."NomeCI"'
        object frxDsPQUItsNomeCI: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 801.260360000000000000
          Height = 18.897650000000000000
          DataField = 'NomeCI'
          DataSet = frxDsPQUIts
          DataSetName = 'frxDsPQUIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPQUIts."NomeCI"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 393.071120000000000000
        Width = 971.339210000000000000
      end
    end
  end
  object QrSumCab: TMySQLQuery
    Database = Dmod.MyDB
    Left = 788
    Top = 276
    object QrSumCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSumCabPedPdArM2: TFloatField
      FieldName = 'PedPdArM2'
    end
  end
  object frxDsPQU: TfrxDBDataset
    UserName = 'frxDsPQU'
    CloseDataSource = False
    DataSet = QrPQUFrm1
    BCDToCurrency = False
    DataSetOptions = []
    Left = 548
    Top = 420
  end
  object QrSumProd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MIN(emi.DataEmis) DtIni, '
      'SUM(emi.Peso) Peso,   '
      'SUM(emi.Qtde) Qtde, SUM(emi.AreaM2) AreaM2,   '
      'SUM(IF(emi.AreaM2>0, emi.AreaM2, 0)) /  '
      'SUM(IF(emi.AreaM2>0, emi.Qtde, 0)) MediaM2Couro,'
      'MAX(emi.DataEmis) DtFim, '
      '(TO_DAYS(MAX(emi.DataEmis)) - TO_DAYS(MIN(emi.DataEmis))) / 7'
      'Semanas,'
      'SUM(emi.Qtde)  / '
      '((TO_DAYS(MAX(emi.DataEmis)) - TO_DAYS(MIN(emi.DataEmis))) / 7)'
      '/ 5 Media5'
      'FROM emit emi  '
      'LEFT JOIN listasetores lse ON lse.Codigo=emi.Setor  '
      'LEFT JOIN formulas rec ON rec.Numero=emi.Numero  '
      'WHERE emi.Numero IN (820,821,822,823,826,827,828,829,831)  '
      'AND DataEmis BETWEEN "2020-06-29" AND "2020-08-29"  '
      ''
      ' ')
    Left = 626
    Top = 421
    object QrSumProdPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumProdQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumProdAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumProdMediaM2Couro: TFloatField
      FieldName = 'MediaM2Couro'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumProdDtIni: TDateTimeField
      FieldName = 'DtIni'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSumProdDtFim: TDateTimeField
      FieldName = 'DtFim'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSumProdSemanas: TFloatField
      FieldName = 'Semanas'
    end
    object QrSumProdMedia5: TFloatField
      FieldName = 'Media5'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsSumProd: TDataSource
    DataSet = QrSumProd
    Left = 626
    Top = 469
  end
  object QrProducao: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT WEEK(emi.DataEmis) Semana, SUM(emi.Peso) Peso,   '
      'SUM(emi.Qtde) Qtde, SUM(emi.AreaM2) AreaM2,   '
      'SUM(IF(emi.AreaM2>0, emi.AreaM2, 0)) /  '
      'SUM(IF(emi.AreaM2>0, emi.Qtde, 0)) MediaM2Couro, '
      'COUNT(emi.Numero) QtFuloes, SUM(emi.Qtde)/5 Media5, '
      'COUNT(DISTINCT(DAYOFWEEK(emi.DataEmis))) DiaSemana   '
      '/*, rec.Nome,  '
      'DATE_FORMAT(MIN(DataEmis), "%Y/%m/%d") Min_DataEmis */ '
      'FROM bluederm_noroeste.emit emi  '
      
        'LEFT JOIN bluederm_noroeste.listasetores lse ON lse.Codigo=emi.S' +
        'etor  '
      
        'LEFT JOIN bluederm_noroeste.formulas rec ON rec.Numero=emi.Numer' +
        'o  '
      'WHERE emi.Numero IN (820,821,822,823,826,827,828,829,831)  '
      'AND DataEmis BETWEEN "2020-06-29" AND "2020-08-29"  '
      'GROUP BY Semana '
      'ORDER BY Semana ')
    Left = 626
    Top = 273
    object QrProducaoSemana: TLargeintField
      FieldName = 'Semana'
    end
    object QrProducaoPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrProducaoQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrProducaoAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrProducaoMediaM2Couro: TFloatField
      FieldName = 'MediaM2Couro'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrProducaoQtFuloes: TLargeintField
      FieldName = 'QtFuloes'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrProducaoMedia5: TFloatField
      FieldName = 'Media5'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrProducaoDiaSemana: TLargeintField
      FieldName = 'DiaSemana'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsProducao: TDataSource
    DataSet = QrProducao
    Left = 626
    Top = 325
  end
  object QrSumCarga: TMySQLQuery
    Database = Dmod.MyDB
    Left = 346
    Top = 295
    object QrSumCargaCargaKgCompra: TFloatField
      FieldName = 'CargaKgCompra'
    end
  end
  object QrPQURecCarga: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPQURecCargaBeforeClose
    AfterScroll = QrPQURecCargaAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM pqurec '
      'WHERE Aplicacao=2'
      'AND Nivel=2;')
    Left = 246
    Top = 171
    object QrPQURecCargaNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPQURecCargaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPQURecCargaControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQURecCargaInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPQURecCargaKgLiqEmb: TFloatField
      FieldName = 'KgLiqEmb'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaPesoEstq: TFloatField
      FieldName = 'PesoEstq'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaPesoUso: TFloatField
      FieldName = 'PesoUso'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaPesoFut: TFloatField
      FieldName = 'PesoFut'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaPesoNeed: TFloatField
      FieldName = 'PesoNeed'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaPesoCompra: TFloatField
      FieldName = 'PesoCompra'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrPQURecCargaRepet: TFloatField
      FieldName = 'Repet'
      Required = True
    end
    object QrPQURecCargaNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Required = True
      Size = 100
    end
    object QrPQURecCargaNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Required = True
      Size = 100
    end
    object QrPQURecCargaLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrPQURecCargaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQURecCargaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQURecCargaUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrPQURecCargaUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrPQURecCargaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPQURecCargaAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPQURecCargaAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPQURecCargaAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPQURecCargaPedidoCod: TIntegerField
      FieldName = 'PedidoCod'
      Required = True
    end
    object QrPQURecCargaPedidoTxt: TWideStringField
      FieldName = 'PedidoTxt'
      Required = True
      Size = 100
    end
    object QrPQURecCargaPedidoOrd: TIntegerField
      FieldName = 'PedidoOrd'
      Required = True
    end
    object QrPQURecCargaInsumM2PercRetr: TFloatField
      FieldName = 'InsumM2PercRetr'
      Required = True
    end
    object QrPQURecCargaInsumM2PedKgM2: TFloatField
      FieldName = 'InsumM2PedKgM2'
      Required = True
    end
    object QrPQURecCargaInsumM2PesoKg: TFloatField
      FieldName = 'InsumM2PesoKg'
      Required = True
    end
    object QrPQURecCargaAreaM2Pedido: TFloatField
      FieldName = 'AreaM2Pedido'
      Required = True
    end
    object QrPQURecCargaIQ: TIntegerField
      FieldName = 'IQ'
      Required = True
    end
    object QrPQURecCargaNomeIQ: TWideStringField
      FieldName = 'NomeIQ'
      Required = True
      Size = 100
    end
    object QrPQURecCargaQtEmbalagens: TFloatField
      FieldName = 'QtEmbalagens'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaPesComprCarga: TFloatField
      FieldName = 'PesComprCarga'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaPesComprPreDef: TFloatField
      FieldName = 'PesComprPreDef'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaCargaKgUso: TFloatField
      FieldName = 'CargaKgUso'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaCargaKgNeed: TFloatField
      FieldName = 'CargaKgNeed'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaCargaKgCompra: TFloatField
      FieldName = 'CargaKgCompra'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaCargaKgQtEmb: TFloatField
      FieldName = 'CargaKgQtEmb'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaCargaPercInc: TFloatField
      FieldName = 'CargaPercInc'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQURecCargaAplicacao: TIntegerField
      FieldName = 'Aplicacao'
      Required = True
    end
  end
  object DsPQURecCarga: TDataSource
    DataSet = QrPQURecCarga
    Left = 248
    Top = 220
  end
  object PMPedItem: TPopupMenu
    Left = 174
    Top = 502
    object Alteraitem1: TMenuItem
      Caption = '&Altera item'
      OnClick = Alteraitem1Click
    end
    object Incluiitemavulso1: TMenuItem
      Caption = '&Inclui item avulso'
      OnClick = Incluiitemavulso1Click
    end
    object Usaclculoautomtico1: TMenuItem
      Caption = 'Usa c'#225'lculo autom'#225'tico'
      OnClick = Usaclculoautomtico1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Abrejaneladocadastrodoinsumo1: TMenuItem
      Caption = 'Abre janela do cadastro do insumo'
      OnClick = Abrejaneladocadastrodoinsumo1Click
    end
  end
  object QrFornecedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOME'
      'FROM entidades'
      'WHERE Fornece2='#39'V'#39
      'ORDER BY Nome')
    Left = 116
    Top = 248
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedoresNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 116
    Top = 296
  end
  object QrUso: TMySQLQuery
    Database = Dmod.MyDB
    Left = 438
    Top = 182
    object QrUsoFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrUsoPorcentUso: TFloatField
      FieldName = 'PorcentUso'
      DisplayFormat = '0.0000'
    end
  end
  object DsUso: TDataSource
    DataSet = QrUso
    Left = 438
    Top = 230
  end
  object QrAvulso: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _pqs_sem_receita_1_;'
      'CREATE TABLE _pqs_sem_receita_1_'
      'SELECT Insumo, SUM(Peso) Peso'
      'FROM bluederm_noroeste.pqx'
      'WHERE DataX BETWEEN "2023-03-01" AND "2023-03-31"'
      'AND Tipo IN (150,190)'
      'AND Peso < 0'
      'GROUP BY Insumo'
      ';'
      'DROP TABLE IF EXISTS _pqs_sem_receita_2_;'
      'CREATE TABLE _pqs_sem_receita_2_'
      'SELECT p1.Insumo, pq.Nome, SUM(p1.Peso) KgConsumo,'
      'ABS(SUM(p1.Peso) / 25) Kg_dia, SUM(pc.Peso) KgEstq, '
      'ABS(SUM(pc.Peso) / (SUM(p1.Peso) / 25)) DiasEstq,'
      'IF(MAX(pc.EstSegur)<45, 45, MAX(pc.EstSegur)) DiasPeriod,'
      
        'ABS(SUM(p1.Peso) / 25) * IF(MAX(pc.EstSegur)<45, 45, pc.EstSegur' +
        ') NecesPeriod, '
      'IF('
      
        '  ((ABS(SUM(p1.Peso) / 25) * IF(MAX(pc.EstSegur)<45, 45, MAX(pc.' +
        'EstSegur))) - SUM(pc.Peso)) > 0,'
      
        '  (ABS(SUM(p1.Peso) / 25) * IF(MAX(pc.EstSegur)<45, 45, MAX(pc.E' +
        'stSegur))) - SUM(pc.Peso), 0'
      ') Comprar'
      'FROM _pqs_sem_receita_1_ p1'
      'LEFT JOIN bluederm_noroeste.pq pq ON pq.Codigo=p1.Insumo'
      'LEFT JOIN bluederm_noroeste.pqcli pc ON pc.PQ=p1.Insumo '
      '  AND pc.CI IN (140,183)'
      'WHERE NOT p1.Insumo IN ('
      '  SELECT DISTINCT Insumo'
      '  FROM bluederm_noroeste.pqurec'
      ')  '
      'GROUP BY p1.Insumo'
      ';'
      'SELECT * '
      'FROM  _pqs_sem_receita_2_'
      'ORDER BY Nome'
      ';')
    Left = 926
    Top = 425
    object QrAvulsoInsumo: TIntegerField
      FieldName = 'Insumo'
    end
  end
  object QrAvulsos: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _pqs_sem_receita_1_;'
      'CREATE TABLE _pqs_sem_receita_1_'
      'SELECT Insumo, SUM(Peso) Peso'
      'FROM bluederm_noroeste.pqx'
      'WHERE DataX BETWEEN "2023-03-01" AND "2023-03-31"'
      'AND Tipo IN (150,190)'
      'AND Peso < 0'
      'GROUP BY Insumo'
      ';'
      'DROP TABLE IF EXISTS _pqs_sem_receita_2_;'
      'CREATE TABLE _pqs_sem_receita_2_'
      'SELECT p1.Insumo, pq.Nome, SUM(p1.Peso) KgConsumo,'
      'ABS(SUM(p1.Peso) / 25) Kg_dia, SUM(pc.Peso) KgEstq, '
      'ABS(SUM(pc.Peso) / (SUM(p1.Peso) / 25)) DiasEstq,'
      'IF(MAX(pc.EstSegur)<45, 45, MAX(pc.EstSegur)) DiasPeriod,'
      
        'ABS(SUM(p1.Peso) / 25) * IF(MAX(pc.EstSegur)<45, 45, pc.EstSegur' +
        ') NecesPeriod, '
      'IF('
      
        '  ((ABS(SUM(p1.Peso) / 25) * IF(MAX(pc.EstSegur)<45, 45, MAX(pc.' +
        'EstSegur))) - SUM(pc.Peso)) > 0,'
      
        '  (ABS(SUM(p1.Peso) / 25) * IF(MAX(pc.EstSegur)<45, 45, MAX(pc.E' +
        'stSegur))) - SUM(pc.Peso), 0'
      ') Comprar'
      'FROM _pqs_sem_receita_1_ p1'
      'LEFT JOIN bluederm_noroeste.pq pq ON pq.Codigo=p1.Insumo'
      'LEFT JOIN bluederm_noroeste.pqcli pc ON pc.PQ=p1.Insumo '
      '  AND pc.CI IN (140,183)'
      'WHERE NOT p1.Insumo IN ('
      '  SELECT DISTINCT Insumo'
      '  FROM bluederm_noroeste.pqurec'
      ')  '
      'GROUP BY p1.Insumo'
      ';'
      'SELECT * '
      'FROM  _pqs_sem_receita_2_'
      'ORDER BY Nome'
      ';')
    Left = 926
    Top = 477
    object QrAvulsosInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrAvulsosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrAvulsosKgConsumo: TFloatField
      FieldName = 'KgConsumo'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrAvulsosKg_dia: TFloatField
      FieldName = 'Kg_dia'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrAvulsosKgEstq: TFloatField
      FieldName = 'KgEstq'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrAvulsosDiasEstq: TFloatField
      FieldName = 'DiasEstq'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrAvulsosDiasPeriod: TIntegerField
      FieldName = 'DiasPeriod'
    end
    object QrAvulsosNecesPeriod: TFloatField
      FieldName = 'NecesPeriod'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrAvulsosComprar: TFloatField
      FieldName = 'Comprar'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrAvulsosCI: TIntegerField
      FieldName = 'CI'
    end
    object QrAvulsosIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrAvulsosNomeIQ: TWideStringField
      FieldName = 'NomeIQ'
      Size = 100
    end
    object QrAvulsosNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Size = 100
    end
    object QrAvulsosKgLiqEmb: TFloatField
      FieldName = 'KgLiqEmb'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object DsAvulsos: TDataSource
    DataSet = QrAvulsos
    Left = 925
    Top = 529
  end
end
