unit TintasCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, dmkDBLookupComboBox, dmkEditCB, UnDmkProcFunc,
  dmkImage, UnDmkEnums, dmkCheckBox;

type
  TFmTintasCab = class(TForm)
    PainelDados: TPanel;
    DsTintasCab: TDataSource;
    QrTintasCab: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdNumero: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdTxtUsu: TdmkEdit;
    PMReceita: TPopupMenu;
    QrTintasIts: TmySQLQuery;
    DsTintasIts: TDataSource;
    PnDados: TPanel;
    QrTintasFlu: TmySQLQuery;
    DsTintasFlu: TDataSource;
    QrTintasCabNOMECLII: TWideStringField;
    QrTintasCabNOMESETOR: TWideStringField;
    QrTintasCabLINHAS: TWideStringField;
    QrTintasCabNumero: TIntegerField;
    QrTintasCabTxtUsu: TWideStringField;
    QrTintasCabNome: TWideStringField;
    QrTintasCabClienteI: TIntegerField;
    QrTintasCabTipificacao: TIntegerField;
    QrTintasCabDataI: TDateField;
    QrTintasCabDataA: TDateField;
    QrTintasCabTecnico: TWideStringField;
    QrTintasCabSetor: TIntegerField;
    QrTintasCabEspessura: TIntegerField;
    QrTintasCabAreaM2: TFloatField;
    QrTintasCabQtde: TFloatField;
    Incluinovareceita1: TMenuItem;
    Altera1: TMenuItem;
    Excluireceitaatual1: TMenuItem;
    Label13: TLabel;
    DBEdit6: TDBEdit;
    Label14: TLabel;
    DBEdit7: TDBEdit;
    Label15: TLabel;
    DBEdit8: TDBEdit;
    Label19: TLabel;
    DBEdit9: TDBEdit;
    Label20: TLabel;
    DBEdit11: TDBEdit;
    DBEdit15: TDBEdit;
    Label27: TLabel;
    DBEdit16: TDBEdit;
    Label21: TLabel;
    Label28: TLabel;
    DBEdit17: TDBEdit;
    QrTintasCabLk: TIntegerField;
    QrTintasCabDataCad: TDateField;
    QrTintasCabDataAlt: TDateField;
    QrTintasCabUserCad: TIntegerField;
    QrTintasCabUserAlt: TIntegerField;
    QrTintasCabAlterWeb: TSmallintField;
    QrTintasCabAtivo: TSmallintField;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    EdAreaM2: TdmkEdit;
    EdQtde: TdmkEdit;
    EdClienteI: TdmkEditCB;
    CBClienteI: TdmkDBLookupComboBox;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    EdTecnico: TdmkEdit;
    EdEspessura: TdmkEditCB;
    CBEspessura: TdmkDBLookupComboBox;
    EdSetor: TdmkEditCB;
    CBSetor: TdmkDBLookupComboBox;
    DsEntidades: TDataSource;
    DsListaSetores: TDataSource;
    DsEspessuras: TDataSource;
    QrEspessuras: TmySQLQuery;
    QrEspessurasCodigo: TIntegerField;
    QrEspessurasLinhas: TWideStringField;
    QrEspessurasEMCM: TFloatField;
    QrListaSetores: TmySQLQuery;
    QrListaSetoresCodigo: TIntegerField;
    QrListaSetoresNome: TWideStringField;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    PMProcesso: TPopupMenu;
    Incluinovoprocesso1: TMenuItem;
    Alteraprocessoatual1: TMenuItem;
    Excluiprocessoatual1: TMenuItem;
    QrTintasTin: TmySQLQuery;
    DsTintasTin: TDataSource;
    QrTintasTinOrdem: TIntegerField;
    QrTintasTinReordem: TIntegerField;
    QrTintasTinNumero: TIntegerField;
    QrTintasTinCodigo: TIntegerField;
    QrTintasTinNome: TWideStringField;
    QrTintasTinObserv: TWideStringField;
    QrTintasItsNOMEPQ: TWideStringField;
    QrTintasItsNumero: TIntegerField;
    QrTintasItsCodigo: TIntegerField;
    QrTintasItsOrdem: TIntegerField;
    QrTintasItsControle: TIntegerField;
    QrTintasItsReordem: TIntegerField;
    QrTintasItsProduto: TIntegerField;
    QrTintasItsGramasTi: TFloatField;
    QrTintasItsGramasKg: TFloatField;
    QrTintasItsObs: TWideStringField;
    QrTintasItsCusto: TFloatField;
    PMItens: TPopupMenu;
    Incluinovoitemdereceita1: TMenuItem;
    Alteraitemdereceitaatual1: TMenuItem;
    Excluiitemdereceitaatual1: TMenuItem;
    QrTintasTinGramasTi: TFloatField;
    QrTintasFluOrdem: TIntegerField;
    QrTintasFluReordem: TIntegerField;
    QrTintasFluNumero: TIntegerField;
    QrTintasFluCodigo: TIntegerField;
    QrTintasFluNome: TWideStringField;
    QrTintasFluTintasTin: TIntegerField;
    QrTintasFluGramasM2: TFloatField;
    PMFluxo: TPopupMenu;
    Incluinovoitemdefluxo1: TMenuItem;
    Alteraitemdefluxoatual1: TMenuItem;
    Excluiitemdefluxoatual1: TMenuItem;
    QrTintasFluNOMEPROCESSO: TWideStringField;
    QrDupl: TmySQLQuery;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel7: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtReceita: TBitBtn;
    BtProcesso: TBitBtn;
    BtItens: TBitBtn;
    BtFluxo: TBitBtn;
    QrTintasFluInfoCargM2: TFloatField;
    N1: TMenuItem;
    Duplicareceitaatual1: TMenuItem;
    N2: TMenuItem;
    Duplicaprocessoatual1: TMenuItem;
    Reordenaprocessos1: TMenuItem;
    N3: TMenuItem;
    N5: TMenuItem;
    Reordenafluxo1: TMenuItem;
    Copianomedaoperaoparadescriodofluxo1: TMenuItem;
    Panel1: TPanel;
    DBGrid3: TDBGrid;
    QrTintasFluDescri: TWideStringField;
    Splitter1: TSplitter;
    N4: TMenuItem;
    QrTintasFluOpProc: TSmallintField;
    QrTintasFluOpProcStr: TWideStringField;
    QrTintasItsStatus_TXT: TWideStringField;
    CkAtiva: TdmkCheckBox;
    DBCheckBox1: TDBCheckBox;
    QrTintasFluInfoCargM2_TXT: TWideStringField;
    QrTintasFluGramasM2_TXT: TWideStringField;
    Excluidiversasreceitas1: TMenuItem;
    N6: TMenuItem;
    QrTintasFluTintasTin_TXT: TWideStringField;
    Panel4: TPanel;
    Panel9: TPanel;
    DBGrid2: TDBGrid;
    DBGrid1: TDBGrid;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    QrTintasFluDESCRI_TXT: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTintasCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTintasCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtReceitaClick(Sender: TObject);
    procedure PMReceitaPopup(Sender: TObject);
    procedure QrTintasCabBeforeClose(DataSet: TDataSet);
    procedure QrTintasCabAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Incluinovareceita1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure Incluinovoprocesso1Click(Sender: TObject);
    procedure BtProcessoClick(Sender: TObject);
    procedure QrTintasTinAfterScroll(DataSet: TDataSet);
    procedure QrTintasTinBeforeClose(DataSet: TDataSet);
    procedure Alteraprocessoatual1Click(Sender: TObject);
    procedure PMProcessoPopup(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure QrTintasTinAfterOpen(DataSet: TDataSet);
    procedure Incluinovoitemdefluxo1Click(Sender: TObject);
    procedure BtFluxoClick(Sender: TObject);
    procedure Alteraitemdefluxoatual1Click(Sender: TObject);
    procedure Excluiitemdefluxoatual1Click(Sender: TObject);
    procedure PMFluxoPopup(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Duplicareceitaatual1Click(Sender: TObject);
    procedure Excluiprocessoatual1Click(Sender: TObject);
    procedure Excluireceitaatual1Click(Sender: TObject);
    procedure Duplicaprocessoatual1Click(Sender: TObject);
    procedure Reordenaprocessos1Click(Sender: TObject);
    procedure Reordenafluxo1Click(Sender: TObject);
    procedure Copianomedaoperaoparadescriodofluxo1Click(Sender: TObject);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QrTintasFluCalcFields(DataSet: TDataSet);
    procedure Excluidiversasreceitas1Click(Sender: TObject);
  private
    procedure ConfiguraOpProc();
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Numero: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    FOpProc: TStringList;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenTintasTin(Codigo: Integer);
    procedure ReopenTintasIts(Controle: Integer);
    procedure ReopenTintasFlu(Codigo: Integer);
  end;

var
  FmTintasCab: TFmTintasCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, Principal, TintasTin, TintasIts, Espessuras,
  ListaSetores, TintasFlu, FormulasImpFI, DmkDAC_PF, ModuleGeral, UnReordena,
  BlueDermConsts, TintasPesq, GerlShowGrid, UnPQ_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTintasCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTintasCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTintasCabNumero.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTintasCab.DefParams;
begin
  VAR_GOTOTABELA := 'TintasCab';
  VAR_GOTOMYSQLTABLE := QrTintasCab;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_NUMERO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT IF(cli.Tipo=0, Cli.RazaoSocial, cli.Nome)');
  VAR_SQLx.Add('NOMECLII, lse.Nome NOMESETOR, esp.LINHAS, cab.*');
  VAR_SQLx.Add('FROM tintascab cab');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=cab.ClienteI');
  VAR_SQLx.Add('LEFT JOIN listasetores lse ON lse.Codigo=cab.Setor');
  VAR_SQLx.Add('LEFT JOIN espessuras esp ON esp.Codigo=cab.Espessura');
  VAR_SQLx.Add('WHERE cab.Numero>0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND cab.Numero=:P0');
  //
  VAR_SQL2.Add('AND cab.TxtUsu=:P0');
  //
  VAR_SQLa.Add('AND cab.Nome Like :P0');
  //
end;

procedure TFmTintasCab.Duplicareceitaatual1Click(Sender: TObject);
var
  TxtUsu, Nome: String;
  Numero, Codigo, Controle, TintasTin, Ordem: Integer;
  Qry: TmySQLQuery;
begin
  if Geral.MB_Pergunta('Confirma a duplica��o da receita atual?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    QrTintasCab.DisableControls;
    QrTintasTin.DisableControls;
    QrTintasIts.DisableControls;
    QrTintasFlu.DisableControls;
    //
    Qry := TmySQLQuery.Create(Dmod);
    try
      Numero := UMyMod.BuscaEmLivreY_Def('TintasCab', 'Numero', stIns, 0);
      Nome   := Copy('<c�pia> ' + QrTintasCabNome.Value, 1, 100);
      TxtUsu := Geral.FFN(Numero, 9);
      //
      if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'tintascab', TMeuDB,
      ['Numero'], [QrTintasCabNumero.Value],
      ['Numero', 'TxtUsu', 'Nome'],
      [Numero, TxtUsu, Nome],
      '', True, LaAviso1, LaAviso2) then
      begin
        QrTintasTin.First;
        while not QrTintasTin.Eof do
        begin
          Codigo := UMyMod.BuscaEmLivreY_Def('tintastin', 'Codigo', stIns, 0);
          if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'tintastin', TMeuDB,
          ['Codigo'], [QrTintasTinCodigo.Value],
          ['Numero', 'Codigo'],
          [Numero, Codigo],
          '', True, LaAviso1, LaAviso2) then
          begin
            QrTintasIts.First;
            while not QrTintasIts.Eof do
            begin
              Controle := UMyMod.BuscaEmLivreY_Def('tintasits', 'Controle', stIns, 0);
              //
              if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'tintasits', TMeuDB,
              ['Controle'], [QrTintasItsControle.Value],
              ['Numero', 'Codigo', 'Controle'],
              [Numero, Codigo, Controle],
              '', True, LaAviso1, LaAviso2) then
              begin
                //
              end;
              //
              QrTintasIts.Next;
            end;
          end;
          //
          QrTintasTin.Next;
        end;
        QrTintasFlu.First;
        while not QrTintasFlu.Eof do
        begin
          Codigo := UMyMod.BuscaEmLivreY_Def('TintasFlu', 'Codigo', stIns, 0);
          UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT Ordem ',
          'FROM tintastin ',
          'WHERE Codigo=' + Geral.FF0(QrTintasFluTintasTin.Value),
          '']);
          Ordem := Qry.FieldByName('Ordem').AsInteger;
          //
          UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT Codigo ',
          'FROM tintastin ',
          'WHERE Numero=' + Geral.FF0(Numero),
          'AND Ordem=' + Geral.FF0(Ordem),
          '']);
          TintasTin := Qry.FieldByName('Codigo').AsInteger;
          //
          if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'TintasFlu', TMeuDB,
          ['Codigo'], [QrTintasFluCodigo.Value],
          ['Numero', 'Codigo', 'TintasTin'],
          [Numero, Codigo, TintasTin],
          '', True, LaAviso1, LaAviso2) then
          begin
            //
          end;
          //
          QrTintasFlu.Next;
        end;
      end;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      LocCod(Numero, Numero);
      //
      Screen.Cursor := crDefault;
      Geral.MB_Info('Duplica��o conclu�da com sucesso!');
    finally
      QrTintasCab.EnableControls;
      QrTintasTin.EnableControls;
      QrTintasIts.EnableControls;
      QrTintasFlu.EnableControls;
      Qry.Free;
    end;
  end;
end;

procedure TFmTintasCab.Duplicaprocessoatual1Click(Sender: TObject);
var
  Codigo, Controle, Ordem, Numero: Integer;
begin
  if (QrTintasTin.State <> dsInactive) and (QrTintasTin.RecordCount > 0) then
  begin
    if Geral.MB_Pergunta('Deseja realmente duplicar este processo?') <> ID_YES then Exit;
    //
    Screen.Cursor := crHourGlass;
    try
      QrTintasTin.DisableControls;
      QrTintasIts.DisableControls;
      //
      Codigo := UMyMod.BuscaEmLivreY_Def('tintastin', 'Codigo', stIns,
                  QrTintasTinCodigo.Value);
      Ordem  := UReordena.VerificaOrdem(Dmod.QrAux, Dmod.MyDB, 'Numero',
                  'Ordem', 'tintastin', QrTintasCabNumero.Value, 0, True);
      //
      if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'tintastin', TMeuDB,
        ['Codigo', 'Ordem'], [QrTintasTinCodigo.Value, QrTintasTinOrdem.Value],
        ['Codigo', 'Ordem'], [Codigo, Ordem],
        '', True, LaAviso1, LaAviso2) then
      begin
        if (QrTintasIts.State <> dsInactive) and (QrTintasIts.RecordCount > 0) then
        begin
          QrTintasIts.First;
          //
          while not QrTintasIts.Eof do
          begin
            Controle := UMyMod.BuscaEmLivreY_Def('tintasits', 'Controle', stIns, 0);
            Numero   := QrTintasItsNumero.Value;
            Ordem    := QrTintasItsOrdem.Value;
            //
            if not UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'tintasits', TMeuDB,
              ['Controle'], [QrTintasItsControle.Value],
              ['Codigo', 'Controle', 'Numero', 'Ordem'], [Codigo, Controle, Numero, Ordem],
              '', True, LaAviso1, LaAviso2) then
            begin
              Geral.MB_Erro('Falha ao duplicar!');
              Exit;
            end;
            //
            QrTintasIts.Next;
          end;
        end;
      end;
    finally
      QrTintasTin.EnableControls;
      QrTintasIts.EnableControls;
      //
      ReopenTintasTin(Codigo);
      //
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmTintasCab.Excluidiversasreceitas1Click(Sender: TObject);
var
(*
  Qry: TmySQLQuery;
  Ds: TDataSource;
  I, Numero: Integer;
*)
  Numero: Integer;
begin
  (*
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT fml.Numero ID, IF(fml.Ativo = 1, "Sim", "N�o") Ativo,',
      'lse.Nome Setor, fml.Nome',
      'FROM tintascab fml ',
      'LEFT JOIN listasetores lse ON lse.Codigo=fml.Setor ',
      'WHERE fml.Numero<> 0 ',
      '']);
    //
    if Qry.RecordCount > 0 then
    begin
      Ds := TDataSource.Create(Dmod);
      Ds.DataSet := Qry;
      try
        if DBCheck.EscolheCodigoUniGrid('Aviso',
          'Exclus�o de Receitas de Acabamento',
          'Selecione os itens que deseja excluir!', Ds, True, False) then
        begin
          if FmGerlShowGrid.DBGSel.SelectedRows.Count > 0 then
          begin
            if Geral.MB_Pergunta('Deseja excluir TODAS as receitas selecionadas (tamb�m seus itens e fluxo)?') <> ID_YES then
              Exit;
            //
            with FmGerlShowGrid.DBGSel.DataSource.DataSet do
            for I := 0 to FmGerlShowGrid.DBGSel.SelectedRows.Count-1 do
            begin
              //GotoBookmark(pointer(FmGerlShowGrid.DBGSel.SelectedRows.Items[I]));
              GotoBookmark(FmGerlShowGrid.DBGSel.SelectedRows.Items[I]);
              //
              Numero := Qry.FieldByName('ID').AsInteger;
              //
              PQ_PF.ExcluiReceita(Numero);
            end;
          end;
          FmGerlShowGrid.Destroy;
          Va(vpLast);
        end;
      finally
        Ds.Free;
      end;
    end else
      Geral.MB_Aviso('N�o foi localizada nenhuma receita!');
  finally
    Qry.Free;
  end;
  *)
  Numero := QrTintasCabNumero.Value;
  //
  Application.CreateForm(TFmTintasPesq, FmTintasPesq);
  FmTintasPesq.FExclui := True;
  FmTintasPesq.ShowModal;
  FmTintasPesq.Destroy;
  //
  UnDmkDAC_PF.AbreQueryApenas(QrTintasCab);
  if QrTintasCab.Locate('Numero', Numero, []) then
    LocCod(Numero, Numero)
  else
    Va(vpLast);
end;

procedure TFmTintasCab.Excluiitemdefluxoatual1Click(Sender: TObject);
begin
  if (QrTintasFlu.State = dsInactive) or (QrTintasFlu.RecordCount = 0) then Exit;
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, FmTintasCab.QrTintasFlu, DBGrid3,
    'tintasflu', ['Codigo'], ['Codigo'], istPergunta, '');
end;

procedure TFmTintasCab.Excluiprocessoatual1Click(Sender: TObject);

  function LiberaExclusao(Processo: Integer): Boolean;
  var
    Qry: TmySQLQuery;
  begin
    Result := False;
    Qry    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM tintasflu ',
        'WHERE TintasTin=' + Geral.FF0(Processo),
        '']);
      if Qry.RecordCount = 0 then
        Result := True;
    finally
      Qry.Free;
    end;
  end;

var
  Libera, Libera2, Libera3: Boolean;
begin
  Libera := (QrTintasTin.State <> dsInactive) and (QrTintasTin.RecordCount > 0);
  //
  if not Libera then Exit;
  //
  Libera2 := QrTintasIts.RecordCount = 0;
  //
  if Libera2 then
  begin
    Libera3 := LiberaExclusao(QrTintasTinCodigo.Value);
    //
    if Libera3 then
    begin
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrTintasTin, 'tintastin', ['Codigo'],
        ['Codigo'], True, 'Confirma a exclus�o do registro?');
    end else
      Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
        'Motivo: Este processo est� inserido no fluxo!');
  end else
    Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
      'Motivo: Este processo possui itens cadastrados para ele!');
end;

procedure TFmTintasCab.Excluireceitaatual1Click(Sender: TObject);
var
  Libera, Libera2, Libera3: Boolean;
begin
  Libera  := (QrTintasCab.State <> dsInactive) and (QrTintasCab.RecordCount > 0);
  //
  if not Libera then Exit;
  //
  Libera2 := (QrTintasTin.RecordCount = 0);
  Libera3 := (QrTintasFlu.RecordCount = 0);
  //
  if Libera2 and Libera3 then
  begin
    if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintazcab', 'tintascab',
      dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [QrTintasCabNumero.Value],
      Dmod.MyDB) then
    begin
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrTintasCab, 'tintascab', ['Numero'],
        ['Numero'], True, 'Confirma a exclus�o do registro?');
      Va(vpLast);
    end;
  end else
  begin
    if Geral.MB_Pergunta('Deseja excluir TODA a receita (tamb�m seus itens e fluxo)?') <> ID_YES then
      Exit;
    //
    PQ_PF.ExcluiReceita(QrTintasCabNumero.Value);
    Va(vpLast);
    (*
    if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintazflu', 'tintasflu',
      dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [QrTintasFluNumero.Value],
      Dmod.MyDB) then
    begin
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrTintasFlu, 'tintasflu', ['Numero'],
        ['Numero'], True, '');
    end;
    //
    if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintazits', 'tintasits',
      dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [QrTintasItsNumero.Value],
      Dmod.MyDB) then
    begin
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrTintasIts, 'tintasits', ['Numero'],
        ['Numero'], True, '');
    end;
    //
    if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintaztin', 'tintastin',
      dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [QrTintasTinNumero.Value],
      Dmod.MyDB) then
    begin
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrTintasTin, 'tintastin', ['Numero'],
        ['Numero'], True, '');
    end;
    //
    if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintazcab', 'tintascab',
      dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [QrTintasCabNumero.Value],
      Dmod.MyDB) then
    begin
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrTintasCab, 'tintascab', ['Numero'],
        ['Numero'], True, '');
      Va(vpLast);
    end;
    *)
  end;
end;

procedure TFmTintasCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Numero: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdNumero.Text := FormatFloat(FFormatFloat, Numero);
        EdNome.Text := '';
        //...
      end else begin
        EdNumero.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmTintasCab.PMFluxoPopup(Sender: TObject);
var
  Cab, Flu: Boolean;
begin
  Cab := (QrTintasCab.State <> dsInactive) and (QrTintasCab.RecordCount > 0);
  Flu := (QrTintasFlu.State <> dsInactive) and (QrTintasFlu.RecordCount > 0);
  //
  Incluinovoitemdefluxo1.Enabled               := Cab;
  Alteraitemdefluxoatual1.Enabled              := Cab and Flu;
  Excluiitemdefluxoatual1.Enabled              := Cab and Flu;
  Reordenafluxo1.Enabled                       := Cab and Flu;
  Copianomedaoperaoparadescriodofluxo1.Enabled := Cab and Flu;
end;

procedure TFmTintasCab.PMItensPopup(Sender: TObject);
var
  Tin, Its: Boolean;
begin
  Tin := (QrTintasTin.State <> dsInactive) and (QrTintasTin.RecordCount > 0);
  Its := (QrTintasIts.State <> dsInactive) and (QrTintasIts.RecordCount > 0);
  //
  Incluinovoitemdereceita1.Enabled  := Tin;
  Alteraitemdereceitaatual1.Enabled := Its;
  Excluiitemdereceitaatual1.Enabled := Its;
end;

procedure TFmTintasCab.PMProcessoPopup(Sender: TObject);
var
  Cab, Tin: Boolean;
begin
  Cab := (QrTintasCab.State <> dsInactive) and (QrTintasCab.RecordCount > 0);
  Tin := (QrTintasTin.State <> dsInactive) and (QrTintasTin.RecordCount > 0);
  //
  Incluinovoprocesso1.Enabled   := Cab;
  Alteraprocessoatual1.Enabled  := Tin;
  Excluiprocessoatual1.Enabled  := Tin;
  Duplicaprocessoatual1.Enabled := Tin;
  Reordenaprocessos1.Enabled    := Tin;
end;

procedure TFmTintasCab.PMReceitaPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrTintasCab.State <> dsInactive) and (QrTintasCab.RecordCount > 0);
  //
  Altera1.Enabled              := Enab;
  Excluireceitaatual1.Enabled  := Enab;
  Duplicareceitaatual1.Enabled := Enab;
end;

procedure TFmTintasCab.ConfiguraOpProc;
begin
  FOpProc := TStringList.Create;
  FOpProc.Add('N�o informado');
  FOpProc.Add('Opera��o');
  FOpProc.Add('Processo');
end;

procedure TFmTintasCab.Copianomedaoperaoparadescriodofluxo1Click(
  Sender: TObject);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE tintasflu SET Descri=Nome ',
  'WHERE Descri IS NULL ',
  'AND Nome <> "" ',
  '']);
  ReopenTintasFlu(0);
end;

procedure TFmTintasCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTintasCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTintasCab.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  Bold: Boolean;
begin
  if (Column.FieldName = 'Status_TXT')then
  begin
    if QrTintasItsStatus_TXT.Value = CO_Forml_Status_Prod_Irregular then
    begin
      Cor  := clRed;
      Bold := True;
    end else
    begin
      Cor  := clGreen;
      Bold := False;
    end;
    with DBGrid2.Canvas do
    begin
      if Bold then
        Font.Style := [fsBold]
      else
        Font.Style := [];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmTintasCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTintasCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTintasCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTintasCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTintasCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTintasCab.SpeedButton5Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEspessuras, FmEspessuras, afmoNegarComAviso) then
  begin
    VAR_CADASTRO := 0;
    FmEspessuras.ShowModal;
    FmEspessuras.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      EdEspessura.ValueVariant := VAR_CADASTRO;
      CBEspessura.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmTintasCab.SpeedButton6Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmListaSetores, FmListaSetores, afmoNegarComAviso) then
  begin
    VAR_CADASTRO := 0;
    FmListaSetores.ShowModal;
    FmListaSetores.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      EdSetor.ValueVariant := VAR_CADASTRO;
      CBSetor.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmTintasCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTintasCabNumero.Value;
  Close;
end;

procedure TFmTintasCab.Altera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrTintasCab, [PainelDados],
  [PainelEdita], EdTxtUsu, ImgTipo, 'tintascab');
end;

procedure TFmTintasCab.BtReceitaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMReceita, BtReceita);
end;

procedure TFmTintasCab.Alteraitemdefluxoatual1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmTintasFlu, FmTintasFlu, afmoNegarComAviso,
    QrTintasFlu, stUpd);
end;

procedure TFmTintasCab.Alteraprocessoatual1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmTintasTin, FmTintasTin, afmoNegarComAviso,
    QrTintasTin, stUpd);
end;

procedure TFmTintasCab.BtConfirmaClick(Sender: TObject);
var
  Numero: Integer;
  Nome, Referencia: String;
begin
  Referencia := EdTxtUsu.Text;
  if ImgTipo.SQLType = stIns then
  begin
    QrDupl.Close;
    QrDupl.Params[0].AsString := Referencia;
    UnDmkDAC_PF.AbreQuery(QrDupl, Dmod.MyDB);
    if MyObjects.FIC(QrDupl.RecordCount > 0, EdTxtUsu, 'Refer�ncia j� existe! Informe outro') then
      Exit;
  end;
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Numero := UMyMod.BuscaEmLivreY_Def('TintasCab', 'Numero', ImgTipo.SQLType,
    QrTintasCabNumero.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmTintasCab, PainelEdit,
    'TintasCab', Numero, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Numero, Numero);
  end;
end;

procedure TFmTintasCab.BtDesisteClick(Sender: TObject);
var
  Numero: Integer;
begin
  Numero := Geral.IMV(EdNumero.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'TintasCab', Numero);
  UMyMod.UpdUnlockY(Numero, Dmod.MyDB, 'TintasCab', 'Numero');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Numero, Dmod.MyDB, 'TintasCab', 'Numero');
end;

procedure TFmTintasCab.BtFluxoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFluxo, BtFluxo);
end;

procedure TFmTintasCab.BtItensClick(Sender: TObject);
begin
  //MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
  if DBCheck.CriaFm(TFmTintasIts, FmTintasIts, afmoNegarComAviso) then
  begin
    FmTintasIts.ShowModal;
    FmTintasIts.Destroy;
  end;
end;

procedure TFmTintasCab.BtProcessoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProcesso, BtProcesso);
end;

procedure TFmTintasCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Align := alClient;
  PainelEdit.Align  := alClient;
  //
  ConfiguraOpProc();
  //
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrListaSetores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEspessuras, Dmod.MyDB);
  //
  CriaOForm;
end;

procedure TFmTintasCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTintasCabNumero.Value, LaRegistro.Caption);
end;

procedure TFmTintasCab.SbImprimeClick(Sender: TObject);
(*
var
  Janela: Integer;
*)
begin
(*
  Janela := MyObjects.SelRadioGroup('Janela de Impress�o',
  'Selecione a Janela de Impress�o', ['Memory table', 'MySQL temp table'], 1);
  case Janela of
    0:
    begin
      if DBCheck.CriaFm(TFmTintasImp, FmTintasImp, afmoNegarComAviso) then
      begin
        FmTintasImp.FEmit := 0;
        FmTintasImp.EdTintasCab.ValueVariant := QrTintasCabNumero.Value;
        FmTintasImp.CBTintasCab.KeyValue     := QrTintasCabNumero.Value;
        FmTintasImp.EdAreaM2.ValueVariant    := QrTintasCabAreaM2.Value;
        FmTintasImp.EdQtde.ValueVariant      := QrTintasCabQtde.Value;
        FmTintasImp.ShowModal;
        FmTintasImp.Destroy;
      end;
    end;
    1:
    begin
*)
      VAR_CADASTRO := 0;
      //
      if DBCheck.CriaFm(TFmFormulasImpFI, FmFormulasImpFI, afmoNegarComAviso) then
      begin
        FmFormulasImpFI.DefineBaixa(False);
        //
        FmFormulasImpFI.EdTintasCab.ValueVariant := QrTintasCabNumero.Value;
        FmFormulasImpFI.CBTintasCab.KeyValue     := QrTintasCabNumero.Value;
        FmFormulasImpFI.EdAreaM2.ValueVariant    := QrTintasCabAreaM2.Value;
        FmFormulasImpFI.EdPecas.ValueVariant     := QrTintasCabQtde.Value;
        FmFormulasImpFI.ShowModal;
        FmFormulasImpFI.Destroy;
      end;
      //
      if VAR_CADASTRO <> 0 then
        LocCod(VAR_CADASTRO, VAR_CADASTRO);
(*
    end;
  end;
*)
end;

procedure TFmTintasCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTintasCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.TxtUsu(QrTintasCabTxtUsu.Value, LaRegistro.Caption);
end;

procedure TFmTintasCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FOpProc.Free;
  //
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTintasCab.QrTintasCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTintasCab.QrTintasCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTintasTin(0);
  ReopenTintasFlu(0);
end;

procedure TFmTintasCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTintasCab.SbQueryClick(Sender: TObject);
var
  NumeroAtu: Integer;
begin
  NumeroAtu := QrTintasCabNumero.Value;
  //
  Application.CreateForm(TFmTintasPesq, FmTintasPesq);
  FmTintasPesq.ShowModal;
  if FmTintasPesq.FPesq <> 0 then
    LocCod(FmTintasPesq.FPesq, FmTintasPesq.FPesq)
  else if NumeroAtu <> 0 then
    LocCod(NumeroAtu, NumeroAtu)
  else
    Va(vpLast);
  //
  FmTintasPesq.Destroy;
  //
  //
(*
  LocCod(QrTintasCabNumero.Value,
  CuringaLoc.CriaForm(CO_NUMERO, CO_NOME, 'TintasCab', Dmod.MyDB, CO_VAZIO));
*)
end;

procedure TFmTintasCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTintasCab.Incluinovareceita1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrTintasCab, [PainelDados],
  [PainelEdita], EdTxtUsu, ImgTipo, 'tintascab');
end;

procedure TFmTintasCab.Incluinovoitemdefluxo1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmTintasFlu, FmTintasFlu, afmoNegarComAviso,
    QrTintasFlu, stIns);
end;

procedure TFmTintasCab.Incluinovoprocesso1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmTintasTin, FmTintasTin, afmoNegarComAviso,
    QrTintasTin, stIns);
end;

procedure TFmTintasCab.QrTintasCabBeforeClose(DataSet: TDataSet);
begin
  QrTintasTin.Close;
  QrTintasFlu.Close;
end;

procedure TFmTintasCab.QrTintasCabBeforeOpen(DataSet: TDataSet);
begin
  QrTintasCabNumero.DisplayFormat := FFormatFloat;
end;

procedure TFmTintasCab.QrTintasFluCalcFields(DataSet: TDataSet);
begin
  if QrTintasFluInfoCargM2.Value <> 0 then
    QrTintasFluInfoCargM2_TXT.Value := Geral.FFF(QrTintasFluInfoCargM2.Value, 3)
  else
    QrTintasFluInfoCargM2_TXT.Value := '';
  //
  if QrTintasFluGramasM2.Value <> 0 then
    QrTintasFluGramasM2_TXT.Value := Geral.FFF(QrTintasFluGramasM2.Value, 3)
  else
    QrTintasFluGramasM2_TXT.Value := '';
  //
  if QrTintasFluTintasTin.Value <> 0 then
    QrTintasFluTintasTin_TXT.Value := Geral.FF0(QrTintasFluTintasTin.Value)
  else
    QrTintasFluTintasTin_TXT.Value := '';
end;

procedure TFmTintasCab.QrTintasTinAfterOpen(DataSet: TDataSet);
begin
  BtItens.Enabled := QrTintasTin.RecordCount > 0;
end;

procedure TFmTintasCab.QrTintasTinAfterScroll(DataSet: TDataSet);
begin
  ReopenTintasIts(0);
end;

procedure TFmTintasCab.QrTintasTinBeforeClose(DataSet: TDataSet);
begin
  QrTintasIts.Close;
  BtItens.Enabled := False;
end;

procedure TFmTintasCab.ReopenTintasTin(Codigo: Integer);
begin
  QrTintasTin.Close;
  QrTintasTin.Params[0].AsInteger := QrTintasCabNumero.Value;
  UnDmkDAC_PF.AbreQuery(QrTintasTin, Dmod.MyDB);
  //
  if Codigo <> 0 then
    QrTintasTin.Locate('Codigo', Codigo, []);
end;

procedure TFmTintasCab.Reordenafluxo1Click(Sender: TObject);
begin
  if (QrTintasFlu.State <> dsInactive) and(QrTintasFlu.RecordCount > 0) then
  begin
    UReordena.ReordenaItens(QrTintasFlu, Dmod.QrUpd, 'tintasflu',
      'Ordem', 'Codigo', 'DESCRI_TXT', '', '', '', nil);
    //
    ReopenTintasFlu(0);
  end;
end;

procedure TFmTintasCab.Reordenaprocessos1Click(Sender: TObject);
begin
  if (QrTintasTin.State <> dsInactive) and(QrTintasTin.RecordCount > 0) then
  begin
    UReordena.ReordenaItens(QrTintasTin, Dmod.QrUpd, 'tintastin',
      'Ordem', 'Codigo', 'Nome', '', '', '', nil);
    //
    ReopenTintasTin(0);
  end;
end;

procedure TFmTintasCab.ReopenTintasIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTintasIts, Dmod.MyDB, [
    'SELECT ',
    'IF((pq_.Ativo = 0 OR pq_.GGXNiv2 = 0), "'+ CO_Forml_Status_Prod_Irregular +'", "'+ CO_Forml_Status_Prod_Ok +'") Status_TXT, ',
    'pq_.Nome NOMEPQ, tii.* ',
    'FROM tintasits tii ',
    'LEFT JOIN pq pq_ ON pq_.Codigo=tii.Produto ',
    'WHERE tii.Codigo=' + Geral.FF0(QrTintasTinCodigo.Value),
    'ORDER BY tii.Ordem ',
    '']);
  //
  if Controle <> 0 then
    QrTintasIts.Locate('Controle', Controle, []);
end;

procedure TFmTintasCab.ReopenTintasFlu(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTintasFlu, Dmod.MyDB, [
    'SELECT tin.Nome NOMEPROCESSO, ',
    'CASE OpProc ',
    '  WHEN 1 THEN "' + FOpProc[1] + '" ',
    '  WHEN 2 THEN "' + FOpProc[2] + '" ',
    '  ELSE "' + FOpProc[0] + '" END OpProcStr, ',
    'CONCAT( ',
    'IF(flu.Nome <> "", flu.Nome, ""), ',
    '" [", ',
    'IF(flu.Descri <> "", flu.Descri, ""), ',
    '"] ", ',
    'IF(tin.Nome <> "", tin.Nome, "")) DESCRI_TXT, ',
    'flu.* ',
    'FROM tintasflu flu ',
    'LEFT JOIN tintastin tin ON tin.Codigo=flu.TintasTin ',
    'WHERE flu.Numero=' + Geral.FF0(QrTintasCabNumero.Value),
    'ORDER BY flu.Ordem ',
    '']);
  //
  if Codigo <> 0 then
    QrTintasFlu.Locate('Codigo', Codigo, []);
end;

end.

