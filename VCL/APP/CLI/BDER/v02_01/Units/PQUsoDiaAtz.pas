unit PQUsoDiaAtz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker,
  dmkCheckGroup;

type
  TFmPQUsoDiaAtz = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPQUso: TMySQLQuery;
    Panel2: TPanel;
    Panel3: TPanel;
    Label4: TLabel;
    TPIni: TdmkEditDateTimePicker;
    Label5: TLabel;
    TPFim: TdmkEditDateTimePicker;
    PB1: TProgressBar;
    QrPQUsoInsumo: TIntegerField;
    QrPQUsoPeso: TFloatField;
    CGDiasSem: TdmkCheckGroup;
    RGFonte: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CGDiasSemClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmPQUsoDiaAtz: TFmPQUsoDiaAtz;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule, UnDmkProcFunc;

{$R *.DFM}

procedure TFmPQUsoDiaAtz.BtOKClick(Sender: TObject);
var
  DataIni, DataFim: TDateTime;
  SQLType: TSQLType;
  DiasUteis: Integer;
begin
  DataIni := Trunc(TPIni.Date);
  DataFim := Trunc(TPFim.Date);

  DiasUteis := UMyMod.DiasUteis2(DataIni, DataFim, CGDiasSem.Value);
  if DiasUteis = 0 then
    DiasUteis := 1;
  //
  case RGFonte.ItemIndex of
    0:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQUso, Dmod.MyDB, [
      'SELECT Insumo, (SUM(-Peso) / ' + Geral.FF0(DiasUteis) + ') Peso ',
      'FROM pqx ',
      'WHERE Peso <0 ',
      'AND Tipo <> 0 ',
      dmkPF.SQL_Periodo('AND DataX ', DataIni,DataFim, True, True),
      'GROUP BY Insumo ',
      '']);
      Dmod.MyDB.Execute('UPDATE pq SET KgUsoDd=0');
      //
      PB1.Position := 0;
      PB1.Max := QrPQUso.RecordCount;
      QrPQUso.First;
      while not QrPQUso.Eof do
      begin
        MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
        Dmod.MyDB.Execute('UPDATE pq SET KgUsoDd = ' +
        Geral.FFT_Dot(QrPQUsoPeso.Value, 3, siNegativo) +
        ' WHERE Codigo = ' + Geral.FF0(QrPQUsoInsumo.Value));
        //
        QrPQUso.Next;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Atualiza��o finalizada! ' + Geral.FF0(QrPQUso.RecordCount) + ' itens! Dias �teis: ' + Geral.FF0(DiasUteis));
    end;
    1:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQUso, Dmod.MyDB, [
      'SELECT Insumo, ',
      '(SUM(PesoUso) / ' + Geral.FF0(DiasUteis) + ') Peso    ',
      'FROM pqurec ',
      'WHERE Nivel=2 ',
      'GROUP BY Insumo ',
      '']);
      Dmod.MyDB.Execute('UPDATE pq SET KgUsoDd=0');
      //
      PB1.Position := 0;
      PB1.Max := QrPQUso.RecordCount;
      QrPQUso.First;
      while not QrPQUso.Eof do
      begin
        MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
        Dmod.MyDB.Execute('UPDATE pq SET KgUsoDd = ' +
        Geral.FFT_Dot(QrPQUsoPeso.Value, 3, siNegativo) +
        ' WHERE Codigo = ' + Geral.FF0(QrPQUsoInsumo.Value));
        //
        QrPQUso.Next;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Atualiza��o finalizada! ' + Geral.FF0(QrPQUso.RecordCount) + ' itens! Dias �teis: ' + Geral.FF0(DiasUteis));
    end;
    else Geral.MB_Erro('Fonte n�o implementada!');
  end;
end;

procedure TFmPQUsoDiaAtz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQUsoDiaAtz.CGDiasSemClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('DiasTrabSem', Application.Title, CGDiasSem.Value, ktInteger);
end;

procedure TFmPQUsoDiaAtz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQUsoDiaAtz.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TPIni.Date := Date - 30;
  TPFim.Date := Date;
end;

procedure TFmPQUsoDiaAtz.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQUsoDiaAtz.FormShow(Sender: TObject);
begin
  CGDiasSem.Value := Geral.ReadAppKeyCU('DiasTrabSem', Application.Title, ktInteger, 62);
end;

end.
