object FmBlueDerm_Dmk: TFmBlueDerm_Dmk
  Left = 399
  Top = 261
  Caption = 'Blue Derm'
  ClientHeight = 374
  ClientWidth = 577
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 577
    Height = 243
    Align = alClient
    Caption = ' Defini'#231#245'es do perfil: '
    TabOrder = 0
    object Label3: TLabel
      Left = 14
      Top = 24
      Width = 43
      Height = 13
      Caption = 'Terminal:'
    end
    object LaLogin: TLabel
      Left = 85
      Top = 24
      Width = 29
      Height = 13
      Caption = 'Login:'
    end
    object LaSenha: TLabel
      Left = 299
      Top = 24
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object LaEmpresa: TLabel
      Left = 513
      Top = 21
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object LaSenhas: TLabel
      Left = 121
      Top = 63
      Width = 80
      Height = 13
      Cursor = crHandPoint
      Caption = 'Gerenciar Senha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = LaSenhasClick
      OnMouseEnter = LaSenhasMouseEnter
      OnMouseLeave = LaSenhasMouseLeave
    end
    object LaConexao: TLabel
      Left = 328
      Top = 63
      Width = 96
      Height = 13
      Cursor = crHandPoint
      Caption = 'Gerenciar Conex'#245'es'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = LaConexaoClick
      OnMouseEnter = LaConexaoMouseEnter
      OnMouseLeave = LaConexaoMouseLeave
    end
    object EdTerminal: TEdit
      Left = 14
      Top = 40
      Width = 65
      Height = 22
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
      OnKeyDown = EdLoginKeyDown
    end
    object EdLogin: TEdit
      Left = 85
      Top = 40
      Width = 208
      Height = 22
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 1
      OnKeyDown = EdLoginKeyDown
    end
    object EdSenha: TEdit
      Left = 299
      Top = 40
      Width = 208
      Height = 22
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 2
      OnExit = EdSenhaExit
      OnKeyDown = EdSenhaKeyDown
    end
    object EdEmpresa: TEdit
      Left = 513
      Top = 40
      Width = 46
      Height = 22
      AutoSize = False
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 3
      OnKeyDown = EdSenhaKeyDown
    end
    object MeAvisos: TMemo
      Left = 12
      Top = 80
      Width = 548
      Height = 145
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      WordWrap = False
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 243
    Width = 577
    Height = 61
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 573
      Height = 44
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 12
        Top = 24
        Width = 546
        Height = 17
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 304
    Width = 577
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 431
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 9
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 429
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object BtEntra: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        ParentDoubleBuffered = True
        TabOrder = 0
        OnClick = BtEntraClick
      end
      object BtConecta: TBitBtn
        Tag = 1000104
        Left = 280
        Top = 3
        Width = 143
        Height = 40
        Caption = '&Tenta conectar'
        NumGlyphs = 2
        ParentDoubleBuffered = True
        TabOrder = 1
        Visible = False
        OnClick = BtConectaClick
      end
    end
  end
end
