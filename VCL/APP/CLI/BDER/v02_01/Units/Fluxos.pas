unit Fluxos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UnDmkProcFunc, dmkLabel, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmFluxos = class(TForm)
    PainelDados: TPanel;
    DsFluxos: TDataSource;
    QrFluxos: TmySQLQuery;
    QrFluxosLk: TIntegerField;
    QrFluxosDataCad: TDateField;
    QrFluxosDataAlt: TDateField;
    QrFluxosUserCad: TIntegerField;
    QrFluxosUserAlt: TIntegerField;
    QrFluxosCodigo: TSmallintField;
    QrFluxosNome: TWideStringField;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    PMInclui: TPopupMenu;
    Novofluxodeproduo1: TMenuItem;
    Itemaofluxoatual1: TMenuItem;
    PainelItens: TPanel;
    Panel1: TPanel;
    Label3: TLabel;
    EdOperacao: TdmkEditCB;
    CBOperacao: TdmkDBLookupComboBox;
    QrOperacoes: TmySQLQuery;
    DsOperacoes: TDataSource;
    QrOperacoesCodigo: TIntegerField;
    QrOperacoesNome: TWideStringField;
    Label4: TLabel;
    EdAcao1: TdmkEdit;
    EdAcao2: TdmkEdit;
    Label5: TLabel;
    EdAcao3: TdmkEdit;
    Label6: TLabel;
    EdAcao4: TdmkEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdOrdem: TdmkEdit;
    DsFluxosIts: TDataSource;
    QrFluxosIts: TmySQLQuery;
    QrFluxosItsCodigo: TIntegerField;
    QrFluxosItsControle: TIntegerField;
    QrFluxosItsOrdem: TIntegerField;
    QrFluxosItsOperacao: TIntegerField;
    QrFluxosItsAcao1: TWideStringField;
    QrFluxosItsAcao2: TWideStringField;
    QrFluxosItsAcao3: TWideStringField;
    QrFluxosItsAcao4: TWideStringField;
    QrFluxosItsLk: TIntegerField;
    QrFluxosItsDataCad: TDateField;
    QrFluxosItsDataAlt: TDateField;
    QrFluxosItsUserCad: TIntegerField;
    QrFluxosItsUserAlt: TIntegerField;
    DBGrid1: TDBGrid;
    Panel4: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit1: TDBEdit;
    DBEdit02: TDBEdit;
    QrFluxosItsNOMEOPERACAO: TWideStringField;
    QrFluxosItsSEQ: TIntegerField;
    PMAltera: TPopupMenu;
    Alterafluxo1: TMenuItem;
    Alteraoperao1: TMenuItem;
    QrOrdena: TmySQLQuery;
    QrOrdenaCodigo: TIntegerField;
    QrOrdenaControle: TIntegerField;
    QrOrdenaOrdem: TIntegerField;
    QrOrdenaOperacao: TIntegerField;
    QrOrdenaAcao1: TWideStringField;
    QrOrdenaAcao2: TWideStringField;
    QrOrdenaAcao3: TWideStringField;
    QrOrdenaAcao4: TWideStringField;
    QrOrdenaLk: TIntegerField;
    QrOrdenaDataCad: TDateField;
    QrOrdenaDataAlt: TDateField;
    QrOrdenaUserCad: TIntegerField;
    QrOrdenaUserAlt: TIntegerField;
    PMExclui: TPopupMenu;
    Excluifluxo1: TMenuItem;
    Excluioperao1: TMenuItem;
    QrOrdenaNOMEOPERACAO: TWideStringField;
    QrOrdenaSEQ: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtReordena: TBitBtn;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Panel8: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel9: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFluxosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrFluxosAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFluxosBeforeOpen(DataSet: TDataSet);
    procedure Novofluxodeproduo1Click(Sender: TObject);
    procedure Itemaofluxoatual1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure EdOperacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBOperacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrFluxosItsCalcFields(DataSet: TDataSet);
    procedure Alterafluxo1Click(Sender: TObject);
    procedure Alteraoperao1Click(Sender: TObject);
    procedure Excluioperao1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure BtReordenaClick(Sender: TObject);
    procedure QrOrdenaCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure CadastraOperacao;
    procedure ReopenFluxosIts(Controle: Integer);
    procedure Reordena(Pula: Boolean; Posicao: Integer);
    procedure ReorganizaItens;
  public
    { Public declarations }
    FSeq: Integer;
  end;

var
  FmFluxos: TFmFluxos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Principal, MyDBGridReorder;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFluxos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFluxos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFluxosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFluxos.DefParams;
begin
  VAR_GOTOTABELA := 'Fluxos';
  VAR_GOTOMYSQLTABLE := QrFluxos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM fluxos');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmFluxos.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PainelItens.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      if SQLType = stIns then
      begin
        EdCodigo.Text := '';
        EdNome.Text   := '';
      end else begin
        EdCodigo.Text := IntToStr(QrFluxosCodigo.Value);
        EdNome.Text   := QrFluxosNome.Value;
      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      PainelItens.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdOrdem.Text := IntToStr(QrFluxosItsOrdem.Value + 1);
        EdOperacao.Text := '';
        CBOperacao.KeyValue := Null;
        EdAcao1.Text := '';
        EdAcao2.Text := '';
        EdAcao3.Text := '';
        EdAcao4.Text := '';
      end else begin
        EdOrdem.Text := IntToStr(QrFluxosItsOrdem.Value);
        EdOperacao.Text := IntToStr(QrFluxosItsOperacao.Value);
        CBOperacao.KeyValue := QrFluxosItsOperacao.Value;
        EdAcao1.Text := QrFluxosItsAcao1.Value;
        EdAcao2.Text := QrFluxosItsAcao2.Value;
        EdAcao3.Text := QrFluxosItsAcao3.Value;
        EdAcao4.Text := QrFluxosItsAcao4.Value;
      end;
      EdOperacao.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmFluxos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFluxos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFluxos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFluxos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFluxos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFluxos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFluxos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFluxos.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmFluxos.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmFluxos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFluxosCodigo.Value;
  Close;
end;

procedure TFmFluxos.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO fluxos SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Fluxos', 'Fluxos', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE fluxos SET ');
    Codigo := QrFluxosCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Nome=:P0, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  //
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[03].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Fluxos', 'Codigo');
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo,Codigo);
  if FSeq = 1 then Close;
end;

procedure TFmFluxos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Fluxos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Fluxos', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Fluxos', 'Codigo');
end;

procedure TFmFluxos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FSeq := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  DBGrid1.Align     := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrOperacoes, Dmod.MyDB);
end;

procedure TFmFluxos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFluxosCodigo.Value,LaRegistro.Caption);
end;

procedure TFmFluxos.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmFluxos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFluxos.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmFluxos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrFluxosCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFluxos.QrFluxosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFluxos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Fluxos', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, stIns, 0);
end;

procedure TFmFluxos.QrFluxosAfterScroll(DataSet: TDataSet);
begin
  //BtAltera.Enabled := GOTOy.BtEnabled(QrFluxosCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrFluxosCodigo.Value, False);
  //
  ReopenFluxosIts(0);
end;

procedure TFmFluxos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFluxosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Fluxos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFluxos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFluxos.QrFluxosBeforeOpen(DataSet: TDataSet);
begin
  QrFluxosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFluxos.Novofluxodeproduo1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmFluxos.Itemaofluxoatual1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmFluxos.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmFluxos.EdOperacaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then CadastraOperacao;
end;

procedure TFmFluxos.CBOperacaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then CadastraOperacao;
end;

procedure TFmFluxos.CadastraOperacao;
begin
  FmPrincipal.CadastraOperacoes;
  QrOperacoes.Close;
  UnDmkDAC_PF.AbreQuery(QrOperacoes, Dmod.MyDB);
  EdOperacao.Text := IntToStr(VAR_OPERACAO);
  CBOperacao.KeyValue := VAR_OPERACAO;
end;

procedure TFmFluxos.BitBtn1Click(Sender: TObject);
var
  Controle, Ordem, Operacao: Integer;
begin
  Ordem := Geral.IMV(EdOrdem.Text);
  //
  Operacao := Geral.IMV(EdOperacao.Text);
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fluxosits SET Ordem=Ordem+1');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Ordem>=:P1');
    Dmod.QrUpd.Params[0].AsInteger := QrFluxosCodigo.Value;
    Dmod.QrUpd.Params[1].AsInteger := Ordem;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO fluxosits SET ');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'FluxosIts', 'FluxosIts', 'Controle');
  end else begin
    Reordena(True, Ordem);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fluxosits SET ');
    Controle := QrFluxosItsControle.Value;
  end;
  Dmod.QrUpd.SQL.Add('Ordem=:P0, Operacao=:P1, Acao1=:P2, Acao2=:P3, ');
  Dmod.QrUpd.SQL.Add('Acao3=:P4, Acao4=:P5 ');
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb ')
  else
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa AND Controle=:Pb');
  Dmod.QrUpd.Params[00].AsInteger := Ordem;
  Dmod.QrUpd.Params[01].AsInteger := Operacao;
  Dmod.QrUpd.Params[02].AsString  := EdAcao1.Text;
  Dmod.QrUpd.Params[03].AsString  := EdAcao2.Text;
  Dmod.QrUpd.Params[04].AsString  := EdAcao3.Text;
  Dmod.QrUpd.Params[05].AsString  := EdAcao4.Text;
  Dmod.QrUpd.Params[06].AsInteger := QrFluxosCodigo.Value;
  Dmod.QrUpd.Params[07].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  Reordena(False, 0);
  ReopenFluxosIts(Controle);
  //
  MostraEdicao(0, stLok, 0);
end;

procedure TFmFluxos.ReopenFluxosIts(Controle: Integer);
begin
  QrFluxosIts.Close;
  QrFluxosIts.Params[0].AsInteger := QrFluxosCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrFluxosIts, Dmod.MyDB);
  //
  if Controle > 0 then QrFluxosIts.Locate('Controle', Controle, []);
end;

procedure TFmFluxos.QrFluxosItsCalcFields(DataSet: TDataSet);
begin
  QrFluxosItsSEQ.Value := QrFluxosIts.RecNo;
end;

procedure TFmFluxos.Alterafluxo1Click(Sender: TObject);
var
  Fluxos : Integer;
begin
  Fluxos := QrFluxosCodigo.Value;
  if not UMyMod.SelLockY(Fluxos, Dmod.MyDB, 'Fluxos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Fluxos, Dmod.MyDB, 'Fluxos', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmFluxos.Alteraoperao1Click(Sender: TObject);
begin
  MostraEdicao(2, stUpd, 0);
end;

procedure TFmFluxos.Reordena(Pula: Boolean; Posicao: Integer);
var
  Ordem, Real: Integer;
begin
  QrOrdena.Close;
  QrOrdena.Params[0].AsInteger := QrFluxosCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrOrdena, Dmod.MyDB);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE fluxosits SET Ordem=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 AND Controle=:P2');
  //
  Ordem := 0;
  while not QrOrdena.Eof do
  begin
    Ordem := Ordem + 1;
    Real := Ordem;
    if Pula then if Real >= Posicao then Real := Real + 1;
    Dmod.QrUpd.Params[00].AsInteger := Real;
    Dmod.QrUpd.Params[01].AsInteger := QrFluxosCodigo.Value;
    Dmod.QrUpd.Params[02].AsInteger := QrOrdenaControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrOrdena.Next;
  end;
end;

procedure TFmFluxos.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmFluxos.PMExcluiPopup(Sender: TObject);
begin
  if QrFluxosIts.RecordCount = 0 then Excluioperao1.Enabled := False
  else Excluioperao1.Enabled := True;
end;

procedure TFmFluxos.Excluioperao1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if Application.MessageBox('Confirma a retirada da opera��o selecionada?',
  'Pergunta de Exclus�o', MB_YESNO+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fluxosits WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrFluxosItsControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Reordena(False, 0);
    Controle := UmyMod.ProximoRegistro(
      QrFluxosIts, 'Controle', QrFluxosItsControle.Value);
    ReopenFluxosIts(Controle);
  end;
end;

procedure TFmFluxos.BtReordenaClick(Sender: TObject);
begin
  ReorganizaItens;
end;

procedure TFmFluxos.ReorganizaItens;
begin
  QrOrdena.Close;
  QrOrdena.Params[0].AsInteger := QrFluxosCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrOrdena, Dmod.MyDB);
  Application.CreateForm(TFmMyDBGridReorder, FmMyDBGridReorder);
  FmMyDBGridReorder.FMySQLQuery     := QrOrdena;
  FmMyDBGridReorder.FTabelaAOrdenar := 'FluxosIts';
  FmMyDBGridReorder.FCampoAOrdenar  := 'Ordem';
  FmMyDBGridReorder.FCampoControle  := 'Controle';
  FmMyDBGridReorder.FSQLExtra1      := '';//'AND Codigo = '+IntToStr(QrCultosCodigo.Value);
  FmMyDBGridReorder.FDBGrid         := DBGrid1;
  FmMyDBGridReorder.FTrocavel1      := '';
  FmMyDBGridReorder.FTrocador1      := '';
  FmMyDBGridReorder.ConfiguraGrade;
  FmMyDBGridReorder.BtConfirma.Visible := True;
  FmMyDBGridReorder.Grid.Options    := FmMyDBGridReorder.Grid.Options + [goRowMoving];
  FmMyDBGridReorder.ShowModal;
  FmMyDBGridReorder.Destroy;
  Reordena(False, 0);
  ReopenFluxosIts(QrFluxosItsControle.Value);
end;

procedure TFmFluxos.QrOrdenaCalcFields(DataSet: TDataSet);
begin
  QrOrdenaSEQ.Value := QrOrdena.RecNo;
end;

end.

