object FmPQB3AdSel: TFmPQB3AdSel
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-047 :: Item de Tabela de Presta'#231#227'o de Servi'#231'o'
  ClientHeight = 444
  ClientWidth = 616
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 616
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 568
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Picture.Data = {
          07544269746D6170360C0000424D360C00000000000036000000280000002000
          0000200000000100180000000000000C00000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000BD946BBD7B39BD946B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000BD7B42DE8C29EF9C42DE8C29BD844200000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000BD7B4ADE8C29FFBD63FFB54AFFC66BDE8C31C6844A00000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000BD7B42DE8C29FFC663FFAD39FFA521FFB54AFFCE84DE9439DEB58C00
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00BD7B4ADE8C29FFC66BFFAD42FFA531FFAD39FFB54AFFC684EFAD63D6945200
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000BD7B
          4ADE8C31FFC673FFB54AFFAD31FFAD42FFB552FFC673FFD69CDE9442E7BD9400
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000BD844ADE8C
          31FFC67BFFB552FFAD42FFAD4AFFB55AFFC67BFFD6A5DE944ADEA56300000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000BD8442DE8C31FFCE
          84FFBD63FFB54AFFB552FFBD63FFC684FFD6A5DE944ADEA56300000000000000
          0000000000000000000000000000000000000000000000000000ADADA59C9C9C
          9C9C9C9C9C9CA59C9C9C9C9C9C9C9CA5A5A5B5B5B5AD7342DE8C39FFCE8CFFBD
          6BFFB55AFFBD63FFBD6BFFCE8CFFDEADDE944ADEA56300000000000000000000
          0000000000000000000000000000000000000000ADA5ADADA5ADBDBDBDCECECE
          DEDEDEE7E7E7E7E7E7DEDEDECECECEBDBDBDADA5AD9C7B5ADEB57BF7B56BFFB5
          63FFBD6BFFC673FFCE94FFDEB5DE944ADEA56B00000000000000000000000000
          0000000000000000000000000000000000A59CA5BDBDBDD6D6D6EFE7E7E7E7E7
          DEDEDEDEDEDEDEDEDEDEDEDEE7E7E7E7E7E7D6D6DEBDBDBDADADADCE9C63F7BD
          6BFFC684FFD69CFFDEB5DE944ADEA56300000000000000000000000000000000
          0000000000000000000000000000BDBDBDE7E7E7EFEFEFEFEFEFEFEFEFEFEFEF
          EFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFC6C6C6ADADADC69C
          84E7BDA5E7CEBDCE8C63CE947300000000000000000000000000000000000000
          0000000000000000000000A59CA5C6C6C6EFEFEFDEDEDEDEDEDEDED6C6E7BD7B
          F7AD39FF9C10FF9C10F7AD39EFBD7BDED6C6DEDEDEDEDEDEE7EFEFCEC6CEADAD
          ADE7CEADDE9452DEA56B00000000000000000000000000000000000000000000
          0000000000000000BDBDBDBDBDBDEFEFEFEFEFEFDEDEDEEFC684E79429E7A56B
          EFBD9CEFC684EFC684EFC684E7AD6BE79429EFC684DEDEDEEFEFEFEFEFEFADAD
          AD9C8473C68C7300000000000000000000000000000000000000000000000000
          0000000000000000ADADADDEDEDEE7E7E7E7DEDEE7CEA5FFAD31FFCE94FFE7CE
          FFDEBDFFD6ADFFD6ADFFDEBDFFE7CEFFCE94FFAD31E7CEA5DEDEDEE7E7E7DEDE
          DEADADAD00000000000000000000000000000000000000000000000000000000
          0000000000BDBDBDADADADEFEFEFDEDEDEF7EFDEE79431EFC684FFE7CEFFD6AD
          FFDEB5FFD6ADFFD6ADFFD6ADFFD6ADFFE7CEEFC684E79431DEDEDEEFEFEFEFEF
          EFADADADADADAD00000000000000000000000000000000000000000000000000
          0000000000ADADADD6D6D6EFEFEFE7E7E7F7C684FFBD63FFEFDEFFDEBDFFDEB5
          FFDEB5FFDEB5FFDEB5FFDEB5FFDEB5FFDEBDFFEFDEFFBD63EFC684E7E7E7EFEF
          EFD6D6D6B5B5B500000000000000000000000000000000000000000000000000
          0000000000ADADADE7E7E7EFEFEFEFEFEFE79C52FFCE94FFEFD6FFDEB5FFDEB5
          FFDEB5FFDEB5FFDEB5FFDEB5FFDEB5FFDEB5FFEFD6EFC684E79C52EFEFEFF7F7
          F7EFEFEFADADAD00000000000000000000000000000000000000000000000000
          0000000000ADADADF7F7F7EFEFEFE7EFE7F79C10FFEFD6FFEFD6FFE7D6FFE7CE
          FFE7CEFFE7CEFFE7CEFFE7CEFFE7CEFFE7D6FFEFD6FFEFD6FF9C10EFEFEFF7F7
          F7EFEFEFA5A5A500000000000000000000000000000000000000000000000000
          0000000000ADADADE7E7E7EFEFEFEFEFEFE78C29FFF7E7FFEFD6FFEFD6FFEFD6
          FFEFD6FFEFD6FFEFD6FFEFD6FFEFD6FFEFD6FFEFD6FFF7E7E78C29EFEFEFF7F7
          F7EFEFEFADADAD00000000000000000000000000000000000000000000000000
          0000000000ADADADE7E7E7F7F7F7EFEFEFF7B542FFDEB5FFF7EFFFF7E7FFEFE7
          FFEFE7FFEFE7F7EFE7FFEFE7FFF7E7FFF7E7FFF7EFFFDEB5FFAD42EFF7EFF7F7
          F7E7E7E7ADADAD00000000000000000000000000000000000000000000000000
          0000000000BDBDBDDEDED6F7F7F7EFF7F7F7CE8CFFC66BFFFFF7FFF7EFFFF7EF
          FFF7EFFFF7EFFFF7EFFFF7EFFFF7EFFFF7EFFFFFF7FFC66BFFCE8CEFF7F7F7F7
          F7DED6D6BDBDBD00000000000000000000000000000000000000000000000000
          0000000000D6D6D6C6C6C6F7F7F7F7F7F7F7EFDEFFA521FFDEADFFFFFFFFFFF7
          FFF7F7FFFFF7FFFFF7FFFFF7FFFFF7FFFFFFFFDEB5FFA521F7EFD6F7F7F7FFFF
          F7C6C6C6D6D6D600000000000000000000000000000000000000000000000000
          0000000000000000ADADADE7E7E7FFFFFFF7F7F7F7DEB5FFAD39FFDEADFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEB5FFAD39F7DEB5F7F7F7FFF7FFE7E7
          E7ADADAD00000000000000000000000000000000000000000000000000000000
          0000000000000000D6D6D6C6C6C6FFFFFFFFF7FFF7F7F7F7DEB5FFA518FFC673
          FFE7B5FFF7E7FFF7E7FFDEB5FFC66BFFA518F7DEB5FFF7FFF7FFF7FFFFFFC6C6
          C6D6D6D600000000000000000000000000000000000000000000000000000000
          0000000000000000000000A5A5A5D6CED6FFFFFFF7FFFFFFFFFFFFEFDEFFCE8C
          FFB542FF9C18FF9C18FFB54AFFCE8CFFEFDEFFFFFFFFFFF7F7FFFFD6D6D69C9C
          A500000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000A5A5A5CED6D6FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D6D6A5A5A50000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000009C9C9CC6C6C6E7E7E7FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE7E7C6C6C6A59CA50000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000D6D6D6ADADADC6C6C6D6DEDE
          EFEFEFFFF7FFF7F7F7EFEFEFDEDEDEC6C6C6ADADADD6D6D60000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000DEDEDEC6C6C6
          B5ADB5A5A5A5A5A5A5B5B5B5C6C6C6DED6DE0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
        Transparent = True
        SQLType = stPsq
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 520
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 204
        Height = 32
        Caption = #218'ltimas entradas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 204
        Height = 32
        Caption = #218'ltimas entradas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 204
        Height = 32
        Caption = #218'ltimas entradas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 330
    Width = 616
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 612
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 374
    Width = 616
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 470
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 468
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 616
    Height = 282
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 812
    ExplicitHeight = 467
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 129
      Width = 616
      Height = 153
      Align = alClient
      DataSource = DsSPqx
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = dmkDBGridZTO1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'OrigemCtrl'
          Title.Caption = 'Controle'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataX'
          Title.Caption = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoUnit'
          Title.Caption = '$ Custo/uni'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Serie'
          Title.Caption = 'S'#233'rie'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NF'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'xLote'
          Title.Caption = 'Lote'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dFab'
          Title.Caption = 'Dta Fab'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dVal'
          Title.Caption = 'Dta Val'
          Width = 56
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 616
      Height = 129
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 812
      object Label1: TLabel
        Left = 16
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label2: TLabel
        Left = 16
        Top = 44
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label3: TLabel
        Left = 16
        Top = 84
        Width = 37
        Height = 13
        Caption = 'Insumo:'
      end
      object EdEmpresa: TdmkEdit
        Left = 16
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNO_Empresa: TdmkEdit
        Left = 76
        Top = 20
        Width = 400
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCliInt: TdmkEdit
        Left = 16
        Top = 60
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNO_CliInt: TdmkEdit
        Left = 76
        Top = 60
        Width = 400
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdInsumo: TdmkEdit
        Left = 16
        Top = 100
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNO_Insumo: TdmkEdit
        Left = 76
        Top = 100
        Width = 400
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65527
  end
  object QrPqx: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pqx.OrigemCtrl, pqx.DataX, pqe.Serie, '
      'pqe.NF, its.xLote, its.dFab, its.dVal,'
      'IF(its.TotalPeso=0, 0.0000, '
      '  its.TotalCusto / its.TotalPeso) CustoUnit'
      'FROM pqx'
      'LEFT JOIN pqe pqe ON pqe.Codigo=pqx.OrigemCodi'
      'LEFT JOIN pqeits its ON its.Controle=pqx.OrigemCtrl'
      'WHERE pqx.Empresa=-11'
      'AND pqx.CliOrig=140'
      'AND pqx.Insumo=883'
      'AND pqx.Tipo=10'
      'ORDER BY DataX DESC, OrigemCtrl DESC')
    Left = 52
    Top = 252
    object QrPqxOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
      Required = True
    end
    object QrPqxDataX: TDateField
      FieldName = 'DataX'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPqxSerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrPqxNF: TIntegerField
      FieldName = 'NF'
    end
    object QrPqxxLote: TWideStringField
      FieldName = 'xLote'
      Size = 120
    end
    object QrPqxCustoUnit: TFloatField
      FieldName = 'CustoUnit'
      DisplayFormat = '###,###,##0.0000'
    end
    object QrPqxdFab: TDateField
      FieldName = 'dFab'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPqxdVal: TDateField
      FieldName = 'dVal'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsSPqx: TDataSource
    DataSet = QrPqx
    Left = 52
    Top = 308
  end
end
