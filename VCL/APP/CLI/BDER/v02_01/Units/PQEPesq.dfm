object FmPQEPesq: TFmPQEPesq
  Left = 348
  Top = 185
  Caption = 'QUI-ENTRA-005 :: Pesquisas de Entrada de PQ'
  ClientHeight = 619
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 463
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 205
      Width = 1008
      Height = 258
      Align = alClient
      DataSource = DsPQE
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'CI'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLIINT'
          Title.Caption = 'Nome do Cliente Interno'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IQ'
          Title.Caption = 'ID IQ'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFORNECEDOR'
          Title.Caption = 'Nome do Fornecedor'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Insumo'
          Title.Caption = 'ID PQ'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEINSUMO'
          Title.Caption = 'Nome do Insumo'
          Width = 172
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NF'
          Title.Caption = 'NF VP'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NF_RP'
          Title.Caption = 'NF RP'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NF_CC'
          Title.Caption = 'NF CC'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID Entrada'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conhecimento'
          Title.Caption = 'Conh.Frete'
          Width = 56
          Visible = True
        end>
    end
    object PainelDados: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 205
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 368
        Top = 84
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label3: TLabel
        Left = 368
        Top = 124
        Width = 37
        Height = 13
        Caption = 'Insumo:'
      end
      object Label4: TLabel
        Left = 368
        Top = 164
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label5: TLabel
        Left = 484
        Top = 164
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object Label6: TLabel
        Left = 600
        Top = 164
        Width = 54
        Height = 13
        Caption = 'ID Entrada:'
      end
      object Label8: TLabel
        Left = 712
        Top = 164
        Width = 71
        Height = 13
        Caption = 'Conhecimento:'
      end
      object Label2: TLabel
        Left = 368
        Top = 44
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label7: TLabel
        Left = 712
        Top = 44
        Width = 34
        Height = 13
        Caption = 'NF VP:'
      end
      object Label9: TLabel
        Left = 712
        Top = 84
        Width = 35
        Height = 13
        Caption = 'NF RP:'
      end
      object Label10: TLabel
        Left = 712
        Top = 124
        Width = 34
        Height = 13
        Caption = 'NF CC:'
      end
      object LaEmpresa: TLabel
        Left = 368
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object RGOrdem2: TRadioGroup
        Left = 120
        Top = 0
        Width = 120
        Height = 205
        Align = alLeft
        Caption = ' Ordem da Pesquisa 2: '
        ItemIndex = 1
        Items.Strings = (
          'Cliente int.'
          'Fornecedor'
          'Insumo'
          'NF VP'
          'NF RP'
          'NF CC'
          'Data'
          'Lan'#231'to'
          'Conhecim.')
        TabOrder = 1
        OnClick = RGOrdem1Click
      end
      object RGOrdem1: TRadioGroup
        Left = 0
        Top = 0
        Width = 120
        Height = 205
        Align = alLeft
        Caption = ' Ordem da Pesquisa1: '
        ItemIndex = 0
        Items.Strings = (
          'Cliente int.'
          'Fornecedor'
          'Insumo'
          'NF VP'
          'NF RP'
          'NF CC'
          'Data'
          'Lan'#231'to'
          'Conhecim.')
        TabOrder = 0
        OnClick = RGOrdem1Click
      end
      object CBIQ: TdmkDBLookupComboBox
        Left = 424
        Top = 100
        Width = 284
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFornecedores
        TabOrder = 6
        dmkEditCB = EdIQ
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBInsumo: TdmkDBLookupComboBox
        Left = 424
        Top = 140
        Width = 284
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsInsumos
        TabOrder = 8
        dmkEditCB = EdInsumo
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPIni: TdmkEditDateTimePicker
        Left = 368
        Top = 180
        Width = 112
        Height = 21
        Date = 37670.000000000000000000
        Time = 0.521748657396528900
        TabOrder = 9
        OnClick = TPIniClick
        OnChange = TPIniChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPFim: TdmkEditDateTimePicker
        Left = 484
        Top = 180
        Width = 112
        Height = 21
        Date = 37670.000000000000000000
        Time = 0.521748657396528900
        TabOrder = 10
        OnClick = TPFimClick
        OnChange = TPFimChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdLancto: TdmkEdit
        Left = 600
        Top = 180
        Width = 109
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdLanctoExit
      end
      object EdIQ: TdmkEditCB
        Left = 368
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdIQChange
        DBLookupComboBox = CBIQ
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdInsumo: TdmkEditCB
        Left = 368
        Top = 140
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdInsumoChange
        DBLookupComboBox = CBInsumo
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdConhecimento: TdmkEdit
        Left = 712
        Top = 180
        Width = 73
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdConhecimentoExit
      end
      object RGOrdem3: TRadioGroup
        Left = 240
        Top = 0
        Width = 120
        Height = 205
        Align = alLeft
        Caption = ' Ordem da Pesquisa 3: '
        ItemIndex = 2
        Items.Strings = (
          'Cliente int.'
          'Fornecedor'
          'Insumo'
          'NF VP'
          'NF RP'
          'NF CC'
          'Data'
          'Lan'#231'to'
          'Conhecim.')
        TabOrder = 2
        OnClick = RGOrdem1Click
      end
      object EdCI: TdmkEditCB
        Left = 368
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdIQChange
        DBLookupComboBox = CBCI
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCI: TdmkDBLookupComboBox
        Left = 424
        Top = 60
        Width = 284
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCI
        TabOrder = 4
        dmkEditCB = EdCI
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdNF: TdmkEdit
        Left = 712
        Top = 60
        Width = 73
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdNFExit
      end
      object EdNF_RP: TdmkEdit
        Left = 712
        Top = 100
        Width = 73
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdNFExit
      end
      object EdNF_CC: TdmkEdit
        Left = 712
        Top = 140
        Width = 73
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdNFExit
      end
      object EdEmpresa: TdmkEditCB
        Left = 368
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 424
        Top = 20
        Width = 362
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 17
        TabStop = False
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 350
        Height = 32
        Caption = 'Pesquisas de Entrada de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 350
        Height = 32
        Caption = 'Pesquisas de Entrada de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 350
        Height = 32
        Caption = 'Pesquisas de Entrada de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 511
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 564
        Height = 17
        Caption = 
          'VP: Venda Proced'#234'ncia.          RP: Remessa Proced'#234'ncia.        ' +
          '  CC: Cobertura Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 555
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso3: TLabel
        Left = 324
        Top = 16
        Width = 9
        Height = 13
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object PnSaiDesis: TPanel
        Left = 859
        Top = 0
        Width = 145
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Imprime'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtImprimeClick
      end
    end
  end
  object QrPQE: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ei.*, fo.Nome NOMEFORNECEDOR,'
      'pq.Nome NOMEINSUMO, pe.*'
      'FROM pqe pe, PQEIts ei,'
      'entidades fo, PQ pq'
      'WHERE pe.Data BETWEEN '#39'2002-11-20'#39' AND '#39'2003-02-18'#39
      'AND ei.Codigo=pe.Codigo'
      'AND fo.Codigo=pe.IQ'
      'AND pq.Codigo=ei.Insumo'
      'AND pe.Codigo>0'
      'AND pe.NF>0'
      'AND pe.Conhecimento>0'
      'AND pe.IQ>0'
      'AND ei.Insumo>0')
    Left = 116
    Top = 269
    object QrPQECodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pqeits.Codigo'
      DisplayFormat = '000;-000; '
    end
    object QrPQEControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'DBMBWET.pqeits.Controle'
    end
    object QrPQEConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'DBMBWET.pqeits.Conta'
    end
    object QrPQEInsumo: TIntegerField
      FieldName = 'Insumo'
      Origin = 'DBMBWET.pqeits.Insumo'
    end
    object QrPQEVolumes: TIntegerField
      FieldName = 'Volumes'
      Origin = 'DBMBWET.pqeits.Volumes'
    end
    object QrPQEPesoVB: TFloatField
      FieldName = 'PesoVB'
      Origin = 'DBMBWET.pqeits.PesoVB'
    end
    object QrPQEPesoVL: TFloatField
      FieldName = 'PesoVL'
      Origin = 'DBMBWET.pqeits.PesoVL'
    end
    object QrPQEValorItem: TFloatField
      FieldName = 'ValorItem'
      Origin = 'DBMBWET.pqeits.ValorItem'
    end
    object QrPQEIPI: TFloatField
      FieldName = 'IPI'
      Origin = 'DBMBWET.pqeits.IPI'
    end
    object QrPQERIPI: TFloatField
      FieldName = 'RIPI'
      Origin = 'DBMBWET.pqeits.RIPI'
    end
    object QrPQETotalCusto: TFloatField
      FieldName = 'TotalCusto'
      Origin = 'DBMBWET.pqeits.TotalCusto'
    end
    object QrPQETotalPeso: TFloatField
      FieldName = 'TotalPeso'
      Origin = 'DBMBWET.pqeits.TotalPeso'
    end
    object QrPQECFin: TFloatField
      FieldName = 'CFin'
      Origin = 'DBMBWET.pqeits.CFin'
    end
    object QrPQEICMS: TFloatField
      FieldName = 'ICMS'
      Origin = 'DBMBWET.pqeits.ICMS'
    end
    object QrPQERICMS: TFloatField
      FieldName = 'RICMS'
      Origin = 'DBMBWET.pqeits.RICMS'
    end
    object QrPQENOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Origin = 'DBMBWET.fornecedores.Nome'
      Size = 128
    end
    object QrPQENOMEINSUMO: TWideStringField
      FieldName = 'NOMEINSUMO'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
    object QrPQENF: TIntegerField
      FieldName = 'NF'
      Origin = 'DBMBWET.pqe.NF'
      DisplayFormat = '000000;-000000; '
    end
    object QrPQEData: TDateField
      FieldName = 'Data'
      Origin = 'DBMBWET.pqe.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQEConhecimento: TIntegerField
      FieldName = 'Conhecimento'
      Origin = 'DBMBWET.pqe.Conhecimento'
      DisplayFormat = '000000;-000000; '
    end
    object QrPQENF_RP: TIntegerField
      FieldName = 'NF_RP'
    end
    object QrPQENF_CC: TIntegerField
      FieldName = 'NF_CC'
    end
    object QrPQEIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQECI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQENOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 100
    end
  end
  object DsPQE: TDataSource
    DataSet = QrPQE
    Left = 116
    Top = 313
  end
  object QrFornecedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END Nome'
      'FROM entidades'
      'WHERE Fornece2='#39'V'#39
      'ORDER BY Nome')
    Left = 188
    Top = 269
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedoresNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 188
    Top = 313
  end
  object QrInsumos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq')
    Left = 264
    Top = 269
    object QrInsumosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrInsumosNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.pq.Nome'
      Size = 41
    end
  end
  object DsInsumos: TDataSource
    DataSet = QrInsumos
    Left = 264
    Top = 313
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END Nome'
      'FROM entidades'
      'WHERE Cliente2='#39'V'#39
      'ORDER BY Nome')
    Left = 344
    Top = 273
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 344
    Top = 317
  end
  object frxQUI_ENTRA_005_001: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxQUI_ENTRA_005_001GetValue
    Left = 468
    Top = 268
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPQE
        DataSetName = 'frxDsPQE'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708710240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Entradas de Insumos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 75.590600000000000000
          Width = 158.740157480000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590600000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Top = 75.590600000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 75.590600000000000000
          Width = 188.976397480000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Insumo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 75.590600000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 75.590600000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF VP')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 75.590600000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF RP')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 75.590600000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF CC')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181510000000000000
          Top = 75.590600000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conh.Frete')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 56.692949999999990000
          Width = 275.905587480000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_FORNECE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente interno')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 41.574830000000000000
          Width = 275.905587480000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692949999999990000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 56.692949999999990000
          Width = 283.464566930000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PRODUTO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 56.692949999999990000
          Width = 45.354311180000010000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 41.574830000000000000
          Width = 283.464566930000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 41.574830000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 170.078850000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPQE
        DataSetName = 'frxDsPQE'
        RowCount = 0
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 158.740157480000000000
          Height = 15.118110240000000000
          DataField = 'NOMEFORNECEDOR'
          DataSet = frxDsPQE
          DataSetName = 'frxDsPQE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQE."NOMEFORNECEDOR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'IQ'
          DataSet = frxDsPQE
          DataSetName = 'frxDsPQE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQE."IQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitValorT: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = frxDsPQE
          DataSetName = 'frxDsPQE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQE."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 188.976397480000000000
          Height = 15.118110240000000000
          DataField = 'NOMEINSUMO'
          DataSet = frxDsPQE
          DataSetName = 'frxDsPQE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQE."NOMEINSUMO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsPQE
          DataSetName = 'frxDsPQE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQE."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'NF'
          DataSet = frxDsPQE
          DataSetName = 'frxDsPQE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQE."NF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'NF_RP'
          DataSet = frxDsPQE
          DataSetName = 'frxDsPQE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQE."NF_RP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'NF_CC'
          DataSet = frxDsPQE
          DataSetName = 'frxDsPQE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQE."NF_CC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181510000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'Conhecimento'
          DataSet = frxDsPQE
          DataSetName = 'frxDsPQE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQE."Conhecimento"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPQE: TfrxDBDataset
    UserName = 'frxDsPQE'
    CloseDataSource = False
    DataSet = QrPQE
    BCDToCurrency = False
    DataSetOptions = []
    Left = 112
    Top = 360
  end
end
