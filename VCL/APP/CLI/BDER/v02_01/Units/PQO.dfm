object FmPQO: TFmPQO
  Left = 368
  Top = 194
  Caption = 'QUI-RECEI-011 :: Baixa de Insumos Diversos'
  ClientHeight = 600
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 504
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object Label23: TLabel
      Left = 16
      Top = 128
      Width = 219
      Height = 13
      Caption = 'Grupo de f'#243'rmulas para relat'#243'rio de consumos:'
    end
    object SbFormulasGru: TSpeedButton
      Left = 580
      Top = 144
      Width = 22
      Height = 21
      Caption = '...'
      OnClick = SbFormulasGruClick
    end
    object PnEdit: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 125
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 76
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit3
      end
      object Label3: TLabel
        Left = 16
        Top = 48
        Width = 80
        Height = 13
        Caption = 'Setor de destino:'
      end
      object Label14: TLabel
        Left = 16
        Top = 88
        Width = 88
        Height = 13
        Caption = 'Grupo de emiss'#227'o:'
      end
      object Label11: TLabel
        Left = 180
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdit3
      end
      object SpeedButton5: TSpeedButton
        Left = 580
        Top = 104
        Width = 22
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label15: TLabel
        Left = 222
        Top = 48
        Width = 122
        Height = 13
        Caption = 'Cor padr'#227'o (recurtimento):'
      end
      object Label24: TLabel
        Left = 464
        Top = 48
        Width = 99
        Height = 13
        Caption = 'Rebaixe (espessura):'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDataB: TdmkEditDateTimePicker
        Left = 76
        Top = 24
        Width = 101
        Height = 21
        Date = 38799.000000000000000000
        Time = 0.930188981503306400
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdSetor: TdmkEditCB
        Left = 16
        Top = 64
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSetor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSetor: TdmkDBLookupComboBox
        Left = 72
        Top = 64
        Width = 145
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSE
        TabOrder = 4
        dmkEditCB = EdSetor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEmitGru: TdmkEditCB
        Left = 16
        Top = 104
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmitGru
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmitGru: TdmkDBLookupComboBox
        Left = 74
        Top = 104
        Width = 503
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEmitGru
        TabOrder = 8
        dmkEditCB = EdEmitGru
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdNome: TdmkEdit
        Left = 180
        Top = 24
        Width = 421
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdGraCorCad: TdmkEditCB
        Left = 220
        Top = 64
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraCorCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraCorCad: TdmkDBLookupComboBox
        Left = 274
        Top = 64
        Width = 187
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGraCorCad
        TabOrder = 6
        dmkEditCB = EdGraCorCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdSemiCodEspReb: TdmkEditCB
        Left = 464
        Top = 64
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSemiCodEspReb
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSemiCodEspReb: TdmkDBLookupComboBox
        Left = 496
        Top = 64
        Width = 104
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Linhas'
        ListSource = DsRebaixe
        TabOrder = 10
        dmkEditCB = EdSemiCodEspReb
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 440
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel7: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 12
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
    object EdFormulasGru: TdmkEditCB
      Left = 16
      Top = 144
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBFormulasGru
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBFormulasGru: TdmkDBLookupComboBox
      Left = 74
      Top = 144
      Width = 503
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsFormulasGru
      TabOrder = 3
      dmkEditCB = EdFormulasGru
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 504
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object PnData: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 89
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label6: TLabel
        Left = 72
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit3
      end
      object Label8: TLabel
        Left = 544
        Top = 48
        Width = 28
        Height = 13
        Caption = 'Setor:'
        FocusControl = DBEdit5
      end
      object Label4: TLabel
        Left = 704
        Top = 8
        Width = 53
        Height = 13
        Caption = 'Custo total:'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 4
        Top = 48
        Width = 88
        Height = 13
        Caption = 'Grupo de emiss'#227'o:'
        FocusControl = DBEdit2
      end
      object Label7: TLabel
        Left = 148
        Top = 8
        Width = 31
        Height = 13
        Caption = 'Nome:'
        FocusControl = DBEdit4
      end
      object Label16: TLabel
        Left = 540
        Top = 8
        Width = 91
        Height = 13
        Caption = 'Cor (Recurtimento):'
        FocusControl = DBEdit10
      end
      object Label21: TLabel
        Left = 668
        Top = 48
        Width = 114
        Height = 13
        Caption = 'Rebaixe (Recurtimento):'
        FocusControl = DBEdit6
      end
      object Label25: TLabel
        Left = 272
        Top = 48
        Width = 219
        Height = 13
        Caption = 'Grupo de f'#243'rmulas para relat'#243'rio de consumos:'
        FocusControl = DBEdit11
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 24
        Width = 65
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPQO
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit3: TDBEdit
        Left = 72
        Top = 24
        Width = 73
        Height = 21
        DataField = 'DataB'
        DataSource = DsPQO
        TabOrder = 1
      end
      object DBEdit5: TDBEdit
        Left = 544
        Top = 64
        Width = 121
        Height = 21
        DataField = 'NOMESETOR'
        DataSource = DsPQO
        TabOrder = 2
      end
      object DBEdit1: TDBEdit
        Left = 704
        Top = 24
        Width = 84
        Height = 21
        DataField = 'CustoTotal'
        DataSource = DsPQO
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 4
        Top = 64
        Width = 265
        Height = 21
        DataField = 'NO_EMITGRU'
        DataSource = DsPQO
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 148
        Top = 24
        Width = 389
        Height = 21
        DataField = 'Nome'
        DataSource = DsPQO
        TabOrder = 5
      end
      object DBEdit10: TDBEdit
        Left = 540
        Top = 24
        Width = 161
        Height = 21
        DataField = 'NoGraCorCad'
        DataSource = DsPQO
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 668
        Top = 64
        Width = 117
        Height = 21
        DataField = 'RebLin'
        DataSource = DsPQO
        TabOrder = 7
      end
      object DBEdit11: TDBEdit
        Left = 272
        Top = 64
        Width = 265
        Height = 21
        DataField = 'NO_FormulasGru'
        DataSource = DsPQO
        TabOrder = 8
      end
    end
    object DBGIts: TDBGrid
      Left = -4
      Top = 94
      Width = 790
      Height = 84
      DataSource = DsPQOIts
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Insumo'
          Title.Alignment = taCenter
          Title.Caption = 'C'#243'digo'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CliDest'
          Title.Caption = 'Empresa'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPQ'
          Title.Caption = 'Mercadoria'
          Width = 366
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Title.Alignment = taRightJustify
          Title.Caption = 'Quantidade'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Title.Alignment = taRightJustify
          Title.Caption = 'Valor total'
          Width = 92
          Visible = True
        end>
    end
    object PnInsumos: TPanel
      Left = 0
      Top = 240
      Width = 792
      Height = 136
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      Visible = False
      object Label10: TLabel
        Left = 324
        Top = 4
        Width = 130
        Height = 13
        Caption = 'Material de uso e consumo:'
      end
      object Label12: TLabel
        Left = 4
        Top = 4
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label13: TLabel
        Left = 684
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label17: TLabel
        Left = 324
        Top = 44
        Width = 98
        Height = 13
        Caption = 'Estoque quantidade:'
        FocusControl = DBEdit7
      end
      object Label18: TLabel
        Left = 432
        Top = 44
        Width = 51
        Height = 13
        Caption = 'Estoque $:'
        FocusControl = DBEdit8
      end
      object Label19: TLabel
        Left = 540
        Top = 44
        Width = 76
        Height = 13
        Caption = 'Custo unit'#225'rio $:'
        FocusControl = DBEdit9
      end
      object Label20: TLabel
        Left = 684
        Top = 44
        Width = 93
        Height = 13
        Caption = 'Saldo futuro quant.:'
      end
      object Label22: TLabel
        Left = 4
        Top = 84
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdPQ: TdmkEditCB
        Left = 324
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPQChange
        DBLookupComboBox = CBPQ
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPQ: TdmkDBLookupComboBox
        Left = 380
        Top = 20
        Width = 301
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPQ
        TabOrder = 3
        dmkEditCB = EdPQ
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCI: TdmkEditCB
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCIChange
        DBLookupComboBox = CBCI
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCI: TdmkDBLookupComboBox
        Left = 60
        Top = 20
        Width = 261
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECI'
        ListSource = DsCI
        TabOrder = 1
        dmkEditCB = EdCI
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPesoAdd: TdmkEdit
        Left = 684
        Top = 20
        Width = 100
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdPesoAddExit
      end
      object DBEdit7: TDBEdit
        Left = 324
        Top = 60
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'Peso'
        DataSource = DsSaldo
        TabOrder = 5
      end
      object DBEdit8: TDBEdit
        Left = 432
        Top = 60
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'Valor'
        DataSource = DsSaldo
        TabOrder = 6
      end
      object DBEdit9: TDBEdit
        Left = 540
        Top = 60
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'CUSTO'
        DataSource = DsSaldo
        TabOrder = 7
      end
      object EdSaldoFut: TdmkEdit
        Left = 684
        Top = 60
        Width = 100
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkDtCorrApo: TCheckBox
        Left = 4
        Top = 64
        Width = 185
        Height = 17
        Caption = #201' corre'#231#227'o de apontamento. Data:'
        TabOrder = 9
        OnClick = CkDtCorrApoClick
      end
      object TPDtCorrApo: TdmkEditDateTimePicker
        Left = 192
        Top = 60
        Width = 129
        Height = 21
        Date = 45209.000000000000000000
        Time = 0.833253726850671200
        Enabled = False
        TabOrder = 10
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN_MAX
      end
      object EdEmpresa: TdmkEditCB
        Left = 4
        Top = 100
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 60
        Top = 100
        Width = 437
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 12
        TabStop = False
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBControle: TGroupBox
      Left = 0
      Top = 376
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 4
      object PnNaviDwn: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 95
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 269
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 124
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 244
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtExcluiClick
        end
      end
    end
    object GBConfirm2: TGroupBox
      Left = 0
      Top = 440
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 3
      Visible = False
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste2: TBitBtn
            Tag = 15
            Left = 16
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesiste2Click
          end
        end
        object BtConfirma2: TBitBtn
          Tag = 14
          Left = 16
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirma2Click
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 329
        Height = 32
        Caption = 'Baixa de Insumos Diversos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 329
        Height = 32
        Caption = 'Baixa de Insumos Diversos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 329
        Height = 32
        Caption = 'Baixa de Insumos Diversos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsPQO: TDataSource
    DataSet = QrPQO
    Left = 32
    Top = 281
  end
  object QrPQO: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPQOBeforeOpen
    AfterOpen = QrPQOAfterOpen
    AfterClose = QrPQOAfterClose
    AfterScroll = QrPQOAfterScroll
    SQL.Strings = (
      'SELECT lse.Nome NOMESETOR,'
      'emg.Nome NO_EMITGRU,  gcc.Nome NoGraCorCad, pqo.*'
      'FROM pqo pqo'
      'LEFT JOIN listasetores lse ON lse.Codigo=pqo.Setor'
      'LEFT JOIN emitgru emg ON emg.Codigo=pqo.EmitGru'
      'LEFT JOIN  gracorcad gcc ON gcc.Codigo=pqo.GraCorCad'
      'WHERE pqo.Codigo > 0'
      '')
    Left = 32
    Top = 237
    object QrPQOCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQOSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrPQOCustoInsumo: TFloatField
      FieldName = 'CustoInsumo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQOCustoTotal: TFloatField
      FieldName = 'CustoTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQODataB: TDateField
      FieldName = 'DataB'
    end
    object QrPQOLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQODataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQODataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQOUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQOUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQONOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrPQONO_EMITGRU: TWideStringField
      FieldName = 'NO_EMITGRU'
      Size = 60
    end
    object QrPQOAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQOAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQOEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrPQONome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPQOGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrPQONoGraCorCad: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NoGraCorCad'
      LookupDataSet = QrGraCorCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'GraCorCad'
      Size = 60
      Lookup = True
    end
    object QrPQOSemiCodEspReb: TIntegerField
      FieldName = 'SemiCodEspReb'
    end
    object QrPQORebLin: TWideStringField
      FieldName = 'RebLin'
      Size = 7
    end
    object QrPQOFormulasGru: TIntegerField
      FieldName = 'FormulasGru'
    end
    object QrPQONO_FormulasGru: TWideStringField
      FieldName = 'NO_FormulasGru'
      Size = 60
    end
  end
  object QrPQOIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico, '
      'pqg.Nome NOMEGRUPO'
      'FROM pqx pqx'
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo'
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ'
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico'
      'WHERE pqx.Tipo=190'
      'AND pqx.OrigemCodi=:P0')
    Left = 224
    Top = 245
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQOItsDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQOItsTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPQOItsCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQOItsCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQOItsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQOItsPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQOItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQOItsOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQOItsOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQOItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQOItsGrupoQuimico: TIntegerField
      FieldName = 'GrupoQuimico'
    end
    object QrPQOItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
    object QrPQOItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object DsPQOIts: TDataSource
    DataSet = QrPQOIts
    Left = 224
    Top = 293
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq '
      'WHERE Ativo=1'
      'AND GGXNiv2 IN (-1,-2,-4, 1, 2, 4)'
      'ORDER BY Nome')
    Left = 96
    Top = 240
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 96
    Top = 284
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 576
    Top = 296
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 576
    Top = 244
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object QrSaldo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO '
      'FROM pqcli'
      'WHERE CI=:P0'
      'AND PQ=:P1')
    Left = 516
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSaldoPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSaldoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSaldoCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
    object QrSaldoCI: TIntegerField
      FieldName = 'CI'
    end
    object QrSaldoPQ: TIntegerField
      FieldName = 'PQ'
    end
  end
  object DsSaldo: TDataSource
    DataSet = QrSaldo
    Left = 516
    Top = 292
  end
  object PMInclui: TPopupMenu
    Left = 313
    Top = 444
    object IncluiNovaBaixa1: TMenuItem
      Caption = 'Inclui nova &Baixa'
      OnClick = IncluiNovaBaixa1Click
    end
    object IncluiInsumobaixaatual1: TMenuItem
      Caption = 'Inclui &Insumo '#224' baixa atual'
      OnClick = IncluiInsumobaixaatual1Click
    end
  end
  object QrSumPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(pqx.Valor)*-1 CUSTO'
      'FROM pqx pqx'
      'WHERE pqx.Tipo=190'
      'AND pqx.OrigemCodi=:P0')
    Left = 160
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPQCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
  end
  object PMExclui: TPopupMenu
    OnPopup = PMExcluiPopup
    Left = 573
    Top = 441
    object ExcluiItemdabaixa1: TMenuItem
      Caption = 'Exclui &Item da baixa'
      Enabled = False
      OnClick = ExcluiItemdabaixa1Click
    end
    object ExcluiBaixa1: TMenuItem
      Caption = 'Exclui &Baixa'
      Enabled = False
      OnClick = ExcluiBaixa1Click
    end
  end
  object PMAltera: TPopupMenu
    OnPopup = PMAlteraPopup
    Left = 445
    Top = 448
    object Alteraitematual1: TMenuItem
      Caption = '&Altera item atual'
      OnClick = Alteraitematual1Click
    end
    object AlteraBaixa1: TMenuItem
      Caption = 'Altera &Baixa'
      OnClick = AlteraBaixa1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 13
    Top = 65533
    object Estabaixa1: TMenuItem
      Caption = '&Baixa atual'
      OnClick = Estabaixa1Click
    end
    object Outros1: TMenuItem
      Caption = '&Outros (Insumos)'
      OnClick = Outros1Click
    end
  end
  object QrPeriodo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lse.Nome NOMESETOR, pqo.* '
      'FROM pqo pqo'
      'LEFT JOIN listasetores lse ON lse.Codigo=pqo.Setor'
      'WHERE pqo.DataB BETWEEN :P0 AND :P1'
      'ORDER BY pqo.DataB'
      '')
    Left = 157
    Top = 289
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPeriodoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPeriodoSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrPeriodoCustoInsumo: TFloatField
      FieldName = 'CustoInsumo'
    end
    object QrPeriodoCustoTotal: TFloatField
      FieldName = 'CustoTotal'
    end
    object QrPeriodoDataB: TDateField
      FieldName = 'DataB'
    end
    object QrPeriodoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPeriodoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPeriodoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPeriodoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPeriodoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPeriodoNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
  end
  object QrSE: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 452
    Top = 244
    object QrSECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSENome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object DsSE: TDataSource
    DataSet = QrSE
    Left = 452
    Top = 292
  end
  object QrEmitGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM emitgru'
      'WHERE Ativo=1'
      'ORDER BY Nome'
      '')
    Left = 280
    Top = 245
    object QrEmitGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEmitGru: TDataSource
    DataSet = QrEmitGru
    Left = 280
    Top = 293
  end
  object frxQUI_RECEI_011_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.850269456000000000
    ReportOptions.LastChange = 39717.850269456000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxQUI_RECEI_011_AGetValue
    Left = 368
    Top = 244
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPQO
        DataSetName = 'frxDsPQO'
      end
      item
        DataSet = frxDsPQOIts
        DataSetName = 'frxDsPQOIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 79.370044570000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385817010000000000
          Top = 60.472406769999990000
          Width = 264.566929130000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 377.952746140000000000
          Top = 60.472406769999990000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543297320000000000
          Top = 60.472406769999990000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo R$')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Baixa de Insumos Qu'#237'micos Diversos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPQOIts
        DataSetName = 'frxDsPQOIts'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543297320000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DataField = 'Valor'
          DataSet = frxDsPQOIts
          DataSetName = 'frxDsPQOIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQOIts."Valor"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385826771653500000
          Width = 264.566929130000000000
          Height = 18.897637800000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsPQOIts
          DataSetName = 'frxDsPQOIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQOIts."NOMEPQ"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 377.952746140000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataField = 'Peso'
          DataSet = frxDsPQOIts
          DataSetName = 'frxDsPQOIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQOIts."Peso"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 283.464750000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543380310000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DataSet = frxDsPQOIts
          DataSetName = 'frxDsPQOIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQOIts."Valor">,Band4)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385826771653500000
          Width = 264.566929130000000000
          Height = 18.897637800000000000
          DataSet = frxDsPQOIts
          DataSetName = 'frxDsPQOIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 377.952829130000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsPQOIts
          DataSetName = 'frxDsPQOIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQOIts."Peso">,Band4)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsPQOIts: TfrxDBDataset
    UserName = 'frxDsPQOIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataX=DataX'
      'Tipo=Tipo'
      'CliOrig=CliOrig'
      'CliDest=CliDest'
      'Insumo=Insumo'
      'Peso=Peso'
      'Valor=Valor'
      'OrigemCodi=OrigemCodi'
      'OrigemCtrl=OrigemCtrl'
      'NOMEPQ=NOMEPQ'
      'GrupoQuimico=GrupoQuimico'
      'NOMEGRUPO=NOMEGRUPO')
    DataSet = QrPQOIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 368
    Top = 292
  end
  object frxDsPQO: TfrxDBDataset
    UserName = 'frxDsPQO'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Setor=Setor'
      'CustoInsumo=CustoInsumo'
      'CustoTotal=CustoTotal'
      'DataB=DataB'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'NOMESETOR=NOMESETOR'
      'NO_EMITGRU=NO_EMITGRU'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'EmitGru=EmitGru'
      'Nome=Nome')
    DataSet = QrPQO
    BCDToCurrency = False
    DataSetOptions = []
    Left = 32
    Top = 328
  end
  object QrGraCorCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gracorcad'
      'ORDER BY Nome')
    Left = 648
    Top = 244
    object QrGraCorCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCorCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGraCorCad: TDataSource
    DataSet = QrGraCorCad
    Left = 644
    Top = 292
  end
  object QrRebaixe: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Linhas, EMCM'
      'FROM espessuras'
      'ORDER BY EMCM')
    Left = 712
    Top = 260
    object QrRebaixeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRebaixeLinhas: TWideStringField
      FieldName = 'Linhas'
      Size = 7
    end
    object QrRebaixeEMCM: TFloatField
      FieldName = 'EMCM'
    end
  end
  object DsRebaixe: TDataSource
    DataSet = QrRebaixe
    Left = 712
    Top = 308
  end
  object QrFormulasGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM formulasgru'
      'ORDER BY Nome')
    Left = 572
    Top = 4
    object QrFormulasGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulasGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsFormulasGru: TDataSource
    DataSet = QrFormulasGru
    Left = 572
    Top = 48
  end
end
