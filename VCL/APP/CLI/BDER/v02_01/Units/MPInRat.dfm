object FmMPInRat: TFmMPInRat
  Left = 277
  Top = 185
  Caption = 'COU-MP_IN-006 :: Rateio de Caminh'#227'o'
  ClientHeight = 601
  ClientWidth = 978
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 978
    Height = 381
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 978
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 46
        Width = 50
        Height = 13
        Caption = 'Caminh'#227'o:'
      end
      object Label10: TLabel
        Left = 56
        Top = 46
        Width = 53
        Height = 13
        Caption = 'Couros NF:'
      end
      object Label6: TLabel
        Left = 260
        Top = 46
        Width = 44
        Height = 13
        Caption = 'PNF real:'
      end
      object Label8: TLabel
        Left = 120
        Top = 46
        Width = 60
        Height = 13
        Caption = 'Couros entr.:'
      end
      object Label4: TLabel
        Left = 336
        Top = 46
        Width = 38
        Height = 13
        Caption = 'PLE kg:'
      end
      object Label7: TLabel
        Left = 420
        Top = 46
        Width = 40
        Height = 13
        Caption = 'PDA kg:'
      end
      object Label2: TLabel
        Left = 4
        Top = 4
        Width = 138
        Height = 13
        Caption = 'Fornecedor original do couro:'
      end
      object Label11: TLabel
        Left = 184
        Top = 46
        Width = 47
        Height = 13
        Caption = 'PNF frigo:'
      end
      object dmkEdCaminhao: TdmkEdit
        Left = 4
        Top = 62
        Width = 49
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdPecasNF: TdmkEdit
        Left = 56
        Top = 62
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 1
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = dmkEdPecasNFChange
      end
      object dmkEdPNF_Far: TdmkEdit
        Left = 184
        Top = 62
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = dmkEdPecasNFChange
      end
      object dmkEdPecas: TdmkEdit
        Left = 120
        Top = 62
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 1
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = dmkEdPecasNFChange
      end
      object dmkEdPLE: TdmkEdit
        Left = 336
        Top = 62
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = dmkEdPecasNFChange
      end
      object dmkEdPDA: TdmkEdit
        Left = 420
        Top = 62
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = dmkEdPecasNFChange
      end
      object BitBtn1: TBitBtn
        Left = 512
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        TabOrder = 9
        TabStop = False
        OnClick = BitBtn1Click
      end
      object EdFornece: TdmkEditCB
        Left = 4
        Top = 20
        Width = 48
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdProcedeChange
        DBLookupComboBox = CBFornece
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornece: TdmkDBLookupComboBox
        Left = 53
        Top = 20
        Width = 456
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsProcede
        TabOrder = 1
        dmkEditCB = EdFornece
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object dmkEdPNF: TdmkEdit
        Left = 260
        Top = 62
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = dmkEdPecasNFChange
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 92
      Width = 978
      Height = 241
      Align = alClient
      DataSource = DsMPInRat
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'ProcedeCod'
          Title.Caption = 'C'#243'digo'
          Width = 41
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ProcedeNom'
          Title.Caption = 'Fornecedor'
          Width = 392
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Custo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PecasNF'
          Title.Caption = 'Pe'#231'as NF'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as LE'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PNF_Fat'
          Title.Caption = 'PNF Fat.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PNF'
          Title.Caption = 'PNF Real'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PLE'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PDA'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = 'Pre'#231'o'
          Visible = True
        end>
    end
    object PnInsUpd: TPanel
      Left = 0
      Top = 333
      Width = 978
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      Visible = False
      object Label35: TLabel
        Left = 4
        Top = 4
        Width = 102
        Height = 13
        Caption = 'Fornecedor do couro:'
      end
      object Label3: TLabel
        Left = 512
        Top = 2
        Width = 39
        Height = 13
        Caption = 'PNF kg:'
      end
      object Label5: TLabel
        Left = 596
        Top = 2
        Width = 49
        Height = 13
        Caption = '$ total NF:'
      end
      object Label9: TLabel
        Left = 676
        Top = 2
        Width = 40
        Height = 13
        Caption = '$ Couro:'
      end
      object EdProcede: TdmkEditCB
        Left = 4
        Top = 20
        Width = 48
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdProcedeChange
        DBLookupComboBox = CBProcede
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBProcede: TdmkDBLookupComboBox
        Left = 53
        Top = 20
        Width = 456
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornece
        TabOrder = 1
        dmkEditCB = EdProcede
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object dmkEdPNF_Fat2: TdmkEdit
        Left = 512
        Top = 18
        Width = 81
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = dmkEdPecasNFChange
      end
      object dmkEdCusto: TdmkEdit
        Left = 596
        Top = 18
        Width = 77
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = dmkEdPecasNFChange
      end
      object dmkEdPreco: TdmkEdit
        Left = 676
        Top = 18
        Width = 89
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = dmkEdPecasNFChange
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 978
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 930
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 882
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 248
        Height = 32
        Caption = 'Rateio de Caminh'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 248
        Height = 32
        Caption = 'Rateio de Caminh'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 248
        Height = 32
        Caption = 'Rateio de Caminh'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 978
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 974
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBControla: TGroupBox
    Left = 0
    Top = 537
    Width = 978
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 974
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 830
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtRateia: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Rateia'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object BitBtn2: TBitBtn
        Left = 140
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Insere'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BitBtn2Click
      end
      object BitBtn5: TBitBtn
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BitBtn5Click
      end
      object BitBtn6: TBitBtn
        Left = 380
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BitBtn6Click
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 473
    Width = 978
    Height = 64
    Align = alBottom
    TabOrder = 4
    Visible = False
    object Panelxxx: TPanel
      Left = 2
      Top = 15
      Width = 974
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 830
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn3: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn3Click
        end
      end
      object BitBtn4: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn4Click
      end
    end
  end
  object QrProcede: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emp.Corretor, emp.Comissao, emp.DescAdiant,'
      'emp.CondPagto, emp.CondComis, emp.LocalEntrg,'
      'emp.PreDescarn, emp.MaskLetras, emp.MaskFormat, '
      'emp.Abate, emp.Transporte, emp.CMPPreco, ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'LEFT JOIN entimp emp ON emp.Codigo=ent.Codigo'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 400
    Top = 58
    object QrProcedeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProcedeNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrProcedePreDescarn: TSmallintField
      FieldName = 'PreDescarn'
    end
    object QrProcedeMaskLetras: TWideStringField
      FieldName = 'MaskLetras'
      Size = 10
    end
    object QrProcedeMaskFormat: TWideStringField
      FieldName = 'MaskFormat'
      Size = 10
    end
    object QrProcedeCorretor: TIntegerField
      FieldName = 'Corretor'
    end
    object QrProcedeComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrProcedeDescAdiant: TFloatField
      FieldName = 'DescAdiant'
    end
    object QrProcedeCondPagto: TWideStringField
      FieldName = 'CondPagto'
      Size = 30
    end
    object QrProcedeCondComis: TWideStringField
      FieldName = 'CondComis'
      Size = 30
    end
    object QrProcedeLocalEntrg: TWideStringField
      FieldName = 'LocalEntrg'
      Size = 50
    end
    object QrProcedeAbate: TIntegerField
      FieldName = 'Abate'
    end
    object QrProcedeTransporte: TIntegerField
      FieldName = 'Transporte'
    end
    object QrProcedeCMPPreco: TFloatField
      FieldName = 'CMPPreco'
    end
  end
  object DsFornece: TDataSource
    DataSet = QrProcede
    Left = 428
    Top = 58
  end
  object QrMPInRat: TMySQLQuery
    AfterOpen = QrMPInRatAfterOpen
    SQL.Strings = (
      'SELECT * FROM mpinrat'
      'ORDER BY Controle')
    Left = 204
    Top = 196
    object QrMPInRatProcedeCod: TIntegerField
      DisplayWidth = 12
      FieldName = 'ProcedeCod'
      Origin = 'mpinrat.ProcedeCod'
    end
    object QrMPInRatProcedeNom: TWideStringField
      DisplayWidth = 31
      FieldName = 'ProcedeNom'
      Origin = 'mpinrat.ProcedeNom'
      Size = 100
    end
    object QrMPInRatPecas: TFloatField
      DisplayWidth = 12
      FieldName = 'Pecas'
      Origin = 'mpinrat.Pecas'
    end
    object QrMPInRatPecasNF: TFloatField
      DisplayWidth = 12
      FieldName = 'PecasNF'
      Origin = 'mpinrat.PecasNF'
    end
    object QrMPInRatPNF: TFloatField
      DisplayWidth = 12
      FieldName = 'PNF'
      Origin = 'mpinrat.PNF'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInRatPLE: TFloatField
      DisplayWidth = 12
      FieldName = 'PLE'
      Origin = 'mpinrat.PLE'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInRatPDA: TFloatField
      DisplayWidth = 12
      FieldName = 'PDA'
      Origin = 'mpinrat.PDA'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInRatPreco: TFloatField
      DisplayWidth = 12
      FieldName = 'Preco'
      Origin = 'mpinrat.Preco'
      DisplayFormat = '###,###,##0.0000;-###,###,##0.0000; '
    end
    object QrMPInRatCusto: TFloatField
      DisplayWidth = 12
      FieldName = 'Custo'
      Origin = 'mpinrat.Custo'
      DisplayFormat = '###,###,##0.00;-###,###,##0.00; '
    end
    object QrMPInRatControle: TAutoIncField
      FieldName = 'Controle'
      Origin = 'mpinrat.Controle'
    end
    object QrMPInRatPNF_Fat: TFloatField
      FieldName = 'PNF_Fat'
      Origin = 'mpinrat.PNF_Fat'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
  end
  object DsMPInRat: TDataSource
    DataSet = QrMPInRat
    Left = 232
    Top = 196
  end
  object DsProcede: TDataSource
    DataSet = QrProcede
    Left = 456
    Top = 58
  end
  object mySQLQuery1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 360
    Top = 208
  end
  object PMFornece: TPopupMenu
    Left = 501
    Top = 100
    object Entidades1: TMenuItem
      Caption = '&Entidades'
      OnClick = Entidades1Click
    end
    object FornecedoresdeMP1: TMenuItem
      Caption = '&Fornecedores de MP'
      OnClick = FornecedoresdeMP1Click
    end
  end
  object QrA: TMySQLQuery
    SQL.Strings = (
      'SELECT * FROM mpinrat'
      'WHERE ProcedeCod<>:P0'
      'ORDER BY Controle')
    Left = 104
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAProcedeCod: TIntegerField
      FieldName = 'ProcedeCod'
      Origin = 'mpinrat.ProcedeCod'
    end
    object QrAProcedeNom: TWideStringField
      FieldName = 'ProcedeNom'
      Origin = 'mpinrat.ProcedeNom'
      Size = 100
    end
    object QrAPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'mpinrat.Pecas'
    end
    object QrAPecasNF: TFloatField
      FieldName = 'PecasNF'
      Origin = 'mpinrat.PecasNF'
    end
    object QrAPNF: TFloatField
      FieldName = 'PNF'
      Origin = 'mpinrat.PNF'
    end
    object QrAPLE: TFloatField
      FieldName = 'PLE'
      Origin = 'mpinrat.PLE'
    end
    object QrAPDA: TFloatField
      FieldName = 'PDA'
      Origin = 'mpinrat.PDA'
    end
    object QrAPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'mpinrat.Preco'
    end
    object QrACusto: TFloatField
      FieldName = 'Custo'
      Origin = 'mpinrat.Custo'
    end
    object QrAControle: TAutoIncField
      FieldName = 'Controle'
      Origin = 'mpinrat.Controle'
    end
  end
  object QrB: TMySQLQuery
    SQL.Strings = (
      'SELECT * FROM mpinrat'
      'WHERE ProcedeCod=:P0'
      'ORDER BY Controle')
    Left = 104
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBProcedeCod: TIntegerField
      FieldName = 'ProcedeCod'
      Origin = 'mpinrat.ProcedeCod'
    end
    object QrBProcedeNom: TWideStringField
      FieldName = 'ProcedeNom'
      Origin = 'mpinrat.ProcedeNom'
      Size = 100
    end
    object QrBPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'mpinrat.Pecas'
    end
    object QrBPecasNF: TFloatField
      FieldName = 'PecasNF'
      Origin = 'mpinrat.PecasNF'
    end
    object QrBPNF: TFloatField
      FieldName = 'PNF'
      Origin = 'mpinrat.PNF'
    end
    object QrBPLE: TFloatField
      FieldName = 'PLE'
      Origin = 'mpinrat.PLE'
    end
    object QrBPDA: TFloatField
      FieldName = 'PDA'
      Origin = 'mpinrat.PDA'
    end
    object QrBPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'mpinrat.Preco'
    end
    object QrBCusto: TFloatField
      FieldName = 'Custo'
      Origin = 'mpinrat.Custo'
    end
    object QrBControle: TAutoIncField
      FieldName = 'Controle'
      Origin = 'mpinrat.Controle'
    end
  end
end
