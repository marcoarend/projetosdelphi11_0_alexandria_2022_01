object FmMPVImp: TFmMPVImp
  Left = 354
  Top = 179
  Caption = 'VEN-COURO-152 :: Relat'#243'rios de Faturamentos de Pedidos'
  ClientHeight = 593
  ClientWidth = 779
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 779
    Height = 449
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 439
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 779
      Height = 181
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label10: TLabel
        Left = 8
        Top = 1
        Width = 35
        Height = 12
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente:'
      end
      object Label1: TLabel
        Left = 367
        Top = 1
        Width = 30
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Artigo:'
      end
      object Label2: TLabel
        Left = 8
        Top = 42
        Width = 49
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Vendedor:'
      end
      object Label3: TLabel
        Left = 367
        Top = 42
        Width = 69
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Transportador:'
      end
      object Label4: TLabel
        Left = 8
        Top = 83
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object EdCliente: TdmkEditCB
        Left = 8
        Top = 17
        Width = 65
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 77
        Top = 17
        Width = 288
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientes
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBMP: TdmkDBLookupComboBox
        Left = 436
        Top = 17
        Width = 288
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsMP
        TabOrder = 2
        dmkEditCB = EdMP
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdMP: TdmkEditCB
        Left = 367
        Top = 17
        Width = 66
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdMPChange
        DBLookupComboBox = CBMP
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdVendedor: TdmkEditCB
        Left = 8
        Top = 58
        Width = 65
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdVendedorChange
        DBLookupComboBox = CBVendedor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBVendedor: TdmkDBLookupComboBox
        Left = 77
        Top = 58
        Width = 288
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsVendedores
        TabOrder = 5
        dmkEditCB = EdVendedor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTransp: TdmkEditCB
        Left = 367
        Top = 58
        Width = 66
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTranspChange
        DBLookupComboBox = CBTransp
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransp: TdmkDBLookupComboBox
        Left = 436
        Top = 58
        Width = 288
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsTransp
        TabOrder = 7
        dmkEditCB = EdTransp
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTexto: TdmkEdit
        Left = 8
        Top = 99
        Width = 357
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdTextoChange
      end
      object RgFiltro: TRadioGroup
        Left = 369
        Top = 83
        Width = 355
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Filtro da descri'#231#227'o: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Sem filtro'
          '%texto%'
          '%texto'
          'texto%')
        TabOrder = 9
        OnClick = RgFiltroClick
      end
      object CkComissoes: TCheckBox
        Left = 817
        Top = 128
        Width = 139
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Mostra comiss'#245'es.'
        TabOrder = 10
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 121
        Width = 262
        Height = 60
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Data do faturamento: '
        TabOrder = 11
        object TPanel
          Left = 2
          Top = 15
          Width = 258
          Height = 43
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 46
          object CkFimDataF: TCheckBox
            Left = 128
            Top = -4
            Width = 100
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final.'
            TabOrder = 2
          end
          object CkIniDataF: TCheckBox
            Left = 10
            Top = -4
            Width = 109
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
            TabOrder = 0
          end
          object TPIniDataF: TDateTimePicker
            Left = 10
            Top = 17
            Width = 114
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 38604.000000000000000000
            Time = 0.601629953700467000
            TabOrder = 1
            OnChange = TPIniDataFChange
          end
          object TPFimDataF: TDateTimePicker
            Left = 128
            Top = 17
            Width = 114
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 38604.000000000000000000
            Time = 0.601629953700467000
            TabOrder = 3
            OnChange = TPFimDataFChange
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 274
        Top = 121
        Width = 262
        Height = 60
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Data da entrega: '
        TabOrder = 12
        object TPanel
          Left = 2
          Top = 15
          Width = 258
          Height = 43
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 58
          object CkFimEntrega: TCheckBox
            Left = 128
            Top = -4
            Width = 100
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final.'
            TabOrder = 2
          end
          object CkIniEntrega: TCheckBox
            Left = 10
            Top = -4
            Width = 109
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
          object TPIniEntrega: TDateTimePicker
            Left = 10
            Top = 17
            Width = 114
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 38604.000000000000000000
            Time = 0.601629953700467000
            TabOrder = 1
            OnChange = TPIniDataFChange
          end
          object TPFimEntrega: TDateTimePicker
            Left = 128
            Top = 17
            Width = 114
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 38604.000000000000000000
            Time = 0.601629953700467000
            TabOrder = 3
            OnChange = TPFimDataFChange
          end
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 181
      Width = 779
      Height = 268
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitTop = 242
      ExplicitHeight = 197
      object DBGrid1: TDBGrid
        Left = 0
        Top = 133
        Width = 779
        Height = 135
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsMPVIts
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DataF'
            Title.Alignment = taCenter
            Title.Caption = 'Data'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEMP'
            Title.Caption = 'Artigo'
            Width = 154
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Texto'
            Title.Caption = 'Observa'#231#227'o'
            Width = 251
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Alignment = taRightJustify
            Title.Caption = 'Quantidade'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Title.Alignment = taRightJustify
            Title.Caption = 'Pre'#231'o'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SubTotal'
            Title.Alignment = taRightJustify
            Title.Caption = 'Sub-total'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Desco'
            Title.Alignment = taRightJustify
            Title.Caption = 'Desconto'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Title.Alignment = taRightJustify
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCOEXT_V'
            Title.Caption = '$ Desc.Extra'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TOTAL'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ComissV_Per'
            Title.Caption = '% Comiss'#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COMISS_ITEM'
            Title.Alignment = taRightJustify
            Title.Caption = '$ Comiss'#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCOEXT_P'
            Title.Caption = '% Desc.Extra'
            Visible = True
          end>
      end
      object PnImprime: TPanel
        Left = 0
        Top = 0
        Width = 779
        Height = 133
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object RGAgrupa: TRadioGroup
          Left = 660
          Top = 0
          Width = 90
          Height = 133
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Agrupamentos: '
          ItemIndex = 0
          Items.Strings = (
            '0'
            '1'
            '2'
            '3')
          TabOrder = 0
          ExplicitHeight = 140
        end
        object RGOrdem4: TRadioGroup
          Left = 396
          Top = 0
          Width = 132
          Height = 133
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Ordem 4: '
          ItemIndex = 3
          Items.Strings = (
            'Cliente'
            'Artigo'
            'Vendedor'
            'Transportador'
            'Descri'#231#227'o'
            'Emiss'#227'o'
            'Entrega')
          TabOrder = 1
          OnClick = RGOrdem4Click
          ExplicitHeight = 140
        end
        object RGOrdem3: TRadioGroup
          Left = 264
          Top = 0
          Width = 132
          Height = 133
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Ordem 3: '
          ItemIndex = 2
          Items.Strings = (
            'Cliente'
            'Artigo'
            'Vendedor'
            'Transportador'
            'Descri'#231#227'o'
            'Emiss'#227'o'
            'Entrega')
          TabOrder = 2
          OnClick = RGOrdem3Click
          ExplicitHeight = 140
        end
        object RGOrdem2: TRadioGroup
          Left = 132
          Top = 0
          Width = 132
          Height = 133
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Ordem 2: '
          ItemIndex = 1
          Items.Strings = (
            'Cliente'
            'Artigo'
            'Vendedor'
            'Transportador'
            'Descri'#231#227'o'
            'Emiss'#227'o'
            'Entrega')
          TabOrder = 3
          OnClick = RGOrdem2Click
          ExplicitHeight = 140
        end
        object RGOrdem1: TRadioGroup
          Left = 0
          Top = 0
          Width = 132
          Height = 133
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Ordem 1: '
          ItemIndex = 0
          Items.Strings = (
            'Cliente'
            'Artigo'
            'Vendedor'
            'Transportador'
            'Descri'#231#227'o'
            'Emiss'#227'o'
            'Entrega')
          TabOrder = 4
          OnClick = RGOrdem1Click
          ExplicitHeight = 140
        end
        object RGOrdem5: TRadioGroup
          Left = 528
          Top = 0
          Width = 132
          Height = 133
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Ordem 5: '
          ItemIndex = 4
          Items.Strings = (
            'Cliente'
            'Artigo'
            'Vendedor'
            'Transportador'
            'Descri'#231#227'o'
            'Emiss'#227'o'
            'Entrega')
          TabOrder = 5
          OnClick = RGOrdem5Click
          ExplicitHeight = 140
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 779
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 728
      Top = 0
      Width = 51
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 6
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object BtOpcoes: TBitBtn
        Tag = 348
        Left = 10
        Top = 2
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOpcoesClick
      end
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 669
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 660
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 480
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Relat'#243'rios de Faturamentos de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 480
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Relat'#243'rios de Faturamentos de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 480
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Relat'#243'rios de Faturamentos de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 496
    Width = 779
    Height = 34
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 775
      Height = 17
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 27
      object LaAviso1: TLabel
        Left = 20
        Top = -2
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 19
        Top = -3
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 530
    Width = 779
    Height = 63
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 775
      Height = 46
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 597
        Top = 0
        Width = 178
        Height = 46
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 18
          Top = -3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 235
        Left = 15
        Top = 1
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Gera'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 138
        Top = 1
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtImprimeClick
      end
      object BtEntrega: TBitBtn
        Left = 258
        Top = 1
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Entrega'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtEntregaClick
      end
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 289
    Top = 69
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 317
    Top = 69
  end
  object QrMPVIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMPVItsAfterOpen
    BeforeClose = QrMPVItsBeforeClose
    OnCalcFields = QrMPVItsCalcFields
    SQL.Strings = (
      'SELECT mpv.Vendedor, mpv.Cliente, art.Nome NOMEMP, mpv.DataF,'
      
        'mpv.ComissV_Per, mpv.ComissV_Val,  mpv.DescoExtra, mpv.Valor VAL' +
        'ORMPV, mvi.*, '
      
        'CASE WHEN ven.Tipo=0 THEN ven.RazaoSocial ELSE ven.Nome END NOME' +
        'VENDEDOR,'
      
        'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOME' +
        'CLIENTE,'
      
        'CASE WHEN tra.Tipo=0 THEN tra.RazaoSocial ELSE tra.Nome END NOME' +
        'TRANSPOR'
      'FROM mpvits mvi'
      'LEFT JOIN mpv       mpv ON mpv.Codigo=mvi.Codigo'
      'LEFT JOIN entidades ven ON ven.Codigo=mpv.Vendedor'
      'LEFT JOIN entidades cli ON cli.Codigo=mpv.Cliente'
      'LEFT JOIN entidades tra ON tra.Codigo=mpv.Transp'
      'LEFT JOIN artigosgrupos art ON art.Codigo=mvi.MP'
      'WHERE mpv.Codigo>-1000')
    Left = 8
    Top = 409
    object QrMPVItsVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrMPVItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrMPVItsNOMEMP: TWideStringField
      FieldName = 'NOMEMP'
      Required = True
      Size = 50
    end
    object QrMPVItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMPVItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMPVItsMP: TIntegerField
      FieldName = 'MP'
      Required = True
    end
    object QrMPVItsQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPVItsPreco: TFloatField
      FieldName = 'Preco'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPVItsDesco: TFloatField
      FieldName = 'Desco'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPVItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPVItsTexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrMPVItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMPVItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMPVItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMPVItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMPVItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMPVItsNOMEVENDEDOR: TWideStringField
      FieldName = 'NOMEVENDEDOR'
      Size = 100
    end
    object QrMPVItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrMPVItsNOMETRANSPOR: TWideStringField
      FieldName = 'NOMETRANSPOR'
      Size = 100
    end
    object QrMPVItsSubTotal: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SubTotal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrMPVItsDataF: TDateField
      FieldName = 'DataF'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPVItsComissV_Per: TFloatField
      FieldName = 'ComissV_Per'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrMPVItsComissV_Val: TFloatField
      FieldName = 'ComissV_Val'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPVItsCOMISS_ITEM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'COMISS_ITEM'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrMPVItsDESCOEXT_V: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DESCOEXT_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrMPVItsTOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrMPVItsDescoExtra: TFloatField
      FieldName = 'DescoExtra'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPVItsVALORMPV: TFloatField
      FieldName = 'VALORMPV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPVItsDESCOEXT_P: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DESCOEXT_P'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
      Calculated = True
    end
    object QrMPVItsEntrega: TDateField
      FieldName = 'Entrega'
      Required = True
    end
    object QrMPVItsPronto: TDateField
      FieldName = 'Pronto'
      Required = True
    end
    object QrMPVItsStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object QrMPVItsFluxo: TIntegerField
      FieldName = 'Fluxo'
      Required = True
    end
    object QrMPVItsClasse: TWideStringField
      FieldName = 'Classe'
      Size = 15
    end
  end
  object DsMPVIts: TDataSource
    DataSet = QrMPVIts
    Left = 36
    Top = 409
  end
  object QrMP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM artigosgrupos'
      'ORDER BY Nome')
    Left = 689
    Top = 65
    object QrMPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMP: TDataSource
    DataSet = QrMP
    Left = 717
    Top = 65
  end
  object QrVendedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece6="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 289
    Top = 105
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 317
    Top = 105
  end
  object QrTransp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 689
    Top = 105
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransp: TDataSource
    DataSet = QrTransp
    Left = 717
    Top = 105
  end
  object PMEntrega: TPopupMenu
    Left = 324
    Top = 456
    object Alteraentrega1: TMenuItem
      Caption = '&Altera entrega'
      OnClick = Alteraentrega1Click
    end
    object Desfazentrega1: TMenuItem
      Caption = '&Desfaz entrega'
      OnClick = Desfazentrega1Click
    end
  end
  object frxDsMPVIts: TfrxDBDataset
    UserName = 'frxDsMPVIts'
    CloseDataSource = False
    DataSet = QrMPVIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 93
    Top = 409
  end
  object frxMPVIts: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.ConvertNulls = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38008.523706979200000000
    ReportOptions.LastChange = 42628.611314363430000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  Memo69.Visible := <VARF_COMI>;'
      '  Memo70.Visible := <VARF_COMI>;'
      '  Memo40.Visible := <VARF_COMI>;'
      '  Memo41.Visible := <VARF_COMI>;'
      '  Memo42.Visible := <VARF_COMI>;'
      '  Memo43.Visible := <VARF_COMI>;'
      '  Memo21.Visible := <VARF_COMI>;'
      '  Memo27.Visible := <VARF_COMI>;'
      '  Memo35.Visible := <VARF_COMI>;'
      '  Memo36.Visible := <VARF_COMI>;'
      '  Memo50.Visible := <VARF_COMI>;'
      '  Memo53.Visible := <VARF_COMI>;'
      '  //'
      '  CabealhodeGrupo1.Visible := <VARF_VISI1>;'
      '  CabealhodeGrupo2.Visible := <VARF_VISI2>;'
      '  CabealhodeGrupo3.Visible := <VARF_VISI3>;'
      '  //'
      '  CabealhodeGrupo1.Condition := <VARF_COND1>;'
      '  CabealhodeGrupo2.Condition := <VARF_COND2>;'
      '  CabealhodeGrupo3.Condition := <VARF_COND3>;'
      '  //'
      '  GroupFooter3.Visible := <VARF_VISI3>;'
      '  GroupFooter2.Visible := <VARF_VISI2>;'
      '  GroupFooter1.Visible := <VARF_VISI1>;'
      '  //'
      '  MemoCG1.Memo.Text := <VARF_MEMO1>;'
      '  MemoCG2.Memo.Text := <VARF_MEMO2>;'
      '  MemoCG3.Memo.Text := <VARF_MEMO3>;'
      'end.')
    OnGetValue = frxMPVItsGetValue
    Left = 65
    Top = 409
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMPVIts
        DataSetName = 'frxDsMPVIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -7
      Font.Name = 'Arial'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 359.055350000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMPVIts
        DataSetName = 'frxDsMPVIts'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 43.779530000000000000
          Width = 247.181102360000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NOMEMP"] - [frxDsMPVIts."Texto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 331.842519685039400000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataField = 'Qtde'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Qtde"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 387.779527559055100000
          Width = 48.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Preco"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 435.401574803149600000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataField = 'SubTotal'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."SubTotal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 491.716535430000000000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Desco"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 596.031496062992100000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."TOTAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 40.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."DataF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 651.968503937007900000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          OnBeforePrint = 'Memo40OnBeforePrint'
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."ComissV_Per"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 703.748031496063000000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          OnBeforePrint = 'Memo41OnBeforePrint'
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."COMISS_ITEM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 543.874015748031600000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."DESCOEXT_V"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Width = 40.881880000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Controle"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object CabealhoDePgina1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 33.897650000000000000
        Top = 139.842610000000000000
        Width = 755.906000000000000000
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 43.779530000000000000
          Top = 18.897650000000000000
          Width = 247.181102360000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 331.842519690000000000
          Top = 18.897650000000000000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 387.779527560000000000
          Top = 18.897650000000000000
          Width = 48.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 435.779527559055200000
          Top = 18.897650000000000000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 491.716535433070800000
          Top = 18.897650000000000000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Desc.item')
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 596.031496062992100000
          Top = 18.897650000000000000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 18.897650000000000000
          Width = 40.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 651.968503937007900000
          Top = 18.897650000000000000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          OnBeforePrint = 'Memo40OnBeforePrint'
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% COMIS.')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 703.748031496063000000
          Top = 18.897650000000000000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          OnBeforePrint = 'Memo41OnBeforePrint'
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ COMIS.')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 543.874015748031600000
          Top = 18.897650000000000000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Desc.ext.')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Top = 18.897650000000000000
          Width = 40.881880000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'OS')
          ParentFont = False
          WordWrap = False
        end
      end
      object CabealhoDeGrupo1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.000000000000000000
        Top = 234.330860000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'CabealhoDeGrupo1OnBeforePrint'
        Condition = 'frxDsMPVIts."NOMEMP"'
        object MemoCG1: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Width = 716.000000000000000000
          Height = 17.000000000000000000
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
      end
      object CabealhoDeGrupo2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.000000000000000000
        Top = 275.905690000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'CabealhoDeGrupo2OnBeforePrint'
        object MemoCG2: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Width = 716.000000000000000000
          Height = 17.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
      end
      object CabealhoDeGrupo3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.000000000000000000
        Top = 317.480520000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'CabealhoDeGrupo3OnBeforePrint'
        object MemoCG3: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Width = 716.000000000000000000
          Height = 17.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
      end
      object TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 36.000000000000000000
        Top = 570.709030000000000000
        Width = 755.906000000000000000
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 331.842519685039400000
          Top = 7.559060000000045000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Qtde">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 387.779527559055100000
          Top = 7.559060000000045000
          Width = 48.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsMPVIts."Qtde">,DadosMestre1)=0,0,SUM(<frxDsMPVIts' +
              '."Subtotal">,DadosMestre1) / SUM(<frxDsMPVIts."Qtde">,DadosMestr' +
              'e1))]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 435.401574803149600000
          Top = 7.559060000000045000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Subtotal">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 491.716535433070800000
          Top = 7.559060000000045000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Desco">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 596.031496062992100000
          Top = 7.559060000000045000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Total">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 651.968503937007900000
          Top = 7.559060000000045000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          OnBeforePrint = 'Memo40OnBeforePrint'
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsMPVIts."Valor">,DadosMestre1)=0,0,SUM(<frxDsMPVIt' +
              's."COMISS_ITEM">,DadosMestre1) / SUM(<frxDsMPVIts."Valor">,Dados' +
              'Mestre1) * 100)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 703.748031496063000000
          Top = 7.559060000000045000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          OnBeforePrint = 'Memo41OnBeforePrint'
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."COMISS_ITEM">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 543.874015748031600000
          Top = 7.559060000000045000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."DESCOEXT_V">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 487.559370000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GroupFooter1OnBeforePrint'
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 331.842519685039400000
          Top = 3.779530000000022000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Qtde">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 387.779527559055100000
          Top = 3.779530000000022000
          Width = 48.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsMPVIts."Qtde">,DadosMestre1) = 0, 0, SUM(<frxDsMP' +
              'VIts."Subtotal">,DadosMestre1) / SUM(<frxDsMPVIts."Qtde">,DadosM' +
              'estre1))]')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 435.401574803149600000
          Top = 3.779530000000022000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Subtotal">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 491.716535433070800000
          Top = 3.779530000000022000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Desco">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 596.031496062992100000
          Top = 3.779530000000022000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Total">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 651.968503937007900000
          Top = 3.779530000000022000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          OnBeforePrint = 'Memo40OnBeforePrint'
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsMPVIts."Valor">,DadosMestre1)=0,0,SUM(<frxDsMPVIt' +
              's."COMISS_ITEM">,DadosMestre1) / SUM(<frxDsMPVIts."Valor">,Dados' +
              'Mestre1) * 100)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 703.748031496063000000
          Top = 3.779530000000022000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          OnBeforePrint = 'Memo41OnBeforePrint'
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."COMISS_ITEM">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 543.874015748031600000
          Top = 3.779530000000022000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."DESCOEXT_V">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 442.205010000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GroupFooter2OnBeforePrint'
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 331.842519685039400000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Qtde">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 387.779527559055100000
          Width = 48.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsMPVIts."Qtde">,DadosMestre1) = 0, 0, SUM(<frxDsMP' +
              'VIts."Subtotal">,DadosMestre1) / SUM(<frxDsMPVIts."Qtde">,DadosM' +
              'estre1))]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 435.401574803149600000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Subtotal">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 491.716535433070800000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Desco">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 596.031496062992100000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Total">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 651.968503937007900000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          OnBeforePrint = 'Memo40OnBeforePrint'
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsMPVIts."Valor">,DadosMestre1)=0,0,SUM(<frxDsMPVIt' +
              's."COMISS_ITEM">,DadosMestre1) / SUM(<frxDsMPVIts."Valor">,Dados' +
              'Mestre1) * 100)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 703.748031496063000000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          OnBeforePrint = 'Memo41OnBeforePrint'
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."COMISS_ITEM">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 543.874015748031600000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."DESCOEXT_V">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 396.850650000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GroupFooter3OnBeforePrint'
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 331.842519685039400000
          Top = 3.779530000000022000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Qtde">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 387.779527559055100000
          Top = 3.779530000000022000
          Width = 48.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsMPVIts."Qtde">,DadosMestre1) = 0, 0, SUM(<frxDsMP' +
              'VIts."Subtotal">,DadosMestre1) / SUM(<frxDsMPVIts."Qtde">,DadosM' +
              'estre1))]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 435.401574803149600000
          Top = 3.779530000000022000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Subtotal">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 491.716535433070800000
          Top = 3.779530000000022000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Desco">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 596.031496062992100000
          Top = 3.779530000000022000
          Width = 56.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Total">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 651.968503937007900000
          Top = 3.779530000000022000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          OnBeforePrint = 'Memo40OnBeforePrint'
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsMPVIts."Valor">,DadosMestre1)=0,0,SUM(<frxDsMPVIt' +
              's."COMISS_ITEM">,DadosMestre1) / SUM(<frxDsMPVIts."Valor">,Dados' +
              'Mestre1) * 100)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 703.748031496063000000
          Top = 3.779530000000022000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          OnBeforePrint = 'Memo41OnBeforePrint'
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."COMISS_ITEM">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 543.874015748031600000
          Top = 3.779530000000022000
          Width = 52.000000000000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."DESCOEXT_V">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 631.181510000000000000
        Width = 755.906000000000000000
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 98.267780000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 591.354360000000000000
          Top = 1.102350000000001000
          Width = 160.220470000000000000
          Height = 16.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES] - [DATE], [TIME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 1.322820000000000000
          Width = 164.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'RELAT'#211'RIO DE VENDAS')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 174.000000000000000000
          Top = 21.102350000000000000
          Width = 577.921460000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 233.338590000000000000
          Top = 59.102350000000000000
          Width = 232.440940000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_MP]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 174.000000000000000000
          Top = 59.102350000000000000
          Width = 57.338590000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 174.000000000000000000
          Top = 40.307049999999990000
          Width = 57.338590000000000000
          Height = 16.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vendedor:')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 233.338590000000000000
          Top = 78.897650000000000000
          Width = 518.456710000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 174.000000000000000000
          Top = 78.897650000000000000
          Width = 57.338590000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 525.338590000000000000
          Top = 59.102350000000000000
          Width = 226.456710000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DESCRICAO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 466.000000000000000000
          Top = 59.102350000000000000
          Width = 57.338590000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 166.299212600000000000
          Height = 64.251968500000000000
          DataField = 'Logo'
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Width = 411.968525910000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 40.440920470000000000
          Width = 506.457020000000000000
          Height = 15.874015750000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_VENDEDOR]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrMPVImpOpc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT op.Codigo, si.Numero, se.Login, en.Nome NOMEFUNCIONARIO '
      'FROM mpvimpopc op'
      'LEFT JOIN senhas si ON si.Numero = op.Usuario'
      'LEFT JOIN entidades en ON en.Codigo=se.Funcionario'
      'ORDER BY NOMEFUNCIONARIO, Login')
    Left = 276
    Top = 400
    object QrMPVImpOpcNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrMPVImpOpcLogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrMPVImpOpcNOMEFUNCIONARIO: TWideStringField
      FieldName = 'NOMEFUNCIONARIO'
      Size = 100
    end
    object QrMPVImpOpcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
end
