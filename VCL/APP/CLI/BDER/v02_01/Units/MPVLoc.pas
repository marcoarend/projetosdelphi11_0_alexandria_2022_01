unit MPVLoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DBCtrls, Db, mySQLDbTables,
  dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage, UnDmkEnums,
  DmkDAC_PF;

type
  TFmMPVLoc = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    Label10: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    DBGrid1: TDBGrid;
    Splitter1: TSplitter;
    QrMPVIts: TmySQLQuery;
    DsMPVIts: TDataSource;
    DsMPV: TDataSource;
    QrMPV: TmySQLQuery;
    DBGrid4: TDBGrid;
    QrMPVNOMEVENDEDOR: TWideStringField;
    QrMPVNOMECLIENTE: TWideStringField;
    QrMPVNOMETRANSP: TWideStringField;
    QrMPVCodigo: TIntegerField;
    QrMPVCliente: TIntegerField;
    QrMPVTransp: TIntegerField;
    QrMPVVendedor: TIntegerField;
    QrMPVDataF: TDateField;
    QrMPVQtde: TFloatField;
    QrMPVValor: TFloatField;
    QrMPVComissV_Per: TFloatField;
    QrMPVComissV_Val: TFloatField;
    QrMPVLk: TIntegerField;
    QrMPVDataCad: TDateField;
    QrMPVDataAlt: TDateField;
    QrMPVUserCad: TIntegerField;
    QrMPVUserAlt: TIntegerField;
    QrMPVDescoExtra: TFloatField;
    QrMPVVolumes: TIntegerField;
    QrMPVObs: TWideStringField;
    QrMPVItsNOMEMP: TWideStringField;
    QrMPVItsCodigo: TIntegerField;
    QrMPVItsControle: TIntegerField;
    QrMPVItsMP: TIntegerField;
    QrMPVItsQtde: TFloatField;
    QrMPVItsPreco: TFloatField;
    QrMPVItsDesco: TFloatField;
    QrMPVItsValor: TFloatField;
    QrMPVItsTexto: TWideStringField;
    QrMPVItsLk: TIntegerField;
    QrMPVItsDataCad: TDateField;
    QrMPVItsDataAlt: TDateField;
    QrMPVItsUserCad: TIntegerField;
    QrMPVItsUserAlt: TIntegerField;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrMPVItsCorTxt: TWideStringField;
    QrMPVItsEntrega: TDateField;
    QrMPVItsPronto: TDateField;
    QrMPVItsStatus: TIntegerField;
    QrMPVItsFluxo: TIntegerField;
    QrMPVItsClasse: TWideStringField;
    QrMPVItsPedido: TIntegerField;
    QrMPVItsEspesTxt: TWideStringField;
    QrMPVItsM2Pedido: TFloatField;
    QrMPVItsPrecoPed: TFloatField;
    QrMPVItsValorPed: TFloatField;
    QrMPVItsPecas: TFloatField;
    QrMPVItsUnidade: TIntegerField;
    QrMPVItsObserv: TWideMemoField;
    QrMPVItsAlterWeb: TSmallintField;
    QrMPVItsDescricao: TWideMemoField;
    QrMPVItsComplementacao: TWideMemoField;
    QrMPVItsAtivo: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrMPVItsPedidCli: TWideStringField;
    EdPedidCli: TEdit;
    CkPedidCli: TCheckBox;
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMPVAfterScroll(DataSet: TDataSet);
    procedure QrMPVBeforeClose(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CkPedidCliClick(Sender: TObject);
    procedure EdClienteRedefinido(Sender: TObject);
    procedure EdPedidCliChange(Sender: TObject);

  private
    { Private declarations }
    procedure PesquisaPedidos();
  public
    { Public declarations }
    FSelCod: Integer;
  end;

  var
  FmMPVLoc: TFmMPVLoc;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmMPVLoc.BtOKClick(Sender: TObject);
begin
  if QrMPVIts.State = dsInactive then
  begin
    Geral.MB_Aviso('Nenhum pedido foi selecionado!');
    Exit;
  end else begin
    //FmMPVn.FMPVLoc := QrMPVItsCodigo.Value;
    FSelCod := QrMPVItsCodigo.Value;
    Close;
  end;
end;

procedure TFmMPVLoc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPVLoc.CkPedidCliClick(Sender: TObject);
begin
  PesquisaPedidos();
end;

procedure TFmMPVLoc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPVLoc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPVLoc.PesquisaPedidos();
var
  Cliente: Integer;
  PedidCli: String;
begin
  QrMPV.Close;
  Cliente  := Geral.IMV(EdCliente.Text);
  PedidCli := EdPedidCli.Text;
  if CkPedidCli.Checked then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrMPV, Dmod.MyDB, [
    'SELECT  ',
    'CASE WHEN ve.Tipo=0 THEN ve.RazaoSocial ',
    'ELSE ve.Nome END NOMEVENDEDOR,  ',
    'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ',
    'ELSE cl.Nome END NOMECLIENTE,  ',
    'CASE WHEN tr.Tipo=0 THEN tr.RazaoSocial ',
    'ELSE tr.Nome END NOMETRANSP, ',
    'mpv.*  ',
    'FROM mpv mpv ',
    'LEFT JOIN mpvits mpi ON mpv.Codigo=mpi.Codigo ',
    'LEFT JOIN mpp mpp ON mpp.Codigo=mpi.Pedido ',
    'LEFT JOIN entidades cl ON cl.Codigo=mpv.Cliente ',
    'LEFT JOIN entidades tr ON tr.Codigo=mpv.Transp ',
    'LEFT JOIN entidades ve ON ve.Codigo=mpv.Vendedor ',
    'WHERE mpv.Cliente=' + Geral.FF0(Cliente),
    'AND mpp.PedidCli LIKE "' + PedidCli + '" ',
    'ORDER BY DataF DESC ',
    '']);
  end else
  begin
    if Cliente <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrMPV, Dmod.MyDB, [
      'SELECT  ',
      'CASE WHEN ve.Tipo=0 THEN ve.RazaoSocial ',
      'ELSE ve.Nome END NOMEVENDEDOR,  ',
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ',
      'ELSE cl.Nome END NOMECLIENTE,  ',
      'CASE WHEN tr.Tipo=0 THEN tr.RazaoSocial ',
      'ELSE tr.Nome END NOMETRANSP, ',
      'mpv.*  ',
      'FROM mpv mpv ',
      'LEFT JOIN entidades cl ON cl.Codigo=mpv.Cliente ',
      'LEFT JOIN entidades tr ON tr.Codigo=mpv.Transp ',
      'LEFT JOIN entidades ve ON ve.Codigo=mpv.Vendedor ',
      'WHERE mpv.Cliente=' + Geral.FF0(Cliente),
      'ORDER BY DataF DESC ',
      '']);
    end;
  end;
end;

procedure TFmMPVLoc.EdClienteRedefinido(Sender: TObject);
begin
  PesquisaPedidos();
end;

procedure TFmMPVLoc.EdPedidCliChange(Sender: TObject);
begin
  PesquisaPedidos();
end;

procedure TFmMPVLoc.QrMPVAfterScroll(DataSet: TDataSet);
begin
  QrMPVIts.Close;
  QrMPVIts.Params[0].AsInteger := QrMPVCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrMPVIts, Dmod.MyDB);
  //
end;

procedure TFmMPVLoc.QrMPVBeforeClose(DataSet: TDataSet);
begin
  QrMPVIts.Close;
end;

procedure TFmMPVLoc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FSelCod := 0;
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
end;

procedure TFmMPVLoc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Geral.WriteAppKey('Pesquisa\MPVLoc\Cliente', Application.Title,
    EdCliente.Text, ktString, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('Pesquisa\MPVLoc\MPV', Application.Title,
    QrMPVCodigo.Value, ktInteger, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('Pesquisa\MPVLoc\MPVIts', Application.Title,
    QrMPVItsControle.Value, ktInteger, HKEY_LOCAL_MACHINE);
end;

end.
