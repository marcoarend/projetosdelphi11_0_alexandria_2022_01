unit PQForEmb;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, Mask, DBCtrls, dmkEdit, DB,
  mySQLDbTables, dmkRadioGroup, dmkEditCB, dmkDBLookupComboBox, dmkGeral,
  dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmPQForEmb = class(TForm)
    DsEmbalagens: TDataSource;
    QrEmbalagens: TmySQLQuery;
    Panel3: TPanel;
    CBEmbalagem: TdmkDBLookupComboBox;
    EdEmbalagem: TdmkEditCB;
    Label1: TLabel;
    QrEmbalagensCodigo: TIntegerField;
    QrEmbalagensNome: TWideStringField;
    EdcProd: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdObservacao: TdmkEdit;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmPQForEmb: TFmPQForEmb;

implementation

uses UnMyObjects, MyDBCheck, UMySQLModule, Module, Embalagens, PQ, UnInternalConsts;

{$R *.DFM}

procedure TFmPQForEmb.BtOKClick(Sender: TObject);
var
  cProd, Observacao: String;
  Embalagem, Controle: Integer;
begin
  cProd      := EdcProd.Text;
  Observacao := EdObservacao.Text;
  Embalagem  := EdEmbalagem.ValueVariant;
  Controle   := FmPQ.QrPQForControle.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pqforemb', False, [
  'Embalagem', 'Observacao'], ['cProd', 'Controle'], [
  Embalagem, Observacao], [cProd, Controle], True) then
  begin
    FmPQ.ReopenPQForEmb(cProd);
    Close;
  end;
end;

procedure TFmPQForEmb.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQForEmb.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQForEmb.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
end;

procedure TFmPQForEmb.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQForEmb.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEmbalagens, FmEmbalagens, afmoNegarComAviso) then
  begin
    FmEmbalagens.ShowModal;
    FmEmbalagens.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrEmbalagens.Close;
      UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
      EdEmbalagem.ValueVariant := VAR_CADASTRO;
      CBEmbalagem.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

end.
