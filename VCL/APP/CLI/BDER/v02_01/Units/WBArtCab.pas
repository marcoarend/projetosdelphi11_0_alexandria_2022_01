unit WBArtCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkMemo;

type
  TFmWBArtCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrWBArtCab: TmySQLQuery;
    DsWBArtCab: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdGraGruX: TdmkEdit;
    Label9: TLabel;
    EdTxtNomeTamCor: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrWBArtCabNO_Fluxo: TWideStringField;
    QrWBArtCabNO_ReceiRecu: TWideStringField;
    QrWBArtCabNO_ReceiRefu: TWideStringField;
    QrWBArtCabNO_ReceiAcab: TWideStringField;
    QrWBArtCabGraGruX: TIntegerField;
    QrWBArtCabFluxo: TIntegerField;
    QrWBArtCabReceiRecu: TIntegerField;
    QrWBArtCabReceiRefu: TIntegerField;
    QrWBArtCabReceiAcab: TIntegerField;
    QrWBArtCabLk: TIntegerField;
    QrWBArtCabDataCad: TDateField;
    QrWBArtCabDataAlt: TDateField;
    QrWBArtCabUserCad: TIntegerField;
    QrWBArtCabUserAlt: TIntegerField;
    QrWBArtCabAlterWeb: TSmallintField;
    QrWBArtCabAtivo: TSmallintField;
    QrWBArtCabGraGru1: TIntegerField;
    QrWBArtCabNO_PRD_TAM_COR: TWideStringField;
    QrFluxos: TmySQLQuery;
    QrFluxosCodigo: TIntegerField;
    QrFluxosNome: TWideStringField;
    DsFluxos: TDataSource;
    Label8: TLabel;
    EdFluxo: TdmkEditCB;
    CBFluxo: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdReceiRecu: TdmkEditCB;
    CBReceiRecu: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdReceiRefu: TdmkEditCB;
    CBReceiRefu: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdReceiAcab: TdmkEditCB;
    CBReceiAcab: TdmkDBLookupComboBox;
    QrReceiRecu: TmySQLQuery;
    QrReceiRecuNumero: TIntegerField;
    QrReceiRecuNome: TWideStringField;
    DsReceiRecu: TDataSource;
    QrReceirefu: TmySQLQuery;
    DsReceiRefu: TDataSource;
    QrReceirefuNumero: TIntegerField;
    QrReceirefuNome: TWideStringField;
    QrReceiAcab: TmySQLQuery;
    QrReceiAcabNumero: TIntegerField;
    QrReceiAcabNome: TWideStringField;
    DsReceiAcab: TDataSource;
    Label6: TLabel;
    EdTxtMPs: TdmkEdit;
    Label10: TLabel;
    MeObserva: TdmkMemo;
    QrWBArtCabTxtMPs: TWideStringField;
    QrWBArtCabObserva: TWideMemoField;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBMemo1: TDBMemo;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWBArtCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWBArtCabBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, GraGruX: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmWBArtCab: TFmWBArtCab;
const
  FFormatFloat = '00000';
  FCo_Codigo = 'GraGruX';
  FCo_Nome = 'CONCAT(gg1.Nome, ' + sLineBreak +
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ' + sLineBreak +
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))';
  FCo_Join =
  'LEFT JOIN gragrux   ggx ON ggx.Controle=wac.GraGruX ' + sLineBreak +
  'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC ' + sLineBreak +
  'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad ' + sLineBreak +
  'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ' + sLineBreak +
  'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1';

implementation

uses UnMyObjects, Module, UnGrade_Jan, DMkDAC_PF;

{$R *.DFM}

procedure TFmWBArtCab.SbQueryClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := CuringaLoc.CriaForm(FCo_Codigo, FCo_Nome, 'wbartcab wac', Dmod.MyDB,
    '', False, FCo_Join);
  //
  if GraGruX <> 0 then
    LocCod(GraGruX, GraGruX);
end;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWBArtCab.LocCod(Atual, GraGruX: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, GraGruX);
end;

procedure TFmWBArtCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWBArtCabGraGruX.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWBArtCab.DefParams;
begin
  VAR_GOTOTABELA := 'wbartcab';
  VAR_GOTOMYSQLTABLE := QrWBArtCab;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := FCo_Codigo;
  VAR_GOTONOME := FCo_Nome;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT flu.Nome NO_Fluxo, fo1.Nome NO_ReceiRecu,');
  VAR_SQLx.Add('fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab,');
  VAR_SQLx.Add('wac.*,');
  VAR_SQLx.Add('ggx.GraGru1, CONCAT(gg1.Nome,');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))');
  VAR_SQLx.Add('NO_PRD_TAM_COR');
  VAR_SQLx.Add('FROM wbartcab wac');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=wac.GraGruX');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  VAR_SQLx.Add('LEFT JOIN fluxos  flu ON flu.Codigo=wac.Fluxo');
  VAR_SQLx.Add('LEFT JOIN formulas fo1 ON fo1.Numero=wac.ReceiRecu');
  VAR_SQLx.Add('LEFT JOIN formulas fo2 ON fo2.Numero=wac.ReceiRefu');
  VAR_SQLx.Add('LEFT JOIN formulas ti1 ON ti1.Numero=wac.ReceiAcab');
  VAR_SQLx.Add('');
  //
  VAR_SQLx.Add('WHERE wac.GraGruX > 0');
  //
  VAR_SQL1.Add('AND wac.GraGruX=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE ' + FCo_Nome + ' LIKE :P0 ');
  //
end;

procedure TFmWBArtCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWBArtCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWBArtCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWBArtCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWBArtCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWBArtCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWBArtCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWBArtCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWBArtCab.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWBArtCab, [PnDados],
  [PnEdita], EdFluxo, ImgTipo, 'wbartcab');
end;

procedure TFmWBArtCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWBArtCabGraGruX.Value;
  Close;
end;

procedure TFmWBArtCab.BtConfirmaClick(Sender: TObject);
var
  GraGruX, Fluxo, ReceiRecu, ReceiRefu, ReceiAcab: Integer;
  TxtMPs, Observa: String;
begin
  GraGruX        := EdGraGruX.ValueVariant;
  Fluxo          := EdFluxo.ValueVariant;
  ReceiRecu      := EdReceiRecu.ValueVariant;
  ReceiRefu      := EdReceiRefu.ValueVariant;
  ReceiAcab      := EdReceiAcab.ValueVariant;
  TxtMPs         := EdTxtMPs.Text;
  Observa        := MeObserva.Text;

  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'wbartcab', False, [
  'Fluxo', 'ReceiRecu', 'ReceiRefu',
  'ReceiAcab', 'TxtMPs', 'Observa'], [
  'GraGruX'], [
  Fluxo, ReceiRecu, ReceiRefu,
  ReceiAcab, TxtMPs, Observa], [
  GraGruX], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(GraGruX, GraGruX);
  end;
end;

procedure TFmWBArtCab.BtDesisteClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(GraGruX, Dmod.MyDB, 'wbartcab', FCo_Codigo);
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmWBArtCab.BtIncluiClick(Sender: TObject);
var
  DefinedSQL: String;
  Qry: TmySQLQuery;
  GraGruX: Integer;
begin
{
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWBArtCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'wbartcab');
}
  (* Mudado em: 2017-02-22
  DefinedSQL := Geral.ATS([
    'SELECT ggx.Controle _Codigo, ',
    'CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) _Nome ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) LIKE %s ',
    'ORDER BY _Nome ',
    '']);
  FmMeuDBUses.PesquisaNome('', '', '', DefinedSQL);

  GraGruX := VAR_CADASTRO;
  *)
  DefinedSQL := Geral.ATS([
    'SELECT gg1.CodUsu, gg1.Nivel1, ',
    'gg1.GraTamCad, gg1.Nome ',
    'FROM gragru1 gg1 ',
    'ORDER BY gg1.Nome ',
    '']);
  GraGruX := Grade_Jan.MostraFormGraGruPesq1(DefinedSQL);

  if GraGruX <> 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      //ShowMessage(Geral.FF0(VAR_CADASTRO));
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GraGruX ',
      'FROM wbartcab ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      '']);
      if Qry.FieldByName('GraGruX').AsInteger = GraGruX then
      begin
        Geral.MB_Aviso('O reduzido ' + Geral.FF0(GraGruX) +
        ' j� estava adicionado � configura��o de artigos!');
      end else
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'wbartcab', False, [
        ], [
        'GraGruX'], [
        ], [
        GraGruX], True);
      end;
      //
      LocCod(GraGruX, GraGruX);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmWBArtCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //GBEdita.Align := alClient;
  //GBDados.Align := alClient;
  MeObserva.Align := alClient;
  DBMemo1.Align := alClient;
  CriaOForm;
  //
  UnDmkDAC_PF.AbreQuery(QrFluxos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReceiRecu, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReceiRefu, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReceiAcab, Dmod.MyDB);
  //
end;

procedure TFmWBArtCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWBArtCabGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmWBArtCab.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o implementada para esta janela!');
end;

procedure TFmWBArtCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Tipo de pesquisa n�o implementado para esta janela!' +
    sLineBreak + 'Utilize a pesquisa parcial!');
end;

procedure TFmWBArtCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrWBArtCabGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmWBArtCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWBArtCab.QrWBArtCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWBArtCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWBArtCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBArtCab.QrWBArtCabBeforeOpen(DataSet: TDataSet);
begin
  QrWBArtCabGraGruX.DisplayFormat := FFormatFloat;
end;

end.

