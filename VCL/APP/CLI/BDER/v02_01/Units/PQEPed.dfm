object FmPQEPed: TFmPQEPed
  Left = 366
  Top = 203
  Caption = 'QUI-ENTRA-004 :: Entrada de PQ por Pedido'
  ClientHeight = 467
  ClientWidth = 621
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 621
    Height = 311
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 49
      Width = 621
      Height = 262
      Align = alClient
      DataSource = DsPedidosIts
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMEINSUMO'
          Title.Caption = 'Insumo'
          Width = 400
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotalPeso'
          Title.Caption = 'Peso kg'
          Width = 79
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotalCusto'
          Title.Caption = 'Valor'
          Width = 81
          Visible = True
        end>
    end
    object PainelDados: TPanel
      Left = 0
      Top = 0
      Width = 621
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label2: TLabel
        Left = 316
        Top = 4
        Width = 36
        Height = 13
        Caption = 'Pedido:'
      end
      object CBFornecedor: TDBLookupComboBox
        Left = 12
        Top = 20
        Width = 300
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornece
        TabOrder = 0
        OnClick = CBFornecedorClick
      end
      object CBPedido: TDBLookupComboBox
        Left = 316
        Top = 20
        Width = 129
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Codigo'
        ListSource = DsPedidos
        TabOrder = 1
      end
      object CBEncerrados: TCheckBox
        Left = 464
        Top = 20
        Width = 121
        Height = 17
        Caption = 'Incluir encerrados.'
        TabOrder = 2
        OnClick = CBEncerradosClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 621
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 573
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 525
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 317
        Height = 32
        Caption = 'Entrada de PQ por Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 317
        Height = 32
        Caption = 'Entrada de PQ por Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 317
        Height = 32
        Caption = 'Entrada de PQ por Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 359
    Width = 621
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 617
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 403
    Width = 621
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 617
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 473
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrFornece: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT fo.Codigo,'
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOMEENTIDADE'
      'FROM entidades fo, PQPed pp'
      'WHERE pp.Fornece=fo.Codigo'
      'AND pp.Sit <:P0'
      'AND fo.Fornece2='#39'V'#39
      'ORDER BY NOMEENTIDADE')
    Left = 136
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      FixedChar = True
      Size = 128
    end
  end
  object QrPedidos: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPedidosAfterScroll
    SQL.Strings = (
      'SELECT Codigo'
      'FROM pqped'
      'WHERE Fornece=:P0'
      'AND Sit<:P1'
      '')
    Left = 236
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPedidosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pqped.Codigo'
      DisplayFormat = '000'
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 168
    Top = 4
  end
  object DsPedidos: TDataSource
    DataSet = QrPedidos
    Left = 264
  end
  object QrPedidosIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT i.*, p.Nome NOMEINSUMO'
      'FROM pqpedits i, PQ p'
      'WHERE i.Codigo=:P0'
      'AND p.Codigo=i.Insumo'
      '')
    Left = 80
    Top = 236
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPedidosItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pqpedits.Codigo'
    end
    object QrPedidosItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'DBMBWET.pqpedits.Controle'
    end
    object QrPedidosItsConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'DBMBWET.pqpedits.Conta'
    end
    object QrPedidosItsInsumo: TIntegerField
      FieldName = 'Insumo'
      Origin = 'DBMBWET.pqpedits.Insumo'
    end
    object QrPedidosItsVolumes: TIntegerField
      FieldName = 'Volumes'
      Origin = 'DBMBWET.pqpedits.Volumes'
    end
    object QrPedidosItsPesoVB: TFloatField
      FieldName = 'PesoVB'
      Origin = 'DBMBWET.pqpedits.PesoVB'
    end
    object QrPedidosItsPesoVL: TFloatField
      FieldName = 'PesoVL'
      Origin = 'DBMBWET.pqpedits.PesoVL'
    end
    object QrPedidosItsValorItem: TFloatField
      FieldName = 'ValorItem'
      Origin = 'DBMBWET.pqpedits.ValorItem'
    end
    object QrPedidosItsIPI: TFloatField
      FieldName = 'IPI'
      Origin = 'DBMBWET.pqpedits.IPI'
    end
    object QrPedidosItsTotalCusto: TFloatField
      FieldName = 'TotalCusto'
      Origin = 'DBMBWET.pqpedits.TotalCusto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPedidosItsTotalPeso: TFloatField
      FieldName = 'TotalPeso'
      Origin = 'DBMBWET.pqpedits.TotalPeso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPedidosItsPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'DBMBWET.pqpedits.Preco'
    end
    object QrPedidosItsICMS: TFloatField
      FieldName = 'ICMS'
      Origin = 'DBMBWET.pqpedits.ICMS'
    end
    object QrPedidosItsEntrada: TFloatField
      FieldName = 'Entrada'
      Origin = 'DBMBWET.pqpedits.Entrada'
    end
    object QrPedidosItsPrazo: TWideStringField
      FieldName = 'Prazo'
      Origin = 'DBMBWET.pqpedits.Prazo'
      Size = 128
    end
    object QrPedidosItscfin: TFloatField
      FieldName = 'cfin'
      Origin = 'DBMBWET.pqpedits.cfin'
    end
    object QrPedidosItsNOMEINSUMO: TWideStringField
      FieldName = 'NOMEINSUMO'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
  end
  object DsPedidosIts: TDataSource
    DataSet = QrPedidosIts
    Left = 108
    Top = 236
  end
end
