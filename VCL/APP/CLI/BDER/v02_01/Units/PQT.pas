unit PQT;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, DmkDAC_PF,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, Menus, frxClass, frxDBSet, Variants, dmkGeral, UnDmkProcFunc,
  dmkDBLookupComboBox, dmkEditCB, dmkDBEdit, dmkEdit, dmkEditDateTimePicker,
  dmkLabel, dmkImage, UnDmkEnums, UnEmpresas;

type
  TFmPQT = class(TForm)
    PnDados: TPanel;
    DsPQT: TDataSource;
    QrPQT: TmySQLQuery;
    PnEdita: TPanel;
    PnEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    PnData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    EdETE_m3: TdmkEdit;
    QrPQTCodigo: TIntegerField;
    QrPQTETE_m3: TFloatField;
    QrPQTLodo: TFloatField;
    QrPQTHorasTrab: TFloatField;
    QrPQTDataB: TDateField;
    QrPQTLk: TIntegerField;
    QrPQTDataCad: TDateField;
    QrPQTDataAlt: TDateField;
    QrPQTUserCad: TIntegerField;
    QrPQTUserAlt: TIntegerField;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    EdLodo: TdmkEdit;
    EdHorasT: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    TPDataB: TdmkEditDateTimePicker;
    QrPQTIts: TmySQLQuery;
    QrPQTItsDataX: TDateField;
    QrPQTItsTipo: TSmallintField;
    QrPQTItsCliOrig: TIntegerField;
    QrPQTItsCliDest: TIntegerField;
    QrPQTItsInsumo: TIntegerField;
    QrPQTItsPeso: TFloatField;
    QrPQTItsValor: TFloatField;
    QrPQTItsOrigemCodi: TIntegerField;
    QrPQTItsOrigemCtrl: TIntegerField;
    QrPQTItsNOMEPQ: TWideStringField;
    QrPQTItsGrupoQuimico: TIntegerField;
    QrPQTItsNOMEGRUPO: TWideStringField;
    DsPQTIts: TDataSource;
    QrPQ: TmySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    DsCI: TDataSource;
    QrCI: TmySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    QrSaldo: TmySQLQuery;
    QrSaldoPeso: TFloatField;
    QrSaldoValor: TFloatField;
    QrSaldoCUSTO: TFloatField;
    QrSaldoCI: TIntegerField;
    QrSaldoPQ: TIntegerField;
    DsSaldo: TDataSource;
    PnInsumos: TPanel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    EdPesoAdd: TdmkEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    EdSaldoFut: TdmkEdit;
    PMInclui: TPopupMenu;
    IncluiNovaBaixa1: TMenuItem;
    IncluiInsumobaixaatual1: TMenuItem;
    Label21: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label24: TLabel;
    DBEdit12: TDBEdit;
    Label25: TLabel;
    QrPQTValorHora: TFloatField;
    QrPQTCustoHora: TFloatField;
    QrPQTCustoInsumo: TFloatField;
    QrPQTCustoTotal: TFloatField;
    QrPQTCUSTO_m3: TFloatField;
    QrSumPQ: TmySQLQuery;
    QrSumPQCUSTO: TFloatField;
    PMExclui: TPopupMenu;
    ExcluiItemdabaixa1: TMenuItem;
    ExcluiBaixa1: TMenuItem;
    PMAltera: TPopupMenu;
    AlteraBaixa1: TMenuItem;
    EdValorHora: TdmkEdit;
    Label26: TLabel;
    PMImprime: TPopupMenu;
    Estabaixa1: TMenuItem;
    Porperodo1: TMenuItem;
    Outros1: TMenuItem;
    QrPeriodo: TmySQLQuery;
    QrPeriodoCodigo: TIntegerField;
    QrPeriodoETE_m3: TFloatField;
    QrPeriodoLodo: TFloatField;
    QrPeriodoHorasTrab: TFloatField;
    QrPeriodoDataB: TDateField;
    QrPeriodoLk: TIntegerField;
    QrPeriodoDataCad: TDateField;
    QrPeriodoDataAlt: TDateField;
    QrPeriodoUserCad: TIntegerField;
    QrPeriodoUserAlt: TIntegerField;
    QrPeriodoValorHora: TFloatField;
    QrPeriodoCustoHora: TFloatField;
    QrPeriodoCustoInsumo: TFloatField;
    QrPeriodoCustoTotal: TFloatField;
    QrPeriodoCUSTO_m3: TFloatField;
    frxQUI_RECEI_010_A: TfrxReport;
    frxDsPQTIts: TfrxDBDataset;
    frxDsPQT: TfrxDBDataset;
    frxPeriodo: TfrxReport;
    frxDsPeriodo: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBConfirm2: TGroupBox;
    Panel6: TPanel;
    Panel7: TPanel;
    BtConfirma2: TBitBtn;
    BtDesiste2: TBitBtn;
    GBControle: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Label14: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrPQTItsEmpresa: TIntegerField;
    QrLodo: TMySQLQuery;
    DsLodo: TDataSource;
    QrLodoPlaca: TWideStringField;
    QrLodoNO_Unidade: TWideStringField;
    QrLodoNO_Motorista: TWideStringField;
    QrLodoNO_LOC_CEN: TWideStringField;
    QrLodoCodigo: TIntegerField;
    QrLodoControle: TIntegerField;
    QrLodoVeiculo: TIntegerField;
    QrLodoMotorista: TIntegerField;
    QrLodoLocal: TIntegerField;
    QrLodoQtde: TFloatField;
    QrLodoUnidade: TIntegerField;
    QrLodoAtivo: TSmallintField;
    PnDBGInsumos: TPanel;
    DBGInsumos: TDBGrid;
    Label15: TLabel;
    PnDBGLodo: TPanel;
    Label16: TLabel;
    DBGLodo: TDBGrid;
    Incluiviagemdelodo1: TMenuItem;
    AletaviagemdeLodo1: TMenuItem;
    ExcluiviagemdeLodo1: TMenuItem;
    QrLodoSigla: TWideStringField;
    N1: TMenuItem;
    ransportedeLodo1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPQTAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrPQTAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPQTBeforeOpen(DataSet: TDataSet);
    procedure EdPQChange(Sender: TObject);
    procedure EdCIChange(Sender: TObject);
    procedure EdPesoAddExit(Sender: TObject);
    procedure QrSaldoAfterOpen(DataSet: TDataSet);
    procedure BtDesiste2Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure IncluiNovaBaixa1Click(Sender: TObject);
    procedure IncluiInsumobaixaatual1Click(Sender: TObject);
    procedure QrPQTCalcFields(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure ExcluiItemdabaixa1Click(Sender: TObject);
    procedure AlteraBaixa1Click(Sender: TObject);
    procedure Estabaixa1Click(Sender: TObject);
    procedure Porperodo1Click(Sender: TObject);
    procedure Outros1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frBaixaAtualUserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure QrPeriodoCalcFields(DataSet: TDataSet);
    procedure frxQUI_RECEI_010_AGetValue(const VarName: String;
      var Value: Variant);
    procedure SbNovoClick(Sender: TObject);
    procedure ExcluiBaixa1Click(Sender: TObject);
    procedure CkDtCorrApoClick(Sender: TObject);
    procedure Incluiviagemdelodo1Click(Sender: TObject);
    procedure AletaviagemdeLodo1Click(Sender: TObject);
    procedure ExcluiviagemdeLodo1Click(Sender: TObject);
    procedure QrPQTBeforeClose(DataSet: TDataSet);
    procedure ransportedeLodo1Click(Sender: TObject);
  private
    FPQTIts: Integer;
    FEmpresa: Integer;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenPQTIts;
    procedure ReopenPQTLodo();
    procedure CalculaSaldoFuturo;
    procedure AtualizaCusto;
    procedure MostraFormPQTLodo(SQLType: TSQLType);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmPQT: TFmPQT;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, PQx, Principal, GetPeriodo, UnPQ_PF, ModuleGeral,
  MyDBCheck, PQTLodo, PQTLodoImp;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPQT.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQT.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPQTCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPQT.DefParams;
begin
  VAR_GOTOTABELA := 'pqt';
  VAR_GOTOMYSQLTABLE := QrPQT;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := 'Empresa=' + Geral.FF0(FEmpresa);

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM pqt');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  VAR_SQLx.Add('AND Empresa=' + Geral.FF0(FEmpresa));
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmPQT.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      GBControle.Visible := True;
      PnDados.Visible    := True;
      PnEdita.Visible    := False;
      PnInsumos.Visible  := False;
      GBConfirm2.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      GBControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := '';
        TPDataB.Date  := Date;
        EdETE_m3.Text := '';
        EdLodo.Text   := '';
        EdHorasT.Text := '';
        EdValorHora.Text := '';
      end else begin
        EdCodigo.Text := IntToStr(QrPQTCodigo.Value);
        TPDataB.Date  := QrPQTDataB.Value;
        EdETE_m3.Text := Geral.FFT(QrPQTETE_m3.Value, 3, siPositivo);
        EdLodo.Text   := Geral.FFT(QrPQTLodo.Value, 3, siPositivo);
        EdHorasT.Text := Geral.FFT(QrPQTHorasTrab.Value, 4, siPositivo);
        EdValorHora.Text := Geral.FFT(QrPQTValorHora.Value, 4, siPositivo);
      end;
      TPDataB.SetFocus;
    end;
    2:
    begin
      // ini 2023-09-09
      // permitir criar cabe�alho para o lodo, mas impedir baixa de PQ!
      if UnPQx.ImpedePeloBalanco(QrPQTDataB.Value) then Exit;
      // fim 2023-09-09
      //
      PnInsumos.Visible  := True;
      GBConfirm2.Visible := True;
      GBControle.Visible := False;
      //
      EdCI.Text := IntToStr(Dmod.QrMasterDono.Value);
      CBCI.KeyValue := Dmod.QrMasterDono.Value;
      EdCI.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmPQT.MostraFormPQTLodo(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmPQTLodo, FmPQTLodo, afmoNegarComAviso) then
  begin
    FmPQTLodo.ImgTipo.SQLType := SQLType;
    FmPQTLodo.FQrIts := QrLodo;
    FmPQTLodo.EdCodigo.ValueVariant := QrPQTCodigo.Value;
    if SQLTYpe = stUpd then
    begin
      FmPQTLodo.EdControle.ValueVariant := QrLodoControle.Value;
      FmPQTLodo.EdVeiculo.ValueVariant := QrLodoVeiculo.Value;
      FmPQTLodo.CBVeiculo.KeyValue := QrLodoVeiculo.Value;
      FmPQTLodo.EdMotorista.ValueVariant := QrLodoMotorista.Value;
      FmPQTLodo.CBMotorista.KeyValue := QrLodoMotorista.Value;
      FmPQTLodo.EdLocal.ValueVariant := QrLodoLocal.Value;
      FmPQTLodo.CBLocal.KeyValue := QrLodoLocal.Value;
      FmPQTLodo.EdQtde.ValueVariant := QrLodoQtde.Value;
      FmPQTLodo.EdUnidMed.ValueVariant := QrLodoUnidade.Value;
      FmPQTLodo.CBUnidMed.KeyValue := QrLodoUnidade.Value;
    end;
    FmPQTLodo.ShowModal;
    FmPQTLodo.Destroy;
  end;
end;

procedure TFmPQT.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPQT.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPQT.ransportedeLodo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPQTLodoImp, FmPQTLodoImp, afmoNegarComAviso) then
  begin
    FmPQTLodoImp.ShowModal;
    FmPQTLodoImp.Destroy;
  end;
end;

procedure TFmPQT.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQT.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQT.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQT.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQT.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQT.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmPQT.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmPQT.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPQTCodigo.Value;
  Close;
end;

procedure TFmPQT.BtConfirmaClick(Sender: TObject);
(*var
  Codigo : Integer;
begin
  if UnPQx.ImpedePeloBalanco(TPDataB.Date) then Exit;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO pqt SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'pqt', 'pqt', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE pqt SET ');
    Codigo := QrPQTCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('ETE_m3=:P0, Lodo=:P1, HorasTrab=:P2, DataB=:P3, ');
  Dmod.QrUpdU.SQL.Add('ValorHora=:P4, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsFloat   := Geral.DMV(EdETE_m3.Text);
  Dmod.QrUpdU.Params[01].AsFloat   := Geral.DMV(EdLodo.Text);
  Dmod.QrUpdU.Params[02].AsFloat   := Geral.DMV(EdHorasT.Text);
  Dmod.QrUpdU.Params[03].AsString  := Geral.FDT(TPDataB.Date, 1);
  Dmod.QrUpdU.Params[04].AsFloat   := Geral.DMV(EdValorHora.Text);
  //
  Dmod.QrUpdU.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[06].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[07].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqt', 'Codigo');
  AtualizaCusto;
  MostraEdicao(0, stLok, Codigo);
*)
var
  DataB: String;
  //Setor,
  Codigo, Empresa: Integer;
  //CustoHora, CustoInsumo, CustoTotal,
  ETE_m3, Lodo, HorasTrab, ValorHora: Double;
  SQLType: TSQLType;
begin
  // ini 2023-09-09
  // permitir criar cabe�alho para o lodo!
  if UnPQx.ImpedePeloBalanco(TPDataB.Date, False,
  'J� existe balan�o com data posterior.' + sLineBreak +
  'Somente viagens de lodo poder�o ser lan�adas nestea data.' + sLineBreak +
  'Deseja continuar assim mesmo?') then Exit;
  // fim 2023-09-09
  //
  SQLType        := ImgTipo.SQLType;
  Codigo         := QrPQTCodigo.Value;
  ETE_m3         := EdETE_m3.ValueVariant;
  Lodo           := EdLodo.ValueVariant;
  HorasTrab      := EdHorasT.ValueVariant;
  ValorHora      := EdValorHora.ValueVariant;
  //CustoHora      := ;
  //CustoInsumo    := ;
  //CustoTotal     := ;
  DataB          := Geral.FDT(TPDataB.Date, 1);
  //Setor          := ;
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('pqt', 'Codigo', SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqt', False, [
  'ETE_m3', 'Lodo', 'HorasTrab',
  //'Setor', 'CustoHora', 'CustoInsumo', 'CustoTotal',
  'ValorHora', 'DataB', 'Empresa'], [
  'Codigo'], [
  ETE_m3, Lodo, HorasTrab,
  //Setor, CustoHora, CustoInsumo, CustoTotal,
  ValorHora, DataB, Empresa], [
  Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqt', 'Codigo');
    AtualizaCusto;
    MostraEdicao(0, stLok, Codigo);
  end;
end;

procedure TFmPQT.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'pqt', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqt', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqt', 'Codigo');
end;

procedure TFmPQT.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  TPDtCorrApo.Date  := 0;
  TPDataB.Date       := Date;
  PnEdit.Align       := alClient;
  PnDados.Align      := alClient;
  PnEdita.Align      := alClient;
  PnDBGInsumos.Align := alLeft;
  PnDBGLodo.Align    := alClient;
  LaRegistro.Align   := alClient;
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  CriaOForm;
end;

procedure TFmPQT.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPQTCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPQT.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPQT.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmPQT.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQT.QrPQTAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPQT.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'pqt', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmPQT.QrPQTAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrPQTCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrPQTCodigo.Value, False);
  ReopenPQTIts();
  ReopenPQTLodo();
end;

procedure TFmPQT.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPQTCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'pqt', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPQT.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQT.QrPQTBeforeClose(DataSet: TDataSet);
begin
  QrLodo.Close;
  QrPQTIts.Close;
end;

procedure TFmPQT.QrPQTBeforeOpen(DataSet: TDataSet);
begin
  QrPQTCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPQT.ReopenPQTIts;
begin
  QrPQTIts.Close;
  QrPQTIts.Params[0].AsInteger := QrPQTCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPQTIts, Dmod.MyDB);
  if FPQTIts <> 0 then QrPQTIts.Locate('OrigemCtrl', FPQTIts, []);
end;

procedure TFmPQT.ReopenPQTLodo();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLodo, Dmod.MyDB, [
  'SELECT vei.Placa, med.Nome NO_Unidade, med.Sigla, ',
  'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_Motorista, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ',
  'pql.*  ',
  'FROM pqtlodo pql ',
  'LEFT JOIN veiculos vei ON vei.Codigo=pql.Veiculo ',
  'LEFT JOIN unidmed med ON med.Codigo=pql.Unidade ',
  'LEFT JOIN entidades mot ON mot.Codigo=pql.Motorista ',
  'LEFT JOIN stqcenloc scl ON scl.Controle=pql.Local ',
  'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
  'WHERE pql.Codigo=' + Geral.FF0(QrPQTCodigo.Value),
  'ORDER BY Controle',
  '']);
end;

procedure TFmPQT.EdPQChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQT.EdCIChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQT.EdPesoAddExit(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQT.CalculaSaldoFuturo;
var
  CI, PQ: Integer;
  PesoAdd, SaldoFut: Double;
begin
  CI := Geral.IMV(EdCI.Text);
  PQ := Geral.IMV(EdPQ.Text);
(*
  QrSaldo.Close;
  QrSaldo.Params[0].AsInteger := CI;
  QrSaldo.Params[1].AsInteger := PQ;
  UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSaldo, Dmod.MyDB, [
  'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO ',
  'FROM pqcli ',
  'WHERE CI=' + Geral.FF0(CI),
  'AND PQ=' + Geral.FF0(PQ),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  '']);
  //
  PesoAdd := Geral.DMV(EdPesoAdd.Text);
  SaldoFut := QrSaldoPeso.Value - PesoAdd;
  EdSaldoFut.Text := Geral.FFT(SaldoFut, 3, siNegativo);
end;

procedure TFmPQT.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmPQT.QrSaldoAfterOpen(DataSet: TDataSet);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQT.BtDesiste2Click(Sender: TObject);
begin
  AtualizaCusto;
  MostraEdicao(0, stLok, 0);
end;

procedure TFmPQT.BtConfirma2Click(Sender: TObject);
const
  Unitario = True;
var
  Controle: Integer;
  KG, RS, SF: Double;
  //
  DataX: String;
  OriCodi, OriCtrl, OriTipo, CliOrig, CliDest, Insumo, Empresa: Integer;
  Peso, Valor: Double;
  DtCorrApo: String;
begin
  if PnInsumos.Visible then
  begin
    if QrSaldo.RecordCount = 0 then
    begin
      Application.MessageBox('Insumo / cliente interno n�o definido!', 'Erro',
      MB_OK+MB_ICONERROR);
      Exit;
    end;
    SF := Geral.DMV(EdSaldoFut.Text);
    if SF < 0 then
    begin
      Application.MessageBox('Quantidade insuficiente no estoque!', 'Erro',
      MB_OK+MB_ICONERROR);
      EdPesoAdd.SetFocus;
      Exit;
    end;
    KG := Geral.DMV(EdPesoAdd.Text);
    RS := KG * QrSaldoCUSTO.Value;
    if MyObjects.FIC(KG <= 0, EdPesoAdd, 'Quantidade inv�lida!') then Exit;
    //
    Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'EmitIts',
      'EmitIts', 'Controle');
(*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO p q x SET Tipo=' + Geral.FF0(VAR_FATID_0150) + ', DataX=:P0, ');
    Dmod.QrUpd.SQL.Add('CliOrig=:P1, CliDest=:P2, Insumo=:P3, Peso=:P4, ');
    Dmod.QrUpd.SQL.Add('Valor=:P5, OrigemCodi=:P6, OrigemCtrl=:P7');
    Dmod.QrUpd.Params[00].AsString  := Geral.FDT(QrPQTDataB.Value, 1);
    Dmod.QrUpd.Params[01].AsInteger := QrSaldoCI.Value;
    Dmod.QrUpd.Params[02].AsInteger := Dmod.QrMasterDono.Value;
    Dmod.QrUpd.Params[03].AsInteger := QrSaldoPQ.Value;
    Dmod.QrUpd.Params[04].AsFloat   := -KG;
    Dmod.QrUpd.Params[05].AsFloat   := -RS;
    Dmod.QrUpd.Params[06].AsInteger := QrPQTCodigo.Value;
    Dmod.QrUpd.Params[07].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
*)
    DataX      := Geral.FDT(QrPQTDataB.Value, 1);
    OriCodi    := QrPQTCodigo.Value;
    OriCtrl    := Controle;
    OriTipo    := VAR_FATID_0150;
    CliOrig    := QrSaldoCI.Value;
    CliDest    := Dmod.QrMasterDono.Value;
    Insumo     := QrSaldoPQ.Value;
    Peso       := -KG;
    Valor      := -RS;
    DtCorrApo  := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
    //
    //
    if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
    //
    PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
      OriCtrl, OriTipo, Unitario, DtCorrApo, Empresa);
    //
    FPQTIts := Controle;
    UnPQx.AtualizaEstoquePQ(QrSaldoCI.Value, QrSaldoPQ.Value, Empresa, aeMsg, '');
    QrSaldo.Close;
    UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
    ReopenPQTIts;
    AtualizaCusto;
    EdPesoAdd.Text := '';
    EdPQ.Text := '';
    CBPQ.KeyValue := NULL;
    EdPQ.SetFocus;
  end;
end;

procedure TFmPQT.IncluiNovaBaixa1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmPQT.Incluiviagemdelodo1Click(Sender: TObject);
begin
  MostraFormPQTLodo(stIns);
end;

procedure TFmPQT.IncluiInsumobaixaatual1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmPQT.QrPQTCalcFields(DataSet: TDataSet);
begin
  if QrPQTETE_m3.Value = 0 then QrPQTCUSTO_m3.Value := 0 else
  QrPQTCUSTO_m3.Value := QrPQTCustoTotal.Value / QrPQTETE_m3.Value;
end;

procedure TFmPQT.AtualizaCusto;
var
  CustoHora: Double;
begin
  LocCod(QrPQTCodigo.Value, QrPQTCodigo.Value);
  CustoHora := QrPQTHorasTrab.Value * QrPQTValorHora.Value;
  //
  QrSumPQ.Close;
  QrSumPQ.Params[0].AsInteger := QrPQTCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSumPQ, Dmod.MyDB);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE pqt SET CustoHora=:P0, CustoInsumo=:P1, ');
  Dmod.QrUpd.SQL.Add('CustoTotal=:P2 WHERE Codigo=:Pa ');
  Dmod.QrUpd.Params[00].AsFloat   := CustoHora;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPQCUSTO.Value;
  Dmod.QrUpd.Params[02].AsFloat   := CustoHora + QrSumPQCUSTO.Value;
  Dmod.QrUpd.Params[03].AsInteger := QrPQTCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  LocCod(QrPQTCodigo.Value, QrPQTCodigo.Value);
end;

procedure TFmPQT.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmPQT.PMExcluiPopup(Sender: TObject);
begin
  ExcluiBaixa1.Enabled := not Geral.IntToBool_0(QrPQTIts.RecordCount);
  ExcluiItemdabaixa1.Enabled := Geral.IntToBool_0(QrPQTIts.RecordCount);
end;

procedure TFmPQT.ExcluiBaixa1Click(Sender: TObject);
begin
  // quando fazer cuidar controle de estoque por item de entrada de NFe!
  // Ver se d� para fazer como no PQO
end;

procedure TFmPQT.ExcluiItemdabaixa1Click(Sender: TObject);
var
  Atual, OriCodi, OriCtrl, OriTipo, Insumo, CliInt, Empresa: Integer;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQTDataB.Value) then Exit;
  // 2023-09-09
  if Application.MessageBox(PChar('Confirma a exclus�o do item selecionado '+
  'nesta baixa?'), 'Pergunta', MB_YESNO+MB_ICONQUESTION)= ID_YES then
  begin
    Insumo  := QrPQTItsInsumo.Value;
    CliInt  := QrPQTItsCliOrig.Value;
    Empresa := QrPQTItsEmpresa.Value;
    Atual   := QrPQTItsOrigemCtrl.Value;
    FPQTIts := UMyMod.ProximoRegistro(QrPQTIts, 'OrigemCtrl', Atual);
(*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM p q x WHERE Tipo=' + Geral.FF0(VAR_FATID_0150) + ' AND OrigemCtrl=:P0');
    Dmod.QrUpd.SQL.Add('AND OrigemCodi=:P1');
    Dmod.QrUpd.Params[00].AsInteger := Atual;
    Dmod.QrUpd.Params[01].AsInteger := QrPQTCodigo.Value;
    Dmod.QrUpd.ExecSQL;
*)
    OriCodi := QrPQTCodigo.Value;
    OriCtrl := Atual;
    OriTipo := VAR_FATID_0150;
    PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
    //
    AtualizaCusto;
    UnPQx.AtualizaEstoquePQ(CliInt, Insumo, Empresa, aeMsg, '');
  end;
end;

procedure TFmPQT.ExcluiviagemdeLodo1Click(Sender: TObject);
begin
  DBCheck.ExcluiRegistro(Dmod.QrUpd, QrLodo, 'pqtlodo', ['Controle'],
    ['Controle'], True, 'Confirma a exclus�o do item de viagem de lodo selecionado?');
end;

procedure TFmPQT.AletaviagemdeLodo1Click(Sender: TObject);
begin
  MostraFormPQTLodo(stUpd);
end;

procedure TFmPQT.AlteraBaixa1Click(Sender: TObject);
var
  PQT : Integer;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQTDataB.Value) then Exit;
  // 2023-09-09
  PQT := QrPQTCodigo.Value;
  if not UMyMod.SelLockY(PQT, Dmod.MyDB, 'pqt', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PQT, Dmod.MyDB, 'pqt', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPQT.Estabaixa1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxQUI_RECEI_010_A, 'Baixa de Insumos Qu�micos da ETE');
end;

procedure TFmPQT.Porperodo1Click(Sender: TObject);
begin
  Application.CreateForm(TFmGetPeriodo, FmGetPeriodo);
  FmGetPeriodo.ShowModal;
  FmGetPeriodo.Destroy;
  //
  if (VAR_GETDATAI<>0) and (VAR_GETDATAF<>0) then
  begin
    QrPeriodo.Close;
    QrPeriodo.Params[00].AsString  := Geral.FDT(VAR_GETDATAI, 1);
    QrPeriodo.Params[01].AsString  := Geral.FDT(VAR_GETDATAF, 1);
    UnDmkDAC_PF.AbreQuery(QrPeriodo, Dmod.MyDB);
    MyObjects.frxMostra(frxPeriodo, 'Custo da ETE por per�odo');
  end;
end;

procedure TFmPQT.Outros1Click(Sender: TObject);
begin
  FmPrincipal.Relatrios1Click(Self);
end;

procedure TFmPQT.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPQT.frBaixaAtualUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if AnsiCompareText(Name, 'VFR_GRD') = 0 then Val := 15//Geral.BoolToInt2(CkGrade.Checked, 15, 0)
end;

procedure TFmPQT.QrPeriodoCalcFields(DataSet: TDataSet);
begin
  if QrPeriodoETE_m3.Value = 0 then QrPeriodoCUSTO_m3.Value := 0 else
  QrPeriodoCUSTO_m3.Value := QrPeriodoCustoTotal.Value / QrPeriodoETE_m3.Value;
end;

procedure TFmPQT.frxQUI_RECEI_010_AGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VFR_GRD' then Value := 15 else
  if VarName = 'VARF_DATA' then Value := Now() else
end;

end.

