object FmPQE: TFmPQE
  Left = 336
  Top = 186
  Caption = 'QUI-ENTRA-001 :: Entrada de Uso e Consumo'
  ClientHeight = 717
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 557
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelGrids: TPanel
      Left = 0
      Top = 180
      Width = 1008
      Height = 313
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 0
        Top = 154
        Width = 1008
        Height = 5
        Cursor = crVSplit
        Align = alTop
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 154
        Align = alTop
        Caption = ' Itens da nota fiscal (valores em R$): '
        TabOrder = 0
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 682
          Height = 137
          Align = alClient
          Color = clWhite
          DataSource = DsPQEIts
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = DBGrid1KeyDown
          Columns = <
            item
              Expanded = False
              FieldName = 'Conta'
              Title.Caption = 'Item'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_cProd'
              Title.Caption = 'C'#243'digo do fornecedor'
              Width = 128
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPQ'
              Title.Caption = 'Insumo qu'#237'mico'
              Width = 165
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorItem'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor Item'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CUSTOITEM'
              Title.Alignment = taRightJustify
              Title.Caption = 'Total item'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotalPeso'
              Title.Alignment = taRightJustify
              Title.Caption = 'kg L tot.'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRECOKG'
              Title.Alignment = taRightJustify
              Title.Caption = 'Pre'#231'o'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALORKG'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor kg'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CUSTOKG'
              Title.Alignment = taRightJustify
              Title.Caption = 'Custo  kg'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RvIPI'
              Title.Caption = '$ IPI'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RvICMS'
              Title.Caption = '$ ICMS'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RvPIS'
              Title.Caption = '$ PIS'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RvCOFINS'
              Title.Caption = '$ COFINS'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'dFab_TXT'
              Title.Caption = 'Fabrica'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'dVal_TXT'
              Title.Caption = 'Vencimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'xLote'
              Title.Caption = 'Lote(s)'
              Width = 300
              Visible = True
            end>
        end
        object Panel8: TPanel
          Left = 684
          Top = 15
          Width = 322
          Height = 137
          Align = alRight
          BevelOuter = bvNone
          Caption = 'PnMensagens'
          ParentBackground = False
          TabOrder = 1
          object Label6: TLabel
            Left = 0
            Top = 0
            Width = 322
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Alertass:'
            ExplicitWidth = 40
          end
          object Label29: TLabel
            Left = 0
            Top = 73
            Width = 322
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Mensagens:'
            ExplicitWidth = 58
          end
          object MeWarn: TMemo
            Left = 0
            Top = 13
            Width = 322
            Height = 60
            Align = alTop
            ReadOnly = True
            TabOrder = 0
          end
          object MeInfo: TMemo
            Left = 0
            Top = 86
            Width = 322
            Height = 51
            Align = alClient
            ReadOnly = True
            TabOrder = 1
          end
        end
      end
      object PCFinCtb: TPageControl
        Left = 0
        Top = 159
        Width = 1008
        Height = 154
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 1
        ExplicitLeft = 80
        ExplicitTop = 163
        object TabSheet1: TTabSheet
          Caption = ' Financeiro '
          object GroupBox5: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 126
            Align = alClient
            Caption = ' Pagamentos:'
            TabOrder = 0
            object GroupBox6: TGroupBox
              Left = 2
              Top = 15
              Width = 392
              Height = 109
              Align = alLeft
              Caption = ' Tranportadora: '
              TabOrder = 0
              object GridT: TDBGrid
                Left = 2
                Top = 15
                Width = 388
                Height = 92
                Align = alClient
                Color = clWhite
                DataSource = DsEmissT
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnKeyDown = GridTKeyDown
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'FatParcela'
                    Title.Caption = 'N'#186
                    Width = 20
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Vencimento'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Debito'
                    Title.Caption = 'D'#233'bito'
                    Width = 70
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESIT'
                    ReadOnly = True
                    Title.Caption = 'Situa'#231#227'o'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMETIPO'
                    Title.Caption = 'Tipo'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMECARTEIRA'
                    Title.Caption = 'Carteira'
                    Width = 300
                    Visible = True
                  end>
              end
            end
            object GroupBox7: TGroupBox
              Left = 394
              Top = 15
              Width = 604
              Height = 109
              Align = alClient
              Caption = ' Fornecedor: '
              TabOrder = 1
              object GridF: TDBGrid
                Left = 2
                Top = 15
                Width = 600
                Height = 92
                Align = alClient
                Color = clWhite
                DataSource = DsEmissF
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnKeyDown = GridFKeyDown
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'FatParcela'
                    Title.Caption = 'N'#186
                    Width = 20
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Vencimento'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Debito'
                    Title.Caption = 'D'#233'bito'
                    Width = 70
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESIT'
                    ReadOnly = True
                    Title.Caption = 'Situa'#231#227'o'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMETIPO'
                    Title.Caption = 'Tipo'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMECARTEIRA'
                    Title.Caption = 'Carteira'
                    Width = 300
                    Visible = True
                  end>
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' NF-es de entrada (RP/CC)'
          ImageIndex = 1
          object Splitter2: TSplitter
            Left = 0
            Top = 81
            Width = 1000
            Height = 5
            Cursor = crVSplit
            Align = alTop
            ExplicitTop = 65
          end
          object DBGInnNFsCab: TDBGrid
            Left = 0
            Top = 0
            Width = 1000
            Height = 81
            Align = alTop
            DataSource = DsEfdInnNFsCab
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Terceiro'
                Title.Caption = 'Fornecedor'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TER'
                Title.Caption = 'Nome do fornecedor'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SER'
                Title.Caption = 'S'#233'rie'
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NUM_DOC'
                Title.Caption = 'NF'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DT_DOC'
                Title.Caption = 'Emiss'#227'o'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DT_E_S'
                Title.Caption = 'Entrada'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_DOC'
                Title.Caption = '$ Docum.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_DESC'
                Title.Caption = '$ Desc.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_MERC'
                Title.Caption = '$ Produtos'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_FRT'
                Title.Caption = '$ Frete'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_SEG'
                Title.Caption = '$ Seguro'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_OUT_DA'
                Title.Caption = '$ Outros'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_BC_ICMS'
                Title.Caption = '$ BC ICMS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_ICMS'
                Title.Caption = '$ ICMS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_IPI'
                Title.Caption = '$ IPI'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_PIS'
                Title.Caption = '$ PIS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_COFINS'
                Title.Caption = '$ COFINS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_PIS_ST'
                Title.Caption = '$ PIS ST'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_COFINS_ST'
                Title.Caption = '$ COFINS ST'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_BC_ICMS_ST'
                Title.Caption = '$ BC ICMS ST'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_ICMS_ST'
                Title.Caption = '$ ICMS ST'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFe_FatID'
                Title.Caption = 'FatID NFe'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFe_FatNum'
                Title.Caption = 'Fat.Num.NFe'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFe_StaLnk'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MovFatID'
                Title.Caption = 'FatID Mov'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MovFatNum'
                Title.Caption = 'Fat.Num. Mov'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MovimCod'
                Title.Caption = 'IME-C'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'COD_MOD'
                Title.Caption = 'Modelo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CHV_NFE'
                Title.Caption = 'Chave NFe'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'COD_SIT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IND_PGTO'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IND_FRT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFeStatus'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Empresa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CliInt'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Motorista'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Placa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_ABAT_NT'
                Visible = True
              end>
          end
          object DBGInnNFsIts: TDBGrid
            Left = 0
            Top = 86
            Width = 1000
            Height = 40
            Align = alClient
            DataSource = DsEfdInnNFsIts
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Conta'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFOP'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NCM'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGru1'
                Title.Caption = 'C'#243'digo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Nome do Produto'
                Width = 180
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'UNID'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QTD'
                Title.Caption = 'Quantid.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'xLote'
                Title.Caption = 'Lote'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_ITEM'
                Title.Caption = '$ Item'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_DESC'
                Title.Caption = '$ Desc.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CST_ICMS'
                Title.Caption = 'CST ICMS'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_BC_ICMS'
                Title.Caption = '$ BC ICMS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ALIQ_ICMS'
                Title.Caption = '% ICMS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_ICMS'
                Title.Caption = '$ ICMS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CST_IPI'
                Title.Caption = 'CST IPI'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_BC_IPI'
                Title.Caption = '$ BC IPI'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ALIQ_IPI'
                Title.Caption = '% IPI'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_IPI'
                Title.Caption = '$ IPI'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CST_PIS'
                Title.Caption = 'CST PIS'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_BC_PIS'
                Title.Caption = '$ BC PIS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ALIQ_PIS_p'
                Title.Caption = '% PIS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_PIS'
                Title.Caption = '$ PIS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CST_COFINS'
                Title.Caption = 'CST COFINS'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_BC_COFINS'
                Title.Caption = '$ BC COFINS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ALIQ_COFINS_p'
                Title.Caption = '% COFINS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_COFINS'
                Title.Caption = '$ COFINS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_BC_ICMS_ST'
                Title.Caption = '$ BC ICMS ST'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ALIQ_ST'
                Title.Caption = '% ST'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_ICMS_ST'
                Title.Caption = '$ ICMS ST'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'COD_ENQ'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'COD_CTA'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_ABAT_NT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QUANT_BC_PIS'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ALIQ_PIS_r'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QUANT_BC_COFINS'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ALIQ_COFINS_r'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtCorrApo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ori_IPIpIPI'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IND_MOV'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IND_APUR'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vProd'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vFrete'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vSeg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vOutro'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ori_IPIvIPI'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GerBxaEstq'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'COD_NAT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'UnidMed'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLAUNIDMED'
                Title.Caption = 'Unidade'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ex_TIPI'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Grandeza'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Tipo_Item'
                Visible = True
              end>
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'CT-es entrada'
          ImageIndex = 2
          object DBGrid2: TDBGrid
            Left = 0
            Top = 0
            Width = 1000
            Height = 126
            Align = alClient
            DataSource = DsEfdInnCTsCab
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Terceiro'
                Title.Caption = 'Fornecedor'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TER'
                Title.Caption = 'Nome do fornecedor'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SER'
                Title.Caption = 'S'#233'rie'
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SUB'
                Title.Caption = 'Sub'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NUM_DOC'
                Title.Caption = 'NF'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DT_DOC'
                Title.Caption = 'Emiss'#227'o'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DT_A_P'
                Title.Caption = 'Aquisi'#231#227'o'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_MERC'
                Title.Caption = '$ Servi'#231'os'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_DESC'
                Title.Caption = '$ Desc.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_DOC'
                Title.Caption = '$ Docum.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_BC_ICMS'
                Title.Caption = '$ BC ICMS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_ICMS'
                Title.Caption = '$ ICMS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFe_FatID'
                Title.Caption = 'FatID NFe'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFe_FatNum'
                Title.Caption = 'Fat.Num.NFe'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFe_StaLnk'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MovFatID'
                Title.Caption = 'FatID Mov'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MovFatNum'
                Title.Caption = 'Fat.Num. Mov'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MovimCod'
                Title.Caption = 'IME-C'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'COD_MOD'
                Title.Caption = 'Modelo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CHV_CTE'
                Title.Caption = 'Chave CTe'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'COD_SIT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IND_FRT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CTeStatus'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Empresa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CliInt'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Motorista'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Placa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VL_NT'
                Title.Caption = 'VL_ABAT_NT'
                Visible = True
              end>
          end
        end
      end
    end
    object PainelCabecalho: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 180
      Align = alTop
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object Painel10: TPanel
        Left = 0
        Top = 0
        Width = 425
        Height = 180
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 425
          Height = 180
          Align = alClient
          Caption = ' Lan'#231'amento:'
          Enabled = False
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 16
            Width = 65
            Height = 13
            Caption = 'N'#186' do lan'#231'to: '
            FocusControl = DBEdit1
          end
          object Label2: TLabel
            Left = 124
            Top = 96
            Width = 56
            Height = 13
            Caption = 'Data sa'#237'da:'
          end
          object Label4: TLabel
            Left = 124
            Top = 136
            Width = 34
            Height = 13
            Caption = 'NF VP:'
            FocusControl = EdNF
          end
          object Label7: TLabel
            Left = 8
            Top = 136
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
            FocusControl = DBEdit7
          end
          object Label3: TLabel
            Left = 240
            Top = 136
            Width = 98
            Height = 13
            Caption = 'Valor da Nota Fiscal:'
            FocusControl = EdValorNF
          end
          object Label5: TLabel
            Left = 72
            Top = 16
            Width = 57
            Height = 13
            Caption = 'Fornecedor:'
          end
          object Label20: TLabel
            Left = 8
            Top = 96
            Width = 67
            Height = 13
            Caption = 'Data emiss'#227'o:'
          end
          object Label24: TLabel
            Left = 8
            Top = 56
            Width = 70
            Height = 13
            Caption = 'Cliente interno:'
          end
          object Label8: TLabel
            Left = 240
            Top = 96
            Width = 71
            Height = 13
            Caption = 'Data chegada:'
          end
          object Label27: TLabel
            Left = 356
            Top = 96
            Width = 35
            Height = 13
            Caption = 'NF RP:'
            FocusControl = DBEdit24
          end
          object Label28: TLabel
            Left = 356
            Top = 136
            Width = 34
            Height = 13
            Caption = 'NF CC:'
            FocusControl = DBEdit25
          end
          object DBEdit1: TDBEdit
            Left = 8
            Top = 32
            Width = 61
            Height = 21
            TabStop = False
            DataField = 'Codigo'
            DataSource = DsPQE
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8281908
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
          end
          object EdNF: TDBEdit
            Left = 124
            Top = 152
            Width = 112
            Height = 21
            DataField = 'NF'
            DataSource = DsPQE
            TabOrder = 1
          end
          object DBEdit7: TDBEdit
            Left = 8
            Top = 152
            Width = 112
            Height = 21
            DataField = 'PesoL'
            DataSource = DsPQE
            TabOrder = 2
          end
          object EdValorNF: TDBEdit
            Left = 240
            Top = 152
            Width = 112
            Height = 21
            DataField = 'ValorNF'
            DataSource = DsPQE
            TabOrder = 4
          end
          object DBEdit5: TDBEdit
            Left = 72
            Top = 32
            Width = 349
            Height = 21
            DataField = 'NOMEFORNECEDOR'
            DataSource = DsPQE
            TabOrder = 3
          end
          object DBEdit01: TDBEdit
            Left = 8
            Top = 72
            Width = 413
            Height = 21
            DataField = 'NOMECLIINT'
            DataSource = DsPQE
            TabOrder = 5
          end
          object DBEdit4: TDBEdit
            Left = 8
            Top = 112
            Width = 112
            Height = 21
            DataField = 'DataE_TXT'
            DataSource = DsPQE
            TabOrder = 6
          end
          object DBEdit2: TDBEdit
            Left = 124
            Top = 112
            Width = 112
            Height = 21
            DataField = 'DataS_TXT'
            DataSource = DsPQE
            TabOrder = 7
          end
          object DBEdit6: TDBEdit
            Left = 240
            Top = 112
            Width = 112
            Height = 21
            DataField = 'Data'
            DataSource = DsPQE
            TabOrder = 8
          end
          object DBEdit24: TDBEdit
            Left = 356
            Top = 112
            Width = 64
            Height = 21
            DataField = 'NF_RP'
            DataSource = DsPQE
            TabOrder = 9
          end
          object DBEdit25: TDBEdit
            Left = 356
            Top = 152
            Width = 64
            Height = 21
            DataField = 'NF_CC'
            DataSource = DsPQE
            TabOrder = 10
          end
        end
      end
      object Panel1: TPanel
        Left = 425
        Top = 0
        Width = 583
        Height = 180
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object PainelCancelado: TPanel
          Left = 0
          Top = 136
          Width = 583
          Height = 44
          Align = alBottom
          BevelOuter = bvNone
          Caption = 'Entrada Cancelada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -23
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Visible = False
          object Image2: TImage
            Left = 0
            Top = 41
            Width = 583
            Height = 3
            Align = alClient
            Transparent = True
            ExplicitHeight = 4
          end
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 583
            Height = 41
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label23: TLabel
              Left = 4
              Top = 0
              Width = 45
              Height = 13
              Caption = 'Cadastro:'
              FocusControl = DBEdit14
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label25: TLabel
              Left = 204
              Top = 0
              Width = 42
              Height = 13
              Caption = 'Alterado:'
              FocusControl = DBEdit17
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object DBEdit14: TDBEdit
              Left = 4
              Top = 16
              Width = 57
              Height = 21
              DataField = 'DataCad'
              DataSource = DsPQE
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
            object DBEdit16: TDBEdit
              Left = 60
              Top = 16
              Width = 141
              Height = 21
              DataField = 'NOMEUSERCAD'
              DataSource = DsPQE
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object DBEdit17: TDBEdit
              Left = 204
              Top = 16
              Width = 53
              Height = 21
              DataField = 'DataAlt'
              DataSource = DsPQE
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
            end
            object DBEdit18: TDBEdit
              Left = 260
              Top = 16
              Width = 141
              Height = 21
              DataField = 'NOMEUSERALT'
              DataSource = DsPQE
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
            end
          end
        end
        object GroupBox3: TGroupBox
          Left = 0
          Top = 0
          Width = 208
          Height = 136
          Align = alLeft
          Caption = ' Transporte: '
          TabOrder = 1
          object Label13: TLabel
            Left = 8
            Top = 16
            Width = 75
            Height = 13
            Caption = 'Transportadora:'
            FocusControl = DBEdit12
          end
          object Label14: TLabel
            Left = 8
            Top = 56
            Width = 53
            Height = 13
            Caption = 'Conhecim.:'
            FocusControl = DBEdit13
          end
          object Label15: TLabel
            Left = 88
            Top = 56
            Width = 54
            Height = 13
            Caption = 'Valor Frete:'
            FocusControl = EdFrete
          end
          object Label16: TLabel
            Left = 8
            Top = 96
            Width = 42
            Height = 13
            Caption = 'Frete kg:'
            FocusControl = DBEdit15
          end
          object DBEdit12: TDBEdit
            Left = 8
            Top = 32
            Width = 193
            Height = 21
            DataField = 'NOMETRANSPORTADORA'
            DataSource = DsPQE
            TabOrder = 0
          end
          object DBEdit13: TDBEdit
            Left = 8
            Top = 72
            Width = 76
            Height = 21
            DataField = 'Conhecimento'
            DataSource = DsPQE
            TabOrder = 1
          end
          object EdFrete: TDBEdit
            Left = 88
            Top = 72
            Width = 113
            Height = 21
            DataField = 'Frete'
            DataSource = DsPQE
            TabOrder = 2
          end
          object DBEdit15: TDBEdit
            Left = 8
            Top = 112
            Width = 101
            Height = 21
            DataField = 'FRETEKG'
            DataSource = DsPQE
            TabOrder = 3
          end
        end
        object GroupBox4: TGroupBox
          Left = 208
          Top = 0
          Width = 197
          Height = 136
          Align = alLeft
          Caption = ' Confer'#234'ncia: '
          TabOrder = 2
          object Label9: TLabel
            Left = 100
            Top = 16
            Width = 55
            Height = 13
            Caption = 'Nota     R$:'
            FocusControl = EdTNota
          end
          object Label10: TLabel
            Left = 8
            Top = 56
            Width = 42
            Height = 13
            Caption = 'kg bruto:'
            FocusControl = EdTBruto
          end
          object Label11: TLabel
            Left = 100
            Top = 56
            Width = 33
            Height = 13
            Caption = 'kg l'#237'q.:'
            FocusControl = EdTLiq
          end
          object Label17: TLabel
            Left = 100
            Top = 96
            Width = 60
            Height = 13
            Caption = 'Pag.F     R$:'
            FocusControl = EdTNota
          end
          object Label18: TLabel
            Left = 8
            Top = 96
            Width = 61
            Height = 13
            Caption = 'Pag. T    R$:'
            FocusControl = EdTNota
          end
          object Label19: TLabel
            Left = 8
            Top = 16
            Width = 36
            Height = 13
            Caption = 'Pedido:'
            FocusControl = EdTNota
          end
          object EdTNota: TdmkEdit
            Left = 100
            Top = 32
            Width = 88
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTBruto: TdmkEdit
            Left = 8
            Top = 72
            Width = 88
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTLiq: TdmkEdit
            Left = 100
            Top = 72
            Width = 88
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdPagF: TdmkEdit
            Left = 100
            Top = 112
            Width = 88
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdPagT: TdmkEdit
            Left = 8
            Top = 112
            Width = 88
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object DBEdit3: TDBEdit
            Left = 8
            Top = 32
            Width = 88
            Height = 21
            TabStop = False
            DataField = 'Pedido'
            DataSource = DsPQE
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8281908
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 5
          end
        end
        object GroupBox8: TGroupBox
          Left = 405
          Top = 0
          Width = 216
          Height = 136
          Align = alLeft
          Caption = ' Recupera'#231#227'o de impostos: '
          TabOrder = 3
          object Label12: TLabel
            Left = 4
            Top = 36
            Width = 38
            Height = 13
            Caption = '$ ICMS:'
          end
          object Label21: TLabel
            Left = 4
            Top = 60
            Width = 25
            Height = 13
            Caption = '$ IPI:'
            Enabled = False
          end
          object Label22: TLabel
            Left = 4
            Top = 84
            Width = 29
            Height = 13
            Caption = '$ PIS:'
          end
          object Label26: TLabel
            Left = 4
            Top = 108
            Width = 51
            Height = 13
            Caption = '$ COFINS:'
          end
          object GroupBox9: TGroupBox
            Left = 142
            Top = 15
            Width = 72
            Height = 119
            Align = alRight
            Caption = ' Item selec.: '
            TabOrder = 0
            object DBEdit10: TDBEdit
              Left = 4
              Top = 16
              Width = 64
              Height = 21
              DataField = 'RvICMS'
              DataSource = DsPQEIts
              TabOrder = 0
            end
            object DBEdit20: TDBEdit
              Left = 4
              Top = 40
              Width = 64
              Height = 21
              DataSource = DsPQEIts
              Enabled = False
              TabOrder = 1
            end
            object DBEdit21: TDBEdit
              Left = 4
              Top = 64
              Width = 64
              Height = 21
              DataField = 'RvPIS'
              DataSource = DsPQEIts
              TabOrder = 2
            end
            object DBEdit22: TDBEdit
              Left = 4
              Top = 88
              Width = 64
              Height = 21
              DataField = 'RvCOFINS'
              DataSource = DsPQEIts
              TabOrder = 3
            end
          end
          object GroupBox10: TGroupBox
            Left = 70
            Top = 15
            Width = 72
            Height = 119
            Align = alRight
            Caption = ' Toda NF:'
            TabOrder = 1
            object DBEdit8: TDBEdit
              Left = 4
              Top = 16
              Width = 64
              Height = 21
              DataField = 'RvICMS'
              DataSource = DsRetImpost
              TabOrder = 0
            end
            object DBEdit9: TDBEdit
              Left = 4
              Top = 40
              Width = 64
              Height = 21
              DataSource = DsRetImpost
              Enabled = False
              TabOrder = 1
            end
            object DBEdit11: TDBEdit
              Left = 4
              Top = 64
              Width = 64
              Height = 21
              DataField = 'RvPIS'
              DataSource = DsRetImpost
              TabOrder = 2
            end
            object DBEdit19: TDBEdit
              Left = 4
              Top = 88
              Width = 64
              Height = 21
              DataField = 'RvCOFINS'
              DataSource = DsRetImpost
              TabOrder = 3
            end
          end
        end
      end
    end
    object GBTrava: TGroupBox
      Left = 0
      Top = 493
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      Visible = False
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 859
          Top = 0
          Width = 145
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 8
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Visible = False
          end
        end
        object BtTrava: TBitBtn
          Tag = 14
          Left = 8
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Trava'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtTravaClick
        end
        object BtIncluiIts: TBitBtn
          Tag = 10
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Insere'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiItsClick
        end
        object BtAlteraIts: TBitBtn
          Tag = 11
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Edita'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtAlteraItsClick
        end
        object BtExcluiIts: TBitBtn
          Tag = 12
          Left = 488
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtExcluiItsClick
        end
        object BtQuita: TBitBtn
          Left = 368
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Pagto R'#225'pido'
          NumGlyphs = 2
          TabOrder = 5
          Visible = False
          OnClick = BtQuitaClick
        end
        object BtCTe2: TBitBtn
          Tag = 1000516
          Left = 728
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&CT-e'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BtCTe2Click
        end
        object BtNFe2: TBitBtn
          Tag = 1000516
          Left = 608
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&NF-e'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          OnClick = BtNFe2Click
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 336
        Height = 32
        Caption = 'Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 336
        Height = 32
        Caption = 'Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 336
        Height = 32
        Caption = 'Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 564
        Height = 17
        Caption = 
          'VP: Venda Proced'#234'ncia.          RP: Remessa Proced'#234'ncia.        ' +
          '  CC: Cobertura Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PB1: TProgressBar
      Left = 848
      Top = 16
      Width = 150
      Height = 17
      TabOrder = 1
    end
  end
  object GBCntrl: TGroupBox
    Left = 0
    Top = 653
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 172
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object LaRegistro: TStaticText
      Left = 174
      Top = 15
      Width = 62
      Height = 47
      Align = alClient
      BevelInner = bvLowered
      BevelKind = bkFlat
      Caption = '[N]: 0'
      TabOrder = 2
    end
    object Panel3: TPanel
      Left = 236
      Top = 15
      Width = 770
      Height = 47
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object Panel6: TPanel
        Left = 637
        Top = 0
        Width = 133
        Height = 47
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 16
          Top = 3
          Width = 108
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 4
        Top = 4
        Width = 108
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 112
        Top = 4
        Width = 108
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 220
        Top = 4
        Width = 108
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtExcluiClick
      end
      object BtBaixa: TBitBtn
        Left = 328
        Top = 4
        Width = 108
        Height = 40
        Cursor = crHandPoint
        Caption = '&Baixa'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtBaixaClick
      end
      object BtNFe1: TBitBtn
        Tag = 1000516
        Left = 436
        Top = 4
        Width = 108
        Height = 40
        Cursor = crHandPoint
        Caption = '&NF-e'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtNFe2Click
      end
      object BtCTe1: TBitBtn
        Tag = 1000516
        Left = 543
        Top = 4
        Width = 108
        Height = 40
        Cursor = crHandPoint
        Caption = '&CT-e'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = BtCTe2Click
      end
    end
  end
  object Query1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome  FROM pqe'
      'WHERE Nome LIKE:Nome')
    Left = 216
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Nome'
        ParamType = ptUnknown
      end>
    object Query1Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'BDLEATHER."MLA0009.DB".Codigo'
    end
    object Query1Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'BDLEATHER."MLA0009.DB".Nome'
      Size = 30
    end
  end
  object DataSource1: TDataSource
    DataSet = Query1
    Left = 244
    Top = 280
  end
  object QrTransporte: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END Nome'
      'FROM entidades'
      'WHERE Fornece3 = '#39'V'#39
      '')
    Left = 626
    Top = 367
    object QrTransporteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransporteNome: TWideStringField
      FieldName = 'Nome'
      Size = 41
    end
  end
  object DsTransporte: TDataSource
    DataSet = QrTransporte
    Left = 654
    Top = 367
  end
  object QrPQE: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPQEAfterOpen
    BeforeClose = QrPQEBeforeClose
    AfterScroll = QrPQEAfterScroll
    OnCalcFields = QrPQECalcFields
    SQL.Strings = (
      'SELECT s1.Login NOMEUSERCAD, s2.Login NOMEUSERALT,'
      'en.*, CASE WHEN tr.Tipo=0 then tr.RazaoSocial'
      'ELSE tr.Nome END NOMETRANSPORTADORA,'
      'CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial ELSE ci.Nome'
      'END NOMECLIINT, ei.CodCliInt'
      'FROM pqe en'
      'LEFT JOIN enticliint ei ON ei.CodEnti=en.CI'
      'LEFT JOIN entidades  tr ON tr.Codigo=en.Transportadora'
      'LEFT JOIN entidades  ci ON ci.Codigo=en.CI'
      'LEFT JOIN senhas     s1 ON tr.UserCad=s1.Numero'
      'LEFT JOIN senhas     s2 ON tr.UserAlt=s2.Numero')
    Left = 4
    Top = 16
    object QrPQECodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pqe.Codigo'
    end
    object QrPQEData: TDateField
      FieldName = 'Data'
      Origin = 'DBMBWET.pqe.Data'
      DisplayFormat = 'dd/mm/yy'
      EditMask = 'dd/mm/yy'
    end
    object QrPQETransportadora: TIntegerField
      FieldName = 'Transportadora'
      Origin = 'DBMBWET.pqe.Transportadora'
    end
    object QrPQENF: TIntegerField
      FieldName = 'NF'
      Origin = 'DBMBWET.pqe.NF'
      DisplayFormat = '000000'
    end
    object QrPQEFrete: TFloatField
      FieldName = 'Frete'
      Origin = 'DBMBWET.pqe.Frete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEPesoB: TFloatField
      FieldName = 'PesoB'
      Origin = 'DBMBWET.pqe.PesoB'
      DisplayFormat = '#,##0.000'
    end
    object QrPQEPesoL: TFloatField
      FieldName = 'PesoL'
      Origin = 'DBMBWET.pqe.PesoL'
      DisplayFormat = '#,##0.000'
    end
    object QrPQEValorNF: TFloatField
      FieldName = 'ValorNF'
      Origin = 'DBMBWET.pqe.ValorNF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQERICMS: TFloatField
      FieldName = 'RICMS'
      Origin = 'DBMBWET.pqe.RICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQERICMSF: TFloatField
      FieldName = 'RICMSF'
      Origin = 'DBMBWET.pqe.RICMSF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQELk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.pqe.Lk'
    end
    object QrPQEConhecimento: TIntegerField
      FieldName = 'Conhecimento'
      Origin = 'DBMBWET.pqe.Conhecimento'
    end
    object QrPQENOMETRANSPORTADORA: TWideStringField
      FieldName = 'NOMETRANSPORTADORA'
      Origin = 'DBMBWET.fornecedores.Nome'
      Size = 128
    end
    object QrPQEIQ: TIntegerField
      FieldName = 'IQ'
      Origin = 'DBMBWET.pqe.IQ'
    end
    object QrPQENOMEFORNECEDOR: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMEFORNECEDOR'
      LookupDataSet = QrFornecedor
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'IQ'
      Size = 50
      Lookup = True
    end
    object QrPQEPedido: TIntegerField
      FieldName = 'Pedido'
      Origin = 'DBMBWET.pqe.Pedido'
      DisplayFormat = '000'
    end
    object QrPQEDataE: TDateField
      FieldName = 'DataE'
      Origin = 'DBMBWET.pqe.DataE'
      DisplayFormat = 'dd/mm/yy'
      EditMask = 'dd/mm/yy'
    end
    object QrPQEJuros: TFloatField
      FieldName = 'Juros'
      Origin = 'DBMBWET.pqe.Juros'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEICMS: TFloatField
      FieldName = 'ICMS'
      Origin = 'DBMBWET.pqe.ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEFRETEKG: TFloatField
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'FRETEKG'
      DisplayFormat = '#,###,##0.0000'
      Calculated = True
    end
    object QrPQECancelado: TWideStringField
      FieldName = 'Cancelado'
      Origin = 'DBMBWET.pqe.Cancelado'
      FixedChar = True
      Size = 1
    end
    object QrPQENOMEUSERCAD: TWideStringField
      FieldName = 'NOMEUSERCAD'
      Required = True
      Size = 30
    end
    object QrPQENOMEUSERALT: TWideStringField
      FieldName = 'NOMEUSERALT'
      Required = True
      Size = 30
    end
    object QrPQEDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQEDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQEUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQEUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQECI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQENOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 100
    end
    object QrPQETipoNF: TSmallintField
      FieldName = 'TipoNF'
    end
    object QrPQErefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrPQEmodNF: TSmallintField
      FieldName = 'modNF'
    end
    object QrPQESerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrPQEDataS: TDateField
      FieldName = 'DataS'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQEIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrPQEPIS: TFloatField
      FieldName = 'PIS'
    end
    object QrPQECOFINS: TFloatField
      FieldName = 'COFINS'
    end
    object QrPQESeguro: TFloatField
      FieldName = 'Seguro'
    end
    object QrPQEDesconto: TFloatField
      FieldName = 'Desconto'
    end
    object QrPQEOutros: TFloatField
      FieldName = 'Outros'
    end
    object QrPQEDataS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataS_TXT'
      Size = 10
      Calculated = True
    end
    object QrPQEDataE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataE_TXT'
      Size = 10
      Calculated = True
    end
    object QrPQEData_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Data_TXT'
      Size = 10
      Calculated = True
    end
    object QrPQENF_RP: TIntegerField
      FieldName = 'NF_RP'
    end
    object QrPQENF_CC: TIntegerField
      FieldName = 'NF_CC'
    end
    object QrPQECodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object QrPQEEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPQEValProd: TFloatField
      FieldName = 'ValProd'
    end
    object QrPQEVolumes: TIntegerField
      FieldName = 'Volumes'
    end
    object QrPQEEFD_INN_AnoMes: TIntegerField
      FieldName = 'EFD_INN_AnoMes'
    end
    object QrPQEEFD_INN_Empresa: TIntegerField
      FieldName = 'EFD_INN_Empresa'
    end
    object QrPQEEFD_INN_LinArq: TIntegerField
      FieldName = 'EFD_INN_LinArq'
    end
    object QrPQEHowLoad: TSmallintField
      FieldName = 'HowLoad'
    end
    object QrPQEFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPQEFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrPQEErrNota: TFloatField
      FieldName = 'ErrNota'
    end
    object QrPQEErrBruto: TFloatField
      FieldName = 'ErrBruto'
    end
    object QrPQEErrLiq: TFloatField
      FieldName = 'ErrLiq'
    end
    object QrPQEErrPagT: TFloatField
      FieldName = 'ErrPagT'
    end
    object QrPQEErrPagF: TFloatField
      FieldName = 'ErrPagF'
    end
    object QrPQEAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQEAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQEFreteRpICMS: TFloatField
      FieldName = 'FreteRpICMS'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrPQEFreteRpPIS: TFloatField
      FieldName = 'FreteRpPIS'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrPQEFreteRpCOFINS: TFloatField
      FieldName = 'FreteRpCOFINS'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrPQEFreteRvICMS: TFloatField
      FieldName = 'FreteRvICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPQEFreteRvPIS: TFloatField
      FieldName = 'FreteRvPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPQEFreteRvCOFINS: TFloatField
      FieldName = 'FreteRvCOFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPQEAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPQEAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPQEmodRP: TSmallintField
      FieldName = 'modRP'
      Required = True
    end
    object QrPQEmodCC: TSmallintField
      FieldName = 'modCC'
      Required = True
    end
    object QrPQESerRP: TIntegerField
      FieldName = 'SerRP'
      Required = True
    end
    object QrPQESerCC: TIntegerField
      FieldName = 'SerCC'
      Required = True
    end
    object QrPQENFe_RP: TWideStringField
      FieldName = 'NFe_RP'
      Size = 44
    end
    object QrPQENFe_CC: TWideStringField
      FieldName = 'NFe_CC'
      Size = 44
    end
    object QrPQECte_Id: TWideStringField
      FieldName = 'Cte_Id'
      Size = 47
    end
    object QrPQECTe_mod: TSmallintField
      FieldName = 'CTe_mod'
      Required = True
    end
    object QrPQECTe_serie: TIntegerField
      FieldName = 'CTe_serie'
      Required = True
    end
    object QrPQENFVP_FatID: TIntegerField
      FieldName = 'NFVP_FatID'
      Required = True
    end
    object QrPQENFVP_FatNum: TIntegerField
      FieldName = 'NFVP_FatNum'
      Required = True
    end
    object QrPQENFVP_StaLnk: TSmallintField
      FieldName = 'NFVP_StaLnk'
      Required = True
    end
    object QrPQENFRP_FatID: TIntegerField
      FieldName = 'NFRP_FatID'
      Required = True
    end
    object QrPQENFRP_FatNum: TIntegerField
      FieldName = 'NFRP_FatNum'
      Required = True
    end
    object QrPQENFRP_StaLnk: TSmallintField
      FieldName = 'NFRP_StaLnk'
      Required = True
    end
    object QrPQENFCC_FatID: TIntegerField
      FieldName = 'NFCC_FatID'
      Required = True
    end
    object QrPQENFCC_FatNum: TIntegerField
      FieldName = 'NFCC_FatNum'
      Required = True
    end
    object QrPQENFCC_StaLnk: TSmallintField
      FieldName = 'NFCC_StaLnk'
      Required = True
    end
    object QrPQEICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrPQEICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrPQEICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrPQEICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrPQEICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrPQEUF_EMPRESA: TSmallintField
      FieldName = 'UF_EMPRESA'
    end
    object QrPQEUF_FORNECE: TSmallintField
      FieldName = 'UF_FORNECE'
    end
    object QrPQESqLinked: TIntegerField
      FieldName = 'SqLinked'
    end
    object QrPQEIND_FRT: TSmallintField
      FieldName = 'IND_FRT'
      Required = True
    end
    object QrPQEIND_PGTO: TSmallintField
      FieldName = 'IND_PGTO'
      Required = True
    end
    object QrPQERegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
    end
    object QrPQECondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
    end
    object QrPQEMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrPQENFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
    end
    object QrPQENFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
    end
  end
  object DsPQE: TDataSource
    DataSet = QrPQE
    Left = 32
    Top = 16
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END Nome'
      'FROM entidades'
      'WHERE Fornece2 = '#39'V'#39
      '')
    Left = 622
    Top = 323
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQEIts: TDataSource
    DataSet = QrPQEIts
    Left = 88
    Top = 16
  end
  object QrEmissT: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmissTCalcFields
    Left = 88
    Top = 396
    object QrEmissTCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrEmissTDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 128
    end
    object QrEmissTNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrEmissTDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissTCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissTDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrEmissTSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrEmissTVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissTFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrEmissTFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrEmissTData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissTTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmissTAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrEmissTGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrEmissTCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrEmissTLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmissTNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrEmissTNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 10
      Calculated = True
    end
    object QrEmissTNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrEmissTID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrEmissTSub: TIntegerField
      FieldName = 'Sub'
    end
    object QrEmissTFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrEmissTBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrEmissTLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrEmissTCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrEmissTLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrEmissTPago: TFloatField
      FieldName = 'Pago'
    end
    object QrEmissTMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEmissTFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrEmissTCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEmissTControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissTID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrEmissTCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmissTQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmissTFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrEmissTOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrEmissTLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrEmissTForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrEmissTMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrEmissTMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrEmissTProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrEmissTDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrEmissTCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrEmissTNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrEmissTVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrEmissTAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEmissTICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrEmissTICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrEmissTDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrEmissTDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEmissTDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrEmissTDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmissTDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmissTUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmissTUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmissTEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrEmissTContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEmissTCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrEmissTFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmissTAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrEmissTGenCtb: TIntegerField
      FieldName = 'GenCtb'
    end
    object QrEmissTSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrEmissTGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrEmissTGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
    object QrEmissTCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
  end
  object DsEmissT: TDataSource
    DataSet = QrEmissT
    Left = 112
    Top = 396
  end
  object QrEmissF: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmissFCalcFields
    Left = 436
    Top = 396
    object QrEmissFData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissFTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmissFCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrEmissFAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrEmissFGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrEmissFDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 128
    end
    object QrEmissFNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrEmissFDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissFCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrEmissFCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissFDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrEmissFSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrEmissFVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissFLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmissFFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrEmissFFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrEmissFNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrEmissFNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 10
      Calculated = True
    end
    object QrEmissFNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrEmissFID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrEmissFSub: TIntegerField
      FieldName = 'Sub'
    end
    object QrEmissFFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrEmissFBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrEmissFLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrEmissFCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrEmissFLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrEmissFPago: TFloatField
      FieldName = 'Pago'
    end
    object QrEmissFMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEmissFFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrEmissFCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEmissFControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissFID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrEmissFCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmissFQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmissFFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrEmissFOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrEmissFLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrEmissFForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrEmissFMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrEmissFMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrEmissFProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrEmissFDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrEmissFCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrEmissFNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrEmissFVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrEmissFAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEmissFICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrEmissFICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrEmissFDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrEmissFDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEmissFDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrEmissFDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmissFDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmissFUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmissFUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmissFEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrEmissFContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEmissFCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrEmissFFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmissFAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrEmissFSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrEmissFGenCtb: TIntegerField
      FieldName = 'GenCtb'
    end
    object QrEmissFGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrEmissFGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
    object QrEmissFCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrEmissFFatParcRef: TIntegerField
      FieldName = 'FatParcRef'
    end
  end
  object DsEmissF: TDataSource
    DataSet = QrEmissF
    Left = 460
    Top = 396
  end
  object QrUpdW: TMySQLQuery
    Database = Dmod.MyDB
    Left = 434
    Top = 279
  end
  object QrUpdM: TMySQLQuery
    Database = Dmod.MyDB
    Left = 246
    Top = 407
  end
  object QrConf: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(TotalPeso) TOTAL'
      'FROM pqeits'
      'WHERE Codigo=:P0')
    Left = 182
    Top = 44
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConfTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'DBMBWET.pqeits.TotalPeso'
    end
  end
  object QrConfT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(TotalPeso)  PesoL,'
      'SUM(Volumes*PesoVB) PesoB,'
      'SUM(TotalCusto) TotalCusto'
      'FROM pqeits'
      'WHERE Codigo=:P0')
    Left = 17
    Top = 466
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConfTPesoL: TFloatField
      FieldName = 'PesoL'
    end
    object QrConfTPesoB: TFloatField
      FieldName = 'PesoB'
      Required = True
    end
    object QrConfTTotalCusto: TFloatField
      FieldName = 'TotalCusto'
    end
    object QrConfTValorItem: TFloatField
      FieldName = 'ValorItem'
    end
  end
  object QrSumF: TMySQLQuery
    Database = Dmod.MyDB
    Left = 488
    Top = 397
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumFDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrSumT: TMySQLQuery
    Database = Dmod.MyDB
    Left = 140
    Top = 397
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumTDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrExPQE: TMySQLQuery
    Database = Dmod.MyDB
    Left = 236
    Top = 468
  end
  object QrExPQT: TMySQLQuery
    Database = Dmod.MyDB
    Left = 292
    Top = 468
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object QrExPQI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 264
    Top = 468
  end
  object QrExPQF: TMySQLQuery
    Database = Dmod.MyDB
    Left = 320
    Top = 468
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object PMInclui: TPopupMenu
    OnPopup = PMIncluiPopup
    Left = 308
    Top = 641
    object EntradaporPedidoXNFebaixada1: TMenuItem
      Caption = 'Entrada por Pedido X NFe baixada '
      OnClick = EntradaporPedidoXNFebaixada1Click
    end
    object Encerramentodepedido1: TMenuItem
      Caption = 'Encerramento de pedido'
      OnClick = Encerramentodepedido1Click
    end
    object Entradaparcialdepedido1: TMenuItem
      Caption = 'Entrada parcial de pedido'
      Enabled = False
      OnClick = Entradaparcialdepedido1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Entradasempedido1: TMenuItem
      Caption = 'Entrada &sem pedido NF Modelo 1 ou 1-A'
      OnClick = Entradasempedido1Click
    end
    object OutrosdadosdeNFnormal1: TMenuItem
      Caption = 'Dados &Fiscais NF modelo 1 ou 1-A'
      Enabled = False
      OnClick = OutrosdadosdeNFnormal1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object EntradaporXML2: TMenuItem
      Caption = 'Entrada por &XML modelo 55 (NFe) (antigo)'
      Enabled = False
      OnClick = EntradaporXML2Click
    end
    object EntradaporXMLmodelo55NFenovo1: TMenuItem
      Caption = 'Entrada por &XML modelo 55 (NFe) (novo)'
      OnClick = EntradaporXMLmodelo55NFenovo1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object LanamentofinancxeiroapagarFornecedor1: TMenuItem
      Caption = 'Lan'#231'amento financeiro a pagar Fornecedor'
      OnClick = LanamentofinancxeiroapagarFornecedor1Click
    end
    object LanamentofinancxeiroapagarTransportadora1: TMenuItem
      Caption = 'Lan'#231'amento financeiro a pagar Transportadora'
      OnClick = LanamentofinancxeiroapagarTransportadora1Click
    end
  end
  object QrPedido: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pqped WHERE Codigo=:P0')
    Left = 640
    Top = 425
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPedidoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pqped.Codigo'
    end
    object QrPedidoData: TDateField
      FieldName = 'Data'
      Origin = 'DBMBWET.pqped.Data'
    end
    object QrPedidoEntrega: TDateField
      FieldName = 'Entrega'
      Origin = 'DBMBWET.pqped.Entrega'
    end
    object QrPedidoFornece: TIntegerField
      FieldName = 'Fornece'
      Origin = 'DBMBWET.pqped.Fornece'
    end
    object QrPedidoContato: TWideStringField
      FieldName = 'Contato'
      Origin = 'DBMBWET.pqped.Contato'
      Size = 128
    end
    object QrPedidoTransporte: TIntegerField
      FieldName = 'Transporte'
      Origin = 'DBMBWET.pqped.Transporte'
    end
    object QrPedidoPesoL: TFloatField
      FieldName = 'PesoL'
      Origin = 'DBMBWET.pqped.PesoL'
    end
    object QrPedidoPesoB: TFloatField
      FieldName = 'PesoB'
      Origin = 'DBMBWET.pqped.PesoB'
    end
    object QrPedidoValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DBMBWET.pqped.Valor'
    end
    object QrPedidoEntrada: TFloatField
      FieldName = 'Entrada'
      Origin = 'DBMBWET.pqped.Entrada'
    end
    object QrPedidoSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'DBMBWET.pqped.Sit'
    end
    object QrPedidoLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.pqped.Lk'
    end
    object QrPedidoSetor: TSmallintField
      FieldName = 'Setor'
      Origin = 'DBMBWET.pqped.Setor'
    end
  end
  object QrPedEntra: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(pi.TotalPeso) TotalPeso'
      'FROM pqeits pi, PQE pe'
      'WHERE pi.Insumo=:P0'
      'AND pe.Codigo=pi.Codigo'
      'AND pe.Pedido=:P1')
    Left = 482
    Top = 224
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPedEntraTotalPeso: TFloatField
      FieldName = 'TotalPeso'
      Origin = 'DBMBWET.pqeits.TotalPeso'
    end
  end
  object QrPedidoInsumo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Insumo'
      'FROM pqpedits'
      'WHERE Insumo=:P0'
      'AND Codigo=:P1')
    Left = 510
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object QrUpdPedido: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'UPDATE pqpedits SET'
      'Entrada=:P0'
      'WHERE Codigo=:P1'
      'AND Insumo=:P2')
    Left = 566
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrInsPedido: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'INSERT INTO pqpedits SET'
      'Entrada=:P0,'
      'Codigo=:P1,'
      'Insumo=:P2,'
      'Conta=:P3')
    Left = 538
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
  end
  object QrPedidoConta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Conta) Conta'
      'FROM pqpedits'
      'WHERE Codigo=:P0')
    Left = 594
    Top = 224
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPedidoContaConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'DBMBWET.pqpedits.Conta'
    end
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Volumes*PesoVB) PesoB,'
      'SUM(Volumes*PesoVL) PesoL,'
      'SUM(TotalCusto) Valor,'
      'SUM(Entrada) Entrada'
      'FROM pqpedits'
      'WHERE Codigo=:P0')
    Left = 622
    Top = 224
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaPesoB: TFloatField
      FieldName = 'PesoB'
    end
    object QrSomaPesoL: TFloatField
      FieldName = 'PesoL'
    end
    object QrSomaValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSomaEntrada: TFloatField
      FieldName = 'Entrada'
    end
  end
  object QrSomaUpd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'UPDATE pqped SET'
      'PesoL=:P0,'
      'PesoB=:P1,'
      'Valor=:P2,'
      'Entrada=:P3'
      'WHERE Codigo=:P4')
    Left = 650
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
  end
  object Query2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 368
    Top = 220
  end
  object PMIncluiIts: TPopupMenu
    OnPopup = PMIncluiItsPopup
    Left = 156
    Top = 577
    object Itemdeinsumo1: TMenuItem
      Caption = '&Item de insumo'
      OnClick = Itemdeinsumo1Click
    end
    object IncluiT: TMenuItem
      Caption = 'Lan'#231'amento financeiro a pagar &transportadora'
      OnClick = IncluiTClick
    end
    object IncluiF1: TMenuItem
      Caption = 'Lan'#231'amento financeiro a pagar &fornecedor'
      OnClick = IncluiF1Click
    end
    object N3: TMenuItem
      Caption = '-'
      Visible = False
    end
    object InsumossemNFdecobertura1: TMenuItem
      Caption = 'Insumos sem NF de cobertura'
      Visible = False
      OnClick = InsumossemNFdecobertura1Click
    end
  end
  object PMAlteraIts: TPopupMenu
    OnPopup = PMAlteraItsPopup
    Left = 280
    Top = 577
    object MenuItem1: TMenuItem
      Caption = '&Item de insumo'
      OnClick = MenuItem1Click
    end
    object AlteraT1: TMenuItem
      Caption = 'Duplicata &transportadora'
      OnClick = AlteraT1Click
    end
    object AlteraF1: TMenuItem
      Caption = 'Duplicata &fornecedor'
      OnClick = AlteraF1Click
    end
  end
  object QrPQEIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPQEItsAfterScroll
    OnCalcFields = QrPQEItsCalcFields
    SQL.Strings = (
      'SELECT pq.Nome NOMEPQ, ei.*'
      'FROM pqeits ei'
      'LEFT JOIN pq ON pq.Codigo=ei.Insumo'
      'WHERE ei.Codigo=:P0'
      'ORDER BY Conta')
    Left = 60
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQEItsCUSTOITEM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOITEM'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object QrPQEItsVALORKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALORKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEItsTOTALKGBRUTO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALKGBRUTO'
      Calculated = True
    end
    object QrPQEItsCUSTOKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEItsPRECOKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECOKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPQEItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQEItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrPQEItsVolumes: TIntegerField
      FieldName = 'Volumes'
      Required = True
    end
    object QrPQEItsPesoVB: TFloatField
      FieldName = 'PesoVB'
      Required = True
      DisplayFormat = '#,##0.000'
    end
    object QrPQEItsPesoVL: TFloatField
      FieldName = 'PesoVL'
      Required = True
      DisplayFormat = '#,##0.000'
    end
    object QrPQEItsValorItem: TFloatField
      FieldName = 'ValorItem'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQEItsIPI: TFloatField
      FieldName = 'IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsRIPI: TFloatField
      FieldName = 'RIPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsCFin: TFloatField
      FieldName = 'CFin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsICMS: TFloatField
      FieldName = 'ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsRICMS: TFloatField
      FieldName = 'RICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsTotalCusto: TFloatField
      FieldName = 'TotalCusto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPQEItsTotalPeso: TFloatField
      FieldName = 'TotalPeso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQEItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQEItsprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrPQEItsprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrPQEItsprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrPQEItsprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrPQEItsprod_EX_TIPI: TWideStringField
      FieldName = 'prod_EX_TIPI'
      Size = 3
    end
    object QrPQEItsprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrPQEItsprod_CFOP: TWideStringField
      FieldName = 'prod_CFOP'
      Size = 4
    end
    object QrPQEItsprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 4
    end
    object QrPQEItsprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrPQEItsprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrPQEItsprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrPQEItsprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrPQEItsprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 4
    end
    object QrPQEItsprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrPQEItsprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrPQEItsprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrPQEItsprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrPQEItsprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrPQEItsICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrPQEItsICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrPQEItsICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrPQEItsICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrPQEItsICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrPQEItsIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsPISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrPQEItsCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsCOFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrPQEItsMoeda: TIntegerField
      FieldName = 'Moeda'
    end
    object QrPQEItsCotacao: TFloatField
      FieldName = 'Cotacao'
    end
    object QrPQEItsHowLoad: TSmallintField
      FieldName = 'HowLoad'
    end
    object QrPQEItsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPQEItsFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrPQEItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPQEItsnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrPQEItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQEItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQEItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQEItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQEItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQEItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQEItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQEItsRpICMS: TFloatField
      FieldName = 'RpICMS'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrPQEItsRpPIS: TFloatField
      FieldName = 'RpPIS'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrPQEItsRpCOFINS: TFloatField
      FieldName = 'RpCOFINS'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrPQEItsRvICMS: TFloatField
      FieldName = 'RvICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPQEItsRvPIS: TFloatField
      FieldName = 'RvPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPQEItsRvCOFINS: TFloatField
      FieldName = 'RvCOFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPQEItsCustoPadraoAtz: TFloatField
      FieldName = 'CustoPadraoAtz'
    end
    object QrPQEItsxLote: TWideStringField
      FieldName = 'xLote'
      Size = 120
    end
    object QrPQEItsRpIPI: TFloatField
      FieldName = 'RpIPI'
    end
    object QrPQEItsRvIPI: TFloatField
      FieldName = 'RvIPI'
    end
    object QrPQEItsRpICMSST: TFloatField
      FieldName = 'RpICMSST'
    end
    object QrPQEItsRvICMSST: TFloatField
      FieldName = 'RvICMSST'
    end
    object QrPQEItsprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrPQEItsSqLinked: TIntegerField
      FieldName = 'SqLinked'
    end
    object QrPQEItsIPI_CST: TWideStringField
      FieldName = 'IPI_CST'
      Size = 2
    end
    object QrPQEItsPIS_CST: TWideStringField
      FieldName = 'PIS_CST'
      Size = 2
    end
    object QrPQEItsCOFINS_CST: TWideStringField
      FieldName = 'COFINS_CST'
      Size = 2
    end
    object QrPQEItsOriPart: TIntegerField
      FieldName = 'OriPart'
      Required = True
    end
    object QrPQEItsTES_ICMS: TSmallintField
      FieldName = 'TES_ICMS'
      Required = True
    end
    object QrPQEItsTES_IPI: TSmallintField
      FieldName = 'TES_IPI'
      Required = True
    end
    object QrPQEItsTES_PIS: TSmallintField
      FieldName = 'TES_PIS'
      Required = True
    end
    object QrPQEItsTES_COFINS: TSmallintField
      FieldName = 'TES_COFINS'
      Required = True
    end
    object QrPQEItsEFD_II_C195: TSmallintField
      FieldName = 'EFD_II_C195'
    end
    object QrPQEItsNFeItsI_nItem: TIntegerField
      FieldName = 'NFeItsI_nItem'
      Required = True
    end
    object QrPQEItsFisRegGenCtbD: TIntegerField
      FieldName = 'FisRegGenCtbD'
      Required = True
    end
    object QrPQEItsFisRegGenCtbC: TIntegerField
      FieldName = 'FisRegGenCtbC'
      Required = True
    end
    object QrPQEItsAjusteVL_BC_ICMS: TFloatField
      FieldName = 'AjusteVL_BC_ICMS'
      Required = True
    end
    object QrPQEItsAjusteALIQ_ICMS: TFloatField
      FieldName = 'AjusteALIQ_ICMS'
      Required = True
    end
    object QrPQEItsAjusteVL_ICMS: TFloatField
      FieldName = 'AjusteVL_ICMS'
      Required = True
    end
    object QrPQEItsAjusteVL_OUTROS: TFloatField
      FieldName = 'AjusteVL_OUTROS'
      Required = True
    end
    object QrPQEItsAjusteVL_OPR: TFloatField
      FieldName = 'AjusteVL_OPR'
      Required = True
    end
    object QrPQEItsAjusteVL_RED_BC: TFloatField
      FieldName = 'AjusteVL_RED_BC'
      Required = True
    end
    object QrPQEItsdFab: TDateField
      FieldName = 'dFab'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQEItsdVal: TDateField
      FieldName = 'dVal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQEItsdFab_TXT: TWideStringField
      FieldName = 'dFab_TXT'
      Size = 10
    end
    object QrPQEItsdVal_TXT: TWideStringField
      FieldName = 'dVal_TXT'
      Size = 10
    end
  end
  object QrPQCli: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Peso, Valor '
      'FROM pqcli'
      'WHERE CI=:P0'
      'AND PQ=:P1')
    Left = 338
    Top = 287
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPQCliPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPQCliValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object PMExcluiIts: TPopupMenu
    OnPopup = PMExcluiItsPopup
    Left = 536
    Top = 577
    object ExcluiInsumo1: TMenuItem
      Caption = '&Item de insumo'
      OnClick = ExcluiInsumo1Click
    end
    object ExcluiDuplT1: TMenuItem
      Caption = 'Duplicata &transportadora'
      OnClick = ExcluiDuplT1Click
    end
    object ExcluiDuplF1: TMenuItem
      Caption = 'Duplicata &fornecedor'
      OnClick = ExcluiDuplF1Click
    end
  end
  object QrAnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Insumo FROM pqx '
      'WHERE Tipo=10 AND OrigemCodi=:P0')
    Left = 518
    Top = 286
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAntInsumo: TIntegerField
      FieldName = 'Insumo'
    end
  end
  object mySQLQuery1: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPQEItsCalcFields
    SQL.Strings = (
      'SELECT ei.*, pq.nome NOMEPQ, px.Insumo, px.Peso, px.Valor'
      'FROM pqeits ei'
      'LEFT JOIN pqx   px ON px.OrigemCtrl=ei.Controle'
      'LEFT JOIN pq    pq ON pq.Codigo=px.Insumo'
      'WHERE ei.Codigo=:P0'
      'ORDER BY Conta')
    Left = 126
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object FloatField1: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOITEM'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object FloatField2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALORKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object FloatField3: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALKGBRUTO'
      DisplayFormat = '#,##0.000'
      Calculated = True
    end
    object FloatField4: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object FloatField5: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECOKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object IntegerField2: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object IntegerField3: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object IntegerField4: TIntegerField
      FieldName = 'Volumes'
      Required = True
    end
    object FloatField6: TFloatField
      FieldName = 'PesoVB'
      Required = True
    end
    object FloatField7: TFloatField
      FieldName = 'PesoVL'
      Required = True
    end
    object FloatField8: TFloatField
      FieldName = 'ValorItem'
      Required = True
    end
    object FloatField9: TFloatField
      FieldName = 'IPI'
    end
    object FloatField10: TFloatField
      FieldName = 'RIPI'
    end
    object FloatField11: TFloatField
      FieldName = 'CFin'
    end
    object FloatField12: TFloatField
      FieldName = 'ICMS'
    end
    object FloatField13: TFloatField
      FieldName = 'RICMS'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object IntegerField5: TIntegerField
      FieldName = 'Insumo'
    end
    object FloatField14: TFloatField
      FieldName = 'Peso'
    end
    object FloatField15: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrEmb: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pqf.PQ, gg1.ICMSRec_pRedBC, gg1.IPIRec_pRedBC,'
      'gg1.PISRec_pRedBC, gg1.COFINSRec_pRedBC'
      'FROM pqforemb pfe'
      'LEFT JOIN pqfor pqf ON pqf.Controle=pfe.Controle'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pqf.PQ'
      'WHERE pqf.CI=:P0'
      'AND pqf.IQ=:P1 '
      'AND pfe.cProd=:P2')
    Left = 648
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEmbPQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrEmbICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrEmbIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrEmbPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrEmbCOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
  end
  object QrNFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND ide_nNF=:P3')
    Left = 116
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNFeCabAStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrNFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrNFeCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrNFeCabAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrNFeCabAcStat: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'cStat'
      Calculated = True
    end
    object QrNFeCabAxMotivo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xMotivo'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAcStat_xMotivo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'cStat_xMotivo'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      DisplayFormat = '000'
    end
    object QrNFeCabASTATUS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS_TXT'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabACOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabACOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabACOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabACOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 144
    Top = 16
  end
  object QrNFeItsI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 172
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeItsIICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeItsIIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeItsIPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeItsICOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeItsIMeuID: TIntegerField
      FieldName = 'MeuID'
    end
  end
  object DsNFeItsI: TDataSource
    DataSet = QrNFeItsI
    Left = 200
    Top = 16
  end
  object QrA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 684
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrAversao: TFloatField
      FieldName = 'versao'
    end
    object QrAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrAide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrAide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrAide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrAide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrAide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrAide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrAemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrAemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrAemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrAemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
    end
    object QrAemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrAemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 10
    end
    object QrAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrAemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrAemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Size = 15
    end
    object QrAemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Size = 7
    end
    object QrAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrAdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrAdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrAdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrAdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrAdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrAdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrAdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrAdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrAdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrAdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 10
    end
    object QrAdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrAdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrAISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrAISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
    end
    object QrAISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
    end
    object QrAISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
    end
    object QrAISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
    end
    object QrARetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
    end
    object QrARetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
    end
    object QrARetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
    end
    object QrARetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
    end
    object QrARetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
    end
    object QrARetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
    end
    object QrARetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
    end
    object QrAModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrATransporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Size = 14
    end
    object QrATransporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Size = 11
    end
    object QrATransporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Size = 60
    end
    object QrATransporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
      Size = 14
    end
    object QrATransporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Size = 60
    end
    object QrATransporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Size = 60
    end
    object QrATransporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Size = 2
    end
    object QrARetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
    end
    object QrARetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
    end
    object QrARetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
    end
    object QrARetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
    end
    object QrARetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Size = 4
    end
    object QrARetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Size = 7
    end
    object QrAVeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Size = 8
    end
    object QrAVeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Size = 2
    end
    object QrAVeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
    end
    object QrACobr_Fat_nFat: TWideStringField
      FieldName = 'Cobr_Fat_nFat'
      Size = 60
    end
    object QrACobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
    end
    object QrACobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
    end
    object QrACobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
    end
    object QrAInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 255
    end
    object QrAInfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrAExporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrAExporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
    object QrACompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrACompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrACompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrAinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrAinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrAinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrAinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrAinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrAinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 30
    end
    object QrAinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrAinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrAinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrAinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrA_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrAFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
    end
    object QrACartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrATabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrACondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrAFreteExtra: TFloatField
      FieldName = 'FreteExtra'
    end
    object QrASegurExtra: TFloatField
      FieldName = 'SegurExtra'
    end
    object QrALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrAICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrAICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
    end
    object QrAICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrAICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
    end
    object QrAIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrAIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
    end
    object QrAIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrAIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
    end
    object QrAPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrAPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
    end
    object QrAPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrAPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
    end
    object QrACOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrACOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
    end
    object QrACOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrACOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
    end
    object QrACodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrACodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
  end
  object QrI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 712
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrINivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
  end
  object QrN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 740
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
  end
  object QrO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 768
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
    end
    object QrOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
  end
  object QrQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsq'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 796
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrQPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
    end
    object QrQPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
    end
    object QrQPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
    end
    object QrQPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
    end
    object QrQPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
    end
  end
  object QrS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitss'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 852
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrSCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrSCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
    end
    object QrSCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
    end
    object QrSCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
    end
    object QrSCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
    end
  end
  object PMAltera: TPopupMenu
    OnPopup = PMAlteraPopup
    Left = 444
    Top = 648
    object AlteraDadosEstoque1: TMenuItem
      Caption = 'Altera dados &Estoque'
      OnClick = AlteraDadosEstoque1Click
    end
    object AlteraDadosFiscais1: TMenuItem
      Caption = 'Altera dados &Fiscais'
      OnClick = AlteraDadosFiscais1Click
    end
    object Alterasomentecabealho1: TMenuItem
      Caption = 'Altera somente cabe'#231'alho'
      OnClick = Alterasomentecabealho1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object AlteralanamentofinanceiroFornecedor1: TMenuItem
      Caption = 'Altera lan'#231'amento financeiro Fornecedor'
      OnClick = AlteralanamentofinanceiroFornecedor1Click
    end
    object AlteralanamentofinanceiroTransportador1: TMenuItem
      Caption = 'Altera lan'#231'amento financeiro Transportador'
      OnClick = AlteralanamentofinanceiroTransportador1Click
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM gragrux'
      'WHERE GraGru1=:P0')
    Left = 40
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrRetImpost: TMySQLQuery
    Database = Dmod.MyDB
    Left = 716
    Top = 320
    object QrRetImpostRvICMS: TFloatField
      FieldName = 'RvICMS'
    end
    object QrRetImpostRvPIS: TFloatField
      FieldName = 'RvPIS'
    end
    object QrRetImpostRvCOFINS: TFloatField
      FieldName = 'RvCOFINS'
    end
    object QrRetImpostRvIPI: TFloatField
      FieldName = 'RvIPI'
    end
  end
  object DsRetImpost: TDataSource
    DataSet = QrRetImpost
    Left = 716
    Top = 368
  end
  object QrIvBC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ' SELECT '
      'SUM(prod_vProd) prod_vProd,'
      'SUM(prod_vFrete) prod_vFrete,'
      'SUM(prod_vSeg) prod_vSeg,'
      'SUM(prod_vOutro) prod_vOutro,'
      'SUM(prod_vDesc) prod_vDesc'
      'FROM pqeits'
      'WHERE Codigo=15')
    Left = 946
    Top = 226
    object QrIvBCprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrIvBCprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrIvBCprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrIvBCprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrIvBCprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
  end
  object QrAvBC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT'
      'SUM(ICMSTot_vProd) ICMSTot_vProd,'
      'SUM(ICMSTot_vFrete) ICMSTot_vFrete,'
      'SUM(ICMSTot_vSeg) ICMSTot_vSeg,'
      'SUM(ICMSTot_vOutro) ICMSTot_vOutro,'
      'SUM(ICMSTot_vDesc) ICMSTot_vDesc'
      'FROM pqe'
      'WHERE Codigo=15 ')
    Left = 886
    Top = 226
    object QrAvBCICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrAvBCICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrAvBCICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrAvBCICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrAvBCICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
  end
  object QrPediVda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvd.*,'
      'SUM(pvi.ValBru) ValBru,'
      'SUM(pvi.vProd) vProd,'
      'SUM(pvi.vFrete) vFrete,'
      'SUM(pvi.vSeg) vSeg,'
      'SUM(pvi.vOutro) vOutro,'
      'SUM(pvi.vDesc) vDesc,'
      'SUM(pvi.vBC) vBC'
      '  '
      'FROM pedivda pvd  '
      'LEFT JOIN pedivdaits pvi ON pvi.Codigo=pvd.Codigo'
      'WHERE pvd.Codigo>0')
    Left = 478
    Top = 518
    object QrPediVdaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediVdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPediVdaEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPediVdaCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPediVdaDtaEmiss: TDateField
      FieldName = 'DtaEmiss'
      Required = True
    end
    object QrPediVdaDtaEntra: TDateField
      FieldName = 'DtaEntra'
      Required = True
    end
    object QrPediVdaDtaInclu: TDateField
      FieldName = 'DtaInclu'
      Required = True
    end
    object QrPediVdaDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
      Required = True
    end
    object QrPediVdaRepresen: TIntegerField
      FieldName = 'Represen'
      Required = True
    end
    object QrPediVdaPrioridade: TSmallintField
      FieldName = 'Prioridade'
      Required = True
    end
    object QrPediVdaCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Required = True
    end
    object QrPediVdaMoeda: TIntegerField
      FieldName = 'Moeda'
      Required = True
    end
    object QrPediVdaSituacao: TIntegerField
      FieldName = 'Situacao'
      Required = True
    end
    object QrPediVdaTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Required = True
    end
    object QrPediVdaMotivoSit: TIntegerField
      FieldName = 'MotivoSit'
      Required = True
    end
    object QrPediVdaLoteProd: TIntegerField
      FieldName = 'LoteProd'
      Required = True
    end
    object QrPediVdaPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
    object QrPediVdaFretePor: TSmallintField
      FieldName = 'FretePor'
      Required = True
    end
    object QrPediVdaTransporta: TIntegerField
      FieldName = 'Transporta'
      Required = True
    end
    object QrPediVdaRedespacho: TIntegerField
      FieldName = 'Redespacho'
      Required = True
    end
    object QrPediVdaRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Required = True
    end
    object QrPediVdaDesoAces_V: TFloatField
      FieldName = 'DesoAces_V'
      Required = True
    end
    object QrPediVdaDesoAces_P: TFloatField
      FieldName = 'DesoAces_P'
      Required = True
    end
    object QrPediVdaFrete_V: TFloatField
      FieldName = 'Frete_V'
      Required = True
    end
    object QrPediVdaFrete_P: TFloatField
      FieldName = 'Frete_P'
      Required = True
    end
    object QrPediVdaSeguro_V: TFloatField
      FieldName = 'Seguro_V'
      Required = True
    end
    object QrPediVdaSeguro_P: TFloatField
      FieldName = 'Seguro_P'
      Required = True
    end
    object QrPediVdaTotalQtd: TFloatField
      FieldName = 'TotalQtd'
      Required = True
    end
    object QrPediVdaTotal_Vlr: TFloatField
      FieldName = 'Total_Vlr'
      Required = True
    end
    object QrPediVdaTotal_Des: TFloatField
      FieldName = 'Total_Des'
      Required = True
    end
    object QrPediVdaTotal_Tot: TFloatField
      FieldName = 'Total_Tot'
      Required = True
    end
    object QrPediVdaObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPediVdaAFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
      Required = True
    end
    object QrPediVdaAFP_Per: TFloatField
      FieldName = 'AFP_Per'
      Required = True
    end
    object QrPediVdaComisFat: TFloatField
      FieldName = 'ComisFat'
      Required = True
    end
    object QrPediVdaComisRec: TFloatField
      FieldName = 'ComisRec'
      Required = True
    end
    object QrPediVdaCartEmis: TIntegerField
      FieldName = 'CartEmis'
      Required = True
    end
    object QrPediVdaQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrPediVdaValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrPediVdaTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPediVdaEntSai: TSmallintField
      FieldName = 'EntSai'
      Required = True
    end
    object QrPediVdaCondPag: TWideStringField
      FieldName = 'CondPag'
      Size = 47
    end
    object QrPediVdaCentroCust: TWideStringField
      FieldName = 'CentroCust'
      Size = 6
    end
    object QrPediVdaItemCust: TWideStringField
      FieldName = 'ItemCust'
      Size = 6
    end
    object QrPediVdaUsaReferen: TSmallintField
      FieldName = 'UsaReferen'
      Required = True
    end
    object QrPediVdaidDest: TSmallintField
      FieldName = 'idDest'
      Required = True
    end
    object QrPediVdaindFinal: TSmallintField
      FieldName = 'indFinal'
      Required = True
    end
    object QrPediVdaindPres: TSmallintField
      FieldName = 'indPres'
      Required = True
    end
    object QrPediVdaIndSinc: TSmallintField
      FieldName = 'IndSinc'
      Required = True
    end
    object QrPediVdafinNFe: TSmallintField
      FieldName = 'finNFe'
      Required = True
    end
    object QrPediVdaRetiradaUsa: TSmallintField
      FieldName = 'RetiradaUsa'
      Required = True
    end
    object QrPediVdaRetiradaEnti: TIntegerField
      FieldName = 'RetiradaEnti'
      Required = True
    end
    object QrPediVdaEntregaUsa: TSmallintField
      FieldName = 'EntregaUsa'
      Required = True
    end
    object QrPediVdaEntregaEnti: TIntegerField
      FieldName = 'EntregaEnti'
      Required = True
    end
    object QrPediVdaCNPJCPFAvulso: TWideStringField
      FieldName = 'CNPJCPFAvulso'
      Size = 14
    end
    object QrPediVdaRazaoNomeAvulso: TWideStringField
      FieldName = 'RazaoNomeAvulso'
      Size = 60
    end
    object QrPediVdaInfIntermedEnti: TIntegerField
      FieldName = 'InfIntermedEnti'
      Required = True
    end
    object QrPediVdaEmiteAvulso: TSmallintField
      FieldName = 'EmiteAvulso'
      Required = True
    end
    object QrPediVdaLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrPediVdaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPediVdaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPediVdaUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrPediVdaUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrPediVdaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPediVdaAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPediVdaAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPediVdaAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPediVdaReferenPedi: TWideStringField
      FieldName = 'ReferenPedi'
      Size = 60
    end
    object QrPediVdaDesco_V: TFloatField
      FieldName = 'Desco_V'
      Required = True
    end
    object QrPediVdaDesco_P: TFloatField
      FieldName = 'Desco_P'
      Required = True
    end
    object QrPediVdaIND_PGTO: TWideStringField
      FieldName = 'IND_PGTO'
      Required = True
      Size = 1
    end
    object QrPediVdaValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrPediVdavProd: TFloatField
      FieldName = 'vProd'
    end
    object QrPediVdavFrete: TFloatField
      FieldName = 'vFrete'
    end
    object QrPediVdavSeg: TFloatField
      FieldName = 'vSeg'
    end
    object QrPediVdavOutro: TFloatField
      FieldName = 'vOutro'
    end
    object QrPediVdavDesc: TFloatField
      FieldName = 'vDesc'
    end
    object QrPediVdavBC: TFloatField
      FieldName = 'vBC'
    end
    object QrPediVdaMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
  end
  object QrAltIts: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 366
    Top = 558
    object QrAltItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object PMBaixa: TPopupMenu
    Left = 650
    Top = 646
    object ETE1: TMenuItem
      Caption = 'ETE'
      OnClick = ETE1Click
    end
    object EPI1: TMenuItem
      Caption = 'EPI'
      OnClick = EPI1Click
    end
    object Manuteno1: TMenuItem
      Caption = 'Manuten'#231#227'o'
      OnClick = Manuteno1Click
    end
    object Outros1: TMenuItem
      Caption = 'Outros'
      OnClick = Outros1Click
      object Consumo1: TMenuItem
        Caption = 'Consumo'
      end
      object Devoluo1: TMenuItem
        Caption = 'Devolu'#231#227'o'
      end
    end
  end
  object QrPVI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(vProd) prod_vProd,'
      'SUM(vFrete) prod_vFrete,'
      'SUM(vSeg) prod_vSeg,'
      'SUM(vOutro) prod_vOutro,'
      'SUM(vDesc) prod_vDesc'
      'FROM pedivdaits'
      'WHERE Codigo>0')
    Left = 914
    Top = 178
    object QrPVIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrPVIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrPVIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrPVIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrPVIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
  end
  object PMFiscal: TPopupMenu
    OnPopup = PMFiscalPopup
    Left = 652
    Top = 566
    object IncluiDocumento1: TMenuItem
      Caption = '&Inclui Documento'
      OnClick = IncluiDocumento1Click
    end
    object AlteraDocumento1: TMenuItem
      Caption = '&Altera Documento'
      OnClick = AlteraDocumento1Click
    end
    object ExcluiDocumento1: TMenuItem
      Caption = '&Exclui Documento'
      OnClick = ExcluiDocumento1Click
    end
    object MenuItem2: TMenuItem
      Caption = '-'
    end
    object IncluiItemdodocumento1: TMenuItem
      Caption = 'Inclui Item do documento'
      OnClick = IncluiItemdodocumento1Click
    end
    object AlteraoItemselecionadododocumento1: TMenuItem
      Caption = 'Altera o  Item selecionado do documento'
      OnClick = AlteraoItemselecionadododocumento1Click
    end
    object ExcluioItemselecionadododocumento1: TMenuItem
      Caption = 'Exclui o  Item selecionado do documento'
      OnClick = ExcluioItemselecionadododocumento1Click
    end
  end
  object QrEfdInnNFsCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEfdInnNFsCabBeforeClose
    AfterScroll = QrEfdInnNFsCabAfterScroll
    SQL.Strings = (
      'SELECT vin.*, '
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  '
      'FROM efdinnnfscab vin '
      'LEFT JOIN entidades ter ON ter.Codigo=vin.Terceiro '
      ' ')
    Left = 844
    Top = 290
    object QrEfdInnNFsCabNO_TER: TWideStringField
      FieldName = 'NO_TER'
      Size = 100
    end
    object QrEfdInnNFsCabMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnNFsCabMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnNFsCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnNFsCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnNFsCabControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnNFsCabTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrEfdInnNFsCabPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrEfdInnNFsCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrEfdInnNFsCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEfdInnNFsCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrEfdInnNFsCabValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEfdInnNFsCabMotorista: TIntegerField
      FieldName = 'Motorista'
      Required = True
    end
    object QrEfdInnNFsCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrEfdInnNFsCabCOD_MOD: TSmallintField
      FieldName = 'COD_MOD'
      Required = True
    end
    object QrEfdInnNFsCabCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Required = True
    end
    object QrEfdInnNFsCabSER: TIntegerField
      FieldName = 'SER'
      Required = True
    end
    object QrEfdInnNFsCabNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Required = True
    end
    object QrEfdInnNFsCabCHV_NFE: TWideStringField
      FieldName = 'CHV_NFE'
      Size = 44
    end
    object QrEfdInnNFsCabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
      Required = True
    end
    object QrEfdInnNFsCabDT_DOC: TDateField
      FieldName = 'DT_DOC'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEfdInnNFsCabDT_E_S: TDateField
      FieldName = 'DT_E_S'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEfdInnNFsCabVL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabIND_PGTO: TWideStringField
      FieldName = 'IND_PGTO'
      Size = 1
    end
    object QrEfdInnNFsCabVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_MERC: TFloatField
      FieldName = 'VL_MERC'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabIND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEfdInnNFsCabVL_FRT: TFloatField
      FieldName = 'VL_FRT'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_SEG: TFloatField
      FieldName = 'VL_SEG'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_OUT_DA: TFloatField
      FieldName = 'VL_OUT_DA'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_PIS_ST: TFloatField
      FieldName = 'VL_PIS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_COFINS_ST: TFloatField
      FieldName = 'VL_COFINS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabNFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
      Required = True
    end
    object QrEfdInnNFsCabNFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
      Required = True
    end
    object QrEfdInnNFsCabNFe_StaLnk: TSmallintField
      FieldName = 'NFe_StaLnk'
      Required = True
    end
    object QrEfdInnNFsCabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
      Required = True
    end
    object QrEfdInnNFsCabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrEfdInnNFsCabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrEfdInnNFsCabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
      Required = True
    end
    object QrEfdInnNFsCabCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrEfdInnNFsCabRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
    end
    object QrEfdInnNFsCabTpEntrd: TIntegerField
      FieldName = 'TpEntrd'
    end
  end
  object DsEfdInnNFsCab: TDataSource
    DataSet = QrEfdInnNFsCab
    Left = 844
    Top = 340
  end
  object QrEfdInnNFsIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, '
      'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, '
      'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, '
      'pgt.Tipo_Item) Tipo_Item '
      'FROM efdinnnfsits     vin'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ')
    Left = 844
    Top = 394
    object QrEfdInnNFsItsMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnNFsItsMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnNFsItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnNFsItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnNFsItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrEfdInnNFsItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrEfdInnNFsItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnNFsItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEfdInnNFsItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object QrEfdInnNFsItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrEfdInnNFsItsprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      Required = True
    end
    object QrEfdInnNFsItsprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrEfdInnNFsItsprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrEfdInnNFsItsprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrEfdInnNFsItsQTD: TFloatField
      FieldName = 'QTD'
      Required = True
    end
    object QrEfdInnNFsItsUNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrEfdInnNFsItsVL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsIND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
    object QrEfdInnNFsItsCFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrEfdInnNFsItsCOD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrEfdInnNFsItsVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_ST: TFloatField
      FieldName = 'ALIQ_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsIND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrEfdInnNFsItsCOD_ENQ: TWideStringField
      FieldName = 'COD_ENQ'
      Size = 3
    end
    object QrEfdInnNFsItsVL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_IPI: TFloatField
      FieldName = 'ALIQ_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_PIS_p: TFloatField
      FieldName = 'ALIQ_PIS_p'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsQUANT_BC_PIS: TFloatField
      FieldName = 'QUANT_BC_PIS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_PIS_r: TFloatField
      FieldName = 'ALIQ_PIS_r'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_COFINS_p: TFloatField
      FieldName = 'ALIQ_COFINS_p'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField
      FieldName = 'QUANT_BC_COFINS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_COFINS_r: TFloatField
      FieldName = 'ALIQ_COFINS_r'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsCOD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEfdInnNFsItsVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrEfdInnNFsItsxLote: TWideStringField
      FieldName = 'xLote'
    end
    object QrEfdInnNFsItsOri_IPIpIPI: TFloatField
      FieldName = 'Ori_IPIpIPI'
      Required = True
    end
    object QrEfdInnNFsItsOri_IPIvIPI: TFloatField
      FieldName = 'Ori_IPIvIPI'
      Required = True
    end
    object QrEfdInnNFsItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEfdInnNFsItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrEfdInnNFsItsGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrEfdInnNFsItsNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrEfdInnNFsItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrEfdInnNFsItsEx_TIPI: TWideStringField
      FieldName = 'Ex_TIPI'
      Size = 3
    end
    object QrEfdInnNFsItsGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEfdInnNFsItsTipo_Item: TSmallintField
      FieldName = 'Tipo_Item'
    end
    object QrEfdInnNFsItsCST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrEfdInnNFsItsCST_PIS: TWideStringField
      FieldName = 'CST_PIS'
      Size = 2
    end
    object QrEfdInnNFsItsCST_COFINS: TWideStringField
      FieldName = 'CST_COFINS'
      Size = 2
    end
    object QrEfdInnNFsItsCST_ICMS: TWideStringField
      FieldName = 'CST_ICMS'
      Size = 3
    end
    object QrEfdInnNFsItsAjusteVL_BC_ICMS: TFloatField
      FieldName = 'AjusteVL_BC_ICMS'
    end
    object QrEfdInnNFsItsAjusteALIQ_ICMS: TFloatField
      FieldName = 'AjusteALIQ_ICMS'
    end
    object QrEfdInnNFsItsAjusteVL_ICMS: TFloatField
      FieldName = 'AjusteVL_ICMS'
    end
    object QrEfdInnNFsItsAjusteVL_OUTROS: TFloatField
      FieldName = 'AjusteVL_OUTROS'
    end
  end
  object DsEfdInnNFsIts: TDataSource
    DataSet = QrEfdInnNFsIts
    Left = 844
    Top = 444
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CST_B, IPI_CST, PIS_CST, COFINS_CST, '
      'ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq,  '
      'COFINSRec_pAliq  '
      'FROM gragru1 '
      'WHERE Nivel1>0')
    Left = 956
    Top = 396
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGru1IPI_CST: TSmallintField
      FieldName = 'IPI_CST'
      Required = True
    end
    object QrGraGru1PIS_CST: TSmallintField
      FieldName = 'PIS_CST'
      Required = True
    end
    object QrGraGru1COFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
      Required = True
    end
    object QrGraGru1ICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      Required = True
    end
    object QrGraGru1IPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      Required = True
    end
    object QrGraGru1PISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      Required = True
    end
    object QrGraGru1COFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      Required = True
    end
  end
  object QrEIN: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 588
    Top = 503
    object QrEINControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object PMCTe: TPopupMenu
    OnPopup = PMCTePopup
    Left = 768
    Top = 551
    object IncluiConhecimentodefrete1: TMenuItem
      Caption = '&Inclui Conhecimento de frete'
      OnClick = IncluiConhecimentodefrete1Click
    end
    object AlteraConhecimentodefrete1: TMenuItem
      Caption = '&Altera Conhecimento de frete'
      OnClick = AlteraConhecimentodefrete1Click
    end
    object ExcluiConhecimentodefrete1: TMenuItem
      Caption = '&Exclui Conhecimento de frete'
      OnClick = ExcluiConhecimentodefrete1Click
    end
  end
  object QrEfdInnCTsCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vic.*, '
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  '
      'FROM efdinnctscab vic '
      'LEFT JOIN entidades ter ON ter.Codigo=vic.Terceiro ')
    Left = 952
    Top = 291
    object QrEfdInnCTsCabMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnCTsCabMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnCTsCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnCTsCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnCTsCabControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnCTsCabIsLinked: TSmallintField
      FieldName = 'IsLinked'
      Required = True
    end
    object QrEfdInnCTsCabSqLinked: TIntegerField
      FieldName = 'SqLinked'
      Required = True
    end
    object QrEfdInnCTsCabCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrEfdInnCTsCabTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrEfdInnCTsCabPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrEfdInnCTsCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrEfdInnCTsCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEfdInnCTsCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrEfdInnCTsCabValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEfdInnCTsCabMotorista: TIntegerField
      FieldName = 'Motorista'
      Required = True
    end
    object QrEfdInnCTsCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrEfdInnCTsCabIND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabIND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabCOD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Required = True
      Size = 2
    end
    object QrEfdInnCTsCabCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Required = True
    end
    object QrEfdInnCTsCabSER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEfdInnCTsCabSUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEfdInnCTsCabNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Required = True
    end
    object QrEfdInnCTsCabCHV_CTE: TWideStringField
      FieldName = 'CHV_CTE'
      Size = 44
    end
    object QrEfdInnCTsCabCTeStatus: TIntegerField
      FieldName = 'CTeStatus'
      Required = True
    end
    object QrEfdInnCTsCabDT_DOC: TDateField
      FieldName = 'DT_DOC'
      Required = True
    end
    object QrEfdInnCTsCabDT_A_P: TDateField
      FieldName = 'DT_A_P'
      Required = True
    end
    object QrEfdInnCTsCabTP_CT_e: TSmallintField
      FieldName = 'TP_CT_e'
      Required = True
    end
    object QrEfdInnCTsCabCHV_CTE_REF: TWideStringField
      FieldName = 'CHV_CTE_REF'
      Size = 44
    end
    object QrEfdInnCTsCabVL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Required = True
    end
    object QrEfdInnCTsCabVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
    end
    object QrEfdInnCTsCabIND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEfdInnCTsCabVL_SERV: TFloatField
      FieldName = 'VL_SERV'
      Required = True
    end
    object QrEfdInnCTsCabVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_NT: TFloatField
      FieldName = 'VL_NT'
      Required = True
    end
    object QrEfdInnCTsCabCOD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEfdInnCTsCabCOD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 30
    end
    object QrEfdInnCTsCabCOD_MUN_ORIG: TIntegerField
      FieldName = 'COD_MUN_ORIG'
      Required = True
    end
    object QrEfdInnCTsCabCOD_MUN_DEST: TIntegerField
      FieldName = 'COD_MUN_DEST'
      Required = True
    end
    object QrEfdInnCTsCabNFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
      Required = True
    end
    object QrEfdInnCTsCabNFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
      Required = True
    end
    object QrEfdInnCTsCabNFe_StaLnk: TSmallintField
      FieldName = 'NFe_StaLnk'
      Required = True
    end
    object QrEfdInnCTsCabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
      Required = True
    end
    object QrEfdInnCTsCabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrEfdInnCTsCabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrEfdInnCTsCabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
      Required = True
    end
    object QrEfdInnCTsCabCST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabCFOP: TIntegerField
      FieldName = 'CFOP'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_OPR: TFloatField
      FieldName = 'VL_OPR'
      Required = True
    end
    object QrEfdInnCTsCabVL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
      Required = True
    end
    object QrEfdInnCTsCabCOD_OBS: TWideStringField
      FieldName = 'COD_OBS'
      Size = 6
    end
    object QrEfdInnCTsCabNO_TER: TWideStringField
      FieldName = 'NO_TER'
      Size = 100
    end
    object QrEfdInnCTsCabRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
    end
    object QrEfdInnCTsCabIND_NAT_FRT: TWideStringField
      FieldName = 'IND_NAT_FRT'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabVL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      Required = True
    end
    object QrEfdInnCTsCabCST_PIS: TWideStringField
      FieldName = 'CST_PIS'
      Size = 2
    end
    object QrEfdInnCTsCabNAT_BC_CRED: TWideStringField
      FieldName = 'NAT_BC_CRED'
      Required = True
      Size = 2
    end
    object QrEfdInnCTsCabVL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_PIS: TFloatField
      FieldName = 'ALIQ_PIS'
      Required = True
    end
    object QrEfdInnCTsCabVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
    end
    object QrEfdInnCTsCabCST_COFINS: TWideStringField
      FieldName = 'CST_COFINS'
      Size = 2
    end
    object QrEfdInnCTsCabVL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_COFINS: TFloatField
      FieldName = 'ALIQ_COFINS'
      Required = True
    end
    object QrEfdInnCTsCabVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
    end
  end
  object DsEfdInnCTsCab: TDataSource
    DataSet = QrEfdInnCTsCab
    Left = 952
    Top = 344
  end
  object QrR: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsr'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 824
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrRPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
    end
    object QrRPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
    end
    object QrRPISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
    end
    object QrRPISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
    end
    object QrRPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
    end
    object QrRPISST_fatorBCST: TFloatField
      FieldName = 'PISST_fatorBCST'
    end
  end
  object QrT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitst'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 880
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrTCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
    end
    object QrTCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
    end
    object QrTCOFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
    end
    object QrTCOFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
    end
    object QrTCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
    end
    object QrTCOFINSST_fatorBCST: TFloatField
      FieldName = 'COFINSST_fatorBCST'
    end
  end
end
