object DfModAppGraG1: TDfModAppGraG1
  Left = 0
  Top = 0
  Caption = 'DfModAppGraG1'
  ClientHeight = 204
  ClientWidth = 312
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  PixelsPerInch = 96
  TextHeight = 13
  object PB1: TProgressBar
    Left = 4
    Top = 80
    Width = 253
    Height = 17
    TabOrder = 0
  end
  object QrPQEIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Nome NOMEPQ, ei.*'
      'FROM pqeits ei'
      'LEFT JOIN pq ON pq.Codigo=ei.Insumo'
      'WHERE ei.Codigo=:P0'
      'ORDER BY Conta')
    Left = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQEItsCUSTOITEM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOITEM'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object QrPQEItsVALORKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALORKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEItsTOTALKGBRUTO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALKGBRUTO'
      Calculated = True
    end
    object QrPQEItsCUSTOKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEItsPRECOKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECOKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPQEItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQEItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrPQEItsVolumes: TIntegerField
      FieldName = 'Volumes'
      Required = True
    end
    object QrPQEItsPesoVB: TFloatField
      FieldName = 'PesoVB'
      Required = True
      DisplayFormat = '#,##0.000'
    end
    object QrPQEItsPesoVL: TFloatField
      FieldName = 'PesoVL'
      Required = True
      DisplayFormat = '#,##0.000'
    end
    object QrPQEItsValorItem: TFloatField
      FieldName = 'ValorItem'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQEItsIPI: TFloatField
      FieldName = 'IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsRIPI: TFloatField
      FieldName = 'RIPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsCFin: TFloatField
      FieldName = 'CFin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsICMS: TFloatField
      FieldName = 'ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsRICMS: TFloatField
      FieldName = 'RICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsTotalCusto: TFloatField
      FieldName = 'TotalCusto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPQEItsTotalPeso: TFloatField
      FieldName = 'TotalPeso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQEItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQEItsprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrPQEItsprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrPQEItsprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrPQEItsprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrPQEItsprod_EX_TIPI: TWideStringField
      FieldName = 'prod_EX_TIPI'
      Size = 3
    end
    object QrPQEItsprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrPQEItsprod_CFOP: TWideStringField
      FieldName = 'prod_CFOP'
      Size = 4
    end
    object QrPQEItsprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 4
    end
    object QrPQEItsprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrPQEItsprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrPQEItsprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrPQEItsprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrPQEItsprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 4
    end
    object QrPQEItsprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrPQEItsprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrPQEItsprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrPQEItsprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrPQEItsprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrPQEItsICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrPQEItsICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrPQEItsICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrPQEItsICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrPQEItsICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrPQEItsIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrPQEItsIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrPQEItsPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsPISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrPQEItsCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrPQEItsCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsCOFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrPQEItsxLote: TWideStringField
      FieldName = 'xLote'
      Size = 120
    end
    object QrPQEItsdFab: TDateField
      FieldName = 'dFab'
      Required = True
    end
    object QrPQEItsdVal: TDateField
      FieldName = 'dVal'
      Required = True
    end
  end
  object frxErrGrandz: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 84
    Top = 4
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsErrGrandz
        DataSetName = 'frxDsErrGrandz'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 56.692940240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 41.574664020000000000
          Width = 302.362136380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'GRANDEZAS INESPERADAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Gdz GGX')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 41.574830000000000000
          Width = 113.385865830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Gdz Sel')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Gdz xco')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsErrGrandz
        DataSetName = 'frxDsErrGrandz'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 302.362170550000000000
          Height = 15.118110240000000000
          DataField = 'NO_GG1'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrGrandz."NO_GG1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Nivel1'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrGrandz."Nivel1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Reduzido'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrGrandz."Reduzido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Grandeza'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrGrandz."Grandeza"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 113.385865830000000000
          Height = 15.118110240000000000
          DataField = 'Sigla'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrGrandz."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'dif_Grandeza'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrGrandz."dif_Grandeza"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'xco_Grandeza'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrGrandz."xco_Grandeza"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrErrGrandz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ggx.GraGru1 Nivel1, ggx.Controle Reduzido,  '
      'gg1.Nome NO_GG1, med.Sigla, med.Grandeza, '
      'xco.Grandeza xco_Grandeza,  '
      'IF(xco.Grandeza > 0, xco.Grandeza,  '
      '  IF((ggx.GraGruY<2048 OR  '
      '  xco.CouNiv2<>1), 2,  '
      '  IF(xco.CouNiv2=1, 1, -1)))  dif_Grandeza '
      ' '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1  '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2  '
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed '
      
        'WHERE vmi.DataHora BETWEEN "2016-01-01" AND "2016-01-31 23:59:59' +
        '" '
      'AND vmi.GraGruX <> 0 '
      'AND ((IF(xco.Grandeza > 0, xco.Grandeza,  '
      '  IF((ggx.GraGruY<2048 OR  '
      '  xco.CouNiv2<>1), 2,  '
      '  IF(xco.CouNiv2=1, 1, -1))) <> med.Grandeza)) ')
    Left = 84
    Top = 52
    object QrErrGrandzNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrErrGrandzReduzido: TIntegerField
      FieldName = 'Reduzido'
    end
    object QrErrGrandzNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrErrGrandzSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrErrGrandzGrandeza: TFloatField
      FieldName = 'Grandeza'
    end
    object QrErrGrandzxco_Grandeza: TFloatField
      FieldName = 'xco_Grandeza'
    end
    object QrErrGrandzdif_Grandeza: TFloatField
      FieldName = 'dif_Grandeza'
    end
  end
  object frxDsErrGrandz: TfrxDBDataset
    UserName = 'frxDsErrGrandz'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nivel1=Nivel1'
      'Reduzido=Reduzido'
      'NO_GG1=NO_GG1'
      'Sigla=Sigla'
      'dif_Grandeza=dif_Grandeza'
      'xco_Grandeza=xco_Grandeza'
      'Grandeza=Grandeza')
    DataSet = QrErrGrandz
    BCDToCurrency = False
    DataSetOptions = []
    Left = 84
    Top = 100
  end
  object QrErrNCMs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome, gg1.NCM'
      'FROM pqx pqx  '
      'LEFT JOIN gragrux ggx ON ggx.GraGru1=pqx.Insumo  '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed  '
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle   '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1   '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2   '
      'WHERE pqx.Tipo=0'
      'AND pqx.Empresa=-11'
      'AND pqx.DataX BETWEEN "2018-11-01" AND "2018-12-01" '
      'AND gg1.Nivel2<>-4 '
      'AND gg1.NCM=""  '
      'GROUP BY pqx.Insumo, pqx.CliDest'
      ' '
      'UNION '
      ' '
      'SELECT gg1.Nivel1, gg1.Nome, gg1.NCM'
      'FROM pqx pqx  '
      'LEFT JOIN gragrux ggx ON ggx.GraGru1=pqx.Insumo  '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed  '
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle   '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1   '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2   '
      'WHERE pqx.Tipo=0'
      'AND pqx.Empresa=-11'
      'AND pqx.DataX BETWEEN "2018-11-01" AND "2018-12-01" '
      'AND gg1.Nivel2=-4 '
      'AND gg1.NCM=""'
      'GROUP BY pqx.Insumo, pqx.CliDest   '
      ''
      'UNION '
      ' '
      'SELECT gg1.Nivel1, gg1.Nome, gg1.NCM'
      'FROM vsmovits vmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed  '
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle   '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1   '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2   '
      'WHERE gg1.NCM=""'
      'AND vmi.DataHora BETWEEN "2018-11-01" AND "2018-12-01"'
      ''
      'ORDER BY Nome   ')
    Left = 180
    Top = 52
    object QrErrNCMsNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrErrNCMsNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrErrNCMsNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
  end
  object frxDsErrNCMs: TfrxDBDataset
    UserName = 'frxDsErrNCMs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nivel1=Nivel1'
      'Nome=Nome'
      'NCM=NCM')
    DataSet = QrErrNCMs
    BCDToCurrency = False
    DataSetOptions = []
    Left = 180
    Top = 100
  end
  object frxErrNCMs: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 180
    Top = 4
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsErrNCMs
        DataSetName = 'frxDsErrNCMs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 56.692940240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 41.574664020000000000
          Width = 514.015816380000100000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'NCMs Inesperados')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#237'vel 1')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 41.574830000000000000
          Width = 113.385865830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsErrNCMs
        DataSetName = 'frxDsErrNCMs'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 514.015850550000000000
          Height = 15.118110240000000000
          DataField = 'Nome'
          DataSet = frxDsErrNCMs
          DataSetName = 'frxDsErrNCMs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNCMs."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Nivel1'
          DataSet = frxDsErrNCMs
          DataSetName = 'frxDsErrNCMs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrNCMs."Nivel1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 113.385865830000000000
          Height = 15.118110240000000000
          DataField = 'NCM'
          DataSet = frxDsErrNCMs
          DataSetName = 'frxDsErrNCMs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNCMs."NCM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
