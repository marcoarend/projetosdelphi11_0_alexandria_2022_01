unit PalletsOrigem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, DBGrids, Db, DmkDAC_PF,
  mySQLDbTables, dmkEdit, dmkImage, UnDmkEnums;

type
  TFmPalletsOrigem = class(TForm)
    PainelDados: TPanel;
    PainelTitulo1: TPanel;
    Label14: TLabel;
    EdMarca: TdmkEdit;
    EdLote: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    DBGrid1: TDBGrid;
    QrLoc: TmySQLQuery;
    DsLoc: TDataSource;
    QrPalet: TmySQLQuery;
    QrPaletPalet: TIntegerField;
    QrPaletData: TDateField;
    QrPaletOS: TIntegerField;
    QrPaletCliInt: TIntegerField;
    QrPaletProcede: TIntegerField;
    QrPaletLote: TIntegerField;
    QrPaletMarca: TWideStringField;
    QrPaletNOMECLIINT: TWideStringField;
    QrPaletNOMEPROCEDE: TWideStringField;
    QrPaletPecas: TFloatField;
    QrPaletSaida1: TFloatField;
    QrPaletSaida2: TFloatField;
    QrPaletSaldo: TFloatField;
    QrLocNOMECLIENTEI: TWideStringField;
    QrLocNOMEPROCEDENCIA: TWideStringField;
    QrLocControle: TIntegerField;
    QrLocData: TDateField;
    QrLocClienteI: TIntegerField;
    QrLocProcedencia: TIntegerField;
    QrLocMarca: TWideStringField;
    QrLocPecas: TFloatField;
    QrLocPecasOut: TFloatField;
    QrLocPecasSal: TFloatField;
    QrLocLote: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtCancela: TBitBtn;
    procedure BtOKClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdMarcaExit(Sender: TObject);
    procedure EdLoteExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FPallet: Integer;
    procedure ReabreLoc;
  end;

  var
  FmPalletsOrigem: TFmPalletsOrigem;

implementation

uses UnMyObjects, Module, Pallets, ModuleGeral;

{$R *.DFM}

procedure TFmPalletsOrigem.BtOKClick(Sender: TObject);
begin
  //formatar Dados DBGrid1!

  QrPalet.Close;
  QrPalet.Params[0].AsInteger := QrLocControle.Value;
  UnDmkDAC_PF.AbreQuery(QrPalet, DModG.MyPID_DB);
  if QrPalet.RecordCount = 0 then
  begin
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Clear;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('INSERT INTO palet SET Palet=:P0, Data=:P1, OS=:P2, ');
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('CliInt=:P3, Lote=:P4, Marca=:P5, NOMECLIINT=:P6, ');
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('NOMEPROCEDE=:P7, Pecas=:P8, Saida1=:P9, ');
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('Saida2=:P10, Saldo=:P11');
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('');
    //
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[00].AsInteger := FPallet;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[01].AsString  := Geral.FDT(QrLocData.Value,1);
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[02].AsInteger := QrLocControle.Value;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[03].AsInteger := QrLocClienteI.Value;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[04].AsString  := QrLocLote.Value;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[05].AsString  := QrLocMarca.Value;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[06].AsString  := QrLocNOMECLIENTEI.Value;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[07].AsString  := QrLocNOMEPROCEDENCIA.Value;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[08].AsFloat   := QrLocPecas.Value;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[09].AsFloat   := QrLocPecasSal.Value;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[10].AsFloat   := 0;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[11].AsFloat   := 0;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.ExecSQL;
    //
    Application.MessageBox(PChar('Marca "'+QrLocMarca.Value+
    '" inserida com sucesso!'), 'Erro', MB_OK+MB_ICONINFORMATION);
    //
    FmPallets.ReopenLPalet;
  end else begin
    Application.MessageBox('Inclus�o cancelada. Marca j� foi adicionada!',
    'Erro', MB_OK+MB_ICONERROR);
  end;
  QrPalet.Close;
end;

procedure TFmPalletsOrigem.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPalletsOrigem.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPalletsOrigem.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmPalletsOrigem.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPalletsOrigem.EdMarcaExit(Sender: TObject);
begin
  ReabreLoc;
end;

procedure TFmPalletsOrigem.ReabreLoc;
var
  Lote, Marca: String;
begin
  Marca := Trim(EdMarca.Text);
  Lote  := Trim(EdLote.Text);
  //
  QrLoc.Close;
  if (Marca <> '') or (Lote <> '') then
  begin
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial');
    QrLoc.SQL.Add('ELSE ci.Nome END NOMECLIENTEI,');
    QrLoc.SQL.Add('CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial');
    QrLoc.SQL.Add('ELSE fo.Nome END NOMEPROCEDENCIA, wi.Controle,');
    QrLoc.SQL.Add('wi.Data, wi.ClienteI, wi.Procedencia, wi.Marca,');
    QrLoc.SQL.Add('wi.Pecas, wi.Lote, wi.PecasOut, wi.PecasSal');
    QrLoc.SQL.Add('FROM mpin wi');
    QrLoc.SQL.Add('LEFT JOIN entidades ci ON ci.Codigo=wi.ClienteI');
    QrLoc.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=wi.Procedencia');
    if Marca <> '' then
      QrLoc.SQL.Add('WHERE wi.Marca="'+Marca+'"')
    else if Lote <> '' then
      QrLoc.SQL.Add('WHERE wi.Lote="'+Lote+'"');
    UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
  end;
end;

procedure TFmPalletsOrigem.EdLoteExit(Sender: TObject);
begin
  ReabreLoc;
end;

end.

