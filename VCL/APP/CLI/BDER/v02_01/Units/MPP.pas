unit MPP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, Grids,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  DBGrids, Menus, frxClass, frxDBSet, Variants, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkGeral, UnDmkProcFunc, dmkImage, UnDmkEnums, UnAppEnums,
  dmkDBGridZTO;

type
  TFmMPP = class(TForm)
    PainelDados: TPanel;
    DsMPP: TDataSource;
    QrMPP: TmySQLQuery;
    PainelEdita: TPanel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DBEdit02: TDBEdit;
    Label6: TLabel;
    PainelIts: TPanel;
    PainelPanels: TPanel;
    QrMPs: TmySQLQuery;
    DsMPs: TDataSource;
    QrMPsCodigo: TIntegerField;
    QrMPsNome: TWideStringField;
    DBGrid4: TDBGrid;
    QrMPPIts: TmySQLQuery;
    DsMPPIts: TDataSource;
    QrMPPItsNOMEMP: TWideStringField;
    QrMPPItsCodigo: TIntegerField;
    QrMPPItsControle: TIntegerField;
    QrMPPItsMP: TIntegerField;
    QrMPPItsQtde: TFloatField;
    QrMPPItsPreco: TFloatField;
    QrMPPItsValor: TFloatField;
    QrMPPItsLk: TIntegerField;
    QrMPPItsDataCad: TDateField;
    QrMPPItsDataAlt: TDateField;
    QrMPPItsUserCad: TIntegerField;
    QrMPPItsUserAlt: TIntegerField;
    QrMPPItsTexto: TWideStringField;
    QrTotais: TmySQLQuery;
    QrTotaisQtde: TFloatField;
    QrTotaisValor: TFloatField;
    Label17: TLabel;
    DBEdit1: TDBEdit;
    Label18: TLabel;
    DBEdit2: TDBEdit;
    QrFornecedor: TmySQLQuery;
    QrFornecedorFAX_TXT: TWideStringField;
    QrFornecedorCEL_TXT: TWideStringField;
    QrFornecedorCPF_TXT: TWideStringField;
    QrFornecedorTEL_TXT: TWideStringField;
    QrFornecedorCEP_TXT: TWideStringField;
    QrFornecedorNOME: TWideStringField;
    QrFornecedorRUA: TWideStringField;
    QrFornecedorNUMERO: TLargeintField;
    QrFornecedorCOMPL: TWideStringField;
    QrFornecedorBAIRRO: TWideStringField;
    QrFornecedorCIDADE: TWideStringField;
    QrFornecedorPAIS: TWideStringField;
    QrFornecedorTELEFONE: TWideStringField;
    QrFornecedorFAX: TWideStringField;
    QrFornecedorCelular: TWideStringField;
    QrFornecedorCNPJ: TWideStringField;
    QrFornecedorIE: TWideStringField;
    QrFornecedorCEP: TLargeintField;
    QrFornecedorContato: TWideStringField;
    QrFornecedorUF: TLargeintField;
    QrFornecedorNOMEUF: TWideStringField;
    QrFornecedorEEMail: TWideStringField;
    QrFornecedorPEmail: TWideStringField;
    QrFornecedorNUMEROTXT: TWideStringField;
    QrFornecedorENDERECO: TWideStringField;
    QrFornecedorLOGRAD: TWideStringField;
    QrTransportador: TmySQLQuery;
    QrTransportadorTEL_TXT: TWideStringField;
    QrTransportadorCEL_TXT: TWideStringField;
    QrTransportadorCPF_TXT: TWideStringField;
    QrTransportadorFAX_TXT: TWideStringField;
    QrTransportadorCEP_TXT: TWideStringField;
    QrTransportadorNOME: TWideStringField;
    QrTransportadorRUA: TWideStringField;
    QrTransportadorNUMERO: TLargeintField;
    QrTransportadorCOMPL: TWideStringField;
    QrTransportadorBAIRRO: TWideStringField;
    QrTransportadorCIDADE: TWideStringField;
    QrTransportadorPAIS: TWideStringField;
    QrTransportadorTELEFONE: TWideStringField;
    QrTransportadorFAX: TWideStringField;
    QrTransportadorCelular: TWideStringField;
    QrTransportadorCNPJ: TWideStringField;
    QrTransportadorIE: TWideStringField;
    QrTransportadorCEP: TLargeintField;
    QrTransportadorContato: TWideStringField;
    QrTransportadorUF: TLargeintField;
    QrTransportadorNOMEUF: TWideStringField;
    QrTransportadorNUMEROTXT: TWideStringField;
    QrTransportadorENDERECO: TWideStringField;
    QrTransportadorEEMail: TWideStringField;
    QrTransportadorPEmail: TWideStringField;
    QrTransportadorLOGRAD: TWideStringField;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    QrMPPItsDesco: TFloatField;
    QrMPPItsSubTo: TFloatField;
    PMEdita: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    QrSumIts: TmySQLQuery;
    QrSumItsQtde: TFloatField;
    QrSumItsDesco: TFloatField;
    QrSumItsValor: TFloatField;
    QrSumItsPRECO: TFloatField;
    PMExclui: TPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    PMMercadoria: TPopupMenu;
    Incluinovamercadoria1: TMenuItem;
    Alteradadosdamercadoriaatual1: TMenuItem;
    N1: TMenuItem;
    Excluimercadoriaatual1: TMenuItem;
    QrMPPItsEntrega: TDateField;
    QrMPPItsPronto: TDateField;
    QrMPPItsPRONTO_TXT: TWideStringField;
    QrMPPItsStatus: TIntegerField;
    QrMPPItsFluxo: TIntegerField;
    QrMPPItsClasse: TWideStringField;
    QrDefPecas: TmySQLQuery;
    DsDefPecas: TDataSource;
    QrMPPItsM2Pedido: TFloatField;
    QrMPPItsPecas: TFloatField;
    QrMPPItsUnidade: TIntegerField;
    QrMPPItsObserv: TWideMemoField;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    QrDefPecasNivel: TIntegerField;
    QrDefPecasValor: TFloatField;
    QrDefPecasLk: TIntegerField;
    QrDefPecasDataCad: TDateField;
    QrDefPecasDataAlt: TDateField;
    QrDefPecasUserCad: TIntegerField;
    QrDefPecasUserAlt: TIntegerField;
    frxDsMPVIts: TfrxDBDataset;
    frxOS_1: TfrxReport;
    QrMPPItsNOMEUNIDADE: TWideStringField;
    QrMPPItsEspesTxt: TWideStringField;
    QrMPPItsCorTxt: TWideStringField;
    DBMemo2: TDBMemo;
    QrMPPItsPedido: TIntegerField;
    QrPesqV: TmySQLQuery;
    QrMPPNOMEVENDEDOR: TWideStringField;
    QrMPPNOMECLIENTE: TWideStringField;
    QrMPPCodigo: TIntegerField;
    QrMPPCliente: TIntegerField;
    QrMPPVendedor: TIntegerField;
    QrMPPDataF: TDateField;
    QrMPPQtde: TFloatField;
    QrMPPValor: TFloatField;
    QrMPPObz: TWideStringField;
    QrMPPLk: TIntegerField;
    QrMPPDataCad: TDateField;
    QrMPPDataAlt: TDateField;
    QrMPPUserCad: TIntegerField;
    QrMPPUserAlt: TIntegerField;
    QrMPPAlterWeb: TSmallintField;
    QrMPPItsAlterWeb: TSmallintField;
    QrMPPItsPrecoPed: TFloatField;
    QrMPPItsValorPed: TFloatField;
    frxDsMPP: TfrxDBDataset;
    QrMPPItsTipoProd: TSmallintField;
    QrMPPItsNOMETIPOPROD: TWideStringField;
    QrFluxos: TmySQLQuery;
    QrFluxosCodigo: TIntegerField;
    QrFluxosNome: TWideStringField;
    DsFluxos: TDataSource;
    QrFluxosIts: TmySQLQuery;
    QrFluxosItsCodigo: TIntegerField;
    QrFluxosItsControle: TIntegerField;
    QrFluxosItsOrdem: TIntegerField;
    QrFluxosItsOperacao: TIntegerField;
    QrFluxosItsAcao1: TWideStringField;
    QrFluxosItsAcao2: TWideStringField;
    QrFluxosItsAcao3: TWideStringField;
    QrFluxosItsAcao4: TWideStringField;
    QrFluxosItsLk: TIntegerField;
    QrFluxosItsDataCad: TDateField;
    QrFluxosItsDataAlt: TDateField;
    QrFluxosItsUserCad: TIntegerField;
    QrFluxosItsUserAlt: TIntegerField;
    QrFluxosItsNOMEOPERACAO: TWideStringField;
    QrFluxosItsSEQ: TIntegerField;
    DsFluxosIts: TDataSource;
    frxDsFluxosIts: TfrxDBDataset;
    QrMPPItsNOMEFLUXO: TWideStringField;
    QrMPPItsDescricao: TWideMemoField;
    QrMPPItsComplementacao: TWideMemoField;
    QrMPPItsAtivo: TSmallintField;
    frxOS_2: TfrxReport;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel8: TPanel;
    BtDesiste: TBitBtn;
    GBEdit: TGroupBox;
    PnEdit: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label5: TLabel;
    Label22: TLabel;
    Label38: TLabel;
    EdCodigo: TdmkEdit;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    TPDataF: TDateTimePicker;
    EdVendedor: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    MeObz: TMemo;
    GroupBox1: TGroupBox;
    BtConfirma2: TBitBtn;
    Panel4: TPanel;
    BtDesiste2: TBitBtn;
    GBItem: TGroupBox;
    PnItem_: TPanel;
    Panel2: TPanel;
    GroupBox3: TGroupBox;
    Label7: TLabel;
    Label13: TLabel;
    Label31: TLabel;
    Label33: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    Label37: TLabel;
    Label3: TLabel;
    EdMP: TdmkEditCB;
    CBMP: TdmkDBLookupComboBox;
    EdTexto: TdmkEdit;
    EdClasse: TdmkEdit;
    EdM2Pedido: TdmkEdit;
    TPEntrega: TDateTimePicker;
    EdEspesTxt: TdmkEdit;
    EdCorTxt: TdmkEdit;
    EdPrecoPed: TdmkEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel9: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtImprime: TBitBtn;
    BtExclui: TBitBtn;
    GBTrava: TGroupBox;
    BtTrava: TBitBtn;
    BtMercadoria: TBitBtn;
    GroupBox2: TGroupBox;
    DBMemo1: TDBMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrMPPItsCustoPQ: TFloatField;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrReceiRecu: TmySQLQuery;
    QrReceiRecuNumero: TIntegerField;
    QrReceiRecuNome: TWideStringField;
    DsReceiRecu: TDataSource;
    QrReceirefu: TmySQLQuery;
    QrReceirefuNumero: TIntegerField;
    QrReceirefuNome: TWideStringField;
    DsReceiRefu: TDataSource;
    QrReceiAcab: TmySQLQuery;
    QrReceiAcabNumero: TIntegerField;
    QrReceiAcabNome: TWideStringField;
    DsReceiAcab: TDataSource;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    MeObserv: TMemo;
    RGTipoProd: TRadioGroup;
    CkContinuar: TCheckBox;
    GroupBox5: TGroupBox;
    Label11: TLabel;
    EdVSArtCab: TdmkEditCB;
    CBVSArtCab: TdmkDBLookupComboBox;
    Label12: TLabel;
    EdReceiRecu: TdmkEditCB;
    CBReceiRecu: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdReceiAcab: TdmkEditCB;
    CBReceiAcab: TdmkDBLookupComboBox;
    Label8: TLabel;
    EdFluxo: TdmkEditCB;
    CBFluxo: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdReceiRefu: TdmkEditCB;
    CBReceiRefu: TdmkDBLookupComboBox;
    Label16: TLabel;
    EdTxtMPs: TdmkEdit;
    QrMPPItsGraGruX: TIntegerField;
    QrMPPItsReceiRecu: TIntegerField;
    QrMPPItsReceiRefu: TIntegerField;
    QrMPPItsReceiAcab: TIntegerField;
    QrMPPItsTxtMPs: TWideStringField;
    QrMPPItsNO_Fluxo: TWideStringField;
    QrMPPItsNO_ReceiRecu: TWideStringField;
    QrMPPItsNO_ReceiRefu: TWideStringField;
    QrMPPItsNO_ReceiAcab: TWideStringField;
    QrMPPItsNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGruX: TIntegerField;
    QrGraGruXART_NO_PRD_TAM_COR: TWideStringField;
    QrMPPItsCustoWB: TFloatField;
    PMLocNumero: TPopupMenu;
    Pedido1: TMenuItem;
    OSitemdepedido1: TMenuItem;
    QrWBMovIts: TmySQLQuery;
    QrWBMovItsCodigo: TIntegerField;
    QrWBMovItsControle: TIntegerField;
    QrWBMovItsMovimCod: TIntegerField;
    QrWBMovItsEmpresa: TIntegerField;
    QrWBMovItsMovimID: TIntegerField;
    QrWBMovItsGraGruX: TIntegerField;
    QrWBMovItsPecas: TFloatField;
    QrWBMovItsPesoKg: TFloatField;
    QrWBMovItsAreaM2: TFloatField;
    QrWBMovItsAreaP2: TFloatField;
    QrWBMovItsLk: TIntegerField;
    QrWBMovItsDataCad: TDateField;
    QrWBMovItsDataAlt: TDateField;
    QrWBMovItsUserCad: TIntegerField;
    QrWBMovItsUserAlt: TIntegerField;
    QrWBMovItsAlterWeb: TSmallintField;
    QrWBMovItsAtivo: TSmallintField;
    QrWBMovItsNO_PRD_TAM_COR: TWideStringField;
    QrWBMovItsSrcMovID: TIntegerField;
    QrWBMovItsSrcNivel1: TIntegerField;
    QrWBMovItsSrcNivel2: TIntegerField;
    QrWBMovItsPallet: TIntegerField;
    QrWBMovItsNO_PALLET: TWideStringField;
    QrWBMovItsSdoVrtArM2: TFloatField;
    QrWBMovItsSdoVrtPeca: TFloatField;
    QrWBMovItsObserv: TWideStringField;
    QrWBMovItsValorT: TFloatField;
    DsWBMovIts: TDataSource;
    DGDados: TDBGrid;
    QrWBMovItsLnkNivXtr1: TIntegerField;
    QrWBMovItsLnkNivXtr2: TIntegerField;
    Panel6: TPanel;
    frxReport1: TfrxReport;
    QrEmitCus: TmySQLQuery;
    QrEmitCusCodigo: TIntegerField;
    QrEmitCusDataEmis: TDateTimeField;
    QrEmitCusNumero: TIntegerField;
    QrEmitCusSetor: TSmallintField;
    QrEmitCusNOMESETOR: TWideStringField;
    QrEmitCusNOME: TWideStringField;
    QrEmitCusControle: TIntegerField;
    QrEmitCusCusto: TFloatField;
    QrEmitCusPecas: TFloatField;
    QrEmitCusPeso: TFloatField;
    QrEmitCusAreaM2: TFloatField;
    QrEmitCusPercTotCus: TFloatField;
    Panel7: TPanel;
    DBGrid1: TDBGrid;
    DsEmitCus: TDataSource;
    frxOS_3: TfrxReport;
    QrMPPAtivo: TSmallintField;
    QrMPPTransp: TIntegerField;
    QrMPPCondicaoPg: TIntegerField;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    DsPediPrzCab: TDataSource;
    Label19: TLabel;
    EdTransp: TdmkEditCB;
    CBTransp: TdmkDBLookupComboBox;
    QrTransp: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsTransp: TDataSource;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    LaCondicaoPG: TLabel;
    QrMPPNOMETRANSP: TWideStringField;
    QrMPPNO_CONDICAOPG: TWideStringField;
    DBEdit3: TDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit4: TDBEdit;
    EdPedidCli: TdmkEdit;
    Label23: TLabel;
    QrMPPPedidCli: TWideStringField;
    Label24: TLabel;
    DBEdit5: TDBEdit;
    TPDtaCrust: TDateTimePicker;
    Label25: TLabel;
    QrMPPItsDtaCrust: TDateField;
    QrGraGruXVSArtGGX: TIntegerField;
    QrVSArtCab: TmySQLQuery;
    DsVSArtCab: TDataSource;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label26: TLabel;
    QrMPPItsVSArtCab: TIntegerField;
    QrMPPItsVSArtGGX: TIntegerField;
    QrMPPItsVSArtCab_TXT: TWideStringField;
    LaFluxPcpCab: TLabel;
    EdFluxPcpCab: TdmkEditCB;
    CBFluxPcpCab: TdmkDBLookupComboBox;
    QrFluxPcpCab: TMySQLQuery;
    DsFluxPcpCab: TDataSource;
    QrFluxPcpCabCodigo: TIntegerField;
    QrFluxPcpCabNome: TWideStringField;
    QrFluxPcpIts: TMySQLQuery;
    QrFluxPcpItsDiaSeq: TIntegerField;
    QrFluxPcpItsIniStg: TIntegerField;
    QrFluxPcpItsFimStg: TIntegerField;
    QrMPPItsFluxPcpCab: TIntegerField;
    QrFeriado: TMySQLQuery;
    SbAlteraFluxPcpCab: TSpeedButton;
    LaLastFluxPcpCab: TLabel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrMPVPss: TMySQLQuery;
    DsMPVPss: TDataSource;
    QrMPVPssNO_IniStg: TWideStringField;
    QrMPVPssNO_FimStg: TWideStringField;
    QrMPVPssMPVIts: TIntegerField;
    QrMPVPssDiaSeq: TIntegerField;
    QrMPVPssConta: TIntegerField;
    QrMPVPssDataSeq: TDateField;
    QrMPVPssIniStg: TIntegerField;
    QrMPVPssFimStg: TIntegerField;
    QrMPVPssDtHrReal: TDateTimeField;
    SpeedButton5: TSpeedButton;
    QrVSArtCabCodigo: TIntegerField;
    QrVSArtCabNome: TWideStringField;
    QrVSArtCabReceiRecu: TIntegerField;
    QrVSArtCabReceiRefu: TIntegerField;
    QrVSArtCabReceiAcab: TIntegerField;
    QrVSArtCabTxtMPs: TWideStringField;
    QrVSArtCabObserva: TWideMemoField;
    QrVSArtCabLk: TIntegerField;
    QrVSArtCabDataCad: TDateField;
    QrVSArtCabDataAlt: TDateField;
    QrVSArtCabUserCad: TIntegerField;
    QrVSArtCabUserAlt: TIntegerField;
    QrVSArtCabAlterWeb: TSmallintField;
    QrVSArtCabAtivo: TSmallintField;
    QrVSArtCabAWServerID: TIntegerField;
    QrVSArtCabAWStatSinc: TSmallintField;
    QrVSArtCabFluxProCab: TIntegerField;
    QrVSArtCabFluxPcpCab: TIntegerField;
    QrVSArtCabLinCulReb: TIntegerField;
    QrVSArtCabLinCabReb: TIntegerField;
    QrVSArtCabLinCulSem: TIntegerField;
    QrVSArtCabLinCabSem: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMPPAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrMPPAfterScroll(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure QrMPPBeforeOpen(DataSet: TDataSet);
    procedure BtDesiste2Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure BtTravaClick(Sender: TObject);
    procedure BtMercadoriaClick(Sender: TObject);
    procedure EdMPChange(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrMPPItsCalcFields(DataSet: TDataSet);
    procedure QrFornecedorCalcFields(DataSet: TDataSet);
    procedure QrTransportadorCalcFields(DataSet: TDataSet);
    procedure MenuItem3Click(Sender: TObject);
    procedure Incluinovamercadoria1Click(Sender: TObject);
    procedure Alteradadosdamercadoriaatual1Click(Sender: TObject);
    procedure Excluimercadoriaatual1Click(Sender: TObject);
    procedure PMMercadoriaPopup(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrMPPItsAfterScroll(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure Pedido1Click(Sender: TObject);
    procedure OSitemdepedido1Click(Sender: TObject);
    procedure QrMPPItsBeforeClose(DataSet: TDataSet);
    procedure frxOS_2GetValue(const VarName: string; var Value: Variant);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QrVSArtCabBeforeClose(DataSet: TDataSet);
    procedure QrVSArtCabAfterScroll(DataSet: TDataSet);
    procedure EdVSArtCabChange(Sender: TObject);
    procedure SbAlteraFluxPcpCabClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    FEditItem, FMostraMoney: Boolean;
    procedure CriaOForm;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ImprimeOS_3();
    procedure ReopenFluxosIts();
    procedure ReopenWBMovIts();
    procedure ReopenEmitCus();
    procedure ReopenGraGruX();
    procedure ReopenMPVPss(Conta: Integer);
    procedure IncluiAlteraItensFluxPcp(MPVIts, FluxPcpCab: Integer; Entrega:
              TDateTime);
  public
    { Public declarations }
    FMPPLoc: Integer;
    FDireto: Boolean;
    procedure ReopenMPPIts(Controle: Integer);
    procedure AtualizaTotais;
    procedure ExcluiMercadoria;
  end;

var
  FmMPP: TFmMPP;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DotPrint, UnInternalConsts3, ModuleGeral, DmkDAC_PF,
  UnApp_Jan, MyDBCheck,
  PQx, ModEmit, Principal;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMPP.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMPP.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMPPCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMPP.DBGrid1DblClick(Sender: TObject);
begin
  if QrEmitCus.State <> dsInactive then
  begin
    if QrEmitCus.RecordCount > 0 then
    begin
      UnPQx.GerenciaPesagem(QrEmitCusCodigo.Value);
    end;
(*
    case QrEmitCusSetor.Value of
      -4: ;
      -1:
      else Geral.MB_Aviso('Abertura de pesagem: Setor n�o implementado: ' +
        Geral.FF0(QrEmitCusSetor.Value));
    end;
*)
  end;
end;

procedure TFmMPP.DefParams;
begin
  VAR_GOTOTABELA := 'MPP';
  VAR_GOTOMYSQLTABLE := QrMPP;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;
  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT');
  VAR_SQLx.Add('CASE WHEN ve.Tipo=0 THEN ve.RazaoSocial');
  VAR_SQLx.Add('ELSE ve.Nome END NOMEVENDEDOR,');
  VAR_SQLx.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  VAR_SQLx.Add('ELSE cl.Nome END NOMECLIENTE,');
  VAR_SQLx.Add('CASE WHEN tr.Tipo=0 THEN tr.RazaoSocial');
  VAR_SQLx.Add('ELSE tr.Nome END NOMETRANSP,');
  VAR_SQLx.Add('pc.Nome NO_CONDICAOPG,');
  VAR_SQLx.Add('MPP.*');
  VAR_SQLx.Add('FROM mpp MPP');
  VAR_SQLx.Add('LEFT JOIN entidades cl ON cl.Codigo=MPP.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades ve ON ve.Codigo=MPP.Vendedor');
  VAR_SQLx.Add('LEFT JOIN entidades tr ON tr.Codigo=MPP.Transp');
  VAR_SQLx.Add('LEFT JOIN pediprzcab pc ON pc.Codigo=mpp.CondicaoPg');
  VAR_SQLx.Add('WHERE MPP.Codigo > 0');
(*
  VAR_SQLx.Add('SELECT');
  VAR_SQLx.Add('CASE WHEN ve.Tipo=0 THEN ve.RazaoSocial');
  VAR_SQLx.Add('ELSE ve.Nome END NOMEVENDEDOR,');
  VAR_SQLx.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  VAR_SQLx.Add('ELSE cl.Nome END NOMECLIENTE,');
  VAR_SQLx.Add('MPP.*');
  VAR_SQLx.Add('FROM mpp MPP');
  VAR_SQLx.Add('LEFT JOIN entidades cl ON cl.Codigo=MPP.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades ve ON ve.Codigo=MPP.Vendedor');
  VAR_SQLx.Add('WHERE MPP.Codigo > 0');
*)
  //
  VAR_SQL1.Add('AND MPP.Codigo=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmMPP.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    GBCntrl.Visible     := False;
    if SQLType = stIns then
    begin
      EdCodigo.Text             := FormatFloat(FFormatFloat, Codigo);
      EdCliente.ValueVariant    := 0;
      CBCliente.KeyValue        := Null;
      EdVendedor.ValueVariant   := 0;
      CBVendedor.KeyValue       := Null;
      MeObz.Lines.Clear;
      EdTransp.ValueVariant     := 0;
      CBTransp.KeyValue         := Null;
      EdCondicaoPG.ValueVariant := 0;
      CBCondicaoPG.KeyValue     := Null;
      EdPedidCli.Text           := '';
    end else begin
      EdCodigo.Text             := DBEdCodigo.Text;
      EdCliente.Text            := IntToStr(QrMPPCliente.Value);
      CBCliente.KeyValue        := QrMPPCliente.Value;
      TPDataF.Date              := QrMPPDataF.Value;
      EdVendedor.Text           := IntToStr(QrMPPVendedor.Value);
      CBVendedor.KeyValue       := QrMPPVendedor.Value;
      MeObz.Text                := QrMPPObz.Value;
      EdTransp.ValueVariant     := QrMPPTransp.Value;
      CBTransp.KeyValue         := QrMPPTransp.Value;
      EdCondicaoPG.ValueVariant := QrMPPCondicaoPg.Value;
      CBCondicaoPG.KeyValue     := QrMPPCondicaoPg.Value;
      EdPedidCli.Text           := QrMPPPedidCli.Value;
    end;
    EdCliente.SetFocus;
  end else begin
    GBCntrl.Visible     := True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmMPP.OSitemdepedido1Click(Sender: TObject);
var
  Num: String;
  Codigo, Controle: Integer;
  Continua, Passa: Boolean;
  Qry: TmySQLQuery;
begin
  Passa := False;
  Num := InputBox(VAR_GOTOTABELA, 'Digite o n�mero da OS.', '' );
  try
    Controle := Geral.IMV(Num);
    Qry := TmySQLQuery.Create(Self);
    try
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Pedido ',
      'FROM mpvits ',
      'WHERE Controle=' + Geral.FF0(Controle),
      '']);
      Codigo := Qry.FieldByName('Pedido').AsInteger;
      //
      LocCod(Codigo, Codigo);
      QrMPPIts.Locate('Controle', Controle, []);
    finally
      Qry.Free;
    end;
  except
    on EConvertError do
      if Num <> '' then Geral.MB_Erro('"' + Num + SMLA_NUMEROINVALIDO);
  end;
end;

procedure TFmMPP.CriaOForm;
begin
  DefParams;
  Va(vpLast);
end;

procedure TFmMPP.AlteraRegistro;
var
  MPP : Integer;
begin
  MPP := QrMPPCodigo.Value;
  if QrMPPCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(MPP, Dmod.MyDB, 'MPP', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(MPP, Dmod.MyDB, 'MPP', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmMPP.IncluiRegistro;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MostraEdicao(True, stIns, 0);
  finally
    Screen.Cursor := Cursor;
  end;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMPP.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMPP.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMPP.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMPP.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMPP.SpeedButton5Click(Sender: TObject);
begin
  App_Jan.MostraFormProgramacaoEntregas();
end;

procedure TFmMPP.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
  AtualizaTotais;
end;

procedure TFmMPP.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
  AtualizaTotais;
end;

procedure TFmMPP.BtSaidaClick(Sender: TObject);
begin
  //VAR_MPP := QrMPPCodigo.Value;
  Close;
end;

procedure TFmMPP.BtConfirmaClick(Sender: TObject);
var
  Codigo, Cliente, Vended, Transp, CondicaoPg: Integer;
  DataF, Obz, PedidCli: String;
begin
  Cliente := EdCliente.ValueVariant;
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Defina um cliente.') then
    Exit;
  Vended := EdVendedor.ValueVariant;
  DataF  := Geral.FDT(TPDataF.Date, 1);
  Obz    := MeObz.Text;
  Transp := EdTransp.ValueVariant;
  CondicaoPg := EdCondicaoPG.ValueVariant;
  PedidCli := EdPedidCli.ValueVariant;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('MPP', 'Codigo', ImgTipo.SQLType, QrMPPCodigo.Value);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'MPP', False,
  ['Cliente', 'DataF', 'Vendedor',
   'Obz', 'Transp', 'CondicaoPg',
   'PedidCli'
  ], ['Codigo'],
  [
  Cliente, DataF, Vended,
  Obz, Transp, CondicaoPg,
  PedidCli
  ], [Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'MPP', 'Codigo');
    MostraEdicao(False, ImgTipo.SQLType, 0);
    LocCod(Codigo,Codigo);
    //
    GBTrava.Visible := True;
    GBCntrl.Visible := False;
    if FDireto then
    begin
      if ImgTipo.SQLType = stIns then
        Incluinovamercadoria1Click(Self)
      else
      begin
        BtTravaClick(Self);
        BtSaidaClick(Self);
      end;
    end;
  end;
end;

procedure TFmMPP.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'MPP', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'MPP', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'MPP', 'Codigo');
  if FDireto then
  begin
    //Geral.MB_(PChar('A inclus�o direta foi desativada para '+
    //'permitir altera��es!'), 'Aviso', MB_OK+MB_ICONWARNING);
    FDireto := False;
  end;
end;

procedure TFmMPP.FormCreate(Sender: TObject);
const
  Aplicacao = '1';
begin
  ImgTipo.SQLType := stLok;
  //
  DBGrid4.Align := alClient;
  PainelPanels.Height := 64;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  GBEdit.Align  := alClient;
  GBItem.Align      := alClient;
  CriaOForm;
  CBGraGruX.LocF7SQLMasc := CO_JOKE_SQL;
  CBGraGruX.LocF7SQLText.Text := Geral.ATS([
  'SELECT ggx.Controle _Codigo, ',
  'CONCAT(vac.Nome, " > ", gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) _Nome ',
  'FROM vsartggx vag',
  'LEFT JOIN vsartcab   vac ON vag.Codigo=vac.Codigo',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vag.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) LIKE "%' + CO_JOKE_SQL + '%" ',
  'ORDER BY _Nome ',
  '']);
  UnDmkDAC_PF.AbreQuery(QrReceiRecu, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReceiRefu, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReceiAcab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMPs, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFluxos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSArtCab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFluxPcpCab, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreQuery(QrTransp, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome, MaxDesco, ',
  'JurosMes, Parcelas, Percent1, Percent2 ',
  'FROM pediprzcab ',
  'WHERE Aplicacao & ' + Aplicacao + ' <> 0 ',
  'ORDER BY Nome ',
  '']);
  //
  TPDataF.Date    := Date;
  TPEntrega.Date  := Date;
  TPDtaCrust.Date := Date;
end;

procedure TFmMPP.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMLocNumero, SbNumero);
end;

procedure TFmMPP.SbQueryClick(Sender: TObject);
begin
//
end;

procedure TFmMPP.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMPP.SbNovoClick(Sender: TObject);
var
  Controle, Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Codigo := QrMPPCodigo.Value;
    Controle := QrMPPItsControle.Value;
    DmModEmit.AtualizaMPVIts(Controle);
    LocCod(Codigo, Codigo);
    QrMPPIts.Locate('Controle', Controle, []);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMPP.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Geral.WriteAppKey('Componentes\MPP\AlturaGrade', Application.Title,
  DBGrid4.Height, ktInteger, HKEY_LOCAL_MACHINE);
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMPP.QrMPPAfterOpen(DataSet: TDataSet);
begin
  ReopenMPPIts(0);
end;

procedure TFmMPP.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if FDireto then IncluiRegistro;
end;

procedure TFmMPP.QrMPPAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrMPPCodigo.Value, False);
  ReopenMPPIts(0);
end;

procedure TFmMPP.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPP.frxOS_2GetValue(const VarName: string; var Value: Variant);
begin
  if VarName = 'VAR_OSImpInfWB' then
    Value := Dmod.QrControleOSImpInfWB.Value = 1;
end;

procedure TFmMPP.QrMPPBeforeOpen(DataSet: TDataSet);
begin
  QrMPPCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmMPP.BtDesiste2Click(Sender: TObject);
begin
  PainelDados.Visible := True;
  PainelIts.Visible := False;
  AtualizaTotais;
  LocCod(QrMPPCodigo.Value, QrMPPCodigo.Value);
  if FDireto then
  begin
    FmostraMoney := False;
  end;
end;

procedure TFmMPP.BtConfirma2Click(Sender: TObject);
var
  MP, Controle, TipoProd, GraGruX, ReceiRecu, ReceiRefu, ReceiAcab, Fluxo,
  VSArtGGX, VSArtCab, FluxPcpCab: Integer;
  PrecoPed, M2Pedido, ValorPed, M2Dia: Double;
  Texto, Classe, Observ, EspesTxt, CorTxt, Entrega, DtaCrust, TxtMPs: String;
  SQLType: TSQLType;
  dEntrega: TDateTime;
begin
  SQLType := ImgTipo.SQLType;
  MP := Geral.IMV(EdMP.Text);
  //
  if MyObjects.FIC(MP = 0, EdMP, 'Defina uma mercadoria!') then Exit;
  //
  dEntrega  := Trunc(TPEntrega.Date);
  Entrega   := Geral.FDT(dEntrega, 1);
  DtaCrust  := Geral.FDT(TPDtaCrust.Date, 1);
  M2Pedido  := Geral.DMV(EdM2Pedido.Text);
  PrecoPed  := Geral.DMV(EdPrecoPed.Text);
  ValorPed  := M2Pedido * PrecoPed;
  Texto     := EdTexto.Text;
  Classe    := EdClasse.Text;
  Observ    := MeObserv.Text;
  EspesTxt  := EdEspesTxt.Text;
  CorTxt    := EdCorTxt.Text;
  TipoProd  := RGTipoProd.ItemIndex;
  GraGruX   := EdGraGruX.ValueVariant;
  ReceiRecu := EdReceiRecu.ValueVariant;
  ReceiRefu := EdReceiRefu.ValueVariant;
  ReceiAcab := EdReceiAcab.ValueVariant;
  Fluxo     := EdFluxo.ValueVariant;
  TxtMPs    := EdTxtMPs.Text;
  VSArtCab  := EdVSArtCab.ValueVariant;
  FluxPcpCab := EdFluxPcpCab.ValueVariant;
  if GraGruX = 0 then
    VSArtGGX := 0
  else
    VSArtGGX := QrGraGruXVSArtGGX.Value;
  //////////////////// M2 m�ximo por dia de entrega ////////////////////////////
  // ini 2020-02-02
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Entrega, SUM(M2Pedido) M2Pedido ',
  'FROM mpvits ',
  'WHERE Entrega = "' + Entrega + '" ',
  EmptyStr]);
  M2Dia := Dmod.QrAux.FieldByName('M2Pedido').AsFloat;
  M2Dia := M2Dia + M2Pedido;
  if MyObjects.FIC(M2Dia > Dmod.QrControleM2DiaAcabPCP.Value, EdM2Pedido,
  'Produ��o do dia ' + Geral.FDT(dEntrega, 2) + ' (' + Geral.FFT(
  M2Dia, 2, siPositivo) + ' m�) extrapola o m�ximo de ' + Geral.FFT(
  Dmod.QrControleM2DiaAcabPCP.Value, 2, siPositivo) + '.') then
  begin
    if Geral.MB_Pergunta(
    'Para confirmar assim mesmo esta data deve ser informada a semha gerencial.' +
    sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
    begin
      if not DBCheck.LiberaPelaSenhaBoss() then
        Exit;
    end else
      Exit;
  end;
  // fim 2020-02-02
  ////////////////// FIM M2 m�ximo por dia de entrega //////////////////////////

  //
  Controle  := UMyMod.BuscaEmLivreY_Def('MPVIts', 'Controle', SQLType,
                 QrMPPItsControle.Value);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'MPVIts', False,
    ['MP', 'Texto', 'Entrega', 'DtaCrust', 'Classe', 'M2Pedido',
    'Observ', 'EspesTxt', 'CorTxt', 'PrecoPed', 'ValorPed',
    'TipoProd', 'Fluxo', 'GraGruX', 'ReceiRecu',
    'ReceiRefu', 'ReceiAcab', 'TxtMPs',
    'VSArtCab', 'VSArtGGX', 'FluxPcpCab'],
    ['Pedido', 'Controle'],
    [MP, Texto, Entrega, DtaCrust, Classe, M2Pedido,
    Observ, EspesTxt, CorTxt, PrecoPed, ValorPed,
    TipoProd, Fluxo, GraGruX, ReceiRecu,
    ReceiRefu, ReceiAcab, TxtMPs,
    VSArtCab, VSArtGGX, FluxPcpCab],
    [QrMPPCodigo.Value, Controle], True) then
  begin
    if (SQLType = stIns)
    or (
       (EdFluxPcpCab.Enabled = True)
       and
       (EdFluxPcpCab.ValueVariant <> Geral.IMV(LaLastFluxPcpCab.Caption))
    ) then
    begin
      // Inclui / altera itens do Fluxo PCP
      IncluiAlteraItensFluxPcp(Controle, FluxPcpCab, dEntrega);
    end;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      EdMP.Text       := '';
      CBMP.KeyValue   := NULL;
      EdTexto.Text    := '';
      MeObserv.Text   := '';
      EdM2Pedido.Text := '';
      EdCorTxt.Text   := '';
      //
      EdVSArtCab.ValueVariant   := 0;
      CBVSArtCab.KeyValue       := 0;
      EdGraGruX.ValueVariant    := 0;
      CBGraGruX.KeyValue        := 0;
      EdFluxo.ValueVariant      := 0;
      CBFluxo.KeyValue          := 0;
      EdReceiRecu.ValueVariant  := 0;
      CBReceiRecu.KeyValue      := 0;
      EdReceiRefu.ValueVariant  := 0;
      CBReceiRefu.KeyValue      := 0;
      EdReceiAcab.ValueVariant  := 0;
      CBReceiAcab.KeyValue      := 0;
      EdTxtMPs.Text             := '';
      EdFluxPcpCab.ValueVariant := 0;
      CBFluxPcpCab.KeyValue     := 0;
      //
      EdMP.SetFocus;
    end else
    begin
      BtDesiste2click(Self);
      ReopenMPPIts(Controle);
    end;
  end;
end;

procedure TFmMPP.BtTravaClick(Sender: TObject);
begin
  GBCntrl.Visible := True;
  GBTrava.Visible := False;
  ImgTipo.SQLType := stLok;
  // Trava
  AtualizaTotais;
  LocCod(QrMPPCodigo.Value, QrMPPCodigo.Value);
  //if FDireto then BtAlteraClick(Self);
end;

procedure TFmMPP.BtMercadoriaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMercadoria, BtMercadoria);
end;

procedure TFmMPP.EdMPChange(Sender: TObject);
begin
  EdTexto.Text := CBMP.Text;
end;

procedure TFmMPP.EdVSArtCabChange(Sender: TObject);
begin
  if EdVSArtCab.ValueVariant = 0 then
  begin
    EdGraGruX.ValueVariant    := 0;
    CBGraGruX.KeyValue        := 0;
    EdFluxo.ValueVariant      := 0;
    CBFluxo.KeyValue          := 0;
    EdReceiRecu.ValueVariant  := 0;
    CBReceiRecu.KeyValue      := 0;
    EdReceiRefu.ValueVariant  := 0;
    CBReceiRefu.KeyValue      := 0;
    EdReceiAcab.ValueVariant  := 0;
    CBReceiAcab.KeyValue      := 0;
    EdTxtMPs.Text             := '';
  end else
  begin
    // ini 2023-06-23
    //EdFluxo.ValueVariant      := QrVSArtCabFluxo.Value;
    //CBFluxo.KeyValue          := QrVSArtCabFluxo.Value;
    EdFluxo.ValueVariant      := QrVSArtCabFluxProCab.Value;
    CBFluxo.KeyValue          := QrVSArtCabFluxProCab.Value;
    // fim 2023-06-23
    EdReceiRecu.ValueVariant  := QrVSArtCabReceiRecu.Value;
    CBReceiRecu.KeyValue      := QrVSArtCabReceiRecu.Value;
    EdReceiRefu.ValueVariant  := QrVSArtCabReceiRefu.Value;
    CBReceiRefu.KeyValue      := QrVSArtCabReceiRefu.Value;
    EdReceiAcab.ValueVariant  := QrVSArtCabReceiAcab.Value;
    CBReceiAcab.KeyValue      := QrVSArtCabReceiAcab.Value;
    EdTxtMPs.Text             := QrVSArtCabTxtMPs.Value;
  end;
end;

procedure TFmMPP.ReopenEmitCus();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitCus, Dmod.MyDB, [
  'SELECT emt.Codigo, emt.DataEmis, emt.Numero, emt.Setor, ',
  'emt.NOMESETOR, emt.NOME, cus.Controle, cus.Custo, ',
  'cus.Pecas, cus.Peso, cus.AreaM2, cus.PercTotCus ',
  'FROM emitcus cus ',
  'LEFT JOIN emit emt ON emt.Codigo=cus.Codigo ',
  'WHERE cus.MPVIts=' + Geral.FF0(QrMPPItsControle.Value),
  '']);
end;

procedure TFmMPP.ReopenFluxosIts();
begin
  QrFluxosIts.Close;
  QrFluxosIts.Params[0].AsInteger := QrMPPItsFluxo.Value;
  UnDmkDAC_PF.AbreQuery(QrFluxosIts, Dmod.MyDB);
end;

procedure TFmMPP.ReopenGraGruX();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT vag.Controle VSArtGGX, ggx.Controle GraGruX, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'ART_NO_PRD_TAM_COR ',
  (*, vac.Fluxo, vac.ReceiRecu, ',
  'vac.ReceiRefu, vac.ReceiAcab, vac.TxtMPs, ',
  'vac.Observa ',*)
  'FROM vsartggx vag ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vag.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE vag.Codigo=' + Geral.FF0(QrVSArtCabCodigo.Value),
  'ORDER BY ART_NO_PRD_TAM_COR, GraGruX ',
  '']);
end;

procedure TFmMPP.ReopenMPPIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMPPIts, Dmod.MyDB, [
    'SELECT pec.Nome NOMEUNIDADE,  ',
    'ag.Nome NOMEMP, flu.Nome NOMEFLUXO, mvi.*, ',
    'flu.Nome NO_Fluxo, fo1.Nome NO_ReceiRecu, ',
    'fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab,  ',
    'CONCAT(gg1.Nome, ',
    '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, vsa.Nome VSArtCab_TXT ',
    'FROM mpvits mvi ',
    'LEFT JOIN defpecas pec ON pec.Codigo=mvi.Unidade ',
    'LEFT JOIN artigosgrupos ag ON ag.Codigo=mvi.MP ',
    'LEFT JOIN fluxos flu ON flu.Codigo=mvi.Fluxo ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=mvi.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    'LEFT JOIN formulas fo1 ON fo1.Numero=mvi.ReceiRecu ',
    'LEFT JOIN formulas fo2 ON fo2.Numero=mvi.ReceiRefu ',
    'LEFT JOIN tintascab ti1 ON ti1.Numero=mvi.ReceiAcab ',
    'LEFT JOIN vsartcab vsa ON vsa.Codigo=mvi.VSArtCab ',
    'WHERE mvi.Pedido<>0 ',
    'AND mvi.Pedido=' + Geral.FF0(QrMPPCodigo.Value),
    '']);
  //
  if Controle > 0 then
    QrMPPIts.Locate('Controle', Controle, []);
end;

procedure TFmMPP.ReopenMPVPss(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMPVPss, Dmod.MyDB, [
  'SELECT fpi.Nome NO_IniStg, fpf.Nome NO_FimStg, mps.* ',
  'FROM mpvpss mps',
  'LEFT JOIN fluxpcpstg fpi ON fpi.Codigo=mps.IniStg',
  'LEFT JOIN fluxpcpstg fpf ON fpf.Codigo=mps.FimStg',
  'WHERE mps.MPVIts=' + Geral.FF0(QrMPPItsControle.Value),
  EmptyStr]);
  if Conta > 0 then
    QrMPVPss.Locate('Conta', Conta, []);
end;

procedure TFmMPP.ReopenWBMovIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBMovIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'WHERE wmi.MovimID=' + Geral.FF0(Integer(emidIndsWE)),
  'AND wmi.Codigo=' + Geral.FF0(QrMPPItsControle.Value),
  '']);
end;

procedure TFmMPP.AtualizaTotais;
begin
  QrTotais.Close;
  QrTotais.Params[0].AsInteger := QrMPPCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrTotais, Dmod.MyDB);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE mpp SET Qtde=:P0, Valor=:P1 ');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  Dmod.QrUpd.Params[0].AsFloat   := QrTotaisQtde.Value;
  Dmod.QrUpd.Params[1].AsFloat   := QrTotaisValor.Value;
  //
  Dmod.QrUpd.Params[2].AsInteger := QrMPPCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmMPP.SbAlteraFluxPcpCabClick(Sender: TObject);
begin
  LaLastFluxPcpCab.Caption   := Geral.FF0(EdFluxPcpCab.ValueVariant);
  LaFluxPcpCab.Enabled       := True;
  EdFluxPcpCab.Enabled       := True;
  CBFluxPcpCab.Enabled       := True;
  SbAlteraFluxPcpCab.Enabled := False;
end;

procedure TFmMPP.SbImprimeClick(Sender: TObject);
begin
  ImprimeOS_3;
end;

procedure TFmMPP.QrMPPItsAfterScroll(DataSet: TDataSet);
begin
  ReopenFluxosIts();
  ReopenWBMovIts();
  ReopenEmitCus();
  ReopenMPVPss(0);
end;

procedure TFmMPP.QrMPPItsBeforeClose(DataSet: TDataSet);
begin
  QrWBMovIts.Close;
  QrEmitCus.Close;
  QrMPVPss.Close;
end;

procedure TFmMPP.QrMPPItsCalcFields(DataSet: TDataSet);
begin
  QrMPPItsSubTo.Value := QrMPPItsValor.Value+QrMPPItsDesco.Value;
  if QrMPPItsPronto.Value < 2 then QrMPPItsPRONTO_TXT.Value := ' * N�O * '
  else QrMPPItsPRONTO_TXT.Value := Geral.FDT(QrMPPItsPronto.Value, 2);
  //
  if QrMPPItsTipoProd.Value = 0 then
    QrMPPItsNOMETIPOPROD.Value := 'VENDA'
  else
    QrMPPItsNOMETIPOPROD.Value := 'M�O DE OBRA';
end;

procedure TFmMPP.QrFornecedorCalcFields(DataSet: TDataSet);
var
  Endereco: String;
begin
  QrFornecedorTEL_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorTelefone.Value);
  QrFornecedorFAX_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorFax.Value);
  QrFornecedorCEL_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorCelular.Value);
  QrFornecedorCPF_TXT.Value := Geral.FormataCNPJ_TT(QrFornecedorCNPJ.Value);
  QrFornecedorCEP_TXT.Value := Geral.FormataCEP_TT(FloatToStr(QrFornecedorCEP.Value));
  //
  Endereco := QrFornecedorRUA.Value;
  if Trim(QrFornecedorLOGRAD.Value) <> '' then
  begin
    if Uppercase(Copy(QrFornecedorRUA.Value, 1, Length(QrFornecedorLOGRAD.Value))) <>
      Uppercase(QrFornecedorLOGRAD.Value) then
    Endereco := QrFornecedorLOGRAD.Value + ' ' + Endereco;
  end;
  //
  if QrFornecedorNUMERO.Value = 0 then
    QrFornecedorNUMEROTXT.Value := 'S/N' else
    QrFornecedorNUMEROTXT.Value :=
    FloatToStr(QrFornecedorNUMERO.Value);
  QrFornecedorENDERECO.Value :=
    Endereco + ', '+
    QrFornecedorNUMEROTXT.Value + '  '+
    QrFornecedorCOMPL.Value;
end;

procedure TFmMPP.QrTransportadorCalcFields(DataSet: TDataSet);
var
  Endereco: String;
begin
  QrTransportadorTEL_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorTelefone.Value);
  QrTransportadorFAX_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorFax.Value);
  QrTransportadorCEL_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorCelular.Value);
  QrTransportadorCPF_TXT.Value := Geral.FormataCNPJ_TT(QrTransportadorCNPJ.Value);
  QrTransportadorCEP_TXT.Value := Geral.FormataCEP_TT(FloatToStr(QrTransportadorCEP.Value));
  //
  Endereco := QrTransportadorRUA.Value;
  if Trim(QrTransportadorLOGRAD.Value) <> '' then
  begin
    if Uppercase(Copy(QrTransportadorRUA.Value, 1, Length(QrTransportadorLOGRAD.Value))) <>
      Uppercase(QrTransportadorLOGRAD.Value) then
    Endereco := QrTransportadorLOGRAD.Value + ' ' + Endereco;
  end;
  //
  if QrTransportadorNUMERO.Value = 0 then
    QrTransportadorNUMEROTXT.Value := 'S/N' else
    QrTransportadorNUMEROTXT.Value :=
    FloatToStr(QrTransportadorNUMERO.Value);
  QrTransportadorENDERECO.Value :=
    Endereco + ', '+
    QrTransportadorNUMEROTXT.Value + '  '+
    QrTransportadorCOMPL.Value;
end;

procedure TFmMPP.QrVSArtCabAfterScroll(DataSet: TDataSet);
begin
  ReopenGraGruX();
end;

procedure TFmMPP.QrVSArtCabBeforeClose(DataSet: TDataSet);
begin
  QrGraGruX.Close;
end;

procedure TFmMPP.MenuItem3Click(Sender: TObject);
begin
  ExcluiMercadoria;
end;

procedure TFmMPP.ExcluiMercadoria;
var
  Pedido, Controle, Venda: Integer;
begin
  Controle := QrMPPItsControle.Value;
  Venda    := QrMPPItsCodigo.Value;
  if Venda = 0 then
  begin
    Pedido   := QrMPPItsPedido.Value;
    if Geral.MB_Pergunta(FIN_MSG_ASKESCLUI) = ID_YES then
    begin
      //if
      UMyMod.ExcluiRegistroInt1('', 'mpvpss', 'MPVIts', Controle, Dmod.MyDB); // = ID_YES then
      begin
        Dmod.QrUpd.Close;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM mpvits WHERE Pedido=:P0');
        Dmod.QrUpd.SQL.Add('AND Controle=:P2 AND Codigo=0'); // Codigo=0 > n�o tem venda confirmada
        Dmod.QrUpd.Params[0].AsInteger := Pedido;
        Dmod.QrUpd.Params[1].AsInteger := Controle;
        Dmod.QrUpd.ExecSQL;
        //
        AtualizaTotais;
        LocCod(QrMPPCodigo.Value, QrMPPCodigo.Value);
        ReopenMPPIts(0);
      end;
    end;
  end else Geral.MB_Aviso('A ordem de produ��o n� ' +
  Geral.FF0(Controle) + ' n�o pode ser exclu�da porque pertence � venda n� ' +
  Geral.FF0(Venda) + '!');
end;

procedure TFmMPP.ImprimeOS_3;
begin
  MyObjects.frxMostra(frxOS_3, 'Ordem de produ��o n� ' + Geral.FF0(QrMPPItsControle.Value));
end;

procedure TFmMPP.IncluiAlteraItensFluxPcp(MPVIts, FluxPcpCab: Integer; Entrega:
  TDateTime);
var
  DataSeq, DtHrReal, sAnoMesDia, sDiaMes: String;
  //MPVIts,
  DiaSeq, Conta, IniStg, FimStg: Integer;
  //SQLType: TSQLType;
  ContasReuso: array of Integer;
  DatasDiasSeq: array of TDateTime;
  DataCurso: TDateTime;
  DiaSem: Word;
  DataUtil: Boolean;
begin
  if MPVIts = 0 then Exit;
  // Obter indices (conta) j� usados para reaproveitar e n�o desperdi�ar indices
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT DISTINCT Conta  ',
  'FROM mpvpss ',
  'WHERE MPVIts=' + Geral.FF0(MPVIts),
  'ORDER BY Conta ',
  EmptyStr]);
  //
  if FluxPcpCab = 0 then Exit;
  //
  SetLength(ContasReuso, Dmod.QrAux.RecordCount);
  Dmod.QrAux.First;
  while not Dmod.QrAux.Eof do
  begin
    ContasReuso[Dmod.QrAux.RecNo -1] := Dmod.QrAux.FieldByName('Conta').AsInteger;
    //
    Dmod.QrAux.Next;
  end;
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'DELETE FROM mpvpss ',
  'WHERE MPVIts=' + Geral.FF0(MPVIts),
  EmptyStr]);
  //
  //
  //
  //SQLType        := stIns;//ImgTipo.SQLType;
  //MPVIts         := ;
  UnDmkDAC_PF.AbreMySQLQuery0(QrFluxPcpIts, Dmod.MyDB, [
  'SELECT DiaSeq, IniStg, FimStg  ',
  'FROM fluxpcpits ',
  'WHERE Codigo=' + Geral.FF0(FluxPcpCab),
  'ORDER BY DiaSeq ',
  EmptyStr]);
  SetLength(DatasDiasSeq, QrFluxPcpIts.RecordCount);
  QrFluxPcpIts.Last;
  DataCurso := Entrega + 1;
  while not QrFluxPcpIts.Bof do
  begin
    DataCurso := DataCurso - 1;
    DataUtil := False;
    while DataUtil = False do
    begin
      DiaSem := DayOfWeek(DataCurso);
      if DiaSem = 1(*Domingo*) then
        DataCurso := DataCurso - 2
      else
      if DiaSem = 7(*Sabado*) then
        DataCurso := DataCurso - 1
      else
      begin
        sAnoMesDia := Geral.FDT(DataCurso, 1);  // yyyy-mm-dd
        sDiaMes    := Geral.FDT(DataCurso, 12); //  dd/mm
        UnDmkDAC_PF.AbreMySQLQuery0(QrFeriado, Dmod.MyDB, [
        'SELECT *',
        'FROM feriados',
        //'WHERE Data="2019-01-29"',
        'WHERE Data="' + sAnoMesDia + '"',
        //'OR DATE_FORMAT(Data, "%m-%d") = "01-29"',
        'OR (DATE_FORMAT(Data, "%d/%m") = "' + sDiaMes + '" AND Anual=1)',
        EmptyStr]);
        if QrFeriado.RecordCount > 0 then
          DataCurso := DataCurso - 1
        else
          DataUtil := True;
      end;
    end;
    //
    DatasDiasSeq[QrFluxPcpIts.RecNo - 1] := DataCurso;
    //
    QrFluxPcpIts.Prior;
  end;
  QrFluxPcpIts.First;
  while not QrFluxPcpIts.Eof do
  begin
    DiaSeq         := QrFluxPcpItsDiaSeq.Value;
    Conta          := 0;
    DataSeq        := Geral.FDT(DatasDiasSeq[QrFluxPcpIts.RecNo - 1], 1);
    IniStg         := QrFluxPcpItsIniStg.Value;
    FimStg         := QrFluxPcpItsFimStg.Value;
    DtHrReal       := '0000-00-00 00:00:00';
    //
    if QrFluxPcpIts.RecNo <= Length(ContasReuso) then
      Conta := ContasReuso[QrFluxPcpIts.RecNo - 1]
    else
      Conta := UMyMod.BPGS1I32('mpvpss', 'Conta', '', '', tsPos, stIns, Conta);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mpvpss', False, [
    'MPVIts', 'DiaSeq', 'DataSeq',
    'IniStg', 'FimStg', 'DtHrReal'], [
    'Conta'], [
    MPVIts, DiaSeq, DataSeq,
    IniStg, FimStg, DtHrReal], [
    Conta], True) then
    ;
    //
    QrFluxPcpIts.Next;
  end;
end;

procedure TFmMPP.Incluinovamercadoria1Click(Sender: TObject);
begin
  FEditItem                  := False;
  PainelIts.Visible          := True;
  PainelDados.Visible        := False;
  MeObserv.Text              := '';
  EdM2Pedido.Text            := '';
  EdPrecoPed.Text            := '';
  EdFluxPcpCab.ValueVariant  := 0;
  CBFluxPcpCab.KeyValue      := 0;
  LaFluxPcpCab.Enabled       := True;
  EdFluxPcpCab.Enabled       := True;
  CBFluxPcpCab.Enabled       := True;
  SbAlteraFluxPcpCab.Enabled := False;
  LaLastFluxPcpCab.Caption   := '0';
  (*LaTipoIts.Caption*)ImgTipo.SQLType   := stIns;
  EdMP.SetFocus;
end;

procedure TFmMPP.Alteradadosdamercadoriaatual1Click(Sender: TObject);
begin
  FEditItem := True;
  PainelIts.Visible   := True;
  PainelDados.Visible := False;
  EdMP.SetFocus;
  //
  EdMP.Text               := Geral.FF0(QrMPPItsMP.Value);
  CBMP.KeyValue           := QrMPPItsMP.Value;
  EdTexto.Text            := QrMPPItsTexto.Value;
  EdClasse.Text           := QrMPPItsClasse.Value;
  TPEntrega.Date          := QrMPPItsEntrega.Value;
  TPDtaCrust.Date         := QrMPPItsDtaCrust.Value;
  MeObserv.Text           := QrMPPItsObserv.Value;
  EdM2Pedido.Text         := Geral.FFT(QrMPPItsM2Pedido.Value, 2, siNegativo);
  EdPrecoPed.Text         := Geral.FFT(QrMPPItsPrecoPed.Value, 2, siNegativo);
  EdEspesTxt.Text         := QrMPPItsEspesTxt.Value;
  EdCorTxt.Text           := QrMPPItsCorTxt.Value;
  RGTipoProd.ItemIndex    := QrMPPItsTipoProd.Value;
  EdFluxo.ValueVariant    := QrMPPItsFluxo.Value;
  CBFluxo.KeyValue        := QrMPPItsFluxo.Value;
  EdVSArtCab.ValueVariant := QrMPPItsVSArtCab.Value;
  CBVSArtCab.KeyValue     := QrMPPItsVSArtCab.Value;
  //
  EdGraGruX.ValueVariant  := QrMPPItsGraGruX.Value;
  CBGraGruX.KeyValue      := QrMPPItsGraGruX.Value;
  //
  LaFluxPcpCab.Enabled      := False;
  EdFluxPcpCab.Enabled      := False;
  CBFluxPcpCab.Enabled      := False;
  SbAlteraFluxPcpCab.Enabled := True;
  EdFluxPcpCab.ValueVariant := QrMPPItsFluxPcpCab.Value;
  CBFluxPcpCab.KeyValue     := QrMPPItsFluxPcpCab.Value;
  LaLastFluxPcpCab.Caption  := '0';
  //
  (*LaTipoIts.Caption*)ImgTipo.SQLType  := stUpd;
end;

procedure TFmMPP.Excluimercadoriaatual1Click(Sender: TObject);
begin
  ExcluiMercadoria;
end;

procedure TFmMPP.Pedido1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMPPCodigo.Value, LaRegistro.Caption);
end;

procedure TFmMPP.PMMercadoriaPopup(Sender: TObject);
begin
  if QrMPPIts.RecordCount > 0 then
  begin
    Alteradadosdamercadoriaatual1.Enabled := True;
    Excluimercadoriaatual1.Enabled := True;
  end else begin
    Alteradadosdamercadoriaatual1.Enabled := False;
    Excluimercadoriaatual1.Enabled := False;
  end;
end;

procedure TFmMPP.BtExcluiClick(Sender: TObject);
var
  Pedido: Integer;
begin
  Pedido := QrMPPCodigo.Value;
  QrPesqV.Close;
  QrPesqV.Params[0].AsInteger := Pedido;
  UnDmkDAC_PF.AbreQuery(QrPesqV, Dmod.MyDB);
  if QrPesqV.RecordCount = 0 then
  begin
    if Geral.MB_Pergunta('Confirma a EXCLUS�O DE TODO pedido?') = ID_YES then
    begin
      Dmod.QrUpd.Close;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM mpvits WHERE Pedido=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Pedido;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.Close;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM mpp WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Pedido;
      Dmod.QrUpd.ExecSQL;
      //
      QrMPP.Close;
      Va(vpNext);
    end;
  end else Geral.MB_Aviso('O pedido n� ' +
  Geral.FF0(Pedido) + ' n�o pode ser exclu�da porque existem ' +
  Geral.FF0(QrPesqV.RecordCount) + ' itens que j� fazem parte de venda(s)!');
end;

procedure TFmMPP.BtImprimeClick(Sender: TObject);
begin
  QrMPPIts.First;
  while not QrMPPIts.Eof do
  begin
    MyObjects.frxImprime(frxOS_3, 'Ordem de produ��o n� ' + IntToStr(QrMPPItsControle.Value));
    QrMPPIts.Next;
  end;
end;

// Parei aqui
//Configurar LTipoIts





{
SELECT its.Entrega, its.M2Pedido, its.Controle,
its.Texto, its.CorTxt, its.EspesTxt, its.Classe,
fpi.Nome NO_IniStg, fpf.Nome NO_FimStg, mps.*
FROM mpvpss mps
LEFT JOIN fluxpcpstg fpi ON fpi.Codigo=mps.IniStg
LEFT JOIN fluxpcpstg fpf ON fpf.Codigo=mps.FimStg
LEFT JOIN mpvits its ON its.Controle=mps.MPVIts
WHERE its.Entrega >= "2020-02-02"
ORDER BY mps.DataSeq, fpi.Ordem, fpf.Ordem
}

end.

