unit UnApp_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, Variants, UnAppEnums, dmkGeral;

type
  TMPVImp = (mpvimpFatur=1, mpvimpExped=2);
  TMPVAcao = (mpvaNenhuma=0, mpvaCabecalho=1, mpvaEncerraOS=2);
  //
  TUnApp_Jan = class(TObject)
  private
    procedure MostraFormMateria_prima;
    { Private declarations }

  public
    { Public declarations }
    procedure CadastroOpcoesBlueDerm();
    procedure ImprimeMonitoramentoSolidos();
    procedure ImprimeControlesETE();
    procedure MostraFormDeGridDeEfdIcmsIpiE001(Sender: TObject);
    procedure MostraFormEmitGru(Codigo: Integer);
    procedure MostraFormFluxProCab(Codigo: Integer);
    procedure MostraFormFluxPcpCab(Codigo: Integer);
    procedure MostraFormFluxPcpStg();
    procedure MostraFormFluxPcpImp();
    procedure MostraFormFormulas(Numero: Integer);
    procedure MostraFormFormulaV(Numero: Integer);
    procedure MostraFormFormulasGru();
    procedure MostraFormFormulasGruImp();
    procedure MostraFormFrmlRibCusCab();
    procedure MostroFormMovimCod(MovimCod: Integer);
    procedure MostraFormProgramacaoEntregas();
    procedure MostraFormPQ(Codigo: Integer);
    procedure MostraFormPQUGer();
    procedure MostraFormPQUsoDiaAtz();
    procedure MostraFormTMapaOperPosProc();
    procedure MostraFormEntradaInsumos(Empresa, FatID, FatNum, MovimCod: Integer);
    procedure MostraFormEntradaMateriaPrima(Empresa, FatID, FatNum, MovimCod: Integer);
    // Compatibilidade
    procedure MostraFormUsoEConsumo();
    procedure MostraFormMateriaPrima();
    procedure MostraFormOsMpvVs();
    function  MostraFormOSsAbertaMPV(Retorna: Boolean): Integer;
    procedure MostraFormGruUsrEdtCab();
    procedure MostraFormMPVImp(MPVImp: TMPVImp);
    procedure MostraFormVeiculos(Codigo: Integer);
    procedure VendasGerencia(AcaoInicial: TMPVAcao);
    procedure MostraFormPQporNF();
    procedure MostraFormPQImpErr();
    function  MostraFormPQB3AdEdit(Empresa, CliInt, Insumo: Integer; NO_Empresa,
              NO_CliInt, NO_Insumo: String; ide_Serie, ide_nNF: Integer; xLote:
              String; dFab, dVal: TDateTime; MostradFabdVal: Boolean): Boolean;
    procedure MostraFormPQCorrigeGGXNiv2();
    procedure MostraFormFormulasCorrigeGrupo();


  end;

var
  App_Jan: TUnApp_Jan;


implementation

uses
  MyDBCheck, Module, UnVS_PF, CfgCadLista, AppFormularios,
  OpcoesBlueDerm, FluxosCab, FluxPcpCab, FluxPcpImp, MPPProgEntreg, PQUGer, PQ,
  Formulas, EmitGru, PQUsoDiaAtz, MapaOperPosProc, FormulasGruImp, UnPQ_PF,
  FrmlRibCusCab, UnVS_CRC_PF, UnGrade_Jan, FormulaV, OSsAbertas, OsMpvVs,
  GruUsrEdtCab, MPVImpFat, MPVImpExp, Veiculos, MPVn, MPVn2, MPV2, PQporNF,
  PQImpErr, PQB3AdEdit, PQCorrigeGGXNiv2, FormulasCorrigeGrupo;

{ TUnApp_Jan }

procedure TUnApp_Jan.CadastroOpcoesBlueDerm();
begin
  if DBCheck.CriaFm(TFmOpcoesBlueDerm, FmOpcoesBlueDerm, afmoSoBoss) then
  begin
    FmOpcoesBlueDerm.ShowModal;
    FmOpcoesBlueDerm.Destroy;
  end;
end;

procedure TUnApp_Jan.ImprimeControlesETE();
begin
  Application.CreateForm(TFmAppFormularios, FmAppFormularios);
  FmAppFormularios.ImprimeControlesDiarios();
  FmAppFormularios.Destroy;
end;

procedure TUnApp_Jan.ImprimeMonitoramentoSolidos();
begin
  Application.CreateForm(TFmAppFormularios, FmAppFormularios);
  FmAppFormularios.ImprimeMonitoramentoSolidos();
  FmAppFormularios.Destroy;
end;

procedure TUnApp_Jan.MostraFormDeGridDeEfdIcmsIpiE001(Sender: TObject);
var
  IMEC, IMEI: Integer;
  DBGrid: TDBGrid;
begin
  IMEI := 0;
  IMEC := 0;
  DBGrid := TDBGrid(Sender);
  //
  if (DBGrid.SelectedField.FieldName = 'KndNSU') then
    IMEC := DBGrid.SelectedField.AsInteger;
  if (DBGrid.SelectedField.FieldName = 'KndItm')
  or (DBGrid.SelectedField.FieldName = 'KndItmOri')
  or (DBGrid.SelectedField.FieldName = 'KndItmDst') then
    IMEI := DBGrid.SelectedField.AsInteger;
  //
  if IMEI <> 0 then
    VS_PF.MostraFormVSMovIts(IMEI);
  if IMEC <> 0 then
    VS_PF.MostraFormVSMovCab(IMEC);
end;

procedure TUnApp_Jan.MostraFormEmitGru(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmEmitGru, FmEmitGru, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEmitGru.LocCod(Codigo, Codigo);
    FmEmitGru.ShowModal;
    FmEmitGru.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormEntradaInsumos(Empresa, FatID, FatNum, MovimCod: Integer);
begin
  PQ_PF.MostraFormPQE(FatNum);
end;

procedure TUnApp_Jan.MostraFormEntradaMateriaPrima(Empresa, FatID, FatNum,
  MovimCod: Integer);
var
  MovimID: Integer;
begin
  MovimID := VS_CRC_PF.ObtemMovimIDdeFatID(FatID);
  if MovimID <> 0 then
    VS_CRC_PF.MostraFormVS_XXX(MovimID, FatNum, (*Controle*)0);
end;

procedure TUnApp_Jan.MostraFormFluxPcpCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmFluxPcpCab, FmFluxPcpCab, afmoSoBoss) then
  begin
    if Codigo <> 0 then
      FmFluxPcpCab.LocCod(Codigo, Codigo);
    FmFluxPcpCab.ShowModal;
    FmFluxPcpCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormFluxPcpImp();
begin
  if DBCheck.CriaFm(TFmFluxPcpImp, FmFluxPcpImp, afmoSoBoss) then
  begin
    FmFluxPcpImp.ShowModal;
    FmFluxPcpImp.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormFluxPcpStg();
begin
  UnCfgCadLista.MostraCadListaOrdem(Dmod.MyDB, 'FluxPcpStg', 30, UnDmkEnums.TNovoCodigo.ncGerlSeq1,
  'Est�gios de Fluxos de PCP',
  [], False, Null, [], [], False);
end;

procedure TUnApp_Jan.MostraFormFluxProCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmFluxosCab, FmFluxosCab, afmoSoBoss) then
  begin
    if Codigo <> 0 then
      FmFluxosCab.LocCod(Codigo, Codigo);
    FmFluxosCab.ShowModal;
    FmFluxosCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormFormulas(Numero: Integer);
begin
  if DBCheck.CriaFm(TFmFormulas, FmFormulas, afmoNegarComAviso) then
  begin
    if Numero <> 0 then
      FmFormulas.TbFormulas.Locate('Numero', Numero, []);
    FmFormulas.ShowModal;
    FmFormulas.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormFormulasCorrigeGrupo();
begin
  if DBCheck.CriaFm(TFmFormulasCorrigeGrupo, FmFormulasCorrigeGrupo, afmoNegarComAviso) then
  begin
    FmFormulasCorrigeGrupo.ShowModal;
    FmFormulasCorrigeGrupo.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormFormulasGru();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'FormulasGru', 60, UnDmkEnums.TNovoCodigo.ncGerlSeq1,
  'Cadastro de Grupos de Receitas',
  [], False, Null, [], [], False);
end;

procedure TUnApp_Jan.MostraFormFormulasGruImp();
begin
  if DBCheck.CriaFm(TFmFormulasGruImp, FmFormulasGruImp, afmoNegarComAviso) then
  begin
    FmFormulasGruImp.ShowModal;
    FmFormulasGruImp.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormFormulaV(Numero: Integer);
begin
  if DBCheck.CriaFm(TFmFormulaV, FmFormulaV, afmoNegarComAviso) then
  begin
    if Numero <> 0 then
      FmFormulaV.LocCod(Numero, Numero);
    FmFormulaV.ShowModal;
    FmFormulaV.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormFrmlRibCusCab();
begin
  if DBCheck.CriaFm(TFmFrmlRibCusCab, FmFrmlRibCusCab, afmoNegarComAviso) then
  begin
    FmFrmlRibCusCab.ShowModal;
    FmFrmlRibCusCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormGruUsrEdtCab();
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  if DBCheck.CriaFm(TFmGruUsrEdtCab, FmGruUsrEdtCab, afmoNegarComAviso) then
  begin
    FmGruUsrEdtCab.ShowModal;
    FmGruUsrEdtCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormMateriaPrima;
begin
  Grade_Jan.MostraFormGraGruY(0, 0, '');
end;

procedure TUnApp_Jan.MostraFormMateria_prima;
begin
  MostraFormPQ(0);
end;

procedure TUnApp_Jan.MostraFormMPVImp(MPVImp: TMPVImp);
const
  sProcName = 'TUnApp_Jan.MostraFormMPVImp()';
begin
  case MPVImp of
    TMPVImp.mpvimpFatur:
    begin
      if DBCheck.CriaFm(TFmMPVImpFat, FmMPVImpFat, afmoNegarComAviso) then
      begin
        FmMPVImpFat.ImgTipo.SQLType := stIns;
        FmMPVImpFat.ShowModal;
        FmMPVImpFat.Destroy;
      end;
    end;
    TMPVImp.mpvimpExped:
    begin
      if DBCheck.CriaFm(TFmMPVImpExp, FmMPVImpExp, afmoNegarComAviso) then
      begin
        FmMPVImpExp.ImgTipo.SQLType := stIns;
        FmMPVImpExp.ShowModal;
        FmMPVImpExp.Destroy;
      end;
    end;
    else
      Geral.MB_Erro('"TMPVImp" n�o implementado em ' + sProcName);
  end;
end;

procedure TUnApp_Jan.MostraFormOsMpvVs();
begin
  if DBCheck.CriaFm(TFmOsMpvVs, FmOsMpvVs, afmoNegarComAviso) then
  begin
    FmOsMpvVs.ImgTipo.SQLType := stPsq;
    FmOsMpvVs.ShowModal;
    FmOsMpvVs.Destroy;
  end;
end;

function TUnApp_Jan.MostraFormOSsAbertaMPV(Retorna: Boolean): Integer;
begin
  if DBCheck.CriaFm(TFmOSsAbertas, FmOSsAbertas, afmoNegarComAviso) then
  begin
    FmOSsAbertas.FRetorna := Retorna;
    FmOSsAbertas.ShowModal;
    Result := FmOSsAbertas.FSelOS;
    FmOSsAbertas.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormPQ(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPQ, FmPQ, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPQ.LocCod(Codigo, Codigo);
    FmPQ.ShowModal;
    FmPQ.Destroy;
  end;
end;

function TUnApp_Jan.MostraFormPQB3AdEdit(Empresa, CliInt, Insumo: Integer;
  NO_Empresa, NO_CliInt, NO_Insumo: String; ide_Serie, ide_nNF: Integer;
  xLote: String; dFab, dVal: TDateTime; MostradFabdVal: Boolean): Boolean;
begin
  if DBCheck.CriaFm(TFmPQB3AdEdit, FmPQB3AdEdit, afmoNegarComAviso) then
  begin
    FmPQB3AdEdit.ImgTipo.SQLType          := stUpd;
    //FmPQB3AdEdit.FBalanco                 := QrPQBAdxCodigo.Value;
    //FmPQB3AdEdit.FControle                := QrPQBAdxControle.Value;
    //FmPQB3AdEdit.FTipo                    := VAR_FATID_0020;
    //
    FmPQB3AdEdit.EdEmpresa.ValueVariant   := Empresa;
    FmPQB3AdEdit.EdNO_Empresa.Text        := NO_EMpresa; //QrBalancosNO_Emp.Value;
    FmPQB3AdEdit.EdCliInt.ValueVariant    := CliInt;
    FmPQB3AdEdit.EdNO_CliInt.Text         := NO_CliInt; //QrCliDestNO_ENT.Value;
    FmPQB3AdEdit.EdInsumo.ValueVariant    := Insumo;
    FmPQB3AdEdit.EdNO_Insumo.Text         := NO_Insumo; //QrPQBAdxNOMEPQ.Value;
    //
    FmPQB3AdEdit.Edide_serie.ValueVariant := ide_Serie; //QrPQBAdxide_Serie.Value;
    FmPQB3AdEdit.Edide_nNF.ValueVariant   := ide_nNF; //VQrPQBAdxide_nNF.Value;
    FmPQB3AdEdit.EdxLote.ValueVariant     := xLote; //QrPQBAdxxLote.Value;
    FmPQB3AdEdit.TPdFab.Date              := dFab; //QrPQBAdxdFab.Value;
    FmPQB3AdEdit.TPdVal.Date              := dVal; //QrPQBAdxdVal.Value;
    //
    if MostradFabdVal = False then
    begin
      FmPQB3AdEdit.TPdFab.Visible := False;
      FmPQB3AdEdit.TPdVal.Visible := False;
    end;
    FmPQB3AdEdit.ShowModal;
    Result := FmPQB3AdEdit.FConfirmou;
    // Destroy ser� ececutado na janela que chamou!
    //FmPQB3AdEdit.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormPQCorrigeGGXNiv2();
begin
  if DBCheck.CriaFm(TFmPQCorrigeGGXNiv2, FmPQCorrigeGGXNiv2, afmoNegarComAviso) then
  begin
    FmPQCorrigeGGXNiv2.ShowModal;
    FmPQCorrigeGGXNiv2.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormPQImpErr;
begin
  if DBCheck.CriaFm(TFmPQImpErr, FmPQImpErr, afmoNegarComAviso) then
  begin
    FmPQImpErr.ShowModal;
    FmPQImpErr.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormPQporNF();
begin
  if DBCheck.CriaFm(TFmPQporNF, FmPQporNF, afmoNegarComAviso) then
  begin
    FmPQporNF.ShowModal;
    FmPQporNF.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormPQUGer();
begin
  if DBCheck.CriaFm(TFmPQUGer, FmPQUGer, afmoNegarComAviso) then
  begin
    FmPQUGer.ShowModal;
    FmPQUGer.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormPQUsoDiaAtz;
begin
  if DBCheck.CriaFm(TFmPQUsoDiaAtz, FmPQUsoDiaAtz, afmoNegarComAviso) then
  begin
    FmPQUsoDiaAtz.ShowModal;
    FmPQUsoDiaAtz.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormProgramacaoEntregas();
begin
  if DBCheck.CriaFm(TFmMPPProgEntreg, FmMPPProgEntreg, afmoSoBoss) then
  begin
    FmMPPProgEntreg.SetaAcabammento();
    //
    FmMPPProgEntreg.ShowModal;
    FmMPPProgEntreg.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormTMapaOperPosProc();
begin
  if DBCheck.CriaFm(TFmMapaOperPosProc, FmMapaOperPosProc, afmoNegarComAviso) then
  begin
    FmMapaOperPosProc.CGOperPosStatus.Value := FmMapaOperPosProc.FAptosAOperacao;
    FmMapaOperPosProc.ReabrePesquisa();
    //
    FmMapaOperPosProc.ShowModal;
    FmMapaOperPosProc.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormUsoEConsumo;
begin

end;

procedure TUnApp_Jan.MostraFormVeiculos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVeiculos, FmVeiculos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVeiculos.LocCod(Codigo, Codigo);
    FmVeiculos.ShowModal;
    FmVeiculos.Destroy;
  end;
end;

procedure TUnApp_Jan.MostroFormMovimCod(MovimCod: Integer);
begin
  VS_PF.MostroFormVSMovimCod(MovimCod);
end;

procedure TUnApp_Jan.VendasGerencia(AcaoInicial: TMPVAcao);
begin
  case Dmod.QrControleVendaCouro.Value of
    1:
    begin
      if AcaoInicial = TMPVAcao.mpvaCabecalho then
      begin
        if DBCheck.CriaFm(TFmMPVn, FmMPVn, afmoNegarComAviso) then
        begin
          FmMPVn.FDireto := True;
          FmMPVn.ShowModal;
          FmMPVn.Destroy;
        end;
      end else
      begin
        if DBCheck.CriaFm(TFmMPVn2, FmMPVn2, afmoNegarComAviso) then
        begin
          FmMPVn2.FAcaoInicial := AcaoInicial;
          FmMPVn2.ShowModal;
          FmMPVn2.Destroy;
        end;
      end;
    end;
    2:
    begin
      if DBCheck.CriaFm(TFmMPV2, FmMPV2, afmoNegarComAviso) then
      begin
        FmMPV2.FDireto := False;
        FmMPV2.ShowModal;
        FmMPV2.Destroy;
      end;
    end;
  end;
end;

end.
