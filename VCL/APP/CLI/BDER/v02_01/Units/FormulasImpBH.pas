unit FormulasImpBH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs, ZCF2,
  UnInternalConsts, UnMsgInt, UnInternalConsts2, Grids, DBGrids, AppListas,
  mySQLDbTables, ComCtrls, Menus, Variants, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkGeral, dmkImage, MyListas, UnDmkProcFunc, UnProjGroup_Consts,
  UnDmkEnums, dmkEditDateTimePicker, UnAppEnums, UnAppPF;

type
  TFmFormulasImpBH = class(TForm)
    QrDefPecas: TmySQLQuery;
    DsDefPecas: TDataSource;
    DsFormulas: TDataSource;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    QrDefPecasLk: TIntegerField;
    Panel2: TPanel;
    QrFormulas: TmySQLQuery;
    QrFormulasNumero: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrFormulasClienteI: TIntegerField;
    QrFormulasTipificacao: TIntegerField;
    QrFormulasDataI: TDateField;
    QrFormulasDataA: TDateField;
    QrFormulasTecnico: TWideStringField;
    QrFormulasTempoR: TIntegerField;
    QrFormulasTempoP: TIntegerField;
    QrFormulasTempoT: TIntegerField;
    QrFormulasHorasR: TIntegerField;
    QrFormulasHorasP: TIntegerField;
    QrFormulasHorasT: TIntegerField;
    QrFormulasHidrica: TIntegerField;
    QrFormulasLinhaTE: TIntegerField;
    QrFormulasCaldeira: TIntegerField;
    QrFormulasSetor: TIntegerField;
    QrFormulasEspessura: TIntegerField;
    QrFormulasPeso: TFloatField;
    QrFormulasQtde: TFloatField;
    QrFormulasLk: TIntegerField;
    QrFormulasDataCad: TDateField;
    QrFormulasDataAlt: TDateField;
    QrFormulasUserCad: TIntegerField;
    QrFormulasUserAlt: TIntegerField;
    QrFormulasHHMM_P: TWideStringField;
    QrFormulasHHMM_R: TWideStringField;
    QrFormulasHHMM_T: TWideStringField;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntNOMECI: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    Panel3: TPanel;
    RGImpRecRib: TRadioGroup;
    GBkgTon: TGroupBox;
    RBTon: TRadioButton;
    RBkg: TRadioButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PainelDados: TPanel;
    PainelReceita: TPanel;
    Label1: TLabel;
    Label11: TLabel;
    EdReceita: TdmkEditCB;
    CBReceita: TdmkDBLookupComboBox;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    PainelEscolhas: TPanel;
    TabSheet3: TTabSheet;
    PainelConfig: TPanel;
    RGTipoPreco: TRadioGroup;
    RGImprime: TRadioGroup;
    QrLotes: TmySQLQuery;
    DsLotes: TDataSource;
    QrFormulasAtivo: TSmallintField;
    QrSoma: TmySQLQuery;
    QrSomaPeso: TFloatField;
    QrSomaPecas: TFloatField;
    PainelEscolhe: TPanel;
    Panel5: TPanel;
    Label6: TLabel;
    BtAdiciona: TBitBtn;
    BtExclui: TBitBtn;
    DBGrid1: TDBGrid;
    CkMatricial: TCheckBox;
    CkGrade: TCheckBox;
    CkContinua: TCheckBox;
    Label10: TLabel;
    EdPeso: TdmkEdit;
    Label3: TLabel;
    EdQtde: TdmkEdit;
    Label7: TLabel;
    SpeedButton1: TSpeedButton;
    EdMedia: TdmkEdit;
    Label12: TLabel;
    Label9: TLabel;
    EdFulao: TdmkEdit;
    TPDataP: TdmkEditDateTimePicker;
    LaData: TLabel;
    PMAdiciona: TPopupMenu;
    Correto1: TMenuItem;
    Mdia1: TMenuItem;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    EdMemo: TMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtCancela: TBitBtn;
    ProgressBar1: TProgressBar;
    EdPeca: TdmkEditCB;
    CBPeca: TdmkDBLookupComboBox;
    LaSP_A: TLabel;
    LaSP_B: TLabel;
    LaSP_C: TLabel;
    QrFormulasRetrabalho: TSmallintField;
    IMEI1: TMenuItem;
    QrLotesControle: TIntegerField;
    QrLotesVSMovIts: TIntegerField;
    QrLotesPeso: TFloatField;
    QrLotesPecas: TFloatField;
    QrLotesSerieFch: TIntegerField;
    QrLotesFicha: TIntegerField;
    QrLotesMarca: TWideStringField;
    QrLotesNO_SerieFch: TWideStringField;
    DBRadioGroup1: TDBRadioGroup;
    QrFormulasBxaEstqVS: TSmallintField;
    QrFormulasGraGruX: TIntegerField;
    EdHoraIni: TdmkEdit;
    LaHora: TLabel;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Label13: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrFormulasNumCODIF: TWideStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    procedure BtCancelaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdPesoChange(Sender: TObject);
    procedure EdQtdeChange(Sender: TObject);
    procedure EdReceitaChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFormulasCalcFields(DataSet: TDataSet);
    procedure BtAdicionaClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Mdia1Click(Sender: TObject);
    procedure Correto1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure IMEI1Click(Sender: TObject);
    procedure QrLotesAfterOpen(DataSet: TDataSet);
    procedure CkDtCorrApoClick(Sender: TObject);
    procedure EdReceitaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBReceitaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FAlturaInicial: Integer;
  public
    { Public declarations }
    FEmit, FMovimCod: Integer;
    FVSMovimSrc: TEstqMovimNiv;
    FNomeForm: String;
    procedure ReopenLotes();
    procedure AtualizaPesoEPecas();
  end;

var
  FmFormulasImpBH: TFmFormulasImpBH;
  REICalArea : Boolean;
  REISetor : ShortInt;

implementation

uses UnMyObjects, Principal, Formulas, Module, FormulasImpShow, DmkDAC_PF,
  BlueDermConsts, FormulasImpBHLocLote, FormulasImpBHInsLote, UMySQLModule,
  ModEmit, FormulasImpShowNew, UnVS_PF, ModuleGeral, UnPQ_PF, PQx;

{$R *.DFM}

procedure TFmFormulasImpBH.BtCancelaClick(Sender: TObject);
begin
  if FEmit <> 0 then
  begin
    DmModEmit.ExcluiLoteCus(FEmit, emidIndsWE); // Nao existe emidIndsBH!
    if ImgTipo.SQLType <> stUpd then
       UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Emit', FEmit);
  end;
  Close;
end;

procedure TFmFormulasImpBH.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  FMovimCod := 0;
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  MyObjects.PreencheComponente(RGImpRecRib, sListaRecImpApresenta, 6);
  RGImpRecRib.ItemIndex := Dmod.QrControleImpRecRib.Value;
  //
  //STSP.Caption := '';
  MyObjects.Entitula(GB_L, [LaSP_A, LaSP_B, LaSP_C],
  [], '', False, taCenter, 2, 10, 20);

  PageControl1.ActivePageIndex := 0;
  Agora := DmodG.ObtemAgora();
  TPDataP.Date := Agora;
  EdHoraIni.ValueVariant := Agora - Int(Agora);
  FAlturaInicial := Height;
  if VAR_NOMEFORMIMP_ANCESTRAL = 'FmPrincipal' then
  begin
    PageControl2.Visible := IC2_WopcoesImpRecShowLotes;
    EdMemo.Visible := IC2_WopcoesImpRecShowLotes;

    Label6.Visible := IC2_WopcoesImpRecShowMedia;
    EdMedia.Visible := IC2_WopcoesImpRecShowMedia;

{
    Label2.Visible := IC2_WopcoesImpRecShowEspessura;
    CBEspessura.Visible := IC2_WopcoesImpRecShowEspessura;

    Label4.Visible := IC2_WopcoesImpRecShowM2;
    EdAreaM.Visible := IC2_WopcoesImpRecShowM2;

    Label5.Visible := IC2_WopcoesImpRecShowP2;
    EdAreaP.Visible := IC2_WopcoesImpRecShowP2;
}

  end;

  REICalArea := False;
  UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  if VAR_SETOR = CO_CALEIRO then REISetor := -3;
  if VAR_SETOR = CO_CURTIM then REISetor := -2;
  if VAR_SETOR = CO_RECURT then REISetor := -1;
  if VAR_SETOR = CO_ACABTO then REISetor := -4;
  case REISetor of
    -3 : CBPeca.KeyValue := -10;
    -2 : CBPeca.KeyValue := -10;
    -1 : CBPeca.KeyValue := -9;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
    'SELECT * FROM formulas ',
    'WHERE Ativo=1 ',
    Geral.ATS_If(VAR_SETOR <> CO_VAZIO, ['AND Setor=' + Geral.FF0(REISetor)]),
    'ORDER BY Nome ',
    '']);
end;

procedure TFmFormulasImpBH.BtConfirmaClick(Sender: TObject);
var
  Codigo, Formula, BxaEstqVS, GraGruX, (*CliInt,*) Empresa: Integer;
  DataEmis: String;
begin
  //Parei Aqui
  //Tipificacao em vez de setor
  //arrumar Custo
  //
  BxaEstqVS := QrFormulasBxaEstqVS.Value;
  GraGruX   := QrFormulasGraGruX.Value;
  Formula   := EdReceita.ValueVariant;
  //
  if MyObjects.FIC(Formula = 0, EdReceita, 'Informe a receita!') then Exit;
  //
  if not PQ_PF.ValidaProdutosFormulasRibeira(Formula) then
  begin
    if Geral.MB_Pergunta('A receita informada possui produtos irregulares ' +
      sLineBreak + 'e por isso n�o poder� ser impressa!' + sLineBreak +
      'Deseja localizar a receita para regularizar o cadastro?') = ID_YES then
    begin
      VAR_CADASTRO := Formula;
      Close;
    end;
    Exit;
  end;
  //
  if MyObjects.FIC(BxaEstqVS < Integer(TBxaEstqVS.bevsNao), nil,
    'Pesagem abortada!' + sLineBreak +
    '"Baixa estq VS In Natura" indefinido no cadastro da Receita!') then Exit;
  if MyObjects.FIC(GraGruX = 0, nil, 'Pesagem abortada!' + sLineBreak +
    '"Artigo padr�o em processo" indefinido no cadastro da Receita!') then Exit;
  //
  if RGTipoPreco.ItemIndex = 2 then
    if not DmModEmit.MostraPrecosAlternativos(dmktfrmSetrEmi_MOLHADO, Formula) then
      Exit;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //CliInt := EdCliInt.ValueVariant;
  //
  // ini 2024-01-01
  if UnPQx.ImpedePeloBalanco(TPDataP.Date, True) then Exit;
  // fim 2024-01-01
  //
  if Geral.MB_Pergunta('Confirma que a receita a ser utilizada �:' +
    sLineBreak + QrFormulasNome.Value + sLineBreak + 'E o peso �: ' +
    EdPeso.Text + ' ?') = ID_YES then
  begin
    if FEmit > 0 then
    begin
      //if PQ_PF.InsumosSemCadastroParaClienteInterno(CliInt, Empresa,
      //(*InsumoEspecifico*)0, TTabPqNaoCad.tpncPesagem) then Exit;
      //
      DataEmis := Geral.FDT(TPDataP.Date, 1);
      Codigo := FEmit;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'emitcus', False, [
      'Formula', 'DataEmis',
      'BxaEstqVS', 'GraGruX'], ['Codigo'], [
      Formula, DataEmis,
      BxaEstqVS, GraGruX], [Codigo], True);
    end;
    BDC_APRESENTACAO_IMPRESSAO := RGImpRecRib.ItemIndex;
    //
    // Configuracoes


    case Dmod.QrControleVerImprRecRib.Value of
      0:
      begin
        FrFormulasImpShow.FReImprime    := False;
        FrFormulasImpShow.FVSMovCod     := FMovimCod;
        FrFormulasImpShow.FVSMovimSrc   := FVSMovimSrc;
        FrFormulasImpShow.FEmit         := FEmit;
        FrFormulasImpShow.FEmitGru      := 0;
        FrFormulasImpShow.FEmitGru_TXT  := '';
        FrFormulasImpShow.FRetrabalho   := QrFormulasRetrabalho.Value;;
        //FrFormulasImpShow.FSourcMP      := CO_SourcMP_Ribeira; // Caleiro / curtimento
        FrFormulasImpShow.FSourcMP      := CO_SourcMP_Ribeira; // Caleiro / curtimento
        FrFormulasImpShow.FDataP        := TPDataP.Date;
        FrFormulasImpShow.FDtCorrApo    := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
        FrFormulasImpShow.FEmpresa      := Empresa;
        FrFormulasImpShow.FHoraIni      := EdHoraIni.ValueVariant;
        FrFormulasImpShow.FProgressBar1 := ProgressBar1;
        FrFormulasImpShow.FTipoPreco    := RGTipoPreco.ItemIndex;
        FrFormulasImpShow.FCliIntCod    := EdCliInt.ValueVariant;
        FrFormulasImpShow.FCliIntTxt    := CBCliInt.Text;
        FrFormulasImpShow.FFormula      := Formula;
        FrFormulasImpShow.FTipoImprime  := RGImprime.ItemIndex;
        FrFormulasImpShow.FPeso         := EdPeso.ValueVariant;
        FrFormulasImpShow.FQtde         := EdQtde.ValueVariant;
        FrFormulasImpShow.FArea         := 0;//EdAreaM.ValueVariant;
        FrFormulasImpShow.FLinhasRebx   := '';//CBEspessura.Text;
        FrFormulasImpShow.FLinhasSemi   := '';//CBEspessura.Text;
        FrFormulasImpShow.FFulao        := EdFulao.Text;
        FrFormulasImpShow.FDefPeca      := CBPeca.Text;
        FrFormulasImpShow.FHHMM_P       := QrFormulasHHMM_P.Value;
        FrFormulasImpShow.FHHMM_R       := QrFormulasHHMM_R.Value;
        FrFormulasImpShow.FHHMM_T       := QrFormulasHHMM_T.Value;
        FrFormulasImpShow.FSetor        := QrFormulasSetor.Value;
        FrFormulasImpShow.FTipific      := QrFormulasTipificacao.Value;
        FrFormulasImpShow.FCod_Espess   := 0;//EdEspessura.ValueVariant;
        FrFormulasImpShow.FCod_Rebaix   := 0;//EdEspessura.ValueVariant;
        FrFormulasImpShow.FCodDefPeca   := EdPeca.ValueVariant;
        FrFormulasImpShow.FSemiAreaM2   := 0;
        FrFormulasImpShow.FSemiRendim   := 0;
        FrFormulasImpShow.FBRL_USD      := 0;
        FrFormulasImpShow.FBRL_EUR      := 0;
        FrFormulasImpShow.FDtaCambio    := 0;
        //
        FrFormulasImpShow.FObs := FmFormulasImpBH.EdMemo.Lines[0]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmFormulasImpBH.EdMemo.Lines[1]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmFormulasImpBH.EdMemo.Lines[2]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmFormulasImpBH.EdMemo.Lines[3]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmFormulasImpBH.EdMemo.Lines[4]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmFormulasImpBH.EdMemo.Lines[5]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmFormulasImpBH.EdMemo.Lines[6];
        //
        if FmFormulasImpBH.CkMatricial.Checked then FrFormulasImpShow.FMatricialNovo := True
          else FrFormulasImpShow.FMatricialNovo := False;
        if FmFormulasImpBH.CkGrade.Checked then FrFormulasImpShow.FGrade := True
          else FrFormulasImpShow.FGrade := False;
        //
        if RGImpRecRib.ItemIndex = 1 then FrFormulasImpShow.FCalcCustos := True
          else FrFormulasImpShow.FCalcCustos := False;
        if RGImpRecRib.ItemIndex = 2 then FrFormulasImpShow.FCalcPrecos := True
          else FrFormulasImpShow.FCalcPrecos := False;
        if FmFormulasImpBH.RBkg.Checked then FrFormulasImpShow.FPesoCalc := 0.01
          else FrFormulasImpShow.FPesoCalc := 10;
        //
        (*
        if (CBEspessura.KeyValue <> NULL) and (CBEspessura.KeyValue <> 0) then FrFormulasImpShow.FMedia := QrEspessurasEMCM.Value
        else begin
        *)
          if FrFormulasImpShow.FQtde > 0 then FrFormulasImpShow.FMedia := FrFormulasImpShow.FPeso / FrFormulasImpShow.FQtde
        else
          FrFormulasImpShow.FMedia := 0;
        //end;
        //
        if MyObjects.FIC(FrFormulasImpShow.FFormula = 0, EdReceita,
        'Defina uma receita!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShow.FCliIntCod = 0, EdCliInt,
        'Defina um cliente interno!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShow.FPeso <= 0, EdPeso, 'Defina o peso!') then
          Exit;
        if QrDefPecasGrandeza.Value <> 1 then
          if MyObjects.FIC(FrFormulasImpShow.FQtde <= 0, EdQtde, 'Defina a quantidade!') then
            Exit;
        //
        //////////////////////////////////////////////////////////////////////////////
        //
        FrFormulasImpShow.CalculaReceita(QrLotes);
        Application.ProcessMessages;
        Hide;
        if CkGrade.Checked then FrFormulasImpShow.Show;
      end;
      1:
      begin
        FrFormulasImpShowNew.FTpReceita    := TReceitaTipoSetor.rectipsetrRibeira;
        FrFormulasImpShowNew.FReImprime    := False;
        FrFormulasImpShowNew.FVSMovCod     := FMovimCod;
        FrFormulasImpShowNew.FVSMovimSrc   := FVSMovimSrc;
        FrFormulasImpShowNew.FEmit         := FEmit;
        FrFormulasImpShowNew.FEmitGru      := 0;
        FrFormulasImpShowNew.FEmitGru_TXT  := '';
        FrFormulasImpShowNew.FRetrabalho   := QrFormulasRetrabalho.Value;;
        FrFormulasImpShowNew.FSourcMP      := CO_SourcMP_Ribeira; // Caleiro / curtimento
        FrFormulasImpShowNew.FDataP        := TPDataP.Date;
        FrFormulasImpShowNew.FDtCorrApo    := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
        FrFormulasImpShowNew.FEmpresa      := Empresa;
        FrFormulasImpShowNew.FHoraIni      := EdHoraIni.ValueVariant;
        FrFormulasImpShowNew.FProgressBar1 := ProgressBar1;
        FrFormulasImpShowNew.FTipoPreco    := RGTipoPreco.ItemIndex;
        FrFormulasImpShowNew.FCliIntCod    := EdCliInt.ValueVariant;
        FrFormulasImpShowNew.FCliIntTxt    := CBCliInt.Text;
        FrFormulasImpShowNew.FFormula      := Formula;
        FrFormulasImpShowNew.FTipoImprime  := RGImprime.ItemIndex;
        FrFormulasImpShowNew.FPeso         := EdPeso.ValueVariant;
        FrFormulasImpShowNew.FQtde         := EdQtde.ValueVariant;
        FrFormulasImpShowNew.FArea         := 0;//EdAreaM.ValueVariant;
        FrFormulasImpShowNew.FLinhasRebx   := '';//CBEspessura.Text;
        FrFormulasImpShowNew.FLinhasSemi   := '';
        FrFormulasImpShowNew.FFulao        := EdFulao.Text;
        FrFormulasImpShowNew.FDefPeca      := CBPeca.Text;
        FrFormulasImpShowNew.FHHMM_P       := QrFormulasHHMM_P.Value;
        FrFormulasImpShowNew.FHHMM_R       := QrFormulasHHMM_R.Value;
        FrFormulasImpShowNew.FHHMM_T       := QrFormulasHHMM_T.Value;
        FrFormulasImpShowNew.FSetor        := QrFormulasSetor.Value;
        FrFormulasImpShowNew.FTipific      := QrFormulasTipificacao.Value;
        FrFormulasImpShowNew.FCod_Espess   := 0;//EdEspessura.ValueVariant;
        FrFormulasImpShowNew.FCod_Rebaix   := 0;
        FrFormulasImpShowNew.FCodDefPeca   := EdPeca.ValueVariant;
        FrFormulasImpShowNew.FSemiAreaM2   := 0;
        FrFormulasImpShowNew.FSemiRendim   := 0;
        FrFormulasImpShowNew.FBRL_USD      := 0;
        FrFormulasImpShowNew.FBRL_EUR      := 0;
        FrFormulasImpShowNew.FDtaCambio    := 0;
        //
        FrFormulasImpShowNew.FObs := FmFormulasImpBH.EdMemo.Lines[0]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmFormulasImpBH.EdMemo.Lines[1]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmFormulasImpBH.EdMemo.Lines[2]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmFormulasImpBH.EdMemo.Lines[3]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmFormulasImpBH.EdMemo.Lines[4]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmFormulasImpBH.EdMemo.Lines[5]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmFormulasImpBH.EdMemo.Lines[6];
        //
        if FmFormulasImpBH.CkMatricial.Checked then FrFormulasImpShowNew.FMatricialNovo := True
          else FrFormulasImpShowNew.FMatricialNovo := False;
        if FmFormulasImpBH.CkGrade.Checked then FrFormulasImpShowNew.FGrade := True
          else FrFormulasImpShowNew.FGrade := False;
        //
        if RGImpRecRib.ItemIndex = 1 then FrFormulasImpShowNew.FCalcCustos := True
          else FrFormulasImpShowNew.FCalcCustos := False;
        if RGImpRecRib.ItemIndex = 2 then FrFormulasImpShowNew.FCalcPrecos := True
          else FrFormulasImpShowNew.FCalcPrecos := False;
        if FmFormulasImpBH.RBkg.Checked then FrFormulasImpShowNew.FPesoCalc := 0.01
          else FrFormulasImpShowNew.FPesoCalc := 10;
        //
        (*
        if (CBEspessura.KeyValue <> NULL) and (CBEspessura.KeyValue <> 0) then FrFormulasImpShowNew.FMedia := QrEspessurasEMCM.Value
        else begin
        *)
          if FrFormulasImpShowNew.FQtde > 0 then FrFormulasImpShowNew.FMedia := FrFormulasImpShowNew.FPeso / FrFormulasImpShowNew.FQtde
        else
          FrFormulasImpShowNew.FMedia := 0;
        //end;
        //
        if MyObjects.FIC(FrFormulasImpShowNew.FFormula = 0, EdReceita,
        'Defina uma receita!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShowNew.FCliIntCod = 0, EdCliInt,
        'Defina um cliente interno!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShowNew.FPeso <= 0, EdPeso, 'Defina o peso!') then
          Exit;
        if QrDefPecasGrandeza.Value <> 1 then
          if MyObjects.FIC(FrFormulasImpShowNew.FQtde <= 0, EdQtde, 'Defina a quantidade!') then
            Exit;
        //
        //////////////////////////////////////////////////////////////////////////////
        //
        FrFormulasImpShowNew.CalculaReceita(QrLotes);
        Application.ProcessMessages;
        Hide;
        if CkGrade.Checked then FrFormulasImpShowNew.Show;




      end;
      else
      begin
        Geral.MB_Erro('Vers�o de impress�o de receita de ribeira n�o implementada [2]!');
        Exit;
      end;
    end;


    if CkContinua.Checked = False then Close else
    begin
    if FEmit > 0 then FEmit := UMyMod.BuscaEmLivreY(
      Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
      //STSP.Caption := IntToStr(FEmit);
      MyObjects.Entitula(GB_L, [LaSP_A, LaSP_B, LaSP_C],
      [], Geral.FF0(FEmit), False, taCenter, 2, 10, 20);
      Show;
    end;
  end;
end;

procedure TFmFormulasImpBH.EdPesoChange(Sender: TObject);
var
  Peso, Qtde, Media : Double;
begin
  Peso := EdPeso.ValueVariant;
  Qtde := EdQtde.ValueVariant;
  if Qtde > 0 then Media := Peso / Qtde else Media := 0;
  EdMedia.ValueVariant := Media;
end;

procedure TFmFormulasImpBH.EdQtdeChange(Sender: TObject);
var
  Peso, Qtde, Media : Double;
begin
  Peso := EdPeso.ValueVariant;
  Qtde := EdQtde.ValueVariant;
  if Qtde > 0 then Media := Peso / Qtde else Media := 0;
  EdMedia.ValueVariant := Media;
end;

procedure TFmFormulasImpBH.EdReceitaChange(Sender: TObject);
begin
  EdCliInt.ValueVariant := QrFormulasClienteI.Value;
  CBCliInt.KeyValue := QrFormulasClienteI.Value;
end;

procedure TFmFormulasImpBH.EdReceitaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdReceita.ValueVariant := AppPF.ObtemNumeroDeCODIF(EdReceita.ValueVariant);
  end;
end;

procedure TFmFormulasImpBH.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if FEmit = 0 then
  begin
    BtAdiciona.Enabled := False;
    BtExclui.Enabled := False;
  end;
  //
  MyObjects.Entitula(GB_L, [LaSP_A, LaSP_B, LaSP_C],
  [], Geral.FF0(FEmit), False, taCenter, 2, 10, 20);
end;

procedure TFmFormulasImpBH.FormDestroy(Sender: TObject);
begin
  VAR_NOMEFORMIMP_ANCESTRAL := CO_VAZIO;
end;

procedure TFmFormulasImpBH.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulasImpBH.IMEI1Click(Sender: TObject);
const
  Empresa      = 0;
  MateriaPrima = 0;
  Formula      = 0;
  BxaEstqVS    = TBxaEstqVS.bevsNao;
var
  //AreaM2,
  PesoKg, Pecas: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    PesoKg := 0;
    Pecas  := 0;
    //AreaM2 := 0;
    VS_PF.MostraFormVSEmitCus(FEmit, Empresa, MateriaPrima, Formula, BxaEstqVS, TPDataP.Date);
    ReopenLotes();
    QrLotes.First;
    while not QrLotes.Eof do
    begin
      PesoKg := PesoKg + QrLotesPeso.Value;
      Pecas  := Pecas  + QrLotesPecas.Value;
      //AreaM2 := AreaM2 + QrLotes.Value;
      //
      QrLotes.Next;
    end;
    EdPeso.ValueVariant  := PesoKg;
    EdQtde.ValueVariant  := Pecas;
    //EdAreaM2.ValueVariant := AreaM2
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFormulasImpBH.QrFormulasCalcFields(DataSet: TDataSet);
begin
  QrFormulasHHMM_P.Value := dmkPF.HorasMH(QrFormulasTempoP.Value, False);
  QrFormulasHHMM_R.Value := dmkPF.HorasMH(QrFormulasTempoR.Value, False);
  QrFormulasHHMM_T.Value := dmkPf.HorasMH(QrFormulasTempoT.Value, False);
  QrFormulasNumCODIF.Value := dmkPF.IntToCODIF_Receita(QrFormulasNumero.Value);
end;

procedure TFmFormulasImpBH.QrLotesAfterOpen(DataSet: TDataSet);
begin
  // 2016-04-25 - Evitar erros (numero e BxaEstqVS) por causa da receita!
  PainelReceita.Enabled := QrLotes.RecordCount = 0;
end;

procedure TFmFormulasImpBH.ReopenLotes();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotes, Dmod.MyDB, [
  'SELECT emc.Controle, emc.VSMovIts, emc.Peso, emc.Pecas,   ',
  'vmi.SerieFch, vmi.Ficha, vmi.Marca,fch.Nome NO_SerieFch  ',
  'FROM emitcus emc ',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=emc.VSMovIts ',
  'LEFT JOIN vsserfch fch ON fch.Codigo=vmi.SerieFch ',
  'WHERE emc.Codigo=' + Geral.FF0(FEmit),
  '']);
end;

procedure TFmFormulasImpBH.BtAdicionaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAdiciona, BtAdiciona);
end;

procedure TFmFormulasImpBH.AtualizaPesoEPecas();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoma, Dmod.MyDB, [
  'SELECT SUM(Peso) Peso,  ',
  'SUM(Pecas) Pecas ',
  'FROM emitcus ',
  'WHERE Codigo=' + Geral.FF0(FEmit),
  '']);
  //QrSoma.Close;
  //QrSoma.Params[0].AsInteger := FEmit;
  //UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
  if (QrSomaPeso.Value >= 0.001) then
  begin
    EdPeso.ValueVariant := QrSomaPeso.Value;
    EdQtde.ValueVariant := QrSomaPecas.Value;
  end;
end;

procedure TFmFormulasImpBH.SpeedButton1Click(Sender: TObject);
begin
  FmPrincipal.CadastrodeDefPecas();
  QrDefPecas.Close;
  UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
end;

procedure TFmFormulasImpBH.Mdia1Click(Sender: TObject);
begin
  ShowMessage('ATEN��O!! Em constru��o!');
  if FEmit > 0 then
  begin
    Application.CreateForm(TFmFormulasImpBHLocLote, FmFormulasImpBHLocLote);
    FmFormulasImpBHLocLote.FEmit                 := FEmit;
    FmFormulasImpBHLocLote.EdCliInt.ValueVariant := EdCliInt.ValueVariant;
    FmFormulasImpBHLocLote.CBCliInt.KeyValue     := CBCliInt.KeyValue;
    FmFormulasImpBHLocLote.ShowModal;
    FmFormulasImpBHLocLote.Destroy;
    AtualizaPesoEPecas();
    ReopenLotes;
  end;
end;

procedure TFmFormulasImpBH.CBReceitaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdReceita.ValueVariant := AppPF.ObtemNumeroDeCODIF(EdReceita.ValueVariant);
  end;
end;

procedure TFmFormulasImpBH.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmFormulasImpBH.Correto1Click(Sender: TObject);
begin
  if FEmit > 0 then
  begin
    Application.CreateForm(TFmFormulasImpBHInsLote, FmFormulasImpBHInsLote);
    FmFormulasImpBHInsLote.FEmit             := FEmit;
    FmFormulasImpBHInsLote.EdCliInt.ValueVariant := EdCliInt.ValueVariant;
    FmFormulasImpBHInsLote.CBCliInt.KeyValue := CBCliInt.KeyValue;
    FmFormulasImpBHInsLote.ShowModal;
    FmFormulasImpBHInsLote.Destroy;
    AtualizaPesoEPecas();
    ReopenLotes;
  end;
end;

procedure TFmFormulasImpBH.BtExcluiClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a retirada do item selecionado?') = ID_YES then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDb, [
    'DELETE FROM emitcus ',
    'WHERE Controle=' + Geral.FF0(QrLotesControle.Value),
    '']);
    //
    AtualizaPesoEPecas();
    ReopenLotes;
  end;
end;

end.

