object FmOSsAbertas: TFmOSsAbertas
  Left = 339
  Top = 185
  Caption = 'OSS-ABERT-001 :: OSs Abertas'
  ClientHeight = 641
  ClientWidth = 1005
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1005
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 953
      Top = 0
      Width = 52
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 894
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 157
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'OSs Abertas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 157
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'OSs Abertas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 157
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'OSs Abertas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 1005
    Height = 499
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1005
      Height = 499
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1005
        Height = 499
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1001
          Height = 482
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel6: TPanel
            Left = 0
            Top = 29
            Width = 1001
            Height = 132
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label10: TLabel
              Left = 8
              Top = 7
              Width = 35
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Cliente:'
            end
            object Label1: TLabel
              Left = 415
              Top = 7
              Width = 30
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Artigo:'
            end
            object Label2: TLabel
              Left = 711
              Top = 7
              Width = 49
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Vendedor:'
            end
            object Label4: TLabel
              Left = 8
              Top = 32
              Width = 51
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Descri'#231#227'o:'
            end
            object Label5: TLabel
              Left = 414
              Top = 32
              Width = 19
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Cor:'
            end
            object Label6: TLabel
              Left = 615
              Top = 32
              Width = 34
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Classe:'
            end
            object Label7: TLabel
              Left = 798
              Top = 32
              Width = 52
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Espessura:'
            end
            object EdCliente: TdmkEditCB
              Left = 48
              Top = 3
              Width = 52
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCliente
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCliente: TdmkDBLookupComboBox
              Left = 102
              Top = 3
              Width = 307
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsClientes
              TabOrder = 1
              dmkEditCB = EdCliente
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CBMP: TdmkDBLookupComboBox
              Left = 502
              Top = 3
              Width = 203
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsMP
              TabOrder = 3
              dmkEditCB = EdMP
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdMP: TdmkEditCB
              Left = 447
              Top = 3
              Width = 52
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBMP
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdVendedor: TdmkEditCB
              Left = 764
              Top = 3
              Width = 52
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBVendedor
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBVendedor: TdmkDBLookupComboBox
              Left = 819
              Top = 3
              Width = 178
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsVendedores
              TabOrder = 5
              dmkEditCB = EdVendedor
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdTexto: TdmkEdit
              Left = 63
              Top = 29
              Width = 346
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object RgFiltro: TRadioGroup
              Left = 551
              Top = 54
              Width = 354
              Height = 47
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Filtro para campos do tipo texto: '
              Columns = 4
              ItemIndex = 1
              Items.Strings = (
                'Sem filtro'
                '%texto%'
                '%texto'
                'texto%')
              TabOrder = 12
            end
            object CkComissoes: TCheckBox
              Left = 551
              Top = 108
              Width = 139
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Mostra comiss'#245'es.'
              Enabled = False
              TabOrder = 13
            end
            object GroupBox2: TGroupBox
              Left = 4
              Top = 54
              Width = 262
              Height = 75
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Data do faturamento: '
              TabOrder = 10
              object TPanel
                Left = 2
                Top = 15
                Width = 258
                Height = 58
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object CkFimDataF: TCheckBox
                  Left = 128
                  Top = 0
                  Width = 100
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data final.'
                  TabOrder = 2
                end
                object CkIniDataF: TCheckBox
                  Left = 10
                  Top = 0
                  Width = 109
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data inicial:'
                  TabOrder = 0
                end
                object TPIniDataF: TDateTimePicker
                  Left = 10
                  Top = 25
                  Width = 112
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 38604.000000000000000000
                  Time = 0.601629953700467000
                  TabOrder = 1
                end
                object TPFimDataF: TDateTimePicker
                  Left = 128
                  Top = 25
                  Width = 112
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 38604.000000000000000000
                  Time = 0.601629953700467000
                  TabOrder = 3
                end
              end
            end
            object GroupBox3: TGroupBox
              Left = 282
              Top = 54
              Width = 262
              Height = 75
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Data da entrega: '
              TabOrder = 11
              object TPanel
                Left = 2
                Top = 15
                Width = 258
                Height = 58
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object CkFimEntrega: TCheckBox
                  Left = 128
                  Top = 0
                  Width = 100
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data final.'
                  TabOrder = 2
                end
                object CkIniEntrega: TCheckBox
                  Left = 10
                  Top = 0
                  Width = 109
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data inicial:'
                  TabOrder = 0
                end
                object TPIniEntrega: TDateTimePicker
                  Left = 10
                  Top = 25
                  Width = 112
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 38604.000000000000000000
                  Time = 0.601629953700467000
                  TabOrder = 1
                end
                object TPFimEntrega: TDateTimePicker
                  Left = 128
                  Top = 25
                  Width = 112
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 38604.000000000000000000
                  Time = 0.601629953700467000
                  TabOrder = 3
                end
              end
            end
            object EdCor: TdmkEdit
              Left = 439
              Top = 29
              Width = 174
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdClasse: TdmkEdit
              Left = 654
              Top = 29
              Width = 140
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEspessura: TdmkEdit
              Left = 854
              Top = 29
              Width = 140
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = 161
            Width = 1001
            Height = 321
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object DBGOSs: TdmkDBGridZTO
              Left = 0
              Top = 120
              Width = 1001
              Height = 201
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsOSs
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              OnDblClick = DBGOSsDblClick
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'DataPedido'
                  Title.Alignment = taCenter
                  Title.Caption = 'Dta. Pedido'
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'Entrega'
                  Title.Alignment = taCenter
                  Width = 65
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEMP'
                  Title.Caption = 'Tipo'
                  Width = 150
                  Visible = True
                end
                item
                  Alignment = taLeftJustify
                  Expanded = False
                  FieldName = 'Pedido'
                  Width = 65
                  Visible = True
                end
                item
                  Alignment = taLeftJustify
                  Expanded = False
                  FieldName = 'OS'
                  Width = 65
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PedidCli'
                  Title.Caption = 'OC'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Texto'
                  Title.Caption = 'Observa'#231#227'o'
                  Width = 251
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CorTxt'
                  Title.Caption = 'Cor'
                  Width = 130
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Classe'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EspesTxt'
                  Title.Caption = 'Espessura'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECLI'
                  Title.Caption = 'Cliente'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEVEN'
                  Title.Caption = 'Vendedor'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'M2Pedido'
                  Title.Alignment = taRightJustify
                  Title.Caption = #193'rea m'#178
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CondicaoPG'
                  Title.Caption = 'Condi'#231#227'o de pagamento'
                  Width = 140
                  Visible = True
                end>
            end
            object PnImprime: TPanel
              Left = 0
              Top = 0
              Width = 1001
              Height = 120
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object RGAgrupa: TRadioGroup
                Left = 860
                Top = 0
                Width = 53
                Height = 120
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Agrup.: '
                ItemIndex = 1
                Items.Strings = (
                  '0'
                  '1'
                  '2'
                  '3')
                TabOrder = 0
              end
              object RGOrdem4: TRadioGroup
                Left = 516
                Top = 0
                Width = 172
                Height = 120
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Ordem 4: '
                Columns = 2
                ItemIndex = 2
                Items.Strings = (
                  'Cliente'
                  'Artigo'
                  'Vendedor'
                  'Descri'#231#227'o'
                  'Emiss'#227'o'
                  'Entrega'
                  'Cor'
                  'Classe'
                  'Espessura')
                TabOrder = 1
              end
              object RGOrdem3: TRadioGroup
                Left = 344
                Top = 0
                Width = 172
                Height = 120
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Ordem 3: '
                Columns = 2
                ItemIndex = 1
                Items.Strings = (
                  'Cliente'
                  'Artigo'
                  'Vendedor'
                  'Descri'#231#227'o'
                  'Emiss'#227'o'
                  'Entrega'
                  'Cor'
                  'Classe'
                  'Espessura')
                TabOrder = 2
              end
              object RGOrdem2: TRadioGroup
                Left = 172
                Top = 0
                Width = 172
                Height = 120
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Ordem 2: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'Cliente'
                  'Artigo'
                  'Vendedor'
                  'Descri'#231#227'o'
                  'Emiss'#227'o'
                  'Entrega'
                  'Cor'
                  'Classe'
                  'Espessura')
                TabOrder = 3
              end
              object RGOrdem1: TRadioGroup
                Left = 0
                Top = 0
                Width = 172
                Height = 120
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Ordem 1: '
                Columns = 2
                ItemIndex = 5
                Items.Strings = (
                  'Cliente'
                  'Artigo'
                  'Vendedor'
                  'Descri'#231#227'o'
                  'Emiss'#227'o'
                  'Entrega'
                  'Cor'
                  'Classe'
                  'Espessura')
                TabOrder = 4
              end
              object RGOrdem5: TRadioGroup
                Left = 688
                Top = 0
                Width = 172
                Height = 120
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Ordem 5: '
                Columns = 2
                ItemIndex = 3
                Items.Strings = (
                  'Cliente'
                  'Artigo'
                  'Vendedor'
                  'Descri'#231#227'o'
                  'Emiss'#227'o'
                  'Entrega'
                  'Cor'
                  'Classe'
                  'Espessura')
                TabOrder = 5
              end
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 1001
            Height = 29
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
            object Label40: TLabel
              Left = 8
              Top = 9
              Width = 44
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Empresa:'
              Enabled = False
            end
            object Label3: TLabel
              Left = 586
              Top = 9
              Width = 69
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Transportador:'
              Enabled = False
            end
            object EdEmpresa: TdmkEditCB
              Left = 60
              Top = 5
              Width = 52
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Filial'
              UpdCampo = 'Empresa'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEmpresa
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBEmpresa: TdmkDBLookupComboBox
              Left = 114
              Top = 5
              Width = 471
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Enabled = False
              KeyField = 'Filial'
              ListField = 'NOMEFILIAL'
              ListSource = DModG.DsEmpresas
              TabOrder = 1
              dmkEditCB = EdEmpresa
              QryCampo = 'Filial'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdTransp: TdmkEditCB
              Left = 656
              Top = 5
              Width = 52
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBTransp
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBTransp: TdmkDBLookupComboBox
              Left = 711
              Top = 5
              Width = 286
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Color = clWhite
              Enabled = False
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsTransp
              TabOrder = 3
              dmkEditCB = EdTransp
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 546
    Width = 1005
    Height = 33
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1001
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 579
    Width = 1005
    Height = 62
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 825
      Top = 15
      Width = 178
      Height = 45
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 11
        Top = 2
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 823
      Height = 45
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 5
        Left = 15
        Top = 2
        Width = 150
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pesquisa e &Imprime'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtPesquisa: TBitBtn
        Tag = 18
        Left = 167
        Top = 1
        Width = 150
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Apenas &Pesquisa'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtPesquisaClick
      end
      object BtImpSel: TBitBtn
        Tag = 5
        Left = 319
        Top = 1
        Width = 150
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Imprime &Selecionados'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtImpSelClick
      end
    end
  end
  object frxDsOSs: TfrxDBDataset
    UserName = 'frxDsOSs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataPedido=DataPedido'
      'Cliente=Cliente'
      'Vendedor=Vendedor'
      'OS=OS'
      'Texto=Texto'
      'Entrega=Entrega'
      'Fluxo=Fluxo'
      'Classe=Classe'
      'Pedido=Pedido'
      'EspesTxt=EspesTxt'
      'CorTxt=CorTxt'
      'M2Pedido=M2Pedido'
      'Observ=Observ'
      'NOMECLI=NOMECLI'
      'NOMEVEN=NOMEVEN'
      'NOMEMP=NOMEMP'
      'NO_CondicaoPG=NO_CondicaoPG'
      'PedidCli=PedidCli')
    DataSet = QrOSs
    BCDToCurrency = False
    DataSetOptions = []
    Left = 496
    Top = 108
  end
  object frxOSs_: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38928.750734444400000000
    ReportOptions.Name = 'Or'#231'amento'
    ReportOptions.LastChange = 38928.750734444400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);'
      'end.')
    Left = 248
    Top = 16
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsOSs
        DataSetName = 'frxDsOSs'
      end>
    Variables = <
      item
        Name = ' MyVars'
        Value = Null
      end
      item
        Name = 'MeuLogo'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 279.400000000000000000
      PaperHeight = 215.900000000000000000
      PaperSize = 1
      LeftMargin = 5.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 64.251968500000000000
        Top = 18.897650000000000000
        Width = 961.512431999999900000
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Width = 166.299212600000000000
          Height = 64.251968500000000000
          DataField = 'Logo'
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Top = 3.779530000000001000
          Width = 642.519855910000000000
          Height = 18.897650000000000000
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Top = 26.456710000000000000
          Width = 117.165430000000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data / hora da impress'#227'o:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Top = 26.456710000000000000
          Width = 642.520100000000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date] [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 823.937540000000000000
          Top = 3.779530000000001000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 336.378170000000000000
        Width = 961.512431999999900000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 204.094620000000000000
        Width = 961.512431999999900000
        DataSet = frxDsOSs
        DataSetName = 'frxDsOSs'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Entrega'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."Entrega"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'NOMEMP'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."NOMEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          DataField = 'Texto'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'CorTxt'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Classe'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOSs."Classe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'EspesTxt'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOSs."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 56.692950000000010000
          Height = 15.118110240000000000
          DataField = 'M2Pedido'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOSs."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299630000000000000
          Width = 264.566931570000000000
          Height = 15.118110240000000000
          DataField = 'NOMECLI'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."NOMECLI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 910.866730000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'NOMEVEN'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."NOMEVEN"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataField = 'Pedido'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOSs."Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Width = 56.692950000000010000
          Height = 15.118110240000000000
          DataField = 'OS'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOSs."OS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'DataPedido'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."DataPedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795290240000000000
        Top = 105.826840000000000000
        Width = 961.512431999999900000
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Width = 737.007910630000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'O.S. ( Ordens de Servi'#231'o ) em Aberto')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 22.677179999999990000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 22.677179999999990000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Espess.')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 22.677179999999990000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 22.677179999999990000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 22.677179999999990000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Entrega')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Top = 22.677179999999990000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299630000000000000
          Top = 22.677179999999990000
          Width = 264.566965750000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 910.866730000000000000
          Top = 22.677179999999990000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Vendedor')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 22.677179999999990000
          Width = 37.795104720000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 22.677179999999990000
          Width = 56.692754720000010000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'OS')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677179999999990000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 279.685220000000000000
        Width = 961.512431999999900000
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 11.338590000000010000
          Width = 249.448980000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total m'#178': [SUM(<frxDsOSs."M2Pedido">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 11.338590000000010000
          Width = 272.126160000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantidade de OSs: [COUNT(MasterData1)] ')
          ParentFont = False
        end
      end
    end
  end
  object QrOSs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pp.DataF DataPedido, pp.Cliente, pp.Vendedor,'
      'mi.Controle OS, mi.Texto, mi.Entrega, mi.Fluxo, mi.Classe,'
      'mi.Pedido, mi.EspesTxt, mi.CorTxt, mi.M2Pedido, mi.Observ,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLI,'
      'IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMEVEN,'
      'ag.Nome NOMEMP, pc.Nome NO_CONDICAOPG'
      'FROM mpvits mi'
      'LEFT JOIN mpp pp ON pp.Codigo=mi.Pedido'
      'LEFT JOIN artigosgrupos ag ON ag.Codigo=mi.MP'
      'LEFT JOIN entidades cl ON cl.Codigo=pp.Cliente'
      'LEFT JOIN entidades ve ON ve.Codigo=pp.Vendedor'
      'LEFT JOIN pediprzcab pc ON pc.Codigo=pp.CondicaoPg'
      'WHERE mi.Codigo=0'
      'ORDER BY mi.Entrega, pp.DataF')
    Left = 496
    Top = 9
    object QrOSsDataPedido: TDateField
      FieldName = 'DataPedido'
    end
    object QrOSsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrOSsVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrOSsOS: TIntegerField
      FieldName = 'OS'
      Required = True
    end
    object QrOSsTexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrOSsEntrega: TDateField
      FieldName = 'Entrega'
      Required = True
    end
    object QrOSsFluxo: TIntegerField
      FieldName = 'Fluxo'
      Required = True
    end
    object QrOSsClasse: TWideStringField
      FieldName = 'Classe'
      Size = 15
    end
    object QrOSsPedido: TIntegerField
      FieldName = 'Pedido'
      Required = True
    end
    object QrOSsEspesTxt: TWideStringField
      FieldName = 'EspesTxt'
      Size = 10
    end
    object QrOSsCorTxt: TWideStringField
      FieldName = 'CorTxt'
      Size = 30
    end
    object QrOSsM2Pedido: TFloatField
      FieldName = 'M2Pedido'
      Required = True
    end
    object QrOSsObserv: TWideMemoField
      FieldName = 'Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSsNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrOSsNOMEVEN: TWideStringField
      FieldName = 'NOMEVEN'
      Size = 100
    end
    object QrOSsNOMEMP: TWideStringField
      FieldName = 'NOMEMP'
      Size = 50
    end
    object QrOSsNO_CondicaoPG: TWideStringField
      FieldName = 'NO_CondicaoPG'
      Size = 50
    end
    object QrOSsPedidCli: TWideStringField
      FieldName = 'PedidCli'
      Size = 60
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 289
    Top = 77
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 317
    Top = 77
  end
  object QrVendedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece6="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 857
    Top = 73
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 885
    Top = 73
  end
  object QrMP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM artigosgrupos'
      'ORDER BY Nome')
    Left = 561
    Top = 17
    object QrMPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMP: TDataSource
    DataSet = QrMP
    Left = 561
    Top = 65
  end
  object QrTransp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 793
    Top = 65
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransp: TDataSource
    DataSet = QrTransp
    Left = 821
    Top = 65
  end
  object frxOSS_ABERT_001_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.407328206020000000
    ReportOptions.LastChange = 39949.407328206020000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  CabealhodeGrupo1.Visible := <VARF_VISI1>;'
      '  CabealhodeGrupo2.Visible := <VARF_VISI2>;'
      '  CabealhodeGrupo3.Visible := <VARF_VISI3>;'
      '  //'
      '  CabealhodeGrupo1.Condition := <VARF_COND1>;'
      '  CabealhodeGrupo2.Condition := <VARF_COND2>;'
      '  CabealhodeGrupo3.Condition := <VARF_COND3>;'
      '  //'
      '  GroupFooter3.Visible := <VARF_VISI3>;'
      '  GroupFooter2.Visible := <VARF_VISI2>;'
      '  GroupFooter1.Visible := <VARF_VISI1>;'
      '  //'
      '  MemoCG1.Memo.Text := <VARF_MEMO1>;'
      '  MemoCG2.Memo.Text := <VARF_MEMO2>;'
      '  MemoCG3.Memo.Text := <VARF_MEMO3>;'
      '  //'
      '  MemoGF1.Memo.Text := '#39'SUB-TOTAL '#39' + <VARF_MEMO1>;'
      '  MemoGF2.Memo.Text := '#39'SUB-TOTAL '#39' + <VARF_MEMO2>;'
      '  MemoGF3.Memo.Text := '#39'SUB-TOTAL '#39' + <VARF_MEMO3>;'
      'end.')
    OnGetValue = frxOSS_ABERT_001_01GetValue
    Left = 160
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsOSs
        DataSetName = 'frxDsOSs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader5: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 75.590590240000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Width = 1046.929810000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 1031.811690000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 18.897650000000000000
          Width = 725.669760000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ORDENS DE SERVI'#199'O EM ABERTO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748610000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 37.795300000000000000
          Width = 1031.811690000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 60.472479999999990000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 60.472479999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638220000000000000
          Top = 60.472479999999990000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Espess.')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 990.236860000000000000
          Top = 60.472480000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 60.472480000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 60.472480000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Entrega')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Top = 60.472479999999990000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 695.433520000000000000
          Top = 60.472479999999990000
          Width = 170.078740160000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 865.512370000000000000
          Top = 60.472480000000000000
          Width = 124.724458270000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Vendedor')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 60.472480000000000000
          Width = 49.133694720000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Top = 60.472479999999990000
          Width = 56.692754720000010000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'OS')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Top = 60.472479999999990000
          Width = 94.488188976377960000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'OC')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 279.685220000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsOSs
        DataSetName = 'frxDsOSs'
        RowCount = 0
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Entrega'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."Entrega"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'NOMEMP'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."NOMEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          DataField = 'Texto'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'CorTxt'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Classe'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOSs."Classe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638220000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'EspesTxt'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOSs."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 990.236860000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOSs."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 695.433520000000000000
          Width = 170.078681570000000000
          Height = 15.118110240000000000
          DataField = 'NOMECLI'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."NOMECLI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 865.512370000000000000
          Width = 124.724458270000000000
          Height = 15.118110240000000000
          DataField = 'NOMEVEN'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."NOMEVEN"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataField = 'Pedido'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOSs."Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Width = 56.692950000000010000
          Height = 15.118110240000000000
          DataField = 'OS'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOSs."OS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'DataPedido'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOSs."DataPedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Width = 94.488250000000000000
          Height = 15.118110240000000000
          DataField = 'PedidCli'
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOSs."PedidCli"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 548.031849999999900000
        Width = 1046.929810000000000000
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 491.338900000000000000
        Width = 1046.929810000000000000
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 11.338590000000010000
          Width = 249.448980000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total m'#178': [SUM(<frxDsOSs."M2Pedido">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 11.338590000000010000
          Width = 272.126160000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantidade de OSs: [COUNT(MasterData4)] ')
          ParentFont = False
        end
      end
      object CabealhoDeGrupo1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.000000000000000000
        Top = 154.960730000000000000
        Visible = False
        Width = 1046.929810000000000000
        Condition = 'frxDsOSs."NOMEMP"'
        object MemoCG1: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Width = 716.000000000000000000
          Height = 17.000000000000000000
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
      end
      object CabealhoDeGrupo2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.000000000000000000
        Top = 196.535560000000000000
        Visible = False
        Width = 1046.929810000000000000
        Condition = 'frxDsOSs."DataPedido"'
        object MemoCG2: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Width = 716.000000000000000000
          Height = 17.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
      end
      object CabealhoDeGrupo3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.000000000000000000
        Top = 238.110390000000000000
        Visible = False
        Width = 1046.929810000000000000
        Condition = 'frxDsOSs."Cliente"'
        object MemoCG3: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Width = 716.000000000000000000
          Height = 17.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 408.189240000000000000
        Width = 1046.929810000000000000
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 990.236860000000000000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = FmMPP.frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOSs."M2Pedido">)]')
          ParentFont = False
          WordWrap = False
        end
        object MemoGF1: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 982.677768270000000000
          Height = 15.118110240000000000
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 362.834880000000000000
        Width = 1046.929810000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 990.236860000000000000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = FmMPP.frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOSs."M2Pedido">)]')
          ParentFont = False
          WordWrap = False
        end
        object MemoGF2: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 982.677768270000000000
          Height = 15.118110240000000000
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 317.480520000000000000
        Width = 1046.929810000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 990.236860000000000000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 15.000000000000000000
          StretchMode = smMaxHeight
          DataSet = FmMPP.frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOSs."M2Pedido">)]')
          ParentFont = False
          WordWrap = False
        end
        object MemoGF3: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 982.677768270000000000
          Height = 15.118110240000000000
          DataSet = frxDsOSs
          DataSetName = 'frxDsOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object DsOSs: TDataSource
    DataSet = QrOSs
    Left = 497
    Top = 57
  end
  object QrSels: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pp.DataF DataPedido, pp.Cliente, pp.Vendedor, '
      'mi.Controle OS, mi.Texto, mi.Entrega, mi.Fluxo, mi.Classe, '
      'mi.Pedido, mi.EspesTxt, mi.CorTxt, mi.M2Pedido, mi.Observ, '
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLI,'
      'IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMEVEN,'
      'ag.Nome NOMEMP'
      'FROM mpvits mi'
      'LEFT JOIN mpp pp ON pp.Codigo=mi.Pedido '
      'LEFT JOIN artigosgrupos ag ON ag.Codigo=mi.MP '
      'LEFT JOIN entidades cl ON cl.Codigo=pp.Cliente '
      'LEFT JOIN entidades ve ON ve.Codigo=pp.Vendedor '
      'WHERE mi.Codigo=0'
      'ORDER BY mi.Entrega, pp.DataF')
    Left = 436
    Top = 9
    object QrSelsNO_CondicaoPG: TWideStringField
      FieldName = 'NO_CondicaoPG'
      Size = 50
    end
    object QrSelsDataPedido: TDateField
      FieldName = 'DataPedido'
    end
    object QrSelsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSelsVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrSelsOS: TIntegerField
      FieldName = 'OS'
      Required = True
    end
    object QrSelsTexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrSelsEntrega: TDateField
      FieldName = 'Entrega'
      Required = True
    end
    object QrSelsFluxo: TIntegerField
      FieldName = 'Fluxo'
      Required = True
    end
    object QrSelsClasse: TWideStringField
      FieldName = 'Classe'
      Size = 15
    end
    object QrSelsPedido: TIntegerField
      FieldName = 'Pedido'
      Required = True
    end
    object QrSelsEspesTxt: TWideStringField
      FieldName = 'EspesTxt'
      Size = 10
    end
    object QrSelsCorTxt: TWideStringField
      FieldName = 'CorTxt'
      Size = 30
    end
    object QrSelsM2Pedido: TFloatField
      FieldName = 'M2Pedido'
      Required = True
    end
    object QrSelsObserv: TWideMemoField
      FieldName = 'Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSelsNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrSelsNOMEVEN: TWideStringField
      FieldName = 'NOMEVEN'
      Size = 100
    end
    object QrSelsNOMEMP: TWideStringField
      FieldName = 'NOMEMP'
      Size = 50
    end
    object QrSelsPedidCli: TWideStringField
      FieldName = 'PedidCli'
      Size = 60
    end
  end
end
