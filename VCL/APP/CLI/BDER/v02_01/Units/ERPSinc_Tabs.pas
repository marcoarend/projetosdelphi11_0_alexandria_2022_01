unit ERPSinc_Tabs;
// ERPSinc - Custos e Resultados Operacionais
{ Colocar no MyListas:

Uses UnERPSinc_Tabs;


//
function TMyListas.CriaListaTabelas:
      ERPSinc_Tb.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
    ERPSinc_Tb.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:

    ERPSinc_Tb.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      ERPSinc_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      ERPSinc_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    ERPSinc_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  ERPSinc_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms,
  UnInternalConsts, dmkGeral, UnDmkEnums;

type
  TUnERPSinc_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  ERPSinc_Tabs: TUnERPSinc_Tabs;

implementation

const
  ERPSync_Prefix = 'ERPSync_';

function TUnERPSinc_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    //Linguas.AdTbLst(Lista, False, Lowercase('Controle'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(ERPSync_Prefix + 'VSPalletA'), '');
    //
    //MyLinguas.AdTbLst(Lista, False, Lowercase('???'), '_lst_sample');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnERPSinc_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
{
separador = "|"
}
  Result := True;
  //end else
{
  if Uppercase(Tabela) = Uppercase('CtrlGeral') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end;
}
end;

function TUnERPSinc_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
{
  if Uppercase(Tabela) = Uppercase('??') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"ND"');
    FListaSQL.Add('1|"Telefone"');
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades"|"Cadastro de pessoas f�sicas e jur�dicas (clientes| fornecedores| etc.)"');
    //FListaSQL.Add('""|""');
  end;
}
end;


function TUnERPSinc_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
var
  Qry: TmySQLQuery;
  Tabori: String;
begin
  Result := True;
  Qry: TmySQLQuery.Create(Dmod);
  try
    TabOri := Copy(Tabela, Length(ERPSync_Prefix));
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SHOW INDEX FROM ' + TabOri,
    'WHERE Key_name="PRIMARY" ',
    ''];
    while not Qry.Eof do
    begin
      New(FRIndices);
      FRIndices.Non_unique    := QrIdx.FieldByName('Non_unique').AsString;
      FRIndices.Key_name      := QrIdx.FieldByName('Key_name').AsString;
      FRIndices.Seq_in_index  := QrIdx.FieldByName('Seq_in_index').AsString
      FRIndices.Column_name   := QrIdx.FieldByName('Column_name').AsString;
      FLIndices.Add(FRIndices);
      //
      Qry.Next;
    end;
    //
{
  //end else
  if Uppercase(Tabela) = Uppercase('ERPSync_VSPalletA') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
  end else
}
  finally
    Qry.Free;
  end;
end;

function TUnERPSinc_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
var
  Qry: TmySQLQuery;
  Tabori: String;
begin
  Result := True;
  Qry: TmySQLQuery.Create(Dmod);
  try
    TabOri := Copy(Tabela, Length(ERPSync_Prefix));
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SHOW COLUMNS FROM ' + TabOri,
    ''];
    while not Qry.Eof do
    begin
      /
      New(FRCampos);
      FRCampos.Field      := ?;
      FRCampos.Tipo       := ?;
      FRCampos.Null       := ?;
      FRCampos.Key        := ?;
      FRCampos.Default    := ?;
      FRCampos.Extra      := ?;
      FLCampos.Add(FRCampos);
      //
      //
      Qry.Next;
    end;
    //
{
    //end else
    if Uppercase(Tabela) = Uppercase('CtrlGeral') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DmkNetPath';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
 }
       //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnERPSinc_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Versao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SoMaiusculas';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := 'F';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaperLef';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaperTop';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaperHei';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '2790';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaperWid';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '2160';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaperFcl';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dono';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNPJ';
      FRCampos.Tipo       := 'varchar(18)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Moeda';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := 'R$';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SenhasIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IdleMinutos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '10';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Vers�o das tabelas banc�rias
      New(FRCampos);
      FRCampos.Field      := 'VerBcoTabs';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ContraSenha';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnERPSinc_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  // TABELA : perfjan
  //
  // XXX-XXXXX-001 :: Pesquisa pela descri��o
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-001';
  FRJanelas.Nome      := 'FmPesqNome';
  FRJanelas.Descricao := 'Pesquisa pela descri��o';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-002 :: Seleciona ?
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-002';
  FRJanelas.Nome      := 'FmSelCod';
  FRJanelas.Descricao := 'Seleciona ?';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-003 :: Pesquisa e Seleciona pela Descri��o
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-003';
  FRJanelas.Nome      := 'FmPesqESel';
  FRJanelas.Descricao := 'Pesquisa e Seleciona pela Descri��o';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-004 :: Multi Sele��o
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-004';
  FRJanelas.Nome      := 'FmSelCods';
  FRJanelas.Descricao := 'Multi Sele��o';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-005 :: Altera��o Parcial de Texto
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-005';
  FRJanelas.Nome      := 'FmMudaTexto';
  FRJanelas.Descricao := 'Altera��o Parcial de Texto';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-006 :: Tipo de Conex�o
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-006';
  FRJanelas.Nome      := 'FmServidor';
  FRJanelas.Descricao := 'Tipo de Conex�o';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-007 :: Conex�o ao Servidor
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-007';
  FRJanelas.Nome      := 'FmServerConnect';
  FRJanelas.Descricao := 'Conex�o ao Servidor';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-008 :: Seleciona ?
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-008';
  FRJanelas.Nome      := 'FmSelStr';
  FRJanelas.Descricao := 'Seleciona ?';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-009 :: Reordena Itens
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-009';
  FRJanelas.Nome      := 'FmMyDBGridReorder';
  FRJanelas.Descricao := 'Reordena Itens';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-010 :: Ativa��o de Cadastro
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-010';
  FRJanelas.Nome      := 'FmGetValor';
  FRJanelas.Descricao := 'Ativa��o de Cadastro';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-011 :: Obtem Valor ?
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-011';
  FRJanelas.Nome      := 'FmGetValor';
  FRJanelas.Descricao := 'Obtem Valor ?';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-012 :: Mudar Texto
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-012';
  FRJanelas.Nome      := 'FmMudaTexto';
  FRJanelas.Descricao := 'Mudar Texto';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-013 :: Calcula Parcial 4 Valores
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-013';
  FRJanelas.Nome      := 'FmCalcAreaEQtd';
  FRJanelas.Descricao := 'Calcula Parcial 4 Valores';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-014 :: Mostra dados em Grade
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-014';
  FRJanelas.Nome      := 'FmGerlShowGrid';
  FRJanelas.Descricao := 'Mostra dados em Grade';
  FLJanelas.Add(FRJanelas);
  //
  // XXX-XXXXX-999 :: ???????????????????????????
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-999';
  FRJanelas.Nome      := 'Fm?';
  FRJanelas.Descricao := '????????????????????';
  FLJanelas.Add(FRJanelas);
  //
  // @MF-SELEC-001 :: Sele��o de Filial
  New(FRJanelas);
  FRJanelas.ID        := '@MF-SELEC-001';
  FRJanelas.Nome      := 'FmMasterSelFilial';
  FRJanelas.Descricao := 'Sele��o de Filial';
  FLJanelas.Add(FRJanelas);
  //
  // ATR-IBUTO-001 :: Cadastro de Atributo
  New(FRJanelas);
  FRJanelas.ID        := 'ATR-IBUTO-001';
  FRJanelas.Nome      := 'FmAtrCad';
  FRJanelas.Descricao := 'Cadastro de Atributo';
  FLJanelas.Add(FRJanelas);
  //
  // ATR-IBUTO-002 :: Cadastro de Item de Atributo
  New(FRJanelas);
  FRJanelas.ID        := 'ATR-IBUTO-002';
  FRJanelas.Nome      := 'FmAtrIts';
  FRJanelas.Descricao := 'Cadastro de Item de Atributo';
  FLJanelas.Add(FRJanelas);
  //
  // ATR-IBUTO-003 :: Defini��o de Atributo
  New(FRJanelas);
  FRJanelas.ID        := 'ATR-IBUTO-003';
  FRJanelas.Nome      := 'FmAtrDef';
  FRJanelas.Descricao := 'Defini��o de Atributo';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-LISTA-001 :: Cadastros de ...
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-LISTA-001';
  FRJanelas.Nome      := 'FmCadLista';
  FRJanelas.Descricao := 'Cadastro de ...';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-LISTA-002 :: Cadastros de ...
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-LISTA-002';
  FRJanelas.Nome      := 'FmCadItemGrupo';
  FRJanelas.Descricao := 'Cadastro de ...';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-LISTA-003 :: Cadastros de ...
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-LISTA-003';
  FRJanelas.Nome      := 'FmCadastroSimples';
  FRJanelas.Descricao := 'Cadastro de ...';
  FLJanelas.Add(FRJanelas);
  //
  // Cashier :: Janela principal do CASHIER
  New(FRJanelas);
  FRJanelas.ID        := 'Cashier';
  FRJanelas.Nome      := 'FmPrincipal';
  FRJanelas.Descricao := 'Janela principal do CASHIER';
  FLJanelas.Add(FRJanelas);
  //
  // FER-BACKP-003 :: Backup 3 (Beta)
  New(FRJanelas);
  FRJanelas.ID        := 'FER-BACKP-003';
  FRJanelas.Nome      := 'FmBackup3';
  FRJanelas.Descricao := 'Backup do banco de dados';
  FLJanelas.Add(FRJanelas);
  //
  // FER-BACKP-101 :: Restaura Banco de Dados (1)
  New(FRJanelas);
  FRJanelas.ID        := 'FER-BACKP-101';
  FRJanelas.Nome      := 'FmRestaura';
  FRJanelas.Descricao := 'Restaura Banco de Dados (1)';
  FLJanelas.Add(FRJanelas);
  //
  // FER-IADOS-001 :: Cadastro de Feriados
  New(FRJanelas);
  FRJanelas.ID        := 'FER-IADOS-001';
  FRJanelas.Nome      := 'FmFeriados';
  FRJanelas.Descricao := 'Cadastro de Feriados';
  FLJanelas.Add(FRJanelas);
  //
  // FER-OPCAO-001 :: Op��es gerais do aplicativo
  New(FRJanelas);
  FRJanelas.ID        := 'FER-OPCAO-001';
  FRJanelas.Nome      := 'FmOpcoes';
  FRJanelas.Descricao := 'Op��es gerais do aplicativo';
  FLJanelas.Add(FRJanelas);
  //
  // FER-OPCAO-004 :: Par�metros Espec�ficos de Filiais
  New(FRJanelas);
  FRJanelas.ID        := 'FER-OPCAO-004';
  FRJanelas.Nome      := 'FmOpcoesFili';
  FRJanelas.Descricao := 'Par�metros Espec�ficos de Filiais';
  FLJanelas.Add(FRJanelas);
  //
  // IMP-QRCOD-001 :: Impress�o de QR Code
  New(FRJanelas);
  FRJanelas.ID        := 'IMP-QRCOD-001';
  FRJanelas.Nome      := 'FmQrCodeImp';
  FRJanelas.Descricao := 'Impress�o de QR Code';
  FLJanelas.Add(FRJanelas);
  //
  // IMP-MATRI-001 :: Impress�o Matricial (Direta)
  New(FRJanelas);
  FRJanelas.ID        := 'IMP-MATRI-001';
  FRJanelas.Nome      := 'FmDotPrint';
  FRJanelas.Descricao := 'Impress�o Matricial (Direta)';
  FLJanelas.Add(FRJanelas);
  //
  // IMP-DUPLC-001 :: Impress�o de Duplicata [Modelo 1]
  New(FRJanelas);
  FRJanelas.ID        := 'IMP-DUPLC-001';
  FRJanelas.Nome      := 'FmDuplicata1';
  FRJanelas.Descricao := 'Impress�o de Duplicata [Modelo 1]';
  FLJanelas.Add(FRJanelas);
  //
  // IMP-OBSER-001 :: Observa��es de Impress�o
  New(FRJanelas);
  FRJanelas.ID        := 'IMP-OBSER-001';
  FRJanelas.Nome      := 'FmImpObs';
  FRJanelas.Descricao := 'Observa��es de Impress�o';
  FLJanelas.Add(FRJanelas);
  //
  // IMP-PROMS-001 :: Impress�o de Promiss�ria
  New(FRJanelas);
  FRJanelas.ID        := 'IMP-PROMS-001';
  FRJanelas.Nome      := 'FmPromissoria';
  FRJanelas.Descricao := 'Impress�o de Promiss�ria';
  FLJanelas.Add(FRJanelas);
  //
  // ZIP-ZFORG-001 :: WinZip
  New(FRJanelas);
  FRJanelas.ID        := 'ZIP-ZFORG-001';
  FRJanelas.Nome      := 'FmZForge';
  FRJanelas.Descricao := 'WinZip';
  FLJanelas.Add(FRJanelas);
  //
  // STP-CODIG-001 :: Intervalos de C�digo (Unit�rio)
  New(FRJanelas);
  FRJanelas.ID        := 'STP-CODIG-001';
  FRJanelas.Nome      := 'FmZStepCodUni';
  FRJanelas.Descricao := 'Intervalos de C�digo (Unit�rio)';
  FLJanelas.Add(FRJanelas);
  //
  // GER-SELEC-001 :: Lista de itens
  New(FRJanelas);
  FRJanelas.ID        := 'GER-SELEC-001';
  FRJanelas.Nome      := 'FmSelOnStringGrid';
  FRJanelas.Descricao := 'Lista de itens';
  FLJanelas.Add(FRJanelas);
  //
  // HOR-VERAO-001 :: Hor�rio de Ver�o
  New(FRJanelas);
  FRJanelas.ID        := 'HOR-VERAO-001';
  FRJanelas.Nome      := 'FmHorVerao';
  FRJanelas.Descricao := 'Hor�rio de Ver�o';
  FLJanelas.Add(FRJanelas);
  //
  // SKN-SELEC-001 :: Sele��o do Tema do Aplicativo
  New(FRJanelas);
  FRJanelas.ID        := 'SKN-SELEC-001';
  FRJanelas.Nome      := 'FmLinkRankSkin';
  FRJanelas.Descricao := 'Sele��o do Tema do Aplicativo';
  FLJanelas.Add(FRJanelas);
  //
  // ARQ-GRAVA-001 :: Salvar Arquivo
  New(FRJanelas);
  FRJanelas.ID        := 'ARQ-GRAVA-001';
  FRJanelas.Nome      := 'FmCfgExpFile';
  FRJanelas.Descricao := 'Salvar Arquivo';
  FLJanelas.Add(FRJanelas);
  //
  // COD-BARRA-000 :: Leitura Centralizada de C�digo de Barras
  New(FRJanelas);
  FRJanelas.ID        := 'COD-BARRA-000';
  FRJanelas.Nome      := 'FmCodBarraRead';
  FRJanelas.Descricao := 'Leitura Centralizada de C�digo de Barras';
  FLJanelas.Add(FRJanelas);
  //
  // INF-INCST-001 :: Inconsist�ncia de Informa��o
  New(FRJanelas);
  FRJanelas.ID        := 'INF-INCST-001';
  FRJanelas.Nome      := 'FmInfoIncCab';
  FRJanelas.Descricao := 'Inconsist�ncia de Informa��o';
  FLJanelas.Add(FRJanelas);
  //
  // INF-INCST-002 :: Item de Inconsist�ncia de Informa��o
  New(FRJanelas);
  FRJanelas.ID        := 'INF-INCST-002';
  FRJanelas.Nome      := 'FmInfoIncIts';
  FRJanelas.Descricao := 'Item de Inconsist�ncia de Informa��o';
  FLJanelas.Add(FRJanelas);
  //
  // INF-INCST-003 :: Impress�es de Inconsist�ncia de Informa��o
  New(FRJanelas);
  FRJanelas.ID        := 'INF-INCST-003';
  FRJanelas.Nome      := 'FmInfoIncImp';
  FRJanelas.Descricao := 'Impress�es de Inconsist�ncia de Informa��o';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
