object FmMPEstagios: TFmMPEstagios
  Left = 510
  Top = 211
  Caption = 'VEN-COURO-203 :: Est'#225'gios de Processamento'
  ClientHeight = 433
  ClientWidth = 488
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 488
    Height = 277
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object dmkDBGridDAC1: TdmkDBGridDAC
      Left = 0
      Top = 0
      Width = 488
      Height = 277
      SQLFieldsToChange.Strings = (
        'Nome')
      SQLIndexesOnUpdate.Strings = (
        'Codigo')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ordem'
          Title.Caption = 'Est'#225'gio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 335
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 17
          Visible = True
        end>
      Color = clWindow
      DataSource = DsMPEstagios
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGridDAC1CellClick
      SQLTable = 'MPEstagios'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ordem'
          Title.Caption = 'Est'#225'gio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 335
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 17
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 488
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 440
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 392
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 339
        Height = 32
        Caption = 'Est'#225'gios de Processamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 339
        Height = 32
        Caption = 'Est'#225'gios de Processamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 339
        Height = 32
        Caption = 'Est'#225'gios de Processamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 325
    Width = 488
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 484
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 369
    Width = 488
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 484
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 340
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtAcao: TBitBtn
        Tag = 294
        Left = 18
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&A'#231#227'o'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAcaoClick
      end
    end
  end
  object DsMPEstagios: TDataSource
    DataSet = QrMPEstagios
    Left = 168
    Top = 176
  end
  object QrMPEstagios: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM mpestagios'
      'ORDER BY Ordem')
    Left = 144
    Top = 176
    object QrMPEstagiosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpestagios.Codigo'
    end
    object QrMPEstagiosNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'mpestagios.Nome'
      Size = 50
    end
    object QrMPEstagiosLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'mpestagios.Lk'
    end
    object QrMPEstagiosDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'mpestagios.DataCad'
    end
    object QrMPEstagiosDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'mpestagios.DataAlt'
    end
    object QrMPEstagiosUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'mpestagios.UserCad'
    end
    object QrMPEstagiosUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'mpestagios.UserAlt'
    end
    object QrMPEstagiosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'mpestagios.AlterWeb'
    end
    object QrMPEstagiosAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'mpestagios.Ativo'
      MaxValue = 1
    end
    object QrMPEstagiosOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object PMAcao: TPopupMenu
    Left = 104
    Top = 249
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
end
