unit PQEPed;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DBCtrls, Db, (*DBTables,*) Grids, DBGrids, ExtCtrls,
  dmkGeral, mySQLDbTables, Variants, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmPQEPed = class(TForm)
    QrFornece: TmySQLQuery;
    QrPedidos: TmySQLQuery;
    DsFornece: TDataSource;
    DsPedidos: TDataSource;
    QrPedidosCodigo: TIntegerField;
    QrPedidosIts: TmySQLQuery;
    DsPedidosIts: TDataSource;
    QrPedidosItsCodigo: TIntegerField;
    QrPedidosItsControle: TIntegerField;
    QrPedidosItsConta: TIntegerField;
    QrPedidosItsInsumo: TIntegerField;
    QrPedidosItsVolumes: TIntegerField;
    QrPedidosItsPesoVB: TFloatField;
    QrPedidosItsPesoVL: TFloatField;
    QrPedidosItsValorItem: TFloatField;
    QrPedidosItsIPI: TFloatField;
    QrPedidosItsTotalCusto: TFloatField;
    QrPedidosItsTotalPeso: TFloatField;
    QrPedidosItsPreco: TFloatField;
    QrPedidosItsICMS: TFloatField;
    QrPedidosItsEntrada: TFloatField;
    QrPedidosItsPrazo: TWideStringField;
    QrPedidosItscfin: TFloatField;
    QrPedidosItsNOMEINSUMO: TWideStringField;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    PainelDados: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    CBFornecedor: TDBLookupComboBox;
    CBPedido: TDBLookupComboBox;
    CBEncerrados: TCheckBox;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTIDADE: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBEncerradosClick(Sender: TObject);
    procedure CBFornecedorClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure QrPedidosAfterScroll(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmPQEPed: TFmPQEPed;

implementation

uses UnMyObjects, PQE, Module;

{$R *.DFM}

procedure TFmPQEPed.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQEPed.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrFornece.Params[0].AsInteger := 2;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
end;

procedure TFmPQEPed.CBEncerradosClick(Sender: TObject);
begin
  QrFornece.Close;
  QrPedidos.Close;
  if CBEncerrados.Checked then QrFornece.Params[0].AsInteger := 3
  else QrFornece.Params[0].AsInteger := 2;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
end;

procedure TFmPQEPed.CBFornecedorClick(Sender: TObject);
begin
  QrPedidos.Close;
  if CBFornecedor.KeyValue = NULL then CBFornecedor.KeyValue := 0;
  QrPedidos.Params[0].AsInteger := CBFornecedor.KeyValue;
  if CBEncerrados.Checked then QrPedidos.Params[1].AsInteger := 3
  else QrPedidos.Params[1].AsInteger := 2;
  UnDmkDAC_PF.AbreQuery(QrPedidos, Dmod.MyDB);
end;

procedure TFmPQEPed.BtConfirmaClick(Sender: TObject);
var
  Pedido : Integer;
begin
  if CBPedido.KeyValue = NULL then Pedido := 0 else Pedido := CBPedido.KeyValue;
  if Pedido = 0 then
  begin
    ShowMessage('Defina o Pedido');
    CBPedido.SetFocus;
    Exit;
  end;
  Hide;
  FmPQE.InsereRegCondicionado1(Pedido);
  Close;
end;

procedure TFmPQEPed.QrPedidosAfterScroll(DataSet: TDataSet);
begin
  QrPedidosIts.Close;
  if CBPedido.KeyValue <> NULL then
  begin
    QrPedidosIts.Params[0].AsInteger := QrPedidosCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrPedidosIts, Dmod.MyDB);
  end;
end;

procedure TFmPQEPed.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQEPed.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
