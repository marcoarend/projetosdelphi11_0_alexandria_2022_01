object FmPQB2Edit: TFmPQB2Edit
  Left = 426
  Top = 332
  Caption = 'QUI-BALAN-003 :: Itens de Balan'#231'o de PQ'
  ClientHeight = 360
  ClientWidth = 416
  Color = clBtnFace
  Constraints.MaxHeight = 535
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 416
    Height = 204
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object LaInsumo: TLabel
      Left = 12
      Top = 56
      Width = 37
      Height = 13
      Caption = 'Insumo:'
    end
    object Label4: TLabel
      Left = 12
      Top = 136
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label10: TLabel
      Left = 100
      Top = 136
      Width = 67
      Height = 13
      Caption = 'Custo unit'#225'rio:'
    end
    object Label6: TLabel
      Left = 196
      Top = 136
      Width = 53
      Height = 13
      Caption = 'Custo total:'
    end
    object Label2: TLabel
      Left = 12
      Top = 96
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object LaEmpresa: TLabel
      Left = 12
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object EdPecas: TdmkEdit
      Left = 12
      Top = 152
      Width = 85
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdPecasExit
    end
    object EdPreco: TdmkEdit
      Left = 100
      Top = 152
      Width = 93
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdPrecoExit
    end
    object EdVTota: TdmkEdit
      Left = 196
      Top = 152
      Width = 109
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdVTotaExit
    end
    object EdInsumo: TdmkEditCB
      Left = 12
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdInsumoChange
      OnEnter = EdInsumoEnter
      OnExit = EdInsumoExit
      DBLookupComboBox = CBInsumo
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBInsumo: TdmkDBLookupComboBox
      Left = 68
      Top = 72
      Width = 333
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPQ
      TabOrder = 1
      dmkEditCB = EdInsumo
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCliente: TdmkEditCB
      Left = 12
      Top = 112
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdClienteChange
      OnEnter = EdClienteEnter
      OnExit = EdClienteExit
      DBLookupComboBox = CBCliente
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 68
      Top = 112
      Width = 333
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCI
      TabOrder = 3
      dmkEditCB = EdCliente
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object CkContinuar: TCheckBox
      Left = 12
      Top = 180
      Width = 113
      Height = 17
      Caption = 'Continuar inserindo'
      Checked = True
      State = cbChecked
      TabOrder = 7
    end
    object EdEmpresa: TdmkEditCB
      Left = 12
      Top = 32
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Empresa'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 68
      Top = 32
      Width = 333
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 9
      TabStop = False
      dmkEditCB = EdEmpresa
      QryCampo = 'Empresa'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 416
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 368
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 320
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 288
        Height = 32
        Caption = 'Itens de Balan'#231'o de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 288
        Height = 32
        Caption = 'Itens de Balan'#231'o de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 288
        Height = 32
        Caption = 'Itens de Balan'#231'o de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 252
    Width = 416
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 412
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 296
    Width = 416
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 412
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 268
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 280
    Top = 48
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 308
    Top = 48
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END Nome'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 280
    Top = 76
    object QrCINome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.artigosgrupos.Nome'
      Size = 32
    end
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.artigosgrupos.Codigo'
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 308
    Top = 76
  end
end
