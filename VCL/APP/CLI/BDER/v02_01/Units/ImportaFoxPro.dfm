object FmImportaFoxPro: TFmImportaFoxPro
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: Importa Visual Fox Pro'
  ClientHeight = 866
  ClientWidth = 1067
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 876
    Height = 710
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 876
      Height = 432
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 876
        Height = 101
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label64: TLabel
          Left = 8
          Top = 4
          Width = 135
          Height = 13
          Caption = 'Pasta que cont'#233'm os dados:'
        end
        object SbDir: TSpeedButton
          Left = 152
          Top = 19
          Width = 23
          Height = 22
          Caption = '...'
          OnClick = SbDirClick
        end
        object LaTabela: TLabel
          Left = 8
          Top = 64
          Width = 9
          Height = 13
          Caption = '...'
        end
        object SpeedButton1: TSpeedButton
          Left = 152
          Top = 40
          Width = 23
          Height = 23
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object EdDir: TdmkEdit
          Left = 8
          Top = 20
          Width = 141
          Height = 21
          MaxLength = 255
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = 'C:\ciw80'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 'C:\ciw80'
          ValWarn = False
        end
        object PB1: TProgressBar
          Left = 8
          Top = 80
          Width = 165
          Height = 17
          TabOrder = 1
        end
        object GroupBox3: TGroupBox
          Left = 196
          Top = 4
          Width = 213
          Height = 61
          Caption = ' Registros: '
          Enabled = False
          TabOrder = 2
          object Label5: TLabel
            Left = 12
            Top = 16
            Width = 27
            Height = 13
            Caption = 'Total:'
          end
          object Label9: TLabel
            Left = 112
            Top = 16
            Width = 50
            Height = 13
            Caption = 'Importado:'
          end
          object LaTamTot: TLabel
            Left = 48
            Top = 16
            Width = 16
            Height = 13
            Caption = 'X 1'
          end
          object LaTamRec: TLabel
            Left = 168
            Top = 16
            Width = 16
            Height = 13
            Caption = 'X 1'
          end
          object EdTamTot: TdmkEdit
            Left = 12
            Top = 32
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTamRec: TdmkEdit
            Left = 112
            Top = 32
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox2: TGroupBox
          Left = 411
          Top = 4
          Width = 217
          Height = 61
          Caption = ' Tempo: '
          Enabled = False
          TabOrder = 3
          object Label6: TLabel
            Left = 12
            Top = 16
            Width = 27
            Height = 13
            Caption = 'Total:'
          end
          object Label7: TLabel
            Left = 80
            Top = 16
            Width = 62
            Height = 13
            Caption = 'Transcorrido:'
          end
          object Label8: TLabel
            Left = 148
            Top = 16
            Width = 46
            Height = 13
            Caption = 'Restante:'
          end
          object EdTmpTot: TdmkEdit
            Left = 12
            Top = 32
            Width = 64
            Height = 21
            TabOrder = 0
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTmpRun: TdmkEdit
            Left = 80
            Top = 32
            Width = 64
            Height = 21
            TabOrder = 1
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTmpFal: TdmkEdit
            Left = 148
            Top = 32
            Width = 64
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox4: TGroupBox
          Left = 631
          Top = 4
          Width = 89
          Height = 61
          Caption = ' Percentual: '
          Enabled = False
          TabOrder = 4
          object Label10: TLabel
            Left = 12
            Top = 16
            Width = 61
            Height = 13
            Caption = '% Importado:'
          end
          object EdPerRec: TdmkEdit
            Left = 12
            Top = 32
            Width = 64
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object ProgressBar1: TProgressBar
          Left = 732
          Top = 28
          Width = 121
          Height = 17
          TabOrder = 5
        end
        object EdBak: TdmkEdit
          Left = 8
          Top = 40
          Width = 141
          Height = 21
          MaxLength = 255
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = 'C:\ciw80\Back'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 'C:\ciw80\Back'
          ValWarn = False
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 101
        Width = 228
        Height = 242
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 228
          Height = 192
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label18: TLabel
            Left = 28
            Top = 168
            Width = 94
            Height = 13
            Caption = 'Entidades incluidas:'
          end
          object GroupBox1: TGroupBox
            Left = 0
            Top = 32
            Width = 228
            Height = 123
            Align = alTop
            Caption = ' C'#243'digos iniciais: '
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 28
              Width = 35
              Height = 13
              Caption = 'Cliente:'
            end
            object Label2: TLabel
              Left = 8
              Top = 52
              Width = 49
              Height = 13
              Caption = 'Vendedor:'
            end
            object Label3: TLabel
              Left = 8
              Top = 76
              Width = 69
              Height = 13
              Caption = 'Transportador:'
            end
            object Label4: TLabel
              Left = 8
              Top = 100
              Width = 57
              Height = 13
              Caption = 'Fornecedor:'
            end
            object EdIniClien: TdmkEdit
              Left = 88
              Top = 24
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '1'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
            end
            object EdIniVende: TdmkEdit
              Left = 88
              Top = 48
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '3301'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 3301
              ValWarn = False
            end
            object EdIniTrans: TdmkEdit
              Left = 88
              Top = 72
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '3401'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 3401
              ValWarn = False
            end
            object EdIniForne: TdmkEdit
              Left = 88
              Top = 96
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '4001'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 4001
              ValWarn = False
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 228
            Height = 32
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Label11: TLabel
              Left = 4
              Top = 8
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label22: TLabel
              Left = 88
              Top = 8
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object EdEmpresa_11: TdmkEdit
              Left = 52
              Top = 4
              Width = 29
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '-11'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = -11
              ValWarn = False
            end
            object EdEmpresa_12: TdmkEdit
              Left = 136
              Top = 4
              Width = 29
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '-12'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = -12
              ValWarn = False
            end
          end
          object EdInclEnts: TdmkEdit
            Left = 128
            Top = 164
            Width = 41
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
      object DBGrid1: TDBGrid
        Left = 228
        Top = 101
        Width = 648
        Height = 242
        Align = alClient
        DataSource = DataSource1
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object Memo2: TMemo
        Left = 0
        Top = 343
        Width = 876
        Height = 89
        Align = alBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 432
      Width = 625
      Height = 278
      ActivePage = TabSheet43
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Entidades'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl2: TPageControl
          Left = 0
          Top = 0
          Width = 617
          Height = 250
          ActivePage = TabSheet2
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 615
          ExplicitHeight = 185
          object TabSheet2: TTabSheet
            Caption = 'Clientes'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid3: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsClientes
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Vendedores'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid2: TDBGrid
              Left = 0
              Top = 0
              Width = 783
              Height = 271
              Align = alClient
              DataSource = DsVendedor
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet4: TTabSheet
            Caption = 'Transportadoras'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid4: TDBGrid
              Left = 0
              Top = 0
              Width = 783
              Height = 271
              Align = alClient
              DataSource = DsTranspor
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet5: TTabSheet
            Caption = 'Fornecedores'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid5: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsForneced
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
      object TabSheet12: TTabSheet
        Caption = 'Produtos'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl5: TPageControl
          Left = 0
          Top = 0
          Width = 617
          Height = 250
          ActivePage = TabSheet27
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 615
          ExplicitHeight = 185
          object TabSheet27: TTabSheet
            Caption = 'GruProd (Tip.  Grup. de prod.)'
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid23: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsGruProd
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet28: TTabSheet
            Caption = 'TipoGrup (GraGru2)'
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid24: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsTipoGrup
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet29: TTabSheet
            Caption = 'F'#225'bricas'
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid25: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsFabrica
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet30: TTabSheet
            Caption = 'Unidades'
            ImageIndex = 7
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid26: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsUnidade
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet31: TTabSheet
            Caption = 'NCMs'
            ImageIndex = 8
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid27: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsClasFisc
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet32: TTabSheet
            Caption = 'Produtos (GraGru1)'
            ImageIndex = 9
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid28: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsProdutos
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
      object TabSheet20: TTabSheet
        Caption = 'Financeiro'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl3: TPageControl
          Left = 0
          Top = 0
          Width = 617
          Height = 250
          ActivePage = TabSheet13
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 615
          ExplicitHeight = 185
          object TabSheet13: TTabSheet
            Caption = 'Carteiras (FormaPag)'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid12: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsFormaPag
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet14: TTabSheet
            Caption = ' Sub-grupos ( CenCusto)'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid13: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsCenCusto
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet15: TTabSheet
            Caption = 'Contas (ItemCust)'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid14: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsItemCust
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet16: TTabSheet
            Caption = 'Pagar'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid15: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsPagar
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet17: TTabSheet
            Caption = 'Receber'
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid16: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsReceber
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
      object TabSheet21: TTabSheet
        Caption = 'Pedido'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl4: TPageControl
          Left = 0
          Top = 0
          Width = 617
          Height = 250
          ActivePage = TabSheet23
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 615
          ExplicitHeight = 185
          object TabSheet22: TTabSheet
            Caption = ' Pedidos '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid17: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsPedido
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet23: TTabSheet
            Caption = 'Itens dos pedidos'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid18: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsItensPed
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
      object TabSheet18: TTabSheet
        Caption = 'NF'
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl6: TPageControl
          Left = 0
          Top = 0
          Width = 617
          Height = 250
          ActivePage = TabSheet7
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 615
          ExplicitHeight = 185
          object TabSheet6: TTabSheet
            Caption = 'NFs Emitidas'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid6: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsNotaFisc
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet7: TTabSheet
            Caption = 'NFs Recebidas'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid7: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsNota_Ent
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet8: TTabSheet
            Caption = ' Itens das NFs Recebidas'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid8: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsItens_Ent
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
      object TabSheet9: TTabSheet
        Caption = 'Empresa'
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid9: TDBGrid
          Left = 0
          Top = 0
          Width = 615
          Height = 60
          Align = alClient
          DataSource = DsEmpresa
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object Panel9: TPanel
          Left = 0
          Top = 60
          Width = 615
          Height = 125
          Align = alBottom
          ParentBackground = False
          TabOrder = 1
          object Label12: TLabel
            Left = 4
            Top = 4
            Width = 57
            Height = 13
            Caption = 'Logradouro:'
          end
          object Label13: TLabel
            Left = 80
            Top = 4
            Width = 99
            Height = 13
            Caption = 'Nome do logradouro:'
          end
          object Label14: TLabel
            Left = 264
            Top = 4
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
          end
          object Label15: TLabel
            Left = 312
            Top = 4
            Width = 67
            Height = 13
            Caption = 'Complemento:'
          end
          object Label16: TLabel
            Left = 396
            Top = 4
            Width = 30
            Height = 13
            Caption = 'Bairro:'
          end
          object Label17: TLabel
            Left = 544
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Cidade:'
          end
          object EdLgr_Txt: TdmkEdit
            Left = 28
            Top = 20
            Width = 49
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLgr_Cod: TdmkEdit
            Left = 4
            Top = 20
            Width = 21
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdRua: TdmkEdit
            Left = 80
            Top = 20
            Width = 181
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdNumero: TdmkEdit
            Left = 264
            Top = 20
            Width = 45
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCompl: TdmkEdit
            Left = 312
            Top = 20
            Width = 80
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdBairro: TdmkEdit
            Left = 396
            Top = 20
            Width = 145
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCidade_Cod: TdmkEdit
            Left = 544
            Top = 20
            Width = 61
            Height = 21
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '4113700'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '4113700'
            ValWarn = False
          end
          object CkAtualiza: TCheckBox
            Left = 8
            Top = 48
            Width = 117
            Height = 17
            Caption = 'Atualiza empresa.'
            TabOrder = 7
          end
        end
      end
      object TabSheet10: TTabSheet
        Caption = ' SINTEGRA '
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl7: TPageControl
          Left = 0
          Top = 0
          Width = 617
          Height = 250
          ActivePage = TabSheet25
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 615
          ExplicitHeight = 185
          object TabSheet11: TTabSheet
            Caption = '10'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid10: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR10
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet19: TTabSheet
            Caption = '11'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid11: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR11
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet24: TTabSheet
            Caption = '50'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid19: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR50
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet25: TTabSheet
            Caption = '51'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid20: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR51
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet26: TTabSheet
            Caption = '53'
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid21: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR53
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet33: TTabSheet
            Caption = '54'
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid22: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR54
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet34: TTabSheet
            Caption = '70'
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid29: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR70
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet35: TTabSheet
            Caption = '71'
            ImageIndex = 7
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid30: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR71
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet36: TTabSheet
            Caption = '74'
            ImageIndex = 8
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid31: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR74
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet37: TTabSheet
            Caption = '75'
            ImageIndex = 9
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid32: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR75
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet38: TTabSheet
            Caption = '85'
            ImageIndex = 10
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid33: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR85
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet39: TTabSheet
            Caption = '86'
            ImageIndex = 11
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid34: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR86
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet40: TTabSheet
            Caption = '88'
            ImageIndex = 12
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid35: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR88
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet41: TTabSheet
            Caption = '90'
            ImageIndex = 13
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid36: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 157
              Align = alClient
              DataSource = DsR90
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
      object TabSheet42: TTabSheet
        Caption = 'NatOper'
        ImageIndex = 7
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid37: TDBGrid
          Left = 0
          Top = 0
          Width = 615
          Height = 185
          Align = alClient
          DataSource = DsR51
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object TabSheet43: TTabSheet
        Caption = ' Lista de Pre'#231'os '
        ImageIndex = 8
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel10: TPanel
          Left = 0
          Top = 202
          Width = 617
          Height = 48
          Align = alBottom
          ParentBackground = False
          TabOrder = 0
          object Label23: TLabel
            Left = 104
            Top = 4
            Width = 175
            Height = 13
            Caption = 'Arquivo Excel (Come'#231'a na c'#233'lula A?)'
          end
          object SbLista: TSpeedButton
            Left = 540
            Top = 19
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SbListaClick
          end
          object Label24: TLabel
            Left = 568
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Tabela:'
          end
          object BtLista: TBitBtn
            Tag = 14
            Left = 8
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Importa'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtListaClick
          end
          object EdLista: TdmkEdit
            Left = 104
            Top = 20
            Width = 433
            Height = 21
            MaxLength = 255
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'C:\_MLArend\Clientes\Curtidora Igap'#243'\Tabela_Precos_201108.xlsx'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'C:\_MLArend\Clientes\Curtidora Igap'#243'\Tabela_Precos_201108.xlsx'
            ValWarn = False
          end
          object EdTabela: TdmkEdit
            Left = 568
            Top = 20
            Width = 41
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object PageControl8: TPageControl
          Left = 0
          Top = 0
          Width = 617
          Height = 202
          ActivePage = TabSheet44
          Align = alClient
          TabOrder = 1
          object TabSheet44: TTabSheet
            Caption = ' Excel '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeLista: TStringGrid
              Left = 0
              Top = 0
              Width = 609
              Height = 81
              Align = alTop
              DefaultColWidth = 48
              DefaultRowHeight = 17
              TabOrder = 0
            end
          end
          object TabSheet45: TTabSheet
            Caption = ' Dados'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid38: TDBGrid
              Left = 0
              Top = 0
              Width = 607
              Height = 109
              Align = alClient
              DataSource = DataSource1
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
    end
    object Memo1: TMemo
      Left = 625
      Top = 432
      Width = 251
      Height = 278
      Align = alRight
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
  end
  object Panel5: TPanel
    Left = 876
    Top = 48
    Width = 191
    Height = 710
    Align = alRight
    Caption = 'Panel5'
    TabOrder = 1
    object GradeTabelas: TdmkDBGrid
      Left = 1
      Top = 145
      Width = 189
      Height = 564
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Ok'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Tabela'
          Width = 136
          Visible = True
        end>
      Color = clWindow
      DataSource = DsTabelas
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = GradeTabelasCellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Ok'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Tabela'
          Width = 136
          Visible = True
        end>
    end
    object CLLista: TCheckListBox
      Left = 1
      Top = 1
      Width = 189
      Height = 144
      Align = alTop
      ItemHeight = 13
      Items.Strings = (
        'Entidade'
        'Produtos'
        'Financeiro'
        'Pedidos'
        'NFs Emitidas'
        'NFs Recebidas'
        'SINTEGRA'
        'CFOPs'
        'Pedidos x NFs emitidas'
        'NFs Entrada')
      TabOrder = 1
      OnClick = CLListaClick
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1067
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1019
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 971
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 273
        Height = 32
        Caption = 'Importa Visual Fox Pro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 273
        Height = 32
        Caption = 'Importa Visual Fox Pro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 273
        Height = 32
        Caption = 'Importa Visual Fox Pro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 758
    Width = 1067
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel11: TPanel
      Left = 2
      Top = 15
      Width = 1063
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 802
    Width = 1067
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel12: TPanel
      Left = 2
      Top = 15
      Width = 1063
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label19: TLabel
        Left = 592
        Top = 4
        Width = 56
        Height = 13
        Caption = 'N'#227'o achou:'
      end
      object Label20: TLabel
        Left = 660
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Achou:'
      end
      object Label21: TLabel
        Left = 724
        Top = 4
        Width = 57
        Height = 13
        Caption = 'Mais de um:'
      end
      object PnSaiDesis: TPanel
        Left = 919
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 90
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 104
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 196
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtNenhumClick
      end
      object BtParar: TBitBtn
        Tag = 126
        Left = 288
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Parar'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtPararClick
      end
      object Button1: TButton
        Left = 420
        Top = 8
        Width = 165
        Height = 37
        Caption = 'Pesquisa Produto'
        TabOrder = 5
        OnClick = Button1Click
      end
      object EdNao: TdmkEdit
        Left = 592
        Top = 20
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSim: TdmkEdit
        Left = 660
        Top = 20
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdVar: TdmkEdit
        Left = 724
        Top = 20
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object BtImprime: TBitBtn
        Tag = 128
        Left = 796
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 9
        OnClick = BtImprimeClick
      end
    end
  end
  object DsTabelas: TDataSource
    DataSet = TbTabelas
    Left = 104
    Top = 12
  end
  object DsClientes: TDataSource
    Left = 336
    Top = 152
  end
  object DsVendedor: TDataSource
    Left = 392
    Top = 152
  end
  object TbTabelas: TmySQLTable
    Database = Dmod.MyDB
    TableName = '_cod_ant_'
    Left = 132
    Top = 12
    object TbTabelasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = '_cod_ant_.Codigo'
    end
    object TbTabelasNome: TWideStringField
      FieldName = 'Nome'
      Origin = '_cod_ant_.Nome'
      Size = 255
    end
    object TbTabelasAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = '_cod_ant_.Ativo'
      MaxValue = 1
    end
  end
  object QrAnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM entidades'
      'WHERE Antigo=:P0')
    Left = 16
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsTranspor: TDataSource
    Left = 448
    Top = 152
  end
  object DsForneced: TDataSource
    Left = 504
    Top = 152
  end
  object DataSource1: TDataSource
    Left = 72
    Top = 12
  end
  object DsNatOper: TDataSource
    Left = 840
    Top = 180
  end
  object DsGruProd: TDataSource
    Left = 560
    Top = 152
  end
  object DsProdutos: TDataSource
    Left = 840
    Top = 152
  end
  object DsTipoGrup: TDataSource
    Left = 616
    Top = 152
  end
  object DsFabrica: TDataSource
    Left = 672
    Top = 152
  end
  object DsUnidade: TDataSource
    Left = 728
    Top = 152
  end
  object DsClasFisc: TDataSource
    Left = 784
    Top = 152
  end
  object QrClasFisc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM clasfisc'
      'ORDER BY Codigo')
    Left = 260
    Top = 12
    object QrClasFiscCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClasFiscNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrClasFiscNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
  end
  object DsFormaPag: TDataSource
    Left = 336
    Top = 180
  end
  object DsCenCusto: TDataSource
    Left = 392
    Top = 180
  end
  object DsItemCust: TDataSource
    Left = 448
    Top = 180
  end
  object DsPagar: TDataSource
    Left = 504
    Top = 180
  end
  object DsReceber: TDataSource
    Left = 560
    Top = 180
  end
  object DsPedido: TDataSource
    Left = 672
    Top = 180
  end
  object DsItensPed: TDataSource
    Left = 728
    Top = 180
  end
  object DsNotaFisc: TDataSource
    Left = 336
    Top = 208
  end
  object QrEntidade: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades'
      'WHERE Antigo=:P0')
    Left = 288
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntidadeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object PMImprime: TPopupMenu
    Left = 748
    Top = 660
    object Lanamentosrfosdecontasapagar1: TMenuItem
      Caption = 'Lan'#231'amentos '#243'rf'#227'os de contas a &Pagar'
      OnClick = Lanamentosrfosdecontasapagar1Click
    end
    object LanamentosrfosdecontasaReceber1: TMenuItem
      Caption = 'Lan'#231'amentos '#243'rf'#227'os de contas a &Receber'
      OnClick = LanamentosrfosdecontasaReceber1Click
    end
  end
  object QrLctOrf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI,'
      'cta.Nome NO_GENERO, sgr.Nome NO_SUBGRUPO, car.Nome NO_CAR,'
      'pg.*'
      'FROM _pagar pg'
      'LEFT JOIN contas cta ON cta.Codigo=ITEMCUST'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN carteiras car ON car.Codigo=pg.FORMAPGTO'
      'LEFT JOIN entidades cli ON cli.Antigo=CONCAT("C",pg.CLIENTE)'
      'WHERE sgr.Codigo <> pg.CENTCUST'
      'ORDER BY ITEMCUST, CENTCUST, DATA '
      '')
    Left = 172
    Top = 12
    object QrLctOrfDOCUMENTO: TWideStringField
      FieldName = 'DOCUMENTO'
      Size = 8
    end
    object QrLctOrfDATA: TDateField
      FieldName = 'DATA'
    end
    object QrLctOrfCLIENTE: TWideStringField
      FieldName = 'CLIENTE'
      Size = 6
    end
    object QrLctOrfNOTAFISCAL: TWideStringField
      FieldName = 'NOTAFISCAL'
      Size = 6
    end
    object QrLctOrfVENCIMENTO: TDateField
      FieldName = 'VENCIMENTO'
    end
    object QrLctOrfVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrLctOrfPAGAMENTO: TDateField
      FieldName = 'PAGAMENTO'
    end
    object QrLctOrfVALORPGTO: TFloatField
      FieldName = 'VALORPGTO'
    end
    object QrLctOrfMULTA: TFloatField
      FieldName = 'MULTA'
    end
    object QrLctOrfJUROS: TFloatField
      FieldName = 'JUROS'
    end
    object QrLctOrfMORA: TIntegerField
      FieldName = 'MORA'
    end
    object QrLctOrfFORMAPGTO: TWideStringField
      FieldName = 'FORMAPGTO'
      Size = 2
    end
    object QrLctOrfCENTCUST: TWideStringField
      FieldName = 'CENTCUST'
      Size = 6
    end
    object QrLctOrfITEMCUST: TWideStringField
      FieldName = 'ITEMCUST'
      Size = 6
    end
    object QrLctOrfPEDIDO: TWideStringField
      FieldName = 'PEDIDO'
      Size = 6
    end
    object QrLctOrfBANCO: TWideStringField
      FieldName = 'BANCO'
      Size = 3
    end
    object QrLctOrfCAIXA: TIntegerField
      FieldName = 'CAIXA'
    end
    object QrLctOrfHISTORICO: TWideStringField
      FieldName = 'HISTORICO'
    end
    object QrLctOrfOBS: TWideMemoField
      FieldName = 'OBS'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLctOrfDESCONTO: TIntegerField
      FieldName = 'DESCONTO'
    end
    object QrLctOrfNO_GENERO: TWideStringField
      FieldName = 'NO_GENERO'
      Size = 50
    end
    object QrLctOrfNO_SUBGRUPO: TWideStringField
      FieldName = 'NO_SUBGRUPO'
      Size = 50
    end
    object QrLctOrfNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrLctOrfNO_CAR: TWideStringField
      FieldName = 'NO_CAR'
      Size = 100
    end
  end
  object frxLctOrf: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.647167257000000000
    ReportOptions.LastChange = 39949.647167257000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxLctOrfGetValue
    Left = 200
    Top = 12
    Datasets = <
      item
      end
      item
        DataSet = frxDsLctOrf
        DataSetName = 'frxDsLctOrf'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 15.000000000000000000
      LargeDesignHeight = True
      object PageHeader3: TfrxPageHeader
        Height = 57.826800940000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        OnBeforePrint = 'PageHeader3OnBeforePrint'
        object Shape2: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo34: TfrxMemoView
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo36: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagin1: TfrxMemoView
          Left = 839.055660000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 687.874460000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Top = 45.354360000000000000
          Width = 45.354330708661420000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 45.354360000000000000
          Top = 45.354360000000000000
          Width = 41.574803150000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 86.929190000000000000
          Top = 45.354360000000000000
          Width = 200.315063150000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 287.244280000000000000
          Top = 45.354360000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NF')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 325.039580000000000000
          Top = 45.354360000000000000
          Width = 41.574803150000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 366.614410000000000000
          Top = 45.354360000000000000
          Width = 60.472440940000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 427.086890000000000000
          Top = 45.354360000000000000
          Width = 41.574803150000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pagto')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 468.661720000000000000
          Top = 45.354360000000000000
          Width = 60.472440940000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Val. Pago')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 529.134200000000000000
          Top = 45.354360000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 566.929500000000000000
          Top = 45.354360000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 604.724800000000000000
          Top = 45.354360000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Mora')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 680.315400000000000000
          Top = 45.354360000000000000
          Width = 98.267780000000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forma')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 778.583180000000000000
          Top = 45.354360000000000000
          Width = 41.574803150000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 820.158010000000000000
          Top = 45.354360000000000000
          Width = 22.677180000000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Bco')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 842.835190000000000000
          Top = 45.354360000000000000
          Width = 18.897650000000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cx')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 861.732840000000000000
          Top = 45.354360000000000000
          Width = 128.504020000000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hist'#243'rico / Observa'#231#245'es')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 642.520100000000000000
          Top = 45.354360000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 336.378170000000000000
        Width = 990.236860000000000000
        OnBeforePrint = 'PageFooter1OnBeforePrint'
        object Memo39: TfrxMemoView
          Width = 1009.134510000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 12.472440940000000000
        Top = 211.653680000000000000
        Width = 990.236860000000000000
        DataSet = frxDsLctOrf
        DataSetName = 'frxDsLctOrf'
        RowCount = 0
        object frxDsLctOrfDOCUMENTO: TfrxMemoView
          Width = 45.354330708661420000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'DOCUMENTO'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLctOrf."DOCUMENTO"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfDATA: TfrxMemoView
          Left = 45.354360000000000000
          Width = 41.574803150000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'DATA'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLctOrf."DATA"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfCLIENTE: TfrxMemoView
          Left = 86.929190000000000000
          Width = 200.315063150000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLctOrf."CLIENTE"] - [frxDsLctOrf."NO_CLI"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfNOTAFISCAL: TfrxMemoView
          Left = 287.244280000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'NOTAFISCAL'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLctOrf."NOTAFISCAL"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfVENCIMENTO: TfrxMemoView
          Left = 325.039580000000000000
          Width = 41.574803150000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'VENCIMENTO'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLctOrf."VENCIMENTO"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfVALOR: TfrxMemoView
          Left = 366.614410000000000000
          Width = 60.472440940000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'VALOR'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLctOrf."VALOR"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfPAGAMENTO: TfrxMemoView
          Left = 427.086890000000000000
          Width = 41.574803150000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'PAGAMENTO'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLctOrf."PAGAMENTO"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfVALORPGTO: TfrxMemoView
          Left = 468.661720000000000000
          Width = 60.472440940000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'VALORPGTO'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLctOrf."VALORPGTO"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfMULTA: TfrxMemoView
          Left = 529.134200000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'MULTA'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLctOrf."MULTA"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfJUROS: TfrxMemoView
          Left = 566.929500000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'JUROS'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLctOrf."JUROS"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfMORA: TfrxMemoView
          Left = 604.724800000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'MORA'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLctOrf."MORA"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfFORMAPGTO: TfrxMemoView
          Left = 680.315400000000000000
          Width = 98.267780000000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLctOrf."FORMAPGTO"] - [frxDsLctOrf."NO_CAR"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfPEDIDO: TfrxMemoView
          Left = 778.583180000000000000
          Width = 41.574803150000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'PEDIDO'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLctOrf."PEDIDO"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfBANCO: TfrxMemoView
          Left = 820.158010000000000000
          Width = 22.677180000000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'BANCO'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLctOrf."BANCO"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfCAIXA: TfrxMemoView
          Left = 842.835190000000000000
          Width = 18.897650000000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'CAIXA'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLctOrf."CAIXA"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfOBS: TfrxMemoView
          Left = 861.732840000000000000
          Width = 128.504020000000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLctOrf."HISTORICO"][frxDsLctOrf."OBS"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsLctOrfDESCONTO: TfrxMemoView
          Left = 642.520100000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          ShowHint = False
          DataField = 'DESCONTO'
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLctOrf."DESCONTO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 136.063080000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsLctOrf."CENTCUST"'
        object Memo19: TfrxMemoView
          Width = 990.236860000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CENTCUST: [frxDsLctOrf."CENTCUST"] - [frxDsLctOrf."NO_SUBGRUPO"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 1.889763779527560000
        Top = 272.126160000000000000
        Width = 990.236860000000000000
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 173.858380000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsLctOrf."ITEMCUST"'
        object Memo20: TfrxMemoView
          Left = 75.590600000000000000
          Width = 914.646260000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDsLctOrf
          DataSetName = 'frxDsLctOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'ITEMCUST: [frxDsLctOrf."ITEMCUST"] - [frxDsLctOrf."NO_GENERO"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 1.889763779527560000
        Top = 245.669450000000000000
        Width = 990.236860000000000000
      end
    end
  end
  object frxDsLctOrf: TfrxDBDataset
    UserName = 'frxDsLctOrf'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DOCUMENTO=DOCUMENTO'
      'DATA=DATA'
      'CLIENTE=CLIENTE'
      'NOTAFISCAL=NOTAFISCAL'
      'VENCIMENTO=VENCIMENTO'
      'VALOR=VALOR'
      'PAGAMENTO=PAGAMENTO'
      'VALORPGTO=VALORPGTO'
      'MULTA=MULTA'
      'JUROS=JUROS'
      'MORA=MORA'
      'FORMAPGTO=FORMAPGTO'
      'CENTCUST=CENTCUST'
      'ITEMCUST=ITEMCUST'
      'PEDIDO=PEDIDO'
      'BANCO=BANCO'
      'CAIXA=CAIXA'
      'HISTORICO=HISTORICO'
      'OBS=OBS'
      'DESCONTO=DESCONTO'
      'NO_GENERO=NO_GENERO'
      'NO_SUBGRUPO=NO_SUBGRUPO'
      'NO_CLI=NO_CLI'
      'NO_CTA=NO_CTA')
    DataSet = QrLctOrf
    BCDToCurrency = False
    Left = 228
    Top = 12
  end
  object DsNota_Ent: TDataSource
    Left = 448
    Top = 208
  end
  object DsItens_Ent: TDataSource
    Left = 504
    Top = 208
  end
  object DsEmpresa: TDataSource
    Left = 336
    Top = 236
  end
  object QrMin: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(DATA) Data'
      'FROM _pedido'
      'WHERE Data > 1'
      '')
    Left = 748
    Top = 8
    object QrMinData: TDateField
      FieldName = 'Data'
    end
  end
  object QrPedido: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _pedido'
      'WHERE Data=:P0')
    Left = 776
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPedidoNUMERO: TWideStringField
      FieldName = 'NUMERO'
      Size = 6
    end
    object QrPedidoDATA: TDateField
      FieldName = 'DATA'
    end
    object QrPedidoTIPO: TIntegerField
      FieldName = 'TIPO'
    end
    object QrPedidoENTSAI: TIntegerField
      FieldName = 'ENTSAI'
    end
    object QrPedidoCLIENTE: TWideStringField
      FieldName = 'CLIENTE'
      Size = 6
    end
    object QrPedidoCONDPAG: TWideStringField
      FieldName = 'CONDPAG'
      Size = 47
    end
    object QrPedidoCENTROCUST: TWideStringField
      FieldName = 'CENTROCUST'
      Size = 6
    end
    object QrPedidoITEMCUST: TWideStringField
      FieldName = 'ITEMCUST'
      Size = 6
    end
    object QrPedidoFORMAPGTO: TWideStringField
      FieldName = 'FORMAPGTO'
      Size = 2
    end
    object QrPedidoVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrPedidoNUMNOTA: TWideStringField
      FieldName = 'NUMNOTA'
      Size = 6
    end
    object QrPedidoVLRNOTA: TFloatField
      FieldName = 'VLRNOTA'
    end
    object QrPedidoVLRICMS: TFloatField
      FieldName = 'VLRICMS'
    end
    object QrPedidoBANCO: TWideStringField
      FieldName = 'BANCO'
      Size = 3
    end
    object QrPedidoCP: TIntegerField
      FieldName = 'CP'
    end
    object QrPedidoCOMISSAO: TWideStringField
      FieldName = 'COMISSAO'
      Size = 2
    end
    object QrPedidoOBS: TWideMemoField
      FieldName = 'OBS'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPedidoOS: TWideStringField
      FieldName = 'OS'
      Size = 5
    end
    object QrPedidoAGRUPAR: TIntegerField
      FieldName = 'AGRUPAR'
    end
    object QrPedidoINSCOM: TIntegerField
      FieldName = 'INSCOM'
    end
    object QrPedidoVENDEDOR: TWideStringField
      FieldName = 'VENDEDOR'
      Size = 3
    end
    object QrPedidoJ07_UKEY: TWideStringField
      FieldName = 'J07_UKEY'
      Size = 60
    end
  end
  object QrNotaFisc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 832
    Top = 8
    object QrNotaFiscNUMERO: TWideStringField
      FieldName = 'NUMERO'
      Size = 6
    end
    object QrNotaFiscORDSERV: TWideStringField
      FieldName = 'ORDSERV'
      Size = 6
    end
    object QrNotaFiscSITUACAO: TIntegerField
      FieldName = 'SITUACAO'
    end
    object QrNotaFiscNATOPER: TWideStringField
      FieldName = 'NATOPER'
      Size = 6
    end
    object QrNotaFiscEMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrNotaFiscSAIDA: TDateField
      FieldName = 'SAIDA'
    end
    object QrNotaFiscHORA: TWideStringField
      FieldName = 'HORA'
      Size = 5
    end
    object QrNotaFiscBASEICMS: TFloatField
      FieldName = 'BASEICMS'
    end
    object QrNotaFiscVLRICMS: TFloatField
      FieldName = 'VLRICMS'
    end
    object QrNotaFiscBCICMSSUB: TFloatField
      FieldName = 'BCICMSSUB'
    end
    object QrNotaFiscVLRICMSSUB: TFloatField
      FieldName = 'VLRICMSSUB'
    end
    object QrNotaFiscTOTALPROD: TFloatField
      FieldName = 'TOTALPROD'
    end
    object QrNotaFiscVLRFRETE: TFloatField
      FieldName = 'VLRFRETE'
    end
    object QrNotaFiscVLRSEGURO: TFloatField
      FieldName = 'VLRSEGURO'
    end
    object QrNotaFiscOUTRAS: TFloatField
      FieldName = 'OUTRAS'
    end
    object QrNotaFiscIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrNotaFiscTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object QrNotaFiscTRANSPORTA: TWideStringField
      FieldName = 'TRANSPORTA'
      Size = 3
    end
    object QrNotaFiscFRETE: TIntegerField
      FieldName = 'FRETE'
    end
    object QrNotaFiscPLACAS: TWideStringField
      FieldName = 'PLACAS'
      Size = 8
    end
    object QrNotaFiscQTDADE: TIntegerField
      FieldName = 'QTDADE'
    end
    object QrNotaFiscESPECIE: TWideStringField
      FieldName = 'ESPECIE'
    end
    object QrNotaFiscMARCA: TWideStringField
      FieldName = 'MARCA'
    end
    object QrNotaFiscNUMPROD: TWideStringField
      FieldName = 'NUMPROD'
      Size = 25
    end
    object QrNotaFiscPESOBRUT: TFloatField
      FieldName = 'PESOBRUT'
    end
    object QrNotaFiscPESOLIQ: TFloatField
      FieldName = 'PESOLIQ'
    end
    object QrNotaFiscOBS: TWideMemoField
      FieldName = 'OBS'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNotaFiscCORPO: TWideMemoField
      FieldName = 'CORPO'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNotaFiscTIPODESC: TIntegerField
      FieldName = 'TIPODESC'
    end
    object QrNotaFiscNOTADESC: TFloatField
      FieldName = 'NOTADESC'
    end
    object QrNotaFiscJ10UKEY: TWideStringField
      FieldName = 'J10UKEY'
    end
    object QrNotaFiscCR: TWideStringField
      FieldName = 'CR'
      Size = 1
    end
  end
  object DsR10: TDataSource
    Left = 336
    Top = 264
  end
  object QrEnti: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, '
      'IF(Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENTI,'
      'IF(Tipo=0, ent.Fantasia, ent.Apelido) N2_ENTI,'
      'IF(Tipo=0, CNPJ, CPF) CNPJ_CPF,'
      'IF(Tipo=0, IE, RG) IE_RG,'
      'IF(Tipo=0, loe.Nome, poe.Nome) NO_Lgr,'
      'IF(Tipo=0, ERua, PRua) Rua,'
      'IF(Tipo=0, ENumero, PNumero) + 0.000 Numero,'
      'IF(Tipo=0, ECompl, PCompl) Compl,'
      'IF(Tipo=0, EBairro, PBairro) Bairro,'
      'IF(Tipo=0, ECidade, PCidade) xCidade,'
      'IF(Tipo=0, ECodMunici, PCodMunici) cCidade,'
      'IF(Tipo=0, euf.Nome, puf.Nome) NO_UF,'
      'IF(Tipo=0, ECEP, PCEP) CEP,'
      'IF(Tipo=0, ECodiPais, PCodiPais) cPais,'
      'IF(Tipo=0, EPais, PPais) xPais,'
      'IF(Tipo=0, ETe1, PTe1) Te1,'
      'IF(Tipo=0, SUFRAMA, "") ISUF'
      'FROM entidades ent'
      'LEFT JOIN ListaLograd loe ON loe.Codigo=ELograd'
      'LEFT JOIN ListaLograd poe ON poe.Codigo=PLograd'
      'LEFT JOIN UFs euf ON euf.Codigo=EUF'
      'LEFT JOIN UFs puf ON puf.Codigo=PUF'
      'WHERE ent.Codigo=:P0')
    Left = 860
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntiNO_ENTI: TWideStringField
      FieldName = 'NO_ENTI'
      Size = 100
    end
    object QrEntiN2_ENTI: TWideStringField
      FieldName = 'N2_ENTI'
      Size = 60
    end
    object QrEntiCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntiIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEntiNO_Lgr: TWideStringField
      FieldName = 'NO_Lgr'
      Size = 10
    end
    object QrEntiRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrEntiNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrEntiCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrEntiBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrEntixCidade: TWideStringField
      FieldName = 'xCidade'
      Size = 25
    end
    object QrEnticCidade: TLargeintField
      FieldName = 'cCidade'
      Required = True
    end
    object QrEntiNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Size = 2
    end
    object QrEntiCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrEnticPais: TLargeintField
      FieldName = 'cPais'
      Required = True
    end
    object QrEntixPais: TWideStringField
      FieldName = 'xPais'
    end
    object QrEntiTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrEntiISUF: TWideStringField
      FieldName = 'ISUF'
      Size = 9
    end
  end
  object QrItensPed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _itensped'
      'WHERE PEDIDO=:P0'
      'ORDER BY ORDEM')
    Left = 804
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItensPedPEDIDO: TWideStringField
      FieldName = 'PEDIDO'
      Size = 6
    end
    object QrItensPedORDEM: TIntegerField
      FieldName = 'ORDEM'
    end
    object QrItensPedPRODUTO: TWideStringField
      FieldName = 'PRODUTO'
      Size = 6
    end
    object QrItensPedCODPESQ: TWideStringField
      FieldName = 'CODPESQ'
      Size = 5
    end
    object QrItensPedMEDIDA: TWideStringField
      FieldName = 'MEDIDA'
      Size = 3
    end
    object QrItensPedPECAS: TIntegerField
      FieldName = 'PECAS'
    end
    object QrItensPedQTDADE: TFloatField
      FieldName = 'QTDADE'
    end
    object QrItensPedVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrItensPedTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object QrItensPedDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 35
    end
  end
  object QrNota_Ent: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _nota_ent'
      'WHERE EMISSAO=:P0')
    Left = 888
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNota_EntNUMERO: TWideStringField
      FieldName = 'NUMERO'
      Size = 6
    end
    object QrNota_EntCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrNota_EntINSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrNota_EntCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 5
    end
    object QrNota_EntRAZAO: TWideStringField
      FieldName = 'RAZAO'
      Size = 60
    end
    object QrNota_EntENDERECO: TWideStringField
      FieldName = 'ENDERECO'
      Size = 60
    end
    object QrNota_EntBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 40
    end
    object QrNota_EntCEP: TWideStringField
      FieldName = 'CEP'
      Size = 8
    end
    object QrNota_EntMUNICIO: TWideStringField
      FieldName = 'MUNICIO'
      Size = 50
    end
    object QrNota_EntFONE: TWideStringField
      FieldName = 'FONE'
      Size = 14
    end
    object QrNota_EntUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrNota_EntEMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrNota_EntBASEICMS: TFloatField
      FieldName = 'BASEICMS'
    end
    object QrNota_EntVALORICMS: TFloatField
      FieldName = 'VALORICMS'
    end
    object QrNota_EntBASESUB: TFloatField
      FieldName = 'BASESUB'
    end
    object QrNota_EntVALORSUB: TFloatField
      FieldName = 'VALORSUB'
    end
    object QrNota_EntVALORPROD: TFloatField
      FieldName = 'VALORPROD'
    end
    object QrNota_EntFRETE: TFloatField
      FieldName = 'FRETE'
    end
    object QrNota_EntSEGURO: TFloatField
      FieldName = 'SEGURO'
    end
    object QrNota_EntOUTRAS: TFloatField
      FieldName = 'OUTRAS'
    end
    object QrNota_EntIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrNota_EntTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object QrNota_EntQTDADE: TIntegerField
      FieldName = 'QTDADE'
    end
    object QrNota_EntESPECIE: TWideStringField
      FieldName = 'ESPECIE'
      Size = 10
    end
    object QrNota_EntMARCA: TWideStringField
      FieldName = 'MARCA'
      Size = 10
    end
    object QrNota_EntNUMMARC: TWideStringField
      FieldName = 'NUMMARC'
      Size = 12
    end
    object QrNota_EntPESOBRUTO: TFloatField
      FieldName = 'PESOBRUTO'
    end
    object QrNota_EntPESOLIQUID: TFloatField
      FieldName = 'PESOLIQUID'
    end
    object QrNota_EntDATACONTAB: TDateField
      FieldName = 'DATACONTAB'
    end
    object QrNota_EntSITUACAO: TIntegerField
      FieldName = 'SITUACAO'
    end
    object QrNota_EntOBSTIT: TWideMemoField
      FieldName = 'OBSTIT'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNota_EntCREDICMS: TIntegerField
      FieldName = 'CREDICMS'
    end
    object QrNota_EntCREDIPI: TIntegerField
      FieldName = 'CREDIPI'
    end
    object QrNota_EntFRETE_CONT: TIntegerField
      FieldName = 'FRETE_CONT'
    end
  end
  object QrEntiDoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM entidades'
      'WHERE CNPJ=:P0'
      'OR CPF=:P1')
    Left = 720
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEntiDocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrItens_Ent: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _itens_ent'
      'WHERE NUMERO=:P0'
      'AND CNPJ=:P1')
    Left = 916
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrItens_EntNUMERO: TWideStringField
      FieldName = 'NUMERO'
      Size = 6
    end
    object QrItens_EntITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object QrItens_EntCODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 10
    end
    object QrItens_EntDESCPROD: TWideStringField
      FieldName = 'DESCPROD'
      Size = 60
    end
    object QrItens_EntSITTRIB: TWideStringField
      FieldName = 'SITTRIB'
      Size = 3
    end
    object QrItens_EntUNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 3
    end
    object QrItens_EntQTDADE: TFloatField
      FieldName = 'QTDADE'
    end
    object QrItens_EntVALORUNIT: TFloatField
      FieldName = 'VALORUNIT'
    end
    object QrItens_EntVALORTOT: TFloatField
      FieldName = 'VALORTOT'
    end
    object QrItens_EntICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrItens_EntIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrItens_EntVALORIPI: TFloatField
      FieldName = 'VALORIPI'
    end
    object QrItens_EntCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
  end
  object QrIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _itens_ent')
    Left = 380
    Top = 676
    object QrItsNUMERO: TWideStringField
      FieldName = 'NUMERO'
      Size = 6
    end
    object QrItsITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object QrItsCODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 10
    end
    object QrItsDESCPROD: TWideStringField
      FieldName = 'DESCPROD'
      Size = 60
    end
    object QrItsSITTRIB: TWideStringField
      FieldName = 'SITTRIB'
      Size = 3
    end
    object QrItsUNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 3
    end
    object QrItsQTDADE: TFloatField
      FieldName = 'QTDADE'
    end
    object QrItsVALORUNIT: TFloatField
      FieldName = 'VALORUNIT'
    end
    object QrItsVALORTOT: TFloatField
      FieldName = 'VALORTOT'
    end
    object QrItsICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrItsIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrItsVALORIPI: TFloatField
      FieldName = 'VALORIPI'
    end
    object QrItsCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSel: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM GraGru1'
      'WHERE Nome=:P0'
      'AND Referencia=:P1'
      ''
      ''
      '')
    Left = 408
    Top = 676
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object DsR11: TDataSource
    Left = 392
    Top = 264
  end
  object DsR50: TDataSource
    Left = 448
    Top = 264
  end
  object DsR51: TDataSource
    Left = 504
    Top = 264
  end
  object DsR53: TDataSource
    Left = 560
    Top = 264
  end
  object DsR54: TDataSource
    Left = 616
    Top = 264
  end
  object DsR85: TDataSource
    Left = 336
    Top = 320
  end
  object DsR70: TDataSource
    Left = 336
    Top = 292
  end
  object DsR71: TDataSource
    Left = 392
    Top = 292
  end
  object DsR74: TDataSource
    Left = 448
    Top = 292
  end
  object DsR75: TDataSource
    Left = 504
    Top = 292
  end
  object DsR86: TDataSource
    Left = 392
    Top = 320
  end
  object DsR88: TDataSource
    Left = 448
    Top = 320
  end
  object DsR90: TDataSource
    Left = 504
    Top = 320
  end
  object QrGGX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1'
      'FROM gragru1'
      'WHERE Nivel1 < -900000'
      'AND Referencia=:P0'
      'AND Nome=:P1')
    Left = 944
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGGXNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object QrMinX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MIN(Nivel1) Nivel1'
      'FROM gragru1'
      'WHERE Nivel1 < -900000')
    Left = 972
    Top = 8
    object QrMinXNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
  end
  object PMLista: TPopupMenu
    Left = 44
    Top = 616
    object Carrega1: TMenuItem
      Caption = '&Carrega'
      OnClick = Carrega1Click
    end
  end
  object QrGG1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1 '
      'FROM gragru1'
      'WHERE Referencia=""')
    Left = 204
    Top = 388
    object QrGG1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
end
