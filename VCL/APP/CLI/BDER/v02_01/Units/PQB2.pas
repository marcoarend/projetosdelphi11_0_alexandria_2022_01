unit PQB2;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, Grids, DBGrids, Menus, mySQLDbTables,
  ComCtrls, Variants, dmkGeral, UnDmkProcFunc, dmkLabel, dmkImage, UnDmkEnums,
  dmkDBGridZTO, frxClass, frxDBSet, dmkDBGrid, UnProjGroup_Vars, dmkEdit,
  UnEmpresas;

type
  TFmPQB2 = class(TForm)
    PainelDados: TPanel;
    DsBalancos: TDataSource;
    DsBalancosIts: TDataSource;
    DsArtigo: TDataSource;
    PainelDados1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    PainelDados2: TPanel;
    Progress1: TProgressBar;
    Panel1: TPanel;
    Panel2: TPanel;
    PnRegistros: TPanel;
    Panel4: TPanel;
    PnTempo: TPanel;
    DBCheckBox1: TDBCheckBox;
    QrBalancos: TmySQLQuery;
    QrBalancosIts: TmySQLQuery;
    QrArtigo: TmySQLQuery;
    QrAtualiza: TmySQLQuery;
    QrMax: TmySQLQuery;
    QrBalancosPeriodo: TIntegerField;
    QrBalancosEstqQ: TFloatField;
    QrBalancosEstqV: TFloatField;
    QrBalancosDataCad: TDateField;
    QrBalancosDataAlt: TDateField;
    QrBalancosUserCad: TIntegerField;
    QrBalancosUserAlt: TIntegerField;
    QrBalancosConfirmado: TWideStringField;
    QrBalancosLk: TIntegerField;
    QrSoma: TmySQLQuery;
    QrBalancosPeriodo2: TWideStringField;
    QrArtigoCodigo: TIntegerField;
    QrArtigoNome: TWideStringField;
    QrSomaEstqQ: TFloatField;
    QrSomaEstqV: TFloatField;
    QrMaxPeriodo: TIntegerField;
    QrProdutos: TmySQLQuery;
    QrBalancosItsDataX: TDateField;
    QrBalancosItsTipo: TSmallintField;
    QrBalancosItsCliOrig: TIntegerField;
    QrBalancosItsCliDest: TIntegerField;
    QrBalancosItsInsumo: TIntegerField;
    QrBalancosItsPeso: TFloatField;
    QrBalancosItsValor: TFloatField;
    QrBalancosItsOrigemCodi: TIntegerField;
    QrBalancosItsOrigemCtrl: TIntegerField;
    QrBalancosItsNOMEPQ: TWideStringField;
    QrBalancosItsGrupoQuimico: TIntegerField;
    QrBalancosItsNOMEGRUPO: TWideStringField;
    QrAtualizaPQ: TIntegerField;
    QrAtualizaCI: TIntegerField;
    QrProdutosPQ: TIntegerField;
    QrProdutosCI: TIntegerField;
    QrProdutosPeso: TFloatField;
    QrProdutosValor: TFloatField;
    QrPQCli: TmySQLQuery;
    QrPQCliInsumo: TIntegerField;
    QrPQCliCliDest: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel7: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtResgata: TBitBtn;
    GBConfirma: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtTrava: TBitBtn;
    BtRefresh: TBitBtn;
    BtIncluiIts: TBitBtn;
    BtAlteraIts: TBitBtn;
    BtExcluiIts: TBitBtn;
    BtPrivativo: TBitBtn;
    BtInicial: TBitBtn;
    PMImprime: TPopupMenu;
    EsteBalano1: TMenuItem;
    Outrasimpressoes1: TMenuItem;
    DBGCI: TdmkDBGridZTO;
    QrCliDest: TmySQLQuery;
    DsCliDest: TDataSource;
    QrCliDestCodigo: TIntegerField;
    QrCliDestNO_ENT: TWideStringField;
    frxDsBalancosIts: TfrxDBDataset;
    frxQUI_BALAN_001_01: TfrxReport;
    QrBalancosItsCUSTOKG: TFloatField;
    BtGeraDif: TBitBtn;
    QrMoviPQ: TmySQLQuery;
    QrMoviPQInsumo: TIntegerField;
    QrMoviPQNomePQ: TWideStringField;
    QrMoviPQNomeFO: TWideStringField;
    QrMoviPQNomeCI: TWideStringField;
    QrMoviPQNomeSE: TWideStringField;
    QrMoviPQInnPes: TFloatField;
    QrMoviPQInnVal: TFloatField;
    QrMoviPQOutPes: TFloatField;
    QrMoviPQOutVal: TFloatField;
    QrMoviPQAntPes: TFloatField;
    QrMoviPQAntVal: TFloatField;
    QrMoviPQFimPes: TFloatField;
    QrMoviPQFimVal: TFloatField;
    QrMoviPQBalPes: TFloatField;
    QrMoviPQBalVal: TFloatField;
    BtAjusta: TBitBtn;
    PB2: TProgressBar;
    QrDif2: TmySQLQuery;
    QrDif2Insumo: TIntegerField;
    QrDif2Peso: TFloatField;
    QrDif2Preco: TFloatField;
    Diferenasgeradas1: TMenuItem;
    QrDifGer: TmySQLQuery;
    frxDsDifGer: TfrxDBDataset;
    QrDifGerNO_PQ: TWideStringField;
    QrDifGerDataX: TDateField;
    QrDifGerOrigemCodi: TIntegerField;
    QrDifGerOrigemCtrl: TIntegerField;
    QrDifGerTipo: TIntegerField;
    QrDifGerCliOrig: TIntegerField;
    QrDifGerCliDest: TIntegerField;
    QrDifGerInsumo: TIntegerField;
    QrDifGerPeso: TFloatField;
    QrDifGerValor: TFloatField;
    QrDifGerRetorno: TSmallintField;
    QrDifGerStqMovIts: TIntegerField;
    QrDifGerAlterWeb: TSmallintField;
    QrDifGerAtivo: TSmallintField;
    QrDifGerRetQtd: TFloatField;
    frxQUI_BALAN_001_02: TfrxReport;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    EdPesquisa: TdmkEdit;
    Panel9: TPanel;
    Label3: TLabel;
    DBGBalancosIts: TdmkDBGridZTO;
    QrAtualizaEmpresa: TIntegerField;
    QrBalancosItsEmpresa: TIntegerField;
    QrBalancosEmpresa: TIntegerField;
    QrBalancosNO_Emp: TWideStringField;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrBalancosFilial: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtTravaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrBalancosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure QrBalancosCalcFields(DataSet: TDataSet);
    procedure DBGBalancosItsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtIncluiItsClick(Sender: TObject);
    procedure BtAlteraItsClick(Sender: TObject);
    procedure QrBalancosItsAfterOpen(DataSet: TDataSet);
    procedure BtExcluiItsClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure QrBalancosItsCalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrBalancosAfterScroll(DataSet: TDataSet);
    procedure BtPrivativoClick(Sender: TObject);
    procedure BtResgataClick(Sender: TObject);
    procedure QrBalancosBeforeClose(DataSet: TDataSet);
    procedure BtInicialClick(Sender: TObject);
    procedure QrBalancosItsBeforeClose(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure Outrasimpressoes1Click(Sender: TObject);
    procedure EsteBalano1Click(Sender: TObject);
    procedure QrCliDestBeforeClose(DataSet: TDataSet);
    procedure QrCliDestAfterScroll(DataSet: TDataSet);
    procedure frxQUI_BALAN_001_01GetValue(const VarName: string;
      var Value: Variant);
    procedure BtGeraDifClick(Sender: TObject);
    procedure BtAjustaClick(Sender: TObject);
    procedure Diferenasgeradas1Click(Sender: TObject);
    procedure EdPesquisaChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FPQBIts: String;
    FEmpresa: Integer;
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure EditaProduto(SQLType: TSQLType);
    function  GeraDadosEstoqueAnterior(const Entidade: Integer; var Tabela:
              String): Boolean;
    //procedure GeraDif_1();
    procedure GeraDif_2();
    function  JaTemBalancoPosterior(): Boolean;
    function  ExcluiTodosItensDoCliInt(): Boolean;
  public
    { Public declarations }

    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
    procedure SomaBalanco();
    procedure ReindexaTabela();
    procedure ReEntitula();
    procedure ReopenCliDest(CliInt: Integer);
    procedure Resgata(ExigeSenha: Boolean);
  end;

var
  FmPQB2: TFmPQB2;

implementation
  uses UnMyObjects, Module, PQB2New, PQB2Edit, Principal, PQx, ModuleGeral,
  UCreate, MyDBCheck, PQB2Mul, DmkDAC_PF, CreateBlueDerm, PQB2Resgata, UnPQ_PF,
  Periodo;

{$R *.DFM}

var
  TimerIni: TDateTime;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPQB2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQB2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrBalancosPeriodo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPQB2.DefParams;
begin
  VAR_GOTOTABELA := 'Balancos';
  VAR_GOTOMYSQLTABLE := QrBalancos;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_PERIODO;
  VAR_GOTONOME := '';//CO_NOME;
  VAR_GOTOMYSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := 'Empresa=' + Geral.FF0(FEmpresa);

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT bal.*, emp.Filial, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_Emp');
  VAR_SQLx.Add('FROM balancos bal');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=bal.Empresa');
  VAR_SQLx.Add('WHERE bal.Periodo > 0');
  VAR_SQLx.Add('AND bal.Empresa=' + Geral.FF0(FEmpresa));
  //
  VAR_SQL1.Add('AND bal.Periodo=:P0');
  //
  VAR_SQLa.Add(''); //AND Nome Like :P0
  //
end;

procedure TFmPQB2.Diferenasgeradas1Click(Sender: TObject);
var
  Periodo, CliInt: Integer;
begin
  Periodo := QrBalancosPeriodo.Value - 1;
  CliInt :=  QrCliDestCodigo.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDifGer, Dmod.MyDB, [
  'SELECT pq.Nome NO_PQ, pqx.* ',
  'FROM pqx pqx ',
  'LEFT JOIN pq pq ON pq.Codigo=pqx.Insumo ',
  'WHERE pqx.OrigemCodi=' + Geral.FF0(Periodo),
  'AND pqx.Tipo=' + Geral.FF0(VAR_FATID__001),
  'AND pqx.CliDest=' + Geral.FF0(CliInt),
  'ORDER BY NO_PQ ',
  '']);
  //
  MyObjects.frxMostra(frxQUI_BALAN_001_02, 'Diferen�as de Balan�o');
end;

procedure TFmPQB2.MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
begin
  if Mostra then
  begin
    GBCntrl.Visible    := False;
    GBConfirma.Visible := True;
  end else
  begin
    GBCntrl.Visible    := True;
    GBConfirma.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPQB2.Outrasimpressoes1Click(Sender: TObject);
begin
  FmPrincipal.CadastroPQImp('FmPQB');
end;

procedure TFmPQB2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPQB2.AlteraRegistro;
begin
  (*if*) JaTemBalancoPosterior() (*then
    Exit*);
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  MostraEdicao(True, stIns, 0);
  UMyMod.UpdLockY(QrBalancosPeriodo.Value, Dmod.MyDB, 'Balancos', 'Periodo');
end;

procedure TFmPQB2.IncluiRegistro;
begin
  Application.CreateForm(TFmPQB2New, FmPQB2New);
  FmPQB2New.ShowModal;
  FmPQB2New.Destroy;
end;

function TFmPQB2.JaTemBalancoPosterior(): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT *  ',
    'FROM balancos ',
    'WHERE Periodo > ' + Geral.FF0(QrBalancosPeriodo.Value),
    'AND Empresa=' + Geral.FF0(QrBalancosEmpresa.Value),
    '']);
    //
    Result := Qry.RecordCount > 0;
    if Result then
      Geral.MB_Aviso('J� existe um balan�o posterior!');
  finally
    Qry.Free;
  end;
end;

procedure TFmPQB2.QueryPrincipalAfterOpen;
begin
  if (QrBalancosPeriodo.Value = UnPQx.VerificaBalanco(* +1 ? *)) then
  begin
    BtAltera.Enabled := True;
    BtGeraDif.Enabled := True;
  end else
  begin
    //UnDmkDAC_PF.AbreQuery(QrMax, Dmod.MyDB);
    UnDmkDAC_PF.AbreMySQLQuery0(QrMax, Dmod.MyDB, [
    'SELECT Max(Periodo) Periodo ',
    'FROM balancos ',
    'WHERE Empresa=' + Geral.FF0(FEmpresa),
    '']);
    //
    if QrMaxPeriodo.Value <> QrBalancosPeriodo.Value then
    begin
      BtAltera.Enabled  := False;
      BtGeraDif.Enabled := False;
      //ReindexaTabela();
      ReopenCliDest(0);
    end else
    begin
      BtAltera.Enabled  := True;
      BtGeraDif.Enabled := True;
    end;
  end;
  ///
end;

procedure TFmPQB2.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQB2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQB2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQB2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQB2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQB2.BtResgataClick(Sender: TObject);
begin
  Resgata(True);
end;

procedure TFmPQB2.BtAjustaClick(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de entidade';
  Prompt = 'Informe a entidade: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Controle, Entidade: Variant;
begin
  Entidade := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT DISTINCT pqc.CI Codigo, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ' + Campo,
    'FROM pqcli pqc ',
    'LEFT JOIN entidades ent ON ent.Codigo=pqc.CI ',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, True);
  if Entidade <> Null then
  begin
    Screen.Cursor := crHourGlass;
    try
      FPQBIts := UnCreateBlueDerm.RecriaTempTableNovo(ntrttPQBIts, DModG.QrUpdPID1, False);
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DModG.MyPID_DB, [
      'INSERT INTO ' + FPQBIts,
      'SELECT pqc.Controle, pqc.PQ, pqc.CI, ',
      'pqc.CustoPadrao, pqc.Peso, pqc.Valor, pqc.Custo, ',
      'pqc.Peso AjPeso, pqc.Valor AjValor, pqc.CustoPadrao AjCusto, ',
      'pq.Nome NO_PQ, 0 Ativo ',
      'FROM ' + TMeuDB + '.pqcli pqc ',
      'LEFT JOIN ' + TMeuDB + '.pq pq ON pq.Codigo=pqc.PQ ',
      'WHERE pqc.CI=' + Geral.FF0(Entidade),
      'ORDER BY pq.Nome ',
      '']);
      if DBCheck.CriaFm(TFmPQB2Mul, FmPQB2Mul, afmoNegarComAviso) then
      begin
        Screen.Cursor := crDefault;
        FmPQB2Mul.FPQBIts := FPQBIts;
        FmPQB2Mul.ReopenPQBIts();
        FmPQB2Mul.ShowModal;
        FmPQB2Mul.Destroy;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPQB2.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmPQB2.BtSaidaClick(Sender: TObject);
begin
  VAR_CAIXA := QrBalancosPeriodo.Value;
  Close;
end;

procedure TFmPQB2.BtTravaClick(Sender: TObject);
var
  Periodo : Integer;
  DataIni: TDateTime;
begin
  //VAR_PERIODO_BAL := -2;
  SomaBalanco();
  Periodo := QrBalancosPeriodo.Value;
  UMyMod.UpdUnlockY(Periodo, Dmod.MyDB, 'Balancos', 'Periodo');
  MostraEdicao(False, stLok, 0);
  LocCod(Periodo, Periodo);
  //
  try
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('UPDATE pqcli SET Peso=0, Valor=0, Custo=0');
    Dmod.QrUpdU.ExecSQL;
////////////////////////////////////////////////////////////////////////////////
    (*
    QrAtualiza.Close;
    UnDmkDAC_PF.AbreQuery(QrAtualiza, Dmod.MyDB);
    Progress1.Position := 0;
    Progress1.Max := GOTOy.Registros(QrAtualiza);
    PainelDados2.Visible := True;
    PainelDados2.Refresh;
    GBCntrl.Refresh;
    Panel1.Refresh;
    Panel2.Refresh;
    TimerIni := Now();

    //  fazer apenas uma vez para n�o demorar o UnPQx.AtualizaEstoquePQ !!!
    DMod.AtualizaStqMovIts_PQX(nil, nil, nil);
    //
    VAR_PQS_ESTQ_NEG := '';
    while not QrAtualiza.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
      PnRegistros.Refresh;
      PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
      PnTempo.Refresh;
      Application.ProcessMessages;
      UnPQx.AtualizaEstoquePQ(QrAtualizaCI.Value, QrAtualizaPQ.Value, QrAtualizaEmpresa.Value, aeVAR, CO_VAZIO, False);
      QrAtualiza.Next;
    end;
    QrAtualiza.Close;
    *)
    DataIni    := Geral.PeriodoToDate(Periodo, 1, False, detJustSum);
    if not PQ_PF.AtualizaTodosPQsPosBalanco(aeVAR, Progress1, PainelDados2,
      PnRegistros, PnTempo, TimerIni, DataIni) then
      Exit;
////////////////////////////////////////////////////////////////////////////////
    if VAR_PQS_ESTQ_NEG <> '' then
    begin
      Geral.MB_Aviso(VAR_PQS_ESTQ_NEG);
      VAR_PQS_ESTQ_NEG := '';
    end else
      Geral.MB_Info('Atualiza��o de estoques finalizada');
    //
    PainelDados2.Visible := False;
    // Parei aqui! N�o usa mais?
    //if QrBalancosConfirmado.Value = 'F' then FmPrincipal.RefreshInsumos;
{    ini tentativa de eliminar TmySQLQuery
    FmPrincipal.AtualizaBalancoDePQNoSMI(nil);
}
  except
    raise;
    Geral.MB_Erro('Ocorreu um erro nas atualiza��es de estoque.');
  end;
  //VAR_PERIODO_BAL := -2;
end;

procedure TFmPQB2.FormCreate(Sender: TObject);
begin
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  ImgTipo.SQLType := stLok;
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  CriaOForm;
  //
  MostraEdicao(False, stLok, 0);
end;

procedure TFmPQB2.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Valor: string;
begin
  if Shift = [ssCtrl] then
  begin
    if dmkPF.EhControlF(Sender, Key, Shift) then
    begin
      if InputQuery('Pesquisa de item pelo c�digo', 'Informe o c�digo:', Valor) then
        QrBalancosIts.Locate('Insumo', Valor, []);
    end;
  end;
end;

procedure TFmPQB2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrBalancosPeriodo.Value, LaRegistro.Caption);
end;

procedure TFmPQB2.SbQueryClick(Sender: TObject);
begin
//
end;

procedure TFmPQB2.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPQB2.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmPQB2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQB2.QrBalancosAfterOpen(DataSet: TDataSet);
begin
  GOTOy.BtEnabled(QrBalancosPeriodo.Value, False);
  QueryPrincipalAfterOpen;
  BtAjusta.Enabled := True;
  BtResgata.Enabled := True;
  BtExclui.Enabled := (QrBalancos.RecordCount > 0)
    and (UnPQx.VerificaBalanco+1 = QrBalancosPeriodo.Value);
  ReEntitula();
end;

procedure TFmPQB2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQB2.BtIncluiClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  IncluiRegistro;
end;

procedure TFmPQB2.QrBalancosCalcFields(DataSet: TDataSet);
begin
  QrBalancosPeriodo2.Value := ' Balan�o de '+
  Geral.Maiusculas(dmkPF.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtTexto), False);
end;

procedure TFmPQB2.DBGBalancosItsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ImgTipo.SQLType <> stLok then
  begin
    if (key = VK_F13) then
      EditaProduto(stUpd);
    if (key = VK_INSERT) then
      EditaProduto(stIns);
  end;
end;

procedure TFmPQB2.ReEntitula();
var
  Texto: String;
begin
  if QrBalancos.State <> dsInactive then
  begin
    //Texto := 'Balan�o de Insumos: ' + QrBalancosPeriodo2.Value;
    Texto := QrBalancosPeriodo2.Value;
    MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Texto, False, taCenter, 2, 10, 20);
  end;
end;

procedure TFmPQB2.ReindexaTabela();
var
  SQL, Txt, Mercadoria: String;
begin
  Mercadoria := Trim(EdPesquisa.ValueVariant);
  //
  if Mercadoria <> '' then
  begin
    Txt := StringReplace(Mercadoria, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
    SQL := 'AND pq_.Nome LIKE "%' + Txt + '%"';
  end else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBalancosIts, Dmod.MyDB, [
    'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, ',
    'pq_.GrupoQuimico, pqg.Nome NOMEGRUPO, ',
    'IF(pqx.Valor=0, 0, pqx.Valor/pqx.Peso) CUSTOKG ',
    'FROM pqx pqx ',
    'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo ',
    'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ ',
    'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico ',
    'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
    'AND pqx.OrigemCodi=' + Geral.FF0(QrBalancosPeriodo.Value),
    'AND pqx.CliDest=' + Geral.FF0(QrClidestCodigo.Value),
    'AND pqx.Empresa=' + Geral.FF0(QrBalancosEmpresa.Value),
    SQL,
    '']);
end;

procedure TFmPQB2.ReopenCliDest(CliInt: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCliDest, Dmod.MyDB, [
  'SELECT DISTINCT ent.Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT' ,
  'FROM pqx pqx ',
  'LEFT JOIN entidades ent ON ent.Codigo=pqx.CliDest ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
  //'AND ent.Codigo <> 0 ',
  'AND pqx.OrigemCodi=' + Geral.FF0(QrBalancosPeriodo.Value),
  'AND pqx.Empresa=' + Geral.FF0(QrBalancosEmpresa.Value),
  'ORDER BY NO_ENT ',
  '']);
  //
  QrCliDest.Locate('Codigo', CliInt, []);
end;

procedure TFmPQB2.Resgata(ExigeSenha: Boolean);
const
  Aviso  = '...';
  Titulo = 'Sele��o de entidade';
  Prompt = 'Informe a entidade: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Controle, Entidade: Variant;
  Periodo: Integer;
  Data: TDateTime;
  DataI, DataF, PQBResgata: String;
  Qry: TmySQLQuery;
begin
  if ExigeSenha then
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
  Entidade := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT DISTINCT pqc.CI Codigo, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ' + Campo,
    'FROM pqcli pqc ',
    'LEFT JOIN entidades ent ON ent.Codigo=pqc.CI ',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, True);
  if Entidade <> Null then
  begin
    Screen.Cursor := crHourGlass;
    try
      Periodo := QrBalancosPeriodo.Value;
      Data    := Geral.PeriodoToDate(Periodo, 1, False, detJustSum);
      DataF   := Geral.FDT(Data, 1);
      //
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Insumo',
        'FROM pqx ',
        'WHERE DataX="' + DataF + '"',
        'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
        'AND pqx.CliOrig=' + Geral.FF0(Entidade),
        'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
        '']);
        if Qry.recordCount > 0 then
        begin
          Geral.MB_Aviso('A��o cancelada!' + sLineBreak +
          'Este cliente interno j� possui itens de balan�o para este per�odo!');
          Exit;
        end;
      finally
        Qry.Free;
      end;
      //
      if GeraDadosEstoqueAnterior(Entidade, PQBResgata) then
      begin
        if DBCheck.CriaFm(TFmPQB2Resgata, FmPQB2Resgata, afmoNegarComAviso) then
        begin
          Screen.Cursor := crDefault;
          FmPQB2Resgata.FPQBResgata := PQBResgata;
          FmPQB2Resgata.FPeriodo    := QrBalancosPeriodo.Value;
          FmPQB2Resgata.FDataX      := DataF;
          FmPQB2Resgata.FCliInt     := Entidade;
          FmPQB2Resgata.FEmpresa    := FEmpresa;
          //
          FmPQB2Resgata.EdCliIntCodi.ValueVariant := Entidade;
          DModG.ReopenEnti(Entidade);
          FmPQB2Resgata.EdCliIntNome.ValueVariant := DModG.QrEntiNO_ENTI.Value;
          FmPQB2Resgata.ReopenPQB2Resgata(0);
          FmPQB2Resgata.ShowModal;
          FmPQB2Resgata.Destroy;
          //
          ReopenCliDest(Entidade);
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPQB2.EditaProduto(SQLType: TSQLType);
var
  Cliente, Produto, Empresa: Integer;
begin
  //VAR_LOCATE011 := QrBalancosItsFORNECEDO.Value;
  VAR_LOCATE012 := QrBalancosItsNOMEGRUPO.Value;
  VAR_LOCATE014 := QrBalancosItsInsumo.Value;
  Produto := QrBalancosItsInsumo.Value;
  Cliente := QrBalancosItsCliOrig.Value;
  Empresa := QrBalancosItsEmpresa.Value;
  Application.CreateForm(TFmPQB2Edit, FmPQB2Edit);
  with FmPQB2Edit do
  begin
    ImgTipo.SQLType := SQLType;
    FBalanco        := QrBalancosPeriodo.Value;
    if SQLType = stUpd then
    begin
      EdEmpresa.ValueVariant  := Empresa;
      CBEmpresa.KeyValue      := Empresa;
      LaEmpresa.Enabled       := False;
      EdEmpresa.Enabled       := False;
      CBEmpresa.Enabled       := False;
      EdInsumo.ValueVariant  := Produto;
      CBInsumo.KeyValue      := Produto;
      EdCliente.ValueVariant := Cliente;
      CBCliente.KeyValue     := Cliente;
      EdPecas.ValueVariant   := QrBalancosItsPeso.Value;
      EdVTota.ValueVariant   := QrBalancosItsValor.Value;
      //
      LaInsumo.Enabled       := False;
      EdInsumo.Enabled       := False;
      CBInsumo.Enabled       := False;
    end else begin
      EdInsumo.ValueVariant  := 0;
      CBInsumo.KeyValue      := 0;
      EdCliente.ValueVariant := 0;
      CBCliente.KeyValue     := 0;
      EdPecas.ValueVariant   := 0;
      EdVTota.ValueVariant   := 0;
      //
      LaInsumo.Enabled       := True;
      EdInsumo.Enabled       := True;
      CBInsumo.Enabled       := True;
    end;
    ShowModal;
    Destroy;
  end;
  SomaBalanco();
  ReindexaTabela();
end;

procedure TFmPQB2.EdPesquisaChange(Sender: TObject);
begin
  ReindexaTabela();
end;

procedure TFmPQB2.EsteBalano1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxQUI_BALAN_001_01, 'Balan�o');
end;

function TFmPQB2.ExcluiTodosItensDoCliInt(): Boolean;
var
  CliInt, Empresa, Insumo, OriCodi, OriCtrl, OriTipo: Integer;
begin
  VAR_PQS_ESTQ_NEG := '';
  try
    QrBalancosIts.First;
    while not QrBalancosIts.Eof do
    begin
      CliInt     := QrCliDestCodigo.Value;
      Empresa    := QrBalancosItsEmpresa.Value;
      Insumo     := QrBalancosItsInsumo.Value;
      OriCodi    := QrBalancosItsOrigemCodi.Value;
      OriCtrl    := QrBalancosItsOrigemCtrl.Value;
      OriTipo    := QrBalancosItsTipo.Value;
      //
      PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, False, Empresa);
      //
      QrBalancosIts.Next;
    end;
    if VAR_PQS_ESTQ_NEG <> '' then
    begin
      Geral.MB_Aviso(VAR_PQS_ESTQ_NEG);
      VAR_PQS_ESTQ_NEG := '';
    end;
    Result := True;
  except;
    Result := False;
  end;
end;

procedure TFmPQB2.BtIncluiItsClick(Sender: TObject);
begin
  EditaProduto(stIns);
end;

procedure TFmPQB2.BtInicialClick(Sender: TObject);
const
 Tipo = VAR_FATID_0000; // Balan�o
 Unitario = False;
var
  Peso, Valor: Double;
  DataI, DataF, DataX, DtCorrApo: String;
  PeriodoBal, CliOrig, CliDest, Insumo, OrigemCodi: Integer;
  OrigemCtrl: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    DtCorrApo := Geral.FDT(0, 1);
    DataX := dmkPF.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtSystem);
    PeriodoBal := UnPQx.VerificaBalancoPelaData(
      Geral.PeriodoToDate(QrBalancosPeriodo.Value-1, 1, False, detJustSum));
    if PeriodoBal = 0 then
    begin
      DataI := FormatDateTime(VAR_FORMATDATE, 2);
      DataF := dmkPF.UltimoDiaDoPeriodo(QrBalancosPeriodo.Value-1, dtSystem);
    end else begin
      DataI := dmkPF.PrimeiroDiaDoPeriodo(PeriodoBal, dtSystem);
      DataF := dmkPF.UltimoDiaDoPeriodo(PeriodoBal, dtSystem);
    end;
    QrPQCli.Close;
    QrPQCli.Params[00].AsString  := DataI;
    QrPQCli.Params[01].AsString  := DataF;
    UnDmkDAC_PF.AbreQuery(QrPQCli, Dmod.MyDB);
    while not QrPQCli.Eof do
    begin
      Peso  := 0;
      Valor := 0;
      //
      { Obsoleto !!! ???
      if PeriodoBal <> 0 then
      begin
        Dmod.QrBalancosIts.Close;
        Dmod.QrBalancosIts.Params[0].AsInteger := Insumo;
        Dmod.QrBalancosIts.Params[1].AsInteger := PeriodoBal;
        Dmod.QrBalancosIts.O p e n;
        Peso  := Dmod.QrBalancosItsEstqQ.Value;
        Valor := Dmod.QrBalancosItsEstqV.Value;
      end;
      }
      Dmod.QrSumPQ_D.Close;
      Dmod.QrSumPQ_D.Params[00].AsInteger := QrPQCliCliDest.Value;
      Dmod.QrSumPQ_D.Params[01].AsInteger := QrPQCliInsumo.Value;
      Dmod.QrSumPQ_D.Params[02].AsString  := DataI;
      Dmod.QrSumPQ_D.Params[03].AsString  := DataF;
        //dmkPF.PrimeiroDiaDoPeriodo(PeriodoBal+1, dtSystem);
      UnDmkDAC_PF.AbreQuery(Dmod.QrSumPQ_D, Dmod.MyDB);
      Peso  := Peso  + Dmod.QrSumPQ_DPeso.Value;
      if Peso <> 0 then
      begin
        Valor := Valor + Dmod.QrSumPQ_DValor.Value;
        //
        CliOrig    := QrPQCliCliDest.Value;
        CliDest    := CliOrig;
        Insumo     := QrPQCliInsumo.Value;
        OrigemCodi := QrBalancosPeriodo.Value;
        OrigemCtrl := UMyMod.BuscaEmLivreY_Double(
          Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
        //
(*
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'p q x', False, [
        'DataX', 'CliOrig', 'CliDest',
        'Insumo', 'Peso', 'Valor'], [
        'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
        DataX, CliOrig, CliDest,
        Insumo, Peso, Valor], [
        OrigemCodi, OrigemCtrl, Tipo], False);
*)
        PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor,
        OrigemCodi, Trunc(OrigemCtrl), Tipo, Unitario, DtCorrApo, FEmpresa);
        //
      end;
      QrPQCli.Next;
    end;
    PQ_PF.VerificaEquiparacaoEstoque(True);
  finally
    Screen.Cursor := crDefault;
    LocCod(QrBalancosPeriodo.Value, QrBalancosPeriodo.Value);
  end;
end;

procedure TFmPQB2.BtAlteraItsClick(Sender: TObject);
begin
  EditaProduto(stUpd);
end;

procedure TFmPQB2.QrBalancosItsAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita            := QrBalancosIts.RecordCount > 0;
  BtAlteraIts.Enabled := Habilita;
  BtInicial.Enabled   := not Habilita;
  BtExcluiIts.Enabled := Habilita;
end;

procedure TFmPQB2.QrBalancosItsBeforeClose(DataSet: TDataSet);
begin
  BtInicial.Enabled := False;
end;

procedure TFmPQB2.SomaBalanco();
begin
  QrSoma.Close;
  QrSoma.Params[0].AsInteger := QrBalancosPeriodo.Value;
  UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE balancos SET ');
  Dmod.QrUpdU.SQL.Add('EstqQ=:P0, EstqV=:P1 ');
  Dmod.QrUpdU.SQL.Add('WHERE Periodo=:P2');
  Dmod.QrUpdU.Params[0].AsFloat   := QrSomaEstqQ.Value;
  Dmod.QrUpdU.Params[1].AsFloat   := QrSomaEstqV.Value;
  Dmod.QrUpdU.Params[2].AsInteger := QrBalancosPeriodo.Value;
  Dmod.QrUpdU.ExecSQL;
  //
  LocCod(QrBalancosPeriodo.Value, QrBalancosPeriodo.Value);
end;

procedure TFmPQB2.BtExcluiItsClick(Sender: TObject);
  procedure ExcluiItemAtual(Avisa: Boolean);
  var
    Periodo, Insumo, Controle, CliInt, OriCodi, OriCtrl, OriTipo, Empresa: Integer;
  begin
    Periodo  := QrBalancosItsOrigemCodi.Value;
    Insumo   := QrBalancosItsInsumo.Value;
    Controle := QrBalancosItsOrigemCtrl.Value;
    //
    (* Gera erro quando exclui todos!!
    QrBalancosIts.Prior;
    if QrBalancosItsOrigemCtrl.Value = Controle then QrBalancosIts.Next;
    VAR_LOCATE014 := QrBalancosItsInsumo.Value;
    *)
    //
    (*
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM pqx WHERE Tipo=' + Geral.FF0(VAR_FATID_0000) + ' AND Insumo=:P0 ');
    Dmod.QrUpdU.SQL.Add('AND OrigemCodi=:P1 AND OrigemCtrl=:P2');
    Dmod.QrUpdU.Params[0].AsInteger := Insumo;
    Dmod.QrUpdU.Params[1].AsInteger := Periodo;
    Dmod.QrUpdU.Params[2].AsInteger := Controle;
    Dmod.QrUpdU.ExecSQL;
    *)
    Empresa  := QrBalancosItsEmpresa.Value;
    CliInt   := QrBalancosItsCliOrig.Value;
    OriCodi  := QrBalancosItsOrigemCodi.Value;
    OriCtrl  := QrBalancosItsOrigemCtrl.Value;
    OriTipo  := QrBalancosItsTipo.Value;
    PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, Avisa, Empresa);
  end;
var
  q: TSelType;
  n: Integer;
begin
  //QrBalancosIts.DisableControls;
  DBCheck.Quais_Selecionou(QrBalancosIts, TDBGrid(DBGBalancosIts), q);
  case q of
    istAtual: ExcluiItemAtual(True);
    istSelecionados:
    begin
      with DBGBalancosIts.DataSource.DataSet do
      for n := 0 to DBGBalancosIts.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DBGBalancosIts.SelectedRows.Items[n]));
        GotoBookmark(DBGBalancosIts.SelectedRows.Items[n]);
        ExcluiItemAtual(False);
      end;
      PQ_PF.VerificaEquiparacaoEstoque(True);
    end;
    istTodos:
    begin
      VAR_PQS_ESTQ_NEG := '';
      QrBalancosIts.First;
      while not QrBalancosIts.Eof do
      begin
        ExcluiItemAtual(False);
        //
        QrBalancosIts.Next;
      end;
      if VAR_PQS_ESTQ_NEG <> '' then
      begin
        Geral.MB_Aviso(VAR_PQS_ESTQ_NEG);
        VAR_PQS_ESTQ_NEG := '';
      end;
      PQ_PF.VerificaEquiparacaoEstoque(True);
    end;
  end;
{
  if Geral.MB_'Deseja excluir este item de balan�o?', PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
  MB_APPLMODAL) = ID_YES then
  begin
    Periodo  := QrBalancosItsOrigemCodi.Value;
    Insumo   := QrBalancosItsInsumo.Value;
    Controle := QrBalancosItsOrigemCtrl.Value;
    //
    QrBalancosIts.Prior;
    if QrBalancosItsOrigemCtrl.Value = Controle then QrBalancosIts.Next;
    //VAR_LOCATE011 := QrBalancosItsFORNECEDO.Value;
    VAR_LOCATE014 := QrBalancosItsInsumo.Value;
    //
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM pqx WHERE Tipo=' + Geral.FF0(VAR_FATID_0000) + ' AND Insumo=:P0 ');
    Dmod.QrUpdU.SQL.Add('AND OrigemCodi=:P1 AND OrigemCtrl=:P2');
    Dmod.QrUpdU.Params[0].AsInteger := Insumo;
    Dmod.QrUpdU.Params[1].AsInteger := Periodo;
    Dmod.QrUpdU.Params[2].AsInteger := Controle;
    Dmod.QrUpdU.ExecSQL;
  end;
  //
}
  QrBalancosIts.EnableControls;
  SomaBalanco();
end;

procedure TFmPQB2.BtGeraDifClick(Sender: TObject);
begin
  GeraDif_2();
end;

{
procedure TFmPQB2.GeraDif_1();
const
  PQ = 0;
  _WHERE = '';
  ORDERBY = '';
  //Continua: Boolean;
  Tipo = VAR_FATID__001;
var
  CI, PeriAtu, PeriAnt: Integer;
  DtIni, DtFIm: TDateTime;
  NomeCI: String;
  //
  OrigemCtrl, OrigemCodi, CliOrig, CliDest: Integer;
  DataX: String;
  //
  Insumo: Integer;
  Peso, Valor: Double;
  procedure InsereAtual();
  var
    Retorno, StqMovIts: Integer;
  begin
    EXIT;
    (*
    OrigemCtrl     := OrigemCtrl + 1;
    Retorno        := 0;
    StqMovIts      := 0;
    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'p q x', False, [
    'DataX', 'CliOrig', 'CliDest',
    'Insumo', 'Peso', 'Valor',
    'Retorno', 'StqMovIts'], [
    'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
    DataX, CliOrig, CliDest,
    Insumo, Peso, Valor,
    Retorno, StqMovIts], [
    OrigemCodi, OrigemCtrl, Tipo], False);
    *)
    PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor,
    OrigemCodi, OrigemCtrl, Tipo, Unitario);
  end;
var
  Qry: TmySQLQuery;
begin
  if QrCliDest.RecordCount <> 0 then
  begin
    CI     := QrCliDestCodigo.Value;
    NomeCI := QrCliDestNO_ENT.Value;
    if Geral.MB_Pergunta(
    'Deseja realmente gerar as diferen�as do balan�o do cliente interno "' +
    NomeCI + '" no m�s anterior?') =
    ID_YES then
    begin
      PainelDados2.Visible := True;
      PeriAtu := QrBalancosPeriodo.Value;
      PeriAnt := PeriAtu - 1;
      DtIni := Geral.PeriodoToDate(PeriAtu, 1, False, detJustSum);
      DtFim := Geral.PeriodoToDate(PeriAtu + 1, 1, False, detJustSum) - 1;
      //
      UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM pqx ',
      'WHERE Tipo=' + Geral.FF0(VAR_FATID__001),
      'AND CliDest=' + Geral.FF0(CI),
      'AND OrigemCodi=' + Geral.FF0(PeriAnt),
      '']);
      //if
      Dmod.PreparaMoviPQ_EstqEm(ntrttMoviPQ, QrMoviPQ, CI, PQ,
      DtIni, DtFim, NomeCI, _WHERE, ORDERBY, Progress1);
      //then
      //begin
        DataX          := Geral.FDT(DtIni - 1, 1);
        OrigemCodi     := PeriAnt;
        CliOrig        := CI;
        CliDest        := CI;

        Progress1.Position := 0;
        Progress1.Max := QrMoviPQ.RecordCount;

        Qry := TmySQLQuery.Create(Dmod);
        try
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT MAX(OrigemCtrl) OrigemCtrl ',
          'FROM pqx ',
          'WHERE Tipo=' + Geral.FF0(VAR_FATID__001),
          'AND OrigemCodi=' + Geral.FF0(PeriAnt),
          '']);
          OrigemCtrl := Qry.FieldByName('OrigemCtrl').AsInteger;
        finally
          Qry.Free;
        end;

        QrMoviPQ.First;
        while not QrMoviPQ.Eof do
        begin
          Progress1.Position := Progress1.Position + 1;
          Peso  := QrMoviPQBalPes.Value - QrMoviPQAntPes.Value;
          Valor := QrMoviPQBalVal.Value - QrMoviPQAntVal.Value;
          if (Peso <> 0) or (Valor <> 0) then
          begin
            Insumo := QrMoviPQInsumo.Value;
            InsereAtual();
          end;
          //
          QrMoviPQ.Next;
        end;
      //end;
      Progress1.Position := 0;
      PainelDados2.Visible := False;
    end;
  end;
end;
}

procedure TFmPQB2.GeraDif_2;
const
  Tipo = VAR_FATID__001;
  GerouDif = 1;
  Unitario = False;
var
  CI: Integer;
  NomeCI: String;
  Qry: TmySQLQuery;
  //
  Peso, Valor, Preco, QtdBal: Double;
  NO_PQ, Tabela: String;
  Insumo: Integer;
  //
  Periodo, OrigemCodi, OrigemCtrl, CliOrig, CliDest, CliInt, OriCodi, OriCtrl,
  OriTipo: Integer;
  DataX, DtCorrApo: String;
  //
begin
  if QrCliDest.RecordCount <> 0 then
  begin
    Periodo    := QrBalancosPeriodo.Value;
    OrigemCodi := Periodo - 1;
    CI         := QrCliDestCodigo.Value;
    CliOrig    := CI;
    CliDest    := CI;
    NomeCI     := QrCliDestNO_ENT.Value;
    DtCorrApo := Geral.FDT(0, 1);
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM pqx ',
      'WHERE Tipo=' + Geral.FF0(VAR_FATID__001),
      'AND CliDest=' + Geral.FF0(CI),
      'AND OrigemCodi=' + Geral.FF0(OrigemCodi),
      'AND RetQtd <> 0 ',
      '']);
      if Qry.RecordCount > 0 then
      begin
        Geral.MB_Aviso('A��o abortada!' + slineBreak +
        'J� existem ' + Geral.FF0(Qry.RecordCount) +
        ' itens gerados anteriormente com NF de retorno!');
        Exit;
      end;
      if Geral.MB_Pergunta(
      'Deseja realmente gerar as diferen�as do balan�o do cliente interno "' +
      NomeCI + '" no m�s anterior?') =
      ID_YES then
      begin
(*
        UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'DELETE FROM pqx ',
        'WHERE Tipo=' + Geral.FF0(VAR_FATID__001),
        'AND CliDest=' + Geral.FF0(CI),
        'AND OrigemCodi=' + Geral.FF0(OrigemCodi),
        '']);
*)
        if not ExcluiTodosItensDoCliInt() then Exit;
        //
        if GeraDadosEstoqueAnterior(CI, Tabela) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT pqx.Insumo, SUM(pqx.Peso) Peso, ',
          'pq.Nome NO_PQ',
          'FROM pqx pqx',
          'LEFT JOIN pq pq ON pq.Codigo=pqx.Insumo ',
          'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
          'AND pqx.CliDest=' + Geral.FF0(CI),
          'AND pqx.OrigemCodi=' + Geral.FF0(Periodo),
          'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
          'GROUP BY pqx.Insumo ',
          '']);
          //
          PB2.Position := 0;
          PB2.Max := Qry.recordCount;
          Qry.First;
          while not Qry.Eof do
          begin
            PB2.Position := PB2.Position + 1;
            //
            Peso := Qry.FieldByName('Peso').AsFloat;
            if Peso <> 0 then
            begin
              Insumo  := Qry.FieldByName('Insumo').AsInteger;
              NO_PQ   := Qry.FieldByName('NO_PQ').AsString;
              QtdBal  := Qry.FieldByName('Peso').AsFloat;
              Peso    := 0;
              Preco   := 0;
              //
              UMyMod.SQLIns_ON_DUPLICATE_KEY(DModG.QrUpdPID1, Tabela, False, [
              'Peso', 'Valor',
              'Preco', 'NO_PQ', 'QtdBal'], [
              'Insumo'], ['QtdBal'], [
              Peso, Valor,
              Preco, NO_PQ, QtdBal], [
              Insumo], [QtdBal], False);
            end;
            Qry.Next;
          end;
          UnDmkDAC_PF.AbreMySQLQuery0(QrDif2, DModG.MyPID_DB, [
          'SELECT Insumo, (QtdBal - Peso) Peso, ',
          'IF(Peso=0, 0, Valor / Peso) Preco ',
          'FROM ' + Tabela,
          'WHERE QtdBal - Peso <> 0 ',
          '']);
          //
          QrDif2.First;
          PB2.Position := 0;
          PB2.Max := QrDif2.RecordCount;
          //
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT MAX(OrigemCtrl) OrigemCtrl ',
          'FROM pqx ',
          'WHERE Tipo=' + Geral.FF0(VAR_FATID__001),
          'AND OrigemCodi=' + Geral.FF0(OrigemCodi),
          '']);
          OrigemCtrl := Qry.FieldByName('OrigemCtrl').AsInteger;
          DataX   := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(OrigemCodi), 1);
          while not QrDif2.Eof do
          begin
            MyObjects.UpdPB(PB2, LaAviso1, LaAviso2);
            //
            OrigemCtrl     := OrigemCtrl + 1;
            Insumo         := QrDif2Insumo.Value;
            Peso           := QrDif2Peso.Value;
            Valor          := Peso * QrDif2Preco.Value;
            //
            {
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'p q x', False, [
            'DataX', 'CliOrig', 'CliDest',
            'Insumo', 'Peso', 'Valor'(*,
            'Retorno', 'StqMovIts'*)], [
            'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
            DataX, CliOrig, CliDest,
            Insumo, Peso, Valor(*,
            Retorno, StqMovIts*)], [
            OrigemCodi, OrigemCtrl, Tipo], False);
            }
            PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor,
            OrigemCodi, OrigemCtrl, Tipo, Unitario, DtCorrApo, FEmpresa);
            //
            QrDif2.Next;
          end;
          PQ_PF.VerificaEquiparacaoEstoque(True);
          //
          CliInt := CliDest;
          UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'balancli', False, [
          'GerouDif'], [
          'Periodo', 'CliInt', 'Empresa'], [
          'GerouDif'], [
          GerouDif], [
          Periodo, CliInt, FEmpresa], [
          GerouDif], False);
          //
          Geral.MB_Aviso(Geral.FF0(QrDif2.RecordCount) + ' itens foram inseridos!');
        end;
      end;
    finally
      Screen.Cursor := crDefault;
      Qry.Free;
    end;
  end else
    Geral.MB_Aviso('N�o existem lan�amentos de insumos para o cliente selecionado!');
end;

procedure TFmPQB2.BtExcluiClick(Sender: TObject);
var
  Periodo: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  Periodo := QrBalancosPeriodo.Value;
  if UnPQx.VerificaBalanco()+ 1 <> Periodo then
    Geral.MB_Aviso('Exclus�o da balan�o abortada! Balan�o n�o � o vigente!')
  else begin
    if Geral.MB_Pergunta('Deseja excluir este balan�o e TODOS itens dele?') = ID_YES then
    begin
      //VAR_PERIODO_BAL := -2;
(*
      if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdU, Dmod.MyDB, [
      'DELETE FROM pqx ',
      'WHERE Tipo=' + Geral.FF0(VAR_FATID_0000),
      'AND OrigemCodi=' + Geral.FF0(Periodo),
      '']) then
*)
      QrCliDest.First;
      while not QrCliDest.Eof do
      begin
        if not ExcluiTodosItensDoCliInt() then Exit;
        //
        QrCliDest.Next;
      end;
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdU, Dmod.MyDB, [
      'DELETE FROM balancos ',
      'WHERE Periodo=' + Geral.FF0(Periodo),
      'AND Empresa=' + Geral.FF0(FEmpresa),
      '']);
      //
      Dmod.DesfazBalancoGrade();
      //
      Periodo := UnPQx.VerificaBalanco()+ 1;
      LocCod(Periodo, Periodo);
      if QrBalancosPeriodo.Value = Periodo then BtTravaClick(Self);
    end;
  end;
end;

procedure TFmPQB2.BtRefreshClick(Sender: TObject);
var
  Periodo: Integer;
  //Sit
  Confirmado: String;
begin
(*
  if QrBalancosConfirmado.Value = 'V' then Sit := 'F' else Sit := 'V';
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE balancos SET Confirmado="'+Sit
  +'" WHERE Periodo='+IntToStr(Periodo));
  Dmod.QrUpdM.ExecSQL;
*)
  Periodo := QrBalancosPeriodo.Value;
  Confirmado := dmkPF.EscolhaDe2Str(QrBalancosConfirmado.Value = 'V', 'V', 'F');
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'balancos', False, [
  'Confirmado'], [
  'Periodo', 'Empresa'], [
  Confirmado], [
  Periodo, FEmpresa], True) then
    LocCod(Periodo, Periodo);
end;

procedure TFmPQB2.QrBalancosItsCalcFields(DataSet: TDataSet);
begin
  QrBalancosItsNOMEGRUPO.Value := 'N�o dispon�vel ainda.';
end;

procedure TFmPQB2.QrCliDestAfterScroll(DataSet: TDataSet);
begin
  ReindexaTabela();
end;

procedure TFmPQB2.QrCliDestBeforeClose(DataSet: TDataSet);
begin
  QrBalancosIts.Close;
  BtAjusta.Enabled := False;
  BtResgata.Enabled := False;
  BtExclui.Enabled := False;
end;

procedure TFmPQB2.FormResize(Sender: TObject);
begin
  ReEntitula();
end;

procedure TFmPQB2.frxQUI_BALAN_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VAR_CINOME' then
    Value := QrCliDestNO_ENT.Value
  else
  if VarName = 'VAR_MES' then
    Value := QrBalancosPeriodo2.Value
  else
  if VarName = 'VAR_MES_MES' then
    Value := QrBalancosPeriodo2.Value
  else
end;

function TFmPQB2.GeraDadosEstoqueAnterior(const Entidade: Integer; var Tabela: String): Boolean;
var
  Periodo: Integer;
  Qry: TmySQLQUery;
  Data: TDateTime;
  DataI, DataF: String;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    Periodo := QrBalancosPeriodo.Value;
    Data    := Geral.PeriodoToDate(Periodo, 1, False, detJustSum);
    DataF   := Geral.FDT(Data, 1);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(Periodo) Periodo ',
    'FROM balancos ',
    'WHERE Periodo<' + Geral.FF0(Periodo),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    '']);
    //
    Periodo := Qry.FieldByName('Periodo').AsInteger;
    Data    := Geral.PeriodoToDate(Periodo, 1, False, detJustSum);
    DataI   := Geral.FDT(Data, 1);
    //
    Tabela := UnCreateBlueDerm.RecriaTempTableNovo(ntrttPQB2_Resgata, DModG.QrUpdPID1, False);
    //
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + Tabela,
    'SELECT pqx.Insumo, SUM(pqx.Peso) Peso, SUM(pqx.Valor) Valor, ',
    'IF(SUM(pqx.Peso) = 0 , 0, SUM(pqx.Valor) / SUM(pqx.Peso)) PRECO, ',
    'pq.Nome NO_PQ, 0 QtdBal, 1 Ativo ',
    'FROM ' + TMeuDB + '.pqx pqx ',
    'LEFT JOIN ' + TMeuDB + '.pq pq ON pq.Codigo=pqx.Insumo ',
    'WHERE pqx.Insumo>0',
    'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
    'AND pqx.CliOrig=' + Geral.FF0(Entidade),
    'AND ( ',
    '    pqx.DataX >="' + DataI + '" ',
    '    AND ',
    '    pqx.DataX < "' + DataF + '" ',
    ') ',
    'GROUP BY pqx.Insumo ',
    'ORDER BY NO_PQ ',
    '']);
  finally
    Qry.Free;
  end;
end;

procedure TFmPQB2.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPQB2.QrBalancosAfterScroll(DataSet: TDataSet);
begin
  ReEntitula();
  ReopenCliDest(0);
end;

procedure TFmPQB2.QrBalancosBeforeClose(DataSet: TDataSet);
begin
  QrCliDest.Close;
end;

procedure TFmPQB2.BtPrivativoClick(Sender: TObject);
const
  Unitario = False;
var
  Conta: Double;
  DataX, DtCorrApo: String;
  OriCodi, OriCtrl, OriTipo, CliOrig, CliDest, Insumo: Integer;
  Peso, Valor: Double;
  DataIni: TDateTime;
begin
  DtCorrApo := Geral.FDT(0, 1);
  DataX := dmkPF.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtSystem);
  if Geral.MB_Pergunta('Todos itens deste balan�o ser�o excluidos, '
  +'e ser�o inclu�dos itens para zerar estoques negativos. Deseja executar '+
  'esta a��o assim mesmo?') = ID_YES then
  begin
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM balancosits WHERE Periodo=:P0 ');
    Dmod.QrUpdU.SQL.Add('AND Empresa=:P1 ');
    Dmod.QrUpdU.Params[00].AsInteger := QrBalancosPeriodo.Value;
    Dmod.QrUpdU.Params[01].AsInteger := QrBalancosEmpresa.Value;
    Dmod.QrUpdU.ExecSQL;
    //
  end else
    Exit;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE pqcli SET Peso=0, Valor=0');
  Dmod.QrUpdU.ExecSQL;

  //////////////////////////////////////////////////////////////////////////////
(*
  QrAtualiza.Close;
  UnDmkDAC_PF.AbreQuery(QrAtualiza, Dmod.MyDB);
  Progress1.Position := 0;
  Progress1.Max := QrAtualiza.RecordCount;
  PainelDados2.Visible := True;
  PainelDados2.Refresh;
  GBCntrl.Refresh;
  Panel1.Refresh;
  Panel2.Refresh;
  TimerIni := Now();
  //  fazer apenas uma vez para n�o demorar o UnPQx.AtualizaEstoquePQ !!!
  DMod.AtualizaStqMovIts_PQX(nil, nil, nil);
  //
  while not QrAtualiza.Eof do
  begin
    Progress1.Position := Progress1.Position + 1;
    PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
    PnRegistros.Refresh;
    PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
    PnTempo.Refresh;
    UnPQx.AtualizaEstoquePQ(QrAtualizaCI.Value, QrAtualizaPQ.Value, QrAtualizaEmpresa.Value, aeNenhum, CO_VAZIO, False);
    QrAtualiza.Next;
  end;
  //PainelDados2.Visible := False;
  QrAtualiza.Close;
*)
    DataIni    := Geral.PeriodoToDate(QrBalancosPeriodo.Value, 1, False, detJustSum);
    if not PQ_PF.AtualizaTodosPQsPosBalanco(aeNenhum, Progress1, PainelDados2,
    PnRegistros, PnTempo, TimerIni, DataIni) then
      Exit;

  //////////////////////////////////////////////////////////////////////////////

  QrProdutos.Close;
  UnDmkDAC_PF.AbreQuery(QrProdutos, Dmod.MyDB);
  Progress1.Position := 0;
  Progress1.Max := QrProdutos.RecordCount;
  while not QrProdutos.Eof do
  begin
    Progress1.Position := Progress1.Position + 1;
    PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
    PnRegistros.Refresh;
    PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
    PnTempo.Refresh;
    //
    Conta := UMyMod.BuscaEmLivreY_Double(
    Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
(*
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO p q x SET Tipo=0, Peso=:P0, Valor=:P1, ');
    Dmod.QrUpdU.SQL.Add('Insumo=:P2, CliOrig=:P3, CliDest=:P4, Datax=:P5, ');
    Dmod.QrUpdU.SQL.Add('OrigemCodi=:P6, OrigemCtrl=:P7 ');
    Dmod.QrUpdU.Params[00].AsFloat   := -QrProdutosPeso.Value;
    Dmod.QrUpdU.Params[01].AsFloat   := -QrProdutosValor.Value;
    Dmod.QrUpdU.Params[02].AsInteger :=  QrProdutosPQ.Value;
    Dmod.QrUpdU.Params[03].AsInteger :=  QrProdutosCI.Value;
    Dmod.QrUpdU.Params[04].AsInteger :=  QrProdutosCI.Value;
    Dmod.QrUpdU.Params[05].AsString  := DataX;
    Dmod.QrUpdU.Params[06].AsInteger := QrBalancosPeriodo.Value;
    Dmod.QrUpdU.Params[07].AsFloat   := Conta;
    Dmod.QrUpdU.ExecSQL;
*)
    //DataX   := DataX;
    OriCodi := QrBalancosPeriodo.Value;
    OriCtrl := Trunc(Conta);
    OriTipo := VAR_FATID_0000;
    CliOrig := QrProdutosCI.Value;
    CliDest := QrProdutosCI.Value;
    Insumo  := QrProdutosPQ.Value;
    Peso    := -QrProdutosPeso.Value;
    Valor   := -QrProdutosValor.Value;
    PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
      OriCtrl, OriTipo, Unitario, DtCorrApo, FEmpresa);
    QrProdutos.Next;
  end;
  PQ_PF.VerificaEquiparacaoEstoque(True);
  //////////////////////////////////////////////////////////////////////////////
  QrProdutos.Close;
  PainelDados2.Visible := False;
  LocCod(QrBalancosPeriodo.Value, QrBalancosPeriodo.Value);
end;

end.

