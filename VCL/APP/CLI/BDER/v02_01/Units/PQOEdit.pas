unit PQOEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit,
  dmkEditDateTimePicker;

type
  TFmPQOEdit = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label13: TLabel;
    EdPeso: TdmkEdit;
    EdValor: TdmkEdit;
    Label1: TLabel;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CkDtCorrApoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCliInt, FInsumo, FOrigemCodi, FOrigemCtrl, FTipo, FCliOrig,
    FCliDest, FEmpresa: Integer;
    FDataX: TDateTime;
  end;

  var
  FmPQOEdit: TFmPQOEdit;

implementation

uses UnMyObjects, Module, UMySQLModule, PQx, UnPQ_PF, PQO;

{$R *.DFM}

procedure TFmPQOEdit.BtOKClick(Sender: TObject);
const
  Unitario = True;
var
  Controle: Integer;
  KG, RS, SF: Double;
  //
  DataX, DtCorrApo: String;
  OriCodi, OriCtrl, OriTipo, CliOrig, CliDest, Insumo: Integer;
  Peso, Valor: Double;
begin
  Peso  := EdPeso.ValueVariant;
  Valor := EdValor.ValueVariant;
  //
  if MyObjects.FIC(Peso <= 0, EdPeso, 'Quantidade inv�lida!') then Exit;
  if MyObjects.FIC(Valor = 0, EdValor, 'Defina o valor!') then Exit;
  //
(*
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'p q x', False, ['Peso', 'Valor'],
    ['OrigemCodi', 'OrigemCtrl', 'Tipo'], [-Qtd, -Valor],
    [FOrigemCodi, FOrigemCtrl, FTipo], False) then
*)
  FmPQO.ExcluiItemSelecionado(True);
  //
  DataX      := Geral.FDT(FDataX, 1);
  OriCodi    := FOrigemCodi;
  OriCtrl    := FOrigemCtrl;
  OriTipo    := FTipo;
  CliOrig    := FCliOrig;
  CliDest    := FCliDest;
  Insumo     := FInsumo;
  Peso       := -Peso;
  Valor      := -Valor;
  DtCorrApo  := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
  //if
  PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
  OriCtrl, OriTipo, Unitario, DtCorrApo, FEmpresa); (*then
  begin*)
    UnPQx.AtualizaEstoquePQ(FCliInt, FInsumo, FEmpresa, aeNenhum, '');
    PQ_PF.VerificaEquiparacaoEstoque(True);
    //
    Close;
  //end;
end;

procedure TFmPQOEdit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQOEdit.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmPQOEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQOEdit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPQOEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
