object FmEntiMP: TFmEntiMP
  Left = 242
  Top = 168
  Caption = 'COU-MP_IN-002 :: Fornecedores de Mat'#233'ria-prima'
  ClientHeight = 699
  ClientWidth = 1020
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelBase: TPanel
    Left = 0
    Top = 651
    Width = 1020
    Height = 48
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object PnControla: TPanel
      Left = 0
      Top = 0
      Width = 441
      Height = 48
      Align = alLeft
      TabOrder = 1
      object BtInclui: TBitBtn
        Tag = 10
        Left = 10
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 106
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 198
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtExcluiClick
      end
      object Panel2: TPanel
        Left = 312
        Top = 1
        Width = 128
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object BtSaida: TBitBtn
          Tag = 13
          Left = 18
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
    end
    object PnConfirma: TPanel
      Left = 441
      Top = 0
      Width = 579
      Height = 48
      Align = alClient
      TabOrder = 0
      Visible = False
      object Label10: TLabel
        Left = 108
        Top = 1
        Width = 216
        Height = 13
        Caption = '*'#185': Limite percentual (%) de quebra de viagem.'
      end
      object Label12: TLabel
        Left = 108
        Top = 16
        Width = 232
        Height = 13
        Caption = '*'#178': Letra(s) da proced'#234'ncia + formata'#231#227'o da data.'
      end
      object Label13: TLabel
        Left = 108
        Top = 32
        Width = 318
        Height = 13
        Caption = '*'#179': 1=Di'#225'rio, 7=Semanal, 10=Decendial, 15=Quinzenal e 30=Mensal'
      end
      object Panel6: TPanel
        Left = 464
        Top = 1
        Width = 114
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 10
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Confirma'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 1020
    Height = 559
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 744
      Top = 0
      Height = 140
      ExplicitLeft = 745
      ExplicitTop = 1
      ExplicitHeight = 75
    end
    object PnInsUpd: TPanel
      Left = 0
      Top = 140
      Width = 1020
      Height = 419
      Align = alBottom
      TabOrder = 0
      Visible = False
      object Panel3: TPanel
        Left = 1
        Top = 1
        Width = 1018
        Height = 96
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Panel14: TPanel
          Left = 0
          Top = 0
          Width = 1018
          Height = 48
          Align = alTop
          TabOrder = 0
          object Panel5: TPanel
            Left = 1
            Top = 1
            Width = 524
            Height = 46
            Align = alLeft
            BevelOuter = bvLowered
            TabOrder = 0
            object LaFornecedor: TLabel
              Left = 8
              Top = 4
              Width = 137
              Height = 13
              Caption = 'Fornecedor de mat'#233'ria-prima:'
            end
            object CBEntiMP: TdmkDBLookupComboBox
              Left = 48
              Top = 20
              Width = 469
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMEENTI'
              ListSource = DsEntidades
              TabOrder = 1
              dmkEditCB = EdEntiMP
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdEntiMP: TdmkEditCB
              Left = 8
              Top = 20
              Width = 37
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEntiMP
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
          end
        end
        object Panel15: TPanel
          Left = 0
          Top = 48
          Width = 1018
          Height = 48
          Align = alClient
          TabOrder = 1
          object Panel13: TPanel
            Left = 1
            Top = 1
            Width = 128
            Height = 46
            Align = alLeft
            BevelOuter = bvLowered
            TabOrder = 0
            object Label25: TLabel
              Left = 4
              Top = 4
              Width = 81
              Height = 13
              Caption = 'Lote (M'#225'scara)*'#178':'
            end
            object EdLoteLetras: TdmkEdit
              Left = 4
              Top = 20
              Width = 49
              Height = 24
              CharCase = ecUpperCase
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'ABC'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'ABC'
              ValWarn = False
            end
            object EdLoteFormat: TdmkEdit
              Left = 56
              Top = 20
              Width = 65
              Height = 24
              CharCase = ecLowerCase
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'sa'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'sa'
              ValWarn = False
            end
          end
          object Panel4: TPanel
            Left = 129
            Top = 1
            Width = 128
            Height = 46
            Align = alLeft
            BevelOuter = bvLowered
            TabOrder = 1
            object Label11: TLabel
              Left = 4
              Top = 4
              Width = 90
              Height = 13
              Caption = 'Marca (M'#225'scara)*'#178':'
            end
            object EdMaskLetras: TdmkEdit
              Left = 4
              Top = 20
              Width = 49
              Height = 24
              CharCase = ecUpperCase
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'ABC'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'ABC'
              ValWarn = False
            end
            object EdMaskFormat: TdmkEdit
              Left = 56
              Top = 20
              Width = 65
              Height = 24
              CharCase = ecLowerCase
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'yymmdd'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'yymmdd'
              ValWarn = False
            end
          end
          object RgPreDescarn: TRadioGroup
            Left = 257
            Top = 1
            Width = 760
            Height = 46
            Align = alClient
            Caption = ' Pr'#233'-descarne: '
            Columns = 3
            Items.Strings = (
              'No curtume '
              'N'#227'o pr'#233'-descarna'
              'J'#225' vem pr'#233'-descarnado')
            TabOrder = 2
          end
        end
      end
      object Panel7: TPanel
        Left = 1
        Top = 97
        Width = 916
        Height = 321
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox4: TGroupBox
          Left = 0
          Top = 177
          Width = 916
          Height = 59
          Align = alTop
          Caption = ' Comiss'#227'o na compra: '
          TabOrder = 3
          object RgCMIUnida: TRadioGroup
            Left = 2
            Top = 15
            Width = 163
            Height = 42
            Align = alLeft
            Caption = ' Unidade referencial: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'kg'
              'Pe'#231'a'
              'm'#178)
            TabOrder = 0
          end
          object RgCMIMoeda: TRadioGroup
            Left = 165
            Top = 15
            Width = 185
            Height = 42
            Align = alLeft
            Caption = ' Moeda: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'R$'
              'U$'
              #8364'$'
              'IDX')
            TabOrder = 1
          end
          object RgCMIPerio: TRadioGroup
            Left = 350
            Top = 15
            Width = 203
            Height = 42
            Align = alLeft
            Caption = ' Periodicidade do pagamento: '
            Columns = 5
            Items.Strings = (
              '1'
              '7'
              '10'
              '15'
              '30')
            TabOrder = 2
          end
          object Panel11: TPanel
            Left = 553
            Top = 15
            Width = 188
            Height = 42
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 3
            object Label20: TLabel
              Left = 4
              Top = 1
              Width = 59
              Height = 13
              Caption = 'Pre'#231'o Unid: '
            end
            object Label21: TLabel
              Left = 64
              Top = 1
              Width = 67
              Height = 13
              Caption = 'Frete viagem: '
            end
            object Label22: TLabel
              Left = 132
              Top = 3
              Width = 42
              Height = 13
              Caption = '% LPQV:'
            end
            object dmkEdCMI_LPQV: TdmkEdit
              Left = 132
              Top = 18
              Width = 49
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdCMIPreco: TdmkEdit
              Left = 4
              Top = 17
              Width = 57
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdCMIFrete: TdmkEdit
              Left = 64
              Top = 17
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object RgCMI_IDQV: TRadioGroup
            Left = 741
            Top = 15
            Width = 173
            Height = 42
            Align = alClient
            Caption = ' Devolu'#231#227'o (LPQV*'#185'): '
            Columns = 3
            Items.Strings = (
              'Nada'
              'Tudo'
              'Exced.')
            TabOrder = 4
          end
        end
        object GroupBox3: TGroupBox
          Left = 0
          Top = 118
          Width = 916
          Height = 59
          Align = alTop
          Caption = ' '#193'gio na compra: '
          TabOrder = 2
          object RgAGIUnida: TRadioGroup
            Left = 2
            Top = 15
            Width = 163
            Height = 42
            Align = alLeft
            Caption = ' Unidade referencial: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'kg'
              'Pe'#231'a'
              'm'#178)
            TabOrder = 0
          end
          object RgAGIMoeda: TRadioGroup
            Left = 165
            Top = 15
            Width = 185
            Height = 42
            Align = alLeft
            Caption = ' Moeda: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'R$'
              'U$'
              #8364'$'
              'IDX')
            TabOrder = 1
          end
          object RgAGIPerio: TRadioGroup
            Left = 350
            Top = 15
            Width = 203
            Height = 42
            Align = alLeft
            Caption = ' Periodicidade do pagamento: '
            Columns = 5
            Items.Strings = (
              '1'
              '7'
              '10'
              '15'
              '30')
            TabOrder = 2
          end
          object Panel10: TPanel
            Left = 553
            Top = 15
            Width = 188
            Height = 42
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 3
            object Label17: TLabel
              Left = 4
              Top = 1
              Width = 59
              Height = 13
              Caption = 'Pre'#231'o Unid: '
            end
            object Label18: TLabel
              Left = 64
              Top = 1
              Width = 67
              Height = 13
              Caption = 'Frete viagem: '
            end
            object Label19: TLabel
              Left = 132
              Top = 3
              Width = 42
              Height = 13
              Caption = '% LPQV:'
            end
            object dmkEdAGI_LPQV: TdmkEdit
              Left = 132
              Top = 18
              Width = 49
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdAGIPreco: TdmkEdit
              Left = 5
              Top = 17
              Width = 57
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdAGIFrete: TdmkEdit
              Left = 64
              Top = 17
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object RgAGI_IDQV: TRadioGroup
            Left = 741
            Top = 15
            Width = 173
            Height = 42
            Align = alClient
            Caption = ' Devolu'#231#227'o (LPQV*'#185'): '
            Columns = 3
            Items.Strings = (
              'Nada'
              'Tudo'
              'Exced.')
            TabOrder = 4
          end
        end
        object GroupBox2: TGroupBox
          Left = 0
          Top = 59
          Width = 916
          Height = 59
          Align = alTop
          Caption = ' Complemento de pagamento: '
          TabOrder = 1
          object RgCPLUnida: TRadioGroup
            Left = 2
            Top = 15
            Width = 163
            Height = 42
            Align = alLeft
            Caption = ' Unidade referencial: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'kg'
              'Pe'#231'a'
              'm'#178)
            TabOrder = 0
          end
          object RgCPLMoeda: TRadioGroup
            Left = 165
            Top = 15
            Width = 185
            Height = 42
            Align = alLeft
            Caption = ' Moeda: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'R$'
              'U$'
              #8364'$'
              'IDX')
            TabOrder = 1
          end
          object RgCPLPerio: TRadioGroup
            Left = 350
            Top = 15
            Width = 203
            Height = 42
            Align = alLeft
            Caption = ' Periodicidade do pagamento: '
            Columns = 5
            Items.Strings = (
              '1'
              '7'
              '10'
              '15'
              '30')
            TabOrder = 2
          end
          object Panel9: TPanel
            Left = 553
            Top = 15
            Width = 188
            Height = 42
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 3
            object Label1: TLabel
              Left = 4
              Top = 1
              Width = 59
              Height = 13
              Caption = 'Pre'#231'o Unid: '
            end
            object Label15: TLabel
              Left = 64
              Top = 1
              Width = 67
              Height = 13
              Caption = 'Frete viagem: '
            end
            object Label16: TLabel
              Left = 132
              Top = 3
              Width = 42
              Height = 13
              Caption = '% LPQV:'
            end
            object dmkEdCPL_LPQV: TdmkEdit
              Left = 132
              Top = 18
              Width = 49
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdCPLPreco: TdmkEdit
              Left = 4
              Top = 17
              Width = 57
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdCPLFrete: TdmkEdit
              Left = 64
              Top = 17
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object RgCPL_IDQV: TRadioGroup
            Left = 741
            Top = 15
            Width = 173
            Height = 42
            Align = alClient
            Caption = ' Devolu'#231#227'o (LPQV*'#185'): '
            Columns = 3
            Items.Strings = (
              'Nada'
              'Tudo'
              'Exced.')
            TabOrder = 4
          end
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 916
          Height = 59
          Align = alTop
          Caption = ' Compra de mat'#233'ria-prima: '
          TabOrder = 0
          object RgCMPUnida: TRadioGroup
            Left = 2
            Top = 15
            Width = 163
            Height = 42
            Align = alLeft
            Caption = ' Unidade referencial: '
            Columns = 3
            ItemIndex = 1
            Items.Strings = (
              'kg'
              'Pe'#231'a'
              'm'#178)
            TabOrder = 0
          end
          object RgCMPMoeda: TRadioGroup
            Left = 165
            Top = 15
            Width = 185
            Height = 42
            Align = alLeft
            Caption = ' Moeda: '
            Columns = 4
            ItemIndex = 1
            Items.Strings = (
              'R$'
              'U$'
              #8364'$'
              'IDX')
            TabOrder = 1
          end
          object RgCMPPerio: TRadioGroup
            Left = 350
            Top = 15
            Width = 203
            Height = 42
            Align = alLeft
            Caption = ' Periodicidade do pagamento*'#179': '
            Columns = 5
            ItemIndex = 4
            Items.Strings = (
              '1'
              '7'
              '10'
              '15'
              '30')
            TabOrder = 2
          end
          object RgCMP_IDQV: TRadioGroup
            Left = 741
            Top = 15
            Width = 173
            Height = 42
            Align = alClient
            Caption = ' Devolu'#231#227'o (LPQV*'#185'): '
            Columns = 3
            Items.Strings = (
              'Nada'
              'Tudo'
              'Exced.')
            TabOrder = 4
          end
          object Panel8: TPanel
            Left = 553
            Top = 15
            Width = 188
            Height = 42
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 3
            object Label2: TLabel
              Left = 4
              Top = 1
              Width = 59
              Height = 13
              Caption = 'Pre'#231'o Unid: '
            end
            object Label3: TLabel
              Left = 64
              Top = 1
              Width = 67
              Height = 13
              Caption = 'Frete viagem: '
            end
            object Label14: TLabel
              Left = 132
              Top = 3
              Width = 42
              Height = 13
              Caption = '% LPQV:'
            end
            object dmkEdCMPPreco: TdmkEdit
              Left = 4
              Top = 18
              Width = 57
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdCMPFrete: TdmkEdit
              Left = 64
              Top = 18
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdCMP_LPQV: TdmkEdit
              Left = 132
              Top = 18
              Width = 49
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
        object Panel12: TPanel
          Left = 0
          Top = 236
          Width = 916
          Height = 85
          Align = alClient
          TabOrder = 4
          object Label01: TLabel
            Left = 748
            Top = 4
            Width = 45
            Height = 13
            Caption = 'Contatos:'
          end
          object Label4: TLabel
            Left = 416
            Top = 4
            Width = 83
            Height = 13
            Caption = 'Local de entrega:'
          end
          object Label5: TLabel
            Left = 4
            Top = 44
            Width = 40
            Height = 13
            Caption = 'Corretor:'
          end
          object Label6: TLabel
            Left = 416
            Top = 44
            Width = 59
            Height = 13
            Caption = '% Comiss'#227'o:'
          end
          object Label7: TLabel
            Left = 584
            Top = 4
            Width = 124
            Height = 13
            Caption = 'Condi'#231#245'es de pagamento:'
          end
          object Label8: TLabel
            Left = 500
            Top = 44
            Width = 124
            Height = 13
            Caption = 'Condi'#231#245'es de pagamento:'
          end
          object Label9: TLabel
            Left = 684
            Top = 44
            Width = 116
            Height = 13
            Caption = 'Desconto adiantamento:'
          end
          object Label23: TLabel
            Left = 4
            Top = 3
            Width = 54
            Height = 13
            Caption = 'Transporte:'
          end
          object SpeedButton3: TSpeedButton
            Left = 392
            Top = 19
            Width = 21
            Height = 21
          end
          object Label24: TLabel
            Left = 804
            Top = 44
            Width = 63
            Height = 13
            Caption = 'Saldo Inicial: '
          end
          object EdContatos: TdmkEdit
            Left = 748
            Top = 20
            Width = 160
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLocalEntrg: TdmkEdit
            Left = 416
            Top = 20
            Width = 160
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCorretor: TdmkEditCB
            Left = 4
            Top = 60
            Width = 37
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCorretor
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCorretor: TdmkDBLookupComboBox
            Left = 44
            Top = 60
            Width = 345
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTI'
            ListSource = DsCorretores
            TabOrder = 6
            dmkEditCB = EdCorretor
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object dmkEdComissao: TdmkEdit
            Left = 416
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCondPagto: TdmkEdit
            Left = 584
            Top = 20
            Width = 160
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCondComis: TdmkEdit
            Left = 500
            Top = 60
            Width = 180
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object dmkEdDescAdiant: TdmkEdit
            Left = 684
            Top = 60
            Width = 117
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTransporte: TdmkEditCB
            Left = 4
            Top = 19
            Width = 48
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBTransporte
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBTransporte: TdmkDBLookupComboBox
            Left = 52
            Top = 19
            Width = 337
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsTransporte
            TabOrder = 1
            dmkEditCB = EdTransporte
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object dmkEdSdoIniPele: TdmkEdit
            Left = 804
            Top = 60
            Width = 105
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-1000000'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 917
        Top = 97
        Width = 102
        Height = 321
        Align = alClient
        Caption = ' Complemento: '
        TabOrder = 2
        object RGCPLSit: TRadioGroup
          Left = 2
          Top = 15
          Width = 98
          Height = 64
          Align = alTop
          Caption = ' Fonte: '
          ItemIndex = 0
          Items.Strings = (
            'Nota fiscal'
            'Entrada')
          TabOrder = 0
        end
        object RGAbate: TRadioGroup
          Left = 2
          Top = 79
          Width = 98
          Height = 105
          Align = alTop
          Caption = ' Tipo de abate: '
          ItemIndex = 1
          Items.Strings = (
            'Matadouro'
            'Frigor'#237'fico')
          TabOrder = 1
        end
        object Memo1: TMemo
          Left = 2
          Top = 184
          Width = 98
          Height = 135
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            '*'#178' formata'#231#227'o:'
            '=============='
            'yy = ano'
            'mm = m'#234's'
            'dd = dia'
            'sa = sem.ano'
            'sm = sem.m'#234's')
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          WordWrap = False
        end
      end
    end
    object PageControl2: TPageControl
      Left = 0
      Top = 0
      Width = 744
      Height = 140
      ActivePage = TabSheet5
      Align = alLeft
      TabOrder = 1
      object TabSheet5: TTabSheet
        Caption = 'Dados do fornecedor de mat'#233'ria prima'
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 736
          Height = 112
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'EntiMP'
              Title.Caption = 'C'#243'digo'
              Width = 39
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEEMP'
              Title.Caption = 'Nome'
              Width = 343
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PreDescarnNOME'
              Title.Caption = 'Pr'#233'-descarne'
              Width = 116
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MaskLetras'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = [fsBold]
              Title.Caption = 'Letras'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MaskFormat'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = [fsBold]
              Title.Caption = 'Data fmt'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LPQV'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPLSit_NOME'
              Title.Caption = 'Complemeto'
              Width = 66
              Visible = True
            end>
          Color = clWindow
          DataSource = DsEntiMP
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          FieldsCalcToOrder.Strings = (
            'PreDescarnNOME=PreDescarn'
            'IDQV_NOME=IDQV'
            'CPLSit_NOME=CPLSit')
          Columns = <
            item
              Expanded = False
              FieldName = 'EntiMP'
              Title.Caption = 'C'#243'digo'
              Width = 39
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEEMP'
              Title.Caption = 'Nome'
              Width = 343
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PreDescarnNOME'
              Title.Caption = 'Pr'#233'-descarne'
              Width = 116
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MaskLetras'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = [fsBold]
              Title.Caption = 'Letras'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MaskFormat'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = [fsBold]
              Title.Caption = 'Data fmt'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LPQV'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPLSit_NOME'
              Title.Caption = 'Complemeto'
              Width = 66
              Visible = True
            end>
        end
      end
    end
    object PageControl1: TPageControl
      Left = 747
      Top = 0
      Width = 273
      Height = 140
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Compra '
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 265
          Height = 112
          Align = alClient
          DataSource = DsEntiMP
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CMPUNIDANOME'
              Title.Caption = 'Unidade'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CMPMOEDANOME'
              Title.Caption = 'Moeda'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CMPPERIONOME'
              Title.Caption = 'Per'#237'odo'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CMPPreco'
              Title.Caption = 'Pre'#231'o'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CMPFrete'
              Title.Caption = 'Frete'
              Width = 52
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Complemento '
        ImageIndex = 1
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 265
          Height = 112
          Align = alClient
          DataSource = DsEntiMP
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CPLUNIDANOME'
              Title.Caption = 'Unidade'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPLMOEDANOME'
              Title.Caption = 'Moeda'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPLPERIONOME'
              Title.Caption = 'Per'#237'odo'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPLPreco'
              Title.Caption = 'Pre'#231'o'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPLFrete'
              Title.Caption = 'Frete'
              Width = 52
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' '#193'gio '
        ImageIndex = 2
        object DBGrid3: TDBGrid
          Left = 0
          Top = 0
          Width = 265
          Height = 112
          Align = alClient
          DataSource = DsEntiMP
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'AGIUNIDANOME'
              Title.Caption = 'Unidade'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AGIMOEDANOME'
              Title.Caption = 'Moeda'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AGIPERIONOME'
              Title.Caption = 'Per'#237'odo'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AGIPreco'
              Title.Caption = 'Pre'#231'o'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AGIFrete'
              Title.Caption = 'Frete'
              Width = 52
              Visible = True
            end>
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' Comiss'#227'o '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid4: TDBGrid
          Left = 0
          Top = 0
          Width = 259
          Height = 139
          Align = alClient
          DataSource = DsEntiMP
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CMIUNIDANOME'
              Title.Caption = 'Unidade'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CMIMOEDANOME'
              Title.Caption = 'Moeda'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CMIPERIONOME'
              Title.Caption = 'Per'#237'odo'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CMIPreco'
              Title.Caption = 'Pre'#231'o'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CMIFrete'
              Title.Caption = 'Frete'
              Width = 52
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1020
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 972
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 924
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 381
        Height = 32
        Caption = 'Fornecedores de Mat'#233'ria-prima'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 381
        Height = 32
        Caption = 'Fornecedores de Mat'#233'ria-prima'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 381
        Height = 32
        Caption = 'Fornecedores de Mat'#233'ria-prima'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1020
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel16: TPanel
      Left = 2
      Top = 15
      Width = 1016
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrEntiMP: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiMPCalcFields
    SQL.Strings = (
      'SELECT emp.*,  ent.Codigo EntiMP, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEEMP '
      'FROM entimp emp'
      'LEFT JOIN entidades ent ON ent.Codigo=emp.Codigo'
      ''
      'ORDER BY NOMEEMP')
    Left = 12
    Top = 8
    object QrEntiMPCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entimp.Codigo'
      Required = True
    end
    object QrEntiMPLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'entimp.Lk'
    end
    object QrEntiMPDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'entimp.DataCad'
    end
    object QrEntiMPDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'entimp.DataAlt'
    end
    object QrEntiMPUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'entimp.UserCad'
    end
    object QrEntiMPUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'entimp.UserAlt'
    end
    object QrEntiMPAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'entimp.AlterWeb'
      Required = True
    end
    object QrEntiMPAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'entimp.Ativo'
      Required = True
    end
    object QrEntiMPCMPUnida: TSmallintField
      FieldName = 'CMPUnida'
      Origin = 'entimp.CMPUnida'
      Required = True
    end
    object QrEntiMPCMPMoeda: TSmallintField
      FieldName = 'CMPMoeda'
      Origin = 'entimp.CMPMoeda'
      Required = True
    end
    object QrEntiMPCMPPerio: TSmallintField
      FieldName = 'CMPPerio'
      Origin = 'entimp.CMPPerio'
      Required = True
    end
    object QrEntiMPCMPPreco: TFloatField
      FieldName = 'CMPPreco'
      Origin = 'entimp.CMPPreco'
      Required = True
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrEntiMPCMPFrete: TFloatField
      FieldName = 'CMPFrete'
      Origin = 'entimp.CMPFrete'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEntiMPCPLUnida: TSmallintField
      FieldName = 'CPLUnida'
      Origin = 'entimp.CPLUnida'
      Required = True
    end
    object QrEntiMPCPLMoeda: TSmallintField
      FieldName = 'CPLMoeda'
      Origin = 'entimp.CPLMoeda'
      Required = True
    end
    object QrEntiMPCPLPerio: TSmallintField
      FieldName = 'CPLPerio'
      Origin = 'entimp.CPLPerio'
      Required = True
    end
    object QrEntiMPCPLPreco: TFloatField
      FieldName = 'CPLPreco'
      Origin = 'entimp.CPLPreco'
      Required = True
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrEntiMPCPLFrete: TFloatField
      FieldName = 'CPLFrete'
      Origin = 'entimp.CPLFrete'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEntiMPAGIUnida: TSmallintField
      FieldName = 'AGIUnida'
      Origin = 'entimp.AGIUnida'
      Required = True
    end
    object QrEntiMPAGIMoeda: TSmallintField
      FieldName = 'AGIMoeda'
      Origin = 'entimp.AGIMoeda'
      Required = True
    end
    object QrEntiMPAGIPerio: TSmallintField
      FieldName = 'AGIPerio'
      Origin = 'entimp.AGIPerio'
      Required = True
    end
    object QrEntiMPAGIPreco: TFloatField
      FieldName = 'AGIPreco'
      Origin = 'entimp.AGIPreco'
      Required = True
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrEntiMPAGIFrete: TFloatField
      FieldName = 'AGIFrete'
      Origin = 'entimp.AGIFrete'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEntiMPCMIUnida: TSmallintField
      FieldName = 'CMIUnida'
      Origin = 'entimp.CMIUnida'
      Required = True
    end
    object QrEntiMPCMIMoeda: TSmallintField
      FieldName = 'CMIMoeda'
      Origin = 'entimp.CMIMoeda'
      Required = True
    end
    object QrEntiMPCMIPerio: TSmallintField
      FieldName = 'CMIPerio'
      Origin = 'entimp.CMIPerio'
      Required = True
    end
    object QrEntiMPCMIPreco: TFloatField
      FieldName = 'CMIPreco'
      Origin = 'entimp.CMIPreco'
      Required = True
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrEntiMPCMIFrete: TFloatField
      FieldName = 'CMIFrete'
      Origin = 'entimp.CMIFrete'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEntiMPCMPUNIDANOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CMPUNIDANOME'
      Size = 10
      Calculated = True
    end
    object QrEntiMPCPLUNIDANOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPLUNIDANOME'
      Size = 10
      Calculated = True
    end
    object QrEntiMPAGIUNIDANOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'AGIUNIDANOME'
      Size = 10
      Calculated = True
    end
    object QrEntiMPCMIUNIDANOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CMIUNIDANOME'
      Size = 10
      Calculated = True
    end
    object QrEntiMPEntiMP: TIntegerField
      FieldName = 'EntiMP'
      Origin = 'entidades.Codigo'
    end
    object QrEntiMPCMPMOEDANOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CMPMOEDANOME'
      Size = 10
      Calculated = True
    end
    object QrEntiMPCPLMOEDANOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPLMOEDANOME'
      Size = 10
      Calculated = True
    end
    object QrEntiMPAGIMOEDANOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'AGIMOEDANOME'
      Size = 10
      Calculated = True
    end
    object QrEntiMPCMIMOEDANOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CMIMOEDANOME'
      Size = 10
      Calculated = True
    end
    object QrEntiMPNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Origin = 'NOMEEMP'
      Size = 100
    end
    object QrEntiMPCMPPERIONOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CMPPERIONOME'
      Size = 15
      Calculated = True
    end
    object QrEntiMPCPLPERIONOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPLPERIONOME'
      Size = 15
      Calculated = True
    end
    object QrEntiMPAGIPERIONOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'AGIPERIONOME'
      Size = 15
      Calculated = True
    end
    object QrEntiMPCMIPERIONOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CMIPERIONOME'
      Size = 15
      Calculated = True
    end
    object QrEntiMPPreDescarn: TSmallintField
      FieldName = 'PreDescarn'
      Origin = 'entimp.PreDescarn'
      Required = True
    end
    object QrEntiMPPreDescarnNOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PreDescarnNOME'
      Size = 30
      Calculated = True
    end
    object QrEntiMPMaskLetras: TWideStringField
      FieldName = 'MaskLetras'
      Origin = 'entimp.MaskLetras'
      Size = 10
    end
    object QrEntiMPMaskFormat: TWideStringField
      FieldName = 'MaskFormat'
      Origin = 'entimp.MaskFormat'
      Size = 10
    end
    object QrEntiMPCPLSit: TSmallintField
      FieldName = 'CPLSit'
      Origin = 'entimp.CPLSit'
      Required = True
    end
    object QrEntiMPCPLSit_NOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPLSit_NOME'
      Size = 15
      Calculated = True
    end
    object QrEntiMPCMP_LPQV: TFloatField
      FieldName = 'CMP_LPQV'
      Origin = 'entimp.CMP_LPQV'
      Required = True
    end
    object QrEntiMPCPL_LPQV: TFloatField
      FieldName = 'CPL_LPQV'
      Origin = 'entimp.CPL_LPQV'
      Required = True
    end
    object QrEntiMPAGI_LPQV: TFloatField
      FieldName = 'AGI_LPQV'
      Origin = 'entimp.AGI_LPQV'
      Required = True
    end
    object QrEntiMPCMI_LPQV: TFloatField
      FieldName = 'CMI_LPQV'
      Origin = 'entimp.CMI_LPQV'
      Required = True
    end
    object QrEntiMPCMP_IDQV: TSmallintField
      FieldName = 'CMP_IDQV'
      Origin = 'entimp.CMP_IDQV'
      Required = True
    end
    object QrEntiMPCPL_IDQV: TSmallintField
      FieldName = 'CPL_IDQV'
      Origin = 'entimp.CPL_IDQV'
      Required = True
    end
    object QrEntiMPAGI_IDQV: TSmallintField
      FieldName = 'AGI_IDQV'
      Origin = 'entimp.AGI_IDQV'
      Required = True
    end
    object QrEntiMPCMI_IDQV: TSmallintField
      FieldName = 'CMI_IDQV'
      Origin = 'entimp.CMI_IDQV'
      Required = True
    end
    object QrEntiMPContatos: TWideStringField
      FieldName = 'Contatos'
      Size = 100
    end
    object QrEntiMPLocalEntrg: TWideStringField
      FieldName = 'LocalEntrg'
      Size = 50
    end
    object QrEntiMPComissao: TFloatField
      FieldName = 'Comissao'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrEntiMPCondPagto: TWideStringField
      FieldName = 'CondPagto'
      Size = 30
    end
    object QrEntiMPCondComis: TWideStringField
      FieldName = 'CondComis'
      Size = 30
    end
    object QrEntiMPDescAdiant: TFloatField
      FieldName = 'DescAdiant'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEntiMPCorretor: TIntegerField
      FieldName = 'Corretor'
      Required = True
    end
    object QrEntiMPAbate: TIntegerField
      FieldName = 'Abate'
      Required = True
    end
    object QrEntiMPTransporte: TIntegerField
      FieldName = 'Transporte'
      Required = True
    end
    object QrEntiMPSdoIniPele: TFloatField
      FieldName = 'SdoIniPele'
      Required = True
    end
    object QrEntiMPLoteLetras: TWideStringField
      FieldName = 'LoteLetras'
      Size = 10
    end
    object QrEntiMPLoteFormat: TWideStringField
      FieldName = 'LoteFormat'
      Size = 10
    end
  end
  object DsEntiMP: TDataSource
    DataSet = QrEntiMP
    Left = 40
    Top = 8
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTI'
      'FROM entidades '
      'WHERE Fornece1="V"'
      'ORDER BY NOMEENTI')
    Left = 760
    Top = 16
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 788
    Top = 16
  end
  object QrCorretores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTI'
      'FROM entidades '
      'ORDER BY NOMEENTI')
    Left = 816
    Top = 16
    object QrCorretoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCorretoresNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Required = True
      Size = 100
    end
  end
  object DsCorretores: TDataSource
    DataSet = QrCorretores
    Left = 844
    Top = 16
  end
  object QrTransporte: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 72
    Top = 10
    object QrTransporteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransporteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransporte: TDataSource
    DataSet = QrTransporte
    Left = 100
    Top = 10
  end
end
