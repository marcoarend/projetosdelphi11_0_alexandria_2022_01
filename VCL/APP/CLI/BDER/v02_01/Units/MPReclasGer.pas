unit MPReclasGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, mySQLDbTables, DmkDAC_PF,
  Menus, ComCtrls, dmkEditDateTimePicker, UnInternalConsts, dmkImage,
  UnDmkEnums;

type
  TFmMPReclasGer = class(TForm)
    Panel1: TPanel;
    QrSMI: TmySQLQuery;
    DsSMI: TDataSource;
    Panel3: TPanel;
    DBGrid2: TDBGrid;
    QrNeg: TmySQLQuery;
    DsNeg: TDataSource;
    QrPos: TmySQLQuery;
    DsPos: TDataSource;
    QrSMISMIMultIns: TIntegerField;
    QrPosNivel1: TIntegerField;
    QrPosNO_PRD: TWideStringField;
    QrPosPrdGrupTip: TIntegerField;
    QrPosUnidMed: TIntegerField;
    QrPosNO_PGT: TWideStringField;
    QrPosSIGLA: TWideStringField;
    QrPosNO_TAM: TWideStringField;
    QrPosNO_COR: TWideStringField;
    QrPosGraCorCad: TIntegerField;
    QrPosGraGruC: TIntegerField;
    QrPosGraGru1: TIntegerField;
    QrPosGraTamI: TIntegerField;
    QrPosDataHora: TDateTimeField;
    QrPosIDCtrl: TIntegerField;
    QrPosTipo: TIntegerField;
    QrPosOriCodi: TIntegerField;
    QrPosOriCtrl: TIntegerField;
    QrPosOriCnta: TIntegerField;
    QrPosOriPart: TIntegerField;
    QrPosEmpresa: TIntegerField;
    QrPosStqCenCad: TIntegerField;
    QrPosGraGruX: TIntegerField;
    QrPosQtde: TFloatField;
    QrPosPecas: TFloatField;
    QrPosPeso: TFloatField;
    QrPosAlterWeb: TSmallintField;
    QrPosAtivo: TSmallintField;
    QrPosAreaM2: TFloatField;
    QrPosAreaP2: TFloatField;
    QrPosFatorClas: TFloatField;
    QrPosQuemUsou: TIntegerField;
    QrPosRetorno: TSmallintField;
    QrPosParTipo: TIntegerField;
    QrPosParCodi: TIntegerField;
    QrPosDebCtrl: TIntegerField;
    QrPosSMIMultIns: TIntegerField;
    QrNegNivel1: TIntegerField;
    QrNegNO_PRD: TWideStringField;
    QrNegPrdGrupTip: TIntegerField;
    QrNegUnidMed: TIntegerField;
    QrNegNO_PGT: TWideStringField;
    QrNegSIGLA: TWideStringField;
    QrNegNO_TAM: TWideStringField;
    QrNegNO_COR: TWideStringField;
    QrNegGraCorCad: TIntegerField;
    QrNegGraGruC: TIntegerField;
    QrNegGraGru1: TIntegerField;
    QrNegGraTamI: TIntegerField;
    QrNegDataHora: TDateTimeField;
    QrNegIDCtrl: TIntegerField;
    QrNegTipo: TIntegerField;
    QrNegOriCodi: TIntegerField;
    QrNegOriCtrl: TIntegerField;
    QrNegOriCnta: TIntegerField;
    QrNegOriPart: TIntegerField;
    QrNegEmpresa: TIntegerField;
    QrNegStqCenCad: TIntegerField;
    QrNegGraGruX: TIntegerField;
    QrNegQtde: TFloatField;
    QrNegPecas: TFloatField;
    QrNegPeso: TFloatField;
    QrNegAlterWeb: TSmallintField;
    QrNegAtivo: TSmallintField;
    QrNegAreaM2: TFloatField;
    QrNegAreaP2: TFloatField;
    QrNegFatorClas: TFloatField;
    QrNegQuemUsou: TIntegerField;
    QrNegRetorno: TSmallintField;
    QrNegParTipo: TIntegerField;
    QrNegParCodi: TIntegerField;
    QrNegDebCtrl: TIntegerField;
    QrNegSMIMultIns: TIntegerField;
    DBGrid3: TDBGrid;
    Panel4: TPanel;
    DBGrid1: TDBGrid;
    Panel5: TPanel;
    Label1: TLabel;
    TPDataI: TdmkEditDateTimePicker;
    Label2: TLabel;
    TPDataF: TdmkEditDateTimePicker;
    BtReabre: TBitBtn;
    QrSMIDataHora: TDateTimeField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSMIBeforeClose(DataSet: TDataSet);
    procedure QrSMIAfterScroll(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrSMIAfterOpen(DataSet: TDataSet);
    procedure TPDataIChange(Sender: TObject);
    procedure TPDataFChange(Sender: TObject);
    procedure TPDataIClick(Sender: TObject);
    procedure TPDataFClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
  private
    { Private declarations }
    procedure FechaSMI();

  public
    { Public declarations }
    procedure ReopenSMI(SMIMultIns: Integer);
  end;

  var
  FmMPReclasGer: TFmMPReclasGer;

implementation

uses UnMyObjects, Module, dmkGeral, UnGrade_Tabs, MPReclasIns, MyDBCheck, ModuleGeral,
UMySQLModule;

{$R *.DFM}

procedure TFmMPReclasGer.BtAlteraClick(Sender: TObject);
var
  Empresa, Filial: Integer;
begin
  if QrNeg.RecordCount > 0 then
    Empresa := QrNegEmpresa.Value
  else
  if QrNeg.RecordCount > 0 then
    Empresa := QrPosEmpresa.Value
  else
    Empresa := 0;
  if Empresa = 0 then
  begin
    Geral.MB_Aviso('N�o foi poss�vel definir a Empresa!');
    Exit;
  end;
  if DModG.QrFiliLog.Locate('Codigo', Empresa, []) then
    Filial := DModG.QrFiliLogFilial.Value
  else
    Filial := 0;
  if Filial = 0 then
  begin
    Geral.MB_Aviso('Filial n�o definida (n�o logada)!');
    Exit;
  end else begin
    if DBCheck.CriaFm(TFmMPReclasIns, FmMPReclasIns, afmoNegarComAviso) then
    begin
      FmMPReclasIns.ImgTipo.SQLType := stUpd;
      FmMPReclasIns.FForcaReopen := True;
      FmMPReclasIns.FSMIMultIns  := QrSMISMIMultIns.Value;
      FmMPReclasIns.EdFilial.ValueVariant := Filial;
      FmMPReclasIns.CBFilial.KeyValue     := Filial;
      FmMPReclasIns.TPData.Date           := QrSMIDataHora.Value;
      FmMPReclasIns.EdHora.ValueVariant   := Geral.FDT(QrSMIDataHora.Value, 100);
      //
      UMyMod.SetaCodUsuDeCodigo(
        FmMPReclasIns.EdStqCenCadA, FmMPReclasIns.CBStqCenCadA,
        FmMPReclasIns.QrStqCenCadA, QrNegStqCenCad.Value);
      FmMPReclasIns.FIDCtrlA                := QrNegIDCtrl.Value;
      FmMPReclasIns.EdGraGruXA.ValueVariant := QrNegGraGruX.Value;
      FmMPReclasIns.CBGraGruXA.KeyValue     := QrNegGraGruX.Value;
      FmMPReclasIns.EdPecasA.ValueVariant   := - QrNegPecas.Value;
      FmMPReclasIns.EdAreaM2A.ValueVariant  := - QrNegAreaM2.Value;
      FmMPReclasIns.EdAreaP2A.ValueVariant  := - QrNegAreaP2.Value;
      FmMPReclasIns.EdPesoA.ValueVariant    := - QrNegPeso.Value;
      //
      UMyMod.SetaCodUsuDeCodigo(
        FmMPReclasIns.EdStqCenCadB, FmMPReclasIns.CBStqCenCadB,
        FmMPReclasIns.QrStqCenCadB, QrPosStqCenCad.Value);
      FmMPReclasIns.FIDCtrlB                := QrPosIDCtrl.Value;
      FmMPReclasIns.EdGraGruXB.ValueVariant := QrPosGraGruX.Value;
      FmMPReclasIns.CBGraGruXB.KeyValue     := QrPosGraGruX.Value;
      FmMPReclasIns.EdPecasB.ValueVariant   := QrPosPecas.Value;
      FmMPReclasIns.EdAreaM2B.ValueVariant  := QrPosAreaM2.Value;
      FmMPReclasIns.EdAreaP2B.ValueVariant  := QrPosAreaP2.Value;
      FmMPReclasIns.EdPesoB.ValueVariant    := QrPosPeso.Value;
      //
      FmMPReclasIns.ShowModal;
      FmMPReclasIns.Destroy;
    end;
  end;
end;

procedure TFmMPReclasGer.BtExcluiClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da reclassifica��o selecionada?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE SMIMultIns > 0');
      Dmod.QrUpd.SQL.Add('AND SMIMultIns=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrSMISMIMultIns.Value;
      Dmod.QrUpd.ExecSQL;
      //
      {
      QrNeg.First;
      while not QrNeg.Eof do
      begin
        Dmod.AtualizaMPIn(QrNegOriCodi.Value);
        //
        QrNeg.Next;
      end;
      //
      QrPos.First;
      while not QrPos.Eof do
      begin
        Dmod.AtualizaMPIn(QrPosOriCodi.Value);
        //
        QrPos.Next;
      end;
      //
      }
      ReopenSMI(QrSMISMIMultIns.Value);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmMPReclasGer.BtIncluiClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPReclasIns, FmMPReclasIns, afmoNegarComAviso) then
  begin
    FmMPReclasIns.ImgTipo.SQLType := stIns;
    FmMPReclasIns.FForcaReopen := True;
    FmMPReclasIns.ShowModal;
    FmMPReclasIns.Destroy;
  end;
end;

procedure TFmMPReclasGer.BtReabreClick(Sender: TObject);
begin
  ReopenSMI(0);
end;

procedure TFmMPReclasGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPReclasGer.FechaSMI;
begin
  QrSMI.Close;
end;

procedure TFmMPReclasGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPReclasGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPDataI.Date := Date - 10;
  TPDataF.Date := Date;
  ReopenSMI(0);
end;

procedure TFmMPReclasGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPReclasGer.QrSMIAfterOpen(DataSet: TDataSet);
begin
  BtExclui.Enabled := QrSMI.RecordCount > 0;
  BtAltera.Enabled := QrSMI.RecordCount > 0;
end;

procedure TFmMPReclasGer.QrSMIAfterScroll(DataSet: TDataSet);
begin
  QrNeg.Close;
  QrNeg.Params[0].AsInteger := QrSMISMIMultIns.Value;
  UnDmkDAC_PF.AbreQuery(QrNeg, Dmod.MyDB);
  //
  QrPos.Close;
  QrPos.Params[0].AsInteger := QrSMISMIMultIns.Value;
  UnDmkDAC_PF.AbreQuery(QrPos, Dmod.MyDB);
end;

procedure TFmMPReclasGer.QrSMIBeforeClose(DataSet: TDataSet);
begin
  BtExclui.Enabled := False;
  BtAltera.Enabled := False;
  QrNeg.Close;
  QrPos.Close;
end;

procedure TFmMPReclasGer.ReopenSMI(SMIMultIns: Integer);
begin
  QrSMI.Close;
  QrSMI.SQL.Clear;
  QrSMI.SQL.Add('SELECT DISTINCT SMIMultIns, DataHora');
  QrSMI.SQL.Add('FROM stqmovitsa');
  QrSMI.SQL.Add('WHERE SMIMultIns > 0');
  QrSMI.SQL.Add('AND Tipo IN (' +
                 FormatFloat('0', VAR_FATID_0107) + ',' +
                 FormatFloat('0', VAR_FATID_0108) + ')');
  QrSMI.SQL.Add('AND DataHora >= "' +
                 Geral.FDT(TPDataI.Date, 1) + '" AND DataHora <"' +
                 Geral.FDT(TPDataF.Date + 1, 1) + '"');
  QrSMI.SQL.Add('ORDER BY SMIMultIns DESC');
{
SELECT DISTINCT SMIMultIns, DataHora
FROM stqmovitsa
WHERE SMIMultIns > 0
AND Tipo IN (107, 108)
AND DataHora >= "2011/03/20" AND DataHora <"2011/04/07"
ORDER BY SMIMultIns DESC
  QrSMI.Params[00].AsInteger := VAR_FATID_0107;
  QrSMI.Params[01].AsInteger := VAR_FATID_0108;
  QrSMI.Params[02].AsString  := Geral.FDT(TPDataI.Date, 1);
  QrSMI.Params[03].AsString  := Geral.FDT(TPDataF.Date, 1);
}
  //dmkPF.LeMeuTexto(QrSMI.SQL.Text);
  UnDmkDAC_PF.AbreQuery(QrSMI, Dmod.MyDB);
  //
  QrSMI.Locate('SMIMultIns', SMIMultIns, []);
end;

procedure TFmMPReclasGer.TPDataFChange(Sender: TObject);
begin
  FechaSMI();
end;

procedure TFmMPReclasGer.TPDataFClick(Sender: TObject);
begin
  FechaSMI();
end;

procedure TFmMPReclasGer.TPDataIChange(Sender: TObject);
begin
  FechaSMI();
end;

procedure TFmMPReclasGer.TPDataIClick(Sender: TObject);
begin
  FechaSMI();
end;

end.
