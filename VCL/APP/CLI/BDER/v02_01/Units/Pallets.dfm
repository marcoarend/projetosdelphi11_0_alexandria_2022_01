object FmPallets: TFmPallets
  Left = 341
  Top = 207
  Caption = 'CAD-BDERM-001 :: Pallets'
  ClientHeight = 618
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnAdicoes: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 522
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Panel7: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 293
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label7: TLabel
          Left = 4
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit01
        end
        object Label8: TLabel
          Left = 108
          Top = 4
          Width = 38
          Height = 13
          Caption = 'Meu ID:'
        end
        object Label11: TLabel
          Left = 212
          Top = 4
          Width = 26
          Height = 13
          Caption = 'Data:'
          FocusControl = DBEdit3
        end
        object Label12: TLabel
          Left = 304
          Top = 4
          Width = 75
          Height = 13
          Caption = 'Dono do Couro:'
          FocusControl = DBEdit4
        end
        object DBEdit01: TDBEdit
          Left = 4
          Top = 20
          Width = 100
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsPallets
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdit02: TDBEdit
          Left = 108
          Top = 20
          Width = 100
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Pallet'
          DataSource = DsPallets
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 212
          Top = 20
          Width = 89
          Height = 21
          DataField = 'DataI'
          DataSource = DsPallets
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 304
          Top = 20
          Width = 477
          Height = 21
          DataField = 'NOMECLIINT'
          DataSource = DsPallets
          TabOrder = 3
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 87
        Width = 792
        Height = 134
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label13: TLabel
          Left = 92
          Top = 4
          Width = 30
          Height = 13
          Caption = 'Artigo:'
        end
        object Label14: TLabel
          Left = 92
          Top = 44
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
        end
        object Label15: TLabel
          Left = 92
          Top = 84
          Width = 34
          Height = 13
          Caption = 'Classe:'
        end
        object Label16: TLabel
          Left = 8
          Top = 4
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label17: TLabel
          Left = 8
          Top = 44
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object Label18: TLabel
          Left = 8
          Top = 84
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
        end
        object EdMPArti: TdmkEditCB
          Left = 92
          Top = 20
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdMPArtiExit
          DBLookupComboBox = CBMPArti
          IgnoraDBLookupComboBox = False
        end
        object CBMPArti: TdmkDBLookupComboBox
          Left = 141
          Top = 20
          Width = 333
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsMPArti
          TabOrder = 4
          OnExit = CBMPArtiExit
          dmkEditCB = EdMPArti
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdMPGrup: TdmkEditCB
          Left = 92
          Top = 60
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdMPGrupExit
          DBLookupComboBox = CBMPGrup
          IgnoraDBLookupComboBox = False
        end
        object CBMPGrup: TdmkDBLookupComboBox
          Left = 141
          Top = 60
          Width = 333
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsMPGrup
          TabOrder = 6
          OnExit = CBMPGrupExit
          dmkEditCB = EdMPGrup
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdMPClas: TdmkEditCB
          Left = 92
          Top = 100
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdMPClasExit
          DBLookupComboBox = CBMPClas
          IgnoraDBLookupComboBox = False
        end
        object CBMPClas: TdmkDBLookupComboBox
          Left = 141
          Top = 100
          Width = 333
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          TabOrder = 8
          OnExit = CBMPClasExit
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPecas: TdmkEdit
          Left = 8
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
          OnExit = EdPecasExit
        end
        object EdM2: TdmkEdit
          Left = 8
          Top = 60
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdM2Exit
        end
        object EdP2: TdmkEdit
          Left = 8
          Top = 100
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdP2Exit
        end
        object RGMutaveis: TdmkCheckGroup
          Left = 476
          Top = 15
          Width = 305
          Height = 105
          Caption = ' Itens a preencher: '
          Columns = 2
          Items.Strings = (
            'Quantidade'
            #193'rea m'#178
            #193'rea ft'#178
            'Artigo (C'#243'digo)'
            'Artigo (Descri'#231#227'o)'
            'Tamanho (C'#243'digo)'
            'Tamanho (Descri'#231#227'o)'
            'Classe (C'#243'digo)'
            'Classe (Descri'#231#227'o)'
            'Bot'#227'o de confirma')
          TabOrder = 9
          OnClick = RGMutaveisClick
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
      end
      object FM2: TdmkEdit
        Left = 316
        Top = 232
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        Visible = False
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object FP2: TdmkEdit
        Left = 316
        Top = 272
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        Visible = False
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdM2Exit
      end
      object DBGOS: TDBGrid
        Left = 0
        Top = 48
        Width = 792
        Height = 39
        Align = alTop
        DataSource = DsPalet
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 4
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Data'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'OS'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLIENTEI'
            Title.Caption = 'Cliente interno'
            Width = 128
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEPROCEDENCIA'
            Title.Caption = 'Proced'#234'ncia'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Lote'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Marca'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 36
            Visible = True
          end>
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 458
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel12: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel13: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida2: TBitBtn
            Tag = 15
            Left = 12
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Sair'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            TabStop = False
            OnClick = BtSaida2Click
          end
        end
        object BtConfirma2: TBitBtn
          Tag = 14
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirma2Click
        end
        object BitBtn1: TBitBtn
          Left = 332
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Origem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          TabStop = False
          OnClick = BitBtn1Click
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 522
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 69
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 56
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 16
          Top = 8
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label10: TLabel
          Left = 120
          Top = 8
          Width = 38
          Height = 13
          Caption = 'Meu ID:'
        end
        object Label4: TLabel
          Left = 224
          Top = 8
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label5: TLabel
          Left = 316
          Top = 8
          Width = 74
          Height = 13
          Caption = 'Dono do couro:'
        end
        object EdCodigo: TdmkEdit
          Left = 16
          Top = 24
          Width = 100
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 2
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNome: TdmkEdit
          Left = 120
          Top = 24
          Width = 100
          Height = 21
          MaxLength = 255
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object TPDataI: TDateTimePicker
          Left = 224
          Top = 24
          Width = 89
          Height = 21
          Date = 38873.902085312500000000
          Time = 38873.902085312500000000
          TabOrder = 2
        end
        object EdClienteI: TdmkEditCB
          Left = 316
          Top = 24
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBClienteI
          IgnoraDBLookupComboBox = False
        end
        object CBClienteI: TdmkDBLookupComboBox
          Left = 365
          Top = 24
          Width = 333
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsDonos
          TabOrder = 4
          dmkEditCB = EdClienteI
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 458
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel11: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 54
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 522
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 120
        Top = 8
        Width = 38
        Height = 13
        Caption = 'Meu ID:'
      end
      object Label3: TLabel
        Left = 224
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 316
        Top = 8
        Width = 75
        Height = 13
        Caption = 'Dono do Couro:'
        FocusControl = DBEdit2
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPallets
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 120
        Top = 24
        Width = 100
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Pallet'
        DataSource = DsPallets
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 224
        Top = 24
        Width = 89
        Height = 21
        DataField = 'DataI'
        DataSource = DsPallets
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 316
        Top = 24
        Width = 469
        Height = 21
        DataField = 'NOMECLIINT'
        DataSource = DsPallets
        TabOrder = 3
      end
    end
    object PnCouros: TPanel
      Left = 0
      Top = 52
      Width = 792
      Height = 276
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 792
        Height = 276
        Align = alClient
        DataSource = DsPalletsIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object PnCourosIn: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 276
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 792
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object DBGrid2: TDBGrid
            Left = 0
            Top = 0
            Width = 792
            Height = 39
            Align = alTop
            DataSource = DsPalet
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Data'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'OS'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ficha'
                Title.Caption = 'F.RAMP'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECLIENTEI'
                Title.Caption = 'Cliente interno'
                Width = 128
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEPROCEDENCIA'
                Title.Caption = 'Proced'#234'ncia'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Lote'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Marca'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PLE'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PDA'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PTA'
                Visible = True
              end>
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 458
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 95
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 269
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel10: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtPallet: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Pallet'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtPalletClick
        end
        object BtCouros: TBitBtn
          Tag = 224
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Couros'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtCourosClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 81
        Height = 32
        Caption = 'Pallets'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 81
        Height = 32
        Caption = 'Pallets'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 81
        Height = 32
        Caption = 'Pallets'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsPallets: TDataSource
    DataSet = QrPallets
    Left = 172
    Top = 113
  end
  object QrPallets: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPalletsBeforeOpen
    AfterOpen = QrPalletsAfterOpen
    BeforeClose = QrPalletsBeforeClose
    AfterScroll = QrPalletsAfterScroll
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial '
      'ELSE cli.Nome END NOMECLIINT, pal.* '
      'FROM pallets pal'
      'LEFT JOIN entidades cli ON cli.Codigo=pal.CliInt'
      'WHERE pal.Codigo =:P0')
    Left = 144
    Top = 113
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPalletsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPalletsPallet: TWideStringField
      FieldName = 'Pallet'
      Size = 11
    end
    object QrPalletsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPalletsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPalletsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPalletsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPalletsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPalletsDataI: TDateField
      FieldName = 'DataI'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPalletsCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPalletsNOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 100
    end
  end
  object QrPalletsIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM palletsits'
      'WHERE Codigo=:P0')
    Left = 145
    Top = 141
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsPalletsIts: TDataSource
    DataSet = QrPalletsIts
    Left = 173
    Top = 141
  end
  object QrDonos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 600
    Top = 66
    object QrDonosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDonosNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsDonos: TDataSource
    DataSet = QrDonos
    Left = 624
    Top = 66
  end
  object QrMPArti: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM mparti'
      'ORDER BY Nome')
    Left = 308
    Top = 114
    object QrMPArtiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPArtiNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMPArti: TDataSource
    DataSet = QrMPArti
    Left = 336
    Top = 114
  end
  object QrMPGrup: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM mpgrup'
      'ORDER BY Nome')
    Left = 308
    Top = 154
    object QrMPGrupCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPGrupNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMPGrup: TDataSource
    DataSet = QrMPGrup
    Left = 336
    Top = 154
  end
  object PMPallet: TPopupMenu
    Left = 336
    Top = 416
    object CrianovoPallet1: TMenuItem
      Caption = '&Cria novo Pallet'
      OnClick = CrianovoPallet1Click
    end
    object AlteraPalletatual1: TMenuItem
      Caption = '&Altera Pallet atual'
      OnClick = AlteraPalletatual1Click
    end
    object ExcluiPalletatual1: TMenuItem
      Caption = '&Exclui Pallet atual'
      Enabled = False
    end
  end
  object QrUpd: TmySQLQuery
    Database = Dmod.MyDB
    Left = 448
    Top = 12
  end
  object QrPalet: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPaletAfterOpen
    SQL.Strings = (
      'SELECT * FROM palet')
    Left = 433
    Top = 149
    object QrPaletPalet: TIntegerField
      FieldName = 'Palet'
      Origin = 'palet.Palet'
    end
    object QrPaletData: TDateField
      FieldName = 'Data'
      Origin = 'palet.Data'
    end
    object QrPaletOS: TIntegerField
      FieldName = 'OS'
      Origin = 'palet.OS'
    end
    object QrPaletCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'palet.CliInt'
    end
    object QrPaletProcede: TIntegerField
      FieldName = 'Procede'
      Origin = 'palet.Procede'
    end
    object QrPaletMarca: TWideStringField
      FieldName = 'Marca'
      Origin = 'palet.Marca'
    end
    object QrPaletNOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Origin = 'palet.NOMECLIINT'
      Size = 255
    end
    object QrPaletNOMEPROCEDE: TWideStringField
      FieldName = 'NOMEPROCEDE'
      Origin = 'palet.NOMEPROCEDE'
      Size = 255
    end
    object QrPaletPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'palet.Pecas'
    end
    object QrPaletSaida1: TFloatField
      FieldName = 'Saida1'
      Origin = 'palet.Saida1'
    end
    object QrPaletSaida2: TFloatField
      FieldName = 'Saida2'
      Origin = 'palet.Saida2'
    end
    object QrPaletSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'palet.Saldo'
    end
    object QrPaletLote: TWideStringField
      FieldName = 'Lote'
      Origin = 'palet.Lote'
    end
  end
  object DsPalet: TDataSource
    DataSet = QrPalet
    Left = 461
    Top = 149
  end
  object QrMPClas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM mpclas'
      'ORDER BY Nome')
    Left = 416
    Top = 216
    object QrMPClasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpclas.Codigo'
    end
    object QrMPClasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'mpclas.Nome'
      Size = 50
    end
  end
  object DsMPClas: TDataSource
    DataSet = QrMPClas
    Left = 440
    Top = 216
  end
end
