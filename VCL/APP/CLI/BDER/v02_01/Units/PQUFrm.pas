unit PQUFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO;

type
  TFmPQUFrm = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrFormulas: TMySQLQuery;
    QrFormulasNumero: TIntegerField;
    QrFormulasNome: TWideStringField;
    DsFormulas: TDataSource;
    QrAreas: TMySQLQuery;
    QrAreasPeso: TFloatField;
    QrAreasTipo: TFloatField;
    QrAreasCodigo: TIntegerField;
    QrAreasData: TDateTimeField;
    QrAreasTipoTXT: TWideStringField;
    QrAreasVSMovCod: TIntegerField;
    QrAreasAreaM2: TFloatField;
    QrAreasPecas: TFloatField;
    QrAreasAreaPeso: TFloatField;
    DsAreas: TDataSource;
    QrAreasPesoComRend: TFloatField;
    QrSumAreas: TMySQLQuery;
    DsSumAreas: TDataSource;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    PnPedOpen: TPanel;
    Panel34: TPanel;
    RGPedGrandeza: TRadioGroup;
    Panel35: TPanel;
    Label16: TLabel;
    Label6: TLabel;
    Label1: TLabel;
    EdReceita2: TdmkEditCB;
    CBReceita2: TdmkDBLookupComboBox;
    EdControle: TdmkEdit;
    EdRendEsperado: TdmkEdit;
    Panel29: TPanel;
    Panel30: TPanel;
    Label25: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label26: TLabel;
    EdPedPdArM2: TdmkEdit;
    EdPedPdArP2: TdmkEdit;
    EdPedPdPeKg: TdmkEdit;
    Panel36: TPanel;
    Label29: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    EdPedPrArM2: TdmkEdit;
    EdPedPrArP2: TdmkEdit;
    EdPedPrPeKg: TdmkEdit;
    Panel37: TPanel;
    Label30: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label19: TLabel;
    EdPedAreaM2: TdmkEdit;
    EdPedAreaP2: TdmkEdit;
    EDPedPesoKg: TdmkEdit;
    Panel39: TPanel;
    Panel38: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TDBEdit;
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    Panel5: TPanel;
    DBGAreas: TdmkDBGridZTO;
    Panel6: TPanel;
    Label2: TLabel;
    DBEdArM2ComRend: TDBEdit;
    Label4: TLabel;
    DBEdPesoComRend: TDBEdit;
    QrAreasArM2ComRend: TFloatField;
    QrAreasArP2ComRend: TFloatField;
    QrSumAreasArM2ComRend: TFloatField;
    QrSumAreasArP2ComRend: TFloatField;
    Label7: TLabel;
    DBEdArP2ComRend: TDBEdit;
    QrSumAreasPesoComRend: TFloatField;
    Panel7: TPanel;
    Panel33: TPanel;
    Label20: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdPedLinhasCul: TdmkEdit;
    EdPedLinhasCab: TdmkEdit;
    Panel28: TPanel;
    LaQtPedido: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    LaQtProduzido: TLabel;
    Label1000: TLabel;
    LaQtAProduzir: TLabel;
    SbPedQtPrGdz: TSpeedButton;
    EdPedQtPdGdz: TdmkEdit;
    EdPedQtPrGdz: TdmkEdit;
    EdPedQtdeGdz: TdmkEdit;
    Label8: TLabel;
    Label9: TLabel;
    EdMargemDBI: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPedLinhasCabRedefinido(Sender: TObject);
    procedure EdPedLinhasCulRedefinido(Sender: TObject);
    procedure EdPedQtdeGdzRedefinido(Sender: TObject);
    procedure EdPedQtPdGdzRedefinido(Sender: TObject);
    procedure EdPedQtPrGdzRedefinido(Sender: TObject);
    procedure RGPedGrandezaClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPQUFrm(Controle: Integer);
    procedure SaldoQt();
    procedure SbPedQtPrGdzFunc(Sender: TObject);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    procedure CalculaDadosPrevConsPed();
  end;

  var
  FmPQUFrm: TFmPQUFrm;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  PQImp, ModuleGeral, UnPQ_PF, PQUGer;

{$R *.DFM}

procedure TFmPQUFrm.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Receita, DefPeca, Pecas1, Pecas2, Pecas3, Pecas4, DMais2,
  DMais3, DMais4, PedGrandeza: Integer;
  Media, Base1, Base2, Base3, Base4, PedQtdeGdz, PedLinhasCul, PedLinhasCab,
  PedAreaM2, PedAreaP2, PedPesoKg, PedQtPdGdz, PedQtPrGdz, PedPdArM2, PedPdArP2,
  PedPdPeKg, PedPrArM2, PedPrArP2, PedPrPeKg, RendEsperado, MargemDBI: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DbEdCodigo.Text); //QrPQUCabCodigo.Value;
  Controle       := EdControle.ValueVariant;
  Receita        := EdReceita2.ValueVariant;
  DefPeca        := 0;
  Pecas1         := 0;
  Pecas2         := 0;
  Pecas3         := 0;
  Pecas4         := 0;
  Media          := 0;
  Base1          := 0;
  Base2          := 0;
  Base3          := 0;
  Base4          := 0;
  DMais2         := 0;
  DMais3         := 0;
  DMais4         := 0;
  PedGrandeza    := RGPedGrandeza.ItemIndex;
  PedQtPdGdz     := EdPedQtPdGdz.ValueVariant;
  PedQtPrGdz     := EdPedQtPrGdz.ValueVariant;
  PedQtdeGdz     := EdPedQtdeGdz.ValueVariant;
  PedLinhasCul   := EdPedLinhasCul.ValueVariant;
  PedLinhasCab   := EdPedLinhasCab.ValueVariant;
  PedAreaM2      := EdPedAreaM2.ValueVariant;
  PedAreaP2      := EdPedAreaP2.ValueVariant;
  PedPesoKg      := EdPedPesoKg.ValueVariant;
  PedPdArM2      := EdPedPdArM2.ValueVariant;
  PedPdArP2      := EdPedPdArP2.ValueVariant;
  PedPdPeKg      := EdPedPdPeKg.ValueVariant;
  PedPrArM2      := EdPedPrArM2.ValueVariant;
  PedPrArP2      := EdPedPrArP2.ValueVariant;
  PedPrPeKg      := EdPedPrPeKg.ValueVariant;
  RendEsperado   := EdRendEsperado.ValueVariant;
  MargemDBI      := EdMargemDBI.ValueVariant;
  //
  if MyObjects.FIC(Receita = 0, EdReceita2, 'Defina a receita!') then Exit;
  Controle := UMyMod.BPGS1I32('pqufrm', 'Controle', '', '', tsPos, SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqufrm', False, [
  'Codigo', 'Receita', 'DefPeca',
  'Pecas1', 'Pecas2', 'Pecas3',
  'Pecas4', 'Media', 'Base1',
  'Base2', 'Base3', 'Base4',
  'DMais2', 'DMais3', 'DMais4',
  'PedGrandeza', 'PedQtdeGdz', 'PedLinhasCul',
  'PedLinhasCab', 'PedAreaM2', 'PedAreaP2',
  'PedPesoKg', 'PedQtPdGdz', 'PedQtPrGdz',
  'PedPdArM2', 'PedPdArP2', 'PedPdPeKg',
  'PedPrArM2', 'PedPrArP2', 'PedPrPeKg',
  'RendEsperado', 'MargemDBI'], [
  'Controle'], [
  Codigo, Receita, DefPeca,
  Pecas1, Pecas2, Pecas3,
  Pecas4, Media, Base1,
  Base2, Base3, Base4,
  DMais2, DMais3, DMais4,
  PedGrandeza, PedQtdeGdz, PedLinhasCul,
  PedLinhasCab, PedAreaM2, PedAreaP2,
  PedPesoKg, PedQtPdGdz, PedQtPrGdz,
  PedPdArM2, PedPdArP2, PedPdPeKg,
  PedPrArM2, PedPrArP2, PedPrPeKg,
  RendEsperado, MargemDBI], [
  Controle], True) then
  begin
    ReopenPQUFrm(Controle);
    //
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      //
      RGPedGrandeza.ItemIndex     := 0;
{
      EdReceita2.ValueVariant     := 0;
      CBReceita2.KeyValue         := 0;
      EdPedQtPdGdz.ValueVariant   := 0;
      EdPedQtPrGdz.ValueVariant   := 0;
      EdPedQtdeGdz.ValueVariant   := 0;
}
      EdPedLinhasCul.ValueVariant := 0;
      EdPedLinhasCab.ValueVariant := 0;
      //
{
      EdPedAreaM2.ValueVariant    := 0;
      EdPedAreaP2.ValueVariant    := 0;
      EdPedPesoKg.ValueVariant    := 0;
      EdPedPdArM2.ValueVariant    := 0;
      EdPedPdArP2.ValueVariant    := 0;
      EdPedPdPeKg.ValueVariant    := 0;
      EdPedPrArM2.ValueVariant    := 0;
      EdPedPrArP2.ValueVariant    := 0;
      EdPedPrPeKg.ValueVariant    := 0;
      //
      EdRendEsperado.ValueVariant := 0;
}
    end else Close;
  end;
end;

procedure TFmPQUFrm.BtSaidaClick(Sender: TObject);
begin
  Close;
  //MostraEdicao2(0, stLok, 0);
end;

procedure TFmPQUFrm.CalculaDadosPrevConsPed();
var
  PedGrandeza: Integer;
  PedQtdeGdz, PedLinhasCul, PedLinhasCab, PedAreaM2, PedAreaP2, PedPesoKg,
  Fator, PedQtPdGdz, PedQtPrGdz, PedPdArM2, PedPdArP2, PedPdPeKg, PedPrArM2,
  PedPrArP2, PedPrPeKg: Double;
begin
  LaQtPedido.Caption := RGPedGrandeza.Items[RGPedGrandeza.ItemIndex];
  LaQtProduzido.Caption := RGPedGrandeza.Items[RGPedGrandeza.ItemIndex];
  LaQtAProduzir.Caption := RGPedGrandeza.Items[RGPedGrandeza.ItemIndex];
  PedGrandeza    := RGPedGrandeza.ItemIndex;
  PedQtdeGdz     := EdPedQtdeGdz.ValueVariant;
  PedLinhasCul   := EdPedLinhasCul.ValueVariant;
  PedLinhasCab   := EdPedLinhasCab.ValueVariant;
  PedAreaM2      := EdPedAreaM2.ValueVariant;
  PedAreaP2      := EdPedAreaP2.ValueVariant;
  PedPesoKg      := EdPedPesoKg.ValueVariant;
  //
  PedQtPdGdz     := EdPedQtPdGdz.ValueVariant;
  PedQtPrGdz     := EdPedQtPrGdz.ValueVariant;
  //
  Fator := ((PedLinhasCul * 0.7) + (PedLinhasCab * 0.3)) * 1.15 / 10;
  case PedGrandeza of
    //0: //
    1: // Area m2
    begin
      EdPedQtPdGdz.DecimalSize := 2;
      EdPedQtPrGdz.DecimalSize := 2;
      EdPedQtdeGdz.DecimalSize := 2;
      //
      PedAreaM2 := PedQtdeGdz;
      PedAreaP2 := Geral.ConverteArea(PedAreaM2, ctM2toP2, cfQuarto);
      PedPesoKg := PedAreaM2 * Fator;
      //
      PedPdArM2 := PedQtPdGdz;
      PedPdArP2 := Geral.ConverteArea(PedPdArM2, ctM2toP2, cfQuarto);
      PedPdPeKg := PedPdArM2 * Fator;
      //
      PedPrArM2 := PedQtPrGdz;
      PedPrArP2 := Geral.ConverteArea(PedPrArM2, ctM2toP2, cfQuarto);
      PedPrPeKg := PedPrArM2 * Fator;
    end;
    2: // Area m2
    begin
      EdPedQtPdGdz.DecimalSize := 2;
      EdPedQtPrGdz.DecimalSize := 2;
      EdPedQtdeGdz.DecimalSize := 2;
      //
      PedAreaP2 := PedQtdeGdz;
      PedAreaM2 := Geral.ConverteArea(PedAreaP2, ctP2toM2, cfCento);
      PedPesoKg := PedAreaM2 * Fator;
      //
      PedPdArP2 := PedQtPdGdz;
      PedPdArM2 := Geral.ConverteArea(PedPdArP2, ctP2toM2, cfCento);
      PedPdPeKg := PedPdArM2 * Fator;
      //
      PedPrArP2 := PedQtPrGdz;
      PedPrArM2 := Geral.ConverteArea(PedPrArP2, ctP2toM2, cfCento);
      PedPrPeKg := PedPrArM2 * Fator;
    end;
    3: // Area m2
    begin
      EdPedQtPdGdz.DecimalSize := 3;
      EdPedQtPrGdz.DecimalSize := 3;
      EdPedQtdeGdz.DecimalSize := 3;
      //
      PedPesoKg := PedQtdeGdz;
      if Fator = 0 then
        PedAreaM2 := 0
      else
        PedAreaM2 := PedPesoKg / Fator;
      PedAreaP2 := Geral.ConverteArea(PedAreaM2, ctM2toP2, cfQuarto);
      PedPesoKg := PedAreaM2 * Fator;
      //
      PedPdPeKg := PedQtPdGdz;
      if Fator = 0 then
        PedPdArM2 := 0
      else
        PedPdArM2 := PedPdPeKg / Fator;
      PedPdArP2 := Geral.ConverteArea(PedPdArM2, ctM2toP2, cfQuarto);
      PedPdPeKg := PedPdArM2 * Fator;
      //
      PedPrPeKg := PedQtPrGdz;
      if Fator = 0 then
        PedPrArM2 := 0
      else
        PedPrArM2 := PedPrPeKg / Fator;
      PedPrArP2 := Geral.ConverteArea(PedPrArM2, ctM2toP2, cfQuarto);
      PedPrPeKg := PedPrArM2 * Fator;
      //
    end;
    else
    begin
      EdPedQtPdGdz.DecimalSize := 0;
      EdPedQtPrGdz.DecimalSize := 0;
      EdPedQtdeGdz.DecimalSize := 0;
      //
      PedAreaM2 := 0;
      PedAreaP2 := 0;
      PedPesoKg := 0;
      //
      PedPdArM2 := 0;
      PedPdArP2 := 0;
      PedPdPeKg := 0;

      PedPrArM2 := 0;
      PedPrArP2 := 0;
      PedPrPeKg := 0;
    end;
  end;
  EdPedAreaM2.ValueVariant := PedAreaM2;
  EdPedAreaP2.ValueVariant := PedAreaP2;
  EdPedPesoKg.ValueVariant := PedPesoKg;
  //
  EdPedPdArM2.ValueVariant := PedPdArM2;
  EdPedPdArP2.ValueVariant := PedPdArP2;
  EdPedPdPeKg.ValueVariant := PedPdPeKg;
  //
  EdPedPrArM2.ValueVariant := PedPrArM2;
  EdPedPrArP2.ValueVariant := PedPrArP2;
  EdPedPrPeKg.ValueVariant := PedPrPeKg;
end;

procedure TFmPQUFrm.EdPedLinhasCabRedefinido(Sender: TObject);
begin
  CalculaDadosPrevConsPed();
end;

procedure TFmPQUFrm.EdPedLinhasCulRedefinido(Sender: TObject);
begin
  CalculaDadosPrevConsPed();
end;

procedure TFmPQUFrm.EdPedQtdeGdzRedefinido(Sender: TObject);
begin
  CalculaDadosPrevConsPed();
end;

procedure TFmPQUFrm.EdPedQtPdGdzRedefinido(Sender: TObject);
begin
  SaldoQt();
end;

procedure TFmPQUFrm.EdPedQtPrGdzRedefinido(Sender: TObject);
begin
  SaldoQt();
end;

procedure TFmPQUFrm.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmPQUFrm.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrFormulas, Dmod.MyDB);
end;

procedure TFmPQUFrm.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQUFrm.ReopenPQUFrm(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmPQUFrm.RGPedGrandezaClick(Sender: TObject);
begin
  CalculaDadosPrevConsPed();
end;

procedure TFmPQUFrm.SaldoQt();
var
  PedQtdeGdz, PedQtPdGdz, PedQtPrGdz: Double;
begin
  PedQtPdGdz := EdPedQtPdGdz.ValueVariant;
  PedQtPrGdz := EdPedQtPrGdz.ValueVariant;
  if PedQtPdGdz > PedQtPrGdz then
    PedQtdeGdz := PedQtPdGdz - PedQtPrGdz
  else
    PedQtdeGdz := 0;
  EdPedQtdeGdz.ValueVariant := PedQtdeGdz;
end;

(*
procedure TFmPQUFrm.SbPedQtPrGdzClick(Sender: TObject);
var
  Codigo, Controle, EmitGru, CliOrig, Formula, PedGrandeza, LinhasCul,
  LinhasCab: Integer;
  RendEsperado: Double;
  InMultiExec: Boolean;
  PedQtPrGdz, ArM2ComRend, ArP2ComRend, PesoComRend: Double;
  ErrTxt: String;
begin
  Codigo       := FmPQImp.QrPQUCabCodigo.Value;
  Controle     := EdControle.ValueVariant;
  EmitGru      := FmPQImp.QrPQUCabEmitGru.Value;
  CliOrig      := FmPQImp.QrPQUCabCliInt.Value;
  Formula      := EdReceita2.ValueVariant;
  RendEsperado := EdRendEsperado.ValueVariant;
  PedGrandeza  := RGPedGrandeza.ItemIndex;
  LinhasCul    := EdPedLinhasCul.ValueVariant;
  LinhasCab    := EdPedLinhasCab.ValueVariant;


  InMultiExec  := False;

  if PQ_PF.CalculaProducaoDeItemDePedidoDePrevisaoDeCompraDeInsumos(Codigo,
  Controle, EmitGru, CliOrig, Formula, PedGrandeza, LinhasCul, LinhasCab,
  RendEsperado, InMultiExec, PedQtPrGdz, ArM2ComRend, ArP2ComRend, PesoComRend,
  ErrTxt) then
  begin
    EdPedQtPrGdz.ValueVariant  := PedQtPrGdz;
    DBGAreas.DataSource        := DsAreas;
    DBEdArM2ComRend.DataSource := DsSumAreas;
    DBEdArP2ComRend.DataSource := DsSumAreas;
    DBEdPesoComRend.DataSource := DsSumAreas;
  end;
end;
*)

procedure TFmPQUFrm.SbPedQtPrGdzFunc(Sender: TObject);
  function CalculaAreaPeso(Peso: Double (*str: String*)): Double;
  var
{
    I: Integer;
    H: Boolean;
    sCul, sCab: String;
}
    dCul, dCab: Double;
  begin
    Result := 0;
    //
{
    sCul := '';
    sCab := '';
    dCul := 0;
    dCab := 0;
    h := False;
    for I := 1 to Length(str) do
    begin
      if str[I] = '-' then H := True;
      if str[I] = '/' then H := True;
      if str[I] = '.' then H := True;
      if str[I] in ['0'..'9'] then
      begin
        if H = False then
          sCul := sCul + Str[I]
        else
          sCab := sCab + Str[I];
      end;
    end;
    if (sCab <> EmptyStr) and (sCul <> EmptyStr) then
    begin
      dCab := Geral.DMV(sCab);
      dCul := Geral.DMV(sCul);
      //
      if (dCab > 0) and (dCul > 0) then
        Result := ((dCab * 0.3) + (dCul * 0.7) * 1.15);
    end;
}
    dCul := EdPedLinhasCul.ValueVariant;
    dCab := EdPedLinhasCab.ValueVariant;
    if (dCab > 0) and (dCul > 0) then
    begin
      Result := ((dCab * 0.3) + (dCul * 0.7)) * 1.15 / 10;
      //Geral.MB_Info(Geral.FFT(Result, 10, siNegativo));
      if Peso > 0 then
      begin
        Result := Peso / Result;
        //Geral.MB_Info(Geral.FFT(Result, 10, siNegativo));
      end else
        Result := 0;
    end else
      Result := 0;
  end;
var
  EmitGru, CliOrig, Numero, PedGrandeza: Integer;
  AreaPeso, AreaM2, Peso, ArM2ComRend, ArP2ComRend, RendEsperado, PesoComRend: Double;
begin
(*  2022-02-25
  EmitGru := FmPQImp.QrPQUCabEmitGru.Value;
  CliOrig := FmPQImp.QrPQUCabCliInt.Value;
*)
  EmitGru := FmPQUGer.QrPQUCabEmitGru.Value;
  CliOrig := FmPQUGer.QrPQUCabCliInt.Value;
  //
  Numero  := EdReceita2.ValueVariant;
  RendEsperado := EdRendEsperado.ValueVariant;
  PedGrandeza := RGPedGrandeza.ItemIndex;
  //
  if MyObjects.FIC(CliOrig = 0, nil, 'Cliente interno n�o definido no grupo!') then Exit;
  if MyObjects.FIC(EmitGru = 0, nil, 'Grupo de Emis�o n�o definido no grupo!') then Exit;
  if MyObjects.FIC(Numero = 0, EdReceita2, 'Defina a receita!') then Exit;
  if MyObjects.FIC(PedGrandeza = 0, RGPedGrandeza, 'Defina a grandeza!') then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAreas, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _emitgru_produzido_;',
  'CREATE TABLE _emitgru_produzido_',
  'SELECT emi.Peso, 110.000 Tipo,  ',
  'Codigo, CAST(DataEmis + 0 AS DATETIME) Data, "Pesagem" TipoTXT,  ',
  'VSMovCod, AreaM2, Qtde Pecas, 999999999.99 AreaPeso, ',
  '999999999.99 ArM2ComRend, 999999999.99 ArP2ComRend, 999999999.99 PesoComRend  ',
  'FROM ' + TMeuDB + '.emit emi ',
  '/*LEFT JOIN ' + TMeuDB + '.pqx pqx ON emi.Codigo=pqx.OrigemCodi */',
  'WHERE EmitGru=' + Geral.FF0(EmitGru),
  //'AND pqx.CliOrig=' + Geral.FF0(CliOrig),
  'AND emi.Numero=' + Geral.FF0(Numero),
  ';',
  'SELECT * FROM _emitgru_produzido_',
  '']);
  QrAreas.First;
  while not QrAreas.Eof do
  begin
    AreaPeso := CalculaAreaPeso(QrAreasPeso.Value);
    AreaM2   := QrAreasAreaM2.Value;
    //
    if AreaPeso > AreaM2 then
      ArM2ComRend := AreaPeso
    else
      ArM2ComRend := AreaM2;
    //
    ArM2ComRend := ArM2ComRend * (1 + (RendEsperado / 100));
    ArP2ComRend := Geral.ConverteArea(ArM2ComRend, ctM2toP2, cfQuarto);
    PesoComRend := QrAreasPeso.Value * (1 + (RendEsperado / 100));
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_emitgru_produzido_', False, [
    'AreaPeso', 'ArM2ComRend', 'ArP2ComRend',
    'PesoComRend'], [
    'Codigo'], [
    AreaPeso, ArM2ComRend, ArP2ComRend,
    PesoComRend], [
    QrAreasCodigo.Value], False);
    //
    QrAreas.Next;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrAreas, DModG.MyPID_DB, [
  'SELECT * FROM _emitgru_produzido_',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumAreas, DModG.MyPID_DB, [
  'SELECT SUM(PesoComRend) PesoComRend, ',
  'SUM(ArM2ComRend) ArM2ComRend,',
  'SUM(ArP2ComRend) ArP2ComRend',
  'FROM _emitgru_produzido_',
  '']);
  //
  ArM2ComRend := QrSumAreasArM2ComRend.Value;
  ArP2ComRend := QrSumAreasArP2ComRend.Value;
  PesoComRend := QrSumAreasPesoComRend.Value;
  case PedGrandeza of
    1: EdPedQtPrGdz.ValueVariant := ArM2ComRend;
    2: EdPedQtPrGdz.ValueVariant := ArP2ComRend;
    3: EdPedQtPrGdz.ValueVariant := PesoComRend;
  end;
end;

end.
