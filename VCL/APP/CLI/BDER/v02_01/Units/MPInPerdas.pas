unit MPInPerdas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, DBCtrls, DmkDAC_PF,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkLabel,
  dmkImage, UnDmkEnums;

type
  TFmMPInPerdas = class(TForm)
    Panel1: TPanel;
    QrListaSetores: TmySQLQuery;
    QrListaSetoresCodigo: TIntegerField;
    QrListaSetoresNome: TWideStringField;
    DsListaSetores: TDataSource;
    Label8: TLabel;
    EdSetor: TdmkEditCB;
    CBSetor: TdmkDBLookupComboBox;
    EdMotivo: TdmkEditCB;
    CBMotivo: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdPecas: TdmkEdit;
    Label2: TLabel;
    EdDataHora: TdmkEdit;
    Label3: TLabel;
    QrRolPerdas: TmySQLQuery;
    DsRolPerdas: TDataSource;
    QrRolPerdasCodigo: TIntegerField;
    QrRolPerdasNome: TWideStringField;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FMPInCtrl, FControle: Integer;
  end;

  var
  FmMPInPerdas: TFmMPInPerdas;

implementation

uses UnMyObjects, UMySQLModule, Module, MPIn, Principal, UnInternalConsts;

{$R *.DFM}

procedure TFmMPInPerdas.BtOKClick(Sender: TObject);
var
  M2, P2: Double;
  DataHora: String;
  Setor, Motivo: Integer;
begin
  M2 := 0; P2 := 0;
  Setor := Geral.IMV(EdSetor.Text);
  if Setor = 0 then
  begin
    Application.MessageBox('Defina o setor!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdSetor.SetFocus;
    Exit;
  end;
  Motivo := Geral.IMV(EdMotivo.Text);
  if Motivo = 0 then
  begin
    Application.MessageBox('Defina o motivo!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdMotivo.SetFocus;
    Exit;
  end;
  DataHora := Geral.FDT(EdDataHora.ValueVariant, 105);
  FControle := UMyMod.BuscaEmLivreY_Def('mpinperdas', 'Controle',
    ImgTipo.SQLType, FControle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'mpinperdas', False, [
    'MPInCtrl', 'Pecas', 'M2',
    'P2', 'DataHora', 'Setor',
    'Motivo'
  ], ['Controle'], [
    FMPInCtrl, EdPecas.ValueVariant, M2,
    P2, DataHora, EdSetor.ValueVariant,
    EdMotivo.ValueVariant
  ], [FControle], True) then
  begin
    //
    Dmod.AtualizaMPIn(FMPInCtrl);
    //
    FmMPIn.ReopenMPInPerdas(FControle);
    Close;
  end;
end;

procedure TFmMPInPerdas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPInPerdas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPInPerdas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrRolPerdas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrListaSetores, Dmod.MyDB);
  EdDataHora.Text := Geral.FDT(Now(), 106);
end;

procedure TFmMPInPerdas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPInPerdas.SpeedButton1Click(Sender: TObject);
var
  Setor: Integer;
begin
  VAR_CADASTRO := 0;
  Setor        := EdSetor.ValueVariant;
  //
  FmPrincipal.CadastroDeSetores(Setor);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrListaSetores.Close;
    UnDmkDAC_PF.AbreQuery(QrListaSetores, Dmod.MyDB);
    //
    QrListaSetores.Locate('Codigo', VAR_CADASTRO, []);
    //
    EdSetor.ValueVariant := VAR_CADASTRO;
    CBSetor.KeyValue     := VAR_CADASTRO;
    EdSetor.SetFocus;
  end;
end;

procedure TFmMPInPerdas.SpeedButton2Click(Sender: TObject);
begin
  FmPrincipal.Perdasdecouros1Click(Self);
  QrRolPerdas.Close;
  UnDmkDAC_PF.AbreQuery(QrRolPerdas, Dmod.MyDB);
  //
  EdMotivo.ValueVariant := VAR_CADASTRO;
  CBMotivo.KeyValue     := VAR_CADASTRO;
  //
end;

end.
