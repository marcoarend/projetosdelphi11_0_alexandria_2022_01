unit PQEEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnInternalConsts,
  UnMsgInt, ExtCtrls, UnInternalConsts2, mySQLDbTables, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, ComCtrls, dmkMemo, dmkRadioGroup,
  dmkLabel, dmkImage, UnDmkEnums, DmkDAC_PF, dmkEditDateTimePicker;

type
  TFmPQEEdit = class(TForm)
    QrPQ1: TmySQLQuery;
    DsPQ1: TDataSource;
    QrLocalizaPQ: TmySQLQuery;
    Panel1: TPanel;
    Painel1: TPanel;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    PainelDados: TPanel;
    Label1: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label10: TLabel;
    Label19: TLabel;
    Label5: TLabel;
    EdProduto: TdmkEditCB;
    CBProduto: TdmkDBLookupComboBox;
    EdVolumes: TdmkEdit;
    EdkgLiq: TdmkEdit;
    EdVlrItem: TdmkEdit;
    EdTotalkgLiq: TdmkEdit;
    EdValorkg: TdmkEdit;
    CkAmostra: TCheckBox;
    SpeedButton1: TSpeedButton;
    QrPQ1NOMEPQ: TWideStringField;
    QrPQ1Controle: TIntegerField;
    QrPQ1IQ: TIntegerField;
    QrPQ1PQ: TIntegerField;
    QrPQ1CI: TIntegerField;
    QrPQ1Prazo: TWideStringField;
    QrPQ1Lk: TIntegerField;
    QrPQ1DataCad: TDateField;
    QrPQ1DataAlt: TDateField;
    QrPQ1UserCad: TIntegerField;
    QrPQ1UserAlt: TIntegerField;
    EdTotalkgBruto: TdmkEdit;
    EdKgBruto: TdmkEdit;
    Label3: TLabel;
    Label6: TLabel;
    EdIPI_pIPI: TdmkEdit;
    Label4: TLabel;
    Label7: TLabel;
    EdIPI_vIPI: TdmkEdit;
    EdCustoItem: TdmkEdit;
    Label8: TLabel;
    Panel3: TPanel;
    Label66: TLabel;
    Edprod_CFOP: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdVolumesChange(Sender: TObject);
    procedure EdKgBrutoChange(Sender: TObject);
    procedure EdkgLiqChange(Sender: TObject);
    procedure EdVlrItemChange(Sender: TObject);
    procedure EdVolumesExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdTotalkgBrutoChange(Sender: TObject);
    procedure EdTotalkgLiqChange(Sender: TObject);
    procedure EdTotalkgLiqExit(Sender: TObject);
    procedure EdProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdkgLiqExit(Sender: TObject);
    procedure EdIPI_pIPIChange(Sender: TObject);
    procedure EdIPI_vIPIChange(Sender: TObject);
  private
    { Private declarations }
    FVol, FkgU, FkgT: Double;
    procedure MostraTodosPQs;
    procedure CalculaIPI();
  public
    { Public declarations }
    procedure Pesos(Index: Integer);
    procedure CalculaOnEdit;
  end;

var
  FmPQEEdit: TFmPQEEdit;

implementation

uses UnMyObjects, PQE, Module, Principal, BlueDermConsts, UMySQLModule;

{$R *.DFM}


procedure TFmPQEEdit.CalculaOnEdit;
var
  Volumes, TotalkgLiq, kgBruto, CustoItem, ValorItem, IPI_vIPI,
  Valor, CFin : Double;
begin
  Volumes    := Geral.DMV(EdVolumes.Text);
  //kgLiq      := Geral.DMV(EdkgLiq.Text);
  kgBruto    := Geral.DMV(EdKgBruto.Text);
  ValorItem  := Geral.DMV(EdVlrItem.Text);
  TotalkgLiq := Geral.DMV(EdTotalkgLiq.Text);
  //TotalkgBru := Geral.DMV(EdTotalkgBruto.Text);
  //
  if FmPQE.QrPQEValorNF.Value > 0 then
  CFin       := 1+(FmPQE.QrPQEJuros.Value / FmPQE.QrPQEValorNF.Value)
  else CFin  := 1;
  //
  IPI_vIPI  := EdIPI_vIPI.ValueVariant;
  CustoItem := ValorItem + IPI_vIPI * CFin;
  if TotalkgLiq > 0 then Valor := CustoItem / TotalkgLiq else Valor := 0;
  //
  EdCustoItem.ValueVariant    := CustoItem;
  EdValorkg.ValueVariant      := Valor;
  EdTotalkgBruto.ValueVariant := Volumes * kgBruto;
end;

procedure TFmPQEEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQEEdit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrPQ1.Close;
  QrPQ1.SQL.Clear;
  QrPQ1.SQL.Add('SELECT pq.Nome NOMEPQ, pfo.*');
  QrPQ1.SQL.Add('FROM pqfor pfo');
  QrPQ1.SQL.Add('LEFT JOIN pq pq ON pfo.PQ=pq.Codigo');
  QrPQ1.SQL.Add('WHERE pfo.CI=:P0');
  QrPQ1.SQL.Add('AND pfo.IQ=:P1');
  QrPQ1.SQL.Add('AND pq.Ativo = 1');
  QrPQ1.SQL.Add('AND pq.GGXNiv2 > 0');
  QrPQ1.SQL.Add('ORDER BY pq.Nome');
  QrPQ1.Params[0].AsInteger := FmPQE.QrPQECI.Value;
  QrPQ1.Params[1].AsInteger := FmPQE.QrPQEIQ.Value;
  UnDmkDAC_PF.AbreQuery(QrPQ1, Dmod.MyDB);
  //
  CBProduto.LocF7SQLText.Text := Geral.ATS([
  'SELECT pqf.PQ _Codigo, pq_.Nome _Nome ',
  'FROM pqfor pqf ',
  'LEFT JOIN pq pq_ ON pq_.Codigo=pqf.PQ ',
  'WHERE pq_.Nome LIKE "%$#%"',
  '']);
end;

procedure TFmPQEEdit.BtConfirmaClick(Sender: TObject);
var
  Cursor : TCursor;
  TotalCusto, ValorItem, IPI_pIPI, IPI_vIPI, kgLiq, kgBruto: Double;
  Controle, Conta, Produto, Codigo, Volumes : Integer;
  CFOP: String;
  //Amostra: String;
  //Atualiza: Boolean;
  //RICMS, RICMSF, , Preco, Valor, Frete, Juros, CFin
  Permite: Boolean;
begin
//  Atualiza := False;
  Produto := CBProduto.KeyValue;
  if Produto < 1 then
  begin
    ShowMessage(FIN_MSG_DEFPRODUTO);
    EdProduto.SetFocus;
    Exit;
  end;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    Codigo := FmPQE.QrPQECodigo.Value;
    Conta := EdCodigo.ValueVariant;

    Produto    := CBProduto.KeyValue;
    Volumes    := Trunc(Geral.DMV(EdVolumes.Text));
    kgLiq      := Geral.DMV(EdkgLiq.Text);
    kgBruto    := Geral.DMV(EdKgBruto.Text);
    ValorItem  := Geral.DMV(EdVlrItem.Text);
    IPI_pIPI   := Geral.DMV(EdIPI_pIPI.Text);
    IPI_vIPI   := Geral.DMV(EdIPI_vIPI.Text);
    TotalCusto := EdCustoItem.ValueVariant;
    Permite    := (ValorItem > 0)
    // Cliente interno diferente da empresa
    or (FmPQE.QrPQECI.Value > 0);
    if MyObjects.FIC(not Permite, EdVlrItem, 'Defina o valor do item.') then Exit;
    (*
    if (ValorItem<= 0) then
    begin
      if
      ShowMessage('Defina o valor do item.');
      Screen.Cursor := Cursor;
      Exit;
    end;
    *)
    if Volumes<=0 then
    begin
      ShowMessage('Defina os volumes do item.');
      Screen.Cursor := Cursor;
      Exit;
    end;
    if int(Geral.DMV(EdVolumes.Text) * 1000) <> (Volumes * 1000) then
    begin
      ShowMessage('Volumes devem ser um n�mero inteiro.');
      Screen.Cursor := Cursor;
      Exit;
    end;
    if MyObjects.FIC(Edprod_CFOP.ValueVariant = 0, Edprod_CFOP, 'Informe o CFOP!') then Exit;
    //
    CFOP := FormatFloat('0000', Edprod_CFOP.ValueVariant);
    (*if CkAmostra.Checked then Amostra := 'V' else amostra := 'F';
    Dmod.QrUpdW.SQL.Clear;
    Dmod.QrUpdW.SQL.Add('UPDATE pq SET Amostra=:P0');
    Dmod.QrUpdW.SQL.Add('WHERE Codigo=:P1');
    Dmod.QrUpdW.Params[0].AsString  := Amostra;
    Dmod.QrUpdW.Params[1].AsInteger := Produto;
    Dmod.QrUpdW.ExecSQL;*)

    if ImgTipo.SQLType = stIns then
    begin
      Dmod.QrUpdW.SQL.Clear;
      Dmod.QrUpdW.SQL.Add('UPDATE pqeits SET Conta=Conta+1');
      Dmod.QrUpdW.SQL.Add('WHERE Conta>=:P0 AND Codigo=:P1');
      Dmod.QrUpdW.Params[0].AsInteger := Conta;
      Dmod.QrUpdW.Params[1].AsInteger := Codigo;
      Dmod.QrUpdW.ExecSQL;
      //
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'PQEIts','PQEIts','Controle');
      //
      Dmod.QrUpdW.SQL.Clear;
      Dmod.QrUpdW.SQL.Add('INSERT INTO pqeits SET Insumo=:P0, Volumes=:P1, ');
      Dmod.QrUpdW.SQL.Add('PesoVB=:P2, PesoVL=:P3, ValorItem=:P4, ');
      Dmod.QrUpdW.SQL.Add('IPI_pIPI=:P5, IPI_vIPI=:P6, TotalCusto=:P7, ');
      Dmod.QrUpdW.SQL.Add('TotalPeso=:P8, prod_CFOP=:P9, Codigo=:P10, Conta=:11, ');
      Dmod.QrUpdW.SQL.Add('Controle=:P12');
      Dmod.QrUpdW.Params[00].AsInteger := Produto;
      Dmod.QrUpdW.Params[01].AsInteger := Volumes;
      Dmod.QrUpdW.Params[02].AsFloat := kgBruto;
      Dmod.QrUpdW.Params[03].AsFloat := kgLiq;
      Dmod.QrUpdW.Params[04].AsFloat := ValorItem;
      Dmod.QrUpdW.Params[05].AsFloat := IPI_pIPI;
      Dmod.QrUpdW.Params[06].AsFloat := IPI_vIPI;
      Dmod.QrUpdW.Params[07].AsFloat := TotalCusto;
      Dmod.QrUpdW.Params[08].AsFloat := Volumes*kgLiq;
      Dmod.QrUpdW.Params[09].AsString  := CFOP;
      Dmod.QrUpdW.Params[10].AsInteger := Codigo;
      Dmod.QrUpdW.Params[11].AsInteger := Conta;
      Dmod.QrUpdW.Params[12].AsInteger := Controle;
      Dmod.QrUpdW.ExecSQL;
      FmPQE.FControle := Controle;
    end
    else
    begin
      Dmod.QrUpdW.SQL.Clear;
      Dmod.QrUpdW.SQL.Add('UPDATE pqeits SET Insumo=:P0, Volumes=:P1, ');
      Dmod.QrUpdW.SQL.Add('PesoVB=:P2, PesoVL=:P3, ValorItem=:P4, IPI=:P5, ');
      Dmod.QrUpdW.SQL.Add('RIPI=:P6, TotalCusto=:P7, TotalPeso=:P8, prod_CFOP=:P9 ');
      Dmod.QrUpdW.SQL.Add('WHERE Codigo=:P10 AND Conta=:P11 AND Controle=:P12');
      Dmod.QrUpdW.Params[00].AsInteger := Produto;
      Dmod.QrUpdW.Params[01].AsInteger := Volumes;
      Dmod.QrUpdW.Params[02].AsFloat   := kgBruto;
      Dmod.QrUpdW.Params[03].AsFloat   := kgLiq;
      Dmod.QrUpdW.Params[04].AsFloat   := ValorItem;
      Dmod.QrUpdW.Params[05].AsFloat   := IPI_pIPI;
      Dmod.QrUpdW.Params[06].AsFloat   := IPI_vIPI;
      Dmod.QrUpdW.Params[07].AsFloat   := TotalCusto;
      Dmod.QrUpdW.Params[08].AsFloat   := Volumes*kgLiq;
      Dmod.QrUpdW.Params[09].AsString  := CFOP;
      Dmod.QrUpdW.Params[10].AsInteger := Codigo;
      Dmod.QrUpdW.Params[11].AsInteger := Conta;
      Dmod.QrUpdW.Params[12].AsInteger := FmPQE.QrPQEItsControle.Value;
      Dmod.QrUpdW.ExecSQL;
      FmPQE.FControle := FmPQE.QrPQEItsControle.Value;
    end;
    Screen.Cursor := crDefault;
    if ImgTipo.SQLType = stIns then
    begin
      FmPQE.ReopenPQEIts(FmPQE.QrPQECodigo.Value);
      EdProduto.Text     := '';
      CBProduto.KeyValue := NULL;
      EdKgBruto.Text     := '';
      EdkgLiq.Text       := '';
      EdTotalkgLiq.Text  := '';
      EdVolumes.Text     := '';
      EdIPI_pIPI.Text    := '';
      EdIPI_vIPI.Text    := '';
      EdVlrItem.Text     := '';
      EdProduto.SetFocus;
    end else Close;
  finally
    Screen.Cursor := crDefault;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmPQEEdit.EdVolumesChange(Sender: TObject);
begin
  if EdVolumes.Focused then
  begin
    Pesos(2);
    CalculaOnEdit;
  end;  
end;

procedure TFmPQEEdit.EdIPI_pIPIChange(Sender: TObject);
begin
  CalculaIPI();
end;

procedure TFmPQEEdit.EdIPI_vIPIChange(Sender: TObject);
begin
  CalculaOnEdit();
end;

procedure TFmPQEEdit.EdKgBrutoChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmPQEEdit.EdkgLiqChange(Sender: TObject);
begin
  if EdkgLiq.Focused then
  begin
    Pesos(0);
    CalculaOnEdit;
  end;
end;

procedure TFmPQEEdit.EdkgLiqExit(Sender: TObject);
begin
  EdKgLiq.ValueVariant := FkgU;
end;

procedure TFmPQEEdit.EdVlrItemChange(Sender: TObject);
begin
  CalculaIPI();
  CalculaOnEdit;
end;

procedure TFmPQEEdit.EdVolumesExit(Sender: TObject);
begin
  EdVolumes.ValueVariant := FVol;
end;

procedure TFmPQEEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQEEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQEEdit.EdTotalkgBrutoChange(Sender: TObject);
begin
 //if EdTotalkgBruto.Focused then FOnEditA := 3;
  CalculaOnEdit;
end;

procedure TFmPQEEdit.EdTotalkgLiqChange(Sender: TObject);
begin
  if EdTotalkgLiq.Focused then
  begin
    Pesos(1);
    CalculaOnEdit;
  end;
end;

procedure TFmPQEEdit.EdTotalkgLiqExit(Sender: TObject);
begin
  EdTotalkgLiq.ValueVariant := FkgT;
end;

procedure TFmPQEEdit.Pesos(Index: Integer);
var
  kgU, kgT, Vol: Double;
begin
  kgU := Geral.DMV(EdkgLiq.Text);
  kgT := Geral.DMV(EdTotalkgLiq.Text);
  Vol := Geral.DMV(EdVolumes.Text);
  //
  case Index of
    // Qtde por volume
    0: {if (kgT <> 0) and (kgU > 0) then
        Vol := kgT / kgU
      else}
        kgT := kgU * Vol;
    // Qtde total
    1: {if (kgT <> 0) and (kgU > 0) then Vol := kgT / kgU else
      begin
        if (Vol>0) then
        begin
          if kgU > 0 then
            kgT := kgU * Vol
          else}
            kgU := kgT / Vol;
        {end;
      end;}
    // Volumes
    2: {
    if (kgT <> 0) and (Vol > 0) then
        kgU := kgT / Vol
      else}
        kgT := kgU * Vol;
  end;
  FkgU := kgU;
  FkgT := kgT;
  FVol := Vol;
  //
  if index <> 0 then EdKgLiq.ValueVariant      := FkgU;
  if index <> 1 then EdTotalkgLiq.ValueVariant := FkgT;
  if index <> 2 then EdVolumes.ValueVariant    := FVol;
end;

procedure TFmPQEEdit.EdProdutoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then MostraTodosPQs;
end;

procedure TFmPQEEdit.CBProdutoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then MostraTodosPQs;
end;

procedure TFmPQEEdit.CalculaIPI();
begin
  EdIPI_vIPI.ValueVariant :=
  EdIPI_pIPI.ValueVariant *
  EdVlrItem.ValueVariant / 100;
end;

procedure TFmPQEEdit.MostraTodosPQs;
begin
  QrPQ1.Close;
  QrPQ1.SQL.Clear;
{
SELECT pq.Nome NOMEPQ, pfo.*
FROM pqfor pfo
LEFT JOIN pq pq ON pfo.PQ=pq.Codigo
WHERE pfo.CI=:P0
AND pfo.IQ=:P1
ORDER BY pq.Nome
}
  QrPQ1.SQL.Add('SELECT pq.Nome NOMEPQ, pfo.*');
  QrPQ1.SQL.Add('FROM pqfor pfo');
  QrPQ1.SQL.Add('LEFT JOIN pq pq ON pfo.PQ=pq.Codigo');
  QrPQ1.SQL.Add('WHERE pq.Codigo>0');
  QrPQ1.SQL.Add('AND pq.Ativo = 1');
  QrPQ1.SQL.Add('AND pq.GGXNiv2 > 0');
  QrPQ1.SQL.Add('ORDER BY pq.Nome');
  UnDmkDAC_PF.AbreQuery(QrPQ1, Dmod.MyDB);
end;

procedure TFmPQEEdit.SpeedButton1Click(Sender: TObject);

  procedure ConfiguraDados(Codigo: Integer);
  begin
    QrPQ1.Close;
    UnDmkDAC_PF.AbreQuery(QrPQ1, Dmod.MyDB);
    //
    EdProduto.ValueVariant := Codigo;
    CBProduto.KeyValue     := Codigo;
    EdProduto.SetFocus;
  end;

begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.CadastroPQ(EdProduto.ValueVariant);
  //
  if BDC_PQCAD <> 0 then
    ConfiguraDados(BDC_PQCAD)
  else if VAR_CADASTRO <> 0 then
    ConfiguraDados(VAR_CADASTRO);
end;

end.

