unit TintasIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, DBCtrls, Grids, DBGrids, DmkDAC_PF,
  dmkLabel, DB, mySQLDbTables, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkDBEdit, dmkGeral, MyDBCheck, UnInternalConsts, dmkImage, UnDmkEnums;

type
  TFmTintasIts = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    PainelEdita: TPanel;
    DBGrid1: TDBGrid;
    QrPQ: TmySQLQuery;
    DsPQ: TDataSource;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    Label3: TLabel;
    Label4: TLabel;
    EdProduto: TdmkEditCB;
    CBProduto: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdGramasTi: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    EdOrdem: TdmkEdit;
    SpeedButton1: TSpeedButton;
    Panel5: TPanel;
    Panel6: TPanel;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit4: TDBEdit;
    DBEdNumero: TdmkDBEdit;
    DBEdCodigo: TdmkDBEdit;
    EdControle: TdmkEdit;
    Label111: TLabel;
    CkContinuar: TCheckBox;
    QrSum: TmySQLQuery;
    QrSumGramasTi: TFloatField;
    Label8: TLabel;
    DBEdit2: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBControle: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtRecalcula: TBitBtn;
    BtSaida: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    Panel10: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    BtReordena: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtRecalculaClick(Sender: TObject);
    procedure BtReordenaClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    procedure VerificaOrdem(ZeraAntes: Boolean);
  public
    { Public declarations }
  end;

  var
  FmTintasIts: TFmTintasIts;

implementation

uses UnMyObjects, TintasCab, Module, UMySQLModule, Principal, UnReordena,
  BlueDermConsts, UnPQ_PF;

{$R *.DFM}

procedure TFmTintasIts.BtRecalculaClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := FmTintasCab.QrTintasItsControle.Value;
  PQ_PF.AtualizaItensProcesso(FmTintasCab.QrTintasTinCodigo.Value, Dmod.MyDB);
  FmTintasCab.ReopenTintasTin(FmTintasCab.QrTintasTinCodigo.Value);
  FmTintasCab.ReopenTintasIts(Controle);
end;

procedure TFmTintasIts.BtReordenaClick(Sender: TObject);
begin
  if (FmTintasCab.QrTintasIts.State <> dsInactive) and(FmTintasCab.QrTintasIts.RecordCount > 0) then
  begin
    UReordena.ReordenaItens(FmTintasCab.QrTintasIts, Dmod.QrUpd, 'tintasits',
      'Ordem', 'Controle', 'NOMEPQ', '', '', '', nil);
    //
    FmTintasCab.ReopenTintasIts(0);
  end;
end;

procedure TFmTintasIts.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, FmTintasCab.QrTintasIts,
  [GBControle], [PainelEdita], EdOrdem, ImgTipo, 'tintasits');
  VerificaOrdem(True);
end;

procedure TFmTintasIts.BtConfirmaClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('tintasits', 'Controle', ImgTipo.SQLType,
    FmTintasCab.QrTintasItsControle.Value);
  //EdControle.ValueVariant := Controle;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmTintasIts, PainelEdita,
  'TintasIts', Controle, Dmod.QrUpd, [PainelEdita], [GBControle],
  ImgTipo, True) then
  begin
    PQ_PF.AtualizaItensProcesso(FmTintasCab.QrTintasTinCodigo.Value, Dmod.MyDB);
    FmTintasCab.ReopenTintasTin(FmTintasCab.QrTintasTinCodigo.Value);
    FmTintasCab.ReopenTintasIts(Controle);
    if CkContinuar.Checked then
      BtIncluiClick(Self);
  end;
end;

procedure TFmTintasIts.BtDesisteClick(Sender: TObject);
begin
  UMyMod.CancelSQLInsUpdPanel(ImgTipo.SQLType, FmTintasIts, PainelEdita,
  'TintasIts', FmTintasCab.QrTintasItsControle.Value, Dmod.QrUpd,
  [PainelEdita], [GBControle], ImgTipo, 'Controle');
end;

procedure TFmTintasIts.BtExcluiClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, FmTintasCab.QrTintasIts, DBGrid1,
    'tintasits', ['Controle'], ['Controle'], istPergunta, '');
  //
  PQ_PF.AtualizaItensProcesso(FmTintasCab.QrTintasTinCodigo.Value, Dmod.MyDB);
  FmTintasCab.ReopenTintasTin(FmTintasCab.QrTintasTinCodigo.Value);
  FmTintasCab.ReopenTintasIts(0);
end;

procedure TFmTintasIts.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, FmTintasCab.QrTintasIts,
  [GBControle], [PainelEdita], EdOrdem, ImgTipo, 'tintasits');
  VerificaOrdem(True);
end;

procedure TFmTintasIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTintasIts.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  Bold: Boolean;
begin
  if (Column.FieldName = 'Status_TXT')then
  begin
    if FmTintasCab.QrTintasItsStatus_TXT.Value = CO_Forml_Status_Prod_Irregular then
    begin
      Cor  := clRed;
      Bold := True;
    end else
    begin
      Cor  := clGreen;
      Bold := False;
    end;
    with DBGrid1.Canvas do
    begin
      if Bold then
        Font.Style := [fsBold]
      else
        Font.Style := [];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmTintasIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTintasIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
end;

procedure TFmTintasIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTintasIts.SpeedButton1Click(Sender: TObject);
var
  Produto: Integer;
begin
  VAR_CADASTRO := 0;
  Produto      := EdProduto.ValueVariant;
  //
  FmPrincipal.CadastroPQ(Produto);
  //
  if VAR_CADASTRO <> 0 then
  begin
    EdProduto.ValueVariant := VAR_CADASTRO;
    CBProduto.KeyValue     := VAR_CADASTRO;
    EdProduto.SetFocus;
  end;
end;

procedure TFmTintasIts.VerificaOrdem(ZeraAntes: Boolean);
begin
  if ZeraAntes then
    EdOrdem.ValueVariant := 0;
  if EdOrdem.ValueVariant = 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT MAX(Ordem) Ordem');
    Dmod.QrAux.SQL.Add('FROM tintasits');
    Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrAux.Params[0].AsInteger := FmTintasCab.QrTintasTinCodigo.Value;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    EdOrdem.ValueVariant := Dmod.QrAux.FieldByName('Ordem').AsInteger + 1;
  end;
end;

end.

