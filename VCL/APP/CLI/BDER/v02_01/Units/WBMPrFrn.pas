unit WBMPrFrn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmWBMPrFrn = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdGraGruX: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    Label6: TLabel;
    CBFornece: TdmkDBLookupComboBox;
    EdFornece: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTIDADE: TWideStringField;
    DsFornece: TDataSource;
    EdM2Medio: TdmkEdit;
    Label001: TdmkLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenWBMPrFrn(Codigo: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmWBMPrFrn: TFmWBMPrFrn;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmWBMPrFrn.BtOKClick(Sender: TObject);
var
  Codigo, GraGruX, Fornece: Integer;
  M2Medio: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  GraGruX        := Geral.IMV(DBEdGraGruX.Text);
  Fornece        := EdFornece.ValueVariant;
  M2Medio        := EdM2Medio.ValueVariant;
  if MyObjects.FIC(Fornece = 0, EdFornece, 'Informe o fornecedor!') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('wbmprfrn', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'wbmprfrn', False, [
  'GraGruX', 'Fornece', 'M2Medio'], [
  'Codigo'], [
  GraGruX, Fornece, M2Medio], [
  Codigo], True) then
  begin
    ReopenWBMPrFrn(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdCodigo.ValueVariant    := 0;
      EdFornece.ValueVariant   := 0;
      CBFornece.KeyValue       := Null;
      EdM2Medio.ValueVariant   := 0;
      //
      EdFornece.SetFocus;
    end else Close;
  end;
end;

procedure TFmWBMPrFrn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWBMPrFrn.FormActivate(Sender: TObject);
begin
  DBEdGraGruX.DataSource := FDsCab;
  DBEdGraGruX.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmWBMPrFrn.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
end;

procedure TFmWBMPrFrn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBMPrFrn.ReopenWBMPrFrn(Codigo: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    if Codigo <> 0 then
      FQrIts.Locate('Codigo', Codigo, []);
  end;
end;

end.
