object FmPQB3CiImp: TFmPQB3CiImp
  Left = 339
  Top = 185
  Caption = 'QUI-BALAN-013 :: Ciclo de Balan'#231'o - Impress'#227'o'
  ClientHeight = 278
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 640
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 592
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 544
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 357
        Height = 32
        Caption = 'Ciclo de Balan'#231'o - Impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 357
        Height = 32
        Caption = 'Ciclo de Balan'#231'o - Impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 357
        Height = 32
        Caption = 'Ciclo de Balan'#231'o - Impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 164
    Width = 640
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 580
        Height = 17
        Caption = 
          'Ser'#225' considerado o saldo final do dia anterior ao selecionado pa' +
          'ra o saldo a ser impresso'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 580
        Height = 17
        Caption = 
          'Ser'#225' considerado o saldo final do dia anterior ao selecionado pa' +
          'ra o saldo a ser impresso'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 208
    Width = 640
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 494
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 492
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 640
    Height = 116
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 640
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label2: TLabel
        Left = 12
        Top = 4
        Width = 41
        Height = 13
        Caption = 'Per'#237'odo:'
      end
      object Label3: TLabel
        Left = 300
        Top = 4
        Width = 64
        Height = 13
        Caption = 'Ciclo anterior:'
      end
      object EdPeriodo: TdmkEdit
        Left = 12
        Top = 20
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdPeriodo2: TdmkEdit
        Left = 68
        Top = 20
        Width = 229
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdDataAnt: TdmkEdit
        Left = 300
        Top = 20
        Width = 105
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfDate
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0d
        ValWarn = False
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 49
      Width = 640
      Height = 67
      Align = alClient
      Caption = 'Data da impress'#227'o (exclusa):'
      TabOrder = 1
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 636
        Height = 50
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 65
          Height = 13
          Caption = 'Data exclusa:'
        end
        object TPDataAtu: TdmkEditDateTimePicker
          Left = 8
          Top = 16
          Width = 112
          Height = 21
          Date = 45225.000000000000000000
          Time = 0.361018391202378600
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65527
  end
  object frxQUI_BALAN_013_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41537.466215289350000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxQUI_BALAN_013_01GetValue
    Left = 468
    Top = 110
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsIterm
        DataSetName = 'frxDsIterm'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 82.252010000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 64.251844020000000000
          Width = 215.432946380000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 215.432941500000000000
          Top = 64.251844020000000000
          Width = 68.031496060000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dif. anterior')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464481500000000000
          Top = 64.251844020000000000
          Width = 26.456651420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'L?')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'BALAN'#199'O INTERMEDI'#193'RIO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_REF]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Top = 64.252010000000000000
          Width = 75.590551181102360000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtd Atual')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 64.252010000000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtd F'#237'sica')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Top = 64.252010000000000000
          Width = 204.094561420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF / Lote (para re-entrada)')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD01: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897803780000000000
        Top = 207.874150000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsIterm
        DataSetName = 'frxDsIterm'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236205830000000000
          Width = 185.196850393700800000
          Height = 18.897637795275590000
          DataField = 'NO_PQ'
          DataSet = frxDsIterm
          DataSetName = 'frxDsIterm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsIterm."NO_PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 215.432946380000000000
          Width = 68.031496060000000000
          Height = 18.897637800000000000
          DataField = 'Ant_PesoDife'
          DataSet = frxDsIterm
          DataSetName = 'frxDsIterm'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsIterm."Ant_PesoDife"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464408270000000000
          Width = 26.456651420000000000
          Height = 18.897637800000000000
          DataSet = frxDsIterm
          DataSetName = 'frxDsIterm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsIterm."NO_Lancar"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 30.236220472440940000
          Height = 18.897637795275590000
          DataField = 'Insumo'
          DataSet = frxDsIterm
          DataSetName = 'frxDsIterm'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsIterm."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921464880000000000
          Top = 0.000165979999999996
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsIterm
          DataSetName = 'frxDsIterm'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsIterm."PesoAtuT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512064880000000000
          Top = 0.000165979999999996
          Width = 90.708661420000000000
          Height = 18.897637800000000000
          DataSet = frxDsIterm
          DataSetName = 'frxDsIterm'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 204.094561420000000000
          Height = 18.897637800000000000
          DataSet = frxDsIterm
          DataSetName = 'frxDsIterm'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 332.598640000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 377.953000000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'L? = Lan'#231'ado?')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsIterm."CliOrig"'
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsIterm."CliOrig"] - [frxDsIterm."NO_ENT"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Width = 309.921460000000000000
          Height = 18.897650000000000000
          DataSet = frxDsIterm
          DataSetName = 'frxDsIterm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'TOTAL [frxDsIterm."NO_ENT"]: ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsIterm
          DataSetName = 'frxDsIterm'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsIterm."PesoAtuT">,MD01)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Width = 294.803281420000000000
          Height = 18.897637800000000000
          DataSet = frxDsIterm
          DataSetName = 'frxDsIterm'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsIterm: TfrxDBDataset
    UserName = 'frxDsIterm'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_ENT=NO_ENT'
      'NO_PQ=NO_PQ'
      'Ant_Empresa=Ant_Empresa'
      'Ant_CliInt=Ant_CliInt'
      'Ant_Insumo=Ant_Insumo'
      'Ant_Tipo=Ant_Tipo'
      'Ant_DstCodi=Ant_DstCodi'
      'Ant_DstCtrl=Ant_DstCtrl'
      'Ant_PesoDife=Ant_PesoDife'
      'Ant_Lancar=Ant_Lancar'
      'Empresa=Empresa'
      'CliOrig=CliOrig'
      'Insumo=Insumo'
      'PesoInic=PesoInic'
      'ValorInic=ValorInic'
      'PesoAtuA=PesoAtuA'
      'ValorAtuA=ValorAtuA'
      'PesoAtuB=PesoAtuB'
      'ValorAtuB=ValorAtuB'
      'PesoAtuC=PesoAtuC'
      'ValorAtuC=ValorAtuC'
      'PesoPeIn=PesoPeIn'
      'ValorPeIn=ValorPeIn'
      'PesoPeBx=PesoPeBx'
      'ValorPeBx=ValorPeBx'
      'PesoAjIn=PesoAjIn'
      'ValorAjIn=ValorAjIn'
      'PesoAjBx=PesoAjBx'
      'ValorAjBx=ValorAjBx'
      'PesoInfo=PesoInfo'
      'ValorInfo=ValorInfo'
      'PesoDife=PesoDife'
      'ValorDife=ValorDife'
      'Serie=Serie'
      'NF=NF'
      'xLote=xLote'
      'dFab=dFab'
      'dVal=dVal'
      'Ativo=Ativo'
      'NO_Lancar=NO_Lancar'
      'PesoAtuT=PesoAtuT'
      'ValorAtuT=ValorAtuT')
    DataSet = QrInterm
    BCDToCurrency = False
    DataSetOptions = []
    Left = 468
    Top = 60
  end
  object QrInterm: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _pqbciits_ant_'
      ';'
      'CREATE TABLE _pqbciits_ant_'
      'SELECT Empresa Ant_Empresa, CliInt Ant_CliInt, '
      'Insumo Ant_Insumo, Tipo Ant_Tipo, '
      'DstCodi Ant_DstCodi, DstCtrl Ant_DstCtrl,'
      'PesoDife Ant_PesoDife, Lancar Ant_Lancar'
      'FROM bluederm.pqbciits'
      'WHERE Controle=-999999999'
      'AND PesoDife <> 0'
      ';'
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'pq.Nome NO_PQ, ant.*, atu.*'
      'FROM _pqb_previa_ atu '
      'LEFT JOIN _pqbciits_ant_ ant ON '
      '  ant.Ant_Empresa=atu.Empresa'
      '  AND ant.Ant_CliInt=atu.CliOrig'
      '  AND ant.Ant_Insumo=atu.Insumo'
      'LEFT JOIN bluederm.entidades ent ON ent.Codigo=atu.CliOrig'
      'LEFT JOIN bluederm.pq pq ON pq.Codigo=atu.Insumo')
    Left = 465
    Top = 11
    object QrIntermNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrIntermNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrIntermAnt_Empresa: TIntegerField
      FieldName = 'Ant_Empresa'
    end
    object QrIntermAnt_CliInt: TIntegerField
      FieldName = 'Ant_CliInt'
    end
    object QrIntermAnt_Insumo: TIntegerField
      FieldName = 'Ant_Insumo'
    end
    object QrIntermAnt_Tipo: TIntegerField
      FieldName = 'Ant_Tipo'
    end
    object QrIntermAnt_DstCodi: TIntegerField
      FieldName = 'Ant_DstCodi'
    end
    object QrIntermAnt_DstCtrl: TIntegerField
      FieldName = 'Ant_DstCtrl'
    end
    object QrIntermAnt_PesoDife: TFloatField
      FieldName = 'Ant_PesoDife'
    end
    object QrIntermAnt_Lancar: TSmallintField
      FieldName = 'Ant_Lancar'
    end
    object QrIntermEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrIntermCliOrig: TIntegerField
      FieldName = 'CliOrig'
      Required = True
    end
    object QrIntermInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrIntermPesoInic: TFloatField
      FieldName = 'PesoInic'
    end
    object QrIntermValorInic: TFloatField
      FieldName = 'ValorInic'
    end
    object QrIntermPesoAtuA: TFloatField
      FieldName = 'PesoAtuA'
    end
    object QrIntermValorAtuA: TFloatField
      FieldName = 'ValorAtuA'
    end
    object QrIntermPesoAtuB: TFloatField
      FieldName = 'PesoAtuB'
      Required = True
    end
    object QrIntermValorAtuB: TFloatField
      FieldName = 'ValorAtuB'
      Required = True
    end
    object QrIntermPesoAtu: TFloatField
      FieldName = 'PesoAtuC'
      Required = True
    end
    object QrIntermValorAtuC: TFloatField
      FieldName = 'ValorAtuC'
      Required = True
    end
    object QrIntermPesoPeIn: TFloatField
      FieldName = 'PesoPeIn'
    end
    object QrIntermValorPeIn: TFloatField
      FieldName = 'ValorPeIn'
    end
    object QrIntermPesoPeBx: TFloatField
      FieldName = 'PesoPeBx'
    end
    object QrIntermValorPeBx: TFloatField
      FieldName = 'ValorPeBx'
    end
    object QrIntermPesoAjIn: TFloatField
      FieldName = 'PesoAjIn'
      Required = True
    end
    object QrIntermValorAjIn: TFloatField
      FieldName = 'ValorAjIn'
      Required = True
    end
    object QrIntermPesoAjBx: TFloatField
      FieldName = 'PesoAjBx'
      Required = True
    end
    object QrIntermValorAjBx: TFloatField
      FieldName = 'ValorAjBx'
      Required = True
    end
    object QrIntermPesoInfo: TFloatField
      FieldName = 'PesoInfo'
      Required = True
    end
    object QrIntermValorInfo: TFloatField
      FieldName = 'ValorInfo'
      Required = True
    end
    object QrIntermPesoDife: TFloatField
      FieldName = 'PesoDife'
      Required = True
    end
    object QrIntermValorDife: TFloatField
      FieldName = 'ValorDife'
      Required = True
    end
    object QrIntermSerie: TIntegerField
      FieldName = 'Serie'
      Required = True
    end
    object QrIntermNF: TIntegerField
      FieldName = 'NF'
      Required = True
    end
    object QrIntermxLote: TWideStringField
      FieldName = 'xLote'
      Required = True
    end
    object QrIntermdFab: TDateField
      FieldName = 'dFab'
      Required = True
    end
    object QrIntermdVal: TDateField
      FieldName = 'dVal'
      Required = True
    end
    object QrIntermAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrIntermNO_Lancar: TWideStringField
      FieldName = 'NO_Lancar'
      Size = 3
    end
    object QrIntermPesoAtuT: TFloatField
      FieldName = 'PesoAtuT'
    end
    object QrIntermValorAtuT: TFloatField
      FieldName = 'ValorAtuT'
    end
  end
end
