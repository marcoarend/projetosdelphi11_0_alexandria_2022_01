unit PQEEdit2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnInternalConsts,
  UnMsgInt, ExtCtrls, UnInternalConsts2, mySQLDbTables, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, ComCtrls, dmkMemo, dmkRadioGroup,
  dmkLabel, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmPQEEdit2 = class(TForm)
    QrPQ1: TmySQLQuery;
    DsPQ1: TDataSource;
    QrLocalizaPQ: TmySQLQuery;
    Panel1: TPanel;
    Painel1: TPanel;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    PainelDados: TPanel;
    Label1: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label10: TLabel;
    Label19: TLabel;
    Label5: TLabel;
    EdProduto: TdmkEditCB;
    CBProduto: TdmkDBLookupComboBox;
    EdVolumes: TdmkEdit;
    EdkgLiq: TdmkEdit;
    EdVlrItem: TdmkEdit;
    EdTotalkgLiq: TdmkEdit;
    EdValorkg: TdmkEdit;
    CkAmostra: TCheckBox;
    SpeedButton1: TSpeedButton;
    QrPQ1NOMEPQ: TWideStringField;
    QrPQ1Controle: TIntegerField;
    QrPQ1IQ: TIntegerField;
    QrPQ1PQ: TIntegerField;
    QrPQ1CI: TIntegerField;
    QrPQ1Prazo: TWideStringField;
    QrPQ1Lk: TIntegerField;
    QrPQ1DataCad: TDateField;
    QrPQ1DataAlt: TDateField;
    QrPQ1UserCad: TIntegerField;
    QrPQ1UserAlt: TIntegerField;
    EdTotalkgBruto: TdmkEdit;
    EdKgBruto: TdmkEdit;
    Label3: TLabel;
    Label6: TLabel;
    EdIPI_pIPI: TdmkEdit;
    Label4: TLabel;
    Label7: TLabel;
    EdIPI_vIPI: TdmkEdit;
    EdCustoItem: TdmkEdit;
    Label8: TLabel;
    Panel25: TPanel;
    Panel4: TPanel;
    Label231: TLabel;
    Label235: TLabel;
    Label236: TLabel;
    Label237: TLabel;
    Label238: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label9: TLabel;
    Label232: TLabel;
    Label234: TLabel;
    SpeedButton2: TSpeedButton;
    EdnItem: TdmkEdit;
    Edprod_NCM: TdmkEdit;
    Edprod_EXTIPI: TdmkEdit;
    Edprod_genero: TdmkEdit;
    Edprod_CFOP: TdmkEdit;
    EdFatNum: TdmkEdit;
    EdEmpresa: TdmkEdit;
    EdFatID: TdmkEdit;
    Edprod_cProd: TdmkEdit;
    Edprod_xProd: TdmkEdit;
    Panel5: TPanel;
    GroupBox11: TGroupBox;
    Label242: TLabel;
    Label241: TLabel;
    Label240: TLabel;
    Label239: TLabel;
    Label233: TLabel;
    Label247: TLabel;
    Label248: TLabel;
    Label249: TLabel;
    Edprod_qCom: TdmkEdit;
    Edprod_vProd: TdmkEdit;
    Edprod_vUnCom: TdmkEdit;
    Edprod_uCom: TdmkEdit;
    Edprod_cEAN: TdmkEdit;
    Edprod_vFrete: TdmkEdit;
    Edprod_vSeg: TdmkEdit;
    Edprod_vDesc: TdmkEdit;
    GroupBox12: TGroupBox;
    Label001: TLabel;
    Label11: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Edprod_qTrib: TdmkEdit;
    Edprod_vUnTrib: TdmkEdit;
    Edprod_uTrib: TdmkEdit;
    Edprod_cEANTrib: TdmkEdit;
    PageControl3: TPageControl;
    TabSheet18: TTabSheet;
    Panel6: TPanel;
    Panel31: TPanel;
    GroupBox13: TGroupBox;
    GroupBox14: TGroupBox;
    STAviso1: TStaticText;
    Panel7: TPanel;
    LaICMS_pRedBC: TLabel;
    RGICMS_modBC: TdmkRadioGroup;
    EdICMS_pRedBC: TdmkEdit;
    Panel38: TPanel;
    Label244: TLabel;
    Label245: TLabel;
    EdICMS_Orig: TdmkEdit;
    EdTextoA: TdmkEdit;
    EdICMS_CST: TdmkEdit;
    EdTextoB: TdmkEdit;
    GroupBox15: TGroupBox;
    LaICMS_pMVAST: TLabel;
    LaICMS_pRedBCST: TLabel;
    RGICMS_modBCST: TdmkRadioGroup;
    EdICMS_pRedBCST: TdmkEdit;
    EdICMS_pMVAST: TdmkEdit;
    GroupBox16: TGroupBox;
    LaICMS_VBC: TLabel;
    LaICMS_pICMS: TLabel;
    Label258: TLabel;
    EdICMS_vBC: TdmkEdit;
    EdICMS_vICMS: TdmkEdit;
    EdICMS_pICMS: TdmkEdit;
    GroupBox17: TGroupBox;
    Label259: TLabel;
    LaICMS_pICMSST: TLabel;
    Label261: TLabel;
    EdICMS_vBCST: TdmkEdit;
    EdICMS_pICMSST: TdmkEdit;
    EdICMS_vICMSST: TdmkEdit;
    TabSheet19: TTabSheet;
    Panel34: TPanel;
    Panel39: TPanel;
    GroupBox18: TGroupBox;
    Label264: TLabel;
    EdIPI_CST: TdmkEdit;
    EdTextoIPI_CST: TdmkEdit;
    GroupBox19: TGroupBox;
    Label265: TLabel;
    Label266: TLabel;
    Label267: TLabel;
    Label268: TLabel;
    Label273: TLabel;
    EdIPI_clEnq: TdmkEdit;
    EdIPI_CNPJProd: TdmkEdit;
    EdIPI_cSelo: TdmkEdit;
    EdIPI_qSelo: TdmkEdit;
    Panel40: TPanel;
    Label251: TLabel;
    Label262: TLabel;
    Label263: TLabel;
    Label269: TLabel;
    Label270: TLabel;
    Label271: TLabel;
    Label272: TLabel;
    EdIPI_cEnq: TdmkEdit;
    dmkEdit1: TdmkEdit;
    EdIPI_vUnid: TdmkEdit;
    EdIPI_vBC: TdmkEdit;
    EdIPI_qUnid: TdmkEdit;
    dmkEdit2: TdmkEdit;
    TabSheet20: TTabSheet;
    Panel33: TPanel;
    Label301: TLabel;
    Label302: TLabel;
    Label303: TLabel;
    Label304: TLabel;
    EdII_vBC: TdmkEdit;
    EdII_vDespAdu: TdmkEdit;
    EdII_vII: TdmkEdit;
    EdII_vIOF: TdmkEdit;
    TabSheet21: TTabSheet;
    Panel32: TPanel;
    GroupBox20: TGroupBox;
    Label274: TLabel;
    Label281: TLabel;
    EdTextoPIS_CST: TdmkEdit;
    EdPIS_CST: TdmkEdit;
    GroupBox22: TGroupBox;
    Label279: TLabel;
    Label276: TLabel;
    EdPIS_vBC: TdmkEdit;
    EdPIS_pPIS: TdmkEdit;
    GroupBox23: TGroupBox;
    Label275: TLabel;
    Label282: TLabel;
    EdPIS_qBCProd: TdmkEdit;
    EdPIS_vAliqProd: TdmkEdit;
    EdPIS_vPIS: TdmkEdit;
    GroupBox21: TGroupBox;
    Label284: TLabel;
    GroupBox24: TGroupBox;
    Label277: TLabel;
    Label278: TLabel;
    EdPISST_vBC: TdmkEdit;
    EdPISST_pPIS: TdmkEdit;
    GroupBox25: TGroupBox;
    Label280: TLabel;
    Label283: TLabel;
    EdPISST_qBCProd: TdmkEdit;
    EdPISST_vAliqProd: TdmkEdit;
    EdPISST_vPIS: TdmkEdit;
    TabSheet22: TTabSheet;
    Panel35: TPanel;
    Panel41: TPanel;
    GroupBox26: TGroupBox;
    Label285: TLabel;
    Label286: TLabel;
    EdTextoCOFINS_CST: TdmkEdit;
    EdCOFINS_CST: TdmkEdit;
    GroupBox27: TGroupBox;
    Label287: TLabel;
    Label288: TLabel;
    EdCOFINS_vBC: TdmkEdit;
    EdCOFINS_pCOFINS: TdmkEdit;
    GroupBox28: TGroupBox;
    Label289: TLabel;
    Label290: TLabel;
    EdCOFINS_qBCProd: TdmkEdit;
    EdCOFINS_vAliqProd: TdmkEdit;
    EdCOFINS_vCOFINS: TdmkEdit;
    GroupBox29: TGroupBox;
    Label291: TLabel;
    GroupBox30: TGroupBox;
    Label292: TLabel;
    Label293: TLabel;
    EdCOFINSST_vBC: TdmkEdit;
    EdCOFINSST_pCOFINS: TdmkEdit;
    GroupBox31: TGroupBox;
    Label294: TLabel;
    Label295: TLabel;
    EdCOFINSST_qBCProd: TdmkEdit;
    EdCOFINSST_vAliqProd: TdmkEdit;
    EdCOFINSST_vCOFINS: TdmkEdit;
    TabSheet24: TTabSheet;
    Panel37: TPanel;
    MeInfAdProd: TdmkMemo;
    Panel8: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    REWarning: TRichEdit;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdVolumesChange(Sender: TObject);
    procedure EdKgBrutoChange(Sender: TObject);
    procedure EdkgLiqChange(Sender: TObject);
    procedure EdVlrItemChange(Sender: TObject);
    procedure EdVolumesExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdTotalkgBrutoChange(Sender: TObject);
    procedure EdTotalkgLiqChange(Sender: TObject);
    procedure EdTotalkgLiqExit(Sender: TObject);
    procedure EdProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdkgLiqExit(Sender: TObject);
    procedure EdIPI_pIPIChange(Sender: TObject);
    procedure EdIPI_vIPIChange(Sender: TObject);
    procedure EdProdutoChange(Sender: TObject);
  private
    { Private declarations }
    FVol, FkgU, FkgT: Double;
    procedure MostraTodosPQs;
    procedure CalculaIPI();
  public
    { Public declarations }
    procedure Pesos(Index: Integer);
    procedure CalculaOnEdit;
  end;

var
  FmPQEEdit2: TFmPQEEdit2;

implementation

uses UnMyObjects, PQE, Module, Principal, BlueDermConsts, UMySQLModule;

{$R *.DFM}


procedure TFmPQEEdit2.CalculaOnEdit;
var
  Volumes, TotalkgLiq, kgBruto, CustoItem, ValorItem, IPI_vIPI,
  Valor, CFin : Double;
begin
  Volumes    := Geral.DMV(EdVolumes.Text);
  //kgLiq      := Geral.DMV(EdkgLiq.Text);
  kgBruto    := Geral.DMV(EdKgBruto.Text);
  ValorItem  := Geral.DMV(EdVlrItem.Text);
  TotalkgLiq := Geral.DMV(EdTotalkgLiq.Text);
  //TotalkgBru := Geral.DMV(EdTotalkgBruto.Text);
  //
  if FmPQE.QrPQEValorNF.Value > 0 then
  CFin       := 1+(FmPQE.QrPQEJuros.Value / FmPQE.QrPQEValorNF.Value)
  else CFin  := 1;
  //
  IPI_vIPI  := EdIPI_vIPI.ValueVariant;
  CustoItem := ValorItem + IPI_vIPI * CFin;
  if TotalkgLiq > 0 then Valor := CustoItem / TotalkgLiq else Valor := 0;
  //
  EdCustoItem.ValueVariant    := CustoItem;
  EdValorkg.ValueVariant      := Valor;
  EdTotalkgBruto.ValueVariant := Volumes * kgBruto;
end;

procedure TFmPQEEdit2.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQEEdit2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl3.ActivePageIndex := 0;
  QrPQ1.Params[0].AsInteger := FmPQE.QrPQECI.Value;
  QrPQ1.Params[1].AsInteger := FmPQE.QrPQEIQ.Value;
  UnDmkDAC_PF.AbreQuery(QrPQ1, Dmod.MyDB);
end;

procedure TFmPQEEdit2.BtConfirmaClick(Sender: TObject);
var
  Cursor : TCursor;
  TotalCusto, ValorItem, IPI_pIPI, IPI_vIPI, kgLiq, kgBruto: Double;
  Controle, Conta, Produto, Codigo, Volumes : Integer;
  //Amostra: String;
  //Atualiza: Boolean;
  //RICMS, RICMSF, , Preco, Valor, Frete, Juros, CFin
begin
//  Atualiza := False;
  Produto := CBProduto.KeyValue;
  if Produto < 1 then
  begin
    ShowMessage(FIN_MSG_DEFPRODUTO);
    EdProduto.SetFocus;
    Exit;
  end;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    Codigo := FmPQE.QrPQECodigo.Value;
    Conta := EdCodigo.ValueVariant;

    Produto    := CBProduto.KeyValue;
    Volumes    := Trunc(Geral.DMV(EdVolumes.Text));
    kgLiq      := Geral.DMV(EdkgLiq.Text);
    kgBruto    := Geral.DMV(EdKgBruto.Text);
    ValorItem  := Geral.DMV(EdVlrItem.Text);
    IPI_pIPI   := Geral.DMV(EdIPI_pIPI.Text);
    IPI_vIPI   := Geral.DMV(EdIPI_vIPI.Text);
    TotalCusto := EdCustoItem.ValueVariant;
    if ValorItem<= 0 then
    begin
      ShowMessage('Defina o valor do item.');
      Screen.Cursor := Cursor;
      Exit;
    end;
    if Volumes<=0 then
    begin
      ShowMessage('Defina os volumes do item.');
      Screen.Cursor := Cursor;
      Exit;
    end;
    if int(Geral.DMV(EdVolumes.Text) * 1000) <> (Volumes * 1000) then
    begin
      ShowMessage('Volumes devem ser um n�mero inteiro.');
      Screen.Cursor := Cursor;
      Exit;
    end;
    (*if CkAmostra.Checked then Amostra := 'V' else amostra := 'F';
    Dmod.QrUpdW.SQL.Clear;
    Dmod.QrUpdW.SQL.Add('UPDATE pq SET Amostra=:P0');
    Dmod.QrUpdW.SQL.Add('WHERE Codigo=:P1');
    Dmod.QrUpdW.Params[0].AsString  := Amostra;
    Dmod.QrUpdW.Params[1].AsInteger := Produto;
    Dmod.QrUpdW.ExecSQL;*)

    if ImgTipo.SQLType = stIns then
    begin
      Dmod.QrUpdW.DataBase := Dmod.MyDB;
      Dmod.QrUpdW.SQL.Clear;
      Dmod.QrUpdW.SQL.Add('UPDATE pqeits SET Conta=Conta+1');
      Dmod.QrUpdW.SQL.Add('WHERE Conta>=:P0 AND Codigo=:P1');
      Dmod.QrUpdW.Params[0].AsInteger := Conta;
      Dmod.QrUpdW.Params[1].AsInteger := Codigo;
      Dmod.QrUpdW.ExecSQL;
      //
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'PQEIts','PQEIts','Controle');
      //
      Dmod.QrUpdW.DataBase := Dmod.MyDB;
      Dmod.QrUpdW.SQL.Clear;
      Dmod.QrUpdW.SQL.Add('INSERT INTO pqeits SET Insumo=:P0, Volumes=:P1, ');
      Dmod.QrUpdW.SQL.Add('PesoVB=:P2, PesoVL=:P3, ValorItem=:P4, ');
      Dmod.QrUpdW.SQL.Add('IPI_pIPI=:P5, IPI_vIPI=:P6, TotalCusto=:P7, ');
      Dmod.QrUpdW.SQL.Add('TotalPeso=:P8, Codigo=:P9, Conta=:10, Controle=:P11');
      Dmod.QrUpdW.Params[00].AsInteger := Produto;
      Dmod.QrUpdW.Params[01].AsInteger := Volumes;
      Dmod.QrUpdW.Params[02].AsFloat := kgBruto;
      Dmod.QrUpdW.Params[03].AsFloat := kgLiq;
      Dmod.QrUpdW.Params[04].AsFloat := ValorItem;
      Dmod.QrUpdW.Params[05].AsFloat := IPI_pIPI;
      Dmod.QrUpdW.Params[06].AsFloat := IPI_vIPI;
      Dmod.QrUpdW.Params[07].AsFloat := TotalCusto;
      Dmod.QrUpdW.Params[08].AsFloat := Volumes*kgLiq;
      Dmod.QrUpdW.Params[09].AsInteger := Codigo;
      Dmod.QrUpdW.Params[10].AsInteger := Conta;
      Dmod.QrUpdW.Params[11].AsInteger := Controle;
      Dmod.QrUpdW.ExecSQL;
      FmPQE.FControle := Controle;
    end
    else
    begin
      Dmod.QrUpdW.DataBase := Dmod.MyDB;
      Dmod.QrUpdW.SQL.Clear;
      Dmod.QrUpdW.SQL.Add('UPDATE pqeits SET Insumo=:P0, Volumes=:P1, ');
      Dmod.QrUpdW.SQL.Add('PesoVB=:P2, PesoVL=:P3, ValorItem=:P4, IPI=:P5, ');
      Dmod.QrUpdW.SQL.Add('RIPI=:P6, TotalCusto=:P7, TotalPeso=:P8 ');
      Dmod.QrUpdW.SQL.Add('WHERE Codigo=:P9 AND Conta=:10 AND Controle=:P11');
      Dmod.QrUpdW.Params[0].AsInteger := Produto;
      Dmod.QrUpdW.Params[1].AsInteger := Volumes;
      Dmod.QrUpdW.Params[2].AsFloat := kgBruto;
      Dmod.QrUpdW.Params[3].AsFloat := kgLiq;
      Dmod.QrUpdW.Params[4].AsFloat := ValorItem;
      Dmod.QrUpdW.Params[5].AsFloat := IPI_pIPI;
      Dmod.QrUpdW.Params[6].AsFloat := IPI_vIPI;
      Dmod.QrUpdW.Params[7].AsFloat := TotalCusto;
      Dmod.QrUpdW.Params[8].AsFloat := Volumes*kgLiq;
      Dmod.QrUpdW.Params[9].AsInteger := Codigo;
      Dmod.QrUpdW.Params[10].AsInteger := Conta;
      Dmod.QrUpdW.Params[11].AsInteger := FmPQE.QrPQEItsControle.Value;
      Dmod.QrUpdW.ExecSQL;
      FmPQE.FControle := FmPQE.QrPQEItsControle.Value;
    end;
  except
    Screen.Cursor := crDefault;
    Exit;
  end;
  Screen.Cursor := crDefault;
  if ImgTipo.SQLType = stIns then
  begin
    FmPQE.ReopenPQEIts(FmPQE.QrPQECodigo.Value);
    EdProduto.Text     := '';
    CBProduto.KeyValue := NULL;
    EdKgBruto.Text     := '';
    EdkgLiq.Text       := '';
    EdTotalkgLiq.Text  := '';
    EdVolumes.Text     := '';
    EdIPI_pIPI.Text    := '';
    EdIPI_vIPI.Text    := '';
    EdVlrItem.Text     := '';
    EdProduto.SetFocus;
  end else Close;  
end;

procedure TFmPQEEdit2.EdVolumesChange(Sender: TObject);
begin
  if EdVolumes.Focused then
  begin
    Pesos(2);
    CalculaOnEdit;
  end;  
end;

procedure TFmPQEEdit2.EdIPI_pIPIChange(Sender: TObject);
begin
  CalculaIPI();
end;

procedure TFmPQEEdit2.EdIPI_vIPIChange(Sender: TObject);
begin
  CalculaOnEdit();
end;

procedure TFmPQEEdit2.EdKgBrutoChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmPQEEdit2.EdkgLiqChange(Sender: TObject);
begin
  if EdkgLiq.Focused then
  begin
    Pesos(0);
    CalculaOnEdit;
  end;
end;

procedure TFmPQEEdit2.EdkgLiqExit(Sender: TObject);
begin
  EdKgLiq.ValueVariant := FkgU;
end;

procedure TFmPQEEdit2.EdVlrItemChange(Sender: TObject);
begin
  CalculaIPI();
  CalculaOnEdit;
end;

procedure TFmPQEEdit2.EdVolumesExit(Sender: TObject);
begin
  EdVolumes.ValueVariant := FVol;
end;

procedure TFmPQEEdit2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQEEdit2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQEEdit2.EdTotalkgBrutoChange(Sender: TObject);
begin
 //if EdTotalkgBruto.Focused then FOnEditA := 3;
  CalculaOnEdit;
end;

procedure TFmPQEEdit2.EdTotalkgLiqChange(Sender: TObject);
begin
  if EdTotalkgLiq.Focused then
  begin
    Pesos(1);
    CalculaOnEdit;
  end;
end;

procedure TFmPQEEdit2.EdTotalkgLiqExit(Sender: TObject);
begin
  EdTotalkgLiq.ValueVariant := FkgT;
end;

procedure TFmPQEEdit2.Pesos(Index: Integer);
var
  kgU, kgT, Vol: Double;
begin
  kgU := Geral.DMV(EdkgLiq.Text);
  kgT := Geral.DMV(EdTotalkgLiq.Text);
  Vol := Geral.DMV(EdVolumes.Text);
  //
  case Index of
    // Qtde por volume
    0: {if (kgT <> 0) and (kgU > 0) then
        Vol := kgT / kgU
      else}
        kgT := kgU * Vol;
    // Qtde total
    1: {if (kgT <> 0) and (kgU > 0) then Vol := kgT / kgU else
      begin
        if (Vol>0) then
        begin
          if kgU > 0 then
            kgT := kgU * Vol
          else}
            kgU := kgT / Vol;
        {end;
      end;}
    // Volumes
    2: {
    if (kgT <> 0) and (Vol > 0) then
        kgU := kgT / Vol
      else}
        kgT := kgU * Vol;
  end;
  FkgU := kgU;
  FkgT := kgT;
  FVol := Vol;
  //
  if index <> 0 then EdKgLiq.ValueVariant      := FkgU;
  if index <> 1 then EdTotalkgLiq.ValueVariant := FkgT;
  if index <> 2 then EdVolumes.ValueVariant    := FVol;
end;

procedure TFmPQEEdit2.EdProdutoChange(Sender: TObject);
begin
  // Parei aqui buscar dados
end;

procedure TFmPQEEdit2.EdProdutoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then MostraTodosPQs;
end;

procedure TFmPQEEdit2.CBProdutoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then MostraTodosPQs;
end;

procedure TFmPQEEdit2.CalculaIPI();
begin
  EdIPI_vIPI.ValueVariant :=
  EdIPI_pIPI.ValueVariant *
  EdVlrItem.ValueVariant / 100;
end;

procedure TFmPQEEdit2.MostraTodosPQs;
begin
  QrPQ1.Close;
  QrPQ1.SQL.Clear;
  QrPQ1.SQL.Add('SELECT * FROM pq');
  QrPQ1.SQL.Add('WHERE Codigo>0');
  QrPQ1.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrPQ1, Dmod.MyDB);
end;

procedure TFmPQEEdit2.SpeedButton1Click(Sender: TObject);
begin
  FmPrincipal.CadastroPQ(EdProduto.ValueVariant);
  if BDC_PQCAD <> 0 then
  begin
    QrPQ1.Close;
    UnDmkDAC_PF.AbreQuery(QrPQ1, Dmod.MyDB);
    EdProduto.Text := IntToStr(BDC_PQCAD);
    CBProduto.KeyValue := BDC_PQCAD;
  end;
end;

end.

