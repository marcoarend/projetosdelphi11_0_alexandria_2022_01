object FmMPClasMulGer: TFmMPClasMulGer
  Left = 339
  Top = 185
  Caption = 'COU-CLASS-004 :: Gerenciamento de Classifica'#231#227'o M'#250'ltipla'
  ClientHeight = 704
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 128
    Width = 1008
    Height = 468
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 104
      Top = 0
      Width = 904
      Height = 468
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label3: TLabel
        Left = 0
        Top = 0
        Width = 545
        Height = 16
        Align = alTop
        Alignment = taCenter
        Caption = 
          'Para alterar um registro, d'#234' um duplo clique na linha da grade c' +
          'orrespondente.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Panel4: TPanel
        Left = 0
        Top = 16
        Width = 904
        Height = 452
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        OnResize = Panel4Resize
        object Splitter1: TSplitter
          Left = 0
          Top = 204
          Width = 904
          Height = 5
          Cursor = crVSplit
          Align = alTop
          ExplicitLeft = 1
          ExplicitTop = 205
          ExplicitWidth = 902
        end
        object Panel5: TPanel
          Left = 0
          Top = 209
          Width = 904
          Height = 243
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 0
            Top = 0
            Width = 904
            Height = 16
            Align = alTop
            Alignment = taCenter
            AutoSize = False
            Caption = 'Produtos que foram gerados'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 1
            ExplicitTop = 1
            ExplicitWidth = 900
          end
          object DBGrid3: TDBGrid
            Left = 0
            Top = 16
            Width = 904
            Height = 227
            Align = alClient
            DataSource = DsPos
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid3DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD'
                Title.Caption = 'Nome do produto'
                Width = 300
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA'
                Title.Caption = 'Unidade'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Peso'
                Title.Caption = 'Peso kg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data/Hora'
                Visible = True
              end>
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 904
          Height = 204
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label2: TLabel
            Left = 0
            Top = 0
            Width = 904
            Height = 16
            Align = alTop
            Alignment = taCenter
            AutoSize = False
            Caption = 'Produtos que foram baixados'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 1
            ExplicitTop = 1
            ExplicitWidth = 900
          end
          object DBGrid2: TDBGrid
            Left = 0
            Top = 16
            Width = 904
            Height = 188
            Align = alClient
            DataSource = DsNeg
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid2DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD'
                Title.Caption = 'Nome do produto'
                Width = 300
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA'
                Title.Caption = 'Unidade'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Peso'
                Title.Caption = 'Peso kg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data/Hora'
                Visible = True
              end>
          end
        end
      end
    end
    object Panel7: TPanel
      Left = 0
      Top = 0
      Width = 104
      Height = 468
      Align = alLeft
      TabOrder = 1
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 102
        Height = 466
        Align = alClient
        DataSource = DsSMI
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SMIMultIns'
            Title.Caption = 'ID Clas.mult.'
            Visible = True
          end>
      end
    end
  end
  object Panel9: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 80
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label6: TLabel
      Left = 564
      Top = 0
      Width = 55
      Height = 13
      Caption = 'Data inicial:'
    end
    object Label7: TLabel
      Left = 680
      Top = 0
      Width = 48
      Height = 13
      Caption = 'Data final:'
    end
    object Label18: TLabel
      Left = 76
      Top = 0
      Width = 71
      Height = 13
      Caption = 'Cliente Interno:'
    end
    object Label10: TLabel
      Left = 476
      Top = 40
      Width = 18
      Height = 13
      Caption = 'OS:'
    end
    object Label11: TLabel
      Left = 76
      Top = 40
      Width = 63
      Height = 13
      Caption = 'Proced'#234'ncia:'
    end
    object Label12: TLabel
      Left = 600
      Top = 40
      Width = 24
      Height = 13
      Caption = 'Lote:'
    end
    object Label13: TLabel
      Left = 672
      Top = 40
      Width = 33
      Height = 13
      Caption = 'Marca:'
    end
    object Label19: TLabel
      Left = 544
      Top = 40
      Width = 17
      Height = 13
      Caption = 'NF:'
    end
    object TPIni: TdmkEditDateTimePicker
      Left = 564
      Top = 16
      Width = 112
      Height = 21
      Date = 38547.409162858800000000
      Time = 38547.409162858800000000
      TabOrder = 3
      OnExit = RGOrdemClick
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object TPFim: TdmkEditDateTimePicker
      Left = 680
      Top = 16
      Width = 112
      Height = 21
      Date = 38547.409162858800000000
      Time = 38547.409162858800000000
      TabOrder = 4
      OnExit = RGOrdemClick
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object RGOrdem: TRadioGroup
      Left = 0
      Top = 0
      Width = 73
      Height = 80
      Align = alLeft
      Caption = ' Ordem: '
      ItemIndex = 0
      Items.Strings = (
        'Data'
        'Empresa'
        'Lote')
      TabOrder = 0
      OnClick = RGOrdemClick
    end
    object CBClienteI: TdmkDBLookupComboBox
      Left = 132
      Top = 16
      Width = 429
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMECLIENTEI'
      ListSource = DsClientesI
      TabOrder = 2
      dmkEditCB = EdClienteI
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object dmkEdOS: TdmkEdit
      Left = 476
      Top = 56
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = RGOrdemClick
    end
    object CBProcedencia: TdmkDBLookupComboBox
      Left = 132
      Top = 56
      Width = 341
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMECLIENTEI'
      ListSource = DsProcedencias
      TabOrder = 7
      dmkEditCB = EdProcedencia
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdLote: TEdit
      Left = 600
      Top = 56
      Width = 68
      Height = 21
      TabOrder = 9
      OnExit = RGOrdemClick
    end
    object EdMarca: TEdit
      Left = 672
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 10
      OnExit = RGOrdemClick
    end
    object EdClienteI: TdmkEditCB
      Left = 76
      Top = 16
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = RGOrdemClick
      DBLookupComboBox = CBClienteI
      IgnoraDBLookupComboBox = False
    end
    object EdProcedencia: TdmkEditCB
      Left = 76
      Top = 56
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = RGOrdemClick
      DBLookupComboBox = CBProcedencia
      IgnoraDBLookupComboBox = False
    end
    object RGEncerrado: TdmkRadioGroup
      Left = 928
      Top = 4
      Width = 72
      Height = 73
      Caption = ' Encerrado: '
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o'
        'Sim'
        'Ambos')
      TabOrder = 12
      OnClick = RGOrdemClick
      UpdType = utYes
      OldValor = 0
    end
    object RGtpNF: TdmkRadioGroup
      Left = 796
      Top = 4
      Width = 128
      Height = 73
      Caption = ' Nota Fiscal: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Tudo'
        'Sa'#237'da'
        'Entrada'
        'Nenhum')
      TabOrder = 11
      OnClick = RGOrdemClick
      UpdType = utYes
      OldValor = 0
    end
    object EdNF: TdmkEdit
      Left = 544
      Top = 56
      Width = 52
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = RGOrdemClick
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 493
        Height = 32
        Caption = 'Gerenciamento de Classifica'#231#227'o M'#250'ltipla'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 493
        Height = 32
        Caption = 'Gerenciamento de Classifica'#231#227'o M'#250'ltipla'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 493
        Height = 32
        Caption = 'Gerenciamento de Classifica'#231#227'o M'#250'ltipla'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 596
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 640
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel10: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 584
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 704
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
        OnClick = BtExcluiClick
      end
    end
  end
  object QrSMI: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSMIAfterOpen
    BeforeClose = QrSMIBeforeClose
    AfterScroll = QrSMIAfterScroll
    Left = 388
    Top = 252
    object QrSMISMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
      Origin = 'stqmovitsa.SMIMultIns'
    end
  end
  object DsSMI: TDataSource
    DataSet = QrSMI
    Left = 416
    Top = 252
  end
  object QrNeg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.* '
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE smia.SMIMultIns=:P0'
      'AND smia.Qtde<0')
    Left = 388
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNegNivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'gragru1.Nivel1'
      Required = True
    end
    object QrNegNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrNegPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru1.PrdGrupTip'
    end
    object QrNegUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Origin = 'gragru1.UnidMed'
    end
    object QrNegNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Origin = 'prdgruptip.Nome'
      Size = 30
    end
    object QrNegSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = 'unidmed.Sigla'
      Size = 6
    end
    object QrNegNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrNegNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrNegGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrNegGraGruC: TIntegerField
      FieldName = 'GraGruC'
      Origin = 'gragrux.GraGruC'
    end
    object QrNegGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
    end
    object QrNegGraTamI: TIntegerField
      FieldName = 'GraTamI'
      Origin = 'gragrux.GraTamI'
    end
    object QrNegDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'stqmovitsa.DataHora'
    end
    object QrNegIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'stqmovitsa.IDCtrl'
    end
    object QrNegTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'stqmovitsa.Tipo'
    end
    object QrNegOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'stqmovitsa.OriCodi'
    end
    object QrNegOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
      Origin = 'stqmovitsa.OriCtrl'
    end
    object QrNegOriCnta: TIntegerField
      FieldName = 'OriCnta'
      Origin = 'stqmovitsa.OriCnta'
    end
    object QrNegOriPart: TIntegerField
      FieldName = 'OriPart'
      Origin = 'stqmovitsa.OriPart'
    end
    object QrNegEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'stqmovitsa.Empresa'
    end
    object QrNegStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = 'stqmovitsa.StqCenCad'
    end
    object QrNegGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'stqmovitsa.GraGruX'
    end
    object QrNegQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'stqmovitsa.Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrNegPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'stqmovitsa.Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrNegPeso: TFloatField
      FieldName = 'Peso'
      Origin = 'stqmovitsa.Peso'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrNegAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'stqmovitsa.AlterWeb'
    end
    object QrNegAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'stqmovitsa.Ativo'
    end
    object QrNegAreaM2: TFloatField
      FieldName = 'AreaM2'
      Origin = 'stqmovitsa.AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNegAreaP2: TFloatField
      FieldName = 'AreaP2'
      Origin = 'stqmovitsa.AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNegFatorClas: TFloatField
      FieldName = 'FatorClas'
      Origin = 'stqmovitsa.FatorClas'
    end
    object QrNegQuemUsou: TIntegerField
      FieldName = 'QuemUsou'
      Origin = 'stqmovitsa.QuemUsou'
    end
    object QrNegRetorno: TSmallintField
      FieldName = 'Retorno'
      Origin = 'stqmovitsa.Retorno'
    end
    object QrNegParTipo: TIntegerField
      FieldName = 'ParTipo'
      Origin = 'stqmovitsa.ParTipo'
    end
    object QrNegParCodi: TIntegerField
      FieldName = 'ParCodi'
      Origin = 'stqmovitsa.ParCodi'
    end
    object QrNegDebCtrl: TIntegerField
      FieldName = 'DebCtrl'
      Origin = 'stqmovitsa.DebCtrl'
    end
    object QrNegSMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
      Origin = 'stqmovitsa.SMIMultIns'
    end
  end
  object DsNeg: TDataSource
    DataSet = QrNeg
    Left = 416
    Top = 284
  end
  object QrPos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.* '
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE smia.SMIMultIns=:P0'
      'AND smia.Qtde>=0')
    Left = 388
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPosNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrPosNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 30
    end
    object QrPosPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrPosUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrPosNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrPosSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrPosNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrPosNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrPosGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrPosGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrPosGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrPosGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrPosDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrPosIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrPosTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPosOriCodi: TIntegerField
      FieldName = 'OriCodi'
    end
    object QrPosOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrPosOriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrPosOriPart: TIntegerField
      FieldName = 'OriPart'
    end
    object QrPosEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPosStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrPosGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrPosQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPosPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrPosPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPosAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPosAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPosFatorClas: TFloatField
      FieldName = 'FatorClas'
    end
    object QrPosQuemUsou: TIntegerField
      FieldName = 'QuemUsou'
    end
    object QrPosRetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrPosParTipo: TIntegerField
      FieldName = 'ParTipo'
    end
    object QrPosParCodi: TIntegerField
      FieldName = 'ParCodi'
    end
    object QrPosDebCtrl: TIntegerField
      FieldName = 'DebCtrl'
    end
    object QrPosSMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
    end
  end
  object DsPos: TDataSource
    DataSet = QrPos
    Left = 416
    Top = 312
  end
  object PMAltera: TPopupMenu
    Left = 148
    Top = 452
    object Data1: TMenuItem
      Caption = '&Data'
      Enabled = False
      object Xdiasapsentrada1: TMenuItem
        Caption = '"X" dias ap'#243's entrada'
        Enabled = False
        OnClick = Xdiasapsentrada1Click
      end
    end
  end
  object QrFonte: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data'
      'FROM MPIn'
      'WHERE Controle=:P0'
      '')
    Left = 388
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFonteData: TDateField
      FieldName = 'Data'
    end
  end
  object QrClientesI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECLIENTEI '
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"'
      'ORDER BY NOMECLIENTEI')
    Left = 821
    Top = 9
    object QrClientesINOMECLIENTEI: TWideStringField
      FieldName = 'NOMECLIENTEI'
      Size = 100
    end
    object QrClientesICodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
  end
  object DsClientesI: TDataSource
    DataSet = QrClientesI
    Left = 849
    Top = 9
  end
  object QrProcedencias: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECLIENTEI '
      'FROM entidades ci'
      'WHERE ci.Fornece1="V"'
      'ORDER BY NOMECLIENTEI')
    Left = 877
    Top = 9
    object QrProcedenciasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrProcedenciasNOMECLIENTEI: TWideStringField
      FieldName = 'NOMECLIENTEI'
      Size = 100
    end
  end
  object DsProcedencias: TDataSource
    DataSet = QrProcedencias
    Left = 905
    Top = 9
  end
end
