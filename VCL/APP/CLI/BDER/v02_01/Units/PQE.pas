unit PQE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ComCtrls, Grids, DBGrids, ZCF2, ResIntStrings,  UnGOTOy, UnInternalConsts,
  UnInternalConsts2, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts3, UnDmkProcFunc, mySQLDbTables, UnMyLinguas, dmkImage,
  dmkEdit, UnPagtos, Variants, dmkGeral, UnGrade_Tabs, NFeImporta_0400, dmkLabel
  (*,nfe_v110, XMLDoc*), UnDmkEnums, UnEmpresas;

type
  StatusVP = (vpErro, vpLiq, VPBruto);
  TFmPQE = class(TForm)
    PainelDados: TPanel;
    PainelGrids: TPanel;
    Query1: TmySQLQuery;
    DataSource1: TDataSource;
    Query1Codigo: TIntegerField;
    Query1Nome: TWideStringField;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    QrTransporte: TmySQLQuery;
    DsTransporte: TDataSource;
    QrPQE: TMySQLQuery;
    DsPQE: TDataSource;
    QrPQECodigo: TIntegerField;
    QrPQEData: TDateField;
    QrPQETransportadora: TIntegerField;
    QrPQENF: TIntegerField;
    QrPQEFrete: TFloatField;
    QrPQEPesoB: TFloatField;
    QrPQEPesoL: TFloatField;
    QrPQEValorNF: TFloatField;
    QrPQERICMS: TFloatField;
    QrPQELk: TIntegerField;
    QrPQEConhecimento: TIntegerField;
    QrPQENOMETRANSPORTADORA: TWideStringField;
    QrPQEIQ: TIntegerField;
    QrPQENOMEFORNECEDOR: TWideStringField;
    QrFornecedor: TmySQLQuery;
    DsPQEIts: TDataSource;
    QrEmissT: TmySQLQuery;
    DsEmissT: TDataSource;
    QrEmissF: TmySQLQuery;
    DsEmissF: TDataSource;
    QrUpdW: TmySQLQuery;
    QrUpdM: TmySQLQuery;
    QrConf: TmySQLQuery;
    QrConfTOTAL: TFloatField;
    QrPQERICMSF: TFloatField;
    QrConfT: TmySQLQuery;
    QrEmissTCarteira: TIntegerField;
    QrEmissTDescricao: TWideStringField;
    QrEmissTNotaFiscal: TIntegerField;
    QrEmissTDebito: TFloatField;
    QrEmissTCompensado: TDateField;
    QrEmissTDocumento: TFloatField;
    QrEmissTSit: TIntegerField;
    QrEmissTVencimento: TDateField;
    QrEmissTFatID: TIntegerField;
    QrEmissTFatParcela: TIntegerField;
    QrEmissFData: TDateField;
    QrEmissFTipo: TSmallintField;
    QrEmissFCarteira: TIntegerField;
    QrEmissFAutorizacao: TIntegerField;
    QrEmissFGenero: TIntegerField;
    QrEmissFDescricao: TWideStringField;
    QrEmissFNotaFiscal: TIntegerField;
    QrEmissFDebito: TFloatField;
    QrEmissFCredito: TFloatField;
    QrEmissFCompensado: TDateField;
    QrEmissFDocumento: TFloatField;
    QrEmissFSit: TIntegerField;
    QrEmissFVencimento: TDateField;
    QrEmissFLk: TIntegerField;
    QrEmissFFatID: TIntegerField;
    QrEmissFFatParcela: TIntegerField;
    QrEmissTData: TDateField;
    QrEmissTTipo: TSmallintField;
    QrEmissTAutorizacao: TIntegerField;
    QrEmissTGenero: TIntegerField;
    QrEmissTCredito: TFloatField;
    QrEmissTLk: TIntegerField;
    QrEmissTNOMESIT: TWideStringField;
    QrEmissFNOMESIT: TWideStringField;
    QrSumF: TmySQLQuery;
    QrSumT: TmySQLQuery;
    QrSumTDebito: TFloatField;
    QrSumFDebito: TFloatField;
    QrEmissTNOMETIPO: TWideStringField;
    QrEmissFNOMETIPO: TWideStringField;
    QrEmissTNOMECARTEIRA: TWideStringField;
    QrEmissFNOMECARTEIRA: TWideStringField;
    QrExPQE: TmySQLQuery;
    QrExPQT: TmySQLQuery;
    QrExPQI: TmySQLQuery;
    QrExPQF: TmySQLQuery;
    PMInclui: TPopupMenu;
    Encerramentodepedido1: TMenuItem;
    Entradaparcialdepedido1: TMenuItem;
    Entradasempedido1: TMenuItem;
    QrPedido: TmySQLQuery;
    QrPedidoCodigo: TIntegerField;
    QrPedidoData: TDateField;
    QrPedidoEntrega: TDateField;
    QrPedidoFornece: TIntegerField;
    QrPedidoContato: TWideStringField;
    QrPedidoTransporte: TIntegerField;
    QrPedidoPesoL: TFloatField;
    QrPedidoPesoB: TFloatField;
    QrPedidoValor: TFloatField;
    QrPedidoEntrada: TFloatField;
    QrPedidoSit: TIntegerField;
    QrPedidoLk: TIntegerField;
    QrPQEPedido: TIntegerField;
    QrPedEntra: TmySQLQuery;
    QrPedidoInsumo: TmySQLQuery;
    QrUpdPedido: TmySQLQuery;
    QrPedEntraTotalPeso: TFloatField;
    QrInsPedido: TmySQLQuery;
    QrPedidoConta: TmySQLQuery;
    QrPedidoContaConta: TIntegerField;
    QrSoma: TmySQLQuery;
    QrSomaUpd: TmySQLQuery;
    QrSomaPesoB: TFloatField;
    QrSomaPesoL: TFloatField;
    QrSomaValor: TFloatField;
    QrSomaEntrada: TFloatField;
    QrPedidoSetor: TSmallintField;
    Query2: TmySQLQuery;
    QrPQEDataE: TDateField;
    PMIncluiIts: TPopupMenu;
    Itemdeinsumo1: TMenuItem;
    IncluiT: TMenuItem;
    IncluiF1: TMenuItem;
    PMAlteraIts: TPopupMenu;
    MenuItem1: TMenuItem;
    AlteraT1: TMenuItem;
    AlteraF1: TMenuItem;
    QrPQEJuros: TFloatField;
    QrPQEICMS: TFloatField;
    QrPQEIts: TmySQLQuery;
    QrPQEItsCUSTOITEM: TFloatField;
    QrPQEItsVALORKG: TFloatField;
    QrPQEItsTOTALKGBRUTO: TFloatField;
    QrPQEItsCUSTOKG: TFloatField;
    QrPQEItsPRECOKG: TFloatField;
    QrEmissTID_Sub: TSmallintField;
    QrEmissTSub: TIntegerField;
    QrEmissTFatura: TWideStringField;
    QrEmissTBanco: TIntegerField;
    QrEmissTLocal: TIntegerField;
    QrEmissTCartao: TIntegerField;
    QrEmissTLinha: TIntegerField;
    QrEmissTPago: TFloatField;
    QrEmissTMez: TIntegerField;
    QrEmissFID_Sub: TSmallintField;
    QrEmissFSub: TIntegerField;
    QrEmissFFatura: TWideStringField;
    QrEmissFBanco: TIntegerField;
    QrEmissFLocal: TIntegerField;
    QrEmissFCartao: TIntegerField;
    QrEmissFLinha: TIntegerField;
    QrEmissFPago: TFloatField;
    QrEmissFMez: TIntegerField;
    QrEmissFFornecedor: TIntegerField;
    QrEmissFCliente: TIntegerField;
    QrEmissTFornecedor: TIntegerField;
    QrEmissTCliente: TIntegerField;
    QrPQEFRETEKG: TFloatField;
    QrTransporteCodigo: TIntegerField;
    QrTransporteNome: TWideStringField;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNome: TWideStringField;
    PainelCabecalho: TPanel;
    Painel10: TPanel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label20: TLabel;
    DBEdit1: TDBEdit;
    EdNF: TDBEdit;
    DBEdit7: TDBEdit;
    EdValorNF: TDBEdit;
    DBEdit5: TDBEdit;
    QrPQECancelado: TWideStringField;
    QrPQCli: TmySQLQuery;
    PMExcluiIts: TPopupMenu;
    ExcluiInsumo1: TMenuItem;
    ExcluiDuplT1: TMenuItem;
    ExcluiDuplF1: TMenuItem;
    QrEmissTControle: TIntegerField;
    QrEmissTID_Pgto: TIntegerField;
    QrEmissFControle: TIntegerField;
    QrEmissFID_Pgto: TIntegerField;
    QrPQENOMEUSERCAD: TWideStringField;
    QrPQENOMEUSERALT: TWideStringField;
    QrPQEDataCad: TDateField;
    QrPQEDataAlt: TDateField;
    QrPQEUserCad: TIntegerField;
    QrPQEUserAlt: TIntegerField;
    QrEmissTCliInt: TIntegerField;
    QrEmissFCliInt: TIntegerField;
    QrEmissFQtde: TFloatField;
    QrEmissFFatID_Sub: TIntegerField;
    QrEmissFOperCount: TIntegerField;
    QrEmissFLancto: TIntegerField;
    QrEmissFForneceI: TIntegerField;
    QrEmissFMoraDia: TFloatField;
    QrEmissFMulta: TFloatField;
    QrEmissFProtesto: TDateField;
    QrEmissFDataDoc: TDateField;
    QrEmissFCtrlIni: TIntegerField;
    QrEmissFNivel: TIntegerField;
    QrEmissFVendedor: TIntegerField;
    QrEmissFAccount: TIntegerField;
    QrEmissFICMS_P: TFloatField;
    QrEmissFICMS_V: TFloatField;
    QrEmissFDuplicata: TWideStringField;
    QrEmissFDepto: TIntegerField;
    QrEmissFDescoPor: TIntegerField;
    QrEmissFDataCad: TDateField;
    QrEmissFDataAlt: TDateField;
    QrEmissFUserCad: TIntegerField;
    QrEmissFUserAlt: TIntegerField;
    QrEmissFEmitente: TWideStringField;
    QrEmissFContaCorrente: TWideStringField;
    QrEmissFCNPJCPF: TWideStringField;
    QrEmissTQtde: TFloatField;
    QrEmissTFatID_Sub: TIntegerField;
    QrEmissTOperCount: TIntegerField;
    QrEmissTLancto: TIntegerField;
    QrEmissTForneceI: TIntegerField;
    QrEmissTMoraDia: TFloatField;
    QrEmissTMulta: TFloatField;
    QrEmissTProtesto: TDateField;
    QrEmissTDataDoc: TDateField;
    QrEmissTCtrlIni: TIntegerField;
    QrEmissTNivel: TIntegerField;
    QrEmissTVendedor: TIntegerField;
    QrEmissTAccount: TIntegerField;
    QrEmissTICMS_P: TFloatField;
    QrEmissTICMS_V: TFloatField;
    QrEmissTDuplicata: TWideStringField;
    QrEmissTDepto: TIntegerField;
    QrEmissTDescoPor: TIntegerField;
    QrEmissTDataCad: TDateField;
    QrEmissTDataAlt: TDateField;
    QrEmissTUserCad: TIntegerField;
    QrEmissTUserAlt: TIntegerField;
    QrEmissTEmitente: TWideStringField;
    QrEmissTContaCorrente: TWideStringField;
    QrEmissTCNPJCPF: TWideStringField;
    QrPQECI: TIntegerField;
    Panel1: TPanel;
    PainelCancelado: TPanel;
    Image2: TImage;
    Panel2: TPanel;
    Label23: TLabel;
    Label25: TLabel;
    DBEdit14: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    GroupBox3: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    EdFrete: TDBEdit;
    DBEdit15: TDBEdit;
    GroupBox4: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    EdTNota: TdmkEdit;
    EdTBruto: TdmkEdit;
    EdTLiq: TdmkEdit;
    EdPagF: TdmkEdit;
    EdPagT: TdmkEdit;
    DBEdit3: TDBEdit;
    Label24: TLabel;
    DBEdit01: TDBEdit;
    QrPQENOMECLIINT: TWideStringField;
    QrAnt: TmySQLQuery;
    QrAntInsumo: TIntegerField;
    mySQLQuery1: TmySQLQuery;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    FloatField6: TFloatField;
    FloatField7: TFloatField;
    FloatField8: TFloatField;
    FloatField9: TFloatField;
    FloatField10: TFloatField;
    FloatField11: TFloatField;
    FloatField12: TFloatField;
    FloatField13: TFloatField;
    StringField1: TWideStringField;
    IntegerField5: TIntegerField;
    FloatField14: TFloatField;
    FloatField15: TFloatField;
    QrPQEItsCodigo: TIntegerField;
    QrPQEItsControle: TIntegerField;
    QrPQEItsConta: TIntegerField;
    QrPQEItsVolumes: TIntegerField;
    QrPQEItsPesoVB: TFloatField;
    QrPQEItsPesoVL: TFloatField;
    QrPQEItsValorItem: TFloatField;
    QrPQEItsIPI: TFloatField;
    QrPQEItsRIPI: TFloatField;
    QrPQEItsCFin: TFloatField;
    QrPQEItsICMS: TFloatField;
    QrPQEItsRICMS: TFloatField;
    QrPQEItsTotalCusto: TFloatField;
    QrPQEItsInsumo: TIntegerField;
    QrPQEItsTotalPeso: TFloatField;
    QrPQCliPeso: TFloatField;
    QrPQCliValor: TFloatField;
    QrPQEItsNOMEPQ: TWideStringField;
    QrEmissTFatNum: TFloatField;
    QrEmissFFatNum: TFloatField;
    QrPQETipoNF: TSmallintField;
    QrPQErefNFe: TWideStringField;
    QrPQEmodNF: TSmallintField;
    QrPQESerie: TIntegerField;
    QrEmb: TmySQLQuery;
    QrEmbPQ: TIntegerField;
    QrPQEItsprod_cProd: TWideStringField;
    QrPQEItsprod_cEAN: TWideStringField;
    QrPQEItsprod_xProd: TWideStringField;
    QrPQEItsprod_NCM: TWideStringField;
    QrPQEItsprod_EX_TIPI: TWideStringField;
    QrPQEItsprod_genero: TSmallintField;
    QrPQEItsprod_CFOP: TWideStringField;
    QrPQEItsprod_uCom: TWideStringField;
    QrPQEItsprod_qCom: TFloatField;
    QrPQEItsprod_vUnCom: TFloatField;
    QrPQEItsprod_vProd: TFloatField;
    QrPQEItsprod_cEANTrib: TWideStringField;
    QrPQEItsprod_uTrib: TWideStringField;
    QrPQEItsprod_qTrib: TFloatField;
    QrPQEItsprod_vUnTrib: TFloatField;
    QrPQEItsprod_vFrete: TFloatField;
    QrPQEItsprod_vSeg: TFloatField;
    QrPQEItsprod_vDesc: TFloatField;
    QrPQEItsICMS_Orig: TSmallintField;
    QrPQEItsICMS_CST: TSmallintField;
    QrPQEItsICMS_modBC: TSmallintField;
    QrPQEItsICMS_pRedBC: TFloatField;
    QrPQEItsICMS_vBC: TFloatField;
    QrPQEItsICMS_pICMS: TFloatField;
    QrPQEItsICMS_vICMS: TFloatField;
    QrPQEItsICMS_modBCST: TSmallintField;
    QrPQEItsICMS_pMVAST: TFloatField;
    QrPQEItsICMS_pRedBCST: TFloatField;
    QrPQEItsICMS_vBCST: TFloatField;
    QrPQEItsICMS_pICMSST: TFloatField;
    QrPQEItsICMS_vICMSST: TFloatField;
    QrPQEItsIPI_cEnq: TWideStringField;
    QrPQEItsIPI_vBC: TFloatField;
    QrPQEItsIPI_qUnid: TFloatField;
    QrPQEItsIPI_vUnid: TFloatField;
    QrPQEItsIPI_pIPI: TFloatField;
    QrPQEItsIPI_vIPI: TFloatField;
    QrPQEItsPIS_vBC: TFloatField;
    QrPQEItsPIS_pPIS: TFloatField;
    QrPQEItsPIS_vPIS: TFloatField;
    QrPQEItsPIS_qBCProd: TFloatField;
    QrPQEItsPIS_vAliqProd: TFloatField;
    QrPQEItsPISST_vBC: TFloatField;
    QrPQEItsPISST_pPIS: TFloatField;
    QrPQEItsPISST_qBCProd: TFloatField;
    QrPQEItsPISST_vAliqProd: TFloatField;
    QrPQEItsPISST_vPIS: TFloatField;
    QrPQEItsCOFINS_vBC: TFloatField;
    QrPQEItsCOFINS_pCOFINS: TFloatField;
    QrPQEItsCOFINS_qBCProd: TFloatField;
    QrPQEItsCOFINS_vAliqProd: TFloatField;
    QrPQEItsCOFINS_vCOFINS: TFloatField;
    QrPQEItsCOFINSST_vBC: TFloatField;
    QrPQEItsCOFINSST_pCOFINS: TFloatField;
    QrPQEItsCOFINSST_qBCProd: TFloatField;
    QrPQEItsCOFINSST_vAliqProd: TFloatField;
    QrPQEItsCOFINSST_vCOFINS: TFloatField;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit6: TDBEdit;
    QrPQEDataS: TDateField;
    QrPQEIPI: TFloatField;
    QrPQEPIS: TFloatField;
    QrPQECOFINS: TFloatField;
    QrPQESeguro: TFloatField;
    QrPQEDesconto: TFloatField;
    QrPQEOutros: TFloatField;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAIDCtrl: TIntegerField;
    QrNFeCabAStatus: TSmallintField;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    QrNFeCabAinfCanc_cStat: TIntegerField;
    QrNFeCabAinfCanc_xMotivo: TWideStringField;
    QrNFeCabAcStat: TIntegerField;
    QrNFeCabAxMotivo: TWideStringField;
    QrNFeCabAcStat_xMotivo: TWideStringField;
    QrNFeCabAide_cNF: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabASTATUS_TXT: TWideStringField;
    QrNFeCabAICMSRec_pRedBC: TFloatField;
    QrNFeCabAICMSRec_vBC: TFloatField;
    QrNFeCabAICMSRec_pAliq: TFloatField;
    QrNFeCabAICMSRec_vICMS: TFloatField;
    QrNFeCabAIPIRec_pRedBC: TFloatField;
    QrNFeCabAIPIRec_vBC: TFloatField;
    QrNFeCabAIPIRec_pAliq: TFloatField;
    QrNFeCabAIPIRec_vIPI: TFloatField;
    QrNFeCabAPISRec_pRedBC: TFloatField;
    QrNFeCabAPISRec_vBC: TFloatField;
    QrNFeCabAPISRec_pAliq: TFloatField;
    QrNFeCabAPISRec_vPIS: TFloatField;
    QrNFeCabACOFINSRec_pRedBC: TFloatField;
    QrNFeCabACOFINSRec_vBC: TFloatField;
    QrNFeCabACOFINSRec_pAliq: TFloatField;
    QrNFeCabACOFINSRec_vCOFINS: TFloatField;
    DsNFeCabA: TDataSource;
    Splitter1: TSplitter;
    QrNFeItsI: TmySQLQuery;
    QrNFeItsIICMSRec_vICMS: TFloatField;
    QrNFeItsIIPIRec_vIPI: TFloatField;
    QrNFeItsIPISRec_vPIS: TFloatField;
    QrNFeItsICOFINSRec_vCOFINS: TFloatField;
    QrNFeItsIMeuID: TIntegerField;
    DsNFeItsI: TDataSource;
    GroupBox8: TGroupBox;
    Label12: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label26: TLabel;
    GroupBox9: TGroupBox;
    DBEdit10: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    GroupBox10: TGroupBox;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit19: TDBEdit;
    QrEmbICMSRec_pRedBC: TFloatField;
    QrEmbIPIRec_pRedBC: TFloatField;
    QrEmbPISRec_pRedBC: TFloatField;
    QrEmbCOFINSRec_pRedBC: TFloatField;
    QrConfTPesoL: TFloatField;
    QrConfTPesoB: TFloatField;
    QrConfTTotalCusto: TFloatField;
    EntradaporXML2: TMenuItem;
    QrA: TmySQLQuery;
    QrAFatID: TIntegerField;
    QrAFatNum: TIntegerField;
    QrAEmpresa: TIntegerField;
    QrAIDCtrl: TIntegerField;
    QrALoteEnv: TIntegerField;
    QrAversao: TFloatField;
    QrAId: TWideStringField;
    QrAide_cUF: TSmallintField;
    QrAide_cNF: TIntegerField;
    QrAide_natOp: TWideStringField;
    QrAide_indPag: TSmallintField;
    QrAide_mod: TSmallintField;
    QrAide_serie: TIntegerField;
    QrAide_nNF: TIntegerField;
    QrAide_dEmi: TDateField;
    QrAide_dSaiEnt: TDateField;
    QrAide_tpNF: TSmallintField;
    QrAide_cMunFG: TIntegerField;
    QrAide_tpImp: TSmallintField;
    QrAide_tpEmis: TSmallintField;
    QrAide_cDV: TSmallintField;
    QrAide_tpAmb: TSmallintField;
    QrAide_finNFe: TSmallintField;
    QrAide_procEmi: TSmallintField;
    QrAide_verProc: TWideStringField;
    QrAemit_CNPJ: TWideStringField;
    QrAemit_CPF: TWideStringField;
    QrAemit_xNome: TWideStringField;
    QrAemit_xFant: TWideStringField;
    QrAemit_xLgr: TWideStringField;
    QrAemit_nro: TWideStringField;
    QrAemit_xCpl: TWideStringField;
    QrAemit_xBairro: TWideStringField;
    QrAemit_cMun: TIntegerField;
    QrAemit_xMun: TWideStringField;
    QrAemit_UF: TWideStringField;
    QrAemit_CEP: TIntegerField;
    QrAemit_cPais: TIntegerField;
    QrAemit_xPais: TWideStringField;
    QrAemit_fone: TWideStringField;
    QrAemit_IE: TWideStringField;
    QrAemit_IEST: TWideStringField;
    QrAemit_IM: TWideStringField;
    QrAemit_CNAE: TWideStringField;
    QrAdest_CNPJ: TWideStringField;
    QrAdest_CPF: TWideStringField;
    QrAdest_xNome: TWideStringField;
    QrAdest_xLgr: TWideStringField;
    QrAdest_nro: TWideStringField;
    QrAdest_xCpl: TWideStringField;
    QrAdest_xBairro: TWideStringField;
    QrAdest_cMun: TIntegerField;
    QrAdest_xMun: TWideStringField;
    QrAdest_UF: TWideStringField;
    QrAdest_CEP: TWideStringField;
    QrAdest_cPais: TIntegerField;
    QrAdest_xPais: TWideStringField;
    QrAdest_fone: TWideStringField;
    QrAdest_IE: TWideStringField;
    QrAdest_ISUF: TWideStringField;
    QrAICMSTot_vBC: TFloatField;
    QrAICMSTot_vICMS: TFloatField;
    QrAICMSTot_vBCST: TFloatField;
    QrAICMSTot_vST: TFloatField;
    QrAICMSTot_vProd: TFloatField;
    QrAICMSTot_vFrete: TFloatField;
    QrAICMSTot_vSeg: TFloatField;
    QrAICMSTot_vDesc: TFloatField;
    QrAICMSTot_vII: TFloatField;
    QrAICMSTot_vIPI: TFloatField;
    QrAICMSTot_vPIS: TFloatField;
    QrAICMSTot_vCOFINS: TFloatField;
    QrAICMSTot_vOutro: TFloatField;
    QrAICMSTot_vNF: TFloatField;
    QrAISSQNtot_vServ: TFloatField;
    QrAISSQNtot_vBC: TFloatField;
    QrAISSQNtot_vISS: TFloatField;
    QrAISSQNtot_vPIS: TFloatField;
    QrAISSQNtot_vCOFINS: TFloatField;
    QrARetTrib_vRetPIS: TFloatField;
    QrARetTrib_vRetCOFINS: TFloatField;
    QrARetTrib_vRetCSLL: TFloatField;
    QrARetTrib_vBCIRRF: TFloatField;
    QrARetTrib_vIRRF: TFloatField;
    QrARetTrib_vBCRetPrev: TFloatField;
    QrARetTrib_vRetPrev: TFloatField;
    QrAModFrete: TSmallintField;
    QrATransporta_CNPJ: TWideStringField;
    QrATransporta_CPF: TWideStringField;
    QrATransporta_XNome: TWideStringField;
    QrATransporta_IE: TWideStringField;
    QrATransporta_XEnder: TWideStringField;
    QrATransporta_XMun: TWideStringField;
    QrATransporta_UF: TWideStringField;
    QrARetTransp_vServ: TFloatField;
    QrARetTransp_vBCRet: TFloatField;
    QrARetTransp_PICMSRet: TFloatField;
    QrARetTransp_vICMSRet: TFloatField;
    QrARetTransp_CFOP: TWideStringField;
    QrARetTransp_CMunFG: TWideStringField;
    QrAVeicTransp_Placa: TWideStringField;
    QrAVeicTransp_UF: TWideStringField;
    QrAVeicTransp_RNTC: TWideStringField;
    QrACobr_Fat_nFat: TWideStringField;
    QrACobr_Fat_vOrig: TFloatField;
    QrACobr_Fat_vDesc: TFloatField;
    QrACobr_Fat_vLiq: TFloatField;
    QrAInfAdic_InfAdFisco: TWideMemoField;
    QrAInfAdic_InfCpl: TWideMemoField;
    QrAExporta_UFEmbarq: TWideStringField;
    QrAExporta_XLocEmbarq: TWideStringField;
    QrACompra_XNEmp: TWideStringField;
    QrACompra_XPed: TWideStringField;
    QrACompra_XCont: TWideStringField;
    QrAStatus: TIntegerField;
    QrAinfProt_Id: TWideStringField;
    QrAinfProt_tpAmb: TSmallintField;
    QrAinfProt_verAplic: TWideStringField;
    QrAinfProt_dhRecbto: TDateTimeField;
    QrAinfProt_nProt: TWideStringField;
    QrAinfProt_digVal: TWideStringField;
    QrAinfProt_cStat: TIntegerField;
    QrAinfProt_xMotivo: TWideStringField;
    QrAinfCanc_Id: TWideStringField;
    QrAinfCanc_tpAmb: TSmallintField;
    QrAinfCanc_verAplic: TWideStringField;
    QrAinfCanc_dhRecbto: TDateTimeField;
    QrAinfCanc_nProt: TWideStringField;
    QrAinfCanc_digVal: TWideStringField;
    QrAinfCanc_cStat: TIntegerField;
    QrAinfCanc_xMotivo: TWideStringField;
    QrAinfCanc_cJust: TIntegerField;
    QrAinfCanc_xJust: TWideStringField;
    QrA_Ativo_: TSmallintField;
    QrAFisRegCad: TIntegerField;
    QrACartEmiss: TIntegerField;
    QrATabelaPrc: TIntegerField;
    QrACondicaoPg: TIntegerField;
    QrAFreteExtra: TFloatField;
    QrASegurExtra: TFloatField;
    QrALk: TIntegerField;
    QrADataCad: TDateField;
    QrADataAlt: TDateField;
    QrAUserCad: TIntegerField;
    QrAUserAlt: TIntegerField;
    QrAAlterWeb: TSmallintField;
    QrAAtivo: TSmallintField;
    QrAICMSRec_pRedBC: TFloatField;
    QrAICMSRec_vBC: TFloatField;
    QrAICMSRec_pAliq: TFloatField;
    QrAICMSRec_vICMS: TFloatField;
    QrAIPIRec_pRedBC: TFloatField;
    QrAIPIRec_vBC: TFloatField;
    QrAIPIRec_pAliq: TFloatField;
    QrAIPIRec_vIPI: TFloatField;
    QrAPISRec_pRedBC: TFloatField;
    QrAPISRec_vBC: TFloatField;
    QrAPISRec_pAliq: TFloatField;
    QrAPISRec_vPIS: TFloatField;
    QrACOFINSRec_pRedBC: TFloatField;
    QrACOFINSRec_vBC: TFloatField;
    QrACOFINSRec_pAliq: TFloatField;
    QrACOFINSRec_vCOFINS: TFloatField;
    QrI: TmySQLQuery;
    QrN: TmySQLQuery;
    QrInItem: TIntegerField;
    QrIprod_qCom: TFloatField;
    QrIprod_vProd: TFloatField;
    QrIprod_vFrete: TFloatField;
    QrIprod_vSeg: TFloatField;
    QrIprod_vDesc: TFloatField;
    QrINivel1: TIntegerField;
    QrIprod_cProd: TWideStringField;
    QrIprod_cEAN: TWideStringField;
    QrIprod_xProd: TWideStringField;
    QrIprod_NCM: TWideStringField;
    QrIprod_EXTIPI: TWideStringField;
    QrIprod_genero: TSmallintField;
    QrIprod_CFOP: TIntegerField;
    QrIprod_uCom: TWideStringField;
    QrIprod_vUnCom: TFloatField;
    QrIprod_cEANTrib: TWideStringField;
    QrIprod_uTrib: TWideStringField;
    QrIprod_qTrib: TFloatField;
    QrIprod_vUnTrib: TFloatField;
    QrO: TmySQLQuery;
    QrOIPI_vIPI: TFloatField;
    QrNICMS_Orig: TSmallintField;
    QrNICMS_CST: TSmallintField;
    QrNICMS_modBC: TSmallintField;
    QrNICMS_pRedBC: TFloatField;
    QrNICMS_vBC: TFloatField;
    QrNICMS_pICMS: TFloatField;
    QrNICMS_vICMS: TFloatField;
    QrNICMS_modBCST: TSmallintField;
    QrNICMS_pMVAST: TFloatField;
    QrNICMS_pRedBCST: TFloatField;
    QrNICMS_vBCST: TFloatField;
    QrNICMS_pICMSST: TFloatField;
    QrNICMS_vICMSST: TFloatField;
    QrOIPI_cEnq: TWideStringField;
    QrOIPI_CST: TSmallintField;
    QrOIPI_vBC: TFloatField;
    QrOIPI_qUnid: TFloatField;
    QrOIPI_vUnid: TFloatField;
    QrOIPI_pIPI: TFloatField;
    QrQ: TmySQLQuery;
    QrQPIS_CST: TSmallintField;
    QrQPIS_vBC: TFloatField;
    QrQPIS_pPIS: TFloatField;
    QrQPIS_vPIS: TFloatField;
    QrQPIS_qBCProd: TFloatField;
    QrQPIS_vAliqProd: TFloatField;
    QrS: TmySQLQuery;
    QrSCOFINS_CST: TSmallintField;
    QrSCOFINS_vBC: TFloatField;
    QrSCOFINS_pCOFINS: TFloatField;
    QrSCOFINS_qBCProd: TFloatField;
    QrSCOFINS_vAliqProd: TFloatField;
    QrSCOFINS_vCOFINS: TFloatField;
    QrPQEDataS_TXT: TWideStringField;
    QrPQEDataE_TXT: TWideStringField;
    QrPQEData_TXT: TWideStringField;
    QrACodInfoEmit: TIntegerField;
    QrACodInfoDest: TIntegerField;
    OutrosdadosdeNFnormal1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    PMAltera: TPopupMenu;
    AlteraDadosEstoque1: TMenuItem;
    AlteraDadosFiscais1: TMenuItem;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrEmissFAgencia: TIntegerField;
    QrEmissTAgencia: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtBaixa: TBitBtn;
    GBTrava: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtIncluiIts: TBitBtn;
    BtAlteraIts: TBitBtn;
    BtExcluiIts: TBitBtn;
    BtQuita: TBitBtn;
    BtDesiste: TBitBtn;
    QrPQENF_RP: TIntegerField;
    QrPQENF_CC: TIntegerField;
    DBEdit24: TDBEdit;
    Label27: TLabel;
    DBEdit25: TDBEdit;
    Label28: TLabel;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrPQECodCliInt: TIntegerField;
    PB1: TProgressBar;
    Alterasomentecabealho1: TMenuItem;
    N3: TMenuItem;
    InsumossemNFdecobertura1: TMenuItem;
    QrPQEItsDtCorrApo: TDateTimeField;
    QrPQEEmpresa: TIntegerField;
    QrPQEItsMoeda: TIntegerField;
    QrPQEItsCotacao: TFloatField;
    QrPQEItsHowLoad: TSmallintField;
    QrPQEItsFatID: TIntegerField;
    QrPQEItsFatNum: TIntegerField;
    QrPQEItsEmpresa: TIntegerField;
    QrPQEItsnItem: TIntegerField;
    QrPQEItsLk: TIntegerField;
    QrPQEItsDataCad: TDateField;
    QrPQEItsDataAlt: TDateField;
    QrPQEItsUserCad: TIntegerField;
    QrPQEItsUserAlt: TIntegerField;
    QrPQEItsAlterWeb: TSmallintField;
    QrPQEItsAtivo: TSmallintField;
    QrPQEItsRpICMS: TFloatField;
    QrPQEItsRpPIS: TFloatField;
    QrPQEItsRpCOFINS: TFloatField;
    QrPQEItsRvICMS: TFloatField;
    QrPQEItsRvPIS: TFloatField;
    QrPQEItsRvCOFINS: TFloatField;
    QrPQEValProd: TFloatField;
    QrPQEVolumes: TIntegerField;
    QrPQEEFD_INN_AnoMes: TIntegerField;
    QrPQEEFD_INN_Empresa: TIntegerField;
    QrPQEEFD_INN_LinArq: TIntegerField;
    QrPQEHowLoad: TSmallintField;
    QrPQEFatID: TIntegerField;
    QrPQEFatNum: TIntegerField;
    QrPQEErrNota: TFloatField;
    QrPQEErrBruto: TFloatField;
    QrPQEErrLiq: TFloatField;
    QrPQEErrPagT: TFloatField;
    QrPQEErrPagF: TFloatField;
    QrPQEAlterWeb: TSmallintField;
    QrPQEAtivo: TSmallintField;
    QrPQEFreteRpICMS: TFloatField;
    QrPQEFreteRpPIS: TFloatField;
    QrPQEFreteRpCOFINS: TFloatField;
    QrPQEFreteRvICMS: TFloatField;
    QrPQEFreteRvPIS: TFloatField;
    QrPQEFreteRvCOFINS: TFloatField;
    QrConfTValorItem: TFloatField;
    QrRetImpost: TmySQLQuery;
    DsRetImpost: TDataSource;
    QrRetImpostRvICMS: TFloatField;
    QrRetImpostRvPIS: TFloatField;
    QrRetImpostRvCOFINS: TFloatField;
    QrPQEItsCustoPadraoAtz: TFloatField;
    QrPQEItsxLote: TWideStringField;
    N4: TMenuItem;
    AlteralanamentofinanceiroFornecedor1: TMenuItem;
    AlteralanamentofinanceiroTransportador1: TMenuItem;
    N5: TMenuItem;
    LanamentofinancxeiroapagarFornecedor1: TMenuItem;
    LanamentofinancxeiroapagarTransportadora1: TMenuItem;
    QrEmissTGenCtb: TIntegerField;
    QrEmissFGenCtb: TIntegerField;
    QrEmissTSerieNF: TWideStringField;
    QrEmissFSerieNF: TWideStringField;
    QrEmissFGenCtbD: TIntegerField;
    QrEmissFGenCtbC: TIntegerField;
    QrPQEItsRpIPI: TFloatField;
    QrPQEItsRvIPI: TFloatField;
    QrRetImpostRvIPI: TFloatField;
    QrPQEAWServerID: TIntegerField;
    QrPQEAWStatSinc: TSmallintField;
    QrPQEmodRP: TSmallintField;
    QrPQEmodCC: TSmallintField;
    QrPQESerRP: TIntegerField;
    QrPQESerCC: TIntegerField;
    QrPQENFe_RP: TWideStringField;
    QrPQENFe_CC: TWideStringField;
    QrPQECte_Id: TWideStringField;
    QrPQECTe_mod: TSmallintField;
    QrPQECTe_serie: TIntegerField;
    QrPQENFVP_FatID: TIntegerField;
    QrPQENFVP_FatNum: TIntegerField;
    QrPQENFVP_StaLnk: TSmallintField;
    QrPQENFRP_FatID: TIntegerField;
    QrPQENFRP_FatNum: TIntegerField;
    QrPQENFRP_StaLnk: TSmallintField;
    QrPQENFCC_FatID: TIntegerField;
    QrPQENFCC_FatNum: TIntegerField;
    QrPQENFCC_StaLnk: TSmallintField;
    QrPQEItsRpICMSST: TFloatField;
    QrPQEItsRvICMSST: TFloatField;
    QrPQEItsprod_vOutro: TFloatField;
    QrPQEICMSTot_vProd: TFloatField;
    QrPQEICMSTot_vFrete: TFloatField;
    QrPQEICMSTot_vSeg: TFloatField;
    QrPQEICMSTot_vOutro: TFloatField;
    QrPQEICMSTot_vDesc: TFloatField;
    QrIvBC: TMySQLQuery;
    QrIvBCprod_vProd: TFloatField;
    QrIvBCprod_vFrete: TFloatField;
    QrIvBCprod_vSeg: TFloatField;
    QrIvBCprod_vOutro: TFloatField;
    QrIvBCprod_vDesc: TFloatField;
    QrAvBC: TMySQLQuery;
    QrAvBCICMSTot_vProd: TFloatField;
    QrAvBCICMSTot_vFrete: TFloatField;
    QrAvBCICMSTot_vSeg: TFloatField;
    QrAvBCICMSTot_vOutro: TFloatField;
    QrAvBCICMSTot_vDesc: TFloatField;
    QrPediVda: TMySQLQuery;
    QrPediVdaCodigo: TIntegerField;
    QrPediVdaCodUsu: TIntegerField;
    QrPediVdaEmpresa: TIntegerField;
    QrPediVdaCliente: TIntegerField;
    QrPediVdaDtaEmiss: TDateField;
    QrPediVdaDtaEntra: TDateField;
    QrPediVdaDtaInclu: TDateField;
    QrPediVdaDtaPrevi: TDateField;
    QrPediVdaRepresen: TIntegerField;
    QrPediVdaPrioridade: TSmallintField;
    QrPediVdaCondicaoPG: TIntegerField;
    QrPediVdaMoeda: TIntegerField;
    QrPediVdaSituacao: TIntegerField;
    QrPediVdaTabelaPrc: TIntegerField;
    QrPediVdaMotivoSit: TIntegerField;
    QrPediVdaLoteProd: TIntegerField;
    QrPediVdaPedidoCli: TWideStringField;
    QrPediVdaFretePor: TSmallintField;
    QrPediVdaTransporta: TIntegerField;
    QrPediVdaRedespacho: TIntegerField;
    QrPediVdaRegrFiscal: TIntegerField;
    QrPediVdaDesoAces_V: TFloatField;
    QrPediVdaDesoAces_P: TFloatField;
    QrPediVdaFrete_V: TFloatField;
    QrPediVdaFrete_P: TFloatField;
    QrPediVdaSeguro_V: TFloatField;
    QrPediVdaSeguro_P: TFloatField;
    QrPediVdaTotalQtd: TFloatField;
    QrPediVdaTotal_Vlr: TFloatField;
    QrPediVdaTotal_Des: TFloatField;
    QrPediVdaTotal_Tot: TFloatField;
    QrPediVdaObserva: TWideMemoField;
    QrPediVdaAFP_Sit: TSmallintField;
    QrPediVdaAFP_Per: TFloatField;
    QrPediVdaComisFat: TFloatField;
    QrPediVdaComisRec: TFloatField;
    QrPediVdaCartEmis: TIntegerField;
    QrPediVdaQuantP: TFloatField;
    QrPediVdaValLiq: TFloatField;
    QrPediVdaTipo: TSmallintField;
    QrPediVdaEntSai: TSmallintField;
    QrPediVdaCondPag: TWideStringField;
    QrPediVdaCentroCust: TWideStringField;
    QrPediVdaItemCust: TWideStringField;
    QrPediVdaUsaReferen: TSmallintField;
    QrPediVdaidDest: TSmallintField;
    QrPediVdaindFinal: TSmallintField;
    QrPediVdaindPres: TSmallintField;
    QrPediVdaIndSinc: TSmallintField;
    QrPediVdafinNFe: TSmallintField;
    QrPediVdaRetiradaUsa: TSmallintField;
    QrPediVdaRetiradaEnti: TIntegerField;
    QrPediVdaEntregaUsa: TSmallintField;
    QrPediVdaEntregaEnti: TIntegerField;
    QrPediVdaCNPJCPFAvulso: TWideStringField;
    QrPediVdaRazaoNomeAvulso: TWideStringField;
    QrPediVdaInfIntermedEnti: TIntegerField;
    QrPediVdaEmiteAvulso: TSmallintField;
    QrPediVdaLk: TIntegerField;
    QrPediVdaDataCad: TDateField;
    QrPediVdaDataAlt: TDateField;
    QrPediVdaUserCad: TIntegerField;
    QrPediVdaUserAlt: TIntegerField;
    QrPediVdaAlterWeb: TSmallintField;
    QrPediVdaAWServerID: TIntegerField;
    QrPediVdaAWStatSinc: TSmallintField;
    QrPediVdaAtivo: TSmallintField;
    QrPediVdaReferenPedi: TWideStringField;
    QrPediVdaDesco_V: TFloatField;
    QrPediVdaDesco_P: TFloatField;
    QrPediVdaIND_PGTO: TWideStringField;
    QrPediVdaValBru: TFloatField;
    QrPediVdavProd: TFloatField;
    QrPediVdavFrete: TFloatField;
    QrPediVdavSeg: TFloatField;
    QrPediVdavOutro: TFloatField;
    QrPediVdavDesc: TFloatField;
    QrPediVdavBC: TFloatField;
    BtTrava: TBitBtn;
    QrAltIts: TMySQLQuery;
    QrAltItsControle: TIntegerField;
    PMBaixa: TPopupMenu;
    ETE1: TMenuItem;
    EPI1: TMenuItem;
    Manuteno1: TMenuItem;
    Outros1: TMenuItem;
    Consumo1: TMenuItem;
    Devoluo1: TMenuItem;
    QrPVI: TMySQLQuery;
    QrPVIprod_vProd: TFloatField;
    QrPVIprod_vFrete: TFloatField;
    QrPVIprod_vSeg: TFloatField;
    QrPVIprod_vOutro: TFloatField;
    QrPVIprod_vDesc: TFloatField;
    PMFiscal: TPopupMenu;
    IncluiDocumento1: TMenuItem;
    AlteraDocumento1: TMenuItem;
    ExcluiDocumento1: TMenuItem;
    MenuItem2: TMenuItem;
    IncluiItemdodocumento1: TMenuItem;
    AlteraoItemselecionadododocumento1: TMenuItem;
    ExcluioItemselecionadododocumento1: TMenuItem;
    QrEfdInnNFsCab: TMySQLQuery;
    QrEfdInnNFsCabNO_TER: TWideStringField;
    QrEfdInnNFsCabMovFatID: TIntegerField;
    QrEfdInnNFsCabMovFatNum: TIntegerField;
    QrEfdInnNFsCabMovimCod: TIntegerField;
    QrEfdInnNFsCabEmpresa: TIntegerField;
    QrEfdInnNFsCabControle: TIntegerField;
    QrEfdInnNFsCabTerceiro: TIntegerField;
    QrEfdInnNFsCabPecas: TFloatField;
    QrEfdInnNFsCabPesoKg: TFloatField;
    QrEfdInnNFsCabAreaM2: TFloatField;
    QrEfdInnNFsCabAreaP2: TFloatField;
    QrEfdInnNFsCabValorT: TFloatField;
    QrEfdInnNFsCabMotorista: TIntegerField;
    QrEfdInnNFsCabPlaca: TWideStringField;
    QrEfdInnNFsCabCOD_MOD: TSmallintField;
    QrEfdInnNFsCabCOD_SIT: TSmallintField;
    QrEfdInnNFsCabSER: TIntegerField;
    QrEfdInnNFsCabNUM_DOC: TIntegerField;
    QrEfdInnNFsCabCHV_NFE: TWideStringField;
    QrEfdInnNFsCabNFeStatus: TIntegerField;
    QrEfdInnNFsCabDT_DOC: TDateField;
    QrEfdInnNFsCabDT_E_S: TDateField;
    QrEfdInnNFsCabVL_DOC: TFloatField;
    QrEfdInnNFsCabIND_PGTO: TWideStringField;
    QrEfdInnNFsCabVL_DESC: TFloatField;
    QrEfdInnNFsCabVL_ABAT_NT: TFloatField;
    QrEfdInnNFsCabVL_MERC: TFloatField;
    QrEfdInnNFsCabIND_FRT: TWideStringField;
    QrEfdInnNFsCabVL_FRT: TFloatField;
    QrEfdInnNFsCabVL_SEG: TFloatField;
    QrEfdInnNFsCabVL_OUT_DA: TFloatField;
    QrEfdInnNFsCabVL_BC_ICMS: TFloatField;
    QrEfdInnNFsCabVL_ICMS: TFloatField;
    QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField;
    QrEfdInnNFsCabVL_ICMS_ST: TFloatField;
    QrEfdInnNFsCabVL_IPI: TFloatField;
    QrEfdInnNFsCabVL_PIS: TFloatField;
    QrEfdInnNFsCabVL_COFINS: TFloatField;
    QrEfdInnNFsCabVL_PIS_ST: TFloatField;
    QrEfdInnNFsCabVL_COFINS_ST: TFloatField;
    QrEfdInnNFsCabNFe_FatID: TIntegerField;
    QrEfdInnNFsCabNFe_FatNum: TIntegerField;
    QrEfdInnNFsCabNFe_StaLnk: TSmallintField;
    QrEfdInnNFsCabVSVmcWrn: TSmallintField;
    QrEfdInnNFsCabVSVmcObs: TWideStringField;
    QrEfdInnNFsCabVSVmcSeq: TWideStringField;
    QrEfdInnNFsCabVSVmcSta: TSmallintField;
    DsEfdInnNFsCab: TDataSource;
    QrEfdInnNFsIts: TMySQLQuery;
    QrEfdInnNFsItsMovFatID: TIntegerField;
    QrEfdInnNFsItsMovFatNum: TIntegerField;
    QrEfdInnNFsItsMovimCod: TIntegerField;
    QrEfdInnNFsItsEmpresa: TIntegerField;
    QrEfdInnNFsItsMovimNiv: TIntegerField;
    QrEfdInnNFsItsMovimTwn: TIntegerField;
    QrEfdInnNFsItsControle: TIntegerField;
    QrEfdInnNFsItsConta: TIntegerField;
    QrEfdInnNFsItsGraGru1: TIntegerField;
    QrEfdInnNFsItsGraGruX: TIntegerField;
    QrEfdInnNFsItsprod_vProd: TFloatField;
    QrEfdInnNFsItsprod_vFrete: TFloatField;
    QrEfdInnNFsItsprod_vSeg: TFloatField;
    QrEfdInnNFsItsprod_vOutro: TFloatField;
    QrEfdInnNFsItsQTD: TFloatField;
    QrEfdInnNFsItsUNID: TWideStringField;
    QrEfdInnNFsItsVL_ITEM: TFloatField;
    QrEfdInnNFsItsVL_DESC: TFloatField;
    QrEfdInnNFsItsIND_MOV: TWideStringField;
    QrEfdInnNFsItsCFOP: TIntegerField;
    QrEfdInnNFsItsCOD_NAT: TWideStringField;
    QrEfdInnNFsItsVL_BC_ICMS: TFloatField;
    QrEfdInnNFsItsALIQ_ICMS: TFloatField;
    QrEfdInnNFsItsVL_ICMS: TFloatField;
    QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField;
    QrEfdInnNFsItsALIQ_ST: TFloatField;
    QrEfdInnNFsItsVL_ICMS_ST: TFloatField;
    QrEfdInnNFsItsIND_APUR: TWideStringField;
    QrEfdInnNFsItsCOD_ENQ: TWideStringField;
    QrEfdInnNFsItsVL_BC_IPI: TFloatField;
    QrEfdInnNFsItsALIQ_IPI: TFloatField;
    QrEfdInnNFsItsVL_IPI: TFloatField;
    QrEfdInnNFsItsVL_BC_PIS: TFloatField;
    QrEfdInnNFsItsALIQ_PIS_p: TFloatField;
    QrEfdInnNFsItsQUANT_BC_PIS: TFloatField;
    QrEfdInnNFsItsALIQ_PIS_r: TFloatField;
    QrEfdInnNFsItsVL_PIS: TFloatField;
    QrEfdInnNFsItsVL_BC_COFINS: TFloatField;
    QrEfdInnNFsItsALIQ_COFINS_p: TFloatField;
    QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField;
    QrEfdInnNFsItsALIQ_COFINS_r: TFloatField;
    QrEfdInnNFsItsVL_COFINS: TFloatField;
    QrEfdInnNFsItsCOD_CTA: TWideStringField;
    QrEfdInnNFsItsVL_ABAT_NT: TFloatField;
    QrEfdInnNFsItsDtCorrApo: TDateTimeField;
    QrEfdInnNFsItsxLote: TWideStringField;
    QrEfdInnNFsItsOri_IPIpIPI: TFloatField;
    QrEfdInnNFsItsOri_IPIvIPI: TFloatField;
    QrEfdInnNFsItsNO_PRD_TAM_COR: TWideStringField;
    QrEfdInnNFsItsSIGLAUNIDMED: TWideStringField;
    QrEfdInnNFsItsGerBxaEstq: TSmallintField;
    QrEfdInnNFsItsNCM: TWideStringField;
    QrEfdInnNFsItsUnidMed: TIntegerField;
    QrEfdInnNFsItsEx_TIPI: TWideStringField;
    QrEfdInnNFsItsGrandeza: TSmallintField;
    QrEfdInnNFsItsTipo_Item: TSmallintField;
    DsEfdInnNFsIts: TDataSource;
    QrGraGru1: TMySQLQuery;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1IPI_CST: TSmallintField;
    QrGraGru1PIS_CST: TSmallintField;
    QrGraGru1COFINS_CST: TSmallintField;
    QrGraGru1ICMSRec_pAliq: TFloatField;
    QrGraGru1IPIRec_pAliq: TFloatField;
    QrGraGru1PISRec_pAliq: TFloatField;
    QrGraGru1COFINSRec_pAliq: TFloatField;
    QrPQEUF_EMPRESA: TSmallintField;
    QrPQEUF_FORNECE: TSmallintField;
    PCFinCtb: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GridT: TDBGrid;
    GroupBox7: TGroupBox;
    GridF: TDBGrid;
    TabSheet2: TTabSheet;
    DBGInnNFsCab: TDBGrid;
    Splitter2: TSplitter;
    DBGInnNFsIts: TDBGrid;
    Panel8: TPanel;
    Label6: TLabel;
    Label29: TLabel;
    MeWarn: TMemo;
    MeInfo: TMemo;
    QrPQESqLinked: TIntegerField;
    QrPQEItsSqLinked: TIntegerField;
    QrPQEIND_FRT: TSmallintField;
    QrPQEIND_PGTO: TSmallintField;
    QrEIN: TMySQLQuery;
    QrEINControle: TIntegerField;
    QrEfdInnNFsCabCliInt: TIntegerField;
    TabSheet3: TTabSheet;
    PMCTe: TPopupMenu;
    BtCTe2: TBitBtn;
    BtNFe2: TBitBtn;
    IncluiConhecimentodefrete1: TMenuItem;
    AlteraConhecimentodefrete1: TMenuItem;
    ExcluiConhecimentodefrete1: TMenuItem;
    QrEfdInnCTsCab: TMySQLQuery;
    DsEfdInnCTsCab: TDataSource;
    QrEfdInnCTsCabMovFatID: TIntegerField;
    QrEfdInnCTsCabMovFatNum: TIntegerField;
    QrEfdInnCTsCabMovimCod: TIntegerField;
    QrEfdInnCTsCabEmpresa: TIntegerField;
    QrEfdInnCTsCabControle: TIntegerField;
    QrEfdInnCTsCabIsLinked: TSmallintField;
    QrEfdInnCTsCabSqLinked: TIntegerField;
    QrEfdInnCTsCabCliInt: TIntegerField;
    QrEfdInnCTsCabTerceiro: TIntegerField;
    QrEfdInnCTsCabPecas: TFloatField;
    QrEfdInnCTsCabPesoKg: TFloatField;
    QrEfdInnCTsCabAreaM2: TFloatField;
    QrEfdInnCTsCabAreaP2: TFloatField;
    QrEfdInnCTsCabValorT: TFloatField;
    QrEfdInnCTsCabMotorista: TIntegerField;
    QrEfdInnCTsCabPlaca: TWideStringField;
    QrEfdInnCTsCabIND_OPER: TWideStringField;
    QrEfdInnCTsCabIND_EMIT: TWideStringField;
    QrEfdInnCTsCabCOD_MOD: TWideStringField;
    QrEfdInnCTsCabCOD_SIT: TSmallintField;
    QrEfdInnCTsCabSER: TWideStringField;
    QrEfdInnCTsCabSUB: TWideStringField;
    QrEfdInnCTsCabNUM_DOC: TIntegerField;
    QrEfdInnCTsCabCHV_CTE: TWideStringField;
    QrEfdInnCTsCabCTeStatus: TIntegerField;
    QrEfdInnCTsCabDT_DOC: TDateField;
    QrEfdInnCTsCabDT_A_P: TDateField;
    QrEfdInnCTsCabTP_CT_e: TSmallintField;
    QrEfdInnCTsCabCHV_CTE_REF: TWideStringField;
    QrEfdInnCTsCabVL_DOC: TFloatField;
    QrEfdInnCTsCabVL_DESC: TFloatField;
    QrEfdInnCTsCabIND_FRT: TWideStringField;
    QrEfdInnCTsCabVL_SERV: TFloatField;
    QrEfdInnCTsCabVL_BC_ICMS: TFloatField;
    QrEfdInnCTsCabVL_ICMS: TFloatField;
    QrEfdInnCTsCabVL_NT: TFloatField;
    QrEfdInnCTsCabCOD_INF: TWideStringField;
    QrEfdInnCTsCabCOD_CTA: TWideStringField;
    QrEfdInnCTsCabCOD_MUN_ORIG: TIntegerField;
    QrEfdInnCTsCabCOD_MUN_DEST: TIntegerField;
    QrEfdInnCTsCabNFe_FatID: TIntegerField;
    QrEfdInnCTsCabNFe_FatNum: TIntegerField;
    QrEfdInnCTsCabNFe_StaLnk: TSmallintField;
    QrEfdInnCTsCabVSVmcWrn: TSmallintField;
    QrEfdInnCTsCabVSVmcObs: TWideStringField;
    QrEfdInnCTsCabVSVmcSeq: TWideStringField;
    QrEfdInnCTsCabVSVmcSta: TSmallintField;
    QrEfdInnCTsCabCST_ICMS: TIntegerField;
    QrEfdInnCTsCabCFOP: TIntegerField;
    QrEfdInnCTsCabALIQ_ICMS: TFloatField;
    QrEfdInnCTsCabVL_OPR: TFloatField;
    QrEfdInnCTsCabVL_RED_BC: TFloatField;
    QrEfdInnCTsCabCOD_OBS: TWideStringField;
    QrEfdInnCTsCabNO_TER: TWideStringField;
    DBGrid2: TDBGrid;
    QrEmissFCentroCusto: TIntegerField;
    QrEmissTCentroCusto: TIntegerField;
    QrEmissTGenCtbD: TIntegerField;
    QrEmissTGenCtbC: TIntegerField;
    QrPQEItsIPI_CST: TWideStringField;
    QrPQEItsPIS_CST: TWideStringField;
    QrPQEItsCOFINS_CST: TWideStringField;
    QrPQEItsOriPart: TIntegerField;
    QrEfdInnNFsItsCST_IPI: TWideStringField;
    QrEfdInnNFsItsCST_PIS: TWideStringField;
    QrEfdInnNFsItsCST_COFINS: TWideStringField;
    QrEfdInnNFsItsCST_ICMS: TWideStringField;
    EntradaporPedidoXNFebaixada1: TMenuItem;
    QrPQEItsTES_ICMS: TSmallintField;
    QrPQEItsTES_IPI: TSmallintField;
    QrPQEItsTES_PIS: TSmallintField;
    QrPQEItsTES_COFINS: TSmallintField;
    QrPQERegrFiscal: TIntegerField;
    QrPQECondicaoPG: TIntegerField;
    QrEmissFFatParcRef: TIntegerField;
    QrEfdInnNFsCabRegrFiscal: TIntegerField;
    QrPQEItsEFD_II_C195: TSmallintField;
    QrEfdInnNFsItsAjusteVL_BC_ICMS: TFloatField;
    QrEfdInnNFsItsAjusteALIQ_ICMS: TFloatField;
    QrEfdInnNFsItsAjusteVL_ICMS: TFloatField;
    QrEfdInnNFsItsAjusteVL_OUTROS: TFloatField;
    QrPQEItsNFeItsI_nItem: TIntegerField;
    QrPQEItsFisRegGenCtbD: TIntegerField;
    QrPQEItsFisRegGenCtbC: TIntegerField;
    QrPQEItsAjusteVL_BC_ICMS: TFloatField;
    QrPQEItsAjusteALIQ_ICMS: TFloatField;
    QrPQEItsAjusteVL_ICMS: TFloatField;
    QrPQEItsAjusteVL_OUTROS: TFloatField;
    QrPQEItsAjusteVL_OPR: TFloatField;
    QrPQEItsAjusteVL_RED_BC: TFloatField;
    QrEfdInnNFsCabTpEntrd: TIntegerField;
    QrPQEMadeBy: TSmallintField;
    QrPediVdaMadeBy: TSmallintField;
    QrEfdInnCTsCabRegrFiscal: TIntegerField;
    QrEfdInnCTsCabIND_NAT_FRT: TWideStringField;
    QrEfdInnCTsCabVL_ITEM: TFloatField;
    QrEfdInnCTsCabCST_PIS: TWideStringField;
    QrEfdInnCTsCabNAT_BC_CRED: TWideStringField;
    QrEfdInnCTsCabVL_BC_PIS: TFloatField;
    QrEfdInnCTsCabALIQ_PIS: TFloatField;
    QrEfdInnCTsCabVL_PIS: TFloatField;
    QrEfdInnCTsCabCST_COFINS: TWideStringField;
    QrEfdInnCTsCabVL_BC_COFINS: TFloatField;
    QrEfdInnCTsCabALIQ_COFINS: TFloatField;
    QrEfdInnCTsCabVL_COFINS: TFloatField;
    BtNFe1: TBitBtn;
    BtCTe1: TBitBtn;
    QrR: TMySQLQuery;
    QrRPISST_vBC: TFloatField;
    QrRPISST_pPIS: TFloatField;
    QrRPISST_qBCProd: TFloatField;
    QrRPISST_vAliqProd: TFloatField;
    QrRPISST_vPIS: TFloatField;
    QrRPISST_fatorBCST: TFloatField;
    QrT: TMySQLQuery;
    QrTCOFINSST_vBC: TFloatField;
    QrTCOFINSST_pCOFINS: TFloatField;
    QrTCOFINSST_qBCProd: TFloatField;
    QrTCOFINSST_vAliqProd: TFloatField;
    QrTCOFINSST_vCOFINS: TFloatField;
    QrTCOFINSST_fatorBCST: TFloatField;
    EntradaporXMLmodelo55NFenovo1: TMenuItem;
    QrPQENFe_FatID: TIntegerField;
    QrPQENFe_FatNum: TIntegerField;
    QrPQEItsdFab: TDateField;
    QrPQEItsdVal: TDateField;
    QrPQEItsdFab_TXT: TWideStringField;
    QrPQEItsdVal_TXT: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtTravaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure GridFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrPQEAfterOpen(DataSet: TDataSet);
    procedure QrPQEItsCalcFields(DataSet: TDataSet);
    procedure QrEmissTCalcFields(DataSet: TDataSet);
    procedure QrEmissFCalcFields(DataSet: TDataSet);
    procedure BtExportaClick(Sender: TObject);
    procedure QrPQEAfterScroll(DataSet: TDataSet);
    procedure Entradasempedido1Click(Sender: TObject);
    procedure Entradaparcialdepedido1Click(Sender: TObject);
    procedure Encerramentodepedido1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtQuitaClick(Sender: TObject);
    procedure IncluiF1Click(Sender: TObject);
    procedure Itemdeinsumo1Click(Sender: TObject);
    procedure IncluiTClick(Sender: TObject);
    procedure AlteraT1Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure AlteraF1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrPQECalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiItsClick(Sender: TObject);
    procedure BtAlteraItsClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtExcluiItsClick(Sender: TObject);
    procedure ExcluiInsumo1Click(Sender: TObject);
    procedure ExcluiDuplT1Click(Sender: TObject);
    procedure ExcluiDuplF1Click(Sender: TObject);
    procedure QrPQEItsAfterScroll(DataSet: TDataSet);
    procedure EntradaporXML2Click(Sender: TObject);
    procedure QrPQEBeforeClose(DataSet: TDataSet);
    procedure OutrosdadosdeNFnormal1Click(Sender: TObject);
    procedure PMIncluiPopup(Sender: TObject);
    procedure AlteraDadosEstoque1Click(Sender: TObject);
    procedure AlteraDadosFiscais1Click(Sender: TObject);
    procedure PMAlteraPopup(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure Alterasomentecabealho1Click(Sender: TObject);
    procedure InsumossemNFdecobertura1Click(Sender: TObject);
    procedure PMAlteraItsPopup(Sender: TObject);
    procedure PMExcluiItsPopup(Sender: TObject);
    procedure PMIncluiItsPopup(Sender: TObject);
    procedure AlteralanamentofinanceiroFornecedor1Click(Sender: TObject);
    procedure AlteralanamentofinanceiroTransportador1Click(Sender: TObject);
    procedure LanamentofinancxeiroapagarFornecedor1Click(Sender: TObject);
    procedure LanamentofinancxeiroapagarTransportadora1Click(Sender: TObject);
    procedure Outros1Click(Sender: TObject);
    procedure ETE1Click(Sender: TObject);
    procedure EPI1Click(Sender: TObject);
    procedure Manuteno1Click(Sender: TObject);
    procedure BtBaixaClick(Sender: TObject);
    procedure Consumo1Click(Sender: TObject);
    procedure Devoluo1Click(Sender: TObject);
    procedure IncluiDocumento1Click(Sender: TObject);
    procedure QrEfdInnNFsCabAfterScroll(DataSet: TDataSet);
    procedure QrEfdInnNFsCabBeforeClose(DataSet: TDataSet);
    procedure ExcluiDocumento1Click(Sender: TObject);
    procedure AlteraDocumento1Click(Sender: TObject);
    procedure IncluiItemdodocumento1Click(Sender: TObject);
    procedure AlteraoItemselecionadododocumento1Click(Sender: TObject);
    procedure ExcluioItemselecionadododocumento1Click(Sender: TObject);
    procedure BtNFe2Click(Sender: TObject);
    procedure BtCTe2Click(Sender: TObject);
    procedure PMCTePopup(Sender: TObject);
    procedure PMFiscalPopup(Sender: TObject);
    procedure IncluiConhecimentodefrete1Click(Sender: TObject);
    procedure AlteraConhecimentodefrete1Click(Sender: TObject);
    procedure ExcluiConhecimentodefrete1Click(Sender: TObject);
    procedure EntradaporPedidoXNFebaixada1Click(Sender: TObject);
    procedure EntradaporXMLmodelo55NFenovo1Click(Sender: TObject);
  private
    { Private declarations }
    FTabLctA: String;
    FEmpresa: Integer;
    FTravando: Boolean;
    FPediVda: Integer;
    FVerPed, FLastPQEIns: Integer;
    //
    procedure PerguntaAQuery;
    procedure CriaOForm;
//    procedure LocalizaNome(Nome: String);
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure AlteraRegistro(EnableBtDesiste, VerificaLk : Boolean);
    procedure IncluiSubRegistro;
    procedure ExcluiSubRegistro;
    procedure AlteraSubRegistro;
    procedure IncluiItemDuplicataTeF;
    procedure ExcluiItemDuplicataT;
    procedure ExcluiItemDuplicataF;

    function  IncluiRegistro(PediPQ, PediVda: Integer): Boolean;
    procedure TravaOForm;

    //procs do form

    function  VerificaPesos(Msg : Boolean) : StatusVP;
    function  CalculaDiferencas(tPag: TAquemPag): Double;
    procedure DistribuiCustoDoFrete();
    procedure DistribuiCustoDoFrete3();
    procedure IniciaAlteracao(Codigo : Integer; EnableBtDesiste : Boolean);
    procedure EscolhePedido;
    procedure EscolhePedido2();
    procedure EscolhePedido3();
    procedure AtualizaEntradaPedidoIts(Insumo : Integer);
    procedure AtualizaEntradaPedido;
    procedure DefineVarDup;
    //procedure AtualizaDadosPQ();
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    function  ImpedePeloRetorno(): Boolean;
    function  NaoPermiteFinanca(Qry: TmySQLQuery; NeedRecs: Boolean): Boolean;
    procedure ReopenretImpost();
    //
    procedure AlteraLctoF(Sender: TObject);
    procedure AlteraLctoT(Sender: TObject);
    procedure IncluiLctoF(Sender: TObject);
    procedure IncluiLctoT(Sender: TObject);
    procedure AlteraF(Sender: TObject);
    procedure AlteraT(Sender: TObject);
    procedure AlteraItensImportadosDePediVda();
    // ini EfdInnNFs
    procedure MostraFormEfdInnNFsCab(SQLType: TSQLType);
    procedure MostraFormEfdInnNFsIts(SQLType: TSQLType);
    procedure ReopenEFdInnNFsCab(Controle: Integer);
    procedure ReopenEfdInnNFsIts(Conta: Integer);
    function  ExcluiEfdInnNFsCab(): Boolean;
    function  ExcluiEfdInnNFsIts(): Boolean;
    function  ExcluiEfdInnCTsCab(): Boolean;
    //function  ExcluiEfdInnCTsIts(): Boolean;
    // fim EfdInnNFs
    // ini EfdInnCTs
    procedure MostraFormEfdInnCTsCab(SQLType: TSQLType);
    procedure MostraFormEfdInnCTsIts(SQLType: TSQLType);
    procedure ReopenEFdInnCTsCab(Controle: Integer);
    // fim EfdInnCTs

  public
    { Public declarations }
    FControle: Integer;
    function  InsereRegCondicionado1(Pedido: Integer): Boolean;
    function  InsereRegCondicionado2(PediVda : Integer): Boolean;
    function  InsereRegCondicionado3(PediVda, FatID, FatNum, Empresa,
              CodInfoEmit, IQ, CI, RegrFiscal, CondicaoPG: Integer; refNFe: String; ModNF, Serie, NF:
              Integer; NFe_CC: String; modCC,SerCC, NF_CC, NFVP_FatID,
              NFVP_FatNum, NFVP_StaLnk, NFCC_FatID, NFCC_FatNum, NFCC_StaLnk:
              Integer; DtEmiss: TDateTime): Boolean;
    procedure LocCod(Atual, Codigo: Integer);
    procedure MostraFormPQENew(Codigo: Integer; EnableBtDesiste, Travando: Boolean);
    procedure SubQuery1Reopen;
    procedure ReopenPQEIts(Codigo: Integer);
    procedure ReopenEmissF(FatParcela: Integer);
    procedure ReopenEmissT(FatParcela: Integer);
    procedure ReopenNFeCabA();
    procedure ExcluiTodaEntradaPediVdaEmProgresso(Codigo: Integer);
    procedure ExcluiTodaEntradaPvdNFeEmProgresso(Codigo: Integer);
    function  FinalizaEntrada(Pergunta: Boolean): Boolean;
  end;

var
  FmPQE: TFmPQE;
  FEhPQPedido: Integer;

implementation

uses Curinga, Principal, Module, (*PQEEdit*)PQEIts, PQENew, BlueDermConsts,
PQEPesq, PQx, MyDBCheck, ModuleGeral, UnitMyXML, OmniXML, UnMyObjects,
Entidade2, PQ, ModuleNFe_0000, NFeCabA_0000, PQO, UnFinanceiro,
DmkDAC_PF, ModuleLct2, UnPQ_PF, (*PQEPed,*) PQEPed3, PQEPed2, PQEPed,
EfdInnNFsCab, EfdInnNFsIts, ModProd, EfdInnCTsCab, PQEXml;

{$R *.DFM}

const
  FEFDInnNFSMainFatID = VAR_FATID_0010;
  CO_MovimCod_ZERO    = 0;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPQE.LanamentofinancxeiroapagarFornecedor1Click(Sender: TObject);
begin
  IncluiLctoF(Sender);
end;

procedure TFmPQE.LanamentofinancxeiroapagarTransportadora1Click(
  Sender: TObject);
begin
  IncluiLctoT(Sender);
end;

procedure TFmPQE.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQE.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPQECodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmPQE.DefParams;
begin
  VAR_GOTOTABELA := 'PQE';
  VAR_GOTOMySQLTABLE := QrPQE;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  //VAR_GOTOVAR1 := 'Empresa='+ Geral.FF0(FEmpresa);
  VAR_GOTOVAR1 := 'Empresa IN (0, '+ Geral.FF0(FEmpresa) + ')';

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT s1.Login NOMEUSERCAD, s2.Login NOMEUSERALT,');
  VAR_SQLx.Add('en.*, CASE WHEN tr.Tipo=0 then tr.RazaoSocial');
  VAR_SQLx.Add('ELSE tr.Nome END NOMETRANSPORTADORA,');
  VAR_SQLx.Add('CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial ELSE ci.Nome');
  VAR_SQLx.Add('END NOMECLIINT, ei.CodCliInt, ');
  VAR_SQLx.Add('IF(em.Tipo=0, em.EUF, em.PUF) UF_EMPRESA, ');
  VAR_SQLx.Add('IF(iq.Tipo=0, iq.EUF, iq.PUF) UF_FORNECE ');
  VAR_SQLx.Add('FROM pqe en');
  VAR_SQLx.Add('LEFT JOIN enticliint ei ON ei.CodEnti=en.CI');
  VAR_SQLx.Add('LEFT JOIN entidades  tr ON tr.Codigo=en.Transportadora');
  VAR_SQLx.Add('LEFT JOIN entidades  ci ON ci.Codigo=en.CI');
  VAR_SQLx.Add('LEFT JOIN entidades  em ON em.Codigo=en.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades  iq ON iq.Codigo=en.IQ');
  VAR_SQLx.Add('LEFT JOIN senhas     s1 ON tr.UserCad=s1.Numero');
  VAR_SQLx.Add('LEFT JOIN senhas     s2 ON tr.UserAlt=s2.Numero');
  VAR_SQLx.Add('WHERE en.Empresa IN (0, ' + Geral.FF0(FEmpresa) + ')');
  //
  VAR_SQL1.Add('AND en.Codigo=:P0');
  //
  VAR_SQLa.Add('');
  //
end;

procedure TFmPQE.Devoluo1Click(Sender: TObject);
begin
  UnPQx.GerenciaBaixaDevolucao(0);
end;

{
procedure TFmPQE.AtualizaDadosPQ();
const
  MoedaPadrao = 0; // s� se for a moeda Real
var
  CustoPadrao: Double;
  PQ, CI: Integer;
begin
(*
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE pqcli SET CustoPadrao=:P0');
  Dmod.QrUpd.SQL.Add('WHERE MoedaPadrao=0'); // s� se for a moeda Real
  Dmod.QrUpd.SQL.Add('AND PQ=:P1 AND CI=:P2');
  Dmod.QrUpd.Params[0].AsFloat   := QrPQEItsVALORKG.Value;
  Dmod.QrUpd.Params[1].AsInteger := QrPQEItsInsumo.Value;
  Dmod.QrUpd.Params[2].AsInteger := QrPQECI.Value;
  Dmod.QrUpd.ExecSQL;
*)
  CustoPadrao := QrPQEItsVALORKG.Value;
  PQ          := QrPQEItsInsumo.Value;
  CI          := QrPQECI.Value;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pqcli', False, [
  'CustoPadrao'], [
  'MoedaPadrao', 'PQ', 'CI', 'Empresa'], [
  CustoPadrao], [
  MoedaPadrao, PQ, CI, FEmpresa], True);
  //
end;
}

procedure TFmPQE.DefineVarDup;
begin
  IC3_ED_FatNum := QrPQECodigo.Value;
  // ini 2022-03-26
  //IC3_ED_NF     := Geral.IMV(EdNF.Text);
  IC3_ED_NF     := QrPQENF.Value;
  // fim 2022-03-26
  IC3_ED_Data   := QrPQEData.Value;
end;

procedure TFmPQE.AtualizaEntradaPedido;
begin
  (*QrSoma.Close;
  QrSoma.Params[0].AsInteger := QrPQEPedido.Value;
  UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
  QrSomaUpd.Params[0].AsFloat := QrSomaPesoL.Value;
  QrSomaUpd.Params[1].AsFloat := QrSomaPesoB.Value;
  QrSomaUpd.Params[2].AsFloat := QrSomaValor.Value;
  QrSomaUpd.Params[3].AsFloat := QrSomaEntrada.Value;
  QrSomaUpd.Params[4].AsInteger := QrPQEPedido.Value;
  QrSomaUpd.ExecSQL;*)
end;

procedure TFmPQE.AtualizaEntradaPedidoIts(Insumo : Integer);
begin
  (*QrPedEntra.Close;
  QrPedEntra.Params[0].AsInteger := Insumo;
  QrPedEntra.Params[1].AsInteger := QrPQEPedido.Value;
  UnDmkDAC_PF.AbreQuery(QrPedEntra, Dmod.MyDB);
  QrPedidoInsumo.Close;
  QrPedidoInsumo.Params[0].AsInteger := Insumo;
  QrPedidoInsumo.Params[1].AsInteger := QrPQEPedido.Value;
  UnDmkDAC_PF.AbreQuery(QrPedidoInsumo, Dmod.MyDB);
  if QrPedidoInsumo.RecordCount > 0 then
  begin
    QrUpdPedido.Params[0].AsFloat := QrPedEntraTotalPeso.Value;
    QrUpdPedido.Params[1].AsFloat := QrPQEPedido.Value;
    QrUpdPedido.Params[2].AsFloat := Insumo;
    QrUpdPedido.ExecSQL;
  end
  else
  begin
    QrPedidoConta.Params[0].AsInteger := QrPQEPedido.Value;
    UnDmkDAC_PF.AbreQuery(QrPedidoConta, Dmod.MyDB);
    QrInsPedido.Params[0].AsFloat := QrPedEntraTotalPeso.Value;
    QrInsPedido.Params[1].AsFloat := QrPQEPedido.Value;
    QrInsPedido.Params[2].AsFloat := Insumo;
    QrInsPedido.Params[3].AsFloat := QrPedidoContaConta.Value;
    QrInsPedido.ExecSQL;
    QrPedidoConta.Close;
  end;
  QrPedEntra.Close;
  QrPedidoInsumo.Close;*)
end;

procedure TFmPQE.EscolhePedido;
begin
{ Atualizar C�digo! Empresa (FEmpresa)!
  if DBCheck.CriaFm(TFmPQEPed, FmPQEPed, afmoNegarComAviso) then
  begin
    FmPQEPed.ShowModal;
    FmPQEPed.Destroy;
  end;
}
end;

procedure TFmPQE.EscolhePedido2();
begin
  if DBCheck.CriaFm(TFmPQEPed2, FmPQEPed2, afmoNegarComAviso) then
  begin
    FmPQEPed2.ShowModal;
    FmPQEPed2.Destroy;
  end;
end;

procedure TFmPQE.EscolhePedido3;
begin
  if DBCheck.CriaFm(TFmPQEPed3, FmPQEPed3, afmoNegarComAviso) then
  begin
    FmPQEPed3.ShowModal;
    FmPQEPed3.Destroy;
  end;
end;

procedure TFmPQE.ETE1Click(Sender: TObject);
begin
  UnPQx.GerenciaBaixaETE(0);
end;

function TFmPQE.InsereRegCondicionado1(Pedido : Integer): Boolean;
//var
//  Status : StatusVP;
begin
  //if FmPrincipal.ImpedePeloBalanco(Date) then Exit;
  Result := IncluiRegistro(Pedido, 0);
  //Status := VerificaPesos(False);
  //if Status <> vpErro then DistribuiCustoDoFrete(Status);
  DistribuiCustoDoFrete;
end;

function TFmPQE.InsereRegCondicionado2(PediVda: Integer): Boolean;
begin
  Result := IncluiRegistro(0, PediVda);
  DistribuiCustoDoFrete;
end;

function TFmPQE.InsereRegCondicionado3(PediVda, FatID, FatNum, Empresa,
  CodInfoEmit, IQ, CI, RegrFiscal, CondicaoPG: Integer; refNFe: String; ModNF, Serie, NF: Integer;
  NFe_CC: String; modCC, SerCC, NF_CC, NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
  NFCC_FatID, NFCC_FatNum, NFCC_StaLnk: Integer; DtEmiss: TDateTime): Boolean;
var
  Desiste: Boolean;
  PQE: Integer;
begin
  Result := True;
  //
  if DBCheck.CriaFm(TFmPQENew, FmPQENew, afmoNegarComAviso) then
  begin
    FmPQENew.FNFe_FatID   := FatID;
    FmPQENew.FNFe_FatNum  := FatNum;
    FmPQENew.FNFe_Empresa := Empresa;
    FmPQENew.FCodInfoEmit := CodInfoEmit;
    //FmPQENew.FRegrFiscal  := RegrFiscal;
    //
    PQE := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'PQE', 'PQE', 'Codigo');
    FmPQENew.ImgTipo.SQLType := stIns;
    FmPQENew.EdCodigo.Text   := Geral.FF0(PQE);
    DModG.SelecionaEmpresaSeUnica(FmPQENew.EdEmpresa, FmPQENew.CBEmpresa);
    FmPQENew.TPEntrada.Date  := Date;
    FmPQENew.TPEmissao.Date  := DtEmiss;
    //
    FmPQENew.EdRegrFiscal.ValueVariant   := RegrFiscal;
    FmPQENew.CBRegrFiscal.KeyValue       := RegrFiscal;
    FmPQENew.EdCondicaoPG.ValueVariant   := CondicaoPG;
    FmPQENew.CBCondicaoPG.KeyValue       := CondicaoPG;
    FmPQENew.EdCI.ValueVariant           := CI;
    FmPQENew.CBCI.KeyValue               := CI;
    FmPQENew.EdFornecedor.ValueVariant   := CodInfoEmit;
    FmPQENew.CBFornecedor.KeyValue       := CodInfoEmit;
    FmPQENew.CkTipoNF.Checked            := True;
    FmPQENew.EdrefNFe.Text               := refNFe;
    FmPQENew.Edmodnf.ValueVariant        := ModNF;
    FmPQENew.EdSerie.ValueVariant        := Serie;
    FmPQENew.EdNFe_CC.ValueVariant       := NFe_CC;
    FmPQENew.EdmodCC.ValueVariant        := modCC;
    FmPQENew.EdSerCC.ValueVariant        := SerCC;
    FmPQENew.EdNF_CC.ValueVariant        := NF_CC;
    FmPQENew.EdNFVP_FatID.ValueVariant   := NFVP_FatID;
    FmPQENew.EdNFVP_FatNum.ValueVariant  := NFVP_FatNum;
    FmPQENew.EdNFVP_StaLnk.ValueVariant  := NFVP_StaLnk;
    FmPQENew.EdNFCC_FatID.ValueVariant   := NFCC_FatID;
    FmPQENew.EdNFCC_FatNum.ValueVariant  := NFCC_FatNum;
    FmPQENew.EdNFCC_StaLnk.ValueVariant  := NFCC_StaLnk;
    //
    FmPQENew.EdCI.Enabled           := False;
    FmPQENew.CBCI.Enabled           := False;
    FmPQENew.EdFornecedor.Enabled   := False;
    FmPQENew.CBFornecedor.Enabled   := False;
    FmPQENew.CkTipoNF.Enabled       := False;
    FmPQENew.EdrefNFe.Enabled       := False;
    FmPQENew.Edmodnf.Enabled        := False;
    FmPQENew.EdSerie.Enabled        := False;
    FmPQENew.EdNFe_CC.Enabled       := False;
    FmPQENew.EdmodCC.Enabled        := False;
    FmPQENew.EdSerCC.Enabled        := False;
    FmPQENew.EdNF_CC.Enabled        := False;
    FmPQENew.EdNFVP_FatID.Enabled   := False;
    FmPQENew.EdNFVP_FatNum.Enabled  := False;
    FmPQENew.EdNFVP_StaLnk.Enabled  := False;
    FmPQENew.EdNFCC_FatID.Enabled   := False;
    FmPQENew.EdNFCC_FatNum.Enabled  := False;
    FmPQENew.EdNFCC_StaLnk.Enabled  := False;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPediVda, Dmod.MyDB, [
    'SELECT pvd.*,',
    'SUM(pvi.ValBru) ValBru,',
    'SUM(pvi.vProd) vProd,',
    'SUM(pvi.vFrete) vFrete,',
    'SUM(pvi.vSeg) vSeg,',
    'SUM(pvi.vOutro) vOutro,',
    'SUM(pvi.vDesc) vDesc,',
    'SUM(pvi.vBC) vBC',
    '  ',
    'FROM pedivda pvd  ',
    'LEFT JOIN pedivdaits pvi ON pvi.Codigo=pvd.Codigo',
    'WHERE pvd.Codigo=' + Geral.FF0(PediVda),
    '']);
    //
    FmPQENew.EdPedido.Text                := Geral.FF0(QrPediVdaCodigo.Value);
    FmPQENew.LaSitPedido.Caption          := Geral.FF0(FEhPQPedido);
    //
    (*FmPQENew.EdFornecedor.ValueVariant    := QrPediVdaCliente.Value;
    FmPQENew.CBFornecedor.KeyValue        := QrPediVdaCliente.Value;
    FmPQENew.EdCI.ValueVariant            := QrPediVdaEmpresa.Value;
    FmPQENew.CBCI.KeyValue                := QrPediVdaEmpresa.Value;*)
    FmPQENew.EdTransportador.ValueVariant := QrPediVdaTransporta.Value;
    FmPQENew.CBTransportador.KeyValue     := QrPediVdaTransporta.Value;
    FmPQENew.EdFrete.ValueVariant         := QrPediVdavFrete.Value;
    FmPQENew.EdkgBruto.ValueVariant       := 0.000;
    FmPQENew.EdkgLiquido.ValueVariant     := QrPediVdaQuantP.Value;
    FmPQENew.EdValorNF.ValueVariant       := QrPediVdaValLiq.Value;
    FmPQENew.EdConhecim.ValueVariant      := 0;
    FmPQENew.EdJuros.ValueVariant         := 0.00;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPVI, Dmod.MyDB, [
    'SELECT ',
    'SUM(ICMSTot_vProd) prod_vProd,',
    'SUM(ICMSTot_vFrete) prod_vFrete,',
    'SUM(ICMSTot_vSeg) prod_vSeg,',
    'SUM(ICMSTot_vOutro) prod_vOutro,',
    'SUM(ICMSTot_vDesc) prod_vDesc',
    'FROM nfecaba',
    'WHERE FatID=' + Geral.FF0(FatID),
    'AND FatNum=' + Geral.FF0(FatNum),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
    FmPQENew.EdICMSTot_vProd.ValueVariant  := QrPVIprod_vProd.Value;
    FmPQENew.EdICMSTot_vFrete.ValueVariant := QrPVIprod_vFrete.Value;
    FmPQENew.EdICMSTot_vSeg.ValueVariant   := QrPVIprod_vSeg.Value;
    FmPQENew.EdICMSTot_vDesc.ValueVariant  := QrPVIprod_vDesc.Value;
    FmPQENew.EdICMSTot_vOutro.ValueVariant := QrPVIprod_vOutro.Value;

    FmPQENew.EdIND_FRT.ValueVariant        := QrPediVdaFretePor.Value;

    FmPQENew.EdIND_PGTO.ValueVariant       := QrPediVdaIND_PGTO.Value;
    // Fazer s� aqui
    FmPQENew.FPediVda                      := PediVda;
    FmPQENew.FVerPedi                      := 4;
    FmPQENew.FTpEntrd                      := 0;


    //
    GBCntrl.Visible               := False;
    GBTrava.Visible               := True;
    //
    GOTOy.BotoesSb(ImgTipo.SQLType);
    FmPQENew.RGMadeBy.ItemIndex := QrPediVdaMadeBy.Value;
    FmPQENew.ShowModal;
    //
    Desiste := FmPQENew.FDesiste;
    //
    FmPQENew.Destroy;
    //
    FEhPQPedido := 0;//zera aviso de  entrada de pedido
    //
    if not Desiste then
    begin
      LocCod(PQE, PQE);
      if QrPQECodigo.Value = PQE then
      begin
        if PediVda > 0 then
          AlteraItensImportadosDePediVda();
      end;
    end;
    //
    Result := not Desiste;
  end;
  //
  DistribuiCustoDoFrete;
  Result := true;
end;

procedure TFmPQE.InsumossemNFdecobertura1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
(*
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _pq_compara_estq_nfs_; ',
    'CREATE TABLE _pq_compara_estq_nfs_ ',
    'SELECT pqx.CliOrig, pqx.Insumo,  ',
    'SUM(pqx.SdoPeso) SdoPeso ',
    'FROM ' + TMeuDB + '.pqx pqx ',
    'WHERE pqx.SdoPeso>0 ',
    'GROUP BY pqx.CliOrig, pqx.Insumo; ',
    'SELECT pcl.Peso, pqx.* ',
    'FROM _pq_compara_estq_nfs_ pqx, ' + TMeuDB + '.pqcli pcl  ',
    'WHERE  pcl.CI=pqx.CliOrig ',
    'AND pcl.PQ=pqx.Insumo  ',
    'AND pcl.Peso > pqx.SdoPeso ',
    '']);
    //Geral.MB_SQL(nil, Qry);
    Result := Qry.RecordCount = 0;
    if (Result = False) and Mostra then
    begin
      if DBCheck.CriaFm(TFmPQwPQEIts, FmPQwPQEIts, afmoLiberado) then
      begin
        //
        FmPQwPQEIts.ShowModal;
        FmPQwPQEIts.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
*)
end;

procedure TFmPQE.IncluiItemdodocumento1Click(Sender: TObject);
begin
  MostraFormEfdInnNFsIts(stIns);
end;

procedure TFmPQE.IncluiItemDuplicataTeF;
const
  Sub = 0;
  Cliente= 0;
  Credito = 0;
  Compensado = '';
var
  Cursor: TCursor;
  Controle, Debito: Double;
  Sit, Documento, FatParcela, Genero, Tipo, Carteira, NotaFiscal, FatID, FatNum,
  Fornecedor, CliInt: Integer;
  Descricao, Data, Vencimento: String;
  //
  procedure InsereLct();
  begin
{
  object QrInsM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'INSERT INTO lan ctos SET'
      'Data=:P0,'
      'Tipo=:P1,'
      'Carteira=:P2,'
      'Controle=:P3,'
      'Documento=:P4,'
      'Genero=:P5,'
      'Descricao=:P6,'
      'NotaFiscal=:P7,'
      'Debito=:P8,'
      'Credito=:P9,'
      'Compensado=:P10,'
      'Vencimento=:P11,'
      'Sit=:P12,'
      'FatID=:P13,'
      'FatNum=:P14,'
      'FatParcela=:P15,'
      'Fornecedor=:P16,'
      'Cliente=0')
}
    Data       := Geral.FDT(QrPQEData.Value, 1);
    Tipo       := 2;//RGTipo.ItemIndex;
    Carteira   := 3;//CBCarteira.KeyValue;
    NotaFiscal := QrPQENF.Value;
    Vencimento := Geral.FDT(Date, 1);
    //
    //Controle := UMyMod.BuscaEmLivreY_Double(Dmod.MyDB, 'LivresM', 'ControlM',
      //LAN_CTOS, LAN_CTOS, 'Controle');
    Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                  VAR_LCT, VAR_LCT, 'Controle');
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, FTabLctA, False, [
    'Documento', 'Genero', 'CliInt',
    'Descricao', 'NotaFiscal', 'Debito',
    'Credito', 'Compensado', 'Vencimento',
    'Sit', 'FatID', 'FatNum',
    'FatParcela', 'Fornecedor', 'Cliente'
    ], [
    'Data', 'Tipo', 'Carteira',
    'Controle', 'Sub'
    ], [
    Documento, Genero, CliInt,
    Descricao, NotaFiscal, Debito,
    Credito, Compensado, Vencimento,
    Sit, FatID, FatNum,
    FatParcela, Fornecedor, Cliente
    ], [
    Data, Tipo, Carteira,
    Controle, Sub
    ], True);
  end;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    FatNum     := QrPQECodigo.Value;
    CliInt     := QrPQECodCliInt.Value;
    if QrPQEValorNF.Value > 0 then
    begin
      Descricao := VAR_MSG_INSUMOQUIMICO;
      Genero := -100;
      FatParcela := 1;
      Documento := 0;
      Debito := QrPQEValorNF.Value;
      Sit := 0;
(*
      QrUpdM.Close;
      QrUpdM.SQL.Clear;
      QrUpdM.SQL.Add('UPDATE lan ctos SET FatParcela=FatParcela+1');
      QrUpdM.SQL.Add('WHERE FatParcela>=:P0 AND FatID=:P1');
      QrUpdM.SQL.Add('AND FatNum=:P2');
      QrUpdM.Params[0].AsInteger := FatParcela;
      QrUpdM.Params[1].AsInteger := VAR_FATID_1001;
      QrUpdM.Params[2].AsInteger := QrPQECodigo.Value;
      QrUpdM.ExecSQL;
*)
      FatID      := VAR_FATID_1001;
      Fornecedor := QrPQEIQ.Value;
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE ' + FTabLctA + ' SET FatParcela=FatParcela+1 ',
      'WHERE FatParcela>=' + Geral.FF0(FatParcela),
      'AND FatID=' + Geral.FF0(FatID),
      'AND FatNum=' + Geral.FF0(FatNum),
      '']);
      InsereLct();

(*
      Controle := UMyMod.BuscaEmLivreY_Double(Dmod.MyDB, 'LivresM', 'ControlM',
        'Lan ctos','Lan ctos','Controle');
      QrInsM.Params[0].AsDate := QrPQEData.Value;
      QrInsM.Params[1].AsInteger := 2;//RGTipo.ItemIndex;
      QrInsM.Params[2].AsInteger := 3;//CBCarteira.KeyValue;
      QrInsM.Params[3].AsFloat := Controle;
      QrInsM.Params[4].AsInteger := Documento;
      QrInsM.Params[5].AsInteger := Genero;
      QrInsM.Params[6].AsString := Descricao;
      QrInsM.Params[7].AsInteger := QrPQENF.Value;
      QrInsM.Params[8].AsFloat := Debito;
      QrInsM.Params[9].AsFloat := 0;
      QrInsM.Params[10].AsString := CO_VAZIO;
      QrInsM.Params[11].AsDate := Date;
      QrInsM.Params[12].AsInteger := Sit;
      QrInsM.Params[13].AsInteger := VAR_FATID_1002;
      QrInsM.Params[14].AsInteger := QrPQECodigo.Value;
      QrInsM.Params[15].AsInteger := FatParcela;
      QrInsM.Params[16].AsInteger := QrPQEIQ.Value;
      QrInsM.ExecSQL;
*)
    end;
    ////////

    if QrPQEFrete.Value > 0 then
    begin
      Descricao := VAR_MSG_FRETEPQ;
      Genero := -101;
      FatParcela := 1;
      Documento := 0;
      Debito := QrPQEFrete.Value;
      Sit := 0;
(*
      QrUpdM.Close;
      QrUpdM.SQL.Clear;
      QrUpdM.SQL.Add('UPDATE lan ctos SET FatParcela=FatParcela+1');
      QrUpdM.SQL.Add('WHERE FatParcela>=:P0 AND FatID=:P1');
      QrUpdM.SQL.Add('AND FatNum=:P2');
      QrUpdM.Params[0].AsInteger := FatParcela;
      QrUpdM.Params[1].AsInteger := VAR_FATID_1002;
      QrUpdM.Params[2].AsInteger := QrPQECodigo.Value;
      QrUpdM.ExecSQL;
*)
      FatID      := VAR_FATID_1002;
      Fornecedor := QrPQETransportadora.Value;
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE ' + FTabLctA + ' SET FatParcela=FatParcela+1 ',
      'WHERE FatParcela>=' + Geral.FF0(FatParcela),
      'AND FatID=' + Geral.FF0(FatID),
      'AND FatNum=' + Geral.FF0(FatNum),
      '']);
      InsereLct();

(*

      Controle := UMyMod.BuscaEmLivreY_Double(Dmod.MyDB, 'LivresM', 'ControlM', 'Lan ctos','Lan ctos','Controle');

      QrInsM.Params[0].AsDate := QrPQEData.Value;
      QrInsM.Params[1].AsInteger := 2;//RGTipo.ItemIndex;
      QrInsM.Params[2].AsInteger := 3;//CBCarteira.KeyValue;
      QrInsM.Params[3].AsFloat := Controle;
      QrInsM.Params[4].AsInteger := Documento;
      QrInsM.Params[5].AsInteger := Genero;
      QrInsM.Params[6].AsString := Descricao;
      QrInsM.Params[7].AsInteger := QrPQENF.Value;
      QrInsM.Params[8].AsFloat := Debito;
      QrInsM.Params[9].AsFloat := 0;
      QrInsM.Params[10].AsString := CO_VAZIO;
      QrInsM.Params[11].AsDate := Date;
      QrInsM.Params[12].AsInteger := Sit;
      QrInsM.Params[13].AsInteger := VAR_FATID_1002;
      QrInsM.Params[14].AsInteger := QrPQECodigo.Value;
      QrInsM.Params[15].AsInteger := FatParcela;
      QrInsM.Params[16].AsInteger := QrPQETransportadora.Value;
      QrInsM.ExecSQL;
*)
    end;
  except
    Screen.Cursor := Cursor;
    Exit;
  end;
  /////
  ReopenEmissF(0);
  ReopenEmissT(0);
end;

procedure TFmPQE.IncluiLctoF(Sender: TObject);
const
  Qtde = 0.000;
  Qtd2 = 0.000;
var
  Terceiro, Cod, Genero, Empresa, NF: Integer;
  SerieNF: String;
  Valor: Double;
  GenCtbD, GenCtbC: Integer;
  TypCtbCadMoF: TTypCtbCadMoF;
  Descricao: String;
begin
  PCFinCtb.ActivePageIndex := 0;
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
(* ini 2022-03-23
  if FmPrincipal.AvisaFaltaDeContaDoPlano(VAR_FATID_1001) then
    Exit;
*)
  //
  DefineVarDup;
  //
  Genero   := Dmod.QrControleCtaPQCompr.Value;
  Cod      := QrPQECodigo.Value;
  Terceiro := QrPQEIQ.Value;
  Valor    := CalculaDiferencas(apFornece);
  Empresa  := DModG.QrEmpresasCodigo.Value;
  SerieNF  := Geral.FF0(QrPQESerie.Value);
  NF       := QrPQENF.Value;
  //
  if Valor = 0 then
    Valor := QrPQEValorNF.Value;
  //

  GenCtbD := 0;
  GenCtbC := 0;
  if DModG.QrCtrlGeralUsarCtbEstqParaUC.Value = 1 then
    TypCtbCadMoF := TTypCtbCadMoF.tccmfEstoqueIn
  else
    TypCtbCadMoF := TTypCtbCadMoF.tccmfUsoConsumoIn;
  //
  if QrPQEIts.RecordCount = 1 then
    PQ_PF.DefineContasContabeisEntradaInsumosPQE_UmItem(TypCtbCadMoF, Cod, GenCtbD, GenCtbC, Descricao)
  else
    PQ_PF.DefineContasContabeisEntradaInsumosPQE_VariosItens(TypCtbCadMoF, Cod, GenCtbD, GenCtbC, Descricao);
  //
(*
SELECT gg1.*
FROM pqeits pqi
LEFT JOIN gragru1    gg1 ON gg1.Nivel1=pqi.Insumo
LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel1
LEFT JOIN prdGrupTip pgt ON pgt.Codigo=gg1.PrdGrupTip
WHERE pqi.Codigo=23
*)
(*
SELECT SUM(pqi.ValorItem) SumValor, gg2.*, gg1.*
FROM pqeits pqi
LEFT JOIN gragru1    gg1 ON gg1.Nivel1=pqi.Insumo
LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2
LEFT JOIN prdGrupTip pgt ON pgt.Codigo=gg1.PrdGrupTip
WHERE pqi.Codigo=23
GROUP BY gg1.Nivel2
ORDER BY SumValor DESC
*)
  UPagtos.Pagto(QrEmissF, tpDeb, Cod, Terceiro, VAR_FATID_1001, 0(*GenCtbD*), 0(*GenCtbC*), stIns,
    'Compra de Insumos Qu�micos', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0,
    0, True, False, 0, 0, 0, 0, NF, FTabLctA, Descricao, SerieNF, Qtde, Qtd2, GenCtbD, GenCtbC);
// fim 2022-03-23
  SubQuery1Reopen;
end;

procedure TFmPQE.IncluiLctoT(Sender: TObject);
var
  Terceiro, Cod, Genero, Empresa, NF: Integer;
  SerieNF: String;
  Valor: Double;
  Descricao: String;
begin
  PCFinCtb.ActivePageIndex := 0;
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
(* ini 2022-03-23
  if FmPrincipal.AvisaFaltaDeContaDoPlano(VAR_FATID_1002) then
    Exit;
*)
  //
  DefineVarDup;
  //
  Genero   := Dmod.QrControleCtaPQFrete.Value;
  Cod      := QrPQECodigo.Value;
  Terceiro := QrPQETransportadora.Value;
  Valor    := CalculaDiferencas(apTranspo);
  Empresa  := DModG.QrEmpresasCodigo.Value;
  SerieNF  := Geral.FF0(QrPQESerie.Value);
  NF       := QrPQENF.Value;
  //
  if Valor = 0 then
    Valor := QrPQEFrete.Value;
  //
  UPagtos.Pagto(QrEmissF, tpDeb, Cod, Terceiro, VAR_FATID_1002, 0(*GenCtbD*), 0(*GenCtbC*), stIns,
    'Tranporte de Insumos Qu�micos', Valor, VAR_USUARIO, 0, Empresa, mmNenhum,
    0, 0, True, False, 0, 0, 0, 0, NF, FTabLctA, 'Frete de compra de uso e consumo', SerieNF);
  SubQuery1Reopen;
  // fim 2022-03-23
end;

procedure TFmPQE.ExcluiItemDuplicataT();
begin
  if NaoPermiteFinanca(QrEmissT, True) then
    Exit;
  //if UFinanceiro.ExcluiLct_FatParcela(DMod.MyDB, QrEmissTFatID.Value,
  if UFinanceiro.ExcluiLct_FatParcela(QrEmissT, QrEmissTFatID.Value,
    QrEmissTFatNum.Value, QrEmissTFatParcela.Value, QrEmissTCarteira.Value,
    QrEmissTSit.Value, QrEmissTTipo.Value, dmkPF.MotivDel_ValidaCodigo(315),
    FTabLctA)
  then
    ReopenEmissT(QrEmissTFatParcela.Value);
end;

procedure TFmPQE.ExcluioItemselecionadododocumento1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if QrEfdInnNFsIts.RecordCount > 0 then
  begin
    Controle := QrEfdInnNFsCabControle.Value;
    //
    DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEfdInnNFsIts, TDBGrid(DBGInnNFsIts),
    'EfdInnNFsIts', ['Conta'], ['Conta'], istPergunta, '');
    //
    DmNFe_0000.AtualizaTotaisEfdInnNFs(Controle);
  end;
end;

procedure TFmPQE.ExcluiItemDuplicataF();
begin
  if QrEmissFFatParcRef.Value = 0 then
  begin
    //if UFinanceiro.ExcluiLct_FatParcela(DMod.MyDB, QrEmissFFatID.Value,
    if UFinanceiro.ExcluiLct_FatParcela(QrEmissF, QrEmissFFatID.Value,
      QrEmissFFatNum.Value, QrEmissFFatParcela.Value, QrEmissFCarteira.Value,
      QrEmissFSit.Value, QrEmissFTipo.Value, dmkPF.MotivDel_ValidaCodigo(314),
      FTabLctA)
    then
      ReopenEmissF(QrEmissFFatParcela.Value);
  end else
    Geral.MB_Info(
      'Os lan�amentos financeiros n�o podem exclu�dos aqu� pois foram emitidos no pedido!');
end;

(*procedure TFmPQE.DistribuiCustoDoFrete1(Status : StatusVP);
var
  CFkg, RICMS, RICMSF : Double;
  Peso : String;
begin
  CFkg := 0;
  Peso := 'TotalPeso';
  RICMSF := (100-QrPQERICMSF.VAlue)/100;
  RICMS := (100-QrPQERICMS.Value)/100;
  if Status = vpLiq then
  begin
    if QrPQEPesoL.Value > 0 then
    CFkg := (QrPQEFrete.Value / QrPQEPesoL.Value) * RICMSF;
  end
  else if Status = vpBruto then
  begin
    if QrPQEPesoB.Value > 0 then
    CFkg := (QrPQEFrete.Value / QrPQEPesoB.Value) * RICMSF;
    Peso := '(Volumes*PesoVB)';
  end
  else
  begin
    Geral.MB_Aviso('Imposs�vel distribuir custo do frete: Status desconhecido.');
    Exit;
  end;
  QrUpdW.Close;
  QrUpdW.SQL.Clear;
  QrUpdW.SQL.Clear;
  QrUpdW.SQL.Add('UPDATE pqeits SET');
  QrUpdW.SQL.Add('TotalCusto=((ValorItem*(1+((IPI-RIPI)/100)))*:P0)+('+Peso+'*:P1)');
  QrUpdW.SQL.Add('WHERE Codigo=:P2');

  QrUpdW.Params[0].AsFloat := RICMS;
  QrUpdW.Params[1].AsFloat := CFkg;
  QrUpdW.Params[2].AsInteger := QrPQECodigo.Value;

  QrUpdW.ExecSQL;

  QueryPrincipalAfterOpen;
  //
end; *)

(*
procedure TFmPQE.DistribuiCustoDoFrete2();
var
  Frete, CFin : Double;
begin
  if (QrPQEValorNF.Value - QrPQEJuros.Value) > 0 then
    CFin := (QrPQEJuros.Value / (QrPQEValorNF.Value - QrPQEJuros.Value)) * 100
  else CFin := 0;
  ///
  Frete := 0;
  if QrPQEPesoL.Value > 0 then
  Frete := (QrPQEFrete.Value / QrPQEPesoL.Value);
  QrUpdW.Close;
  QrUpdW.SQL.Clear;
  QrUpdW.SQL.Clear;
  QrUpdW.SQL.Add('UPDATE pqeits SET ');
  QrUpdW.SQL.Add('TotalCusto=((ValorItem + IPI_vIPI) * (1+(:P00 /100))) ');
  QrUpdW.SQL.Add(' + (PesoVL * Volumes* :P01), ');
  QrUpdW.SQL.Add('CFin=:P02 WHERE Codigo=:P03');
  QrUpdW.Params[00].AsFloat := CFin;
  QrUpdW.Params[01].AsFloat := Frete;
  QrUpdW.Params[02].AsFloat := CFin;
  QrUpdW.Params[03].AsInteger := QrPQECodigo.Value;
  QrUpdW.ExecSQL;
  //
  QueryPrincipalAfterOpen;
end;
*)

procedure TFmPQE.DistribuiCustoDoFrete();
begin
  DistribuiCustoDoFrete3();
end;

procedure TFmPQE.DistribuiCustoDoFrete3();
var
  Frete, CFin, RetornoImpostos: Double;
  CFin_TXT, Frete_TXT: String;
begin
  if (QrPQEValorNF.Value - QrPQEJuros.Value) > 0 then
    CFin := (QrPQEJuros.Value / (QrPQEValorNF.Value - QrPQEJuros.Value)) * 100
  else
    CFin := 0;
  //
  if QrPQEPesoL.Value > 0 then
  begin
    RetornoImpostos := QrPQEFreteRvICMS.Value + QrPQEFreteRvPIS.Value +
                        QrPQEFreteRvCOFINS.Value; // + QrPQEFreteRvIPI.Value;
    Frete           := ((QrPQEFrete.Value - RetornoImpostos) / QrPQEPesoL.Value);
  end else
    Frete := 0;
(*  QrUpdW.Close;
  QrUpdW.SQL.Clear;
  QrUpdW.SQL.Clear;
  QrUpdW.SQL.Add('UPDATE pqeits SET ');
  QrUpdW.SQL.Add('TotalCusto=((ValorItem + IPI_vIPI - (RvICMS + RvPIS + RvCOFINS)) * (1+(:P00 /100))) ');
  QrUpdW.SQL.Add(' + (PesoVL * Volumes* :P01), ');
  QrUpdW.SQL.Add('CFin=:P02 WHERE Codigo=:P03');
  QrUpdW.Params[00].AsFloat := CFin;
  QrUpdW.Params[01].AsFloat := Frete;
  QrUpdW.Params[02].AsFloat := CFin;
  QrUpdW.Params[03].AsInteger := QrPQECodigo.Value;
  QrUpdW.ExecSQL;
*)
  CFin_TXT  := Geral.FFT_Dot(CFin, 6, siNegativo);
  Frete_TXT := Geral.FFT_Dot(Frete, 6, siNegativo);
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdW, Dmod.MyDB, [
    'UPDATE pqeits SET ',
    'TotalCusto=((ValorItem + IPI_vIPI - (RvICMS + RvPIS + RvCOFINS + RvIPI)) * (1+(' +
    CFin_TXT + ' /100))) ',
    ' + (PesoVL * Volumes * ' + Frete_TXT + '), ',
    'CFin=' + CFin_TXT,
    'WHERE Codigo=' + Geral.FF0(QrPQECodigo.Value),
    '']);
  //Geral.MB_SQL(self, Dmod.QrUpdW);
  //
  QueryPrincipalAfterOpen;
end;

function TFmPQE.VerificaPesos(Msg : Boolean) : StatusVP;
begin
  Result := vpErro;
  QrConf.Close;
  QrConf.SQL.Clear;
  QrConf.SQL.Add('SELECT (SUM(Volumes * PesoVB)) TOTAL');
  QrConf.SQL.Add('FROM pqeits');
  QrConf.SQL.Add('WHERE Codigo=:P0');
  QrConf.Params[0].AsInteger := QrPQECodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrConf, Dmod.MyDB);
  if (QrPQEPesoB.Value > 0.09) and (QrConfTOTAL.Value > 0) then
  begin
    if QrConfTOTAL.Value <> QrPQEPesoB.Value then
    begin
      if Msg then
        Geral.MB_Erro('Os pesos brutos n�o conferem.'+sLineBreak+
          ' Se voc� n�o conhece todos os pesos brutos dos itens,'+sLineBreak+
          ' informe apenas os pesos l�quidos.');
      Exit;
    end;
    Result := vpBruto;
  end
  else
  begin
    QrConf.Close;
    QrConf.SQL.Clear;
    QrConf.SQL.Add('SELECT SUM(TotalPeso) TOTAL');
    QrConf.SQL.Add('FROM pqeits');
    QrConf.SQL.Add('WHERE Codigo=:P0');
    QrConf.Params[0].AsInteger := QrPQECodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrConf, Dmod.MyDB);
    if QrConfTOTAL.Value <> QrPQEPesoL.Value then
    begin
      if Msg then
        Geral.MB_Erro('Imposivel calcular o custo do frete.' + sLineBreak +
          'Os pesos l�quidos n�o conferem:' + sLineBreak +
          'Peso Cabe�alho: ' + Geral.FFT(QrPQEPesoL.Value, 3, siNegativo) +
          sLineBreak +
          'Peso Itens: ' + Geral.FFT(QrConfTOTAL.Value, 3, siNegativo) +
          sLineBreak + '');
      Exit;
    end;
    Result := vpLiq;
  end;
end;

procedure TFmPQE.ExcluiSubRegistro;
var
  Cliente, Codigo, Conta, Controle, Insumo, Empresa, SqLinked: Integer;
begin
  if ImpedePeloRetorno() then
    Exit;
  Cliente := QrPQECI.Value;
  Insumo  := QrPQEItsInsumo.Value;
  Codigo  := QrPQEItsCodigo.Value;
  Conta   := QrPQEItsConta.Value;
  // ini 2022-04-11
  //Empresa := QrPQEItsConta.Value;
  Empresa := QrPQEEmpresa.Value;
  Controle := QrPQEItsControle.Value;
  SqLinked := QrPQEItsSqLinked.Value;
  // fim 2022-04-11
  //
  if Geral.MB_Pergunta('Confirma a exclus�o deste item de nota fiscal?') = ID_YES then
  begin
    if ExcluiEfdInnNFsIts() then
    begin
      QrUpdW.Close;
      QrUpdW.DataBase := Dmod.MyDB;
      QrUpdW.SQL.Clear;
      QrUpdW.SQL.Add('DELETE FROM pqeits WHERE Codigo=:P0');
      QrUpdW.SQL.Add('AND Conta=:P1 AND Controle=:P2');
      QrUpdW.Params[0].AsInteger := Codigo;
      QrUpdW.Params[1].AsInteger := Conta;
      QrUpdW.Params[2].AsInteger := Controle;
      QrUpdW.ExecSQL;

      QrUpdW.Close;
      QrUpdW.DataBase := Dmod.MyDB;
      QrUpdW.SQL.Clear;
      QrUpdW.SQL.Add('UPDATE pqeits SET Conta=Conta-1');
      QrUpdW.SQL.Add('WHERE Conta>:P0 AND Codigo=:P1');
      QrUpdW.Params[0].AsInteger := Conta;
      QrUpdW.Params[1].AsInteger := Codigo;
      QrUpdW.ExecSQL;

      FControle := QrPQEItsControle.Value;
      ReopenPQEIts(Codigo);
      //
      UnPQx.AtualizaEstoquePQ(Cliente, Insumo, Empresa, aeMsg, CO_VAZIO);
    end;
  end;
end;

procedure TFmPQE.ExcluiTodaEntradaPediVdaEmProgresso(Codigo: Integer);
begin
  DMod.MyDB.Execute(Geral.ATS([
  'DELETE FROM pqe WHERE Codigo=' + Geral.FF0(Codigo),
  'DELETE FROM pqeits WHERE Codigo=' + Geral.FF0(Codigo)
  ]));
end;

procedure TFmPQE.ExcluiTodaEntradaPvdNFeEmProgresso(Codigo: Integer);
begin
  DMod.MyDB.Execute(Geral.ATS([
  'DELETE FROM pqe WHERE Codigo=' + Geral.FF0(Codigo) + ';',
  'DELETE FROM pqeits WHERE Codigo=' + Geral.FF0(Codigo) + ';'
  ]));
end;

procedure TFmPQE.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);

  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  with FmPQE do
    Width := 800;
    if Screen.Width = 640 then // 480
      begin
        Geral.MB_Aviso('Para melhor resolu��o desta janela,' + sLineBreak +
          'modifique a configura��o de v�deo' + sLineBreak + 'para 600 x 800 pixels.');
        Top := 0;
        Left := 0;
        Height := 450;
      end
    else
    if Screen.Width = 800 then  // 600
      begin
        Top := 40;
        Left := 0;
        Height := 530;
      end
    else                       //  1024 * 768
      begin
        Top := 120;
        Left := 112;
        Height := 600;
    end;
  Label3.Caption := 'Val. NF '+IC2_PadroesMoeda+':';
  Label15.Caption := 'Frete '+IC2_PadroesMoeda+':';
  Label9.Caption := 'Nota '+IC2_PadroesMoeda+':';
  GroupBox1.Caption := ' Itens da nota fiscal (valores em '+
    IC2_PadroesMoeda+'): ';
  if IC2_PadroesUsaRIPIPQ = 'F' then
    DBGrid1.Columns[6].Visible := False;
end;

procedure TFmPQE.TravaOForm;
begin
  ImgTipo.SQLType := stLok;
  GBTrava.Visible := False;
  GBCntrl.Visible := True;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  UMyMod.UpdUnlockY(QrPQECodigo.Value, Dmod.MyDB, 'PQE', 'Codigo');
end;

procedure TFmPQE.AlteraSubRegistro;
begin
  if ImpedePeloRetorno() then
    Exit;
{
  if DBCheck.CriaFm(TFmPQEEdit,FmPQEEdit, afmoNegarComAviso) then
  begin
    FmPQEEdit.EdCodigo.Enabled := False;
    FmPQEEdit.ImgTipo.SQLType := stUpd;
    FmPQEEdit.EdCodigo.ValueVariant := QrPQEItsConta.Value;

    FmPQEEdit.EdProduto.Text := Geral.TFT(Geral.FF0(QrPQEItsInsumo.Value), 0, siPositivo);
    FmPQEEdit.CBProduto.KeyValue := QrPQEItsInsumo.Value;
    FmPQEEdit.EdVolumes.Text := Geral.TFT(FloatToStr(QrPQEItsVolumes.Value), 3, siPositivo);
    FmPQEEdit.EdKgBruto.Text := Geral.TFT(FloatToStr(QrPQEItsPesoVB.Value), 3, siPositivo);
    FmPQEEdit.EdkgLiq.Text := Geral.TFT(FloatToStr(QrPQEItsPesoVL.Value), 3, siPositivo);
    FmPQEEdit.EdVlrItem.Text := Geral.TFT(FloatToStr(QrPQEItsValorItem.Value), 2, siPositivo);
    FmPQEEdit.EdIPI_pIPI.ValueVariant := QrPQEItsIPI_pIPI.Value;
    FmPQEEdit.EdIPI_vIPI.ValueVariant := QrPQEItsIPI_vIPI.Value;
    FmPQEEdit.Edprod_CFOP.Text        := QrPQEItsprod_CFOP.Value;
    FmPQEEdit.Pesos(0);
    FmPQEEdit.CalculaOnEdit;
    FControle := QrPQEItsControle.Value;
    ReopenPQEIts(QrPQEItsCodigo.Value);
    FmPQEEdit.ShowModal;
    FmPQEEdit.Destroy;
  end;
}
  if DBCheck.CriaFm(TFmPQEIts, FmPQEIts, afmoNegarComAviso) then
  begin
    FmPQEIts.FEmpresa                 := FEmpresa;
    FmPQEIts.FSqLinked                := QrPQEItsSqLinked.Value;
    FmPQEIts.FData                    := QrPQEDataE.Value;
    FmPQEIts.FRegrFiscal              := QrPQERegrFiscal.Value;

    //FmPQEIts.MostraTodosPQs();
    FmPQEIts.EdConta.Enabled         := False;
    FmPQEIts.ImgTipo.SQLType          := stUpd;
    FmPQEIts.EdConta.ValueVariant    := QrPQEItsConta.Value;
    //
    FmPQEIts.ReopenPQ(ptParcial);
    if not FmPQEIts.QrPQ1.Locate('PQ', QrPQEItsInsumo.Value, []) then
      FmPQEIts.ReopenPQ(ptTotal);
    //
    FmPQEIts.FTES_ICMS                := QrPQEItsTES_ICMS.Value;
    FmPQEIts.FTES_IPI                 := QrPQEItsTES_IPI.Value;
    FmPQEIts.FTES_PIS                 := QrPQEItsTES_PIS.Value;
    FmPQEIts.FTES_COFINS              := QrPQEItsTES_COFINS.Value;
    FmPQEIts.FEFD_II_C195             := QrPQEItsEFD_II_C195.Value;
    FmPQEIts.EdInsumo.ValueVariant    := QrPQEItsInsumo.Value;
    FmPQEIts.CBInsumo.KeyValue        := QrPQEItsInsumo.Value;
    FmPQEIts.EdVolumes.ValueVariant   := QrPQEItsVolumes.Value;
    FmPQEIts.EdPesoVB.ValueVariant    := QrPQEItsPesoVB.Value;
    FmPQEIts.EdPesoVL.ValueVariant    := QrPQEItsPesoVL.Value;
    FmPQEIts.EdVlrItem.ValueVariant   := QrPQEItsValorItem.Value;
    FmPQEIts.EdIPI_pIPI.ValueVariant  := QrPQEItsIPI_pIPI.Value;
    FmPQEIts.EdIPI_vIPI.ValueVariant  := QrPQEItsIPI_vIPI.Value;
    //FmPQEIts.Edprod_CFOP.ValueVariant := QrPQEItsprod_CFOP.Value;
    FmPQEIts.EdCFOP.ValueVariant      := QrPQEItsprod_CFOP.Value;

    FmPQEIts.EdRpICMS.ValueVariant    := QrPQEItsRpICMS.Value;
    FmPQEIts.EdRpPIS.ValueVariant     := QrPQEItsRpPIS.Value;
    FmPQEIts.EdRpCOFINS.ValueVariant  := QrPQEItsRpCOFINS.Value;
    FmPQEIts.EdRpIPI.ValueVariant     := QrPQEItsRpIPI.Value;

    FmPQEIts.EdRvICMS.ValueVariant    := QrPQEItsRvICMS.Value;
    FmPQEIts.EdRvPIS.ValueVariant     := QrPQEItsRvPIS.Value;
    FmPQEIts.EdRvCOFINS.ValueVariant  := QrPQEItsRvCOFINS.Value;
    FmPQEIts.EdRvIPI.ValueVariant     := QrPQEItsRvIPI.Value;

    FmPQEIts.EdxLote.ValueVariant     := QrPQEItsxLote.Value;

    FmPQEIts.EdICMS_Orig.ValueVariant  := QrPQEItsICMS_Orig.Value;
    FmPQEIts.EdICMS_CST.ValueVariant   := QrPQEItsICMS_CST.Value;
    FmPQEIts.EdIPI_CST.ValueVariant    := QrPQEItsIPI_CST.Value;
    FmPQEIts.EdPIS_CST.ValueVariant    := QrPQEItsPIS_CST.Value;
    FmPQEIts.EdCOFINS_CST.ValueVariant := QrPQEItsCOFINS_CST.Value;

    FmPQEIts.Edprod_vProd.ValueVariant  := QrPQEItsprod_vProd.Value;
    FmPQEIts.Edprod_vFrete.ValueVariant := QrPQEItsprod_vFrete.Value;
    FmPQEIts.Edprod_vSeg.ValueVariant   := QrPQEItsprod_vSeg.Value;
    FmPQEIts.Edprod_vOutro.ValueVariant := QrPQEItsprod_vOutro.Value;
    FmPQEIts.Edprod_vDesc.ValueVariant  := QrPQEItsprod_vDesc.Value;

    FmPQEIts.EdICMS_vBC.ValueVariant   := QrPQEItsICMS_vBC.Value;
    FmPQEIts.EdIPI_vBC.ValueVariant    := QrPQEItsIPI_vBC.Value;
    FmPQEIts.EdPIS_vBC.ValueVariant    := QrPQEItsPIS_vBC.Value;
    FmPQEIts.EdCOFINS_vBC.ValueVariant := QrPQEItsCOFINS_vBC.Value;
    FmPQEIts.EdICMS_vBCST.ValueVariant := QrPQEItsICMS_vBCST.Value;
    FmPQEIts.EdRpICMSST.ValueVariant   := QrPQEItsRpICMSST.Value;
    FmPQEIts.EdRvICMSST.ValueVariant   := QrPQEItsRvICMSST.Value;

    FmPQEIts.EdAjusteVL_BC_ICMS.ValueVariant   := QrPQEItsAjusteVL_BC_ICMS.Value;
    FmPQEIts.EdAjusteALIQ_ICMS.ValueVariant    := QrPQEItsAjusteALIQ_ICMS.Value;
    FmPQEIts.EdAjusteVL_ICMS.ValueVariant      := QrPQEItsAjusteVL_ICMS.Value;
    FmPQEIts.EdAjusteVL_OUTROS.ValueVariant    := QrPQEItsAjusteVL_OUTROS.Value;
    FmPQEIts.EdAjusteVL_OPR.ValueVariant       := QrPQEItsAjusteVL_OPR.Value;
    FmPQEIts.EdAjusteVL_RED_BC.ValueVariant    := QrPQEItsAjusteVL_RED_BC.Value;

    //...
    //
    FmPQEIts.Pesos(0);
    FmPQEIts.CalculaOnEdit;
    FControle := QrPQEItsControle.Value;
    ReopenPQEIts(QrPQEItsCodigo.Value);
    FmPQEIts.ShowModal;
    FmPQEIts.Destroy;
  end;
end;

procedure TFmPQE.AlteraRegistro(EnableBtDesiste, VerificaLk : Boolean);
var
  PQE : Integer;
begin
  PQE := QrPQECodigo.Value;
  if not VerificaLk then
    IniciaAlteracao(PQE, EnableBtDesiste)
  else
    if UMyMod.SellockY(PQE, Dmod.MyDB, 'PQE', 'Codigo') then Exit
    else  IniciaAlteracao(PQE, EnableBtDesiste);
end;

procedure TFmPQE.Alterasomentecabealho1Click(Sender: TObject);
const
  EnableBtDesiste = True;
var
  Codigo: Integer;
begin
  if UnPQx.ImpedePeloBalanco(QrPQEData.Value) then Exit;
  //
  Codigo := QrPQECodigo.Value;
  //
  MostraFormPQENew(Codigo, EnableBtDesiste, False);
  LocCod(Codigo, Codigo);
  //
  ImgTipo.SQLType := stLok;
end;

procedure TFmPQE.IniciaAlteracao(Codigo: Integer; EnableBtDesiste : Boolean);
begin
  try
    UMyMod.UpdlockY(Codigo, Dmod.MyDB, 'PQE', 'Codigo');
    GBCntrl.Visible := False;
    GBTrava.Visible := True;

    ImgTipo.SQLType := stUpd;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    MostraFormPQENew(Codigo, EnableBtDesiste, FTravando);
  finally
  Screen.Cursor := Cursor;
  end;
  LocCod(Codigo, Codigo);
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

function TFmPQE.IncluiRegistro(PediPQ, PediVda: Integer): Boolean;
var
  Desiste, UsouNFe: Boolean;
  PQE : Integer;
begin
  Result := True;
  //
  if DBCheck.CriaFm(TFmPQENew, FmPQENew, afmoNegarComAviso) then
  begin
    PQE := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'PQE', 'PQE', 'Codigo');
    FmPQENew.ImgTipo.SQLType := stIns;
    FmPQENew.EdCodigo.Text   := Geral.FF0(PQE);
    DModG.SelecionaEmpresaSeUnica(FmPQENew.EdEmpresa, FmPQENew.CBEmpresa);
    FmPQENew.TPEntrada.Date  := Date;
    FmPQENew.TPEmissao.Date  := 0;
    //
    FmPQENew.EdCI.Text            := '';
    FmPQENew.CBCI.KeyValue        := NULL;
    FmPQENew.CkTipoNF.Checked     := True;
    FmPQENew.EdrefNFe.Text        := '';
    FmPQENew.Edmodnf.ValueVariant := 55;
    FmPQENew.EdSerie.ValueVariant := 0;
    FmPQENew.EdCI.Enabled         := True;
    FmPQENew.CBCI.Enabled         := True;
    // Dados do Pedido (se existir)
    if FEhPQPedido > 0 then
    begin
      //FmPQENew.RGMadeBy.Enabled := False;
      if PediPQ <> 0 then
      begin
        QrPedido.Close;
        QrPedido.Params[0].AsInteger := FmPQEPed.CBPedido.KeyValue;
        UnDmkDAC_PF.AbreQuery(QrPedido, Dmod.MyDB);
        //
        FmPQENew.FVerPedi                 := 2;
        FmPQENew.FTpEntrd                 := 0;
        FmPQENew.EdPedido.Text            := Geral.FF0(PediPQ);
        FmPQENew.LaSitPedido.Caption      := Geral.FF0(FEhPQPedido);
        FmPQENew.EdFornecedor.Text        := Geral.FF0(QrPedidoFornece.Value);
        FmPQENew.EdTransportador.Text     := Geral.FF0(QrPedidoTransporte.Value);
        FmPQENew.CBFornecedor.KeyValue    := QrPedidoFornece.Value;
        FmPQENew.CBTransportador.KeyValue := QrPedidoTransporte.Value;
        FmPQENew.EdkgBruto.Text           := FloatToStr(QrPedidoPesoB.Value);
        FmPQENew.EdkgLiquido.Text         := FloatToStr(QrPedidoPesoL.Value);
        FmPQENew.EdValorNF.Text           := FloatToStr(QrPedidoValor.Value);
      end else
      if PediVda > 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrPediVda, Dmod.MyDB, [
        'SELECT pvd.*,',
        'SUM(pvi.ValBru) ValBru,',
        'SUM(pvi.vProd) vProd,',
        'SUM(pvi.vFrete) vFrete,',
        'SUM(pvi.vSeg) vSeg,',
        'SUM(pvi.vOutro) vOutro,',
        'SUM(pvi.vDesc) vDesc,',
        'SUM(pvi.vBC) vBC',
        '  ',
        'FROM pedivda pvd  ',
        'LEFT JOIN pedivdaits pvi ON pvi.Codigo=pvd.Codigo',
        'WHERE pvd.Codigo=' + Geral.FF0(PediVda),
        '']);
        //
        FmPQENew.EdPedido.Text                := Geral.FF0(QrPediVdaCodigo.Value);
        FmPQENew.FVerPedi                     := 3;
        FmPQENew.FTpEntrd                     := 0;
        FmPQENew.LaSitPedido.Caption          := Geral.FF0(FEhPQPedido);
        //FmPQENew.CkTipoNF.Checked             := ?
        //
        //FmPQENew.TPEntrada.Data               := N�o! Tem data m�nima!
        FmPQENew.EdRegrFiscal.ValueVariant    := QrPediVdaRegrFiscal.Value;
        FmPQENew.CBRegrFiscal.KeyValue        := QrPediVdaRegrFiscal.Value;
        FmPQENew.EdCondicaoPG.ValueVariant    := QrPediVdaCondicaoPG.Value;
        FmPQENew.CBCondicaoPG.KeyValue        := QrPediVdaCondicaoPG.Value;
        FmPQENew.EdFornecedor.ValueVariant    := QrPediVdaCliente.Value;
        FmPQENew.CBFornecedor.KeyValue        := QrPediVdaCliente.Value;
        FmPQENew.EdCI.ValueVariant            := QrPediVdaEmpresa.Value;
        FmPQENew.CBCI.KeyValue                := QrPediVdaEmpresa.Value;
        FmPQENew.EdTransportador.ValueVariant := QrPediVdaTransporta.Value;
        FmPQENew.CBTransportador.KeyValue     := QrPediVdaTransporta.Value;
        FmPQENew.EdFrete.ValueVariant         := QrPediVdavFrete.Value;
        FmPQENew.EdkgBruto.ValueVariant       := 0.000;
        FmPQENew.EdkgLiquido.ValueVariant     := QrPediVdaQuantP.Value;
        FmPQENew.EdValorNF.ValueVariant       := QrPediVdaValLiq.Value;
        FmPQENew.EdConhecim.ValueVariant      := 0;
        //FmPQENew.TPEmissao.Date               := ??
        FmPQENew.EdJuros.ValueVariant         := 0.00;
        //FmPQENew.EdrefNFe.Text                := ???
        //
        {
        FmPQENew.EdFreteRpICMS.ValueVariant
        FmPQENew.EdFreteRpPIS.ValueVariant
        FmPQENew.EdFreteRpCOFINS.ValueVariant
        FmPQENew.EdFreteRvICMS.ValueVariant
        FmPQENew.EdFreteRvPIS.ValueVariant
        FmPQENew.EdFreteRvCOFINS.ValueVariant
        // NF de venda
        FmPQENew.EdmodNF.ValueVariant
        FmPQENew.EdSerie.ValueVariant
        FmPQENew.EdNF.ValueVariant
        //
        // NF de Remessa para Produ��o (Industrializa��o)
        FmPQENew.EdNFe_RP.ValueVariant
        FmPQENew.EdmodRP.ValueVariant
        FmPQENew.EdSerRP.ValueVariant
        FmPQENew.EdNF_RP.ValueVariant

        // NF de Cobertura do Cliente
        FmPQENew.EdNFe_CC.ValueVariant
        FmPQENew.EdmodCC.ValueVariant
        FmPQENew.EdSerCC.ValueVariant
        FmPQENew.EdNF_CC.ValueVariant
        //
        // CTe - Conhecimento de Frete
        FmPQENew.EdCte_Id.ValueVariant
        FmPQENew.EdCTe_mod.ValueVariant
        FmPQENew.EdCTe_serie.ValueVariant
        //
        FmPQENew.EdNFVP_FatID.ValueVariant
        FmPQENew.EdNFVP_FatNum.ValueVariant
        FmPQENew.EdNFVP_StaLnk.ValueVariant
        FmPQENew.EdNFRP_FatID.ValueVariant
        FmPQENew.EdNFRP_FatNum.ValueVariant
        FmPQENew.EdNFRP_StaLnk.ValueVariant
        FmPQENew.EdNFCC_FatID.ValueVariant
        FmPQENew.EdNFCC_FatNum.ValueVariant
        FmPQENew.EdNFCC_StaLnk.ValueVariant
        }
        //FmPQENew.EdCodigo.ValueVariant;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrPVI, Dmod.MyDB, [
        'SELECT ',
        'SUM(vProd) prod_vProd,',
        'SUM(vFrete) prod_vFrete,',
        'SUM(vSeg) prod_vSeg,',
        'SUM(vOutro) prod_vOutro,',
        'SUM(vDesc) prod_vDesc',
        'FROM pedivdaits',
        'WHERE Codigo=' + Geral.FF0(PediVda),
        '']);


        FmPQENew.EdICMSTot_vProd.ValueVariant  := QrPVIprod_vProd.Value;
        FmPQENew.EdICMSTot_vFrete.ValueVariant := QrPVIprod_vFrete.Value;
        FmPQENew.EdICMSTot_vSeg.ValueVariant   := QrPVIprod_vSeg.Value;
        FmPQENew.EdICMSTot_vDesc.ValueVariant  := QrPVIprod_vDesc.Value;
        FmPQENew.EdICMSTot_vOutro.ValueVariant := QrPVIprod_vOutro.Value;

        FmPQENew.EdIND_FRT.ValueVariant        := QrPediVdaFretePor.Value;

        FmPQENew.EdIND_PGTO.ValueVariant := QrPediVdaIND_PGTO.Value;

        FmPQENew.RGMadeBy.ItemIndex := QrPediVdaMadeBy.Value;

        // Fazer s� aqui
        FmPQENew.FPediVda := PediVda;
      end;
    end else
    begin
      //FmPQENew.RGMadeBy.Enabled         := True;
      FmPQENew.FVerPedi                 := 1;
      FmPQENew.EdPedido.Text            := '';
      FmPQENew.LaSitPedido.Caption      := '';
      FmPQENew.EdFornecedor.Text        := '';
      FmPQENew.EdTransportador.Text     := '';
      FmPQENew.CBFornecedor.KeyValue    := NULL;
      FmPQENew.CBTransportador.KeyValue := NULL;
      FmPQENew.CBCI.KeyValue            := NULL;
      FmPQENew.EdkgBruto.Text           := '';
      FmPQENew.EdkgLiquido.Text         := '';
      FmPQENew.EdValorNF.Text           := '';
    end;
    {
    FmPQENew.EdCI.Text            := '';
    FmPQENew.CBCI.KeyValue        := NULL;
    FmPQENew.CkTipoNF.Checked     := True;
    FmPQENew.EdrefNFe.Text        := '';
    FmPQENew.Edmodnf.ValueVariant := 55;
    FmPQENew.EdSerie.ValueVariant := 0;
    FmPQENew.EdCI.Enabled         := True;
    FmPQENew.CBCI.Enabled         := True;
    }
    GBCntrl.Visible               := False;
    GBTrava.Visible               := True;
    //
    GOTOy.BotoesSb(ImgTipo.SQLType);
    FmPQENew.ShowModal;
    //
    Desiste := FmPQENew.FDesiste;
    //
    UsouNFe := FmPQENew.FUsouNFe;
    FmPQENew.Destroy;
    //
    FEhPQPedido := 0;//zera aviso de  entrada de pedido
    //
    if not Desiste then
    begin
      LocCod(PQE, PQE);
      if QrPQECodigo.Value = PQE then
      begin
        if (PediVda > 0) or (UsouNFe) then
          AlteraItensImportadosDePediVda();
      end;
    end;
    //
    Result := not Desiste;
  end;
end;

procedure TFmPQE.QueryPrincipalAfterOpen;
begin
  //SubQuery1Reopen;
end;

procedure TFmPQE.DefineONomeDoForm;
begin
end;

procedure TFmPQE.SubQuery1Reopen;
begin
  ReopenPQEIts(QrPQECodigo.Value);

  ReopenEmissT(0);

  ReopenEmissF(0);

end;

procedure TFmPQE.IncluiSubRegistro();
var
  Conta, PQE: Integer;
  Desiste, UsouNFe: Boolean;
begin
{
  if QrPQEIts.State <> dsInactive then
  begin
    Conta := 1;
    if DBCheck.CriaFm(TFmPQEEdit, FmPQEEdit, afmoNegarComAviso) then
    begin
      FmPQEEdit.ImgTipo.SQLType := stIns;
      if FmPQE.QrPQEItsConta.Value > 0 then
         Conta := FmPQE.QrPQEItsConta.Value+1;
      FmPQEEdit.EdCodigo.ValMax := Geral.FF0(QrPQEIts.RecordCount + 1);
      FmPQEEdit.EdCodigo.ValueVariant := Conta;
      FmPQEEdit.ShowModal;
      FmPQEEdit.Destroy;
      //FControle := QrPQEItsControle.Value;
      if QrPQE.State <> dsInactive then
        ReopenPQEIts(QrPQECodigo.Value);
    end;
  end;
}
  if QrPQEIts.State <> dsInactive then
  begin
    Conta := 1;
    if DBCheck.CriaFm(TFmPQEIts, FmPQEIts, afmoNegarComAviso) then
    begin
      FmPQEIts.ImgTipo.SQLType := stIns;
      FmPQEIts.FEmpresa        := FEmpresa;
      FmPQEIts.FData                    := QrPQEDataE.Value;
      FmPQEIts.FRegrFiscal              := QrPQERegrFiscal.Value;
      //
      FmPQEIts.ReopenPQ(ptParcial);
      if FmPQE.QrPQEItsConta.Value > 0 then
         Conta := FmPQE.QrPQEItsConta.Value + 1;
      FmPQEIts.EdConta.ValMax := Geral.FF0(QrPQEIts.RecordCount + 1);
      FmPQEIts.EdConta.ValueVariant := Conta;
      //
      FmPQEIts.EdICMS_CST.ValueVariant   := DModG.QrPrmsEmpNFeUC_ICMS_CST_B.Value;
      FmPQEIts.EdIPI_CST.ValueVariant    := DModG.QrPrmsEmpNFeUC_IPI_CST.Value;
      FmPQEIts.EdPIS_CST.ValueVariant    := DModG.QrPrmsEmpNFeUC_PIS_CST.Value;
      FmPQEIts.EdCOFINS_CST.ValueVariant := DModG.QrPrmsEmpNFeUC_COFINS_CST.Value;
      //
      FmPQEIts.EdUCTextoIPI_CST.Text     := UFinanceiro.CST_IPI_Get(FmPQEIts.EdIPI_CST.Text);

      //
      FmPQEIts.ShowModal;
      FmPQEIts.Destroy;
      //FControle := QrPQEItsControle.Value;

      if QrPQE.State <> dsInactive then
        ReopenPQEIts(QrPQECodigo.Value);
      PQE := FmPQENew.FPQE;
    end;
    //
  end;
  //
  DistribuiCustoDoFrete;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQE.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQE.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQE.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQE.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQE.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmPQE.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQE.BtTravaClick(Sender: TObject);
var
  Status : StatusVP;
  Erro : Double;
  //Atualiza: Boolean;
  //DataE: String;
  Msg: String;
begin
  FTravando := True;
  try
    CalculaDiferencas(apNinguem);
    Erro := Geral.DMV(EdTNota.Text)+
    Geral.DMV(EdTBruto.Text)+
    Geral.DMV(EdTLiq.Text)+
    Geral.DMV(EdPagT.Text)+
    Geral.DMV(EdPagF.Text);

    UnDmkDAC_PF.AbreMySQLQuery0(QrIvBC, Dmod.MyDB, [
    'SELECT ',
    'SUM(prod_vProd) prod_vProd,',
    'SUM(prod_vFrete) prod_vFrete,',
    'SUM(prod_vSeg) prod_vSeg,',
    'SUM(prod_vOutro) prod_vOutro,',
    'SUM(prod_vDesc) prod_vDesc',
    'FROM pqeits',
    'WHERE Codigo=' + Geral.FF0(QrPQECodigo.Value),
    '']);
    UnDmkDAC_PF.AbreMySQLQuery0(QrAvBC, Dmod.MyDB, [
    'SELECT',
    'SUM(ICMSTot_vProd) ICMSTot_vProd,',
    'SUM(ICMSTot_vFrete) ICMSTot_vFrete,',
    'SUM(ICMSTot_vSeg) ICMSTot_vSeg,',
    'SUM(ICMSTot_vOutro) ICMSTot_vOutro,',
    'SUM(ICMSTot_vDesc) ICMSTot_vDesc',
    'FROM pqe',
    'WHERE Codigo=' + Geral.FF0(QrPQECodigo.Value),
    '']);
    //
    Msg := '';
    if QrIvBCprod_vProd.Value <> QrAvBCICMSTot_vProd.Value then
      Msg := Msg + 'A soma dos itens n�o confere com o total no campo ' + 'Valor dos Produtos' + sLineBreak;
    if QrIvBCprod_vFrete.Value <> QrAvBCICMSTot_vFrete.Value then
      Msg := Msg + 'A soma dos itens n�o confere com o total no campo ' + 'Valor do Frete na NF-e' + sLineBreak;
    if QrIvBCprod_vSeg.Value <> QrAvBCICMSTot_vSeg.Value then
      Msg := Msg + 'A soma dos itens n�o confere com o total no campo ' + 'Valor do Seguro' + sLineBreak;
    if QrIvBCprod_vOutro.Value <> QrAvBCICMSTot_vOutro.Value then
      Msg := Msg + 'A soma dos itens n�o confere com o total no campo ' + 'Valor de despesas Acess�rias' + sLineBreak;
    if QrIvBCprod_vDesc.Value <> QrAvBCICMSTot_vDesc.Value then
      Msg := Msg + 'A soma dos itens n�o confere com o total no campo ' + 'Valor do Desconto' + sLineBreak;
    if Msg <> EmptyStr then
      Geral.MB_Aviso(Msg);
    //
    if (Erro > 0.009) or (Erro < -0.009) then
    begin
      Geral.MB_Erro('Erro na confer�ncia!');
      AlteraRegistro(False, False);
      //Status := VerificaPesos(False);
      //if Status <>  vpErro then DistribuiCustoDoFrete(Status);
      DistribuiCustoDoFrete;
      Exit;
    end;
    Status := VerificaPesos(True);
    if Status = vpErro then
    begin
      AlteraRegistro(False, False);
      Status := VerificaPesos(False);
      if Status <> vpErro then
        DistribuiCustoDoFrete;
    end
    else
    begin
      FinalizaEntrada(True);
    end;
    LocCod(QrPQECodigo.Value, QrPQECodigo.Value);
  finally
    FTravando := False;
  end;
end;

procedure TFmPQE.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FTravando := False;
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  //FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, DModG.QrFiliLogFilial.Value);
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  //PainelCabecalho.Height := 180;
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
  //
  CriaOForm;
  FEhPQPedido := 0;// sem pedido
  PCFinCtb.ActivePageIndex := 0;
end;

procedure TFmPQE.AlteraConhecimentodefrete1Click(Sender: TObject);
begin
  MostraFormEfdInnCTsCab(stUpd);
end;

procedure TFmPQE.AlteraDadosEstoque1Click(Sender: TObject);
var
  Cancela:Integer;
  Qry: TmySQLQuery;
begin
  Cancela := 0;
  if UnPQx.ImpedePeloBalanco(QrPQEData.Value) then Exit;
  if PQ_PF.ImpedePeloEstoqueNF_Toda(QrPQECodigo.Value, VAR_FATID_0010) then
    Exit;
    // Verifica estoque negativo
  Dmod.QrUpdW.DataBase := Dmod.MyDB;
  Dmod.QrUpdW.SQL.Clear;
  Dmod.QrUpdW.SQL.Add('UPDATE pqe SET Cancelado = "V", DataAlt=:P0, UserAlt=:P1');
  Dmod.QrUpdW.SQL.Add('WHERE Codigo=''' + Geral.FF0(QrPQECodigo.Value) + '''');
  Dmod.QrUpdW.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, date);
  Dmod.QrUpdW.Params[1].AsInteger := VAR_USUARIO;
  Dmod.QrUpdW.ExecSQL;
  //
  QrPQEIts.First;
  while not QrPQEIts.Eof do
  begin
    UnPQx.AtualizaEstoquePQ(QrPQECI.Value, QrPQEItsInsumo.Value, QrPQEEmpresa.Value, aeNenhum, '');
    //
    QrPQCli.Close;
    QrPQCli.Params[0].AsInteger := QrPQECI.Value;
    QrPQCli.Params[0].AsInteger := QrPQEItsInsumo.Value;
    UnDmkDAC_PF.AbreQuery(QrPQCli, Dmod.MyDB);
    //
    if QrPQCliPeso.Value < 0 then
      Cancela := Cancela + 1
    else if QrPQCliValor.Value < 0 then
      Cancela := Cancela + 1;
    //
    QrPQEIts.Next;
  end;
  if Cancela > 0 then
  begin
    Geral.MB_Erro('Exclus�o cancelada. Existem ' + Geral.FF0(Cancela)
      + ' insumos que ficariam com estoque ou valor financeiro negativo.');
    //
    Dmod.QrUpdW.SQL.Clear;
    Dmod.QrUpdW.SQL.Add('UPDATE pqe SET Cancelado = "F" WHERE Codigo=''' + Geral.FF0(QrPQECodigo.Value) + '''');
    Dmod.QrUpdW.ExecSQL;
    //
    QrPQEIts.First;
    while not QrPQEIts.Eof do
    begin
      UnPQx.AtualizaEstoquePQ(QrPQECI.Value, QrPQEItsInsumo.Value, QrPQEEmpresa.Value, aeNenhum, '');
      QrPQEIts.Next;
    end;
    AtualizaEntradaPedido;
    Exit;
  end;
  AlteraRegistro(True, True);
  DistribuiCustoDoFrete;
end;

procedure TFmPQE.AlteraDadosFiscais1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeCabA_0000, FmNFeCabA_0000, afmoNegarComAviso) then
  begin
    FmNFeCabA_0000.FForm := 'FmPQE';
    if FmNFeCabA_0000.LocCod(QrNFeCabAIDCtrl.Value, QrNFeCabAIDCtrl.Value) then
    begin
      if QrPQECI.Value = 0 then
        Geral.MB_Aviso(
          'Para inclus�o de NF-e � necess�rio informar uma empresa!')
      else begin
        UMyMod.ConfigPanelInsUpd(stUpd, FmNFeCabA_0000, FmNFeCabA_0000.PainelEdit,
          FmNFeCabA_0000.QrNFeCabA, [FmNFeCabA_0000.PainelDados], [FmNFeCabA_0000.PainelEdita],
          nil, FmNFeCabA_0000.ImgTipo, 'nfecaba');
      end;
      FmNFeCabA_0000.ShowModal;
      FmNFeCabA_0000.Destroy;
    end else begin
      Geral.MB_Aviso('N�o foi poss�vel localizar a NF!');
      FmNFeCabA_0000.Destroy;
    end;
    //
    ReopenNFeCabA();
  end;
end;

procedure TFmPQE.AlteraDocumento1Click(Sender: TObject);
begin
  MostraFormEfdInnNFsCab(stUpd);
end;

procedure TFmPQE.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_TAB) then GridT.SetFocus;
  if (Key=VK_TAB) and (Shift = [ssShift]) then DBGrid1.SetFocus;
  if ImgTipo.SQLType <> stLok then
  begin
    if (key=VK_DELETE) and (Shift = [ssCtrl]) then ExcluiSubRegistro;
  end;
end;

procedure TFmPQE.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPQECodigo.Value, LaRegistro.Caption);
end;

procedure TFmPQE.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmPQE.SbNomeClick(Sender: TObject);
begin
  PerguntaAQuery;
end;

procedure TFmPQE.SbNovoClick(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Ser� atualizado o estoque virtual do item de NF selecionado!' + sLineBreak +
  'Deseja continuar?') = ID_YES then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    //
    PQ_PF.AtualizaSdoPQx(Dmod.QrAux, QrPQEItsCodigo.Value,
    QrPQEItsControle.Value, VAR_FATID_0010);
    //
    PQ_PF.VerificaEquiparacaoEstoque(True);
  end;
end;

procedure TFmPQE.SbQueryClick(Sender: TObject);
begin
  PerguntaAQuery;
end;

procedure TFmPQE.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQE.GridFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_TAB) then BtTrava.SetFocus;
  if (Key=VK_TAB) and (Shift = [ssShift]) then GridT.SetFocus;
  if ImgTipo.SQLType <> stLok then
  begin
    if (key=VK_DELETE) and (Shift = [ssCtrl]) then ExcluiItemDuplicataF;
  end;
end;

procedure TFmPQE.GridTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_TAB) then GridF.SetFocus;
  if (Key=VK_TAB) and (Shift = [ssShift]) then DBGrid1.SetFocus;
  if ImgTipo.SQLType <> stLok then
  begin
    if (key=VK_DELETE) and (Shift = [ssCtrl]) then ExcluiItemDuplicataT;
  end;

end;

procedure TFmPQE.QrPQEAfterOpen(DataSet: TDataSet);
begin
  GOTOy.BtEnabled(QrPQECodigo.Value, False);
  //QueryPrincipalAfterOpen;
end;

procedure TFmPQE.QrPQEItsAfterScroll(DataSet: TDataSet);
begin
  QrNFeItsI.Close;
  QrNFeItsI.SQL.Clear;
  QrNFeItsI.SQL.Add('SELECT *');
  QrNFeItsI.SQL.Add('FROM nfeitsi');
  QrNFeItsI.SQL.Add('WHERE FatID in (:P0, :P1)');
  QrNFeItsI.SQL.Add('AND FatNum=:P2');
  QrNFeItsI.SQL.Add('AND Empresa=:P3');
  QrNFeItsI.SQL.Add('AND MeuID=:P4');
  QrNFeItsI.SQL.Add('');
  QrNFeItsI.Params[00].AsInteger := VAR_FATID_0051;
  QrNFeItsI.Params[01].AsInteger := VAR_FATID_0151;
  QrNFeItsI.Params[02].AsInteger := QrPQECodigo.Value;
  QrNFeItsI.Params[03].AsInteger := QrPQECI.Value;
  QrNFeItsI.Params[04].AsInteger := QrPQEItsControle.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeItsI, Dmod.MyDB);
end;

procedure TFmPQE.QrPQEItsCalcFields(DataSet: TDataSet);
//var
//  RIPI : Double;
begin
//  if QrPQEItsIPI.Value > QrPQEItsRIPI.Value  then
//  RIPI := 1 + ((QrPQEItsIPI.Value - QrPQEItsRIPI.Value)/100) else RIPI := 1;
  QrPQEItsCUSTOITEM.Value := QrPQEItsValorItem.Value*(1+(QrPQEItsIPI.Value/100));

  QrPQEItsTOTALKGBRUTO.Value := QrPQEItsVolumes.Value * QrPQEItsPesoVB.Value;

  if (QrPQEItsVolumes.Value * QrPQEItsPesoVL.Value) > 0 then
   QrPQEItsVALORKG.Value := QrPQEItsCUSTOITEM.Value /
  (QrPQEItsVolumes.Value * QrPQEItsPesoVL.Value)
  else QrPQEItsVALORKG.Value := 0;

  QrPQEItsPRECOKG.Value := QrPQEItsVALORKG.Value;
  //(QrPQEItsVALORKG.Value /
  //(1 + (QrPQEItsIPI.Value / 100))) * (1 - (QrPQEICMS.Value/100));

  if QrPQEItsTotalPeso.Value > 0 then QrPQEItsCUSTOKG.Value :=
     QrPQEItsTotalCusto.Value / QrPQEItsTotalPeso.Value
  else
     QrPQEItsCUSTOKG.Value := 0;
end;

procedure TFmPQE.QrEmissTCalcFields(DataSet: TDataSet);
begin
  QrEmissTNOMESIT.Value := UFinanceiro.NomeSitLancto(QrEmissTSit.Value,
    QrEmissTTipo.Value, 0(*QrEmissTPrazo.Value*), QrEmissTVencimento.Value,
    0(*QrEmissTReparcel.Value*));
end;

procedure TFmPQE.QrEfdInnNFsCabAfterScroll(DataSet: TDataSet);
begin
  ReopenEFdInnNFsIts(0);
end;

procedure TFmPQE.QrEfdInnNFsCabBeforeClose(DataSet: TDataSet);
begin
  QrEFdInnNFsIts.Close;
end;

procedure TFmPQE.QrEmissFCalcFields(DataSet: TDataSet);
begin
  QrEmissFNOMESIT.Value := UFinanceiro.NomeSitLancto(QrEmissFSit.Value,
    QrEmissFTipo.Value, 0(*QrEmissFPrazo.Value*), QrEmissFVencimento.Value,
    0(*QrEmissFReparcel.Value*));
end;

procedure TFmPQE.BtExportaClick(Sender: TObject);
var
  Arqe, ArqI, ArqT, ArqF, Texto: String;
begin
  try
    ArqE := 'C:\WBExporta\PQE'+FormatFloat('00000000',QrPQECodigo.Value)+'.WBE';
    ArqI := 'C:\WBExporta\PQEIts'+FormatFloat('00000000',QrPQECodigo.Value)+'.WBE';
    ArqT := 'C:\WBExporta\PQET'+FormatFloat('00000000',QrPQECodigo.Value)+'.WBE';
    ArqF := 'C:\WBExporta\PQEF'+FormatFloat('00000000',QrPQECodigo.Value)+'.WBE';
    if FileExists(ArqE) then
    begin
      Texto := 'O arquivo ' + ExtractFileName(ArqE) +
      ' j� exite. Deseja substitu�-lo?';
      if Geral.MB_Pergunta(Texto) = ID_YES then
      begin
         DeleteFile(ArqE);
         DeleteFile(ArqI);
         DeleteFile(ArqT);
         DeleteFile(ArqF);
      end
      else begin
        Geral.MB_Aviso('Exporta��o cancelada');
        Exit;
      end;
    end;
    QrExPQE.Params[0].AsString := ArqE;
    QrExPQE.Params[1].AsInteger := QrPQECodigo.Value;
    QrExPQE.ExecSQL;
    //
    QrExPQI.Params[0].AsString := ArqI;
    QrExPQI.Params[1].AsInteger := QrPQECodigo.Value;
    QrExPQI.ExecSQL;
    //
    (*
    QrExPQT.Params[0].AsString := ArqT;
    QrExPQT.Params[1].AsInteger := QrPQECodigo.Value;
    QrExPQT.ExecSQL;
    *)
    UnDmkDAC_PF.ExecutaMySQLQuery0(QrExPQT, Dmod.MyDB, [
    'SELECT la.*, ca.Nome NOMECARTEIRA ',
    'INTO OUTFILE "'  + ArqT + '" ',
    'FIELDS ',
    'TERMINATED BY '#39','#39' ',
    'FROM ' + FTabLctA + ' la, Carteiras ca ',
    'WHERE ca.Codigo=la.Carteira ',
    'AND FatID=' + Geral.FF0(VAR_FATID_1002),
    'AND FatNum=' + Geral.FF0(QrPQECodigo.Value),
    '']);
    (*
    QrExPQF.Params[0].AsString := ArqF;
    QrExPQF.Params[1].AsInteger := QrPQECodigo.Value;
    QrExPQF.ExecSQL;
    *)
    UnDmkDAC_PF.ExecutaMySQLQuery0(QrExPQT, Dmod.MyDB, [
    'SELECT la.*, ca.Nome NOMECARTEIRA ',
    'INTO OUTFILE "'  + ArqF + '" ',
    'FIELDS ',
    'TERMINATED BY '#39','#39' ',
    'FROM ' + FTabLctA + ' la, Carteiras ca ',
    'WHERE ca.Codigo=la.Carteira ',
    'AND FatID=' + Geral.FF0(VAR_FATID_1001),
    'AND FatNum=' + Geral.FF0(QrPQECodigo.Value),
    '']);
    //
    Geral.MB_Info('Exporta��o realizada com sucesso em: ' + sLineBreak + ArqF);
  except
    Geral.MB_Erro('Ocorreu um ou mais erros na exporta��o.');
  end;
end;

procedure TFmPQE.BtNFe2Click(Sender: TObject);
begin
  PCFinCtb.ActivePageIndex := 1;
  MyObjects.MostraPopupDeBotao(PMFiscal, TBitBtn(Sender));
end;

procedure TFmPQE.QrPQEAfterScroll(DataSet: TDataSet);
var
  Filial: Integer;
begin
  (* ini 2023-20-23
  if QrPQECodCliInt.Value <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrPQECodCliInt.Value)
  else
    FTabLctA := sTabLctErr;
  //*)
  if QrPQEEmpresa.Value <> 0 then
  begin
    Filial := DModG.ObtemFilialDeEntidade(QrPQEEmpresa.Value);
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial)
  end else
    FTabLctA := sTabLctErr;
  // fim 2023-20-23
  SubQuery1Reopen;
  ReopenNFeCabA();
  ReopenretImpost();
  ReopenEFdInnNFsCab(0);
  ReopenEFdInnCTsCab(0);
(*  if QrPQECancelado.Value = 'V' then
  begin
    PainelCabecalho.Height := 180;
    BtExclui.Enabled := False;
    BtAltera.Visible := True;
  end else begin
    PainelCabecalho.Height := 140;
    BtExclui.Enabled := True;
    if GOTOy.Registros(QrPQEIts) = 0
    then BtAltera.Visible := True
    else BtAltera.Visible := False;
  end;*)
end;

procedure TFmPQE.QrPQEBeforeClose(DataSet: TDataSet);
begin
  QrPQEIts.Close;
  QrEmissT.Close;
  QrEmissF.Close;
  QrRetImpost.Close;
  QrEFdInnNFsCab.Close;
  QrEFdInnCTsCab.Close;
end;

procedure TFmPQE.Entradasempedido1Click(Sender: TObject);
var
  Desiste, UsouNFe: Boolean;
  PQE: Integer;
begin
  FEhPQPedido := 0;//zera aviso de  entrada de pedido
  //
  if InsereRegCondicionado1(0) then
  begin
    if QrPQEIts.RecordCount = 0 then
      IncluiSubRegistro;
    DistribuiCustoDoFrete;
  end;
end;

procedure TFmPQE.EPI1Click(Sender: TObject);
begin
  UnPQx.MostraFormPQI(0);
end;

procedure TFmPQE.Entradaparcialdepedido1Click(Sender: TObject);
begin
  FEhPQPedido := 1;
  EscolhePedido;
end;

procedure TFmPQE.EntradaporPedidoXNFebaixada1Click(Sender: TObject);
begin
  FEhPQPedido := 2;
  EscolhePedido3;
end;

procedure TFmPQE.EntradaporXML2Click(Sender: TObject);
  function ExcluirEntrada(FatID, FatNum, Empresa: Integer; Avisa: Boolean): Boolean;
  begin
    if Avisa then
    Geral.MB_Aviso('Ocorreu um erro na importa��o do XML.' + sLineBreak +
    'Os dados da entrada ser�o exclu�dos, pois s�o parciais!');
    DmNFe_0000.ExcluiNfe(0(*Status*), FatID, FatNum, Empresa, True, True);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM pqeits WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := FatNum;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM pqe WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := FatNum;
    Dmod.QrUpd.ExecSQL;
    //
    Result := True;
    //
    LocCod(FatNum, FatNum);
    //
    Screen.Cursor := crDefault;
  end;
var
  cab_Pedido, cab_TipoNF, cab_qVol, FatID, FatNum, Empresa, IDCtrl, Terceiro,
  Transporta, cab_Conhecimento, cab_NF, cab_modNF, cab_Serie: Integer;
  cab_Juros, cab_RICMS, cab_RICMSF, ValFrete, cab_PesoB, cab_PesoL, SumPesoLIts,
  cab_ICMS, cab_ValProd, cab_Frete, cab_Seguro, cab_Desconto, cab_IPI, cab_PIS,
  cab_COFINS, cab_Outros, cab_ValorNF: Double;
  cab_Data, cab_DataE, cab_DataS, cab_Cancelado, cab_refNFe: String;
  //
  Conta, Controle, Insumo, its_Volumes: Integer;
  its_PesoVB, its_PesoVL, its_ValorItem, its_RIPI, its_CFin, its_Seguro,
  its_Frete, its_Desconto, its_TotalCusto, its_TotalPeso, its_IPI, its_ICMS,
  its_RICMS: Double;
  DataFiscal: TDateTime;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  FatID := VAR_FATID_0051;
  FatNum := UMyMod.BuscaEmLivreY_Def('pqe', 'Codigo', stIns, 0);
  //
  cab_PesoB := 0;
  cab_PesoL := 0;
  if UnNFeImporta_0400.ImportaDadosNFeDeXML(tpemiTerceiros, tpnfEntrada,
  MeWarn, MeInfo, MeWarn, '', FmPQE, FatID, FatNum,
  Empresa, IDCtrl, Terceiro, Transporta, cab_qVol, cab_PesoB, cab_PesoL,
  SumPesoLIts, DataFiscal, FTabLcta) then
  begin
    //cab_Data          := FormatDateTime('yyyy-mm-dd', DataFiscal); // Entrada
    cab_Data          := FormatDateTime('yyyy-mm-dd', DModG.ObtemAgora()); // Entrada
    cab_TipoNF        := 1; // Nota Fiscal Eletr�nica
    //
    cab_Pedido        := 0;
    cab_Juros         := 0;
    cab_RICMS         := 0;
    cab_RICMSF        := 0;
    cab_Conhecimento  := 0;
    cab_Cancelado     := 'V';
    QrA.Close;
    QrA.Params[00].AsInteger := FatID;
    QrA.Params[01].AsInteger := FatNum;
    QrA.Params[02].AsInteger := Empresa;
    UnDmkDAC_PF.AbreQuery(QrA, Dmod.MyDB);
    //
    cab_NF      := QrAide_nNF.Value;
    cab_dataS   := Geral.FDT(QrAide_dSaiEnt.Value, 1);
    cab_dataE   := Geral.FDT(QrAide_dEmi.Value, 1);
    cab_refNFe  := QrAId.Value;
    cab_modNF   := QrAide_mod.Value;
    cab_Serie   := QrAide_serie.Value;
    cab_ICMS    := QrAICMSTot_vICMS.Value;
    cab_ValProd := QrAICMSTot_vProd.Value;
    cab_Frete   := QrAICMSTot_vFrete.Value;
    cab_Seguro  := QrAICMSTot_vSeg.Value;
    cab_Desconto:= QrAICMSTot_vDesc.Value;
    cab_IPI     := QrAICMSTot_vIPI.Value;
    cab_PIS     := QrAICMSTot_vPIS.Value;
    cab_COFINS  := QrAICMSTot_vCOFINS.Value;
    cab_Outros  := QrAICMSTot_vOutro.Value;
    cab_ValorNF := QrAICMSTot_vNF.Value;
    //
    if cab_PesoL < SumPesoLIts then
      cab_PesoL := SumPesoLIts;
    //
    if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqe', False, [
    'Data', 'IQ', 'CI', 'Empresa',
    'Transportadora', 'NF', 'Frete',
    'PesoB', 'PesoL', 'ValorNF',
    'RICMS', 'RICMSF', 'Conhecimento',
    'Pedido', 'DataE', 'Juros',
    'ICMS', 'Cancelado', 'TipoNF',
    'refNFe', 'modNF', 'Serie',
    'ValProd', 'Volumes', 'IPI',
    'PIS', 'COFINS', 'Seguro',
    'Desconto', 'Outros', 'DataS'], [
    'Codigo'], [
    cab_Data, Terceiro, Empresa, VAR_LIB_EMPRESA_SEL,
    Transporta, cab_NF, cab_Frete,
    cab_PesoB, cab_PesoL, cab_ValorNF,
    cab_RICMS, cab_RICMSF, cab_Conhecimento,
    cab_Pedido, cab_DataE, cab_Juros,
    cab_ICMS, cab_Cancelado, cab_TipoNF,
    cab_refNFe, cab_modNF, cab_Serie,
    cab_ValProd, cab_qVol, cab_IPI,
    cab_PIS, cab_COFINS, cab_Seguro,
    cab_Desconto, cab_Outros, cab_DataS], [
    FatNum], True) then
    begin
      ExcluirEntrada(FatID, FatNum, Empresa, False);
      Exit;
    end;

    // ITENS

    QrI.Close;
    QrI.Params[00].AsInteger := FatID;
    QrI.Params[01].AsInteger := FatNum;
    QrI.Params[02].AsInteger := Empresa;
    UnDmkDAC_PF.AbreQuery(QrI, Dmod.MyDB);
    //
    QrI.First;
    while not QrI.Eof do
    begin
      QrN.Close;
      QrN.Params[00].AsInteger := FatID;
      QrN.Params[01].AsInteger := FatNum;
      QrN.Params[02].AsInteger := Empresa;
      QrN.Params[03].AsInteger := QrInItem.Value;
      UnDmkDAC_PF.AbreQuery(QrN, Dmod.MyDB);
      //
      QrO.Close;
      QrO.Params[00].AsInteger := FatID;
      QrO.Params[01].AsInteger := FatNum;
      QrO.Params[02].AsInteger := Empresa;
      QrO.Params[03].AsInteger := QrInItem.Value;
      UnDmkDAC_PF.AbreQuery(QrO, Dmod.MyDB);
      //
      QrQ.Close;
      QrQ.Params[00].AsInteger := FatID;
      QrQ.Params[01].AsInteger := FatNum;
      QrQ.Params[02].AsInteger := Empresa;
      QrQ.Params[03].AsInteger := QrInItem.Value;
      UnDmkDAC_PF.AbreQuery(QrQ, Dmod.MyDB);
      //
      QrR.Close;
      QrR.Params[00].AsInteger := FatID;
      QrR.Params[01].AsInteger := FatNum;
      QrR.Params[02].AsInteger := Empresa;
      QrR.Params[03].AsInteger := QrInItem.Value;
      UnDmkDAC_PF.AbreQuery(QrR, Dmod.MyDB);
      //
      QrS.Close;
      QrS.Params[00].AsInteger := FatID;
      QrS.Params[01].AsInteger := FatNum;
      QrS.Params[02].AsInteger := Empresa;
      QrS.Params[03].AsInteger := QrInItem.Value;
      UnDmkDAC_PF.AbreQuery(QrS, Dmod.MyDB);
      //
      QrT.Close;
      QrT.Params[00].AsInteger := FatID;
      QrT.Params[01].AsInteger := FatNum;
      QrT.Params[02].AsInteger := Empresa;
      QrT.Params[03].AsInteger := QrInItem.Value;
      UnDmkDAC_PF.AbreQuery(QrT, Dmod.MyDB);
      //
      Conta          := QrInItem.Value;
      Insumo         := QrINivel1.Value;
      its_Volumes    := 1;
      its_PesoVB     := 0;
      its_PesoVL     := QrIprod_qCom.Value;
      its_ValorItem  := QrIprod_vProd.Value;
      its_IPI        := 0;
      its_RIPI       := 0;
      its_ICMS       := 0;
      its_RICMS      := 0;
      its_CFin       := 0;
      //its_Seguro     := QrIprod_vSeg.Value;
      //its_Frete      := QrIprod_vFrete.Value;
      its_Desconto   := QrIprod_vDesc.Value;
      its_TotalCusto := its_ValorItem - its_Desconto + QrOIPI_vIPI.Value;
      its_TotalPeso  := its_PesoVL;
      //
      Controle := UMyMod.BuscaEmLivreY_Def('pqeits', 'Controle', stIns, 0);
      if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqeits', False, [
      'Codigo', 'Conta', 'Insumo',
      'Volumes', 'PesoVB', 'PesoVL',
      'ValorItem', 'IPI', 'RIPI',
      'CFin', 'ICMS', 'RICMS',
      'TotalCusto', 'TotalPeso', 'prod_cProd',
      'prod_cEAN', 'prod_xProd', 'prod_NCM',
      'prod_EX_TIPI', 'prod_genero', 'prod_CFOP',
      'prod_uCom', 'prod_qCom', 'prod_vUnCom',
      'prod_vProd', 'prod_cEANTrib', 'prod_uTrib',
      'prod_qTrib', 'prod_vUnTrib', 'prod_vFrete',
      'prod_vSeg', 'prod_vDesc', 'ICMS_Orig',
      'ICMS_CST', 'ICMS_modBC', 'ICMS_pRedBC',
      'ICMS_vBC', 'ICMS_pICMS', 'ICMS_vICMS',
      'ICMS_modBCST', 'ICMS_pMVAST', 'ICMS_pRedBCST',
      'ICMS_vBCST', 'ICMS_pICMSST', 'ICMS_vICMSST',
      'IPI_cEnq', 'IPI_CST', 'IPI_vBC',
      'IPI_qUnid', 'IPI_vUnid', 'IPI_pIPI',
      'IPI_vIPI', 'PIS_CST', 'PIS_vBC',
      'PIS_pPIS', 'PIS_vPIS', 'PIS_qBCProd',
      'PIS_vAliqProd', 'PISST_vBC', 'PISST_pPIS',
      'PISST_qBCProd', 'PISST_vAliqProd', 'PISST_vPIS',
      'COFINS_CST', 'COFINS_vBC', 'COFINS_pCOFINS',
      'COFINS_qBCProd', 'COFINS_vAliqProd', 'COFINS_vCOFINS',
      'COFINSST_vBC', 'COFINSST_pCOFINS', 'COFINSST_qBCProd',
      'COFINSST_vAliqProd', 'COFINSST_vCOFINS'], [
      'Controle'], [
      FatNum, Conta, Insumo,
      its_Volumes, its_PesoVB, its_PesoVL,
      its_ValorItem, its_IPI, its_RIPI,
      its_CFin, its_ICMS, its_RICMS,
      its_TotalCusto, its_TotalPeso, QrIprod_cProd.Value,
      QrIprod_cEAN.Value, QrIprod_xProd.Value, QrIprod_NCM.Value,
      QrIprod_EXTIPI.Value, QrIprod_genero.Value, QrIprod_CFOP.Value,
      QrIprod_uCom.Value, QrIprod_qCom.Value, QrIprod_vUnCom.Value,
      QrIprod_vProd.Value, QrIprod_cEANTrib.Value, QrIprod_uTrib.Value,
      QrIprod_qTrib.Value, QrIprod_vUnTrib.Value, QrIprod_vFrete.Value,
      QrIprod_vSeg.Value, QrIprod_vDesc.Value, QrNICMS_Orig.Value,
      QrNICMS_CST.Value, QrNICMS_modBC.Value, QrNICMS_pRedBC.Value,
      QrNICMS_vBC.Value, QrNICMS_pICMS.Value, QrNICMS_vICMS.Value,
      QrNICMS_modBCST.Value, QrNICMS_pMVAST.Value, QrNICMS_pRedBCST.Value,
      QrNICMS_vBCST.Value, QrNICMS_pICMSST.Value, QrNICMS_vICMSST.Value,
      QrOIPI_cEnq.Value, QrOIPI_CST.Value, QrOIPI_vBC.Value,
      QrOIPI_qUnid.Value, QrOIPI_vUnid.Value, QrOIPI_pIPI.Value,
      QrOIPI_vIPI.Value, QrQPIS_CST.Value, QrQPIS_vBC.Value,
      QrQPIS_pPIS.Value, QrQPIS_vPIS.Value, QrQPIS_qBCProd.Value,
      QrQPIS_vAliqProd.Value, QrRPISST_vBC.Value, QrRPISST_pPIS.Value,
      QrRPISST_qBCProd.Value, QrRPISST_vAliqProd.Value, QrRPISST_vPIS.Value,
      QrSCOFINS_CST.Value, QrSCOFINS_vBC.Value, QrSCOFINS_pCOFINS.Value,
      QrSCOFINS_qBCProd.Value, QrSCOFINS_vAliqProd.Value, QrSCOFINS_vCOFINS.Value,
      QrTCOFINSST_vBC.Value, QrTCOFINSST_pCOFINS.Value, QrTCOFINSST_qBCProd.Value,
      QrTCOFINSST_vAliqProd.Value, QrTCOFINSST_vCOFINS.Value], [
      Controle], True) then
      begin
        ExcluirEntrada(FatID, FatNum, Empresa, True);
        Exit;
      end else begin
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeitsi', False, [
        'MeuID'], ['FatID', 'FatNum', 'Empresa', 'nItem'], [
        Controle], [FatID, FatNum, Empresa, QrInItem.Value], True) then
        begin
          ExcluirEntrada(FatID, FatNum, Empresa, True);
          Exit;
        end;
      end;
      //
      QrI.Next;
    end;
  end;
  LocCod(FatNum, FatNum);
  FinalizaEntrada(False);
  AlteraDadosEstoque1Click(Self);
end;

procedure TFmPQE.EntradaporXMLmodelo55NFenovo1Click(Sender: TObject);
var
  PQE: Integer;
begin
  if DBCheck.CriaFm(TFmPQEXml, FmPQEXml, afmoNegarComAviso) then
  begin
    PQE := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'PQE', 'PQE', 'Codigo');
    FmPQEXml.ImgTipo.SQLType := stIns;
    FmPQEXml.EdCodigo.Text   := Geral.FF0(PQE);
    FmPQEXml.FFatID := VAR_FATID_0051;
    DModG.SelecionaEmpresaSeUnica(FmPQEXml.EdEmpresa, FmPQEXml.CBEmpresa);
    FmPQEXml.TPEntrada.Date  := Trunc(DmodG.ObtemAgora());
    //
    if UnNFeImporta_0400.ImportaDadosNFeDeXML(tpemiTerceiros, tpnfEntrada,
    FmPQEXml.MeWarn, FmPQEXml.MeInfo, FmPQEXml.MeWarn, '', FmPQE, FmPQEXml.FFatID,
    FmPQEXml.FFatNum, FmPQEXml.FEmpresa, FmPQEXml.FIDCtrl, FmPQEXml.FTerceiro,
    FmPQEXml.FTransporta, FmPQEXml.Fcab_qVol, FmPQEXml.Fcab_PesoB,
    FmPQEXml.Fcab_PesoL, FmPQEXml.FSumPesoLIts, FmPQEXml.FDataFiscal, FTabLcta) then
    begin
      FmPQEXml.ReopenA_e_ConfiguraRegrFiscal();
      FmPQEXml.ShowModal;
    end else
      Geral.MB_Aviso('Inclus�o por XML abortada!');
    FmPQEXml.Destroy;
  end;
end;

procedure TFmPQE.Encerramentodepedido1Click(Sender: TObject);
begin
  FEhPQPedido := 2;
  EscolhePedido2;
end;

function TFmPQE.FinalizaEntrada(Pergunta: Boolean): Boolean;
const
  HowLoad = 0;
var
  Atualiza: Boolean;
  DataE: String;
  //
  DataX, DtCorrApo, xLote, dFab, dVal: String;
  CliOrig, CliDest, Insumo, OrigemCodi, OrigemCtrl, Tipo, Empresa,
  CustoPadraoAtz, ide_serie, ide_nNF: Integer;
  Peso, Valor: Double;
  Qry: TmySQLQuery;
  Controle: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEIN, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM efdinnnfscab',
  'WHERE MovFatID=' + Geral.FF0(FEFDInnNFSMainFatID),
  'AND MovFatNum=' + Geral.FF0(QrPQECodigo.Value),
  'AND MovimCod=' + Geral.FF0(CO_MovimCod_ZERO),
  'AND Empresa=' + Geral.FF0(QrPQEEmpresa.Value),
  'AND SqLinked=' + Geral.FF0(QrPQESqLinked.Value),
  '']);
  //
  Controle := QrEINControle.Value;
  if Controle > 0 then
    DmNFe_0000.AtualizaTotaisEfdInnNFs(Controle);
  //  Result   := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    QrAnt.Close;
    QrAnt.Params[0].AsInteger := QrPQECodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrAnt, Dmod.MyDB);
(*
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM pqx WHERE Tipo=' + Geral.FF0(VAR_FATID_0010) + ' AND OrigemCodi=:P0');
    Dmod.QrUpdM.Params[0].AsInteger := QrPQECodigo.Value;
    Dmod.QrUpdM.ExecSQL;
*)
    PQ_PF.ExcluiPQx_Mul(nil, QrPQECodigo.Value, VAR_FATID_0010, PB1, False);
    while not QrAnt.Eof do
    begin
      // MsgNenhum pois ir� incluir depois !
      UnPQx.AtualizaEstoquePQ(QrPQECI.Value, QrAntInsumo.Value, QrPQEEmpresa.Value, aeNenhum, CO_VAZIO);
      QrAnt.Next;
    end;
    // 2014-11-07
    DistribuiCustoDoFrete();
    ReopenPQEIts(QrPQECodigo.Value);
    // 2014-11-07
    QrPQEIts.DisableControls;
    QrPQEIts.First;
  (*
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('INSERT INTO p q x SET Tipo=' + Geral.FF0(VAR_FATID_0010) + ', DataX=:P0, ');
    Dmod.QrUpdM.SQL.Add('CliOrig=:P1, CliDest=:P2, Insumo=:P3, Peso=:P4, ');
    Dmod.QrUpdM.SQL.Add('Valor=:P5, OrigemCodi=:P6, OrigemCtrl=:P7');
    DataE := Geral.FDT(QrPQEData.Value, 1);
    while not QrPQEIts.Eof do
    begin
      Dmod.QrUpdM.Params[00].AsString  := DataE;
      Dmod.QrUpdM.Params[01].AsInteger := QrPQECI.Value;
      Dmod.QrUpdM.Params[02].AsInteger := QrPQECI.Value;
      Dmod.QrUpdM.Params[03].AsInteger := QrPQEItsInsumo.Value;
      Dmod.QrUpdM.Params[04].AsFloat   := QrPQEItsTotalPeso.Value;
      Dmod.QrUpdM.Params[05].AsFloat   := QrPQEItsTotalCusto.Value;
      Dmod.QrUpdM.Params[06].AsInteger := QrPQECodigo.Value;
      Dmod.QrUpdM.Params[07].AsInteger := QrPQEItsControle.Value;
      //Dmod.QrUpdM.Params[08].AsInteger := 10;
      Dmod.QrUpdM.ExecSQL;
      QrPQEIts.Next;
    end;
  *)
    while not QrPQEIts.Eof do
    begin
      DataX      := Geral.FDT(QrPQEData.Value, 1);
      Empresa    := QrPQEEmpresa.Value;
      CliOrig    := QrPQECI.Value;
      CliDest    := QrPQECI.Value;
      Insumo     := QrPQEItsInsumo.Value;
      Peso       := QrPQEItsTotalPeso.Value;
      Valor      := QrPQEItsTotalCusto.Value;
      OrigemCodi := QrPQECodigo.Value;
      OrigemCtrl := QrPQEItsControle.Value;
      Tipo       := VAR_FATID_0010;
      DtCorrApo  := Geral.FDT(QrPQEItsDtCorrApo.Value, 1);
      dFab       := Geral.FDT(QrPQEItsdFab.Value, 1);
      dVal       := Geral.FDT(QrPQEItsdFab.Value, 1);
      xLote      := QrPQEItsxLote.Value;
      ide_serie  := QrPQESerie.Value;
      ide_nNF    := QrPQENF.Value;
      //
{
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'p q x', False, [
      'DataX', 'CliOrig', 'CliDest',
      'Insumo', 'Peso', 'Valor'(*,
      'Retorno', 'StqMovIts', 'RetQtd',
      'HowLoad', 'StqCenLoc', 'SdoPeso',
      'SdoValr', 'AcePeso', 'AceValr'*)], [
      'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
      DataX, CliOrig, CliDest,
      Insumo, Peso, Valor(*,
      Retorno, StqMovIts, RetQtd,
      HowLoad, StqCenLoc, SdoPeso,
      SdoValr, AcePeso, AceValr*)], [
      OrigemCodi, OrigemCtrl, Tipo], False) then
      begin
        PQ_PF.AtualizaSdoPQx(Qry, OrigemCodi, OrigemCtrl, Tipo);
      end;
}
      PQ_PF.InserePQx_Inn(Qry, DataX, CliOrig, CliDest, Insumo, Peso, Valor,
      HowLoad, OrigemCodi, OrigemCtrl, Tipo, DtCorrApo, Empresa,
      ide_serie, ide_nNF, xLote, dFab, dVal);
      //
      QrPQEIts.Next;
    end;
    QrPQEIts.First;
    //
    while not QrPQEIts.Eof do
    begin
      CustoPadraoAtz := Trunc(QrPQEItsCustoPadraoAtz.Value);
      //
      case CustoPadraoAtz of
        1: //Atualiza
          Atualiza := True;
        2: //N�o atualiza
          Atualiza := False;
        else
        begin
          if (*(ImgTipo.SQLType = stIns) and*) Pergunta then
          begin
            Atualiza := Geral.MB_Pergunta(
            'Deseja atualizar o(s) pre�o(s) do(s) insumo(s)?') = ID_YES;
          end else
            Atualiza := False;
        end;
      end;
      //
      if Atualiza then
        Atualiza := BDC_ATUALIZA_PRECO_PQ;
      //
      if Atualiza then
        UnPQx.AtualizaDadosPQ(QrPQECI.Value, QrPQEItsInsumo.Value,
          QrPQEItsMoeda.Value, QrPQEItsVALORKG.Value, QrPQEItsCotacao.Value);
  (*    Tirei 2014-11-07
      UnPQx.AtualizaEstoquePQ(QrPQECI.Value, QrPQEItsInsumo.Value, aeMsg, CO_VAZIO);
  *)
      AtualizaEntradaPedidoIts(QrPQEItsInsumo.Value);
      //
      /// Pre�o m�dio e �ltimo pre�o GraGruX
      QrGraGruX.Close;
      QrGraGruX.Params[0].AsInteger := QrPQEItsInsumo.Value;
      UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
      while not QrGraGruX.Eof do
      begin
        DmodG.AtualizaPrecosGraGruVal2(QrGraGruXControle.Value, QrPQECI.Value);
        QrGraGruX.Next;
      end;
      /// FIM Pre�o m�dio e �ltimo pre�o GraGruX
      //
      QrPQEIts.Next;
    end;
    QrPQEIts.EnableControls;
    AtualizaEntradaPedido;
    Dmod.QrUpdW.DataBase := Dmod.MyDB;
    Dmod.QrUpdW.SQL.Clear;
    Dmod.QrUpdW.SQL.Add('UPDATE pqe SET Cancelado = "F" WHERE Codigo=' + Geral.FF0(QrPQECodigo.Value));
    Dmod.QrUpdW.ExecSQL;

    // 2014-11-07
    DistribuiCustoDoFrete();
    QrPQEIts.First;
    while not QrPQEIts.Eof do
    begin
      UnPQx.AtualizaEstoquePQ(QrPQECI.Value, QrPQEItsInsumo.Value, QrPQEEmpresa.Value, aeMsg, CO_VAZIO);
      //
      QrPQEIts.Next;
    end;
    // FIM 2014-11-07
    TravaOForm;
    //
    Result := True;
    PQ_PF.VerificaEquiparacaoEstoque(True);
  finally
    Qry.Free;
  end;
end;

procedure TFmPQE.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQE.BtQuitaClick(Sender: TObject);
begin
  PCFinCtb.ActivePageIndex := 0;
  IncluiItemDuplicataTeF;
end;

procedure TFmPQE.Itemdeinsumo1Click(Sender: TObject);
begin
  IncluiSubRegistro;
  DistribuiCustoDoFrete;
end;

procedure TFmPQE.Manuteno1Click(Sender: TObject);
begin
  UnPQx.MostraFormPQN(0);
end;

procedure TFmPQE.MenuItem1Click(Sender: TObject);
var
  Cliente, Insumo: Integer;
begin
  Cliente := QrPQECI.Value;
  Insumo  := QrPQEItsInsumo.Value;
  //
  AlteraSubRegistro;
  DistribuiCustoDoFrete;
  UnPQx.AtualizaEstoquePQ(Cliente, Insumo, QrPQEEmpresa.Value, aeMsg, CO_VAZIO);
end;

procedure TFmPQE.MostraFormEfdInnCTsCab(SQLType: TSQLType);
var
  NewControle, Controle: Integer;
begin
{$IfDef sAllVS}
  NewControle := 0;
  Controle := QrEfdInnCTsCabControle.Value;
  //
  if DBCheck.CriaFm(TFmEfdInnCTsCab, FmEfdInnCTsCab, afmoNegarComAviso) then
  begin
    FmEfdInnCTsCab.ImgTipo.SQLType := SQLType;
    FmEfdInnCTsCab.FQrIts                         := QrEfdInnCTsCab;
    FmEfdInnCTsCab.EdMovFatID.ValueVariant        := FEFDInnNFsMainFatID;
    FmEfdInnCTsCab.EdMovFatNum.ValueVariant       := QrPQECodigo.Value;
    FmEfdInnCTsCab.EdMovimCod.ValueVariant        := CO_MovimCod_ZERO; // N�o tem!!! QrPQEMovimCod.Value;
    FmEfdInnCTsCab.EdEmpresa.ValueVariant         := QrPQEEmpresa.Value;
    //FmEfdInnCTsCab.EdSerieFch.ValueVariant        := QrPQEItsSerieFch.Value;
    //FmEfdInnCTsCab.EdFicha.ValueVariant           := QrPQEItsFicha.Value;
    FmEfdInnCTsCab.EdControle.ValueVariant        := 0;

    if (SQLType = stIns) and (QrPQEIts.RecordCount > 0) then
    begin
      FmEfdInnCTsCab.EdControle.ValueVariant      := 0;
      FmEfdInnCTsCab.EdPecas.ValueVariant         := 0; //QrVSMovDifInfPecas.Value;
      FmEfdInnCTsCab.EdPesoKg.ValueVariant        := QrPQEPesoL.Value;
      FmEfdInnCTsCab.EdAreaM2.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdAreaP2.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdValorT.ValueVariant        := QrPQEFrete.Value;
      FmEfdInnCTsCab.EdMotorista.ValueVariant     := 0;
      FmEfdInnCTsCab.CBMotorista.KeyValue         := 0;
      FmEfdInnCTsCab.EdPlaca.ValueVariant         := '';
      FmEfdInnCTsCab.EdCOD_SIT.ValueVariant       := 0; // Documento regular
      FmEfdInnCTsCab.EdCOD_MOD.ValueVariant       := '57';
      (*if QrPQECI.Value = QrPQEEmpresa.Value then
      begin*)
        FmEfdInnCTsCab.EdSER.ValueVariant           := QrPQECTe_serie.Value;
        FmEfdInnCTsCab.EdNUM_DOC.ValueVariant       := QrPQEConhecimento.Value;
        FmEfdInnCTsCab.EdCHV_CTE.ValueVariant       := QrPQECte_Id.Value;
      (*end else
      begin
        FmEfdInnCTsCab.EdSER.ValueVariant           := 0;
        FmEfdInnCTsCab.EdNUM_DOC.ValueVariant       := ?;
        FmEfdInnCTsCab.EdCHV_NFE.ValueVariant       := '';
      end;*)
      //FmEfdInnCTsCab.EdNFeStatus.ValueVariant
      FmEfdInnCTsCab.TPDT_DOC.Date                := QrPQEDataE.Value;
      FmEfdInnCTsCab.TPDT_A_P.Date                := QrPQEData.Value;

      FmEfdInnCTsCab.EdCliInt.ValueVariant        := QrPQECI.Value;
      FmEfdInnCTsCab.CBCliInt.KeyValue            := QrPQECI.Value;

      FmEfdInnCTsCab.EdTransportador.ValueVariant    := QrPQETransportadora.Value;
      FmEfdInnCTsCab.CBTransportador.KeyValue        := QrPQETransportadora.Value;

      FmEfdInnCTsCab.EdVL_DOC.ValueVariant        := QrPQEFrete.Value;
      //FmEfdInnCTsCab.EdIND_PGTO.ValueVariant      := 1; //� Prazo
      FmEfdInnCTsCab.EdVL_DESC.ValueVariant       := 0.00;
      //FmEfdInnCTsCab.EdVL_ABAT_NT.ValueVariant    := 0.00;  // Abatimento n�o comercia
      FmEfdInnCTsCab.EdVL_SERV.ValueVariant       := QrPQEFrete.Value;
      FmEfdInnCTsCab.EdVL_BC_ICMS.ValueVariant      := QrPQEFrete.Value;
      FmEfdInnCTsCab.EdALIQ_ICMS.ValueVariant     := QrPQEFreteRpICMS.Value;
      //FmEfdInnCTsCab.EdIND_FRT.ValueVariant       := 1; // Contrata��o de frete por conta do destinat�rio
(*
      FmEfdInnCTsCab.EdVL_FRT.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdVL_SEG.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdVL_OUT_DA.ValueVariant     := 0.00;
      FmEfdInnCTsCab.EdVL_BC_ICMS.ValueVariant    := QrVSMovDifInfValorT.Value;;
      FmEfdInnCTsCab.EdVL_ICMS.ValueVariant       := 0.00;
      FmEfdInnCTsCab.EdVL_BC_ICMS_ST.ValueVariant := 0.00;
      FmEfdInnCTsCab.EdVL_ICMS_ST.ValueVariant    := 0.00;
      FmEfdInnCTsCab.EdVL_IPI.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdVL_PIS.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdVL_COFINS.ValueVariant     := 0.00;
      FmEfdInnCTsCab.EdVL_PIS_ST.ValueVariant     := 0.00;
      FmEfdInnCTsCab.EdVL_COFINS_ST.ValueVariant  := 0.00;
*)
      FmEfdInnCTsCab.EdNFe_FatID.ValueVariant         := 0;
      FmEfdInnCTsCab.EdNFe_FatNum.ValueVariant        := 0;
      FmEfdInnCTsCab.EdNFe_StaLnk.ValueVariant        := 0;
      //
    end else
    begin
      FmEfdInnCTsCab.EdControle.ValueVariant      := QrEfdInnCTsCabControle.Value;
      FmEfdInnCTsCab.EdPecas.ValueVariant         := QrEfdInnCTsCabPecas.Value;
      FmEfdInnCTsCab.EdPesoKg.ValueVariant        := QrEfdInnCTsCabPesoKg.Value;
      FmEfdInnCTsCab.EdAreaM2.ValueVariant        := QrEfdInnCTsCabAreaM2.Value;
      FmEfdInnCTsCab.EdAreaP2.ValueVariant        := QrEfdInnCTsCabAreaP2.Value;
      FmEfdInnCTsCab.EdValorT.ValueVariant        := QrEfdInnCTsCabValorT.Value;
      FmEfdInnCTsCab.EdMotorista.ValueVariant     := QrEfdInnCTsCabMotorista.Value;
      FmEfdInnCTsCab.CBMotorista.KeyValue         := QrEfdInnCTsCabMotorista.Value;
      FmEfdInnCTsCab.EdPlaca.ValueVariant         := QrEfdInnCTsCabPlaca.Value;
      FmEfdInnCTsCab.EdCOD_SIT.ValueVariant       := QrEfdInnCTsCabCOD_SIT.Value;
      //FmEfdInnCTsCab.EdNFeStatus.ValueVariant     := QrVSInnCTsCEdNFeStaab.Value;
      FmEfdInnCTsCab.TPDT_DOC.Date                := QrEfdInnCTsCabDT_DOC.Value;
      FmEfdInnCTsCab.TPDT_A_P.Date                := QrEfdInnCTsCabDT_A_P.Value;
      //
      FmEfdInnCTsCab.EdCliInt.ValueVariant        := QrEfdInnCTsCabCliInt.Value;
      FmEfdInnCTsCab.CBCliInt.KeyValue            := QrEfdInnCTsCabCliInt.Value;
      //
      FmEfdInnCTsCab.EdTransportador.ValueVariant := QrEfdInnCTsCabTerceiro.Value;
      FmEfdInnCTsCab.CBTransportador.KeyValue     := QrEfdInnCTsCabTerceiro.Value;
      //
      FmEfdInnCTsCab.EdCFOP.ValueVariant          := QrEfdInnCTsCabCFOP.Value;
      FmEfdInnCTsCab.CBCFOP.KeyValue              := QrEfdInnCTsCabCFOP.Value;
      //
      FmEfdInnCTsCab.EdICMS_CST.ValueVariant      := QrEfdInnCTsCabCST_ICMS.Value;
      //
      //FmEfdInnCTsCab.EdIND_PGTO.ValueVariant      := QrEfdInnCTsCabIND_PGTO.Value;
      FmEfdInnCTsCab.EdIND_FRT.ValueVariant       := QrEfdInnCTsCabIND_FRT.Value;
      FmEfdInnCTsCab.EdVL_SERV.ValueVariant       := QrEfdInnCTsCabVL_SERV.Value;
      FmEfdInnCTsCab.EdVL_DESC.ValueVariant       := QrEfdInnCTsCabVL_DESC.Value;
      FmEfdInnCTsCab.EdVL_NT.ValueVariant         := QrEfdInnCTsCabVL_NT.Value;  // Abatimento n�o comercia


      FmEfdInnCTsCab.EdVL_RED_BC.ValueVariant     := QrEfdInnCTsCabVL_RED_BC.Value;
      FmEfdInnCTsCab.EdVL_BC_ICMS.ValueVariant    := QrEfdInnCTsCabVL_BC_ICMS.Value;
      FmEfdInnCTsCab.EdALIQ_ICMS.ValueVariant     := QrEfdInnCTsCabALIQ_ICMS.Value;
      FmEfdInnCTsCab.EdVL_ICMS.ValueVariant       := QrEfdInnCTsCabVL_ICMS.Value;
      FmEfdInnCTsCab.EdVL_OPR.ValueVariant        := QrEfdInnCTsCabVL_OPR.Value;
      FmEfdInnCTsCab.EdVL_DOC.ValueVariant        := QrEfdInnCTsCabVL_DOC.Value;

      FmEfdInnCTsCab.EdCOD_MOD.ValueVariant       := QrEfdInnCTsCabCOD_MOD.Value;
      FmEfdInnCTsCab.EdSER.ValueVariant           := QrEfdInnCTsCabSER.Value;
      FmEfdInnCTsCab.EdSUB.ValueVariant           := QrEfdInnCTsCabSUB.Value;
      FmEfdInnCTsCab.EdNUM_DOC.ValueVariant       := QrEfdInnCTsCabNUM_DOC.Value;
      FmEfdInnCTsCab.EdCHV_CTE.ValueVariant       := QrEfdInnCTsCabCHV_CTE.Value;

      FmEfdInnCTsCab.EdCOD_SIT.ValueVariant       := QrEfdInnCTsCabCOD_SIT.Value;

      FmEfdInnCTsCab.EdTP_CT_e.ValueVariant       := QrEfdInnCTsCabTP_CT_e.Value;


      FmEfdInnCTsCab.EdCOD_MUN_ORIG.ValueVariant  := QrEfdInnCTsCabCOD_MUN_ORIG.Value;
      FmEfdInnCTsCab.CBCOD_MUN_ORIG.KeyValue      := QrEfdInnCTsCabCOD_MUN_ORIG.Value;

      FmEfdInnCTsCab.EdCOD_MUN_DEST.ValueVariant  := QrEfdInnCTsCabCOD_MUN_DEST.Value;
      FmEfdInnCTsCab.CBCOD_MUN_DEST.KeyValue      := QrEfdInnCTsCabCOD_MUN_DEST.Value;

      FmEfdInnCTsCab.EdRegrFiscal.ValueVariant    := QrEfdInnCTsCabRegrFiscal.Value;
      FmEfdInnCTsCab.CBRegrFiscal.KeyValue        := QrEfdInnCTsCabRegrFiscal.Value;

      FmEfdInnCTsCab.EdIND_NAT_FRT.ValueVariant     := QrEfdInnCTsCabIND_NAT_FRT.Value;
      //FmEfdInnCTsCab.EdVL_ITEM.ValueVariant         := QrEfdInnCTsCabVL_ITEM.Value;
      FmEfdInnCTsCab.EdCST_PIS.ValueVariant         := QrEfdInnCTsCabCST_PIS.Value;
      FmEfdInnCTsCab.EdNAT_BC_CRED.ValueVariant     := QrEfdInnCTsCabNAT_BC_CRED.Value;
      FmEfdInnCTsCab.EdVL_BC_PIS.ValueVariant       := QrEfdInnCTsCabVL_BC_PIS.Value;
      FmEfdInnCTsCab.EdALIQ_PIS.ValueVariant        := QrEfdInnCTsCabALIQ_PIS.Value;
      FmEfdInnCTsCab.EdVL_PIS.ValueVariant          := QrEfdInnCTsCabVL_PIS.Value;
      FmEfdInnCTsCab.EdCST_COFINS.ValueVariant      := QrEfdInnCTsCabCST_COFINS.Value;
      FmEfdInnCTsCab.EdVL_BC_COFINS.ValueVariant    := QrEfdInnCTsCabVL_BC_COFINS.Value;
      FmEfdInnCTsCab.EdALIQ_COFINS.ValueVariant     := QrEfdInnCTsCabALIQ_COFINS.Value;
      FmEfdInnCTsCab.EdVL_COFINS.ValueVariant       := QrEfdInnCTsCabVL_COFINS.Value;

   end;
    FmEfdInnCTsCab.ShowModal;
    NewControle := FmEfdInnCTsCab.FControle;
    FmEfdInnCTsCab.Destroy;
    //
    ReopenEfdInnCTsCab(NewControle);
    if (SQLType = stIns) and (NewControle > Controle) then
    begin
      if QrEfdInnCTsCabControle.Value = NewControle then
        MostraFormEfdInnCTsIts(stIns);
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmPQE.MostraFormEfdInnCTsIts(SQLType: TSQLType);
begin
// Fazer no futuro?
end;

procedure TFmPQE.MostraFormEfdInnNFsCab(SQLType: TSQLType);
var
  NewControle, Controle: Integer;
begin
{$IfDef sAllVS}
  NewControle := 0;
  Controle := QrEfdInnNFsCabControle.Value;
  //
  if DBCheck.CriaFm(TFmEfdInnNFsCab, FmEfdInnNFsCab, afmoNegarComAviso) then
  begin
    FmEfdInnNFsCab.ImgTipo.SQLType := SQLType;
    FmEfdInnNFsCab.FQrIts                         := QrEfdInnNFsCab;
    FmEfdInnNFsCab.EdMovFatID.ValueVariant        := FEFDInnNFSMainFatID;
    FmEfdInnNFsCab.EdMovFatNum.ValueVariant       := QrPQECodigo.Value;
    FmEfdInnNFsCab.EdMovimCod.ValueVariant        := CO_MovimCod_ZERO; // N�o tem!!! QrPQEMovimCod.Value;
    FmEfdInnNFsCab.EdEmpresa.ValueVariant         := QrPQEEmpresa.Value;
    FmEfdInnNFsCab.FFornecedor                    := QrPQEIQ.Value;
    //FmEfdInnNFsCab.EdSerieFch.ValueVariant        := QrPQEItsSerieFch.Value;
    //FmEfdInnNFsCab.EdFicha.ValueVariant           := QrPQEItsFicha.Value;
    FmEfdInnNFsCab.EdControle.ValueVariant        := 0;
    FmEfdInnNFsCab.FExigeQtde                     := False;
    if (SQLType = stIns) and (QrPQEIts.RecordCount > 0) then
    begin
      FmEfdInnNFsCab.EdControle.ValueVariant      := 0;
      FmEfdInnNFsCab.EdPecas.ValueVariant         := 0; //QrVSMovDifInfPecas.Value;
      FmEfdInnNFsCab.EdPesoKg.ValueVariant        := QrPQEPesoL.Value;
      FmEfdInnNFsCab.EdAreaM2.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdAreaP2.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdValorT.ValueVariant        := QrPQEValorNF.Value;
      FmEfdInnNFsCab.EdMotorista.ValueVariant     := 0;
      FmEfdInnNFsCab.CBMotorista.KeyValue         := 0;
      FmEfdInnNFsCab.EdPlaca.ValueVariant         := '';
      FmEfdInnNFsCab.EdCOD_SIT.ValueVariant       := 0; // Documento regular
      FmEfdInnNFsCab.EdCOD_MOD.ValueVariant       := 55;
      if QrPQECI.Value = QrPQEEMpresa.Value then
      begin
        FmEfdInnNFsCab.EdSER.ValueVariant           := QrPQESerie.Value;
        FmEfdInnNFsCab.EdNUM_DOC.ValueVariant       := QrPQENF.Value;
        FmEfdInnNFsCab.EdCHV_NFE.ValueVariant       := QrPQErefNFe.Value;
      end else
      begin
        FmEfdInnNFsCab.EdSER.ValueVariant           := 0;
        FmEfdInnNFsCab.EdNUM_DOC.ValueVariant       := QrPQENF_CC.Value;
        FmEfdInnNFsCab.EdCHV_NFE.ValueVariant       := '';
      end;
      //FmEfdInnNFsCab.EdNFeStatus.ValueVariant
      FmEfdInnNFsCab.TPDT_DOC.Date                := QrPQEDataE.Value;
      FmEfdInnNFsCab.TPDT_E_S.Date                := QrPQEData.Value;

      FmEfdInnNFsCab.EdCliInt.ValueVariant        := QrPQECI.Value;
      FmEfdInnNFsCab.CBCliInt.KeyValue            := QrPQECI.Value;

      FmEfdInnNFsCab.EdFornecedor.ValueVariant    := QrPQEIQ.Value;
      FmEfdInnNFsCab.CBFornecedor.KeyValue        := QrPQEIQ.Value;

      FmEfdInnNFsCab.EdTransportador.ValueVariant    := QrPQETransportadora.Value;
      FmEfdInnNFsCab.CBTransportador.KeyValue        := QrPQETransportadora.Value;

      FmEfdInnNFsCab.EdVL_DOC.ValueVariant        := QrPQEValorNF.Value;
      FmEfdInnNFsCab.EdIND_PGTO.ValueVariant      := 1; //� Prazo
      FmEfdInnNFsCab.EdVL_DESC.ValueVariant       := 0.00;
      //FmEfdInnNFsCab.EdVL_ABAT_NT.ValueVariant    := 0.00;  // Abatimento n�o comercia
      FmEfdInnNFsCab.EdVL_MERC.ValueVariant       := QrPQEValProd.Value;
      //FmEfdInnNFsCab.EdIND_FRT.ValueVariant       := 1; // Contrata��o de frete por conta do destinat�rio
(*
      FmEfdInnNFsCab.EdVL_FRT.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_SEG.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_OUT_DA.ValueVariant     := 0.00;
      FmEfdInnNFsCab.EdVL_BC_ICMS.ValueVariant    := QrVSMovDifInfValorT.Value;;
      FmEfdInnNFsCab.EdVL_ICMS.ValueVariant       := 0.00;
      FmEfdInnNFsCab.EdVL_BC_ICMS_ST.ValueVariant := 0.00;
      FmEfdInnNFsCab.EdVL_ICMS_ST.ValueVariant    := 0.00;
      FmEfdInnNFsCab.EdVL_IPI.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_PIS.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_COFINS.ValueVariant     := 0.00;
      FmEfdInnNFsCab.EdVL_PIS_ST.ValueVariant     := 0.00;
      FmEfdInnNFsCab.EdVL_COFINS_ST.ValueVariant  := 0.00;
*)
      FmEfdInnNFsCab.EdNFe_FatID.ValueVariant         := 0;
      FmEfdInnNFsCab.EdNFe_FatNum.ValueVariant        := 0;
      FmEfdInnNFsCab.EdNFe_StaLnk.ValueVariant        := 0;
      //
    end else
    begin
      FmEfdInnNFsCab.RGTpEntrd.ItemIndex          := QrEfdInnNFsCabTpEntrd.Value;
      FmEfdInnNFsCab.EdControle.ValueVariant      := QrEfdInnNFsCabControle.Value;
      FmEfdInnNFsCab.EdPecas.ValueVariant         := QrEfdInnNFsCabPecas.Value;
      FmEfdInnNFsCab.EdPesoKg.ValueVariant        := QrEfdInnNFsCabPesoKg.Value;
      FmEfdInnNFsCab.EdAreaM2.ValueVariant        := QrEfdInnNFsCabAreaM2.Value;
      FmEfdInnNFsCab.EdAreaP2.ValueVariant        := QrEfdInnNFsCabAreaP2.Value;
      FmEfdInnNFsCab.EdValorT.ValueVariant        := QrEfdInnNFsCabValorT.Value;
      FmEfdInnNFsCab.EdMotorista.ValueVariant     := QrEfdInnNFsCabMotorista.Value;
      FmEfdInnNFsCab.CBMotorista.KeyValue         := QrEfdInnNFsCabMotorista.Value;
      FmEfdInnNFsCab.EdPlaca.ValueVariant         := QrEfdInnNFsCabPlaca.Value;
      FmEfdInnNFsCab.EdCOD_MOD.ValueVariant       := QrEfdInnNFsCabCOD_MOD.Value;
      FmEfdInnNFsCab.EdCOD_SIT.ValueVariant       := QrEfdInnNFsCabCOD_SIT.Value;
      FmEfdInnNFsCab.EdSER.ValueVariant           := QrEfdInnNFsCabSER.Value;
      FmEfdInnNFsCab.EdNUM_DOC.ValueVariant       := QrEfdInnNFsCabNUM_DOC.Value;
      FmEfdInnNFsCab.EdCHV_NFE.ValueVariant       := QrEfdInnNFsCabCHV_NFE.Value;
      //FmEfdInnNFsCab.EdNFeStatus.ValueVariant     := QrVSInnNFsCEdNFeStaab.Value;
      FmEfdInnNFsCab.TPDT_DOC.Date                := QrEfdInnNFsCabDT_DOC.Value;
      FmEfdInnNFsCab.TPDT_E_S.Date                := QrEfdInnNFsCabDT_E_S.Value;
      FmEfdInnNFsCab.EdVL_DOC.ValueVariant        := QrEfdInnNFsCabVL_DOC.Value;
      FmEfdInnNFsCab.EdIND_PGTO.ValueVariant      := QrEfdInnNFsCabIND_PGTO.Value;
      FmEfdInnNFsCab.EdVL_DESC.ValueVariant       := QrEfdInnNFsCabVL_DESC.Value;
      //FmEfdInnNFsCab.EdVL_ABAT_NT.ValueVariant    := 0.00;  // Abatimento n�o comercia
      FmEfdInnNFsCab.EdVL_MERC.ValueVariant       := QrEfdInnNFsCabVL_MERC.Value;
      FmEfdInnNFsCab.EdIND_FRT.ValueVariant       := QrEfdInnNFsCabIND_FRT.Value;
      FmEfdInnNFsCab.EdVL_FRT.ValueVariant        := QrEfdInnNFsCabVL_FRT.Value;
      FmEfdInnNFsCab.EdVL_SEG.ValueVariant        := QrEfdInnNFsCabVL_SEG.Value;
      FmEfdInnNFsCab.EdVL_OUT_DA.ValueVariant     := QrEfdInnNFsCabVL_OUT_DA.Value;
      FmEfdInnNFsCab.EdVL_BC_ICMS.ValueVariant    := QrEfdInnNFsCabVL_BC_ICMS.Value;
      FmEfdInnNFsCab.EdVL_ICMS.ValueVariant       := QrEfdInnNFsCabVL_ICMS.Value;
      FmEfdInnNFsCab.EdVL_BC_ICMS_ST.ValueVariant := QrEfdInnNFsCabVL_BC_ICMS_ST.Value;
      FmEfdInnNFsCab.EdVL_ICMS_ST.ValueVariant    := QrEfdInnNFsCabVL_ICMS_ST.Value;
      FmEfdInnNFsCab.EdVL_IPI.ValueVariant        := QrEfdInnNFsCabVL_IPI.Value;
      FmEfdInnNFsCab.EdVL_PIS.ValueVariant        := QrEfdInnNFsCabVL_PIS.Value;
      FmEfdInnNFsCab.EdVL_COFINS.ValueVariant     := QrEfdInnNFsCabVL_COFINS.Value;
      FmEfdInnNFsCab.EdVL_PIS_ST.ValueVariant     := QrEfdInnNFsCabVL_PIS_ST.Value;
      FmEfdInnNFsCab.EdVL_COFINS_ST.ValueVariant  := QrEfdInnNFsCabVL_COFINS.Value;
      FmEfdInnNFsCab.EdNFe_FatID.ValueVariant     := QrEfdInnNFsCabNFe_FatID.Value;
      FmEfdInnNFsCab.EdNFe_FatNum.ValueVariant    := QrEfdInnNFsCabNFe_FatNum.Value;
      FmEfdInnNFsCab.EdNFe_StaLnk.ValueVariant    := QrEfdInnNFsCabNFe_StaLnk.Value;
      //
      FmEfdInnNFsCab.EdCliInt.ValueVariant        := QrEfdInnNFsCabCliInt.Value;
      FmEfdInnNFsCab.CBCliInt.KeyValue            := QrEfdInnNFsCabCliInt.Value;
      //
      FmEfdInnNFsCab.EdFornecedor.ValueVariant    := QrEfdInnNFsCabTerceiro.Value;
      FmEfdInnNFsCab.CBFornecedor.KeyValue        := QrEfdInnNFsCabTerceiro.Value;
      //
      FmEfdInnNFsCab.EdRegrFiscal.ValueVariant    := QrEfdInnNFsCabRegrFiscal.Value;
      FmEfdInnNFsCab.CBRegrFiscal.KeyValue        := QrEfdInnNFsCabRegrFiscal.Value;
   end;
    FmEfdInnNFsCab.ShowModal;
    NewControle := FmEfdInnNFsCab.FControle;
    FmEfdInnNFsCab.Destroy;
    //
    ReopenEfdInnNFsCab(NewControle);
    if (SQLType = stIns) and (NewControle > Controle) then
    begin
      if QrEfdInnNFsCabControle.Value = NewControle then
        MostraFormEfdInnNFsIts(stIns);
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmPQE.MostraFormEfdInnNFsIts(SQLType: TSQLType);
var
  NewConta, Conta, GraGruX, Grandeza: Integer;
  CST_B, IPI_CST, PIS_CST, COFINS_CST: Integer;
  ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq, COFINSRec_pAliq, ICMSSTRec_pAliq: Double;
begin
{$IfDef sAllVS}
  //NewConta := 0;
  //Conta := QrEfdInnNFsItsConta.Value;
  //
  if DBCheck.CriaFm(TFmEfdInnNFsIts, FmEfdInnNFsIts, afmoNegarComAviso) then
  begin
    FmEfdInnNFsIts.ImgTipo.SQLType := SQLType;
    FmEfdInnNFsIts.FData           := QrEfdInnNFsCabDT_E_S.Value;
    FmEfdInnNFsIts.FRegrFiscal     := QrEfdInnNFsCabRegrFiscal.Value;
    //
    FmEfdInnNFsIts.FQrCab                         := QrEfdInnNFsCab;
    FmEfdInnNFsIts.FQrIts                         := QrEfdInnNFsIts;
    FmEfdInnNFsIts.EdEmpresa.ValueVariant         := QrPQEEmpresa.Value;
    FmEfdInnNFsIts.EdMovFatID.ValueVariant        := FEFDInnNFSMainFatID;
    FmEfdInnNFsIts.EdMovFatNum.ValueVariant       := QrPQECodigo.Value;
    FmEfdInnNFsIts.EdMovimCod.ValueVariant        := CO_MovimCod_ZERO; // N�o tem! QrPQEMovimCod.Value;
    FmEfdInnNFsIts.EdMovimNiv.ValueVariant        := 0;
    FmEfdInnNFsIts.EdMovimTwn.ValueVariant        := 0;
    FmEfdInnNFsIts.EdControle.ValueVariant        := QrEfdInnNFsCabControle.Value;
    //FmEfdInnNFsIts.FQrIts                         := QrVSInnNFs;
    if SQLType = stIns then
    begin
      FmEfdInnNFsIts.EdConta.ValueVariant           := 0;

      GraGruX := Dmod.ObtemGraGruXDeGraGru1(QrPQEItsInsumo.Value);

      FmEfdInnNFsIts.EdGraGruX.ValueVariant                       := GraGruX;
      FmEfdInnNFsIts.CBGraGruX.KeyValue                           := GraGruX;

(*
      UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT med.Grandeza ',
      'FROM gragru1 gg1 ',
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
      'WHERE gg1.Nivel1=' + Geral.FF0(GraGruX),
      '']);
*)
(*
      Grandeza := Dmod.QrAux.FieldByName('Grandeza').AsInteger;
      //case QrPQEItsGrandeza.Value of
      case Grandeza of
        0: FmEfdInnNFsIts.EdQTD.ValueVariant                      := QrVSMovDifInfPecas.Value;
        2: FmEfdInnNFsIts.EdQTD.ValueVariant                      := QrVSMovDifInfPesoKg.Value;
        else Geral.MB_Aviso('Grandeza no cadastro grade da mat�ria-prima deve ser Pe�a ou Peso(kg)!');
      end;
*)
      FmEfdInnNFsIts.EdQTD.ValueVariant                             := QrPQEItsPesoVL.Value;
      //FmEfdInnNFsIts.EdCFOP.Text                                  := '';
      if QrPQEUF_Empresa.Value = QrPQEUF_Fornece.Value then
        FmEfdInnNFsIts.EdCFOP.ValueVariant := Dmod.QrControleCreTrib_MP_CFOP_DE.Value
      else
        FmEfdInnNFsIts.EdCFOP.ValueVariant := Dmod.QrControleCreTrib_MP_CFOP_FE.Value;
      //
      FmEfdInnNFsIts.Edprod_vProd.ValueVariant                    := QrPQEItsValorItem.Value;
      FmEfdInnNFsIts.EdVL_ITEM.ValueVariant                       := QrPQEItsprod_vProd.Value;
      FmEfdInnNFsIts.EdVL_DESC.ValueVariant                       := 0.00;
      FmEfdInnNFsIts.EdIND_MOV.ValueVariant                       := '';
      {
      if Dmod.QrControleNaoRecupImposPQ.Value = 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
        'SELECT CST_B, IPI_CST, PIS_CST, COFINS_CST, ',
        'ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq,  ',
        'COFINSRec_pAliq  ',
        'FROM gragru1 ',
        'WHERE Nivel1=' + Geral.FF0(GraGru1),
        '']);
        //
        CST_B           := QrGraGru1CST_B.Value;
        IPI_CST         := QrGraGru1IPI_CST.Value;
        PIS_CST         := QrGraGru1PIS_CST.Value;
        COFINS_CST      := QrGraGru1COFINS_CST.Value;
        ICMSRec_pAliq   := QrGraGru1ICMSRec_pAliq.Value;
        ICMSSTRec_pAliq := 0.00;
        IPIRec_pAliq    := QrGraGru1IPIRec_pAliq.Value;
        PISRec_pAliq    := QrGraGru1PISRec_pAliq.Value;
        COFINSRec_pAliq := QrGraGru1COFINSRec_pAliq.Value;
      end else
      begin
      }
        CST_B           := Geral.IMV(Dmod.QrControleCreTrib_MP_ICMS_CST.VAlue);
        IPI_CST         := Geral.IMV(Dmod.QrControleCreTrib_MP_IPI_CST.VAlue);
        PIS_CST         := Geral.IMV(Dmod.QrControleCreTrib_MP_PIS_CST.VAlue);
        COFINS_CST      := Geral.IMV(Dmod.QrControleCreTrib_MP_COFINS_CST.VAlue);
        ICMSRec_pAliq   := Dmod.QrControleCreTrib_MP_ICMS_ALIQ.Value;
        ICMSSTRec_pAliq := Dmod.QrControleCreTrib_MP_ICMS_ALIQ_ST.Value;
        IPIRec_pAliq    := Dmod.QrControleCreTrib_MP_IPI_ALIQ.Value;
        PISRec_pAliq    := Dmod.QrControleCreTrib_MP_PIS_ALIQ.Value;
        COFINSRec_pAliq := Dmod.QrControleCreTrib_MP_COFINS_ALIQ.Value;
      {end;}
      FmEfdInnNFsIts.EdICMS_CST.ValueVariant                      := CST_B;
      //FmEfdInnNFsIts.EdCOD_NAT.ValueVariant                       := COD_NAT
      FmEfdInnNFsIts.EdVL_BC_ICMS.ValueVariant                    := QrPQEItsValorItem.Value;
      FmEfdInnNFsIts.EdALIQ_ICMS.ValueVariant                     := ICMSRec_pAliq;
      //FmEfdInnNFsIts.EdVL_ICMS.ValueVariant                       := VL_ICMS
      //FmEfdInnNFsIts.EdVL_BC_ICMS_ST.ValueVariant                 := VL_BC_ICMS_ST
      FmEfdInnNFsIts.EdALIQ_ST.ValueVariant                       := ICMSSTRec_pAliq;
      //FmEfdInnNFsIts.EdVL_ICMS_ST.ValueVariant                    := VL_ICMS_ST
      //FmEfdInnNFsIts.EdIND_APUR  .ValueVariant                    := IND_APUR
      FmEfdInnNFsIts.EdCST_IPI.ValueVariant                       := IPI_CST;
      //FmEfdInnNFsIts.EdCOD_ENQ   .ValueVariant                    := COD_ENQ
      //FmEfdInnNFsIts.EdVL_BC_IPI.ValueVariant                     := VL_BC_IPI
      FmEfdInnNFsIts.EdALIQ_IPI.ValueVariant                      := IPIRec_pAliq;
      //FmEfdInnNFsIts.EdVL_IPI.ValueVariant                        := VL_IPI
      FmEfdInnNFsIts.EdCST_PIS.ValueVariant                       := PIS_CST;
      //FmEfdInnNFsIts.EdVL_BC_PIS .ValueVariant                    := VL_BC_PIS
      FmEfdInnNFsIts.EdALIQ_PIS.ValueVariant                      := PISRec_pAliq;
      //FmEfdInnNFsIts.EdQUANT_BC_PIS.ValueVariant                  := QUANT_BC_PIS
      //FmEfdInnNFsIts.EdALIQ_PIS_r.ValueVariant                    := ALIQ_PIS_r
      //FmEfdInnNFsIts.EdVL_PIS.ValueVariant                        := VL_PIS
      FmEfdInnNFsIts.EdCST_COFINS.ValueVariant                    := COFINS_CST;
      //FmEfdInnNFsIts.EdVL_BC_COFINS.ValueVariant                  := VL_BC_COFINS
      FmEfdInnNFsIts.EdALIQ_COFINS.ValueVariant                   := COFINSRec_pAliq;
      //FmEfdInnNFsIts.EdQUANT_BC_COFINS.ValueVariant               := QUANT_BC_COFINS
      //FmEfdInnNFsIts.EdALIQ_COFINS_r.ValueVariant                 := ALIQ_COFINS_r
      //FmEfdInnNFsIts.EdVL_COFINS.ValueVariant                     := VL_COFINS
      //FmEfdInnNFsIts.EdCOD_CTA.ValueVariant                       := COD_CTA
      //FmEfdInnNFsIts.EdVL_ABAT_NT     .ValueVariant               := VL_ABAT_NT

    end else
    begin
      FmEfdInnNFsIts.EdConta.ValueVariant           := QrEfdInnNFsItsConta.Value;
      FmEfdInnNFsIts.FTpEntrd                       := QrEfdInnNFsCabTpEntrd.Value;

      FmEfdInnNFsIts.EdGraGruX.ValueVariant                       := QrEfdInnNFsItsGraGruX.Value;
      FmEfdInnNFsIts.CBGraGruX.KeyValue                           := QrEfdInnNFsItsGraGruX.Value;
      FmEfdInnNFsIts.EdCFOP.Text                                  := Geral.FormataCFOP(Geral.FF0(QrEfdInnNFsItsCFOP.Value));
      //FmEfdInnNFsIts.EdCOD_NAT.ValueVariant                       :=
      FmEfdInnNFsIts.EdQTD.ValueVariant                           := QrEfdInnNFsItsQTD.Value;
      FmEfdInnNFsIts.EdIND_MOV.ValueVariant                       := QrEfdInnNFsItsIND_MOV.Value;
      FmEfdInnNFsIts.Edprod_vProd.ValueVariant                    := QrEfdInnNFsItsprod_vProd.Value;
      FmEfdInnNFsIts.Edprod_vFrete.ValueVariant                   := QrEfdInnNFsItsprod_vFrete.Value;
      FmEfdInnNFsIts.Edprod_vSeg.ValueVariant                     := QrEfdInnNFsItsprod_vSeg.Value;
      FmEfdInnNFsIts.Edprod_vOutro.ValueVariant                   := QrEfdInnNFsItsprod_vOutro.Value;
      FmEfdInnNFsIts.EdVL_ITEM.ValueVariant                       := QrEfdInnNFsItsVL_ITEM.Value;
      FmEfdInnNFsIts.EdVL_DESC.ValueVariant                       := QrEfdInnNFsItsVL_DESC.Value;
      FmEfdInnNFsIts.EdOri_IPIpIPI.ValueVariant                   := QrEfdInnNFsItsOri_IPIpIPI.Value;
      FmEfdInnNFsIts.EdOri_IPIvIPI.ValueVariant                   := QrEfdInnNFsItsOri_IPIvIPI.Value;
      FmEfdInnNFsIts.EdxLote.ValueVariant                         := QrEfdInnNFsItsxLote.Value;
      FmEfdInnNFsIts.EdICMS_CST.ValueVariant                      := QrEfdInnNFsItsCST_ICMS.Value;
      FmEfdInnNFsIts.EdVL_BC_ICMS.ValueVariant                    := QrEfdInnNFsItsVL_BC_ICMS.Value;
      FmEfdInnNFsIts.EdALIQ_ICMS.ValueVariant                     := QrEfdInnNFsItsALIQ_ICMS.Value;
      FmEfdInnNFsIts.EdVL_ICMS.ValueVariant                       := QrEfdInnNFsItsVL_ICMS.Value;
      FmEfdInnNFsIts.EdVL_BC_ICMS_ST.ValueVariant                 := QrEfdInnNFsItsVL_BC_ICMS_ST.Value;
      FmEfdInnNFsIts.EdALIQ_ST.ValueVariant                       := QrEfdInnNFsItsALIQ_ST.Value;
      FmEfdInnNFsIts.EdVL_ICMS_ST.ValueVariant                    := QrEfdInnNFsItsVL_ICMS_ST.Value;
      //FmEfdInnNFsIts.EdIND_APUR.ValueVariant                      := IND_APUR
      //FmEfdInnNFsIts.EdCOD_ENQ.ValueVariant                       := COD_ENQ


      FmEfdInnNFsIts.EdCST_IPI.ValueVariant                       := QrEfdInnNFsItsCST_IPI.Value;;
      FmEfdInnNFsIts.EdVL_BC_IPI.ValueVariant                     := QrEfdInnNFsItsVL_BC_IPI.Value;
      FmEfdInnNFsIts.EdALIQ_IPI.ValueVariant                      := QrEfdInnNFsItsALIQ_IPI.Value;
      FmEfdInnNFsIts.EdVL_IPI.ValueVariant                        := QrEfdInnNFsItsVL_IPI.Value;
      FmEfdInnNFsIts.EdCST_PIS.ValueVariant                       := QrEfdInnNFsItsCST_PIS.Value;
      FmEfdInnNFsIts.EdVL_BC_PIS .ValueVariant                    := QrEfdInnNFsItsVL_BC_PIS.Value;
      FmEfdInnNFsIts.EdALIQ_PIS.ValueVariant                      := QrEfdInnNFsItsALIQ_PIS_p.Value;
      //FmEfdInnNFsIts.EdQUANT_BC_PIS.ValueVariant                  := QrEfdInnNFsItsQUANT_BC_PIS.Value;
      //FmEfdInnNFsIts.EdALIQ_PIS_r.ValueVariant                    := QrEfdInnNFsItsALIQ_PIS_r.Value;
      FmEfdInnNFsIts.EdVL_PIS.ValueVariant                        := QrEfdInnNFsItsVL_PIS.Value;
      FmEfdInnNFsIts.EdCST_COFINS.ValueVariant                    := QrEfdInnNFsItsCST_COFINS.Value;
      FmEfdInnNFsIts.EdVL_BC_COFINS.ValueVariant                  := QrEfdInnNFsItsVL_BC_COFINS.Value;
      FmEfdInnNFsIts.EdALIQ_COFINS.ValueVariant                   := QrEfdInnNFsItsALIQ_COFINS_p.Value;
      //FmEfdInnNFsIts.EdQUANT_BC_COFINS.ValueVariant               := QrEfdInnNFsItsQUANT_BC_COFINS.Value;
      //FmEfdInnNFsIts.EdALIQ_COFINS_r.ValueVariant                 := QrEfdInnNFsItsALIQ_COFINS_r.Value;
      FmEfdInnNFsIts.EdVL_COFINS.ValueVariant                     := QrEfdInnNFsItsVL_COFINS.Value;
      //FmEfdInnNFsIts.EdCOD_CTA.ValueVariant                       := QrEfdInnNFsItsCOD_CTA.Value;
      //FmEfdInnNFsIts.EdVL_ABAT_NT.ValueVariant                    := QrEfdInnNFsItsVL_ABAT_NT.Value;
      //
      FmEfdInnNFsIts.EdAjusteVL_BC_ICMS.ValueVariant              := QrEfdInnNFsItsAjusteVL_BC_ICMS.Value;
      FmEfdInnNFsIts.EdAjusteALIQ_ICMS.ValueVariant               := QrEfdInnNFsItsAjusteALIQ_ICMS.Value;
      FmEfdInnNFsIts.EdAjusteVL_ICMS.ValueVariant                 := QrEfdInnNFsItsAjusteVL_ICMS.Value;
      FmEfdInnNFsIts.EdAjusteVL_OUTROS.ValueVariant               := QrEfdInnNFsItsAjusteVL_OUTROS.Value;
    end;
    FmEfdInnNFsIts.ShowModal;
    NewConta := FmEfdInnNFsIts.FConta;
    FmEfdInnNFsIts.Destroy;
        //
    ReopenEfdInnNFsIts(NewConta);
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmPQE.MostraFormPQENew(Codigo: Integer; EnableBtDesiste, Travando: Boolean);
var
  Filial: Integer;
begin
  Application.CreateForm(TFmPQENew,FmPQENew);
  with FmPQENew do
  begin
  //fazer reteio juros
    BtDesiste.Enabled := EnableBtDesiste;
    ImgTipo.SQLType := stUpd;
    FSqLinked := QrPQESqLinked.Value;
    if (QrPQEIts.RecordCount > 0) and (Travando = False) then
    begin
      LaCI.Enabled := False;
      EdCI.Enabled := False;
      CBCI.Enabled := False;
      //
      EdkgLiquido.Enabled := False;
      EdkgBruto.Enabled := False;
      EdValorNF.Enabled := False;
    end;
    EdCodigo.Text := Geral.FF0(Codigo);
    Filial := DmodG.ObtemFilialDeEntidade(QrPQEEmpresa.Value);
    EdEmpresa.ValueVariant := Filial;
    CBEmpresa.KeyValue := Filial;
    LaEmpresa.Enabled := False;
    EdEmpresa.Enabled := False;
    CBEmpresa.Enabled := False;
    EdRegrFiscal.ValueVariant    := QrPQERegrFiscal.Value;
    CBRegrFiscal.KeyValue        := QrPQERegrFiscal.Value;
    EdCondicaoPG.ValueVariant    := QrPQECondicaoPG.Value;
    CBCondicaoPG.KeyValue        := QrPQECondicaoPG.Value;
    EdFornecedor.Text := Geral.FF0(QrPQEIQ.Value);
    CBFornecedor.KeyValue := QrPQEIQ.Value;
    EdCI.ValueVariant := QrPQECI.Value;
    CBCI.KeyValue := QrPQECI.Value;
    EdTransportador.Text := Geral.FF0(QrPQETransportadora.Value);
    CBTransportador.KeyValue := QrPQETransportadora.Value;
    EdkgBruto.Text := Geral.TFT(FloatToStr(QrPQEPesoB.Value), 3, siPositivo);
    EdkgLiquido.Text := Geral.TFT(FloatToStr(QrPQEPesoL.Value), 3, siPositivo);
    EdNF.Text := Geral.FF0(QrPQENF.Value);
    EdValorNF.Text := Geral.TFT(FloatToStr(QrPQEValorNF.Value), 2, siPositivo);
    EdICMS.Text := Geral.TFT(FloatToStr(QrPQEICMS.Value), 2, siPositivo);
    EdRICMS.Text := Geral.TFT(FloatToStr(QrPQERICMS.Value), 2, siPositivo);
    EdRICMSF.Text := Geral.TFT(FloatToStr(QrPQERICMSF.Value), 2, siPositivo);
    EdConhecim.Text := Geral.FF0(QrPQEConhecimento.Value);
    EdFrete.Text := Geral.TFT(FloatToStr(QrPQEFrete.Value), 2, siPositivo);
    EdJuros.Text := Geral.TFT(FloatToStr(QrPQEJuros.Value), 2, siPositivo);
    TPEntrada.Date := QrPQEData.Value;
    TPEmissao.Date := QrPQEDataE.Value;
    FmPQENew.EdrefNFe.Text := QrPQErefNFe.Value;
    FmPQENew.Edmodnf.ValueVariant := QrPQEmodNF.Value;
    FmPQENew.EdSerie.ValueVariant := QrPQESerie.Value;
    FmPQENew.CkTipoNF.Checked     := QrPQETipoNF.Value = 1;
    //
    EdFreteRpICMS.ValueVariant    := QrPQEFreteRpICMS.Value;
    EdFreteRpPIS.ValueVariant     := QrPQEFreteRpPIS.Value;
    EdFreteRpCOFINS.ValueVariant  := QrPQEFreteRpCOFINS.Value;
    EdFreteRvICMS.ValueVariant    := QrPQEFreteRvICMS.Value;
    EdFreteRvPIS.ValueVariant     := QrPQEFreteRvPIS.Value;
    EdFreteRvCOFINS.ValueVariant  := QrPQEFreteRvCOFINS.Value;
    //
    EdNF_RP.ValueVariant          := QrPQENF_RP.Value;
    EdNF_CC.ValueVariant          := QrPQENF_CC.Value;
    //
    EdNFe_RP.ValueVariant         := QrPQENFe_RP.Value;
    EdNFe_CC.ValueVariant         := QrPQENFe_CC.Value;
    EdCte_Id.ValueVariant         := QrPQECte_Id.Value;
    EdmodRP.ValueVariant          := QrPQEmodRP.Value;
    EdmodCC.ValueVariant          := QrPQEmodCC.Value;
    EdSerRP.ValueVariant          := QrPQESerRP.Value;
    EdSerCC.ValueVariant          := QrPQESerCC.Value;
    EdCTe_mod.ValueVariant        := QrPQECTe_mod.Value;
    EdCTe_serie.ValueVariant      := QrPQECTe_serie.Value;
    //
    EdNFVP_FatID.ValueVariant     := QrPQENFVP_FatID.Value;
    EdNFVP_FatNum.ValueVariant    := QrPQENFVP_FatNum.Value;
    EdNFVP_StaLnk.ValueVariant    := QrPQENFVP_StaLnk.Value;
    EdNFRP_FatID.ValueVariant     := QrPQENFRP_FatID.Value;
    EdNFRP_FatNum.ValueVariant    := QrPQENFRP_FatNum.Value;
    EdNFRP_StaLnk.ValueVariant    := QrPQENFRP_StaLnk.Value;
    EdNFCC_FatID.ValueVariant     := QrPQENFCC_FatID.Value;
    EdNFCC_FatNum.ValueVariant    := QrPQENFCC_FatNum.Value;
    EdNFCC_StaLnk.ValueVariant    := QrPQENFCC_StaLnk.Value;
    //
    EdICMSTot_vProd.ValueVariant  := QrPQEICMSTot_vProd.Value;
    EdICMSTot_vFrete.ValueVariant := QrPQEICMSTot_vFrete.Value;
    EdICMSTot_vSeg.ValueVariant   := QrPQEICMSTot_vSeg.Value;
    EdICMSTot_vOutro.ValueVariant := QrPQEICMSTot_vOutro.Value;
    EdICMSTot_vDesc.ValueVariant  := QrPQEICMSTot_vDesc.Value;
    EdIPI.ValueVariant            := QrPQEIPI.Value;
    //
    EdIND_FRT.ValueVariant        := QrPQEIND_FRT.Value;
    EdIND_PGTO.ValueVariant       := QrPQEIND_PGTO.Value;
    //

    FmPQENew.RGMadeBy.ItemIndex := QrPQEMadeBy.Value;
    //FmPQENew.RGMadeBy.Enabled := True;
  end;
  FmPQENew.ShowModal;
  FmPQENew.Destroy;
end;

function TFmPQE.NaoPermiteFinanca(Qry: TmySQLQuery; NeedRecs: Boolean): Boolean;
begin
  Result := True;
  if Qry <> nil then
  begin
    if Qry.State = dsInactive then
    begin
      Geral.MB_Aviso('A tabela "' + Qry.Name + '" n�o est� aberta!');
      Exit;
    end;
    if NeedRecs and (Qry.RecordCount = 0) then
    begin
      Geral.MB_Aviso('A tabela "' + Qry.Name + '" n�o tem registros!');
      Exit;
    end;
  end;
  if QrPQECodCliInt.Value = 0 then
  begin
    Geral.MB_Aviso('Cliente interno sem cadastro em tabela de clientes internos!');
    Exit;
  end;
  //
  Result := False;
end;

procedure TFmPQE.Outros1Click(Sender: TObject);
begin
  UnPQx.GerenciaBaixaOutros(0);
end;

procedure TFmPQE.OutrosdadosdeNFnormal1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeCabA_0000, FmNFeCabA_0000, afmoNegarComAviso) then
  begin
    FmNFeCabA_0000.FForm := 'FmPQE';
    //PageControl1.ActivePageIndex := 0;
    //PageControl2.ActivePageIndex := 5;
    if QrPQECI.Value = 0 then
      Geral.MB_Aviso(
        'Para inclus�o de NF-e � necess�rio informar uma empresa!')
    else begin
      UMyMod.ConfigPanelInsUpd(stIns, FmNFeCabA_0000, FmNFeCabA_0000.PainelEdit,
        FmNFeCabA_0000.QrNFeCabA, [FmNFeCabA_0000.PainelDados], [FmNFeCabA_0000.PainelEdita],
        nil, FmNFeCabA_0000.ImgTipo, 'nfecaba');
      FmNFeCabA_0000.EdFatID.ValueVariant          := VAR_FATID_0151;
      FmNFeCabA_0000.EdFatNum.ValueVariant         := QrPQECodigo.Value;
      FmNFeCabA_0000.EdEmpresa.ValueVariant        := QrPQECI.Value;
      FmNFeCabA_0000.EdCodInfoDest.ValueVariant    := QrPQECI.Value;
      FmNFeCabA_0000.CBCodInfoDest.KeyValue        := QrPQECI.Value;
      FmNFeCabA_0000.EdCodInfoEmit.ValueVariant    := QrPQEIQ.Value;
      FmNFeCabA_0000.CBCodInfoEmit.KeyValue        := QrPQEIQ.Value;
      FmNFeCabA_0000.Edide_verProc.Text     := '';
      FmNFeCabA_0000.Edide_natOp.Text       := '';
      FmNFeCabA_0000.Edide_mod.Text         := '1';
      FmNFeCabA_0000.Edide_tpNF.Text        := '1';
      FmNFeCabA_0000.TPide_dEmi.Date        := Date;
      FmNFeCabA_0000.TPide_dSaiEnt.Date     := Date;
      FmNFeCabA_0000.TPDataFiscal.Date      := Date;
      FmNFeCabA_0000.Edversao.ValueVariant  := 0;
      //
      FmNFeCabA_0000.Edide_CUF.ValueVariant          := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(FmNFeCabA_0000.Edemit_UF.Text);
      FmNFeCabA_0000.EdICMSRec_pAliq.ValueVariant    := 7;
      FmNFeCabA_0000.EdIPIRec_pAliq.ValueVariant     := 0;
      FmNFeCabA_0000.EdPISRec_pAliq.ValueVariant     := 1.65;
      FmNFeCabA_0000.EdCOFINSRec_pAliq.ValueVariant  := 7.6;
      FmNFeCabA_0000.EdICMSRec_pRedBC.ValueVariant   := 0;
      FmNFeCabA_0000.EdIPIRec_pRedBC.ValueVariant    := 100;
      FmNFeCabA_0000.EdPISRec_pRedBC.ValueVariant    := 0;
      FmNFeCabA_0000.EdCOFINSRec_pRedBC.ValueVariant := 0;
    end;
    FmNFeCabA_0000.ShowModal;
    FmNFeCabA_0000.Destroy;
    //
    ReopenNFeCabA();
  end;
end;

procedure TFmPQE.BtExcluiClick(Sender: TObject);
var
  Continua: Boolean;
  Cancela, FatParcela,FatID: Integer;
  FatNum: Double;
begin
  Continua := False;
  Cancela  := 0;
  if UnPQx.ImpedePeloBalanco(QrPQEData.Value) then Exit;
  if PQ_PF.ImpedePeloEstoqueNF_Toda(QrPQECodigo.Value, VAR_FATID_0010) then
  begin
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
  end;
  if Geral.MB_Pergunta('Confirma a exclus�o de toda nota fiscal?') = ID_YES then
  begin
    try
      if QrEmissF.State <> dsInactive then
      begin
        QrEmissF.First;
        while not QrEmissF.Eof do
        begin
          if (QrEmissFTipo.Value = 2) and  (QrEmissFSit.Value <> 0) then
          Cancela := Cancela + 1;
          QrEmissF.Next;
        end;
      end;
      if QrEmissT.State <> dsInactive then
      begin
        QrEmissT.First;
        while not QrEmissT.Eof do
        begin
          if (QrEmissTTipo.Value = 2) and  (QrEmissTSit.Value <> 0) then
          Cancela := Cancela + 1;
          QrEmissT.Next;
        end;
      end;
      if Cancela > 0 then
      begin
        Geral.MB_Aviso('Exclus�o cancelada. Existem ' + Geral.FF0(Cancela)
        +' duplicatas que j� foram baixadas ou parcialmente pagas.');
        AtualizaEntradaPedido;
        Exit;
      end;

      // rotina de Emporium
      Dmod.QrUpdW.DataBase := Dmod.MyDB;
      Dmod.QrUpdW.SQL.Clear;
      Dmod.QrUpdW.SQL.Add('UPDATE pqe SET Cancelado = "V" WHERE Codigo=''' + Geral.FF0(QrPQECodigo.Value) + '''');
      Dmod.QrUpdW.ExecSQL;
      //
      QrPQEIts.First;
      while not QrPQEIts.Eof do
      begin
        UnPQx.AtualizaEstoquePQ(QrPQECI.Value, QrPQEItsInsumo.Value, QrPQEEmpresa.Value, aeNenhum, '');
        QrPQCli.Close;
        QrPQCli.Params[0].AsInteger := QrPQECI.Value;
        QrPQCli.Params[1].AsInteger := QrPQEItsInsumo.Value;
        UnDmkDAC_PF.AbreQuery(QrPQCli, Dmod.MyDB);
        if QrPQCliPeso.Value < 0 then Cancela := Cancela + 1
        else if QrPQCliValor.Value < 0 then Cancela := Cancela + 1;
        //
        QrPQEIts.Next;
      end;
      if Cancela > 0 then
      begin
        Geral.MB_Aviso('Exclus�o cancelada. Existem ' + Geral.FF0(Cancela)
        +' insumos que ficariam com estoque ou valor financeiro negativo.');
        Dmod.QrUpdW.DataBase := Dmod.MyDB;
        Dmod.QrUpdW.SQL.Clear;
        Dmod.QrUpdW.SQL.Add('UPDATE pqe SET Cancelado = "F" WHERE Codigo=''' + Geral.FF0(QrPQECodigo.Value) + '''');
        Dmod.QrUpdW.ExecSQL;
        QrPQEIts.First;
        while not QrPQEIts.Eof do
        begin
          UnPQx.AtualizaEstoquePQ(QrPQECI.Value, QrPQEItsInsumo.Value, QrPQEEmpresa.Value, aeNenhum, '');
          QrPQEIts.Next;
        end;
        AtualizaEntradaPedido;
        Exit;
      end;
      //
      if QrEmissF.State <> dsInactive then
      begin
        if QrEmissFFatParcRef.Value = 0 then
        begin
          if QrEmissF.RecordCount > 0 then
            UFinanceiro.ExcluiLct_FatNum(QrEmissF, QrEmissFFatID.Value,
              QrEmissFFatNum.Value, QrPQECI.Value, QrEmissFCarteira.Value,
              dmkPF.MotivDel_ValidaCodigo(314), False, FtabLctA);
          if QrEmissT.RecordCount > 0 then
            UFinanceiro.ExcluiLct_FatNum(QrEmissT, QrEmissTFatID.Value,
              QrEmissTFatNum.Value, QrPQECI.Value, QrEmissTCarteira.Value,
              dmkPF.MotivDel_ValidaCodigo(315), False, FtabLctA);
          //
          QrEmissF.First;
          while not QrEmissF.Eof do
          begin
            FatParcela := QrEmissFFatParcela.Value;
            FatNum := QrEmissFFatNum.Value;
            FatID := QrEmissFFatID.Value;
            if (FatParcela<>0) and (FatNum<>0) and (FatID<>0) then
              DmLct2.RecalcSaldoCarteira(QrEmissFTipo.Value, QrEmissFCarteira.Value,
              0, FTabLctA);
            QrEmissF.Next;
          end;

          QrEmissT.First;
          while not QrEmissT.Eof do
          begin
            FatParcela := QrEmissTFatParcela.Value;
            FatNum := QrEmissTFatNum.Value;
            FatID := QrEmissTFatID.Value;
            if (FatParcela <> 0) and (FatNum<>0) and (FatID<>0) then
              DmLct2.RecalcSaldoCarteira(QrEmissTTipo.Value, QrEmissTCarteira.Value,
              0, FTabLctA);
            QrEmissT.Next;
          end;
        end else
          Geral.MB_Info(
            'Os lan�amentos financeiros n�o foram exclu�dos pois foram emitidos no pedido!');
        Continua := True;
      end else
        Continua := true;
    except
      Continua := False;
    end;
  end;

  QrPQEIts.First;
  while not QrPQEIts.Eof do
  begin
    UnPQx.AtualizaEstoquePQ(QrPQECI.Value, QrPQEItsInsumo.Value, QrPQEEmpresa.Value, aeNenhum, '');
    QrPQEIts.Next;
  end;
  AtualizaEntradaPedido;

  if Continua = True then
  begin
    QrPQEIts.First;
    while not QrPQEIts.Eof do
    begin
      if UMyMod.ExcluiRegistroIntArr('', 'pqx',
      ['Tipo', 'OrigemCodi', 'OrigemCtrl'],
      [10, QrPQEItsCodigo.Value, QrPQEItsControle.Value](*, Dmod.MyDB*)) <>
      ID_YES then
      begin
        Continua := False;
        Exit;
      end else
      begin
        if ExcluiEfdInnNFsIts() then
        begin
          if UMyMod.ExcluiRegistroInt1('', 'pqeits', 'Controle', QrPQEItsControle.Value,
            Dmod.MyDB) <> ID_YES then
          begin
            Continua := False;
            Exit;
          end;
        end;
      end;
      UnPQx.AtualizaEstoquePQ(QrPQECI.Value, QrPQEItsInsumo.Value, QrPQEEmpresa.Value, aeNenhum, '');
      QrPQEIts.Next;
    end;
  end;

  if Continua = True then
  begin
    if ExcluiEfdInnNFsCab() then
    begin
      if ExcluiEfdInnCTsCab() then
      begin
        if UMyMod.ExcluiRegistroInt1('', 'pqe', 'Codigo', QrPQECodigo.Value,
          Dmod.MyDB) = ID_YES
        then
          Va(vpLast);
      end;
    end;
  end;
end;

procedure TFmPQE.PerguntaAQuery;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmPQEPesq, FmPQEPesq);
    FmPQEPesq.ShowModal;
    FmPQEPesq.Destroy;
  finally
    Screen.Cursor := Cursor;
  end;
  if VAR_PQELANCTO <> -1 then LocCod(VAR_PQELANCTO, VAR_PQELANCTO);
end;

procedure TFmPQE.PMAlteraItsPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrPQEIts.State <> dsInactive) and  (QrPQEIts.RecordCount > 0);
  Enab2 := (QrEmissT.State <> dsInactive) and  (QrEmissT.RecordCount > 0);
  Enab3 := (QrEmissF.State <> dsInactive) and  (QrEmissF.RecordCount > 0);
  //
  MenuItem1.Enabled := Enab;
  AlteraT1.Enabled  := Enab and Enab2;
  AlteraF1.Enabled  := Enab and Enab2;
end;

procedure TFmPQE.PMAlteraPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrPQE.State <> dsInactive) and (QrPQE.RecordCount > 0);
  AlteraDadosEstoque1.Enabled := Habilita;
  Habilita := Habilita and (QrNFeCabA.RecordCount > 0);
  AlteraDadosFiscais1.Enabled := Habilita;
  // ini 2022-03-16
  Habilita := (QrEmissF.State <> dsInactive) and (QrEmissF.RecordCount > 0);
  AlteralanamentofinanceiroFornecedor1.Enabled := Habilita;
  Habilita := (QrEmissT.State <> dsInactive) and (QrEmissT.RecordCount > 0);
  AlteralanamentofinanceiroTransportador1.Enabled := Habilita;
  // fim 2022-03-16
end;

procedure TFmPQE.PMCTePopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrPQE.State <> dsInactive) and (QrPQE.RecordCount > 0);
  IncluiConhecimentodefrete1.Enabled := Habilita;
  Habilita := Habilita and (QrEfdInnCTsCab.RecordCount > 0);
  AlteraConhecimentodefrete1.Enabled := Habilita;
  ExcluiConhecimentodefrete1.Enabled := Habilita;
end;

procedure TFmPQE.PMExcluiItsPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrPQEIts.State <> dsInactive) and  (QrPQEIts.RecordCount > 0);
  Enab2 := (QrEmissT.State <> dsInactive) and  (QrEmissT.RecordCount > 0);
  Enab3 := (QrEmissF.State <> dsInactive) and  (QrEmissF.RecordCount > 0);
  //
  ExcluiInsumo1.Enabled := Enab;
  ExcluiDuplT1.Enabled  := (*Enab and*) Enab2;
  ExcluiDuplF1.Enabled  := (*Enab and*) Enab3;
end;

procedure TFmPQE.PMFiscalPopup(Sender: TObject);
var
  Habilita1, Habilita2: Boolean;
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiDocumento1, QrPQE);
  MyObjects.HabilitaMenuItemItsDel(AlteraDocumento1, QrEfdInnNFsCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraDocumento1, QrEfdInnNFsCab);

  MyObjects.HabilitaMenuItemItsIns(IncluiItemdodocumento1, QrEfdInnNFsCab);
  MyObjects.HabilitaMenuItemItsDel(AlteraoItemselecionadododocumento1, QrEfdInnNFsIts);
  MyObjects.HabilitaMenuItemItsUpd(ExcluioItemselecionadododocumento1, QrEfdInnNFsIts);
end;

procedure TFmPQE.PMIncluiItsPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab :=(QrPQEIts.State <> dsInactive) and  (QrPQEIts.RecordCount > 0);
  //
  IncluiT.Enabled  := Enab;
  IncluiF1.Enabled := Enab;
end;

procedure TFmPQE.PMIncluiPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  OutrosdadosdeNFnormal1.Enabled :=
    (QrNFeCabA.State <> dsInactive) and (QrPQE.State <> dsInactive) and
    (QrNFeCabA.RecordCount = 0) and (QrPQEmodNF.Value = 1);
  //
  Habilita := (QrPQE.State <> dsInactive) and (QrPQE.RecordCount > 0);
  LanamentofinancxeiroapagarTransportadora1.Enabled := Habilita;
  LanamentofinancxeiroapagarFornecedor1.Enabled := Habilita;
end;

procedure TFmPQE.QrPQECalcFields(DataSet: TDataSet);
begin
  if QrPQEPesoL.Value > 0 then
  QrPQEFRETEKG.Value := QrPQEFrete.Value / QrPQEPesoL.Value
  else QrPQEFRETEKG.Value := 0;
  //
  QrPQEDataE_TXT.Value := Geral.FDT(QrPQEDataE.Value, 2);
  QrPQEDataS_TXT.Value := Geral.FDT(QrPQEDataS.Value, 2);
  QrPQEData_TXT.Value := Geral.FDT(QrPQEData.Value, 2);
end;

procedure TFmPQE.FormResize(Sender: TObject);
var
  Texto: String;
begin
  if PainelCancelado.Visible = True then
    Texto := 'ENTRADA CANCELADA'
  else
    Texto := 'Entrada de Uso e Consumo';
  //
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Texto, True, taCenter, 2, 10, 20);
end;

function TFmPQE.CalculaDiferencas(tPag: TAquemPag): Double;
var
  N, B, L, X, T: Double;
begin
  DistribuiCustoDoFrete;
(*
  QrConfT.Close;
  QrConfT.Params[0].AsInteger := QrPQECodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrConfT, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrConfT, Dmod.MyDB, [
  'SELECT SUM(TotalPeso)  PesoL, ',
  'SUM(Volumes*PesoVB) PesoB, ',
  'SUM(TotalCusto) TotalCusto, ',
  'SUM(ValorItem + IPI_vIPI) ValorItem ',
  'FROM pqeits ',
  'WHERE Codigo=' + Geral.FF0(QrPQECodigo.Value),
  '']);
  //
  T := 0;
  X := 0;
  //
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
  begin
    //Exit;  Cliente sem financeiro!
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumF, Dmod.MyDB, [
      'SELECT SUM(Debito) Debito ',
      'FROM ' + FTabLctA,
      'WHERE FatID=' + Geral.FF0(VAR_FATID_1001),
      'AND FatNum=' + Geral.FF0(QrPQECodigo.Value),
      'AND ID_Pgto = 0 ',
      '']);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumT, Dmod.MyDB, [
      'SELECT SUM(Debito) Debito ',
      'FROM ' + FTabLctA,
      'WHERE FatID=' + Geral.FF0(VAR_FATID_1002),
      'AND FatNum=' + Geral.FF0(QrPQECodigo.Value),
      'AND ID_Pgto = 0 ',
      '']);
    //
    if QrSumFDebito.Value <> 0 then
      X := QrSumFDebito.Value - QrPQEValorNF.Value;
    if QrSumTDebito.Value <> 0 then
      T := QrSumTDebito.Value - QrPQEFrete.Value;
  end;
  // 2010-07-26
  //N := QrConfTTotalCusto.Value - QrPQEValorNF.Value + QrPQEJuros.Value;
  // 2017-11-29
  N := QrConfTValorItem.Value - QrPQEValorNF.Value + QrPQEJuros.Value;
(*
  N := QrConfTTotalCusto.Value - QrPQEValorNF.Value + QrPQEJuros.Value -
       QrPQEFrete.Value + QrPQEFreteRvICMS.Value + QrPQEFreteRvPIS.Value +
       QrPQEFreteRvCOFINS.Value;
*)
  // NFe n�o tem peso bruto por item
  if QrConfTPesoB.Value = 0 then
    B := 0
  else
    B := QrConfTPesoB.Value - QrPQEPesoB.Value;
  //
  L := QrConfTPesoL.Value - QrPQEPesoL.Value;
  //
  EdTNota.Text  := Geral.TFT(FloatToStr(N), 2, siNegativo);
  EdTBruto.Text := Geral.TFT(FloatToStr(B), 2, siNegativo);
  EdTLiq.Text   := Geral.TFT(FloatToStr(L), 2, siNegativo);
  EdPagT.Text   := Geral.TFT(FloatToStr(T), 2, siNegativo);
  EdPagF.Text   := Geral.TFT(FloatToStr(X), 2, siNegativo);
  //
  if tPag = apCliente then result := X else
  if tPag = apFornece then result := X else
  if tPag = apTranspo then result := T else
  if tPag = apNinguem then result := 0 else
  //
  Result := 0;
end;

procedure TFmPQE.Consumo1Click(Sender: TObject);
begin
  UnPQx.GerenciaBaixaOutros(0);
end;

function TFmPQE.ImpedePeloRetorno(): Boolean;
begin
  Result :=  Dmod.ImpedePeloRetornoPQ(
  QrPQEItsCodigo.Value, QrPQEItsControle.Value, VAR_FATID_0010, True);
end;

procedure TFmPQE.IncluiConhecimentodefrete1Click(Sender: TObject);
begin
  MostraFormEfdInnCTsCab(stIns);
end;

procedure TFmPQE.IncluiDocumento1Click(Sender: TObject);
begin
  MostraFormEfdInnNFsCab(stIns);
end;

procedure TFmPQE.IncluiF1Click(Sender: TObject);
begin
  IncluiLctoF(Sender);
end;

procedure TFmPQE.AlteraF(Sender: TObject);
const
  Qtde = 0;
  Qtd2 = 0;
  SerieNF = '';
var
  CliInt, Terceiro, Cod, Genero: Integer;
  Valor: Double;
  GenCtbD, GenCtbC: Integer;
begin
  PCFinCtb.ActivePageIndex := 0;
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  IC3_ED_Controle := QrEmissFControle.Value;
  //
  DefineVarDup;
  //
  Genero   := Dmod.QrControleCtaPQCompr.Value;
  Cod      := QrPQECodigo.Value;
  Terceiro := QrPQEIQ.Value;
  Valor    := QrEmissFDebito.Value;
  CliInt   := QrEmissFCliInt.Value;
  //
  GenCtbD  := QrEmissFGenCtbD.Value;
  GenCtbC  := QrEmissFGenCtbC.Value;
  //
  UPagtos.Pagto(QrEmissF, tpDeb, Cod, Terceiro, VAR_FATID_1001,  0(*GenCtbD*), 0(*GenCtbC*), stUpd,
    'Compra de Insumos Qu�micos', Valor, VAR_USUARIO, 0, CliInt, mmNenhum, 0, 0,
    True, False, 0, 0, 0, 0, 0, FTabLctA, '', SerieNF, Qtde, Qtd2, GenCtbD, GenCtbC);
  SubQuery1Reopen;
end;

procedure TFmPQE.AlteraF1Click(Sender: TObject);
begin
  AlteraF(Sender);
end;

procedure TFmPQE.AlteraItensImportadosDePediVda();
var
  Codigo, Cliente, Insumo: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAltIts, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM pqeits',
  'WHERE Codigo=' + Geral.FF0(QrPQECodigo.Value),
  'ORDER BY Controle',
  '']);
  QrAltIts.First;
  while not QrAltIts.Eof do
  begin
    if QrPQEIts.Locate('Controle', QrAltItsControle.Value, []) then
    begin
       Cliente := QrPQECI.Value;
       Insumo  := QrPQEItsInsumo.Value;
       //
       AlteraSubRegistro;
       DistribuiCustoDoFrete;
       UnPQx.AtualizaEstoquePQ(Cliente, Insumo, QrPQEEmpresa.Value, aeMsg, CO_VAZIO);
       //
     end else
     begin
       ExcluiTodaEntradaPediVdaEmProgresso(Codigo);
       Close;
       Exit;
     end;
     QrAltIts.Next;
  end;
end;

procedure TFmPQE.AlteralanamentofinanceiroFornecedor1Click(Sender: TObject);
begin
  AlteraF(Sender);
end;

procedure TFmPQE.AlteralanamentofinanceiroTransportador1Click(Sender: TObject);
begin
  AlteraT(Sender);
end;

procedure TFmPQE.AlteraLctoF(Sender: TObject);
begin

end;

procedure TFmPQE.AlteraLctoT(Sender: TObject);
begin

end;

procedure TFmPQE.AlteraoItemselecionadododocumento1Click(Sender: TObject);
begin
  MostraFormEfdInnNFsIts(stUpd);
end;

procedure TFmPQE.IncluiTClick(Sender: TObject);
begin
  IncluiLctoT(Sender);
end;

procedure TFmPQE.AlteraT(Sender: TObject);
const
  Qtde = 0.000;
  Qtd2 = 0.000;
  SerieNF = '';
var
  CliInt, Terceiro, Cod, Genero: Integer;
  Valor: Double;
  GenCtbD, GenCtbC: Integer;
begin
  PCFinCtb.ActivePageIndex := 0;
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  IC3_ED_Controle := QrEmissTControle.Value;
  //
  DefineVarDup;
  //
  Genero   := Dmod.QrControleCtaPQFrete.Value;
  Cod      := QrPQECodigo.Value;
  Terceiro := QrPQETransportadora.Value;
  Valor    := QrEmissTDebito.Value;
  CliInt   := QrEmissTCliInt.Value;
  //
  GenCtbD  := QrEmissFGenCtbD.Value;
  GenCtbC  := QrEmissFGenCtbC.Value;
  //
  UPagtos.Pagto(QrEmissT, tpDeb, Cod, Terceiro, VAR_FATID_1002, 0(*GenCtbD*), 0(*GenCtbC*), stUpd,
    'Tranporte de Insumos Qu�micos', Valor, VAR_USUARIO, 0, CliInt, mmNenhum, 0,
    0, True, False, 0, 0, 0, 0, 0, FTabLctA, '', SerieNF, Qtde, Qtd2, GenCtbD, GenCtbC);
  SubQuery1Reopen;
end;

procedure TFmPQE.AlteraT1Click(Sender: TObject);
begin
  AlteraT(Sender);
end;

procedure TFmPQE.BtIncluiItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIncluiIts, BtIncluiIts);
end;

procedure TFmPQE.BtAlteraItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAlteraIts, BtAlteraIts);
end;

procedure TFmPQE.BtBaixaClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMBaixa, BtBaixa);
end;

procedure TFmPQE.BtCTe2Click(Sender: TObject);
begin
  PCFinCtb.ActivePageIndex := 2;
  MyObjects.MostraPopupDeBotao(PMCTe, TBitBtn(Sender));
end;

procedure TFmPQE.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmPQE.BtExcluiItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExcluiIts, BtExcluiIts);
end;

procedure TFmPQE.ExcluiInsumo1Click(Sender: TObject);
begin
  ExcluiSubRegistro;
end;

procedure TFmPQE.ExcluiDuplT1Click(Sender: TObject);
begin
  ExcluiItemDuplicataT;
end;

function TFmPQE.ExcluiEfdInnCTsCab: Boolean;
begin
  if (QrPQECodigo.Value <> 0) (*and (QrPQESqLinked.Value <> 0)*) then
  begin
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM efdinnctscab ',
    'WHERE MovFatID=' + Geral.FF0(FEFDInnNFsMainFatID),
    'AND MovFatNum=' + Geral.FF0(QrPQECodigo.Value),
    'AND MovimCod=' + Geral.FF0(CO_MovimCod_ZERO), // N�o precisa
    //AND SqLinked=' + Geral.FF0(QrPQESqLinked.Value),
    '']);
  end else
    Result := True; // no futuro mudar para false!
end;

(*
function TFmPQE.ExcluiEfdInnCTsIts: Boolean;
begin
 /
end;
*)

function TFmPQE.ExcluiEfdInnNFsCab(): Boolean;
begin
  if (QrPQECodigo.Value <> 0) and (QrPQESqLinked.Value <> 0) then
  begin
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM efdinnnfscab',
    'WHERE MovFatID=' + Geral.FF0(FEFDInnNFSMainFatID),
    'AND MovFatNum=' + Geral.FF0(QrPQECodigo.Value),
    'AND MovimCod=' + Geral.FF0(CO_MovimCod_ZERO),
    'AND Empresa=' + Geral.FF0(QrPQEEmpresa.Value),
    'AND SqLinked=' + Geral.FF0(QrPQESqLinked.Value),
    '']);
  end else
    Result := True; // no futuro mudar para false!
end;

function TFmPQE.ExcluiEfdInnNFsIts(): Boolean;
var
  Controle: Integer;
begin
  if (QrPQEItsControle.Value <> 0) and (QrPQEItsSqLinked.Value <> 0) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEIN, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM efdinnnfsits',
    'WHERE MovFatID=' + Geral.FF0(FEFDInnNFSMainFatID),
    'AND MovFatNum=' + Geral.FF0(QrPQECodigo.Value),
    'AND MovimCod=' + Geral.FF0(CO_MovimCod_ZERO),
    'AND Empresa=' + Geral.FF0(QrPQEEmpresa.Value),
    'AND SqLinked=' + Geral.FF0(QrPQEItsSqLinked.Value),
    'AND SqLnkID=' + Geral.FF0(QrPQEItsControle.Value),
    '']);
    Controle := QrEINControle.Value;
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM efdinnnfsits',
    'WHERE MovFatID=' + Geral.FF0(FEFDInnNFSMainFatID),
    'AND MovFatNum=' + Geral.FF0(QrPQECodigo.Value),
    'AND MovimCod=' + Geral.FF0(CO_MovimCod_ZERO),
    'AND Empresa=' + Geral.FF0(QrPQEEmpresa.Value),
    'AND SqLinked=' + Geral.FF0(QrPQEItsSqLinked.Value),
    'AND SqLnkID=' + Geral.FF0(QrPQEItsControle.Value),
    '']);
    DmNFe_0000.AtualizaTotaisEfdInnNFs(Controle);

  end else
    Result := True; // no futuro mudar para false!
end;

procedure TFmPQE.ExcluiConhecimentodefrete1Click(Sender: TObject);
var
  Controle: Integer;
begin
(*
  if QrEfdInnNFsIts.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Exclus�o abortada! Documento com item!');
  end else
  begin
*)
    if QrEfdInnCTsCab.RecordCount > 0 then
    begin
      if UMyMod.ExcluiRegistroInt1('Confirma a retirada do CT-e selecionado?',
      'EfdInnCTsCab', 'Controle', QrEfdInnCTsCabControle.Value, Dmod.MyDB) = ID_YES then
      begin
        Controle := GOTOy.LocalizaPriorNextIntQr(QrEfdInnCTsCab,
          QrEfdInnCTsCabControle, QrEfdInnCTsCabControle.Value);
        ReopenEfdInnCTsCab(Controle);
      end;
    end;
  //end;
end;

procedure TFmPQE.ExcluiDocumento1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if QrEfdInnNFsIts.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Exclus�o abortada! Documento com item!');
  end else
  begin
    if QrEfdInnNFsCab.RecordCount > 0 then
    begin
      if UMyMod.ExcluiRegistroInt1('Confirma a retirada do DF-e selecionado?',
      'EfdInnNFsCab', 'Controle', QrEfdInnNFsCabControle.Value, Dmod.MyDB) = ID_YES then
      begin
        Controle := GOTOy.LocalizaPriorNextIntQr(QrEfdInnNFsCab,
          QrEfdInnNFsCabControle, QrEfdInnNFsCabControle.Value);
        ReopenEfdInnNFsCab(Controle);
      end;
    end;
  end;
end;

procedure TFmPQE.ExcluiDuplF1Click(Sender: TObject);
begin
  ExcluiItemDuplicataF;
end;

procedure TFmPQE.ReopenPQEIts(Codigo: Integer);
begin
(*
  QrPQEIts.Close;
  QrPQEIts.Params[0].AsInteger := Codigo;
  QrPQEIts. O p e n ;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQEIts, Dmod.MyDB, [
    'SELECT pq.Nome NOMEPQ, ',
    'IF(pc.CustoPadraoAtz IS NULL, 0, pc.CustoPadraoAtz) + 0.000 CustoPadraoAtz, ',
    'IF(dFab<2, "", DATE_FORMAT(dFab, "%d/%m/%Y")) dFab_TXT, ',
    'IF(dFab<2, "", DATE_FORMAT(dVal, "%d/%m/%Y")) dVal_TXT, ei.* ',
    'FROM pqeits ei ',
    'LEFT JOIN pqe ON pqe.Codigo = ei.Codigo ',
    'LEFT JOIN pq ON pq.Codigo=ei.Insumo ',
    'LEFT JOIN pqcli pc ON (pc.PQ = ei.Insumo AND pc.CI = pqe.CI) ',
    'WHERE ei.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY Conta ',
    '']);
  //
  if FControle > 0 then QrPQEIts.Locate('Controle', FControle, []);
end;

procedure TFmPQE.ReopenretImpost();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRetImpost, Dmod.MyDB, [
  'SELECT ',
  'SUM(pqi.RvICMS)  + pqe.FreteRvICMS RvICMS, ',
  'SUM(pqi.RvPIS)  + pqe.FreteRvPIS  RvPIS, ',
  'SUM(pqi.RvCOFINS)  + pqe.FreteRvCOFINS  RvCOFINS, ',
  'SUM(pqi.RvIPI) RvIPI ',
  'FROM pqeits pqi',
  'LEFT JOIN pqe pqe ON pqe.Codigo=pqi.Codigo',
  'WHERE pqe.Codigo=' + Geral.FF0(QrPQECodigo.Value),
  '']);
end;

procedure TFmPQE.ReopenEFdInnCTsCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnCTsCab, Dmod.MyDB, [
  'SELECT vic.*, ',
  'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  ',
  'FROM efdinnctscab vic ',
  'LEFT JOIN entidades ter ON ter.Codigo=vic.Terceiro ',
  'WHERE vic.MovFatID=' + Geral.FF0(FEFDInnNFsMainFatID),
  'AND vic.MovFatNum=' + Geral.FF0(QrPQECodigo.Value),
  'AND vic.MovimCod=' + Geral.FF0(CO_MovimCod_ZERO), // N�o precisa
  ' ']);
  //
  QrEfdInnCTsCab.Locate('Controle', Controle, []);
end;

procedure TFmPQE.ReopenEFdInnNFsCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnNFsCab, Dmod.MyDB, [
  'SELECT vic.*, ',
  'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  ',
  'FROM efdinnnfscab vic ',
  'LEFT JOIN entidades ter ON ter.Codigo=vic.Terceiro ',
  'WHERE vic.MovFatID=' + Geral.FF0(FEFDInnNFSMainFatID),
  'AND vic.MovFatNum=' + Geral.FF0(QrPQECodigo.Value),
  'AND vic.MovimCod=' + Geral.FF0(CO_MovimCod_ZERO), // N�o precisa
  ' ']);
  //
  QrEfdInnNFsCab.Locate('Controle', Controle, []);
end;

procedure TFmPQE.ReopenEfdInnNFsIts(Conta: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnNFsIts, Dmod.MyDB, [
  'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM efdinnnfsits     vin',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE vin.Controle=' + Geral.FF0(QrEfdInnNFsCabControle.Value),
  '']);
  //
  QrEfdInnNFsIts.Locate('Conta', Conta, []);
end;

procedure TFmPQE.ReopenEmissF(FatParcela: Integer);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  // ini 2023-09-18
  if (QrPQENFe_FatID.Value = 51) and (QrPQENFe_FatNum.Value > 0) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmissF, Dmod.MyDB, [
      'SELECT la.*, ca.Nome NOMECARTEIRA ',
      'FROM ' + FTabLctA + ' la  ',
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
      'WHERE la.FatID=' + Geral.FF0(QrPQENFe_FatID.Value),
      'AND la.FatNum=' + Geral.FF0(QrPQENFe_FatNum.Value),
      'AND la.ID_Pgto = 0 ',
      'ORDER BY FatParcela ',
      '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmissF, Dmod.MyDB, [
      'SELECT la.*, ca.Nome NOMECARTEIRA ',
      'FROM ' + FTabLctA + ' la  ',
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
      'WHERE la.FatID=' + Geral.FF0(VAR_FATID_1001),
      'AND la.FatNum=' + Geral.FF0(QrPQECodigo.Value),
      'AND la.ID_Pgto = 0 ',
      'ORDER BY FatParcela ',
      '']);
  end;
end;

procedure TFmPQE.ReopenEmissT(FatParcela: Integer);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissT, Dmod.MyDB, [
    'SELECT la.*, ca.Nome NOMECARTEIRA ',
    'FROM ' + FTabLctA + ' la  ',
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
    'WHERE FatID=' + Geral.FF0(VAR_FATID_1002),
    'AND FatNum=' + Geral.FF0(QrPQECodigo.Value),
    'AND la.ID_Pgto = 0 ',
    'ORDER BY FatParcela ',
    '']);
end;


procedure TFmPQE.ReopenNFeCabA();
begin
  QrNFeCabA.Close;
  QrNFeCabA.SQL.Clear;
  QrNFeCabA.SQL.Add('SELECT *');
  QrNFeCabA.SQL.Add('FROM nfecaba');
  QrNFeCabA.SQL.Add('WHERE FatID in (:P0, :P1)');
  QrNFeCabA.SQL.Add('AND FatNum=:P2');
  QrNFeCabA.SQL.Add('AND Empresa=:P3');
  //QrNFeCabA.SQL.Add('AND ide_nNF=:P4');
  QrNFeCabA.SQL.Add('');
  QrNFeCabA.SQL.Add('');
  QrNFeCabA.Params[00].AsInteger := VAR_FATID_0051;
  QrNFeCabA.Params[01].AsInteger := VAR_FATID_0151;
  QrNFeCabA.Params[02].AsInteger := QrPQECodigo.Value;
  QrNFeCabA.Params[03].AsInteger := QrPQECI.Value;
  //QrNFeCabA.Params[04].AsInteger := QrFatPedNFsNumeroNF.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeCabA, Dmod.MyDB);
end;

// Parei aqui! EX na uf

end.

