object FmWBArtCab: TFmWBArtCab
  Left = 368
  Top = 194
  Caption = 'WET-RECUR-008 :: Configura'#231#227'o de Artigo Semi / Acabado'
  ClientHeight = 570
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 474
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 411
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 16
        Width = 120
        Height = 41
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 645
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 273
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 48
        Height = 13
        Caption = 'Reduzido:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 173
        Height = 13
        Caption = 'Descri'#231#227'o do artigo / tamanho / cor:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 16
        Top = 56
        Width = 91
        Height = 13
        Caption = 'Fluxo de produ'#231#227'o:'
      end
      object Label3: TLabel
        Left = 16
        Top = 96
        Width = 116
        Height = 13
        Caption = 'Receita de recurtimento:'
      end
      object Label4: TLabel
        Left = 16
        Top = 136
        Width = 189
        Height = 13
        Caption = 'Receita de recurtimento - segunda fase:'
      end
      object Label5: TLabel
        Left = 16
        Top = 176
        Width = 117
        Height = 13
        Caption = 'Receita de acabamento:'
      end
      object Label6: TLabel
        Left = 16
        Top = 216
        Width = 165
        Height = 13
        Caption = 'Classes de mat'#233'ria-prima utilizadas:'
      end
      object Label10: TLabel
        Left = 16
        Top = 256
        Width = 66
        Height = 13
        Caption = 'Observa'#231#245'es:'
      end
      object EdGraGruX: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraGruX'
        UpdCampo = 'GraGruX'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdTxtNomeTamCor: TdmkEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NO_PRD_TAM_COR'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdFluxo: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fluxo'
        UpdCampo = 'Fluxo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFluxo
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFluxo: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 697
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFluxos
        TabOrder = 3
        dmkEditCB = EdFluxo
        QryCampo = 'Fluxo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdReceiRecu: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReceiRecu'
        UpdCampo = 'ReceiRecu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBReceiRecu
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBReceiRecu: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 697
        Height = 21
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsReceiRecu
        TabOrder = 5
        dmkEditCB = EdReceiRecu
        QryCampo = 'ReceiRecu'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdReceiRefu: TdmkEditCB
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReceiRefu'
        UpdCampo = 'ReceiRefu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBReceiRefu
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBReceiRefu: TdmkDBLookupComboBox
        Left = 72
        Top = 152
        Width = 697
        Height = 21
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsReceiRefu
        TabOrder = 7
        dmkEditCB = EdReceiRefu
        QryCampo = 'ReceiRefu'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdReceiAcab: TdmkEditCB
        Left = 16
        Top = 192
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReceiAcab'
        UpdCampo = 'ReceiAcab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBReceiAcab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBReceiAcab: TdmkDBLookupComboBox
        Left = 72
        Top = 192
        Width = 697
        Height = 21
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsReceiAcab
        TabOrder = 9
        dmkEditCB = EdReceiAcab
        QryCampo = 'ReceiAcab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTxtMPs: TdmkEdit
        Left = 16
        Top = 232
        Width = 753
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'TxtMPs'
        UpdCampo = 'TxtMPs'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object MeObserva: TdmkMemo
      Left = 0
      Top = 273
      Width = 784
      Height = 72
      Align = alTop
      TabOrder = 2
      QryCampo = 'Observa'
      UpdCampo = 'Observa'
      UpdType = utYes
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 474
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 273
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label11: TLabel
        Left = 16
        Top = 56
        Width = 91
        Height = 13
        Caption = 'Fluxo de produ'#231#227'o:'
      end
      object Label12: TLabel
        Left = 16
        Top = 96
        Width = 116
        Height = 13
        Caption = 'Receita de recurtimento:'
      end
      object Label13: TLabel
        Left = 16
        Top = 136
        Width = 189
        Height = 13
        Caption = 'Receita de recurtimento - segunda fase:'
      end
      object Label14: TLabel
        Left = 16
        Top = 176
        Width = 117
        Height = 13
        Caption = 'Receita de acabamento:'
      end
      object Label15: TLabel
        Left = 16
        Top = 216
        Width = 165
        Height = 13
        Caption = 'Classes de mat'#233'ria-prima utilizadas:'
      end
      object Label16: TLabel
        Left = 16
        Top = 256
        Width = 66
        Height = 13
        Caption = 'Observa'#231#245'es:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'GraGruX'
        DataSource = DsWBArtCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsWBArtCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Fluxo'
        DataSource = DsWBArtCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'ReceiRecu'
        DataSource = DsWBArtCab
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        DataField = 'ReceiRefu'
        DataSource = DsWBArtCab
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 16
        Top = 192
        Width = 56
        Height = 21
        DataField = 'ReceiAcab'
        DataSource = DsWBArtCab
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 232
        Width = 748
        Height = 21
        DataField = 'TxtMPs'
        DataSource = DsWBArtCab
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 72
        Top = 72
        Width = 692
        Height = 21
        DataField = 'NO_Fluxo'
        DataSource = DsWBArtCab
        TabOrder = 7
      end
      object DBEdit7: TDBEdit
        Left = 72
        Top = 112
        Width = 394
        Height = 21
        DataField = 'NO_ReceiRecu'
        DataSource = DsWBArtCab
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 72
        Top = 152
        Width = 394
        Height = 21
        DataField = 'NO_ReceiRefu'
        DataSource = DsWBArtCab
        TabOrder = 9
      end
      object DBEdit9: TDBEdit
        Left = 72
        Top = 192
        Width = 394
        Height = 21
        DataField = 'NO_ReceiAcab'
        DataSource = DsWBArtCab
        TabOrder = 10
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 410
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 262
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object DBMemo1: TDBMemo
      Left = 0
      Top = 273
      Width = 784
      Height = 60
      Align = alTop
      DataField = 'Observa'
      DataSource = DsWBArtCab
      TabOrder = 2
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 476
        Height = 32
        Caption = 'Configura'#231#227'o de Artigo Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 476
        Height = 32
        Caption = 'Configura'#231#227'o de Artigo Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 476
        Height = 32
        Caption = 'Configura'#231#227'o de Artigo Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrWBArtCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrWBArtCabBeforeOpen
    AfterOpen = QrWBArtCabAfterOpen
    SQL.Strings = (
      'SELECT flu.Nome NO_Fluxo, fo1.Nome NO_ReceiRecu,'
      'fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab,'
      'wac.*, '
      'ggx.GraGru1, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM wbartcab wac'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wac.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN fluxos  flu ON flu.Codigo=wac.Fluxo'
      'LEFT JOIN formulas fo1 ON fo1.Numero=wac.ReceiRecu'
      'LEFT JOIN formulas fo2 ON fo2.Numero=wac.ReceiRefu'
      'LEFT JOIN formulas ti1 ON ti1.Numero=wac.ReceiAcab')
    Left = 92
    Top = 20
    object QrWBArtCabNO_Fluxo: TWideStringField
      FieldName = 'NO_Fluxo'
      Size = 100
    end
    object QrWBArtCabNO_ReceiRecu: TWideStringField
      FieldName = 'NO_ReceiRecu'
      Size = 30
    end
    object QrWBArtCabNO_ReceiRefu: TWideStringField
      FieldName = 'NO_ReceiRefu'
      Size = 30
    end
    object QrWBArtCabNO_ReceiAcab: TWideStringField
      FieldName = 'NO_ReceiAcab'
      Size = 30
    end
    object QrWBArtCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrWBArtCabFluxo: TIntegerField
      FieldName = 'Fluxo'
    end
    object QrWBArtCabReceiRecu: TIntegerField
      FieldName = 'ReceiRecu'
    end
    object QrWBArtCabReceiRefu: TIntegerField
      FieldName = 'ReceiRefu'
    end
    object QrWBArtCabReceiAcab: TIntegerField
      FieldName = 'ReceiAcab'
    end
    object QrWBArtCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWBArtCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWBArtCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWBArtCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWBArtCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWBArtCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWBArtCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWBArtCabGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrWBArtCabNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrWBArtCabTxtMPs: TWideStringField
      FieldName = 'TxtMPs'
      Size = 255
    end
    object QrWBArtCabObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
    end
  end
  object DsWBArtCab: TDataSource
    DataSet = QrWBArtCab
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 168
    Top = 20
  end
  object QrFluxos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM fluxos'
      'ORDER BY Nome')
    Left = 260
    Top = 48
    object QrFluxosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFluxosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsFluxos: TDataSource
    DataSet = QrFluxos
    Left = 260
    Top = 96
  end
  object QrReceiRecu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 325
    Top = 50
    object QrReceiRecuNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrReceiRecuNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsReceiRecu: TDataSource
    DataSet = QrReceiRecu
    Left = 325
    Top = 98
  end
  object QrReceirefu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 389
    Top = 50
    object QrReceirefuNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrReceirefuNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsReceiRefu: TDataSource
    DataSet = QrReceirefu
    Left = 389
    Top = 98
  end
  object QrReceiAcab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome'
      'FROM tintascab'
      'ORDER BY Nome'
      ''
      '')
    Left = 456
    Top = 48
    object QrReceiAcabNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrReceiAcabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsReceiAcab: TDataSource
    DataSet = QrReceiAcab
    Left = 456
    Top = 96
  end
end
