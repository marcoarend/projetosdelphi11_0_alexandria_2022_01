﻿unit FormulasImpShow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, (*DBTables,*) Buttons, UnInternalConsts, mySQLDbTables,
  ExtCtrls, Grids, DBGrids, ComCtrls, frxClass, frxDBSet, frxDMPExport,
  dmkGeral, MyListas, AppListas, UnDmkEnums, UnMyPrinters, UnDmkProcFunc,
  UnPQ_PF, UnVS_EFD_ICMS_IPI, UnAppEnums;

type
  TFrFormulasImpShow = class(TForm)
    QrFormulas: TmySQLQuery;
    QrFormulasNumero: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrFormulasClienteI: TIntegerField;
    QrFormulasTipificacao: TIntegerField;
    QrFormulasDataI: TDateField;
    QrFormulasDataA: TDateField;
    QrFormulasTecnico: TWideStringField;
    QrFormulasTempoR: TIntegerField;
    QrFormulasTempoP: TIntegerField;
    QrFormulasTempoT: TIntegerField;
    QrFormulasHorasR: TIntegerField;
    QrFormulasHorasP: TIntegerField;
    QrFormulasHorasT: TIntegerField;
    QrFormulasHidrica: TIntegerField;
    QrFormulasLinhaTE: TIntegerField;
    QrFormulasCaldeira: TIntegerField;
    QrFormulasSetor: TIntegerField;
    QrFormulasEspessura: TIntegerField;
    QrFormulasQtde: TFloatField;
    QrFormulasLk: TIntegerField;
    QrFormulasDataCad: TDateField;
    QrFormulasDataAlt: TDateField;
    QrFormulasUserCad: TIntegerField;
    QrFormulasUserAlt: TIntegerField;
    QrFormulasIts: TmySQLQuery;
    QrFormulasItsPESO_PQ: TFloatField;
    QrFormulasItsPRECO_PQ: TFloatField;
    QrFormulasItsCUSTO_PQ: TFloatField;
    QrFormulasItsCPRODP: TFloatField;
    QrFormulasItsCSOLUP: TFloatField;
    QrFormulasItsCUSTOP: TFloatField;
    QrFormulasItsSEQ: TIntegerField;
    Panel1: TPanel;
    Button1: TButton;
    BitBtn1: TBitBtn;
    Button2: TButton;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Edit1: TEdit;
    QrPages: TmySQLQuery;
    QrPagesPAGINAS: TIntegerField;
    QrView1: TmySQLQuery;
    QrView1Campo: TIntegerField;
    QrView1Valor: TWideStringField;
    QrView1Prefixo: TWideStringField;
    QrView1Sufixo: TWideStringField;
    QrView1Conta: TIntegerField;
    QrView1DBand: TIntegerField;
    QrView1DTopo: TIntegerField;
    QrView1DMEsq: TIntegerField;
    QrView1DComp: TIntegerField;
    QrView1DAltu: TIntegerField;
    QrView1Set_N: TIntegerField;
    QrView1Set_I: TIntegerField;
    QrView1Set_U: TIntegerField;
    QrView1Set_T: TIntegerField;
    QrView1Set_A: TIntegerField;
    QrView1Tipo: TIntegerField;
    QrView1Substituicao: TWideStringField;
    QrView1Substitui: TSmallintField;
    QrView1Nulo: TSmallintField;
    QrView1Forma: TIntegerField;
    QrView1Linha: TIntegerField;
    QrView1Colun: TIntegerField;
    QrView1Pagin: TIntegerField;
    QrTopos1: TmySQLQuery;
    QrTopos1DTopo: TIntegerField;
    QrTopos1Tipo: TIntegerField;
    QrImprimeBand: TmySQLQuery;
    QrImprimeBandCodigo: TIntegerField;
    QrImprimeBandControle: TIntegerField;
    QrImprimeBandTipo: TIntegerField;
    QrImprimeBandDataset: TIntegerField;
    QrImprimeBandPos_Topo: TIntegerField;
    QrImprimeBandPos_Altu: TIntegerField;
    QrImprimeBandNome: TWideStringField;
    QrImprimeBandLk: TIntegerField;
    QrImprimeBandDataCad: TDateField;
    QrImprimeBandDataAlt: TDateField;
    QrImprimeBandUserCad: TIntegerField;
    QrImprimeBandUserAlt: TIntegerField;
    QrImprimeBandCol_Qtd: TIntegerField;
    QrImprimeBandCol_Lar: TIntegerField;
    QrImprimeBandCol_GAP: TIntegerField;
    QrImprimeBandOrdem: TIntegerField;
    QrImprimeBandLinhas: TIntegerField;
    QrImprimeBandRepetencia: TSmallintField;
    QrImprime: TmySQLQuery;
    QrImprimeCodigo: TIntegerField;
    QrImprimeNome: TWideStringField;
    QrImprimeNomeFonte: TWideStringField;
    QrImprimeMSupDOS: TIntegerField;
    QrImprimePos_MSup2: TIntegerField;
    QrImprimePos_MEsq: TIntegerField;
    DsImprime: TDataSource;
    DsImprimeBand: TDataSource;
    QrFormulasNOMESETOR: TWideStringField;
    QrFormulasItsNome: TWideStringField;
    QrFormulasItsPQCI: TIntegerField;
    QrFormulasItsCustoPadrao: TFloatField;
    QrFormulasItsMoedaPadrao: TSmallintField;
    QrFormulasItsNumero: TIntegerField;
    QrFormulasItsOrdem: TIntegerField;
    QrFormulasItsControle: TIntegerField;
    QrFormulasItsPorcent: TFloatField;
    QrFormulasItsProduto: TIntegerField;
    QrFormulasItsTempoR: TIntegerField;
    QrFormulasItsTempoP: TIntegerField;
    QrFormulasItsObs: TWideStringField;
    QrFormulasItsSC: TIntegerField;
    QrFormulasItsReciclo: TIntegerField;
    QrFormulasItsDiluicao: TFloatField;
    QrFormulasItsCProd: TFloatField;
    QrFormulasItsCSolu: TFloatField;
    QrFormulasItsCusto: TFloatField;
    QrFormulasItsMinimo: TFloatField;
    QrFormulasItsMaximo: TFloatField;
    QrFormulasItsGraus: TFloatField;
    QrFormulasItsBoca: TWideStringField;
    QrFormulasNOMECI: TWideStringField;
    QrFormulasItsProcesso: TWideStringField;
    QrLFormIts: TmySQLQuery;
    QrLFormItsLinha: TIntegerField;
    QrLFormItsEtapa: TIntegerField;
    QrLFormItsNomeEtapa: TWideStringField;
    QrLFormItsTempoP: TIntegerField;
    QrLFormItsTempoR: TIntegerField;
    QrLFormItsObservacos: TWideStringField;
    QrLFormItsPeso: TFloatField;
    QrLFormItsDescricao: TWideStringField;
    QrLFormItsPERC_TXT: TWideStringField;
    QrLFormItsPESO_TXT: TWideStringField;
    QrLFormItsTEXT_TXT: TWideStringField;
    QrLFormItsTEMPO_TXT: TWideStringField;
    QrLFormItsSubEtapa: TIntegerField;
    QrLFormItsDiluicao: TFloatField;
    QrLFormItsTXT_DILUICAO: TWideStringField;
    QrLFormItsGraus: TFloatField;
    QrLFormItsInsumo: TIntegerField;
    QrFormulasItsObsProces: TWideStringField;
    QrLFormItsObserProce: TWideStringField;
    QrFormulasItsReordem: TIntegerField;
    QrPreuso: TmySQLQuery;
    QrFormulasItsPRODUTOCI: TIntegerField;
    QrPreusoPeso_PQ: TFloatField;
    QrPreusoProduto: TIntegerField;
    QrPreusoPQCI: TIntegerField;
    QrPreusoProdutoCI: TIntegerField;
    QrPreusoNomePQ: TWideStringField;
    QrPreusoPeso: TFloatField;
    QrPreusoPESOFUTURO: TFloatField;
    QrPreusoCli_Orig: TIntegerField;
    QrPreusoNOMECLI_ORIG: TWideStringField;
    QrPreusoDEFASAGEM: TFloatField;
    QrPreusoCodigo: TIntegerField;
    QrPreusoControle: TIntegerField;
    QrPreusoCustoPadrao: TFloatField;
    QrPreusoMoedaPadrao: TSmallintField;
    QrBaixa: TmySQLQuery;
    QrBaixaControle: TIntegerField;
    QrEmitIts: TmySQLQuery;
    QrEmitItsCodigo: TIntegerField;
    QrEmitItsControle: TIntegerField;
    QrEmitItsNomePQ: TWideStringField;
    QrEmitItsAtivo: TSmallintField;
    QrEmitItsPQCI: TIntegerField;
    QrEmitItsCustoPadrao: TFloatField;
    QrEmitItsMoedaPadrao: TIntegerField;
    QrEmitItsNumero: TIntegerField;
    QrEmitItsPorcent: TFloatField;
    QrEmitItsProduto: TIntegerField;
    QrEmitItsTempoR: TIntegerField;
    QrEmitItsTempoP: TIntegerField;
    QrEmitItsObs: TWideStringField;
    QrEmitItsCusto: TFloatField;
    QrEmitItsGraus: TFloatField;
    QrEmitItsBoca: TWideStringField;
    QrEmitItsProcesso: TWideStringField;
    QrEmitItsObsProces: TWideStringField;
    QrEmitItsLk: TIntegerField;
    QrEmitItsDataCad: TDateField;
    QrEmitItsDataAlt: TDateField;
    QrEmitItsUserCad: TIntegerField;
    QrEmitItsUserAlt: TIntegerField;
    QrEmitItsOrdem: TIntegerField;
    QrEmitItsPeso_PQ: TFloatField;
    QrEmitItsProdutoCI: TIntegerField;
    QrEmitItsCli_Orig: TIntegerField;
    QrEmitItsCli_Dest: TIntegerField;
    QrEmitItsPeso: TFloatField;
    QrEmitItsCUSTOKG: TFloatField;
    QrEmitItsNOMECLI_ORIG: TWideStringField;
    QrPreusoEXISTE: TFloatField;
    frxRec2: TfrxReport;
    frxDsFormulasIts: TfrxDBDataset;
    frxDsFormulas: TfrxDBDataset;
    frxRec1: TfrxReport;
    frxBMZ: TfrxReport;
    frxDsBMZ: TfrxDBDataset;
    frxDsLFormIts: TfrxDBDataset;
    frxPesagem1: TfrxReport;
    frxPreuso: TfrxReport;
    frxDsPreUso: TfrxDBDataset;
    QrFormulasItspH_Min: TFloatField;
    QrFormulasItspH_Max: TFloatField;
    QrFormulasItsBe_Min: TFloatField;
    QrFormulasItsBe_Max: TFloatField;
    QrEmitItspH_Min: TFloatField;
    QrEmitItspH_Max: TFloatField;
    QrEmitItsBe_Min: TFloatField;
    QrEmitItsBe_Max: TFloatField;
    QrFormulasItsAtivo: TSmallintField;
    QrLFormItspH_Min: TFloatField;
    QrLFormItspH_Max: TFloatField;
    QrLFormItsBe_Min: TFloatField;
    QrLFormItsBe_Max: TFloatField;
    QrLFormItsGC_Min: TFloatField;
    QrLFormItsGC_Max: TFloatField;
    QrFormulasItsGC_Min: TFloatField;
    QrFormulasItsGC_Max: TFloatField;
    frxRec10: TfrxReport;
    QrLFormItsCustoKg: TFloatField;
    QrLFormItsCustoIt: TFloatField;
    QrLFormItsMOSTRA_LINHA: TBooleanField;
    QrEmitItsGC_Min: TFloatField;
    QrEmitItsGC_Max: TFloatField;
    QrEmitItsPRECO_PQ: TFloatField;
    QrEmitItsCUSTO_PQ: TFloatField;
    QrLFormItsPorcent: TFloatField;
    QrLFormItsTEMPO_R_TXT: TWideStringField;
    QrLFormItsMOSTRA_GRADE: TFloatField;
    QrLotes: TmySQLQuery;
    QrFulonadas: TmySQLQuery;
    frxDsLotes: TfrxDBDataset;
    QrLotesControle: TIntegerField;
    QrLotesMPIn: TIntegerField;
    QrLotesPeso: TFloatField;
    QrLotesPecas: TFloatField;
    QrLotesFicha: TIntegerField;
    QrLotesMarca: TWideStringField;
    QrLotesLote: TWideStringField;
    QrFulonadasLote: TWideStringField;
    QrFulonadasPesoF: TFloatField;
    QrFulonadasPecasF: TFloatField;
    QrFulonadasPesoL: TFloatField;
    QrFulonadasPecasL: TFloatField;
    QrFulonadasNOMECLII: TWideStringField;
    frxDsFulonadas: TfrxDBDataset;
    QrFulonadasControle: TIntegerField;
    QrSumFul: TmySQLQuery;
    frxDsSumFul: TfrxDBDataset;
    QrFormulasItspH_All: TWideStringField;
    QrFormulasItsBe_All: TWideStringField;
    frxBMZMini: TfrxReport;
    QrLFormItsDESCRI_TEMP: TWideStringField;
    frxRec1_MS: TfrxReport;
    frxDotMatrixExport1: TfrxDotMatrixExport;
    frxReport1: TfrxReport;
    frxRec1_ML: TfrxReport;
    QrPreusoAtivo: TSmallintField;
    frxRecIdeal: TfrxReport;
    QrEmitItsDiluicao: TFloatField;
    QrEmitItsMinimo: TFloatField;
    QrEmitItsMaximo: TFloatField;
    QrFormulasItsCustoMedio: TFloatField;
    QrEmitItsCustoMedio: TFloatField;
    QrPreusoCustoMedio: TFloatField;
    QrVMIOri: TmySQLQuery;
    QrVMIOriCodigo: TLargeintField;
    QrVMIOriControle: TLargeintField;
    QrVMIOriMovimCod: TLargeintField;
    QrVMIOriMovimNiv: TLargeintField;
    QrVMIOriMovimTwn: TLargeintField;
    QrVMIOriEmpresa: TLargeintField;
    QrVMIOriTerceiro: TLargeintField;
    QrVMIOriCliVenda: TLargeintField;
    QrVMIOriMovimID: TLargeintField;
    QrVMIOriDataHora: TDateTimeField;
    QrVMIOriPallet: TLargeintField;
    QrVMIOriGraGruX: TLargeintField;
    QrVMIOriPecas: TFloatField;
    QrVMIOriPesoKg: TFloatField;
    QrVMIOriAreaM2: TFloatField;
    QrVMIOriAreaP2: TFloatField;
    QrVMIOriValorT: TFloatField;
    QrVMIOriSrcMovID: TLargeintField;
    QrVMIOriSrcNivel1: TLargeintField;
    QrVMIOriSrcNivel2: TLargeintField;
    QrVMIOriSrcGGX: TLargeintField;
    QrVMIOriSdoVrtPeca: TFloatField;
    QrVMIOriSdoVrtPeso: TFloatField;
    QrVMIOriSdoVrtArM2: TFloatField;
    QrVMIOriObserv: TWideStringField;
    QrVMIOriSerieFch: TLargeintField;
    QrVMIOriFicha: TLargeintField;
    QrVMIOriMisturou: TLargeintField;
    QrVMIOriFornecMO: TLargeintField;
    QrVMIOriCustoMOKg: TFloatField;
    QrVMIOriCustoMOM2: TFloatField;
    QrVMIOriCustoMOTot: TFloatField;
    QrVMIOriValorMP: TFloatField;
    QrVMIOriDstMovID: TLargeintField;
    QrVMIOriDstNivel1: TLargeintField;
    QrVMIOriDstNivel2: TLargeintField;
    QrVMIOriDstGGX: TLargeintField;
    QrVMIOriQtdGerPeca: TFloatField;
    QrVMIOriQtdGerPeso: TFloatField;
    QrVMIOriQtdGerArM2: TFloatField;
    QrVMIOriQtdGerArP2: TFloatField;
    QrVMIOriQtdAntPeca: TFloatField;
    QrVMIOriQtdAntPeso: TFloatField;
    QrVMIOriQtdAntArM2: TFloatField;
    QrVMIOriQtdAntArP2: TFloatField;
    QrVMIOriNotaMPAG: TFloatField;
    QrVMIOriNO_PALLET: TWideStringField;
    QrVMIOriNO_PRD_TAM_COR: TWideStringField;
    QrVMIOriNO_TTW: TWideStringField;
    QrVMIOriID_TTW: TLargeintField;
    QrVMIOriNO_FORNECE: TWideStringField;
    QrVMIOriNO_SerieFch: TWideStringField;
    QrVMIOriReqMovEstq: TLargeintField;
    QrVMIOriNO_ClientMO: TWideStringField;
    QrVMIOriMarca: TWideStringField;
    QrVMIOriClientMO: TLargeintField;
    frxDsVMIOri: TfrxDBDataset;
    frxRec1_BH: TfrxReport;
    frxPesagem2: TfrxReport;
    QrVMIOriFatorImp: TFloatField;
    QrFormulasGraCorCad: TIntegerField;
    QrFormulasNoGraCorCad: TWideStringField;
    QrFormulasEspRebaixe: TIntegerField;
    frxRec1_WE: TfrxReport;
    QrFormulasNumCODIF: TWideStringField;
    procedure Button1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure QrFormulasItsCalcFields(DataSet: TDataSet);
    procedure QrLFormItsCalcFields(DataSet: TDataSet);
    procedure QrPreusoCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure frxRec2GetValue(const VarName: String; var Value: Variant);
    procedure QrEmitItsCalcFields(DataSet: TDataSet);
    procedure QrFormulasCalcFields(DataSet: TDataSet);
    // 2014-06-12 - Falta de objetos no dfm!
    //procedure QrEmitCusAfterScroll(DataSet: TDataSet);
    // FIM 2014-06-12
  private
    { Private declarations }
    FSomaT, FLinAtu: Integer;
    FPagina: Integer;
    FArqPrn: TextFile;
    FPrintedLine: Boolean;
    FTamLinha, FAltDados: Integer;
    F_A, F_M, F_T, F_L, F_R, F_W, FPula: Integer;
    FCustoTotal, FCustokg, FCustoM2, FCustoM2Semi: Double;
    FDivisorTop, FDivisorMid, FDivisorBot: String;
    FColunas: array[0..17] of Integer;
    FColMax: Integer;
    procedure Insere_Divisor(Separador: String);
    procedure ImprimeMatricial1_1;
    procedure ImprimeMatricial1_2(Mostra: Integer);
    procedure ImprimeMatricial;
    procedure CalculaItensA;
    //procedure CalculaItensB;
    function TextoAImprimir(Campo: Integer; Tipo: Integer): String;
    function TextoAImprimirB(Campo: Integer; Tipo: Integer): String;
    function MeuGetValue(ParName: String): Variant;
    function AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
    function EspacoEntreTextos(QrView: TmySQLQuery): String;
    function Formata(Texto, Prefixo, Sufixo: String; Formato: Integer; Nulo:
             Boolean): String;
    function FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
             Substitui, Alinhamento, Comprimento, Fonte, Formatacao: Integer;
             Nulo: Boolean): String;
    function CS(Texto, Compl: String; Tamanho, Centralizacao: Integer): String;
    //function N2(Valor: Double; Tamanho: Integer): String;
    //function N3(Valor: Double; Tamanho: Integer): String;
    procedure InsereLinha(Tipo: Integer);
    function  InsereLinhaB(Tipo: Integer): String;
    procedure InsereItem(Caption: String; T, L, W, N, I, F: Integer);
    procedure PreparaDivisores;
    function  Emite(Emit: Integer): Boolean;
    function  EfetuaBaixa(QrLotes: TmySQLQuery): Boolean;
    //function  AtualizaCustosEmit(SumC: Double; PreUsoCodigo: Integer): Boolean;

    // 2014-06-12 - Falta de objetos no dfm!
    //procedure ReopenEmitCus();
    //procedure FrxRec1_Mostra();
    //procedure FrxRec1_Imprime();
    // FIM 2014-06-12

  public
    { Public declarations }
    FProgressBar1: TProgressBar;
    FEmit, FCliIntCod, FFoiExpand, FCPI, FTipoPreco, FTipoImprime, FFormula,
    FSetor, FTipific, FCod_Espess, FCod_Rebaix, FCodDefPeca, FSourcMP, FEmitGru,
    FRetrabalho: Integer;
    FReImprime, FCancela, FCalcCustos, FCalcPrecos, FNoCloseAll,
    FLogo0Exists, FMatricialNovo, FGrade: Boolean;
    FEMCM, FPesoCalc, FArea, FPeso, FMedia, FQtde, FSemiAreaM2, FSemiRendim,
    FBRL_USD, FBRL_EUR: Double;
    FDtaCambio: TDateTime;
    FHHMM_P, FHHMM_R, FHHMM_T, FDefPeca, FObs, FFulao, FLinhasRebx, FLinhasSemi,
    FCliIntTxt, FLogo0Path, FEmitGru_TXT: String;
    FDivVert: Char;
    FDataP: TDateTime;
    FDtCorrApo: String;
    FEmpresa: Integer;
    FVSMovCod, FTemIMEIMrt: Integer;
    FVSMovimSrc: TEstqmovimNiv;
    FHoraIni: TTime;
    FTpReceita: TReceitaTipoSetor;
    //
    procedure CalculaReceita(QrLotes: TmySQLQuery);
    procedure ReopenCustos1;
    procedure ReopenCustos2;
    procedure ReopenPreUso(Codigo: Integer);
    //procedure ImprimeDiferencasEstoqueNFs(Mostra: Boolean);
  end;

var
  FrFormulasImpShow: TFrFormulasImpShow;

implementation

uses UnMyObjects, Module, Principal, BlueDermConsts, UMySQLModule, PQChange, PQx,
  MyDBCheck, UnGrade_Tabs, ModEmit, DmkDAC_PF, UCreate, ModuleGeral, UnVS_PF;

{$R *.DFM}

const
  FAltLin = CO_POLEGADA / 2.16;
  FLinhaTxt = '________________________________________________________________________________________________________________________________________';
  FFloat_15_2 = '#,###,##0.00;-#,###,##0.00; ';
  FFloat_15_3 = '#,###,##0.000;-#,###,##0.000; ';
  FFloat_15_4 = '#,###,##0.0000;-#,###,##0.0000; ';

procedure TFrFormulasImpShow.ReopenCustos1;
begin
  FCustoTotal := 0;
  QrFormulasIts.First;
  while not QrFormulasIts.Eof do
  begin
    FCustoTotal := FCustoTotal + QrFormulasItsCUSTO_PQ.Value;
    QrFormulasIts.Next;
  end;
  //
  if FPeso = 0 then FCustokg := 0 else FCustokg := FCustoTotal / FPeso;
  if FArea = 0 then FCustoM2 := 0 else FCustoM2 := FCustoTotal / FArea;
  if FSemiAreaM2 = 0 then FCustoM2Semi := 0 else FCustoM2Semi := FCustoTotal / FSemiAreaM2;
  //
  QrFormulasIts.First;
end;

procedure TFrFormulasImpShow.ReopenCustos2;
begin
  FCustoTotal := 0;
  QrEmitIts.First;
  while not QrEmitIts.Eof do
  begin
    FCustoTotal := FCustoTotal + QrEmitItsCUSTO_PQ.Value;
    QrEmitIts.Next;
  end;
  //
  if FPeso = 0 then FCustokg := 0 else FCustokg := FCustoTotal / FPeso;
  if FArea = 0 then FCustoM2 := 0 else FCustoM2 := FCustoTotal / FArea;
  if FSemiAreaM2 = 0 then FCustoM2Semi := 0 else FCustoM2Semi := FCustoTotal / FSemiAreaM2;
  //
  QrEmitIts.First;
end;

(*    // 2014-06-12 - Falta de objetos no dfm!
procedure TFrFormulasImpShow.ReopenEmitCus();
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrEmitCus, Dmod.MyDB, [
  'SELECT CONCAT(mpi.Texto, " ", mpi.CorTxt) TextoECor,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_CLI,',
  'ecu.* ',
  'FROM emitcus ecu',
  'LEFT JOIN mpvits mpi ON mpi.Controle=ecu.MPVIts',
  'LEFT JOIN mpp mpp ON mpp.Codigo=mpi.Pedido ',
  'LEFT JOIN entidades ent ON ent.Codigo=mpp.Cliente ',
  'WHERE ecu.Codigo=' + Geral.FF0(FEmit),
  '']);
end;
    // FIM 2014-06-12
*)

procedure TFrFormulasImpShow.ReopenPreUso(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPreUso, Dmod.MyDB, [
  'SELECT pq.GGXNiv2 Ativo, ems.Codigo, ems.Controle,  ',
  'pqc.MoedaPadrao, ems.ProdutoCI,  ',
  'SUM(ems.Peso_PQ) Peso_PQ, ems.Produto, ems.PQCI,  ',
  'ems.NomePQ, pqc.Peso, (pqc.Peso+999999999999) EXISTE,  ',
  ' ',
  'IF(pqc.CustoPadrao > 0, pqc.CustoPadrao,  ',
  'pqc.Valor/pqc.Peso) CustoPadrao,  ',
  ' ',
  'IF((pqc.Valor/pqc.Peso>0) AND (pqc.Valor>0) AND (pqc.Peso>0),  ',
  'pqc.Valor/pqc.Peso, pqc.CustoPadrao) CustoMedio,  ',
  ' ',
  'ems.Cli_Orig, ',
  'IF(pqc.Peso IS NULL, 0, pqc.Peso) - SUM(ems.Peso_PQ) PESOFUTURO,  ',
  'IF(cli.Tipo=0, cli.RazaoSocial,cli.Nome) NOMECLI_ORIG ',
  'FROM emitits ems ',
  'LEFT JOIN PQCli     pqc ON pqc.Controle=ems.ProdutoCI ',
  'LEFT JOIN PQ     pq ON pq.Codigo=pqc.PQ ',
  'LEFT JOIN Entidades cli ON cli.Codigo=ems.Cli_Orig ',
  'WHERE ems.Ativo <> ' + Geral.FF0(CO_TipoCadPQ_003_Cod_Texto),
  'AND ems.Codigo=' + Geral.FF0(Codigo),
  'AND ems.Produto > 0',
  'GROUP BY ems.Produto, Cli_Orig ',
  ' ']);
end;

function TFrFormulasImpShow.Formata(Texto, Prefixo, Sufixo: String; Formato: Integer;
 Nulo: Boolean): String;
var
  MeuTexto: String;
begin
  if Trim(Texto) = '' then MeuTexto := Texto else
  begin
    case Formato of
      1: MeuTexto := Geral.TFT(Texto, 0, siNegativo);
      2: MeuTexto := Geral.TFT(Texto, 1, siNegativo);
      3: MeuTexto := Geral.TFT(Texto, 2, siNegativo);
      4: MeuTexto := Geral.TFT(Texto, 3, siNegativo);
      5: MeuTexto := Geral.TFT(Texto, 4, siNegativo);
      6: MeuTexto := FormatDateTime(VAR_FORMATDATE3, StrToDate(Texto));
      7: MeuTexto := FormatDateTime(VAR_FORMATDATE2, StrToDate(Texto));
      8: MeuTexto := FormatDateTime(VAR_FORMATTIME2, StrToTime(Texto));
      9: MeuTexto := FormatDateTime(VAR_FORMATTIME , StrToTime(Texto));
      else MeuTexto := Texto;
    end;
  end;
  if (Formato in ([1,2,3,4,5])) and Nulo then
  begin
    if Geral.DMV(MeuTexto) < 0.0001 then MeuTexto := '';
  end;
  if MeuTexto = '' then Result := '' else
  begin
    if Prefixo<> '' then Prefixo := Prefixo + ' ';
    if Sufixo<> '' then Sufixo := Sufixo + ' ';
    Result := Prefixo+MeuTexto+Sufixo;
  end;
end;

function TFrFormulasImpShow.FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
  Substitui, Alinhamento, Comprimento, Fonte, Formatacao: Integer; Nulo:
  Boolean): String;
var
  i, Tam, Letras, CPI, Vazios: Integer;
  Esq: Boolean;
  Txt, Vaz: String;
  Increm: Double;
begin
  if Substitui = 1 then Txt := Substituicao else
  begin
    if Texto = '' then Txt := ''
    else Txt := Formata(Texto, Prefixo, Sufixo, Formatacao, Nulo);
  end;
  if Txt = '' then
  Txt := ' '; // evitar erro de Margem Esquerda ao imprimir ??
  if FCPI = 0 then CPI := 10 else CPI := FCPI;
  Tam := Fonte + (CPI*10);
  Letras := 0;
  case Tam of
    100: Letras := 17;
    101: Letras := 10;
    102: Letras := 05;
    120: Letras := 20;
    121: Letras := 12;
    122: Letras := 06;
    else Geral.MB_Erro('CPI indefinido!');
  end;
  Increm := (CO_POLEGADA * 100) / Letras;
  Letras := Trunc(((Comprimento / 100) / CO_POLEGADA) * Letras);
  FTamLinha := FTamLinha + Trunc(Increm * Letras);
  Vazios := Letras - Length(Txt);
  Esq := True;
  // [0: Esq] [1: Centro] [2: Direita]
  if Alinhamento = 1 then
  begin
    for i := 1 to Vazios do
    begin
      if Esq then
      begin
        Txt := Txt + ' ';
        Esq := False;
      end else begin
        Txt := ' ' + Txt;
        Esq := True;
      end;
    end;
  end else begin
    for i := 1 to Vazios do Vaz := Vaz + ' ';
    if Alinhamento = 0 then Txt := Txt + Vaz else Txt := Vaz + Txt;
  end;
  Result := Txt;
end;

procedure TFrFormulasImpShow.Button1Click(Sender: TObject);
begin
  if VAR_SP <> 'Sem Baixa' then
  begin
    //frRec1.PrepareReport;
    //frRec1.SaveToFile('C:\Dermatek\'+FloatToStr(VAR_SPNO)+'.frf');
    MyObjects.frxSalva(frxRec1, 'Receita', 'C:\Dermatek\'+FloatToStr(VAR_SPNO)+'.frf');
  end;
  //frRec1.PrepareReport;
  //frRec1.ShowPreparedReport;
  //
  MyObjects.frxMostra(frxRec1, 'Receita');
end;

procedure TFrFormulasImpShow.BitBtn1Click(Sender: TObject);
begin
  Hide;
end;

procedure TFrFormulasImpShow.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFrFormulasImpShow.Button2Click(Sender: TObject);
var
  Arquivo: String;
begin
  Arquivo := 'C:\Dermatek\'+Edit1.Text+'.frf';
  if FileExists(Arquivo) then
  begin
    //frRec1.LoadFromFile(Arquivo);
    //frRec1.PrepareReport;
    //frRec1.PrintPreparedReport(CO_VAZIO,1,False, frAll);
    frxRec1.LoadFromFile(Arquivo);
    MyObjects.frxImprime(frxRec1, 'Receita (Arquivo aberto)');
  end else ShowMessage('Arquivo não localizado!');
end;

procedure TFrFormulasImpShow.CalculaReceita(QrLotes: TmySQLQuery);
var                          // estoque negativo
  SubEtapa, EtapaNum, Linha, Neg: Integer;
  EtapaTit, ObserProce, Inexiste: String;
  TabLoc_ImpBMZ, Lotes: String;
begin
  MyObjects.Informa2VAR_LABELs('Preparando cálculo da receita');
  if Trim(FDefPeca) = '' then
    FDefPeca := 'Peça';
  PreparaDivisores;
  if not(Dmod.QrControleDOSGrades.Value in ([2,3])) then FDivVert := ' '
    else FDivVert := '|';
  if not (Dmod.QrControleDOSGrades.Value in ([1,3])) then
    FPula := Dmod.QrControleDOSDotLinBot.Value else FPula := 0;

  if FVSMovCod > 0 then
    VS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(QrVMIOri, FVSMovCod, 0, FTemIMEIMrt, FVSMovimSrc(*eminSorcCal*), '')
  else
  begin
    //VS_PF.ReopenVSOpePrcOriIMEI(QrVMIOri, -999999999, 0, 0, FVSMovimSrc(*eminSorcCal*), '');
    //
    VS_PF.ReopenVSEmitCusIMEI(QrVMIOri, FEmit, 0, '');
  end;
(*
  QrFormulas.Close;
  QrFormulas.Params[0].AsInteger := FFormula;
  UnDmkDAC_PF.AbreQuery(QrFormulas, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
  'SELECT IF(ci.Tipo=0, ci.RazaoSocial, ci.Nome) NOMECI, ',
  'ls.Nome NOMESETOR, gcc.Nome NoGraCorCad, fo.* ',
  'FROM Formulas fo',
  'LEFT JOIN listasetores ls ON ls.Codigo=fo.Setor',
  'LEFT JOIN entidades    ci ON ci.Codigo=fo.ClienteI',
  'LEFT JOIN  gracorcad gcc ON gcc.Codigo=fo.GraCorCad',
  'WHERE fo.Numero=' + Geral.FF0(FFormula),
  '']);
  //
  QrFormulasIts.Close;
  QrFormulasIts.Params[0].AsInteger := FCliIntCod;
  QrFormulasIts.Params[1].AsInteger := FFormula;
  QrFormulasIts.Params[2].AsFloat   := FMedia;
  UnDmkDAC_PF.AbreQuery(QrFormulasIts, Dmod.MyDB);
  //Geral.MB_SQL(Self, QrFormulasIts);
  //
  if (FEmit > 0) and (not FReImprime) then
  begin
    if PQ_PF.InsumosSemCadastroParaClienteInterno(FCliIntCod, FEmpresa,
    (*InsumoEspecifico*)0, FFormula, TTabPqNaoCad.tpncPesagem) then Exit;
    Emite(FEmit);
    //
    Neg := 0;
    ReopenPreUso(FEmit);
    QrPreuso.First;
    Inexiste := '';
    while not QrPreuso.Eof do
    begin
      if (QrPreusoEXISTE.Value <= 0) and (not (QrPreusoAtivo.Value in ([0,3]))) then
        Inexiste := Inexiste + QrPreusoNomePQ.Value+sLineBreak;
      if QrPreusoPESOFUTURO.Value < 0 then
      Neg := Neg + 1;
      QrPreuso.Next;
    end;
    if Inexiste <> '' then
    begin
      FCancela := True;
      Geral.MB_Aviso('A empresa "' + QrPreusoNOMECLI_ORIG.Value +
      '" não está cadastrada no(s) seguinte(s) insumo(s): ' + sLineBreak +
      Inexiste);
    end else if Neg > 0 then
    begin
      if Dmod.QrControlePqNegBaixa.Value = 0 then
      begin
        if DBCheck.CriaFm(TFmPQChange, FmPQChange, afmoNegarComAviso) then
        begin
          //FmPQChange.QrBaixa.Params[0].AsInteger := FEmit;
          //FmPQChange.QrPreuso.Params[0].AsInteger := FEmit;
          FmPQChange.FEmit := FEmit;
          FmPQChange.ReopenBaixa;
          FmPQChange.ShowModal;
          FmPQChange.Destroy;
        end else begin
          Geral.MB_Aviso('Existe um ou mais insumos que não tem ' +
          'estoque suficiente para emitir esta pesagem!');
        end
      end;
    end;
    if ( (Neg > 0) and (Dmod.QrControlePQNegBaixa.Value = 0) )
    or (Inexiste <> '') then
    begin
      if not FCancela then
      begin
        Neg := 0;
        ReopenPreuso(FEmit);
        QrPreuso.First;
        while not QrPreuso.Eof do
        begin
          if QrPreusoPESOFUTURO.Value < 0 then Neg := Neg + 1;
          QrPreuso.Next;
        end;
      end;
      if ( (Neg > 0) and (Dmod.QrControlePQNegBaixa.Value = 0) )
      or FCancela then
      begin
        Screen.Cursor := crHourGlass;
        QrBaixa.Close;
        QrBaixa.Params[0].AsInteger := FEmit;
        UnDmkDAC_PF.AbreQuery(QrBaixa, Dmod.MyDB);
        if FProgressBar1 <> nil then
        begin
          FProgressBar1.Max := QrBaixa.RecordCount;
          FProgressBar1.Position := FProgressBar1.Max;
          FProgressBar1.Visible := True;
        end;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM Emit WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := FEmit;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM EmitIts WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := FEmit;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO Livres SET Tabela="EmitIts", Codigo=:P0');
        QrBaixa.First;
        while not QrBaixa.Eof do
        begin
          if FProgressBar1 <> nil then
          begin
            FProgressBar1.Position := FProgressBar1.Position - 1;
            FProgressBar1.Update;
            Application.ProcessMessages;
          end;
          //
          Dmod.QrUpd.Params[0].AsInteger := QrBaixaControle.Value;
          Dmod.QrUpd.ExecSQL;
          QrBaixa.Next;
        end;
        Screen.Cursor := crDefault;
        Exit;
      end else EfetuaBaixa(QrLotes);
    end else EfetuaBaixa(QrLotes);
  end;
  MyObjects.Informa2VAR_LABELs('Abrindo custos');
  if FEmit = 0 then ReopenCustos1 else
  begin
    QrEmitIts.Close;
    QrEmitIts.Params[0].AsInteger := FEmit;
    UnDmkDAC_PF.AbreQuery(QrEmitIts, Dmod.MyDB);
    ReopenCustos2;
  end;
  //
  MyObjects.Informa2VAR_LABELs('Preparando dados para decidir impressão');
  if FTipoImprime in ([2,3]) then
  begin
    if FMatricialNovo then ImprimeMatricial1_2(FTipoImprime)
    else ImprimeMatricial1_1;
  end else begin
    if BDC_APRESENTACAO_IMPRESSAO = 12 then
    begin
      MyObjects.frxMostra(frxRecIdeal, 'Receita de Ribeira');
    end else
    if BDC_APRESENTACAO_IMPRESSAO < 3 then
    begin
      if FCalcPrecos then
      begin
        {frRec2.PrepareReport;
        case FTipoImprime of
          0: frRec2.ShowPreparedReport;
          1: frRec2.PrintPreparedReport(CO_VAZIO,1,True, frAll);
        end;}
        //
        case FTipoImprime of
          0: MyObjects.frxMostra(frxRec2, 'Receita');
          1: MyObjects.frxImprime(frxRec2, 'Receita');
        end;
      end else begin
        case FTipoImprime of
          0:
          if FMatricialNovo then
          begin
            if FGrade then
              MyObjects.frxMostra(frxRec1_ML, 'Receita')
            else
              MyObjects.frxMostra(frxRec1_MS, 'Receita');
          end else
          begin
            //if FVSMovCod > 0 then
              if FTpReceita = TReceitaTipoSetor.rectipsetrRecurtimento then
                MyObjects.frxMostra(frxRec1_WE, 'Receita')
              else
                MyObjects.frxMostra(frxRec1_BH, 'Receita');
            //else
              //MyObjects.frxMostra(frxRec1, 'Receita');
          end;
          1:
          if FMatricialNovo then
          begin
            if FGrade then
              MyObjects.frxImprime(frxRec1_ML, 'Receita')
            else
              MyObjects.frxImprime(frxRec1_MS, 'Receita')
          end else
            //if FVSMovCod > 0 then
              MyObjects.frxImprime(frxRec1_BH, 'Receita');
            //else
              //MyObjects.frxImprime(frxRec1, 'Receita');
        end;
      end;
    end else if BDC_APRESENTACAO_IMPRESSAO in ([6,7,9,10,11]) then
    begin
      Screen.Cursor := crHourGlass;
      try
        TabLoc_ImpBMZ := UCriar.RecriaTempTableNovo(ntrttImpBMZ, DmodG.QrUpdPID1, False, 0);
        //
        DmodG.QrUpdPID1.SQL.Clear;
        DmodG.QrUpdPID1.SQL.Add('DELETE FROM ' + TabLoc_ImpBMZ);
        DmodG.QrUpdPID1.ExecSQL;
        //
        DmodG.QrUpdPID1.SQL.Clear;
        DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + TabLoc_ImpBMZ + ' SET ');
        DmodG.QrUpdPID1.SQL.Add('Etapa=:P0, NomeEtapa=:P1, TempoR=:P2, TempoP=:P3, ');
        DmodG.QrUpdPID1.SQL.Add('Observacos=:P4, Perc=:P5, Peso=:P6, Descricao=:P7, ');
        DmodG.QrUpdPID1.SQL.Add('Linha=:P8, SubEtapa=:P9, pH_Min=:P10, pH_Max=:P11, ');
        DmodG.QrUpdPID1.SQL.Add('Be_Min=:P12, Be_Max=:P13, GC_Min=:P14, GC_Max=:P5, ');
        DmodG.QrUpdPID1.SQL.Add('Diluicao=:P16, Graus=:P17, Insumo=:P18, ');
        DmodG.QrUpdPID1.SQL.Add('ObserProce=:P19, Custokg=:P20, CustoIt=:P21, ');
        DmodG.QrUpdPID1.SQL.Add('Porcent=:P22');
        //
        Linha := 0;
        EtapaNum := 0;
        SubEtapa := 0;
        EtapaTit := '';
        ObserProce := '';
        if FEmit = 0 then
        begin
          QrFormulasIts.First;
          while not QrFormulasIts.Eof do
          begin
            Linha := Linha + 1;
            if Trim(QrFormulasItsProcesso.Value) <> '' then
            begin
              EtapaNum := EtapaNum + 1;
              EtapaTit := QrFormulasItsProcesso.Value;
              ObserProce := '';
            end;
            if QrFormulasItsObsProces.Value <> '' then
            begin
              if ObserProce <> '' then ObserProce := ObserProce + '; ';
              ObserProce := ObserProce + QrFormulasItsObsProces.Value;
            end;
            DmodG.QrUpdPID1.Params[00].AsInteger := EtapaNum;
            DmodG.QrUpdPID1.Params[01].AsString  := EtapaTit;
            DmodG.QrUpdPID1.Params[02].AsInteger := QrFormulasItsTempoR.Value;
            DmodG.QrUpdPID1.Params[03].AsInteger := QrFormulasItsTempoP.Value;
            DmodG.QrUpdPID1.Params[04].AsString  := QrFormulasItsObs.Value;
            DmodG.QrUpdPID1.Params[05].AsFloat   := 0;//não funciona QrFormulasItsPorcent.Value;
            DmodG.QrUpdPID1.Params[06].AsFloat   := QrFormulasItsPESO_PQ.Value;
            DmodG.QrUpdPID1.Params[07].AsString  := QrFormulasItsNome.Value;
            DmodG.QrUpdPID1.Params[08].AsInteger := Linha;
            DmodG.QrUpdPID1.Params[09].AsInteger := SubEtapa;
            DmodG.QrUpdPID1.Params[10].AsFloat   := QrFormulasItspH_Min.Value;
            DmodG.QrUpdPID1.Params[11].AsFloat   := QrFormulasItspH_Max.Value;
            DmodG.QrUpdPID1.Params[12].AsFloat   := QrFormulasItsBe_Min.Value;
            DmodG.QrUpdPID1.Params[13].AsFloat   := QrFormulasItsBe_Max.Value;
            DmodG.QrUpdPID1.Params[14].AsFloat   := QrFormulasItsGC_Min.Value;
            DmodG.QrUpdPID1.Params[15].AsFloat   := QrFormulasItsGC_Max.Value;
            DmodG.QrUpdPID1.Params[16].AsFloat   := QrFormulasItsDiluicao.Value;
            DmodG.QrUpdPID1.Params[17].AsFloat   := QrFormulasItsGraus.Value;
            DmodG.QrUpdPID1.Params[18].AsInteger := QrFormulasItsProduto.Value;
            DmodG.QrUpdPID1.Params[19].AsString  := ObserProce;
            DmodG.QrUpdPID1.Params[20].AsFloat   := QrFormulasItsPRECO_PQ.Value;
            DmodG.QrUpdPID1.Params[21].AsFloat   := QrFormulasItsCUSTO_PQ.Value;
            DmodG.QrUpdPID1.Params[22].AsFloat   := QrFormulasItsPorcent.Value;
            DmodG.QrUpdPID1.ExecSQL;
            //
            if (QrFormulasItsTempoR.Value + QrFormulasItsTempoR.Value > 0) then
            begin
              SubEtapa := SubEtapa + 1;
              ObserProce := '';
            end;
            QrFormulasIts.Next;
          end;
        end else begin
          QrEmitIts.First;
          while not QrEmitIts.Eof do
          begin
            Linha := Linha + 1;
            if Trim(QrEmitItsProcesso.Value) <> '' then
            begin
              EtapaNum := EtapaNum + 1;
              EtapaTit := QrEmitItsProcesso.Value;
              ObserProce := '';
            end;
            ObserProce := ObserProce + QrEmitItsObsProces.Value;
            DmodG.QrUpdPID1.Params[00].AsInteger := EtapaNum;
            DmodG.QrUpdPID1.Params[01].AsString  := EtapaTit;
            DmodG.QrUpdPID1.Params[02].AsInteger := QrEmitItsTempoR.Value;
            DmodG.QrUpdPID1.Params[03].AsInteger := QrEmitItsTempoP.Value;
            DmodG.QrUpdPID1.Params[04].AsString  := QrEmitItsObs.Value;
            DmodG.QrUpdPID1.Params[05].AsFloat   := QrEmitItsPorcent.Value;
            DmodG.QrUpdPID1.Params[06].AsFloat   := QrEmitItsPESO_PQ.Value;
            DmodG.QrUpdPID1.Params[07].AsString  := QrEmitItsNomePQ.Value;
            DmodG.QrUpdPID1.Params[08].AsInteger := Linha;
            DmodG.QrUpdPID1.Params[09].AsInteger := SubEtapa;
            DmodG.QrUpdPID1.Params[10].AsFloat   := QrEmitItspH_Min.Value;
            DmodG.QrUpdPID1.Params[11].AsFloat   := QrEmitItspH_Max.Value;
            DmodG.QrUpdPID1.Params[12].AsFloat   := QrEmitItsBe_Min.Value;
            DmodG.QrUpdPID1.Params[13].AsFloat   := QrEmitItsBe_Max.Value;
            DmodG.QrUpdPID1.Params[14].AsFloat   := QrEmitItsGC_Min.Value;
            DmodG.QrUpdPID1.Params[15].AsFloat   := QrEmitItsGC_Max.Value;
            DmodG.QrUpdPID1.Params[16].AsFloat   := QrEmitItsDiluicao.Value;
            DmodG.QrUpdPID1.Params[17].AsFloat   := QrEmitItsGraus.Value;
            DmodG.QrUpdPID1.Params[18].AsInteger := QrEmitItsProduto.Value;
            DmodG.QrUpdPID1.Params[19].AsString  := ObserProce;
            DmodG.QrUpdPID1.Params[20].AsFloat   := QrEmitItsPRECO_PQ.Value;
            DmodG.QrUpdPID1.Params[21].AsFloat   := QrEmitItsCUSTO_PQ.Value;
            DmodG.QrUpdPID1.Params[22].AsFloat   := QrEmitItsPorcent.Value;
            DmodG.QrUpdPID1.ExecSQL;
            //
            if (QrEmitItsTempoR.Value + QrEmitItsTempoR.Value > 0) then
            begin
              SubEtapa := SubEtapa + 1;
              ObserProce := '';
            end;
            QrEmitIts.Next;
          end;
        end;
        if BDC_APRESENTACAO_IMPRESSAO in ([6,9,11]) then
        begin
          QrLFormIts.Close;
          QrLFormIts.Params[0].AsFloat   := -100000000;
          QrLFormIts.Params[1].AsInteger := -100000000;
          UnDmkDAC_PF.AbreQuery(QrLFormIts, DmodG.MyPID_DB);
          Screen.Cursor := crDefault;
          if BDC_APRESENTACAO_IMPRESSAO = 11 then
            MyObjects.frxMostra(frxBMZMini, 'Receita')
          else
            MyObjects.frxMostra(frxBMZ, 'Receita');
        end;
        if BDC_APRESENTACAO_IMPRESSAO in ([7,9]) then
        begin
          QrLFormIts.Close;
          QrLFormIts.Params[0].AsFloat   := 0;
          QrLFormIts.Params[1].AsInteger := 0;
          UnDmkDAC_PF.AbreQuery(QrLFormIts, DmodG.MyPID_DB);
          Screen.Cursor := crDefault;
          MyObjects.frxMostra(frxPesagem2, 'Pesagem');
        end;
        if BDC_APRESENTACAO_IMPRESSAO in ([10]) then
        begin
          QrLFormIts.Close;
          QrLFormIts.Params[0].AsFloat   := -100000000;
          QrLFormIts.Params[1].AsInteger := -100000000;
          UnDmkDAC_PF.AbreQuery(QrLFormIts, DmodG.MyPID_DB);
          //
          QrLotes.Close;
          QrLotes.Params[0].AsInteger := FEmit;
          UnDmkDAC_PF.AbreQuery(QrLotes, Dmod.MyDB);
          Lotes := '';
          while not QrLotes.Eof do
          begin
            Lotes := Lotes + ',' + FormatFloat('0', QrLotesMPIn.Value);
            QrLotes.Next;
          end;
          QrLotes.First;
          //
          if Length(Lotes) > 1 then
          begin
            Lotes := Copy(Lotes, 2, Length(Lotes));
            QrFulonadas.Close;
            QrFulonadas.SQL.Clear;
            QrFulonadas.SQL.Add('SELECT mpi.Controle, mpi.Lote, emc.Peso PesoF,');
            QrFulonadas.SQL.Add('emc.Pecas PecasF, mpi.PLE PesoL, mpi.Pecas PecasL,');
            QrFulonadas.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLII');
            QrFulonadas.SQL.Add('FROM Emit emi');
            QrFulonadas.SQL.Add('LEFT JOIN emitcus emc ON emc.Codigo=emi.Codigo');
            QrFulonadas.SQL.Add('LEFT JOIN MPIn mpi ON mpi.Controle=emc.MPIn');
            QrFulonadas.SQL.Add('LEFT JOIN Entidades ent ON ent.Codigo=mpi.ClienteI');
            QrFulonadas.SQL.Add('');
            QrFulonadas.SQL.Add('WHERE emi.Setor=' + IntToStr(FSetor));
            QrFulonadas.SQL.Add('AND emc.MPIn IN (' + Lotes + ')');
            QrFulonadas.SQL.Add('');
            QrFulonadas.SQL.Add('ORDER BY mpi.Data, mpi.Controle');
            QrFulonadas.SQL.Add('');
            UnDmkDAC_PF.AbreQuery(QrFulonadas, Dmod.MyDB);
            //
            QrSumFul.Close;
            QrSumFul.SQL.Clear;
            QrSumFul.SQL.Add('SELECT SUM(mpi.Pecas) Pecas, SUM(mpi.PLE) PLE');
            QrSumFul.SQL.Add('FROM Emit emi');
            QrSumFul.SQL.Add('LEFT JOIN emitcus emc ON emc.Codigo=emi.Codigo');
            QrSumFul.SQL.Add('LEFT JOIN MPIn mpi ON mpi.Controle=emc.MPIn');
            QrSumFul.SQL.Add('');
            QrSumFul.SQL.Add('WHERE emi.Setor=' + IntToStr(FSetor));
            QrSumFul.SQL.Add('AND emc.MPIn IN (' + Lotes + ')');
            QrSumFul.SQL.Add('');
            QrSumFul.SQL.Add('ORDER BY mpi.Data, mpi.Controle');
            QrSumFul.SQL.Add('');
            UnDmkDAC_PF.AbreQuery(QrSumFul, Dmod.MyDB);
          end;
          //
          Screen.Cursor := crDefault;
          frxRec10.Variables['LogoReceExiste'] :=
            FileExists(Dmod.QrControle.FieldByName('PathLogoRece').AsString);
          frxRec10.Variables['LogoReceCaminho'] :=
            QuotedStr(Dmod.QrControle.FieldByName('PathLogoRece').AsString);
          MyObjects.frxMostra(frxRec10, 'Receita COT-01');
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    {
    end else if BDC_APRESENTACAO_IMPRESSAO in ([10]) then
    begin
      Geral.MB_Aviso('Em construção', 'Aviso', MB_OK+MB_ICONINFORMATION);
      //
      //fazer nova tabela ?
    }
    end;
  end;
end;

procedure TFrFormulasImpShow.QrFormulasCalcFields(DataSet: TDataSet);
begin
  QrFormulasNumCODIF.Value := dmkPF.IntToCODIF_Receita(QrFormulasNumero.Value);
end;

procedure TFrFormulasImpShow.QrFormulasItsCalcFields(DataSet: TDataSet);
begin
  QrFormulasItsSEQ.Value := QrFormulasIts.RecNo;
  //
  QrFormulasItsPESO_PQ.Value := QrFormulasItsPorcent.Value / 100 * FPeso;
  //
  case FTipoPreco of
    1: // Cadastro manual
      QrFormulasItsPRECO_PQ.Value := DmModEmit.PrecoEmReais(
        QrFormulasItsMoedaPadrao.Value,
        QrFormulasItsCustoPadrao.Value, 0);
    2: // desativado
      QrFormulasItsPRECO_PQ.Value := 0;
    else   // 0: Estoque Cliente Interno
      QrFormulasItsPRECO_PQ.Value := QrFormulasItsCustoMedio.Value;
  end;
  //
  QrFormulasItsCUSTO_PQ.Value := QrFormulasItsPESO_PQ.Value *
  QrFormulasItsPRECO_PQ.Value;
  //
  if (QrFormulasItspH_Min.Value > 0) and (QrFormulasItspH_Max.Value > 0) then
    QrFormulasItspH_All.Value := FloatToStr(QrFormulasItspH_Min.Value) + '-' +
      FloatToStr(QrFormulasItspH_Max.Value)
  else if (QrFormulasItspH_Min.Value > 0) then
    QrFormulasItspH_All.Value := FloatToStr(QrFormulasItspH_Min.Value)
  else if (QrFormulasItspH_Max.Value > 0) then
    QrFormulasItspH_All.Value := FloatToStr(QrFormulasItspH_Max.Value)
  else
    QrFormulasItspH_All.Value := '';

  //

  if (QrFormulasItsBe_Min.Value > 0) and (QrFormulasItsBe_Max.Value > 0) then
    QrFormulasItsBe_All.Value := FloatToStr(QrFormulasItsBe_Min.Value) + '-' +
      FloatToStr(QrFormulasItsBe_Max.Value)
  else if (QrFormulasItsBe_Min.Value > 0) then
    QrFormulasItsBe_All.Value := FloatToStr(QrFormulasItsBe_Min.Value)
  else if (QrFormulasItsBe_Max.Value > 0) then
    QrFormulasItsBe_All.Value := FloatToStr(QrFormulasItsBe_Max.Value)
  else
    QrFormulasItsBe_All.Value := '';
end;

procedure TFrFormulasImpShow.ImprimeMatricial1_1;
{
var
  TabLoc_Imprimir1: String;
}
begin
  ShowMessage('Procedimento desativado!');
{
  Screen.Cursor := crHourGlass;
  try
    TabLoc_Imprimir1 := UCriar.RecriaTempTableNovo(ntrttImprimir1, DmodG.QrUpdPID1, False, 0);
    FPagina          := 1;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('DELETE FROM ' + TabLoc_Imprimir1);
    DmodG.QrUpdPID1.ExecSQL;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + TabLoc_Imprimir1 + ' SET ');
    DmodG.QrUpdPID1.SQL.Add('Valor=:P0, Prefixo=:P1, Sufixo=:P2, ');
    DmodG.QrUpdPID1.SQL.Add('DTopo=:P3, DMEsq=:P4, DComp=:P5, DAltu=:P6, ');
    DmodG.QrUpdPID1.SQL.Add('DBand=:P7, Set_N=:P8, Set_I=:P9, ');
    DmodG.QrUpdPID1.SQL.Add('Set_U=:P10, Set_T=:P11, Set_A=:P12, ');
    DmodG.QrUpdPID1.SQL.Add('Forma=:P13, Substitui=:P14, ');
    DmodG.QrUpdPID1.SQL.Add('Substituicao=:P15, Nulo=:P16, ');
    DmodG.QrUpdPID1.SQL.Add('Campo=:P17, Tipo=:P18, Conta=:P19, Pagin=:P20');
    CalculaItensA;
    ImprimeMatricial;
    Screen.Cursor := crDefault;
  except;
    Screen.Cursor := crDefault;
    raise;
  end;
}
end;

procedure TFrFormulasImpShow.CalculaItensA;
//const
  //TxtCol = FDivVert;
var
  Texto: String;
  LinCab: Integer;
  AltLinha: Integer;
begin
  F_M := 0;
  AltLinha := 40;
  FSomat := 0;
  FLinAtu := 0;
  LinCab := -1;
  FAltDados := CO_AltLetraDOS + Dmod.QrControleDOSDotLinTop.Value;
  //////////////////////////////////////////////////////////////////////////////
  //InsereItem(Caption,T, R, L, W, N, I, F);
  Texto := CS(String(MeuGetValue('SP')),' ', 20, 0) +
           CS('F: '+String(MeuGetValue('FU')),' ', 20, 2);
  InsereItem(Texto, 0, 0, 2030, 0, 0, 2);
  //////////////////////////////////////////////////////////////////////////////
  LinCab := LinCab+1;
  Texto := CS(String(MeuGetValue('Custo_total')),' ', 15, 0) +
           CS(String(MeuGetValue('CustoTotal')) ,' ', 15, 2);
  InsereItem(Texto, AltLinha*LinCab+15, 1665, 1000, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  //LinCab := LinCab+1;
  Texto := CS(String(MeuGetValue('Custo_ton')),' ', 15, 0) +
           CS(String(MeuGetValue('CustoTon')) ,' ', 15, 2);
  InsereItem(Texto, AltLinha*LinCab+15, 3055, 1000, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  LinCab := LinCab+1;
  Texto := CS(String(MeuGetValue('Custo_kg')),' ', 15, 0) +
           CS(String(MeuGetValue('Custokg')) ,' ', 15, 2);
  InsereItem(Texto, AltLinha*LinCab+15, 1665, 1000, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  //LinCab := LinCab+1;
  Texto := CS(String(MeuGetValue('Custo_m')),' ', 15, 0) +
           CS(String(MeuGetValue('Custom2')) ,' ', 15, 2);
  InsereItem(Texto, AltLinha*LinCab+15, 3055, 1000, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  LinCab := LinCab+1;
  Texto := CS('Receita de '+QrFormulasNOMESETOR.Value ,' ', 25, 0);
  InsereItem(Texto, AltLinha*LinCab+15, 0, 1000, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  //LinCab := LinCab+1;
  Texto := CS('Responsável: '+QrFormulasTecnico.Value ,' ', 25, 0);
  InsereItem(Texto, AltLinha*LinCab+15, 1100, 1000, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  LinCab := LinCab+1;
  Texto := CS('Receita '+IntToStr(QrFormulasNumero.Value)+
              ' - '+QrFormulasNome.Value ,' ', 80, 0);
  InsereItem(Texto, AltLinha*LinCab+15, 0, 5600, 0, 0, 1);
  //////////////////////////////////////////////////////////////////////////////
  LinCab := LinCab+1;
  Texto := CS('Rebaixe: '+FLinhasRebx+ ' Área: '+FormatFloat(FFloat_15_2, FArea)+
              ' Peso médio: '+FormatFloat(FFloat_15_2, FMedia) ,' ', 136, 0);
  InsereItem(Texto, AltLinha*LinCab+15, 0, 5600, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  LinCab := LinCab+2;
  Texto := CS('Qtde: '+FormatFloat('0', FQtde)+
              '  Peso: '+FormatFloat('#,###,##0', FPeso) ,' ', 40, 0);
  InsereItem(Texto, AltLinha*LinCab, 0, 5600, 0, 0, 2);
  //////////////////////////////////////////////////////////////////////////////
  F_M := 95;
  //////////////////////////////////////////////////////////////////////////////
  Inserelinha(0);
  QrFormulasIts.First;
  Insere_Divisor(FDivisorTop);
  while not QrFormulasIts.Eof do
  begin
    InsereLinha(1);
    QrFormulasIts.Next;
  end;
  Insere_Divisor(FDivisorBot);
  InsereLinha(2);
end;

procedure TFrFormulasImpShow.ImprimeMatricial1_2(Mostra: Integer);
var
  FArqPrn: TextFile;
  ImprimeMid: Boolean;
  Arquivo: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Screen.Cursor := crDefault;
    { 2012-02-26 não funciona em SO 64 bits
    if Mostra = 2 then if not UnIO_DLL.CheckPort($0378) then Exit;
    }
    Arquivo := 'C:\Formula'+FormatDateTime('yymmdd"_"hhmmss', Now)+'.Txt';
    if Mostra = 2 then AssignFile(FArqPrn, 'LPT1:')
      else AssignFile(FArqPrn, Arquivo);
    ReWrite(FArqPrn);
    Write(FArqPrn, Char($1B)+Char($52)+Char($12)); // Seleciona tabela de caracteres sul americana
    Write(FArqPrn, Char($1B)+Char($74)+Char($01)); // Seleciona Caracteres Graficos extendidos
    Write(FArqPrn, Char($1B)+Char($06));           // Habilita caracteres imprimíveis
    Write(FArqPrn, Char($1B)+Char($78));           // Seleciona Fonte Draft (rápida)
    Write(FArqPrn, Char($1B)+Char($0F));           // Ativa   modo condensado
    //Write(FArqPrn, Char($12));                   // Cancela modo condensado
    Write(FArqPrn, Char($1B)+Char($50));           // Seleciona 10 CPI
    //
    WriteLn(FArqPrn, FDivisorTop);
      WriteLn(FArqPrn, InsereLinhaB(0));
    QrFormulasIts.First;
    ImprimeMid := True;
    while not QrFormulasIts.Eof do
    begin
      if ImprimeMid then
        WriteLn(FArqPrn, FDivisorMid);
      ImprimeMid := True;
      WriteLn(FArqPrn, InsereLinhaB(1));
      QrFormulasIts.Next;
    end;
    WriteLn(FArqPrn, FDivisorBot);
    WriteLn(FArqPrn, InsereLinhaB(2));
    //
    Write(FArqPrn, Char($1B)+Char($40)); // ESC @  (Reseta (inicializa) impressora)
    CloseFile(FArqPrn);
  except;
    Screen.Cursor := crDefault;
    raise;
  end;
end;

(*procedure TFrFormulasImpShow.CalculaItensB;
begin
  F_M := 0;
  FSomat := 0;
  FLinAtu := 0;
  FAltDados := CO_AltLetraDOS + Dmod.QrControleDOSDotLinTop.Value;
end;*)

procedure TFrFormulasImpShow.InsereItem(Caption: String; T, L, W, N, I, F:
Integer);
begin
  F_T := Trunc(T / VAR_DOTIMP);
  F_L := Trunc(L / VAR_DOTIMP);
  F_W := Trunc(W / VAR_DOTIMP);
  F_A := F_T + F_M;
  if Trim(Caption) <> '' then
  begin
    DmodG.QrUpdPID1.Params[00].AsString  := Caption;
    DmodG.QrUpdPID1.Params[01].AsString  := '';//QrItensAPrefixo.Value;
    DmodG.QrUpdPID1.Params[02].AsString  := '';//QrItensASufixo.Value;
    DmodG.QrUpdPID1.Params[03].AsInteger := F_A;
    DmodG.QrUpdPID1.Params[04].AsInteger := F_L;//QrItensAPos_MEsq.Value+QrImprimePos_MEsq.Value;
    DmodG.QrUpdPID1.Params[05].AsInteger := F_W;//QrItensAPos_Larg.Value;
    DmodG.QrUpdPID1.Params[06].AsInteger := F_R;//QrItensAPos_Altu.Value;
    DmodG.QrUpdPID1.Params[07].AsInteger := 1;//QrItensABand.Value;
    DmodG.QrUpdPID1.Params[08].AsInteger := N;//QrItensASet_Negr.Value;
    DmodG.QrUpdPID1.Params[09].AsInteger := I;//QrItensASet_Ital.Value;
    DmodG.QrUpdPID1.Params[10].AsInteger := 0;//QrItensASet_Unde.Value;
    DmodG.QrUpdPID1.Params[11].AsInteger := F;//QrItensASet_T_DOS.Value;
    DmodG.QrUpdPID1.Params[12].AsInteger := 0;//QrItensASet_TAli.Value;
    DmodG.QrUpdPID1.Params[13].AsInteger := 0;//QrItensAFormato.Value;
    DmodG.QrUpdPID1.Params[14].AsInteger := 0;//QrItensASubstitui.Value;
    DmodG.QrUpdPID1.Params[15].AsString  := '';//QrItensASubstituicao.Value;
    DmodG.QrUpdPID1.Params[16].AsInteger := 0;//QrItensANulo.Value;
    DmodG.QrUpdPID1.Params[17].AsInteger := 0;//QrItensAControle.Value;
    DmodG.QrUpdPID1.Params[18].AsInteger := -1;//QrItensATab_Tipo.Value;
    DmodG.QrUpdPID1.Params[19].AsInteger := FLinAtu;
    DmodG.QrUpdPID1.Params[20].AsInteger := FPagina;
    DmodG.QrUpdPID1.ExecSQL;
  end;
end;

procedure TFrFormulasImpShow.InsereLinha(Tipo: Integer);
var
  Texto: String;
  z: Integer;
begin
  Insere_Divisor(FDivisorMid);
  ////////////////////////////////////////////////////////////////////////////
  FSomaT := FSomaT + FAltDados + FPula;
  F_T := Trunc(FSomaT                  / VAR_DOTIMP);
  F_R := Trunc(FAltDados               / VAR_DOTIMP);
  F_L := Trunc(0                       / VAR_DOTIMP);
  F_W := Trunc(2030                    / VAR_DOTIMP);
  ////////////////////////////////////////////////////////////////////////////
  case Tipo of
    0: for z := 1 to 17 do Texto := Texto + TextoAImprimir(z, Tipo);
    1: for z := 1 to 17 do Texto := Texto + TextoAImprimir(z, Tipo);
    2: Texto := CS(FormatDateTime('dd/mm/yy - hh:nn:ss', Now) ,' ', 136, 2);
    else Texto := '?*?';
  end;
  ////////////////////////////////////////////////////////////////////////////
  F_A := F_T + F_M;
  if Trim(Texto) <> '' then
  begin
    DModG.QrUpdPID1.Params[00].AsString  := Texto;
    DModG.QrUpdPID1.Params[01].AsString  := '';//QrItensAPrefixo.Value;
    DModG.QrUpdPID1.Params[02].AsString  := '';//QrItensASufixo.Value;
    DModG.QrUpdPID1.Params[03].AsInteger := F_A;
    DModG.QrUpdPID1.Params[04].AsInteger := F_L;//QrItensAPos_MEsq.Value+QrImprimePos_MEsq.Value;
    DModG.QrUpdPID1.Params[05].AsInteger := F_W;//QrItensAPos_Larg.Value;
    DModG.QrUpdPID1.Params[06].AsInteger := F_R;//QrItensAPos_Altu.Value;
    DModG.QrUpdPID1.Params[07].AsInteger := 1;//QrItensABand.Value;
    DModG.QrUpdPID1.Params[08].AsInteger := 0;//QrItensASet_Negr.Value;
    DModG.QrUpdPID1.Params[09].AsInteger := 0;//QrItensASet_Ital.Value;
    DModG.QrUpdPID1.Params[10].AsInteger := 0;//QrItensASet_Unde.Value;
    DModG.QrUpdPID1.Params[11].AsInteger := 0;//QrItensASet_T_DOS.Value;
    DModG.QrUpdPID1.Params[12].AsInteger := 0;//QrItensASet_TAli.Value;
    DModG.QrUpdPID1.Params[13].AsInteger := 0;//QrItensAFormato.Value;
    DModG.QrUpdPID1.Params[14].AsInteger := 0;//QrItensASubstitui.Value;
    DModG.QrUpdPID1.Params[15].AsString  := '';//QrItensASubstituicao.Value;
    DModG.QrUpdPID1.Params[16].AsInteger := 0;//QrItensANulo.Value;
    DModG.QrUpdPID1.Params[17].AsInteger := 0;//QrItensAControle.Value;
    DModG.QrUpdPID1.Params[18].AsInteger := -1;//QrItensATab_Tipo.Value;
    DModG.QrUpdPID1.Params[19].AsInteger := FLinAtu;
    DModG.QrUpdPID1.Params[20].AsInteger := FPagina;
    DModG.QrUpdPID1.ExecSQL;
  end;
end;

function TFrFormulasImpShow.InsereLinhaB(Tipo: Integer): String;
var
  Texto: String;
  z: Integer;
begin
  case Tipo of
    0: for z := 1 to 17 do Texto := Texto + TextoAImprimirB(z, Tipo);
    1: for z := 1 to 17 do Texto := Texto + TextoAImprimirB(z, Tipo);
    2: Texto := CS(FormatDateTime('dd/mm/yy - hh:nn:ss', Now) ,' ', 136, 2);
    else Texto := '?*?';
  end;
  ////////////////////////////////////////////////////////////////////////////
  Result := Texto;
end;

function TFrFormulasImpShow.TextoAImprimir(Campo: Integer; Tipo: Integer): String;
var
  Texto: String;
begin
  Texto := '';
  if Tipo=0 then
  begin
    case Campo of
      01: Texto := 'Seq.';
      02: Texto := String(MeuGetValue('Uso kg/L'));
      03: Texto := 'Uso %';
      04: Texto := 'Cód.';
      05: Texto := 'Nome do produto ou texto';
      06: Texto := 'B';
      07: Texto := '1:x';
      08: Texto := 'ºC';
      09: Texto := 'Roda';
      10: Texto := 'Pára';
      11: Texto := 'pH min';
      12: Texto := 'pH máx';
      13: Texto := 'Bé min';
      14: Texto := 'Bé máx';
      15: Texto := 'Observações';
      16: Texto := String(MeuGetValue('Ini_txt'));
      17: Texto := String(MeuGetValue('Fin_txt'));
      else Texto := '***'
    end;
  end else begin
    case Campo of
      01: Texto := FormatFloat('00', QrFormulasItsSeq.Value);
      02: Texto := String(MeuGetValue('kgLCusto'));
      03: Texto := FormatFloat('#,##0.000; ; ', QrFormulasItsPorcent.Value);
      04: Texto := FormatFloat('0; ; ', QrFormulasItsProduto.Value);
      05: Texto := QrFormulasItsNome.Value;
      06: Texto := QrFormulasItsBoca.Value;
      07: Texto := FormatFloat('0;-0; ', QrFormulasItsDiluicao.Value);
      08: Texto := FormatFloat('0;-0; ', QrFormulasItsGraus.Value);
      09: Texto := FormatFloat('0;-0; ', QrFormulasItsTempoR.Value);
      10: Texto := FormatFloat('0;-0; ', QrFormulasItsTempoP.Value);
      11: Texto := FormatFloat('0;-0; ', QrFormulasItspH_Min.Value);
      12: Texto := FormatFloat('0;-0; ', QrFormulasItspH_Max.Value);
      13: Texto := FormatFloat('0;-0; ', QrFormulasItsBe_Min.Value);
      14: Texto := FormatFloat('0;-0; ', QrFormulasItsBe_Max.Value);
      15: Texto := FormatFloat('0;-0; ', QrFormulasItsGC_Min.Value);
      16: Texto := FormatFloat('0;-0; ', QrFormulasItsGC_Max.Value);
      17: Texto := QrFormulasItsObs.Value;
      18: Texto := String(MeuGetValue('Inicio'));
      19: Texto := String(MeuGetValue('Final'));
      else Texto := '***'
    end;
  end;
  case Campo of
    01: Texto := CS(Texto, ' ', FColunas[01], 0)+FDivVert;//?? Seq.Sub
    02: Texto := CS(Texto, ' ', FColunas[02], 2)+FDivVert;//?? Peso
    03: Texto := CS(Texto, ' ', FColunas[03], 2)+FDivVert;//09 %
    04: Texto := CS(Texto, ' ', FColunas[04], 2)+FDivVert;//04 Prod
    05: Texto := CS(Texto, ' ', FColunas[05], 0)+FDivVert;//?? Nome
    06: Texto := CS(Texto, ' ', FColunas[06], 1)+FDivVert;//01 Boca
    07: Texto := CS(Texto, ' ', FColunas[07], 1)+FDivVert;//02 Diluicao
    08: Texto := CS(Texto, ' ', FColunas[08], 1)+FDivVert;//02 Graus
    09: Texto := CS(Texto, ' ', FColunas[09], 1)+FDivVert;//03 TempoR
    10: Texto := CS(Texto, ' ', FColunas[10], 1)+FDivVert;//03 TempoP
    11: Texto := CS(Texto, ' ', FColunas[11], 1)+FDivVert;//05 pH Min
    12: Texto := CS(Texto, ' ', FColunas[12], 1)+FDivVert;//05 pH Máx
    13: Texto := CS(Texto, ' ', FColunas[13], 1)+FDivVert;// Bé Min
    14: Texto := CS(Texto, ' ', FColunas[14], 1)+FDivVert;// Bé Máx
    15: Texto := CS(Texto, ' ', FColunas[13], 1)+FDivVert;// GC Min
    16: Texto := CS(Texto, ' ', FColunas[14], 1)+FDivVert;// GC Máx
    17: Texto := CS(Texto, ' ', FColunas[15], 0)+FDivVert;//?? Obs
    18: Texto := CS(Texto, ' ', FColunas[16], 1)+FDivVert;//?? Inicio
    19: Texto := CS(Texto, ' ', FColunas[17], 1)+FDivVert;//?? Final
    else Texto := '***'
  end;
  FLinAtu := FLinAtu +1;
  Result := Texto;
end;

function TFrFormulasImpShow.TextoAImprimirB(Campo: Integer; Tipo: Integer): String;
var
  Divisor: Char;
  Texto: String;
begin
  Divisor := '|';//Char(Dmod.QrControleCharSepara_11.Value);
  Texto := '';
  if Tipo=0 then
  begin
    case Campo of
      01: Texto := 'Seq.';
      02: Texto := String(MeuGetValue('Uso kg/L'));
      03: Texto := 'Uso %';
      04: Texto := 'Cód.';
      05: Texto := 'Nome do produto ou texto';
      06: Texto := 'B';
      07: Texto := '1:x';
      08: Texto := 'ºC';
      09: Texto := 'Roda';
      10: Texto := 'Pára';
      11: Texto := 'pH min';
      12: Texto := 'pH máx';
      13: Texto := 'Bé min';
      14: Texto := 'Bé máx';
      15: Texto := 'Observações';
      16: Texto := String(MeuGetValue('Ini_txt'));
      17: Texto := String(MeuGetValue('Fin_txt'));
      else Texto := '***'
    end;
  end else begin
    case Campo of
      01: Texto := FormatFloat('00', QrFormulasItsSeq.Value);
      02: Texto := String(MeuGetValue('kgLCusto'));
      03: Texto := FormatFloat('#,##0.000; ; ', QrFormulasItsPorcent.Value);
      04: Texto := FormatFloat('0; ; ', QrFormulasItsProduto.Value);
      05: Texto := QrFormulasItsNome.Value;
      06: Texto := QrFormulasItsBoca.Value;
      07: Texto := FormatFloat('0;-0; ', QrFormulasItsDiluicao.Value);
      08: Texto := FormatFloat('0;-0; ', QrFormulasItsGraus.Value);
      09: Texto := FormatFloat('0;-0; ', QrFormulasItsTempoR.Value);
      10: Texto := FormatFloat('0;-0; ', QrFormulasItsTempoP.Value);
      11: Texto := FormatFloat('0;-0; ', QrFormulasItspH_Min.Value);
      12: Texto := FormatFloat('0;-0; ', QrFormulasItspH_Max.Value);
      13: Texto := FormatFloat('0;-0; ', QrFormulasItsBe_Min.Value);
      14: Texto := FormatFloat('0;-0; ', QrFormulasItsBe_Max.Value);
      15: Texto := FormatFloat('0;-0; ', QrFormulasItsGC_Min.Value);
      16: Texto := FormatFloat('0;-0; ', QrFormulasItsGC_Max.Value);
      17: Texto := QrFormulasItsObs.Value;
      18: Texto := String(MeuGetValue('Inicio'));
      19: Texto := String(MeuGetValue('Final'));
      else Texto := '***'
    end;
  end;
  case Campo of
    01: Texto := Divisor+
                 CS(Texto, ' ', FColunas[01], 0)+Divisor;//?? Seq.Sub
    02: Texto := CS(Texto, ' ', FColunas[02], 2)+Divisor;//?? Peso
    03: Texto := CS(Texto, ' ', FColunas[03], 2)+Divisor;//09 %
    04: Texto := CS(Texto, ' ', FColunas[04], 2)+Divisor;//04 Prod
    05: Texto := CS(Texto, ' ', FColunas[05], 0)+Divisor;//?? Nome
    06: Texto := CS(Texto, ' ', FColunas[06], 1)+Divisor;//01 Boca
    07: Texto := CS(Texto, ' ', FColunas[07], 1)+Divisor;//02 Diluicao
    08: Texto := CS(Texto, ' ', FColunas[08], 1)+Divisor;//02 Graus
    09: Texto := CS(Texto, ' ', FColunas[09], 1)+Divisor;//03 TempoR
    10: Texto := CS(Texto, ' ', FColunas[10], 1)+Divisor;//03 TempoP
    11: Texto := CS(Texto, ' ', FColunas[11], 1)+Divisor;//05 pH Min
    12: Texto := CS(Texto, ' ', FColunas[12], 1)+Divisor;//05 pH Máx
    13: Texto := CS(Texto, ' ', FColunas[13], 1)+Divisor;// Bé Min
    14: Texto := CS(Texto, ' ', FColunas[14], 1)+Divisor;// Bé Máx
    15: Texto := CS(Texto, ' ', FColunas[13], 1)+Divisor;// GC Min
    16: Texto := CS(Texto, ' ', FColunas[14], 1)+Divisor;// GC Máx
    17: Texto := CS(Texto, ' ', FColunas[15], 0)+Divisor;//?? Obs
    18: Texto := CS(Texto, ' ', FColunas[16], 1)+Divisor;//?? Inicio
    19: Texto := CS(Texto, ' ', FColunas[17], 1)+Divisor;//?? Final
    else Texto := '***'
  end;
  FLinAtu := FLinAtu +1;
  Result := Texto;
end;

procedure TFrFormulasImpShow.Insere_Divisor(Separador: String); //Separador
var
  AltDivis: Integer;
begin
  if not (Dmod.QrControleDOSGrades.Value in ([1,3])) then Exit;
  AltDivis := Dmod.QrControleDOSDotLinBot.Value;
  FSomaT := FSomaT + AltDivis;
  F_L := Trunc(0                       / VAR_DOTIMP);
  F_T := Trunc(FSomaT                  / VAR_DOTIMP);
  F_W := Trunc(2030                    / VAR_DOTIMP);
  F_R := Trunc(AltDivis                / VAR_DOTIMP);
  F_A := F_T + F_M;
  FLinAtu := FLinAtu +1;
  DModG.QrUpdPID1.Params[00].AsString  := Separador;
  DModG.QrUpdPID1.Params[01].AsString  := '';//QrItensAPrefixo.Value;
  DModG.QrUpdPID1.Params[02].AsString  := '';//QrItensASufixo.Value;
  DModG.QrUpdPID1.Params[03].AsInteger := F_A;
  DModG.QrUpdPID1.Params[04].AsInteger := F_L;//QrItensAPos_MEsq.Value+QrImprimePos_MEsq.Value;
  DModG.QrUpdPID1.Params[05].AsInteger := F_W;//QrItensAPos_Larg.Value;
  DModG.QrUpdPID1.Params[06].AsInteger := F_R;//QrItensAPos_Altu.Value;
  DModG.QrUpdPID1.Params[07].AsInteger := 1;//QrItensABand.Value;
  DModG.QrUpdPID1.Params[08].AsInteger := 0;//QrItensASet_Negr.Value;
  DModG.QrUpdPID1.Params[09].AsInteger := 0;//QrItensASet_Ital.Value;
  DModG.QrUpdPID1.Params[10].AsInteger := 0;//QrItensASet_Unde.Value;
  DModG.QrUpdPID1.Params[11].AsInteger := 0;//QrItensASet_T_DOS.Value;
  DModG.QrUpdPID1.Params[12].AsInteger := 0;//QrItensASet_TAli.Value;
  DModG.QrUpdPID1.Params[13].AsInteger := 0;//QrItensAFormato.Value;
  DModG.QrUpdPID1.Params[14].AsInteger := 0;//QrItensASubstitui.Value;
  DModG.QrUpdPID1.Params[15].AsString  := '';//QrItensASubstituicao.Value;
  DModG.QrUpdPID1.Params[16].AsInteger := 0;//QrItensANulo.Value;
  DModG.QrUpdPID1.Params[17].AsInteger := 0;//QrItensAControle.Value;
  DModG.QrUpdPID1.Params[18].AsInteger := -1;//QrItensATab_Tipo.Value;
  DModG.QrUpdPID1.Params[19].AsInteger := FLinAtu;
  DModG.QrUpdPID1.Params[20].AsInteger := FPagina;
  DModG.QrUpdPID1.ExecSQL;
end;

(*
procedure TFrFormulasImpShow.ImprimeDiferencasEstoqueNFs(Mostra: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQ_NF_Err, DmodG.MyPID_DB, [
  'DROP TABLE IF EXISTS _pq_compara_estq_nfs_; ',
  'CREATE TABLE _pq_compara_estq_nfs_ ',
  'SELECT pqx.CliOrig, pqx.Insumo, ',
  'OrigemCodi, OrigemCtrl, Tipo, ',
  'SUM(pqx.SdoPeso) SdoPeso ',
  'FROM ' + TMeuDB + '.pqx pqx ',
  'WHERE pqx.SdoPeso>0 ',
  'GROUP BY pqx.CliOrig, pqx.Insumo; ',
  'SELECT pcl.Peso, pq_.Nome NO_PQ, pqx.*,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMP ',
  'FROM _pq_compara_estq_nfs_ pqx',
  'LEFT JOIN ' + TMeuDB + '.pqcli pcl ON pcl.CI=pqx.CliOrig ',
  'LEFT JOIN ' + TMeuDB + '.pq pq_ ON pq_.Codigo=pqx.Insumo',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=CliOrig',
  'WHERE pcl.PQ=pqx.Insumo  ',
  'AND pcl.Peso <> pqx.SdoPeso ',
  '']);
  if Mostra then
  begin
    MyObjects.frxDefineDataSets(frxPQ_NF_Err, [
      frxDsPQ_NF_Err,
      DModG.frxDsDono]);
  end;
  //
  MyObjects.frxMostra(frxPQ_NF_Err, 'Diferenças esoque NFs');
end;
*)

procedure TFrFormulasImpShow.ImprimeMatricial;
var
  //Cancela,
  i, BTopo, LinhaAtual, MSupDOS, Substitui: Integer;
  Nulo: Boolean;
  Arquivo, Espaco, MyCPI, Linha, FormataA, FormataI, FormataNI, FormataNF, Texto: String;
begin
    { 2012-02-26 não funciona em SO 64 bits
    if Mostra = 2 then if not UnIO_DLL.CheckPort($0378) then Exit;
    }
  Screen.Cursor := crHourGlass;
  try
    // Compatibilidade
    UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
    //
    Arquivo := 'C:\Formula'+FormatDateTime('yymmdd"_"hhmmss', Now)+'.Txt';
    if FTipoImprime = 2 then AssignFile(FArqPrn, 'LPT1:')
    else AssignFile(FArqPrn, Arquivo);
    ReWrite(FArqPrn);
    //
    //
    Write(FArqPrn, Char($1B)+Char($52)+Char($12)); // Seleciona tabela de caracteres sul americana
    Write(FArqPrn, Char($1B)+Char($74)+Char($01)); // Seleciona Caracteres Graficos extendidos
    Write(FArqPrn, Char($1B)+Char($06));           // Habilita caracteres imprimíveis
    Write(FArqPrn, Char($1B)+Char($78));           // Seleciona Fonte Draft (rápida)
    Write(FArqPrn, Char($1B)+Char($0F));           // Ativa   modo condensado
    //
    //Write(FArqPrn, #27't1'); // caracter table
    //Write(FArqPrn, #27+'%'+'1');
    Write(FArqPrn, #13);
    if FCPI = 12 then MyCPI := 'M' else MyCPI := 'P';
    Write(FArqPrn, #27+MyCPI);
    FormataA  := #15;
    FormataI  := #15;
    FormataNI := #15;
    FormataNF := #15;
    //
    QrPages.Close;
    UnDmkDAC_PF.AbreQuery(QrPages, DmodG.MyPID_DB);
    MSupDOS := QrImprimeMSupDOS.Value;
    for i := 0 to QrPagesPAGINAS.Value-1 do
    begin
      if (i < QrPagesPAGINAS.Value-1) then Substitui := 1 else Substitui := 0;
      LinhaAtual := 0;
      if i = 0 then
      begin
        if MSupDos < 0 then LinhaAtual := LinhaAtual - MSupDos else
          LinhaAtual := AvancaCarro(LinhaAtual, MSupDos);
      end;
      QrImprimeBand.Close;
      QrImprimeBand.Params[0].AsInteger := QrImprimeCodigo.Value;
      UnDmkDAC_PF.AbreQuery(QrImprimeBand, Dmod.MyDB);
      QrImprimeBand.First;
      //while not QrImprimeBand.Eof do
      //begin
        BTopo := QrImprimeBandPos_Topo.Value;
        if i > 0 then BTopo := BTopo + QrImprimePos_MSup2.Value;
        QrTopos1.Close;
        QrTopos1.Params[0].AsInteger := 1;//QrImprimeBandControle.Value;
        UnDmkDAC_PF.AbreQuery(QrTopos1, DmodG.MyPID_DB);
        QrTopos1.First;
        while not QrTopos1.Eof do
        begin
          LinhaAtual := AvancaCarro(LinhaAtual, QrTopos1DTopo.Value+BTopo);
          if not FPrintedLine then
          begin
            QrView1.Close;
            QrView1.Params[0].AsInteger := QrTopos1DTopo.Value;
            QrView1.Params[1].AsInteger := 1;//QrImprimeBandControle.Value;
            UnDmkDAC_PF.AbreQuery(QrView1, DmodG.MyPID_DB);
            QrView1.First;
            Linha := '';
            FTamLinha  := 0;
            FFoiExpand := 0;
            while not QrView1.Eof do
            begin
              case QrView1Set_T.Value of
                0: FormataA := #15;
                1: FormataA := #18;
                2: FormataA := #18#14;
              end;
              Espaco := EspacoEntreTextos(QrView1);
              if QrView1Nulo.Value = 1 then Nulo := True else Nulo := False;
                Texto := FormataTexto(Geral.SemAcento(QrView1Valor.Value),
                QrView1Prefixo.Value,
                QrView1Sufixo.Value, QrView1Substituicao.Value, Substitui *
                QrView1Substitui.Value, QrView1Set_A.Value, QrView1DComp.Value,
                QrView1Set_T.Value, QrView1Forma.Value, Nulo);
              //
              Linha := Linha + Espaco + FormataA + Texto;
              // Já usou expandido na mesma linha
              if QrView1Set_T.Value = 2 then FFoiExpand := 100;
              QrView1.Next;
            end;
            Write(FarqPrn, #27+'3'+#0);
            //WriteLn(FArqPrn, Geral.SemAcento(Linha));
            WriteLn(FArqPrn, Linha);
            FPrintedLine := True;
          end;
          QrTopos1.Next;
        end;
        //QrImprimeBand.Next;
      //end;
    end;
    Write(FArqPrn, #13);
    WriteLn(FArqPrn, #27+'0');
    Write(FArqPrn, #12);
    Write(FArqPrn, Char($1B)+Char($40)); //  Reseta
    CloseFile(FArqPrn);
    if FTipoImprime = 3 then
      Geral.MB_Aviso('Arquivo salvo: "'+Arquivo+'"');
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFrFormulasImpShow.MeuGetValue(ParName: String): Variant;
var
  CProd, CSolu, Custo, Formato: String;
  ParValue: Variant;
begin
  if FPesoCalc < 1 then
    Formato := FFloat_15_4
  else
    Formato := FFloat_15_2;
  //
  CProd := CO_VAZIO;
  CSolu := CO_VAZIO;
  Custo := CO_VAZIO;
  if FCalcCustos or FCalcPrecos then
  begin
    if QrFormulasItsCUSTO_PQ.Value > 0 then
       CProd := FormatFloat(Formato, QrFormulasItsCUSTO_PQ.Value);
    if QrFormulasItsCSolu.Value > 0 then
       CSolu := FormatFloat(Formato, QrFormulasItsCSoluP.Value);
    if QrFormulasItsCUSTO_PQ.Value > 0 then
       Custo := FormatFloat(Formato, QrFormulasItsCUSTO_PQ.Value);
    if ParName = 'Uso kg/L' then ParValue := 'Custo item';
    if ParName = 'CustoTotal' then ParValue := FormatFloat(FFloat_15_4, FCustoTotal);
    if ParName = 'CustoTon' then ParValue := FormatFloat(FFloat_15_2, FCustokg*1000);
    if ParName = 'Custokg' then ParValue := FormatFloat(FFloat_15_4, FCustokg);
    if ParName = 'Custom2_WB' then ParValue := FormatFloat(FFloat_15_4, FCustoM2);
    if ParName = 'Custom2_SA' then ParValue := FormatFloat(FFloat_15_4, FCustoM2Semi);
    if ParName = 'Custo_total' then ParValue := 'Custo total:';
    if ParName = 'Custo_ton' then ParValue := 'Custo ton:';
    if ParName = 'Custo_kg' then ParValue := 'Custo kg:';
    if ParName = 'Custo_m' then ParValue := 'Custo m²:';
    if ParName = 'Custo_ft' then ParValue := 'Custo ft²:';
    if ParName = 'Ini_txt' then ParValue := 'C.Prod.';
    if ParName = 'Fin_txt' then ParValue := 'C.Outros';
    if ParName = 'Inicio' then ParValue := CProd;
    if ParName = 'Final' then ParValue := CSolu;
    if ParName = 'kgLCusto' then
    ParValue := Custo;

  end else begin
    if QrFormulasItsPESO_PQ.Value > 0 then
{Peso}Custo := FormatFloat(FFloat_15_3,QrFormulasItsPESO_PQ.Value);
    if ParName = 'Uso kg/L' then ParValue := ParName;
    if ParName = 'CustoTotal' then ParValue := CO_VAZIO;
    if ParName = 'CustoTon' then ParValue := CO_VAZIO;
    if ParName = 'Custokg' then ParValue := CO_VAZIO;
    if ParName = 'Custom2_WB' then ParValue := CO_VAZIO;
    if ParName = 'Custom2_SA' then ParValue := CO_VAZIO;
    if ParName = 'Custo_total' then ParValue := CO_VAZIO;
    if ParName = 'Custo_ton' then ParValue := CO_VAZIO;
    if ParName = 'Custo_kg' then ParValue := CO_VAZIO;
    if ParName = 'Custo_m' then ParValue := CO_VAZIO;
    if ParName = 'Custo_ft' then ParValue := CO_VAZIO;
    if ParName = 'Ini_txt' then ParValue := 'Início';
    if ParName = 'Fin_txt' then ParValue := 'Final';
    if ParName = 'Inicio' then ParValue := CO_TempoVAZIO;
    if ParName = 'Final' then ParValue := CO_TempoVAZIO;
    if ParName = 'kgLCusto' then
    ParValue := Custo;//Peso
  end;
  if ParName = 'Espessura' then ParValue := FLinhasSemi; // compatibilidade. Tirar no futuro!
  if ParName = 'EspessuraRebx' then ParValue := FLinhasRebx;
  if ParName = 'EspessuraSemi' then ParValue := FLinhasSemi;
  if ParName = 'Qtde' then ParValue :=
                FormatFloat('#,###,##0.0',FQtde)+' '+FDefPeca;
  if ParName = 'Peso' then ParValue := FormatFloat(FFloat_15_3, FPeso)+' kg';
  if ParName = 'PesoMedio' then
    ParValue := FormatFloat(FFloat_15_3, FMedia)+' kg/'+FDefPeca;
  if ParName = 'Area' then
    ParValue := FormatFloat(FFloat_15_2, FArea);
  if ParName = 'FU' then ParValue := FFulao;
  if ParName = 'OS' then ParValue := '';//VAR_OS;
  if ParName = 'SP' then ParValue := VAR_SP;
  //if ParName = 'Lotes' then ParValue := Geral.Substitui(FObs, sLineBreak, ' ');
  if ParName = 'Lotes' then ParValue := FOBs;//Geral.Substitui(FObs, sLineBreak, ' ');
  Result := ParValue;
end;

function TFrFormulasImpShow.AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
var
  LinAtu: Integer;
begin
  NovaPosicao := Trunc(NovaPosicao * 250 / 90);
  if PosicaoAtual < NovaPosicao then
  begin
    LinAtu := Trunc((NovaPosicao - PosicaoAtual) / FAltLin);
    while LinAtu > 0 do
    begin
      if LinAtu > 216 then
      begin
        Write(FArqPrn, #27+'3'+#216);
        LinAtu := LinAtu - 216;
        WriteLn(FArqPrn,' ');
      end else begin
        Write(FArqPrn, MyPrinters.DefAlturaDotImp(3, LinAtu, ''));
        WriteLn(FArqPrn,' ');
        LinAtu := 0;
      end;
    end;
  end;
  Result := NovaPosicao;
  FPrintedLine := False;
end;

function TFrFormulasImpShow.EspacoEntreTextos(QrView: TmySQLQuery): String;
var
  Texto: String;
  i, CPI, Letras: Integer;
  Soma: Double;
begin
  Texto := '';
  if FTamLinha > QrView.FieldByName('DMEsq').AsInteger then
  begin
    // Erro !!!
  end;
  if FTamLinha < QrView.FieldByName('DMEsq').AsInteger then
  begin
    Texto := #15;
    if FCPI = 0 then CPI := 10 else CPI := FCPI;
    CPI := CPI + FFoiExpand;
    case CPI of
     010: CPI := 17;
     012: CPI := 20;
     110: CPI := 10;
     112: CPI := 10;
    end;
    Soma := (CO_POLEGADA * 100)/ CPI;
    Letras := Trunc((QrView.FieldByName('DMEsq').AsInteger - FTamLinha) / (CO_POLEGADA * 100) * CPI);
    for i := 1 to Letras do Texto := Texto + ' ';
    FTamLinha := FTamLinha + Trunc(Soma * Letras);
  end;
  Result := Texto;
end;

function TFrFormulasImpShow.CS(Texto, Compl: String; Tamanho, Centralizacao: Integer): String;
var
  Alinha: TAlignment;
begin
  if Length(Texto) > Tamanho then
  begin
    if Compl = '0' then Texto := Copy(Texto, Length(Texto)-Tamanho+1, Tamanho)
    else Texto := Copy(Texto, 1, Tamanho);
  end;
  case Centralizacao of
    0: Alinha := taLeftJustify;
    1: Alinha := taCenter;
    2: Alinha := taRightJustify;
    else Alinha := taLeftJustify;
  end;
  Result := Geral.CompletaString(Texto, Compl, Tamanho, Alinha, False);
end;

procedure TFrFormulasImpShow.PreparaDivisores;
var
  //i, j: Integer;
  FArqPrn: TextFile;
const
  Arquivo = 'C:\Dermatek\CarcteresUnicode.txt';
begin
  FColMax := 15;
  FColunas[01] := 05;
  FColunas[02] := 10;
  FColunas[03] := 10;
  FColunas[04] := 05;
  FColunas[05] := 26;
  FColunas[06] := 02;
  FColunas[07] := 03;
  FColunas[08] := 03;
  FColunas[09] := 04;
  FColunas[10] := 04;
  FColunas[11] := 05;
  FColunas[12] := 05;
  FColunas[13] := 05;
  FColunas[14] := 05;
  FColunas[15] := 24;
  FColunas[16] := 07;
  FColunas[17] := 07;
  //
  FDivisorTop := '_';//Char(Dmod.QrControleCharSepara_01.Value);
  FDivisorMid := '|';//Char(Dmod.QrControleCharSepara_04.Value);
  FDivisorBot := '_';//Char(Dmod.QrControleCharSepara_07.Value);
  (*for i := 1 to FColMax do
  begin
    for j := 1 to FColunas[i] do
    begin
      FDivisorTop := FDivisorTop + Char(Dmod.QrControleCharSepara_10.Value);
      FDivisorMid := FDivisorMid + Char(Dmod.QrControleCharSepara_10.Value);
      FDivisorBot := FDivisorBot + Char(Dmod.QrControleCharSepara_10.Value);
    end;
    if i < FColMax then
    begin
      FDivisorTop := FDivisorTop + Char(Dmod.QrControleCharSepara_02.Value);
      FDivisorMid := FDivisorMid + Char(Dmod.QrControleCharSepara_05.Value);
      FDivisorBot := FDivisorBot + Char(Dmod.QrControleCharSepara_08.Value);
    end;
  end;
  FDivisorTop := FDivisorTop + Char(Dmod.QrControleCharSepara_03.Value);
  FDivisorMid := FDivisorMid + Char(Dmod.QrControleCharSepara_06.Value);
  FDivisorBot := FDivisorBot + Char(Dmod.QrControleCharSepara_09.Value);
  //*)
  AssignFile(FArqPrn, Arquivo);
  ReWrite(FArqPrn);
  Write(FArqPrn, Char($1B)+Char($52)+Char($12)); // Seleciona tabela de caracteres sul americana
  Write(FArqPrn, Char($1B)+Char($74)+Char($01)); // Seleciona Caracteres Graficos extendidos
  Write(FArqPrn, Char($1B)+Char($06));           // Habilita caracteres imprimíveis
  Write(FArqPrn, Char($1B)+Char($78));           // Seleciona Fonte Draft (rápida)
  Write(FArqPrn, Char($1B)+Char($0F));           // Ativa   modo condensado
  //Write(FArqPrn, Char($12));                   // Cancela modo condensado
  Write(FArqPrn, Char($1B)+Char($50));           // Seleciona 10 CPI
  //
  WriteLn(FArqPrn, FDivisorTop);
  WriteLn(FArqPrn, FDivisorMid);
  WriteLn(FArqPrn, FDivisorBot);
  WriteLn(FArqPrn, '');
  (*WriteLn(FArqPrn, Char(C01)+Char(C10)+Char(C02)+Char(C10)+Char(C03));
  WriteLn(FArqPrn, Char(C11)+Char(C_A)+Char(C11)+Char(C_B)+Char(C11));
  WriteLn(FArqPrn, Char(C04)+Char(C10)+Char(C05)+Char(C10)+Char(C06));
  WriteLn(FArqPrn, Char(C11)+Char(C_C)+Char(C11)+Char(C_D)+Char(C11));
  WriteLn(FArqPrn, Char(C07)+Char(C10)+Char(C08)+Char(C10)+Char(C09));*)
  //
  Write(FArqPrn, Char($1B)+Char($40));           // ESC @  (Reseta (inicializa) impressora)
  CloseFile(FArqPrn);
end;

procedure TFrFormulasImpShow.QrLFormItsCalcFields(DataSet: TDataSet);
var
  Tempo, Min, Max, Medida: String;
begin
  if QrLFormItsPorcent.Value <= 0 then QrLFormItsPERC_TXT.Value := '' else
  QrLFormItsPERC_TXT.Value :=
    Geral.FFT(QrLFormItsPorcent.Value, 3, siPositivo) + ' %';
  //
  if QrLFormItsInsumo.Value > 0 then Medida := ' kg' else Medida := ' L ';
  if QrLFormItsPeso.Value <= 0 then QrLFormItsPESO_TXT.Value := '' else
  QrLFormItsPESO_TXT.Value := Geral.FFT(QrLFormItsPeso.Value, 3, siPositivo)
  + Medida;
  //
  {if BDC_APRESENTACAO_IMPRESSAO = 10 then
    QrLFormItsTEXT_TXT.Value := '- * - * -'
  else}
    QrLFormItsTEXT_TXT.Value := QrLFormItsPERC_TXT.Value +
      QrLFormItsPESO_TXT.Value + QrLFormItsDescricao.Value +
      QrLFormItsObservacos.Value;

  if BDC_APRESENTACAO_IMPRESSAO = 10 then
    QrLFormItsMOSTRA_LINHA.Value := True
  else
    QrLFormItsMOSTRA_LINHA.Value := Trim(QrLFormItsPERC_TXT.Value +
      QrLFormItsPESO_TXT.Value + QrLFormItsDescricao.Value +
      QrLFormItsObservacos.Value) <> '';

  if Trim(QrLFormItsPERC_TXT.Value +
    QrLFormItsPESO_TXT.Value + QrLFormItsDescricao.Value) <> '' then
      QrLFormItsMOSTRA_GRADE.Value := 0.1
    else
      QrLFormItsMOSTRA_GRADE.Value := 0;

  if QrLFormItsDiluicao.Value = 0 then QrLFormItsTXT_DILUICAO.Value := ''
  else QrLFormItsTXT_DILUICAO.Value := 'Diluir em '+Geral.FFT(
    QrLFormItsPeso.Value * QrLFormItsDiluicao.Value, 3, siPositivo)+
    ' litros de água.';
  //
  Tempo := '';
  if QrLFormItsTempoR.Value > 0 then Tempo := Tempo +
    'Rodar ' + dmkPF.HorasMHT(QrLFormItsTempoR.Value, True);
  if (QrLFormItsTempoR.Value > 0) and (QrLFormItsTempoP.Value > 0) then
    Tempo := Tempo + ' e ';
  if QrLFormItsTempoP.Value > 0 then Tempo := Tempo +
    'Parar ' + dmkPF.HorasMHT(QrLFormItsTempoP.Value, True);
  if (QrLFormItsTempoR.Value + QrLFormItsTempoR.Value) > 0 then
    Tempo := Tempo + '    Início: ________ Final: ________';
  if (QrLFormItspH_Min.Value <> 0) or (QrLFormItspH_Max.Value <> 0) then
  begin
    Min := Geral.FFT(QrLFormItspH_Min.Value, 2, siPositivo);
    Max := Geral.FFT(QrLFormItspH_Max.Value, 2, siPositivo);
    if (QrLFormItspH_Min.Value <> 0) and (QrLFormItspH_Max.Value <> 0) then
      Tempo := Tempo + ' pH ('+ Min + ' a ' +Max + '):__________'
    else if (QrLFormItspH_Min.Value <> 0) then
      Tempo := Tempo + ' pH ('+Min + '):__________'
    else Tempo := Tempo + ' pH ('+Max + '):__________';
  end;
  if (QrLFormItsBe_Min.Value <> 0) or (QrLFormItsBe_Max.Value <> 0)  then
  begin
    Min := Geral.FFT(QrLFormItsBe_Min.Value, 2, siPositivo);
    Max := Geral.FFT(QrLFormItsBe_Max.Value, 2, siPositivo);
    if (QrLFormItsBe_Min.Value <> 0) and (QrLFormItsBe_Max.Value <> 0)  then
      Tempo := Tempo + ' Bé ('+ Min + ' a ' + Max +'):__________'
    else if (QrLFormItsBe_Min.Value <> 0) then
      Tempo := Tempo + ' Bé ('+ Min + '):__________'
    else
      Tempo := Tempo + ' Bé ('+ Max + '):__________';
  end;
  if (QrLFormItsGC_Min.Value <> 0) or (QrLFormItsGC_Max.Value <> 0)  then
  begin
    Min := FloatToStr(QrLFormItsGC_Min.Value);
    Max := FloatToStr(QrLFormItsGC_Max.Value);
    if (QrLFormItsGC_Min.Value <> 0) and (QrLFormItsGC_Max.Value <> 0)  then
      Tempo := Tempo + ' ºC ('+ Min + ' a ' + Max +'):__________'
    else if (QrLFormItsGC_Min.Value <> 0) then
      Tempo := Tempo + ' ºC ('+ Min + '):__________'
    else
      Tempo := Tempo + ' ºC ('+ Max + '):__________';
  end;
  if QrLFormItsGraus.Value <> 0 then Tempo := Tempo + ' ºC ('+FloatToStr(
    QrLFormItsGraus.Value)+'):______';
  if Tempo <> '' then Tempo := Tempo + ' Responsável:_______________________';
  QrLFormItsTEMPO_TXT.Value := Tempo;
  QrLFormItsTEMPO_R_TXT.Value := dmkPF.IntToTempo(QrLFormItsTempoR.Value);
  //
  QrLFormItsDESCRI_TEMP.Value := QrLFormItsDescricao.Value;
  if QrLFormItsGraus.Value <> 0 then
  // Parei Aqui??
    QrLFormItsDESCRI_TEMP.Value := QrLFormItsDescricao.Value + ' ' +
    FloatToStr(QrLFormItsGraus.Value) + ' ºC';

end;

function TFrFormulasImpShow.Emite(Emit: Integer): Boolean;
var
  Controle: Integer;
var
  DataEmis, NOMECI, NOMESETOR, Tecnico, NOME, Espessura, DefPeca, Fulao,
  Obs: String;
  Codigo, Status, Numero, ClienteI, Tipificacao, TempoR, TempoP, Setor, Tipific,
  SetrEmi, Cod_Espess, CodDefPeca, Empresa: Integer;
  Peso, Custo, Qtde, AreaM2: Double;
var
  NomePQ, (*Obs,*) Boca, Processo, ObsProces, SiglaMoeda: String;
  (*Codigo, Controle,*) PQCI, Cli_Orig, Cli_Dest, MoedaPadrao, (*Numero,*)
  Ordem, Produto, (*TempoR, TempoP,*) ProdutoCI, GraGruX, Ativo, SourcMP,
  GraCorCad: Integer;
  CustoMedio, CustoPadrao, Porcent, pH, Be, (*Custo,*) Graus, Peso_PQ, pH_Min,
  pH_Max, Be_Min, Be_Max, GC_Min, GC_Max, GramasTi, GramasKg, GramasTo,
  PrecoMo_Kg, Cotacao_Mo, CustoRS_Kg, CustoRS_To, Minimo, Maximo, Diluicao,
  SemiAreaM2, SemiRendim, BRL_USD, BRL_EUR: Double;
  DtaCambio, HoraIni, NoGraCorCad, SemiTxtEspReb, DtaBaixa, DtCorrApo: String;
  VSMovCod, SemiCodEspReb: Integer;
begin
  try
    if FProgressBar1 <> nil then
    begin
      FProgressBar1.Max := QrFormulasIts.RecordCount + 1;
      FProgressBar1.Position := 0;
      FProgressBar1.Visible := True;
    end;
    Screen.Cursor := crHourGlass;
(*
    Dmod.QrUpdW.SQL.Clear;
    Dmod.QrUpdW.SQL.Add('INSERT INTO Emit SET');
    Dmod.QrUpdW.SQL.Add('DataEmis=:P0, Status=:P1, Numero=:P2, NOMECI=:P3, ');
    Dmod.QrUpdW.SQL.Add('NOMESETOR=:P4, Tecnico=:P5, Nome=:P6, ClienteI=:P7, ');
    Dmod.QrUpdW.SQL.Add('Tipificacao=:P8, TempoR=:P9, TempoP=:P10, Setor=:P11, ');
    Dmod.QrUpdW.SQL.Add('Espessura=:P12, Peso=:P13, Qtde=:P14, AreaM2=:P15, ');
    Dmod.QrUpdW.SQL.Add('Fulao=:P16, DefPeca=:P17, Obs=:P18, Tipific=:P19, ');
    Dmod.QrUpdW.SQL.Add('Codigo=:Pa');
    Dmod.QrUpdW.Params[00].AsDateTime  := Now();
    Dmod.QrUpdW.Params[01].AsInteger   := 0;
    Dmod.QrUpdW.Params[02].AsInteger   := QrFormulasNumero.Value;
    Dmod.QrUpdW.Params[03].AsString    := FCliIntTxt;
    Dmod.QrUpdW.Params[04].AsString    := QrFormulasNOMESETOR.Value;
    Dmod.QrUpdW.Params[05].AsString    := QrFormulasTecnico.Value;
    Dmod.QrUpdW.Params[06].AsString    := QrFormulasNome.Value;
    Dmod.QrUpdW.Params[07].AsInteger   := FCliIntCod;
    Dmod.QrUpdW.Params[08].AsInteger   := QrFormulasTipificacao.Value;
    Dmod.QrUpdW.Params[09].AsInteger   := QrFormulasTempoR.Value;
    Dmod.QrUpdW.Params[10].AsInteger   := QrFormulasTempoP.Value;
    Dmod.QrUpdW.Params[11].AsInteger   := QrFormulasSetor.Value;
    Dmod.QrUpdW.Params[12].AsString    := FLinhas;
    Dmod.QrUpdW.Params[13].AsFloat     := FPeso;
    Dmod.QrUpdW.Params[14].AsFloat     := FQtde;
    Dmod.QrUpdW.Params[15].AsFloat     := FArea;
    Dmod.QrUpdW.Params[16].AsString    := FFulao;
    Dmod.QrUpdW.Params[17].AsString    := FDefPeca;
    Dmod.QrUpdW.Params[18].AsString    := FObs;
    Dmod.QrUpdW.Params[19].AsInteger   := FTipific;
    //
    Dmod.QrUpdW.Params[20].AsInteger   := Emit;
    Dmod.QrUpdW.ExecSQL;
*)

    Codigo              := Emit;
    DataEmis            := Geral.FDT(Now(), 1);
    DtaBaixa            := Geral.FDT(FDataP, 1);
    DtCorrApo           := FDtCorrApo;
    Empresa             := FEmpresa;
    Status              := 0;
    Numero              := QrFormulasNumero.Value;
    NOMECI              := FCliIntTxt;
    NOMESETOR           := QrFormulasNOMESETOR.Value;
    Tecnico             := QrFormulasTecnico.Value;
    Nome                := QrFormulasNome.Value;
    ClienteI            := FCliIntCod;
    Tipificacao         := QrFormulasTipificacao.Value;
    TempoR              := QrFormulasTempoR.Value;
    TempoP              := QrFormulasTempoP.Value;
    Setor               := QrFormulasSetor.Value;
    Espessura           := FLinhasRebx;
    DefPeca             := FDefPeca;
    Peso                := FPeso;
    Custo               := 0;
    Qtde                := FQtde;
    AreaM2              := FArea;
    Fulao               := FFulao;
    Obs                 := FObs;
    Tipific             := FTipific;
    SetrEmi             := CO_SetrEmi_MOLHADO;
    SourcMP             := FSourcMP; // CO_SourcMP_WetSome;
    Cod_Espess          := FCod_Espess;
    SemiCodEspReb       := FCod_Rebaix;
    CodDefPeca          := FCodDefPeca;
    SemiAreaM2          := FSemiAreaM2;
    SemiRendim          := FSemiRendim;
    BRL_USD             := FBRL_USD;
    BRL_EUR             := FBRL_EUR;
    DtaCambio           := Geral.FDT(FDtaCambio, 1);
    VSMovCod            := FVSMovCod;
    HoraIni             := Geral.FDT(FHoraIni, 100);
    GraCorCad           := QrFormulasGraCorCad.Value;
    NoGraCorCad         := QrFormulasNoGraCorCad.Value;
    SemiTxtEspReb       := FLinhasSemi;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'emit', False, [
    'DataEmis', 'Status', 'Numero',
    'NOMECI', 'NOMESETOR', 'Tecnico',
    'NOME', 'ClienteI', 'Tipificacao',
    'TempoR', 'TempoP', 'Setor',
    'Espessura', 'DefPeca', 'Peso',
    'Custo', 'Qtde', 'AreaM2',
    'Fulao', 'Obs', 'Tipific',
    'SetrEmi', 'SourcMP', 'Cod_Espess',
    'CodDefPeca', 'EmitGru', 'Retrabalho',
    'SemiAreaM2', 'SemiRendim',
    'BRL_USD', 'BRL_EUR', 'DtaCambio',
    'VSMovCod', 'HoraIni', 'GraCorCad',
    'NoGraCorCad', 'SemiCodEspReb', 'SemiTxtEspReb',
    'DtaBaixa', 'DtCorrApo'], [
    'Codigo'], [
    DataEmis, Status, Numero,
    NOMECI, NOMESETOR, Tecnico,
    NOME, ClienteI, Tipificacao,
    TempoR, TempoP, Setor,
    Espessura, DefPeca, Peso,
    Custo, Qtde, AreaM2,
    Fulao, Obs, Tipific,
    SetrEmi, SourcMP, Cod_Espess,
    CodDefPeca, FEmitGru, FRetrabalho,
    SemiAreaM2, SemiRendim,
    BRL_USD, BRL_EUR, DtaCambio,
    VSMovCod, HoraIni, GraCorCad,
    NoGraCorCad, SemiCodEspReb, SemiTxtEspReb,
    DtaBaixa, DtCorrApo], [
    Codigo], True) then
    begin
      if FProgressBar1 <> nil then
      begin
        FProgressBar1.Position := FProgressBar1.Position + 1;
        FProgressBar1.Update;
      end;
      //
{
      Dmod.QrUpdW.SQL.Clear;
      Dmod.QrUpdW.SQL.Add('INSERT INTO EmitIts SET');
      Dmod.QrUpdW.SQL.Add('NOMEPQ=:P0, PQCI=:P1, CustoPadrao=:P2, MoedaPadrao=:P3, ');
      Dmod.QrUpdW.SQL.Add('Numero=:P4, Ordem=:P5, Porcent=:P6, Produto=:P7, ');
      Dmod.QrUpdW.SQL.Add('TempoR=:P8, TempoP=:P9, pH_Min=:P10, pH_Max=:P11, ');
      Dmod.QrUpdW.SQL.Add('Be_Min=:P12, Be_Max=:P13, GC_Min=:P14, GC_Max=:P15, ');
      Dmod.QrUpdW.SQL.Add('Obs=:P16, Diluicao=:P17, Custo=:P18, Minimo=:P19, ');
      Dmod.QrUpdW.SQL.Add('Maximo=:P20, Graus=:P21, Boca=:P22, Processo=:P23, ');
      Dmod.QrUpdW.SQL.Add('ObsProces=:P24, Ativo=:P25, Peso_PQ=:P26, ');
      Dmod.QrUpdW.SQL.Add('ProdutoCI=:P27, Cli_Orig=:P28, Cli_Dest=:P29, ');
      //
      Dmod.QrUpdW.SQL.Add('Codigo=:Pa, Controle=:Pb');
}
      QrFormulasIts.First;
      while not QrFormulasIts.Eof do
      begin
        Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'EmitIts',
          'EmitIts', 'Controle');
(*
        Dmod.QrUpdW.Params[00].AsString  := QrFormulasItsNome.Value;
        Dmod.QrUpdW.Params[01].AsInteger := QrFormulasItsPQCI.Value;
        Dmod.QrUpdW.Params[02].AsFloat   := QrFormulasItsCustoPadrao.Value;
        Dmod.QrUpdW.Params[03].AsInteger := QrFormulasItsMoedaPadrao.Value;
        Dmod.QrUpdW.Params[04].AsInteger := QrFormulasItsNumero.Value;
        Dmod.QrUpdW.Params[05].AsInteger := QrFormulasItsOrdem.Value;
        Dmod.QrUpdW.Params[06].AsFloat   := QrFormulasItsPorcent.Value;
        Dmod.QrUpdW.Params[07].AsInteger := QrFormulasItsProduto.Value;
        Dmod.QrUpdW.Params[08].AsInteger := QrFormulasItsTempoR.Value;
        Dmod.QrUpdW.Params[09].AsInteger := QrFormulasItsTempoP.Value;
        Dmod.QrUpdW.Params[10].AsFloat   := QrFormulasItspH_Min.Value;
        Dmod.QrUpdW.Params[11].AsFloat   := QrFormulasItspH_Max.Value;
        Dmod.QrUpdW.Params[12].AsFloat   := QrFormulasItsBe_Min.Value;
        Dmod.QrUpdW.Params[13].AsFloat   := QrFormulasItsBe_Max.Value;
        Dmod.QrUpdW.Params[14].AsFloat   := QrFormulasItsGC_Min.Value;
        Dmod.QrUpdW.Params[15].AsFloat   := QrFormulasItsGC_Max.Value;
        Dmod.QrUpdW.Params[16].AsString  := QrFormulasItsObs.Value;
        Dmod.QrUpdW.Params[17].AsFloat   := QrFormulasItsDiluicao.Value;
        Dmod.QrUpdW.Params[18].AsFloat   := QrFormulasItsCusto.Value;
        Dmod.QrUpdW.Params[19].AsFloat   := QrFormulasItsMinimo.Value;
        Dmod.QrUpdW.Params[20].AsFloat   := QrFormulasItsMaximo.Value;
        Dmod.QrUpdW.Params[21].AsFloat   := QrFormulasItsGraus.Value;
        Dmod.QrUpdW.Params[22].AsString  := QrFormulasItsBoca.Value;
        Dmod.QrUpdW.Params[23].AsString  := QrFormulasItsProcesso.Value;
        Dmod.QrUpdW.Params[24].AsString  := QrFormulasItsObsProces.Value;
        Dmod.QrUpdW.Params[25].AsFloat   := QrFormulasItsAtivo.Value;
        Dmod.QrUpdW.Params[26].AsFloat   := QrFormulasItsPESO_PQ.Value;
        Dmod.QrUpdW.Params[27].AsInteger := QrFormulasItsPRODUTOCI.Value;
        Dmod.QrUpdW.Params[28].AsInteger := FCliIntCod;
        Dmod.QrUpdW.Params[29].AsInteger := FCliIntCod;
        //
        Dmod.QrUpdW.Params[30].AsInteger := Emit;
        Dmod.QrUpdW.Params[31].AsInteger := Controle;
        Dmod.QrUpdW.ExecSQL;
*)
        Codigo          := Emit;
        //Controle        := Controle;
        NOMEPQ          := QrFormulasItsNome.Value;
        PQCI            := QrFormulasItsPQCI.Value;
        Cli_Orig        := FCliIntCod;
        Cli_Dest        := FCliIntCod;
        CustoMedio      := QrFormulasItsCustoMedio.Value;
        CustoPadrao     := QrFormulasItsCustoPadrao.Value;
        MoedaPadrao     := QrFormulasItsMoedaPadrao.Value;
        Numero          := QrFormulasItsNumero.Value;
        Ordem           := QrFormulasItsOrdem.Value;
        Porcent         := QrFormulasItsPorcent.Value;
        Produto         := QrFormulasItsProduto.Value;
        TempoR          := QrFormulasItsTempoR.Value;
        TempoP          := QrFormulasItsTempoP.Value;
        pH              := 0;
        Be              := 0;
        Obs             := QrFormulasItsObs.Value;
        Diluicao        := QrFormulasItsDiluicao.Value;
        Custo           := QrFormulasItsCusto.Value;
        Minimo          := QrFormulasItsMinimo.Value;
        Maximo          := QrFormulasItsMaximo.Value;
        Graus           := QrFormulasItsGraus.Value;
        Boca            := QrFormulasItsBoca.Value;
        Processo        := QrFormulasItsProcesso.Value;
        ObsProces       := QrFormulasItsObsProces.Value;
        Peso_PQ         := QrFormulasItsPESO_PQ.Value;
        ProdutoCI       := QrFormulasItsPRODUTOCI.Value;
        pH_Min          := QrFormulasItspH_Min.Value;
        pH_Max          := QrFormulasItspH_Max.Value;
        Be_Min          := QrFormulasItsBe_Min.Value;
        Be_Max          := QrFormulasItsBe_Max.Value;
        GC_Min          := QrFormulasItsGC_Min.Value;
        GC_Max          := QrFormulasItsGC_Max.Value;
        GraGruX         := 0;
        GramasTi        := 0;
        GramasKg        := 0;
        GramasTo        := 0;
        SiglaMoeda      := 'R$';
        PrecoMo_Kg      := 0;
        Cotacao_Mo      := 0;
        CustoRS_Kg      := 0;
        CustoRS_To      := 0;
        //
        Ativo           := QrFormulasItsAtivo.Value;
        //
        Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'emitits', False, [
        'Codigo', 'NomePQ', 'PQCI',
        'Cli_Orig', 'Cli_Dest', 'CustoPadrao', 'CustoMedio',
        'MoedaPadrao', 'Numero', 'Ordem',
        'Porcent', 'Produto', 'TempoR',
        'TempoP', 'pH', 'Be',
        'Obs', 'Diluicao', 'Custo',
        'Minimo', 'Maximo', 'Graus',
        'Boca', 'Processo', 'ObsProces',
        'Peso_PQ', 'ProdutoCI', 'pH_Min',
        'pH_Max', 'Be_Min', 'Be_Max',
        'GC_Min', 'GC_Max', 'GraGruX',
        'GramasTi', 'GramasKg', 'GramasTo',
        'SiglaMoeda', 'PrecoMo_Kg', 'Cotacao_Mo',
        'CustoRS_Kg', 'CustoRS_To', 'Ativo'], [
        'Controle'], [
        Codigo, NomePQ, PQCI,
        Cli_Orig, Cli_Dest, CustoPadrao, CustoMedio,
        MoedaPadrao, Numero, Ordem,
        Porcent, Produto, TempoR,
        TempoP, pH, Be,
        Obs, Diluicao, Custo,
        Minimo, Maximo, Graus,
        Boca, Processo, ObsProces,
        Peso_PQ, ProdutoCI, pH_Min,
        pH_Max, Be_Min, Be_Max,
        GC_Min, GC_Max, GraGruX,
        GramasTi, GramasKg, GramasTo,
        SiglaMoeda, PrecoMo_Kg, Cotacao_Mo,
        CustoRS_Kg, CustoRS_To, Ativo], [
        Controle], True);
        //
        if FProgressBar1 <> nil then
        begin
          FProgressBar1.Position := FProgressBar1.Position + 1;
          FProgressBar1.Update;
        end;
        //
        QrFormulasIts.Next;
      end;
      if FProgressBar1 <> nil then
      begin
        FProgressBar1.Position := 0;
        FProgressBar1.Visible := False;
      end;
      Screen.Cursor := crDefault;
      Result := True;
    end;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFrFormulasImpShow.QrPreusoCalcFields(DataSet: TDataSet);
begin
  if QrPreusoPESOFUTURO.Value < 0 then
     QrPreusoDEFASAGEM.Value := QrPreusoPESOFUTURO.Value * -1
  else
     QrPreusoDEFASAGEM.Value := 0;
end;

function TFrFormulasImpShow.EfetuaBaixa(QrLotes: TmySQLQuery): Boolean;
begin
  DmModEmit.EfetuaBaixa(FProgressBar1, FDataP, FTipoPreco, FCliIntCod,
  FEmit,  QrPreuso, QrLotes, QrPreusoMoedaPadrao, QrPreusoCustoPadrao,
  QrPreusoPeso_PQ, QrPreusoCustoMedio, QrPreusoCli_Orig, QrPreusoProduto,
  QrPreUsoCodigo, QrPreusoControle, TTipoSourceMP(FSourcMP), FDtCorrApo,
  FEmpresa);
end;

procedure TFrFormulasImpShow.FormCreate(Sender: TObject);
begin
  FEmitGru     := 0;
  FRetrabalho  := 0;
  FEmitGru_TXT := '';
  FVSMovCod    := 0;
  FLogo0Path   := Geral.ReadAppKey('Logo0', Application.Title, ktString, '',
    HKEY_LOCAL_MACHINE);
  FLogo0Exists := FileExists(FLogo0Path);
end;

{
    // 2014-06-12 - Falta de objetos no dfm!
procedure TFrFormulasImpShow.FrxRec1_Imprime;
begin
  ReopenEmitCus();
  MyObjects.frxImprime(frxQUI_RECEI_999_01_B, 'Receita');
end;

procedure TFrFormulasImpShow.FrxRec1_Mostra;
begin
  ReopenEmitCus();
  MyObjects.frxMostra(frxQUI_RECEI_999_01_B, 'Receita');
end;
    // FIM 2014-06-12
}

procedure TFrFormulasImpShow.frxRec2GetValue(const VarName: String;
  var Value: Variant);
var
  CProd, CSolu, Custo, Formato: String;
begin
  if FPesoCalc < 1 then
    Formato := FFloat_15_4
  else
    Formato := FFloat_15_2;
  //
  CProd := CO_VAZIO;
  CSolu := CO_VAZIO;
  Custo := CO_VAZIO;
  if VarName = 'VAR_MOSTRA_LINHA' then Value := QrLFormItsMOSTRA_LINHA.Value
  else if VarName = 'VAR_MOSTRA_GRADE' then Value := QrLFormItsMOSTRA_GRADE.Value else
  if VarName = 'Espessura' then Value   := FLinhasSemi else // Compatibilidade
  if VarName = 'EspessuraRebx' then Value   := FLinhasRebx else
  if VarName = 'EspessuraSemi' then Value   := FLinhasSemi else
  if VarName = 'Qtde' then Value :=
                FormatFloat('#,###,##0.0',FQtde)+' '+FDefPeca else
  if VarName = 'Peso' then Value := FormatFloat(FFloat_15_3, FPeso)+' kg' else
  if VarName = 'PesoFulao' then Value := FPeso else
  //fazer peso base (salgado -> pré-descarnado)
  if VarName = 'PesoMedio' then
    Value := FormatFloat(FFloat_15_3,FMedia)+' kg/'+FDefPeca else
  if VarName = 'Area' then
    Value := FormatFloat(FFloat_15_2,FArea);
  if VarName = 'FU' then Value := FFulao else
  if VarName = 'OS' then Value := ''(*VAR_OS*) else
  if VarName = 'SP' then Value := VAR_SP else
  //if VarName = 'Lotes' then Value := Geral.Substitui(FObs, sLineBreak, ' ');
  if VarName = 'Lotes' then Value := FObs;
  if FCalcCustos or FCalcPrecos then
  begin
    if QrFormulasItsCUSTO_PQ.Value > 0 then
       CProd := FormatFloat(Formato, QrFormulasItsCUSTO_PQ.Value);
    if QrFormulasItsCSolu.Value > 0 then
       CSolu := FormatFloat(Formato, QrFormulasItsCSoluP.Value);
    if QrFormulasItsCUSTO_PQ.Value > 0 then
       Custo := FormatFloat(Formato, QrFormulasItsCUSTO_PQ.Value);
    if VarName = 'Uso kg/L' then Value := 'Custo item';
    //
    if VarName = 'CustoTotal' then Value := FormatFloat(FFloat_15_4,FCustoTotal);
    if VarName = 'CustoTon' then Value := FormatFloat(FFloat_15_2,FCustokg*1000);
    if VarName = 'Custokg' then Value := FormatFloat(FFloat_15_4,FCustokg);
    if VarName = 'Custom2_WB' then Value := FormatFloat(FFloat_15_4,FCustoM2);
    if VarName = 'Custom2_SA' then Value := FormatFloat(FFloat_15_4,FCustoM2Semi);
    if VarName = 'Custoft2_WB' then Value := FormatFloat(FFloat_15_4,FCustoM2 * CO_M2toFT2);
    if VarName = 'Custoft2_SA' then Value := FormatFloat(FFloat_15_4,FCustoM2Semi * CO_M2toFT2);
    //
    if FBRL_USD = 0 then
    begin
      if VarName = 'VAR_BRL_USD' then Value := '';
      if VarName = 'U_CustoTotal' then Value := '';
      if VarName = 'U_CustoTon' then Value := '';
      if VarName = 'U_Custokg' then Value := '';
      if VarName = 'U_Custom2_WB' then Value := '';
      if VarName = 'U_Custom2_SA' then Value := '';
      if VarName = 'U_Custoft2_WB' then Value := '';
      if VarName = 'U_Custoft2_SA' then Value := '';
    end else begin
      if VarName = 'VAR_BRL_USD' then Value := Geral.FFT(FBRL_USD, 6, siPositivo);
      if VarName = 'U_CustoTotal' then Value := FormatFloat(FFloat_15_4, FCustoTotal/FBRL_USD);
      if VarName = 'U_CustoTon' then Value := FormatFloat(FFloat_15_2, FCustokg*1000/FBRL_USD);
      if VarName = 'U_Custokg' then Value := FormatFloat(FFloat_15_4,(FCustokg/FBRL_USD));
      if VarName = 'U_Custom2_WB' then Value := FormatFloat(FFloat_15_4, FCustoM2/FBRL_USD);
      if VarName = 'U_Custom2_SA' then Value := FormatFloat(FFloat_15_4, FCustoM2Semi/FBRL_USD);
      if VarName = 'U_Custoft2_WB' then Value := FormatFloat(FFloat_15_4, FCustoM2/FBRL_USD * CO_M2toFT2);
      if VarName = 'U_Custoft2_SA' then Value := FormatFloat(FFloat_15_4, FCustoM2Semi/FBRL_USD * CO_M2toFT2);
    end;
    //
    if FBRL_EUR = 0 then
    begin
      if VarName = 'VAR_BRL_EUR' then Value := '';
      if VarName = 'E_CustoTotal' then Value := '';
      if VarName = 'E_CustoTon' then Value := '';
      if VarName = 'E_Custokg' then Value := '';
      if VarName = 'E_Custom2_WB' then Value := '';
      if VarName = 'E_Custom2_SA' then Value := '';
      if VarName = 'E_Custoft2_WB' then Value := '';
      if VarName = 'E_Custoft2_SA' then Value := '';
    end else begin
      if VarName = 'VAR_BRL_EUR' then Value := Geral.FFT(FBRL_EUR, 6, siPositivo);
      if VarName = 'E_CustoTotal' then Value := FormatFloat(FFloat_15_4,FCustoTotal/FBRL_EUR);
      if VarName = 'E_CustoTon' then Value := FormatFloat(FFloat_15_2,FCustokg*1000/FBRL_EUR);
      if VarName = 'E_Custokg' then Value := FormatFloat(FFloat_15_4,(FCustokg/FBRL_EUR));
      if VarName = 'E_Custom2_WB' then Value := FormatFloat(FFloat_15_4,FCustoM2/FBRL_EUR);
      if VarName = 'E_Custom2_SA' then Value := FormatFloat(FFloat_15_4,FCustoM2Semi/FBRL_EUR);
      if VarName = 'E_Custoft2_WB' then Value := FormatFloat(FFloat_15_4,FCustoM2/FBRL_EUR * CO_M2toFT2);
      if VarName = 'E_Custoft2_SA' then Value := FormatFloat(FFloat_15_4,FCustoM2Semi/FBRL_EUR * CO_M2toFT2);
    end;
    //
    if VarName = 'Data_Moedas' then
    begin
      if FDtaCambio < 2 then
        Value := 'N/D'
      else
        Value := Geral.FDT(FDtaCambio, 2);
    end;
    if VarName = 'Custo_total' then Value := 'Custo total:';
    if VarName = 'Custo_ton' then Value := 'Custo ton:';
    if VarName = 'Custo_kg' then Value := 'Custo kg:';
    if VarName = 'Custo_m' then Value := 'Custo m²:';
    if VarName = 'Custo_ft' then Value := 'Custo ft²:';
    if VarName = 'Ini_txt' then Value := 'C.Prod.';
    if VarName = 'Fin_txt' then Value := 'C.Outros';
    if VarName = 'Inicio' then Value := CProd;
    if VarName = 'Final' then Value := CSolu;
    if VarName = 'kgLCusto' then
    Value := Custo;

  end else begin
    if QrFormulasItsPESO_PQ.Value > 0 then
{Peso}Custo := FormatFloat(FFloat_15_3, QrFormulasItsPESO_PQ.Value);
    if VarName = 'Uso kg/L' then Value := VarName;
    if VarName = 'CustoTotal' then Value := CO_VAZIO;
    if VarName = 'CustoTon' then Value := CO_VAZIO;
    if VarName = 'Custokg' then Value := CO_VAZIO;
    if VarName = 'Custom2_WB' then Value := CO_VAZIO;
    if VarName = 'Custom2_SA' then Value := CO_VAZIO;
    if VarName = 'Custoft2_WB' then Value := CO_VAZIO;
    if VarName = 'Custoft2_SA' then Value := CO_VAZIO;
    if VarName = 'Custo_total' then Value := CO_VAZIO;
    if VarName = 'Custo_ton' then Value := CO_VAZIO;
    if VarName = 'Custo_kg' then Value := CO_VAZIO;
    if VarName = 'Custo_m' then Value := CO_VAZIO;
    if VarName = 'Custo_ft' then Value := CO_VAZIO;
    if VarName = 'Ini_txt' then Value := 'Início';
    if VarName = 'Fin_txt' then Value := 'Final';
    if VarName = 'Inicio' then Value := CO_TempoVAZIO;
    if VarName = 'Final' then Value := CO_TempoVAZIO;
    if VarName = 'kgLCusto' then
    Value := Custo;//Peso
    //
    if VarName = 'U_CustoTotal' then Value := '';
    if VarName = 'U_CustoTon' then Value := '';
    if VarName = 'U_Custokg' then Value := '';
    if VarName = 'U_Custom2_WB' then Value := '';
    if VarName = 'U_Custom2_SA' then Value := '';
    if VarName = 'U_Custoft2_WB' then Value := '';
    if VarName = 'U_Custoft2_SA' then Value := '';
    //
    if VarName = 'E_CustoTotal' then Value := '';
    if VarName = 'E_CustoTon' then Value := '';
    if VarName = 'E_Custokg' then Value := '';
    if VarName = 'E_Custom2_WB' then Value := '';
    if VarName = 'E_Custom2_SA' then Value := '';
    if VarName = 'E_Custoft2_WB' then Value := '';
    if VarName = 'E_Custoft2_SA' then Value := '';
    //
    if VarName = 'Data_Moedas' then Value := '';
    if VarName = 'VAR_BRL_USD' then Value := '';
    if VarName = 'VAR_BRL_EUR' then Value := '';
  end;
  if VarName = 'VAR_TEMPORECEITA' then Value :=
  'Tempo parado: '+FHHMM_P+' + Tempo rodando: '+FHHMM_R+' = Tempo total: '+FHHMM_T
  else if AnsiCompareText(VarName, 'VAR_OBSERPROCES') = 0 then
    Value := Trim(QrLFormItsObserProce.Value)
  else if AnsiCompareText(VarName, 'VAR_PESAGEM') = 0 then
  begin
    if FEmit = 0 then Value := 'SEM BAIXA'
    else Value := 'Pesagem Nº '+IntToStr(FEmit);
  end
  else if AnsiCompareText(VarName, 'OBS') = 0 then Value := FObs;



  if AnsiCompareText(VarName, 'VAR_TXT') = 0 then
  begin
    if QrLFormItsInsumo.Value = -5 then
      Value := '-5'
    else
      Value := QrLFormItsTEXT_TXT.Value;
  end;
  if AnsiCompareText(VarName, 'VAR_TEMPO') = 0 then
  begin
    if QrLFormItsTEMPO_TXT.Value = '' then Value := False else Value := True;
  end
  else if AnsiCompareText(VarName, 'VAR_TEMOBSERPROCES') = 0 then
  begin
    if Trim(QrLFormItsObserProce.Value) = '' then Value := False else Value := True;
  end
  else if AnsiCompareText(VarName, 'VARF_NEGATIVO') = 0 then
  begin
    if QrPreusoPESOFUTURO.Value < 0 then Value := True else Value := False;
  end
  else if AnsiCompareText(VarName, 'VARF_CLIINT_TXT') = 0 then
    Value := FCliIntTxt
  else
  if VarName = 'LogoReceExiste' then
    Value := FileExists(Dmod.QrControle.FieldByName('PathLogoRece').AsString)
  else
  if VarName = 'LogoRecePath' then
    Value := Dmod.QrControle.FieldByName('PathLogoRece').AsString
  else
  if VarName = 'VAR_EMITGRU' then
  begin
    if FEmitGru <> 0 then
      Value := 'Grupo: ' + FEmitGru_TXT
    else
      Value := ''//'Sem grupo definido.'
  end else
  if VarName = 'VAR_RETRABALHO' then
  begin
    if FRetrabalho = 0 then
      Value := ''//'NÃO'
    else
      Value := 'SIM';
  end else
  if VarName = 'VAR_TIT_RETRAB' then
  begin
    if FRetrabalho = 0 then
      Value := ''//'NÃO'
    else
      Value := 'Retrabalho:';
  end else
  //?
end;

{
    // 2014-06-12 - Falta de objetos no dfm!
procedure TFrFormulasImpShow.QrEmitCusAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBMovIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'WHERE wmi.Codigo=' + Geral.FF0(QrEmitCusMPVIts.Value),
  'AND wmi.LnkNivXtr1=' + Geral.FF0(QrEmitCusCodigo.Value),
  'AND wmi.LnkNivXtr2=' + Geral.FF0(QrEmitCusControle.Value),
  //'ORDER BY NO_Pallet, wmi.Controle ',
  'ORDER BY wmi.Controle ',
  '']);
end;
    // FIM 2014-06-12
}

procedure TFrFormulasImpShow.QrEmitItsCalcFields(DataSet: TDataSet);
begin
  case FTipoPreco of
    1: // Cadastro manual
      QrEmitItsPRECO_PQ.Value := DmModEmit.PrecoEmReais(
        QrEmitItsMoedaPadrao.Value,
        QrEmitItsCustoPadrao.Value, 0);
    2: // desativado
      QrEmitItsPRECO_PQ.Value := 0;
    else   // 0: Estoque Cliente Interno
      QrEmitItsPRECO_PQ.Value := QrEmitItsCustoMedio.Value;
  end;
  //
  //
  QrEmitItsCUSTO_PQ.Value := QrEmitItsPESO_PQ.Value *
  QrEmitItsPRECO_PQ.Value;
  //
end;

end.
