unit MPVImpOpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkPermissoes, Vcl.Menus,
  dmkDBGrid, mySQLDbTables;

type
  TFmMPVImpOpc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtUsuario: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    PMUsuario: TPopupMenu;
    Adicionausurio1: TMenuItem;
    Retirausurio1: TMenuItem;
    dmkDBGrid2: TdmkDBGrid;
    DsMPVImpOpc: TDataSource;
    QrMPVImpOpc: TmySQLQuery;
    QrMPVImpOpcNumero: TIntegerField;
    QrMPVImpOpcLogin: TWideStringField;
    QrMPVImpOpcNOMEFUNCIONARIO: TWideStringField;
    QrMPVImpOpcCodigo: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Adicionausurio1Click(Sender: TObject);
    procedure Retirausurio1Click(Sender: TObject);
    procedure BtUsuarioClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmMPVImpOpc: TFmMPVImpOpc;

implementation

uses UnMyObjects, Module, DmkDAC_PF, MyDBCheck, Variants, UMySQLModule;

{$R *.DFM}

procedure TFmMPVImpOpc.Adicionausurio1Click(Sender: TObject);
const
  Aviso   = '...';
  Titulo  = 'Adiciona Usu�rio';
  Prompt  = 'Informe o usu�rio:';
  Campo   = 'Descricao';
var
  CodJan: Variant;
  Codigo, Usuario: Integer;
begin
  if (QrMPVImpOpc.State = dsInactive) then Exit;
  //
  CodJan := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Numero Codigo, Login ' + Campo,
    'FROM senhas ',
    'WHERE Numero > 0 ',
    'AND Numero NOT IN ( ',
    'SELECT Usuario ',
    'FROM mpvimpopc ',
    ')',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, False);
  //
  if CodJan <> Null then
    Usuario := Integer(CodJan)
  else
    Usuario := 0;
  //
  if Usuario <> 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Usuario FROM mpvimpopc ');
    Dmod.QrAux.SQL.Add('WHERE Usuario=:P0 ');
    Dmod.QrAux.Params[00].AsInteger := Usuario;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Geral.MB_Aviso('O usu�rio ' + Geral.FF0(Codigo) + ' j� foi adicionado anteriormente!');
      Exit;
    end;
    Codigo := UMyMod.BuscaEmLivreY_Def('mpvimpopc', 'Codigo', stIns, 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mpvimpopc', False, [
      'Usuario'], ['Codigo'], [Usuario], [Codigo], False)
    then
      UnDmkDAC_PF.AbreQuery(QrMPVImpOpc, Dmod.MyDB);
  end;
end;

procedure TFmMPVImpOpc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPVImpOpc.BtUsuarioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMUsuario, BtUsuario);
end;

procedure TFmMPVImpOpc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPVImpOpc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrMPVImpOpc, Dmod.MyDB);
end;

procedure TFmMPVImpOpc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPVImpOpc.Retirausurio1Click(Sender: TObject);
begin
  if (QrMPVImpOpc.State = dsInactive) or (QrMPVImpOpc.RecordCount = 0) then Exit;
  //
  UmyMod.ExcluiRegistroInt1('Confirma a retirada do usu�rio selecionado?',
  'mpvimpopc', 'Codigo', QrMPVImpOpcCodigo.Value, DMod.MyDB);
  //
  UnDmkDAC_PF.AbreQuery(QrMPVImpOpc, Dmod.MyDB);
end;

end.
