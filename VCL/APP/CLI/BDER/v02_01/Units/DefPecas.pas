unit DefPecas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnDmkProcFunc, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkEdit, dmkImage, UnDmkEnums;

type
  TFmDefPecas = class(TForm)
    PainelDados: TPanel;
    DsDefPecas: TDataSource;
    QrDefPecas: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    QrDefPecasNivel: TIntegerField;
    QrDefPecasValor: TFloatField;
    QrDefPecasLk: TIntegerField;
    QrDefPecasDataCad: TDateField;
    QrDefPecasDataAlt: TDateField;
    QrDefPecasUserCad: TIntegerField;
    QrDefPecasUserAlt: TIntegerField;
    DBRadioGroup1: TDBRadioGroup;
    RGGrandeza: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrDefPecasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrDefPecasAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrDefPecasBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmDefPecas: TFmDefPecas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmDefPecas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmDefPecas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrDefPecasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmDefPecas.DefParams;
begin
  VAR_GOTOTABELA := 'DefPecas';
  VAR_GOTOMYSQLTABLE := QrDefPecas;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM defpecas');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmDefPecas.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      if SQLType = stIns then
      begin
        EdCodigo.Text := '';
        EdNome.Text   := '';
        RGGrandeza.ItemIndex := 0;
      end else begin
        EdCodigo.Text := IntToStr(QrDefPecasCodigo.Value);
        EdNome.Text   := QrDefPecasNome.Value;
        RGGrandeza.ItemIndex := QrDefPecasGrandeza.Value;
      end;
      EdNome.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmDefPecas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmDefPecas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmDefPecas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmDefPecas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmDefPecas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmDefPecas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmDefPecas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmDefPecas.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmDefPecas.BtAlteraClick(Sender: TObject);
var
  DefPecas : Integer;
begin
  DefPecas := QrDefPecasCodigo.Value;
  if not UMyMod.SelLockY(DefPecas, Dmod.MyDB, 'DefPecas', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(DefPecas, Dmod.MyDB, 'DefPecas', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmDefPecas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrDefPecasCodigo.Value;
  Close;
end;

procedure TFmDefPecas.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descrição.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO defpecas SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'DefPecas', 'DefPecas', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE defpecas SET ');
    Codigo := QrDefPecasCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Grandeza=:P1, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := RGGrandeza.ItemIndex;
  //
  Dmod.QrUpdU.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[03].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[04].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'DefPecas', 'Codigo');
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmDefPecas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'DefPecas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'DefPecas', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'DefPecas', 'Codigo');
end;

procedure TFmDefPecas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmDefPecas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrDefPecasCodigo.Value,LaRegistro.Caption);
end;

procedure TFmDefPecas.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmDefPecas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmDefPecas.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmDefPecas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmDefPecas.QrDefPecasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmDefPecas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'DefPecas', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmDefPecas.QrDefPecasAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrDefPecasCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrDefPecasCodigo.Value, False);
end;

procedure TFmDefPecas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrDefPecasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'DefPecas', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmDefPecas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDefPecas.QrDefPecasBeforeOpen(DataSet: TDataSet);
begin
  QrDefPecasCodigo.DisplayFormat := FFormatFloat;
end;

end.

