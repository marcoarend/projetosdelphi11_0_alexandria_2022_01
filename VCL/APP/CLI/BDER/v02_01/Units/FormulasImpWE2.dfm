object FmFormulasImpWE2: TFmFormulasImpWE2
  Left = 363
  Top = 167
  Caption = 'QUI-RECEI-012 :: Impress'#227'o de Receitas de Recurtimento'
  ClientHeight = 646
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  Scaled = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 478
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 73
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object RGImpRecRib: TRadioGroup
        Left = 4
        Top = 4
        Width = 700
        Height = 65
        Caption = ' Apresenta'#231#227'o: '
        Columns = 2
        ItemIndex = 3
        Items.Strings = (
          'Procure em:'
          ''
          'FmPrincipal.VAR_RecImpApresentaCol'
          'FmPrincipal.VAR_RecImpApresentaRol')
        TabOrder = 0
      end
      object GBkgTon: TGroupBox
        Left = 710
        Top = 4
        Width = 95
        Height = 65
        Caption = ' Grandeza: '
        TabOrder = 1
        object RBTon: TRadioButton
          Left = 8
          Top = 16
          Width = 50
          Height = 17
          Caption = 'Ton'
          Checked = True
          TabOrder = 0
          TabStop = True
        end
        object RBkg: TRadioButton
          Left = 8
          Top = 36
          Width = 50
          Height = 17
          Caption = 'kg'
          TabOrder = 1
        end
      end
      object CkMatricial: TCheckBox
        Left = 808
        Top = 8
        Width = 97
        Height = 17
        Caption = 'Novo (Matricial)'
        TabOrder = 2
      end
      object CkGrade: TCheckBox
        Left = 808
        Top = 28
        Width = 125
        Height = 17
        Caption = 'Ver grade (Matricial)'
        TabOrder = 3
      end
      object CkContinua: TCheckBox
        Left = 808
        Top = 48
        Width = 125
        Height = 17
        Caption = 'Continuar imprimindo.'
        TabOrder = 4
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 73
      Width = 1008
      Height = 405
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Dados b'#225'sicos'
        object PainelEscolhas: TPanel
          Left = 0
          Top = 323
          Width = 1000
          Height = 54
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label10: TLabel
            Left = 4
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Peso:'
          end
          object Label3: TLabel
            Left = 88
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label7: TLabel
            Left = 308
            Top = 4
            Width = 28
            Height = 13
            Caption = 'Pe'#231'a:'
          end
          object SpeedButton1: TSpeedButton
            Left = 476
            Top = 20
            Width = 24
            Height = 21
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object Label12: TLabel
            Left = 872
            Top = 4
            Width = 45
            Height = 13
            Caption = 'kg/Pe'#231'a:'
          end
          object Label9: TLabel
            Left = 504
            Top = 4
            Width = 29
            Height = 13
            Caption = 'Ful'#227'o:'
          end
          object Label2: TLabel
            Left = 560
            Top = 4
            Width = 52
            Height = 13
            Caption = 'Espessura:'
          end
          object Label4: TLabel
            Left = 158
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object Label5: TLabel
            Left = 226
            Top = 4
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object Label8: TLabel
            Left = 716
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Rebaixe:'
          end
          object EdPeso: TdmkEdit
            Left = 4
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPesoChange
          end
          object EdPecas: TdmkEdit
            Left = 88
            Top = 20
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPecasChange
          end
          object CBPeca: TdmkDBLookupComboBox
            Left = 344
            Top = 20
            Width = 132
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsDefPecas
            TabOrder = 5
            dmkEditCB = EdPeca
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdMedia: TdmkEdit
            Left = 872
            Top = 20
            Width = 61
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdFulao: TdmkEdit
            Left = 504
            Top = 20
            Width = 53
            Height = 21
            MaxLength = 5
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdPeca: TdmkEditCB
            Left = 308
            Top = 20
            Width = 36
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdPecasChange
            DBLookupComboBox = CBPeca
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEspessura: TdmkDBLookupComboBox
            Left = 600
            Top = 20
            Width = 112
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Linhas'
            ListSource = DsEspessuras
            TabOrder = 8
            OnClick = CBEspessuraClick
            OnKeyDown = CBEspessuraKeyDown
            dmkEditCB = EdEspessura
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdAreaM2: TdmkEdit
            Left = 156
            Top = 20
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdAreaM2Change
          end
          object EdAreaP2: TdmkEdit
            Left = 224
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdAreaP2Change
          end
          object EdEspessura: TdmkEditCB
            Left = 560
            Top = 20
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdPecasChange
            DBLookupComboBox = CBEspessura
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdSemiCodEspReb: TdmkEditCB
            Left = 716
            Top = 20
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdPecasChange
            DBLookupComboBox = CBSemiCodEspReb
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBSemiCodEspReb: TdmkDBLookupComboBox
            Left = 756
            Top = 20
            Width = 112
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Linhas'
            ListSource = DsRebaixe
            TabOrder = 10
            OnClick = CBEspessuraClick
            OnKeyDown = CBEspessuraKeyDown
            dmkEditCB = EdSemiCodEspReb
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object PainelReceita: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 93
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 125
            Height = 13
            Caption = 'Receita (F4 para amostra):'
          end
          object Label11: TLabel
            Left = 8
            Top = 48
            Width = 198
            Height = 13
            Caption = 'Cliente interno (dono do produto qu'#237'mico):'
          end
          object LaData: TLabel
            Left = 552
            Top = 50
            Width = 26
            Height = 13
            Caption = 'Data:'
            Visible = False
          end
          object Label14: TLabel
            Left = 612
            Top = 4
            Width = 88
            Height = 13
            Caption = 'Grupo de emiss'#227'o:'
          end
          object Label18: TLabel
            Left = 536
            Top = 4
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
            FocusControl = DBEdit1
          end
          object EdReceita: TdmkEditCB
            Left = 8
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdReceitaChange
            OnKeyDown = EdReceitaKeyDown
            DBLookupComboBox = CBReceita
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBReceita: TdmkDBLookupComboBox
            Left = 90
            Top = 20
            Width = 443
            Height = 21
            Color = clWhite
            KeyField = 'Numero'
            ListField = 'Nome'
            ListSource = DsFormulas
            TabOrder = 1
            OnKeyDown = CBReceitaKeyDown
            dmkEditCB = EdReceita
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCliInt: TdmkEditCB
            Left = 8
            Top = 64
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCliInt
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCliInt: TdmkDBLookupComboBox
            Left = 90
            Top = 64
            Width = 371
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMECI'
            ListSource = DsCliInt
            TabOrder = 5
            dmkEditCB = EdCliInt
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object TPDataP: TdmkEditDateTimePicker
            Left = 552
            Top = 64
            Width = 122
            Height = 21
            Date = 38795.000000000000000000
            Time = 0.975709085701964800
            TabOrder = 7
            Visible = False
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpInsumMovimMin
          end
          object EdEmitGru: TdmkEditCB
            Left = 612
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdReceitaChange
            DBLookupComboBox = CBEmitGru
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmitGru: TdmkDBLookupComboBox
            Left = 668
            Top = 20
            Width = 185
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEmitGru
            TabOrder = 3
            dmkEditCB = EdEmitGru
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CkRetrabalho: TdmkCheckBox
            Left = 468
            Top = 68
            Width = 81
            Height = 17
            Caption = #201' retrabalho.'
            TabOrder = 6
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkDtCorrApo: TCheckBox
            Left = 680
            Top = 68
            Width = 185
            Height = 17
            Caption = #201' corre'#231#227'o de apontamento. Data:'
            TabOrder = 8
            OnClick = CkDtCorrApoClick
          end
          object TPDtCorrApo: TdmkEditDateTimePicker
            Left = 868
            Top = 64
            Width = 129
            Height = 21
            Date = 44767.000000000000000000
            Time = 0.833253726850671200
            Enabled = False
            TabOrder = 9
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpSPED_EFD_MIN_MAX
          end
          object DBEdit1: TDBEdit
            Left = 536
            Top = 20
            Width = 73
            Height = 21
            DataField = 'NumCODIF'
            DataSource = DsFormulas
            TabOrder = 10
          end
        end
        object PainelEscolhe: TPanel
          Left = 0
          Top = 93
          Width = 1000
          Height = 230
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 101
            Height = 230
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label6: TLabel
              Left = 4
              Top = 4
              Width = 79
              Height = 13
              Caption = 'Lotes de couros:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object BtAdiciona: TBitBtn
              Tag = 10
              Left = 4
              Top = 22
              Width = 90
              Height = 40
              Hint = 'Confirma a senha digitada'
              Caption = '&Adiciona'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              TabOrder = 0
              OnClick = BtAdicionaClick
            end
            object BtExclui: TBitBtn
              Tag = 12
              Left = 4
              Top = 64
              Width = 90
              Height = 40
              Hint = 'Confirma a senha digitada'
              Caption = 'E&xclui'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              TabOrder = 1
              OnClick = BtExcluiClick
            end
          end
          object DBGrid1: TDBGrid
            Left = 101
            Top = 0
            Width = 674
            Height = 230
            Align = alClient
            DataSource = DsLotes
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'VSMovIts'
                Title.Caption = 'IME-I'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OP'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Peso'
                Width = 76
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Visible = True
              end>
          end
          object PageControl2: TPageControl
            Left = 775
            Top = 0
            Width = 225
            Height = 230
            ActivePage = TabSheet4
            Align = alRight
            MultiLine = True
            TabOrder = 2
            TabPosition = tpLeft
            object TabSheet4: TTabSheet
              Caption = 'Observa'#231#245'es'
              object EdMemo: TMemo
                Left = 0
                Top = 0
                Width = 198
                Height = 222
                TabStop = False
                Align = alClient
                Color = clWhite
                TabOrder = 0
              end
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Configura'#231#245'es'
        ImageIndex = 2
        object PainelConfig: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 377
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object RGTipoPreco: TRadioGroup
            Left = 12
            Top = 6
            Width = 297
            Height = 47
            Caption = ' Origem pre'#231'os: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'Estoque'
              'Cadastro'
              'Manual')
            TabOrder = 0
          end
          object RGImprime: TRadioGroup
            Left = 12
            Top = 53
            Width = 393
            Height = 47
            Caption = ' Op'#231#245'es: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'Visualizar'
              'Imprimir'
              'Matricial'
              'Arquivo')
            TabOrder = 1
          end
          object GroupBox1: TGroupBox
            Left = 12
            Top = 100
            Width = 393
            Height = 60
            Caption = ' Semi acabado: '
            TabOrder = 2
            object Panel7: TPanel
              Left = 2
              Top = 15
              Width = 389
              Height = 43
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label21: TLabel
                Left = 10
                Top = 0
                Width = 39
                Height = 13
                Caption = #193'rea m'#178':'
              end
              object Label22: TLabel
                Left = 114
                Top = 0
                Width = 37
                Height = 13
                Caption = #193'rea ft'#178':'
              end
              object EdSemiAreaM2: TdmkEditCalc
                Left = 8
                Top = 16
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                dmkEditCalcA = EdSemiAreaP2
                CalcType = ctM2toP2
                CalcFrac = cfQuarto
              end
              object EdSemiAreaP2: TdmkEditCalc
                Left = 112
                Top = 16
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                dmkEditCalcA = EdSemiAreaM2
                CalcType = ctP2toM2
                CalcFrac = cfCento
              end
            end
          end
          object GroupBox2: TGroupBox
            Left = 12
            Top = 160
            Width = 393
            Height = 60
            Caption = ' C'#226'mbios: '
            TabOrder = 3
            object Panel9: TPanel
              Left = 2
              Top = 15
              Width = 389
              Height = 43
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label15: TLabel
                Left = 10
                Top = 0
                Width = 52
                Height = 13
                Caption = 'BRL/USD:'
              end
              object Label16: TLabel
                Left = 114
                Top = 0
                Width = 52
                Height = 13
                Caption = 'BRL/EUR:'
              end
              object Label17: TLabel
                Left = 218
                Top = 0
                Width = 68
                Height = 13
                Caption = 'Data cota'#231#227'o:'
              end
              object SpeedButton2: TSpeedButton
                Left = 332
                Top = 16
                Width = 23
                Height = 22
                Caption = '...'
                OnClick = SpeedButton2Click
              end
              object TPDtaCambio: TdmkEditDateTimePicker
                Left = 216
                Top = 16
                Width = 112
                Height = 21
                Date = 44767.000000000000000000
                Time = 0.423349398144637200
                TabOrder = 2
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object EdBRL_USD: TdmkEdit
                Left = 8
                Top = 16
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdBRL_EUR: TdmkEdit
                Left = 112
                Top = 16
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 193
      Height = 48
      Align = alLeft
      TabOrder = 1
      object LaSP_A: TLabel
        Left = 7
        Top = 9
        Width = 135
        Height = 32
        Caption = '000000000'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaSP_B: TLabel
        Left = 7
        Top = 11
        Width = 135
        Height = 32
        Caption = '000000000'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaSP_C: TLabel
        Left = 7
        Top = 10
        Width = 135
        Height = 32
        Caption = '000000000'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GB_M: TGroupBox
      Left = 193
      Top = 0
      Width = 767
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 488
        Height = 32
        Caption = 'Impress'#227'o de Receitas de Recurtimento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 488
        Height = 32
        Caption = 'Impress'#227'o de Receitas de Recurtimento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 488
        Height = 32
        Caption = 'Impress'#227'o de Receitas de Recurtimento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 526
    Width = 1008
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 22
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
        Visible = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 582
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label13: TLabel
        Left = 148
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtCancela: TBitBtn
          Tag = 15
          Left = 4
          Top = 1
          Width = 120
          Height = 40
          Hint = 'Cancela exibi'#231#227'o do cadastro de senhas'
          Caption = '&Desiste'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtCancelaClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 1
        Width = 120
        Height = 40
        Hint = 'Confirma a senha digitada'
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object EdEmpresa: TdmkEditCB
        Left = 148
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 204
        Top = 16
        Width = 437
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        TabStop = False
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object QrEspessura1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT EMCM From Espessuras'
      'WHERE Codigo=:P0')
    Left = 664
    Top = 228
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEspessura1EMCM: TFloatField
      FieldName = 'EMCM'
      Origin = 'DBMBWET.espessuras.EMCM'
    end
  end
  object QrEspessuras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM Espessuras'
      'ORDER BY Linhas')
    Left = 196
    Top = 300
    object QrEspessurasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.espessuras.Codigo'
    end
    object QrEspessurasLinhas: TWideStringField
      DisplayWidth = 20
      FieldName = 'Linhas'
      Origin = 'DBMBWET.espessuras.Linhas'
    end
    object QrEspessurasEMCM: TFloatField
      FieldName = 'EMCM'
      Origin = 'DBMBWET.espessuras.EMCM'
    end
    object QrEspessurasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.espessuras.Lk'
    end
  end
  object QrDefPecas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * From DefPecas')
    Left = 492
    Top = 112
    object QrDefPecasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.defpecas.Codigo'
    end
    object QrDefPecasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.defpecas.Nome'
      Size = 6
    end
    object QrDefPecasGrandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'DBMBWET.defpecas.Grandeza'
    end
    object QrDefPecasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.defpecas.Lk'
    end
  end
  object DsEspessuras: TDataSource
    DataSet = QrEspessuras
    Left = 196
    Top = 344
  end
  object DsDefPecas: TDataSource
    DataSet = QrDefPecas
    Left = 492
    Top = 160
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 540
    Top = 280
  end
  object QrFormulas: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFormulasCalcFields
    SQL.Strings = (
      'SELECT * FROM formulas'
      'ORDER BY Nome')
    Left = 540
    Top = 236
    object QrFormulasHHMM_P: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_P'
      Size = 10
      Calculated = True
    end
    object QrFormulasHHMM_R: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_R'
      Size = 10
      Calculated = True
    end
    object QrFormulasHHMM_T: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_T'
      Size = 10
      Calculated = True
    end
    object QrFormulasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrFormulasClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrFormulasTipificacao: TIntegerField
      FieldName = 'Tipificacao'
    end
    object QrFormulasDataI: TDateField
      FieldName = 'DataI'
    end
    object QrFormulasDataA: TDateField
      FieldName = 'DataA'
    end
    object QrFormulasTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrFormulasTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrFormulasTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrFormulasTempoT: TIntegerField
      FieldName = 'TempoT'
    end
    object QrFormulasHorasR: TIntegerField
      FieldName = 'HorasR'
    end
    object QrFormulasHorasP: TIntegerField
      FieldName = 'HorasP'
    end
    object QrFormulasHorasT: TIntegerField
      FieldName = 'HorasT'
    end
    object QrFormulasHidrica: TIntegerField
      FieldName = 'Hidrica'
    end
    object QrFormulasLinhaTE: TIntegerField
      FieldName = 'LinhaTE'
    end
    object QrFormulasCaldeira: TIntegerField
      FieldName = 'Caldeira'
    end
    object QrFormulasSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrFormulasEspessura: TIntegerField
      FieldName = 'Espessura'
    end
    object QrFormulasPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrFormulasQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrFormulasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFormulasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFormulasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFormulasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFormulasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFormulasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFormulasNumCODIF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NumCODIF'
      Size = 11
      Calculated = True
    end
    object QrFormulasRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
    end
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECI, ci.Codigo'
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"')
    Left = 604
    Top = 228
    object QrCliIntNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 604
    Top = 276
  end
  object QrEmitCus: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEmitCusAfterOpen
    BeforeClose = QrEmitCusBeforeClose
    AfterScroll = QrEmitCusAfterScroll
    SQL.Strings = (
      'SELECT CONCAT(mpi.Texto, " ", mpi.CorTxt) TextoECor,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_CLI,'
      'ecu.* '
      'FROM emitcus ecu'
      'LEFT JOIN mpvits mpi ON mpi.Controle=ecu.MPVIts'
      'LEFT JOIN mpp mpp ON mpp.Codigo=mpi.Pedido '
      'LEFT JOIN entidades ent ON ent.Codigo=mpp.Cliente '
      'WHERE ecu.Controle>0 ')
    Left = 276
    Top = 113
    object QrEmitCusTextoECor: TWideStringField
      FieldName = 'TextoECor'
      Size = 81
    end
    object QrEmitCusNOME_CLI: TWideStringField
      FieldName = 'NOME_CLI'
      Size = 100
    end
    object QrEmitCusCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitCusControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEmitCusMPIn: TIntegerField
      FieldName = 'MPIn'
    end
    object QrEmitCusFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrEmitCusDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrEmitCusPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitCusCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrEmitCusPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEmitCusAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitCusMPVIts: TIntegerField
      FieldName = 'MPVIts'
    end
    object QrEmitCusAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEmitCusAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsEmitCus: TDataSource
    DataSet = QrEmitCus
    Left = 276
    Top = 157
  end
  object QrWBMovIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrWBMovItsAfterOpen
    BeforeClose = QrWBMovItsBeforeClose
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM wbmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 352
    Top = 113
    object QrWBMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWBMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrWBMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrWBMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrWBMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrWBMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrWBMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWBMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWBMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWBMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWBMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWBMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWBMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWBMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrWBMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrWBMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrWBMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrWBMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrWBMovItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrWBMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrWBMovItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsWBMovIts: TDataSource
    DataSet = QrWBMovIts
    Left = 352
    Top = 157
  end
  object QrEmitGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM emitgru'
      'WHERE Ativo=1'
      'ORDER BY Nome'
      '')
    Left = 432
    Top = 113
    object QrEmitGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEmitGru: TDataSource
    DataSet = QrEmitGru
    Left = 432
    Top = 157
  end
  object QrLotes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emc.Controle, emc.VSMovIts, emc.Peso, emc.Pecas,   '
      'vmi.SerieFch, vmi.Ficha, vmi.Marca,fch.Nome NO_SerieFch  '
      'FROM emitcus emc '
      'LEFT JOIN vsmovits vmi ON vmi.Controle=emc.VSMovIts '
      'LEFT JOIN vsserfch fch ON fch.Codigo=vmi.SerieFch '
      'WHERE emc.Codigo>0 ')
    Left = 352
    Top = 261
    object QrLotesControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLotesVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrLotesPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrLotesPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrLotesAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotesOP: TIntegerField
      FieldName = 'OP'
    end
  end
  object DsLotes: TDataSource
    DataSet = QrLotes
    Left = 352
    Top = 309
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Peso) Peso, '
      'SUM(Pecas) Pecas'
      'FROM emitcus'
      'WHERE Codigo=:P0')
    Left = 472
    Top = 233
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSomaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSomaAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
  end
  object QrRebaixe: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM Espessuras'
      'ORDER BY Linhas')
    Left = 284
    Top = 300
    object QrRebaixeCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.espessuras.Codigo'
    end
    object QrRebaixeLinhas: TWideStringField
      DisplayWidth = 20
      FieldName = 'Linhas'
      Origin = 'DBMBWET.espessuras.Linhas'
    end
    object QrRebaixeEMCM: TFloatField
      FieldName = 'EMCM'
      Origin = 'DBMBWET.espessuras.EMCM'
    end
    object QrRebaixeLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.espessuras.Lk'
    end
  end
  object DsRebaixe: TDataSource
    DataSet = QrRebaixe
    Left = 284
    Top = 344
  end
end
