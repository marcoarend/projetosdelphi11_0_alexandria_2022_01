unit PQwPQEItsNeg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, mySQLDbTables,
  frxClass, frxDBSet, Variants;

type
  TFmPQwPQEItsNeg = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    frxPQ_NF_Err: TfrxReport;
    frxDsPQ_NF_Err: TfrxDBDataset;
    QrPQ_NF_Err: TmySQLQuery;
    QrPQ_NF_ErrPeso: TFloatField;
    QrPQ_NF_ErrNO_PQ: TWideStringField;
    QrPQ_NF_ErrCliOrig: TIntegerField;
    QrPQ_NF_ErrInsumo: TIntegerField;
    QrPQ_NF_ErrOrigemCodi: TIntegerField;
    QrPQ_NF_ErrOrigemCtrl: TIntegerField;
    QrPQ_NF_ErrTipo: TIntegerField;
    QrPQ_NF_ErrSdoPeso: TFloatField;
    QrPQ_NF_ErrNO_EMP: TWideStringField;
    PB1: TProgressBar;
    DsPQ_NF_Err: TDataSource;
    BtImprime: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrPQ_NF_ErrSdoValr: TFloatField;
    QrPQ_NF_ErrValor: TFloatField;
    BtAtualiza: TBitBtn;
    Qry: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrPQ_NF_ErrBeforeClose(DataSet: TDataSet);
    procedure QrPQ_NF_ErrAfterOpen(DataSet: TDataSet);
    procedure BtAcoesClick(Sender: TObject);
    procedure BtAtualizaClick(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaTodosPQxPositivos();
    procedure ImprimeDiferencasEstoqueNFs();
    procedure BaixaDiferencaDoInsumoAtual();

  public
    { Public declarations }
    FAtualizaTodos: Boolean;
    //
    procedure ReopenPQ_NF_Err();
  end;

  var
  FmPQwPQEItsNeg: TFmPQwPQEItsNeg;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnPQ_PF, ModuleGeral, UMySQLModule;

{$R *.DFM}

procedure TFmPQwPQEItsNeg.AtualizaTodosPQxPositivos();
var
  Qry1, Qry2: TmySQLQuery;
  DstCodi, DstCtrl, DstTipo: Integer;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  try
  Qry2 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT * ',
    'FROM pqx',
    'WHERE SdoPeso > 0',
    '']);
    PB1.Position := 0;
    PB1.Max := Qry1.RecordCount;
    Qry1.First;
    while not Qry1.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //
      DstCodi   := Qry1.FieldByName('OrigemCodi').AsInteger;
      DstCtrl   := Qry1.FieldByName('OrigemCtrl').AsInteger;
      DstTipo   := Qry1.FieldByName('Tipo').AsInteger;
      PQ_PF.AtualizaSdoPQx(Qry2, DstCodi, DstCtrl, DstTipo);
      //
      Qry1.Next;
    end;
    PB1.Position := 0;
    PB1.Max := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    ReopenPQ_NF_Err();
  finally
    Qry2.Free;
  end;
  finally
    Qry1.Free;
  end;
end;

procedure TFmPQwPQEItsNeg.BaixaDiferencaDoInsumoAtual();
var
  Peso, Valor: Double;
  OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo: Integer;
  Qry: TmySQLQuery;
  SQLType: TSQLType;
begin
  Peso    := QrPQ_NF_ErrSdoPeso.Value - QrPQ_NF_ErrPeso.Value;
  Valor   := QrPQ_NF_ErrSdoValr.Value - QrPQ_NF_ErrValor.Value;
  OriCodi := 0;
  OriCtrl := 0;
  OriTipo := 0;
  DstCodi := QrPQ_NF_ErrOrigemCodi.Value;
  DstCtrl := QrPQ_NF_ErrOrigemCtrl.Value;
  DstTipo := QrPQ_NF_ErrTipo.Value;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Peso, Valor  ',
    'FROM pqw ',
    'WHERE OriCodi=0 ',
    'AND OriCtrl=0 ',
    'AND OriTipo=0 ',
    'AND DstCodi=' + Geral.FF0(DstCodi),
    'AND DstCtrl=' + Geral.FF0(DstCtrl),
    'AND DstTipo=' + Geral.FF0(DstTipo),
    '']);
    if Qry.RecordCount > 0 then
    begin
      SQLType := stUpd;
      Peso    := Qry.FieldByName('Peso').AsFloat + Peso;
      Valor   := Qry.FieldByName('Valor').AsFloat + Valor;
    end else
    begin
      SQLType := stIns;
    end;
    //
    if Peso > 0 then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqw', False, [
      'Peso', 'Valor'], [
      'OriCodi', 'OriCtrl', 'OriTipo', 'DstCodi', 'DstCtrl', 'DstTipo'], [
      Peso, Valor], [
      OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo], False) then
      begin
        PQ_PF.AtualizaSdoPQx(Qry, DstCodi, DstCtrl, DstTipo);
        ReopenPQ_NF_Err();
        QrPQ_NF_Err.Locate('OrigemCodi;OrigemCtrl;Tipo',
          VarArrayOf([DstCodi,DstCtrl,DstTipo]), []);
      end;
    end else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqw', False, [
      'Peso', 'Valor'], [
      'OriCodi', 'OriCtrl', 'OriTipo', 'DstCodi', 'DstCtrl', 'DstTipo'], [
      Peso, Valor], [
      OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo], False);
      //
      PQ_PF.AtualizaSdoPQx(Qry, DstCodi, DstCtrl, DstTipo);
      ReopenPQ_NF_Err();
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmPQwPQEItsNeg.BtAcoesClick(Sender: TObject);
begin
{
object BtAcoes: TBitBtn
  Tag = 14
  Left = 12
  Top = 4
  Width = 118
  Height = 39
  Caption = '&Baixa'
  Enabled = False
  NumGlyphs = 2
  TabOrder = 0
  OnClick = BtAcoesClick
end
}
  BaixaDiferencaDoInsumoAtual();
end;

procedure TFmPQwPQEItsNeg.BtAtualizaClick(Sender: TObject);
var
  Qry2: TmySQLQuery;
  DstCodi, DstCtrl, DstTipo: Integer;
begin
  Qry2 := TmySQLQuery.Create(Dmod);
  try
    DstCodi   := QrPQ_NF_ErrOrigemCodi.Value;
    DstCtrl   := QrPQ_NF_ErrOrigemCtrl.Value;
    DstTipo   := QrPQ_NF_ErrTipo.Value;
    PQ_PF.AtualizaSdoPQx(Qry2, DstCodi, DstCtrl, DstTipo);
    //
    ReopenPQ_NF_Err();
  finally
    Qry2.Free;
  end;
end;

procedure TFmPQwPQEItsNeg.BtImprimeClick(Sender: TObject);
begin
  ImprimeDiferencasEstoqueNFs();
end;

procedure TFmPQwPQEItsNeg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQwPQEItsNeg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ReopenPQ_NF_Err();
  if FAtualizaTodos then
  begin
    FAtualizaTodos := False;
    if Geral.MB_Pergunta(
    'Confirma a atualização de todos itens de NF com saldo positivo?') = ID_YES
    then
      AtualizaTodosPQxPositivos();
  end;
end;

procedure TFmPQwPQEItsNeg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizaTodos := False;
end;

procedure TFmPQwPQEItsNeg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQwPQEItsNeg.ImprimeDiferencasEstoqueNFs();
begin
  MyObjects.frxDefineDataSets(frxPQ_NF_Err, [
    frxDsPQ_NF_Err,
    DModG.frxDsDono]);
  //
  MyObjects.frxMostra(frxPQ_NF_Err, 'Diferenças esoque NFs');
end;

procedure TFmPQwPQEItsNeg.QrPQ_NF_ErrAfterOpen(DataSet: TDataSet);
begin
  //BtAcoes.Enabled := True;
  BtImprime.Enabled := True;
  BtAtualiza.Enabled := True;
end;

procedure TFmPQwPQEItsNeg.QrPQ_NF_ErrBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
  BtAtualiza.Enabled := False;
end;

procedure TFmPQwPQEItsNeg.ReopenPQ_NF_Err();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQ_NF_Err, DmodG.MyPID_DB, [
  'DROP TABLE IF EXISTS _pq_compara_estq_nfs_; ',
  'CREATE TABLE _pq_compara_estq_nfs_ ',
  'SELECT pqx.CliOrig, pqx.Insumo, ',
  'OrigemCodi, OrigemCtrl, Tipo, ',
  'SUM(pqx.SdoPeso) SdoPeso, ',
  'SUM(pqx.SdoValr) SdoValr ',
  'FROM ' + TMeuDB + '.pqx pqx ',
  'WHERE pqx.SdoPeso<0 ',
  'GROUP BY pqx.CliOrig, pqx.Insumo; ',
  'SELECT pcl.Peso, pcl.Valor, pq_.Nome NO_PQ, pqx.*,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMP ',
  'FROM _pq_compara_estq_nfs_ pqx',
  'LEFT JOIN ' + TMeuDB + '.pqcli pcl ON pcl.CI=pqx.CliOrig ',
  'LEFT JOIN ' + TMeuDB + '.pq pq_ ON pq_.Codigo=pqx.Insumo',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=CliOrig',
  'WHERE pcl.PQ=pqx.Insumo  ',
  'AND pcl.Peso <> pqx.SdoPeso ',
  '']);
  //Geral.MB_SQL(Self, QrPQ_NF_Err);
end;

end.
