unit CMPTOut;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkDBGrid, DB,
  mySQLDbTables, dmkEdit, dmkEditCB, DBCtrls, dmkDBLookupComboBox, ComCtrls,
  dmkEditDateTimePicker, dmkLabel, dmkGeral, Menus, MyDBCheck, Variants,
  UnDmkProcFunc, dmkImage, UnDmkEnums, dmkDBGridZTO, frxClass, frxDBSet;

type
  TFmCMPTOut = class(TForm)
    PMNF: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    PMItens: TPopupMenu;
    Incluiitemdedevoluo1: TMenuItem;
    Retiraitemdedevoluo1: TMenuItem;
    Incluinovafatura1: TMenuItem;
    Alteradevoluofaturaatual1: TMenuItem;
    PMNFe: TPopupMenu;
    RecriatodaNFe1: TMenuItem;
    QrCMPTOut: TmySQLQuery;
    QrCMPTOutNOMECLI: TWideStringField;
    QrCMPTOutCodigo: TIntegerField;
    QrCMPTOutCliente: TIntegerField;
    QrCMPTOutDataS: TDateField;
    QrCMPTOutNFOut: TIntegerField;
    QrCMPTOutTotQtdkg: TFloatField;
    QrCMPTOutTotQtdPc: TFloatField;
    QrCMPTOutTotQtdVal: TFloatField;
    QrCMPTOutEmpresa: TIntegerField;
    QrCMPTOutTotQtdM2: TFloatField;
    QrCMPTOutAbertura: TDateTimeField;
    QrCMPTOutEncerrou: TDateTimeField;
    DsCMPTOut: TDataSource;
    QrCMPTOutIts: TmySQLQuery;
    QrCMPTOutItsDataE: TDateField;
    QrCMPTOutItsNFInn: TIntegerField;
    QrCMPTOutItsNFRef: TIntegerField;
    QrCMPTOutItsCodigo: TIntegerField;
    QrCMPTOutItsControle: TIntegerField;
    QrCMPTOutItsCMPTInn: TIntegerField;
    QrCMPTOutItsOutQtdkg: TFloatField;
    QrCMPTOutItsOutQtdPc: TFloatField;
    QrCMPTOutItsOutQtdVal: TFloatField;
    QrCMPTOutItsGraGruX: TIntegerField;
    QrCMPTOutItsBenefikg: TFloatField;
    QrCMPTOutItsBenefiPc: TFloatField;
    QrCMPTOutItsBenefiM2: TFloatField;
    QrCMPTOutItsOutQtdM2: TFloatField;
    QrCMPTOutItsTipoNF: TSmallintField;
    QrCMPTOutItsrefNFe: TWideStringField;
    QrCMPTOutItsmodNF: TSmallintField;
    QrCMPTOutItsSerie: TIntegerField;
    QrCMPTOutItsSitDevolu: TSmallintField;
    DsCMPTOutIts: TDataSource;
    QrClientesPesq: TmySQLQuery;
    QrClientesPesqCodigo: TIntegerField;
    QrClientesPesqNOMECLI: TWideStringField;
    DsClientesPesq: TDataSource;
    QrClientesEdit: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientesEdit: TDataSource;
    DsNFeCabA: TDataSource;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAStatus: TSmallintField;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfCanc_cStat: TIntegerField;
    QrNFeCabAinfCanc_xMotivo: TWideStringField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    QrNFeCabAcStat: TIntegerField;
    QrNFeCabAxMotivo: TWideStringField;
    QrNFeCabAcStat_xMotivo: TWideStringField;
    DsFatPedNFs: TDataSource;
    QrFatPedNFs: TmySQLQuery;
    QrFatPedNFsFilial: TIntegerField;
    QrFatPedNFsIDCtrl: TIntegerField;
    QrFatPedNFsTipo: TSmallintField;
    QrFatPedNFsOriCodi: TIntegerField;
    QrFatPedNFsEmpresa: TIntegerField;
    QrFatPedNFsNumeroNF: TIntegerField;
    QrFatPedNFsIncSeqAuto: TSmallintField;
    QrFatPedNFsAlterWeb: TSmallintField;
    QrFatPedNFsAtivo: TSmallintField;
    QrFatPedNFsSerieNFCod: TIntegerField;
    QrFatPedNFsSerieNFTxt: TWideStringField;
    QrFatPedNFsCO_ENT_EMP: TIntegerField;
    QrFatPedNFsDataCad: TDateField;
    QrFatPedNFsDataAlt: TDateField;
    QrFatPedNFsDataAlt_TXT: TWideStringField;
    QrFatPedNFsFreteVal: TFloatField;
    QrFatPedNFsSeguro: TFloatField;
    QrFatPedNFsOutros: TFloatField;
    QrFatPedNFsPlacaUF: TWideStringField;
    QrFatPedNFsPlacaNr: TWideStringField;
    QrFatPedNFsEspecie: TWideStringField;
    QrFatPedNFsMarca: TWideStringField;
    QrFatPedNFsNumero: TWideStringField;
    QrFatPedNFskgBruto: TFloatField;
    QrFatPedNFskgLiqui: TFloatField;
    QrFatPedNFsQuantidade: TWideStringField;
    QrFatPedNFsObservacao: TWideStringField;
    QrFatPedNFsCFOP1: TWideStringField;
    QrFatPedNFsDtEmissNF: TDateField;
    QrFatPedNFsDtEntraSai: TDateField;
    QrFatPedNFsStatus: TIntegerField;
    QrFatPedNFsinfProt_cStat: TIntegerField;
    QrFatPedNFsinfAdic_infCpl: TWideMemoField;
    QrFatPedNFsinfCanc_cStat: TIntegerField;
    QrFatPedNFsDTEMISSNF_TXT: TWideStringField;
    QrFatPedNFsDTENTRASAI_TXT: TWideStringField;
    QrFatPedNFsRNTC: TWideStringField;
    QrFatPedNFsUFembarq: TWideStringField;
    QrFatPedNFsxLocEmbarq: TWideStringField;
    PnControla: TPanel;
    PnPesquisa: TPanel;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    Label1: TLabel;
    Label10: TLabel;
    CBClientePesq: TdmkDBLookupComboBox;
    EdClientePesq: TdmkEditCB;
    EdFilialPesq: TdmkEditCB;
    CBFilialPesq: TdmkDBLookupComboBox;
    Panel5: TPanel;
    TPFim: TdmkEditDateTimePicker;
    CkFim: TCheckBox;
    TPIni: TdmkEditDateTimePicker;
    CkIni: TCheckBox;
    Panel4: TPanel;
    PnGrades: TPanel;
    Splitter1: TSplitter;
    GradeOut: TdmkDBGridZTO;
    GradeIts: TDBGrid;
    Panel6: TPanel;
    Label6: TLabel;
    DBMemo1: TDBMemo;
    DBGrid1: TDBGrid;
    QrNFeCabAide_cNF: TIntegerField;
    QrFatPedNFsHrEntraSai: TTimeField;
    QrFatPedNFside_dhCont: TDateTimeField;
    QrFatPedNFside_xJust: TWideStringField;
    QrFatPedNFsemit_CRT: TSmallintField;
    QrFatPedNFsdest_email: TWideStringField;
    QrFatPedNFsvagao: TWideStringField;
    QrFatPedNFsbalsa: TWideStringField;
    QrFatPedNFsCompra_XNEmp: TWideStringField;
    QrFatPedNFsCompra_XPed: TWideStringField;
    QrFatPedNFsCompra_XCont: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnEdita: TPanel;
    PnDadosEdita: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    SpeedButton1: TSpeedButton;
    TPDataS: TdmkEditDateTimePicker;
    EdNFOut: TdmkEdit;
    EdClienteEdit: TdmkEditCB;
    CBClienteEdit: TdmkDBLookupComboBox;
    EdFilialEdit: TdmkEditCB;
    CBFilialEdit: TdmkDBLookupComboBox;
    GBConfirma: TGroupBox;
    Panel10: TPanel;
    Panel11: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GbControla: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtNF: TBitBtn;
    BtItens: TBitBtn;
    BtEncerra: TBitBtn;
    BtNFe: TBitBtn;
    QrCMPTOutMOFatuKg: TFloatField;
    QrCMPTOutMOFatuVal: TFloatField;
    QrCMPTOutItsLk: TIntegerField;
    QrCMPTOutItsDataCad: TDateField;
    QrCMPTOutItsDataAlt: TDateField;
    QrCMPTOutItsUserCad: TIntegerField;
    QrCMPTOutItsUserAlt: TIntegerField;
    QrCMPTOutItsAlterWeb: TSmallintField;
    QrCMPTOutItsAtivo: TSmallintField;
    QrCMPTOutItsMOPesoKg: TFloatField;
    QrCMPTOutItsMOPrcKg: TFloatField;
    QrCMPTOutItsMOValor: TFloatField;
    QrCMPTOutItsHowCobrMO: TSmallintField;
    QrCMPTOutItsNFCMO: TIntegerField;
    QrCMPTOutItsNFDMP: TIntegerField;
    QrCMPTOutItsNFDIQ: TIntegerField;
    frxPST__CMPT_002_01: TfrxReport;
    frxDsCMPTOut: TfrxDBDataset;
    BtImpressao: TBitBtn;
    QrCMPTOutNOMEEMP: TWideStringField;
    QrCMPTOutSrvValTot: TFloatField;
    QrCMPTOutCondicaoPg: TIntegerField;
    QrCMPTOutCarteira: TIntegerField;
    QrCMPTOutFretePor: TIntegerField;
    QrCMPTOutTransporta: TIntegerField;
    QrCMPTOutFisRegCad: TIntegerField;
    QrCMPTOutDataFat: TDateField;
    QrCMPTOutStatus: TSmallintField;
    QrCMPTOutReabriu: TDateTimeField;
    QrCMPTOutSerieDesfe: TIntegerField;
    QrCMPTOutNFDesfeita: TIntegerField;
    QrCMPTOutLk: TIntegerField;
    QrCMPTOutDataCad: TDateField;
    QrCMPTOutDataAlt: TDateField;
    QrCMPTOutUserCad: TIntegerField;
    QrCMPTOutUserAlt: TIntegerField;
    QrCMPTOutAlterWeb: TSmallintField;
    QrCMPTOutAtivo: TSmallintField;
    QrCMPTOutJaAtz: TSmallintField;
    QrCMPTOutFilial: TIntegerField;
    QrCMPTOutENCERROU_TXT: TWideStringField;
    frxDsCMPTOutIts: TfrxDBDataset;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdClientePesqChange(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrCMPTOutBeforeClose(DataSet: TDataSet);
    procedure QrCMPTOutAfterScroll(DataSet: TDataSet);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure BtNFClick(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure Incluiitemdedevoluo1Click(Sender: TObject);
    procedure RGAbertosClick(Sender: TObject);
    procedure CkIniClick(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure CkFimClick(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure Retiraitemdedevoluo1Click(Sender: TObject);
    procedure Incluinovafatura1Click(Sender: TObject);
    procedure Alteradevoluofaturaatual1Click(Sender: TObject);
    procedure QrCMPTOutCalcFields(DataSet: TDataSet);
    procedure EdFilialPesqChange(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtCondicaoPGClick(Sender: TObject);
    procedure SbRegrFiscalClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure QrFatPedNFsAfterScroll(DataSet: TDataSet);
    procedure QrNFeCabACalcFields(DataSet: TDataSet);
    procedure QrFatPedNFsBeforeClose(DataSet: TDataSet);
    procedure BtNFeClick(Sender: TObject);
    procedure RecriatodaNFe1Click(Sender: TObject);
    procedure PMNFePopup(Sender: TObject);
    procedure frxPST__CMPT_002_01GetValue(const VarName: string;
      var Value: Variant);
    procedure BtImpressaoClick(Sender: TObject);
  private
    { Private declarations }
    FCriando: Boolean;
    procedure GerarNFe(Recria: Boolean);
    procedure ReopenFatPedNFs(Filial: Integer);
  public
    { Public declarations }
    procedure MostraEdicao(Acao: TSQLType);
    procedure MostraEdicaoIts(Acao: TSQLType);
    procedure ReopenCMPTOut(Codigo: Integer; OrderBy: String = '');
    procedure ReopenCMPTOutIts(Controle: Integer);
  end;

  var
  FmCMPTOut: TFmCMPTOut;

implementation

uses UnMyObjects, Module, UMySQLModule, CMPTOutIts, ModuleGeral, ModPediVda,
  Principal, Carteiras, UnInternalConsts, Entidade2, ModuleNFe_0000,
  UnGrade_Tabs, DmkDAC_PF;

{$R *.DFM}

procedure TFmCMPTOut.Altera1Click(Sender: TObject);
begin
  MostraEdicao(stUpd);
end;

procedure TFmCMPTOut.Alteradevoluofaturaatual1Click(Sender: TObject);
begin
  MostraEdicao(stUpd);
end;

procedure TFmCMPTOut.BitBtn1Click(Sender: TObject);
begin
  MostraEdicao(stLok);
end;

procedure TFmCMPTOut.BitBtn2Click(Sender: TObject);
var
  CondicaoPg, Carteira, FretePor, Transporta, FisRegCad,
  NFOut, Empresa, Cliente, Codigo: Integer;
  Abertura, DataS: String;
begin
  if MyObjects.FIC(EdFilialEdit.ValueVariant = 0, EdFilialEdit,
    'Informe a empresa!') then Exit;
  if MyObjects.FIC(EdClienteEdit.ValueVariant = 0, EdClienteEdit,
    'Informe o cliente!') then Exit;
{
  if MyObjects.FIC(EdCondicaoPG.ValueVariant = 0, EdCondicaoPG,
    'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(EdCartEmis.ValueVariant = 0, EdCartEmis,
    'Informe a carteira!') then Exit;
  if MyObjects.FIC(EdRegrFiscal.ValueVariant = 0, EdRegrFiscal,
    'Informe a movimenta��o (fiscal)!') then Exit;
  //if MyObjects.FIC(EdFretePor.ValueVariant = 0, EdFretePor,
    //'Informe o campo "Frete por"!') then Exit;
  if MyObjects.FIC((EdFretePor.ValueVariant > 1) and (DModG.QrPrmsEmpNFeversao.Value < 2),
    EdFretePor, 'Tipo de frete inv�lido na vers�o atual da NFe! Selecione 0 ou 1') then Exit;
}
  //

  if not UMyMod.ObtemCodigoDeCodUsu(EdFilialEdit, Empresa, 'Informe a empresa!',
    'Codigo', 'Filial') then Exit;
  Cliente   := Geral.IMV(EdClienteEdit.Text);
  if Cliente = 0 then
  begin
    Application.MessageBox('Defina o cliente!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  DataS     := Geral.FDT(TPDataS.Date, 1);
  NFOut := EdNFOut.ValueVariant;
  if ImgTipo.SQLType = stIns then
    Abertura := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora())
  else
    Abertura := FormatDateTime('yyyy-mm-dd hh:nn:ss', QrCMPTOutAbertura.Value);
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('CMPTout', 'Codigo', ImgTipo.SQLType,
    QrCMPTOutCodigo.Value);
  CondicaoPg   := 0;//DmPediVda.QrPediPrzCabCodigo.Value;
  Carteira     := 0;//DmPediVda.QrCartEmisCodigo.Value;
  FretePor     := 0;//EdFretePor.ValueVariant;
  Transporta   := 0;//EdTransporta.ValueVariant;
  FisRegCad    := DmPediVda.QrFisRegCadCodigo.Value;
{
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'CMPTout', False, [
    'Cliente', 'DataS', 'NFOut',
    'Empresa', 'Abertura'
  ], ['Codigo'], [
    Cliente, DataS, NFOut,
    Empresa, Abertura
  ], [Codigo], True) then
}
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'CMPTout', False, [
  'Cliente', 'DataS', 'NFOut',
  'Empresa', 'Abertura', 'CondicaoPg',
  'Carteira', 'FretePor', 'Transporta',
  'FisRegCad'], [
  'Codigo'], [
  Cliente, DataS, NFOut,
  Empresa, Abertura, CondicaoPg,
  Carteira, FretePor, Transporta,
  FisRegCad], [
  Codigo], True) then
  begin
    MostraEdicao(stLok);
    ReopenCMPTOut(Codigo);
  end;
end;

procedure TFmCMPTOut.BtCondicaoPGClick(Sender: TObject);
begin
(*
  VAR_CADASTRO := 0;
  FmPrincipal.MostraPediPrzCab(EdCondicaoPG.ValueVariant);
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrPediPrzCab . O p e n;
    if DmPediVda.QrPediPrzCab.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdCondicaoPg.ValueVariant := VAR_CADASTRO;
      CBCondicaoPg.KeyValue     := VAR_CADASTRO;
    end;
  end;
*)
end;

procedure TFmCMPTOut.BtEncerraClick(Sender: TObject);
(*
  procedure ExcluiItens(OriCodi: Integer);
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE ');
    Dmod.QrUpd.SQL.Add('Tipo=:P0 AND OriCodi=:P1');
    Dmod.QrUpd.Params[00].AsInteger := VAR_FATID_0102;
    Dmod.QrUpd.Params[01].AsInteger := OriCodi;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovvala WHERE ');
    Dmod.QrUpd.SQL.Add('Tipo=:P0 AND OriCodi=:P1');
    Dmod.QrUpd.Params[00].AsInteger := VAR_FATID_0102;
    Dmod.QrUpd.Params[01].AsInteger := OriCodi;
    Dmod.QrUpd.ExecSQL;
    //
  end;
var
  Especie, SerieNFTxt: String;
  NumeroNF, Quantidade, _OriCtrl_: Integer;
  //
  //IDCtrl, ID, SeqInReduz,
  NFe_FatID, OriCodi, OriCtrl, OriCnta, Empresa, Associada, Cliente, RegrFiscal,
  StqCenCad, FatSemEstq, GraGruX, InfAdCuztm, Preco_MedOrdem, TipoNF, modNF,
  Serie, nNF, SitDevolu, Servico, AFP_Sit, Cli_Tipo, Cli_UF, EMP_UF, EMP_FILIAL,
  ASS_CO_UF, ASS_FILIAL, Item_MadeBy, PediVda, OriPart: Integer;
  Qtde, Preco_PrecoF, Total, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
  Preco_MedidaA, Preco_MedidaE, AFP_Per, Item_IPI_ALq, TotalPreCalc: Double;
  Cli_IE, refNFe, CFOP, NO_tablaPrc: String;
*)
begin
{
  //SeqInReduz := 0;
  OriCodi      := QrCMPTOutCodigo.Value;
  ExcluiItens(OriCodi);
  //
  ReopenCMPTOutIts(0);
  QrCMPTOutIts.First;
  while not QrCMPTOutIts.Eof do
  begin
    NFe_FatID    := VAR_FATID_0102; // 102
    OriCtrl      := QrCMPTOutItsControle.Value;
    OriCnta      := 0;
    Empresa      := QrCMPTOutEmpresa.Value;
    Associada    := 0;
    Cliente      := QrCMPTOutCliente.Value;
    RegrFiscal   := QrCMPTOutFisRegCad.Value;
    StqCenCad    := 0; // Fazer??? Parei Aqui
    GraGruX      := QrCMPTOutItsGraGruX.Value;
    FatSemEstq   := 1; // Sim > Ver se faz
    AFP_Per      := 0;
    AFP_Sit      := 0;
    Cli_IE       := QrCMPTOutCLI_IE.Value;
    Cli_Tipo     := QrCMPTOutCLI_Tipo.Value;
    Cli_UF       := Trunc(QrCMPTOutCLI_UF.Value);
    EMP_UF       := Trunc(QrCMPTOutEMP_UF.Value);
    EMP_FILIAL   := QrCMPTOutEMP_FILIAL.Value;
    ASS_CO_UF    := Trunc(QrCMPTOutASS_CO_UF.Value);
    ASS_FILIAL   := QrCMPTOutASS_FILIAL.Value;
    Item_MadeBy  := 1;//???
    Item_IPI_ALq := 0; // S� devolu��o
    // Campo excluido em 2014-10-05
    case QrCMPTOutItsGerBxaEstq.Value of
      1: (*Pecas*) Qtde := QrCMPTOutItsOutQtdPc.Value;
      2: (*Area*)  Qtde := QrCMPTOutItsOutQtdM2.Value;
      3: (*Peso*)  Qtde := QrCMPTOutItsOutQtdkg.Value;
      else         Qtde := 0;
    end;
    if Qtde = 0 then
    begin
      //Preco_PrecoF := 0;
      ExcluiItens(OriCodi);
      Geral.MB_Aviso('Quantidade do produto de reduzido n� ' +
      IntToStr(GraGruX) +
      ' n�o definido. Verifique se o item gerenciador est� correto!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    Preco_PrecoF   := QrCMPTOutItsOutQtdVal.Value / Qtde;
    TotalPreCalc   := QrCMPTOutItsOutQtdVal.Value;
    InfAdCuztm     := 0;//QrCMPTOutIts.Value;
    Preco_PercCustom := 0;//QrCMPTOutIts.Value;
    Preco_MedidaC  := 0;//QrCMPTOutIts.Value;
    Preco_MedidaL  := 0;//QrCMPTOutIts.Value;
    Preco_MedidaA  := 0;//QrCMPTOutIts.Value;
    Preco_MedidaE  := 0;//QrCMPTOutIts.Value;
    Preco_MedOrdem := 0;//QrCMPTOutIts.Value;
    TipoNF         := QrCMPTOutItsTipoNF.Value;
    refNFe         := QrCMPTOutItsrefNFe.Value;
    modNF          := QrCMPTOutItsmodNF.Value;
    Serie          := QrCMPTOutItsSerie.Value;
    nNF            := QrCMPTOutItsNFInn.Value;
    SitDevolu      := QrCMPTOutItsSitDevolu.Value;
    Servico        := 0; // Produto
    PediVda        := 0;
    OriPart        := 0;
    //
    (*
    if DmNFe_0000.AppCodeNFe(Empresa) = 0 then
    begin
    *)
      if not DmNFe_0000.InsereItemStqMov(NFe_FatID, OriCodi, OriCnta, Empresa, Cliente,
               Associada, RegrFiscal, GraGruX, NO_tablaPrc,
               StqCenCad, FatSemEstq, AFP_Sit, AFP_Per, Qtde,
               Cli_Tipo, Cli_IE, Cli_UF, EMP_UF, EMP_FILIAL,
               ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Item_IPI_ALq,
               Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
               Preco_MedidaA, Preco_MedidaE, Preco_MedOrdem,
               stIns, PediVda, OriPart, InfAdCuztm,
               TipoNF, modNF, Serie, nNF, SitDevolu, Servico,
               refNFe, TotalPreCalc, _OriCtrl_,
               0,0,0,0, 1, 1, 0) then
      begin
        Geral.MB_Aviso('Encerramento cancelado!');
        Exit;
      end;
    (*
    end else begin
      if not DmNFe.InsereItemStqMov(NFe_FatID, OriCodi, OriCnta, Empresa, Cliente,
               Associada, RegrFiscal, GraGruX, NO_tablaPrc,
               StqCenCad, FatSemEstq, AFP_Sit, AFP_Per, Qtde,
               Cli_Tipo, Cli_IE, Cli_UF, EMP_UF, EMP_FILIAL,
               ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Item_IPI_ALq,
               Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
               Preco_MedidaA, Preco_MedidaE, Preco_MedOrdem,
               stIns, PediVda, OriPart, InfAdCuztm,
               TipoNF, modNF, Serie, nNF, SitDevolu, Servico,
               refNFe, TotalPreCalc, _OriCtrl_) then
      begin
        Geral.MB_Aviso('Encerramento cancelado!');
        Exit;
      end;
    end;
    *)
    //  FIM Produto / In�cio Servico
    //  Exclu� o campo de servico!! 2014-10-04
    if QrCMPTOutItsSrvCodigo.Value > 0 then
    begin
      NFe_FatID    := VAR_FATID_0102; // 102
      OriCodi      := QrCMPTOutCodigo.Value;
      OriCtrl      := QrCMPTOutItsControle.Value;
      OriCnta      := 0;
      Empresa      := QrCMPTOutEmpresa.Value;
      Associada    := 0;
      Cliente      := QrCMPTOutCliente.Value;
      RegrFiscal   := QrCMPTOutFisRegCad.Value;
      StqCenCad    := 0; // � servi�o
      GraGruX      := 0; // � servi�o
      FatSemEstq   := 1; // � servi�o
      AFP_Per      := 0;
      AFP_Sit      := 0;
      Cli_IE       := QrCMPTOutCLI_IE.Value;
      Cli_Tipo     := QrCMPTOutCLI_Tipo.Value;
      Cli_UF       := Trunc(QrCMPTOutCLI_UF.Value);
      EMP_UF       := Trunc(QrCMPTOutEMP_UF.Value);
      EMP_FILIAL   := QrCMPTOutEMP_FILIAL.Value;
      ASS_CO_UF    := Trunc(QrCMPTOutASS_CO_UF.Value);
      ASS_FILIAL   := QrCMPTOutASS_FILIAL.Value;
      Item_MadeBy  := 0; // � servi�o
      Item_IPI_ALq := 0; // � servi�o
      //case QrCMPTOutItsComoCobra.Value of
      case QrCMPTOutItsGerBxaEstq.Value of
        1: (*Pecas*) Qtde := QrCMPTOutItsBenefiPc.Value;
        2: (*Area*)  Qtde := QrCMPTOutItsBenefiM2.Value;
        3: (*Peso*)  Qtde := QrCMPTOutItsBenefikg.Value;
        else Qtde := 0;
      end;
      if Qtde = 0 then Preco_PrecoF := 0 else
        Preco_PrecoF   := QrCMPTOutItsSrvValUni.Value;
      TotalPreCalc   := QrCMPTOutItsSrvValTot.Value;
      InfAdCuztm     := 0;//QrCMPTOutIts.Value;
      Preco_PercCustom := 0;//QrCMPTOutIts.Value;
      Preco_MedidaC  := 0;//QrCMPTOutIts.Value;
      Preco_MedidaL  := 0;//QrCMPTOutIts.Value;
      Preco_MedidaA  := 0;//QrCMPTOutIts.Value;
      Preco_MedidaE  := 0;//QrCMPTOutIts.Value;
      Preco_MedOrdem := 0;//QrCMPTOutIts.Value;
      TipoNF         := 0;
      refNFe         := '';
      modNF          := 0;
      Serie          := 0;
      nNF            := 0;
      SitDevolu      := 0;
      Servico        := QrCMPTOutItsSrvCodigo.Value;
      PediVda        := 0;
      OriPart        := 0;
      //
      (*
      if DmNFe_0000.AppCodeNFe(Empresa) = 0 then
      begin
      *)
        if not DmNFe_0000.InsereItemStqMov(NFe_FatID, OriCodi, OriCnta, Empresa, Cliente,
                 Associada, RegrFiscal, GraGruX, NO_tablaPrc,
                 StqCenCad, FatSemEstq, AFP_Sit, AFP_Per, Qtde,
                 Cli_Tipo, Cli_IE, Cli_UF, EMP_UF, EMP_FILIAL,
                 ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Item_IPI_ALq,
                 Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
                 Preco_MedidaA, Preco_MedidaE, Preco_MedOrdem,
                 stIns, PediVda, OriPart, InfAdCuztm,
                 TipoNF, modNF, Serie, nNF, SitDevolu, Servico,
                 refNFe, TotalPreCalc, _OriCtrl_,
                 0,0,0,0, 1, 0, 0) then
        begin
          Geral.MB_Aviso('Encerramento cancelado!');
          Exit;
        end;
      (*
      end else begin
        if not DmNFe.InsereItemStqMov(NFe_FatID, OriCodi, OriCnta, Empresa, Cliente,
                 Associada, RegrFiscal, GraGruX, NO_tablaPrc,
                 StqCenCad, FatSemEstq, AFP_Sit, AFP_Per, Qtde,
                 Cli_Tipo, Cli_IE, Cli_UF, EMP_UF, EMP_FILIAL,
                 ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Item_IPI_ALq,
                 Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
                 Preco_MedidaA, Preco_MedidaE, Preco_MedOrdem,
                 stIns, PediVda, OriPart, InfAdCuztm,
                 TipoNF, modNF, Serie, nNF, SitDevolu, Servico,
                 refNFe, TotalPreCalc, _OriCtrl_) then
        begin
          Geral.MB_Aviso('Encerramento cancelado!');
          Exit;
        end;
      end;
      *)
    end;
    QrCMPTOutIts.Next;
  end;
  if DBCheck.CriaFm(TFmNFaEdit, FmNFaEdit, afmoNegarComAviso) then
  begin
    //DmPediVda.ReopenFatPedCab(QrFatPedCabCodigo.Value, True);
    // Configura��o do tipo
    FmNFaEdit.F_Tipo            := VAR_FATID_0102; // 102;
    FmNFaEdit.F_OriCodi         := QrCMPTOutCodigo        .Value;
    // Integer
    FmNFaEdit.F_Empresa         := QrCMPTOutEmpresa       .Value;
    FmNFaEdit.F_ModeloNF        := QrCMPTOutModeloNF      .Value;
    FmNFaEdit.F_Cliente         := QrCMPTOutCliente       .Value;
    FmNFaEdit.F_EMP_FILIAL      := QrCMPTOutFilial        .Value;
    FmNFaEdit.F_AFP_Sit         := 0;//QrCMPTOutAFP_Sit       .Value;
    FmNFaEdit.F_Associada       := QrCMPTOutAssociada     .Value;
    FmNFaEdit.F_ASS_FILIAL      := QrCMPTOutASS_FILIAL    .Value;
    FmNFaEdit.F_EMP_CtaFaturas  := QrCMPTOutEMP_CtaServico.Value;
    FmNFaEdit.F_ASS_CtaFaturas  := QrCMPTOutASS_CtaServico.Value;
    FmNFaEdit.F_CartEmis        := QrCMPTOutCarteira      .Value;
    FmNFaEdit.F_CondicaoPG      := QrCMPTOutCondicaoPG    .Value;
    FmNFaEdit.F_Financeiro      := QrCMPTOutFinanceiro    .Value;
    FmNFaEdit.F_EMP_FaturaDta   := QrCMPTOutEMP_FaturaDta .Value;
    FmNFaEdit.F_EMP_IDDuplicata := QrCMPTOutCodigo        .Value;
    FmNFaEdit.F_EMP_FaturaSeq   := QrCMPTOutEMP_FaturaSeq .Value;
    FmNFaEdit.F_EMP_FaturaNum   := QrCMPTOutEMP_FaturaNum .Value;
    FmNFaEdit.F_TIPOCART        := QrCMPTOutTIPOCART      .Value;
    FmNFaEdit.F_Represen        := 0;//QrCMPTOutRepresen      .Value;
    FmNFaEdit.F_ASS_IDDuplicata := QrCMPTOutCodigo        .Value;
    FmNFaEdit.F_ASS_FaturaSeq   := QrCMPTOutASS_FaturaSeq .Value;
    FmNFaEdit.F_ASS_FaturaNum   := QrCMPTOutASS_FaturaNum .Value;
    // String
    FmNFaEdit.F_EMP_FaturaSep   := QrCMPTOutEMP_FaturaSep .Value;
    FmNFaEdit.F_EMP_TxtFaturas  := QrCMPTOutEMP_TxtServico.Value;
    FmNFaEdit.F_EMP_TpDuplicata := QrCMPTOutEMP_DupServico.Value;
    FmNFaEdit.F_ASS_FaturaSep   := QrCMPTOutASS_FaturaSep .Value;
    FmNFaEdit.F_ASS_TxtFaturas  := QrCMPTOutASS_TxtServico.Value;
    FmNFaEdit.F_ASS_TpDuplicata := QrCMPTOutASS_DupServico.Value;
    // Double
    FmNFaEdit.F_AFP_Per         := 0;//QrCMPTOutAFP_Per       .Value;
    // TDateTime
    FmNFaEdit.F_Abertura        := QrCMPTOutAbertura      .Value;
    //
    FmNFaEdit.ReopenFatPedNFs(1,0);
    // NF-e 2.00
    FmNFaEdit.RGCRT.ItemIndex   := DmodG.QrParamsEmpCRT.Value;
    FmNFaEdit.EdHrEntraSai.ValueVariant := Time();
    FmNFaEdit.Eddest_email.Text         := DmodG.ObtemPrimeiroEMail_NFe(QrCMPTOutEmpresa.Value, QrCMPTOutCliente.Value);
    FmNFaEdit.EdVagao.Text              := '';
    FmNFaEdit.EdBalsa.Text              := '';
    // fim NF-e 2.00
    FmNFaEdit.EdCompra_XNEmp.Text := '';
    FmNFaEdit.EdCompra_XPed.Text := '';
    FmNFaEdit.EdCompra_XCont.Text := '';

    //

    if not DmNFe_0000.Obtem_Serie_e_NumNF_Novo(
    QrCMPTOutSerieDesfe.Value,
    QrCMPTOutNFDesfeita.Value,
    FmNFaEdit.QrImprimeSerieNF_Normal.Value,
    FmNFaEdit.QrImprimeCtrl_nfs.Value,
    QrCMPTOutEmpresa.Value,
    FmNFaEdit.QrFatPedNFs.FieldByName('Filial').AsInteger,
    FmNFaEdit.QrImprimeMaxSeqLib.Value,
    FmNFaEdit.EdSerieNF, FmNFaEdit.EdNumeroNF(*),
    SerieNFTxt, NumeroNF*)) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;

    //

    Especie := '';
    Quantidade := 0;
    //
    (*
    QrVolumes.Close;
    QrVolumes.Params[00].AsInteger := QrFatPedCabCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrVolumes . O p e n;
    QrVolumes.First;
    while not QrVolumes.Eof do
    begin
      Quantidade := Quantidade + QrVolumesVolumes.Value;
      if Especie <> '' then
        Especie := Especie + ' + ';
      Especie := Especie + ' ' +
        IntToStr(QrVolumesVolumes.Value) + ' ' +
        QrVolumesNO_UnidMed.Value;
      //
      QrVolumes.Next;
    end;
    (*
    FmNFaEdit.EdQuantidade.ValueVariant := FloatToStr(Quantidade);
    FmNFaEdit.EdEspecie.ValueVariant := Especie;
    //
    FmNFaEdit.EdNumeroNF.Enabled := (FmNFaEdit.QrImprimeIncSeqAuto.Value = 0);
    //
    (*
    FmNFaEdit.QrCFOP.Close;
    FmNFaEdit.QrCFOP.Params[00].AsInteger := VAR_FATID_0102;
    FmNFaEdit.QrCFOP.Params[00].AsInteger := FmFatPedCab.QrFatPedCabCodigo.Value;
    FmNFaEdit.QrCFOP.Params[01].AsInteger := FmFatPedCab.QrFatPedCabEmpresa.Value;
    UnDmkDAC_PF.AbreQuery(FmNFaEdit.QrCFOP .O p e n;
    FmNFaEdit.EdCFOP1.Text := FmNFaEdit.QrCFOPCFOP.Value;
    FmNFaEdit.CBCFOP1.KeyValue := FmNFaEdit.QrCFOPCFOP.Value;
    *)
    //
    FmNFaEdit.ReopenStqMovValX(0);
    FmNFaEdit.ImgTipo.SQLType := stIns;
    FmNFaEdit.ShowModal;
    FmNFaEdit.Destroy;
    // Reabrir de novo!
    ReopenCMPTOut(QrCMPTOutCodigo.Value);
    //
(*
    if QrFatPedCabEncerrou.Value > 0 then
    begin
      if DBCheck.CriaFm(TFmFatPedNFs, FmFatPedNFs, afmoNegarComAviso) then
      begin
        FmFatPedNFs.EdFilial.ValueVariant  := QrFatPedCabEMP_FILIAL.Value;
        FmFatPedNFs.CBFilial.KeyValue      := QrFatPedCabEMP_FILIAL.Value;
        FmFatPedNFs.EdCliente.ValueVariant := QrFatPedCabCliente.Value;
        FmFatPedNFs.CBCliente.KeyValue     := QrFatPedCabCliente.Value;
        FmFatPedNFs.EdPedido.ValueVariant  := QrFatPedCabCU_PediVda.Value;
        FmFatPedNFs.ShowModal;
        FmFatPedNFs.Destroy;
      end;
    end;
*)
  end;
}
end;

procedure TFmCMPTOut.BtImpressaoClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrCMPTOutCodigo.Value;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando relat�rio');
  ReopenCMPTOut(0, 'ORDER BY NOMECLI, NFOut');
  //
  MyObjects.frxDefineDataSets(frxPST__CMPT_002_01, [
    DModG.frxDsDono,
    frxDsCMPTOut,
    frxDsCMPTOutIts
  ]);
  MyObjects.frxMostra(frxPST__CMPT_002_01, 'Estoque de couros de terceiros');
  ReopenCMPTOut(Codigo);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmCMPTOut.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmCMPTOut.BtNFClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNF, BtNF);
end;

procedure TFmCMPTOut.BtNFeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNFe, BtNFe);
end;

procedure TFmCMPTOut.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCMPTOut.CkFimClick(Sender: TObject);
begin
  ReopenCMPTOut(QrCMPTOutCodigo.Value);
end;

procedure TFmCMPTOut.CkIniClick(Sender: TObject);
begin
  ReopenCMPTOut(QrCMPTOutCodigo.Value);
end;

procedure TFmCMPTOut.EdClientePesqChange(Sender: TObject);
begin
  ReopenCMPTOut(QrCMPTOutCodigo.Value);
end;

procedure TFmCMPTOut.EdFilialPesqChange(Sender: TObject);
begin
  ReopenCMPTOut(QrCMPTOutCodigo.Value);
end;

procedure TFmCMPTOut.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCMPTOut.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCriando := True;
  //
  Panel4.Align := alClient;
  UnDmkDAC_PF.AbreQuery(QrClientesEdit, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientesPesq, Dmod.MyDB);
  TPIni.date := Date - 30;
  TPFim.Date := Date;
  TPDataS.Date := Date;
  //
  FCriando := False;
  //
  if DModG.QrFiliLog.State = dsInactive then
    Geral.MB_Aviso('N�o empresa logada!')
  else
  if DModG.QrFiliLog.RecordCount = 1 then
  begin
    EdFilialPesq.ValueVariant := DModG.QrFiliLogFilial.Value;
    CBFilialPesq.KeyValue     := DModG.QrFiliLogFilial.Value;
  end;
end;

procedure TFmCMPTOut.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCMPTOut.frxPST__CMPT_002_01GetValue(const VarName: string;
  var Value: Variant);
begin

(*
  if AnsiCompareText(VarName, 'VFR_ORDENADO') = 0 then
    Value := Trim(QrCMPTInn.SortFieldNames) <> ''

  //

  else
*)
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBFilialPesq.Text, EdFilialPesq.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
(*
  else
  if VarName ='VARF_STATUS' then
    Value := RGAbertos.Items[RGAbertos.ItemIndex]
*)
  else
  if VarName = 'VARF_CLIENTE' then
    Value := dmkPF.ParValueCodTxt(
      '', CBClientePesq.Text, EdClientePesq.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_PROCEDENCIA' then
    Value := 'TODAS'
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp1(TPIni.Date, TPFim.Date,
    CkIni.Checked, CkFim.Checked, 'Per�odo: ', 'at�', '')
  else
end;

procedure TFmCMPTOut.Inclui1Click(Sender: TObject);
begin
  MostraEdicao(stIns);
end;

procedure TFmCMPTOut.Incluiitemdedevoluo1Click(Sender: TObject);
begin
  MostraEdicaoIts(stIns);
end;

procedure TFmCMPTOut.Incluinovafatura1Click(Sender: TObject);
begin
  MostraEdicao(stIns);
end;

procedure TFmCMPTOut.MostraEdicao(Acao: TSQLType);
begin
  EdNFOut.ValueVariant := 0;
  TPDataS.Date         := Date;
  //
  case Acao of
    stIns, stUpd:
    begin
      PnEdita.Visible    := True;
      PnControla.Visible := False;
      if Acao = stUpd then
      begin
        TPDataS.Date               := QrCMPTOutDataS.Value;
        EdClienteEdit.ValueVariant := QrCMPTOutCliente.Value;
        CBClienteEdit.KeyValue     := QrCMPTOutCliente.Value;
        EdFilialEdit.ValueVariant  := QrCMPTOutFilial.Value;
        CBFilialEdit.KeyValue      := QrCMPTOutFilial.Value;
        EdNFOut.ValueVariant       := QrCMPTOutNFOut.Value;
        //
        (*
        if DmPediVda.QrPediPrzCab.Locate('Codigo', QrCMPTOutCondicaoPg.Value, []) then
        begin
          EdCondicaoPG.ValueVariant := DmPediVda.QrPediPrzCabCodUsu.Value;
          CBCondicaoPG.KeyValue     := DmPediVda.QrPediPrzCabCodUsu.Value;
        end else begin
          EdCondicaoPG.ValueVariant := 0;
          CBCondicaoPG.KeyValue     := null;
        end;
        //
        EdCartEmis.ValueVariant := QrCMPTOutCarteira.Value;
        CBCartEmis.KeyValue     := QrCMPTOutCarteira.Value;
        //
        if DmPediVda.QrFisRegCad.Locate('Codigo', QrCMPTOutFisRegCad.Value, []) then
        begin
          EdRegrFiscal.ValueVariant := DmPediVda.QrFisRegCadCodUsu.Value;
          CBRegrFiscal.KeyValue     := DmPediVda.QrFisRegCadCodUsu.Value;
        end else begin
          EdRegrFiscal.ValueVariant := 0;
          CBRegrFiscal.KeyValue     := null;
        end;
        //
        EdTransporta.ValueVariant := QrCMPTOutTransporta.Value;
        CBTransporta.KeyValue     := QrCMPTOutTransporta.Value;
        //
        //
        EdFretePor.ValueVariant := QrCMPTOutFretePor.Value;
        CBFretePor.KeyValue     := QrCMPTOutFretePor.Value;
        *)
        //
        if QrCMPTOutIts.RecordCount > 0 then
        begin
          EdClienteEdit.Enabled := False;
          CBClienteEdit.Enabled := False;
          EdFilialEdit.Enabled := False;
          CBFilialEdit.Enabled := False;
          Application.MessageBox(PChar('N�o ser� poss�vel alterar a empresa ' +
          'e o cliente, pois j� existe devolu��o para esta NF!'),
          'Aviso', MB_OK+MB_ICONWARNING);
        end else begin
          EdClienteEdit.Enabled := True;
          CBClienteEdit.Enabled := True;
          EdFilialEdit.Enabled := True;
          CBFilialEdit.Enabled := True;
        end;
      end;
      if EdFilialEdit.Enabled then
        EdFilialEdit.SetFocus;
      //TPDataS.SetFocus;
    end;
    //stDel: ;
    //stCpy: ;
    stLok:
    begin
      PnControla.Visible := True;
      PnEdita.Visible    := False;
    end;
  end;
  ImgTipo.SQLType := Acao;
end;

procedure TFmCMPTOut.MostraEdicaoIts(Acao: TSQLType);
begin
  if DBCheck.CriaFm(TFmCMPTOutIts, FmCMPTOutIts, afmoNegarComAviso) then
  begin
    FmCMPTOutIts.ImgTipo.SQLType := Acao;
    FmCMPTOutIts.ShowModal;
    FmCMPTOutIts.Destroy;
  end;
end;

procedure TFmCMPTOut.PMNFePopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  {
  if DmNFe_0000.AppCodeNFe(DModG.QrFiliLogCodigo.Value) = 0 then
  begin
  }
    Habilita := (QrNFeCabAStatus.Value < DmNFe_0000.stepNFeAdedLote()) or
      DmNFe_0000.NotaRejeitada(QrNFeCabAinfProt_cStat.Value);
    RecriatodaNFe1.Enabled := Habilita;
  {
  end else begin
    Habilita := (QrNFeCabAStatus.Value < DmNFe.stepNFeAdedLote()) or
      DmNFe.NotaRejeitada(QrNFeCabAinfProt_cStat.Value);
    RecriatodaNFe1.Enabled := Habilita;
  end;
  }
end;

procedure TFmCMPTOut.QrCMPTOutAfterScroll(DataSet: TDataSet);
begin
  BtEncerra.Enabled := (QrCMPTOutEncerrou.Value = 0);
  ReopenCMPTOutIts(QrCMPTOutItsControle.Value);
  ReopenFatPedNFs(0);
end;

procedure TFmCMPTOut.QrCMPTOutBeforeClose(DataSet: TDataSet);
begin
  QrCMPTOutIts.Close;
  QrFatPedNFs.Close;
  BtEncerra.Enabled := False;
end;

procedure TFmCMPTOut.QrCMPTOutCalcFields(DataSet: TDataSet);
begin
  if QrCMPTOutEncerrou.Value = 0 then
    QrCMPTOutENCERROU_TXT.Value := 'EM FATURAMENTO'
  else
    QrCMPTOutENCERROU_TXT.Value := Geral.FDT(QrCMPTOutEncerrou.Value, 0);
end;

procedure TFmCMPTOut.QrFatPedNFsAfterScroll(DataSet: TDataSet);
begin
(*
  QrNFeCabA.Close;
  QrNFeCabA.Params[00].AsInteger := VAR_FATID_0102;
  QrNFeCabA.Params[01].AsInteger := QrCMPTOutCodigo.Value;
  QrNFeCabA.Params[02].AsInteger := QrFatPedNFsEmpresa.Value;
  QrNFeCabA.Params[03].AsInteger := QrFatPedNFsNumeroNF.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeCabA . O p e n ;
*)
end;

procedure TFmCMPTOut.QrFatPedNFsBeforeClose(DataSet: TDataSet);
begin
  QrNFeCabA.Close;
end;

procedure TFmCMPTOut.QrNFeCabACalcFields(DataSet: TDataSet);
begin
  if QrNFeCabAinfCanc_cStat.Value = 101 then
  begin
    QrNFeCabAcStat.Value := QrNFeCabAinfCanc_cStat.Value;
    QrNFeCabAxMotivo.Value := QrNFeCabAinfCanc_xMotivo.Value;
  end else begin
    QrNFeCabAcStat.Value := QrNFeCabAinfProt_cStat.Value;
    QrNFeCabAxMotivo.Value := QrNFeCabAinfProt_xMotivo.Value;
  end;
    QrNFeCabAcStat_xMotivo.Value := IntToStr(QrNFeCabAcStat.Value) + ': ' +
      QrNFeCabAxMotivo.Value;
end;

procedure TFmCMPTOut.RecriatodaNFe1Click(Sender: TObject);
begin
  GerarNFe(True);
end;

procedure TFmCMPTOut.GerarNFe(Recria: Boolean);
(*
var
  ide_serie: Variant;
  ide_dEmi, ide_dSaiEnt: TDateTime;
  FatNum, Empresa, Cliente, ide_indPag, ide_nNF, ide_tpNF, ide_tpEmis,
  modFrete, Transporta, NFeStatus, FretePor: Integer;
  //
  IDCtrl: Integer;
  //
  CFOP1, CodTXT, EmpTXT, infAdic_infAdFisco, infAdic_infCpl,
  VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
  Exporta_UFEmbarq, Exporta_xLocEmbarq,
  SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES, SQL_CUSTOMZ
  : String;
  //
  FreteVal, Seguro, Outros: Double;
  GravaCampos, RegrFiscal, Financeiro: Integer;
  UF_TXT_Emp, UF_TXT_Cli: String;
  cNF_Atual: Integer;
  //
  //2.00
  ide_hSaiEnt: TTime;
  ide_dhCont: TDateTime;
  emit_CRT: Integer;
  ide_xJust, dest_email, Vagao, Balsa: String;
  // fim 2.00
  Compra_XNEmp, Compra_XPed, Compra_XCont: String;
*)
begin
{
 Mudar de NFe 1.1 para 2.x!

  FatNum     := QrCMPTOutCodigo.Value;
  RegrFiscal := QrCMPTOutFisRegCad.Value;
  Empresa    := QrFatPedNFsEmpresa.Value;
  IDCtrl     := QrFatPedNFsIDCtrl.Value;
  FreteVal   := QrFatPedNFsFreteVal.Value;
  Seguro     := QrFatPedNFsSeguro.Value;
  Outros     := QrFatPedNFsOutros.Value;
  Financeiro := QrCMPTOutFinanceiro.Value;
  if not DmodG.QrFiliLog.Locate('Codigo', Empresa, []) then
  begin
    Application.MessageBox(PChar('Empresa (' + IntToStr(Empresa) +
    ') diferente da logada (' + VAR_LIB_EMPRESAS +
    ')! � necess�rio logoff!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if Geral.IMV(EdFilialPesq.Text) = 0 then
  begin
    Application.MessageBox('Empresa n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdFilialPesq.SetFocus;
    Exit;
  end;
  if Geral.IMV(EdClientePesq.Text) = 0 then
  begin
    Application.MessageBox('Cliente n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdClientePesq.SetFocus;
    Exit;
  end;
  if not DModG.DefineFretePor(QrCMPTOutFretePor.Value, FretePor, modFrete) then
    Exit;
  //
  Cliente     := QrCMPTOutCliente.Value;
  Transporta  := QrCMPTOutTransporta.Value;
  ide_indPag  := ???.EscolhaDe2Int(QrCMPTOutMedDDSimpl.Value = 0, 0, 1);
  ide_Serie   := QrFatPedNFsSerieNFTxt.Value;
  ide_nNF     := QrFatPedNFsNumeroNF.Value;
  ide_dEmi    := QrFatPedNFsDtEmissNF.Value;
  ide_dSaiEnt := QrFatPedNFsDtEntraSai.Value;
  ide_tpNF    := 1; // Sa�da
  //ide_tpImp   :=  1-Retrato/ 2-Paisagem
  ide_tpEmis  := 1;
  (*
   1 � Normal � emiss�o normal;
   2 � Conting�ncia FS � emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a;
   3 � Conting�ncia SCAN � emiss�o em conting�ncia no Sistema de Conting�ncia do Ambiente Nacional � SCAN;
   4 � Conting�ncia DPEC - emiss�o em conting�ncia com envio da Declara��o Pr�via de Emiss�o em Conting�ncia � DPEC;
   5 � Conting�ncia FS-DA - emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a para Impress�o de Documento Auxiliar de Documento Fiscal Eletr�nico (FS-DA).
  *)
  infAdic_infAdFisco := QrCMPTOutinfAdFisco.Value;
  infAdic_infCpl     := QrFatPedNFsinfAdic_infCpl.Value;
  VeicTransp_Placa   := QrFatPedNFsPlacaNr.Value;
  VeicTransp_UF      := QrFatPedNFsPlacaUF.Value;
  VeicTransp_RNTC    := QrFatPedNFsRNTC.Value;
  Exporta_UFEmbarq   := QrFatPedNFsUFEmbarq.Value;
  Exporta_XLocEmbarq := QrFatPedNFsxLocEmbarq.Value;
  //
  CFOP1 := QrFatPedNFsCFOP1.Value;
  CodTXT := dmkPF.FFP(QrCMPTOutCodigo.Value, 0);
  EmpTXT := dmkPF.FFP(QrCMPTOutEmpresa.Value, 0);
  UF_TXT_Emp := QrCMPTOutUF_TXT_Emp.Value;
  UF_TXT_Cli := QrCMPTOutUF_TXT_Cli.Value;

  NFeStatus := QrNFeCabAStatus.Value;

  // 2.00
  ide_hSaiEnt := QrFatPedNFsHrEntraSai.Value;
  ide_dhCont  := 0;   // Parei aqui! Ver o que fazer!
  ide_xJust   := ''; // Parei aqui! Ver o que fazer!
  emit_CRT    := QrFatPedNFsemit_CRT.Value;
  dest_email  := QrFatPedNFsdest_EMail.Value;
  Vagao       := QrFatPedNFsVagao.Value;
  Balsa       := QrFatPedNFsBalsa.Value;
  // fim 2.00
  Compra_XNEmp := QrFatPedNFsCompra_XNEmp.Value;
  Compra_XPed  := QrFatPedNFsCompra_XPed.Value;
  Compra_XCont := QrFatPedNFsCompra_XCont.Value;
  (*
  if DmNFe_0000.AppCodeNFe(Empresa) = 0 then
  begin
  *)
    SQL_ITS_ITS := DmNFe_0000.SQL_ITS_ITS_Prod1_Padrao(VAR_FATID_0102, FatNum, Empresa);
    SQL_ITS_TOT := DmNFe_0000.SQL_ITS_TOT_Prod1_Padrao(VAR_FATID_0102, FatNum, Empresa);
    SQL_FAT_ITS := DmNFe_0000.SQL_FAT_ITS_Prod1_Padrao(VAR_FATID_0102, FatNum, Empresa);
    SQL_FAT_TOT := DmNFe_0000.SQL_FAT_TOT_Prod1_Padrao(VAR_FATID_0102, FatNum, Empresa);
    SQL_VOLUMES := DmNFe_0000.SQL_VOLUMES_Prod1_Padrao(VAR_FATID_0102, FatNum, Empresa);
    if DBCheck.CriaFm(TFmNFeSteps_ 0 1 1 0 , FmNFeSteps_ 0 1 1 0 , afmoNegarComAviso) then
    begin
      FmNFeSteps_ 0 1 1 0 .Show;
      //
      (*
      GravaCampos := Application.MessageBox(PChar('Deseja salvar demonstrativo ' +
      ' no banco de dados para verifica��o de dados em relat�rio? ' + sLineBreak +
      'Este demostrativo n�o � obrigat�rio e sua grava��o pode ser demorada!' +
      sLineBreak + 'Deseja salvar assim mesmo?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
      *)
      cNF_Atual := QrNFeCabAide_cNF.Value;
      GravaCampos := ID_NO;
      DmNFe_0000.CriaNFe_vXX_XX(Recria, NFeStatus, VAR_FATID_0102, FatNum, Empresa,
          IDCtrl, Cliente, FretePor, modFrete, Transporta, ide_indPag,
          RegrFiscal, FreteVal, Seguro, Outros, ide_Serie,
          ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF, ide_tpEmis,
          infAdic_infAdFisco, infAdic_infCpl,
          VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
          Exporta_UFEmbarq, Exporta_xLocEmbarq,
          SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
          SQL_CUSTOMZ, UF_TXT_Emp, UF_TXT_Cli, GravaCampos, cNF_Atual, Financeiro,
          ide_hSaiEnt, ide_dhCont, ide_xJust, emit_CRT, dest_email, Vagao, Balsa,
          Compra_XNEmp, Compra_XPed, Compra_XCont, False);
(*
      if GravaCampos <> ID_CANCEL then
        FmNFeSteps_xxxx.Cria NFe Normal(Recria, NFeStatus, VAR_FATID_0102, FatNum, Empresa,
          IDCtrl, Cliente, FretePor, modFrete, Transporta, ide_indPag,
          RegrFiscal, FreteVal, Seguro, Outros, ide_Serie,
          ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF, ide_tpEmis,
          infAdic_infAdFisco, infAdic_infCpl,
          VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
          Exporta_UFEmbarq, Exporta_xLocEmbarq,
          SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
          SQL_CUSTOMZ, UF_TXT_Emp, UF_TXT_Cli, GravaCampos, cNF_Atual, Financeiro)
      else Geral.MB_Aviso('Gera��o total da NF-e cancelada pelo usu�rio!');
      //
      FmNFeSteps_XXXX.Destroy;
*)
    (*
    end else begin
      SQL_ITS_ITS := DmNFe.SQL_ITS_ITS_Prod1_Padrao(VAR_FATID_0102, FatNum, Empresa);
      SQL_ITS_TOT := DmNFe.SQL_ITS_TOT_Prod1_Padrao(VAR_FATID_0102, FatNum, Empresa);
      SQL_FAT_ITS := DmNFe.SQL_FAT_ITS_Prod1_Padrao(VAR_FATID_0102, FatNum, Empresa);
      SQL_FAT_TOT := DmNFe.SQL_FAT_TOT_Prod1_Padrao(VAR_FATID_0102, FatNum, Empresa);
      SQL_VOLUMES := DmNFe.SQL_VOLUMES_Prod1_Padrao(VAR_FATID_0102, FatNum, Empresa);
      if DBCheck.CriaFm(TFmNFeSteps, FmNFeSteps, afmoNegarComAviso) then
      begin
        FmNFeSteps.Show;
        //
        (*
        GravaCampos := Application.MessageBox(PChar('Deseja salvar demonstrativo ' +
        ' no banco de dados para verifica��o de dados em relat�rio? ' + sLineBreak +
        'Este demostrativo n�o � obrigat�rio e sua grava��o pode ser demorada!' +
        sLineBreak + 'Deseja salvar assim mesmo?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
        *)
        GravaCampos := ID_NO;
        if GravaCampos <> ID_CANCEL then
          FmNFeSteps.Cria NFe Normal(Recria, NFeStatus, VAR_FATID_0102, FatNum, Empresa,
            IDCtrl, Cliente, FretePor, modFrete, Transporta, ide_indPag,
            RegrFiscal, FreteVal, Seguro, Outros, ide_Serie,
            ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF, ide_tpEmis,
            infAdic_infAdFisco, infAdic_infCpl,
            VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
            Exporta_UFEmbarq, Exporta_xLocEmbarq,
            SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
            SQL_CUSTOMZ, UF_TXT_Emp, UF_TXT_Cli, GravaCampos)
        else Geral.MB_Aviso('Gera��o total da NF-e cancelada pelo usu�rio!');
        //
        FmNFeSteps.Destroy;
      end;
    end;
    *)
    ReopenFatPedNFs(QrFatPedNFsFilial.Value);
  end;
}  
end;

procedure TFmCMPTOut.ReopenCMPTOut(Codigo: Integer; OrderBy: String);
var
  Cliente, Empresa: Integer;
  Ordem: String;
begin
  if FCriando then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    Cliente := Geral.IMV(EdClientePesq.Text);
    UMyMod.ObtemCodigoDeCodUsu(EdFilialEdit, Empresa, '', 'Codigo', 'Filial');
    //
(*
    QrCMPTOut.Close;
    QrCMPTOut.SQL.Clear;
    QrCMPTOut.SQL.Add('SELECT  ppc.MedDDSimpl,');
    QrCMPTOut.SQL.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI,');
    QrCMPTOut.SQL.Add('cli.Tipo CLI_TIPO, cli.IE CLI_IE,');
    QrCMPTOut.SQL.Add('IF(cli.Tipo=0, cli.EUF, cli.PUF) + 0.000 CLI_UF,');
    QrCMPTOut.SQL.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,');
    QrCMPTOut.SQL.Add('emp.Filial, frc.ModeloNF, frc.infAdFisco,');
    QrCMPTOut.SQL.Add('frc.Financeiro, car.Tipo TIPOCART,');
    QrCMPTOut.SQL.Add('IF(emp.Tipo=0, emp.EUF, emp.PUF) + 0.000 EMP_UF,');
    QrCMPTOut.SQL.Add('');
    QrCMPTOut.SQL.Add('par.Associada,');
    QrCMPTOut.SQL.Add('par.CtaServico EMP_CtaServico,');
    QrCMPTOut.SQL.Add('par.FaturaSeq EMP_FaturaSeq,');
    QrCMPTOut.SQL.Add('par.FaturaSep EMP_FaturaSep,');
    QrCMPTOut.SQL.Add('par.FaturaDta EMP_FaturaDta,');
    QrCMPTOut.SQL.Add('par.FaturaNum EMP_FaturaNum,');
    QrCMPTOut.SQL.Add('par.TxtServico EMP_TxtServico,');
    QrCMPTOut.SQL.Add('par.DupServico  EMP_DupServico,');
    QrCMPTOut.SQL.Add('emp.Filial EMP_FILIAL,');
    QrCMPTOut.SQL.Add('');
    QrCMPTOut.SQL.Add('ase.Filial ASS_FILIAL,');
    QrCMPTOut.SQL.Add('IF(ase.Tipo=0, ase.EUF, ase.PUF) +0.000 ASS_CO_UF,');
    QrCMPTOut.SQL.Add('');
    QrCMPTOut.SQL.Add('ass.CtaServico ASS_CtaServico,');
    QrCMPTOut.SQL.Add('ass.FaturaSeq ASS_FaturaSeq,');
    QrCMPTOut.SQL.Add('ass.FaturaSep ASS_FaturaSep,');
    QrCMPTOut.SQL.Add('ass.FaturaDta ASS_FaturaDta,');
    QrCMPTOut.SQL.Add('ass.FaturaNum ASS_FaturaNum,');
    QrCMPTOut.SQL.Add('ass.TxtServico ASS_TxtServico,');
    QrCMPTOut.SQL.Add('ass.DupServico  ASS_DupServico,');
    QrCMPTOut.SQL.Add('');
    QrCMPTOut.SQL.Add('ufe.Nome UF_TXT_Emp, ufc.Nome UF_TXT_Cli,');
    QrCMPTOut.SQL.Add('');
    QrCMPTOut.SQL.Add('cmp.*');
    QrCMPTOut.SQL.Add('');
    QrCMPTOut.SQL.Add('FROM cmptout cmp');
    QrCMPTOut.SQL.Add('LEFT JOIN entidades cli ON cmp.Cliente=cli.Codigo');
    QrCMPTOut.SQL.Add('LEFT JOIN entidades emp ON cmp.Empresa=emp.Codigo');
    QrCMPTOut.SQL.Add('LEFT JOIN paramsemp  par ON par.Codigo=cmp.Empresa');
    QrCMPTOut.SQL.Add('LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada');
    QrCMPTOut.SQL.Add('LEFT JOIN entidades ase ON ase.Codigo=ass.Codigo');
    QrCMPTOut.SQL.Add('LEFT JOIN fisregcad frc ON frc.Codigo=cmp.FisRegCad');
    QrCMPTOut.SQL.Add('LEFT JOIN carteiras car ON cmp.Codigo=cmp.Carteira');
    QrCMPTOut.SQL.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=cmp.CondicaoPg');
    QrCMPTOut.SQL.Add('LEFT JOIN ufs ufe ON ufe.Codigo=IF(emp.Tipo=0, emp.EUF, emp.PUF)');
    QrCMPTOut.SQL.Add('LEFT JOIN ufs ufc ON ufc.Codigo=IF(cli.Tipo=0, cli.EUF, cli.PUF)');
    QrCMPTOut.SQL.Add('');
    //
    QrCMPTOut.SQL.Add(dmkPF.SQL_Periodo('WHERE cmp.DataS ',
      TPIni.date, TPFim.Date, CkIni.Checked, CkFim.Checked));
    if Cliente <> 0 then
      QrCMPTOut.SQL.Add(' AND cmp.Cliente=' + FormatFloat('0', Cliente));
    if Empresa <> 0 then
      QrCMPTOut.SQL.Add(' AND cmp.Empresa=' + FormatFloat('0', Empresa));
    QrCMPTOut.SQL.Add('ORDER BY DataS DESC');
    UnDmkDAC_PF.AbreQuery(QrCMPTOut . O p e n;
*)
    if OrderBy <> '' then
      Ordem := OrderBy
    else
      Ordem := 'ORDER BY DataS DESC';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCMPTOut, Dmod.MyDB, [
    'SELECT emp.Filial, ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI, ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP, ',
    'cmp.* ',
    'FROM cmptout cmp ',
    'LEFT JOIN entidades cli ON cmp.Cliente=cli.Codigo ',
    'LEFT JOIN entidades emp ON cmp.Empresa=emp.Codigo ',
    dmkPF.SQL_Periodo('WHERE cmp.DataS ',
      TPIni.date, TPFim.Date, CkIni.Checked, CkFim.Checked),
    Geral.ATS_if(Cliente <> 0, [' AND cmp.Cliente=' + FormatFloat('0', Cliente)]),
    Geral.ATS_if(Empresa <> 0, [' AND cmp.Empresa=' + FormatFloat('0', Empresa)]),
    Ordem,
    '']);
    //
    if Codigo > 0 then
      QrCMPTOut.Locate('Codigo', Codigo, []);
  finally
    Screen.Cursor := crDefault;
  end;
  //
end;

procedure TFmCMPTOut.ReopenCMPTOutIts(Controle: Integer);
begin
  (*
  QrCMPTOutIts.Close;
  QrCMPTOutIts.Params[0].AsInteger := QrCMPTOutCodigo.Value;
  QrCMPTOutIts . O p e n ;
  *)
  UnDmkDAC_PF.AbreMySQLQuery0(QrCMPTOutIts, Dmod.MyDB, [
  'SELECT inn.DataE, inn.NFInn, inn.NFRef, inn.GraGruX, ',
  'inn.TipoNF, inn.refNFe, inn.modNF, inn.Serie, its.* ',
  'FROM CMPToutits its ',
  'LEFT JOIN CMPTinn inn ON inn.Codigo=its.CMPTInn ',
  'WHERE its.Codigo=' + Geral.FF0(QrCMPTOutCodigo.Value),
  'ORDER BY DataE ',
  '']);
  //
  if Controle < 0 then
    QrCMPTOutIts.Locate('Controle', Controle, []);
end;

procedure TFmCMPTOut.Retiraitemdedevoluo1Click(Sender: TObject);
var
  CMPTInn, Codigo, Controle: Integer;
begin
  if Application.MessageBox('Confirma a retirada do item selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    CMPTInn  := QrCMPTOutItsCMPTinn.Value;
    Codigo   := QrCMPTOutItsCodigo.Value;
    Controle := QrCMPTOutItsControle.Value;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM CMPToutits WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.AtualizaEstoqueCMPT(CMPTInn, Codigo);
    FmCMPTOut.ReopenCMPTOut(Codigo);
    //
  end;
end;

procedure TFmCMPTOut.RGAbertosClick(Sender: TObject);
begin
  ReopenCMPTOut(QrCMPTOutCodigo.Value);
end;

procedure TFmCMPTOut.SbRegrFiscalClick(Sender: TObject);
begin
(*
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFisRegCad;
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrFisRegCad . O p e n ;
    if DmPediVda.QrFisRegCad.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdRegrFiscal.ValueVariant := DmPediVda.QrFisRegCadCodUsu.Value;
      CBRegrFiscal.KeyValue     := DmPediVda.QrFisRegCadCodUsu.Value;
    end;
  end;
*)
end;

procedure TFmCMPTOut.SpeedButton13Click(Sender: TObject);
begin
(*
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UnDmkDAC_PF.AbreQuery(DmPediVda.QrCartEmis, Dmod.MyDB);
      EdMoeda.ValueVariant := VAR_CADASTRO;
      CBMoeda.KeyValue     := VAR_CADASTRO;
    end;
  end;
*)
end;

procedure TFmCMPTOut.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  DModG.CadastroDeEntidade(EdClienteEdit.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrClientesEdit, Dmod.MyDB);
    //
    EdClienteEdit.ValueVariant := VAR_CADASTRO;
    CBClienteEdit.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmCMPTOut.SpeedButton6Click(Sender: TObject);
begin
(*
  VAR_ENTIDADE := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    if VAR_ENTIDADE <> 0 then
    begin
      UnDmkDAC_PF.AbreQuery(DmPediVda.QrTransportas, Dmod.MyDB);
      if DmPediVda.QrTransportas.Locate('Codigo', VAR_ENTIDADE, []) then
      begin
        EdTransporta.ValueVariant := VAR_ENTIDADE;
        CBTransporta.KeyValue     := VAR_ENTIDADE;
      end;
    end;
  end;
*)
end;

procedure TFmCMPTOut.TPFimClick(Sender: TObject);
begin
  ReopenCMPTOut(QrCMPTOutCodigo.Value);
end;

procedure TFmCMPTOut.TPIniClick(Sender: TObject);
begin
  ReopenCMPTOut(QrCMPTOutCodigo.Value);
end;

procedure TFmCMPTOut.ReopenFatPedNFs(Filial: Integer);
begin
(*
  QrFatPedNFs.Close;
  QrFatPedNFs.Params[00].AsInteger := VAR_FATID_0102;
  QrFatPedNFs.Params[01].AsInteger := QrCMPTOutCodigo.Value;
  QrFatPedNFs.Params[02].AsInteger := QrCMPTOutEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrFatPedNFs . O p e n ;
  //
  QrFatPedNFs.Locate('Filial', Filial, []);
*)
end;

end.

