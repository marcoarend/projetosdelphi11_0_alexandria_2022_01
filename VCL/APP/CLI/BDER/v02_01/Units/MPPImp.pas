unit MPPImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DBCtrls, Db, DmkDAC_PF,
  mySQLDbTables, ComCtrls, Menus, frxClass, frxDBSet, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkGeral, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmMPPImp = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    Label10: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrMPVIts: TmySQLQuery;
    DsMPVIts: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DBGrid1: TDBGrid;
    CBMP: TdmkDBLookupComboBox;
    EdMP: TdmkEditCB;
    Label1: TLabel;
    EdVendedor: TdmkEditCB;
    Label2: TLabel;
    CBVendedor: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdTexto: TdmkEdit;
    RgFiltro: TRadioGroup;
    QrMP: TmySQLQuery;
    DsMP: TDataSource;
    QrMPCodigo: TIntegerField;
    QrMPNome: TWideStringField;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    PnImprime: TPanel;
    RGAgrupa: TRadioGroup;
    RGOrdem4: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    RGOrdem5: TRadioGroup;
    PMEntrega: TPopupMenu;
    Alteraentrega1: TMenuItem;
    Desfazentrega1: TMenuItem;
    QrMPVItsVendedor: TIntegerField;
    QrMPVItsCliente: TIntegerField;
    QrMPVItsNOMEMP: TWideStringField;
    QrMPVItsDataF: TDateField;
    QrMPVItsVALORMPP: TFloatField;
    QrMPVItsCodigo: TIntegerField;
    QrMPVItsControle: TIntegerField;
    QrMPVItsMP: TIntegerField;
    QrMPVItsQtde: TFloatField;
    QrMPVItsPreco: TFloatField;
    QrMPVItsValor: TFloatField;
    QrMPVItsTexto: TWideStringField;
    QrMPVItsLk: TIntegerField;
    QrMPVItsDataCad: TDateField;
    QrMPVItsDataAlt: TDateField;
    QrMPVItsUserCad: TIntegerField;
    QrMPVItsUserAlt: TIntegerField;
    QrMPVItsDesco: TFloatField;
    QrMPVItsEntrega: TDateField;
    QrMPVItsPronto: TDateField;
    QrMPVItsStatus: TIntegerField;
    QrMPVItsFluxo: TIntegerField;
    QrMPVItsClasse: TWideStringField;
    QrMPVItsPedido: TIntegerField;
    QrMPVItsEspesTxt: TWideStringField;
    QrMPVItsCorTxt: TWideStringField;
    QrMPVItsM2Pedido: TFloatField;
    QrMPVItsPecas: TFloatField;
    QrMPVItsUnidade: TIntegerField;
    QrMPVItsObserv: TWideMemoField;
    QrMPVItsAlterWeb: TSmallintField;
    QrMPVItsPrecoPed: TFloatField;
    QrMPVItsValorPed: TFloatField;
    QrMPVItsNOMEVENDEDOR: TWideStringField;
    QrMPVItsNOMECLIENTE: TWideStringField;
    frxMPVIts: TfrxReport;
    frxDsMPVIts: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    BtImprime: TBitBtn;
    BtEntrega: TBitBtn;
    GroupBox1: TGroupBox;
    Panel4: TPanel;
    CkIniPed: TCheckBox;
    TPIniPed: TDateTimePicker;
    CkFimPed: TCheckBox;
    TPFimPed: TDateTimePicker;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    CkIniEnt: TCheckBox;
    TPIniEnt: TDateTimePicker;
    CkFimEnt: TCheckBox;
    TPFimEnt: TDateTimePicker;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMPChange(Sender: TObject);
    procedure EdVendedorChange(Sender: TObject);
    procedure EdTextoChange(Sender: TObject);
    procedure RgFiltroClick(Sender: TObject);
    procedure QrMPVItsBeforeClose(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure QrMPVItsAfterOpen(DataSet: TDataSet);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure RGOrdem3Click(Sender: TObject);
    procedure RGOrdem4Click(Sender: TObject);
    procedure RGOrdem5Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure TPIniPedChange(Sender: TObject);
    procedure TPFimPedChange(Sender: TObject);
    procedure BtEntregaClick(Sender: TObject);
    procedure Alteraentrega1Click(Sender: TObject);
    procedure Desfazentrega1Click(Sender: TObject);
    procedure frxMPVItsGetValue(const VarName: String; var Value: Variant);
  private
    { Private declarations }
    procedure FechaMPVIts;
    procedure AbreMPVIts;
    function OrdemBy1(Item: Integer): String;
    function OrdemBy2(Item: Integer): String;
  public
    { Public declarations }
  end;

  var
  FmMPPImp: TFmMPPImp;

implementation

uses UnMyObjects, UnInternalConsts, Principal, UMySQLModule, Module, ModuleGeral;

{$R *.DFM}

procedure TFmMPPImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPPImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPPImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPPImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.MyDB);
  if FM_MASTER = 'F'then //QrVendedores.Open else
  begin
    EdVendedor.Text := IntToStr(VAR_FUNCILOGIN);
    CBVendedor.KeyValue := VAR_FUNCILOGIN;
    EdVendedor.Enabled := False;
    CBVendedor.Enabled := False;
  end;
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMP, Dmod.MyDB);
  TPIniPed.Date := Date-30;
  TPFimPed.Date := Date;
  TPIniEnt.Date := Date;
  TPFimEnt.Date := Date + 30;
end;

procedure TFmMPPImp.EdClienteChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPPImp.EdMPChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPPImp.EdVendedorChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPPImp.FechaMPVIts;
begin
  QrMPVIts.Close;
end;

procedure TFmMPPImp.BtOKClick(Sender: TObject);
begin
  AbreMPVIts;
end;

procedure TFmMPPImp.AbreMPVIts;
var
  Pre, Pos: String;
  Cliente, MP, Vendedor: Integer;
begin
  QrMPVIts.Close;
  QrMPVIts.SQL.Clear;
  QrMPVIts.SQL.Add('SELECT mpp.Vendedor, mpp.Cliente, art.Nome NOMEMP, mpp.DataF,');
  QrMPVIts.SQL.Add('mpp.Valor VALORMPP, mvi.*,');
  QrMPVIts.SQL.Add('CASE WHEN ven.Tipo=0 THEN ven.RazaoSocial ELSE ven.Nome END NOMEVENDEDOR,');
  QrMPVIts.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLIENTE');
  QrMPVIts.SQL.Add('FROM mpvits mvi');
  QrMPVIts.SQL.Add('LEFT JOIN mpp       mpp ON mpp.Codigo=mvi.Pedido');
  QrMPVIts.SQL.Add('LEFT JOIN entidades ven ON ven.Codigo=mpp.Vendedor');
  QrMPVIts.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=mpp.Cliente');
  QrMPVIts.SQL.Add('LEFT JOIN artigosgrupos art ON art.Codigo=mvi.MP');
  QrMPVIts.SQL.Add('');
  QrMPVIts.SQL.Add(dmkPF.SQL_Periodo('WHERE mpp.DataF ', TPIniPed.Date,
      TPFimPed.Date, CkIniPed.Checked, CkFimPed.Checked));
  QrMPVIts.SQL.Add(dmkPF.SQL_Periodo('AND mvi.Entrega ', TPIniEnt.Date,
      TPFimEnt.Date, CkIniEnt.Checked, CkFimEnt.Checked));
  //
  QrMPVIts.SQL.Add('');
  Cliente := EdCliente.ValueVariant;
  if Cliente <> 0 then
    QrMPVIts.SQL.Add('AND mpp.Cliente = ' + Geral.FF0(Cliente));
  MP := EdMP.ValueVariant;
  if MP <> 0 then
    QrMPVIts.SQL.Add('AND mvi.MP = '  + Geral.FF0(MP));
  Vendedor := EdVendedor.ValueVariant;
  if Vendedor <> 0 then
    QrMPVIts.SQL.Add('AND mpp.Vendedor = ' + Geral.FF0(Vendedor));
  if EdTexto.Text <> '' then
  begin
    if RgFiltro.ItemIndex in ([1,2]) then Pre := '%' else Pre := '';
    if RgFiltro.ItemIndex in ([1,3]) then Pos := '%' else Pos := '';
    QrMPVIts.SQL.Add('AND mvi.Texto LIKE "'+Pre+EdTexto.Text+Pos+'"');
  end;
  //////////////////////////////////////////////////////////////////////////////
  QrMPVIts.SQL.Add('ORDER BY '+
  OrdemBy1(RGOrdem1.ItemIndex)+', '+
  OrdemBy1(RGOrdem2.ItemIndex)+', '+
  OrdemBy1(RGOrdem3.ItemIndex)+', '+
  OrdemBy1(RGOrdem4.ItemIndex)+', '+
  OrdemBy1(RGOrdem5.ItemIndex));
  UnDmkDAC_PF.AbreQuery(QrMPVIts, Dmod.MyDB);
end;

function TFmMPPImp.OrdemBy1(Item: Integer): String;
begin
  case Item of
    0: Result := 'NOMECLIENTE';
    1: Result := 'mvi.MP';
    2: Result := 'NOMEVENDEDOR';
    3: Result := 'mvi.Texto';
    4: Result := 'mpp.DataF';
    5: Result := 'mvi.Entrega';
    else Result := '**EROR OrdemBy **';
  end;
end;

function TFmMPPImp.OrdemBy2(Item: Integer): String;
begin
  case Item of
    0: Result := 'NOMECLIENTE';
    1: Result := 'NOMEMP';
    2: Result := 'NOMEVENDEDOR';
    3: Result := 'Texto';
    4: Result := 'DataF';
    5: Result := 'Entrega';
    else Result := '**EROR OrdemBy **';
  end;
end;

procedure TFmMPPImp.EdTextoChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPPImp.RgFiltroClick(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPPImp.QrMPVItsBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmMPPImp.QrMPVItsAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := True;
end;

procedure TFmMPPImp.RGOrdem1Click(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPPImp.RGOrdem2Click(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPPImp.RGOrdem3Click(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPPImp.RGOrdem4Click(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPPImp.RGOrdem5Click(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPPImp.BtImprimeClick(Sender: TObject);
  function Condicao(Agrupa: Boolean; Item: Integer): String;
  begin
    if Agrupa then
      Result := '''' + 'frxDsMPVIts."' + OrdemBy2(Item) + '"'''
    else Result := '';
  end;

begin
  frxMPVIts.Variables['VARF_VISI1']   := RGAgrupa.ItemIndex > 0;
  frxMPVIts.Variables['VARF_VISI2']   := RGAgrupa.ItemIndex > 1;
  frxMPVIts.Variables['VARF_VISI3']   := RGAgrupa.ItemIndex > 2;

  frxMPVIts.Variables['VARF_COND1']   := Condicao(RGAgrupa.ItemIndex > 0, RGOrdem1.ItemIndex);
  frxMPVIts.Variables['VARF_COND2']   := Condicao(RGAgrupa.ItemIndex > 1, RGOrdem2.ItemIndex);
  frxMPVIts.Variables['VARF_COND3']   := Condicao(RGAgrupa.ItemIndex > 2, RGOrdem3.ItemIndex);

  frxMPVIts.Variables['VARF_MEMO1']   := '''' + RGOrdem1.Items[RGOrdem1.ItemIndex] +
    ': ' + '[frxDsMPVIts."' + OrdemBy2(RGOrdem1.ItemIndex) + '"]''';
  frxMPVIts.Variables['VARF_MEMO2']   := '''' + RGOrdem2.Items[RGOrdem2.ItemIndex] +
    ': ' + '[frxDsMPVIts."' + OrdemBy2(RGOrdem2.ItemIndex) + '"]''';
  frxMPVIts.Variables['VARF_MEMO3']   := '''' + RGOrdem3.Items[RGOrdem3.ItemIndex] +
    ': ' + '[frxDsMPVIts."' + OrdemBy2(RGOrdem3.ItemIndex) + '"]''';

  //

  MyObjects.frxDefineDataSets(frxMPVIts, [DmodG.frxDsDono, frxDsMPVIts]);
  MyObjects.frxMostra(frxMPVIts, 'Relat�rio de vendas');
end;

procedure TFmMPPImp.TPIniPedChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPPImp.TPFimPedChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPPImp.BtEntregaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntrega, BtEntrega);
end;

procedure TFmMPPImp.Alteraentrega1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrMPVItsControle.Value;
  Fmprincipal.StatusPedido(Controle, QrMPVItsEntrega.Value);
  //Controle := UMyMod.ProximoRegistro(QrMPVIts, 'Controle', Controle);
  AbreMPVIts;
end;

procedure TFmMPPImp.Desfazentrega1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrMPVItsControle.Value;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE mpvits SET Pronto=0');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
  Dmod.QrUpd.Params[00].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  //Controle := UMyMod.ProximoRegistro(QrMPVIts, 'Controle', Controle);
  AbreMPVIts;
end;

procedure TFmMPPImp.frxMPVItsGetValue(const VarName: String;
  var Value: Variant);
var
  Pre, Pos: String;
begin
  if VarName = 'VARF_PERIODO' then Value :=  
    dmkPF.PeriodoImp(TPIniPed.Date, TPFimPed.Date, 0, 0, CkIniPed.Checked,
      CkFimPed.Checked, False, False, 'Emiss�o:', '') +
    dmkPF.PeriodoImp(TPIniEnt.Date, TPFimEnt.Date, 0, 0, CkIniEnt.Checked,
      CkFimEnt.Checked, False, False, 'Entrega:', '')
  else if VarName = 'VARF_CLIENTE' then
    Value := dmkPF.CampoReportTxt(CBCliente.Text, 'TODOS')
  else if VarName = 'VARF_MP' then
    Value := dmkPF.CampoReportTxt(CBMP.Text, 'TODOS')
  else if VarName = 'VARF_VENDEDOR' then
    Value := dmkPF.CampoReportTxt(CBVendedor.Text, 'TODOS')
  else if VarName = 'VARF_DESCRICAO' then
  begin
    if EdTexto.Text <> '' then
    begin
      if RgFiltro.ItemIndex in ([1,2]) then Pre := '%' else Pre := '';
      if RgFiltro.ItemIndex in ([1,3]) then Pos := '%' else Pos := '';
      Value := Pre+EdTexto.Text+Pos;
    end else Value := ' ';
  end
  else
  if AnsiCompareText(VarName, 'VARF_DATA') = 0 then
    Value := Geral.FDT(Date, 3)
end;

end.

