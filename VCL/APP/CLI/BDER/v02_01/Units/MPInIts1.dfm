object FmMPInIts1: TFmMPInIts1
  Left = 339
  Top = 166
  Caption = 'COU-MP_IN-007 :: Itens de Mat'#233'ria-prima'
  ClientHeight = 696
  ClientWidth = 1002
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1002
    Height = 540
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 40
      Width = 1002
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitLeft = 1
      ExplicitTop = 41
      ExplicitWidth = 854
    end
    object DBGrid2: TDBGrid
      Left = 0
      Top = 0
      Width = 1002
      Height = 40
      TabStop = False
      Align = alTop
      DataSource = FmMPIn.DsMPIn
      Enabled = False
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'OS'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLIENTEI'
          Title.Caption = 'Cliente interno'
          Width = 128
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPROCEDENCIA'
          Title.Caption = 'Proced'#234'ncia'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PecasNF'
          Title.Caption = 'P'#231' NF'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PNF'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas curtume'
          Title.Caption = 'P'#231' Ent.'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PLE'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Frete'
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 43
      Width = 1002
      Height = 217
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 540
        Height = 217
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        TabOrder = 0
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 540
          Height = 217
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 540
            Height = 89
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 540
              Height = 89
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label1: TLabel
                Left = 4
                Top = 46
                Width = 50
                Height = 13
                Caption = 'Caminh'#227'o:'
              end
              object Label10: TLabel
                Left = 56
                Top = 46
                Width = 53
                Height = 13
                Caption = 'Couros NF:'
              end
              object Label8: TLabel
                Left = 116
                Top = 46
                Width = 57
                Height = 13
                Caption = 'Pe'#231'as entr.:'
              end
              object Label4: TLabel
                Left = 260
                Top = 46
                Width = 38
                Height = 13
                Caption = 'PLE kg:'
              end
              object Label7: TLabel
                Left = 344
                Top = 46
                Width = 40
                Height = 13
                Caption = 'PDA kg:'
              end
              object Label35: TLabel
                Left = 4
                Top = 4
                Width = 138
                Height = 13
                Caption = 'Fornecedor original do couro:'
              end
              object Label37: TLabel
                Left = 176
                Top = 46
                Width = 44
                Height = 13
                Caption = 'PNF real:'
              end
              object EdCaminhao: TdmkEdit
                Left = 4
                Top = 62
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdPecasNF: TdmkEdit
                Left = 56
                Top = 62
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 1
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPecasNFChange
              end
              object EdPecas: TdmkEdit
                Left = 116
                Top = 62
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 1
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPecasChange
              end
              object EdPLE: TdmkEdit
                Left = 260
                Top = 62
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 7
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 3
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPLEChange
              end
              object EdPDA: TdmkEdit
                Left = 344
                Top = 62
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 3
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPDAChange
              end
              object EdFornece: TdmkEditCB
                Left = 4
                Top = 20
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdForneceChange
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBFornece: TdmkDBLookupComboBox
                Left = 53
                Top = 20
                Width = 460
                Height = 21
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'NOMEENTIDADE'
                ListSource = DsFornece
                TabOrder = 1
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object BitBtn1: TBitBtn
                Left = 516
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                TabOrder = 2
                TabStop = False
                OnClick = BitBtn1Click
              end
              object EdPNF: TdmkEdit
                Left = 176
                Top = 62
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 3
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPNF_FatxxxChange
              end
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 89
            Width = 540
            Height = 128
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 540
              Height = 63
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object GroupBox5: TGroupBox
                Left = 0
                Top = 0
                Width = 269
                Height = 63
                Align = alLeft
                Caption = ' Custo compra: '
                TabOrder = 0
                object Label15: TLabel
                  Left = 4
                  Top = 16
                  Width = 40
                  Height = 13
                  Caption = '$ Couro:'
                end
                object Label16: TLabel
                  Left = 92
                  Top = 16
                  Width = 36
                  Height = 13
                  Caption = '$ Frete:'
                end
                object Label30: TLabel
                  Left = 180
                  Top = 16
                  Width = 64
                  Height = 13
                  Caption = '$ Devolu'#231#227'o:'
                end
                object EdCMPValor: TdmkEdit
                  Left = 4
                  Top = 32
                  Width = 84
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCMPFrete: TdmkEdit
                  Left = 92
                  Top = 32
                  Width = 84
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCMPDevol: TdmkEdit
                  Left = 180
                  Top = 32
                  Width = 84
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object GroupBox2: TGroupBox
                Left = 269
                Top = 0
                Width = 271
                Height = 63
                Align = alClient
                Caption = ' Complemento: '
                TabOrder = 1
                object LaCPLValor: TLabel
                  Left = 4
                  Top = 16
                  Width = 40
                  Height = 13
                  Caption = '$ Couro:'
                end
                object Label9: TLabel
                  Left = 92
                  Top = 16
                  Width = 36
                  Height = 13
                  Caption = '$ Frete:'
                  Enabled = False
                end
                object Label32: TLabel
                  Left = 180
                  Top = 16
                  Width = 64
                  Height = 13
                  Caption = '$ Devolu'#231#227'o:'
                  Enabled = False
                end
                object EdCPLValor: TdmkEdit
                  Left = 4
                  Top = 32
                  Width = 84
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCPLFrete: TdmkEdit
                  Left = 92
                  Top = 32
                  Width = 84
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCPLDevol: TdmkEdit
                  Left = 180
                  Top = 32
                  Width = 84
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
            object Panel11: TPanel
              Left = 0
              Top = 63
              Width = 540
              Height = 65
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object Panel18: TPanel
                Left = 0
                Top = 0
                Width = 540
                Height = 65
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Memo1: TMemo
                  Left = 253
                  Top = 0
                  Width = 287
                  Height = 65
                  TabStop = False
                  Align = alClient
                  BevelOuter = bvNone
                  BorderStyle = bsNone
                  Color = clBtnFace
                  Lines.Strings = (
                    '  PNF: Peso nota fiscal (peso origem).'
                    '  PLE: Peso l'#237'quido de entrada (peso recep'#231#227'o - balan'#231#227'o).'
                    '  PDA: Peso descarnado e aparado (peso curtume).')
                  ReadOnly = True
                  TabOrder = 0
                end
                object GroupBox9: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 253
                  Height = 65
                  Align = alLeft
                  Caption = ' Presta'#231#227'o de servi'#231'o: '
                  TabOrder = 1
                  object Label52: TLabel
                    Left = 4
                    Top = 16
                    Width = 24
                    Height = 13
                    Caption = '$ kg:'
                  end
                  object Label53: TLabel
                    Left = 72
                    Top = 16
                    Width = 73
                    Height = 13
                    Caption = 'Quantidade kg:'
                  end
                  object Label54: TLabel
                    Left = 148
                    Top = 16
                    Width = 50
                    Height = 13
                    Caption = 'Valor total:'
                  end
                  object EdPS_ValUni: TdmkEdit
                    Left = 4
                    Top = 32
                    Width = 65
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 4
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,0000'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdPS_ValUniChange
                  end
                  object EdPS_QtdTot: TdmkEdit
                    Left = 72
                    Top = 32
                    Width = 73
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 3
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,000'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdPS_QtdTotChange
                  end
                  object EdPS_ValTot: TdmkEdit
                    Left = 148
                    Top = 32
                    Width = 97
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdPS_ValTotChange
                  end
                end
              end
            end
          end
        end
      end
      object Panel12: TPanel
        Left = 540
        Top = 0
        Width = 462
        Height = 217
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object TPanel
          Left = 0
          Top = 0
          Width = 462
          Height = 217
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox6: TGroupBox
            Left = 0
            Top = 0
            Width = 462
            Height = 60
            Align = alTop
            Caption = ' Informa'#231#227'o de c'#226'mbio do dia ou o mais pr'#243'ximo: '
            TabOrder = 0
            object SpeedButton1: TSpeedButton
              Left = 284
              Top = 32
              Width = 21
              Height = 21
              OnClick = SpeedButton1Click
            end
            object Panel13: TPanel
              Left = 2
              Top = 15
              Width = 279
              Height = 43
              Align = alLeft
              BevelOuter = bvNone
              Enabled = False
              TabOrder = 0
              object Label23: TLabel
                Left = 212
                Top = 4
                Width = 50
                Height = 13
                Caption = 'Indexador:'
                FocusControl = DBEdit4
              end
              object Label22: TLabel
                Left = 138
                Top = 4
                Width = 25
                Height = 13
                Caption = 'Euro:'
                FocusControl = DBEdit3
              end
              object Label21: TLabel
                Left = 64
                Top = 4
                Width = 28
                Height = 13
                Caption = 'D'#243'lar:'
                FocusControl = DBEdit2
              end
              object Label20: TLabel
                Left = 4
                Top = 4
                Width = 26
                Height = 13
                Caption = 'Data:'
                FocusControl = DBEdit1
              end
              object DBEdit4: TDBEdit
                Left = 209
                Top = 18
                Width = 70
                Height = 21
                DataField = 'Indexador'
                DataSource = DsCambios
                TabOrder = 0
              end
              object DBEdit3: TDBEdit
                Left = 138
                Top = 18
                Width = 70
                Height = 21
                DataField = 'Euro'
                DataSource = DsCambios
                TabOrder = 1
              end
              object DBEdit2: TDBEdit
                Left = 64
                Top = 18
                Width = 70
                Height = 21
                DataField = 'Dolar'
                DataSource = DsCambios
                TabOrder = 2
              end
              object DBEdit1: TDBEdit
                Left = 4
                Top = 18
                Width = 56
                Height = 21
                DataField = 'Data'
                DataSource = DsCambios
                TabOrder = 3
              end
            end
          end
          object GroupBox7: TGroupBox
            Left = 0
            Top = 60
            Width = 462
            Height = 60
            Align = alTop
            Caption = ' C'#226'mbio utilizado: '
            TabOrder = 1
            object Label24: TLabel
              Left = 4
              Top = 16
              Width = 28
              Height = 13
              Caption = 'D'#243'lar:'
            end
            object Label25: TLabel
              Left = 96
              Top = 16
              Width = 25
              Height = 13
              Caption = 'Euro:'
            end
            object Label26: TLabel
              Left = 188
              Top = 16
              Width = 50
              Height = 13
              Caption = 'Indexador:'
            end
            object SpeedButton2: TSpeedButton
              Left = 284
              Top = 32
              Width = 21
              Height = 21
              OnClick = SpeedButton2Click
            end
            object EdDolar: TdmkEdit
              Left = 4
              Top = 32
              Width = 88
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdDolarChange
            end
            object EdEuro: TdmkEdit
              Left = 96
              Top = 32
              Width = 88
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdEuroChange
            end
            object EdIndexador: TdmkEdit
              Left = 188
              Top = 32
              Width = 94
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = EdIndexadorExit
            end
          end
          object GroupBox8: TGroupBox
            Left = 0
            Top = 146
            Width = 462
            Height = 71
            Align = alBottom
            Caption = '  % Devolu'#231#227'o aplicado: '
            TabOrder = 2
            object Label28: TLabel
              Left = 6
              Top = 20
              Width = 39
              Height = 13
              Caption = 'Compra:'
              Enabled = False
            end
            object Label18: TLabel
              Left = 82
              Top = 20
              Width = 67
              Height = 13
              Caption = 'Complemento:'
              Enabled = False
            end
            object Label29: TLabel
              Left = 234
              Top = 20
              Width = 51
              Height = 13
              Caption = 'Comiss'#227'o::'
              Enabled = False
            end
            object Label34: TLabel
              Left = 158
              Top = 20
              Width = 24
              Height = 13
              Caption = #193'gio:'
              Enabled = False
            end
            object EdCMPQuebra: TdmkEdit
              Left = 6
              Top = 36
              Width = 72
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdCPLQuebra: TdmkEdit
              Left = 82
              Top = 36
              Width = 72
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdCMIQuebra: TdmkEdit
              Left = 234
              Top = 36
              Width = 72
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdAGIQuebra: TdmkEdit
              Left = 158
              Top = 36
              Width = 72
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 260
      Width = 1002
      Height = 280
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Fornecedor '#250'nico'
        object Panel10: TPanel
          Left = 0
          Top = 105
          Width = 994
          Height = 63
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object GroupBox3: TGroupBox
            Left = 0
            Top = 0
            Width = 269
            Height = 63
            Align = alLeft
            Caption = ' '#193'gio: '
            TabOrder = 0
            object Label11: TLabel
              Left = 4
              Top = 16
              Width = 40
              Height = 13
              Caption = '$ Couro:'
              Enabled = False
            end
            object Label12: TLabel
              Left = 92
              Top = 16
              Width = 36
              Height = 13
              Caption = '$ Frete:'
              Enabled = False
            end
            object Label31: TLabel
              Left = 180
              Top = 16
              Width = 64
              Height = 13
              Caption = '$ Devolu'#231#227'o:'
              Enabled = False
            end
            object EdAGIValor: TdmkEdit
              Left = 4
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdAgiFrete: TdmkEdit
              Left = 92
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdAGIDevol: TdmkEdit
              Left = 180
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object GroupBox4: TGroupBox
            Left = 269
            Top = 0
            Width = 272
            Height = 63
            Align = alLeft
            Caption = ' Comiss'#227'o: '
            TabOrder = 1
            object Label13: TLabel
              Left = 4
              Top = 16
              Width = 40
              Height = 13
              Caption = '$ Couro:'
              Enabled = False
            end
            object Label14: TLabel
              Left = 92
              Top = 16
              Width = 36
              Height = 13
              Caption = '$ Frete:'
              Enabled = False
            end
            object Label33: TLabel
              Left = 180
              Top = 16
              Width = 64
              Height = 13
              Caption = '$ Devolu'#231#227'o:'
              Enabled = False
            end
            object EdCMIValor: TdmkEdit
              Left = 4
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdCMIFrete: TdmkEdit
              Left = 92
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdCMIDevol: TdmkEdit
              Left = 180
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object GroupBox1: TGroupBox
            Left = 541
            Top = 0
            Width = 453
            Height = 63
            Align = alClient
            Caption = ' Somas custos: '
            Color = clBtnFace
            ParentBackground = False
            ParentColor = False
            TabOrder = 2
            object Label3: TLabel
              Left = 4
              Top = 16
              Width = 40
              Height = 13
              Caption = '$ Couro:'
              Enabled = False
            end
            object Label5: TLabel
              Left = 92
              Top = 16
              Width = 36
              Height = 13
              Caption = '$ Frete:'
              Enabled = False
            end
            object Label17: TLabel
              Left = 180
              Top = 16
              Width = 73
              Height = 13
              Caption = '$ Couro + frete:'
              Enabled = False
            end
            object Label19: TLabel
              Left = 361
              Top = 16
              Width = 61
              Height = 13
              Caption = '$ Custo final:'
              Enabled = False
            end
            object Label27: TLabel
              Left = 273
              Top = 16
              Width = 47
              Height = 13
              Caption = '% quebra:'
              Enabled = False
            end
            object EdCustoInfo: TdmkEdit
              Left = 4
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdFreteInfo: TdmkEdit
              Left = 92
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdTotalInfo: TdmkEdit
              Left = 180
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdFinalInfo: TdmkEdit
              Left = 361
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdQuebraViagm: TdmkEdit
              Left = 273
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
        object StaticText1: TStaticText
          Left = 0
          Top = 0
          Width = 994
          Height = 20
          Align = alTop
          Alignment = taCenter
          Caption = 
            'Verifique se o c'#226'mbio est'#225' atualizado antes de iniciar inclus'#245'es' +
            '!'
          Font.Charset = ANSI_CHARSET
          Font.Color = 2116575
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
        end
        object Panel17: TPanel
          Left = 0
          Top = 20
          Width = 994
          Height = 85
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object Label6: TLabel
            Left = 4
            Top = 4
            Width = 65
            Height = 13
            Caption = 'Emiss'#227'o N.F.:'
          end
          object Label45: TLabel
            Left = 96
            Top = 4
            Width = 59
            Height = 13
            Caption = 'Vencimento:'
          end
          object Label2: TLabel
            Left = 308
            Top = 4
            Width = 32
            Height = 13
            Caption = 'N'#186' NF:'
          end
          object Label44: TLabel
            Left = 412
            Top = 4
            Width = 43
            Height = 13
            Caption = 'Conhec.:'
          end
          object Label50: TLabel
            Left = 464
            Top = 4
            Width = 84
            Height = 13
            Caption = 'Emiss'#227'o conhec.:'
          end
          object Label51: TLabel
            Left = 556
            Top = 4
            Width = 76
            Height = 13
            Caption = 'Vencto conhec:'
          end
          object Label64: TLabel
            Left = 188
            Top = 4
            Width = 55
            Height = 13
            Caption = 'Modelo NF:'
          end
          object Label65: TLabel
            Left = 248
            Top = 4
            Width = 44
            Height = 13
            Caption = 'S'#233'rie NF:'
          end
          object Label66: TLabel
            Left = 360
            Top = 4
            Width = 31
            Height = 13
            Caption = 'CFOP:'
          end
          object Label67: TLabel
            Left = 4
            Top = 44
            Width = 314
            Height = 13
            Caption = 
              'Emissor avulso da NF (quando for diferente do fornecedor origina' +
              'l):'
          end
          object TPEmissao: TDateTimePicker
            Left = 4
            Top = 20
            Width = 89
            Height = 21
            Date = 39576.000000000000000000
            Time = 0.340970231489336600
            TabOrder = 0
          end
          object TPVencto: TDateTimePicker
            Left = 96
            Top = 20
            Width = 89
            Height = 21
            Date = 39576.000000000000000000
            Time = 0.340970231489336600
            TabOrder = 1
          end
          object EdNF: TdmkEdit
            Left = 308
            Top = 20
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdConheci: TdmkEdit
            Left = 412
            Top = 20
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnExit = EdConheciExit
          end
          object CkNF_Inn_Fut: TCheckBox
            Left = 652
            Top = 24
            Width = 209
            Height = 17
            Caption = 'Ser'#225' criada uma Nota fiscal de entrada.'
            TabOrder = 9
          end
          object EdNF_Modelo: TdmkEdit
            Left = 188
            Top = 20
            Width = 57
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdNF_Serie: TdmkEdit
            Left = 248
            Top = 20
            Width = 57
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCFOP: TdmkEdit
            Left = 360
            Top = 20
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 4
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdEmitNFAvul: TdmkEditCB
            Left = 4
            Top = 60
            Width = 48
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdForneceChange
            DBLookupComboBox = CBEmitNFAvul
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmitNFAvul: TdmkDBLookupComboBox
            Left = 53
            Top = 60
            Width = 460
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsEmit
            TabOrder = 11
            dmkEditCB = EdEmitNFAvul
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object BitBtn3: TBitBtn
            Left = 516
            Top = 60
            Width = 21
            Height = 21
            Caption = '...'
            TabOrder = 12
            TabStop = False
            OnClick = BitBtn3Click
          end
          object TPEmisFrete: TdmkEditDateTimePicker
            Left = 464
            Top = 20
            Width = 90
            Height = 21
            Time = 0.691679363422735900
            TabOrder = 7
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPVctoFrete: TdmkEditDateTimePicker
            Left = 556
            Top = 20
            Width = 90
            Height = 21
            Time = 0.691679363422735900
            TabOrder = 8
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object Panel19: TPanel
          Left = 0
          Top = 168
          Width = 994
          Height = 84
          Align = alClient
          ParentBackground = False
          TabOrder = 3
          object GroupBox10: TGroupBox
            Left = 365
            Top = 1
            Width = 364
            Height = 82
            Align = alLeft
            Caption = ' Impostos a recuperar: '
            TabOrder = 0
            Visible = False
            object Label55: TLabel
              Left = 4
              Top = 16
              Width = 38
              Height = 13
              Caption = '$ ICMS:'
            end
            object Label56: TLabel
              Left = 92
              Top = 16
              Width = 25
              Height = 13
              Caption = '$ IPI:'
            end
            object Label57: TLabel
              Left = 180
              Top = 16
              Width = 29
              Height = 13
              Caption = '$ PIS:'
            end
            object Label58: TLabel
              Left = 273
              Top = 16
              Width = 53
              Height = 13
              Caption = '% COFINS:'
            end
            object dmkEdit1: TdmkEdit
              Left = 4
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdit2: TdmkEdit
              Left = 92
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdit3: TdmkEdit
              Left = 180
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdit4: TdmkEdit
              Left = 273
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object GroupBox11: TGroupBox
            Left = 1
            Top = 1
            Width = 364
            Height = 82
            Align = alLeft
            Caption = ' Impostos pagos: '
            TabOrder = 1
            Visible = False
            object Label59: TLabel
              Left = 4
              Top = 16
              Width = 38
              Height = 13
              Caption = '$ ICMS:'
            end
            object Label60: TLabel
              Left = 92
              Top = 16
              Width = 25
              Height = 13
              Caption = '$ IPI:'
            end
            object Label61: TLabel
              Left = 180
              Top = 16
              Width = 29
              Height = 13
              Caption = '$ PIS:'
            end
            object Label62: TLabel
              Left = 273
              Top = 16
              Width = 53
              Height = 13
              Caption = '% COFINS:'
            end
            object dmkEdit5: TdmkEdit
              Left = 4
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdit6: TdmkEdit
              Left = 92
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdit7: TdmkEdit
              Left = 180
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdit8: TdmkEdit
              Left = 273
              Top = 32
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Fornecedor com sub-fornecedores'
        ImageIndex = 1
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 994
          Height = 115
          Align = alClient
          DataSource = DsMPInRat
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'ProcedeCod'
              Title.Caption = 'C'#243'digo'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ProcedeNom'
              Title.Caption = 'Fornecedor'
              Width = 392
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Custo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PecasNF'
              Title.Caption = 'Pe'#231'as NF'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as LE'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PNF_Fat'
              Title.Caption = 'PNF Fat.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PNF'
              Title.Caption = 'PNF Real'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PLE'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PDA'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Preco'
              Title.Caption = 'Pre'#231'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Frete'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conheci'
              Visible = True
            end>
        end
        object PnInsUpd: TPanel
          Left = 0
          Top = 115
          Width = 994
          Height = 89
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object Label36: TLabel
            Left = 4
            Top = 4
            Width = 102
            Height = 13
            Caption = 'Fornecedor do couro:'
          end
          object Label38: TLabel
            Left = 68
            Top = 46
            Width = 39
            Height = 13
            Caption = 'PNF kg:'
          end
          object Label39: TLabel
            Left = 140
            Top = 46
            Width = 49
            Height = 13
            Caption = '$ total NF:'
          end
          object Label40: TLabel
            Left = 224
            Top = 46
            Width = 40
            Height = 13
            Caption = '$ Couro:'
          end
          object LaTipoRat: TLabel
            Left = 917
            Top = 0
            Width = 77
            Height = 89
            Align = alRight
            Alignment = taCenter
            AutoSize = False
            Caption = 'Travado'
            Font.Charset = ANSI_CHARSET
            Font.Color = 8281908
            Font.Height = -15
            Font.Name = 'Times New Roman'
            Font.Style = [fsBold]
            ParentFont = False
            Transparent = True
            ExplicitLeft = 768
            ExplicitTop = 1
            ExplicitHeight = 87
          end
          object Label41: TLabel
            Left = 284
            Top = 46
            Width = 36
            Height = 13
            Caption = '$ Frete:'
          end
          object Label42: TLabel
            Left = 368
            Top = 47
            Width = 17
            Height = 13
            Caption = 'NF:'
          end
          object Label43: TLabel
            Left = 420
            Top = 47
            Width = 43
            Height = 13
            Caption = 'Conhec.:'
          end
          object Label46: TLabel
            Left = 576
            Top = 4
            Width = 65
            Height = 13
            Caption = 'Emiss'#227'o N.F.:'
          end
          object Label47: TLabel
            Left = 668
            Top = 4
            Width = 59
            Height = 13
            Caption = 'Vencimento:'
          end
          object Label48: TLabel
            Left = 472
            Top = 46
            Width = 84
            Height = 13
            Caption = 'Emiss'#227'o conhec.:'
          end
          object Label49: TLabel
            Left = 564
            Top = 46
            Width = 76
            Height = 13
            Caption = 'Vencto conhec:'
          end
          object Label63: TLabel
            Left = 4
            Top = 46
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object EdProcede: TdmkEditCB
            Left = 4
            Top = 20
            Width = 48
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdProcedeChange
            DBLookupComboBox = CBProcede
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBProcede: TdmkDBLookupComboBox
            Left = 53
            Top = 20
            Width = 496
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsProcede
            TabOrder = 1
            dmkEditCB = EdProcede
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdPNF_Fat2: TdmkEdit
            Left = 68
            Top = 62
            Width = 68
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 1
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPecasNFChange
          end
          object EdCusto: TdmkEdit
            Left = 140
            Top = 62
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdCustoChange
            OnExit = EdCustoExit
          end
          object EdPreco: TdmkEdit
            Left = 224
            Top = 62
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPrecoChange
            OnExit = EdPrecoExit
          end
          object EdFrete: TdmkEdit
            Left = 284
            Top = 62
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPecasNFChange
          end
          object EdNF2: TdmkEdit
            Left = 368
            Top = 62
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdConheci2: TdmkEdit
            Left = 420
            Top = 62
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object TPEmissao2: TDateTimePicker
            Left = 576
            Top = 20
            Width = 89
            Height = 21
            Date = 39576.000000000000000000
            Time = 0.340970231489336600
            TabOrder = 2
          end
          object TPVencto2: TDateTimePicker
            Left = 668
            Top = 20
            Width = 89
            Height = 21
            Date = 39576.000000000000000000
            Time = 0.340970231489336600
            TabOrder = 3
          end
          object TPEmisFrete2: TDateTimePicker
            Left = 472
            Top = 62
            Width = 89
            Height = 21
            Date = 39576.000000000000000000
            Time = 0.340970231489336600
            TabOrder = 11
          end
          object TPVctoFrete2: TDateTimePicker
            Left = 564
            Top = 62
            Width = 89
            Height = 21
            Date = 39576.000000000000000000
            Time = 0.340970231489336600
            TabOrder = 12
          end
          object BitBtn2: TBitBtn
            Left = 552
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            TabOrder = 13
            TabStop = False
            OnClick = BitBtn2Click
          end
          object EdPecas_Fat2: TdmkEdit
            Left = 4
            Top = 62
            Width = 61
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 1
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPecas_Fat2Change
            OnExit = EdPecas_Fat2Exit
          end
        end
        object Panel14: TPanel
          Left = 0
          Top = 204
          Width = 994
          Height = 48
          Align = alBottom
          Alignment = taLeftJustify
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          object PnControla: TPanel
            Left = 0
            Top = 0
            Width = 528
            Height = 48
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object BtRateia: TBitBtn
              Tag = 14
              Left = 20
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Rateia'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 0
              Visible = False
            end
            object BtInsere: TBitBtn
              Tag = 10
              Left = 116
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Insere'
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtInsereClick
            end
            object Panel15: TPanel
              Left = 417
              Top = 0
              Width = 111
              Height = 48
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
            end
            object BitBtn5: TBitBtn
              Tag = 11
              Left = 208
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Altera'
              NumGlyphs = 2
              TabOrder = 3
              OnClick = BitBtn5Click
            end
            object BitBtn6: TBitBtn
              Tag = 12
              Left = 300
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Exclui'
              NumGlyphs = 2
              TabOrder = 4
              OnClick = BitBtn6Click
            end
          end
          object PnConfirma: TPanel
            Left = 528
            Top = 0
            Width = 466
            Height = 48
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            Visible = False
            object Panel16: TPanel
              Left = 355
              Top = 0
              Width = 111
              Height = 48
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BitBtn4: TBitBtn
                Tag = 15
                Left = 2
                Top = 3
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Sai da janela atual'
                Caption = '&Desiste'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BitBtn4Click
              end
            end
            object BitBtn7: TBitBtn
              Tag = 14
              Left = 20
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Confirma'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BitBtn7Click
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1002
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 954
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 906
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 273
        Height = 32
        Caption = 'Itens de Mat'#233'ria-prima'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 273
        Height = 32
        Caption = 'Itens de Mat'#233'ria-prima'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 273
        Height = 32
        Caption = 'Itens de Mat'#233'ria-prima'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 588
    Width = 1002
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel20: TPanel
      Left = 2
      Top = 15
      Width = 998
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 632
    Width = 1002
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel21: TPanel
      Left = 2
      Top = 15
      Width = 998
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 854
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrEntiMP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM entimp'
      'WHERE Codigo=:P0')
    Left = 576
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiMPCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entimp.Codigo'
    end
    object QrEntiMPLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'entimp.Lk'
    end
    object QrEntiMPDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'entimp.DataCad'
    end
    object QrEntiMPDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'entimp.DataAlt'
    end
    object QrEntiMPUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'entimp.UserCad'
    end
    object QrEntiMPUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'entimp.UserAlt'
    end
    object QrEntiMPAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'entimp.AlterWeb'
    end
    object QrEntiMPAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'entimp.Ativo'
    end
    object QrEntiMPCMPUnida: TSmallintField
      FieldName = 'CMPUnida'
      Origin = 'entimp.CMPUnida'
    end
    object QrEntiMPCMPMoeda: TSmallintField
      FieldName = 'CMPMoeda'
      Origin = 'entimp.CMPMoeda'
    end
    object QrEntiMPCMPPerio: TSmallintField
      FieldName = 'CMPPerio'
      Origin = 'entimp.CMPPerio'
    end
    object QrEntiMPCMPPreco: TFloatField
      FieldName = 'CMPPreco'
      Origin = 'entimp.CMPPreco'
    end
    object QrEntiMPCMPFrete: TFloatField
      FieldName = 'CMPFrete'
      Origin = 'entimp.CMPFrete'
    end
    object QrEntiMPCPLUnida: TSmallintField
      FieldName = 'CPLUnida'
      Origin = 'entimp.CPLUnida'
    end
    object QrEntiMPCPLMoeda: TSmallintField
      FieldName = 'CPLMoeda'
      Origin = 'entimp.CPLMoeda'
    end
    object QrEntiMPCPLPerio: TSmallintField
      FieldName = 'CPLPerio'
      Origin = 'entimp.CPLPerio'
    end
    object QrEntiMPCPLPreco: TFloatField
      FieldName = 'CPLPreco'
      Origin = 'entimp.CPLPreco'
    end
    object QrEntiMPCPLFrete: TFloatField
      FieldName = 'CPLFrete'
      Origin = 'entimp.CPLFrete'
    end
    object QrEntiMPAGIUnida: TSmallintField
      FieldName = 'AGIUnida'
      Origin = 'entimp.AGIUnida'
    end
    object QrEntiMPAGIMoeda: TSmallintField
      FieldName = 'AGIMoeda'
      Origin = 'entimp.AGIMoeda'
    end
    object QrEntiMPAGIPerio: TSmallintField
      FieldName = 'AGIPerio'
      Origin = 'entimp.AGIPerio'
    end
    object QrEntiMPAGIPreco: TFloatField
      FieldName = 'AGIPreco'
      Origin = 'entimp.AGIPreco'
    end
    object QrEntiMPAGIFrete: TFloatField
      FieldName = 'AGIFrete'
      Origin = 'entimp.AGIFrete'
    end
    object QrEntiMPCMIUnida: TSmallintField
      FieldName = 'CMIUnida'
      Origin = 'entimp.CMIUnida'
    end
    object QrEntiMPCMIMoeda: TSmallintField
      FieldName = 'CMIMoeda'
      Origin = 'entimp.CMIMoeda'
    end
    object QrEntiMPCMIPerio: TSmallintField
      FieldName = 'CMIPerio'
      Origin = 'entimp.CMIPerio'
    end
    object QrEntiMPCMIPreco: TFloatField
      FieldName = 'CMIPreco'
      Origin = 'entimp.CMIPreco'
    end
    object QrEntiMPCMIFrete: TFloatField
      FieldName = 'CMIFrete'
      Origin = 'entimp.CMIFrete'
    end
    object QrEntiMPPreDescarn: TSmallintField
      FieldName = 'PreDescarn'
      Origin = 'entimp.PreDescarn'
    end
    object QrEntiMPMaskLetras: TWideStringField
      FieldName = 'MaskLetras'
      Origin = 'entimp.MaskLetras'
      Size = 10
    end
    object QrEntiMPMaskFormat: TWideStringField
      FieldName = 'MaskFormat'
      Origin = 'entimp.MaskFormat'
      Size = 10
    end
    object QrEntiMPCPLSit: TSmallintField
      FieldName = 'CPLSit'
      Origin = 'entimp.CPLSit'
    end
    object QrEntiMPCMP_LPQV: TFloatField
      FieldName = 'CMP_LPQV'
    end
    object QrEntiMPCPL_LPQV: TFloatField
      FieldName = 'CPL_LPQV'
    end
    object QrEntiMPAGI_LPQV: TFloatField
      FieldName = 'AGI_LPQV'
    end
    object QrEntiMPCMI_LPQV: TFloatField
      FieldName = 'CMI_LPQV'
    end
    object QrEntiMPCMP_IDQV: TSmallintField
      FieldName = 'CMP_IDQV'
    end
    object QrEntiMPCPL_IDQV: TSmallintField
      FieldName = 'CPL_IDQV'
    end
    object QrEntiMPAGI_IDQV: TSmallintField
      FieldName = 'AGI_IDQV'
    end
    object QrEntiMPCMI_IDQV: TSmallintField
      FieldName = 'CMI_IDQV'
    end
  end
  object QrCambios: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCambiosAfterOpen
    SQL.Strings = (
      'SELECT Data, Dolar, Euro, Indexador '
      'FROM cambios'
      'WHERE Data <= :P0'
      'ORDER BY Data DESC'
      'LIMIT 0, 1')
    Left = 9
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCambiosData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCambiosDolar: TFloatField
      FieldName = 'Dolar'
    end
    object QrCambiosEuro: TFloatField
      FieldName = 'Euro'
    end
    object QrCambiosIndexador: TFloatField
      FieldName = 'Indexador'
    end
  end
  object DsCambios: TDataSource
    DataSet = QrCambios
    Left = 37
    Top = 8
  end
  object QrFornece: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emp.Corretor, emp.Comissao, emp.DescAdiant,'
      'emp.CondPagto, emp.CondComis, emp.LocalEntrg,'
      'emp.PreDescarn, emp.MaskLetras, emp.MaskFormat, '
      'emp.Abate, emp.Transporte, ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'LEFT JOIN entimp emp ON emp.Codigo=ent.Codigo'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 88
    Top = 10
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrFornecePreDescarn: TSmallintField
      FieldName = 'PreDescarn'
    end
    object QrForneceMaskLetras: TWideStringField
      FieldName = 'MaskLetras'
      Size = 10
    end
    object QrForneceMaskFormat: TWideStringField
      FieldName = 'MaskFormat'
      Size = 10
    end
    object QrForneceCorretor: TIntegerField
      FieldName = 'Corretor'
    end
    object QrForneceComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrForneceDescAdiant: TFloatField
      FieldName = 'DescAdiant'
    end
    object QrForneceCondPagto: TWideStringField
      FieldName = 'CondPagto'
      Size = 30
    end
    object QrForneceCondComis: TWideStringField
      FieldName = 'CondComis'
      Size = 30
    end
    object QrForneceLocalEntrg: TWideStringField
      FieldName = 'LocalEntrg'
      Size = 50
    end
    object QrForneceAbate: TIntegerField
      FieldName = 'Abate'
    end
    object QrForneceTransporte: TIntegerField
      FieldName = 'Transporte'
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 116
    Top = 10
  end
  object PMFornece: TPopupMenu
    Left = 201
    Top = 12
    object Entidades1: TMenuItem
      Caption = '&Entidades'
      OnClick = Entidades1Click
    end
    object FornecedoresdeMP1: TMenuItem
      Caption = '&Fornecedores de MP'
      OnClick = FornecedoresdeMP1Click
    end
  end
  object DsProcede: TDataSource
    DataSet = QrProcede
    Left = 176
    Top = 10
  end
  object QrProcede: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 148
    Top = 10
    object QrProcedeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProcedeNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Required = True
      Size = 100
    end
  end
  object QrMPInRat: TMySQLQuery
    SQL.Strings = (
      'SELECT * FROM mpinrat'
      'ORDER BY Controle')
    Left = 608
    Top = 8
    object QrMPInRatProcedeCod: TIntegerField
      DisplayWidth = 12
      FieldName = 'ProcedeCod'
      Origin = 'mpinrat.ProcedeCod'
    end
    object QrMPInRatProcedeNom: TWideStringField
      DisplayWidth = 31
      FieldName = 'ProcedeNom'
      Origin = 'mpinrat.ProcedeNom'
      Size = 100
    end
    object QrMPInRatPecas: TFloatField
      DisplayWidth = 12
      FieldName = 'Pecas'
      Origin = 'mpinrat.Pecas'
    end
    object QrMPInRatPecasNF: TFloatField
      DisplayWidth = 12
      FieldName = 'PecasNF'
      Origin = 'mpinrat.PecasNF'
    end
    object QrMPInRatPNF: TFloatField
      DisplayWidth = 12
      FieldName = 'PNF'
      Origin = 'mpinrat.PNF'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInRatPLE: TFloatField
      DisplayWidth = 12
      FieldName = 'PLE'
      Origin = 'mpinrat.PLE'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInRatPDA: TFloatField
      DisplayWidth = 12
      FieldName = 'PDA'
      Origin = 'mpinrat.PDA'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInRatPreco: TFloatField
      DisplayWidth = 12
      FieldName = 'Preco'
      Origin = 'mpinrat.Preco'
      DisplayFormat = '###,###,##0.0000;-###,###,##0.0000; '
    end
    object QrMPInRatCusto: TFloatField
      DisplayWidth = 12
      FieldName = 'Custo'
      Origin = 'mpinrat.Custo'
      DisplayFormat = '###,###,##0.00;-###,###,##0.00; '
    end
    object QrMPInRatControle: TAutoIncField
      FieldName = 'Controle'
      Origin = 'mpinrat.Controle'
    end
    object QrMPInRatPNF_Fat: TFloatField
      FieldName = 'PNF_Fat'
      Origin = 'mpinrat.PNF_Fat'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInRatFrete: TFloatField
      FieldName = 'Frete'
      DisplayFormat = '###,###,##0.00;-###,###,##0.00; '
    end
    object QrMPInRatNF: TIntegerField
      FieldName = 'NF'
      DisplayFormat = '000000'
    end
    object QrMPInRatConheci: TIntegerField
      FieldName = 'Conheci'
      DisplayFormat = '000000'
    end
  end
  object DsMPInRat: TDataSource
    DataSet = QrMPInRat
    Left = 636
    Top = 8
  end
  object QrB: TMySQLQuery
    SQL.Strings = (
      'SELECT * FROM mpinrat'
      'WHERE ProcedeCod=:P0'
      'ORDER BY Controle')
    Left = 692
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBProcedeCod: TIntegerField
      FieldName = 'ProcedeCod'
      Origin = 'mpinrat.ProcedeCod'
    end
    object QrBProcedeNom: TWideStringField
      FieldName = 'ProcedeNom'
      Origin = 'mpinrat.ProcedeNom'
      Size = 100
    end
    object QrBPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'mpinrat.Pecas'
    end
    object QrBPecasNF: TFloatField
      FieldName = 'PecasNF'
      Origin = 'mpinrat.PecasNF'
    end
    object QrBPNF: TFloatField
      FieldName = 'PNF'
      Origin = 'mpinrat.PNF'
    end
    object QrBPLE: TFloatField
      FieldName = 'PLE'
      Origin = 'mpinrat.PLE'
    end
    object QrBPDA: TFloatField
      FieldName = 'PDA'
      Origin = 'mpinrat.PDA'
    end
    object QrBPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'mpinrat.Preco'
    end
    object QrBCusto: TFloatField
      FieldName = 'Custo'
      Origin = 'mpinrat.Custo'
    end
    object QrBControle: TAutoIncField
      FieldName = 'Controle'
      Origin = 'mpinrat.Controle'
    end
  end
  object QrA: TMySQLQuery
    SQL.Strings = (
      'SELECT * FROM mpinrat'
      'WHERE ProcedeCod<>:P0'
      'ORDER BY Controle')
    Left = 664
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAProcedeCod: TIntegerField
      FieldName = 'ProcedeCod'
      Origin = 'mpinrat.ProcedeCod'
    end
    object QrAProcedeNom: TWideStringField
      FieldName = 'ProcedeNom'
      Origin = 'mpinrat.ProcedeNom'
      Size = 100
    end
    object QrAPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'mpinrat.Pecas'
    end
    object QrAPecasNF: TFloatField
      FieldName = 'PecasNF'
      Origin = 'mpinrat.PecasNF'
    end
    object QrAPNF: TFloatField
      FieldName = 'PNF'
      Origin = 'mpinrat.PNF'
    end
    object QrAPLE: TFloatField
      FieldName = 'PLE'
      Origin = 'mpinrat.PLE'
    end
    object QrAPDA: TFloatField
      FieldName = 'PDA'
      Origin = 'mpinrat.PDA'
    end
    object QrAPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'mpinrat.Preco'
    end
    object QrACusto: TFloatField
      FieldName = 'Custo'
      Origin = 'mpinrat.Custo'
    end
    object QrAControle: TAutoIncField
      FieldName = 'Controle'
      Origin = 'mpinrat.Controle'
    end
  end
  object QrSumRat: TMySQLQuery
    SQL.Strings = (
      'SELECT SUM(Custo) Custo, SUM(Frete) Frete'
      'FROM mpinrat'
      '')
    Left = 721
    Top = 8
    object QrSumRatCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrSumRatFrete: TFloatField
      FieldName = 'Frete'
    end
  end
  object QrEmit: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 244
    Top = 10
    object QrEmitCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Required = True
      Size = 100
    end
  end
  object DsEmit: TDataSource
    DataSet = QrEmit
    Left = 272
    Top = 10
  end
end
