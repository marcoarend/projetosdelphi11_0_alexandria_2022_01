unit FormulasCorrigeGrupo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGrid,
  Vcl.Menus, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkEditDateTimePicker,
  Variants;

type
  TFmFormulasCorrigeGrupo = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAcao: TBitBtn;
    LaTitulo1C: TLabel;
    QrFormulas: TMySQLQuery;
    DsFormulas: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    PMAcao: TPopupMenu;
    Corrigirgrupo1: TMenuItem;
    PnDados: TPanel;
    LaGrupo: TLabel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PB1: TProgressBar;
    LaAviso: TLabel;
    Panel5: TPanel;
    Label1: TLabel;
    QrFormulasNumero: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrFormulasAtivo: TSmallintField;
    QrOri: TMySQLQuery;
    QrOriCodigo: TIntegerField;
    QrOriNome: TWideStringField;
    DsOri: TDataSource;
    EdOri: TdmkEditCB;
    CBOri: TdmkDBLookupComboBox;
    EdDst: TdmkEditCB;
    CBDst: TdmkDBLookupComboBox;
    QrDst: TMySQLQuery;
    QrDstCodigo: TIntegerField;
    QrDstNome: TWideStringField;
    DsDst: TDataSource;
    TpPesaIni: TdmkEditDateTimePicker;
    Label2: TLabel;
    QrListaSetores: TMySQLQuery;
    QrListaSetoresCodigo: TIntegerField;
    QrListaSetoresNome: TWideStringField;
    DsListaSetores: TDataSource;
    CBSetor: TDBLookupComboBox;
    Label22: TLabel;
    Query: TMySQLQuery;
    RGCampo: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure PMAcaoPopup(Sender: TObject);
    procedure Corrigirgrupo1Click(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure EdOriRedefinido(Sender: TObject);
    procedure TpPesaIniChange(Sender: TObject);
    procedure TpPesaIniClick(Sender: TObject);
    procedure CBSetorClick(Sender: TObject);
    procedure RGCampoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ConfiguraPnDados(Mostra: Boolean);
  public
    { Public declarations }
    function ReopenFormulas(): Boolean;
  end;

  var
  FmFormulasCorrigeGrupo: TFmFormulasCorrigeGrupo;

implementation

uses UnMyObjects, UMySQLModule, Module, DmkDAC_PF, AppListas, ModuleGeral;

{$R *.DFM}

procedure TFmFormulasCorrigeGrupo.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmFormulasCorrigeGrupo.BtConfirmaClick(Sender: TObject);

  function AtualizaReceita(Valor: Integer): Boolean;
  var
    Numero: Integer;
  begin
    Result := False;
    Numero := QrFormulasNumero.Value;
    //
    if Valor > 0 then
    begin
      case RGCampo.ItemIndex of
        0:
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'formulas', False,
            ['Grupo'], ['Numero'], [Valor], [Numero], True);
        1:
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'formulas', False,
            ['Setor'], ['Numero'], [Valor], [Numero], True);
      end;
      //
      Result := True;
    end;
  end;

var
  I, Valor: Integer;
begin
  Valor := EdDst.ValueVariant;
  //
  //if MyObjects.FIC(Valor <= 0, EdValorDest, 'Informe o Valor!') then Exit;
  //
  if dmkDBGrid1.SelectedRows.Count > 1 then
  begin
    with dmkDBGrid1.DataSource.DataSet do
    begin
      try
        PB1.Position := 0;
        PB1.Max      := dmkDBGrid1.SelectedRows.Count;
        //
        for I := 0 to dmkDBGrid1.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(dmkDBGrid1.SelectedRows.Items[I]));
          GotoBookmark(dmkDBGrid1.SelectedRows.Items[I]);
          //
          if AtualizaReceita(Valor) then
          begin
            PB1.Position := PB1.Position + 1;
            PB1.Update;
            Application.ProcessMessages;
          end else
          begin
            Geral.MB_Erro('Falha ao atualizar receita!');
            Exit;
          end;
        end;
      finally
        PB1.Position := 0;
      end;
    end;
  end else
  begin
    if not AtualizaReceita(Valor) then
      Geral.MB_Erro('Falha ao atualizar receita!');
  end;
  ReopenFormulas;
  ConfiguraPnDados(False);
end;

procedure TFmFormulasCorrigeGrupo.BtDesisteClick(Sender: TObject);
begin
  ConfiguraPnDados(False);
end;

procedure TFmFormulasCorrigeGrupo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulasCorrigeGrupo.CBSetorClick(Sender: TObject);
begin
  ReopenFormulas();
end;

procedure TFmFormulasCorrigeGrupo.ConfiguraPnDados(Mostra: Boolean);
begin
  PB1.Position      := 0;
  PnDados.Visible   := Mostra;
  //
  BtAcao.Enabled  := not Mostra;
  BtSaida.Enabled := not Mostra;
end;

procedure TFmFormulasCorrigeGrupo.Corrigirgrupo1Click(Sender: TObject);
begin
  ConfiguraPnDados(True);
end;

procedure TFmFormulasCorrigeGrupo.EdOriRedefinido(Sender: TObject);
begin
  LaAviso.Visible := EdOri.ValueVariant = 0;
  ReopenFormulas();
end;

procedure TFmFormulasCorrigeGrupo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormulasCorrigeGrupo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType       := stLok;
  dmkDBGrid1.DataSource := DsFormulas;
  UnDmkDAC_PF.AbreQuery(QrListaSetores, Dmod.MyDB);
  TpPesaIni.Date := Trunc(DModG.ObtemAgora());
  //
  ConfiguraPnDados(False);
  ReopenFormulas();
end;

procedure TFmFormulasCorrigeGrupo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulasCorrigeGrupo.PMAcaoPopup(Sender: TObject);
begin
  Corrigirgrupo1.Enabled := dmkDBGrid1.SelectedRows.Count > 0;
end;

function TFmFormulasCorrigeGrupo.ReopenFormulas(): Boolean;
var
  DtIni, SQL_Numeros, Numeros, SQL_Setor: String;
begin
  Result := False;
  //
  DtIni := Geral.FDT(TPPesaIni.Date, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  //'SELECT GROUP_CONCAT(DISTINCT(Numero)) Numeros ',
  'SELECT DISTINCT(em.Numero) Numero',
  'FROM emit em',
  'WHERE em.DtaBaixa>="' + DtIni + '"',
  '']);
  //Numeros := Query.Fields[0].AsString;
  Numeros := MyObjects.CordaDeQuery(query, 'Numero', '0');
  SQL_Numeros := EmptyStr;
  if Numeros <> EmptyStr then
    SQL_Numeros := 'AND Numero IN (' + Numeros + ')';
  //
  SQL_Setor := EmptyStr;
  if CBSetor.KeyValue <> Null then
    SQL_Setor := 'AND Setor=' + Geral.FF0(CBSetor.KeyValue);
    //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
    'SELECT Numero, Nome, Ativo ',
    'FROM formulas ',
    'WHERE Grupo=' + Geral.FF0(EdOri.ValueVariant),
    SQL_Numeros,
    SQL_Setor,
    '']);
  //Geral.MB_Teste(QrFormulas.SQL.Text);
  if QrFormulas.RecordCount > 0 then
    Result := True
  else
    QrFormulas.Close;
end;

procedure TFmFormulasCorrigeGrupo.RGCampoClick(Sender: TObject);
var
  Tabela: String;
begin
  QrOri.Close;
  QrDst.Close;
  case RGCampo.ItemIndex of
    0: Tabela := 'formulasgru';
    1: Tabela := 'listasetores';
    else Tabela := EmptyStr;
  end;
  if Tabela <> EmptyStr then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + Tabela,
    'ORDER BY Nome ',
    '']);
    UnDmkDAC_PF.AbreMySQLQuery0(QrDst, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + Tabela,
    'ORDER BY Nome ',
    '']);
  end;
  ReopenFormulas();
end;

procedure TFmFormulasCorrigeGrupo.TpPesaIniChange(Sender: TObject);
begin
  ReopenFormulas();
end;

procedure TFmFormulasCorrigeGrupo.TpPesaIniClick(Sender: TObject);
begin
  ReopenFormulas();
end;

end.
