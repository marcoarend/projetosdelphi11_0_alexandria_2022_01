unit Espessuras;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, DBGrids, dmkDBGridDAC, Db,
  mySQLDbTables, dmkDBGrid, Menus, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmEspessuras = class(TForm)
    Panel1: TPanel;
    DsEspessuras: TDataSource;
    QrEspessuras: TmySQLQuery;
    dmkDBGridDAC1: TdmkDBGridDAC;
    PMAcao: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrEspessurasCodigo: TIntegerField;
    QrEspessurasLinhas: TWideStringField;
    QrEspessurasEMCM: TFloatField;
    QrEspessurasLk: TIntegerField;
    QrEspessurasDataCad: TDateField;
    QrEspessurasDataAlt: TDateField;
    QrEspessurasUserCad: TIntegerField;
    QrEspessurasUserAlt: TIntegerField;
    QrEspessurasAlterWeb: TSmallintField;
    QrEspessurasAtivo: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtAcao: TBitBtn;
    BtSaida: TBitBtn;
    Altera1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
    procedure dmkDBGridDAC1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Altera1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrEspessuras(Codigo: Double);
  public
    { Public declarations }
  end;

  var
  FmEspessuras: TFmEspessuras;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts;

{$R *.DFM}

procedure TFmEspessuras.BtSaidaClick(Sender: TObject);
begin
  Close;
  VAR_ESPESSURA := QrEspessurasCodigo.Value;
end;

procedure TFmEspessuras.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEspessuras.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEspessuras.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenQrEspessuras(0);
end;

procedure TFmEspessuras.Inclui1Click(Sender: TObject);
const
  SQLType = stIns;
  //Linhas  = '99/99';
var
  Codigo: Integer;
  Linhas: String;
begin
  if InputQuery('Espessura', 'Informe a espessura', Linhas) then
  begin
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Espessuras', 'Espessuras', 'Codigo');
  (*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO espessuras SET Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
  *)
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'espessuras', False, [
    'Linhas'(*, 'EMCM'*)], [
    'Codigo'], [
    Linhas(*, EMCM*)], [
    Codigo], True) then
      ReopenQrEspessuras(Codigo);
  end;
end;

procedure TFmEspessuras.ReopenQrEspessuras(Codigo: Double);
begin
  QrEspessuras.Close;
  UnDmkDAC_PF.AbreQuery(QrEspessuras, Dmod.MyDB);
  QrEspessuras.Locate('Codigo', Codigo, []);
end;

procedure TFmEspessuras.Altera1Click(Sender: TObject);
const
  SQLType = stUpd;
  //Linhas  = '99/99';
var
  Codigo: Integer;
  Linhas: String;
begin
  Linhas := QrEspessurasLinhas.Value;
  if InputQuery('Espessura', 'Informe a espessura', Linhas) then
  begin
    Codigo := QrEspessurasCodigo.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'espessuras', False, [
    'Linhas'(*, 'EMCM'*)], [
    'Codigo'], [
    Linhas(*, EMCM*)], [
    Codigo], True) then
      ReopenQrEspessuras(Codigo);
  end;
end;

procedure TFmEspessuras.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmEspessuras.Exclui1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o do registro selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM espessuras WHERE Codigo=:P0');
    Dmod.QrUpd.Params[00].AsInteger := QrEspessurasCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrEspessuras(Int(Date));
  end;
end;

procedure TFmEspessuras.dmkDBGridDAC1CellClick(Column: TColumn);
var
  Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if QrEspessurasAtivo.Value = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE espessuras SET Ativo=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
    //
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsFloat   := QrEspessurasCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrEspessuras(QrEspessurasCodigo.Value);
  end;
end;

procedure TFmEspessuras.dmkDBGridDAC1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  i : Integer;
  h : Boolean;
  Str, Cul, Cab : String;
  Media, PCula : Double;
begin
  if Key = VK_F4 then
  begin
    Cul := '';
    Cab := '';
    h := False;
    str := QrEspessurasLinhas.Value;
    for i := 1 to Length(str) do
    begin
      if str[i] = '-' then h := True;
      if str[i] = '/' then h := True;
      if str[i] = '.' then h := True;
      if str[i] in ['0'..'9'] then
      begin
        if h = False then
          Cul := Cul + Str[i]
        else
          Cab := Cab + Str[i];
      end;
    end;
    Pcula := Dmod.QrControlePerCula.Value;
    Media := ((Geral.DMV(Cul) * PCula) +
             (Geral.DMV(Cab) * (100 - PCula))) / 100;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE espessuras SET EMCM=:P0 ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[00].AsFloat := Media;
    Dmod.QrUpd.Params[01].AsFloat := QrEspessurasCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrEspessuras(QrEspessurasCodigo.Value);
  end;
end;

end.

