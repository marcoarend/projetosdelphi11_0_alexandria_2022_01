unit UnAppEnums;

interface

uses  System.SysUtils, System.Types {$IfDef MSWINDOWS}, Winapi.Windows {$EndIf};

//const

type
  TPQUAplicacao = (pquaplicNenhum=0, pquaplicRibeira=1, pquaplicRecurtimento=2);
  TRelatorioVSImpEstoque = (rieNenhum=0, rieCusto=1, rieBaseVenda=2, rieBaseCusto=3);
  TMapaOperPosProcStat = (moppsIndef=0, moppsEmProcessoENaoApto=1, moppsEMProcessoEApto=2,
    moppsProcessadoEApto=3, moppsEmOperacao=4, moppsAprontado=5);

implementation

end.
