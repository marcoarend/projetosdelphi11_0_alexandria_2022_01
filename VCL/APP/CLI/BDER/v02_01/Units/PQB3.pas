unit PQB3;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, Grids, DBGrids, Menus, mySQLDbTables,
  ComCtrls, Variants, dmkGeral, UnDmkProcFunc, dmkLabel, dmkImage, UnDmkEnums,
  dmkDBGridZTO, frxClass, frxDBSet, dmkDBGrid, UnProjGroup_Vars, dmkEdit,
  UnEmpresas, dmkDBGridDAC;

type
  TFmPQB3 = class(TForm)
    PainelDados: TPanel;
    DsBalancos: TDataSource;
    DsBalancosIts: TDataSource;
    DsArtigo: TDataSource;
    PainelDados1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    PainelDados2: TPanel;
    Progress1: TProgressBar;
    Panel1: TPanel;
    Panel2: TPanel;
    PnRegistros: TPanel;
    Panel4: TPanel;
    PnTempo: TPanel;
    DBCheckBox1: TDBCheckBox;
    QrBalancos: TmySQLQuery;
    QrBalancosIts: TmySQLQuery;
    QrArtigo: TmySQLQuery;
    QrAtualiza: TmySQLQuery;
    QrMax: TmySQLQuery;
    QrBalancosPeriodo: TIntegerField;
    QrBalancosEstqQ: TFloatField;
    QrBalancosEstqV: TFloatField;
    QrBalancosDataCad: TDateField;
    QrBalancosDataAlt: TDateField;
    QrBalancosUserCad: TIntegerField;
    QrBalancosUserAlt: TIntegerField;
    QrBalancosConfirmado: TWideStringField;
    QrBalancosLk: TIntegerField;
    QrSoma: TmySQLQuery;
    QrBalancosPeriodo2: TWideStringField;
    QrArtigoCodigo: TIntegerField;
    QrArtigoNome: TWideStringField;
    QrSomaEstqQ: TFloatField;
    QrSomaEstqV: TFloatField;
    QrMaxPeriodo: TIntegerField;
    QrProdutos: TmySQLQuery;
    QrBalancosItsDataX: TDateField;
    QrBalancosItsTipo: TSmallintField;
    QrBalancosItsCliOrig: TIntegerField;
    QrBalancosItsCliDest: TIntegerField;
    QrBalancosItsInsumo: TIntegerField;
    QrBalancosItsPeso: TFloatField;
    QrBalancosItsValor: TFloatField;
    QrBalancosItsOrigemCodi: TIntegerField;
    QrBalancosItsOrigemCtrl: TIntegerField;
    QrBalancosItsNOMEPQ: TWideStringField;
    QrBalancosItsGrupoQuimico: TIntegerField;
    QrBalancosItsNOMEGRUPO: TWideStringField;
    QrAtualizaPQ: TIntegerField;
    QrAtualizaCI: TIntegerField;
    QrProdutosPQ: TIntegerField;
    QrProdutosCI: TIntegerField;
    QrProdutosPeso: TFloatField;
    QrProdutosValor: TFloatField;
    QrPQCli: TmySQLQuery;
    QrPQCliInsumo: TIntegerField;
    QrPQCliCliDest: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel7: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAlteraCab: TBitBtn;
    BtExclui: TBitBtn;
    GBConfirma: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtTrava: TBitBtn;
    PMImprime: TPopupMenu;
    EsteBalano1: TMenuItem;
    Outrasimpressoes1: TMenuItem;
    DBGCI: TdmkDBGridZTO;
    QrCliDest: TmySQLQuery;
    DsCliDest: TDataSource;
    QrCliDestCodigo: TIntegerField;
    QrCliDestNO_ENT: TWideStringField;
    frxDsBalancosIts: TfrxDBDataset;
    frxQUI_BALAN_001_01: TfrxReport;
    QrBalancosItsCUSTOKG: TFloatField;
    QrMoviPQ: TmySQLQuery;
    QrMoviPQInsumo: TIntegerField;
    QrMoviPQNomePQ: TWideStringField;
    QrMoviPQNomeFO: TWideStringField;
    QrMoviPQNomeCI: TWideStringField;
    QrMoviPQNomeSE: TWideStringField;
    QrMoviPQInnPes: TFloatField;
    QrMoviPQInnVal: TFloatField;
    QrMoviPQOutPes: TFloatField;
    QrMoviPQOutVal: TFloatField;
    QrMoviPQAntPes: TFloatField;
    QrMoviPQAntVal: TFloatField;
    QrMoviPQFimPes: TFloatField;
    QrMoviPQFimVal: TFloatField;
    QrMoviPQBalPes: TFloatField;
    QrMoviPQBalVal: TFloatField;
    BtAjusta: TBitBtn;
    PB2: TProgressBar;
    QrDif2: TmySQLQuery;
    QrDif2Insumo: TIntegerField;
    QrDif2Peso: TFloatField;
    QrDif2Preco: TFloatField;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    EdPesquisa: TdmkEdit;
    QrAtualizaEmpresa: TIntegerField;
    QrBalancosItsEmpresa: TIntegerField;
    QrBalancosEmpresa: TIntegerField;
    QrBalancosNO_Emp: TWideStringField;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrBalancosFilial: TIntegerField;
    QrCliInts: TMySQLQuery;
    QrPQB3Resgata: TMySQLQuery;
    QrPQB3ResgataInsumo: TIntegerField;
    QrPQB3ResgataPeso: TFloatField;
    QrPQB3ResgataValor: TFloatField;
    QrPQB3ResgataPreco: TFloatField;
    QrPQB3ResgataNO_PQ: TWideStringField;
    QrPQB3ResgataAtivo: TSmallintField;
    QrCliIntsCodigo: TIntegerField;
    QrCliIntsNome: TWideStringField;
    PCItens: TPageControl;
    TabSheet1: TTabSheet;
    Panel9: TPanel;
    DBGBalancosIts: TDBGrid;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel10: TPanel;
    PnPQBAdx: TPanel;
    BtPosInclui: TBitBtn;
    BtPosAltera: TBitBtn;
    BtPosExclui: TBitBtn;
    Panel12: TPanel;
    PnPQBBxa: TPanel;
    BtNegInclui: TBitBtn;
    BtNegAltera: TBitBtn;
    BtNegExclui: TBitBtn;
    QrPQBBxa: TMySQLQuery;
    QrPQBBxaCodigo: TIntegerField;
    QrPQBBxaControle: TIntegerField;
    QrPQBBxaEmpresa: TIntegerField;
    QrPQBBxaCliInt: TIntegerField;
    QrPQBBxaInsumo: TIntegerField;
    QrPQBBxaPeso: TFloatField;
    QrPQBBxaValor: TFloatField;
    QrPQBBxaDtCorrApo: TDateTimeField;
    QrPQBBxaAtivo: TSmallintField;
    QrPQBBxaNOMEPQ: TWideStringField;
    QrPQBBxaCUSTOKG: TFloatField;
    DsPQBBxa: TDataSource;
    QrPQBAdx: TMySQLQuery;
    QrPQBAdxCodigo: TIntegerField;
    QrPQBAdxControle: TIntegerField;
    QrPQBAdxEmpresa: TIntegerField;
    QrPQBAdxCliInt: TIntegerField;
    QrPQBAdxInsumo: TIntegerField;
    QrPQBAdxPeso: TFloatField;
    QrPQBAdxValor: TFloatField;
    QrPQBAdxide_serie: TSmallintField;
    QrPQBAdxide_nNF: TIntegerField;
    QrPQBAdxDtCorrApo: TDateTimeField;
    QrPQBAdxxLote: TWideStringField;
    QrPQBAdxdFab: TDateField;
    QrPQBAdxdVal: TDateField;
    QrPQBAdxAtivo: TSmallintField;
    QrPQBAdxNOMEPQ: TWideStringField;
    QrPQBAdxCUSTOKG: TFloatField;
    DsPQBAdx: TDataSource;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    QrPQBAdxHowLoad: TSmallintField;
    DBGPQBBxa: TDBGrid;
    DBGPQBAdx: TDBGrid;
    PMAlteraCab: TPopupMenu;
    TodoBalanco1: TMenuItem;
    DadosDoAjusteAPositivo1: TMenuItem;
    TabSheet4: TTabSheet;
    BtCiclo: TBitBtn;
    PMCiclo: TPopupMenu;
    Incluinovociclo1: TMenuItem;
    DBGCiCab: TDBGrid;
    QrPQBCiCab: TMySQLQuery;
    DsPQBCiCab: TDataSource;
    QrPQBCiCabCodigo: TIntegerField;
    QrPQBCiCabControle: TIntegerField;
    QrPQBCiCabDataAnt: TDateField;
    QrPQBCiCabDataAtu: TDateField;
    QrPQBCiIts: TMySQLQuery;
    DsPQBCiIts: TDataSource;
    QrPQBCiItsNO_PQ: TWideStringField;
    QrPQBCiItsCodigo: TIntegerField;
    QrPQBCiItsControle: TIntegerField;
    QrPQBCiItsConta: TIntegerField;
    QrPQBCiItsEmpresa: TIntegerField;
    QrPQBCiItsCliInt: TIntegerField;
    QrPQBCiItsInsumo: TIntegerField;
    QrPQBCiItsTipo: TIntegerField;
    QrPQBCiItsDstCodi: TIntegerField;
    QrPQBCiItsDstCtrl: TIntegerField;
    QrPQBCiItsPesoInic: TFloatField;
    QrPQBCiItsValorInic: TFloatField;
    QrPQBCiItsPesoAtuA: TFloatField;
    QrPQBCiItsValorAtuA: TFloatField;
    QrPQBCiItsPesoAtuB: TFloatField;
    QrPQBCiItsValorAtuB: TFloatField;
    QrPQBCiItsPesoAtuC: TFloatField;
    QrPQBCiItsValorAtuC: TFloatField;
    QrPQBCiItsPesoPeIn: TFloatField;
    QrPQBCiItsValorPeIn: TFloatField;
    QrPQBCiItsPesoPeBx: TFloatField;
    QrPQBCiItsValorPeBx: TFloatField;
    QrPQBCiItsPesoInfo: TFloatField;
    QrPQBCiItsValorInfo: TFloatField;
    QrPQBCiItsPesoDife: TFloatField;
    QrPQBCiItsValorDife: TFloatField;
    QrPQBCiItsSerie: TIntegerField;
    QrPQBCiItsNF: TIntegerField;
    QrPQBCiItsxLote: TWideStringField;
    QrPQBCiItsdFab: TDateField;
    QrPQBCiItsdVal: TDateField;
    QrPQBCiItsLancar: TSmallintField;
    QrPQBCiItsAtivo: TSmallintField;
    DBGCiIts: TdmkDBGridDAC;
    QrPQBCiItsPesoAjIn: TFloatField;
    QrPQBCiItsValorAjIn: TFloatField;
    QrPQBCiItsPesoAjBx: TFloatField;
    QrPQBCiItsValorAjBx: TFloatField;
    QrPQBCiItsNO_Lancar: TWideStringField;
    QrSdo: TMySQLQuery;
    QrSdoDifPeso: TFloatField;
    QrItImped: TMySQLQuery;
    DsItImped: TDataSource;
    QrItImpedDescricao: TWideStringField;
    QrItImpedItens: TFloatField;
    QrPQBCiItsdFab_TXT: TWideStringField;
    QrPQBCiItsdVal_TXT: TWideStringField;
    BalanoIntermedirio1: TMenuItem;
    Alteraitemselecionado1: TMenuItem;
    Excluiciclo1: TMenuItem;
    QrCiItsDif: TMySQLQuery;
    DsCiItsDif: TDataSource;
    QrCiItsDifEmpresa: TIntegerField;
    QrCiItsDifCliInt: TIntegerField;
    QrCiItsDifInsumo: TIntegerField;
    QrCiItsDifPesoDIfe: TFloatField;
    QrPQBBxaDataCad: TDateField;
    QrPQBAdxDataCad: TDateField;
    QrPQBCiItsPesoAtuT: TFloatField;
    QrPQBCiItsValorAtuT: TFloatField;
    N1: TMenuItem;
    Corrigeitensdeajustecfepqx1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraCabClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtTravaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrBalancosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure QrBalancosCalcFields(DataSet: TDataSet);
    procedure DBGBalancosItsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    //procedure BtIncluiItsClick(Sender: TObject);
    //procedure BtAlteraItsClick(Sender: TObject);
    procedure QrBalancosItsAfterOpen(DataSet: TDataSet);
    //procedure BtExcluiItsClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    //procedure BtRefreshClick(Sender: TObject);
    procedure QrBalancosItsCalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrBalancosAfterScroll(DataSet: TDataSet);
    //procedure BtPrivativoClick(Sender: TObject);
    procedure QrBalancosBeforeClose(DataSet: TDataSet);
    //procedure BtInicialClick(Sender: TObject);
    procedure QrBalancosItsBeforeClose(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure Outrasimpressoes1Click(Sender: TObject);
    procedure EsteBalano1Click(Sender: TObject);
    procedure QrCliDestBeforeClose(DataSet: TDataSet);
    procedure QrCliDestAfterScroll(DataSet: TDataSet);
    procedure frxQUI_BALAN_001_01GetValue(const VarName: string;
      var Value: Variant);
    procedure BtGeraDifClick(Sender: TObject);
    procedure BtAjustaClick(Sender: TObject);
    procedure EdPesquisaChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtPosIncluiClick(Sender: TObject);
    procedure BtPosAlteraClick(Sender: TObject);
    procedure BtNegIncluiClick(Sender: TObject);
    procedure BtNegAlteraClick(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
    procedure BtNegExcluiClick(Sender: TObject);
    procedure QrPQBAdxBeforeOpen(DataSet: TDataSet);
    procedure QrPQBAdxAfterOpen(DataSet: TDataSet);
    procedure QrPQBAdxBeforeClose(DataSet: TDataSet);
    procedure QrPQBBxaAfterOpen(DataSet: TDataSet);
    procedure QrPQBBxaBeforeClose(DataSet: TDataSet);
    procedure PMAlteraCabPopup(Sender: TObject);
    procedure TodoBalanco1Click(Sender: TObject);
    procedure DadosDoAjusteAPositivo1Click(Sender: TObject);
    procedure Incluinovociclo1Click(Sender: TObject);
    procedure BtCicloClick(Sender: TObject);
    procedure QrPQBCiCabAfterScroll(DataSet: TDataSet);
    procedure QrPQBCiCabBeforeClose(DataSet: TDataSet);
    procedure DBGCiItsDblClick(Sender: TObject);
    procedure BalanoIntermedirio1Click(Sender: TObject);
    procedure Alteraitemselecionado1Click(Sender: TObject);
    procedure PMCicloPopup(Sender: TObject);
    procedure Excluiciclo1Click(Sender: TObject);
    procedure BtPosExcluiClick(Sender: TObject);
    procedure Corrigeitensdeajustecfepqx1Click(Sender: TObject);
  private
    FPQBIts, FPQBResgata: String;
    FEmpresa: Integer;
    FBtAlteraCab_Enabled: Boolean;
    FLiberouEdicaoItem: Boolean;
    //
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    //procedure EditaProduto(SQLType: TSQLType);
    function  GeraDadosEstoqueAnterior(const Entidade: Integer; var Tabela:
              String): Boolean;
    //procedure GeraDif_1();
    //procedure GeraDif_2();
    function  JaTemBalancoPosterior(): Boolean;
    function  ExcluiTodosItensDoCliInt(): Boolean;
    function  ItensImpedemExclusao(): Boolean;
    function  VerificaSeEhUltimoBalanco(): Boolean;
    function  LiberouEdicaoItem(): Boolean;
    function  CiItsImpedemExclusao(Controle: Integer): Boolean;
  public
    { Public declarations }
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
    procedure SomaBalanco();
    procedure ReindexaTabela();
    procedure ReEntitula();
    procedure ReopenCliDest(CliInt: Integer);
    procedure Resgata(ExigeSenha: Boolean);
    procedure ReopenPQB3Resgata(Insumo: Integer);
    procedure MostraFormPQB3Adx(SQLType: TSQLType);
    procedure MostraFormPQB3Bxa(SQLType: TSQLType);
    procedure ReopenPQBAdx(Controle: Integer);
    procedure ReopenPQBBxa(Controle: Integer);
    procedure ReopenPQBCiCab(Controle: Integer);
    procedure ReopenPQBCiIts(Conta: Integer);
    procedure MostraFormPQB3CiIts();
  end;

var
  FmPQB3: TFmPQB3;

implementation
  uses UnMyObjects, Module, PQB3New, Principal, PQx, ModuleGeral, UCreate,
  MyDBCheck, PQB3Mul, DmkDAC_PF, CreateBlueDerm, UnPQ_PF, Periodo,
  PQB3Bxa, PQB3Adx, PQB3AdEdit, UMySQLDB, UnApp_Jan, PQB3CiCab, PQB3CiIts,
  PQB3CiImp, GerlShowGrid;

{$R *.DFM}

var
  TimerIni: TDateTime;

/////////////////////////////////////////////////////////////////////////////////////
function TFmPQB3.LiberouEdicaoItem(): Boolean;
begin
  Result := FLiberouEdicaoItem;
  if Result = False then
  begin
    FLiberouEdicaoItem := DBCheck.LiberaPelaSenhaBoss();
    Result := FLiberouEdicaoItem;
  end;
end;

procedure TFmPQB3.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQB3.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrBalancosPeriodo.Value, LaRegistro.Caption[2]);
end;

function TFmPQB3.VerificaSeEhUltimoBalanco(): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMax, Dmod.MyDB, [
  'SELECT Max(Periodo) Periodo ',
  'FROM balancos ',
  'WHERE Empresa=' + Geral.FF0(FEmpresa),
  '']);
  //
  Result := QrMaxPeriodo.Value = QrBalancosPeriodo.Value;
(*
  if QrMaxPeriodo.Value <> QrBalancosPeriodo.Value then
  begin
    //BtAlteraCab.Enabled  := False;
    FBtAlteraCab_Enabled := False;
    //BtGeraDif.Enabled := False;
    //ReindexaTabela();
    //ReopenCliDest(0);
  end else
  begin
    //BtAlteraCab.Enabled  := True;
    FBtAlteraCab_Enabled := True;
    //BtGeraDif.Enabled := True;
  end;
*)
  FBtAlteraCab_Enabled := Result;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPQB3.DefParams;
begin
  VAR_GOTOTABELA := 'Balancos';
  VAR_GOTOMYSQLTABLE := QrBalancos;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_PERIODO;
  VAR_GOTONOME := '';//CO_NOME;
  VAR_GOTOMYSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := 'Empresa=' + Geral.FF0(FEmpresa);

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT bal.*, emp.Filial, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_Emp');
  VAR_SQLx.Add('FROM balancos bal');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=bal.Empresa');
  VAR_SQLx.Add('WHERE bal.Periodo > 0');
  VAR_SQLx.Add('AND bal.Empresa=' + Geral.FF0(FEmpresa));
  //
  VAR_SQL1.Add('AND bal.Periodo=:P0');
  //
  VAR_SQLa.Add(''); //AND Nome Like :P0
  //
end;

procedure TFmPQB3.MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
begin
  if Mostra then
  begin
    GBCntrl.Visible    := False;
    GBConfirma.Visible := True;
  end else
  begin
    GBCntrl.Visible    := True;
    GBConfirma.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPQB3.MostraFormPQB3Adx(SQLType: TSQLType);
var
  Produto, Cliente, Empresa: Integer;
  OrigemCodi, OrigemCtrl, OrigemTipo: Integer;
begin
  OrigemCodi := QrPQBAdxCodigo.Value;
  OrigemCtrl := QrPQBAdxControle.Value;
  OrigemTipo := VAR_FATID_0020;
  if SQLType = stUpd then
    if PQ_PF.ImpedePeloEstoqueNF_Item(OrigemCodi, OrigemCtrl, OrigemTipo) then
      Exit;
  //VAR_LOCATE011 := QrPQBAdxFORNECEDO.Value;
  //VAR_LOCATE012 := QrPQBAdxNOMEGRUPO.Value;
  VAR_LOCATE014 := QrPQBAdxInsumo.Value;
  Produto := QrPQBAdxInsumo.Value;
  Cliente := QrPQBAdxCliInt.Value;
  if DBCheck.CriaFm(TFmPQB3Adx, FmPQB3Adx, afmoNegarComAviso) then
  begin
    with FmPQB3Adx do
    begin
      ImgTipo.SQLType := SQLType;
      FBalanco        := QrBalancosPeriodo.Value;
      if SQLType = stUpd then
      begin
        Empresa := DModG.ObtemFilialDeEntidade(QrPQBAdxEmpresa.Value);
        FControle                := QrPQBAdxControle.Value;
        EdEmpresa.ValueVariant   := Empresa;
        CBEmpresa.KeyValue       := Empresa;
        LaEmpresa.Enabled        := False;
        EdEmpresa.Enabled        := False;
        CBEmpresa.Enabled        := False;
        EdInsumo.ValueVariant    := Produto;
        CBInsumo.KeyValue        := Produto;
        LaInsumo.Enabled         := False;
        EdInsumo.Enabled         := False;
        CBInsumo.Enabled         := False;
        EdCliente.ValueVariant   := Cliente;
        CBCliente.KeyValue       := Cliente;
        LaCliente.Enabled        := False;
        EdCliente.Enabled        := False;
        CBCliente.Enabled        := False;
        //
        EdQtde.ValueVariant      := QrPQBAdxPeso.Value;
        EdVTota.ValueVariant     := QrPQBAdxValor.Value;
        Edide_serie.ValueVariant := QrPQBAdxide_serie.Value;
        Edide_nNF.ValueVariant   := QrPQBAdxide_nNF.Value;
        EdxLote.ValueVariant     := QrPQBAdxxLote.Value;
        TPdFab.Date              := QrPQBAdxdFab.Value;
        TPdVal.Date              := QrPQBAdxdVal.Value;
        //
      end else begin
        Empresa                  := DModG.ObtemFilialDeEntidade(QrCliDestCodigo.Value);
        EdInsumo.ValueVariant    := 0;
        CBInsumo.KeyValue        := 0;
        EdCliente.ValueVariant   := 0;
        CBCliente.KeyValue       := 0;
        EdQtde.ValueVariant      := 0;
        EdVTota.ValueVariant     := 0;
        Edide_serie.ValueVariant := 0;
        Edide_nNF.ValueVariant   := 0;
        EdxLote.ValueVariant     := '';
        TPdFab.Date              := 0;
        TPdVal.Date              := 0;
        //
        LaInsumo.Enabled         := True;
        EdInsumo.Enabled         := True;
        CBInsumo.Enabled         := True;
      end;
      ShowModal;
      Destroy;
    end;
  end;
  SomaBalanco();
  ReindexaTabela();
end;

procedure TFmPQB3.MostraFormPQB3Bxa(SQLType: TSQLType);
var
  Produto, Cliente, Empresa: Integer;
begin
  //VAR_LOCATE011 := QrPQBBxaFORNECEDO.Value;
  //VAR_LOCATE012 := QrPQBBxaNOMEGRUPO.Value;
  VAR_LOCATE014 := QrPQBBxaInsumo.Value;
  Produto := QrPQBBxaInsumo.Value;
  Cliente := QrPQBBxaCliInt.Value;
  if DBCheck.CriaFm(TFmPQB3Bxa, FmPQB3Bxa, afmoNegarComAviso) then
  begin
    with FmPQB3Bxa do
    begin
      ImgTipo.SQLType := SQLType;
      FBalanco        := QrBalancosPeriodo.Value;
      FControle       := QrPQBBxaControle.Value;
      //
      if SQLType = stUpd then
      begin
        Empresa                  := DModG.ObtemFilialDeEntidade(QrPQBBxaEmpresa.Value);
        EdEmpresa.ValueVariant   := Empresa;
        CBEmpresa.KeyValue       := Empresa;
        LaEmpresa.Enabled        := False;
        EdEmpresa.Enabled        := False;
        CBEmpresa.Enabled        := False;
        EdInsumo.ValueVariant    := Produto;
        CBInsumo.KeyValue        := Produto;
        LaInsumo.Enabled         := False;
        EdInsumo.Enabled         := False;
        CBInsumo.Enabled         := False;
        EdCliente.ValueVariant   := Cliente;
        CBCliente.KeyValue       := Cliente;
        LaCliente.Enabled        := False;
        EdCliente.Enabled        := False;
        CBCliente.Enabled        := False;
        //
        EdQtde.ValueVariant    := QrPQBBxaPeso.Value;
        EdVTota.ValueVariant   := QrPQBBxaValor.Value;
        //
      end else begin
        Empresa                  := DModG.ObtemFilialDeEntidade(QrCliDestCodigo.Value);
        EdInsumo.ValueVariant  := 0;
        CBInsumo.KeyValue      := 0;
        EdCliente.ValueVariant := 0;
        CBCliente.KeyValue     := 0;
        EdQtde.ValueVariant    := 0;
        EdVTota.ValueVariant   := 0;
        //
        LaCliente.Enabled       := True;
        EdCliente.Enabled       := True;
        CBCliente.Enabled       := True;
        //
        LaInsumo.Enabled       := True;
        EdInsumo.Enabled       := True;
        CBInsumo.Enabled       := True;
      end;
      ShowModal;
      Destroy;
    end;
  end;
  //SomaBalanco();
  ReopenPQBBxa(0);
end;

procedure TFmPQB3.MostraFormPQB3CiIts();
var
  Conta, Periodo: Integer;
  Empresa, CliInt, Insumo: Integer;
  NO_Empresa, NO_CliInt, NO_Insumo: String;
  //PesoAtuT, ValorAtuT,
  Preco: Double;
begin
  Periodo := QrBalancosPeriodo.Value;
  if UnPQx.VerificaBalanco()+ 1 <> Periodo then
  begin
    Geral.MB_Aviso('Altera��o abortada! Balan�o n�o � o vigente!');
    Exit;
  end;
  if not LiberouEdicaoItem() then Exit;
  //
  if QrPQBCiItsDstCtrl.Value <> 0 then
  begin
    // se for re-entrada...
    if QrPQBCiItsTipo.Value = VAR_FATID_0020 then
    begin
      // ...ent�o verificar se j� teve baixa desta re-entrada...
      UnDmkDAC_PF.AbreMySQLQuery0(QrSdo, Dmod.MyDB, [
      'SELECT Peso - SdoPeso DifPeso',
      'FROM pqx',
      'WHERE OrigemCodi=' + Geral.FF0(QrPQBCiItsDstCodi.Value),
      'AND OrigemCtrl=' + Geral.FF0(QrPQBCiItsDstCtrl.Value),
      'AND Tipo=' + Geral.FF0(VAR_FATID_0020),
      '']);
      if QrSdoDifPeso.Value >= 0.001 then
      begin
        // ...caso tiver saldo menor que entrada, ent�o cancelar edi��o.
        Geral.MB_Aviso('Item n�o pode mais ser editado pois j� tem baixa atrelada!');
        Exit;
      end;
    end;
  end;
  //else
  begin
    if QrPQBCiCab.RecNo = 1 then
    begin
      Empresa    := QrPQBCiItsEmpresa.Value;
      CliInt     := QrPQBCiItsCliInt.Value;
      Insumo     := QrPQBCiItsInsumo.Value;
      NO_Empresa := QrBalancosNO_Emp.Value;
      NO_CliInt  := QrCliDestNO_ENT.Value;
      //
      Conta := QrPQBCiItsConta.Value;
      if DBCheck.CriaFm(TFmPQB3CiIts, FmPQB3CiIts, afmoNegarComAviso) then
      begin
        FmPQB3CiIts.ImgTipo.SQLType            := stUpd;
        //
        FmPQB3CiIts.EdEmpresa.ValueVariant   := Empresa;
        FmPQB3CiIts.EdNO_Empresa.Text        := QrBalancosNO_Emp.Value;
        FmPQB3CiIts.EdCliInt.ValueVariant    := CliInt;
        FmPQB3CiIts.EdNO_CliInt.Text         := QrCliDestNO_ENT.Value;
        FmPQB3CiIts.EdInsumo.ValueVariant    := Insumo;
        FmPQB3CiIts.EdNO_Insumo.Text         := QrPQBCiItsNO_PQ.Value;
        //
        FmPQB3CiIts.FPQ                        := Insumo;
        FmPQB3CiIts.FCI                        := CliInt;
        FmPQB3CiIts.FEmpresa                   := Empresa;
        FmPQB3CiIts.FConta                     := QrPQBCiItsConta.Value;
        FmPQB3CiIts.FCodigo                    := QrBalancosPeriodo.Value;
        FmPQB3CiIts.FDataX                     := QrPQBCiCabDataAtu.Value;
        FmPQB3CiIts.FAntLancar                 := QrPQBCiItsLancar.Value;
        FmPQB3CiIts.EdTipo.ValueVariant        := QrPQBCiItsTipo.Value;
        FmPQB3CiIts.EdDstCodi.ValueVariant     := QrPQBCiItsDstCodi.Value;
        FmPQB3CiIts.EdDstCtrl.ValueVariant     := QrPQBCiItsDstCtrl.Value;
        // ini 2023-11-04
(*
        PesoAtuT := QrPQBCiItsPesoAtuA.Value + QrPQBCiItsPesoAtuB.Value + QrPQBCiItsPesoAtuC.Value;
        ValorAtuT := QrPQBCiItsValorAtuA.Value + QrPQBCiItsValorAtuB.Value + QrPQBCiItsValorAtuC.Value;
        if PesoAtuT <> 0 then
          Preco := ValorAtuT / PesoAtuT
        else
          Preco := 0.00;
        FmPQB3CiIts.EdPreco.ValueVariant       := Preco;
        FmPQB3CiIts.EdPesoAtuT.ValueVariant    := PesoAtuT;
        FmPQB3CiIts.EdValorAtuT.ValueVariant   := ValorAtuT;
*)
        if QrPQBCiItsPesoAtuT.Value <> 0 then
          Preco := QrPQBCiItsValorAtuT.Value / QrPQBCiItsPesoAtuT.Value
        else
          Preco := 0.00;
        FmPQB3CiIts.EdPreco.ValueVariant       := Preco;
        FmPQB3CiIts.EdPesoAtuT.ValueVariant    := QrPQBCiItsPesoAtuT.Value;
        FmPQB3CiIts.EdValorAtuT.ValueVariant   := QrPQBCiItsValorAtuT.Value;
        // fim 2023-11-04
        FmPQB3CiIts.EdPesoInfo.ValueVariant    := QrPQBCiItsPesoInfo.Value;
        FmPQB3CiIts.EdValorInfo.ValueVariant   := QrPQBCiItsValorInfo.Value;
        FmPQB3CiIts.EdPesoDife .ValueVariant   := QrPQBCiItsPesoDife.Value;
        FmPQB3CiIts.EdValorDife.ValueVariant   := QrPQBCiItsValorDife.Value;
        FmPQB3CiIts.EdSerie.ValueVariant       := QrPQBCiItsSerie.Value;
        FmPQB3CiIts.EdNF.ValueVariant          := QrPQBCiItsNF.Value;
        FmPQB3CiIts.EdxLote.ValueVariant       := QrPQBCiItsxLote.Value;
        FmPQB3CiIts.TPdFab.Date                := QrPQBCiItsdFab.Value;
        FmPQB3CiIts.TPdVal.Date                := QrPQBCiItsdVal.Value;
        if QrPQBCiItsValorDife.Value <> 0 then
          FmPQB3CiIts.CkLancar.Checked           := Geral.IntToBool(QrPQBCiItsLancar.Value)
        else
          FmPQB3CiIts.CkLancar.Checked           := True;
        FmPQB3CiIts.ShowModal;
        FmPQB3CiIts.Destroy;
        //
        ReopenPQBCiIts(Conta);
        ReopenPQBAdx(QrPQBAdxControle.Value);
        ReopenPQBBxa(QrPQBBxaControle.Value);
      end;
    end else
      Geral.MB_Aviso('Edi��o permitida somente no �ltimo ciclo!');
  end;
end;

procedure TFmPQB3.Outrasimpressoes1Click(Sender: TObject);
begin
  FmPrincipal.CadastroPQImp('FmPQB');
end;

procedure TFmPQB3.PMAlteraCabPopup(Sender: TObject);
begin
  TodoBalanco1.Enabled := FBtAlteraCab_Enabled;
  DadosDoAjusteAPositivo1.Enabled := (QrPQBAdx.State <> dsInactive) and
    (QrPQBAdx.RecordCount > 0) and (PCItens.ActivePageIndex = 1);
end;

procedure TFmPQB3.PMCicloPopup(Sender: TObject);
var
  HabilitaAll, HabilitaCab, HabilitaIts: Boolean;
begin
  HabilitaAll := FBtAlteraCab_Enabled;
  HabilitaCab := (QrPQBCiCab.State <> dsInactive) and (QrPQBCiCab.RecNo = 1);
  HabilitaIts := (QrPQBCiIts.State <> dsInactive) and (QrPQBCiIts.RecordCount > 0);
  //
  Incluinovociclo1.Enabled := HabilitaAll;
  Alteraitemselecionado1.Enabled := HabilitaAll and HabilitaIts;
  Excluiciclo1.Enabled := HabilitaAll and HabilitaCab;
end;

function TFmPQB3.CiItsImpedemExclusao(Controle: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCiItsDif, Dmod.MyDB, [
  'SELECT Empresa, CliInt, Insumo, PesoDIfe  ',
  'FROM pqbciits ',
  'WHERE Controle=' + Geral.FF0(Controle),
  'AND PesoDife <> 0 ',
  '']);
  //
  Result := QrCiItsDif.RecordCount > 0;
  if Result then
  begin
    Application.CreateForm(TFmGerlShowGrid, FmGerlShowGrid);
    FmGerlShowGrid.MeAvisos.Lines.Add('Eclus�o de ciclo cancelado. ');
    FmGerlShowGrid.MeAvisos.Lines.Add('Os itens abaixo impedem a exclus�o');
    FmGerlShowGrid.DBGSel.DataSource := DsCiItsDif;
    FmGerlShowGrid.ShowModal;
    FmGerlShowGrid.Destroy;
  end;
end;

procedure TFmPQB3.Corrigeitensdeajustecfepqx1Click(Sender: TObject);
begin
(*   Parei Aqui
  'SELECT Peso, Valor ',
  'FROM pqx',
  'WHERE OrigemCodi=287',
  'AND Tipo IN (20, 120)',
*)
end;

procedure TFmPQB3.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPQB3.Alteraitemselecionado1Click(Sender: TObject);
begin
  MostraFormPQB3CiIts();
end;

procedure TFmPQB3.AlteraRegistro;
begin
  (*if*) JaTemBalancoPosterior() (*then
    Exit*);
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  MostraEdicao(True, stIns, 0);
  UMyMod.UpdLockY(QrBalancosPeriodo.Value, Dmod.MyDB, 'Balancos', 'Periodo');
end;

procedure TFmPQB3.ImgTipoChange(Sender: TObject);
var
  Visivel: Boolean;
begin
  Visivel := ImgTipo.SQLType in ([stIns, stUpd]);
  PnPQBAdx.Visible := Visivel;
  PnPQBBxa.Visible := Visivel;
end;

procedure TFmPQB3.Incluinovociclo1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Periodo: Integer;
  DataAnt, DataBal: TDateTime;
begin
  Periodo := QrBalancosPeriodo.Value;
  //
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(DataAtu) DataIni',
    'FROM pqbcicab',
    'WHERE Codigo=' + Geral.FF0(Periodo),
    '']);
    DataAnt := Qry.Fields[0].AsDateTime;
    DataBal := dmkPF.PrimeiroDiaDoPeriodo_Date(Periodo);
    if DataAnt < DataBal then
      DataAnt := DataBal;
  finally
    Qry.Free;
  end;
  if DBCheck.CriaFm(TFmPQB3CiCab, FmPQB3CiCab, afmoNegarComAviso) then
  begin
    FmPQB3CiCab.ImgTipo.SQLType := stIns;
    FmPQB3CiCab.EdPeriodo.ValueVariant := QrBalancosPeriodo.Value;
    FmPQB3CiCab.EdPeriodo2.ValueVariant := QrBalancosPeriodo2.Value;
    FmPQB3CiCab.EdDataAnt.ValueVariant := DataAnt;
    FmPQB3CiCab.TPDataAtu.Date := Trunc(DModG.ObtemAgora());
    //
    FmPQB3CiCab.ShowModal;
    FmPQB3CiCab.Destroy;
    //
    LocCod(Periodo, Periodo);
  end;
end;

procedure TFmPQB3.IncluiRegistro;
begin
  Application.CreateForm(TFmPQB3New, FmPQB3New);
  FmPQB3New.ShowModal;
  FmPQB3New.Destroy;
end;

function TFmPQB3.ItensImpedemExclusao(): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrItImped, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _no_del_balanco_; ',
  ' ',
  'CREATE TABLE _no_del_balanco_ ',
  ' ',
  'SELECT "Ajustes a positivo (re-entradas)" Descricao, COUNT(Codigo) Itens ',
  'FROM ' + TMeuDB + '.pqbadx ',
  'WHERE Codigo=' + Geral.FF0(QrBalancosPeriodo.Value),
  ' ',
  'UNION ',
  ' ',
  'SELECT "Ajustes a negativo (baixas)" Descricao, COUNT(Codigo) Itens ',
  'FROM ' + TMeuDB + '.pqbbxa ',
  'WHERE Codigo=' + Geral.FF0(QrBalancosPeriodo.Value),
  ' ',
  'UNION ',
  ' ',
  'SELECT "Ciclos do per�odo (balan�os intermedi�rios)" Descricao, COUNT(Codigo) Itens ',
  'FROM ' + TMeuDB + '.pqbcicab ',
  'WHERE Codigo=' + Geral.FF0(QrBalancosPeriodo.Value),
  '; ',
  ' ',
  'SELECT *  ',
  'FROM _no_del_balanco_ ',
  'WHERE Itens > 0 ',
  'UNION ALL ',
  '  SELECT "TOTAL" Descricao, SUM(Itens) FROM _no_del_balanco_ ',
  ';  ',
  '']);
  //
  // ir para o registro do TOTAL
  QrItImped.Last;
  //
  Result := QrItImpedItens.Value > 0;
  if Result then
  begin
    Application.CreateForm(TFmGerlShowGrid, FmGerlShowGrid);
    FmGerlShowGrid.MeAvisos.Lines.Add('Eclus�o de balan�o cancelado. ');
    FmGerlShowGrid.MeAvisos.Lines.Add('Os itens abaixo impedem a exclus�o');
    FmGerlShowGrid.DBGSel.DataSource := DsItImped;
    FmGerlShowGrid.ShowModal;
    FmGerlShowGrid.Destroy;
  end;
end;

function TFmPQB3.JaTemBalancoPosterior(): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT *  ',
    'FROM balancos ',
    'WHERE Periodo > ' + Geral.FF0(QrBalancosPeriodo.Value),
    'AND Empresa=' + Geral.FF0(QrBalancosEmpresa.Value),
    '']);
    //
    Result := Qry.RecordCount > 0;
    if Result then
      Geral.MB_Aviso('J� existe um balan�o posterior!');
  finally
    Qry.Free;
  end;
end;

procedure TFmPQB3.QueryPrincipalAfterOpen;
begin
{
  if (QrBalancosPeriodo.Value = (UnPQx.VerificaBalanco (*+ 1*))) then
  begin
    //BtAlteraCab.Enabled := True;
    FBtAlteraCab_Enabled := True;
    BtGeraDif.Enabled := True;
  end else
  begin
    //UnDmkDAC_PF.AbreQuery(QrMax, Dmod.MyDB);
    UnDmkDAC_PF.AbreMySQLQuery0(QrMax, Dmod.MyDB, [
    'SELECT Max(Periodo) Periodo ',
    'FROM balancos ',
    'WHERE Empresa=' + Geral.FF0(FEmpresa),
    '']);
    //
    if QrMaxPeriodo.Value <> QrBalancosPeriodo.Value then
    begin
      //BtAlteraCab.Enabled  := False;
      FBtAlteraCab_Enabled := False;
      BtGeraDif.Enabled := False;
      //ReindexaTabela();
      ReopenCliDest(0);
    end else
    begin
      //BtAlteraCab.Enabled  := True;
      FBtAlteraCab_Enabled := True;
      BtGeraDif.Enabled := True;
    end;
  end;
  ///
}
  VerificaSeEhUltimoBalanco();
end;

procedure TFmPQB3.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQB3.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQB3.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQB3.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQB3.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQB3.TodoBalanco1Click(Sender: TObject);
begin
  AlteraRegistro();
end;

procedure TFmPQB3.BalanoIntermedirio1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Periodo: Integer;
  //DataBal,
  DataAnt: TDateTime;
begin
  if JaTemBalancoPosterior() then
    Exit;
  if MyObjects.FIC(VerificaSeEhUltimoBalanco() = False, nil,
  'O Balan�o selecionado n�o � o �ltimo!') then Exit;
  //
  (*
  if MyObjects.FIC(QrPQBCiCab.RecordCount = 0, nil,
  'N�o h� ciclo a ser impresso neste balan�o') then Exit;
  *)
  //
  // ir para o �ltimo balanco intermedi�rio do per�odo
  //QrPQBCiCab.First;
  if QrPQBCiCab.RecordCount > 0 then
    if QrPQBCiCab.RecNo > 1 then
      if Geral.MB_Pergunta(
      'O balan�o intermedi�rio selecionado n�o � o �ltimo feito.' + sLineBreak +
      'Deseja continuar assim mesmo?') <> ID_YES then Exit;
  //
  Periodo  := QrBalancosPeriodo.Value;
  //Controle := QrPQBCiCabControle.Value;

  //
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(DataAtu) DataIni',
    'FROM pqbcicab',
    //'WHERE Codigo=' + Geral.FF0(Periodo),
    '']);
    DataAnt := Qry.Fields[0].AsDateTime;
    (*
    DataBal := dmkPF.PrimeiroDiaDoPeriodo_Date(Periodo);
    if DataAnt < DataBal then
      DataAnt := DataBal;
    *)
  finally
    Qry.Free;
  end;
  if DBCheck.CriaFm(TFmPQB3CiImp, FmPQB3CiImp, afmoNegarComAviso) then
  begin
    FmPQB3CiImp.ImgTipo.SQLType := stPsq;
    FmPQB3CiImp.EdPeriodo.ValueVariant := QrBalancosPeriodo.Value;
    FmPQB3CiImp.EdPeriodo2.ValueVariant := QrBalancosPeriodo2.Value;
    FmPQB3CiImp.EdDataAnt.ValueVariant := DataAnt;
    FmPQB3CiImp.TPDataAtu.Date := Trunc(DModG.ObtemAgora());
    //
    FmPQB3CiImp.ShowModal;
    FmPQB3CiImp.Destroy;
  end;
end;

procedure TFmPQB3.BtAjustaClick(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de entidade';
  Prompt = 'Informe a entidade: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Controle, Entidade: Variant;
begin
  Entidade := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT DISTINCT pqc.CI Codigo, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ' + Campo,
    'FROM pqcli pqc ',
    'LEFT JOIN entidades ent ON ent.Codigo=pqc.CI ',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, True);
  if Entidade <> Null then
  begin
    Screen.Cursor := crHourGlass;
    try
      FPQBIts := UnCreateBlueDerm.RecriaTempTableNovo(ntrttPQBIts, DModG.QrUpdPID1, False);
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DModG.MyPID_DB, [
      'INSERT INTO ' + FPQBIts,
      'SELECT pqc.Controle, pqc.PQ, pqc.CI, ',
      'pqc.CustoPadrao, pqc.Peso, pqc.Valor, pqc.Custo, ',
      'pqc.Peso AjPeso, pqc.Valor AjValor, pqc.CustoPadrao AjCusto, ',
      'pq.Nome NO_PQ, 0 Ativo ',
      'FROM ' + TMeuDB + '.pqcli pqc ',
      'LEFT JOIN ' + TMeuDB + '.pq pq ON pq.Codigo=pqc.PQ ',
      'WHERE pqc.CI=' + Geral.FF0(Entidade),
      'ORDER BY pq.Nome ',
      '']);
      if DBCheck.CriaFm(TFmPQB3Mul, FmPQB3Mul, afmoNegarComAviso) then
      begin
        Screen.Cursor := crDefault;
        FmPQB3Mul.FPQBIts := FPQBIts;
        FmPQB3Mul.ReopenPQBIts();
        FmPQB3Mul.ShowModal;
        FmPQB3Mul.Destroy;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPQB3.BtAlteraCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAlteraCab, BtAlteraCab);
end;

procedure TFmPQB3.BtCicloClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 3;
  //
  MyObjects.MostraPopUpDeBotao(PMCiclo, BtCiclo);
end;

procedure TFmPQB3.BtSaidaClick(Sender: TObject);
begin
  VAR_CAIXA := QrBalancosPeriodo.Value;
  Close;
end;

procedure TFmPQB3.BtTravaClick(Sender: TObject);
var
  Periodo : Integer;
  DataIni: TDateTime;
begin
  //VAR_PERIODO_BAL := -2;
  SomaBalanco();
  Periodo := QrBalancosPeriodo.Value;
  UMyMod.UpdUnlockY(Periodo, Dmod.MyDB, 'Balancos', 'Periodo');
  MostraEdicao(False, stLok, 0);
  LocCod(Periodo, Periodo);
  //
  try
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('UPDATE pqcli SET Peso=0, Valor=0, Custo=0');
    Dmod.QrUpdU.ExecSQL;
////////////////////////////////////////////////////////////////////////////////
    (*
    QrAtualiza.Close;
    UnDmkDAC_PF.AbreQuery(QrAtualiza, Dmod.MyDB);
    Progress1.Position := 0;
    Progress1.Max := GOTOy.Registros(QrAtualiza);
    PainelDados2.Visible := True;
    PainelDados2.Refresh;
    GBCntrl.Refresh;
    Panel1.Refresh;
    Panel2.Refresh;
    TimerIni := Now();

    //  fazer apenas uma vez para n�o demorar o UnPQx.AtualizaEstoquePQ !!!
    DMod.AtualizaStqMovIts_PQX(nil, nil, nil);
    //
    VAR_PQS_ESTQ_NEG := '';
    while not QrAtualiza.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
      PnRegistros.Refresh;
      PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
      PnTempo.Refresh;
      Application.ProcessMessages;
      UnPQx.AtualizaEstoquePQ(QrAtualizaCI.Value, QrAtualizaPQ.Value, QrAtualizaEmpresa.Value, aeVAR, CO_VAZIO, False);
      QrAtualiza.Next;
    end;
    QrAtualiza.Close;
    *)
    DataIni    := Geral.PeriodoToDate(Periodo, 1, False, detJustSum);
    if not PQ_PF.AtualizaTodosPQsPosBalanco(aeVAR, Progress1, PainelDados2,
      PnRegistros, PnTempo, TimerIni, DataIni) then
      Exit;
////////////////////////////////////////////////////////////////////////////////
    if VAR_PQS_ESTQ_NEG <> '' then
    begin
      Geral.MB_Aviso(VAR_PQS_ESTQ_NEG);
      VAR_PQS_ESTQ_NEG := '';
    end else
      Geral.MB_Info('Atualiza��o de estoques finalizada');
    //
    PainelDados2.Visible := False;
    // Parei aqui! N�o usa mais?
    //if QrBalancosConfirmado.Value = 'F' then FmPrincipal.RefreshInsumos;
{    ini tentativa de eliminar TmySQLQuery
    FmPrincipal.AtualizaBalancoDePQNoSMI(nil);
}
  except
    raise;
    Geral.MB_Erro('Ocorreu um erro nas atualiza��es de estoque.');
  end;
end;

procedure TFmPQB3.FormCreate(Sender: TObject);
begin
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  ImgTipo.SQLType := stLok;
  PCItens.ActivePageIndex := 0;
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  CriaOForm;
  //
  MostraEdicao(False, stLok, 0);
end;

procedure TFmPQB3.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Valor: string;
begin
  if Shift = [ssCtrl] then
  begin
    if dmkPF.EhControlF(Sender, Key, Shift) then
    begin
      if InputQuery('Pesquisa de item pelo c�digo', 'Informe o c�digo:', Valor) then
        QrBalancosIts.Locate('Insumo', Valor, []);
    end;
  end;
end;

procedure TFmPQB3.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrBalancosPeriodo.Value, LaRegistro.Caption);
end;

procedure TFmPQB3.SbQueryClick(Sender: TObject);
begin
//
end;

procedure TFmPQB3.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPQB3.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmPQB3.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQB3.QrBalancosAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  GOTOy.BtEnabled(QrBalancosPeriodo.Value, False);
  QueryPrincipalAfterOpen;
  BtAjusta.Enabled := True;
  //BtResgata.Enabled := True;
  Habilita := (QrBalancos.RecordCount > 0)
    and (UnPQx.VerificaBalanco+1 = QrBalancosPeriodo.Value);
  BtExclui.Enabled := Habilita;
  BtCiclo.Enabled := Habilita;
  ReEntitula();
end;

procedure TFmPQB3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQB3.BtIncluiClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  IncluiRegistro;
end;

procedure TFmPQB3.QrBalancosCalcFields(DataSet: TDataSet);
begin
  QrBalancosPeriodo2.Value := ' Balan�o de '+
  Geral.Maiusculas(dmkPF.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtTexto), False);
end;

procedure TFmPQB3.DadosDoAjusteAPositivo1Click(Sender: TObject);
  procedure ConfirmouClick(ide_serie, ide_nNF: Integer; xLote, dFab, dVal:
  String);
  var
    Controle, Tipo, Serie, NF: Integer;
    SQLType: TSQLType;
    OriCodi, OriCtrl, OriTipo: Integer;
  begin
    SQLType        := stUpd; //ImgTipo.SQLType;
    OriCodi        := QrPQBAdxCodigo.Value; //FBalanco;
    Controle       := QrPQBAdxControle.Value; //FControle;
    OriCtrl        := Controle;
    OriTipo        := VAR_FATID_0010; // FTipo;
    {ide_serie      := Edide_serie.ValueVariant;
    ide_nNF        := Edide_nNF.ValueVariant;
    xLote          := EdxLote.Text;
    dFab           := Geral.FDT(TPdFab.Date, 1);
    dVal           := Geral.FDT(TPdVal.Date, 1);}
    Tipo           := OriTipo;
    Serie          := ide_serie;
    NF             := ide_nNF;
    //
    //if MyObjects.FIC(ide_nNF = 0, Edide_nNF, 'Informe a NF (VP)!') then Exit;
    //if MyObjects.FIC(xLote = EmptyStr, EdxLote, 'Informe o Lote!') then Exit;
    //
    Cursor := Screen.Cursor;
    Screen.Cursor := crHourglass;
    try
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqx', False, [
      'NF', 'xLote', 'Serie'], [
      'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
      NF, xLote, Serie], [
      OriCodi, OriCtrl, Tipo], False) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqbadx', False, [
        'ide_serie', 'ide_nNF',
        'xLote', 'dFab', 'dVal'], [
        'Controle'], [
        ide_serie, ide_nNF,
        xLote, dFab, dVal], [
        Controle], True) then
        begin
          //
        end;
      end;
    except
      Screen.Cursor := Cursor;
      Exit;
    end;
  end;
const
  MostradFabdVal = True;
var
  Empresa, CliInt, Insumo: Integer;
  NO_Empresa, NO_CliInt, NO_Insumo: String;
  ide_Serie, ide_nNF: Integer;
  xLote: String;
  dFab, dVal: TDateTime;
begin
  Empresa    := QrPQBAdxEmpresa.Value;
  CliInt     := QrPQBAdxCliInt.Value;
  Insumo     := QrPQBAdxInsumo.Value;
  NO_Empresa := QrBalancosNO_Emp.Value;
  NO_CliInt  := QrCliDestNO_ENT.Value;
  NO_Insumo  := QrPQBAdxNOMEPQ.Value;
  ide_Serie  := QrPQBAdxide_Serie.Value;
  ide_nNF    := QrPQBAdxide_nNF.Value;
  xLote      := QrPQBAdxxLote.Value;
  dFab       := QrPQBAdxdFab.Value;
  dVal       := QrPQBAdxdVal.Value;
  //
(*
  if DBCheck.CriaFm(TFmPQB3AdEdit, FmPQB3AdEdit, afmoNegarComAviso) then
  begin
    FmPQB3AdEdit.ImgTipo.SQLType          := stUpd;
    FmPQB3AdEdit.FBalanco                 := QrPQBAdxCodigo.Value;
    FmPQB3AdEdit.FControle                := QrPQBAdxControle.Value;
    FmPQB3AdEdit.FTipo                    := VAR_FATID_0020;
    //
    FmPQB3AdEdit.EdEmpresa.ValueVariant   := Empresa;
    FmPQB3AdEdit.EdNO_Empresa.Text        := QrBalancosNO_Emp.Value;
    FmPQB3AdEdit.EdCliInt.ValueVariant    := CliInt;
    FmPQB3AdEdit.EdNO_CliInt.Text         := QrCliDestNO_ENT.Value;
    FmPQB3AdEdit.EdInsumo.ValueVariant    := Insumo;
    FmPQB3AdEdit.EdNO_Insumo.Text         := QrPQBAdxNOMEPQ.Value;
    //
    FmPQB3AdEdit.Edide_serie.ValueVariant := QrPQBAdxide_Serie.Value;
    FmPQB3AdEdit.Edide_nNF.ValueVariant   := QrPQBAdxide_nNF.Value;
    FmPQB3AdEdit.EdxLote.ValueVariant     := QrPQBAdxxLote.Value;
    FmPQB3AdEdit.TPdFab.Date              := QrPQBAdxdFab.Value;
    FmPQB3AdEdit.TPdVal.Date              := QrPQBAdxdVal.Value;
    //
    FmPQB3AdEdit.ShowModal;
    //
    if FmPQB3AdEdit.FConfirmou then
*)
   if App_Jan.MostraFormPQB3AdEdit(Empresa, CliInt, Insumo, NO_Empresa,
   NO_CliInt, NO_Insumo, ide_Serie, ide_nNF, xLote, dFab, dVal, MostradFabdVal) then
   begin
      ConfirmouClick(FmPQB3AdEdit.Fide_serie, FmPQB3AdEdit.Fide_nNF,
        FmPQB3AdEdit.FxLote, FmPQB3AdEdit.FdFab, FmPQB3AdEdit.FdVal);
    end;
  //
  FmPQB3AdEdit.Destroy;
  //
  ReopenPQBAdx(QrPQBAdxControle.Value);
end;

procedure TFmPQB3.DBGBalancosItsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
(*
  if ImgTipo.SQLType <> stLok then
  begin
    if (key = VK_F13) then
      EditaProduto(stUpd);
    if (key = VK_INSERT) then
      EditaProduto(stIns);
  end;
*)
end;

procedure TFmPQB3.DBGCiItsDblClick(Sender: TObject);
begin
  MostraFormPQB3CiIts();
end;

procedure TFmPQB3.ReEntitula();
var
  Texto: String;
begin
  if QrBalancos.State <> dsInactive then
  begin
    //Texto := 'Balan�o de Insumos: ' + QrBalancosPeriodo2.Value;
    Texto := QrBalancosPeriodo2.Value;
    MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Texto, False, taCenter, 2, 10, 20);
  end;
end;

procedure TFmPQB3.ReindexaTabela();
var
  SQL, Txt, Mercadoria: String;
begin
  Mercadoria := Trim(EdPesquisa.ValueVariant);
  //
  if Mercadoria <> '' then
  begin
    Txt := StringReplace(Mercadoria, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
    SQL := 'AND pq_.Nome LIKE "%' + Txt + '%"';
  end else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBalancosIts, Dmod.MyDB, [
    'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, ',
    'pq_.GrupoQuimico, pqg.Nome NOMEGRUPO, ',
    'IF(pqx.Valor=0, 0, pqx.Valor/pqx.Peso) CUSTOKG ',
    'FROM pqx pqx ',
    'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo ',
    'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ ',
    'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico ',
    'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
    'AND pqx.OrigemCodi=' + Geral.FF0(QrBalancosPeriodo.Value),
    'AND pqx.CliDest=' + Geral.FF0(QrClidestCodigo.Value),
    'AND pqx.Empresa=' + Geral.FF0(QrBalancosEmpresa.Value),
    SQL,
    '']);
end;

procedure TFmPQB3.ReopenCliDest(CliInt: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCliDest, Dmod.MyDB, [
  'SELECT DISTINCT ent.Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT' ,
  'FROM pqx pqx ',
  'LEFT JOIN entidades ent ON ent.Codigo=pqx.CliDest ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
  //'AND ent.Codigo <> 0 ',
  'AND pqx.OrigemCodi=' + Geral.FF0(QrBalancosPeriodo.Value),
  'AND pqx.Empresa=' + Geral.FF0(QrBalancosEmpresa.Value),
  'ORDER BY NO_ENT ',
  '']);
  //
  QrCliDest.Locate('Codigo', CliInt, []);
end;

procedure TFmPQB3.ReopenPQB3Resgata(Insumo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQB3Resgata, DModG.MyPID_DB, [
  'SELECT * FROM ' + FPQBResgata,
  '']);
  //
  if Insumo <> 0 then
    QrPQB3Resgata.Locate('Insumo', Insumo, []);
end;

procedure TFmPQB3.ReopenPQBAdx(Controle: Integer);
var
  SQL, Txt, Mercadoria: String;
begin
  Mercadoria := Trim(EdPesquisa.ValueVariant);
  //
  if Mercadoria <> '' then
  begin
    Txt := StringReplace(Mercadoria, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
    SQL := 'AND pq_.Nome LIKE "%' + Txt + '%"';
  end else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQBAdx, Dmod.MyDB, [
  'SELECT DISTINCT adx.*, pq_.Nome NOMEPQ,',
  'IF(adx.Valor=0, 0, adx.Valor/adx.Peso) CUSTOKG',
  'FROM pqbadx adx',
  'LEFT JOIN pq pq_ ON pq_.Codigo=adx.Insumo',
  'WHERE adx.Codigo=' + Geral.FF0(QrBalancosPeriodo.Value),
  'AND adx.CliInt=' + Geral.FF0(QrClidestCodigo.Value),
  'AND adx.Empresa=' + Geral.FF0(QrBalancosEmpresa.Value),
  SQL,
  '']);
  //
  if Controle <> 0 then
    QrPQBAdx.Locate('Controle', Controle, []);
end;

procedure TFmPQB3.ReopenPQBBxa(Controle: Integer);
var
  SQL, Txt, Mercadoria: String;
begin
  Mercadoria := Trim(EdPesquisa.ValueVariant);
  //
  if Mercadoria <> '' then
  begin
    Txt := StringReplace(Mercadoria, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
    SQL := 'AND pq_.Nome LIKE "%' + Txt + '%"';
  end else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQBBxa, Dmod.MyDB, [
  'SELECT DISTINCT bxa.*, pq_.Nome NOMEPQ,',
  'IF(bxa.Valor=0, 0, bxa.Valor/bxa.Peso) CUSTOKG',
  'FROM pqbbxa bxa',
  'LEFT JOIN pq pq_ ON pq_.Codigo=bxa.Insumo',
  'WHERE bxa.Codigo=' + Geral.FF0(QrBalancosPeriodo.Value),
  'AND bxa.CliInt=' + Geral.FF0(QrCliDestCodigo.Value),
  'AND bxa.Empresa=' + Geral.FF0(QrBalancosEmpresa.Value),
  SQL,
  '']);
  //
  if Controle <> 0 then
    QrPQBBxa.Locate('Controle', Controle, []);
end;

procedure TFmPQB3.ReopenPQBCiCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQBCiCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM pqbcicab ',
  'WHERE Codigo=' + Geral.FF0(QrBalancosPeriodo.Value),
  'ORDER BY DataAtu DESC ',
  '']);
end;

procedure TFmPQB3.ReopenPQBCiIts(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQBCiIts, Dmod.MyDB, [
  'SELECT pq.Nome NO_PQ, ',
  'IF(PesoDife=0, "N/A", IF(its.Lancar=0, "N�o", "Sim")) NO_Lancar,',
  'IF(dFab < 1 , "", DATE_FORMAT(dFab, "%d/%m/%y")) dFab_TXT, ',
  'IF(dVal < 1 , "", DATE_FORMAT(dVal, "%d/%m/%y")) dVal_TXT, ',
  'its.* ',
  'FROM pqbciits its ',
  'LEFT JOIN pq pq ON pq.Codigo=its.Insumo',
  'WHERE its.Controle=' + Geral.FF0(QrPQBCiCabControle.Value),
  'AND its.Empresa=' + Geral.FF0(QrBalancosEmpresa.Value),
  'AND its.CliInt=' + Geral.FF0(QrCliDestCodigo.Value),
  'ORDER BY pq.Nome ',
  '']);
end;

(*
procedure TFmPQB3.Resgata(ExigeSenha: Boolean);
const
  Aviso  = '...';
  Titulo = 'Sele��o de entidade';
  Prompt = 'Informe a entidade: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Controle, Entidade: Variant;
  Periodo: Integer;
  Data: TDateTime;
  DataI, DataF, PQBResgata: String;
  Qry: TmySQLQuery;
begin
  if ExigeSenha then
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
  Entidade := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT DISTINCT pqc.CI Codigo, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ' + Campo,
    'FROM pqcli pqc ',
    'LEFT JOIN entidades ent ON ent.Codigo=pqc.CI ',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, True);
  if Entidade <> Null then
  begin
    Screen.Cursor := crHourGlass;
    try
      Periodo := QrBalancosPeriodo.Value;
      Data    := Geral.PeriodoToDate(Periodo, 1, False, detJustSum);
      DataF   := Geral.FDT(Data, 1);
      //
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Insumo',
        'FROM pqx ',
        'WHERE DataX="' + DataF + '"',
        'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
        'AND pqx.CliOrig=' + Geral.FF0(Entidade),
        'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
        '']);
        if Qry.recordCount > 0 then
        begin
          Geral.MB_Aviso('A��o cancelada!' + sLineBreak +
          'Este cliente interno j� possui itens de balan�o para este per�odo!');
          Exit;
        end;
      finally
        Qry.Free;
      end;
      //
      if GeraDadosEstoqueAnterior(Entidade, PQBResgata) then
      begin
        if DBCheck.CriaFm(TFmPQB3Resgata, FmPQB3Resgata, afmoNegarComAviso) then
        begin
          Screen.Cursor := crDefault;
          FmPQB3Resgata.FPQBResgata := PQBResgata;
          FmPQB3Resgata.FPeriodo    := QrBalancosPeriodo.Value;
          FmPQB3Resgata.FDataX      := DataF;
          FmPQB3Resgata.FCliInt     := Entidade;
          FmPQB3Resgata.FEmpresa    := FEmpresa;
          //
          FmPQB3Resgata.EdCliIntCodi.ValueVariant := Entidade;
          DModG.ReopenEnti(Entidade);
          FmPQB3Resgata.EdCliIntNome.ValueVariant := DModG.QrEntiNO_ENTI.Value;
          FmPQB3Resgata.ReopenPQB3Resgata(0);
          FmPQB3Resgata.ShowModal;
          FmPQB3Resgata.Destroy;
          //
          ReopenCliDest(Entidade);
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;
*)

procedure TFmPQB3.Resgata(ExigeSenha: Boolean);
  procedure ResgataItensCliIntAtual(FCliInt, FPeriodo: Integer; FDataX: String;
  PB1: TProgressBar; LaAviso1, LaAviso2: TLabel; NomeEnt: String);
  const
    Tipo = VAR_FATID_0000;
    Unitario = False;
  var
    Conta, CliOrig, CliDest, Insumo, OrigemCodi, OriCodi, OriCtrl, OriTipo: Integer;
    Peso, Valor, OrigemCtrl: Double;
    DataX, DtCorrApo: String;
  begin
    Screen.Cursor := crHourGlass;
    try
      DtCorrApo := Geral.FDT(0, 1);
      DataX := FDataX;
      CliOrig := FCliInt;
      CliDest := FCliInt;
      QrPQB3Resgata.First;
      PB1.Position := 0;
      PB1.Max := QrPQB3Resgata.RecordCount;
      while not QrPQB3Resgata.Eof do
      begin
        Insumo := QrPQB3ResgataInsumo.Value;
        Peso   := QrPQB3ResgataPeso.Value;
        Valor  := QrPQB3ResgataValor.Value;
        //
        OrigemCodi := FPeriodo;
        //
        if (Peso <> 0) or (Valor <> 0) then
        begin
          MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
            'Insumo ' + Geral.FF0(Insumo) + ' > ' + NomeEnt);
          OrigemCtrl := UMyMod.BuscaEmLivreY_Double(
            Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
          //
          OriCodi := OrigemCodi;
          OriCtrl := Trunc(OrigemCtrl);
          OriTipo := Tipo;
          PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
            OriCtrl, OriTipo, Unitario, DtCorrApo, FEmpresa);
        end;
        //
        QrPQB3Resgata.Next;
      end;
      PQ_PF.VerificaEquiparacaoEstoque(True);
      Screen.Cursor := crDefault;
      //Close;
    finally
      Screen.Cursor := crDefault;
    end;
  end;

var
  Controle, Entidade: Variant;
  Periodo: Integer;
  Data: TDateTime;
  DataI, DataF, NO_Ent: String;
  Qry: TmySQLQuery;
begin
(*  if ExigeSenha then
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrCliInts, Dmod.MyDB, [
  'SELECT DISTINCT pqc.CI Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome ',
  'FROM pqcli pqc ',
  'LEFT JOIN entidades ent ON ent.Codigo=pqc.CI ',
  'ORDER BY Codigo',
  '']);
  QrCliInts.First;
  while not QrCliInts.Eof do
  begin
    Entidade := QrCliIntsCodigo.Value;
    NO_Ent := QrCliIntsNome.Value;
    if Entidade <> Null then
    begin
      Screen.Cursor := crHourGlass;
      try
        Periodo := QrBalancosPeriodo.Value;
        Data    := Geral.PeriodoToDate(Periodo, 1, False, detJustSum);
        DataF   := Geral.FDT(Data, 1);
        //
        if GeraDadosEstoqueAnterior(Entidade, FPQBResgata) then
        begin
          ReopenPQB3Resgata(0);
          ResgataItensCliIntAtual(Entidade, QrBalancosPeriodo.Value, DataF,
          FmPQB3New.PB1, FmPQB3New.LaAviso1, FmPQB3New.LaAviso2, NO_Ent);
          (*
          if DBCheck.CriaFm(TFmPQB3Resgata, FmPQB3Resgata, afmoNegarComAviso) then
          begin
            Screen.Cursor := crDefault;
            FmPQB3Resgata.FPQBResgata := PQBResgata;
            FmPQB3Resgata.FPeriodo    := QrBalancosPeriodo.Value;
            FmPQB3Resgata.FDataX      := DataF;
            FmPQB3Resgata.FCliInt     := Entidade;
            FmPQB3Resgata.FEmpresa    := FEmpresa;
            //
            FmPQB3Resgata.EdCliIntCodi.ValueVariant := Entidade;
            DModG.ReopenEnti(Entidade);
            FmPQB3Resgata.EdCliIntNome.ValueVariant := DModG.QrEntiNO_ENTI.Value;
            FmPQB3Resgata.ReopenPQB3Resgata(0);
            FmPQB3Resgata.ShowModal;
            FmPQB3Resgata.Destroy;
            //
            ReopenCliDest(Entidade);
          end;
          *)
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
    //
    QrCliInts.Next;
  end;
end;

{
procedure TFmPQB3.EditaProduto(SQLType: TSQLType);
var
  Cliente, Produto, Empresa: Integer;
begin
  Geral.MB_Info('Procedimento desabilitado! Solicite � Dermatek!');
  EXIT;
  //
  //VAR_LOCATE011 := QrBalancosItsFORNECEDO.Value;
  VAR_LOCATE012 := QrBalancosItsNOMEGRUPO.Value;
  VAR_LOCATE014 := QrBalancosItsInsumo.Value;
  Produto := QrBalancosItsInsumo.Value;
  Cliente := QrBalancosItsCliOrig.Value;
  Empresa := QrBalancosItsEmpresa.Value;
  Application.CreateForm(TFmPQB3Edit, FmPQB3Edit);
  with FmPQB3Edit do
  begin
    ImgTipo.SQLType := SQLType;
    FBalanco        := QrBalancosPeriodo.Value;
    if SQLType = stUpd then
    begin
      EdEmpresa.ValueVariant  := Empresa;
      CBEmpresa.KeyValue      := Empresa;
      LaEmpresa.Enabled       := False;
      EdEmpresa.Enabled       := False;
      CBEmpresa.Enabled       := False;
      EdInsumo.ValueVariant  := Produto;
      CBInsumo.KeyValue      := Produto;
      EdCliente.ValueVariant := Cliente;
      CBCliente.KeyValue     := Cliente;
      EdPecas.ValueVariant   := QrBalancosItsPeso.Value;
      EdVTota.ValueVariant   := QrBalancosItsValor.Value;
      //
      LaInsumo.Enabled       := False;
      EdInsumo.Enabled       := False;
      CBInsumo.Enabled       := False;
    end else begin
      EdInsumo.ValueVariant  := 0;
      CBInsumo.KeyValue      := 0;
      EdCliente.ValueVariant := 0;
      CBCliente.KeyValue     := 0;
      EdPecas.ValueVariant   := 0;
      EdVTota.ValueVariant   := 0;
      //
      LaInsumo.Enabled       := True;
      EdInsumo.Enabled       := True;
      CBInsumo.Enabled       := True;
    end;
    ShowModal;
    Destroy;
  end;
  SomaBalanco();
  ReindexaTabela();
end;
}

procedure TFmPQB3.EdPesquisaChange(Sender: TObject);
begin
  ReindexaTabela();
  ReopenPQBAdx(0);
  ReopenPQBBxa(0);
end;

procedure TFmPQB3.EsteBalano1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxQUI_BALAN_001_01, 'Balan�o');
end;

procedure TFmPQB3.Excluiciclo1Click(Sender: TObject);
var
  Controle, Periodo: Integer;
begin
  Controle := QrPQBCiCabControle.Value;
  Periodo := QrBalancosPeriodo.Value;
  if UnPQx.VerificaBalanco()+ 1 <> Periodo then
    Geral.MB_Aviso('Exclus�o da ciclo abortada! Balan�o n�o � o vigente!');
  if MyObjects.FIC(QrPQBCiCab.RecNo <> 1, nil,
  'Este ciclo n�o pode ser exclu�do pois n�o � o �ltimo!') then Exit;
  //
  if CiItsImpedemExclusao(Controle) then Exit;
  //
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  if Geral.MB_Pergunta('Confirma a exclus�o de todo o ciclo selecionado?') =
  ID_YES then
  begin
    UndmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM pqbciits WHERE Controle=' + Geral.FF0(Controle) + '; ' + sLineBreak +
    'DELETE FROM pqbcicab WHERE Controle=' + Geral.FF0(Controle) + '; ');
    //
    UnPQx.DefineVAR_Data_Insum_Movim();
    //
    LocCod(Periodo, Periodo);
  end;
end;

function TFmPQB3.ExcluiTodosItensDoCliInt(): Boolean;
var
  CliInt, Empresa, Insumo, OriCodi, OriCtrl, OriTipo: Integer;
begin
  VAR_PQS_ESTQ_NEG := '';
  try
    QrBalancosIts.First;
    while not QrBalancosIts.Eof do
    begin
      CliInt     := QrCliDestCodigo.Value;
      Empresa    := QrBalancosItsEmpresa.Value;
      Insumo     := QrBalancosItsInsumo.Value;
      OriCodi    := QrBalancosItsOrigemCodi.Value;
      OriCtrl    := QrBalancosItsOrigemCtrl.Value;
      OriTipo    := QrBalancosItsTipo.Value;
      //
      PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, False, Empresa);
      //
      QrBalancosIts.Next;
    end;
    if VAR_PQS_ESTQ_NEG <> '' then
    begin
      Geral.MB_Aviso(VAR_PQS_ESTQ_NEG);
      VAR_PQS_ESTQ_NEG := '';
    end;
    Result := True;
  except;
    Result := False;
  end;
end;

{
procedure TFmPQB3.BtIncluiItsClick(Sender: TObject);
begin
object BtIncluiIts: TBitBtn
  Tag = 10
  Left = 324
  Top = 4
  Width = 90
  Height = 40
  Cursor = crHandPoint
  Caption = '&Insere'
  ParentShowHint = False
  ShowHint = True
  TabOrder = 2
  OnClick = BtIncluiItsClick
end
  //EditaProduto(stIns);
end;
}

{
procedure TFmPQB3.BtInicialClick(Sender: TObject);
const
 Tipo = VAR_FATID_0000; // Balan�o
 Unitario = False;
var
  Peso, Valor: Double;
  DataI, DataF, DataX, DtCorrApo: String;
  PeriodoBal, CliOrig, CliDest, Insumo, OrigemCodi: Integer;
  OrigemCtrl: Double;
begin
object BtInicial: TBitBtn
  Left = 692
  Top = 4
  Width = 90
  Height = 40
  Cursor = crHandPoint
  Caption = 'I&nicial'
  Enabled = False
  NumGlyphs = 2
  ParentShowHint = False
  ShowHint = True
  TabOrder = 2
  OnClick = BtInicialClick
end
  Geral.MB_Info('Procedimento desabilitado! Solicite � Dermatek!');
  EXIT;
  //
  Screen.Cursor := crHourGlass;
  try
    DtCorrApo := Geral.FDT(0, 1);
    DataX := dmkPF.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtSystem);
    PeriodoBal := UnPQx.VerificaBalancoPelaData(
      Geral.PeriodoToDate(QrBalancosPeriodo.Value-1, 1, False, detJustSum));
    if PeriodoBal = 0 then
    begin
      DataI := FormatDateTime(VAR_FORMATDATE, 2);
      DataF := dmkPF.UltimoDiaDoPeriodo(QrBalancosPeriodo.Value-1, dtSystem);
    end else begin
      DataI := dmkPF.PrimeiroDiaDoPeriodo(PeriodoBal, dtSystem);
      DataF := dmkPF.UltimoDiaDoPeriodo(PeriodoBal, dtSystem);
    end;
    QrPQCli.Close;
    QrPQCli.Params[00].AsString  := DataI;
    QrPQCli.Params[01].AsString  := DataF;
    UnDmkDAC_PF.AbreQuery(QrPQCli, Dmod.MyDB);
    while not QrPQCli.Eof do
    begin
      Peso  := 0;
      Valor := 0;
      //
      Dmod.QrSumPQ_D.Close;
      Dmod.QrSumPQ_D.Params[00].AsInteger := QrPQCliCliDest.Value;
      Dmod.QrSumPQ_D.Params[01].AsInteger := QrPQCliInsumo.Value;
      Dmod.QrSumPQ_D.Params[02].AsString  := DataI;
      Dmod.QrSumPQ_D.Params[03].AsString  := DataF;
        //dmkPF.PrimeiroDiaDoPeriodo(PeriodoBal+1, dtSystem);
      UnDmkDAC_PF.AbreQuery(Dmod.QrSumPQ_D, Dmod.MyDB);
      Peso  := Peso  + Dmod.QrSumPQ_DPeso.Value;
      if Peso <> 0 then
      begin
        Valor := Valor + Dmod.QrSumPQ_DValor.Value;
        //
        CliOrig    := QrPQCliCliDest.Value;
        CliDest    := CliOrig;
        Insumo     := QrPQCliInsumo.Value;
        OrigemCodi := QrBalancosPeriodo.Value;
        OrigemCtrl := UMyMod.BuscaEmLivreY_Double(
          Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
        //
        PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor,
        OrigemCodi, Trunc(OrigemCtrl), Tipo, Unitario, DtCorrApo, FEmpresa);
        //
      end;
      QrPQCli.Next;
    end;
    PQ_PF.VerificaEquiparacaoEstoque(True);
  finally
    Screen.Cursor := crDefault;
    LocCod(QrBalancosPeriodo.Value, QrBalancosPeriodo.Value);
  end;
end;
}

procedure TFmPQB3.BtNegAlteraClick(Sender: TObject);
begin
  MostraFormPQB3Bxa(stUpd);
end;

procedure TFmPQB3.BtNegExcluiClick(Sender: TObject);
  procedure ExcluiItemAtual(Avisa: Boolean);
  var
    Periodo, Insumo, Controle, CliInt, OriCodi, OriCtrl, OriTipo, Empresa: Integer;
  begin
    Periodo  := QrPQBBxaCodigo.Value;
    Insumo   := QrPQBBxaInsumo.Value;
    Controle := QrPQBBxaControle.Value;
    //
    Empresa  := QrPQBBxaEmpresa.Value;
    CliInt   := QrPQBBxaCliInt.Value;
    OriCodi  := QrPQBBxaCodigo.Value;
    OriCtrl  := QrPQBBxaControle.Value;
    OriTipo  := VAR_FATID_0120; //QrPQBBxaTipo.Value;
    if PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, Avisa, Empresa) then
    begin
      USQLDB.ExcluiRegistroInt1((*Pergunta*)'', 'pqbbxa', 'Controle', Controle, Dmod.MyDB);
    end;
  end;
var
  q: TSelType;
  n: Integer;
begin
  //QrPQBBxa.DisableControls;
  DBCheck.Quais_Selecionou(QrPQBBxa, TDBGrid(DBGPQBBxa), q);
  case q of
    istAtual: ExcluiItemAtual(True);
    istSelecionados:
    begin
      with DBGPQBBxa.DataSource.DataSet do
      for n := 0 to DBGPQBBxa.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DBGPQBBxa.SelectedRows.Items[n]));
        GotoBookmark(DBGPQBBxa.SelectedRows.Items[n]);
        ExcluiItemAtual(False);
      end;
      PQ_PF.VerificaEquiparacaoEstoque(True);
    end;
    istTodos:
    begin
      VAR_PQS_ESTQ_NEG := '';
      QrPQBBxa.First;
      while not QrPQBBxa.Eof do
      begin
        ExcluiItemAtual(False);
        //
        QrPQBBxa.Next;
      end;
      if VAR_PQS_ESTQ_NEG <> '' then
      begin
        Geral.MB_Aviso(VAR_PQS_ESTQ_NEG);
        VAR_PQS_ESTQ_NEG := '';
      end;
      PQ_PF.VerificaEquiparacaoEstoque(True);
    end;
  end;
  QrPQBBxa.EnableControls;
  //
  SomaBalanco();
end;

procedure TFmPQB3.BtNegIncluiClick(Sender: TObject);
begin
  MostraFormPQB3Bxa(stIns);
end;

{
procedure TFmPQB3.BtAlteraItsClick(Sender: TObject);
begin
object BtAlteraIts: TBitBtn
  Tag = 11
  Left = 416
  Top = 4
  Width = 90
  Height = 40
  Cursor = crHandPoint
  Caption = 'Edit&a'
  Enabled = False
  ParentShowHint = False
  ShowHint = True
  TabOrder = 2
  OnClick = BtAlteraItsClick
end
  //EditaProduto(stUpd);
end;
}

procedure TFmPQB3.QrBalancosItsAfterOpen(DataSet: TDataSet);
(*
var
  Habilita: Boolean;
*)
begin
  (*
  Habilita            := QrBalancosIts.RecordCount > 0;
  //BtAlteraIts.Enabled := Habilita;
  //BtInicial.Enabled   := not Habilita;
  //BtExcluiIts.Enabled := Habilita;
  *)
end;

procedure TFmPQB3.QrBalancosItsBeforeClose(DataSet: TDataSet);
begin
  //BtInicial.Enabled := False;
end;

procedure TFmPQB3.SomaBalanco();
begin
  QrSoma.Close;
  QrSoma.Params[0].AsInteger := QrBalancosPeriodo.Value;
  UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE balancos SET ');
  Dmod.QrUpdU.SQL.Add('EstqQ=:P0, EstqV=:P1 ');
  Dmod.QrUpdU.SQL.Add('WHERE Periodo=:P2');
  Dmod.QrUpdU.Params[0].AsFloat   := QrSomaEstqQ.Value;
  Dmod.QrUpdU.Params[1].AsFloat   := QrSomaEstqV.Value;
  Dmod.QrUpdU.Params[2].AsInteger := QrBalancosPeriodo.Value;
  Dmod.QrUpdU.ExecSQL;
  //
  LocCod(QrBalancosPeriodo.Value, QrBalancosPeriodo.Value);
end;

{
procedure TFmPQB3.BtExcluiItsClick(Sender: TObject);
  procedure ExcluiItemAtual(Avisa: Boolean);
  var
    Periodo, Insumo, Controle, CliInt, OriCodi, OriCtrl, OriTipo, Empresa: Integer;
  begin
    Periodo  := QrBalancosItsOrigemCodi.Value;
    Insumo   := QrBalancosItsInsumo.Value;
    Controle := QrBalancosItsOrigemCtrl.Value;
    //
    Empresa  := QrBalancosItsEmpresa.Value;
    CliInt   := QrBalancosItsCliOrig.Value;
    OriCodi  := QrBalancosItsOrigemCodi.Value;
    OriCtrl  := QrBalancosItsOrigemCtrl.Value;
    OriTipo  := QrBalancosItsTipo.Value;
    PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, Avisa, Empresa);
  end;
var
  q: TSelType;
  n: Integer;
begin
object BtExcluiIts: TBitBtn
  Tag = 12
  Left = 508
  Top = 4
  Width = 90
  Height = 40
  Cursor = crHandPoint
  Caption = '&Exclui'
  NumGlyphs = 2
  ParentShowHint = False
  ShowHint = True
  TabOrder = 2
  OnClick = BtExcluiItsClick
end
  Geral.MB_Info('Procedimento desabilitado! Solicite � Dermatek!');
  EXIT;
  //
  //QrBalancosIts.DisableControls;
  DBCheck.Quais_Selecionou(QrBalancosIts, TDBGrid(DBGBalancosIts), q);
  case q of
    istAtual: ExcluiItemAtual(True);
    istSelecionados:
    begin
      with DBGBalancosIts.DataSource.DataSet do
      for n := 0 to DBGBalancosIts.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DBGBalancosIts.SelectedRows.Items[n]));
        GotoBookmark(DBGBalancosIts.SelectedRows.Items[n]);
        ExcluiItemAtual(False);
      end;
      PQ_PF.VerificaEquiparacaoEstoque(True);
    end;
    istTodos:
    begin
      VAR_PQS_ESTQ_NEG := '';
      QrBalancosIts.First;
      while not QrBalancosIts.Eof do
      begin
        ExcluiItemAtual(False);
        //
        QrBalancosIts.Next;
      end;
      if VAR_PQS_ESTQ_NEG <> '' then
      begin
        Geral.MB_Aviso(VAR_PQS_ESTQ_NEG);
        VAR_PQS_ESTQ_NEG := '';
      end;
      PQ_PF.VerificaEquiparacaoEstoque(True);
    end;
  end;
  QrBalancosIts.EnableControls;
  SomaBalanco();
end;
}

procedure TFmPQB3.BtGeraDifClick(Sender: TObject);
begin
  //GeraDif_2();
end;

{procedure TFmPQB3.GeraDif_2;
const
  Tipo = VAR_FATID__001;
  GerouDif = 1;
  Unitario = False;
var
  CI: Integer;
  NomeCI: String;
  Qry: TmySQLQuery;
  //
  Peso, Valor, Preco, QtdBal: Double;
  NO_PQ, Tabela: String;
  Insumo: Integer;
  //
  Periodo, OrigemCodi, OrigemCtrl, CliOrig, CliDest, CliInt, OriCodi, OriCtrl,
  OriTipo: Integer;
  DataX, DtCorrApo: String;
  //
begin
object BtGeraDif: TBitBtn
  Left = 372
  Top = 4
  Width = 90
  Height = 40
  Cursor = crHandPoint
  Caption = '&Gera dif m'#234's ant'
  Enabled = False
  NumGlyphs = 2
  ParentShowHint = False
  ShowHint = True
  TabOrder = 4
  Visible = False
  OnClick = BtGeraDifClick
end
  Geral.MB_Info('Procedimento desabilitado por depreca��o.');
  EXIT;
  //
  if QrCliDest.RecordCount <> 0 then
  begin
    Periodo    := QrBalancosPeriodo.Value;
    OrigemCodi := Periodo - 1;
    CI         := QrCliDestCodigo.Value;
    CliOrig    := CI;
    CliDest    := CI;
    NomeCI     := QrCliDestNO_ENT.Value;
    DtCorrApo := Geral.FDT(0, 1);
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM pqx ',
      'WHERE Tipo=' + Geral.FF0(VAR_FATID__001),
      'AND CliDest=' + Geral.FF0(CI),
      'AND OrigemCodi=' + Geral.FF0(OrigemCodi),
      'AND RetQtd <> 0 ',
      '']);
      if Qry.RecordCount > 0 then
      begin
        Geral.MB_Aviso('A��o abortada!' + slineBreak +
        'J� existem ' + Geral.FF0(Qry.RecordCount) +
        ' itens gerados anteriormente com NF de retorno!');
        Exit;
      end;
      if Geral.MB_Pergunta(
      'Deseja realmente gerar as diferen�as do balan�o do cliente interno "' +
      NomeCI + '" no m�s anterior?') =
      ID_YES then
      begin
        if not ExcluiTodosItensDoCliInt() then Exit;
        //
        if GeraDadosEstoqueAnterior(CI, Tabela) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT pqx.Insumo, SUM(pqx.Peso) Peso, ',
          'pq.Nome NO_PQ',
          'FROM pqx pqx',
          'LEFT JOIN pq pq ON pq.Codigo=pqx.Insumo ',
          'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
          'AND pqx.CliDest=' + Geral.FF0(CI),
          'AND pqx.OrigemCodi=' + Geral.FF0(Periodo),
          'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
          'GROUP BY pqx.Insumo ',
          '']);
          //
          PB2.Position := 0;
          PB2.Max := Qry.recordCount;
          Qry.First;
          while not Qry.Eof do
          begin
            PB2.Position := PB2.Position + 1;
            //
            Peso := Qry.FieldByName('Peso').AsFloat;
            if Peso <> 0 then
            begin
              Insumo  := Qry.FieldByName('Insumo').AsInteger;
              NO_PQ   := Qry.FieldByName('NO_PQ').AsString;
              QtdBal  := Qry.FieldByName('Peso').AsFloat;
              Peso    := 0;
              Preco   := 0;
              //
              UMyMod.SQLIns_ON_DUPLICATE_KEY(DModG.QrUpdPID1, Tabela, False, [
              'Peso', 'Valor',
              'Preco', 'NO_PQ', 'QtdBal'], [
              'Insumo'], ['QtdBal'], [
              Peso, Valor,
              Preco, NO_PQ, QtdBal], [
              Insumo], [QtdBal], False);
            end;
            Qry.Next;
          end;
          UnDmkDAC_PF.AbreMySQLQuery0(QrDif2, DModG.MyPID_DB, [
          'SELECT Insumo, (QtdBal - Peso) Peso, ',
          'IF(Peso=0, 0, Valor / Peso) Preco ',
          'FROM ' + Tabela,
          'WHERE QtdBal - Peso <> 0 ',
          '']);
          //
          QrDif2.First;
          PB2.Position := 0;
          PB2.Max := QrDif2.RecordCount;
          //
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT MAX(OrigemCtrl) OrigemCtrl ',
          'FROM pqx ',
          'WHERE Tipo=' + Geral.FF0(VAR_FATID__001),
          'AND OrigemCodi=' + Geral.FF0(OrigemCodi),
          '']);
          OrigemCtrl := Qry.FieldByName('OrigemCtrl').AsInteger;
          DataX   := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(OrigemCodi), 1);
          while not QrDif2.Eof do
          begin
            MyObjects.UpdPB(PB2, LaAviso1, LaAviso2);
            //
            OrigemCtrl     := OrigemCtrl + 1;
            Insumo         := QrDif2Insumo.Value;
            Peso           := QrDif2Peso.Value;
            Valor          := Peso * QrDif2Preco.Value;
            //
            PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor,
            OrigemCodi, OrigemCtrl, Tipo, Unitario, DtCorrApo, FEmpresa);
            //
            QrDif2.Next;
          end;
          PQ_PF.VerificaEquiparacaoEstoque(True);
          //
          CliInt := CliDest;
          UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'balancli', False, [
          'GerouDif'], [
          'Periodo', 'CliInt', 'Empresa'], [
          'GerouDif'], [
          GerouDif], [
          Periodo, CliInt, FEmpresa], [
          GerouDif], False);
          //
          Geral.MB_Aviso(Geral.FF0(QrDif2.RecordCount) + ' itens foram inseridos!');
        end;
      end;
    finally
      Screen.Cursor := crDefault;
      Qry.Free;
    end;
  end else
    Geral.MB_Aviso('N�o existem lan�amentos de insumos para o cliente selecionado!');
end;
}

procedure TFmPQB3.BtExcluiClick(Sender: TObject);
var
  Periodo: Integer;
begin
  if Geral.MB_Pergunta('Deseja excluir este balan�o e TODOS itens dele?') = ID_YES then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
    Periodo := QrBalancosPeriodo.Value;
    if UnPQx.VerificaBalanco()+ 1 <> Periodo then
      Geral.MB_Aviso('Exclus�o da balan�o abortada! Balan�o n�o � o vigente!')
    else begin
      // verificar baixas e reentradas...
      // ... e verificar balan�os parciais
      if ItensImpedemExclusao() then Exit;
      //
      if Geral.MB_Pergunta('Deseja REALMENTE excluir este balan�o e TODOS itens dele?') = ID_YES then
      begin
        //VAR_PERIODO_BAL := -2;
        QrCliDest.First;
        while not QrCliDest.Eof do
        begin
          if not ExcluiTodosItensDoCliInt() then Exit;
          //
          QrCliDest.Next;
        end;
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdU, Dmod.MyDB, [
        'DELETE FROM balancos ',
        'WHERE Periodo=' + Geral.FF0(Periodo),
        'AND Empresa=' + Geral.FF0(FEmpresa),
        '']);
        //
        Dmod.DesfazBalancoGrade();
        //
        Periodo := UnPQx.VerificaBalanco()+ 1;
        LocCod(Periodo, Periodo);
        if QrBalancosPeriodo.Value = Periodo then BtTravaClick(Self);
      end;
    end;
  end;
  //VAR_PERIODO_BAL := -2;
end;

{
procedure TFmPQB3.BtRefreshClick(Sender: TObject);
var
  Periodo: Integer;
  //Sit
  Confirmado: String;
begin
object BtRefresh: TBitBtn
  Tag = 18
  Left = 152
  Top = 4
  Width = 120
  Height = 40
  Caption = '&Faz / Desfaz'
  TabOrder = 2
  OnClick = BtRefreshClick
end
  Geral.MB_Info('Procedimento desabilitado! Solicite � Dermatek!');
  EXIT;
  //
  Periodo := QrBalancosPeriodo.Value;
  Confirmado := dmkPF.EscolhaDe2Str(QrBalancosConfirmado.Value = 'V', 'V', 'F');
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'balancos', False, [
  'Confirmado'], [
  'Periodo', 'Empresa'], [
  Confirmado], [
  Periodo, FEmpresa], True) then
    LocCod(Periodo, Periodo);
end;
}
procedure TFmPQB3.QrBalancosItsCalcFields(DataSet: TDataSet);
begin
  QrBalancosItsNOMEGRUPO.Value := 'N�o dispon�vel ainda.';
end;

procedure TFmPQB3.QrCliDestAfterScroll(DataSet: TDataSet);
begin
  ReindexaTabela();
  ReopenPQBBxa(0);
  ReopenPQBAdx(0);
  ReopenPQBCiCab(0);
end;

procedure TFmPQB3.QrCliDestBeforeClose(DataSet: TDataSet);
begin
  BtAjusta.Enabled := False;
  //BtResgata.Enabled := False;
  BtExclui.Enabled := False;
  //
  QrBalancosIts.Close;
  QrPQBCiCab.Close;
end;

procedure TFmPQB3.QrPQBAdxAfterOpen(DataSet: TDataSet);
begin
  BtPosAltera.Enabled := QrPQBAdx.RecordCount > 0;
end;

procedure TFmPQB3.QrPQBAdxBeforeClose(DataSet: TDataSet);
begin
  BtPosAltera.Enabled := False;
end;

procedure TFmPQB3.QrPQBAdxBeforeOpen(DataSet: TDataSet);
begin
  BtNegAltera.Enabled := QrPQBAdx.RecordCount > 0;
end;

procedure TFmPQB3.QrPQBBxaAfterOpen(DataSet: TDataSet);
begin
  BtNegAltera.Enabled := QrPQBBxa.RecordCount > 0;
end;

procedure TFmPQB3.QrPQBBxaBeforeClose(DataSet: TDataSet);
begin
  BtNegAltera.Enabled := False;
end;

procedure TFmPQB3.QrPQBCiCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPQBCiIts(0);
end;

procedure TFmPQB3.QrPQBCiCabBeforeClose(DataSet: TDataSet);
begin
  QrPQBCiIts.Close;
end;

procedure TFmPQB3.FormResize(Sender: TObject);
begin
  ReEntitula();
end;

procedure TFmPQB3.frxQUI_BALAN_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VAR_CINOME' then
    Value := QrCliDestNO_ENT.Value
  else
  if VarName = 'VAR_MES' then
    Value := QrBalancosPeriodo2.Value
  else
  if VarName = 'VAR_MES_MES' then
    Value := QrBalancosPeriodo2.Value
  else
end;

function TFmPQB3.GeraDadosEstoqueAnterior(const Entidade: Integer; var Tabela: String): Boolean;
var
  Periodo: Integer;
  Qry: TmySQLQUery;
  Data: TDateTime;
  DataI, DataF: String;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    Periodo := QrBalancosPeriodo.Value;
    Data    := Geral.PeriodoToDate(Periodo, 1, False, detJustSum);
    DataF   := Geral.FDT(Data, 1);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(Periodo) Periodo ',
    'FROM balancos ',
    'WHERE Periodo<' + Geral.FF0(Periodo),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    '']);
    //
    Periodo := Qry.FieldByName('Periodo').AsInteger;
    Data    := Geral.PeriodoToDate(Periodo, 1, False, detJustSum);
    DataI   := Geral.FDT(Data, 1);
    //
    Tabela := UnCreateBlueDerm.RecriaTempTableNovo(ntrttPQB2_Resgata, DModG.QrUpdPID1, False);
    //
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + Tabela,
    'SELECT pqx.Insumo, SUM(pqx.Peso) Peso, SUM(pqx.Valor) Valor, ',
    'IF(SUM(pqx.Peso) = 0 , 0, SUM(pqx.Valor) / SUM(pqx.Peso)) PRECO, ',
    'pq.Nome NO_PQ, 0 QtdBal, 1 Ativo ',
    'FROM ' + TMeuDB + '.pqx pqx ',
    'LEFT JOIN ' + TMeuDB + '.pq pq ON pq.Codigo=pqx.Insumo ',
    'WHERE pqx.Insumo>0',
    'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
    'AND pqx.CliOrig=' + Geral.FF0(Entidade),
    'AND ( ',
    '    pqx.DataX >="' + DataI + '" ',
    '    AND ',
    '    pqx.DataX < "' + DataF + '" ',
    ') ',
    'GROUP BY pqx.Insumo ',
    'ORDER BY NO_PQ ',
    '']);
  finally
    Qry.Free;
  end;
end;

procedure TFmPQB3.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPQB3.QrBalancosAfterScroll(DataSet: TDataSet);
begin
  ReEntitula();
  ReopenCliDest(0);
end;

procedure TFmPQB3.QrBalancosBeforeClose(DataSet: TDataSet);
begin
  QrCliDest.Close;
end;

procedure TFmPQB3.BtPosAlteraClick(Sender: TObject);
begin
  MostraFormPQB3Adx(stUpd);
end;

procedure TFmPQB3.BtPosExcluiClick(Sender: TObject);
const
  Tipo = VAR_FATID_0020;
var
  Empresa, CliInt, Insumo, OriCodi, OriCtrl, OriTipo, Controle: Integer;
begin
  Controle := QrPQBAdxControle.Value;
  //
  Empresa  := QrPQBAdxEmpresa.Value;
  CliInt   := QrPQBAdxCliInt.Value;
  Insumo   := QrPQBAdxInsumo.Value;
  OriCodi  := QrPQBAdxCodigo.Value;
  OriCtrl  := QrPQBAdxControle.Value;
  OriTipo  := VAR_FATID_0020;
  //
  if PQ_PF.ImpedePeloEstoqueNF_Item(OriCodi, OriCtrl, OriTipo) then Exit;
  //
  // j� fez para alterar todo o balan�o.
  //if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  if Geral.MB_Pergunta('Confirma a exclus�o do item de re-entrada selecionado?') =
  ID_YES then
  begin
    if PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, Tipo, True, Empresa) then
      USQLDB.ExcluiRegistroInt1((*Pergunta*)'', 'pqbadx', 'Controle', Controle, Dmod.MyDB);
    //
    ReopenPQBAdx(0);
  end;
end;

procedure TFmPQB3.BtPosIncluiClick(Sender: TObject);
begin
  MostraFormPQB3Adx(stIns);
end;

{
procedure TFmPQB3.BtPrivativoClick(Sender: TObject);
const
  Unitario = False;
var
  Conta: Double;
  DataX, DtCorrApo: String;
  OriCodi, OriCtrl, OriTipo, CliOrig, CliDest, Insumo: Integer;
  Peso, Valor: Double;
  DataIni: TDateTime;
begin
object BtPrivativo: TBitBtn
  Tag = 182
  Left = 600
  Top = 4
  Width = 90
  Height = 40
  Cursor = crHandPoint
  Caption = '&Zera'
  NumGlyphs = 2
  ParentShowHint = False
  ShowHint = True
  TabOrder = 2
  OnClick = BtPrivativoClick
end
  Geral.MB_Info('Procedimento desabilitado! Solicite � Dermatek!');
  EXIT;
  //
  DtCorrApo := Geral.FDT(0, 1);
  DataX := dmkPF.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtSystem);
  if Geral.MB_Pergunta('Todos itens deste balan�o ser�o excluidos, '
  +'e ser�o inclu�dos itens para zerar estoques negativos. Deseja executar '+
  'esta a��o assim mesmo?') = ID_YES then
  begin
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM balancosits WHERE Periodo=:P0 ');
    Dmod.QrUpdU.SQL.Add('AND Empresa=:P1 ');
    Dmod.QrUpdU.Params[00].AsInteger := QrBalancosPeriodo.Value;
    Dmod.QrUpdU.Params[01].AsInteger := QrBalancosEmpresa.Value;
    Dmod.QrUpdU.ExecSQL;
    //
  end else
    Exit;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE pqcli SET Peso=0, Valor=0');
  Dmod.QrUpdU.ExecSQL;

  //////////////////////////////////////////////////////////////////////////////
    DataIni    := Geral.PeriodoToDate(QrBalancosPeriodo.Value, 1, False, detJustSum);
    if not PQ_PF.AtualizaTodosPQsPosBalanco(aeNenhum, Progress1, PainelDados2,
    PnRegistros, PnTempo, TimerIni, DataIni) then
      Exit;

  //////////////////////////////////////////////////////////////////////////////

  QrProdutos.Close;
  UnDmkDAC_PF.AbreQuery(QrProdutos, Dmod.MyDB);
  Progress1.Position := 0;
  Progress1.Max := QrProdutos.RecordCount;
  while not QrProdutos.Eof do
  begin
    Progress1.Position := Progress1.Position + 1;
    PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
    PnRegistros.Refresh;
    PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
    PnTempo.Refresh;
    //
    Conta := UMyMod.BuscaEmLivreY_Double(
    Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
    //DataX   := DataX;
    OriCodi := QrBalancosPeriodo.Value;
    OriCtrl := Trunc(Conta);
    OriTipo := VAR_FATID_0000;
    CliOrig := QrProdutosCI.Value;
    CliDest := QrProdutosCI.Value;
    Insumo  := QrProdutosPQ.Value;
    Peso    := -QrProdutosPeso.Value;
    Valor   := -QrProdutosValor.Value;
    PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
      OriCtrl, OriTipo, Unitario, DtCorrApo, FEmpresa);
    QrProdutos.Next;
  end;
  PQ_PF.VerificaEquiparacaoEstoque(True);
  //////////////////////////////////////////////////////////////////////////////
  QrProdutos.Close;
  PainelDados2.Visible := False;
  LocCod(QrBalancosPeriodo.Value, QrBalancosPeriodo.Value);
end;
}

end.

