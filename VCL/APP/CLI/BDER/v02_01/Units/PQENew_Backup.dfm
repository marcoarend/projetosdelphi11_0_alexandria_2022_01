object FmPQENew: TFmPQENew
  Left = 377
  Top = 210
  Caption = 'QUI-ENTRA-002 :: Entrada de Uso e Consumo'
  ClientHeight = 524
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 91
    Width = 1008
    Height = 202
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label17: TLabel
      Left = 64
      Top = 0
      Width = 65
      Height = 13
      Caption = 'Data entrada:'
    end
    object Label11: TLabel
      Left = 184
      Top = 0
      Width = 67
      Height = 13
      Caption = 'Data emiss'#227'o:'
    end
    object Label6: TLabel
      Left = 300
      Top = 0
      Width = 57
      Height = 13
      Caption = 'Fornecedor:'
    end
    object Label19: TLabel
      Left = 510
      Top = 80
      Width = 75
      Height = 13
      Caption = 'Transportadora:'
    end
    object Label5: TLabel
      Left = 8
      Top = 80
      Width = 61
      Height = 13
      Caption = 'Qtde l'#237'quido:'
    end
    object Label3: TLabel
      Left = 905
      Top = 39
      Width = 53
      Height = 13
      Caption = '$ Total NF:'
    end
    object LaCI: TLabel
      Left = 8
      Top = 39
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object Label7: TLabel
      Left = 102
      Top = 80
      Width = 43
      Height = 13
      Caption = 'kg Bruto:'
    end
    object Label10: TLabel
      Left = 11
      Top = 0
      Width = 36
      Height = 13
      Caption = 'Pedido:'
    end
    object LaSitPedido: TLabel
      Left = 108
      Top = 0
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object Label108: TLabel
      Left = 196
      Top = 80
      Width = 139
      Height = 13
      Caption = 'Indicador do tipo de frete [F4]'
      FocusControl = EdIND_FRT
    end
    object Label16: TLabel
      Left = 796
      Top = 0
      Width = 171
      Height = 13
      Caption = 'Indicador do tipo de pagamento [F4]'
      FocusControl = EdIND_PGTO
    end
    object Label18: TLabel
      Left = 353
      Top = 39
      Width = 54
      Height = 13
      Caption = '$ Produtos:'
    end
    object Label20: TLabel
      Left = 445
      Top = 39
      Width = 74
      Height = 13
      Caption = '$ Frete na NFe:'
    end
    object Label22: TLabel
      Left = 537
      Top = 39
      Width = 46
      Height = 13
      Caption = '$ Seguro:'
    end
    object Label28: TLabel
      Left = 629
      Top = 39
      Width = 75
      Height = 13
      Caption = '$ Desp. Acess.:'
    end
    object Label29: TLabel
      Left = 721
      Top = 39
      Width = 58
      Height = 13
      Caption = '$ Desconto:'
    end
    object Label30: TLabel
      Left = 813
      Top = 39
      Width = 25
      Height = 13
      Caption = '$ IPI:'
    end
    object CkTipoNF: TdmkCheckBox
      Left = 8
      Top = 184
      Width = 162
      Height = 17
      Caption = 'Nota Fiscal Eletr'#244'nica (NF-e)'
      Checked = True
      State = cbChecked
      TabOrder = 23
      OnClick = CkTipoNFClick
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object TPEntrada: TdmkEditDateTimePicker
      Left = 65
      Top = 16
      Width = 113
      Height = 21
      Date = 40060.000000000000000000
      Time = 0.661368159722769600
      TabOrder = 1
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object TPEmissao: TdmkEditDateTimePicker
      Left = 184
      Top = 16
      Width = 112
      Height = 21
      Date = 40060.000000000000000000
      Time = 0.661368159722769600
      TabOrder = 2
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdFornecedor: TdmkEditCB
      Left = 300
      Top = 16
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBFornecedor
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBFornecedor: TdmkDBLookupComboBox
      Left = 360
      Top = 16
      Width = 433
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOME'
      ListSource = DsFornecedores
      TabOrder = 4
      dmkEditCB = EdFornecedor
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object CBCI: TdmkDBLookupComboBox
      Left = 64
      Top = 55
      Width = 284
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOME'
      ListSource = DsCI
      TabOrder = 8
      dmkEditCB = EdCI
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCI: TdmkEditCB
      Left = 8
      Top = 55
      Width = 55
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdCIChange
      DBLookupComboBox = CBCI
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBTransportador: TdmkDBLookupComboBox
      Left = 569
      Top = 96
      Width = 424
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOME'
      ListSource = DsTransportadores
      TabOrder = 21
      dmkEditCB = EdTransportador
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdTransportador: TdmkEditCB
      Left = 510
      Top = 96
      Width = 55
      Height = 21
      Alignment = taRightJustify
      TabOrder = 20
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBTransportador
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdkgLiquido: TdmkEdit
      Left = 8
      Top = 96
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 16
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdValorNF: TdmkEdit
      Left = 905
      Top = 55
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 15
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdkgBruto: TdmkEdit
      Left = 102
      Top = 96
      Width = 91
      Height = 21
      Alignment = taRightJustify
      TabOrder = 17
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 118
      Width = 993
      Height = 59
      Caption = 'Conhecimento do Frete: e restitui'#231#227'o de tributos:'
      TabOrder = 22
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 989
        Height = 42
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 461
          Height = 42
          Align = alLeft
          BevelOuter = bvNone
          Caption = 'Panel4'
          TabOrder = 0
          object Label27: TLabel
            Left = 4
            Top = 0
            Width = 124
            Height = 13
            Caption = 'Chave de acesso da CTe:'
          end
          object Label21: TLabel
            Left = 312
            Top = 0
            Width = 38
            Height = 13
            Caption = 'Modelo:'
          end
          object Label23: TLabel
            Left = 355
            Top = 0
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object Label25: TLabel
            Left = 388
            Top = 0
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
          end
          object EdCte_Id: TdmkEdit
            Left = 3
            Top = 14
            Width = 306
            Height = 21
            MaxLength = 44
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdrefNFeExit
          end
          object EdCTe_mod: TdmkEdit
            Left = 312
            Top = 14
            Width = 39
            Height = 21
            Alignment = taRightJustify
            MaxLength = 2
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCTe_serie: TdmkEdit
            Left = 355
            Top = 14
            Width = 31
            Height = 21
            Alignment = taRightJustify
            MaxLength = 3
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdConhecim: TdmkEdit
            Left = 388
            Top = 14
            Width = 70
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 9
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object Panel5: TPanel
          Left = 461
          Top = 0
          Width = 528
          Height = 42
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Label8: TLabel
            Left = 4
            Top = 0
            Width = 62
            Height = 13
            Caption = '$ Frete CT-e:'
          end
          object Label14: TLabel
            Left = 102
            Top = 0
            Width = 75
            Height = 13
            Caption = 'ICMS % e valor:'
          end
          object Label24: TLabel
            Left = 243
            Top = 0
            Width = 66
            Height = 13
            Caption = 'PIS % e valor:'
            Enabled = False
          end
          object Label26: TLabel
            Left = 385
            Top = 0
            Width = 88
            Height = 13
            Caption = 'COFINS % e valor:'
            Enabled = False
          end
          object EdFrete: TdmkEdit
            Left = 4
            Top = 14
            Width = 96
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdFreteChange
          end
          object EdFreteRpICMS: TdmkEdit
            Left = 102
            Top = 14
            Width = 48
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdFreteRpICMSChange
          end
          object EdFreteRvICMS: TdmkEdit
            Left = 153
            Top = 14
            Width = 88
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdFreteRpPIS: TdmkEdit
            Left = 243
            Top = 14
            Width = 48
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdFreteRpPISChange
          end
          object EdFreteRvPIS: TdmkEdit
            Left = 294
            Top = 14
            Width = 88
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdFreteRpCOFINS: TdmkEdit
            Left = 385
            Top = 14
            Width = 48
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdFreteRpCOFINSChange
          end
          object EdFreteRvCOFINS: TdmkEdit
            Left = 436
            Top = 14
            Width = 88
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
    object EdPedido: TdmkEdit
      Left = 9
      Top = 16
      Width = 52
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdIND_FRT: TdmkEdit
      Left = 196
      Top = 96
      Width = 21
      Height = 21
      Alignment = taRightJustify
      TabOrder = 18
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '9'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'IND_PGTO'
      UpdCampo = 'IND_PGTO'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdIND_FRTChange
      OnKeyDown = EdIND_FRTKeyDown
    end
    object EdIND_FRT_TXT: TdmkEdit
      Left = 218
      Top = 96
      Width = 291
      Height = 21
      ReadOnly = True
      TabOrder = 19
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdIND_PGTO: TdmkEdit
      Left = 796
      Top = 16
      Width = 21
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '9'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'IND_PGTO'
      UpdCampo = 'IND_PGTO'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdIND_PGTOChange
      OnKeyDown = EdIND_PGTOKeyDown
    end
    object EdIND_PGTO_TXT: TdmkEdit
      Left = 818
      Top = 16
      Width = 179
      Height = 21
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdICMSTot_vProd: TdmkEdit
      Left = 353
      Top = 55
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = CalculaTotalNF
    end
    object EdICMSTot_vFrete: TdmkEdit
      Left = 445
      Top = 55
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = CalculaTotalNF
    end
    object EdICMSTot_vSeg: TdmkEdit
      Left = 537
      Top = 55
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = CalculaTotalNF
    end
    object EdICMSTot_vOutro: TdmkEdit
      Left = 629
      Top = 55
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 12
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = CalculaTotalNF
    end
    object EdICMSTot_vDesc: TdmkEdit
      Left = 721
      Top = 55
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = CalculaTotalNF
    end
    object EdIPI: TdmkEdit
      Left = 813
      Top = 55
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = CalculaTotalNF
    end
  end
  object PnNFs: TPanel
    Left = 0
    Top = 293
    Width = 1008
    Height = 124
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object LarefNFe: TLabel
      Left = 8
      Top = 2
      Width = 150
      Height = 13
      Caption = 'Chave de acesso da NF-e (VP):'
    end
    object LaModNF: TLabel
      Left = 292
      Top = 1
      Width = 38
      Height = 13
      Caption = 'Modelo:'
    end
    object LaSerie: TLabel
      Left = 335
      Top = 1
      Width = 27
      Height = 13
      Caption = 'S'#233'rie:'
    end
    object LaNF: TLabel
      Left = 368
      Top = 2
      Width = 34
      Height = 13
      Caption = 'NF VP:'
    end
    object LaNFe_CC: TLabel
      Left = 8
      Top = 42
      Width = 150
      Height = 13
      Caption = 'Chave de acesso da NF-e (CC):'
    end
    object LamodCC: TLabel
      Left = 292
      Top = 41
      Width = 38
      Height = 13
      Caption = 'Modelo:'
    end
    object LaSerCC: TLabel
      Left = 335
      Top = 41
      Width = 27
      Height = 13
      Caption = 'S'#233'rie:'
    end
    object LaNF_CC: TLabel
      Left = 367
      Top = 40
      Width = 34
      Height = 13
      Caption = 'NF CC:'
    end
    object SbNF_VP: TSpeedButton
      Left = 436
      Top = 14
      Width = 25
      Height = 25
      Caption = '<>'
      OnClick = SbNF_VPClick
    end
    object LaNFe_RP: TLabel
      Left = 8
      Top = 82
      Width = 151
      Height = 13
      Caption = 'Chave de acesso da NF-e (RP):'
    end
    object LamodRP: TLabel
      Left = 292
      Top = 81
      Width = 38
      Height = 13
      Caption = 'Modelo:'
    end
    object LaSerRP: TLabel
      Left = 335
      Top = 81
      Width = 27
      Height = 13
      Caption = 'S'#233'rie:'
    end
    object LaNF_RP: TLabel
      Left = 372
      Top = 80
      Width = 35
      Height = 13
      Caption = 'NF RP:'
    end
    object Label4: TLabel
      Left = 468
      Top = 1
      Width = 125
      Height = 13
      Caption = 'Atrelamento com XML BD:'
    end
    object Label9: TLabel
      Left = 468
      Top = 41
      Width = 125
      Height = 13
      Caption = 'Atrelamento com XML BD:'
    end
    object Label15: TLabel
      Left = 468
      Top = 81
      Width = 125
      Height = 13
      Caption = 'Atrelamento com XML BD:'
      Enabled = False
    end
    object SbNF_CC: TSpeedButton
      Left = 436
      Top = 54
      Width = 25
      Height = 25
      Caption = '<>'
      OnClick = SbNF_CCClick
    end
    object SpeedButton2: TSpeedButton
      Left = 436
      Top = 94
      Width = 25
      Height = 25
      Caption = '<>'
      Enabled = False
      OnClick = SpeedButton2Click
    end
    object EdrefNFe: TdmkEdit
      Left = 8
      Top = 16
      Width = 280
      Height = 21
      MaxLength = 44
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdrefNFeExit
      OnRedefinido = EdrefNFeRedefinido
    end
    object EdModNF: TdmkEdit
      Left = 292
      Top = 16
      Width = 39
      Height = 21
      Alignment = taRightJustify
      MaxLength = 2
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSerie: TdmkEdit
      Left = 335
      Top = 16
      Width = 31
      Height = 21
      Alignment = taRightJustify
      MaxLength = 3
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNF: TdmkEdit
      Left = 368
      Top = 16
      Width = 66
      Height = 21
      Alignment = taRightJustify
      MaxLength = 9
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFe_CC: TdmkEdit
      Left = 8
      Top = 56
      Width = 280
      Height = 21
      MaxLength = 44
      TabOrder = 7
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdNFe_CCExit
      OnRedefinido = EdNFe_CCRedefinido
    end
    object EdmodCC: TdmkEdit
      Left = 292
      Top = 56
      Width = 39
      Height = 21
      Alignment = taRightJustify
      MaxLength = 2
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSerCC: TdmkEdit
      Left = 335
      Top = 56
      Width = 31
      Height = 21
      Alignment = taRightJustify
      MaxLength = 3
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNF_CC: TdmkEdit
      Left = 367
      Top = 56
      Width = 67
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      QryCampo = 'NF_CC'
      UpdCampo = 'NF_CC'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFe_RP: TdmkEdit
      Left = 8
      Top = 96
      Width = 280
      Height = 21
      MaxLength = 44
      TabOrder = 14
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdNFe_RPExit
      OnRedefinido = EdNFe_RPRedefinido
    end
    object EdmodRP: TdmkEdit
      Left = 292
      Top = 96
      Width = 39
      Height = 21
      Alignment = taRightJustify
      MaxLength = 2
      TabOrder = 15
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSerRP: TdmkEdit
      Left = 335
      Top = 96
      Width = 31
      Height = 21
      Alignment = taRightJustify
      MaxLength = 3
      TabOrder = 16
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNF_RP: TdmkEdit
      Left = 368
      Top = 96
      Width = 67
      Height = 21
      Alignment = taRightJustify
      TabOrder = 17
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      QryCampo = 'NFRef'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFVP_StaLnk: TdmkEdit
      Left = 468
      Top = 16
      Width = 28
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      MaxLength = 9
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFVP_FatID: TdmkEdit
      Left = 500
      Top = 16
      Width = 39
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      MaxLength = 2
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFVP_FatNum: TdmkEdit
      Left = 543
      Top = 16
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      MaxLength = 3
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFCC_StaLnk: TdmkEdit
      Left = 468
      Top = 56
      Width = 28
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      MaxLength = 9
      ReadOnly = True
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFCC_FatID: TdmkEdit
      Left = 500
      Top = 56
      Width = 39
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      MaxLength = 2
      ReadOnly = True
      TabOrder = 12
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFCC_FatNum: TdmkEdit
      Left = 543
      Top = 56
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      MaxLength = 3
      ReadOnly = True
      TabOrder = 13
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFRP_StaLnk: TdmkEdit
      Left = 468
      Top = 96
      Width = 28
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      MaxLength = 9
      ReadOnly = True
      TabOrder = 18
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFRP_FatID: TdmkEdit
      Left = 500
      Top = 96
      Width = 39
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      MaxLength = 2
      ReadOnly = True
      TabOrder = 19
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFRP_FatNum: TdmkEdit
      Left = 543
      Top = 96
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      MaxLength = 3
      ReadOnly = True
      TabOrder = 20
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnInativo: TPanel
    Left = 0
    Top = 417
    Width = 1008
    Height = 80
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 6
    Visible = False
    object Label2: TLabel
      Left = 270
      Top = 0
      Width = 57
      Height = 13
      Caption = '%RICMS F.:'
      Enabled = False
      Visible = False
    end
    object Label12: TLabel
      Left = 202
      Top = -1
      Width = 52
      Height = 13
      Caption = 'Valor juros:'
      Enabled = False
      Visible = False
    end
    object Label1: TLabel
      Left = 409
      Top = 0
      Width = 62
      Height = 13
      Caption = '%RICMS NF:'
      Enabled = False
      Visible = False
    end
    object Label13: TLabel
      Left = 342
      Top = 0
      Width = 54
      Height = 13
      Caption = '%ICMS NF:'
      Enabled = False
      Visible = False
    end
    object EdRICMSF: TdmkEdit
      Left = 270
      Top = 16
      Width = 68
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdJuros: TdmkEdit
      Left = 202
      Top = 16
      Width = 64
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdRICMS: TdmkEdit
      Left = 409
      Top = 16
      Width = 68
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdICMS: TdmkEdit
      Left = 342
      Top = 16
      Width = 60
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GB_R: TGroupBox
      Left = 961
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 914
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 317
        Height = 31
        Caption = 'Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 317
        Height = 31
        Caption = 'Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 317
        Height = 31
        Caption = 'Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 461
    Width = 1008
    Height = 63
    Align = alBottom
    TabOrder = 3
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 46
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 861
        Top = 0
        Width = 143
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 14
          Top = 3
          Width = 119
          Height = 39
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 7
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 417
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 5
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 945
        Height = 16
        Caption = 
          'VP: NFe de venda do fornecedor para a empresa ou cliente.       ' +
          '  RP:  Simples remessa do formecedor do cliente.        CC: NFe ' +
          'do cliente interno para a empresa.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 945
        Height = 16
        Caption = 
          'VP: NFe de venda do fornecedor para a empresa ou cliente.       ' +
          '  RP:  Simples remessa do formecedor do cliente.        CC: NFe ' +
          'do cliente interno para a empresa.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 1008
    Height = 44
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object LaEmpresa: TLabel
      Left = 64
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object LaLancamento: TLabel
      Left = 8
      Top = 4
      Width = 40
      Height = 13
      Caption = 'N'#250'mero:'
    end
    object EdEmpresa: TdmkEditCB
      Left = 64
      Top = 20
      Width = 55
      Height = 20
      TabStop = False
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Empresa'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEmpresaChange
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 119
      Top = 20
      Width = 878
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 2
      TabStop = False
      dmkEditCB = EdEmpresa
      QryCampo = 'Empresa'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCodigo: TdmkEdit
      Left = 8
      Top = 20
      Width = 52
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object QrFornecedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME'
      'FROM entidades'
      'WHERE Fornece2='#39'V'#39
      'ORDER BY Nome')
    Left = 12
    Top = 4
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedoresNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrFornecedoresTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrFornecedoresCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrFornecedoresCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 40
    Top = 4
  end
  object QrTransportadores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME'
      'FROM entidades'
      'WHERE Fornece3='#39'V'#39
      'ORDER BY Nome')
    Left = 68
    Top = 4
    object QrTransportadoresTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrTransportadoresCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTransportadoresCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTransportadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportadoresNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
  end
  object DsTransportadores: TDataSource
    DataSet = QrTransportadores
    Left = 96
    Top = 4
  end
  object QrItens: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *FROM pqpedits'
      'WHERE Codigo=:P0')
    Left = 464
    Top = 4
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItensCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pqpedits.Codigo'
    end
    object QrItensControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'DBMBWET.pqpedits.Controle'
    end
    object QrItensConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'DBMBWET.pqpedits.Conta'
    end
    object QrItensInsumo: TIntegerField
      FieldName = 'Insumo'
      Origin = 'DBMBWET.pqpedits.Insumo'
    end
    object QrItensVolumes: TIntegerField
      FieldName = 'Volumes'
      Origin = 'DBMBWET.pqpedits.Volumes'
    end
    object QrItensPesoVB: TFloatField
      FieldName = 'PesoVB'
      Origin = 'DBMBWET.pqpedits.PesoVB'
    end
    object QrItensPesoVL: TFloatField
      FieldName = 'PesoVL'
      Origin = 'DBMBWET.pqpedits.PesoVL'
    end
    object QrItensValorItem: TFloatField
      FieldName = 'ValorItem'
      Origin = 'DBMBWET.pqpedits.ValorItem'
    end
    object QrItensIPI: TFloatField
      FieldName = 'IPI'
      Origin = 'DBMBWET.pqpedits.IPI'
    end
    object QrItensTotalCusto: TFloatField
      FieldName = 'TotalCusto'
      Origin = 'DBMBWET.pqpedits.TotalCusto'
    end
    object QrItensTotalPeso: TFloatField
      FieldName = 'TotalPeso'
      Origin = 'DBMBWET.pqpedits.TotalPeso'
    end
    object QrItensPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'DBMBWET.pqpedits.Preco'
    end
    object QrItensICMS: TFloatField
      FieldName = 'ICMS'
      Origin = 'DBMBWET.pqpedits.ICMS'
    end
    object QrItensEntrada: TFloatField
      FieldName = 'Entrada'
      Origin = 'DBMBWET.pqpedits.Entrada'
    end
    object QrItensPrazo: TWideStringField
      FieldName = 'Prazo'
      Origin = 'DBMBWET.pqpedits.Prazo'
      Size = 128
    end
    object QrItensCFin: TFloatField
      FieldName = 'CFin'
      Origin = 'DBMBWET.pqpedits.CFin'
    end
  end
  object QrInsIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'INSERT INTO pqeits SET'
      'Insumo=:P0,'
      'Volumes=:P1,'
      'PesoVB=:P2,'
      'PesoVL=:P3,'
      'ValorItem=:P4,'
      'IPI=:P5,'
      'RIPI=:P6,'
      'TotalCusto=:P7,'
      'TotalPeso=:P8,'
      'CFin=:P9,'
      'Codigo=:P10,'
      'Conta=:11'
      ''
      '')
    Left = 520
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P9'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P10'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = '11'
        ParamType = ptUnknown
      end>
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME'
      'FROM entidades'
      'WHERE Cliente2='#39'V'#39
      'ORDER BY Nome')
    Left = 408
    Top = 4
    object QrCITipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCICNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrCICPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 408
    Top = 52
  end
  object Qr00_NFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, IDCtrl,'
      'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk,'
      'ide_mod, ide_serie, ide_nNF  '
      'FROM nfecaba '
      'WHERE FatID=53 '
      'AND Empresa=-11 '
      'AND ide_mod>0 '
      'AND ide_serie>0 '
      'AND ide_nNF>0 ')
    Left = 642
    Top = 361
    object Qr00_NFeCabAFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object Qr00_NFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object Qr00_NFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object Qr00_NFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object Qr00_NFeCabAAtrelaFatID: TIntegerField
      FieldName = 'AtrelaFatID'
      Required = True
    end
    object Qr00_NFeCabAAtrelaFatNum: TIntegerField
      FieldName = 'AtrelaFatNum'
      Required = True
    end
    object Qr00_NFeCabAAtrelaStaLnk: TSmallintField
      FieldName = 'AtrelaStaLnk'
      Required = True
    end
    object Qr00_NFeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
      Required = True
    end
    object Qr00_NFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      Required = True
    end
    object Qr00_NFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Required = True
    end
    object Qr00_NFeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
  object QrPediVdaIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pedivdaits')
    Left = 768
    Top = 385
    object QrPediVdaItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediVdaItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediVdaItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrPediVdaItsPrecoO: TFloatField
      FieldName = 'PrecoO'
      Required = True
    end
    object QrPediVdaItsPrecoR: TFloatField
      FieldName = 'PrecoR'
      Required = True
    end
    object QrPediVdaItsQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrPediVdaItsQuantC: TFloatField
      FieldName = 'QuantC'
      Required = True
    end
    object QrPediVdaItsQuantV: TFloatField
      FieldName = 'QuantV'
      Required = True
    end
    object QrPediVdaItsValBru: TFloatField
      FieldName = 'ValBru'
      Required = True
    end
    object QrPediVdaItsDescoP: TFloatField
      FieldName = 'DescoP'
      Required = True
    end
    object QrPediVdaItsDescoV: TFloatField
      FieldName = 'DescoV'
      Required = True
    end
    object QrPediVdaItsValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrPediVdaItsPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrPediVdaItsMedidaC: TFloatField
      FieldName = 'MedidaC'
      Required = True
    end
    object QrPediVdaItsMedidaL: TFloatField
      FieldName = 'MedidaL'
      Required = True
    end
    object QrPediVdaItsMedidaA: TFloatField
      FieldName = 'MedidaA'
      Required = True
    end
    object QrPediVdaItsMedidaE: TFloatField
      FieldName = 'MedidaE'
      Required = True
    end
    object QrPediVdaItsPercCustom: TFloatField
      FieldName = 'PercCustom'
      Required = True
    end
    object QrPediVdaItsCustomizad: TSmallintField
      FieldName = 'Customizad'
      Required = True
    end
    object QrPediVdaItsInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrPediVdaItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrPediVdaItsReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 11
    end
    object QrPediVdaItsMulti: TIntegerField
      FieldName = 'Multi'
      Required = True
    end
    object QrPediVdaItsvProd: TFloatField
      FieldName = 'vProd'
      Required = True
    end
    object QrPediVdaItsvFrete: TFloatField
      FieldName = 'vFrete'
      Required = True
    end
    object QrPediVdaItsvSeg: TFloatField
      FieldName = 'vSeg'
      Required = True
    end
    object QrPediVdaItsvOutro: TFloatField
      FieldName = 'vOutro'
      Required = True
    end
    object QrPediVdaItsvDesc: TFloatField
      FieldName = 'vDesc'
      Required = True
    end
    object QrPediVdaItsvBC: TFloatField
      FieldName = 'vBC'
      Required = True
    end
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ICMSRec_pAliq, PISRec_pAliq,  '
      'COFINSRec_pAliq  '
      'FROM gragru1 '
      'WHERE Nivel1=1 ')
    Left = 584
    Top = 4
    object QrGraGru1ICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrGraGru1PISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrGraGru1COFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
  end
end
