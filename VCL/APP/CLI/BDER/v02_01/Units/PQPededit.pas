unit PQPedEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnInternalConsts, UnMsgInt,
  ExtCtrls, ZCF2, UnInternalConsts2, ResIntStrings, UMySQLModule, DmkDAC_PF,
  mySQLDbTables, UnMyLinguas, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkLabel, dmkImage, UnDmkEnums;

type
  TFmPQPedEdit = class(TForm)
    QrPQ1: TmySQLQuery;
    DsPQ1: TDataSource;
    QrPQ2: TmySQLQuery;
    QrUpdPQ: TmySQLQuery;
    QrPQ3: TmySQLQuery;
    QrPQ3Codigo: TIntegerField;
    QrPQ3Nome: TWideStringField;
    QrPQ3IQ: TIntegerField;
    QrPQ3Preco: TFloatField;
    QrPQ3ICMS: TFloatField;
    QrPQ3Frete: TFloatField;
    QrPQ3IPI: TFloatField;
    QrPQ3RIPI: TFloatField;
    QrPQ3Prazo: TWideStringField;
    QrPQ3Corante: TWideStringField;
    QrPQ3Pigmento: TWideStringField;
    QrPQ3Desperdicio: TFloatField;
    QrPQ3ClienteI: TIntegerField;
    QrPQ3EstqPeso: TFloatField;
    QrPQ3Valor: TFloatField;
    QrPQ3EstqValor: TFloatField;
    QrPQ3EstqFin: TFloatField;
    QrPQ3RICMS: TFloatField;
    QrPQ3RICMSF: TFloatField;
    QrPQ3Custo: TFloatField;
    QrPQ3MinEstq: TFloatField;
    QrPQ3Ativo: TWideStringField;
    QrPQ3Lk: TIntegerField;
    QrPQ3PBEmb: TFloatField;
    QrPQ3PLEmb: TFloatField;
    QrPQ3EstSegur: TIntegerField;
    QrPQ3SAkg: TFloatField;
    QrPQ3SAVlr: TFloatField;
    QrPQ3SetorMaster: TIntegerField;
    QrPQ3CFin: TFloatField;
    QrMaxCod: TmySQLQuery;
    QrConsumo: TmySQLQuery;
    Query2: TmySQLQuery;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    QrConsumoPeso: TFloatField;
    Panel1: TPanel;
    PainelTitulo2: TPanel;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    PainelDados: TPanel;
    Label1: TLabel;
    Label14: TLabel;
    Label4: TLabel;
    Label15: TLabel;
    Label3: TLabel;
    Label16: TLabel;
    Label7: TLabel;
    Label5: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label10: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    EdProduto: TdmkEditCB;
    CBProduto: TdmkDBLookupComboBox;
    EdVolumes: TdmkEdit;
    EdKgBruto: TdmkEdit;
    EdkgLiq: TdmkEdit;
    BtCalcula: TBitBtn;
    EdICMS: TdmkEdit;
    EdIPI: TdmkEdit;
    EdCFin: TdmkEdit;
    EdValorkg: TdmkEdit;
    EdTotalkgBruto: TdmkEdit;
    EdTotalkgLiq: TdmkEdit;
    EdTotalItem: TdmkEdit;
    EdPrazo: TEdit;
    EdConsumodd: TdmkEdit;
    ImgTipo: TdmkLabel;
    QrMaxCodCodigo: TIntegerField;
    QrMaxCodConta: TIntegerField;
    QrMaxCodControle: TIntegerField;
    QrPQ3Dolar: TFloatField;
    QrPQ3Euro: TFloatField;
    QrPQ3Moeda: TSmallintField;
    QrPQ3Reais: TFloatField;
    RGMoeda: TRadioGroup;
    Label13: TLabel;
    EdIdx: TdmkEdit;
    QrPQ1NOMEPQ: TWideStringField;
    QrPQ1Controle: TIntegerField;
    QrPQ1IQ: TIntegerField;
    QrPQ1PQ: TIntegerField;
    QrPQ1CI: TIntegerField;
    QrPQ1Prazo: TWideStringField;
    QrPQ1Lk: TIntegerField;
    QrPQ1DataCad: TDateField;
    QrPQ1DataAlt: TDateField;
    QrPQ1UserCad: TIntegerField;
    QrPQ1UserAlt: TIntegerField;
    QrPQ1DDESTQ: TFloatField;
    QrPQ1Peso: TFloatField;
    QrPQ2NOMEEMBLAGEM: TWideStringField;
    QrPQ2Controle: TIntegerField;
    QrPQ2IQ: TIntegerField;
    QrPQ2PQ: TIntegerField;
    QrPQ2CI: TIntegerField;
    QrPQ2Embalagem: TIntegerField;
    QrPQ2DataCot: TDateField;
    QrPQ2Preco: TFloatField;
    QrPQ2Moeda: TSmallintField;
    QrPQ2Frete: TFloatField;
    QrPQ2ICMS: TFloatField;
    QrPQ2IPI: TFloatField;
    QrPQ2PIS: TFloatField;
    QrPQ2COFINS: TFloatField;
    QrPQ2RICMS: TFloatField;
    QrPQ2RIPI: TFloatField;
    QrPQ2RPIS: TFloatField;
    QrPQ2RCOFINS: TFloatField;
    QrPQ2Custo: TFloatField;
    QrPQ2Lk: TIntegerField;
    QrPQ2DataCad: TDateField;
    QrPQ2DataAlt: TDateField;
    QrPQ2UserCad: TIntegerField;
    QrPQ2UserAlt: TIntegerField;
    Label17: TLabel;
    QrPQ2Bruto: TFloatField;
    QrPQ2Liqui: TFloatField;
    QrIQ1: TmySQLQuery;
    QrIQ1Prazo: TWideStringField;
    QrMaxCodMaxCodigo: TIntegerField;
    Label12: TLabel;
    EdPreco: TdmkEdit;
    EdDolar: TdmkEdit;
    Label9: TLabel;
    Label11: TLabel;
    EdEuro: TdmkEdit;
    Label20: TLabel;
    EdReais: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    dmkImage1: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtSoma: TBitBtn;
    BtRefresh: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdVolumesChange(Sender: TObject);
    procedure EdKgBrutoChange(Sender: TObject);
    procedure EdkgLiqChange(Sender: TObject);
    procedure EdIPIChange(Sender: TObject);
    procedure EdRIPIChange(Sender: TObject);
    procedure EdPrecoChange(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure EdCFinChange(Sender: TObject);
    procedure BtSomaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdVolumesExit(Sender: TObject);
    procedure EdKgBrutoExit(Sender: TObject);
    procedure EdkgLiqExit(Sender: TObject);
    procedure EdICMSExit(Sender: TObject);
    procedure EdCFinExit(Sender: TObject);
    procedure QrPQ1AfterScroll(DataSet: TDataSet);
    procedure BtCalculaClick(Sender: TObject);
    procedure QrPQ1CalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure EdIdxExit(Sender: TObject);
    procedure RGMoedaClick(Sender: TObject);
    procedure EdIPIExit(Sender: TObject);
    procedure EdDolarKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEuroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIdxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure CalculaOnEdit;
    procedure LocalizaPQ2(PQ, CI, IQ: Integer);
    //function CalculaPrecoInicial(Valor : Double) : Double;
  public
    { Public declarations }
  end;

var
  FmPQPedEdit: TFmPQPedEdit;

implementation

uses UnMyObjects, PQPed, PQPedEditCalc, Module, PQx, MyDBCheck;

{$R *.DFM}

procedure TFmPQPedEdit.LocalizaPQ2(PQ, CI, IQ: Integer);
begin
  QrPQ2.Close;
  QrPQ2.Params[00].AsInteger := PQ;
  QrPQ2.Params[01].AsInteger := CI;
  QrPQ2.Params[02].AsInteger := IQ;
  UnDmkDAC_PF.AbreQuery(QrPQ2, Dmod.MyDB);
  //
  QrIQ1.Close;
  QrIQ1.Params[00].AsInteger := PQ;
  QrIQ1.Params[01].AsInteger := CI;
  QrIQ1.Params[02].AsInteger := IQ;
  UnDmkDAC_PF.AbreQuery(QrIQ1, Dmod.MyDB);
  //
  RGMoeda.ItemIndex := QrPQ2Moeda.Value;
  {case QrPQ2Moeda.Value of
    0:} EdPreco.Text := Geral.FFT(QrPQ2Preco.Value, 4, siPositivo);
    {1: EdDolar.Text := Geral.FFT(QrPQ2Preco.Value, 4, siPositivo);
    2: EdEuro .Text := Geral.FFT(QrPQ2Preco.Value, 4, siPositivo);
    3: EdIdx  .Text := Geral.FFT(QrPQ2Preco.Value, 4, siPositivo);
  end;}
  EdIPI.Text     := Geral.FFT(QrPQ2IPI.Value,   2, siPositivo);
  EdICMS.Text    := Geral.FFT(QrPQ2ICMS.Value,  2, siPositivo);
  EdKgBruto.Text := Geral.FFT(QrPQ2Bruto.Value, 3, siPositivo);
  EdkgLiq.Text   := Geral.FFT(QrPQ2Liqui.Value, 3, siPositivo);
  EdPrazo.Text   := QrIQ1Prazo.Value;
end;

procedure TFmPQPedEdit.CalculaOnEdit;
var
  Volumes : Integer;
  TotalkgLiq, kgLiq, kgBruto, ValorItem, IPI : Double;
  ICMS, Preco, PrecoICMS, ValorKg, CFin : Double;
begin
  Preco := Geral.DMV(EdPreco.Text);
  case RGMoeda.ItemIndex of
    1: Preco := Geral.DMV(EdDolar.Text) * Preco;
    2: Preco := Geral.DMV(EdEuro .Text) * Preco;
    3: Preco := Geral.DMV(EdIdx  .Text) * Preco;
  end;
  Volumes := Geral.IMV(EdVolumes.Text);
  kgLiq := Geral.DMV(EdkgLiq.Text);
  kgBruto := Geral.DMV(EdKgBruto.Text);
  IPI := 1+(Geral.DMV(EdIPI.Text)/100);
  CFin := 1+(Geral.DMV(EdCFin.Text)/100);
  ICMS := 1-(Geral.DMV(EdICMS.Text)/100);

  TotalkgLiq := Volumes*kgLiq;

  PrecoICMS := (Round(Preco / ICMS * 100))/100;

  ValorKg := PrecoICMS*IPI*CFin;
  ValorItem := ValorKg * TotalKgLiq;

  EdReais.Text        := Geral.TFT(FloatToStr(Preco)          , 4, siPositivo);
  EdValorKg.Text      := Geral.TFT(FloatToStr(ValorKg)        , 4, siPositivo);
  EdTotalkgBruto.Text := Geral.TFT(FloatToStr(Volumes*kgBruto), 3, siPositivo);
  EdTotalkgLiq.Text   := Geral.TFT(FloatToStr(TotalkgLiq)     , 3, siPositivo);
  EdTotalItem.Text    := Geral.TFT(FloatToStr(ValorItem)      , 2, siPositivo);
end;

{
function TFmPQPedEdit.CalculaPrecoInicial(Valor : Double) : Double;
var
  IPI, ICMS, CFin, PrecoICMS : Double;
begin
  IPI := 1+(Geral.DMV(EdIPI.Text)/100);
  CFin := 1+(Geral.DMV(EdCFin.Text)/100);
  ICMS := 1-(Geral.DMV(EdICMS.Text)/100);
  PrecoICMS := (Round(Valor*ICMS*100))/100;
  Result := (PrecoICMS / IPI / CFin);
end;
}

procedure TFmPQPedEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQPedEdit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrPQ1.Params[00].AsInteger := FmPQPed.QrPQPedCliInt.Value;
  QrPQ1.Params[01].AsInteger := FmPQPed.QrPQPedFornece.Value;
  UnDmkDAC_PF.AbreQuery(QrPQ1, Dmod.MyDB);
end;

procedure TFmPQPedEdit.BtConfirmaClick(Sender: TObject);
var
  Cursor : TCursor;
  ValorItem, IPI, ICMS, kgLiq, kgBruto, Preco, ValorKg, TotalKgLiquido, CFin,
  Reais, Dolar, Euro, Idx: Double;
  Conta, Produto, Codigo, Volumes, Controle, Consumodd : Integer;
  Ini, Fim: String;
  Consumokg: Double;
  MaxCod, MaxCta, MaxCtr: Integer;
begin
  MaxCod := 0;
  MaxCta := 0;
  MaxCtr := 0;
  CalculaOnEdit;
  Produto := CBProduto.KeyValue;
  if Produto < 1 then
  begin
    ShowMessage(FIN_MSG_DEFPRODUTO);
    EdProduto.SetFocus;
    Exit;
  end;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    Codigo := FmPQPed.QrPQPedCodigo.Value;
    Conta := EdCodigo.ValueVariant;

    Preco := Geral.DMV(EdPreco.Text);
    Volumes := Geral.IMV(EdVolumes.Text);
    kgLiq := Geral.DMV(EdkgLiq.Text);
    kgBruto := Geral.DMV(EdKgBruto.Text);
    ValorKg := Geral.DMV(EdValorKg.Text);
    TotalKgLiquido := Geral.DMV(EdTotalkgLiq.Text);
    ValorItem := Geral.DMV(EdTotalItem.Text);
    IPI := Geral.DMV(EdIPI.Text);
    ICMS := Geral.DMV(EdICMS.Text);
    CFin := Geral.DMV(EdCFin.Text);
    Reais := Geral.DMV(EdReais.Text);
    Dolar := Geral.DMV(EdDolar.Text);
    Euro  := Geral.DMV(EdEuro .Text);
    Idx   := Geral.DMV(EdIdx  .Text);

    Controle := 0;
    Consumodd := Geral.IMV(EdConsumodd.Text);
    Ini := FormatDateTime(VAR_FORMATDATE, FmPQPed.QrPQPedData.Value-Consumodd);
    Fim := FormatDateTime(VAR_FORMATDATE, FmPQPed.QrPQPedData.Value);
    QrConsumo.Close;
    QrConsumo.Params[0].AsString := Ini;
    QrConsumo.Params[1].AsString := Fim;
    QrConsumo.Params[2].AsInteger := Produto;
    QrConsumo.Params[3].AsInteger := FmPQPed.QrPQPedCliInt.Value;
    UnDmkDAC_PF.AbreQuery(QrConsumo, Dmod.MyDB);
    Consumokg := QrConsumoPeso.Value;
    QrConsumo.Close;

    Dmod.QrUpdW.Close;
    Dmod.QrUpdW.SQL.Clear;
    if ImgTipo.SQLType = stIns then
    begin
      Dmod.QrUpdW.SQL.Add('UPDATE PQPedIts SET Conta=Conta+1');
      Dmod.QrUpdW.SQL.Add('WHERE Conta>=:P0 AND Codigo=:P1');
      Dmod.QrUpdW.Params[0].AsInteger := Conta;
      Dmod.QrUpdW.Params[1].AsInteger := Codigo;
      Dmod.QrUpdW.ExecSQL;
      //
      QrMaxCod.Close;
      QrMaxCod.Params[0].AsInteger := Produto;
      UnDmkDAC_PF.AbreQuery(QrMaxCod, Dmod.MyDB);
      QrMaxCod.Last;
      MaxCod := QrMaxCodCodigo.Value;
      MaxCta := QrMaxCodConta.Value;
      MaxCtr := QrMaxCodControle.Value;
      QrMaxCod.Close;
      //
      QrConsumo.Close;
      QrConsumo.Params[0].AsString := Ini;
      QrConsumo.Params[1].AsString := Fim;
      QrConsumo.Params[2].AsInteger := Produto;
      UnDmkDAC_PF.AbreQuery(QrConsumo, Dmod.MyDB);
      //
      Dmod.QrUpdW.SQL.Clear;
      Dmod.QrUpdW.SQL.Add('INSERT INTO PQPedIts SET');
      Dmod.QrUpdW.SQL.Add('Insumo=:P0, Preco=:P1, Volumes=:P2, PesoVB=:P3, ');
      Dmod.QrUpdW.SQL.Add('PesoVL=:P4, ValorItem=:P5, IPI=:P6, ICMS=:P7, ');
      Dmod.QrUpdW.SQL.Add('TotalCusto=:P8, TotalPeso=:P9, Prazo=:P10,');
      Dmod.QrUpdW.SQL.Add('CFin=:P11, ddEstq=:P12, Estqkg=:P13, Moeda=:P14, ');
      Dmod.QrUpdW.SQL.Add('Reais=:P15, Dolar=:P16, Euro=:P17, Idx=:P18 ');
      Dmod.QrUpdW.SQL.Add(',Codigo=:Px, Conta=:Py');
      Dmod.QrUpdW.SQL.Add(',UCCodigo=:Pv, UCConta=:Pw, UCControle=:Pz');
      Dmod.QrUpdW.SQL.Add(',Consumodd=:Pe1, Consumokg=:Pe2');
    end else begin
      Controle := FmPQPed.QrPQPedItsControle.Value;
      Dmod.QrUpdW.SQL.Add('UPDATE PQPedIts SET');
      Dmod.QrUpdW.SQL.Add('Insumo=:P0, Preco=:P1, Volumes=:P2, PesoVB=:P3, ');
      Dmod.QrUpdW.SQL.Add('PesoVL=:P4, ValorItem=:P5, IPI=:P6, ICMS=:P7, ');
      Dmod.QrUpdW.SQL.Add('TotalCusto=:P8, TotalPeso=:P9, Prazo=:P10,');
      Dmod.QrUpdW.SQL.Add('CFin=:P11, ddEstq=:P12, Estqkg=:P13, Moeda=:P14, ');
      Dmod.QrUpdW.SQL.Add('Reais=:P15, Dolar=:P16, Euro=:P17, Idx=:P18 ');
      Dmod.QrUpdW.SQL.Add('WHERE Codigo=:Px AND Conta=:Py AND Controle=:Pz');
    end;
    Dmod.QrUpdW.Params[00].AsInteger := Produto;
    Dmod.QrUpdW.Params[01].AsFloat   := Preco;
    Dmod.QrUpdW.Params[02].AsInteger := Volumes;
    Dmod.QrUpdW.Params[03].AsFloat   := kgBruto;
    Dmod.QrUpdW.Params[04].AsFloat   := kgLiq;
    Dmod.QrUpdW.Params[05].AsFloat   := ValorKg;
    Dmod.QrUpdW.Params[06].AsFloat   := IPI;
    Dmod.QrUpdW.Params[07].AsFloat   := ICMS;
    Dmod.QrUpdW.Params[08].AsFloat   := ValorItem;
    Dmod.QrUpdW.Params[09].AsFloat   := TotalKgLiquido;
    Dmod.QrUpdW.Params[10].AsString  := EdPrazo.Text;
    Dmod.QrUpdW.Params[11].AsFloat   := CFin;
    Dmod.QrUpdW.Params[12].AsFloat   := QrPQ1DDESTQ.Value;
    Dmod.QrUpdW.Params[13].AsFloat   := QrPQ1Peso.Value;//QrPQ1EstqPeso.Value;
    Dmod.QrUpdW.Params[14].AsInteger := RGMoeda.ItemIndex;
    Dmod.QrUpdW.Params[15].AsFloat   := Reais;
    Dmod.QrUpdW.Params[16].AsFloat   := Dolar;
    Dmod.QrUpdW.Params[17].AsFloat   := Euro;
    Dmod.QrUpdW.Params[18].AsFloat   := Idx;
    Dmod.QrUpdW.Params[19].AsInteger := Codigo;
    Dmod.QrUpdW.Params[20].AsInteger := Conta;
    if ImgTipo.SQLType = stIns then
    begin
      Dmod.QrUpdW.Params[21].AsInteger := MaxCod;
      Dmod.QrUpdW.Params[22].AsInteger := MaxCta;
      Dmod.QrUpdW.Params[23].AsInteger := MaxCtr;
      Dmod.QrUpdW.Params[24].AsInteger := Consumodd;
      Dmod.QrUpdW.Params[25].AsFloat   := Consumokg;
    end else Dmod.QrUpdW.Params[21].AsInteger := Controle;
    Dmod.QrUpdW.ExecSQL;
  except
    Screen.Cursor := Cursor;
    Exit;
  end;
  Close;
end;

procedure TFmPQPedEdit.EdVolumesChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmPQPedEdit.EdKgBrutoChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmPQPedEdit.EdkgLiqChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmPQPedEdit.EdIPIChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmPQPedEdit.EdRIPIChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmPQPedEdit.EdPrecoChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmPQPedEdit.BtRefreshClick(Sender: TObject);
{var
  Valor, RICMS, RICMSF, RIPI, Frete : Double;
  Produto, Cliente: Integer;}
begin
  {Produto := Geral.IMV(EdProduto.Text);
  // Parei Aqui
  Cliente := FmPQPed.QrPQPedCliInt.Value;//-1;

  QrPQ3.Close;
  QrPQ3.Params[0].AsInteger := Produto;
  UnDmkDAC_PF.AbreQuery(QrPQ3, Dmod.MyDB);
  RICMS := QrPQ3RICMS.Value;
  RICMSF := QrPQ3RICMSF.Value;
  RIPI := QrPQ3RIPI.Value;
  Frete := QrPQ3Frete.Value;
  QrPQ3.Close;

  Valor := ???.CalculaValorPQ(Geral.DMV(EdPreco.Text),
  Geral.DMV(EdDolar.Text), Geral.DMV(EdEuro.Text),Geral.DMV(EdReais.Text),
  Geral.DMV(EdICMS.Text), RICMS, RICMSF,
  Geral.DMV(EdIPI.Text), RIPI, Frete,
  Geral.DMV(EdCFin.Text), Dmod.QrCambiosDolar.Value,
  Dmod.QrCambiosEuro.Value, RGMoeda.ItemIndex, 0);

  QrUpdPQ.Params[0].AsFloat := Geral.DMV(EdReais.Text);
  QrUpdPQ.Params[1].AsFloat := Geral.DMV(EdKgBruto.Text);
  QrUpdPQ.Params[2].AsFloat := Geral.DMV(EdkgLiq.Text);
  QrUpdPQ.Params[3].AsFloat := Geral.DMV(EdICMS.Text);
  QrUpdPQ.Params[4].AsFloat := Geral.DMV(EdIPI.Text);
  QrUpdPQ.Params[5].AsFloat := Valor;
  QrUpdPQ.Params[6].AsString := EdPrazo.Text;
  QrUpdPQ.Params[7].AsFloat := Geral.DMV(EdCFin.Text);
  QrUpdPQ.Params[8].AsInteger := Produto;
  QrUpdPQ.ExecSQL;
  if UnPQx.AtualizaEstoquePQ(Cliente, Produto, aeMsg, CO_VAZIO) = False then
  ShowMessage('Insumo atualizado com sucesso'); }
end;

procedure TFmPQPedEdit.EdCFinChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmPQPedEdit.BtSomaClick(Sender: TObject);
{var
  Num : String;
  Valor : Double;}
begin
  {Num := InputBox('Pedido de Insumos Qu�micos','Informe o "Valor kg" final.', EdValorkg.Text);
  try
    Valor := Geral.DMV(Num);
    if Valor > 0 then EdReais.Text :=
      Geral.TFT(FloatToStr(CalculaPrecoInicial(Valor)), 4, siPositivo);
  except
    on EConvertError do
        if Num <> '' then ShowMessage('"'+Num+SMLA_NUMEROINVALIDO);
  end;}
end;

procedure TFmPQPedEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQPedEdit.EdVolumesExit(Sender: TObject);
begin
  EdVolumes.Text := Geral.TFT(EdVolumes.Text, 0, siPositivo);
end;

procedure TFmPQPedEdit.EdKgBrutoExit(Sender: TObject);
begin
  EdKgBruto.Text := Geral.TFT(EdKgBruto.Text, 3, siPositivo);
end;

procedure TFmPQPedEdit.EdkgLiqExit(Sender: TObject);
begin
  EdkgLiq.Text := Geral.TFT(EdkgLiq.Text, 3, siPositivo);
end;

procedure TFmPQPedEdit.EdICMSExit(Sender: TObject);
begin
  EdICMS.Text := Geral.TFT(EdICMS.Text, 2, siPositivo);
end;

procedure TFmPQPedEdit.EdCFinExit(Sender: TObject);
begin
  EdCFin.Text := Geral.TFT(EdCFin.Text, 4, siPositivo);
end;

procedure TFmPQPedEdit.QrPQ1AfterScroll(DataSet: TDataSet);
begin
  LocalizaPQ2(QrPQ1PQ.Value, QrPQ1CI.Value, QrPQ1IQ.Value);
end;

procedure TFmPQPedEdit.BtCalculaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPQPedEditCalc, FmPQPedEditCalc, afmoNegarComAviso) then
  begin
    FmPQPedEditCalc.EdVolumes.Text := EdVolumes.Text;
    FmPQPedEditCalc.EdkgBruto.Text := EdkgBruto.Text;
    FmPQPedEditCalc.EdkgLiq.Text := EdkgLiq.Text;
    FmPQPedEditCalc.EdTotalkgLiq.Text := EdTotalkgLiq.Text;
    FmPQPedEditCalc.EdTotalkgBruto.Text := EdTotalkgBruto.Text;
    FmPQPedEditCalc.ShowModal;
    FmPQPedEditCalc.Destroy;
  end;
end;

procedure TFmPQPedEdit.QrPQ1CalcFields(DataSet: TDataSet);
begin
  {if QrPQ1MinEstq.Value > 0 then
    QrPQ1DDESTQ.Value :=
    (QrPQ1EstqPeso.Value /
    QrPQ1MinEstq.Value) *
    QrPQ1EstSegur.Value
  else
    QrPQ1DDESTQ.Value := 0;}
end;

procedure TFmPQPedEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQPedEdit.EdIdxExit(Sender: TObject);
begin
  EdIdx.Text := Geral.TFT(EdIdx.Text, 4, siPositivo);
end;

procedure TFmPQPedEdit.RGMoedaClick(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmPQPedEdit.EdIPIExit(Sender: TObject);
begin
  EdIPI.Text := Geral.TFT(EdIPI.Text, 2, siPositivo);
end;

procedure TFmPQPedEdit.EdDolarKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdDolar.ValueVariant := VAR_CAMBIO_USD;
end;

procedure TFmPQPedEdit.EdEuroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdEuro.ValueVariant := VAR_CAMBIO_EUR;
end;

procedure TFmPQPedEdit.EdIdxKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdIdx.ValueVariant := VAR_CAMBIO_IDX;
end;

end.
