object FmFluxosItsMul: TFmFluxosItsMul
  Left = 339
  Top = 185
  Caption = 'FLU-PRODU-004 :: Opera'#231#227'o de Fluxo de Produ'#231#227'o - Edi'#231#227'o m'#250'ltipla'
  ClientHeight = 629
  ClientWidth = 925
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 925
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 877
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 829
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 600
        Height = 32
        Caption = 'Opera'#231#227'o de Fluxo de Produ'#231#227'o - Edi'#231#227'o m'#250'ltipla'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 600
        Height = 32
        Caption = 'Opera'#231#227'o de Fluxo de Produ'#231#227'o - Edi'#231#227'o m'#250'ltipla'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 600
        Height = 32
        Caption = 'Opera'#231#227'o de Fluxo de Produ'#231#227'o - Edi'#231#227'o m'#250'ltipla'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 925
    Height = 451
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 925
      Height = 451
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 925
        Height = 451
        Align = alClient
        TabOrder = 0
        object DBGFluxosIts: TDBGrid
          Left = 2
          Top = 15
          Width = 921
          Height = 434
          Align = alClient
          DataSource = DsFluxosIts
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = DBGFluxosItsKeyDown
          Columns = <
            item
              Expanded = False
              FieldName = 'Operacao'
              Title.Caption = 'Oper. ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEOPERACAO'
              Title.Caption = '* Opera'#231#227'o - Descri'#231#227'o'
              Width = 161
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Acao1'
              Title.Caption = '* Controle 1'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Acao2'
              Title.Caption = '* Controle 2'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Acao3'
              Title.Caption = '* Controle 3'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Acao4'
              Title.Caption = '* Controle 4'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ordem'
              ReadOnly = True
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              ReadOnly = True
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 499
    Width = 925
    Height = 60
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 921
      Height = 43
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 418
        Height = 16
        Caption = 
          'ATEN'#199#195'O: Os itens editados neste local ser'#227'o salvos automaticame' +
          'nte'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 418
        Height = 16
        Caption = 
          'ATEN'#199#195'O: Os itens editados neste local ser'#227'o salvos automaticame' +
          'nte'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso3: TLabel
        Left = 13
        Top = 18
        Width = 547
        Height = 16
        Caption = 
          'Na coluna "Opera'#231#227'o - Descri'#231#227'o" => F4 para abrir cadastro de op' +
          'era'#231#245'es / F7 para pesquisar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso4: TLabel
        Left = 12
        Top = 17
        Width = 547
        Height = 16
        Caption = 
          'Na coluna "Opera'#231#227'o - Descri'#231#227'o" => F4 para abrir cadastro de op' +
          'era'#231#245'es / F7 para pesquisar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 925
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 779
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 777
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtReordena: TBitBtn
        Tag = 73
        Left = 10
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Reordena'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtReordenaClick
      end
    end
  end
  object TbFluxosIts: TmySQLTable
    Database = Dmod.MyDB
    BeforePost = TbFluxosItsBeforePost
    OnCalcFields = TbFluxosItsCalcFields
    SortFieldNames = 'Ordem'
    IndexFieldNames = 'Controle'
    TableName = 'fluxosits'
    Left = 204
    Top = 144
    object TbFluxosItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbFluxosItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object TbFluxosItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object TbFluxosItsOperacao: TIntegerField
      FieldName = 'Operacao'
    end
    object TbFluxosItsNOMEOPERACAO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMEOPERACAO'
      LookupDataSet = QrOperacoes
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Operacao'
      Size = 30
      Lookup = True
    end
    object TbFluxosItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbFluxosItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbFluxosItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbFluxosItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbFluxosItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbFluxosItsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object TbFluxosItsAcao1: TWideStringField
      FieldName = 'Acao1'
      Size = 100
    end
    object TbFluxosItsAcao2: TWideStringField
      FieldName = 'Acao2'
      Size = 100
    end
    object TbFluxosItsAcao3: TWideStringField
      FieldName = 'Acao3'
      Size = 100
    end
    object TbFluxosItsAcao4: TWideStringField
      FieldName = 'Acao4'
      Size = 100
    end
  end
  object DsFluxosIts: TDataSource
    DataSet = TbFluxosIts
    Left = 232
    Top = 144
  end
  object QrOperacoes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM operacoes'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 392
    Top = 113
    object QrOperacoesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOperacoesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
end
