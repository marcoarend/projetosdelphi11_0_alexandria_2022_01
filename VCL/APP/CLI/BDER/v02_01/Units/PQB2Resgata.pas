unit PQB2Resgata;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBGridZTO,
  mySQLDbTables;

type
  THackDBGrid = class(TDBGrid);
  TFmPQB2Resgata = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Panel6: TPanel;
    EdCliIntCodi: TdmkEdit;
    EdCliIntNome: TdmkEdit;
    DBGPQBResgata: TdmkDBGridZTO;
    DsPQB2Resgata: TDataSource;
    QrPQB2Resgata: TmySQLQuery;
    QrPQB2ResgataInsumo: TIntegerField;
    QrPQB2ResgataPeso: TFloatField;
    QrPQB2ResgataValor: TFloatField;
    QrPQB2ResgataPreco: TFloatField;
    QrPQB2ResgataNO_PQ: TWideStringField;
    QrPQB2ResgataAtivo: TSmallintField;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGPQBResgataCellClick(Column: TColumn);
    procedure DBGPQBResgataDblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FPQBResgata, FCampo, FDataX: String;
    FPeriodo, FCliInt, FEmpresa: Integer;
    //
    procedure ReopenPQB2Resgata(Insumo: Integer);
  end;

  var
  FmPQB2Resgata: TFmPQB2Resgata;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, GetValor, UMySQLModule,
  UnPQ_PF;

{$R *.DFM}

procedure TFmPQB2Resgata.BtOKClick(Sender: TObject);
const
  Tipo = VAR_FATID_0000;
  Unitario = False;
var
  Conta, CliOrig, CliDest, Insumo, OrigemCodi, OriCodi, OriCtrl, OriTipo: Integer;
  Peso, Valor, OrigemCtrl: Double;
  DataX, DtCorrApo: String;
begin
  Screen.Cursor := crHourGlass;
  try
    DBGPQBResgata.Enabled := False;
    QrPQB2Resgata.DisableControls;
    //
    DtCorrApo := Geral.FDT(0, 1);
    DataX := FDataX;
    CliOrig := FCliInt;
    CliDest := FCliInt;
    QrPQB2Resgata.First;
    PB1.Position := 0;
    PB1.Max := QrPQB2Resgata.RecordCount;
    while not QrPQB2Resgata.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //
      Insumo := QrPQB2ResgataInsumo.Value;
      Peso   := QrPQB2ResgataPeso.Value;
      Valor  := QrPQB2ResgataValor.Value;
      //
      OrigemCodi := FPeriodo;
      //
      if (Peso <> 0) or (Valor <> 0) then
      begin
        OrigemCtrl := UMyMod.BuscaEmLivreY_Double(
          Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
        //
        //if
{
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'p q x', False, [
        'DataX', 'CliOrig', 'CliDest',
        'Insumo', 'Peso', 'Valor'(*,
        'Retorno', 'StqMovIts', 'RetQtd'*)], [
        'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
        DataX, CliOrig, CliDest,
        Insumo, Peso, Valor(*,
        Retorno, StqMovIts, RetQtd*)], [
        OrigemCodi, OrigemCtrl, Tipo], False);
}
        OriCodi := OrigemCodi;
        OriCtrl := Trunc(OrigemCtrl);
        OriTipo := Tipo;
        PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
          OriCtrl, OriTipo, Unitario, DtCorrApo, FEmpresa);
      end;
      //
      QrPQB2Resgata.Next;
    end;
    PQ_PF.VerificaEquiparacaoEstoque(True);
    Screen.Cursor := crDefault;
    Close;
  finally
    Screen.Cursor := crDefault;
    //
    DBGPQBResgata.Enabled := True;
    QrPQB2Resgata.EnableControls;
  end;
end;

procedure TFmPQB2Resgata.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQB2Resgata.DBGPQBResgataCellClick(Column: TColumn);
begin
  FCampo := Column.FieldName;
end;

procedure TFmPQB2Resgata.DBGPQBResgataDblClick(Sender: TObject);
var
  Preco, Valor, Peso: Double;
  ResVar: Variant;
  Atualiza: Boolean;
  Insumo: Integer;
begin
  Atualiza := False;
  Preco := QrPQB2ResgataPreco.Value;
  Valor := QrPQB2ResgataValor.Value;
  Peso  := QrPQB2ResgataPeso.Value;
  //
  if FCampo = 'Preco' then
  begin
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    Preco, 6, 0, '', '', True, 'Novo Pre�o', 'Informe o pre�o: ',
    0, ResVar) then
    begin
      Preco := Geral.DMV(ResVar);
      Valor := Peso * Preco;
      Atualiza := True;
    end;
  end;
  if FCampo = 'Peso' then
  begin
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    Peso, 3, 0, '', '', True, 'Nova Quantidade', 'Informe a quantidade: ',
    0, ResVar) then
    begin
      Peso := Geral.DMV(ResVar);
      Valor := Peso * Preco;
      Atualiza := True;
    end;
  end;
  if FCampo = 'Valor' then
  begin
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    Valor, 2, 0, '', '', True, 'Novo Valor', 'Informe o valor: ',
    0, ResVar) then
    begin
      Valor := Geral.DMV(ResVar);
      if Peso = 0 then
        Preco := 0
      else
        Preco := Valor / Peso;
      Atualiza := True;
    end;
  end;
  if Atualiza then
  begin
    Insumo := QrPQB2ResgataInsumo.Value;
    //
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FPQBResgata, False, [
    'Peso', 'Valor', 'Preco'], [
    'Insumo'], [
    Peso, Valor, Preco], [
    Insumo], False) then
      ReopenPQB2Resgata(Insumo);
  end;
end;

procedure TFmPQB2Resgata.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQB2Resgata.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPQB2Resgata.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQB2Resgata.ReopenPQB2Resgata(Insumo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQB2Resgata, DModG.MyPID_DB, [
  'SELECT * FROM ' + FPQBResgata,
  '']);
  //
  QrPQB2Resgata.Locate('Insumo', Insumo, []);
end;

end.
