unit PQExclPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, dmkEditCB, DBCtrls,
  dmkDBLookupComboBox, DB, mySQLDbTables, Grids, DBGrids, dmkDBGridDAC,
  ComCtrls, dmkEditDateTimePicker, dmkCheckBox, dmkCheckGroup, UnDmkProcFunc,
  dmkGeral, dmkImage, UnDmkEnums, DmkDAC_PF, frxClass, frxDBSet,
  UnProjGroup_Consts, UnAppPF;

type
  TFmPQExclPesq = class(TForm)
    Panel1: TPanel;
    DsFormulas: TDataSource;
    QrFormulas: TmySQLQuery;
    QrFormulasNumero: TIntegerField;
    QrFormulasNome: TWideStringField;
    Panel3: TPanel;
    Label1: TLabel;
    EdReceita: TdmkEditCB;
    CBReceita: TdmkDBLookupComboBox;
    dmkDBGridDAC1: TdmkDBGridDAC;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqDataEmis: TDateTimeField;
    QrPesqStatus: TSmallintField;
    QrPesqNumero: TIntegerField;
    QrPesqNOMECI: TWideStringField;
    QrPesqNOMESETOR: TWideStringField;
    QrPesqTecnico: TWideStringField;
    QrPesqNOME: TWideStringField;
    QrPesqClienteI: TIntegerField;
    QrPesqTipificacao: TSmallintField;
    QrPesqTempoR: TIntegerField;
    QrPesqTempoP: TIntegerField;
    QrPesqSetor: TSmallintField;
    QrPesqTipific: TSmallintField;
    QrPesqEspessura: TWideStringField;
    QrPesqDefPeca: TWideStringField;
    QrPesqPeso: TFloatField;
    QrPesqCusto: TFloatField;
    QrPesqQtde: TFloatField;
    QrPesqAreaM2: TFloatField;
    QrPesqFulao: TWideStringField;
    QrPesqObs: TWideStringField;
    QrPesqLk: TIntegerField;
    QrPesqDataCad: TDateField;
    QrPesqDataAlt: TDateField;
    QrPesqUserCad: TIntegerField;
    QrPesqUserAlt: TIntegerField;
    QrPesqAlterWeb: TSmallintField;
    QrPesqAtivo: TSmallintField;
    GroupBox1: TGroupBox;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label3: TLabel;
    QrPesqSTATUS_TXT: TWideStringField;
    EdSetor: TdmkEditCB;
    Label5: TLabel;
    CBSetor: TdmkDBLookupComboBox;
    QrSetores: TmySQLQuery;
    DsSetores: TDataSource;
    CGStatus: TdmkCheckGroup;
    CkStatus: TdmkCheckBox;
    GroupBox2: TGroupBox;
    EdFulao: TdmkEdit;
    Label8: TLabel;
    GroupBox3: TGroupBox;
    EdPesoMin: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    EdPesoMax: TdmkEdit;
    CkFulao: TCheckBox;
    CkCarga: TCheckBox;
    GroupBox4: TGroupBox;
    EdQtdeMin: TdmkEdit;
    Label9: TLabel;
    Label10: TLabel;
    EdQtdeMax: TdmkEdit;
    CkQtde: TCheckBox;
    QrPQ: TmySQLQuery;
    DsPQ: TDataSource;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    GroupBox5: TGroupBox;
    EdPQ: TdmkEditCB;
    Label11: TLabel;
    CBPQ: TdmkDBLookupComboBox;
    RGTipoProd: TRadioGroup;
    CkPesoZero: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtPesquisa: TBitBtn;
    BtLocaliza: TBitBtn;
    Label4: TLabel;
    LaQtde: TLabel;
    BtSaida: TBitBtn;
    frxQUI_RECEI_009_A: TfrxReport;
    frxDsPesq: TfrxDBDataset;
    BtImprime: TBitBtn;
    GroupBox6: TGroupBox;
    Panel2: TPanel;
    Label12: TLabel;
    EdMarca: TdmkEdit;
    RGPeriodo: TRadioGroup;
    QrPesqNO_Grupo: TWideStringField;
    QrPesqGrupo: TIntegerField;
    CGFinalidrec: TdmkCheckGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure TPDataIniChange(Sender: TObject);
    procedure TPDataFimChange(Sender: TObject);
    procedure EdReceitaChange(Sender: TObject);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure QrPesqCalcFields(DataSet: TDataSet);
    procedure EdPesoMinExit(Sender: TObject);
    procedure EdQtdeMinExit(Sender: TObject);
    procedure CkStatusClick(Sender: TObject);
    procedure CGStatusClick(Sender: TObject);
    procedure CkFulaoClick(Sender: TObject);
    procedure EdFulaoChange(Sender: TObject);
    procedure CkCargaClick(Sender: TObject);
    procedure EdPesoMinChange(Sender: TObject);
    procedure EdPesoMaxChange(Sender: TObject);
    procedure CkQtdeClick(Sender: TObject);
    procedure EdQtdeMinChange(Sender: TObject);
    procedure EdQtdeMaxChange(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
    procedure dmkDBGridDAC1DblClick(Sender: TObject);
    procedure RGTipoProdClick(Sender: TObject);
    procedure CkPesoZeroClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxQUI_RECEI_009_AGetValue(const VarName: string;
      var Value: Variant);
    procedure EdReceitaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBReceitaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure FechaPesquisa;
  public
    { Public declarations }
  end;

  var
  FmPQExclPesq: TFmPQExclPesq;

implementation

uses UnMyObjects, UMySQLModule, Module, PQxExcl;

{$R *.DFM}

procedure TFmPQExclPesq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrFormulas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSetores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  TPDataIni.Date := Date - 30;
  TPDataFim.Date := Date;
  CGStatus.Value := 7;
  CGFinalidrec.Value := 3;
end;

procedure TFmPQExclPesq.BtImprimeClick(Sender: TObject);
begin
  QrPesq.DisableControls;
  try
    MyObjects.frxMostra(frxQUI_RECEI_009_A, 'Pesquisa de pesagens');
  finally
    QrPesq.EnableControls;
  end;
end;

procedure TFmPQExclPesq.BtLocalizaClick(Sender: TObject);
begin
  FmPQxExcl.EdPesagem.Text := IntToStr(QrPesqCodigo.Value);
  FmPQxExcl.ReopenEmit(True);
  Close;
end;

procedure TFmPQExclPesq.BtPesquisaClick(Sender: TObject);
const
  sProcName = 'TFmPQExclPesq.BtPesquisaClick()';
  function ItensStatusPesagem: String;
  begin
    Result := 'AND em.Status in (';
    Result := Result + FormatFloat('0',
      dmkPF.IntInConjunto2Def(1, CGStatus.Value, 0, -1000)) + ',';
    Result := Result + FormatFloat('0',
      dmkPF.IntInConjunto2Def(2, CGStatus.Value, 1, -1000)) + ',';
    Result := Result + FormatFloat('0',
      dmkPF.IntInConjunto2Def(4, CGStatus.Value, 2, -1000));
    Result := Result + ')';
  end;
var
  Qry: TmySQLQuery;
  sVMIs, Campo, SQL_FinalidRec: String;
begin
  SQL_FinalidRec := EmptyStr;
  case CGFinalidrec.Value of
    1: SQL_FinalidRec := 'AND fo.Numero<0';
    2: SQL_FinalidRec := 'AND fo.Numero>0';
    else SQL_FinalidRec := '';
  end;
  QrPesq.Close;
  QrPesq.SQL.Clear;

  QrPesq.SQL.Add('SELECT DISTINCT em.*, fo.Grupo, gr.Nome NO_Grupo');
  QrPesq.SQL.Add('FROM emit em');
  QrPesq.SQL.Add('LEFT JOIN formulas fo ON fo.Numero=em.Numero');
  QrPesq.SQL.Add('LEFT JOIN formulasgru gr ON gr.Codigo=fo.Grupo');
  if EdPQ.ValueVariant > 0 then
  begin
    case RGTipoProd.ItemIndex of
      0: QrPesq.SQL.Add('LEFT JOIN emitits ei ON ei.Codigo=em.Codigo');
      1: QrPesq.SQL.Add('LEFT JOIN pqx px ON px.OrigemCodi=em.Codigo AND px.Tipo=110');
    end;
  end;
  // ini 2023-07-08
  case RGPeriodo.ItemIndex of
    0: Campo := 'em.DtaBaixa';
    1: Campo := 'em.DataEmis';
    else
    begin
      Geral.MB_Erro('Campo n�o implementado: ' + RGPeriodo.Items[RGPeriodo.ItemIndex] +
      ' em ' + sProcName);
      Campo := 'em.DtaBaixa';
    end;
  end;
  //QrPesq.SQL.Add(dmkPF.SQL_Periodo('WHERE em.DataEmis ',
  QrPesq.SQL.Add(dmkPF.SQL_Periodo('WHERE ' + Campo + ' ',
    TPDataIni.Date, TPDataFim.Date, True, True));
  // fim 2023-07-08
  if EdReceita.ValueVariant > 0 then
    QrPesq.SQL.Add('AND em.Numero = ' + FormatFloat('0', EdReceita.ValueVariant));
  if EdSetor.ValueVariant > 0 then
    QrPesq.SQL.Add('AND fo.Setor = ' + FormatFloat('0', EdSetor.ValueVariant));
  if EdPQ.ValueVariant > 0 then
  begin
    case RGTipoProd.ItemIndex of
      0: QrPesq.SQL.Add('AND ei.Produto = ' + FormatFloat('0', EdPQ.ValueVariant));
      1:
      begin
        QrPesq.SQL.Add('AND px.Insumo = ' + FormatFloat('0', EdPQ.ValueVariant));
        if CkPesoZero.Checked then
          QrPesq.SQL.Add('AND px.Peso <> 0');
      end;
    end;
  end;
  if CkStatus.Checked then
    QrPesq.SQL.Add(ItensStatusPesagem);
  if CkFulao.Checked then
    QrPesq.SQL.Add('AND em.Fulao = "' + EdFulao.Text + '"');
  if CkCarga.Checked then
    QrPesq.SQL.Add('AND em.Peso BETWEEN "' +
      dmkPF.FFP(EdPesoMin.ValueVariant, 3) + '" AND "' +
      dmkPF.FFP(EdPesoMax.ValueVariant, 3) + '"');
  if CkQtde.Checked then
    QrPesq.SQL.Add('AND em.Qtde BETWEEN "' +
      dmkPF.FFP(EdQtdeMin.ValueVariant, 3) + '" AND "' +
      dmkPF.FFP(EdQtdeMax.ValueVariant, 3) + '"');
  // inicio 2018-08-22
  if Trim(EdMarca.Text) <> '' then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
(*
      'SELECT Controle ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE Marca = "' + EdMarca.Text + '" ',
*)
      'SELECT DISTINCT emc.Codigo',
      'FROM emitcus emc',
      'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=emc.VSMovIts',
      'WHERE vmi.Marca="' + EdMarca.Text + '" ',
      '']);
      sVMIs := '';
      Qry.First;
      while not Qry.Eof do
      begin
        if Qry.RecNo = 1 then
          sVMIs := Geral.FF0(Qry.FieldByName('Codigo').Value)
        else
          sVMIs := sVMIs + ', ' + Geral.FF0(Qry.FieldByName('Codigo').Value);
        //
        Qry.Next;
      end;
      if sVMIs <> '' then
        QrPesq.SQL.Add('AND em.Codigo IN (' + sVMIs + ')');
    finally
      Qry.Free;
    end;
  end;
  // fim 2018-08-22
  //
  QrPesq.SQL.Add(SQL_FinalidRec);
  QrPesq.SQL.Add('ORDER BY gr.Nome, fo.Grupo, em.DataEmis DESC, em.Codigo DESC');
  UMyMod.AbreQuery(QrPesq, Dmod.MyDB, 'TFmPQExclPesq.BtPesquisaClick()');
  //Geral.MB_Teste(QrPesq.SQL.Text);
end;

procedure TFmPQExclPesq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQExclPesq.CBReceitaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdReceita.ValueVariant := AppPF.ObtemNumeroDeCODIF(EdReceita.ValueVariant);
  end;
end;

procedure TFmPQExclPesq.CGStatusClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmPQExclPesq.CkCargaClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmPQExclPesq.CkFulaoClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmPQExclPesq.CkPesoZeroClick(Sender: TObject);
begin
  QrPesq.Close;
end;

procedure TFmPQExclPesq.CkQtdeClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmPQExclPesq.CkStatusClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmPQExclPesq.dmkDBGridDAC1DblClick(Sender: TObject);
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
    BtLocalizaClick(Self);
end;

procedure TFmPQExclPesq.EdFulaoChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmPQExclPesq.EdPesoMaxChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmPQExclPesq.EdPesoMinChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmPQExclPesq.EdPesoMinExit(Sender: TObject);
begin
  EdPesoMax.ValueVariant := EdPesoMin.ValueVariant;
end;

procedure TFmPQExclPesq.EdQtdeMaxChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmPQExclPesq.EdQtdeMinChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmPQExclPesq.EdQtdeMinExit(Sender: TObject);
begin
  EdQtdeMax.ValueVariant := EdQtdeMin.ValueVariant;
end;

procedure TFmPQExclPesq.EdReceitaChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmPQExclPesq.EdReceitaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdReceita.ValueVariant := AppPF.ObtemNumeroDeCODIF(EdReceita.ValueVariant);
  end;
end;

procedure TFmPQExclPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQExclPesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQExclPesq.frxQUI_RECEI_009_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VAR_REC_NOME') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBReceita.Text, EdReceita.ValueVariant, 'TODOS')
  else if AnsiCompareText(VarName, 'VAR_SET_NOME') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBSetor.Text, EdSetor.ValueVariant, 'TODOS')
  else if AnsiCompareText(VarName, 'VAR_PERIODO') = 0 then
    Value := dmkPF.PeriodoImp(TPDataIni.Date, TPDataFim.Date, 0, 0, True, True, False, False, '', '')
end;

procedure TFmPQExclPesq.QrPesqAfterOpen(DataSet: TDataSet);
begin
  LaQtde.Caption := FormatFloat('0', QrPesq.RecordCount);
  BtLocaliza.Enabled := QrPesq.RecordCount > 0;
  BtImprime.Enabled := QrPesq.RecordCount > 0;
end;

procedure TFmPQExclPesq.QrPesqBeforeClose(DataSet: TDataSet);
begin
  LaQtde.Caption := '0';
  BtLocaliza.Enabled := False;
end;

procedure TFmPQExclPesq.QrPesqCalcFields(DataSet: TDataSet);
begin
  case QrPesqStatus.Value of
    0: QrPesqSTATUS_TXT.Value := 'Custo n�o rateado';
    1: QrPesqSTATUS_TXT.Value := 'Custo rateado';
    2: QrPesqSTATUS_TXT.Value := 'Custo alterado';
    else QrPesqSTATUS_TXT.Value := '** Status desconhecido **';
  end;
end;

procedure TFmPQExclPesq.RGTipoProdClick(Sender: TObject);
begin
  QrPesq.Close;
  CkPesoZero.Checked := RGTipoProd.ItemIndex = 1;
  CkPesoZero.Enabled := RGTipoProd.ItemIndex = 1;
end;

procedure TFmPQExclPesq.FechaPesquisa;
begin
  QrPesq.Close;
end;

procedure TFmPQExclPesq.TPDataFimChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmPQExclPesq.TPDataIniChange(Sender: TObject);
begin
  FechaPesquisa;
end;

end.

