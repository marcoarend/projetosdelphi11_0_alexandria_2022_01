object FmPQEEdit: TFmPQEEdit
  Left = 368
  Top = 178
  Caption = 'QUI-ENTRA-003 :: Item de Entrada de Uso e Consumo'
  ClientHeight = 356
  ClientWidth = 566
  Color = clBtnFace
  Constraints.MaxHeight = 536
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 566
    Height = 200
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 201
    object Painel1: TPanel
      Left = 0
      Top = 0
      Width = 566
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 29
        Height = 13
        Caption = 'Linha:'
      end
      object EdCodigo: TdmkEdit
        Left = 112
        Top = 12
        Width = 57
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object PainelDados: TPanel
      Left = 0
      Top = 41
      Width = 566
      Height = 184
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 76
        Top = 8
        Width = 131
        Height = 13
        Caption = 'Produto: [F4 - Mostra todos]'
      end
      object Label14: TLabel
        Left = 76
        Top = 48
        Width = 43
        Height = 13
        Caption = 'Volumes:'
      end
      object Label15: TLabel
        Left = 172
        Top = 48
        Width = 83
        Height = 13
        Caption = 'Qtde l'#237'quido/vol.:'
      end
      object Label10: TLabel
        Left = 76
        Top = 88
        Width = 50
        Height = 13
        Caption = 'Valor Item:'
      end
      object Label19: TLabel
        Left = 268
        Top = 48
        Width = 86
        Height = 13
        Caption = 'Total qtde l'#237'quido:'
      end
      object Label5: TLabel
        Left = 172
        Top = 88
        Width = 64
        Height = 13
        Caption = 'Valor unit'#225'rio:'
      end
      object SpeedButton1: TSpeedButton
        Left = 476
        Top = 24
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object Label3: TLabel
        Left = 364
        Top = 48
        Width = 76
        Height = 13
        Caption = 'Qtde bruto/Vol.:'
      end
      object Label6: TLabel
        Left = 460
        Top = 48
        Width = 82
        Height = 13
        Caption = 'Total qtde  Bruto:'
      end
      object Label4: TLabel
        Left = 268
        Top = 88
        Width = 27
        Height = 13
        Caption = '% IPI:'
      end
      object Label7: TLabel
        Left = 364
        Top = 88
        Width = 25
        Height = 13
        Caption = '$ IPI:'
      end
      object Label8: TLabel
        Left = 460
        Top = 88
        Width = 61
        Height = 13
        Caption = '$ Custo item:'
      end
      object Label66: TLabel
        Left = 504
        Top = 8
        Width = 31
        Height = 13
        Caption = 'CFOP:'
      end
      object EdProduto: TdmkEditCB
        Left = 76
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdProdutoKeyDown
        DBLookupComboBox = CBProduto
        IgnoraDBLookupComboBox = False
      end
      object CBProduto: TdmkDBLookupComboBox
        Left = 132
        Top = 24
        Width = 344
        Height = 21
        Color = clWhite
        KeyField = 'PQ'
        ListField = 'NOMEPQ'
        ListSource = DsPQ1
        TabOrder = 1
        OnKeyDown = CBProdutoKeyDown
        dmkEditCB = EdProduto
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdVolumes: TdmkEdit
        Left = 76
        Top = 64
        Width = 93
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '1'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.000000000000000000
        ValWarn = False
        OnChange = EdVolumesChange
        OnExit = EdVolumesExit
      end
      object EdkgLiq: TdmkEdit
        Left = 172
        Top = 64
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdkgLiqChange
        OnExit = EdkgLiqExit
      end
      object EdVlrItem: TdmkEdit
        Left = 76
        Top = 104
        Width = 92
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdVlrItemChange
      end
      object EdTotalkgLiq: TdmkEdit
        Left = 268
        Top = 64
        Width = 92
        Height = 21
        Alignment = taRightJustify
        Color = clWhite
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdTotalkgLiqChange
        OnExit = EdTotalkgLiqExit
      end
      object EdValorkg: TdmkEdit
        Left = 172
        Top = 104
        Width = 92
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkAmostra: TCheckBox
        Left = 484
        Top = 136
        Width = 70
        Height = 17
        Caption = #201' amostra.'
        TabOrder = 13
        Visible = False
      end
      object EdTotalkgBruto: TdmkEdit
        Left = 460
        Top = 64
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdTotalkgBrutoChange
      end
      object EdKgBruto: TdmkEdit
        Left = 364
        Top = 64
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdKgBrutoChange
      end
      object EdIPI_pIPI: TdmkEdit
        Left = 268
        Top = 104
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdIPI_pIPIChange
      end
      object EdIPI_vIPI: TdmkEdit
        Left = 364
        Top = 104
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdIPI_vIPIChange
      end
      object EdCustoItem: TdmkEdit
        Left = 460
        Top = 104
        Width = 92
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object Edprod_CFOP: TdmkEdit
        Left = 504
        Top = 24
        Width = 49
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 4
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CkDtCorrApo: TCheckBox
        Left = 76
        Top = 136
        Width = 185
        Height = 17
        Caption = #201' corre'#231#227'o de apontamento. Data:'
        TabOrder = 14
        Visible = False
      end
      object TPDtCorrApo: TdmkEditDateTimePicker
        Left = 264
        Top = 132
        Width = 186
        Height = 21
        Date = 0.833253726850671200
        Time = 0.833253726850671200
        Enabled = False
        TabOrder = 15
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 225
      Width = 566
      Height = 7
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      ExplicitHeight = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 566
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 518
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 470
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 435
        Height = 32
        Caption = 'Item de Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 435
        Height = 32
        Caption = 'Item de Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 435
        Height = 32
        Caption = 'Item de Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 248
    Width = 566
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 249
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 562
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 292
    Width = 566
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 293
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 562
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 417
        Top = 0
        Width = 145
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrPQ1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Nome NOMEPQ, pfo.* '
      'FROM pqfor pfo'
      'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo'
      'WHERE pfo.CI=:P0'
      'AND pfo.IQ=:P1'
      'ORDER BY pq.Nome')
    Left = 260
    Top = 46
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
    object QrPQ1NOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQ1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQ1IQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQ1PQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrPQ1CI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQ1Prazo: TWideStringField
      FieldName = 'Prazo'
    end
    object QrPQ1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQ1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQ1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQ1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQ1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsPQ1: TDataSource
    DataSet = QrPQ1
    Left = 288
    Top = 46
  end
  object QrLocalizaPQ: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM pq'
      'WHERE Codigo=:P0'
      'AND IQ=:P1')
    Left = 316
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
end
