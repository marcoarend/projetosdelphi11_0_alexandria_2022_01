object FmPQPedEditCalc: TFmPQPedEditCalc
  Left = 234
  Top = 154
  Caption = 'C'#225'lculo de Pesos de Pedidos de Insumos'
  ClientHeight = 143
  ClientWidth = 343
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 0
    Width = 343
    Height = 95
    Align = alClient
    TabOrder = 0
    object Label14: TLabel
      Left = 140
      Top = 8
      Width = 43
      Height = 13
      Caption = 'Volumes:'
    end
    object Label4: TLabel
      Left = 200
      Top = 8
      Width = 61
      Height = 13
      Caption = 'kg/vol bruto:'
    end
    object Label15: TLabel
      Left = 272
      Top = 8
      Width = 69
      Height = 13
      Caption = 'kg/vol l'#237'quido:'
    end
    object Label18: TLabel
      Left = 138
      Top = 52
      Width = 69
      Height = 13
      Caption = 'Total kg bruto:'
    end
    object Label19: TLabel
      Left = 243
      Top = 52
      Width = 77
      Height = 13
      Caption = 'Total kg l'#237'quido:'
    end
    object EdVolumes: TdmkEdit
      Left = 140
      Top = 24
      Width = 57
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = EdVolumesExit
    end
    object EdKgBruto: TdmkEdit
      Left = 200
      Top = 24
      Width = 70
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdKgBrutoExit
    end
    object EdkgLiq: TdmkEdit
      Left = 272
      Top = 24
      Width = 71
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdkgLiqExit
    end
    object EdTotalkgBruto: TdmkEdit
      Left = 138
      Top = 68
      Width = 102
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdTotalkgBrutoExit
    end
    object EdTotalkgLiq: TdmkEdit
      Left = 243
      Top = 68
      Width = 100
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdTotalkgLiqExit
    end
    object RGCalc: TRadioGroup
      Left = 12
      Top = 4
      Width = 117
      Height = 85
      Caption = ' Calcular por: '
      Items.Strings = (
        'Volume e kg/vol'
        'Volume e total kg'
        'kg/vol e total kg')
      TabOrder = 0
      TabStop = True
      OnClick = RGCalcClick
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 95
    Width = 343
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Left = 8
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&OK'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Left = 253
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
end
