object FmCMPTInn: TFmCMPTInn
  Left = 339
  Top = 185
  Caption = 
    'PST-_CMPT-001 :: Controle de Mat'#233'ria-prima de Terceiros - Entrad' +
    'a'
  ClientHeight = 626
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 470
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnPesquisa: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 108
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 108
        Align = alClient
        Caption = ' Filtros de pesquisa: '
        TabOrder = 0
        object RGAbertos: TRadioGroup
          Left = 569
          Top = 15
          Width = 96
          Height = 91
          Align = alLeft
          Caption = ' Status: '
          ItemIndex = 2
          Items.Strings = (
            'Abertos'
            'Encerrados'
            'Qualquer')
          TabOrder = 0
          OnClick = RGAbertosClick
        end
        object Panel3: TPanel
          Left = 2
          Top = 15
          Width = 567
          Height = 91
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Label1: TLabel
            Left = 8
            Top = 44
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object Label10: TLabel
            Left = 8
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object CBClientePesq: TdmkDBLookupComboBox
            Left = 64
            Top = 58
            Width = 500
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECLI'
            ListSource = DsClientesPesq
            TabOrder = 2
            dmkEditCB = EdClientePesq
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdClientePesq: TdmkEditCB
            Left = 8
            Top = 58
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClientePesqChange
            DBLookupComboBox = CBClientePesq
            IgnoraDBLookupComboBox = False
          end
          object EdFilialPesq: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFilialPesqChange
            DBLookupComboBox = CBFilialPesq
            IgnoraDBLookupComboBox = False
          end
          object CBFilialPesq: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 500
            Height = 21
            KeyField = 'Filial'
            ListField = 'NomeEmp'
            ListSource = DModG.DsFiliLog
            TabOrder = 1
            dmkEditCB = EdFilialPesq
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object Panel6: TPanel
          Left = 665
          Top = 15
          Width = 120
          Height = 91
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          object CkIni: TCheckBox
            Left = 5
            Top = 4
            Width = 110
            Height = 17
            Caption = 'Data inicial:'
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = CkIniClick
          end
          object TPIni: TdmkEditDateTimePicker
            Left = 5
            Top = 24
            Width = 110
            Height = 21
            Date = 39750.896049618060000000
            Time = 39750.896049618060000000
            TabOrder = 1
            OnClick = TPIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object CkFim: TCheckBox
            Left = 5
            Top = 48
            Width = 110
            Height = 17
            Caption = 'Data final:'
            Checked = True
            State = cbChecked
            TabOrder = 2
            OnClick = CkFimClick
          end
          object TPFim: TdmkEditDateTimePicker
            Left = 5
            Top = 66
            Width = 110
            Height = 21
            Date = 39750.896066076390000000
            Time = 39750.896066076390000000
            TabOrder = 3
            OnClick = TPFimClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
        end
        object GroupBox2: TGroupBox
          Left = 785
          Top = 15
          Width = 144
          Height = 91
          Align = alLeft
          Caption = '                               '
          TabOrder = 3
          object CkInnQtdVal: TCheckBox
            Left = 12
            Top = 0
            Width = 97
            Height = 17
            Caption = 'Valor entrada:'
            TabOrder = 0
            OnClick = CkInnQtdValClick
          end
          object Panel2: TPanel
            Left = 2
            Top = 15
            Width = 140
            Height = 74
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Label2: TLabel
              Left = 8
              Top = 20
              Width = 38
              Height = 13
              Caption = 'M'#237'nimo:'
            end
            object Label3: TLabel
              Left = 8
              Top = 44
              Width = 39
              Height = 13
              Caption = 'M'#225'ximo:'
            end
            object EdInnQtdValMin: TdmkEdit
              Left = 52
              Top = 16
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdInnQtdValMinChange
            end
            object EdInnQtdValMax: TdmkEdit
              Left = 52
              Top = 40
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdInnQtdValMaxChange
            end
          end
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 108
      Width = 1008
      Height = 362
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object PnGrades: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 362
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 0
          Top = 253
          Width = 1008
          Height = 5
          Cursor = crVSplit
          Align = alBottom
          ExplicitLeft = 1
          ExplicitTop = 202
          ExplicitWidth = 900
        end
        object GradeInn: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 1008
          Height = 253
          Align = alClient
          DataSource = DsCMPTInn
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMEEMP'
              Title.Caption = 'Empresa'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPRC'
              Title.Caption = 'Proced'#234'ncia'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataE'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFInn'
              Title.Caption = 'NF Entrada'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFRef'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NF_CC'
              Title.Caption = 'NF CC'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InnQtdkg'
              Title.Caption = 'kg PNF'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InnQtdPc'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InnQtdVal'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoQtdkg'
              Title.Caption = 'Sdo kg'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoQtdPc'
              Title.Caption = 'Sdo pe'#231'as'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoQtdVal'
              Title.Caption = 'Sdo valor'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValMaoObra'
              Title.Caption = 'Val. MDO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PLEKg'
              Title.Caption = 'Kg PLE'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PrcUnitMO'
              Title.Caption = '$ Unit. MO'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoFMOKg'
              Title.Caption = 'Sdo Fat MO kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoFMOVal'
              Title.Caption = 'Sdo Fat MO $'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end>
        end
        object GradeIts: TDBGrid
          Left = 0
          Top = 258
          Width = 1008
          Height = 104
          Align = alBottom
          DataSource = DsCMPTInnIts
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Devolu'#231#227'o'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID Item'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataS'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFOut'
              Title.Caption = 'NF'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OutQtdkg'
              Title.Caption = 'Peso kg'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OutQtdPc'
              Title.Caption = 'Pe'#231'as'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OutQtdVal'
              Title.Caption = 'Valor'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MOPesoKg'
              Title.Caption = 'Kg MO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MOPrcKg'
              Title.Caption = 'MO $ Kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MOValor'
              Title.Caption = 'MO Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFCMO'
              Title.Caption = 'NF CMO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFDMP'
              Title.Caption = 'NF DMP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFDIQ'
              Title.Caption = 'NF DIQ'
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 591
        Height = 32
        Caption = 'Controle de Mat'#233'ria-prima de Terceiros - Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 591
        Height = 32
        Caption = 'Controle de Mat'#233'ria-prima de Terceiros - Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 591
        Height = 32
        Caption = 'Controle de Mat'#233'ria-prima de Terceiros - Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 518
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 562
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 5
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Inclui'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 140
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtExcluiClick
      end
      object BtDevolve: TBitBtn
        Tag = 425
        Left = 388
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Devolu'#231#245'es'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtDevolveClick
      end
      object BtImpressao: TBitBtn
        Tag = 5
        Left = 514
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Impress'#227'o'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtImpressaoClick
      end
    end
  end
  object QrCMPTInn: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCMPTInnAfterOpen
    BeforeClose = QrCMPTInnBeforeClose
    AfterScroll = QrCMPTInnAfterScroll
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NOMEPRC,'
      'emp.Filial, inn.*'
      'FROM cmptinn inn'
      'LEFT JOIN entidades cli ON inn.Cliente=cli.Codigo'
      'LEFT JOIN entidades emp ON inn.Empresa=emp.Codigo'
      'LEFT JOIN entidades prc ON inn.Procedencia=prc.Codigo'
      '')
    Left = 344
    Top = 192
    object QrCMPTInnNOMECLI: TWideStringField
      DisplayLabel = 'Cliente'
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCMPTInnCodigo: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'Codigo'
      Origin = 'cmptinn.Codigo'
      Required = True
    end
    object QrCMPTInnCliente: TIntegerField
      DisplayLabel = 'C'#243'd.Cli.'
      FieldName = 'Cliente'
      Origin = 'cmptinn.Cliente'
      Required = True
    end
    object QrCMPTInnDataE: TDateField
      DisplayLabel = 'Data entrada'
      FieldName = 'DataE'
      Origin = 'cmptinn.DataE'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCMPTInnNFInn: TIntegerField
      DisplayLabel = 'NF entrada'
      FieldName = 'NFInn'
      Origin = 'cmptinn.NFInn'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTInnNFRef: TIntegerField
      DisplayLabel = 'NF Ref.'
      FieldName = 'NFRef'
      Origin = 'cmptinn.NFRef'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTInnInnQtdkg: TFloatField
      DisplayLabel = 'kg entrada'
      FieldName = 'InnQtdkg'
      Origin = 'cmptinn.InnQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTInnInnQtdPc: TFloatField
      DisplayLabel = 'Pe'#231'as entrada'
      FieldName = 'InnQtdPc'
      Origin = 'cmptinn.InnQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCMPTInnInnQtdVal: TFloatField
      DisplayLabel = 'Valor entrada'
      FieldName = 'InnQtdVal'
      Origin = 'cmptinn.InnQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTInnSdoQtdkg: TFloatField
      DisplayLabel = 'Saldo kg'
      FieldName = 'SdoQtdkg'
      Origin = 'cmptinn.SdoQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTInnSdoQtdPc: TFloatField
      DisplayLabel = 'Saldo pe'#231'as'
      FieldName = 'SdoQtdPc'
      Origin = 'cmptinn.SdoQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCMPTInnSdoQtdVal: TFloatField
      DisplayLabel = 'Saldo Valor'
      FieldName = 'SdoQtdVal'
      Origin = 'cmptinn.SdoQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTInnNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrCMPTInnInnQtdM2: TFloatField
      FieldName = 'InnQtdM2'
      Origin = 'cmptinn.InnQtdM2'
    end
    object QrCMPTInnSdoQtdM2: TFloatField
      FieldName = 'SdoQtdM2'
      Origin = 'cmptinn.SdoQtdM2'
    end
    object QrCMPTInnEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'cmptinn.Empresa'
    end
    object QrCMPTInnGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'cmptinn.GraGruX'
    end
    object QrCMPTInnTipoNF: TSmallintField
      FieldName = 'TipoNF'
      Origin = 'cmptinn.TipoNF'
    end
    object QrCMPTInnrefNFe: TWideStringField
      FieldName = 'refNFe'
      Origin = 'cmptinn.refNFe'
      Size = 44
    end
    object QrCMPTInnmodNF: TSmallintField
      FieldName = 'modNF'
      Origin = 'cmptinn.modNF'
    end
    object QrCMPTInnSerie: TIntegerField
      FieldName = 'Serie'
      Origin = 'cmptinn.Serie'
    end
    object QrCMPTInnFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
    end
    object QrCMPTInnLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cmptinn.Lk'
    end
    object QrCMPTInnDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cmptinn.DataCad'
    end
    object QrCMPTInnDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cmptinn.DataAlt'
    end
    object QrCMPTInnUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cmptinn.UserCad'
    end
    object QrCMPTInnUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cmptinn.UserAlt'
    end
    object QrCMPTInnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'cmptinn.AlterWeb'
    end
    object QrCMPTInnAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cmptinn.Ativo'
    end
    object QrCMPTInnjaAtz: TSmallintField
      FieldName = 'jaAtz'
      Origin = 'cmptinn.jaAtz'
    end
    object QrCMPTInnProcedencia: TIntegerField
      FieldName = 'Procedencia'
      Origin = 'cmptinn.Procedencia'
    end
    object QrCMPTInnValMaoObra: TFloatField
      FieldName = 'ValMaoObra'
      Origin = 'cmptinn.ValMaoObra'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTInnNF_CC: TIntegerField
      FieldName = 'NF_CC'
      Origin = 'cmptinn.NF_CC'
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTInnNOMEPRC: TWideStringField
      FieldName = 'NOMEPRC'
      Size = 100
    end
    object QrCMPTInnHowCobrMO: TSmallintField
      FieldName = 'HowCobrMO'
      Origin = 'cmptinn.HowCobrMO'
    end
    object QrCMPTInnPLEKg: TFloatField
      FieldName = 'PLEKg'
      Origin = 'cmptinn.PLEKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTInnPrcUnitMO: TFloatField
      FieldName = 'PrcUnitMO'
      Origin = 'cmptinn.PrcUnitMO'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrCMPTInnSdoFMOVal: TFloatField
      FieldName = 'SdoFMOVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTInnSdoFMOKg: TFloatField
      FieldName = 'SdoFMOKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
  end
  object DsCMPTInn: TDataSource
    DataSet = QrCMPTInn
    Left = 344
    Top = 240
  end
  object QrClientesPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLI'
      'FROM entidades en '
      'WHERE en.Cliente2="V"'
      'ORDER BY NOMECLI')
    Left = 132
    Top = 192
    object QrClientesPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClientesPesqNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsClientesPesq: TDataSource
    DataSet = QrClientesPesq
    Left = 128
    Top = 236
  end
  object QrCMPTInnIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.DataS, cab.NFOut, its.*'
      'FROM CMPToutits its'
      'LEFT JOIN CMPTout cab ON cab.Codigo=its.Codigo'
      'WHERE its.CMPTinn=:P0')
    Left = 28
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCMPTInnItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cmptoutits.Codigo'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTInnItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'cmptoutits.Controle'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTInnItsCMPTInn: TIntegerField
      FieldName = 'CMPTInn'
      Origin = 'cmptoutits.CMPTInn'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTInnItsOutQtdkg: TFloatField
      FieldName = 'OutQtdkg'
      Origin = 'cmptoutits.OutQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTInnItsOutQtdPc: TFloatField
      FieldName = 'OutQtdPc'
      Origin = 'cmptoutits.OutQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCMPTInnItsOutQtdVal: TFloatField
      FieldName = 'OutQtdVal'
      Origin = 'cmptoutits.OutQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00;  '
    end
    object QrCMPTInnItsDataS: TDateField
      FieldName = 'DataS'
      Origin = 'cmptout.DataS'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCMPTInnItsNFOut: TIntegerField
      FieldName = 'NFOut'
      Origin = 'cmptout.NFOut'
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTInnItsOutQtdM2: TFloatField
      FieldName = 'OutQtdM2'
      Origin = 'cmptoutits.OutQtdM2'
    end
    object QrCMPTInnItsBenefikg: TFloatField
      FieldName = 'Benefikg'
      Origin = 'cmptoutits.Benefikg'
    end
    object QrCMPTInnItsBenefiPc: TFloatField
      FieldName = 'BenefiPc'
      Origin = 'cmptoutits.BenefiPc'
    end
    object QrCMPTInnItsBenefiM2: TFloatField
      FieldName = 'BenefiM2'
      Origin = 'cmptoutits.BenefiM2'
    end
    object QrCMPTInnItsSitDevolu: TSmallintField
      FieldName = 'SitDevolu'
      Origin = 'cmptoutits.SitDevolu'
    end
    object QrCMPTInnItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cmptoutits.Lk'
    end
    object QrCMPTInnItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cmptoutits.DataCad'
    end
    object QrCMPTInnItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cmptoutits.DataAlt'
    end
    object QrCMPTInnItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cmptoutits.UserCad'
    end
    object QrCMPTInnItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cmptoutits.UserAlt'
    end
    object QrCMPTInnItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'cmptoutits.AlterWeb'
    end
    object QrCMPTInnItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cmptoutits.Ativo'
    end
    object QrCMPTInnItsMOPesoKg: TFloatField
      FieldName = 'MOPesoKg'
      Origin = 'cmptoutits.MOPesoKg'
    end
    object QrCMPTInnItsMOPrcKg: TFloatField
      FieldName = 'MOPrcKg'
      Origin = 'cmptoutits.MOPrcKg'
    end
    object QrCMPTInnItsMOValor: TFloatField
      FieldName = 'MOValor'
      Origin = 'cmptoutits.MOValor'
    end
    object QrCMPTInnItsHowCobrMO: TSmallintField
      FieldName = 'HowCobrMO'
      Origin = 'cmptoutits.HowCobrMO'
    end
    object QrCMPTInnItsNFCMO: TIntegerField
      FieldName = 'NFCMO'
      Origin = 'cmptoutits.NFCMO'
    end
    object QrCMPTInnItsNFDMP: TIntegerField
      FieldName = 'NFDMP'
      Origin = 'cmptoutits.NFDMP'
    end
    object QrCMPTInnItsNFDIQ: TIntegerField
      FieldName = 'NFDIQ'
      Origin = 'cmptoutits.NFDIQ'
    end
  end
  object DsCMPTInnIts: TDataSource
    DataSet = QrCMPTInnIts
    Left = 24
    Top = 240
  end
  object frxCMPTInn: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38928.750734444400000000
    ReportOptions.Name = 'Or'#231'amento'
    ReportOptions.LastChange = 38928.750734444400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '(*'
      '  if <VFR_ORDENADO> then'
      '  begin'
      
        '    GroupHeader1.Visible := True;                               ' +
        '                                   '
      '    GroupFooter1.Visible := True;'
      '    //          '
      '    GroupHeader1.Condition := <VARF_GRUPO1>;            '
      '    MeGrupo1Head.Memo.Text := <VARF_HEADR1>;            '
      '    MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;            '
      '  end else begin'
      
        '    GroupHeader1.Visible := False;                              ' +
        '                                    '
      
        '    GroupFooter1.Visible := False;                              ' +
        '                                    '
      '  end;            '
      
        '  //if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLog' +
        'oCaminho>);'
      '*)'
      'end.')
    OnGetValue = frxCMPTInnGetValue
    Left = 417
    Top = 236
    Datasets = <
      item
        DataSet = frxDsCMPTInn
        DataSetName = 'frxDsCMPTInn'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <
      item
        Name = ' MyVars'
        Value = Null
      end
      item
        Name = 'MeuLogo'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 272.126160000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsCMPTInn
        DataSetName = 'frxDsCMPTInn'
        KeepTogether = True
        RowCount = 0
        object Memo10: TfrxMemoView
          Width = 990.236860000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 18.897650000000000000
          Width = 967.559435910000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 536.693260000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'SdoQtdVal'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."SdoQtdVal"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 480.000310000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'SdoQtdPc'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."SdoQtdPc"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 411.968770000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'SdoQtdkg'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."SdoQtdkg"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 173.858380000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NF_CC'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCMPTInn."NF_CC"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 343.937230000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'InnQtdVal'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."InnQtdVal"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 287.244280000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'InnQtdPc'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."InnQtdPc"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 219.212740000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."InnQtdkg"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 83.149660000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NFInn'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCMPTInn."NFInn"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'DataE'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCMPTInn."DataE"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 128.504020000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NFRef'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCMPTInn."NFRef"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 767.244590000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."ValMaoObra"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 835.276130000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."SdoFMOVal"]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 710.551640000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'PrcUnitMO'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."PrcUnitMO"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 642.520100000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."PLEKg"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 903.307670000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."SdoFMOKg"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 34.015770000000010000
        Top = 445.984540000000000000
        Width = 1009.134510000000000000
        object Memo30: TfrxMemoView
          Left = 536.693260000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdVal">)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 480.000310000000000000
          Top = 7.559059999999988000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdPc">)]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 411.968770000000000000
          Top = 7.559059999999875000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdkg">)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 343.937230000000000000
          Top = 7.559059999999875000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdVal">)]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 287.244280000000000000
          Top = 7.559059999999875000
          Width = 56.692906060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdPc">)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 219.212740000000000000
          Top = 7.559059999999875000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdkg">)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Top = 7.559059999999875000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 767.244590000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."ValMaoObra">)]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 642.520100000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."PLEKg">)]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 835.276130000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoFMOVal">)]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 903.307670000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoFMOKg">)]')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 710.551640000000000000
          Top = 7.559060000000045000
          Width = 56.692906060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsCMPTInn."PLEKg">) > 0, SUM(<frxDsCMPTInn."ValMaoO' +
              'bra">) / SUM(<frxDsCMPTInn."PLEKg">), 0)]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 22.677180000000000000
        Top = 166.299320000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsCMPTInn."Cliente"'
        object MeGrupo1Head: TfrxMemoView
          Top = 7.559059999999988000
          Width = 990.236860000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente: [frxDsCMPTInn."NOMECLI"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 26.456710000000000000
        Top = 359.055350000000000000
        Width = 1009.134510000000000000
        object Memo52: TfrxMemoView
          Width = 990.236860000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 536.693260000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdVal">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 480.000310000000000000
          Top = 7.559059999999988000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdPc">)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 411.968770000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdkg">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 343.937230000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdVal">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 287.244280000000000000
          Top = 7.559060000000045000
          Width = 56.692906060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdPc">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 219.212740000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdkg">)]')
          ParentFont = False
        end
        object MeGrupo1Foot: TfrxMemoView
          Top = 7.559059999999988000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total: [frxDsCMPTInn."NOMECLI"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 767.244590000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."ValMaoObra">)]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 642.520100000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."PLEKg">)]')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 835.276130000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoFMOVal">)]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 903.307670000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoFMOKg">)]')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 710.551640000000000000
          Top = 7.559060000000045000
          Width = 56.692906060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsCMPTInn."PLEKg">) > 0, SUM(<frxDsCMPTInn."ValMaoO' +
              'bra">) / SUM(<frxDsCMPTInn."PLEKg">), 0)]')
          ParentFont = False
        end
      end
      object PageHeader4: TfrxPageHeader
        Height = 86.929180240000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        object Shape3: TfrxShapeView
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 994.016390000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 1009.134510000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 706.772110000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle de Estoque de Couros de Terceiros')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 857.953310000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 332.598640000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Top = 64.252010000000000000
          Width = 1009.134510000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo40: TfrxMemoView
          Left = 498.897960000000000000
          Top = 64.252010000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Proced'#234'ncia:')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          Left = 570.709030000000000000
          Top = 64.252010000000000000
          Width = 430.866420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROCEDENCIA]')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          Top = 64.252010000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          Left = 45.354360000000000000
          Top = 64.252010000000000000
          Width = 427.086890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          Left = 668.976810000000000000
          Top = 37.795300000000000000
          Width = 332.598640000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Status: [VARF_STATUS]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 37.795300000000000000
        Top = 211.653680000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsCMPTInn."Procedencia"'
        object Memo3: TfrxMemoView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 18.897650000000000000
          Top = 7.559059999999988000
          Width = 967.559680000000000000
          Height = 30.236230240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Proced'#234'ncia: [frxDsCMPTInn."NOMEPRC"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 128.504020000000000000
          Top = 22.677179999999940000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'NF VP')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 83.149660000000000000
          Top = 22.677179999999940000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'NF RP')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 37.795300000000000000
          Top = 22.677179999999940000
          Width = 45.354320940000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 343.937230000000000000
          Top = 22.677179999999940000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Valor entrada')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 287.244280000000000000
          Top = 22.677179999999940000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pe'#231'as entrada')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 219.212740000000000000
          Top = 22.677179999999940000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'kg entrada')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 536.693260000000000000
          Top = 22.677179999999940000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Saldo valor')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 480.000310000000000000
          Top = 22.677179999999940000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Saldo pe'#231'as')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 411.968770000000000000
          Top = 22.677179999999940000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Saldo kg')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 173.858380000000000000
          Top = 22.677179999999940000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'NF CC')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 767.244590000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'MO valor')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 835.276130000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sdo a Fat MO Val')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 710.551640000000000000
          Top = 22.677179999999990000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ Un. MO')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 642.520100000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'PLE kg')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 903.307670000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sdo a Fat MO Kg')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 26.456710000000000000
        Top = 309.921460000000000000
        Width = 1009.134510000000000000
        object Memo54: TfrxMemoView
          Width = 990.236860000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 18.897650000000000000
          Width = 967.559435910000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 536.693260000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdVal">)]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 480.000310000000000000
          Top = 3.779530000000022000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdPc">)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 411.968770000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdkg">)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 343.937230000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdVal">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 287.244280000000000000
          Top = 3.779530000000022000
          Width = 56.692906060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdPc">)]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 219.212740000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdkg">)]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 22.677180000000000000
          Top = 3.779530000000022000
          Width = 196.535560000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total: [frxDsCMPTInn."NOMEPRC"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 767.244590000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."ValMaoObra">)]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 642.520100000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."PLEKg">)]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 835.276130000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoFMOVal">)]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 903.307670000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoFMOKg">)]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 710.551640000000000000
          Top = 3.779530000000022000
          Width = 56.692906060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsCMPTInn."PLEKg">) > 0, SUM(<frxDsCMPTInn."ValMaoO' +
              'bra">) / SUM(<frxDsCMPTInn."PLEKg">), 0)]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 502.677490000000000000
        Width = 1009.134510000000000000
        object Memo93: TfrxMemoView
          Width = 695.433520000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 687.874460000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsCMPTInn: TfrxDBDataset
    UserName = 'frxDsCMPTInn'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECLI=NOMECLI'
      'Codigo=Codigo'
      'Cliente=Cliente'
      'DataE=DataE'
      'NFInn=NFInn'
      'NFRef=NFRef'
      'InnQtdkg=InnQtdkg'
      'InnQtdPc=InnQtdPc'
      'InnQtdVal=InnQtdVal'
      'SdoQtdkg=SdoQtdkg'
      'SdoQtdPc=SdoQtdPc'
      'SdoQtdVal=SdoQtdVal'
      'NOMEEMP=NOMEEMP'
      'InnQtdM2=InnQtdM2'
      'SdoQtdM2=SdoQtdM2'
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'TipoNF=TipoNF'
      'refNFe=refNFe'
      'modNF=modNF'
      'Serie=Serie'
      'Filial=Filial'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'jaAtz=jaAtz'
      'Procedencia=Procedencia'
      'ValMaoObra=ValMaoObra'
      'NF_CC=NF_CC'
      'NOMEPRC=NOMEPRC'
      'HowCobrMO=HowCobrMO'
      'PLEKg=PLEKg'
      'PrcUnitMO=PrcUnitMO'
      'SdoFMOVal=SdoFMOVal'
      'SdoFMOKg=SdoFMOKg')
    DataSet = QrCMPTImp
    BCDToCurrency = False
    Left = 416
    Top = 188
  end
  object PMImpressao: TPopupMenu
    Left = 428
    Top = 404
    object Imprimeestoquepesquisado1: TMenuItem
      Caption = '&Imprime estoque pesquisado'
      OnClick = Imprimeestoquepesquisado1Click
    end
    object Removeordenao1: TMenuItem
      Caption = '&Remove ordena'#231#227'o'
      OnClick = Removeordenao1Click
    end
  end
  object QrGBE: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.GerBxaEstq'
      'FROM CMPTinn inn'
      'LEFT JOIN gragrux ggx ON ggx.Controle=inn.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE inn.Codigo=:P0'
      '')
    Left = 108
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGBEGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
  end
  object QrCMPTImp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI,'
      ' IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP, '
      'emp.Filial, inn.*'
      'FROM CMPTinn inn'
      'LEFT JOIN entidades cli ON inn.Cliente=cli.Codigo'
      'LEFT JOIN entidades emp ON inn.Empresa=emp.Codigo')
    Left = 492
    Top = 236
    object QrCMPTImpNOMECLI: TWideStringField
      DisplayLabel = 'Cliente'
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCMPTImpCodigo: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'Codigo'
      Origin = 'CMPTinn.Codigo'
      Required = True
    end
    object QrCMPTImpCliente: TIntegerField
      DisplayLabel = 'C'#243'd.Cli.'
      FieldName = 'Cliente'
      Origin = 'CMPTinn.Cliente'
      Required = True
    end
    object QrCMPTImpDataE: TDateField
      DisplayLabel = 'Data entrada'
      FieldName = 'DataE'
      Origin = 'CMPTinn.DataE'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCMPTImpNFInn: TIntegerField
      DisplayLabel = 'NF entrada'
      FieldName = 'NFInn'
      Origin = 'CMPTinn.NFInn'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTImpNFRef: TIntegerField
      DisplayLabel = 'NF Ref.'
      FieldName = 'NFRef'
      Origin = 'CMPTinn.NFRef'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTImpInnQtdkg: TFloatField
      DisplayLabel = 'kg entrada'
      FieldName = 'InnQtdkg'
      Origin = 'CMPTinn.InnQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTImpInnQtdPc: TFloatField
      DisplayLabel = 'Pe'#231'as entrada'
      FieldName = 'InnQtdPc'
      Origin = 'CMPTinn.InnQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCMPTImpInnQtdVal: TFloatField
      DisplayLabel = 'Valor entrada'
      FieldName = 'InnQtdVal'
      Origin = 'CMPTinn.InnQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTImpSdoQtdkg: TFloatField
      DisplayLabel = 'Saldo kg'
      FieldName = 'SdoQtdkg'
      Origin = 'CMPTinn.SdoQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTImpSdoQtdPc: TFloatField
      DisplayLabel = 'Saldo pe'#231'as'
      FieldName = 'SdoQtdPc'
      Origin = 'CMPTinn.SdoQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCMPTImpSdoQtdVal: TFloatField
      DisplayLabel = 'Saldo Valor'
      FieldName = 'SdoQtdVal'
      Origin = 'CMPTinn.SdoQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTImpNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrCMPTImpInnQtdM2: TFloatField
      FieldName = 'InnQtdM2'
      Origin = 'CMPTinn.InnQtdM2'
    end
    object QrCMPTImpSdoQtdM2: TFloatField
      FieldName = 'SdoQtdM2'
      Origin = 'CMPTinn.SdoQtdM2'
    end
    object QrCMPTImpEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'CMPTinn.Empresa'
    end
    object QrCMPTImpGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'CMPTinn.GraGruX'
    end
    object QrCMPTImpTipoNF: TSmallintField
      FieldName = 'TipoNF'
      Origin = 'CMPTinn.TipoNF'
    end
    object QrCMPTImprefNFe: TWideStringField
      FieldName = 'refNFe'
      Origin = 'CMPTinn.refNFe'
      Size = 44
    end
    object QrCMPTImpmodNF: TSmallintField
      FieldName = 'modNF'
      Origin = 'CMPTinn.modNF'
    end
    object QrCMPTImpSerie: TIntegerField
      FieldName = 'Serie'
      Origin = 'CMPTinn.Serie'
    end
    object QrCMPTImpFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrCMPTImpLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCMPTImpDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCMPTImpDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCMPTImpUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCMPTImpUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCMPTImpAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCMPTImpAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCMPTImpjaAtz: TSmallintField
      FieldName = 'jaAtz'
    end
    object QrCMPTImpProcedencia: TIntegerField
      FieldName = 'Procedencia'
    end
    object QrCMPTImpValMaoObra: TFloatField
      FieldName = 'ValMaoObra'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTImpNF_CC: TIntegerField
      FieldName = 'NF_CC'
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTImpNOMEPRC: TWideStringField
      FieldName = 'NOMEPRC'
      Size = 100
    end
    object QrCMPTImpHowCobrMO: TSmallintField
      FieldName = 'HowCobrMO'
      Origin = 'cmptinn.HowCobrMO'
    end
    object QrCMPTImpPLEKg: TFloatField
      FieldName = 'PLEKg'
      Origin = 'cmptinn.PLEKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTImpPrcUnitMO: TFloatField
      FieldName = 'PrcUnitMO'
      Origin = 'cmptinn.PrcUnitMO'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrCMPTImpSdoFMOVal: TFloatField
      FieldName = 'SdoFMOVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTImpSdoFMOKg: TFloatField
      FieldName = 'SdoFMOKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
  end
  object frxReport1: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38928.750734444400000000
    ReportOptions.Name = 'Or'#231'amento'
    ReportOptions.LastChange = 38928.750734444400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '(*'
      '  if <VFR_ORDENADO> then'
      '  begin'
      
        '    GroupHeader1.Visible := True;                               ' +
        '                                   '
      '    GroupFooter1.Visible := True;'
      '    //          '
      '    GroupHeader1.Condition := <VARF_GRUPO1>;            '
      '    MeGrupo1Head.Memo.Text := <VARF_HEADR1>;            '
      '    MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;            '
      '  end else begin'
      
        '    GroupHeader1.Visible := False;                              ' +
        '                                    '
      
        '    GroupFooter1.Visible := False;                              ' +
        '                                    '
      '  end;            '
      
        '  //if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLog' +
        'oCaminho>);'
      '*)'
      'end.')
    OnGetValue = frxCMPTInnGetValue
    Left = 493
    Top = 188
    Datasets = <
      item
        DataSet = frxDsCMPTInn
        DataSetName = 'frxDsCMPTInn'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <
      item
        Name = ' MyVars'
        Value = Null
      end
      item
        Name = 'MeuLogo'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 306.141930000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsCMPTInn
        DataSetName = 'frxDsCMPTInn'
        KeepTogether = True
        RowCount = 0
        object Memo10: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 18.897650000000000000
          Width = 653.858445910000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 536.693260000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'SdoQtdVal'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."SdoQtdVal"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 480.000310000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'SdoQtdPc'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."SdoQtdPc"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 411.968770000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'SdoQtdkg'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."SdoQtdkg"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 173.858380000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NF_CC'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCMPTInn."NF_CC"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 343.937230000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'InnQtdVal'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."InnQtdVal"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 287.244280000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'InnQtdPc'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."InnQtdPc"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 219.212740000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."InnQtdkg"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 83.149660000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NFInn'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCMPTInn."NFInn"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'DataE'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCMPTInn."DataE"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 128.504020000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NFRef'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCMPTInn."NFRef"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 604.724800000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."ValMaoObra"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 816.378480000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."SdoFMOVal"]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 759.685530000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'PrcUnitMO'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."PrcUnitMO"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 691.653990000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'PLEKg'
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."PLEKg"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 884.410020000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTInn."SdoFMOKg"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 34.015770000000010000
        Top = 480.000310000000000000
        Width = 1009.134510000000000000
        object Memo30: TfrxMemoView
          Left = 536.693260000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdVal">)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 480.000310000000000000
          Top = 7.559059999999988000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdPc">)]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 411.968770000000000000
          Top = 7.559059999999875000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdkg">)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 343.937230000000000000
          Top = 7.559059999999875000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdVal">)]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 287.244280000000000000
          Top = 7.559059999999875000
          Width = 56.692906060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdPc">)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 219.212740000000000000
          Top = 7.559059999999875000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdkg">)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Top = 7.559059999999875000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 604.724800000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."ValMaoObra">)]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 22.677180000000000000
        Top = 200.315090000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsCMPTInn."Cliente"'
        object MeGrupo1Head: TfrxMemoView
          Top = 7.559059999999988000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente: [frxDsCMPTInn."NOMECLI"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 26.456710000000000000
        Top = 393.071120000000000000
        Width = 1009.134510000000000000
        object Memo52: TfrxMemoView
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 536.693260000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdVal">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 480.000310000000000000
          Top = 7.559059999999988000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdPc">)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 411.968770000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdkg">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 343.937230000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdVal">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 287.244280000000000000
          Top = 7.559060000000045000
          Width = 56.692906060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdPc">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 219.212740000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdkg">)]')
          ParentFont = False
        end
        object MeGrupo1Foot: TfrxMemoView
          Top = 7.559060000000045000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total: [frxDsCMPTInn."NOMECLI"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 604.724800000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."ValMaoObra">)]')
          ParentFont = False
        end
      end
      object PageHeader4: TfrxPageHeader
        Height = 120.944950240000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle de Estoque de Couros de Terceiros')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134199999999900000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 332.598640000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo40: TfrxMemoView
          Top = 83.149660000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Proced'#234'ncia:')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          Left = 71.811070000000000000
          Top = 83.149660000000000000
          Width = 600.945270000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROCEDENCIA]')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          Top = 64.252010000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          Left = 71.811070000000000000
          Top = 64.252010000000000000
          Width = 600.945270000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          Left = 340.157700000000000000
          Top = 41.574830000000000000
          Width = 332.598640000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Status: [VARF_STATUS]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 37.795300000000000000
        Top = 245.669450000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsCMPTInn."Procedencia"'
        object Memo3: TfrxMemoView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 18.897650000000000000
          Top = 7.559059999999988000
          Width = 653.858690000000000000
          Height = 30.236230240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Proced'#234'ncia: [frxDsCMPTInn."NOMEPRC"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 128.504020000000000000
          Top = 22.677179999999990000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'NF VP')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 83.149660000000000000
          Top = 22.677179999999990000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'NF RP')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 37.795300000000000000
          Top = 22.677179999999990000
          Width = 45.354320940000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 343.937230000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Valor entrada')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 287.244280000000000000
          Top = 22.677179999999990000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pe'#231'as entrada')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 219.212740000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'kg entrada')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 536.693260000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Saldo valor')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 480.000310000000000000
          Top = 22.677179999999990000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Saldo pe'#231'as')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 411.968770000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Saldo kg')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 173.858380000000000000
          Top = 22.677179999999990000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'NF CC')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 604.724800000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'MDO valor')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 816.378480000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'SdoFMOVal')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 759.685530000000000000
          Top = 22.677179999999990000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ Un. MO')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 691.653990000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'PLE kg')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 884.410020000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'SdoFMOKg')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 26.456710000000000000
        Top = 343.937230000000000000
        Width = 1009.134510000000000000
        object Memo54: TfrxMemoView
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 18.897650000000000000
          Width = 653.858445910000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 536.693260000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdVal">)]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 480.000310000000000000
          Top = 3.779530000000022000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdPc">)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 411.968770000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."SdoQtdkg">)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 343.937230000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdVal">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 287.244280000000000000
          Top = 3.779530000000022000
          Width = 56.692906060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdPc">)]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 219.212740000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."InnQtdkg">)]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 22.677180000000000000
          Top = 3.779530000000022000
          Width = 196.535560000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total: [frxDsCMPTInn."NOMEPRC"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 604.724800000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsCMPTInn
          DataSetName = 'frxDsCMPTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTInn."ValMaoObra">)]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 536.693260000000000000
        Width = 1009.134510000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
end
