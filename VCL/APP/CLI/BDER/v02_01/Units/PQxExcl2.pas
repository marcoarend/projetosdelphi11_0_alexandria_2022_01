unit PQxExcl2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Mask, DBCtrls, Db,
  mySQLDbTables, ComCtrls, Grids, DBGrids, Menus, dmkDBLookupComboBox,
  dmkEditCB, dmkEdit, dmkLabel, dmkImage, UnDmkProcFunc, AppListas,
  UnDmkEnums, UnPQ_PF, dmkEditDateTimePicker;

type
  TFmPQxExcl2 = class(TForm)
    Panel1: TPanel;
    Label3: TLabel;
    EdPesagem: TdmkEdit;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit6: TDBEdit;
    QrEmit: TmySQLQuery;
    QrEmitCodigo: TIntegerField;
    QrEmitDataEmis: TDateTimeField;
    QrEmitStatus: TSmallintField;
    QrEmitNumero: TIntegerField;
    QrEmitNOMECI: TWideStringField;
    QrEmitNOMESETOR: TWideStringField;
    QrEmitTecnico: TWideStringField;
    QrEmitNOME: TWideStringField;
    QrEmitClienteI: TIntegerField;
    QrEmitTipificacao: TSmallintField;
    QrEmitTempoR: TSmallintField;
    QrEmitTempoP: TSmallintField;
    QrEmitSetor: TSmallintField;
    QrEmitEspessura: TWideStringField;
    QrEmitPeso: TFloatField;
    QrEmitQtde: TFloatField;
    QrEmitLk: TIntegerField;
    QrEmitDataCad: TDateField;
    QrEmitDataAlt: TDateField;
    QrEmitUserCad: TIntegerField;
    QrEmitUserAlt: TIntegerField;
    QrEmitAreaM2: TFloatField;
    QrEmitFulao: TWideStringField;
    QrEmitDefPeca: TWideStringField;
    QrEmitCusto: TFloatField;
    DsEmit: TDataSource;
    QrMPIn: TmySQLQuery;
    QrMPInMPIn: TIntegerField;
    QrSMIA: TmySQLQuery;
    DsSMIA: TDataSource;
    PMExclui: TPopupMenu;
    Itemdepesagem1: TMenuItem;
    Todapesagem1: TMenuItem;
    QrEmitObs: TWideStringField;
    QrSMIACUSTOMEDIO: TFloatField;
    DsCI: TDataSource;
    QrCI: TmySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    QrSumEmit: TmySQLQuery;
    QrSumEmitCUSTO: TFloatField;
    QrPesq: TmySQLQuery;
    QrPesqUltimaPesagem: TIntegerField;
    QrEmitCus: TmySQLQuery;
    DsEmitCus: TDataSource;
    Label15: TLabel;
    DBEdit10: TDBEdit;
    QrEmitCusMarca: TWideStringField;
    QrEmitCusDataMPIn: TDateField;
    QrEmitCusPecas: TFloatField;
    QrEmitCusPeso: TFloatField;
    QrEmitCusCusto: TFloatField;
    QrEmitCusMPIn: TIntegerField;
    QrGraGruVal: TmySQLQuery;
    QrGraGruValCustoPreco: TFloatField;
    DsGraGruVal: TDataSource;
    DsGraCusPrc: TDataSource;
    QrGraCusPrc: TmySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcCodUsu: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    QrSaldo: TmySQLQuery;
    QrSaldoQTDE: TFloatField;
    DsSaldo: TDataSource;
    Panel4: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    QrSMIANOMECLI: TWideStringField;
    QrSMIANOMEPQ: TWideStringField;
    QrSMIADataHora: TDateTimeField;
    QrSMIAIDCtrl: TIntegerField;
    QrSMIATipo: TIntegerField;
    QrSMIAOriCodi: TIntegerField;
    QrSMIAOriCtrl: TIntegerField;
    QrSMIAOriCnta: TIntegerField;
    QrSMIAOriPart: TIntegerField;
    QrSMIAEmpresa: TIntegerField;
    QrSMIAStqCenCad: TIntegerField;
    QrSMIAGraGruX: TIntegerField;
    QrSMIAQtde: TFloatField;
    QrSMIAPecas: TFloatField;
    QrSMIAPeso: TFloatField;
    QrSMIAAlterWeb: TSmallintField;
    QrSMIAAtivo: TSmallintField;
    QrSMIAAreaM2: TFloatField;
    QrSMIAAreaP2: TFloatField;
    QrSMIAFatorClas: TFloatField;
    QrSMIAQuemUsou: TIntegerField;
    QrSMIARetorno: TSmallintField;
    QrSMIAParTipo: TIntegerField;
    QrSMIAParCodi: TIntegerField;
    QrSMIADebCtrl: TIntegerField;
    QrSMIASMIMultIns: TIntegerField;
    QrSMIACustoAll: TFloatField;
    QrSMIAValorAll: TFloatField;
    QrSMIAGrupoBal: TIntegerField;
    QrSMIABaixa: TSmallintField;
    QrSMIAAntQtde: TFloatField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNome: TWideStringField;
    DsGraGruX: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBControle: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtCancela: TBitBtn;
    BtImprime: TBitBtn;
    BtPesquisa: TBitBtn;
    BitBtn1: TBitBtn;
    BtInclui: TBitBtn;
    BtExclui: TBitBtn;
    Label14: TLabel;
    GBConfirma: TGroupBox;
    Panel3: TPanel;
    Panel9: TPanel;
    BtConfirma2: TBitBtn;
    BtDesiste2: TBitBtn;
    PnInsumos: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label19: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label20: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    EdPrecokg: TdmkEdit;
    EdPesoAdd: TdmkEdit;
    DBEdit7: TDBEdit;
    EdValorEstq: TdmkEdit;
    EdSaldoFut: TdmkEdit;
    Panel5: TPanel;
    GroupBox1: TGroupBox;
    RGTipoPreco: TRadioGroup;
    GBGraCusPrc: TGroupBox;
    Label16: TLabel;
    EdGraCusPrc: TdmkEditCB;
    CBGraCusPrc: TdmkDBLookupComboBox;
    PnImprime: TPanel;
    RGImpRecRib: TRadioGroup;
    RGImprime: TRadioGroup;
    GBkgTon: TGroupBox;
    RBTon: TRadioButton;
    RBkg: TRadioButton;
    CkMatricial: TCheckBox;
    ProgressBar1: TProgressBar;
    QrEmitAlterWeb: TSmallintField;
    QrEmitTipific: TSmallintField;
    QrEmitAtivo: TSmallintField;
    QrEmitSetrEmi: TSmallintField;
    QrEmitCod_Espess: TIntegerField;
    QrEmitCodDefPeca: TIntegerField;
    QrEmitSourcMP: TSmallintField;
    QrEmitCustoTo: TFloatField;
    QrEmitCustoKg: TFloatField;
    QrEmitCustoM2: TFloatField;
    QrEmitCusUSM2: TFloatField;
    QrEmitEmitGru: TIntegerField;
    QrEmitRetrabalho: TSmallintField;
    QrEmitNO_EmitGru: TWideStringField;
    QrEmitSemiAreaM2: TFloatField;
    QrEmitSemiRendim: TFloatField;
    QrEmitBRL_USD: TFloatField;
    QrEmitBRL_EUR: TFloatField;
    QrEmitDtaCambio: TDateField;
    QrEmitSemiTxtEspReb: TWideStringField;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    QrEmitNumCODIF: TWideStringField;
    DBEdit8: TDBEdit;
    QrEmitTpReceita: TSmallintField;
    QrEmitSemiCodPeca: TIntegerField;
    QrEmitSemiTxtPeca: TWideStringField;
    QrEmitSemiPeso: TFloatField;
    QrEmitSemiQtde: TFloatField;
    QrEmitSemiCodEspe: TIntegerField;
    QrEmitSemiTxtEspe: TWideStringField;
    QrEmitVSMovCod: TIntegerField;
    QrEmitHoraIni: TTimeField;
    QrEmitGraCorCad: TIntegerField;
    QrEmitNoGraCorCad: TWideStringField;
    QrEmitSemiCodEspReb: TIntegerField;
    QrEmitDtaBaixa: TDateField;
    QrEmitDtCorrApo: TDateTimeField;
    QrEmitEmpresa: TIntegerField;
    QrEmitAWServerID: TIntegerField;
    QrEmitAWStatSinc: TSmallintField;
    QrEmitSbtCouIntros: TFloatField;
    QrEmitSbtAreaM2: TFloatField;
    QrEmitSbtAreaP2: TFloatField;
    QrEmitSbtRendEsper: TFloatField;
    QrEmitOperPosStatus: TSmallintField;
    QrEmitOperPosGrandz: TIntegerField;
    QrEmitOperPosQtdDon: TFloatField;
    QrEmitOperPosDthIni: TDateTimeField;
    QrEmitOperPosDthFim: TDateTimeField;
    QrEmitVersao: TIntegerField;
    procedure BtExcluiClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPesagemExit(Sender: TObject);
    procedure QrEmitAfterOpen(DataSet: TDataSet);
    procedure Todapesagem1Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
    procedure Itemdepesagem1Click(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdPesoAddExit(Sender: TObject);
    procedure EdPesagemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrEmitBeforeScroll(DataSet: TDataSet);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CkDtCorrApoClick(Sender: TObject);
    procedure QrEmitCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FGraGruX: Integer;
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    function Pertence: Boolean;
    procedure CalculaSaldoFuturo();
    procedure AtualizaEmit;
    procedure ReopenEmitCus;

  public
    { Public declarations }
    procedure AtualizaCustosEmit();
    procedure ReopenEmit(Avisa: Boolean);
  end;

  var
  FmPQxExcl2: TFmPQxExcl2;

implementation

uses UnMyObjects, Module, PQx, FormulasImpShow, BlueDermConsts, DmkDAC_PF,
  UnInternalConsts, UnGrade_Tabs, UMySQLModule, Principal, PQExclPesq, MyDBCheck,
  ModProd, FormulasImpShowNew;

{$R *.DFM}

procedure TFmPQxExcl2.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmPQxExcl2.BitBtn1Click(Sender: TObject);
begin
  AtualizaCustosEmit();
end;

procedure TFmPQxExcl2.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQxExcl2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQxExcl2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQxExcl2.EdPesagemExit(Sender: TObject);
begin
  ReopenEmit(False);
end;

procedure TFmPQxExcl2.ReopenEmit(Avisa: Boolean);
var
  Pesagem: Integer;
begin
  Pesagem := EdPesagem.ValueVariant;
  QrEmit.Close;
  if Pesagem > 0 then
  begin
//    QrEmit.Params[0].AsInteger := Pesagem;
//    UnDmkDAC_PF.AbreQuery(QrEmit, Dmod.MyDB);
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
(* 2023-03-01
    'SELECT emi.*, gru.Nome NO_EmitGru  ',
    'FROM emit emi ',
    'LEFT JOIN emitgru gru ON gru.Codigo=emi.EmitGru ',
    'WHERE emi.Codigo=' + Geral.FF0(Pesagem),
*)
    'SELECT lis.TpReceita, emi.*, gru.Nome NO_EmitGru  ',
    'FROM emit emi ',
    'LEFT JOIN emitgru gru ON gru.Codigo=emi.EmitGru ',
    'LEFT JOIN listasetores lis ON lis.Codigo=emi.Setor',
    'WHERE emi.Codigo=' + Geral.FF0(Pesagem),
    '']);
  end else
  if Avisa then
    Geral.MB_Erro('Pesagem n�o localizada!');
end;

procedure TFmPQxExcl2.QrEmitAfterOpen(DataSet: TDataSet);
begin
  QrSMIA.Close;
  QrSMIA.Params[00].AsInteger := VAR_FATID_1110;
  QrSMIA.Params[01].AsInteger := VAR_FATID_1111;
  QrSMIA.Params[02].AsInteger := QrEmitCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSMIA, Dmod.MyDB);
  if FGraGruX > 0 then QrSMIA.Locate('OriCtrl', FGraGruX, []);
  ReopenEmitCus;
end;

procedure TFmPQxExcl2.Todapesagem1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o de TODA pesagem?' + sLineBreak +
  'Obs.: A receita espelho ser� mantida!') = ID_YES then
  begin
    ProgressBar1.Visible := True;
    try
      if Pertence then Exit;
      ProgressBar1.Position := 0;
      ProgressBar1.Max := QrSMIA.RecordCount + 3;
      //
      ProgressBar1.Position := ProgressBar1.Position + 1;
      ProgressBar1.Update;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE OriCodi=:P0 ');
      Dmod.QrUpd.SQL.Add('AND Tipo IN (:P1, :P2)');
      Dmod.QrUpd.Params[00].AsInteger := QrEmitCodigo.Value;
      Dmod.QrUpd.Params[01].AsInteger := VAR_FATID_1110;
      Dmod.QrUpd.Params[02].AsInteger := VAR_FATID_1111;
      Dmod.QrUpd.ExecSQL;
      //
      ProgressBar1.Position := ProgressBar1.Position + 1;
      ProgressBar1.Update;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM emitits WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrEmitCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      ProgressBar1.Position := ProgressBar1.Position + 1;
      ProgressBar1.Update;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM emit WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrEmitCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      {
      QrPQx.First;
      while not QrPQx.Eof do
      begin
        ProgressBar1.Position := ProgressBar1.Position + 1;
        ProgressBar1.Update;
        Application.ProcessMessages;
        UnPQx.AtualizaEstoquePQ(QrPQxCliOrig.Value, QrPQxInsumo.Value, aeMsg, '');
        QrPQx.Next;
      end;
      }
      ReopenEmit(False);
    except
      raise;
    end;
    Screen.Cursor := crDefault;
    ProgressBar1.Visible := False;
  end;
end;

procedure TFmPQxExcl2.BtImprimeClick(Sender: TObject);
begin
  MostraEdicao(1, stPsq, 0);
end;

procedure TFmPQxExcl2.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      GBControle.Visible := True;
      GBConfirma.Visible := False;
      PnImprime.Visible  := False;
      PnInsumos.Visible  := False;
      //
      EdPesagem.Enabled  := True;
    end;
    1:
    begin
      PnImprime.Visible  := True;
      GBConfirma.Visible := True;
      GBControle.Visible := False;
      //
      EdPesagem.Enabled  := False;
    end;
    2:
    begin
      PnInsumos.Visible  := True;
      GBConfirma.Visible := True;
      GBControle.Visible := False;
      //
      EdPesagem.Enabled  := False;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  (*GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then
  begin
    FBaixaControle := Codigo;
    ReopenBaixa;
  end;*)
end;

procedure TFmPQxExcl2.BtConfirma2Click(Sender: TObject);
const
  Tipo = VAR_FATID_1111; // // Baixa por Pesagem no Blue Derm (Uso e consumo)
  ParTipo = GRADE_TABS_PARTIPO_0001; // Identifica o par no PQX e no StqMovItsA(B)
  OriCnta = 0;
  OriPart = 0;
  StqCenCad = 1;  // Parei Aqui! Mudar?
  FatorClas = 1;
  Pecas = 0;
  AreaM2 = 0;
  AreaP2 = 0;
  Retorno = 0;
  DebCtrl = 0;
  SMIMultIns = 0;
  ValorAll = 0;
  GrupoBal = 0;
  Baixa = 1;
  AntQtde = 0;
var
  Controle: Integer;
  //KG, RS,
  SF: Double;
  //
  IDCtrl, ParCodi, OriCodi, OriCtrl, Empresa, QuemUsou, GraGruX: Integer;
  DataHora: String;
  Qtde, Peso, CustoAll: Double;
begin
  if PnImprime.Visible then
  begin
    BDC_APRESENTACAO_IMPRESSAO := RGImpRecRib.ItemIndex;
    VAR_SETOR    := '';
    VAR_SETORNUM := QrEmitSetor.Value;


    case Dmod.QrControleVerImprRecRib.Value of
      0:
      begin
        FrFormulasImpShow.FReImprime    := True;
        FrFormulasImpShow.FEmit         := QrEmitCodigo.Value;
        FrFormulasImpShow.FEmitGru      := QrEmitEmitGru.Value;
        FrFormulasImpShow.FEmitGru_TXT  := QrEmitNO_EmitGru.Value;
        FrFormulasImpShow.FRetrabalho   := QrEmitRetrabalho.Value;
        FrFormulasImpShow.FSourcMP      := QrEmitSourcMP.Value;
        FrFormulasImpShow.FDataP        := QrEmitDataEmis.Value;
        FrFormulasImpShow.FDtCorrApo    := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
        FrFormulasImpShow.FEmpresa      := Empresa;
        FrFormulasImpShow.FProgressBar1 := ProgressBar1;
        FrFormulasImpShow.FTipoPreco    := -1;// n�o deve usar
        FrFormulasImpShow.FCliIntCod    := QrEmitClienteI.Value;
        FrFormulasImpShow.FCliIntTxt    := QrEmitNOMECI.Value;
        FrFormulasImpShow.FFormula      := QrEmitNumero.Value;
        FrFormulasImpShow.FTipoImprime  := RGImprime.ItemIndex;
        FrFormulasImpShow.FPeso         := QrEmitPeso.Value;
        FrFormulasImpShow.FQtde         := QrEmitQtde.Value;
        FrFormulasImpShow.FArea         := QrEmitAreaM2.Value;
        FrFormulasImpShow.FLinhasRebx   := QrEmitEspessura.Value;
        FrFormulasImpShow.FLinhasSemi   := QrEmitSemiTxtEspReb.Value;
        FrFormulasImpShow.FFulao        := QrEmitFulao.Value;
        FrFormulasImpShow.FDefPeca      := QrEmitDefPeca.Value;
        FrFormulasImpShow.FHHMM_P       := dmkPF.HorasMH(QrEmitTempoP.Value, False);
        FrFormulasImpShow.FHHMM_R       := dmkPF.HorasMH(QrEmitTempoR.Value, False);
        FrFormulasImpShow.FHHMM_T       := dmkPF.HorasMH(QrEmitTempoP.Value+QrEmitTempoR.Value, False);
        FrFormulasImpShow.FObs          := QrEmitObs.Value;
        FrFormulasImpShow.FSetor        := QrEmitSetor.Value;
        FrFormulasImpShow.FTipific      := QrEmitTipificacao.Value;
        FrFormulasImpShow.FCod_Espess   := QrEmitCod_Espess.Value;
        FrFormulasImpShow.FCodDefPeca   := QrEmitCodDefPeca.Value;
        FrFormulasImpShow.FSemiAreaM2   := QrEmitSemiAreaM2.Value;
        FrFormulasImpShow.FSemiRendim   := QrEmitSemiRendim.Value;
        FrFormulasImpShow.FBRL_USD      := QrEmitBRL_USD.Value;
        FrFormulasImpShow.FBRL_EUR      := QrEmitBRL_EUR.Value;
        FrFormulasImpShow.FDtaCambio    := QrEmitDtaCambio.Value;
        //
        if CkMatricial.Checked then FrFormulasImpShow.FMatricialNovo := True
        else FrFormulasImpShow.FMatricialNovo := False;
        //
        if RGImpRecRib.ItemIndex = 1 then FrFormulasImpShow.FCalcCustos := True
          else FrFormulasImpShow.FCalcCustos := False;
        if RGImpRecRib.ItemIndex = 2 then FrFormulasImpShow.FCalcPrecos := True
          else FrFormulasImpShow.FCalcPrecos := False;
        if RBkg.Checked then FrFormulasImpShow.FPesoCalc := 0.01
        else FrFormulasImpShow.FPesoCalc := 10;
        //
        if FrFormulasImpShow.FLinhasRebx = '' then
          FrFormulasImpShow.FMedia := FrFormulasImpShow.FEMCM
        else begin
          if FrFormulasImpShow.FQtde > 0 then FrFormulasImpShow.FMedia := FrFormulasImpShow.FPeso / FrFormulasImpShow.FQtde
          else FrFormulasImpShow.FMedia := 0;
        end;
        //
        FrFormulasImpShow.CalculaReceita(nil);
      end;
      1:
      begin
        case TReceitaTipoSetor(QrEmitTpReceita.Value) of
          //recsetrNenhum:
          rectipsetrRibeira:      FrFormulasImpShowNew.FTpReceita    := TReceitaTipoSetor.rectipsetrRibeira;
          rectipsetrRecurtimento: FrFormulasImpShowNew.FTpReceita    := TReceitaTipoSetor.rectipsetrRecurtimento;
          //recsetrAcabamento:
          else
          begin
            Geral.MB_Aviso(
              'Nenhum tipo de receita foi definido no cadastro do setor!' + sLineBreak +
              'Ser� utilizado o de ribeira!');
            FrFormulasImpShowNew.FTpReceita    := TReceitaTipoSetor.rectipsetrRecurtimento;
          end;
        end;
        FrFormulasImpShowNew.FReImprime    := True;
        FrFormulasImpShowNew.FEmit         := QrEmitCodigo.Value;
        FrFormulasImpShowNew.FEmitGru      := QrEmitEmitGru.Value;
        FrFormulasImpShowNew.FEmitGru_TXT  := QrEmitNO_EmitGru.Value;
        FrFormulasImpShowNew.FRetrabalho   := QrEmitRetrabalho.Value;
        FrFormulasImpShowNew.FSourcMP      := QrEmitSourcMP.Value;
        FrFormulasImpShowNew.FDataP        := QrEmitDataEmis.Value;
        FrFormulasImpShowNew.FDtCorrApo    := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
        FrFormulasImpShowNew.FEmpresa      := Empresa;
        FrFormulasImpShowNew.FProgressBar1 := ProgressBar1;
        FrFormulasImpShowNew.FTipoPreco    := -1;// n�o deve usar
        FrFormulasImpShowNew.FCliIntCod    := QrEmitClienteI.Value;
        FrFormulasImpShowNew.FCliIntTxt    := QrEmitNOMECI.Value;
        FrFormulasImpShowNew.FFormula      := QrEmitNumero.Value;
        FrFormulasImpShowNew.FTipoImprime  := RGImprime.ItemIndex;
        FrFormulasImpShowNew.FPeso         := QrEmitPeso.Value;
        FrFormulasImpShowNew.FQtde         := QrEmitQtde.Value;
        FrFormulasImpShowNew.FArea         := QrEmitAreaM2.Value;
        FrFormulasImpShowNew.FLinhasRebx   := QrEmitEspessura.Value;
        FrFormulasImpShowNew.FLinhasSemi   := QrEmitSemiTxtEspReb.Value;
        FrFormulasImpShowNew.FFulao        := QrEmitFulao.Value;
        FrFormulasImpShowNew.FDefPeca      := QrEmitDefPeca.Value;
        FrFormulasImpShowNew.FHHMM_P       := dmkPF.HorasMH(QrEmitTempoP.Value, False);
        FrFormulasImpShowNew.FHHMM_R       := dmkPF.HorasMH(QrEmitTempoR.Value, False);
        FrFormulasImpShowNew.FHHMM_T       := dmkPF.HorasMH(QrEmitTempoP.Value+QrEmitTempoR.Value, False);
        FrFormulasImpShowNew.FObs          := QrEmitObs.Value;
        FrFormulasImpShowNew.FSetor        := QrEmitSetor.Value;
        FrFormulasImpShowNew.FTipific      := QrEmitTipificacao.Value;
        FrFormulasImpShowNew.FCod_Espess   := QrEmitCod_Espess.Value;
        FrFormulasImpShowNew.FCodDefPeca   := QrEmitCodDefPeca.Value;
        FrFormulasImpShowNew.FSemiAreaM2   := QrEmitSemiAreaM2.Value;
        FrFormulasImpShowNew.FSemiRendim   := QrEmitSemiRendim.Value;
        FrFormulasImpShowNew.FBRL_USD      := QrEmitBRL_USD.Value;
        FrFormulasImpShowNew.FBRL_EUR      := QrEmitBRL_EUR.Value;
        FrFormulasImpShowNew.FDtaCambio    := QrEmitDtaCambio.Value;
        //
        if CkMatricial.Checked then FrFormulasImpShowNew.FMatricialNovo := True
        else FrFormulasImpShowNew.FMatricialNovo := False;
        //
        if RGImpRecRib.ItemIndex = 1 then FrFormulasImpShowNew.FCalcCustos := True
          else FrFormulasImpShowNew.FCalcCustos := False;
        if RGImpRecRib.ItemIndex = 2 then FrFormulasImpShowNew.FCalcPrecos := True
          else FrFormulasImpShowNew.FCalcPrecos := False;
        if RBkg.Checked then FrFormulasImpShowNew.FPesoCalc := 0.01
        else FrFormulasImpShowNew.FPesoCalc := 10;
        //
        if FrFormulasImpShowNew.FLinhasRebx = '' then
          FrFormulasImpShowNew.FMedia := FrFormulasImpShowNew.FEMCM
        else begin
          if FrFormulasImpShowNew.FQtde > 0 then FrFormulasImpShowNew.FMedia := FrFormulasImpShowNew.FPeso / FrFormulasImpShowNew.FQtde
          else FrFormulasImpShowNew.FMedia := 0;
        end;
        //
        FrFormulasImpShowNew.CalculaReceita(nil);
      end;
      else
      begin
        Geral.MB_Erro('Vers�o de impress�o de receita de ribeira n�o implementada [5]!');
        Exit;
      end;
    end;



    Application.ProcessMessages;
    MostraEdicao(0, stLok, 0);
  end else if PnInsumos.Visible then
  begin
    Empresa := EdEntidade.ValueVariant;
    if MyObjects.FIC(Empresa = 0, EdEntidade, 'Defina o Cliente Interno!') then
      Exit;
    GraGruX  := EdGraGruX.ValueVariant;
    if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Produto!') then
      Exit;
    if MyObjects.FIC(QrSaldo.RecordCount = 0, nil,
    'Insumo / cliente interno n�o definido!') then
      Exit;
    SF := EdSaldoFut.ValueVariant;
    if MyObjects.FIC(SF < 0, nil, 'Quantidade insuficiente no estoque!') then
      Exit;
    Qtde := - EdPesoAdd.ValueVariant;
    // N�o Usa
    //RS := KG * QrSaldoCUSTO.Value;
    Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'EmitIts',
      'EmitIts', 'Controle');
    //
    DataHora := Geral.FDT(QrEmitDataEmis.Value, 1);
    //Qtde     :=
    Peso     := Qtde;
    CustoAll := Qtde * EdPrecoKg.ValueVariant;
    //
    IDCtrl   := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
    ParCodi  := IDCtrl;
    OriCodi  := QrEmitCodigo.Value;
    OriCtrl  := IDCtrl;
    QuemUsou := Empresa;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
    'DataHora', 'Tipo', 'OriCodi',
    'OriCtrl', 'OriCnta', 'OriPart',
    'Empresa', 'StqCenCad', 'GraGruX',
    'Qtde', 'Pecas', 'Peso',
    'AreaM2', 'AreaP2', 'FatorClas',
    'QuemUsou', 'Retorno', 'ParTipo',
    'ParCodi', 'DebCtrl', 'SMIMultIns',
    'CustoAll', 'ValorAll', 'GrupoBal',
    'Baixa', 'AntQtde'], [
    'IDCtrl'], [
    DataHora, Tipo, OriCodi,
    OriCtrl, OriCnta, OriPart,
    Empresa, StqCenCad, GraGruX,
    Qtde, Pecas, Peso,
    AreaM2, AreaP2, FatorClas,
    QuemUsou, Retorno, ParTipo,
    ParCodi, DebCtrl, SMIMultIns,
    CustoAll, ValorAll, GrupoBal,
    Baixa, AntQtde], [
    IDCtrl], False) then
    begin
      {
      if QrSaldo.RecordCount = 0 then
      begin
        Geral.MB_Erro('Insumo / cliente interno n�o definido!';
        Exit;
      end;
      SF := Geral.DMV(EdSaldoFut.Text);
      if SF < 0 then
      begin
        Geral.MB_Erro('Quantidade insuficiente no estoque!');
        Exit;
      end;
      KG := Geral.DMV(EdPesoAdd.Text);
      RS := KG * QrSaldoCUSTO.Value;
      Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'EmitIts',
        'EmitIts', 'Controle');
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO p q x SET Tipo=110, DataX=:P0, ');
      Dmod.QrUpd.SQL.Add('CliOrig=:P1, CliDest=:P2, Insumo=:P3, Peso=:P4, ');
      Dmod.QrUpd.SQL.Add('Valor=:P5, OrigemCodi=:P6, OrigemCtrl=:P7');
      Dmod.QrUpd.Params[00].AsString  := Geral.FDT(QrEmitDataEmis.Value, 1);
      Dmod.QrUpd.Params[01].AsInteger := QrSaldoCI.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrEmitClienteI.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrSaldoPQ.Value;
      Dmod.QrUpd.Params[04].AsFloat   := -KG;
      Dmod.QrUpd.Params[05].AsFloat   := -RS;
      Dmod.QrUpd.Params[06].AsInteger := QrEmitCodigo.Value;
      Dmod.QrUpd.Params[07].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
      UnPQx.AtualizaEstoquePQ(QrSaldoCI.Value, QrSaldoPQ.Value, aeMsg, '');
      }
      FGraGruX := Controle;
      QrSaldo.Close;
      UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
      AtualizaCustosEmit();
    end;
  end;
end;

procedure TFmPQxExcl2.BtDesiste2Click(Sender: TObject);
begin
 if PnImprime.Visible then MostraEdicao(0, stLok, 0) else
 begin
   ReopenEmit(True);
   MostraEdicao(0, stLok, 0);
 end;
end;

procedure TFmPQxExcl2.Itemdepesagem1Click(Sender: TObject);
{
var
  GraGruX, Entidade: Integer;
}
begin
  //if Pertence then Exit;
  if Geral.MB_Pergunta('O item (uso e consumo) "' + QrSMIANOMEPQ.Value +
  '" ser� excluido da pesagem, mas n�o da receita espelho.' + sLineBreak +
  'Confirma a exclus�o do insumo "'+ QrSMIANOMEPQ.Value + '" desta pesagem?') = ID_YES then
  begin
    {
    GraGruX  := QrSMIAGraGruX.Value;
    Entidade := QrSMIAEmpresa.Value;
    }
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE Tipo IN (:P0, :P1) AND ');
    Dmod.QrUpd.SQL.Add('OriCodi=:P2 AND OriCtrl=:P3');
    Dmod.QrUpd.Params[00].AsInteger := VAR_FATID_1110;
    Dmod.QrUpd.Params[01].AsInteger := VAR_FATID_1111;
    Dmod.QrUpd.Params[02].AsInteger := QrSMIAOriCodi.Value;
    Dmod.QrUpd.Params[03].AsInteger := QrSMIAOriCtrl.Value;
    Dmod.QrUpd.ExecSQL;
    //
    //UnPQx.AtualizaEstoquePQ(CliInt, Insumo, aeMsg, '');
    //
    AtualizaCustosEmit();
    //
    FGraGruX := UMyMod.ProximoRegistro(QrSMIA, 'OriCtrl', QrSMIAOriCtrl.Value);
    ReopenEmit(False);
  end;
end;

function TFmPQxExcl2.Pertence: Boolean;
var
  OSs, Liga: String;
begin
  QrMPIn.Close;
  QrMPIn.Params[0].AsInteger := QrEmitCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrMPIn, Dmod.MyDB);
  if QrMPIn.RecordCount > 0 then
  begin
    Result := True;
    if QrMPIn.RecordCount = 1 then
    Geral.MB_Aviso(
    'Esta pesagem n�o pode ser exclu�da ou modificada porque j� pertence a OS n� '
    + Geral.FF0(QrMPInMPIn.Value)+'.')
    else begin
      QrMPIn.First;
      OSs := '';
      Liga := '';
      while not QrMPIn.Eof do
      begin
        OSs := OSs + Liga + FormatFloat('000', QrMPInMPIn.Value);
        Liga := ', ';
        QrMPIn.Next;
      end;
      Geral.MB_Erro(
      'Esta pesagem n�o pode ser exclu�da ou modificada porque j� pertence as OS de n�: '
      + OSs + '.');
    end;
  end else Result := False;
end;

procedure TFmPQxExcl2.EdGraGruXChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQxExcl2.EdEntidadeChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQxExcl2.CalculaSaldoFuturo();
var
  Entidade, GraGruX, GraCusPrc: Integer;
  PesoAdd, SaldoFut: Double;
  //
  Preco: Double;
begin
  //
  Entidade := EdEntidade.ValueVariant;
  GraGruX  := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
  begin
    // Seleciona Tipo (e se Tipo=1 a Lista) de Pre�o pr�-configurada
    DmProd.ConfiguraPrecoDeCusto(Entidade,
      RGTipoPreco, EdGraCusPrc, CBGraCusPrc);
    if RGTipoPreco.ItemIndex = 1 then
    begin
      GraCusPrc := EdGraCusPrc.ValueVariant;
      if GraCusPrc <> 0 then
        GraCusPrc := QrGraCusPrcCodigo.Value;
      // Busca o pre�o apropriado
      QrGraGruVal.Close;
      QrGraGruVal.Params[00].AsInteger := GraGruX;
      QrGraGruVal.Params[01].AsInteger := GraCusPrc;
      QrGraGruVal.Params[02].AsInteger := Entidade;
      UnDmkDAC_PF.AbreQuery(QrGraGruVal, Dmod.MyDB);
      //
      Preco := QrGraGruValCustoPreco.Value;
      EdPrecoKg.ReadOnly := True;
      EdPrecoKg.ValueVariant := Preco;
    end else
    begin
      Preco := EdPrecoKg.ValueVariant;
      EdPrecoKg.ReadOnly := False;
    end;
    //
    QrSaldo.Close;
    QrSaldo.Params[00].AsInteger := GraGruX;
    QrSaldo.Params[01].AsInteger := Entidade;
    UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
    //
    PesoAdd := EdPesoAdd.ValueVariant;
    SaldoFut := QrSaldoQTDE.Value - PesoAdd;
    EdSaldoFut.ValueVariant := SaldoFut;
    //
    EdValorEstq.ValueVariant := QrSaldoQTDE.Value * Preco;
  end else
  begin
    EdPrecoKg.ValueVariant   := 0;
    EdSaldoFut.ValueVariant  := 0;
    EdPesoAdd.ValueVariant   := 0;
    EdValorEstq.ValueVariant := 0;
  end;
end;

procedure TFmPQxExcl2.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmPQxExcl2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.PreencheComponente(RGImpRecRib, sListaRecImpApresenta, 6);
  RGImpRecRib.ItemIndex := Dmod.QrControleImpRecRib.Value;
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCusPrc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
end;

procedure TFmPQxExcl2.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmPQxExcl2.BtPesquisaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPQExclPesq, FmPQExclPesq, afmoNegarComAviso) then
  begin
    FmPQExclPesq.ShowModal;
    FmPQExclPesq.Destroy;
  end;
end;

procedure TFmPQxExcl2.EdPesoAddExit(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmPQxExcl2.EdPesagemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    QrPesq.Close;
    UnDmkDAC_PF.AbreQuery(QrPesq, Dmod.MyDB);
    EdPesagem.Text := IntToStr(QrPesqUltimaPesagem.Value);
    //
    ReopenEmit(True);
  end;
end;

procedure TFmPQxExcl2.QrEmitBeforeScroll(DataSet: TDataSet);
begin
  ReopenEmitCus;
end;

procedure TFmPQxExcl2.QrEmitCalcFields(DataSet: TDataSet);
begin
  QrEmitNumCODIF.Value := dmkPF.IntToCODIF_Receita(QrEmitNumero.Value);
end;

procedure TFmPQxExcl2.ReopenEmitCus;
begin
  QrEmitCus.Close;
  QrEmitCus.Params[0].AsInteger := QrEmitCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrEmitCus, Dmod.MyDB);
end;

procedure TFmPQxExcl2.AtualizaEmit;
begin
  QrSumEmit.Close;
  QrSumEmit.Params[0].AsInteger := QrEmitCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSumEmit, Dmod.MyDB);
  //
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE emit SET Custo=:P0, Status=2 WHERE Codigo=:P1');
  Dmod.QrUpdM.Params[0].AsFloat   := QrSumEmitCUSTO.Value;
  Dmod.QrUpdM.Params[1].AsInteger := QrEmitCodigo.Value;
  Dmod.QrUpdM.ExecSQL;
end;

procedure TFmPQxExcl2.AtualizaCustosEmit();
var
  MPIn: Integer;
begin
  AtualizaEmit;
  if QrEmit.State <> dsInactive then
  begin
    MPIn := QrEmitCusMPIn.Value;
    //
    Dmod.AtualizaCustosEmit_2(QrEmitCodigo.Value);
    QrEmit.Close;
    UnDmkDAC_PF.AbreQuery(QrEmit, Dmod.MyDB);
    QrEmitCus.First;
    while not QrEmitCus.Eof do
    begin
      Dmod.AtualizaMPIn(QrEmitCusMPIn.Value);
      QrEmitCus.Next;
    end;
    QrEmitCus.Locate('MPIn', MPIn, []);
  end;
end;

end.

