unit WBPallet;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, frxClass, frxDBSet;

type
  TFmWBPallet = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrWBPallet: TmySQLQuery;
    DsWBPallet: TDataSource;
    QrWBPalArt: TmySQLQuery;
    DsWBPalArt: TDataSource;
    PMArt: TPopupMenu;
    ArtInclui1: TMenuItem;
    ArtExclui1: TMenuItem;
    ArtAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DBGArt: TDBGrid;
    QrWBPalFrn: TmySQLQuery;
    DsWBPalFrn: TDataSource;
    QrWBPalletCodigo: TIntegerField;
    QrWBPalletNome: TWideStringField;
    QrWBPalletLk: TIntegerField;
    QrWBPalletDataCad: TDateField;
    QrWBPalletDataAlt: TDateField;
    QrWBPalletUserCad: TIntegerField;
    QrWBPalletUserAlt: TIntegerField;
    QrWBPalletAlterWeb: TSmallintField;
    QrWBPalletAtivo: TSmallintField;
    QrWBPalletEmpresa: TIntegerField;
    QrWBPalletNO_EMPRESA: TWideStringField;
    QrWBPalFrnCodigo: TIntegerField;
    QrWBPalFrnControle: TIntegerField;
    QrWBPalFrnConta: TIntegerField;
    QrWBPalFrnFornece: TIntegerField;
    QrWBPalFrnLk: TIntegerField;
    QrWBPalFrnDataCad: TDateField;
    QrWBPalFrnDataAlt: TDateField;
    QrWBPalFrnUserCad: TIntegerField;
    QrWBPalFrnUserAlt: TIntegerField;
    QrWBPalFrnAlterWeb: TSmallintField;
    QrWBPalFrnAtivo: TSmallintField;
    QrWBPalFrnNO_FORNECE: TWideStringField;
    QrWBPalArtCodigo: TIntegerField;
    QrWBPalArtControle: TIntegerField;
    QrWBPalArtGraGruX: TIntegerField;
    QrWBPalArtLk: TIntegerField;
    QrWBPalArtDataCad: TDateField;
    QrWBPalArtDataAlt: TDateField;
    QrWBPalArtUserCad: TIntegerField;
    QrWBPalArtUserAlt: TIntegerField;
    QrWBPalArtAlterWeb: TSmallintField;
    QrWBPalArtAtivo: TSmallintField;
    QrWBPalArtNO_PRD_TAM_COR: TWideStringField;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBGFrn: TDBGrid;
    BtFrn: TBitBtn;
    PMFrn: TPopupMenu;
    FrnInclui1: TMenuItem;
    FrnAltera1: TMenuItem;
    FrnExclui1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWBPalletAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWBPalletBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrWBPalletAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ArtInclui1Click(Sender: TObject);
    procedure ArtExclui1Click(Sender: TObject);
    procedure ArtAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMArtPopup(Sender: TObject);
    procedure QrWBPalletBeforeClose(DataSet: TDataSet);
    procedure QrWBPalArtBeforeClose(DataSet: TDataSet);
    procedure QrWBPalArtAfterScroll(DataSet: TDataSet);
    procedure FrnInclui1Click(Sender: TObject);
    procedure FrnAltera1Click(Sender: TObject);
    procedure FrnExclui1Click(Sender: TObject);
    procedure PMFrnPopup(Sender: TObject);
    procedure BtFrnClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraFormWBPalArt(SQLType: TSQLType);
    procedure MostraFormWBPalFrn(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenWBPalArt(Controle: Integer);
    procedure ReopenWBPalFrn(Conta: Integer);

  end;

var
  FmWBPallet: TFmWBPallet;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, WBPalArt, WBPalFrn, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWBPallet.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWBPallet.MostraFormWBPalArt(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmWBPalArt, FmWBPalArt, afmoNegarComAviso) then
  begin
    FmWBPalArt.ImgTipo.SQLType := SQLType;
    FmWBPalArt.FQrCab := QrWBPallet;
    FmWBPalArt.FDsCab := DsWBPallet;
    FmWBPalArt.FQrIts := QrWBPalArt;
    if SQLType = stIns then
      //
    else
    begin
      FmWBPalArt.EdControle.ValueVariant := QrWBPalArtControle.Value;
      //
      FmWBPalArt.EdGraGruX.ValueVariant := QrWBPalArtGraGruX.Value;
      FmWBPalArt.CBGraGruX.KeyValue     := QrWBPalArtGraGruX.Value;
    end;
    FmWBPalArt.ShowModal;
    FmWBPalArt.Destroy;
  end;
end;

procedure TFmWBPallet.MostraFormWBPalFrn(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmWBPalFrn, FmWBPalFrn, afmoNegarComAviso) then
  begin
    FmWBPalFrn.ImgTipo.SQLType := SQLType;
    FmWBPalFrn.FQrCab := QrWBPallet;
    FmWBPalFrn.FDsCab := DsWBPallet;
    FmWBPalFrn.FQrArt := QrWBPalArt;
    FmWBPalFrn.FDsArt := DsWBPalArt;
    FmWBPalFrn.FQrFrn := QrWBPalFrn;
    if SQLType = stIns then
      //
    else
    begin
      FmWBPalFrn.EdConta.ValueVariant   := QrWBPalFrnConta.Value;
      //
      FmWBPalFrn.EdFornece.ValueVariant := QrWBPalFrnFornece.Value;
      FmWBPalFrn.CBFornece.KeyValue     := QrWBPalFrnFornece.Value;
    end;
    FmWBPalFrn.ShowModal;
    FmWBPalFrn.Destroy;
  end;
end;

procedure TFmWBPallet.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrWBPallet);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrWBPallet, QrWBPalArt);
end;

procedure TFmWBPallet.PMFrnPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(FrnInclui1, QrWBPalArt);
  MyObjects.HabilitaMenuItemItsUpd(FrnAltera1, QrWBPalFrn);
  MyObjects.HabilitaMenuItemItsDel(FrnExclui1, QrWBPalFrn);
end;

procedure TFmWBPallet.PMArtPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ArtInclui1, QrWBPallet);
  MyObjects.HabilitaMenuItemItsUpd(ArtAltera1, QrWBPalArt);
  MyObjects.HabilitaMenuItemCabDel(ArtExclui1, QrWBPalArt, QrWBPalFrn);
end;

procedure TFmWBPallet.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWBPalletCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWBPallet.DefParams;
begin
  VAR_GOTOTABELA := 'wbpallet';
  VAR_GOTOMYSQLTABLE := QrWBPallet;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT let.*, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA ');
  VAR_SQLx.Add('FROM wbpallet let ');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=let.Empresa ');
  VAR_SQLx.Add('WHERE let.Codigo > 0');
  //
  VAR_SQL1.Add('AND let.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND let.Nome Like :P0');
  //
end;

procedure TFmWBPallet.ArtAltera1Click(Sender: TObject);
begin
  MostraFormWBPalArt(stUpd);
end;

procedure TFmWBPallet.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmWBPallet.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWBPallet.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWBPallet.ArtExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da mat�ria-prima selecionada?',
  'wbpalart', 'Controle', QrWBPalArtControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrWBPalArt,
      QrWBPalArtControle, QrWBPalArtControle.Value);
    ReopenWBPalArt(Controle);
  end;
end;

procedure TFmWBPallet.ReopenWBPalArt(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBPalArt, Dmod.MyDB, [
  'SELECT art.*, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR ',
  'FROM wbpalart art ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=art.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE art.Codigo=' + Geral.FF0(QrWBPalletCodigo.Value),
  '']);
  //
  QrWBPalArt.Locate('Controle', Controle, []);
end;


procedure TFmWBPallet.ReopenWBPalFrn(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBPalFrn, Dmod.MyDB, [
  'SELECT frn.*,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE',
  'FROM wbpalfrn frn',
  'LEFT JOIN entidades ent ON ent.Codigo=frn.Fornece',
  'WHERE Controle=' + Geral.FF0(QrWBPalArtControle.Value),
  '']);
  //
  QrWBPalFrn.Locate('Conta', Conta, []);
end;

procedure TFmWBPallet.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWBPallet.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWBPallet.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWBPallet.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWBPallet.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWBPallet.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWBPallet.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWBPalletCodigo.Value;
  Close;
end;

procedure TFmWBPallet.ArtInclui1Click(Sender: TObject);
begin
  MostraFormWBPalArt(stIns);
end;

procedure TFmWBPallet.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWBPallet, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'wbpallet');
end;

procedure TFmWBPallet.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, Empresa: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);

  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('wbpallet', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'wbpallet', False, [
  'Nome', 'Empresa'], [
  'Codigo'], [
  Nome, Empresa], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmWBPallet.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'wbpallet', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'wbpallet', 'Codigo');
end;

procedure TFmWBPallet.BtFrnClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFrn, BtFrn);
end;

procedure TFmWBPallet.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArt, BtIts);
end;

procedure TFmWBPallet.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmWBPallet.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  DBGFrn.Align    := alClient;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmWBPallet.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWBPalletCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWBPallet.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWBPallet.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrWBPalletCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWBPallet.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWBPallet.QrWBPalArtAfterScroll(DataSet: TDataSet);
begin
  ReopenWBPalFrn(0);
end;

procedure TFmWBPallet.QrWBPalArtBeforeClose(DataSet: TDataSet);
begin
  QrWBPalFrn.Close;
end;

procedure TFmWBPallet.QrWBPalletAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWBPallet.QrWBPalletAfterScroll(DataSet: TDataSet);
begin
  ReopenWBPalArt(0);
end;

procedure TFmWBPallet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrWBPalletCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmWBPallet.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWBPalletCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wbpallet', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmWBPallet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBPallet.FrnAltera1Click(Sender: TObject);
begin
  MostraFormWBPalFrn(stUpd);
end;

procedure TFmWBPallet.FrnExclui1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do fornecedor selecionado?',
  'wbpalfrn', 'Conta', QrWBPalFrnConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrWBPalFrn,
      QrWBPalFrnConta, QrWBPalFrnConta.Value);
    ReopenWBPalFrn(Conta);
  end;
end;

procedure TFmWBPallet.FrnInclui1Click(Sender: TObject);
begin
  MostraFormWBPalFrn(stIns);
end;

procedure TFmWBPallet.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWBPallet, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'wbpallet');
end;

procedure TFmWBPallet.QrWBPalletBeforeClose(
  DataSet: TDataSet);
begin
  QrWBPalArt.Close;
end;

procedure TFmWBPallet.QrWBPalletBeforeOpen(DataSet: TDataSet);
begin
  QrWBPalletCodigo.DisplayFormat := FFormatFloat;
end;

end.

