object FmPQporNF: TFmPQporNF
  Left = 339
  Top = 185
  Caption = 'QUI-INSUM-011 :: Insumo por NF'
  ClientHeight = 629
  ClientWidth = 1148
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1148
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 1264
    object GB_R: TGroupBox
      Left = 1100
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 1216
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1052
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 1168
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 180
        Height = 32
        Caption = 'Insumo por NF'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 180
        Height = 32
        Caption = 'Insumo por NF'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 180
        Height = 32
        Caption = 'Insumo por NF'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1148
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 1264
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1144
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1260
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1148
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 1264
    object PnSaiDesis: TPanel
      Left = 1002
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 1118
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1000
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 1116
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1148
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 1264
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1148
      Height = 81
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1264
      object LaPQ: TLabel
        Left = 8
        Top = 40
        Width = 37
        Height = 13
        Caption = 'Insumo:'
      end
      object LaCI: TLabel
        Left = 8
        Top = 0
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label4: TLabel
        Left = 536
        Top = 40
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label5: TLabel
        Left = 653
        Top = 40
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object Label40: TLabel
        Left = 536
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdPQ: TdmkEditCB
        Left = 8
        Top = 56
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdPQRedefinido
        DBLookupComboBox = CBPQ
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPQ: TdmkDBLookupComboBox
        Left = 76
        Top = 56
        Width = 457
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPQ
        TabOrder = 3
        dmkEditCB = EdPQ
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCI: TdmkEditCB
        Left = 8
        Top = 16
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdCIRedefinido
        DBLookupComboBox = CBCI
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCI: TdmkDBLookupComboBox
        Left = 76
        Top = 16
        Width = 457
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECI'
        ListSource = DsCI
        TabOrder = 1
        dmkEditCB = EdCI
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPIni: TdmkEditDateTimePicker
        Left = 536
        Top = 56
        Width = 112
        Height = 21
        Date = 37670.000000000000000000
        Time = 0.521748657396528900
        TabOrder = 4
        OnClick = TPIniClick
        OnChange = TPIniChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPFim: TdmkEditDateTimePicker
        Left = 653
        Top = 56
        Width = 112
        Height = 21
        Date = 37670.000000000000000000
        Time = 0.521748657396528900
        TabOrder = 5
        OnClick = TPFimClick
        OnChange = TPFimChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdEmpresa: TdmkEditCB
        Left = 536
        Top = 16
        Width = 40
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Filial'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 576
        Top = 16
        Width = 189
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 7
        TabStop = False
        dmkEditCB = EdEmpresa
        QryCampo = 'Filial'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 81
      Width = 1148
      Height = 386
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 1
      ExplicitWidth = 1264
      object TabSheet1: TTabSheet
        Caption = 'Entradas edit'#225'veis'
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 1140
          Height = 358
          Align = alClient
          BevelOuter = bvNone
          Caption = 'Panel5'
          TabOrder = 0
          object DBGPqx: TDBGrid
            Left = 0
            Top = 0
            Width = 1140
            Height = 309
            Align = alClient
            DataSource = DsPqx
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'DataX'
                Title.Caption = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Tipo'
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NomeVFI'
                Title.Caption = 'Movimento'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OrigemCodi'
                Title.Caption = 'C'#243'digo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OrigemCtrl'
                Title.Caption = 'Controle'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Peso'
                Title.Caption = 'Qtde'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SdoPeso'
                Title.Caption = 'Sdo Qtde'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SdoValr'
                Title.Caption = 'Sdo Valr'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Serie'
                Title.Caption = 'S'#233'rie NF'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NF'
                Title.Caption = 'N'#186' NF'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'xLote'
                Visible = True
              end>
          end
          object Panel6: TPanel
            Left = 0
            Top = 309
            Width = 1140
            Height = 49
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object BtReabre: TBitBtn
              Tag = 18
              Left = 12
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Reabre'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtReabreClick
            end
            object BtAltera: TBitBtn
              Tag = 11
              Left = 104
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Altera'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtAlteraClick
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Entradas x Baixas'
        ImageIndex = 1
        object Panel7: TPanel
          Left = 0
          Top = 309
          Width = 1140
          Height = 49
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 1256
          object ReabrPqw: TBitBtn
            Tag = 18
            Left = 12
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Reabre'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = ReabrPqwClick
          end
          object BtImprime: TBitBtn
            Tag = 5
            Left = 104
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Imprime'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BtImprimeClick
          end
          object BtCorrigePqw: TBitBtn
            Left = 200
            Top = 4
            Width = 233
            Height = 40
            Caption = '&Atualizar campos novos da tabela PQW'
            NumGlyphs = 2
            TabOrder = 2
            OnClick = BtCorrigePqwClick
          end
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1140
          Height = 309
          Align = alClient
          DataSource = DsPqw
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Serie'
              Title.Caption = 'S'#233'rie NF'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NF'
              Title.Caption = 'N'#186' NF'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataX'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tipo'
              Title.Caption = 'Tipo (+)'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomeVFX'
              Title.Caption = 'Movimento (+)'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrigemCodi'
              Title.Caption = 'C'#243'digo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrigemCtrl'
              Title.Caption = 'Controle'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OriTipo'
              Title.Caption = 'Tipo (-)'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomeVFW'
              Title.Caption = 'Movimento (-)'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoInn'
              Title.Caption = 'Qtde (+)'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoBxa'
              Title.Caption = 'Qtde (-)'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'xLote'
              Title.Caption = 'Lote'
              Width = 137
              Visible = True
            end>
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrPqx: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPqxAfterOpen
    BeforeClose = QrPqxBeforeClose
    SQL.Strings = (
      'SELECT vfi.Nome NomeVFI, pqx.*'
      'FROM pqx pqx'
      'LEFT JOIN varfatid vfi ON vfi.Codigo=pqx.Tipo'
      'WHERE pqx.CliOrig=140'
      'AND pqx.Insumo=175'
      'AND pqx.Peso>0 '
      'AND pqx.SdoPeso>0 '
      'ORDER BY pqx.DataX DESC, pqx.Tipo DESC, pqx.OrigemCtrl DESC ')
    Left = 384
    Top = 284
    object QrPqxNomeVFI: TWideStringField
      FieldName = 'NomeVFI'
      Size = 120
    end
    object QrPqxDataX: TDateField
      FieldName = 'DataX'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPqxOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
      Required = True
    end
    object QrPqxOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
      Required = True
    end
    object QrPqxTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPqxCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPqxCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPqxInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPqxPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPqxValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPqxRetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrPqxStqMovIts: TIntegerField
      FieldName = 'StqMovIts'
      Required = True
    end
    object QrPqxAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPqxRetQtd: TFloatField
      FieldName = 'RetQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPqxHowLoad: TSmallintField
      FieldName = 'HowLoad'
      Required = True
    end
    object QrPqxStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrPqxSdoPeso: TFloatField
      FieldName = 'SdoPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPqxSdoValr: TFloatField
      FieldName = 'SdoValr'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPqxAcePeso: TFloatField
      FieldName = 'AcePeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPqxAceValr: TFloatField
      FieldName = 'AceValr'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPqxDtCorrApo: TDateField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrPqxEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPqxTmpPer: TIntegerField
      FieldName = 'TmpPer'
      Required = True
    end
    object QrPqxxLote: TWideStringField
      FieldName = 'xLote'
    end
    object QrPqxSerie: TFloatField
      FieldName = 'Serie'
    end
    object QrPqxNF: TFloatField
      FieldName = 'NF'
    end
  end
  object DsPqx: TDataSource
    DataSet = QrPqx
    Left = 384
    Top = 332
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq '
      'WHERE Ativo NOT IN (0,3)'
      'AND Codigo > 0'
      'ORDER BY Nome')
    Left = 68
    Top = 196
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 68
    Top = 248
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 120
    Top = 196
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 120
    Top = 248
  end
  object QrPqw: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPqwAfterOpen
    BeforeClose = QrPqwBeforeClose
    SQL.Strings = (
      'DROP TABLE IF EXISTS _fatid_inn_pqw;'
      'CREATE TABLE _fatid_inn_pqw'
      'SELECT pqw.OriTipo, pqw.DstTipo, pqw.DstCtrl, '
      'SUM(pqw.Peso) PesoBxa  '
      'FROM bluederm_noroeste.pqw pqw'
      'WHERE pqw.DstEmpresa=-11'
      'AND pqw.DstCliInt=183'
      'AND pqw.DstInsumo=223'
      'AND pqw.Peso <> 0'
      'GROUP BY pqw.DstCtrl, pqw.DstTipo, pqw.OriTipo'
      'ORDER BY pqw.DstCtrl, pqw.DstTipo, pqw.OriTipo'
      ';'
      'DROP TABLE IF EXISTS _fatid_inn_pqx;'
      'CREATE TABLE _fatid_inn_pqx'
      'SELECT '
      'CONCAT(Tipo, "-", OrigemCtrl) Tipo_OriCtrl,'
      'CONCAT(Serie, "-", NF) Serie_NF,'
      'DataX, OrigemCodi, OrigemCtrl, Tipo,'
      'Empresa, CliOrig, CliDest, Peso PesoInn, '
      'Serie, NF, xLote'
      'FROM bluederm_noroeste.pqx pqx'
      'WHERE pqx.Empresa=-11'
      'AND pqx.CliOrig=183'
      'AND pqx.Insumo=223'
      'AND pqx.Peso>0 '
      'AND pqx.Tipo <> 0'
      '/*AND pqx.DataX BETWEEN "2023-01-01" AND "2023-12-31"*/'
      ';'
      'SELECT '
      'IF(pqx.Tipo=10, pqe.Serie, pqx.Serie) + 0.000 Serie, '
      'IF(pqx.Tipo=10, pqe.NF, pqx.NF) + 0.000 NF, '
      'vfx.Nome NomeVFX, '
      'vfw.Nome NomeVFW, '
      'pqx.*, pqw.*'
      'FROM _fatid_inn_pqx pqx'
      'LEFT JOIN _fatid_inn_pqw pqw ON '
      '  pqw.DstTipo=pqx.Tipo'
      '  AND pqw.DstCtrl=pqx.OrigemCtrl'
      'LEFT JOIN bluederm_noroeste.pqe pqe ON pqe.Codigo=pqx.OrigemCodi'
      'LEFT JOIN bluederm_noroeste.varfatid vfx ON vfx.Codigo=pqx.Tipo'
      
        'LEFT JOIN bluederm_noroeste.varfatid vfw ON vfw.Codigo=pqw.OriTi' +
        'po'
      'ORDER BY pqx.DataX, pqx.Tipo, pqx.OrigemCtrl')
    Left = 452
    Top = 284
    object QrPqwSerie: TFloatField
      FieldName = 'Serie'
    end
    object QrPqwNF: TFloatField
      FieldName = 'NF'
    end
    object QrPqwNomeVFX: TWideStringField
      FieldName = 'NomeVFX'
      Size = 255
    end
    object QrPqwNomeVFW: TWideStringField
      FieldName = 'NomeVFW'
      Size = 255
    end
    object QrPqwDataX: TDateField
      FieldName = 'DataX'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPqwOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
      Required = True
    end
    object QrPqwOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
      Required = True
    end
    object QrPqwTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPqwEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPqwCliOrig: TIntegerField
      FieldName = 'CliOrig'
      Required = True
    end
    object QrPqwCliDest: TIntegerField
      FieldName = 'CliDest'
      Required = True
    end
    object QrPqwPesoInn: TFloatField
      FieldName = 'PesoInn'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPqwxLote: TWideStringField
      FieldName = 'xLote'
    end
    object QrPqwOriTipo: TIntegerField
      FieldName = 'OriTipo'
    end
    object QrPqwDstTipo: TIntegerField
      FieldName = 'DstTipo'
    end
    object QrPqwDstCtrl: TIntegerField
      FieldName = 'DstCtrl'
    end
    object QrPqwPesoBxa: TFloatField
      FieldName = 'PesoBxa'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPqwTipo_OriCtrl: TWideStringField
      FieldName = 'Tipo_OriCtrl'
      Required = True
      Size = 21
    end
    object QrPqwSerie_NF: TWideStringField
      FieldName = 'Serie_NF'
      Required = True
      Size = 23
    end
    object QrPqwInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPqwNO_Insumo: TWideStringField
      FieldName = 'NO_Insumo'
      Size = 50
    end
  end
  object DsPqw: TDataSource
    DataSet = QrPqw
    Left = 452
    Top = 332
  end
  object frxQUI_INSUM_011_01_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41537.466215289400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var                '
      '  LastTipoOriCtrl: String;'
      '  SomaNF, SomaPQ, SomaIQ: Double;'
      '    '
      'procedure MD001OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if LastTipoOriCtrl <> <frxDsPqw."Tipo_OriCtrl"> then'
      '  begin              '
      '    SomaNF := SomaNF + <frxDsPqw."PesoInn">;'
      '    SomaPQ := SomaPQ + <frxDsPqw."PesoInn">;'
      
        '    SomaIQ := SomaIQ + <frxDsPqw."PesoInn">;                    ' +
        '   '
      
        '    MePesoInn.Memo.Text := FormatFloat('#39'###,###,##0.000'#39', <frxDs' +
        'Pqw."PesoInn">);        '
      '  end else'
      '    MePesoInn.Memo.Text := '#39#39';        '
      '  //                     '
      '  LastTipoOriCtrl := <frxDsPqw."Tipo_OriCtrl">;    '
      'end;'
      ''
      'procedure GF001OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MeTotPesoInnNF.Memo.Text := FormatFloat('#39'###,###,##0.000'#39', Som' +
        'aNF);                              '
      '  LastTipoOriCtrl := '#39#39';    '
      '  SomaNF := 0.000;  '
      'end;'
      ''
      'procedure GF002OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MeTotPesoInnPQ.Memo.Text := FormatFloat('#39'###,###,##0.000'#39', Som' +
        'aPQ);                              '
      '  SomaPQ := 0.000;  '
      'end;'
      ''
      'procedure RS0001OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MeTotPesoInnIQ.Memo.Text := FormatFloat('#39'###,###,##0.000'#39', Som' +
        'aIQ);    '
      '  SomaIQ := 0.000;  '
      'end;'
      ''
      'begin'
      '  LastTipoOriCtrl := '#39#39';    '
      '  SomaNF := 0.000;'
      '  SomaPQ := 0.000;'
      '  SomaIQ := 0.000;                                      '
      'end.')
    OnGetValue = frxQUI_INSUM_011_01_AGetValue
    Left = 452
    Top = 432
    Datasets = <
      item
        DataSet = frxDsPqw
        DataSetName = 'frxDsPqw'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708710240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795124250000000000
          Width = 680.314960630000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590434020000000000
          Width = 64.251746380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie e N'#186' NF')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031581500000000000
          Top = 75.590434020000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde entrada')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283591500000000000
          Top = 75.590434020000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde baixa')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Entradas x Baixas de Uso e Consumo por NF')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 75.590600000000000000
          Width = 37.795036380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 75.590600000000000000
          Width = 181.417176380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Movimento da entrada')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 75.590600000000000000
          Width = 41.574566380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 325.039580000000000000
          Top = 75.590600000000000000
          Width = 41.574566380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Top = 75.590600000000000000
          Width = 181.417176380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Movimento da entrada')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118276220000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MD001OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPqw
        DataSetName = 'frxDsPqw'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 124.724455830000000000
          Width = 158.740157480000000000
          Height = 15.118110240000000000
          DataField = 'NomeVFX'
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPqw."NomeVFX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePesoInn: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031586380000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283459690000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'PesoBxa'
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPqw."PesoBxa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 18.897615830000000000
          Height = 15.118110240000000000
          DataField = 'Serie'
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPqw."Serie"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 45.354325830000000000
          Height = 15.118110240000000000
          DataField = 'NF'
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPqw."NF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 0.000165979999999996
          Width = 37.795265830000000000
          Height = 15.118110240000000000
          DataField = 'DataX'
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPqw."DataX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 0.000165979999999996
          Width = 22.677145830000000000
          Height = 15.118110240000000000
          DataField = 'Tipo'
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPqw."Tipo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 0.000165979999999996
          Width = 41.574795830000000000
          Height = 15.118110240000000000
          DataField = 'OrigemCodi'
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPqw."OrigemCodi"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 325.039580000000000000
          Top = 0.000165979999999996
          Width = 41.574795830000000000
          Height = 15.118110240000000000
          DataField = 'OrigemCtrl'
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPqw."OrigemCtrl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291555830000000000
          Width = 158.740157480000000000
          Height = 15.118110240000000000
          DataField = 'NomeVFW'
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPqw."NomeVFW"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Top = 0.000165979999999996
          Width = 22.677145830000000000
          Height = 15.118110240000000000
          DataField = 'OriTipo'
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPqw."OriTipo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPqw."Serie_NF"'
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Width = 653.669450000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'S'#233'rie: [frxDsPqw."Serie"]  N'#186' NF [frxDsPqw."NF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GF001: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GF001OnBeforePrint'
        object MeTotPesoInnNF: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283723310000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPqw."PesoBxa">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Width = 517.606370000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Total da S'#233'rie: [frxDsPqw."Serie"]  N'#186' NF [frxDsPqw."NF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 18.897640240000000000
        ParentFont = False
        Top = 170.078850000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPqw."Insumo"'
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Width = 680.126160000000000000
          Height = 18.897640240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Insumo: [frxDsPqw."Insumo"] - [frxDsPqw."NO_Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GF002: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GF002OnBeforePrint'
        object MeTotPesoInnPQ: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283723310000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPqw."PesoBxa">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Width = 544.063080000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Total do Insumo: [frxDsPqw."Insumo"] - [frxDsPqw."NO_Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object RS0001: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 411.968770000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'RS0001OnBeforePrint'
        object MeTotPesoInnIQ: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 3.779530000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283723310000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPqw
          DataSetName = 'frxDsPqw'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPqw."PesoBxa">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 544.063080000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Total no per'#237'odo do Cliente Interno: [VARF_CLIINT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPqw: TfrxDBDataset
    UserName = 'frxDsPqw'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Serie=Serie'
      'NF=NF'
      'NomeVFX=NomeVFX'
      'NomeVFW=NomeVFW'
      'DataX=DataX'
      'OrigemCodi=OrigemCodi'
      'OrigemCtrl=OrigemCtrl'
      'Tipo=Tipo'
      'Empresa=Empresa'
      'CliOrig=CliOrig'
      'CliDest=CliDest'
      'PesoInn=PesoInn'
      'xLote=xLote'
      'OriTipo=OriTipo'
      'DstTipo=DstTipo'
      'DstCtrl=DstCtrl'
      'PesoBxa=PesoBxa'
      'Tipo_OriCtrl=Tipo_OriCtrl'
      'Serie_NF=Serie_NF'
      'Insumo=Insumo'
      'NO_Insumo=NO_Insumo')
    DataSet = QrPqw
    BCDToCurrency = False
    DataSetOptions = []
    Left = 452
    Top = 381
  end
end
