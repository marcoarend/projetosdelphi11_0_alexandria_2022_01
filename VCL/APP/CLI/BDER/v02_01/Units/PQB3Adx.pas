unit PQB3Adx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnInternalConsts,
  UnMsgInt, ExtCtrls, UnInternalConsts2, mySQLDbTables, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, UMySQLModule, dmkGeral, dmkLabel, dmkImage, UnDmkEnums,
  UnDmkProcFunc, DmkDAC_PF, Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmPQB3Adx = class(TForm)
    PainelDados: TPanel;
    LaInsumo: TLabel;
    Label4: TLabel;
    EdQtde: TdmkEdit;
    Label10: TLabel;
    EdPreco: TdmkEdit;
    Label6: TLabel;
    EdVTota: TdmkEdit;
    QrPQ: TmySQLQuery;
    DsPQ: TDataSource;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    LaCliente: TLabel;
    QrCI: TmySQLQuery;
    QrCINome: TWideStringField;
    QrCICodigo: TIntegerField;
    DsCI: TDataSource;
    EdInsumo: TdmkEditCB;
    CBInsumo: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    LaEmpresa: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrSaldo: TMySQLQuery;
    QrSaldoPeso: TFloatField;
    QrSaldoValor: TFloatField;
    QrSaldoCUSTO: TFloatField;
    QrSaldoCI: TIntegerField;
    QrSaldoPQ: TIntegerField;
    DsSaldo: TDataSource;
    Label12: TLabel;
    EdxLote: TdmkEdit;
    Edide_nNF: TdmkEdit;
    DBEdit9: TDBEdit;
    EdSaldoFut: TdmkEdit;
    Label20: TLabel;
    Label19: TLabel;
    DBEdit8: TDBEdit;
    Label18: TLabel;
    Label17: TLabel;
    DBEdit7: TDBEdit;
    Edide_serie: TdmkEdit;
    Label1: TLabel;
    TPdFab: TdmkEditDateTimePicker;
    TPdVal: TdmkEditDateTimePicker;
    Label3: TLabel;
    Label5: TLabel;
    Qry: TMySQLQuery;
    SpeedButton1: TSpeedButton;
    Label2: TLabel;
    Label7: TLabel;
    TPDataX: TdmkEditDateTimePicker;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdQtdeExit(Sender: TObject);
    procedure EdPrecoExit(Sender: TObject);
    procedure EdVTotaExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdInsumoEnter(Sender: TObject);
    procedure EdClienteEnter(Sender: TObject);
    procedure EdInsumoExit(Sender: TObject);
    procedure EdClienteExit(Sender: TObject);
    procedure EdInsumoChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdClienteRedefinido(Sender: TObject);
    procedure EdInsumoRedefinido(Sender: TObject);
    procedure EdQtdeRedefinido(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    FPQ, FCI, FEmpresa: Integer;
    procedure BuscaPrecoCadastro();
    procedure CalculaOnEdit(Tipo: Integer);
    procedure CalculaSaldoFuturo();
  public
    { Public declarations }
    FBalanco, FControle: Integer;
  end;

var
  FmPQB3Adx: TFmPQB3Adx;

implementation

uses UnMyObjects, Module, PQB3, PQB3AdSel, PQx, UnPQ_PF, ModuleGeral, MyDBCheck;

{$R *.DFM}


procedure TFmPQB3Adx.CalculaOnEdit(Tipo: Integer);
var
  Qtde, Preco, VTota: Double;
begin
  Qtde := Geral.DMV(EdQtde.Text);
  Preco := Geral.DMV(EdPreco.Text);
  VTota := Geral.DMV(EdVTota.Text);
  //
  if Tipo <> 1 then VTota := Qtde * Preco
  else if Qtde <> 0 then Preco := VTota / Qtde
  else Preco := 0;
  //
  EdQtde.ValueVariant := Qtde;
  EdPreco.ValueVariant := Preco;
  EdVTota.ValueVariant := VTota;
end;

procedure TFmPQB3Adx.CalculaSaldoFuturo();
var
  CI, PQ: Integer;
  Qtde, SaldoFut: Double;
begin
  CI := Geral.IMV(EdCliente.Text);
  PQ := Geral.IMV(EdInsumo.Text);
  UnDmkDAC_PF.AbreMySQLQuery0(QrSaldo, Dmod.MyDB, [
  'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO ',
  'FROM pqcli ',
  'WHERE CI=' + Geral.FF0(CI),
  'AND PQ=' + Geral.FF0(PQ),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  '']);
  //
  Qtde := Geral.DMV(EdQtde.Text);
  //SaldoFut := QrSaldoPeso.Value - Qtde;
  SaldoFut := QrSaldoPeso.Value + Qtde;
  EdSaldoFut.Text := Geral.FFT(SaldoFut, 3, siNegativo);
  //
  // Buscar Pre�o do estoque e n�o do cadastro!
  if ImgTipo.SQLType = stIns then
    EdPreco.ValueVariant := QrSaldoCUSTO.Value;
end;

procedure TFmPQB3Adx.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQB3Adx.BuscaPrecoCadastro();
begin
  // Buscar pre�o do estoque e n�o do cadastro!
  EXIT;
  //
  if ImgTipo.SQLType = stIns then
  begin
    FPQ := EdInsumo.ValueVariant;
    FCI := EdCliente.ValueVariant;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT CustoPadrao FROM pqcli');
    Dmod.QrAux.SQL.Add('WHERE PQ=:P0');
    Dmod.QrAux.SQL.Add('AND CI=:P1');
    Dmod.QrAux.Params[00].AsInteger := FPQ;
    Dmod.QrAux.Params[01].AsInteger := FCI;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    EdPreco.ValueVariant := Dmod.QrAux.FieldByName('CustoPadrao').AsFloat;
    CalculaOnEdit(0);
    //
  end;
end;

procedure TFmPQB3Adx.BtConfirmaClick(Sender: TObject);
var
  DtCorrApo, xLote, dFab, dVal: String;
  Codigo, Controle, Empresa, CliInt, Insumo, ide_serie, ide_nNF, HowLoad: Integer;
  Peso, Valor: Double;
  SQLType: TSQLType;
  //
  Cliente: Integer;
  Qtde, VTota: Double;
  //
  DataX: String;
  Preco, SaldoFut: Double;
  CliOrig, CliDest, OriCodi, OriCtrl, OriTipo: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FBalanco;
  Controle       := FControle;
  Empresa        := 0; // Abaixo
  Cliente        := Geral.IMV(EdCliente.Text);
  CliInt         := Cliente;
  Insumo         := Geral.IMV(EdInsumo.Text);
  Qtde           := Geral.DMV(EdQtde.Text);
  Peso           := Qtde;
  VTota          := Geral.DMV(EdVTota.Text);
  Valor          := VTota;
  ide_serie      := Edide_serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
  DtCorrApo      := Geral.FDT(0, 1);
  xLote          := EdxLote.Text;
  dFab           := Geral.FDT(TPdFab.Date, 1);
  dVal           := Geral.FDT(TPdVal.Date, 1);
  HowLoad        := 0;
  //
  //DataX := dmkPF.PrimeiroDiaDoPeriodo(FmPQB3.QrBalancosPeriodo.Value, dtSystem);
  DataX := Geral.FDT(TPDataX.Date, 1);
  Preco := Geral.DMV(EdPreco.Text);
  SaldoFut := EdSaldoFut.ValueVariant;
  //
  if MyObjects.FIC((Preco <= 0) and (Cliente < 1), EdPreco,
    'Pre�o n�o pode ser negativo para este cliente interno.') then Exit;
  if MyObjects.FIC((VTota = 0) and (Cliente < 1), EdPreco,
    'Defina o valor.') then Exit;
  if MyObjects.FIC(Insumo = 0, EdInsumo, 'Defina o insumo.') then Exit;
  if MyObjects.FIC(Qtde <= 0, EdQtde, 'Quantidade inv�lida!') then Exit;
  if MyObjects.FIC(ide_nNF = 0, Edide_nNF, 'Informe a NF (VP)!') then Exit;
  if MyObjects.FIC(xLote = EmptyStr, EdxLote, 'Informe o Lote!') then Exit;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    OriCodi := FBalanco;
    Codigo  := FBalanco;
    OriTipo := VAR_FATID_0020;
    CliOrig := Cliente;
    CliDest := Cliente;
    CLiInt  := Cliente;
    //Insumo  := Insumo;
    Peso    := Qtde;
    Valor   := VTota;
    //
    Controle := UMyMod.BPGS1I32('pqbadx', 'Controle', '', '', tsPos, SQLType, FControle);
    OriCtrl := Controle;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqbadx', False, [
    'Codigo', 'Empresa', 'CliInt',
    'Insumo', 'Peso', 'Valor',
    'ide_serie', 'ide_nNF', 'DtCorrApo',
    'xLote', 'dFab', 'dVal',
    'HowLoad'], [
    'Controle'], [
    Codigo, Empresa, CliInt,
    Insumo, Peso, Valor,
    ide_serie, ide_nNF, DtCorrApo,
    xLote, dFab, dVal,
    HowLoad], [
    Controle], True) then
    begin
      if ImgTipo.SQLType = stUpd then
        PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
      //
      PQ_PF.InserePQx_Inn(Qry, DataX, CliOrig, CliDest, Insumo, Peso, Valor,
        HowLoad, OriCodi, OriCtrl, OriTipo, DtCorrApo, Empresa,
        ide_serie, ide_nNF, xLote, dFab, dVal);
    end;
  except
    Screen.Cursor := Cursor;
    Exit;
  end;
  if CkContinuar.Checked then
  begin
    ImgTipo.SQLType        := stIns;
    FPQ                    := 0;
    FCI                    := 0;
    LaInsumo.Enabled       := True;
    EdInsumo.Enabled       := True;
    CBInsumo.Enabled       := True;
    EdInsumo.ValueVariant  := 0;
    CBInsumo.KeyValue      := 0;
    EdPreco.ValueVariant   := 0;
    EdQtde.ValueVariant   := 0;
    //
    CalculaOnEdit(0);
    EdInsumo.SetFocus;
    //FmPQB3.SomaBalanco();
    FmPQB3.ReopenPQBAdx(Controle);
    Screen.Cursor := crDefault;
  end else Close;
end;

procedure TFmPQB3Adx.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CalculaOnEdit(1);
  EdPreco.SetFocus;
  EdQtde.SetFocus;
  if ImgTipo.SQLType = stIns then
  begin
    if EdCliente.ValueVariant = 0 then
      EdCliente.SetFocus
    else
      EdInsumo.SetFocus;
  end;
end;

procedure TFmPQB3Adx.EdClienteChange(Sender: TObject);
begin
  if not EdCliente.Focused then
    BuscaPrecoCadastro();
end;

procedure TFmPQB3Adx.EdClienteEnter(Sender: TObject);
begin
  FCI := EdCliente.ValueVariant;
end;

procedure TFmPQB3Adx.EdClienteExit(Sender: TObject);
begin
  if FCI <> EdCliente.ValueVariant then
    BuscaPrecoCadastro();
end;

procedure TFmPQB3Adx.EdClienteRedefinido(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmPQB3Adx.EdEmpresaRedefinido(Sender: TObject);
var
  Filial: Integer;
begin
  Filial := EdEmpresa.ValueVariant;
  FEmpresa := DModG.ObtemEntidadeDeFilial(Filial);
end;

procedure TFmPQB3Adx.EdInsumoChange(Sender: TObject);
begin
  if not EdInsumo.Focused then
    BuscaPrecoCadastro();
end;

procedure TFmPQB3Adx.EdInsumoEnter(Sender: TObject);
begin
  FPQ := EdInsumo.ValueVariant;
end;

procedure TFmPQB3Adx.EdInsumoExit(Sender: TObject);
begin
  if FPQ <> EdInsumo.ValueVariant then
    BuscaPrecoCadastro();
end;

procedure TFmPQB3Adx.EdInsumoRedefinido(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmPQB3Adx.EdQtdeExit(Sender: TObject);
begin
  CalculaOnEdit(0);
end;

procedure TFmPQB3Adx.EdQtdeRedefinido(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmPQB3Adx.EdPrecoExit(Sender: TObject);
begin
  CalculaOnEdit(0);
end;

procedure TFmPQB3Adx.EdVTotaExit(Sender: TObject);
begin
  CalculaOnEdit(1);
end;

procedure TFmPQB3Adx.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQB3Adx.SpeedButton1Click(Sender: TObject);
var
  Filial, Empresa, CliInt, Insumo: Integer;
begin
  Filial  := EdEmpresa.ValueVariant;
  Empresa := DModG.ObtemEntidadeDeFilial(Filial);
  CliInt  := EdCliente.ValueVariant;
  Insumo  := EdInsumo.ValueVariant;
  //
  if DBCheck.CriaFm(TFmPQB3AdSel, FmPQB3AdSel, afmoNegarComAviso) then
  begin
    FmPQB3AdSel.EdEmpresa.ValueVariant := Empresa;
    FmPQB3AdSel.EdNO_Empresa.Text      := CBEmpresa.Text;
    FmPQB3AdSel.EdCliInt.ValueVariant  := CliInt;
    FmPQB3AdSel.EdNO_CliInt.Text       := CBCliente.Text;
    FmPQB3AdSel.EdInsumo.ValueVariant  := Insumo;
    FmPQB3AdSel.EdNO_Insumo.Text       := CBInsumo.Text;
    //
    FmPQB3AdSel.ReopenPqx(Empresa, CliInt, Insumo);
    //
    FmPQB3AdSel.ShowModal;
    //
    if FmPQB3AdSel.FSelecionou = True then
    begin
      Edide_serie.ValueVariant := FmPQB3AdSel.QrPqxSerie.Value;
      Edide_nNF.ValueVariant   := FmPQB3AdSel.QrPqxNF.Value;
      EdxLote.ValueVariant     := FmPQB3AdSel.QrPqxxLote.Value;
      EdPreco.ValueVariant     := FmPQB3AdSel.QrPqxCustoUnit.Value;
      TPdFab.Date              := FmPQB3AdSel.QrPqxdFab.Value;
      TPdVal.Date              := FmPQB3AdSel.QrPqxdVal.Value;
    end;
    //
    FmPQB3AdSel.Destroy;
  end;
end;

procedure TFmPQB3Adx.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TPdFab.Date := 0;
  TPdVal.Date := 0;
  //
  TPDataX.Date := DModG.ObtemAgora();
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
end;

end.
