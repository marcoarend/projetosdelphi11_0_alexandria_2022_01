object FmTintasFlu: TFmTintasFlu
  Left = 339
  Top = 185
  Caption = 'QUI-RECEI-104 :: Fluxo de Receita de Acabamento'
  ClientHeight = 434
  ClientWidth = 602
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 602
    Height = 278
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 602
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdNumero
      end
      object Label3: TLabel
        Left = 72
        Top = 4
        Width = 55
        Height = 13
        Caption = 'Refer'#234'ncia:'
        FocusControl = DBEdit1
      end
      object Label2: TLabel
        Left = 156
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label27: TLabel
        Left = 12
        Top = 44
        Width = 166
        Height = 13
        Caption = 'Cliente interno (Dono dos insumos):'
      end
      object DBEdNumero: TdmkDBEdit
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Numero'
        DataSource = FmTintasCab.DsTintasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdCampo = 'Numero'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 72
        Top = 20
        Width = 80
        Height = 21
        DataField = 'TxtUsu'
        DataSource = FmTintasCab.DsTintasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object DBEdNome: TDBEdit
        Left = 156
        Top = 20
        Width = 429
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = FmTintasCab.DsTintasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object DBEdit15: TDBEdit
        Left = 12
        Top = 60
        Width = 573
        Height = 21
        DataField = 'NOMECLII'
        DataSource = FmTintasCab.DsTintasCab
        TabOrder = 3
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 88
      Width = 602
      Height = 190
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 12
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label5: TLabel
        Left = 96
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Ordem:'
      end
      object Label6: TLabel
        Left = 12
        Top = 84
        Width = 50
        Height = 13
        Caption = 'Opera'#231#227'o:'
      end
      object Label7: TLabel
        Left = 504
        Top = 44
        Width = 71
        Height = 13
        Caption = 'g/m'#178' consumo:'
      end
      object Label8: TLabel
        Left = 160
        Top = 4
        Width = 47
        Height = 13
        Caption = 'Processo:'
      end
      object Label9: TLabel
        Left = 420
        Top = 44
        Width = 59
        Height = 13
        Caption = 'g/m'#178' carga*:'
      end
      object Label10: TLabel
        Left = 12
        Top = 124
        Width = 91
        Height = 13
        Caption = 'Descri'#231#227'o do fluxo:'
      end
      object EdOrdem: TdmkEdit
        Left = 96
        Top = 20
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Ordem'
        UpdCampo = 'Ordem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 12
        Top = 100
        Width = 573
        Height = 21
        MaxLength = 100
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 20
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utIdx
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CkContinuar: TCheckBox
        Left = 12
        Top = 168
        Width = 121
        Height = 17
        Caption = 'Continuar inserindo.'
        Checked = True
        State = cbChecked
        TabOrder = 9
      end
      object EdGramasM2: TdmkEdit
        Left = 504
        Top = 60
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'GramasM2'
        UpdCampo = 'GramasM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdTintasTin: TdmkEditCB
        Left = 160
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TintasTin'
        UpdCampo = 'TintasTin'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTintasTin
        IgnoraDBLookupComboBox = False
      end
      object CBTintasTin: TdmkDBLookupComboBox
        Left = 216
        Top = 20
        Width = 369
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTintasTin
        TabOrder = 3
        dmkEditCB = EdTintasTin
        QryCampo = 'TintasTin'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdInfoCargM2: TdmkEdit
        Left = 420
        Top = 60
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'InfoCargM2'
        UpdCampo = 'InfoCargM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdDescri: TdmkEdit
        Left = 12
        Top = 140
        Width = 573
        Height = 21
        MaxLength = 255
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Descri'
        UpdCampo = 'Descri'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGOpProc: TdmkRadioGroup
        Left = 12
        Top = 44
        Width = 405
        Height = 37
        Caption = ' Tipo: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o informado'
          'Opera'#231#227'o'
          'Processo')
        TabOrder = 4
        OnClick = RGOpProcClick
        QryCampo = 'OpProc'
        UpdCampo = 'OpProc'
        UpdType = utYes
        OldValor = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 602
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 554
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 506
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 401
        Height = 32
        Caption = 'Fluxo de Receita de Acabamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 401
        Height = 32
        Caption = 'Fluxo de Receita de Acabamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 401
        Height = 32
        Caption = 'Fluxo de Receita de Acabamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 326
    Width = 602
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 598
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 192
        Height = 16
        Caption = '* g/m'#178' carga*: Apenas informativo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 192
        Height = 16
        Caption = '* g/m'#178' carga*: Apenas informativo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 370
    Width = 602
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 598
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 454
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrTintasTin: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tintastin'
      'WHERE Numero=:P0'
      'ORDER BY Nome')
    Left = 472
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTintasTinCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTintasTinNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsTintasTin: TDataSource
    DataSet = QrTintasTin
    Left = 500
    Top = 4
  end
end
