unit PQporNF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, dmkEditDateTimePicker, Variants, frxClass, frxDBSet;

type
  TFmPQporNF = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPqx: TMySQLQuery;
    DsPqx: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    QrPQ: TMySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    LaPQ: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    QrCI: TMySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    DsCI: TDataSource;
    LaCI: TLabel;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    Label4: TLabel;
    TPIni: TdmkEditDateTimePicker;
    Label5: TLabel;
    TPFim: TdmkEditDateTimePicker;
    QrPqw: TMySQLQuery;
    DsPqw: TDataSource;
    QrPqxNomeVFI: TWideStringField;
    QrPqxDataX: TDateField;
    QrPqxOrigemCodi: TIntegerField;
    QrPqxOrigemCtrl: TIntegerField;
    QrPqxTipo: TIntegerField;
    QrPqxCliOrig: TIntegerField;
    QrPqxCliDest: TIntegerField;
    QrPqxInsumo: TIntegerField;
    QrPqxPeso: TFloatField;
    QrPqxValor: TFloatField;
    QrPqxRetorno: TSmallintField;
    QrPqxStqMovIts: TIntegerField;
    QrPqxAtivo: TSmallintField;
    QrPqxRetQtd: TFloatField;
    QrPqxHowLoad: TSmallintField;
    QrPqxStqCenLoc: TIntegerField;
    QrPqxSdoPeso: TFloatField;
    QrPqxSdoValr: TFloatField;
    QrPqxAcePeso: TFloatField;
    QrPqxAceValr: TFloatField;
    QrPqxDtCorrApo: TDateField;
    QrPqxEmpresa: TIntegerField;
    QrPqxTmpPer: TIntegerField;
    QrPqxxLote: TWideStringField;
    Label40: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrPqxSerie: TFloatField;
    QrPqxNF: TFloatField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel5: TPanel;
    DBGPqx: TDBGrid;
    Panel6: TPanel;
    BtReabre: TBitBtn;
    BtAltera: TBitBtn;
    Panel7: TPanel;
    ReabrPqw: TBitBtn;
    BtImprime: TBitBtn;
    DBGrid1: TDBGrid;
    QrPqwSerie: TFloatField;
    QrPqwNF: TFloatField;
    QrPqwNomeVFX: TWideStringField;
    QrPqwNomeVFW: TWideStringField;
    QrPqwDataX: TDateField;
    QrPqwOrigemCodi: TIntegerField;
    QrPqwOrigemCtrl: TIntegerField;
    QrPqwTipo: TIntegerField;
    QrPqwEmpresa: TIntegerField;
    QrPqwCliOrig: TIntegerField;
    QrPqwCliDest: TIntegerField;
    QrPqwPesoInn: TFloatField;
    QrPqwxLote: TWideStringField;
    QrPqwOriTipo: TIntegerField;
    QrPqwDstTipo: TIntegerField;
    QrPqwDstCtrl: TIntegerField;
    QrPqwPesoBxa: TFloatField;
    frxQUI_INSUM_011_01_A: TfrxReport;
    frxDsPqw: TfrxDBDataset;
    QrPqwTipo_OriCtrl: TWideStringField;
    QrPqwSerie_NF: TWideStringField;
    QrPqwInsumo: TIntegerField;
    QrPqwNO_Insumo: TWideStringField;
    BtCorrigePqw: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdCIRedefinido(Sender: TObject);
    procedure EdPQRedefinido(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrPqxAfterOpen(DataSet: TDataSet);
    procedure QrPqxBeforeClose(DataSet: TDataSet);
    procedure ReabrPqwClick(Sender: TObject);
    procedure QrPqwAfterOpen(DataSet: TDataSet);
    procedure QrPqwBeforeClose(DataSet: TDataSet);
    procedure frxQUI_INSUM_011_01_AGetValue(const VarName: string;
      var Value: Variant);
    procedure BtImprimeClick(Sender: TObject);
    procedure BtCorrigePqwClick(Sender: TObject);
  private
    { Private declarations }
    procedure FechaPesquisa();
    procedure ReopenPqx(OrigemCodi, OrigemCtrl, Tipo: Integer);
  public
    { Public declarations }
  end;

  var
  FmPQporNF: TFmPQporNF;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UnDmkProcFunc, PQB3AdEdit,
  UMySQLModule, UnApp_Jan, MyDBCheck;

{$R *.DFM}

procedure TFmPQporNF.BtAlteraClick(Sender: TObject);
const
  sProcName = 'TFmPQporNF.BtAlteraCabClick()';
  //
  procedure ConfirmouClick(ide_serie, ide_nNF: Integer; xLote, dFab, dVal:
  String);
  var
    Controle, Tipo, Serie, NF, Codigo: Integer;
    SQLType: TSQLType;
    OriCodi, OriCtrl, OriTipo: Integer;
  begin
    SQLType        := stUpd;
    OriCodi        := QrPqxOrigemCodi.Value;
    Codigo         := OriCodi;
    Controle       := QrPqxOrigemCtrl.Value;
    OriCtrl        := Controle;
    OriTipo        := QrPqxTipo.Value;
    Tipo           := OriTipo;
    Serie          := ide_serie;
    NF             := ide_nNF;
    //
    Cursor := Screen.Cursor;
    Screen.Cursor := crHourglass;
    try
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqx', False, [
      'NF', 'xLote', 'Serie'], [
      'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
      NF, xLote, Serie], [
      OriCodi, OriCtrl, Tipo], False) then
      begin
        case Tipo of
          VAR_FATID_0010:
          begin
            if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqe', False, [
            'serie', 'NF'], ['Codigo'], [
            Serie, NF], [Codigo], True) then
            begin
              if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqeits', False, [
              'xLote'], ['Controle'], [xLote], [Controle], True) then
              begin
                //
              end;
            end;
          end;
          VAR_FATID_0020:
          begin
            if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqbadx', False, [
            'ide_serie', 'ide_nNF', 'xLote'], [
            'Controle'], [
            ide_serie, ide_nNF, xLote], [
            Controle], True) then
            begin
              //
            end;
          end;
          else
            Geral.MB_Erro('O Tipo ' + Geral.FF0(Tipo) +
            ' n�o foi implementado em ' + sProcName);
        end;
      end;
    except
      Screen.Cursor := Cursor;
      Exit;
    end;
  end;
const
  MostradFabdVal = False;
var
  I, Empresa, CliInt, Insumo: Integer;
  NO_Empresa, NO_CliInt, NO_Insumo: String;
  ide_Serie, ide_nNF: Integer;
  xLote: String;
  //dFab, dVal: TDateTime;
begin
  Empresa    := QrPqxEmpresa.Value;
  CliInt     := QrPqxCliOrig.Value;
  Insumo     := QrPqxInsumo.Value;
  NO_Empresa := CBEmpresa.Text;
  NO_CliInt  := CBCI.Text;
  NO_Insumo  := CBPQ.Text;
  ide_Serie  := Trunc(QrPqxSerie.Value);
  ide_nNF    := Trunc(QrPqxNF.Value);
  xLote      := QrPqxxLote.Value;
  //dFab       := QrPqxdFab.Value;
  //dVal       := QrPqxdVal.Value;
  //
  if App_Jan.MostraFormPQB3AdEdit(Empresa, CliInt, Insumo, NO_Empresa,
  NO_CliInt, NO_Insumo, ide_Serie, ide_nNF, xLote, (*dFab, dVal*)0, 0,
  MostradFabdVal) then
  begin
    if DBGPqx.SelectedRows.Count < 2 then
      ConfirmouClick(FmPQB3AdEdit.Fide_serie, FmPQB3AdEdit.Fide_nNF,
      FmPQB3AdEdit.FxLote, FmPQB3AdEdit.FdFab, FmPQB3AdEdit.FdVal)
    else
    begin
      try
        DBGPqx.Enabled := True;
        QrPqx.DisableControls;
        with DBGPqx.DataSource.DataSet do
        for I := 0 to DBGPqx.SelectedRows.Count-1 do
        begin
          GotoBookmark(DBGPqx.SelectedRows.Items[I]);
          //
          ConfirmouClick(FmPQB3AdEdit.Fide_serie, FmPQB3AdEdit.Fide_nNF,
          FmPQB3AdEdit.FxLote, FmPQB3AdEdit.FdFab, FmPQB3AdEdit.FdVal);
          //
          //PB1.Position := PB1.Position + 1;
          //PB1.Update;
          Application.ProcessMessages;
        end;
      finally
        QrPqx.EnableControls;
        //
        //PB1.Visible      := False;
        DBGPqx.Enabled := True;
        Screen.Cursor    := crDefault;
      end;
    end;
    //
    FmPQB3AdEdit.Destroy;
    ReopenPqx(QrPqxOrigemCodi.Value,  QrPqxOrigemCtrl.Value,  QrPqxTipo.Value);
    //
    QrPqx.EnableControls;
    DBGPqx.Enabled := True;
    Screen.Cursor  := crDefault;
  end;
end;

procedure TFmPQporNF.BtCorrigePqwClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaMaster() then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
    'UPDATE pqw ',
    'LEFT JOIN pqx ON  ',
    '      pqw.OriCodi = pqx.OrigemCodi ',
    '  AND pqw.OriCtrl = pqx.OrigemCtrl ',
    ' ',
    'SET pqw.OriInsumo = pqx.Insumo,  ',
    '    pqw.DstInsumo = pqx.Insumo,  ',
    '    pqw.OriCliInt = pqx.CliOrig, ',
    '    pqw.OriEmpresa = pqx.Empresa, ',
    '    pqw.DstInsumo = pqx.Insumo,  ',
    '    pqw.DstCliInt = pqx.CliDest, ',
    '    pqw.DstEmpresa = pqx.Empresa,  ',
    '    pqw.DstValr=-pqx.Valor ',
    'WHERE pqw.OriInsumo=0 ',
    '']));
    //
    Geral.MB_Info('Atualiza��o finalizada!');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPQporNF.BtImprimeClick(Sender: TObject);
begin
  QrPqw.DisableControls;
  try
    MyObjects.FrxDefineDatasets(frxQUI_INSUM_011_01_A, [
    frxDsPqw
    ]);
    //
    MyObjects.FrxMostra(frxQUI_INSUM_011_01_A,
    'Entradas x Baixas de Uso e Consumo por NF');
  finally
    QrPqw.EnableControls;
  end;
end;

procedure TFmPQporNF.BtReabreClick(Sender: TObject);
begin
  ReopenPqx(0, 0, 0);
end;

procedure TFmPQporNF.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQporNF.EdCIRedefinido(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPQporNF.EdPQRedefinido(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPQporNF.FechaPesquisa();
begin
  QrPQX.Close;
end;

procedure TFmPQporNF.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQporNF.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 0;
  //
  Agora := Int(DModG.ObtemAgora());
  TPIni.Date := Agora - 365;
  TPFim.Date := Agora;
  //
  UnDMkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  //CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmPQporNF.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQporNF.frxQUI_INSUM_011_01_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName = 'VARF_CLIINT' then
    Value := dmkPF.ParValueCodTxt(
      'Cliente interno: ', CBCI.Text, EdCI.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_INSUMO' then
    Value := dmkPF.ParValueCodTxt(
      'Insumo: ', CBPQ.Text, EdPQ.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp1(
    TPIni.Date, TPFim.Date, True, True, 'Per�odo: ', '', '')
  else
end;

procedure TFmPQporNF.QrPqwAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := QrPqw.RecordCount > 0;
end;

procedure TFmPQporNF.QrPqwBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmPQporNF.QrPqxAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrPQX.RecordCount > 0;
end;

procedure TFmPQporNF.QrPqxBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
end;

procedure TFmPQporNF.ReabrPqwClick(Sender: TObject);
var
  Filial, Empresa, CliOrig, Insumo: Integer;
  SQLPeriodo, SQL_InsumoPqw, SQL_InsumoPqx : String;
begin
  Filial  := EdEmpresa.ValueVariant;
  Empresa := DModG.ObtemEntidadeDeFilial(Filial);
  CliOrig := EdCI.ValueVariant;
  Insumo  := EdPQ.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  if MyObjects.FIC(CliOrig = 0, EdCI, 'Informe o cliente interno!') then Exit;
  if MyObjects.FIC(Insumo = 0, EdPQ, 'Pesquisa sem informar o insumo popde ser demorada!') then ; //Exit;
  //
  SQLPeriodo := dmkPF.SQL_Periodo('AND pqx.DataX ', TPIni.Date, TPFim.Date, True, True);
  if Insumo <> 0 then
  begin
    SQL_InsumoPqw := 'AND pqw.OriInsumo=' + Geral.FF0(Insumo);
    SQL_InsumoPqx := 'AND pqx.Insumo=' + Geral.FF0(Insumo);
  end else
  begin
    SQL_InsumoPqw := '';
    SQL_InsumoPqx := '';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQUery0(QrPQW, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _fatid_inn_pqw;',
  'CREATE TABLE _fatid_inn_pqw',
  'SELECT pqw.OriTipo, pqw.DstTipo, pqw.DstCtrl, ',
  'SUM(pqw.Peso) PesoBxa  ',
  'FROM ' + TMeuDB + '.pqw pqw',
  'WHERE pqw.OriEmpresa=' + Geral.FF0(Empresa),
  'AND pqw.OriCliInt=' + Geral.FF0(CliOrig),
   SQL_InsumoPqw,
  'AND pqw.Peso <> 0',
  'GROUP BY pqw.DstCtrl, pqw.DstTipo, pqw.OriTipo',
  'ORDER BY pqw.DstCtrl, pqw.DstTipo, pqw.OriTipo',
  ';',
  'DROP TABLE IF EXISTS _fatid_inn_pqx;',
  'CREATE TABLE _fatid_inn_pqx',
  'SELECT DataX, OrigemCodi, OrigemCtrl, Tipo,',
  'Empresa, CliOrig, CliDest, Peso PesoInn, ',
  'Insumo, Serie, NF, xLote ', //, ',
  //'CONCAT(Tipo, "-", OrigemCtrl) Tipo_OriCtrl, ',
  //'CONCAT(Serie, "-", NF) Serie_NF ',
  'FROM ' + TMeuDB + '.pqx pqx',
  'WHERE pqx.Empresa=' + Geral.FF0(Empresa),
  'AND pqx.CliOrig=' + Geral.FF0(CliOrig),
   SQL_InsumoPqx,
  'AND pqx.Peso>0 ',
  'AND pqx.Tipo <> ' + Geral.FF0(VAR_FATID_0000),
  SQLPeriodo,
  ';',
  'SELECT pq.Nome NO_Insumo, ',
  'IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0010) + ', pqe.Serie, pqx.Serie) + 0.000 Serie, ',
  'IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0010) + ', pqe.NF, pqx.NF) + 0.000 NF, ',
   //
  'CONCAT(Tipo, "-", OrigemCtrl) Tipo_OriCtrl, ',
  'CONCAT( ',
  '  IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0010) + ', pqe.Serie, pqx.Serie), ',
  '  "-", ',
  '  IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0010) + ', pqe.NF, pqx.NF) ',
  ') Serie_NF, ',
   //
  'vfx.Nome NomeVFX, ',
  'vfw.Nome NomeVFW, ',
  'pqx.*, pqw.*',
  'FROM _fatid_inn_pqx pqx',
  'LEFT JOIN _fatid_inn_pqw pqw ON ',
  '  pqw.DstTipo=pqx.Tipo',
  '  AND pqw.DstCtrl=pqx.OrigemCtrl',
  'LEFT JOIN ' + TMeuDB + '.pqe pqe ON pqe.Codigo=pqx.OrigemCodi',
  'LEFT JOIN ' + TMeuDB + '.varfatid vfx ON vfx.Codigo=pqx.Tipo',
  'LEFT JOIN ' + TMeuDB + '.varfatid vfw ON vfw.Codigo=pqw.OriTipo',
  'LEFT JOIN ' + TMeuDB + '.pq pq ON pq.Codigo=pqx.Insumo',
  //'ORDER BY pqx.DataX, pqx.Tipo, pqx.OrigemCtrl',
  'ORDER BY pqx.Insumo, NF, Serie, OriTipo, OrigemCodi, OrigemCtrl',
  '']);
  //
  //Geral.MB_Teste(QrPQW.SQL.Text);
end;

procedure TFmPQporNF.ReopenPqx(OrigemCodi, OrigemCtrl, Tipo: Integer);
var
  Filial, Empresa, CliOrig, Insumo: Integer;
  SQLPeriodo: String;
begin
  Filial  := EdEmpresa.ValueVariant;
  Empresa := DModG.ObtemEntidadeDeFilial(Filial);
  CliOrig := EdCI.ValueVariant;
  Insumo  := EdPQ.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  if MyObjects.FIC(CliOrig = 0, EdCI, 'Informe o cliente interno!') then Exit;
  if MyObjects.FIC(Insumo = 0, EdPQ, 'Informe o insumo!') then Exit;
  //
  SQLPeriodo := dmkPF.SQL_Periodo('AND pqx.DataX ', TPIni.Date, TPFim.Date, True, True);
  //
  UnDmkDAC_PF.AbreMySQLQUery0(QrPQX, Dmod.MyDB, [
  'SELECT vfi.Nome NomeVFI, ',
  'IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0010) + ', pqe.NF, pqx.NF) + 0.0 NF, ',
  'IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0010) + ', pqe.Serie, pqx.Serie) + 0.0 Serie, ',
  'IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0010) + ', pqi.xLote, pqx.xLote) xLote, ',
  'pqx.*',
  'FROM pqx pqx',
  'LEFT JOIN varfatid vfi ON vfi.Codigo=pqx.Tipo',
  'LEFT JOIN pqeits pqi  ',
  '  ON pqi.Codigo=pqx.OrigemCodi  ',
  '  AND pqi.Controle=pqx.OrigemCtrl  ',
  'LEFT JOIN pqe ON pqe.Codigo=pqi.Codigo  ',
  'WHERE pqx.Empresa=' + Geral.FF0(Empresa),
  'AND pqx.CliOrig=' + Geral.FF0(CliOrig),
  'AND pqx.Insumo=' + Geral.FF0(Insumo),
  'AND pqx.Peso>0 ',
  'AND pqx.Tipo <> ' + Geral.FF0(VAR_FATID_0000),
  //'AND pqx.SdoPeso>0 ',
  SQLPeriodo,
  'ORDER BY pqx.DataX DESC, pqx.Tipo DESC, pqx.OrigemCtrl DESC ',
  '']);
  //Geral.MB_Teste(QrPqx.SQL.Text);
  //
  if QrPqx.RecordCount > 0 then
    QrPqx.Locate('OrigemCodi;OrigemCtrl;Tipo', VarArrayOf([OrigemCodi, OrigemCtrl, Tipo]), []);
end;

procedure TFmPQporNF.TPFimChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPQporNF.TPFimClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPQporNF.TPIniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPQporNF.TPIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

end.
