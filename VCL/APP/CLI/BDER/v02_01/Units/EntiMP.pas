unit EntiMP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkDBGridDAC, Db, DmkDAC_PF,
  mySQLDbTables, dmkDBGrid, DBCtrls, dmkEdit, ComCtrls, Variants,
  dmkDBLookupComboBox, dmkEditCB, dmkGeral, dmkLabel, dmkImage, UnDmkEnums;

type
  TFmEntiMP = class(TForm)
    PainelBase: TPanel;
    Panel1: TPanel;
    QrEntiMP: TmySQLQuery;
    DsEntiMP: TDataSource;
    PnInsUpd: TPanel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTI: TWideStringField;
    PnControla: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    PnConfirma: TPanel;
    Panel6: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    QrEntiMPCodigo: TIntegerField;
    QrEntiMPLk: TIntegerField;
    QrEntiMPDataCad: TDateField;
    QrEntiMPDataAlt: TDateField;
    QrEntiMPUserCad: TIntegerField;
    QrEntiMPUserAlt: TIntegerField;
    QrEntiMPAlterWeb: TSmallintField;
    QrEntiMPAtivo: TSmallintField;
    QrEntiMPCMPUnida: TSmallintField;
    QrEntiMPCMPMoeda: TSmallintField;
    QrEntiMPCMPPerio: TSmallintField;
    QrEntiMPCMPPreco: TFloatField;
    QrEntiMPCMPFrete: TFloatField;
    QrEntiMPCPLUnida: TSmallintField;
    QrEntiMPCPLMoeda: TSmallintField;
    QrEntiMPCPLPerio: TSmallintField;
    QrEntiMPCPLPreco: TFloatField;
    QrEntiMPCPLFrete: TFloatField;
    QrEntiMPAGIUnida: TSmallintField;
    QrEntiMPAGIMoeda: TSmallintField;
    QrEntiMPAGIPerio: TSmallintField;
    QrEntiMPAGIPreco: TFloatField;
    QrEntiMPAGIFrete: TFloatField;
    QrEntiMPCMIUnida: TSmallintField;
    QrEntiMPCMIMoeda: TSmallintField;
    QrEntiMPCMIPerio: TSmallintField;
    QrEntiMPCMIPreco: TFloatField;
    QrEntiMPCMIFrete: TFloatField;
    QrEntiMPNOMEEMP: TWideStringField;
    QrEntiMPCMPUNIDANOME: TWideStringField;
    QrEntiMPCPLUNIDANOME: TWideStringField;
    QrEntiMPAGIUNIDANOME: TWideStringField;
    QrEntiMPCMIUNIDANOME: TWideStringField;
    QrEntiMPEntiMP: TIntegerField;
    QrEntiMPCMPMOEDANOME: TWideStringField;
    QrEntiMPCPLMOEDANOME: TWideStringField;
    QrEntiMPAGIMOEDANOME: TWideStringField;
    QrEntiMPCMIMOEDANOME: TWideStringField;
    QrEntiMPCMPPERIONOME: TWideStringField;
    QrEntiMPCPLPERIONOME: TWideStringField;
    QrEntiMPAGIPERIONOME: TWideStringField;
    QrEntiMPCMIPERIONOME: TWideStringField;
    QrEntiMPPreDescarn: TSmallintField;
    QrEntiMPPreDescarnNOME: TWideStringField;
    Label10: TLabel;
    Label12: TLabel;
    QrEntiMPMaskLetras: TWideStringField;
    QrEntiMPMaskFormat: TWideStringField;
    Panel7: TPanel;
    GroupBox4: TGroupBox;
    RgCMIUnida: TRadioGroup;
    RgCMIMoeda: TRadioGroup;
    RgCMIPerio: TRadioGroup;
    GroupBox3: TGroupBox;
    RgAGIUnida: TRadioGroup;
    RgAGIMoeda: TRadioGroup;
    RgAGIPerio: TRadioGroup;
    GroupBox2: TGroupBox;
    RgCPLUnida: TRadioGroup;
    RgCPLMoeda: TRadioGroup;
    RgCPLPerio: TRadioGroup;
    GroupBox1: TGroupBox;
    RgCMPUnida: TRadioGroup;
    RgCMPMoeda: TRadioGroup;
    RgCMPPerio: TRadioGroup;
    GroupBox5: TGroupBox;
    RGCPLSit: TRadioGroup;
    QrEntiMPCPLSit: TSmallintField;
    QrEntiMPCPLSit_NOME: TWideStringField;
    Label13: TLabel;
    RgCMP_IDQV: TRadioGroup;
    Panel8: TPanel;
    dmkEdCMPPreco: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    dmkEdCMPFrete: TdmkEdit;
    dmkEdCMP_LPQV: TdmkEdit;
    Label14: TLabel;
    Panel9: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    dmkEdCPL_LPQV: TdmkEdit;
    Panel10: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    dmkEdAGI_LPQV: TdmkEdit;
    Panel11: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    dmkEdCMI_LPQV: TdmkEdit;
    dmkEdCPLPreco: TdmkEdit;
    dmkEdCPLFrete: TdmkEdit;
    dmkEdAGIPreco: TdmkEdit;
    dmkEdAGIFrete: TdmkEdit;
    dmkEdCMIPreco: TdmkEdit;
    dmkEdCMIFrete: TdmkEdit;
    RgCPL_IDQV: TRadioGroup;
    RgAGI_IDQV: TRadioGroup;
    RgCMI_IDQV: TRadioGroup;
    QrEntiMPCMP_LPQV: TFloatField;
    QrEntiMPCPL_LPQV: TFloatField;
    QrEntiMPAGI_LPQV: TFloatField;
    QrEntiMPCMI_LPQV: TFloatField;
    QrEntiMPCMP_IDQV: TSmallintField;
    QrEntiMPCPL_IDQV: TSmallintField;
    QrEntiMPAGI_IDQV: TSmallintField;
    QrEntiMPCMI_IDQV: TSmallintField;
    Panel12: TPanel;
    EdContatos: TdmkEdit;
    Label01: TLabel;
    EdLocalEntrg: TdmkEdit;
    Label4: TLabel;
    EdCorretor: TdmkEditCB;
    Label5: TLabel;
    CBCorretor: TdmkDBLookupComboBox;
    dmkEdComissao: TdmkEdit;
    Label6: TLabel;
    EdCondPagto: TdmkEdit;
    Label7: TLabel;
    EdCondComis: TdmkEdit;
    Label8: TLabel;
    Label9: TLabel;
    dmkEdDescAdiant: TdmkEdit;
    QrEntiMPContatos: TWideStringField;
    QrEntiMPLocalEntrg: TWideStringField;
    QrEntiMPComissao: TFloatField;
    QrEntiMPCondPagto: TWideStringField;
    QrEntiMPCondComis: TWideStringField;
    QrEntiMPDescAdiant: TFloatField;
    QrEntiMPCorretor: TIntegerField;
    QrCorretores: TmySQLQuery;
    DsCorretores: TDataSource;
    QrCorretoresCodigo: TIntegerField;
    QrCorretoresNOMEENTI: TWideStringField;
    QrTransporte: TmySQLQuery;
    QrTransporteCodigo: TIntegerField;
    QrTransporteNOMEENTIDADE: TWideStringField;
    DsTransporte: TDataSource;
    Label23: TLabel;
    EdTransporte: TdmkEditCB;
    CBTransporte: TdmkDBLookupComboBox;
    SpeedButton3: TSpeedButton;
    RGAbate: TRadioGroup;
    QrEntiMPAbate: TIntegerField;
    QrEntiMPTransporte: TIntegerField;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    Splitter1: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet2: TTabSheet;
    DBGrid2: TDBGrid;
    TabSheet3: TTabSheet;
    DBGrid3: TDBGrid;
    TabSheet4: TTabSheet;
    DBGrid4: TDBGrid;
    Label24: TLabel;
    dmkEdSdoIniPele: TdmkEdit;
    QrEntiMPSdoIniPele: TFloatField;
    Memo1: TMemo;
    QrEntiMPLoteLetras: TWideStringField;
    QrEntiMPLoteFormat: TWideStringField;
    Panel14: TPanel;
    Panel5: TPanel;
    LaFornecedor: TLabel;
    CBEntiMP: TdmkDBLookupComboBox;
    EdEntiMP: TdmkEditCB;
    Panel15: TPanel;
    Panel13: TPanel;
    Label25: TLabel;
    EdLoteLetras: TdmkEdit;
    EdLoteFormat: TdmkEdit;
    Panel4: TPanel;
    Label11: TLabel;
    EdMaskLetras: TdmkEdit;
    EdMaskFormat: TdmkEdit;
    RgPreDescarn: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel16: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure QrEntiMPCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenEntiMP(Codigo: Integer);
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
  public
    { Public declarations }
  end;

  var
  FmEntiMP: TFmEntiMP;

implementation

uses UnMyObjects, Module, UnInternalConsts, UnGOTOy, UMySQLModule, Principal;

{$R *.DFM}

procedure TFmEntiMP.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiMP.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ReopenEntiMP(QrEntiMPCodigo.Value);
end;

procedure TFmEntiMP.FormResize(Sender: TObject);
begin
   MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiMP.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmEntiMP.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      dmkDBGrid1.Enabled := True;
      PnControla.Visible   := True;
      PnConfirma.Visible   := False;
      PnInsUpd.Visible     := False;
    end;
    1:
    begin
      PnConfirma.Visible := True;
      PnInsUpd.Visible   := True;
      PnControla.Visible := False;
      if SQLType = stIns then
      begin
        EdEntiMP.Text              := '';
        CBEntiMP.KeyValue          := NULL;
        //
        RgCMPUnida.ItemIndex       := 0;
        RgCMPMoeda.ItemIndex       := 0;
        RgCMPPerio.ItemIndex       := 0;
        dmkEdCMPPreco.ValueVariant := 0;
        dmkEdCMPFrete.ValueVariant := 0;
        //
        RgCPLUnida.ItemIndex       := 0;
        RgCPLMoeda.ItemIndex       := 0;
        RgCPLPerio.ItemIndex       := 0;
        dmkEdCPLPreco.ValueVariant := 0;
        dmkEdCPLFrete.ValueVariant := 0;
        //
        RgAGIUnida.ItemIndex       := 0;
        RgAGIMoeda.ItemIndex       := 0;
        RgAGIPerio.ItemIndex       := 0;
        dmkEdAGIPreco.ValueVariant := 0;
        dmkEdAGIFrete.ValueVariant := 0;
        //
        RgCMIUnida.ItemIndex       := 0;
        RgCMIMoeda.ItemIndex       := 0;
        RgCMIPerio.ItemIndex       := 0;
        dmkEdCMIPreco.ValueVariant := 0;
        dmkEdCMIFrete.ValueVariant := 0;
        //
        dmkEdCMP_LPQV.ValueVariant := 0;
        dmkEdCPL_LPQV.ValueVariant := 0;
        dmkEdAGI_LPQV.ValueVariant := 0;
        dmkEdCMI_LPQV.ValueVariant := 0;
        //
        RgCMP_IDQV.ItemIndex       := 1;
        RgCPL_IDQV.ItemIndex       := 0;
        RgAGI_IDQV.ItemIndex       := 0;
        RgCMI_IDQV.ItemIndex       := 0;
        //
        RgCPLSit.ItemIndex         := 0;
        RgPreDescarn.ItemIndex     := 0;
        //
        EdLoteLetras.Text            := 'ABC';
        EdLoteFormat.Text            := 'sa';
        //
        EdMaskLetras.Text            := 'ABC';
        EdMaskFormat.Text            := 'yymmdd';
        //
        EdLocalEntrg.Text            := '';
        EdCondPagto.Text             := '';
        EdContatos.Text              := '';
        dmkEdDescAdiant.ValueVariant := 0;
        EdCorretor.Text              := '';
        CBCorretor.KeyValue          := NULL;
        dmkEdComissao.ValueVariant   := 0;
        EdCondComis.Text             := '';
        RGAbate.ItemIndex            := 1;
        EdTransporte.Text            := '';
        CBTransporte.KeyValue        := NULL;
        //
        dmkEdSdoIniPele.ValueVariant := 0;
        //
      end else begin
        EdEntiMP.Text           := IntToStr(QrEntiMPCodigo.Value);
        CBEntiMP.KeyValue       := QrEntiMPCodigo.Value;
        //
        RgCMPUnida.ItemIndex       := QrEntiMPCMPUnida.Value;
        RgCMPMoeda.ItemIndex       := QrEntiMPCMPMoeda.Value;
        RgCMPPerio.ItemIndex       := QrEntiMPCMPPerio.Value;
        dmkEdCMPPreco.ValueVariant := QrEntiMPCMPPreco.Value;
        dmkEdCMPFrete.ValueVariant := QrEntiMPCMPFrete.Value;
        //
        RgCPLUnida.ItemIndex       := QrEntiMPCPLUnida.Value;
        RgCPLMoeda.ItemIndex       := QrEntiMPCPLMoeda.Value;
        RgCPLPerio.ItemIndex       := QrEntiMPCPLPerio.Value;
        dmkEdCPLPreco.ValueVariant := QrEntiMPCPLPreco.Value;
        dmkEdCPLFrete.ValueVariant := QrEntiMPCPLFrete.Value;
        //
        RgAGIUnida.ItemIndex       := QrEntiMPAGIUnida.Value;
        RgAGIMoeda.ItemIndex       := QrEntiMPAGIMoeda.Value;
        RgAGIPerio.ItemIndex       := QrEntiMPAGIPerio.Value;
        dmkEdAGIPreco.ValueVariant := QrEntiMPAGIPreco.Value;
        dmkEdAGIFrete.ValueVariant := QrEntiMPAGIFrete.Value;
        //
        RgCMIUnida.ItemIndex       := QrEntiMPCMIUnida.Value;
        RgCMIMoeda.ItemIndex       := QrEntiMPCMIMoeda.Value;
        RgCMIPerio.ItemIndex       := QrEntiMPCMIPerio.Value;
        dmkEdCMIPreco.ValueVariant := QrEntiMPCMIPreco.Value;
        dmkEdCMIFrete.ValueVariant := QrEntiMPCMIFrete.Value;
        //
        dmkEdCMP_LPQV.ValueVariant := QrEntiMPCMP_LPQV.Value;
        dmkEdCPL_LPQV.ValueVariant := QrEntiMPCPL_LPQV.Value;
        dmkEdAGI_LPQV.ValueVariant := QrEntiMPAGI_LPQV.Value;
        dmkEdCMI_LPQV.ValueVariant := QrEntiMPCMI_LPQV.Value;
        //
        RgCMP_IDQV.ItemIndex       := QrEntiMPCMP_IDQV.Value;
        RgCPL_IDQV.ItemIndex       := QrEntiMPCPL_IDQV.Value;
        RgAGI_IDQV.ItemIndex       := QrEntiMPAGI_IDQV.Value;
        RgCMI_IDQV.ItemIndex       := QrEntiMPCMI_IDQV.Value;
        //
        RgCPLSit.ItemIndex         := QrEntiMPCPLSit.Value;
        RgPreDescarn.ItemIndex     := QrEntiMPPreDescarn.Value;
        //
        EdLoteLetras.Text            := QrEntiMPLoteLetras.Value;
        EdLoteFormat.Text            := QrEntiMPLoteFormat.Value;
        //
        EdMaskLetras.Text            := QrEntiMPMaskLetras.Value;
        EdMaskFormat.Text            := QrEntiMPMaskFormat.Value;
        //
        EdLocalEntrg.Text            := QrEntiMPLocalEntrg.Value;
        EdCondPagto.Text             := QrEntiMPCondPagto.Value;
        EdContatos.Text              := QrEntiMPContatos.Value;
        dmkEdDescAdiant.ValueVariant := QrEntiMPDescAdiant.Value;
        EdCorretor.Text              := IntToStr(QrEntiMPCorretor.Value);
        CBCorretor.KeyValue          := QrEntiMPCorretor.Value;
        dmkEdComissao.ValueVariant   := QrEntiMPComissao.Value;
        EdCondComis.Text             := QrEntiMPCondComis.Value;
        //
        RGAbate.ItemIndex            := QrEntiMPAbate.Value;
        EdTransporte.Text            := IntToStr(QrEntiMPTransporte.Value);
        CBTransporte.KeyValue        := QrEntiMPTransporte.Value;
        //
        dmkEdSdoIniPele.ValueVariant := QrEntiMPSdoIniPele.Value;
        //
        dmkDBGrid1.Enabled := False;
      end;
      EdEntiMP.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo > 0 then
    ReopenEntiMP(Codigo)
end;

procedure TFmEntiMP.ReopenEntiMP(Codigo: Integer);
begin
  QrEntiMP.Close;
  UnDmkDAC_PF.AbreQuery(QrEntiMP, Dmod.MyDB);
  //
  QrEntiMP.Locate('Codigo', Codigo, []);
end;

procedure TFmEntiMP.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(1, stUpd, 0);
end;

procedure TFmEntiMP.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, QrEntiMPCodigo.Value);
end;

procedure TFmEntiMP.BtExcluiClick(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o da entidade selecionada?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM entimp ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo='+IntToStr(QrEntiMPCodigo.Value));
    Dmod.QrUpd.ExecSQL;
    ReopenEntiMP(0);
  end;
end;

procedure TFmEntiMP.BtConfirmaClick(Sender: TObject);
var
  SQLType: TSQLType;
  EntiMP: Integer;
begin
  EntiMP := Geral.IMV(EdEntiMP.Text);
  //
  if EntiMP = 0 then
  begin
    Application.MessageBox('� necess�rio definir a entidade!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT Codigo FROM entimp ');
  Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
  Dmod.QrAux.Params[0].AsInteger := EntiMP;
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  if Dmod.QrAux.RecordCount = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entimp', False, [
    'CMP_LPQV', 'CPL_LPQV',
    'AGI_LPQV', 'CMI_LPQV',
    'CMP_IDQV', 'CPL_IDQV',
    'AGI_IDQV', 'CMI_IDQV',
    'CPLSit',
    'PreDescarn', 'MaskLetras', 'MaskFormat',
    'CMPUnida', 'CMPMoeda', 'CMPPerio', 'CMPPreco', 'CMPFrete',
    'CPLUnida', 'CPLMoeda', 'CPLPerio', 'CPLPreco', 'CPLFrete',
    'AGIUnida', 'AGIMoeda', 'AGIPerio', 'AGIPreco', 'AGIFrete',
    'CMIUnida', 'CMIMoeda', 'CMIPerio', 'CMIPreco', 'CMIFrete',
    'LocalEntrg', 'CondPagto', 'Contatos',
    'DescAdiant', 'Corretor',
    'Comissao', 'CondComis',
    'Transporte', 'Abate', 'SdoIniPele',
    'LoteLetras', 'LoteFormat'
  ], ['Codigo'], [
    dmkEdCMP_LPQV.ValueVariant, dmkEdCPL_LPQV.ValueVariant,
    dmkEdAGI_LPQV.ValueVariant, dmkEdCMI_LPQV.ValueVariant,
    RgCMP_IDQV.ItemIndex, RgCPL_IDQV.ItemIndex,
    RgAGI_IDQV.ItemIndex, RgCMI_IDQV.ItemIndex,
    RgCPLSit.ItemIndex,
    RgPreDescarn.ItemIndex, EdMaskLetras.Text, EdMaskFormat.Text,
    RgCMPUnida.ItemIndex, RgCMPMoeda.ItemIndex, RgCMPPerio.ItemIndex,
      dmkEdCMPPreco.ValueVariant, dmkEdCMPFrete.ValueVariant,
    RgCPLUnida.ItemIndex, RgCPLMoeda.ItemIndex, RgCPLPerio.ItemIndex,
      dmkEdCPLPreco.ValueVariant, dmkEdCPLFrete.ValueVariant,
    RgAGIUnida.ItemIndex, RgAGIMoeda.ItemIndex, RgAGIPerio.ItemIndex,
      dmkEdAGIPreco.ValueVariant, dmkEdAGIFrete.ValueVariant,
    RgCMIUnida.ItemIndex, RgCMIMoeda.ItemIndex, RgCMIPerio.ItemIndex,
      dmkEdCMIPreco.ValueVariant, dmkEdCMIFrete.ValueVariant,
    EdLocalEntrg.Text, EdCondPagto.Text, EdContatos.Text,
    dmkEdDescAdiant.ValueVariant, Geral.IMV(EdCorretor.Text),
    dmkEdComissao.ValueVariant, EdCondComis.Text,
    Geral.IMV(EdTransporte.Text), RGAbate.ItemIndex,
    dmkEdSdoIniPele.ValueVariant,
    EdLoteLetras.Text, EdLoteFormat.Text
  ], [EntiMP], True) then MostraEdicao(0, stLok, EntiMP);
end;

procedure TFmEntiMP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  VAR_ENTIDADE := QrEntiMPCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEntiMP.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCorretores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporte, Dmod.MyDB);
  PnControla.Align := alClient;
end;

procedure TFmEntiMP.QrEntiMPCalcFields(DataSet: TDataSet);
begin
  case QrEntiMPPreDescarn.Value of
    0: QrEntiMPPreDescarnNOME.Value := 'No curtume';
    1: QrEntiMPPreDescarnNOME.Value := 'N�o pr�-descarna';
    2: QrEntiMPPreDescarnNOME.Value := 'J� vem pr�-descarnado';
    else QrEntiMPPreDescarnNOME.Value := '? ? ? ?';
  end;
  {
  case QrEntiMPIDQV.Value of
    0: QrEntiMPIDQV_NOME.Value := 'Nada';
    1: QrEntiMPIDQV_NOME.Value := 'Tudo';
    2: QrEntiMPIDQV_NOME.Value := 'Excedente';
    else QrEntiMPIDQV_NOME.Value := '? ? ? ?';
  end;
  }
  case QrEntiMPCPLSit.Value of
    0: QrEntiMPCPLSit_NOME.Value := 'Nota Fiscal';
    1: QrEntiMPCPLSit_NOME.Value := 'Entrada';
    else QrEntiMPCPLSit_NOME.Value := '? ? ? ?';
  end;
  QrEntiMPCMPUNIDANOME.Value := FmPrincipal.ObtemUnidadeTxt(QrEntiMPCMPUnida.Value);
  QrEntiMPCPLUNIDANOME.Value := FmPrincipal.ObtemUnidadeTxt(QrEntiMPCPLUnida.Value);
  QrEntiMPAGIUNIDANOME.Value := FmPrincipal.ObtemUnidadeTxt(QrEntiMPAGIUnida.Value);
  QrEntiMPCMIUNIDANOME.Value := FmPrincipal.ObtemUnidadeTxt(QrEntiMPCMIUnida.Value);
  //
  QrEntiMPCMPMOEDANOME.Value := FmPrincipal.ObtemMoedaTxt(QrEntiMPCMPMoeda.Value);
  QrEntiMPCPLMOEDANOME.Value := FmPrincipal.ObtemMoedaTxt(QrEntiMPCPLMoeda.Value);
  QrEntiMPAGIMOEDANOME.Value := FmPrincipal.ObtemMoedaTxt(QrEntiMPAGIMoeda.Value);
  QrEntiMPCMIMOEDANOME.Value := FmPrincipal.ObtemMoedaTxt(QrEntiMPCMIMoeda.Value);
  //
  QrEntiMPCMPPERIONOME.Value := FmPrincipal.ObtemPeriodoTxt(QrEntiMPCMPPerio.Value);
  QrEntiMPCPLPERIONOME.Value := FmPrincipal.ObtemPeriodoTxt(QrEntiMPCPLPerio.Value);
  QrEntiMPAGIPERIONOME.Value := FmPrincipal.ObtemPeriodoTxt(QrEntiMPAGIPerio.Value);
  QrEntiMPCMIPERIONOME.Value := FmPrincipal.ObtemPeriodoTxt(QrEntiMPCMIPerio.Value);
  //
end;

end.

