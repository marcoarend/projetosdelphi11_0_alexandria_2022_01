object FmPQImpErr: TFmPQImpErr
  Left = 339
  Top = 185
  Caption = 'QUI-INSUM-012 :: Incongru'#234'ncias de Estoque de Uso e Consumo'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 572
        Height = 32
        Caption = 'Incongru'#234'ncias de Estoque de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 572
        Height = 32
        Caption = 'Incongru'#234'ncias de Estoque de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 572
        Height = 32
        Caption = 'Incongru'#234'ncias de Estoque de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 504
    Width = 812
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 808
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 456
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object PCErrEstq: TPageControl
      Left = 0
      Top = 0
      Width = 812
      Height = 456
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet4: TTabSheet
        Caption = 'Entradas orf'#227's'
        ImageIndex = 1
        object Panel25: TPanel
          Left = 0
          Top = 0
          Width = 804
          Height = 428
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label15: TLabel
            Left = 0
            Top = 0
            Width = 804
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Itens de entradas orf'#227'os (n'#227'o excluidos!)'
            ExplicitWidth = 191
          end
          object Panel26: TPanel
            Left = 0
            Top = 380
            Width = 804
            Height = 48
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object BtDelErrE10: TBitBtn
              Tag = 12
              Left = 16
              Top = 4
              Width = 150
              Height = 40
              Caption = '&Exclui selecionados'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtDelErrE10Click
            end
          end
          object DBGErrE10: TDBGrid
            Left = 0
            Top = 13
            Width = 804
            Height = 367
            Align = alClient
            DataSource = DsErrE10
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGErrE10DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'OrigemCodi'
                Title.Caption = 'Entrada'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OrigemCtrl'
                Title.Caption = 'ID Item'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataX'
                Title.Caption = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Insumo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PQ'
                Title.Caption = 'Nome insumo'
                Width = 178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Peso'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Width = 72
                Visible = True
              end>
          end
        end
      end
      object TabSheet12: TTabSheet
        Caption = 'Datas inv'#225'lidas'
        ImageIndex = 2
        object DBGErrDta: TDBGrid
          Left = 0
          Top = 0
          Width = 804
          Height = 380
          Align = alClient
          DataSource = DsErrDta
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object Panel3: TPanel
          Left = 0
          Top = 380
          Width = 804
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object BtCorrigeDta: TBitBtn
            Tag = 11
            Left = 16
            Top = 4
            Width = 150
            Height = 40
            Caption = '&Corrige selecionados'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtCorrigeDtaClick
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'Cllientes internos inv'#225'lidos'
        ImageIndex = 2
        object DBGErrCli: TDBGrid
          Left = 0
          Top = 0
          Width = 804
          Height = 380
          Align = alClient
          DataSource = DsErrCli
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object Panel5: TPanel
          Left = 0
          Top = 380
          Width = 804
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object BtCorrigeCli: TBitBtn
            Tag = 11
            Left = 16
            Top = 4
            Width = 150
            Height = 40
            Caption = '&Corrige selecionados'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtCorrigeCliClick
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Corrigir valores em sequencia'
        ImageIndex = 3
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 804
          Height = 33
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          Visible = False
          object LaPQ: TLabel
            Left = 4
            Top = 8
            Width = 37
            Height = 13
            Caption = 'Insumo:'
          end
          object EdPQ: TdmkEditCB
            Left = 48
            Top = 4
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPQ
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBPQ: TdmkDBLookupComboBox
            Left = 116
            Top = 4
            Width = 457
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPQ
            TabOrder = 1
            dmkEditCB = EdPQ
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 33
          Width = 804
          Height = 76
          Align = alTop
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
        end
        object Grade: TStringGrid
          Left = 0
          Top = 109
          Width = 804
          Height = 271
          Align = alClient
          ColCount = 3
          RowCount = 100
          TabOrder = 2
          ExplicitHeight = 319
        end
        object Panel7: TPanel
          Left = 0
          Top = 380
          Width = 804
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = 1
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          object BtExecuta03: TBitBtn
            Left = 12
            Top = 4
            Width = 120
            Height = 40
            Caption = '&Executa'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtExecuta03Click
          end
          object TPData03: TdmkEditDateTimePicker
            Left = 148
            Top = 16
            Width = 137
            Height = 21
            Date = 45231.000000000000000000
            Time = 0.501874664354545500
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 48
    Top = 103
  end
  object QrErrE10: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrErrE10AfterOpen
    BeforeClose = QrErrE10BeforeClose
    SQL.Strings = (
      'SELECT its.Controle, pq_.Nome NO_PQ, pqx.*  '
      'FROM pqx pqx '
      'LEFT JOIN pqeits its ON its.Controle=pqx.OrigemCtrl '
      'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo '
      'WHERE pqx.Tipo=10 '
      'AND its.Controle IS NULL ')
    Left = 28
    Top = 172
    object QrErrE10Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrErrE10NO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrErrE10DataX: TDateField
      FieldName = 'DataX'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrErrE10OrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
      Required = True
    end
    object QrErrE10OrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
      Required = True
    end
    object QrErrE10Tipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrErrE10CliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrErrE10CliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrErrE10Insumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrErrE10Peso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrErrE10Valor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrErrE10Empresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
  end
  object DsErrE10: TDataSource
    DataSet = QrErrE10
    Left = 28
    Top = 220
  end
  object QrErrDta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DataX, OrigemCodi, OrigemCtrl, Tipo,'
      'CliOrig, CliDest, Insumo, Peso, Valor '
      'FROM pqx'
      'WHERE DataX=0'
      'ORDER BY Tipo, OrigemCodi, OrigemCtrl')
    Left = 104
    Top = 172
    object QrErrDtaDataX: TDateField
      FieldName = 'DataX'
      Required = True
    end
    object QrErrDtaOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
      Required = True
    end
    object QrErrDtaOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
      Required = True
    end
    object QrErrDtaTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrErrDtaCliOrig: TIntegerField
      FieldName = 'CliOrig'
      Required = True
    end
    object QrErrDtaCliDest: TIntegerField
      FieldName = 'CliDest'
      Required = True
    end
    object QrErrDtaInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrErrDtaPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrErrDtaValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
  end
  object DsErrDta: TDataSource
    DataSet = QrErrDta
    Left = 104
    Top = 220
  end
  object QrErrCli: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrErrCliAfterOpen
    SQL.Strings = (
      'SELECT DataX, OrigemCodi, OrigemCtrl, Tipo,'
      'CliOrig, CliDest, Insumo, Peso, Valor '
      'FROM pqx'
      'WHERE CliOrig=0'
      'OR CliDest=0'
      'ORDER BY Tipo, OrigemCodi, OrigemCtrl')
    Left = 184
    Top = 172
    object QrErrCliDataX: TDateField
      FieldName = 'DataX'
      Required = True
    end
    object QrErrCliOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
      Required = True
    end
    object QrErrCliOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
      Required = True
    end
    object QrErrCliTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrErrCliCliOrig: TIntegerField
      FieldName = 'CliOrig'
      Required = True
    end
    object QrErrCliCliDest: TIntegerField
      FieldName = 'CliDest'
      Required = True
    end
    object QrErrCliInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrErrCliPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrErrCliValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
  end
  object DsErrCli: TDataSource
    DataSet = QrErrCli
    Left = 184
    Top = 220
  end
  object MySQLQuery2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT its.Controle, pq_.Nome NO_PQ, pqx.*  '
      'FROM pqx pqx '
      'LEFT JOIN pqeits its ON its.Controle=pqx.OrigemCtrl '
      'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo '
      'WHERE pqx.Tipo=10 '
      'AND its.Controle IS NULL ')
    Left = 268
    Top = 172
  end
  object DataSource2: TDataSource
    DataSet = MySQLQuery2
    Left = 268
    Top = 220
  end
  object Query: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT its.Controle, pq_.Nome NO_PQ, pqx.*  '
      'FROM pqx pqx '
      'LEFT JOIN pqeits its ON its.Controle=pqx.OrigemCtrl '
      'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo '
      'WHERE pqx.Tipo=10 '
      'AND its.Controle IS NULL ')
    Left = 408
    Top = 176
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq '
      'WHERE Ativo NOT IN (0,3)'
      'AND Codigo > 0'
      'ORDER BY Nome')
    Left = 348
    Top = 172
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 348
    Top = 224
  end
end
