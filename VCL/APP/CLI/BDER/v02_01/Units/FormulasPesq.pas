unit FormulasPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Db, mySQLDbTables, Grids,
  DBGrids, dmkImage, UnDmkEnums, DmkDAC_PF, dmkDBGrid, frxClass, frxDBSet,
  Vcl.DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmFormulasPesq = class(TForm)
    Panel1: TPanel;
    DBGrid1: TdmkDBGrid;
    QrFormulas: TmySQLQuery;
    DsFormulas: TDataSource;
    QrFormulasNumero: TIntegerField;
    QrFormulasNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtPesquisa: TBitBtn;
    BtLocaliza: TBitBtn;
    BtSaida: TBitBtn;
    QrFormulasAtivo: TSmallintField;
    QrFormulasSetor_TXT: TWideStringField;
    frxDsFormulas: TfrxDBDataset;
    BtImprime: TBitBtn;
    frxFormulas: TfrxReport;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    RGMascara: TRadioGroup;
    RGAtivo: TRadioGroup;
    LaTotal: TLabel;
    RGArquivo: TRadioGroup;
    BtResgata: TBitBtn;
    QrFormulasNumCODIF: TWideStringField;
    RGAmbAplic: TRadioGroup;
    QrEntidades: TMySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNO_CLIENTE: TWideStringField;
    DsEntidades: TDataSource;
    LaPrompt: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxFormulasGetValue(const VarName: string; var Value: Variant);
    procedure QrFormulasAfterScroll(DataSet: TDataSet);
    procedure QrFormulasBeforeClose(DataSet: TDataSet);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure Edit1Change(Sender: TObject);
    procedure RGArquivoClick(Sender: TObject);
    procedure QrFormulasAfterOpen(DataSet: TDataSet);
    procedure BtResgataClick(Sender: TObject);
    procedure QrFormulasCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure AtualizaAtivo(Numero, Ativo: Integer);
    procedure ReopenPesquisa(Numero: Integer);
    function  TabCab(): String;
    function  TabIts(): String;
  public
    { Public declarations }
  end;

  var
  FmFormulasPesq: TFmFormulasPesq;

implementation

{$R *.DFM}

uses UnMyObjects, Module, ModuleGeral, Formulas, UMySQLModule, UnMySQLCuringa,
  UnPQ_PF, UnDmkProcFunc;

procedure TFmFormulasPesq.BtSaidaClick(Sender: TObject);
begin
  FmFormulas.FRecPesq := 0;
  Close;
end;

procedure TFmFormulasPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  Edit1.SetFocus;
end;

procedure TFmFormulasPesq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB, [
  'SELECT ent.Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE',
  'FROM entidades ent ',
  'ORDER BY NO_CLIENTE',
  '']);
end;

procedure TFmFormulasPesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulasPesq.frxFormulasGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Date
end;

procedure TFmFormulasPesq.QrFormulasAfterOpen(DataSet: TDataSet);
begin
  BtPesquisa.Enabled := (RGArquivo.ItemIndex = 0) and (QrFormulas.RecordCount > 0);
  BtResgata.Enabled  := (RGArquivo.ItemIndex = 1) and (QrFormulas.RecordCount > 0);
end;

procedure TFmFormulasPesq.QrFormulasAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrFormulas.RecordCount)
end;

procedure TFmFormulasPesq.QrFormulasBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmFormulasPesq.QrFormulasCalcFields(DataSet: TDataSet);
begin
  QrFormulasNumCODIF.Value := dmkPF.IntToCODIF_Receita(QrFormulasNumero.Value);
end;

procedure TFmFormulasPesq.ReopenPesquisa(Numero: Integer);
var
  Cliente: Integer;
  Texto, SQL_Ativo, SQL_AmbAplic, SQL_Cliente, LFJ_Cliente: String;
begin
  case RGAtivo.ItemIndex of
    1:
      SQL_Ativo := 'AND fml.Ativo=1';
    2:
      SQL_Ativo := 'AND fml.Ativo=0';
    else
      SQL_Ativo := '';
  end;
  //
  case RGAmbAplic.ItemIndex of
    1: SQL_AmbAplic := 'AND fml.Numero > 0';
    2: SQL_AmbAplic := 'AND fml.Numero < 0';
    else
      SQL_AmbAplic := '';
  end;
  //
  Cliente := EdCliente.ValueVariant;
  SQL_Cliente := '';
  LFJ_Cliente := '';
  if Cliente <> 0 then
  begin
    SQL_Cliente := 'AND cli.Cliente=' + Geral.FF0(Cliente);
    LFJ_Cliente := 'LEFT JOIN formulascli cli ON cli.Numero=fml.Numero ';
  end;
  //
  Texto := CuringaLoc.ObtemSQLPesquisaText(RGMascara.ItemIndex, Edit1.Text);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
    'SELECT DISTINCT fml.Numero, fml.Nome, ',
    'lse.Nome Setor_TXT, fml.Ativo ',
    'FROM ' + TabCab() + ' fml ',
    'LEFT JOIN listasetores lse ON lse.Codigo=fml.Setor ',
    LFJ_Cliente,
    'WHERE fml.Nome LIKE "' + Texto + '"',
    'AND fml.Numero<> 0 ',
    SQL_Ativo,
    SQL_AmbAplic,
    SQL_Cliente,
    '']);
  //Geral.MB_Teste(QrFormulas.SQL.Text);
  if Numero <> 0 then
    QrFormulas.Locate('Numero', Numero, []);
end;

procedure TFmFormulasPesq.RGArquivoClick(Sender: TObject);
begin
  if QrFormulas.State <> dsInactive then
    ReopenPesquisa(0);
end;

function TFmFormulasPesq.TabCab(): String;
begin
  case RGArquivo.ItemIndex of
    0: Result := 'formulas';
    1: Result := 'formulaz';
    else Result := 'formula?';
  end;
end;

function TFmFormulasPesq.TabIts(): String;
begin
  case RGArquivo.ItemIndex of
    0: Result := 'formulasits';
    1: Result := 'formulazits';
    else Result := 'formula?its';
  end;
end;

procedure TFmFormulasPesq.BtPesquisaClick(Sender: TObject);
begin
  ReopenPesquisa(0);
end;

procedure TFmFormulasPesq.BtResgataClick(Sender: TObject);
const
  Motivo = 0;
begin
  PQ_PF.TransfereReceitaDeArquivo(TOrientTabArqBD.stabdMortoToAcessivel,
    QrFormulasNumero.Value, Motivo);
end;

procedure TFmFormulasPesq.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxFormulas, [
    DmodG.frxDsDono,
    frxDsFormulas
    ]);
  MyObjects.frxMostra(frxFormulas, 'Pesquisa de receitas');
end;

procedure TFmFormulasPesq.BtLocalizaClick(Sender: TObject);
begin
  FmFormulas.FRecPesq := QrFormulasNumero.Value;
  Close;
end;

procedure TFmFormulasPesq.DBGrid1CellClick(Column: TColumn);
var
  Numero, Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Ativo  := QrFormulasAtivo.Value;
    Numero := QrFormulasNumero.Value;
    //
    AtualizaAtivo(Numero, Ativo);
    //
    ReopenPesquisa(Numero);
  end;
end;

procedure TFmFormulasPesq.AtualizaAtivo(Numero, Ativo: Integer);
begin
  if Ativo = 0 then
    Ativo := 1
  else
    Ativo := 0;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabCab(), False,
    ['Ativo'], ['Numero'],
    [Ativo], [Numero], True);
end;

procedure TFmFormulasPesq.DBGrid1DblClick(Sender: TObject);
begin
  BtLocalizaClick(Self);
end;

procedure TFmFormulasPesq.Edit1Change(Sender: TObject);
var
  Txt: String;
begin
  Txt := Edit1.Text;
  //
  if (Txt <> '') and (Length(Txt) >= 3) then
    ReopenPesquisa(0);
end;

end.
