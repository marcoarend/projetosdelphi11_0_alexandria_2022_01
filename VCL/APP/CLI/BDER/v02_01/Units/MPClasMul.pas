unit MPClasMul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, mySQLDbTables,
  dmkEdit, dmkEditCalc, DBCtrls, dmkDBLookupComboBox, dmkEditCB, Mask,
  UnInternalConsts, dmkImage, UnDmkEnums, DmkDAC_PF, UnProjGroup_Consts;

type
  TFmMPClasMul = class(TForm)
    Panel1: TPanel;
    QrMPIn: TmySQLQuery;
    QrMPInCMPUnida: TSmallintField;
    QrMPInCMP_IDQV: TSmallintField;
    QrMPInCMP_LPQV: TFloatField;
    QrMPInContatos: TWideStringField;
    QrMPInNOMECLIENTEI: TWideStringField;
    QrMPInNOMECIDADEF: TWideStringField;
    QrMPInNOMECORRETOR: TWideStringField;
    QrMPInNOMEASSINA: TWideStringField;
    QrMPInNOMEUFF: TWideStringField;
    QrMPInTEL1F: TWideStringField;
    QrMPInTEL2F: TWideStringField;
    QrMPInNOMEPROCEDENCIA: TWideStringField;
    QrMPInCodigo: TIntegerField;
    QrMPInControle: TIntegerField;
    QrMPInFicha: TIntegerField;
    QrMPInData: TDateField;
    QrMPInClienteI: TIntegerField;
    QrMPInProcedencia: TIntegerField;
    QrMPInTransportadora: TIntegerField;
    QrMPInMarca: TWideStringField;
    QrMPInPecas: TFloatField;
    QrMPInPecasNF: TFloatField;
    QrMPInM2: TFloatField;
    QrMPInP2: TFloatField;
    QrMPInFimM2: TFloatField;
    QrMPInFimP2: TFloatField;
    QrMPInPNF: TFloatField;
    QrMPInPLE: TFloatField;
    QrMPInPDA: TFloatField;
    QrMPInRecorte_PDA: TFloatField;
    QrMPInPTA: TFloatField;
    QrMPInRecorte_PTA: TFloatField;
    QrMPInRaspa_PTA: TFloatField;
    QrMPInTipificacao: TIntegerField;
    QrMPInAparasCabelo: TSmallintField;
    QrMPInSeboPreDescarne: TSmallintField;
    QrMPInCustoInfo: TFloatField;
    QrMPInFreteInfo: TFloatField;
    QrMPInValor: TFloatField;
    QrMPInCustoPQ: TFloatField;
    QrMPInFrete: TFloatField;
    QrMPInAbateTipo: TIntegerField;
    QrMPInLote: TWideStringField;
    QrMPInPecasOut: TFloatField;
    QrMPInPecasSal: TFloatField;
    QrMPInCaminhoes: TIntegerField;
    QrMPInQuemAssina: TIntegerField;
    QrMPInCorretor: TIntegerField;
    QrMPInComisPer: TFloatField;
    QrMPInComisVal: TFloatField;
    QrMPInDescAdiant: TFloatField;
    QrMPInCondPagto: TWideStringField;
    QrMPInCondComis: TWideStringField;
    QrMPInLocalEntrg: TWideStringField;
    QrMPInObserv: TWideStringField;
    QrMPInAnimal: TSmallintField;
    QrMPInCMPValor: TFloatField;
    QrMPInCMPFrete: TFloatField;
    QrMPInQuebrVReal: TFloatField;
    QrMPInQuebrVCobr: TFloatField;
    QrMPInCMPDesQV: TFloatField;
    QrMPInCMPPagar: TFloatField;
    QrMPInPcBxa: TFloatField;
    QrMPInkgBxa: TFloatField;
    QrMPInM2Bxa: TFloatField;
    QrMPInP2Bxa: TFloatField;
    QrMPInPecasCal: TFloatField;
    QrMPInPecasCur: TFloatField;
    QrMPInPecasExp: TFloatField;
    QrMPInM2Exp: TFloatField;
    QrMPInP2Exp: TFloatField;
    QrMPInPecasNeg: TFloatField;
    QrMPInPesoCal: TFloatField;
    QrMPInPesoCur: TFloatField;
    QrMPInCusPQCal: TFloatField;
    QrMPInCusPQCur: TFloatField;
    QrMPInFuloesCal: TIntegerField;
    QrMPInFuloesCur: TIntegerField;
    QrMPInMinDtaCal: TDateField;
    QrMPInMinDtaCur: TDateField;
    QrMPInMinDtaEnx: TDateField;
    QrMPInMaxDtaCal: TDateField;
    QrMPInMaxDtaCur: TDateField;
    QrMPInMaxDtaEnx: TDateField;
    QrMPInForcaEncer: TSmallintField;
    QrMPInEncerrado: TSmallintField;
    QrMPInPS_ValUni: TFloatField;
    QrMPInPS_QtdTot: TFloatField;
    QrMPInPS_ValTot: TFloatField;
    QrMPInLk: TIntegerField;
    QrMPInDataCad: TDateField;
    QrMPInDataAlt: TDateField;
    QrMPInUserCad: TIntegerField;
    QrMPInUserAlt: TIntegerField;
    QrMPInAlterWeb: TSmallintField;
    QrMPInAtivo: TSmallintField;
    QrMPInEmClassif: TSmallintField;
    QrMPInPesoExp: TFloatField;
    QrMPInEstq_Pecas: TFloatField;
    QrMPInEstq_M2: TFloatField;
    QrMPInEstq_P2: TFloatField;
    QrMPInEstq_Peso: TFloatField;
    QrMPInStqMovIts: TIntegerField;
    QrMPInClasMul: TmySQLQuery;
    QrMPInClasMulCodigo: TIntegerField;
    QrMPInClasMulControle: TIntegerField;
    QrMPInClasMulData: TDateField;
    QrMPInClasMulClienteI: TIntegerField;
    QrMPInClasMulProcedencia: TIntegerField;
    QrMPInClasMulTransportadora: TIntegerField;
    QrMPInClasMulMarca: TWideStringField;
    QrMPInClasMulLote: TWideStringField;
    QrMPInClasMulPecasOut: TFloatField;
    QrMPInClasMulPecasSal: TFloatField;
    QrMPInClasMulEstq_Pecas: TFloatField;
    QrMPInClasMulEstq_M2: TFloatField;
    QrMPInClasMulEstq_P2: TFloatField;
    QrMPInClasMulEstq_Peso: TFloatField;
    QrMPInClasMulStqMovIts: TIntegerField;
    DsMPInClasMul: TDataSource;
    DBGrid1: TDBGrid;
    QrSoma: TmySQLQuery;
    QrSomaEstq_Pecas: TFloatField;
    QrSomaEstq_M2: TFloatField;
    QrSomaEstq_P2: TFloatField;
    QrSomaEstq_Peso: TFloatField;
    QrMPInClasMulClas_Pecas: TFloatField;
    QrMPInClasMulClas_M2: TFloatField;
    QrMPInClasMulClas_P2: TFloatField;
    QrMPInClasMulClas_Peso: TFloatField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXNivel3: TIntegerField;
    QrGraGruXNivel2: TIntegerField;
    QrGraGruXNivel1: TIntegerField;
    QrGraGruXNome: TWideStringField;
    QrGraGruXPrdGrupTip: TIntegerField;
    QrGraGruXGraTamCad: TIntegerField;
    QrGraGruXNOMEGRATAMCAD: TWideStringField;
    QrGraGruXCODUSUGRATAMCAD: TIntegerField;
    QrGraGruXCST_A: TSmallintField;
    QrGraGruXCST_B: TSmallintField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXPeso: TFloatField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrGraGruXHowBxaEstq: TSmallintField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXFatorClas: TIntegerField;
    DsGraGruX: TDataSource;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    QrMPInClasMulDebCtrl: TIntegerField;
    QrMPInClasMulDebStqCenCad: TIntegerField;
    QrMPInClasMulDebGraGruX: TIntegerField;
    QrMPInClasMulPecas: TFloatField;
    QrMPInClasMulPLE: TFloatField;
    QrSomaClas_M2: TFloatField;
    QrSomaClas_P2: TFloatField;
    QrSomaClas_Peso: TFloatField;
    Panel6: TPanel;
    DsSoma: TDataSource;
    QrSomaClas_Pecas: TFloatField;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    GroupBox2: TGroupBox;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Panel7: TPanel;
    Panel8: TPanel;
    PnMultiGrandeza: TPanel;
    Panel3: TPanel;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    EdPecas: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    EdPeso: TdmkEdit;
    Panel5: TPanel;
    BtRateia: TBitBtn;
    RGMovimento: TRadioGroup;
    Panel4: TPanel;
    Label3: TLabel;
    Label1: TLabel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    EdGraGrux: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    RGBaixa: TRadioGroup;
    QrMPInClasMulBxa_Pecas: TFloatField;
    QrMPInClasMulBxa_M2: TFloatField;
    QrMPInClasMulBxa_P2: TFloatField;
    QrMPInClasMulBxa_Peso: TFloatField;
    QrMPInClasMulGerBxaEstq: TIntegerField;
    QrMPInClasMulClas_Qtde: TFloatField;
    QrMPInClasMulBxa_Qtde: TFloatField;
    RGRateio: TRadioGroup;
    RGData: TRadioGroup;
    QrMPInClasMulDataHora: TDateTimeField;
    QrGraGruXGraGruX: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel9: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel10: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtRateiaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruxChange(Sender: TObject);
    procedure EdGraGruxEnter(Sender: TObject);
    procedure EdGraGruxExit(Sender: TObject);
    procedure EdPecasChange(Sender: TObject);
    procedure EdAreaM2Change(Sender: TObject);
    procedure EdAreaP2Change(Sender: TObject);
    procedure RGRateioClick(Sender: TObject);
    procedure RGMovimentoClick(Sender: TObject);
    procedure EdPesoChange(Sender: TObject);
    procedure EdReduzidoChange(Sender: TObject);
    procedure EdStqCenCadChange(Sender: TObject);
    procedure RGGrandezaClick(Sender: TObject);
    procedure RGBaixaClick(Sender: TObject);
    procedure RGDataClick(Sender: TObject);
  private
    { Private declarations }
    FMPInClasMul: String;
    FRateou: Boolean;
    //FGraGru1: Integer;
    procedure HabilitaOK();
    procedure HabilitaRateio();
    procedure ReabreMPInClasMul();
    procedure CancelaRateio();

  public
    { Public declarations }
  end;

  var
  FmMPClasMul: TFmMPClasMul;

implementation

uses UnMyObjects, MPIn, ModuleGeral, UCreate, dmkGeral, Module, UMySQLModule,
UnGrade_Tabs;

{$R *.DFM}

procedure TFmMPClasMul.BtOKClick(Sender: TObject);
const
  OriCnta = 0;
  OriPart = 0;
var
  IDCtrl, OriCtrl, OriCodi, Empresa, StqCenCad, GraGruX, DebCtrl, SMIMultIns: Integer;
  QtdeGE, QtdeQI, QtdeQK, AreaM2, AreaP2: Double;
  DataHora: String;
  FatorClas: Double;
  Continua: Boolean;
begin
  //FatorClas := 1;
  StqCenCad := EdStqCenCad.ValueVariant;
  if MyObjects.FIC(StqCenCad = 0, EdStqCenCad, 'Informe o centro de estoque') then Exit;
  GraGruX := EdGraGruX.ValueVariant;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o produto') then Exit;
  //
  SMIMultIns := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'StqMovItsA', 'SMIMultIns', 'SMIMultIns');
  QrMPInClasMul.First;
  while not QrMPInClasMul.Eof do
  begin
    IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
    OriCtrl := IDCtrl;
    //
    OriCodi := QrMPInClasMulControle.Value;
    Empresa := QrMPInClasMulClienteI.Value;
    //
    DebCtrl := QrMPInClasMulDebCtrl.Value;
    // obrigat�rio a cada registro pois os dados s�o modificados na segunda SQL abaixo
    FatorClas := 1;
    StqCenCad := QrStqCenCadCodigo.Value;
    GraGruX := EdGraGruX.ValueVariant;
    //
    QtdeQI    := QrMPInClasMulClas_Pecas.Value;
    QtdeQK    := QrMPInClasMulClas_Peso.Value;
    AreaM2    := QrMPInClasMulClas_M2.Value;
    AreaP2    := QrMPInClasMulClas_P2.Value;
    QtdeGE    := QrMPInClasMulClas_Qtde.Value;
    //
    DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', QrMPInClasMulDataHora.Value);
    if RGMovimento.ItemIndex in ([1,3]) then
    begin
      Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
        CO_DATA_HORA_SMI, 'Tipo', 'OriCodi',
        'OriCtrl', 'OriCnta', 'OriPart',
        'Empresa', 'StqCenCad', 'GraGruX',
        'Qtde', 'Pecas', 'Peso', 'AreaM2', 'AreaP2',
        'FatorClas', 'DebCtrl', 'SMIMultIns'
      ], ['IDCtrl'], [
        DataHora, VAR_FATID_0101, OriCodi,
        OriCtrl, OriCnta, OriPart,
        Empresa, StqCenCad, GraGruX,
        //Qtde, Pecas,  Peso,   Area
        QtdeGE, QtdeQI, QtdeQK, AreaM2, AreaP2,
        FatorClas, DebCtrl, SMIMultIns
      ], [IDCtrl], False);
    end else Continua := True;
    if Continua then
    begin
      IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
      FatorClas := 1 / FatorClas; // Inverter!!
      StqCenCad := QrMPInClasMulDebStqCenCad.Value;
      GraGruX   := QrMPInClasMulDebGraGruX.Value;
      //
      QtdeQI    := QrMPInClasMulBxa_Pecas.Value;
      QtdeQK    := QrMPInClasMulBxa_Peso.Value;
      AreaM2    := QrMPInClasMulBxa_M2.Value;
      AreaP2    := QrMPInClasMulBxa_P2.Value;
      QtdeGE    := QrMPInClasMulBxa_Qtde.Value;
      //
      //
      if RGMovimento.ItemIndex in ([1,2]) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
          CO_DATA_HORA_SMI, 'Tipo', 'OriCodi',
          'OriCtrl', 'OriCnta', 'OriPart',
          'Empresa', 'StqCenCad', 'GraGruX',
          'Qtde', 'Pecas', 'Peso', 'AreaM2', 'AreaP2',
          'FatorClas', 'SMIMultIns'
        ], ['IDCtrl'], [
          DataHora, VAR_FATID_0104, OriCodi,
          OriCtrl, OriCnta, OriPart,
          Empresa, StqCenCad, GraGruX,
          //Qtde, Pecas,  Peso,   Area
          QtdeGE, QtdeQI, QtdeQK, AreaM2, AreaP2,
          FatorClas, SMIMultIns
        ], [IDCtrl], False) then ;
      end;
    end;
    Dmod.AtualizaMPIn(OriCodi);
    //
    QrMPInClasMul.Next;
  end;
  FmMPIn.ReopenPRi(qqMPIn, FmMPIn.FControle);
  Close;
end;

procedure TFmMPClasMul.BtRateiaClick(Sender: TObject);
var
  Controle, Continua: Integer;
  FatorPc(*, FatorM2, FatorP2*), FatorKg: Double;
  SdoM2, SdoKg: Double;
  SdoPc, Clas_Pecas, DebCtrl, DebStqCenCad, DebGraGruX: Integer;
  Clas_Qtde, Clas_M2, Clas_P2, Clas_Peso: Double;
  Bxa_Qtde, Bxa_Pecas, Bxa_M2, Bxa_P2, Bxa_Peso, MyFat: Double;
  DataHora: String;
begin
  ReabreMPInClasMul();
  if QrSomaEstq_Pecas.Value <> EdPecas.ValueVariant then
    Continua := Geral.MB_Pergunta('Quantidade de pe�as n�o confere!' + sLineBreak +
    'Pe�as estoque:          ' + FloatToStr(QrSomaEstq_Pecas.Value) + sLineBreak +
    'Pe�as classifica��o: ' + FloatToStr(EdPecas.ValueVariant) + sLineBreak +
    'Deseja continuar assim mesmo?')
  else Continua := ID_YES;
  FatorPC := 0;
  FatorKg := 0;
  if Continua = ID_YES then
  begin
    case RGRateio.ItemIndex of
      0:
      begin
        if QrSomaEstq_Pecas.Value = 0 then FatorPc := 0 else
        FatorPc := EdPecas.ValueVariant / QrSomaEstq_Pecas.Value;
      end;
      1:
      begin
        if QrSomaEstq_Peso.Value = 0 then FatorKg := 0 else
        Fatorkg := EdPeso.ValueVariant / QrSomaEstq_Peso.Value;
      end;
      else
      begin
        Geral.MB_Erro('Forma de rateio n�o implementada!');
        Exit;
      end;
    end;
    //
    {
    if EdPecas.ValueVariant = 0 then FatorM2 := 0 else
    FatorM2 := EdAreaM2.ValueVariant / EdPecas.ValueVariant;
    //
    if EdPecas.ValueVariant = 0 then FatorKg := 0 else
    FatorKg := EdPeso.ValueVariant / EdPecas.ValueVariant;;
    }
    //
    {
    SdoPc := QrSomaEstq_Pecas.Value;
    SdoM2 := QrSomaEstq_M2.Value;
    SdoP2 := QrSomaEstq_P2.Value;
    SdoKg := QrSomaEstq_Peso.Value;
    }
    SdoPc := EdPecas.ValueVariant;
    SdoM2 := EdAreaM2.ValueVariant;
    Sdokg := EdPeso.ValueVariant;
    //
    //DModG.QrUpdPID1.SQL.Clear; Parei aqui!
    //
    //Bxa_Pecas := 0;
    //Bxa_M2    := 0;
    //Bxa_P2    := 0;
    Bxa_Peso  := 0;
    QrMPInClasMul.First;
    while not QrMPInClasMul.Eof do
    begin
      Clas_Qtde := 0;
      Controle := QrMPInClasMulControle.Value;
      if not Dmod.ReopenSMI(QrMPInClasMulCodigo.Value, Controle) then
        Exit;
      DebCtrl := Dmod.QrSMIIDCtrl.Value;
      DebStqCenCad := Dmod.QrSMIStqCenCad.Value;
      DebGraGruX := Dmod.QrSMIGraGruX.Value;
      //
      Clas_Pecas := 0;
      Clas_M2    := 0;
      Clas_P2    := 0;
      Clas_Peso  := 0;
      if QrMPInClasMul.RecNo = QrMPInClasMul.RecordCount then
      begin
        {
        Bxa_Pecas := SdoPc;
        Bxa_M2    := SdoM2;
        Bxa_P2    := SdoP2;
        Bxa_Peso  := SdoKg;
        }
        Clas_Pecas := SdoPc;
        Clas_M2    := SdoM2;
        Clas_P2    := Geral.ConverteArea(SdoM2, ctM2toP2, cfQuarto);
        Clas_Peso  := SdoKg;
      end else begin
        {
        Bxa_Pecas := QrMPInBxaMulEstq_Pecas.Value * FatorPc;
        Bxa_M2    := QrMPInBxaMulEstq_M2.Value    * FatorM2;
        Bxa_P2    := QrMPInBxaMulEstq_P2.Value    * FatorP2;
        Bxa_Peso  := QrMPInBxaMulEstq_Peso.Value  * FatorKg;
        }
        //
        case RGRateio.ItemIndex of
          0:
          begin
            Clas_Pecas := Round(QrMPInClasMulEstq_Pecas.Value * FatorPc);
            if EdPecas.ValueVariant = 0 then
            begin
              Clas_M2    := 0;
              Clas_P2    := 0;
              Clas_Peso  := 0;
            end else begin
              Clas_M2    := EdAreaM2.ValueVariant / EdPecas.ValueVariant * Clas_Pecas;
              Clas_P2    := Geral.ConverteArea(Clas_M2, ctM2toP2, cfQuarto);
              Clas_Peso  := EdPeso.ValueVariant / EdPecas.ValueVariant * Clas_Pecas;
            end;
          end;
          1:
          begin
            Clas_Peso := Round(QrMPInClasMulEstq_Peso.Value * Fatorkg * 1000) / 1000;
            if EdPeso.ValueVariant = 0 then
            begin
              Clas_M2    := 0;
              Clas_P2    := 0;
              Clas_Peso  := 0;
            end else begin
              Clas_M2    := EdAreaM2.ValueVariant / EdPeso.ValueVariant * Clas_Peso;
              Clas_P2    := Geral.ConverteArea(Clas_M2, ctM2toP2, cfQuarto);
              Clas_Pecas := Round(EdPecas.ValueVariant / EdPeso.ValueVariant * Clas_Peso);
            end;
          end;
        end;
        //
        case QrGraGruXGerBxaEstq.Value of
          1:
          begin
            case RGBaixa.ItemIndex of
              0: Clas_Qtde := QrMPInClasMulEstq_Pecas.Value;
              1: Clas_Qtde := Clas_Pecas;
            end;
          end;
          2: Clas_Qtde := Clas_M2; // Controlar sempre por M2 o estoque da �rea?
          3: Clas_Qtde := Clas_Peso;
          else Clas_Qtde := 0;
        end;
        //
        SdoPc := SdoPc - Clas_Pecas;
        SdoM2 := SdoM2 - Clas_M2;
        SdoKg := SdoKg - Clas_Peso;
        //
      end;
      Bxa_Qtde  := 0;
      Bxa_Pecas := 0;
      Bxa_M2    := 0;
      Bxa_P2    := 0;
      if RGMovimento.ItemIndex in ([1,2]) then
      begin
        case QrMPInClasMulGerBxaEstq.Value of
          1:
          begin
            //case RGBaixa.ItemIndex of
              //0: Bxa_Qtde := - QrMPInClasMulEstq_Pecas.Value;
              //1:
              Bxa_Qtde := - Clas_Pecas;
            //end;
            Bxa_Pecas := Bxa_Qtde;
            if QrMPInClasMulEstq_Pecas.Value > 0 then
            begin
              MyFat := Clas_Pecas / QrMPInClasMulEstq_Pecas.Value;
              Bxa_M2    := - (QrMPInClasMulEstq_M2.Value * MyFat);
              Bxa_P2    := Geral.ConverteArea(Bxa_M2, ctM2toP2, cfQuarto);
              Bxa_Peso  := - (QrMPInClasMulEstq_Peso.Value * MyFat);
            end else begin
              Bxa_M2    := 0;
              Bxa_P2    := 0;
              Bxa_Peso  := 0;
            end;
          end;
          else
          begin
            Geral.MB_Erro('Gerenciador de baixa de estoque n�o implementado!'
            + sLineBreak + 'AVISE a dermatek');
            Screen.Cursor := crDefault;
            Exit;
          end;
        end;
      end;
      if (RGMovimento.ItemIndex in ([1,3])) = False then
      begin
        Clas_Pecas := 0;
        Clas_M2    := 0;
        Clas_P2    := 0;
        Clas_Peso  := 0;
      end;
      //
      case RGData.ItemIndex of
        0: DataHora := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
        1: DataHora := FormatDateTime('yyyy-mm-dd hh:nn:ss', Dmod.QrSMIDataHora.Value + 2);
        2: DataHora := FormatDateTime('yyyy-mm-dd hh:nn:ss', Dmod.QrSMIDataHora.Value + 3);
      end;
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, 'mpinclasmul', False, [
      'Bxa_Qtde', 'Bxa_Pecas', 'Bxa_M2', 'Bxa_P2', 'Bxa_Peso',
      'Clas_Qtde', 'Clas_Pecas', 'Clas_M2', 'Clas_P2', 'Clas_Peso',
      'DebCtrl', 'DebStqCenCad', 'DebGraGruX', 'DataHora'], ['Controle'], [
      Bxa_Qtde, Bxa_Pecas, Bxa_M2, Bxa_P2, Bxa_Peso,
      Clas_Qtde, Clas_Pecas, Clas_M2, Clas_P2, Clas_Peso,
      Debctrl, DebStqCenCad, DebGraGruX, DataHora], [Controle], False);
      //
      QrMPInClasMul.Next;
    end;
    QrMPInClasMul.Close;
    UnDmkDAC_PF.AbreQuery(QrMPInClasMul, DModG.MyPID_DB);
    QrSoma.Close;
    UnDmkDAC_PF.AbreQuery(QrSoma, DmodG.MyPID_DB);
    //  Parei aqui
    FRateou := QrMPInClasMul.RecordCount > 0;
    HabilitaOK();
  end;
end;

procedure TFmMPClasMul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPClasMul.CancelaRateio();
begin
  BtOk.Enabled := False;
  BtRateia.Enabled := False;
end;

procedure TFmMPClasMul.EdAreaM2Change(Sender: TObject);
begin
  CancelaRateio();
  HabilitaRateio();
end;

procedure TFmMPClasMul.EdAreaP2Change(Sender: TObject);
begin
  CancelaRateio();
  HabilitaRateio();
end;

procedure TFmMPClasMul.EdGraGruxChange(Sender: TObject);
begin
  case QrGraGruXGerBxaEstq.Value of
    0:
    begin
      RGMovimento.ItemIndex := 0;
    end;
    1,2:
    begin
      RGRateio.ItemIndex := 0;
      RGRateioClick(Self);
    end;
    3:
    begin
      RGRateio.ItemIndex := 1;
      RGRateioClick(Self);
    end;
  end;
(*
  if EdGraGru1.ValueVariant = 0 then
    EdReduzido.ValueVariant := 0;
  if not EdGraGru1.Focused then
    EdReduzido.ValueVariant := DModG.ObtemReduzidoDeNivel1(QrGraGruXNivel1.Value);
*)
  CancelaRateio();
  HabilitaRateio();
end;

procedure TFmMPClasMul.EdGraGruxEnter(Sender: TObject);
begin
  //FGraGru1 := QrGraGruXNivel1.Value;
end;

procedure TFmMPClasMul.EdGraGruxExit(Sender: TObject);
begin
(*
  if FGraGru1 <> QrGraGruXNivel1.Value then
  begin
    FGraGru1 := QrGraGruXNivel1.Value;
    EdReduzido.ValueVariant := DModG.ObtemReduzidoDeNivel1(FGraGru1);
  end;
*)
end;

procedure TFmMPClasMul.EdPecasChange(Sender: TObject);
begin
  CancelaRateio();
  HabilitaRateio();
end;

procedure TFmMPClasMul.EdPesoChange(Sender: TObject);
begin
  CancelaRateio();
  HabilitaRateio();
end;

procedure TFmMPClasMul.EdReduzidoChange(Sender: TObject);
begin
  CancelaRateio();
  HabilitaRateio();
end;

procedure TFmMPClasMul.EdStqCenCadChange(Sender: TObject);
begin
  CancelaRateio();
  HabilitaRateio();
end;

procedure TFmMPClasMul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPClasMul.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //
  //FMPInClasMul := UCriar.RecriaTempTable('MPInClasMul', DmodG.QrUpdPID1, False);
  FMPInClasMul := UCriar.RecriaTempTableNovo(ntrttMPInClasMul, DmodG.QrUpdPID1, False);
  //
  FRateou := False;
  QrMPInClasMul.Database := DModG.MyPID_DB;
  QrSoma.Database := DModG.MyPID_DB;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  if QrStqCenCad.RecordCount = 1 then
  begin
    EdStqCenCad.ValueVariant := QrStqCenCadCodUsu.Value;
    CBStqCenCad.KeyValue     := QrStqCenCadCodUsu.Value;
  end;
  RGRateioClick(Self);
end;

procedure TFmMPClasMul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPClasMul.HabilitaOK();
var
  Habilita: Boolean;
begin
  Habilita := (EdStqCenCad.ValueVariant > 0) and (EdGraGruX.ValueVariant > 0) and FRateou;
  {
  case RGRateio.ItemIndex of
    0: Habilita := Habilita and (QrSomaClas_M2.Value > 0);
    1: Habilita := Habilita and (QrSomaClas_Peso.Value > 0);
  end;
  }
  BtOK.Enabled := Habilita;
end;

procedure TFmMPClasMul.HabilitaRateio();
var
  Habilita: Boolean;
begin
  Habilita := (EdPecas.ValueVariant > 0) and (EdGraGruX.ValueVariant > 0);
  case QrGraGruXGerBxaEstq.Value of
    1: Habilita := Habilita and (EdPecas.ValueVariant > 0);
    2: Habilita := Habilita and (EdAreaM2.ValueVariant > 0);
    3: Habilita := Habilita and (EdPeso.ValueVariant > 0);
  end;
  Habilita := Habilita and (RGMovimento.ItemIndex > 0);
  //
  BtRateia.Enabled := Habilita;
end;

procedure TFmMPClasMul.ReabreMPInClasMul();
var
  Texto: String;
  p: Integer;
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM mpinclasmul');
  DModG.QrUpdPID1.ExecSQL;
  //
  Texto := FmMPIn.QrMPIn.SQL.Text;
  p := Pos(FmMPIn.FFromMPIn, Texto);
  Texto := Copy(Texto, p);// + Length(FmMPIn.FFromMPIn));
  Texto := 'INSERT INTO ' + DModG.MyPID_DB.DatabaseName + '.' + FMPInClasMul + sLineBreak +
  'SELECT wi.Codigo, wi.Controle, wi.Data, wi.ClienteI,' + sLineBreak +
  'wi.Procedencia, wi.Transportadora, wi.Marca, wi.Lote,' + sLineBreak +
  'wi.Pecas, wi.PLE, ' + sLineBreak +
  'wi.PecasOut, wi.PecasSal, wi.Estq_Pecas, wi.Estq_M2,' + sLineBreak +
  'wi.Estq_P2, wi.Estq_Peso, wi.StqMovIts, ' + sLineBreak +
  '0 Debctrl, 0 DebStqCenCad, 0 DebGraGruX, 1 GerBxaEstq, ' + sLineBreak +     // GerBxaEstq > Couro
  '0 Clas_Qtde, 0 Clas_Pecas, 0 Clas_M2, 0 Clas_P2, 0 Clas_Peso, ' + sLineBreak +
  '0 Bxa_Qtde, 0 Bxa_Pecas, 0 Bxa_M2, 0 Bxa_P2, 0 Bxa_Peso, ' + sLineBreak +
  '0 DataHora, wi.Tipificacao, wi.Animal ' + sLineBreak + Texto;
  //'FROM ' + TMeuDB + '.mpin wi' + Texto;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Text := Texto;
  Dmod.QrUpd.ExecSQL;
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM mpinclasmul');
  case RGRateio.ItemIndex of
    0: DModG.QrUpdPID1.SQL.Add('WHERE Estq_Pecas < 1');
    1: DModG.QrUpdPID1.SQL.Add('WHERE Estq_Peso <= 0');
  end;
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.ExecSQL;
  //
  QrMPInClasMul.Close;
  UnDmkDAC_PF.AbreQuery(QrMPInClasMul, DmodG.MyPID_DB);
  QrSoma.Close;
  UnDmkDAC_PF.AbreQuery(QrSoma, DmodG.MyPID_DB);
end;

procedure TFmMPClasMul.RGBaixaClick(Sender: TObject);
begin
  CancelaRateio();
  HabilitaRateio();
end;

procedure TFmMPClasMul.RGDataClick(Sender: TObject);
begin
  CancelaRateio();
  HabilitaRateio();
end;

procedure TFmMPClasMul.RGGrandezaClick(Sender: TObject);
begin
  CancelaRateio();
  HabilitaRateio();
end;

procedure TFmMPClasMul.RGMovimentoClick(Sender: TObject);
begin
  CancelaRateio();
  HabilitaRateio();
end;

procedure TFmMPClasMul.RGRateioClick(Sender: TObject);
begin
  CancelaRateio();
  case RGRateio.ItemIndex of
    0: RGMovimento.ItemIndex := 1;
    1: RGMovimento.ItemIndex := 3;
  end;
  ReabreMPInClasMul();
  HabilitaRateio();
end;

end.
