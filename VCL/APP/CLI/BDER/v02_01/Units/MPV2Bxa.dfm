object FmMPV2Bxa: TFmMPV2Bxa
  Left = 339
  Top = 185
  Caption = 'VEN-COURO-202 :: Baixa de Couros'
  ClientHeight = 516
  ClientWidth = 680
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 680
    Height = 360
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 222
    ExplicitHeight = 260
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 680
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 678
      object RGPeso: TRadioGroup
        Left = 301
        Top = 0
        Width = 379
        Height = 48
        Align = alClient
        Caption = ' Baixa de peso por: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'PNF (Peso Nota Fiscal)'
          'PLE (Peso L'#237'quido de Entrada)')
        TabOrder = 0
        ExplicitWidth = 377
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 301
        Height = 48
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 64
          Height = 13
          Caption = 'Marca (Filtro):'
        end
        object CkMostrarTodas: TCheckBox
          Left = 136
          Top = 24
          Width = 161
          Height = 17
          Caption = 'Mostrar marcas sem estoque.'
          TabOrder = 0
        end
        object EdMarca: TEdit
          Left = 8
          Top = 20
          Width = 121
          Height = 21
          TabOrder = 1
          OnChange = EdMarcaChange
        end
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 48
      Width = 680
      Height = 255
      Align = alClient
      DataSource = DsMarcas
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PcsSdo'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PNFSdo'
          Title.Caption = 'PNF'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PLESdo'
          Title.Caption = 'PLE'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'M2Sdo'
          Title.Caption = #193'rea m'#178
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'P2Sdo'
          Title.Caption = #193'rea ft'#178
          Width = 80
          Visible = True
        end>
    end
    object Panel4: TPanel
      Left = 0
      Top = 303
      Width = 680
      Height = 57
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      ExplicitLeft = 1
      ExplicitTop = 202
      ExplicitWidth = 678
      object Label2: TLabel
        Left = 8
        Top = 8
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label3: TLabel
        Left = 80
        Top = 8
        Width = 42
        Height = 13
        Caption = 'Peso kg:'
      end
      object Label4: TLabel
        Left = 176
        Top = 8
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
      end
      object Label5: TLabel
        Left = 260
        Top = 8
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
      end
      object dmkEdPecas: TdmkEdit
        Left = 8
        Top = 24
        Width = 69
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 1
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object dmkEdPeso: TdmkEdit
        Left = 80
        Top = 24
        Width = 93
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object dmkEdM2: TdmkEdit
        Left = 176
        Top = 24
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object dmkEdP2: TdmkEdit
        Left = 260
        Top = 24
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 680
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 632
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 584
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 420
        Height = 32
        Caption = 'Gerenciamento de Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 420
        Height = 32
        Caption = 'Gerenciamento de Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 420
        Height = 32
        Caption = 'Gerenciamento de Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 408
    Width = 680
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 192
    ExplicitWidth = 635
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 676
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 452
    Width = 680
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 236
    ExplicitWidth = 635
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 676
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 532
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrMarcas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Marca, (Pecas-PcBxa) PcsSdo, '
      '(PNF-kgBxa) PNFSdo, (PLE-kgBxa) PLESdo, (M2-M2Bxa) '
      'M2Sdo, (P2-P2Bxa) P2Sdo'
      'FROM mpin'
      'WHERE Marca LIKE :P0'
      'AND Pecas > PcBxa'
      'ORDER BY Data DESC')
    Left = 88
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMarcasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMarcasMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrMarcasPcsSdo: TFloatField
      FieldName = 'PcsSdo'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrMarcasPNFSdo: TFloatField
      FieldName = 'PNFSdo'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrMarcasPLESdo: TFloatField
      FieldName = 'PLESdo'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrMarcasM2Sdo: TFloatField
      FieldName = 'M2Sdo'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMarcasP2Sdo: TFloatField
      FieldName = 'P2Sdo'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsMarcas: TDataSource
    DataSet = QrMarcas
    Left = 116
    Top = 148
  end
end
