unit CMPTInn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkDBGrid, DB,
  mySQLDbTables, dmkEdit, dmkEditCB, DBCtrls, dmkDBLookupComboBox,
  ComCtrls, dmkEditDateTimePicker, dmkLabel, dmkGeral, MyDBCheck, frxClass,
  frxDBSet, Menus, Mask, UnDmkProcFunc, dmkImage, UnDmkEnums, dmkDBGridZTO;

type
  TFmCMPTInn = class(TForm)
    Panel1: TPanel;
    PnPesquisa: TPanel;
    Panel4: TPanel;
    PnGrades: TPanel;
    GradeInn: TdmkDBGridZTO;
    QrCMPTInn: TmySQLQuery;
    DsCMPTInn: TDataSource;
    QrCMPTInnNOMECLI: TWideStringField;
    QrCMPTInnCodigo: TIntegerField;
    QrCMPTInnCliente: TIntegerField;
    QrCMPTInnDataE: TDateField;
    QrCMPTInnNFInn: TIntegerField;
    QrCMPTInnNFRef: TIntegerField;
    QrCMPTInnInnQtdkg: TFloatField;
    QrCMPTInnInnQtdPc: TFloatField;
    QrCMPTInnInnQtdVal: TFloatField;
    QrCMPTInnSdoQtdkg: TFloatField;
    QrCMPTInnSdoQtdPc: TFloatField;
    QrCMPTInnSdoQtdVal: TFloatField;
    GroupBox1: TGroupBox;
    QrClientesPesq: TmySQLQuery;
    DsClientesPesq: TDataSource;
    QrClientesPesqCodigo: TIntegerField;
    QrClientesPesqNOMECLI: TWideStringField;
    GradeIts: TDBGrid;
    Splitter1: TSplitter;
    QrCMPTInnIts: TmySQLQuery;
    DsCMPTInnIts: TDataSource;
    QrCMPTInnItsCodigo: TIntegerField;
    QrCMPTInnItsControle: TIntegerField;
    QrCMPTInnItsCMPTInn: TIntegerField;
    QrCMPTInnItsOutQtdkg: TFloatField;
    QrCMPTInnItsOutQtdPc: TFloatField;
    QrCMPTInnItsOutQtdVal: TFloatField;
    QrCMPTInnItsDataS: TDateField;
    QrCMPTInnItsNFOut: TIntegerField;
    RGAbertos: TRadioGroup;
    Panel3: TPanel;
    CBClientePesq: TdmkDBLookupComboBox;
    EdClientePesq: TdmkEditCB;
    Label1: TLabel;
    frxCMPTInn: TfrxReport;
    frxDsCMPTInn: TfrxDBDataset;
    PMImpressao: TPopupMenu;
    Imprimeestoquepesquisado1: TMenuItem;
    Removeordenao1: TMenuItem;
    Panel6: TPanel;
    CkIni: TCheckBox;
    TPIni: TdmkEditDateTimePicker;
    CkFim: TCheckBox;
    TPFim: TdmkEditDateTimePicker;
    Label10: TLabel;
    EdFilialPesq: TdmkEditCB;
    CBFilialPesq: TdmkDBLookupComboBox;
    QrCMPTInnNOMEEMP: TWideStringField;
    QrCMPTInnInnQtdM2: TFloatField;
    QrCMPTInnSdoQtdM2: TFloatField;
    QrCMPTInnEmpresa: TIntegerField;
    QrCMPTInnGraGruX: TIntegerField;
    QrCMPTInnTipoNF: TSmallintField;
    QrCMPTInnrefNFe: TWideStringField;
    QrCMPTInnmodNF: TSmallintField;
    QrCMPTInnSerie: TIntegerField;
    QrCMPTInnFilial: TIntegerField;
    QrGBE: TmySQLQuery;
    QrGBEGerBxaEstq: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtDevolve: TBitBtn;
    BtImpressao: TBitBtn;
    BtSaida: TBitBtn;
    QrCMPTInnLk: TIntegerField;
    QrCMPTInnDataCad: TDateField;
    QrCMPTInnDataAlt: TDateField;
    QrCMPTInnUserCad: TIntegerField;
    QrCMPTInnUserAlt: TIntegerField;
    QrCMPTInnAlterWeb: TSmallintField;
    QrCMPTInnAtivo: TSmallintField;
    QrCMPTInnjaAtz: TSmallintField;
    QrCMPTInnProcedencia: TIntegerField;
    QrCMPTInnValMaoObra: TFloatField;
    QrCMPTInnNF_CC: TIntegerField;
    QrCMPTInnNOMEPRC: TWideStringField;
    QrCMPTImp: TmySQLQuery;
    QrCMPTImpNOMECLI: TWideStringField;
    QrCMPTImpCodigo: TIntegerField;
    QrCMPTImpCliente: TIntegerField;
    QrCMPTImpDataE: TDateField;
    QrCMPTImpNFInn: TIntegerField;
    QrCMPTImpNFRef: TIntegerField;
    QrCMPTImpInnQtdkg: TFloatField;
    QrCMPTImpInnQtdPc: TFloatField;
    QrCMPTImpInnQtdVal: TFloatField;
    QrCMPTImpSdoQtdkg: TFloatField;
    QrCMPTImpSdoQtdPc: TFloatField;
    QrCMPTImpSdoQtdVal: TFloatField;
    QrCMPTImpNOMEEMP: TWideStringField;
    QrCMPTImpInnQtdM2: TFloatField;
    QrCMPTImpSdoQtdM2: TFloatField;
    QrCMPTImpEmpresa: TIntegerField;
    QrCMPTImpGraGruX: TIntegerField;
    QrCMPTImpTipoNF: TSmallintField;
    QrCMPTImprefNFe: TWideStringField;
    QrCMPTImpmodNF: TSmallintField;
    QrCMPTImpSerie: TIntegerField;
    QrCMPTImpFilial: TIntegerField;
    QrCMPTImpLk: TIntegerField;
    QrCMPTImpDataCad: TDateField;
    QrCMPTImpDataAlt: TDateField;
    QrCMPTImpUserCad: TIntegerField;
    QrCMPTImpUserAlt: TIntegerField;
    QrCMPTImpAlterWeb: TSmallintField;
    QrCMPTImpAtivo: TSmallintField;
    QrCMPTImpjaAtz: TSmallintField;
    QrCMPTImpProcedencia: TIntegerField;
    QrCMPTImpValMaoObra: TFloatField;
    QrCMPTImpNF_CC: TIntegerField;
    QrCMPTImpNOMEPRC: TWideStringField;
    GroupBox2: TGroupBox;
    CkInnQtdVal: TCheckBox;
    Panel2: TPanel;
    EdInnQtdValMin: TdmkEdit;
    Label2: TLabel;
    EdInnQtdValMax: TdmkEdit;
    Label3: TLabel;
    QrCMPTInnItsOutQtdM2: TFloatField;
    QrCMPTInnItsBenefikg: TFloatField;
    QrCMPTInnItsBenefiPc: TFloatField;
    QrCMPTInnItsBenefiM2: TFloatField;
    QrCMPTInnItsSitDevolu: TSmallintField;
    QrCMPTInnItsLk: TIntegerField;
    QrCMPTInnItsDataCad: TDateField;
    QrCMPTInnItsDataAlt: TDateField;
    QrCMPTInnItsUserCad: TIntegerField;
    QrCMPTInnItsUserAlt: TIntegerField;
    QrCMPTInnItsAlterWeb: TSmallintField;
    QrCMPTInnItsAtivo: TSmallintField;
    QrCMPTInnItsMOPesoKg: TFloatField;
    QrCMPTInnItsMOPrcKg: TFloatField;
    QrCMPTInnItsMOValor: TFloatField;
    QrCMPTInnItsHowCobrMO: TSmallintField;
    QrCMPTInnItsNFCMO: TIntegerField;
    QrCMPTInnItsNFDMP: TIntegerField;
    QrCMPTInnItsNFDIQ: TIntegerField;
    QrCMPTInnHowCobrMO: TSmallintField;
    QrCMPTInnPLEKg: TFloatField;
    QrCMPTInnPrcUnitMO: TFloatField;
    QrCMPTInnSdoFMOVal: TFloatField;
    QrCMPTInnSdoFMOKg: TFloatField;
    QrCMPTImpHowCobrMO: TSmallintField;
    QrCMPTImpPLEKg: TFloatField;
    QrCMPTImpPrcUnitMO: TFloatField;
    QrCMPTImpSdoFMOVal: TFloatField;
    QrCMPTImpSdoFMOKg: TFloatField;
    frxReport1: TfrxReport;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdClientePesqChange(Sender: TObject);
    procedure CkAbertosClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrCMPTInnBeforeClose(DataSet: TDataSet);
    procedure QrCMPTInnAfterScroll(DataSet: TDataSet);
    procedure BtDevolveClick(Sender: TObject);
    procedure RGAbertosClick(Sender: TObject);
    procedure CkIniClick(Sender: TObject);
    procedure CkFimClick(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure BtImpressaoClick(Sender: TObject);
    procedure Removeordenao1Click(Sender: TObject);
    procedure Imprimeestoquepesquisado1Click(Sender: TObject);
    procedure frxCMPTInnGetValue(const VarName: string; var Value: Variant);
    procedure EdFilialPesqChange(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdInnQtdValMinClick(Sender: TObject);
    procedure CkInnQtdValClick(Sender: TObject);
    procedure EdInnQtdValMaxChange(Sender: TObject);
    procedure EdInnQtdValMinChange(Sender: TObject);
    procedure QrCMPTInnAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FCriando: Boolean;
  public
    { Public declarations }
    procedure MostraEdicao(Acao: TSQLType);
    procedure ReopenCMPTInX(Qry: TmySQLQuery; OrderBy: String);
    procedure ReopenCMPTInn(Codigo: Integer);
    //procedure ReopenCMPTOut(Codigo: Integer);
    procedure ReopenCMPTInnIts(Controle: Integer);
  end;

  var
  FmCMPTInn: TFmCMPTInn;

implementation

uses UnMyObjects, Module, UMySQLModule, CMPTOut, ModuleGeral, CMPTInnAdd,
  DMkDAC_PF;

{$R *.DFM}

procedure TFmCMPTInn.BtImpressaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImpressao, BtImpressao);
end;

procedure TFmCMPTInn.BtAlteraClick(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(
    TFmCMPTInnAdd, FmCMPTInnAdd, afmoNegarComAviso, QrCMPTInn, stUpd);
end;

procedure TFmCMPTInn.BtDevolveClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCMPTOut, FmCMPTOut, afmoNegarComAviso) then
  begin
    FmCMPTOut.ShowModal;
    FmCMPTOut.Destroy;
  end;
end;

procedure TFmCMPTInn.BtExcluiClick(Sender: TObject);
var
  OK: Boolean;
  Resp: Integer;
begin
  //OK := False;
  if (QrCMPTInn.State <> dsInactive) and (QrCMPTInn.RecordCount > 0) then
  begin
    QrGBE.Close;
    QrGBE.Params[0].AsInteger := QrCMPTInnCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrGBE, Dmod.MyDB);
    case QrGBEGerBxaEstq.Value of
      1: OK := QrCMPTInnSdoQtdPc.Value = QrCMPTInnInnQtdPc.Value;
      2: OK := QrCMPTInnSdoQtdM2.Value = QrCMPTInnInnQtdM2.Value;
      3: OK := QrCMPTInnSdoQtdkg.Value = QrCMPTInnInnQtdkg.Value;
      else begin
        Geral.MB_Aviso(
        'Mat�ria-prima sem defini��o de "Unidade Controladora de Estoque"');
        Exit;
      end;
    end;
    if OK then
    begin
      Resp := Geral.MB_Pergunta(
      'Deseja realmente exclui esta nota de entrada?');
      if Resp = ID_YES then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM CMPTinn WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrCMPTInnCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        ReopenCMPTInn(0);
      end;
    end else Geral.MB_Aviso(
    'N�o � mais permitido excluir esta entrada pois ela j� tem movimento!');
  end;
end;

procedure TFmCMPTInn.BtIncluiClick(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(
    TFmCMPTInnAdd, FmCMPTInnAdd, afmoNegarComAviso, QrCMPTInnIts, stIns);
end;

procedure TFmCMPTInn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCMPTInn.CkAbertosClick(Sender: TObject);
begin
  ReopenCMPTInn(QrCMPTInnCodigo.Value);
end;

procedure TFmCMPTInn.CkFimClick(Sender: TObject);
begin
  ReopenCMPTInn(QrCMPTInnCodigo.Value);
end;

procedure TFmCMPTInn.CkIniClick(Sender: TObject);
begin
  ReopenCMPTInn(QrCMPTInnCodigo.Value);
end;

procedure TFmCMPTInn.CkInnQtdValClick(Sender: TObject);
begin
  ReopenCMPTInn(QrCMPTInnCodigo.Value);
end;

procedure TFmCMPTInn.EdClientePesqChange(Sender: TObject);
begin
  ReopenCMPTInn(QrCMPTInnCodigo.Value);
end;

procedure TFmCMPTInn.EdFilialPesqChange(Sender: TObject);
begin
  ReopenCMPTInn(QrCMPTInnCodigo.Value);
end;

procedure TFmCMPTInn.EdInnQtdValMaxChange(Sender: TObject);
begin
  if CkInnQtdVal.Checked then
    ReopenCMPTInn(QrCMPTInnCodigo.Value);
end;

procedure TFmCMPTInn.EdInnQtdValMinChange(Sender: TObject);
begin
  if CkInnQtdVal.Checked then
    ReopenCMPTInn(QrCMPTInnCodigo.Value);
end;

procedure TFmCMPTInn.EdInnQtdValMinClick(Sender: TObject);
begin
  ReopenCMPTInn(QrCMPTInnCodigo.Value);
end;

procedure TFmCMPTInn.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCMPTInn.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCriando := True;
  //
  UnDmkDAC_PF.AbreQuery(QrClientesPesq, Dmod.MyDB);
  TPIni.date   := Date - 30;
  TPFim.date   := Date;
  //
  FCriando := False;
  //
  if DModG.QrFiliLog.State = dsInactive then
    Geral.MB_Aviso('N�o empresa logada!')
  else
  if DModG.QrFiliLog.RecordCount = 1 then
  begin
    EdFilialPesq.ValueVariant := DModG.QrFiliLogFilial.Value;
    CBFilialPesq.KeyValue     := DModG.QrFiliLogFilial.Value;
  end;
end;

procedure TFmCMPTInn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCMPTInn.frxCMPTInnGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VFR_ORDENADO') = 0 then
    Value := Trim(QrCMPTInn.SortFieldNames) <> ''

  //

  else
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBFilialPesq.Text, EdFilialPesq.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_STATUS' then
    Value := RGAbertos.Items[RGAbertos.ItemIndex]
  else
  if VarName = 'VARF_CLIENTE' then
    Value := dmkPF.ParValueCodTxt(
      '', CBClientePesq.Text, EdClientePesq.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_PROCEDENCIA' then
    Value := 'TODAS'
(*
    Value := dmkPF.ParValueCodTxt(
      '', CB01Terceiro.Text, Ed01Terceiro.ValueVariant, 'TODOS')
*)
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp1(TPIni.Date, TPFim.Date,
    CkIni.Checked, CkFim.Checked, 'Per�odo: ', 'at�', '')
  else
end;

procedure TFmCMPTInn.Imprimeestoquepesquisado1Click(Sender: TObject);
(*
const
  SubTotal = 'Sub-total ';
var
  Campo, Titulo: String;
  k, i: Integer;
*)
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando relat�rio');
  ReopenCMPTInX(QrCMPTImp, 'ORDER BY NOMECLI, NOMEPRC, DataE');
  //
(*
  Campo := QrCMPTInn.SortFieldNames;
  k := pos(' ', Campo);
  if k > 2 then
    Campo := Copy(Campo, 1, k);
  Campo := Trim(Campo);
  if Campo = '' then
    Campo := 'NOMECLI';
  Titulo := '? ? ?';
  for i := 0 to QrCMPTInn.FieldCount - 1 do
  begin
    if QrCMPTInn.Fields[i].FieldName = Campo then
      Titulo := QrCMPTInn.Fields[i].DisplayLabel;
  end;
  //
  frxCMPTInn.Variables['VARF_GRUPO1']   := ''''+'frxDsCMPTInn."'+Campo+'"''';
  frxCMPTInn.Variables['VARF_HEADR1']   := ''''+Titulo+': [frxDsCMPTInn."'+Campo+'"]''';
  frxCMPTInn.Variables['VARF_FOOTR1']   := ''''+SubTotal+Titulo+': [frxDSCMPTInn."'+Campo+'"]''';
*)

  MyObjects.frxMostra(frxCMPTInn, 'Estoque de couros de terceiros');
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmCMPTInn.MostraEdicao(Acao: TSQLType);
begin
  {
  case Acao of
    stIns, stUpd:
    begin
      PnEdita.Visible    := True;
      PnConfirma.Visible := True;
      PnControla.Visible := False;
      PnPesquisa.Enabled := False;
      PnGrades.Enabled   := False;
      if Acao = stUpd then
      begin
        TPDataE.Date               := QrCMPTInnDataE.Value;
        EdClienteEdit.ValueVariant := QrCMPTInnCliente.Value;
        CBClienteEdit.KeyValue     := QrCMPTInnCliente.Value;
        EdNFInn.ValueVariant       := QrCMPTInnNFInn.Value;
        EdNFRef.ValueVariant       := QrCMPTInnNFRef.Value;
        EdInnQtdkg.ValueVariant    := QrCMPTInnInnQtdkg.Value;
        EdInnQtdPc.ValueVariant    := QrCMPTInnInnQtdPc.Value;
        EdInnQtdVal.ValueVariant   := QrCMPTInnInnQtdVal.Value;
        //
        if QrCMPTInnIts.RecordCount > 0 then
        begin
          EdClienteEdit.Enabled := False;
          CBClienteEdit.Enabled := False;
          Application.MessageBox(PChar('N�o ser� poss�vel alterar o cliente, ' +
          'pois j� existe devolu��o para esta NF!'), 'Aviso', MB_OK+MB_ICONWARNING);
        end else begin
          EdClienteEdit.Enabled := True;
          CBClienteEdit.Enabled := True;
        end;
        CkContinuar.Visible := False;
      end else CkContinuar.Visible := True;
      TPDataE.SetFocus;
    end;
    //stDel: ;
    //stCpy: ;
    stLok:
    begin
      PnControla.Visible := True;
      PnPesquisa.Enabled := True;
      PnGrades.Enabled   := True;
      PnConfirma.Visible := False;
      PnEdita.Visible    := False;
    end;
  end;
  ImgTipo.SQLType := Acao;
  }
end;

procedure TFmCMPTInn.QrCMPTInnAfterOpen(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Registros localizados: ' +
    Geral.FF0(QrCMPTInn.RecordCount));
end;

procedure TFmCMPTInn.QrCMPTInnAfterScroll(DataSet: TDataSet);
begin
  ReopenCMPTInnIts(QrCMPTInnItsControle.Value);
end;

procedure TFmCMPTInn.QrCMPTInnBeforeClose(DataSet: TDataSet);
begin
  QrCMPTInnIts.Close;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmCMPTInn.Removeordenao1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  QrCMPTInn.SortFieldNames := '';
  if QrCMPTInn.State <> dsInactive then
  begin
    Codigo := QrCMPTInnCodigo.Value;
    QrCMPTInn.Close;
    UnDmkDAC_PF.AbreQuery(QrCMPTInn, Dmod.MyDB);
    QrCMPTInn.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmCMPTInn.ReopenCMPTInn(Codigo: Integer);
begin
  if FCriando then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    ReopenCMPTInX(QrCMPTInn, 'ORDER BY DataE DESC');
    if Codigo > 0 then
      QrCMPTInn.Locate('Codigo', Codigo, []);
  finally
    Screen.Cursor := crDefault;
  end;
  //
end;

procedure TFmCMPTInn.ReopenCMPTInnIts(Controle: Integer);
begin
  QrCMPTInnIts.Close;
  QrCMPTInnIts.Params[0].AsInteger := QrCMPTInnCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrCMPTInnIts, Dmod.MyDB);
  //
  if Controle > 0 then
    QrCMPTInnIts.Locate('Controle', Controle, []);
end;

procedure TFmCMPTInn.ReopenCMPTInX(Qry: TmySQLQuery; OrderBy: String);
var
  Empresa, Cliente: Integer;
  TXT_Valores: String;
begin
  Cliente := Geral.IMV(EdClientePesq.Text);
  UMyMod.ObtemCodigoDeCodUsu(EdFilialPesq, Empresa, '', 'Codigo', 'Filial');
  TXT_Valores := 'AND inn.InnQtdVal BETWEEN ' +
    Geral.FFT_Dot(EdInnQtdValMin.ValueVariant, 2, siNegativo) + ' AND ' +
    Geral.FFT_Dot(EdInnQtdValMax.ValueVariant, 2, siNegativo);
  //
  UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP, ',
  'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NOMEPRC, ',
  'emp.Filial, inn.* ',
  'FROM cmptinn inn ',
  'LEFT JOIN entidades cli ON inn.Cliente=cli.Codigo ',
  'LEFT JOIN entidades emp ON inn.Empresa=emp.Codigo ',
  'LEFT JOIN entidades prc ON inn.Procedencia=prc.Codigo ',
  dmkPF.SQL_Periodo('WHERE inn.DataE ', TPIni.date, TPFim.Date, CkIni.Checked, CkFim.Checked),
  Geral.ATS_if(Cliente <> 0, [' AND inn.Cliente=' + Geral.FF0(Cliente)]),
  Geral.ATS_if(Empresa <> 0, [' AND inn.Empresa=' + Geral.FF0(Empresa)]),
  Geral.ATS_if(RGAbertos.ItemIndex = 0, ['AND inn.SdoQtdPC > 0 ']),
  Geral.ATS_if(RGAbertos.ItemIndex = 1, ['AND inn.SdoQtdPC = 0 ']),
  Geral.ATS_if(CkInnQtdVal.Checked, [TXT_Valores]),
  OrderBy,
  '']);
end;

procedure TFmCMPTInn.RGAbertosClick(Sender: TObject);
begin
  ReopenCMPTInn(QrCMPTInnCodigo.Value);
end;

procedure TFmCMPTInn.TPFimClick(Sender: TObject);
begin
  ReopenCMPTInn(QrCMPTInnCodigo.Value);
end;

procedure TFmCMPTInn.TPIniClick(Sender: TObject);
begin
  ReopenCMPTInn(QrCMPTInnCodigo.Value);
end;

end.

