unit MPV2;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, Menus, frxClass, frxDBSet, dmkEdit, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEditCB, UnDmkProcFunc, dmkLabel, dmkImage, UnDmkEnums;

type
  TFmMPV2 = class(TForm)
    PainelDados: TPanel;
    DsMPV2: TDataSource;
    QrMPV2: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrTransp: TmySQLQuery;
    DsTransp: TDataSource;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrTranspCodigo: TIntegerField;
    QrTranspNOMEENTIDADE: TWideStringField;
    TPDataF: TDateTimePicker;
    Label5: TLabel;
    QrMPV2NOMECLIENTE: TWideStringField;
    QrMPV2NOMETRANSP: TWideStringField;
    QrMPV2Codigo: TIntegerField;
    QrMPV2Cliente: TIntegerField;
    QrMPV2Transp: TIntegerField;
    QrMPV2DataF: TDateField;
    QrMPV2Lk: TIntegerField;
    QrMPV2DataCad: TDateField;
    QrMPV2DataAlt: TDateField;
    QrMPV2UserCad: TIntegerField;
    QrMPV2UserAlt: TIntegerField;
    PainelIts: TPanel;
    Painel1: TPanel;
    QrMPs: TmySQLQuery;
    DsMPs: TDataSource;
    QrMPsCodigo: TIntegerField;
    QrMPsNome: TWideStringField;
    DBGrid4: TDBGrid;
    QrMPV2Its: TmySQLQuery;
    DsMPV2Its: TDataSource;
    QrTotais: TmySQLQuery;
    QrMPV2Qtde: TFloatField;
    QrMPV2Valor: TFloatField;
    QrFornecedor: TmySQLQuery;
    QrFornecedorFAX_TXT: TWideStringField;
    QrFornecedorCEL_TXT: TWideStringField;
    QrFornecedorCPF_TXT: TWideStringField;
    QrFornecedorTEL_TXT: TWideStringField;
    QrFornecedorCEP_TXT: TWideStringField;
    QrFornecedorNOME: TWideStringField;
    QrFornecedorRUA: TWideStringField;
    QrFornecedorCOMPL: TWideStringField;
    QrFornecedorBAIRRO: TWideStringField;
    QrFornecedorCIDADE: TWideStringField;
    QrFornecedorPAIS: TWideStringField;
    QrFornecedorTELEFONE: TWideStringField;
    QrFornecedorFAX: TWideStringField;
    QrFornecedorCelular: TWideStringField;
    QrFornecedorCNPJ: TWideStringField;
    QrFornecedorIE: TWideStringField;
    QrFornecedorContato: TWideStringField;
    QrFornecedorNOMEUF: TWideStringField;
    QrFornecedorEEMail: TWideStringField;
    QrFornecedorPEmail: TWideStringField;
    QrFornecedorNUMEROTXT: TWideStringField;
    QrFornecedorENDERECO: TWideStringField;
    QrFornecedorLOGRAD: TWideStringField;
    QrTransportador: TmySQLQuery;
    QrTransportadorTEL_TXT: TWideStringField;
    QrTransportadorCEL_TXT: TWideStringField;
    QrTransportadorCPF_TXT: TWideStringField;
    QrTransportadorFAX_TXT: TWideStringField;
    QrTransportadorCEP_TXT: TWideStringField;
    QrTransportadorNOME: TWideStringField;
    QrTransportadorRUA: TWideStringField;
    QrTransportadorCOMPL: TWideStringField;
    QrTransportadorBAIRRO: TWideStringField;
    QrTransportadorCIDADE: TWideStringField;
    QrTransportadorPAIS: TWideStringField;
    QrTransportadorTELEFONE: TWideStringField;
    QrTransportadorFAX: TWideStringField;
    QrTransportadorCelular: TWideStringField;
    QrTransportadorCNPJ: TWideStringField;
    QrTransportadorIE: TWideStringField;
    QrTransportadorContato: TWideStringField;
    QrTransportadorNOMEUF: TWideStringField;
    QrTransportadorNUMEROTXT: TWideStringField;
    QrTransportadorENDERECO: TWideStringField;
    QrTransportadorEEMail: TWideStringField;
    QrTransportadorPEmail: TWideStringField;
    QrTransportadorLOGRAD: TWideStringField;
    QrMPV2Vendedor: TIntegerField;
    QrMPV2ComissV_Per: TFloatField;
    QrMPV2ComissV_Val: TFloatField;
    QrMPV2NOMEVENDEDOR: TWideStringField;
    Label22: TLabel;
    EdVendedor: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    Label23: TLabel;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    QrSumC: TmySQLQuery;
    DsEmissC: TDataSource;
    QrEmissC: TmySQLQuery;
    QrEmissCData: TDateField;
    QrEmissCTipo: TSmallintField;
    QrEmissCCarteira: TIntegerField;
    QrEmissCAutorizacao: TIntegerField;
    QrEmissCGenero: TIntegerField;
    QrEmissCDescricao: TWideStringField;
    QrEmissCNotaFiscal: TIntegerField;
    QrEmissCDebito: TFloatField;
    QrEmissCCredito: TFloatField;
    QrEmissCCompensado: TDateField;
    QrEmissCDocumento: TFloatField;
    QrEmissCSit: TIntegerField;
    QrEmissCVencimento: TDateField;
    QrEmissCLk: TIntegerField;
    QrEmissCNOMESIT: TWideStringField;
    QrEmissCNOMETIPO: TWideStringField;
    QrEmissCNOMECARTEIRA: TWideStringField;
    QrEmissCFornecedor: TIntegerField;
    QrEmissCCliente: TIntegerField;
    QrEmissCSub: TSmallintField;
    QrEmissCFatID: TIntegerField;
    QrEmissCFatParcela: TIntegerField;
    QrEmissCID_Sub: TSmallintField;
    QrEmissCFatura: TWideStringField;
    QrEmissCBanco: TIntegerField;
    QrEmissCLocal: TIntegerField;
    QrEmissCCartao: TIntegerField;
    QrEmissCLinha: TIntegerField;
    QrEmissCOperCount: TIntegerField;
    QrEmissCLancto: TIntegerField;
    QrEmissCPago: TFloatField;
    QrEmissCMez: TIntegerField;
    QrEmissCMoraDia: TFloatField;
    QrEmissCMulta: TFloatField;
    QrEmissCProtesto: TDateField;
    QrEmissCDataCad: TDateField;
    QrEmissCDataAlt: TDateField;
    QrEmissCUserCad: TIntegerField;
    QrEmissCUserAlt: TIntegerField;
    QrEmissCDataDoc: TDateField;
    QrEmissCNivel: TIntegerField;
    QrEmissCVendedor: TIntegerField;
    QrEmissCAccount: TIntegerField;
    QrEmissCCliInt: TIntegerField;
    QrSumCCredito: TFloatField;
    Splitter1: TSplitter;
    QrEmissCCOMPENSA_TXT: TWideStringField;
    QrEmissCVENCTO_TXT: TWideStringField;
    QrEmissCVALOR_TXT: TWideStringField;
    PMEdita: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    PMImpressao: TPopupMenu;
    Impressodireta1: TMenuItem;
    ImpressoNormal1: TMenuItem;
    QrSumIts: TmySQLQuery;
    QrSumItsQtde: TFloatField;
    QrSumItsDesco: TFloatField;
    QrSumItsValor: TFloatField;
    QrSumItsPRECO: TFloatField;
    LaDescoExtra: TLabel;
    QrMPV2DescoExtra: TFloatField;
    QrMPV2Volumes: TIntegerField;
    PMExclui: TPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    QrMPV2Obs: TWideStringField;
    PMItem: TPopupMenu;
    Incluinovamercadoria1: TMenuItem;
    Alteradadosdamercadoriaatual1: TMenuItem;
    N1: TMenuItem;
    Excluimercadoriaatual1: TMenuItem;
    PMPagtos: TPopupMenu;
    Incluinovospagamentos1: TMenuItem;
    Alterapagamentoatual1: TMenuItem;
    N2: TMenuItem;
    Excluipagamentoatual1: TMenuItem;
    Excluitodospagamentos1: TMenuItem;
    QrEmissCQtde: TFloatField;
    QrEmissCFatID_Sub: TIntegerField;
    QrEmissCForneceI: TIntegerField;
    QrEmissCICMS_P: TFloatField;
    QrEmissCICMS_V: TFloatField;
    QrEmissCDuplicata: TWideStringField;
    QrEmissCDepto: TIntegerField;
    QrEmissCDescoPor: TIntegerField;
    QrEmissCEmitente: TWideStringField;
    QrEmissCContaCorrente: TWideStringField;
    QrEmissCCNPJCPF: TWideStringField;
    QrEmissCSEQ: TIntegerField;
    QrEmissCControle: TFloatField;
    QrEmissCDescoVal: TFloatField;
    QrEmissCDescoControle: TIntegerField;
    QrEmissCNFVal: TFloatField;
    QrEmissCUnidade: TIntegerField;
    QrEmissCAntigo: TWideStringField;
    QrDefPecas: TmySQLQuery;
    DsDefPecas: TDataSource;
    frxDsMPV2Its: TfrxDBDataset;
    frxOS: TfrxReport;
    PMImprimeDireto: TPopupMenu;
    Pedido1: TMenuItem;
    Ordemdeproduo1: TMenuItem;
    QrMPV2Obz: TWideStringField;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrMPsPreco: TFloatField;
    Label25: TLabel;
    QrMPV2AlterWeb: TSmallintField;
    QrMPV2Ativo: TSmallintField;
    QrMPV2NF: TIntegerField;
    QrMPsICMS: TFloatField;
    QrMPsIPI: TFloatField;
    QrMPsPrecoICM: TSmallintField;
    QrTotaisQtde: TFloatField;
    QrTotaisTotal: TFloatField;
    QrTotaisICMS_Val: TFloatField;
    QrMPV2ItsNOMEUNIDADE: TWideStringField;
    QrMPV2ItsNOMEMP: TWideStringField;
    QrMPV2ItsCodigo: TIntegerField;
    QrMPV2ItsPedido: TIntegerField;
    QrMPV2ItsControle: TIntegerField;
    QrMPV2ItsEstagio: TIntegerField;
    QrMPV2ItsQtde: TFloatField;
    QrMPV2ItsPreco: TFloatField;
    QrMPV2ItsUnidade: TSmallintField;
    QrMPV2ItsLk: TIntegerField;
    QrMPV2ItsDataCad: TDateField;
    QrMPV2ItsDataAlt: TDateField;
    QrMPV2ItsUserCad: TIntegerField;
    QrMPV2ItsUserAlt: TIntegerField;
    QrMPV2ItsAlterWeb: TSmallintField;
    QrMPV2ItsAtivo: TSmallintField;
    QrMPV2ItsMP: TIntegerField;
    QrMPV2ItsPeso: TFloatField;
    QrMPV2ItsPecas: TFloatField;
    QrMPV2ItsM2: TFloatField;
    QrMPV2ItsP2: TFloatField;
    QrMPV2ItsTotal: TFloatField;
    QrMPV2ItsICMS_Per: TFloatField;
    QrMPV2ItsICMS_Val: TFloatField;
    QrMPV2ItsIPI_Per: TFloatField;
    QrMPV2ItsGrandeza: TSmallintField;
    QrMPV2ItsIPI_Val: TFloatField;
    QrMPEstagios: TmySQLQuery;
    DsMPEstagios: TDataSource;
    QrMPEstagiosCodigo: TIntegerField;
    QrMPEstagiosNome: TWideStringField;
    Panel1: TPanel;
    Label7: TLabel;
    EdMP: TdmkEditCB;
    CBMP: TdmkDBLookupComboBox;
    Label40: TLabel;
    EdEstagio: TdmkEditCB;
    CBEstagio: TdmkDBLookupComboBox;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    RGGrandeza: TRadioGroup;
    GroupBox2: TGroupBox;
    Label34: TLabel;
    Label8: TLabel;
    Label35: TLabel;
    dmkEdPeso: TdmkEdit;
    dmkEdM2: TdmkEdit;
    dmkEdP2: TdmkEdit;
    GroupBox3: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label31: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label39: TLabel;
    dmkEdPreco: TdmkEdit;
    dmkEdTotal: TdmkEdit;
    dmkEdICMS_Per: TdmkEdit;
    dmkEdICMS_Val: TdmkEdit;
    dmkEdIPI_Per: TdmkEdit;
    dmkEdIPI_Val: TdmkEdit;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label32: TLabel;
    Label33: TLabel;
    SpeedButton5: TSpeedButton;
    CBUnidade: TdmkDBLookupComboBox;
    EdUnidade: TdmkEditCB;
    dmkEdPecas: TdmkEdit;
    Panel2: TPanel;
    StaticText1: TStaticText;
    DBGrid2: TDBGrid;
    QrMPV2Bxa: TmySQLQuery;
    DsMPV2Bxa: TDataSource;
    Panel6: TPanel;
    PMBaixa: TPopupMenu;
    Incluibaixadecouro1: TMenuItem;
    Alterabaixadecouro1: TMenuItem;
    Excluiaixadecouro1: TMenuItem;
    QrMPV2BxaCodigo: TIntegerField;
    QrMPV2BxaControle: TIntegerField;
    QrMPV2BxaConta: TIntegerField;
    QrMPV2BxaEstagio: TIntegerField;
    QrMPV2BxaMarca_ID: TIntegerField;
    QrMPV2BxaPeso: TFloatField;
    QrMPV2BxaPecas: TFloatField;
    QrMPV2BxaM2: TFloatField;
    QrMPV2BxaP2: TFloatField;
    QrMPV2BxaLk: TIntegerField;
    QrMPV2BxaDataCad: TDateField;
    QrMPV2BxaDataAlt: TDateField;
    QrMPV2BxaUserCad: TIntegerField;
    QrMPV2BxaUserAlt: TIntegerField;
    QrMPV2BxaAlterWeb: TSmallintField;
    QrMPV2BxaAtivo: TSmallintField;
    QrMPV2ItsNOMEESTAGIO: TWideStringField;
    QrMPV2ItsNOMEGRANDEZA: TWideStringField;
    QrMPV2BxaMarca: TWideStringField;
    Notafiscalmatricial1: TMenuItem;
    N3: TMenuItem;
    QrMPV2ItsCodImp: TWideStringField;
    QrMPV2ItsCF: TWideStringField;
    QrMPV2ItsST: TWideStringField;
    QrMPV2Seguro: TFloatField;
    QrMPV2Desp_Acess: TFloatField;
    PMFrete: TPopupMenu;
    Incluinovoitemdefrete1: TMenuItem;
    QrEmissT: TmySQLQuery;
    QrEmissTData: TDateField;
    QrEmissTTipo: TSmallintField;
    QrEmissTCarteira: TIntegerField;
    QrEmissTAutorizacao: TIntegerField;
    QrEmissTGenero: TIntegerField;
    QrEmissTDescricao: TWideStringField;
    QrEmissTNotaFiscal: TIntegerField;
    QrEmissTDebito: TFloatField;
    QrEmissTCredito: TFloatField;
    QrEmissTCompensado: TDateField;
    QrEmissTDocumento: TFloatField;
    QrEmissTSit: TIntegerField;
    QrEmissTVencimento: TDateField;
    QrEmissTLk: TIntegerField;
    QrEmissTNOMESIT: TWideStringField;
    QrEmissTNOMETIPO: TWideStringField;
    QrEmissTNOMECARTEIRA: TWideStringField;
    QrEmissTFornecedor: TIntegerField;
    QrEmissTCliente: TIntegerField;
    QrEmissTControle: TIntegerField;
    QrEmissTSub: TSmallintField;
    QrEmissTFatID: TIntegerField;
    QrEmissTFatParcela: TIntegerField;
    QrEmissTID_Pgto: TIntegerField;
    QrEmissTID_Sub: TSmallintField;
    QrEmissTFatura: TWideStringField;
    QrEmissTBanco: TIntegerField;
    QrEmissTLocal: TIntegerField;
    QrEmissTCartao: TIntegerField;
    QrEmissTLinha: TIntegerField;
    QrEmissTOperCount: TIntegerField;
    QrEmissTLancto: TIntegerField;
    QrEmissTPago: TFloatField;
    QrEmissTMez: TIntegerField;
    QrEmissTMoraDia: TFloatField;
    QrEmissTMulta: TFloatField;
    QrEmissTProtesto: TDateField;
    QrEmissTDataCad: TDateField;
    QrEmissTDataAlt: TDateField;
    QrEmissTUserCad: TIntegerField;
    QrEmissTUserAlt: TIntegerField;
    QrEmissTDataDoc: TDateField;
    QrEmissTCtrlIni: TIntegerField;
    QrEmissTNivel: TIntegerField;
    QrEmissTVendedor: TIntegerField;
    QrEmissTAccount: TIntegerField;
    QrEmissTCliInt: TIntegerField;
    QrEmissTQtde: TFloatField;
    QrEmissTFatID_Sub: TIntegerField;
    QrEmissTEmitente: TWideStringField;
    QrEmissTContaCorrente: TWideStringField;
    QrEmissTCNPJCPF: TWideStringField;
    QrEmissTForneceI: TIntegerField;
    QrEmissTICMS_P: TFloatField;
    QrEmissTICMS_V: TFloatField;
    QrEmissTDuplicata: TWideStringField;
    QrEmissTDepto: TIntegerField;
    QrEmissTDescoPor: TIntegerField;
    QrEmissTDescoVal: TFloatField;
    QrEmissTDescoControle: TIntegerField;
    QrEmissTNFVal: TFloatField;
    DsEmissT: TDataSource;
    QrSumT: TmySQLQuery;
    QrSumTDebito: TFloatField;
    Panel9: TPanel;
    Panel10: TPanel;
    DBGrid1: TDBGrid;
    StaticText3: TStaticText;
    Panel11: TPanel;
    DBGrid5: TDBGrid;
    StaticText4: TStaticText;
    QrMPV2Conhecim: TIntegerField;
    Alteraitemdefrete1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrMPV2CobraFrete: TSmallintField;
    QrMPV2FretePg: TFloatField;
    QrMPV2FreteNF: TFloatField;
    GroupBox4: TGroupBox;
    Label41: TLabel;
    CkCobraFrete: TCheckBox;
    Label29: TLabel;
    EdTransp: TdmkEditCB;
    CBTransp: TdmkDBLookupComboBox;
    EdPlaca: TEdit;
    EdUFPlaca: TEdit;
    Label43: TLabel;
    Label44: TLabel;
    dmkEdkgBruto: TdmkEdit;
    dmkEdkgLiqui: TdmkEdit;
    Label45: TLabel;
    Label46: TLabel;
    dmkEdConhecim: TdmkEdit;
    dmkEdVolumes: TdmkEdit;
    EdEspecie: TEdit;
    Label47: TLabel;
    EdMarca: TEdit;
    Label48: TLabel;
    EdNumero: TEdit;
    Label49: TLabel;
    Label30: TLabel;
    Memo1: TMemo;
    Label38: TLabel;
    Memo2: TMemo;
    dmkEdComissV_Per: TdmkEdit;
    dmkEdDescoExtra: TdmkEdit;
    dmkEdNF: TdmkEdit;
    QrMPV2Placa: TWideStringField;
    QrMPV2UFPlaca: TWideStringField;
    QrMPV2Especie: TWideStringField;
    QrMPV2Marca: TWideStringField;
    QrMPV2Numero: TWideStringField;
    QrMPV2kgBruto: TFloatField;
    QrMPV2kgLiqui: TFloatField;
    TabSheet3: TTabSheet;
    DBMemo2: TDBMemo;
    DBMemo1: TDBMemo;
    Panel7: TPanel;
    DBGrid3: TDBGrid;
    Label3: TLabel;
    dmkEdFreteNF: TdmkEdit;
    QrMPV2SUBTOTAL: TFloatField;
    QrMPV2RECEITAREAL: TFloatField;
    Label61: TLabel;
    dmkEdSeguro: TdmkEdit;
    Label62: TLabel;
    dmkEdDesp_Acess: TdmkEdit;
    CkCobraSegur: TCheckBox;
    QrMPV2CobraSegur: TSmallintField;
    Panel8: TPanel;
    Label1: TLabel;
    DBEdit4: TDBEdit;
    Label2: TLabel;
    DBEdit5: TDBEdit;
    Label4: TLabel;
    DBEdit10: TDBEdit;
    Label54: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    Label55: TLabel;
    DBEdit3: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit7: TDBEdit;
    Label6: TLabel;
    Label17: TLabel;
    DBEdit18: TDBEdit;
    DBEdit17: TDBEdit;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit16: TDBEdit;
    Label63: TLabel;
    DBEdit13: TDBEdit;
    DBEdit11: TDBEdit;
    Label64: TLabel;
    DBEdit12: TDBEdit;
    Label60: TLabel;
    DBEdit29: TDBEdit;
    DBEdit20: TDBEdit;
    Label59: TLabel;
    Label56: TLabel;
    DBEdit14: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    Label57: TLabel;
    GroupBox5: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label42: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    DBEdit2: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Splitter2: TSplitter;
    QrTotaisIPI_Val: TFloatField;
    QrMPV2Pago: TFloatField;
    QrMPV2ICMS_Val: TFloatField;
    QrMPV2IPI_Val: TFloatField;
    QrMPV2TOTALNOTA: TFloatField;
    Label58: TLabel;
    DBEdit30: TDBEdit;
    QrEmissCFatNum: TFloatField;
    QrEmissTFatNum: TFloatField;
    QrEmissCAgencia: TIntegerField;
    QrEmissTAgencia: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel15: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel16: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GroupBox6: TGroupBox;
    Panel13: TPanel;
    Panel17: TPanel;
    BtConfirma2: TBitBtn;
    BtDesiste2: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel4: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtImprime: TBitBtn;
    BtExclui: TBitBtn;
    GBTrava: TGroupBox;
    Panel12: TPanel;
    Panel14: TPanel;
    BtTrava: TBitBtn;
    BtItem: TBitBtn;
    BtBaixa: TBitBtn;
    BtPagamento: TBitBtn;
    BtFrete: TBitBtn;
    QrMPV2EntInt: TIntegerField;
    QrMPV2CodCliInt: TIntegerField;
    Panel18: TPanel;
    Label13: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label14: TLabel;
    DBEdit31: TDBEdit;
    QrTransportadorNUMERO: TFloatField;
    QrTransportadorCEP: TFloatField;
    QrTransportadorUF: TFloatField;
    QrFornecedorNUMERO: TFloatField;
    QrFornecedorCEP: TFloatField;
    QrFornecedorUF: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure QrMPV2AfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMPV2BeforeOpen(DataSet: TDataSet);
    procedure BtDesiste2Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure BtTravaClick(Sender: TObject);
    procedure BtItemClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrMPV2ItsCalcFields(DataSet: TDataSet);
    procedure BtPagamentoClick(Sender: TObject);
    procedure QrEmissCCalcFields(DataSet: TDataSet);
    procedure frMovimentoUserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure QrFornecedorCalcFields(DataSet: TDataSet);
    procedure QrTransportadorCalcFields(DataSet: TDataSet);
    procedure ImpressoNormal1Click(Sender: TObject);
    procedure Impressodireta1Click(Sender: TObject);
    procedure EdDescoExtraExit(Sender: TObject);
    procedure QrMPV2CalcFields(DataSet: TDataSet);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure Incluinovamercadoria1Click(Sender: TObject);
    procedure Alteradadosdamercadoriaatual1Click(Sender: TObject);
    procedure Excluimercadoriaatual1Click(Sender: TObject);
    procedure Incluinovospagamentos1Click(Sender: TObject);
    procedure Alterapagamentoatual1Click(Sender: TObject);
    procedure Excluipagamentoatual1Click(Sender: TObject);
    procedure Excluitodospagamentos1Click(Sender: TObject);
    procedure PMItemPopup(Sender: TObject);
    procedure PMPagtosPopup(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure Pedido1Click(Sender: TObject);
    procedure Ordemdeproduo1Click(Sender: TObject);
    procedure EdMPChange(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure RGGrandezaClick(Sender: TObject);
    procedure dmkEdPecasChange(Sender: TObject);
    procedure dmkEdPesoChange(Sender: TObject);
    procedure dmkEdM2Change(Sender: TObject);
    procedure dmkEdP2Change(Sender: TObject);
    procedure dmkEdPrecoChange(Sender: TObject);
    procedure dmkEdICMS_PerChange(Sender: TObject);
    procedure dmkEdIPI_PerChange(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure QrMPV2ItsBeforeClose(DataSet: TDataSet);
    procedure QrMPV2ItsAfterScroll(DataSet: TDataSet);
    procedure Incluibaixadecouro1Click(Sender: TObject);
    procedure BtBaixaClick(Sender: TObject);
    procedure Excluiaixadecouro1Click(Sender: TObject);
    procedure PMBaixaPopup(Sender: TObject);
    procedure Notafiscalmatricial1Click(Sender: TObject);
    procedure Incluinovoitemdefrete1Click(Sender: TObject);
    procedure QrMPV2BeforeClose(DataSet: TDataSet);
    procedure Alteraitemdefrete1Click(Sender: TObject);
    procedure BtFreteClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    FFat1, FFat2: Double;
    FQtde, (*FQtdeD, FDesco, FCompl,*) FTotal: Double;
    FEditItem, FMostraMoney: Boolean;
    FTabLctA: String;
    //
    procedure CriaOForm;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure CalculaTotal;
    procedure ImpressaoOculta;
    procedure ImpressaoNormal(ProdutosM, ProdutosMIts, Pagtos: TmySQLQuery);
    procedure ImpressaoDireta2(ProdutosM, ProdutosMIts, Pagtos: TmySQLQuery;
              Cria, Mostra: Boolean);
    procedure DefineVarDup;
    function DiferencasEmPagtos: Double;
    procedure ReopenSumC();
    procedure ReopenSumT();
    procedure ReopenFornecedor(Qry: TmySQLQuery);
    procedure ReopenTransportador(Qry: TmySQLQuery);
    procedure ReopenSumIts(Qry: TmySQLQuery);
  public
    { Public declarations }
    FMPV2Loc: Integer;
    FDireto: Boolean;
    procedure ReopenMPV2Its(Controle: Integer);
    procedure ReopenMPV2Bxa(Conta: Integer);
    procedure AtualizaTotais;
    procedure ExcluiMercadoria;
    procedure ExcluiPagto;
    procedure ExcluiTodosPagtos;
    procedure ReopenEmissC(Controle: Integer);
    procedure ReopenEmissT(Controle: Integer);
  end;

var
  FmMPV2: TFmMPV2;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DotPrint, UnInternalConsts3, UnPagtos, Principal,
MPV2Bxa, UCreate, (*NF1,*) UnFinanceiro, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMPV2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMPV2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMPV2Codigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMPV2.DefParams;
begin
  VAR_GOTOTABELA := 'mpv2';
  VAR_GOTOMYSQLTABLE := QrMPV2;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;
  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT');
  VAR_SQLx.Add('CASE WHEN ve.Tipo=0 THEN ve.RazaoSocial');
  VAR_SQLx.Add('ELSE ve.Nome END NOMEVENDEDOR,');
  VAR_SQLx.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  VAR_SQLx.Add('ELSE cl.Nome END NOMECLIENTE,');
  VAR_SQLx.Add('CASE WHEN tr.Tipo=0 THEN tr.RazaoSocial');
  VAR_SQLx.Add('ELSE tr.Nome END NOMETRANSP,');
  VAR_SQLx.Add('mpv2.*, ei.CodCliInt');
  VAR_SQLx.Add('FROM mpv2 mpv2');
  VAR_SQLx.Add('LEFT JOIN enticliint ei ON ei.CodEnti=mpv2.EntInt');
  VAR_SQLx.Add('LEFT JOIN entidades  cl ON cl.Codigo=mpv2.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades  tr ON tr.Codigo=mpv2.Transp');
  VAR_SQLx.Add('LEFT JOIN entidades  ve ON ve.Codigo=mpv2.Vendedor');
  VAR_SQLx.Add('WHERE mpv2.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND mpv2.Codigo=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmMPV2.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      AtualizaTotais;
      GBCntrl.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      //
      if Codigo > 0 then
        LocCod(Codigo, Codigo);
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      GBCntrl.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text        := FormatFloat(FFormatFloat, Codigo);
        //TPDataF.Date        := Date;
        dmkEdComissV_Per.ValueVariant   := 0;
        LaDescoExtra.Enabled := False;
        dmkEdDescoExtra.Enabled := False;
        dmkEdDescoExtra.ValueVariant    := 0;
        dmkEdNF.ValueVariant            := 0;
        EdCliente.Text       := '';
        CBCliente.KeyValue   := NULL;
        EdVendedor.Text      := '';
        CBVendedor.KeyValue  := NULL;
        EdTransp.Text        := '';
        CBTransp.KeyValue    := NULL;
        dmkEdConhecim.ValueVariant      := '0';
        dmkEdVolumes.ValueVariant       := 0;

        EdPlaca.Text                    := '';
        EdUFPlaca.Text                  := '';
        dmkEdkgLiqui.ValueVariant       := 0;
        dmkEdkgBruto.ValueVariant       := 0;
        EdEspecie.Text                  := '';
        EdMarca.Text                    := '';
        EdNumero.Text                   := '';
        dmkEdDesp_Acess.ValueVariant    := 0;
        dmkEdSeguro.ValueVariant        := 0;
        dmkEdFreteNF.ValueVariant       := 0;

        CkCobraFrete.Checked            := False;
        CkCobraSegur.Checked            := False;

        Memo1.Lines.Clear;
        Memo2.Lines.Clear;
      end else begin
        EdCodigo.Text                   := IntToStr(QrMPV2Codigo.Value);
        EdCliente.Text                  := IntToStr(QrMPV2Cliente.Value);
        CBCliente.KeyValue              := QrMPV2Cliente.Value;
        EdTransp.Text                   := IntToStr(QrMPV2Transp.Value);
        CBTransp.KeyValue               := QrMPV2Transp.Value;
        TPDataF.Date                    := QrMPV2DataF.Value;
        //EdNome.Text                   := DBEdNome.Text;
        EdVendedor.Text                 := IntToStr(QrMPV2Vendedor.Value);
        CBVendedor.KeyValue             := QrMPV2Vendedor.Value;
        dmkEdComissV_Per.ValueVariant   := QrMPV2ComissV_Per.Value;
        LaDescoExtra.Enabled            := True;
        dmkEdDescoExtra.Enabled         := True;
        dmkEdDescoExtra.ValueVariant    := QrMPV2DescoExtra.Value;
        dmkEdVolumes.ValueVariant       := QrMPV2Volumes.Value;
        dmkEdNF.ValueVariant            := QrMPV2NF.Value;
        dmkEdConhecim.ValueVariant      := QrMPV2Conhecim.Value;

        EdPlaca.Text                    := QrMPV2Placa.Value;
        EdUFPlaca.Text                  := QrMPV2UFPlaca.Value;
        dmkEdkgLiqui.ValueVariant       := QrMPV2kgLiqui.Value;
        dmkEdkgBruto.ValueVariant       := QrMPV2kgBruto.Value;
        EdEspecie.Text                  := QrMPV2Especie.Value;
        EdMarca.Text                    := QrMPV2Marca.Value;
        EdNumero.Text                   := QrMPV2Numero.Value;
        dmkEdDesp_Acess.ValueVariant    := QrMPV2Desp_Acess.Value;
        dmkEdSeguro.ValueVariant        := QrMPV2Seguro.Value;
        dmkEdFreteNF.ValueVariant       := QrMPV2FreteNF.Value;

        CkCobraFrete.Checked            := Geral.IntToBool(QrMPV2CobraFrete.Value);
        CkCobraSegur.Checked            := Geral.IntToBool(QrMPV2CobraSegur.Value);
        //
        Memo1.Text                      := QrMPV2Obs.Value;
        Memo2.Text                      := QrMPV2Obz.Value;
      end;
      TPDataF.SetFocus;
    end;
    2:
    begin
      PainelIts.Visible   := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        FEditItem                  := False;
        EdMP.Text                  := '';
        CBMP.KeyValue              := NULL;
        RGGrandeza.ItemIndex       := 0;
        EdUnidade.Text             := '';
        CBUnidade.KeyValue         := NULL;
        dmkEdPecas.ValueVariant    := 0;
        dmkEdPeso.ValueVariant     := 0;
        dmkEdM2.ValueVariant       := 0;
        dmkEdP2.ValueVariant       := 0;
        dmkEdPreco.ValueVariant    := 0;
        dmkEdTotal.ValueVariant    := 0;
        dmkEdICMS_Per.ValueVariant := 0;
        dmkEdICMS_Val.ValueVariant := 0;
        dmkEdIPI_Per.ValueVariant  := 0;
        dmkEdIPI_Val.ValueVariant  := 0;

      end else begin
        FEditItem                  := True;
        EdMP.Text                  := IntToStr(QrMPV2ItsMP.Value);
        CBMP.KeyValue              := QrMPV2ItsMP.Value;
        RGGrandeza.ItemIndex       := QrMPV2ItsGrandeza.Value;
        EdUnidade.Text             := IntToStr(QrMPV2ItsUnidade.Value);
        CBUnidade.KeyValue         := QrMPV2ItsUnidade.Value;
        dmkEdPecas.ValueVariant    := QrMPV2ItsPecas.Value;
        dmkEdPeso.ValueVariant     := QrMPV2ItsPeso.Value;
        dmkEdM2.ValueVariant       := QrMPV2ItsM2.Value;
        dmkEdP2.ValueVariant       := QrMPV2ItsP2.Value;
        dmkEdPreco.ValueVariant    := QrMPV2ItsPreco.Value;
        dmkEdTotal.ValueVariant    := QrMPV2ItsTotal.Value;
        dmkEdICMS_Per.ValueVariant := QrMPV2ItsICMS_Per.Value;
        dmkEdICMS_Val.ValueVariant := QrMPV2ItsICMS_Val.Value;
        dmkEdIPI_Per.ValueVariant  := QrMPV2ItsIPI_Per.Value;
        dmkEdIPI_Val.ValueVariant  := QrMPV2ItsIPI_Val.Value;
        //CalculaTotal;
      end;
      EdMP.SetFocus;
      //
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmMPV2.CriaOForm;
begin
  DefParams;
  Va(vpLast);
end;

procedure TFmMPV2.AlteraRegistro;
var
  MPV2 : Integer;
begin
  MPV2 := QrMPV2Codigo.Value;
  if QrMPV2Codigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(MPV2, Dmod.MyDB, 'mpv2', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(MPV2, Dmod.MyDB, 'mpv2', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmMPV2.IncluiRegistro;
var
  Cursor : TCursor;
  MPV2 : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MPV2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'mpv2', 'mpv2', 'Codigo');
    if Length(FormatFloat(FFormatFloat, MPV2))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, MPV2);
  finally
    Screen.Cursor := Cursor;
  end;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMPV2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMPV2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMPV2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMPV2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMPV2.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
  AtualizaTotais;
end;

procedure TFmMPV2.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
  AtualizaTotais;
end;

procedure TFmMPV2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrMPV2Codigo.Value;
  Close;
end;

procedure TFmMPV2.BtConfirmaClick(Sender: TObject);
var
  Codigo, Cliente, Vended, Transp, CobraFrete, CobraSegur, Empresa, EntInt: Integer;
  Comiss, DescoExtra: Double;
begin
  Empresa    := EdEmpresa.ValueVariant;
  EntInt     := DModG.QrEmpresasCodigo.Value;
  Cliente    := EdCliente.ValueVariant;
  Transp     := Geral.IMV(EdTransp.Text);
  Vended     := Geral.IMV(EdVendedor.Text);
  Comiss     := dmkEdComissV_Per.ValueVariant;
  DescoExtra := dmkEdDescoExtra.ValueVariant;
  CobraFrete := Geral.BoolToInt(CkCobraFrete.Checked);
  CobraSegur := Geral.BoolToInt(CkCobraSegur.Checked);
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Defina o cliente!') then
    Exit;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a empresa!') then
    Exit;
  if MyObjects.FIC((Comiss > 0.009) and (Vended = 0), EdVendedor,
  'Defina um Vendedor para a comiss�o a pagar!') then
    Exit;
  Codigo := EdCodigo.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'mpv2', False,
  [
    'DataF', 'ComissV_Per', 'DescoExtra',
    'NF', 'Cliente', 'Vendedor', 'Transp',
    'Conhecim', 'Volumes', 'Placa',
    'UFPlaca', 'kgLiqui', 'kgBruto',
    'Especie', 'Marca', 'Numero',
    'CobraFrete', 'Obs', 'Obz',
    'Desp_Acess', 'Seguro',
    'CobraSegur', 'FreteNF', 'EntInt'
  ], ['Codigo'], [
    FormatDateTime(VAR_FORMATDATE, TPDataF.Date),  Comiss, DescoExtra,
    dmkEdNF.ValueVariant, Cliente, Vended, Transp,
    dmkEdConhecim.ValueVariant, dmkEdVolumes.ValueVariant, EdPlaca.Text,
    EdUFPlaca.Text, dmkEdkgLiqui.ValueVariant, dmkEdkgBruto.ValueVariant,
    EdEspecie.Text, EdMarca.Text, EdNumero.Text,
    CobraFrete, Memo1.Text, Memo2.Text,
    dmkEdDesp_Acess.ValueVariant, dmkEdSeguro.ValueVariant,
    CobraSegur, dmkEdFreteNF.ValueVariant, EntInt
  ], [Codigo], True) then
  begin
    {
    Dmod.QrUpdU.SQL.Clear;
    if ImgTipo.SQLType = stIns then
      Dmod.QrUpdU.SQL.Add('INSERT INTO mpv2 SET ')
    else Dmod.QrUpdU.SQL.Add('UPDATE mpv2 SET ');
    Dmod.QrUpdU.SQL.Add('Cliente=:P0, Transp=:P1, DataF=:P2, ');
    Dmod.QrUpdU.SQL.Add('Vendedor=:P3, ComissV_Per=:P4, DescoExtra=:P5, ');
    Dmod.QrUpdU.SQL.Add('Volumes=:P6, Obs=:P7, Obz=:P8, ');
    //
    if ImgTipo.SQLType = stIns then
      Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
    else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
    Dmod.QrUpdU.Params[00].AsInteger := Cliente;
    Dmod.QrUpdU.Params[01].AsInteger := Transp;
    Dmod.QrUpdU.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataF.Date);
    Dmod.QrUpdU.Params[03].AsInteger := Vended;
    Dmod.QrUpdU.Params[04].AsFloat   := Comiss;
    Dmod.QrUpdU.Params[05].AsFloat   := DescoExtra;
    Dmod.QrUpdU.Params[06].AsInteger := Geral.IMV(EdVolumes.Text);
    Dmod.QrUpdU.Params[07].AsString  := Memo1.Text;
    Dmod.QrUpdU.Params[08].AsString  := Memo2.Text;
    //
    Dmod.QrUpdU.Params[09].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[10].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.Params[11].AsInteger := Codigo;
    Dmod.QrUpdU.ExecSQL;
    }
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'mpv2', 'Codigo');
    MostraEdicao(0, ImgTipo.SQLType, Codigo);
    //N�o precisa;
    //
    GBTrava.Visible := True;
    GBCntrl.Visible := False;
    if FDireto then
    begin
      if ImgTipo.SQLType = stIns then Incluinovamercadoria1Click(Self) else
      begin
        BtTravaClick(Self);
        if Application.MessageBox('Deseja imprimir o pedido?', 'Pergunta',
        MB_YESNOCANCEL+MB_ICONQUESTION)= ID_YES then ImpressaoOculta;
        BtSaidaClick(Self);
      end;
    end;
  end;
end;

procedure TFmMPV2.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'mpv2', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'mpv2', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'mpv2', 'Codigo');
  if FDireto then
  begin
    //Application.MessageBox(PChar('A inclus�o direta foi desativada para '+
    //'permitir altera��es!'), 'Aviso', MB_OK+MB_ICONWARNING);
    FDireto := False;
  end;
end;

procedure TFmMPV2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrMPV2CodCliInt.Value);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa, DModG.QrEmpresas);
  //
  DBGrid4.Height := Geral.ReadAppKey('Componentes\MPV2\AlturaGrade',
  Application.Title, ktInteger, 73, HKEY_LOCAL_MACHINE);
  if DBGrid4.Height < 40 then DBGrid4.Height := 40;
  while DBGrid4.Height < 40 do DBGrid4.Height := DBGrid4.Height - 20;

  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  Painel1.Align     := alClient;
  Panel9.Align      := alClient;
  DBGrid1.Align     := alClient;
  CriaOForm;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransp, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMPs, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.MyDB);
  TPDataF.Date := Date;
  UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMPEstagios, Dmod.MyDB);
end;

procedure TFmMPV2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMPV2Codigo.Value,LaRegistro.Caption);
end;

procedure TFmMPV2.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMPV2.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmMPV2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Geral.WriteAppKey('Componentes\MPV2\AlturaGrade', Application.Title,
  DBGrid4.Height, ktInteger, HKEY_LOCAL_MACHINE);
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMPV2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'MPV2', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FDireto then IncluiRegistro;
end;

procedure TFmMPV2.QrMPV2AfterScroll(DataSet: TDataSet);
begin
  FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrMPV2CodCliInt.Value);
  BtAltera.Enabled := GOTOy.BtEnabled(QrMPV2Codigo.Value, False);
  ReopenMPV2Its(0);
  ReopenEmissC(0);
  ReopenEmissT(0);
end;

procedure TFmMPV2.SbQueryClick(Sender: TObject);
begin
  {
  FMPV2Loc := 0;
  Application.CreateForm(TFmMPV2Loc, FmMPV2Loc);
  FmMPV2Loc.ShowModal;
  FmMPV2Loc.Destroy;
  if FMPV2Loc > 0 then LocCod(FMPV2Loc, FMPV2Loc);
  }
end;

procedure TFmMPV2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPV2.QrMPV2BeforeOpen(DataSet: TDataSet);
begin
  QrMPV2Codigo.DisplayFormat := FFormatFloat;
end;

procedure TFmMPV2.BtDesiste2Click(Sender: TObject);
begin
  PainelDados.Visible := True;
  PainelIts.Visible := False;
  AtualizaTotais;
  LocCod(QrMPV2Codigo.Value, QrMPV2Codigo.Value);
  if FDireto then
  begin
    Incluinovospagamentos1Click(Self);
    FmostraMoney := False;
  end;
end;

procedure TFmMPV2.BtConfirma2Click(Sender: TObject);
var
  MP, Controle, Unidade, Pedido, Estagio: Integer;
  Preco: Double;
begin
  // N�o tem pedido por enquanto
  Pedido := 0;
  MP := Geral.IMV(EdMP.Text);
  if MP = 0 then
  begin
    Application.MessageBox('Defina uma mercadoria.', 'Erro', MB_OK+MB_ICONERROR);
    EdMP.SetFocus;
    Exit;
  end;
  Estagio := Geral.IMV(EdEstagio.Text);
  if Estagio = 0 then
  begin
    Application.MessageBox('Defina o est�gio da mercadoria.', 'Erro', MB_OK+MB_ICONERROR);
    EdEstagio.SetFocus;
    Exit;
  end;
  //FQtde := dmkEdQtde.Text);
  Preco := dmkEdPreco.ValueVariant;
  //Total := dmkEdTotal.ValueVariant;
  //Desco := Geral.DMV(EdDesco.Text);
  if CBUnidade.KeyValue <> NULL then
  Unidade := CBUnidade.KeyValue else Unidade := 0;
  (*if Valor = 0 then
  begin
    Application.MessageBox('Valor n�o definido.', 'Erro', MB_OK+MB_ICONERROR);
    if Qtde = 0 then EdQtde.SetFocus else EdPreco.SetFocus;
    Exit;
  end;*)
  //
  CalculaTotal;
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'mpv2its', 'mpv2its', 'Codigo');
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'mpv2its', False,
  [
    'Codigo', 'Pedido', 'Estagio',
    'Qtde', 'Preco', 'Unidade',
    'MP', 'Peso',
    'Pecas', 'M2', 'P2',
    'Total', 'ICMS_Per',
    'ICMS_Val', 'IPI_Per',
    'Grandeza', 'IPI_Val'
  ], ['Controle'], [
    QrMPV2Codigo.Value, Pedido, Estagio,
    FQtde, Preco, Unidade,
    MP, dmkEdPeso.ValueVariant,
    dmkEdPecas.ValueVariant, dmkEdM2.ValueVariant, dmkEdP2.ValueVariant,
    dmkedTotal.ValueVariant, dmkedICMS_Per.ValueVariant,
    dmkEdICMS_Val.ValueVariant, dmkEdIPI_Per.ValueVariant,
    RGGrandeza.ItemIndex, dmkEdIPI_Val.ValueVariant
  ], [Controle], True) then
  begin
  {
  Dmod.QrUpd.SQL.Clear;
  if not FEditItem then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO mpv2its SET ');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'mpv2its', 'mpv2its', 'Codigo');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE mpv2its SET ');
    Controle := QrMPV2ItsControle.Value;
  end;
  Dmod.QrUpd.SQL.Add('MP=:P0, Qtde=:P1, Preco=:P2, Valor=:P3, Texto=:P4,');
  Dmod.QrUpd.SQL.Add('Desco=:P5, Entrega=:P6, Classe=:P7, M2Pedido=:P8, ');
  Dmod.QrUpd.SQL.Add('Pecas=:P9, Unidade=:P10, Observ=:P11, EspesTxt=:P12, ');
  Dmod.QrUpd.SQL.Add('CorTxt=:P13 ');
  if not FEditItem then
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb')
  else
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa AND Controle=:Pb');
  //
  Dmod.QrUpd.Params[00].AsInteger := MP;
  Dmod.QrUpd.Params[01].AsFloat   := Qtde;
  Dmod.QrUpd.Params[02].AsFloat   := Preco;
  Dmod.QrUpd.Params[03].AsFloat   := Valor;
  Dmod.QrUpd.Params[04].AsString  := EdTexto.Text;
  Dmod.QrUpd.Params[05].AsFloat   := Desco;
  Dmod.QrUpd.Params[06].AsString  := Geral.FDT(TPEntrega.Date, 1);
  Dmod.QrUpd.Params[07].AsString  := EdClasse.Text;
  Dmod.QrUpd.Params[08].AsFloat   := Geral.DMV(EdM2Pedido.Text);
  Dmod.QrUpd.Params[09].AsFloat   := Geral.DMV(EdPecas.Text);
  Dmod.QrUpd.Params[10].AsInteger := Unidade;
  Dmod.QrUpd.Params[11].AsString  := MeObserv.Text;
  Dmod.QrUpd.Params[12].AsString  := EdEspesTxt.Text;
  Dmod.QrUpd.Params[13].AsString  := EdCorTxt.Text;
  //
  Dmod.QrUpd.Params[14].AsInteger := QrMPV2Codigo.Value;
  Dmod.QrUpd.Params[15].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  }
    if CkContinuar.Checked then
    begin
      EdMP.Text                  := '';
      CBMP.KeyValue              := NULL;
      RGGrandeza.ItemIndex       := 0;
      EdUnidade.Text             := '';
      CBUnidade.KeyValue         := NULL;
      dmkEdPecas.ValueVariant    := 0;
      dmkEdPeso.ValueVariant     := 0;
      dmkEdM2.ValueVariant       := 0;
      dmkEdP2.ValueVariant       := 0;
      dmkEdPreco.ValueVariant    := 0;
      dmkEdTotal.ValueVariant    := 0;
      dmkEdICMS_Per.ValueVariant := 0;
      dmkEdICMS_Val.ValueVariant := 0;
      dmkEdIPI_Per.ValueVariant  := 0;
      dmkEdIPI_Val.ValueVariant  := 0;
      //
      EdMP.SetFocus;
    end else
    begin
      BtDesiste2click(Self);
      ReopenMPV2Its(Controle);
    end;
  end;
end;

procedure TFmMPV2.BtTravaClick(Sender: TObject);
begin
  GBCntrl.Visible := True;
  GBTrava.Visible := False;
  ImgTipo.SQLType := stLok;
  // Trava
  AtualizaTotais;
  LocCod(QrMPV2Codigo.Value, QrMPV2Codigo.Value);
  //if FDireto then BtAlteraClick(Self);
end;

procedure TFmMPV2.BtItemClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItem, BtItem);
end;

procedure TFmMPV2.ReopenMPV2Its(Controle: Integer);
begin
  QrMPV2Its.Close;
  QrMPV2Its.Params[0].AsInteger := QrMPV2Codigo.Value;
  UnDmkDAC_PF.AbreQuery(QrMPV2Its, Dmod.MyDB);
  //
  if Controle > 0 then
    QrMPV2Its.Locate('Controle', Controle, []);
end;

procedure TFmMPV2.ReopenSumC();
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumC, Dmod.MyDB, [
  'SELECT SUM(Credito) Credito ',
  'FROM ' + FTabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FATID_1013),
  'AND FatNum=' + Geral.FF0(QrMPV2Codigo.Value),
  '']);
  (*
  QrSumC.Close;
  QrSumC.Params[0].AsInteger := QrMPV2Codigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSumC, Dmod.MyDB);
  *)
end;

procedure TFmMPV2.ReopenSumIts(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumIts, Dmod.MyDB, [
  'SELECT SUM(Qtde) Qtde, SUM(Desco) Desco, ',
  'SUM(Valor) Valor, SUM(Valor) / SUM(Qtde) PRECO ',
  'FROM mpv2its  ',
  'WHERE Codigo=' + Geral.FF0(Qry.FieldByName('Codigo').AsInteger),
  '']);
end;

procedure TFmMPV2.ReopenSumT();
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumT, Dmod.MyDB, [
  'SELECT SUM(Debito) Debito ',
  'FROM ' + FTabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FATID_1014),
  'AND FatNum=' + Geral.FF0(QrMPV2Codigo.Value),
  '']);
  (*
  QrSumT.Close;
  QrSumT.Params[0].AsInteger := QrMPV2Codigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSumT, Dmod.MyDB);
  *)
end;

procedure TFmMPV2.ReopenTransportador(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTransportador, Dmod.MyDB, [
  'SELECT  ',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOME, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ERua ',
  'ELSE fo.PRua END RUA, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ENumero ',
  'ELSE fo.PNumero END + 0.000 NUMERO, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECompl ',
  'ELSE fo.PCompl END COMPL, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EBairro ',
  'ELSE fo.PBairro END BAIRRO, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECidade ',
  'ELSE fo.PCidade END CIDADE, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EPais ',
  'ELSE fo.PPais END PAIS, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ETe1 ',
  'ELSE fo.PTe1 END TELEFONE, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EFax ',
  'ELSE fo.PPais END FAX, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECel ',
  'ELSE fo.PCel END Celular, ',
  'CASE WHEN fo.Tipo=0 THEN fo.CNPJ ',
  'ELSE fo.CPF END CNPJ, ',
  'CASE WHEN fo.Tipo=0 THEN fo.IE ',
  'ELSE fo.RG END IE, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECEP ',
  'ELSE fo.PCEP END + 0.000 CEP, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EContato ',
  'ELSE fo.PContato END Contato, ',
  ' ',
  'CASE WHEN fo.Tipo=0 THEN fo.EUF ',
  'ELSE fo.PUF END + 0.000 UF, ',
  'CASE WHEN fo.Tipo=0 THEN uf1.Nome ',
  'ELSE uf2.Nome END NOMEUF, ',
  'EEMail, PEmail, ',
  ' ',
  'CASE WHEN fo.Tipo=0 THEN ll1.Nome ',
  'ELSE ll2.Nome END LOGRAD ',
  ' ',
  'FROM entidades fo ',
  'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf ',
  'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf ',
  'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd ',
  'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd ',
  'WHERE fo.Codigo=' + Geral.FF0(Qry.FieldByName('Cliente').AsInteger),
  '']);
end;

procedure TFmMPV2.AtualizaTotais;
var
  ComissVal: Double;
begin
  QrTotais.Close;
  QrTotais.Params[0].AsInteger := QrMPV2Codigo.Value;
  UnDmkDAC_PF.AbreQuery(QrTotais, Dmod.MyDB);
  ComissVal := (QrTotaisTotal.Value - QrTotaisICMS_Val.Value) *
                QrMPV2ComissV_Per.Value / 100;
  //
  ReopenSumC();
  //
  ReopenSumT();
  //
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE mpv2 SET Qtde=:P0, Valor=:P1, ComissV_Val=:P2, ');
  Dmod.QrUpd.SQL.Add('FretePg=:P3, Pago=:P4, ICMS_Val=:P5, IPI_Val=:P6 ');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  Dmod.QrUpd.Params[00].AsFloat   := QrTotaisQtde.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrTotaisTotal.Value;
  Dmod.QrUpd.Params[02].AsFloat   := ComissVal;
  Dmod.QrUpd.Params[03].AsFloat   := QrSumTDebito.Value;
  Dmod.QrUpd.Params[04].AsFloat   := QrSumCCredito.Value;
  Dmod.QrUpd.Params[05].AsFloat   := QrTotaisICMS_Val.Value;
  Dmod.QrUpd.Params[06].AsFloat   := QrTotaisIPI_Val.Value;
  //
  Dmod.QrUpd.Params[07].AsInteger := QrMPV2Codigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmMPV2.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImpressao, SbImprime);
end;

procedure TFmMPV2.ImpressaoNormal(ProdutosM, ProdutosMIts, Pagtos: TmySQLQuery);
begin
end;

procedure TFmMPV2.ImpressaoDireta2(ProdutosM, ProdutosMIts, Pagtos: TmySQLQuery;
Cria, Mostra: Boolean);
const
  Titulo = 'ORCAMENTO N. ';
  TA1 = 11;
  TA2 = 55;
  TA3 = 11;
  TA4 = 55;
  TB1 = 11;
  TB2 = 22;
  TB3 = 11;
  TB4 = 22;
  TB5 = 11;
  TB6 = 22;
  TB7 = 11;
  TB8 = 22;
var
  //ITENS: Integer;// = 37;
  i: Integer;
  Divisor, Data: String;
  Textos: TMyTextos_136;
  Alinha: TMyAlinha_136;
  TamCol: TMyTamCol_136;
  //ColsImpCompress: Integer;
  DesTo, DesPo, Qtde, Desco, Compl, Total, Bruto: Double;
begin
  {ITENS := 0;
  case Dmod.QrControleOrcaLinhas.Value of
    0: ITENS := 42;
    1: ITENS := 11;
  end;
  if Dmod.QrControleOrcaRodape.Value = 1 then ITENS := ITENS + 4;
  if Dmod.QrControleOrcaCabecalho.Value = 2 then ITENS := ITENS - 2;}
  ////////////////////////////////////////////////////////////////////////////////
  VAR_LOCCOLUNASIMP := 132;
  ////////////////////////////////////////////////////////////////////////////////
  Data := FormatDateTime('hh:nn:ss "  "dd/mm/yyyy', Now());
  //ColsImpCompress := Trunc(VAR_LOCCOLUNASIMP * 17 / 10);
  Divisor := '';
  for i := 1 to VAR_LOCCOLUNASIMP do Divisor := Divisor + '=';
  //
  ReopenTransportador(ProdutosM);
  ReopenFornecedor(ProdutosM);
  //
  if Cria then Application.CreateForm(TFmDotPrint, FmDotPrint);
  with FmDotPrint do
  begin
    FTipoLinhas := True;
    FQtdeLinhas := Dmod.QrControleOrcaLinhas.Value;
    with RE1 do
    begin
      if Cria then
      begin
        Lines.Clear;
        AdicionaLinhaTexto('', [fdpCompress], []);
        AdicionaLinhaTexto(Geral.CompletaString(
          'PEDIDO N� '+IntToStr(QrMPV2Codigo.Value)
          , ' ', 132, taRightJustify, True), [fdpCompress], []);
        AdicionaLinhaTexto(Geral.CompletaString(Data, ' ', 132, taRightJustify,
          True), [fdpCompress], []);
        if Dmod.QrControleOrcaCabecalho.Value in [1,2] then
        begin
          AdicionaLinhaTexto(Dmod.QrMasterEm.Value+ ' Fone: '+
            DModG.QrDonoFONES.Value, [fdpCompress], []);
          AdicionaLinhaTexto(DModG.QrDonoE_CUC.Value, [fdpCompress], []);
        end;
        if Dmod.QrControleOrcaCabecalho.Value in [0,2] then
          AdicionaLinhaTexto(Titulo+FormatFloat('000000',
          ProdutosM.FieldByName('Codigo').AsInteger)+'    Vendedor: '+
          ProdutosM.FieldByName('NOMEVENDEDOR').AsString+
          '  VOLUMES: '+IntToStr(QrMPV2Volumes.Value),
          [fdpCompress], []);
      end;
      AdicionaLinhaTexto(Divisor, [fdpCompress], []);
      AdicionaLinhaTexto('Transportadora: '+ QrTransportadorNOME.Value +
      '  Tel: '+Geral.FormataTelefone_TT(QrTransportadorTelefone.Value)+
      '  Fax: '+Geral.FormataTelefone_TT(QrTransportadorFax.Value),
      [fdpCompress], []);
      if FEntidade <> ProdutosM.FieldByName('Cliente').AsInteger then
      begin
        AdicionaLinhaTexto(Divisor, [fdpCompress], []);
        FEntidade := ProdutosM.FieldByName('Cliente').AsInteger;
        Textos[1] := 'Cliente:';
        Textos[2] := QrFornecedorNOME.Value;
        Textos[3] := 'Endereco:';
        Textos[4] := QrFornecedorENDERECO.Value;
        Alinha[1] := taLeftJustify;
        Alinha[2] := taLeftJustify;
        Alinha[3] := taLeftJustify;
        Alinha[4] := taLeftJustify;
        Alinha[5] := taLeftJustify;
        Alinha[6] := taLeftJustify;
        Alinha[7] := taLeftJustify;
        Alinha[8] := taLeftJustify;
        TamCol[1] := TA1;
        TamCol[2] := TA2;
        TamCol[3] := TA3;
        TamCol[4] := TA4;
        AdicionaLinhaColunas(4, Textos, Alinha, TamCol, [fdpCompress], '');
        //
        Textos[1] := 'Bairro:';
        Textos[2] := QrFornecedorBAIRRO.Value;
        Textos[3] := 'Cidade:';
        Textos[4] := QrFornecedorCIDADE.Value;
        AdicionaLinhaColunas(4, Textos, Alinha, TamCol, [fdpCompress], '');
        //
        TamCol[1] := TB1;
        TamCol[2] := TB2;
        TamCol[3] := TB3;
        TamCol[4] := TB4;
        TamCol[5] := TB5;
        TamCol[6] := TB6;
        TamCol[7] := TB7;
        TamCol[8] := TB8;
        Textos[1] := 'UF:';
        Textos[2] := QrFornecedorNOMEUF.Value;
        Textos[3] := 'CEP:';
        Textos[4] := Geral.FormataCEP_NT(QrFornecedorCEP.Value);
        Textos[5] := 'CNPJ/CPF:';
        Textos[6] := Geral.FormataCNPJ_TT(QrFornecedorCNPJ.Value);
        Textos[7] := 'I.E.:/RG';
        Textos[8] := QrFornecedorIE.Value;
        AdicionaLinhaColunas(8, Textos, Alinha, TamCol, [fdpCompress], '');
        //
        Textos[1] := 'Telefone:';
        Textos[2] := Geral.FormataTelefone_TT(QrFornecedorTelefone.Value);
        Textos[3] := 'Fax:';
        Textos[4] := Geral.FormataTelefone_TT(QrFornecedorFax.Value);
        Textos[5] := 'Contato:';
        Textos[6] := QrFornecedorContato.Value;
        Textos[7] := 'Celular:';
        Textos[8] := Geral.FormataTelefone_TT(QrFornecedorCelular.Value);
        AdicionaLinhaColunas(8, Textos, Alinha, TamCol, [fdpCompress], '');
        //
        AdicionaLinhaTexto(Divisor, [fdpCompress], []);
        //
      end;
      Alinha[1] := taCenter;
      Alinha[2] := taLeftJustify;
      Alinha[3] := taLeftJustify;
      Alinha[4] := taRightJustify;
      Alinha[5] := taLeftJustify;
      Alinha[6] := taRightJustify;
      Alinha[7] := taRightJustify;
      Alinha[8] := taRightJustify;
      Alinha[9] := taRightJustify;
      TamCol[1] := 08;
      TamCol[2] := 26;
      TamCol[3] := 28;
      TamCol[4] := 08;
      TamCol[5] := 08;
      TamCol[6] := 11;
      TamCol[7] := 11;
      TamCol[8] := 11;
      TamCol[9] := 11;
      Textos[1] := 'Codigo';
      Textos[2] := 'Artigo';
      Textos[3] := 'Descri��o';
      Textos[4] := 'Qtde';
      Textos[5] := ' Un';
      Textos[6] := 'Preco';
      Textos[7] := 'Sub-Total';
      Textos[8] := 'Desconto';
      Textos[9] := 'Total';
      AdicionaLinhaColunas(9, Textos, Alinha, TamCol, [fdpCompress], '');
      ProdutosMIts.First;
      Qtde  := 0;
      Desco := 0;
      Compl := 0;
      Bruto := 0;
      Total := 0;
      while not ProdutosMIts.Eof do
      begin
        Qtde  := Qtde  + ProdutosMIts.FieldByName('Qtde').AsFloat;
        Desco := Desco + 0;//ProdutosMIts.FieldByName('DescoItens').AsFloat;
        Compl := Compl + 0;//ProdutosMIts.FieldByName('Complemento').AsFloat;
        Bruto := Bruto + (
                     ProdutosMIts.FieldByName('Qtde').AsFloat *
                     ProdutosMIts.FieldByName('Preco').AsFloat );
        Total := Total + ProdutosMIts.FieldByName('Valor').AsFloat;
        //
        Textos[1] := FormatFloat('000', ProdutosMIts.FieldByName('MP').AsInteger);
        Textos[2] := ProdutosMIts.FieldByName('NOMEMP').AsString;
        Textos[3] := ProdutosMIts.FieldByName('TEXTO').AsString;
        Textos[4] := FormatFloat('#,###,##0.00', ProdutosMIts.FieldByName('Qtde').AsFloat);
        Textos[5] := ' '+'m2';//ProdutosMIts.FieldByName('NOMEUNIDADE').AsString;
        Textos[6] := FormatFloat('#,###,##0.00', ProdutosMIts.FieldByName('Preco').AsFloat);
        Textos[7] := FormatFloat('#,###,##0.00', ProdutosMIts.FieldByName('SubTo').AsFloat);
        Textos[8] := FormatFloat('#,###,##0.00', ProdutosMIts.FieldByName('Desco').AsFloat);
        Textos[9] := FormatFloat('#,###,##0.00', ProdutosMIts.FieldByName('Valor').AsFloat );
        AdicionaLinhaColunas2(9, Textos, Alinha, TamCol, '', 0, 0);
        ProdutosMIts.Next;
      end;
      Total := Total - 0;
      //
      AdicionaLinhaTexto(Divisor, [fdpCompress], []);
      //if Qtde > 0 then Preco := Total / Qtde else Preco := 0;
      Textos[1] := '';
      Textos[2] := 'TOTAL ITENS:';
      Textos[3] := '';
      Textos[4] := FormatFloat('#,###,##0.00', Qtde);
      Textos[5] := '';
      Textos[6] := '';
      Textos[7] := '';
      Textos[8] := '';
      Textos[9] := FormatFloat('#,###,##0.00', Total);
      AdicionaLinhaColunas(9, Textos, Alinha, TamCol, [fdpCompress], '');
      AdicionaLinhaTexto(Divisor, [fdpCompress], []);
      //
      DesTo := Compl-Desco-0;
      desPo := 0;
      if DesTo <> 0 then
      begin
        if Bruto > 0 then DesPo := DesTo / Bruto * 100 else DesTo := 0;
        Textos[1] := '';
        Textos[2] := '';
        if DesTo <= 0 then
        Textos[3] := 'DESCONTOS'
        else
        Textos[3] := 'ACR�SCIMOS';
        Textos[4] := FormatFloat('#,###,##0.00', Despo)+' %';
        Textos[5] := FormatFloat('#,###,##0.00', DesTo)+' $';
        Textos[6] := '';
        Textos[7] := '';
        Textos[8] := 'TOTAL';
        Textos[9] := FormatFloat('#,###,##0.00', Total);
        AdicionaLinhaColunas(7, Textos, Alinha, TamCol, [fdpCompress], '');
        AdicionaLinhaTexto(Divisor, [fdpCompress], []);
        //
      end;
      //
      if ProdutosM.FieldByName('DescoExtra').AsFloat <> 0 then
      begin
        Textos[1] := '';
        Textos[2] := 'DESCONTO EXTRA:';
        Textos[3] := '';
        Textos[4] := '';
        Textos[5] := '';
        Textos[6] := '';
        Textos[7] := '';
        Textos[8] := '';
        Textos[9] := FormatFloat('#,###,##0.00', ProdutosM.FieldByName('DescoExtra').AsFloat);
        AdicionaLinhaColunas(9, Textos, Alinha, TamCol, [fdpCompress], '');
        AdicionaLinhaTexto(Divisor, [fdpCompress], []);
        //
        Textos[1] := '';
        Textos[2] := 'TOTAL GERAL:';
        Textos[3] := '';
        Textos[4] := '';
        Textos[5] := '';
        Textos[6] := '';
        Textos[7] := '';
        Textos[8] := '';
        Textos[9] := FormatFloat('#,###,##0.00', ProdutosM.FieldByName('TOTAL').AsFloat);
        AdicionaLinhaColunas(9, Textos, Alinha, TamCol, [fdpCompress], '');
        AdicionaLinhaTexto(Divisor, [fdpCompress], []);
        //
      end;
      //FQtdeD  := FQtdeD  + Qtde;
      //FDesco := FDesco + Desco;
      //FCompl := FCompl + Compl;
      FTotal := FTotal + Total;
    end;
    if (Cria=False) and (Mostra=True) then
    begin
        Textos[1] := '';
        Textos[2] := 'TOTAL GERAL:';
        Textos[3] := '';
        Textos[4] := '';
        Textos[5] := '';
        Textos[6] := '';
        Textos[7] := '';
        Textos[8] := '';
        Textos[9] := FormatFloat('#,###,##0.00', FTotal);
        AdicionaLinhaColunas(7, Textos, Alinha, TamCol, [fdpCompress], '');
        AdicionaLinhaTexto(Divisor, [fdpCompress], []);
    end;

    if Cria and Mostra then
    begin
      Alinha[1] := taCenter;
      Alinha[2] := taRightJustify;
      Alinha[3] := taLeftJustify;
      TamCol[1] := 11;
      TamCol[2] := 14;
      TamCol[3] := 55;
      Textos[1] := 'Vencimento';
      Textos[2] := 'Valor';
      Textos[3] := '  Carteira';
      AdicionaLinhaColunas2(3, Textos, Alinha, TamCol, '', 1, 1);
      Pagtos.First;
      Total := 0;
      while not Pagtos.Eof do
      begin
        Textos[1] := Pagtos.FieldByName('VENCTO_TXT').AsString;
        Textos[2] := Pagtos.FieldByName('VALOR_TXT').AsString;
        Textos[3] := '  '+Pagtos.FieldByName('NOMECARTEIRA').AsString;
        Total := Total + Pagtos.FieldByName('Credito').AsFloat;
        //AdicionaLinhaColunas(3, Textos, Alinha, TamCol, [fdpNormalSize], '');
        AdicionaLinhaColunas2(3, Textos, Alinha, TamCol, '', 1, 1);
        Pagtos.Next;
      end;
      AdicionaLinhaTexto(Divisor, [fdpCompress], []);
      //
      Alinha[1] := taLeftJustify;
      Alinha[2] := taRightJustify;
      Alinha[3] := taCenter;
      Alinha[4] := taLeftJustify;
      Alinha[5] := taRightJustify;
      TamCol[1] := 11;
      TamCol[2] := 14;
      TamCol[3] := 72;
      TamCol[4] := 11;
      TamCol[5] := 14;
      Textos[1] := 'Total';
      Textos[2] := FormatFloat('#,###,##0.00', Total);
      Textos[3] := '';
      Textos[4] := 'Pendente';
      Textos[5] := FormatFloat('#,###,##0.00', QrMPV2SUBTOTAL.Value-Total);
      AdicionaLinhaColunas2(5, Textos, Alinha, TamCol, '', 0, 0);
      AdicionaLinhaTexto(Divisor, [fdpCompress], []);
    end;
    if Mostra then
    begin
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmMPV2.QrMPV2ItsCalcFields(DataSet: TDataSet);
begin
  {
  QrMPV2ItsSubTo.Value := QrMPV2ItsValor.Value+QrMPV2ItsDesco.Value;
  if QrMPV2ItsPronto.Value < 2 then QrMPV2ItsPRONTO_TXT.Value := ' * N�O * '
  else QrMPV2ItsPRONTO_TXT.Value := Geral.FDT(QrMPV2ItsPronto.Value, 2);
  }
end;

procedure TFmMPV2.DefineVarDup;
begin
  IC3_ED_FatNum := QrMPV2Codigo.Value;
  IC3_ED_NF     := 0;//Geral.IMV(EdNF.Text);
  IC3_ED_Data   := QrMPV2DataF.Value;
end;

function TFmMPV2.DiferencasEmPagtos: Double;
var
  Valor: Double;
begin
  Result := 0;
  LocCod(QrMPV2Codigo.Value, QrMPV2Codigo.Value);
  ReopenSumC();
  //
  Valor := QrMPV2SUBTOTAL.Value - QrSumCCredito.Value;
  if Valor > 0 then FFat1 := Valor else FFat1 := 0;
  //
  //FFat2 := QrMPV2Frete.Value - QrMPV2CONFDUPLIT.Value;
end;

procedure TFmMPV2.BtPagamentoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagtos, BtPagamento);
end;

procedure TFmMPV2.ExcluiPagto;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  if UFinanceiro.ExcluiLct_FatParcela(QrEmissC, QrEmissCFatID.Value,
    QrEmissCFatNum.Value, QrEmissCFatParcela.Value, QrEmissCCarteira.Value,
    QrEmissCSit.Value, QrEmissCTipo.Value, dmkPF.MotivDel_ValidaCodigo(318),
    FTabLctA) then
  begin
    ReopenEmissC(0);
  end;
end;

procedure TFmMPV2.ExcluiTodosPagtos;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  if UFinanceiro.ExcluiLct_FatNum(QrEmissC, QrEmissCFatID.Value,
  QrEmissCFatNum.Value, 0, QrEmissCCarteira.Value,
  dmkPF.MotivDel_ValidaCodigo(318), True, FTabLctA) then
    ReopenEmissC(0);
end;

procedure TFmMPV2.QrEmissCCalcFields(DataSet: TDataSet);
begin
  if QrEmissCCompensado.Value < 2 then
     QrEmissCCOMPENSA_TXT.Value := '' else
     QrEmissCCOMPENSA_TXT.Value := FormatDateTime(VAR_FORMATDATE2,
     QrEmissCCompensado.Value);
  //
  if QrEmissCVencimento.Value < 2 then
     QrEmissCVENCTO_TXT.Value := '' else
     QrEmissCVENCTO_TXT.Value := FormatDateTime(VAR_FORMATDATE2,
     QrEmissCVencimento.Value);
  //
  QrEmissCVALOR_TXT.Value := Geral.FFT(QrEmissCCredito.Value, 2, siNegativo);
  QrEmissCSEQ.Value := QrEmissC.RecordCount;
end;

procedure TFmMPV2.frMovimentoUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if Name = 'VARF_GRADE' then Val := 15;
  if Name = 'VARF_DESEX' then Val := dmkPF.FloatToBool(QrMPV2DescoExtra.Value, 2);
  if Name = 'VARF_PENDE' then
  begin
    ReopenSumC();
    Val := dmkPF.FloatToBool(QrMPV2SUBTOTAL.Value-QrSumCCredito.Value, 2);
  end;
end;

procedure TFmMPV2.QrFornecedorCalcFields(DataSet: TDataSet);
var
  Endereco: String;
begin
  QrFornecedorTEL_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorTelefone.Value);
  QrFornecedorFAX_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorFax.Value);
  QrFornecedorCEL_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorCelular.Value);
  QrFornecedorCPF_TXT.Value := Geral.FormataCNPJ_TT(QrFornecedorCNPJ.Value);
  QrFornecedorCEP_TXT.Value := Geral.FormataCEP_TT(FloatToStr(QrFornecedorCEP.Value));
  //
  Endereco := QrFornecedorRUA.Value;
  if Trim(QrFornecedorLOGRAD.Value) <> '' then
  begin
    if Uppercase(Copy(QrFornecedorRUA.Value, 1, Length(QrFornecedorLOGRAD.Value))) <>
      Uppercase(QrFornecedorLOGRAD.Value) then
    Endereco := QrFornecedorLOGRAD.Value + ' ' + Endereco;
  end;
  //
  if QrFornecedorNUMERO.Value = 0 then
    QrFornecedorNUMEROTXT.Value := 'S/N' else
    QrFornecedorNUMEROTXT.Value :=
    FloatToStr(QrFornecedorNUMERO.Value);
  QrFornecedorENDERECO.Value :=
    Endereco + ', '+
    QrFornecedorNUMEROTXT.Value + '  '+
    QrFornecedorCOMPL.Value;
end;

procedure TFmMPV2.QrTransportadorCalcFields(DataSet: TDataSet);
var
  Endereco: String;
begin
  QrTransportadorTEL_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorTelefone.Value);
  QrTransportadorFAX_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorFax.Value);
  QrTransportadorCEL_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorCelular.Value);
  QrTransportadorCPF_TXT.Value := Geral.FormataCNPJ_TT(QrTransportadorCNPJ.Value);
  QrTransportadorCEP_TXT.Value := Geral.FormataCEP_TT(FloatToStr(QrTransportadorCEP.Value));
  //
  Endereco := QrTransportadorRUA.Value;
  if Trim(QrTransportadorLOGRAD.Value) <> '' then
  begin
    if Uppercase(Copy(QrTransportadorRUA.Value, 1, Length(QrTransportadorLOGRAD.Value))) <>
      Uppercase(QrTransportadorLOGRAD.Value) then
    Endereco := QrTransportadorLOGRAD.Value + ' ' + Endereco;
  end;
  //
  if QrTransportadorNUMERO.Value = 0 then
    QrTransportadorNUMEROTXT.Value := 'S/N' else
    QrTransportadorNUMEROTXT.Value :=
    FloatToStr(QrTransportadorNUMERO.Value);
  QrTransportadorENDERECO.Value :=
    Endereco + ', '+
    QrTransportadorNUMEROTXT.Value + '  '+
    QrTransportadorCOMPL.Value;
end;

procedure TFmMPV2.ImpressoNormal1Click(Sender: TObject);
begin
  ImpressaoNormal(QrMPV2, QrMPV2Its, QrEmissC);
end;

procedure TFmMPV2.Impressodireta1Click(Sender: TObject);
begin
  ImpressaoDireta2(QrMPV2, QrMPV2Its, QrEmissC, True, True);
end;

procedure TFmMPV2.EdDescoExtraExit(Sender: TObject);
begin
  dmkEdDescoExtra.ValMax := Geral.FFT(QrMPV2Valor.Value, 2, siPositivo);
end;

procedure TFmMPV2.QrMPV2CalcFields(DataSet: TDataSet);
begin
  QrMPV2SUBTOTAL.Value := QrMPV2Valor.Value - QrMPV2DescoExtra.Value +
    QrMPV2Desp_Acess.Value;
  if QrMPV2CobraSegur.Value > 0 then
    QrMPV2SUBTOTAL.Value := QrMPV2SUBTOTAL.Value + QrMPV2Seguro.Value;

 //

  QrMPV2TOTALNOTA.Value := QrMPV2SUBTOTAL.Value;
  if QrMPV2CobraFrete.Value > 0 then
    QrMPV2TOTALNOTA.Value := QrMPV2TOTALNOTA.Value + QrMPV2FreteNF.Value;

  //

  QrMPV2RECEITAREAL.Value := QrMPV2TOTALNOTA.Value - QrMPV2FretePg.Value;

end;

procedure TFmMPV2.MenuItem3Click(Sender: TObject);
begin
  ExcluiMercadoria;
end;

procedure TFmMPV2.ExcluiMercadoria;
var
  Codigo, Controle: Integer;
begin
  Codigo := QrMPV2ItsCodigo.Value;
  Controle := QrMPV2ItsControle.Value;
  if Application.MessageBox(PChar(FIN_MSG_ASKESCLUI), 'Pergunta', MB_YESNOCANCEL) = ID_YES then
  begin
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mpv2its WHERE Codigo=:P0');
    Dmod.QrUpd.SQL.Add('AND Controle=:P2');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.Params[1].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    AtualizaTotais;
    LocCod(QrMPV2Codigo.Value, QrMPV2Codigo.Value);
    ReopenMPV2Its(0);
  end;
end;

procedure TFmMPV2.MenuItem4Click(Sender: TObject);
begin
  ExcluiPagto;
end;

procedure TFmMPV2.ImpressaoOculta;
begin
  {
  ReopenTransportador(QrMPV2);
  ReopenFornecedor(QrMPV2);
  ReopenSumIts(QrMPV2);
  //
  frMovimento.PrepareReport;
  frMovimento.PrintPreparedReport('', 2, True, frAll);
  }
end;

procedure TFmMPV2.Incluinovamercadoria1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmMPV2.Alteradadosdamercadoriaatual1Click(Sender: TObject);
begin
  MostraEdicao(2, stUpd, 0);
end;

procedure TFmMPV2.Excluimercadoriaatual1Click(Sender: TObject);
begin
  ExcluiMercadoria;
end;

procedure TFmMPV2.Incluinovospagamentos1Click(Sender: TObject);
var
  Terceiro, Cod, Genero: Integer;
begin
  if FmPrincipal.AvisaFaltaDeContaDoPlano(VAR_FATID_1007) then Exit;
  DiferencasEmPagtos;
  DefineVarDup;
  VAR_MYPAGTOSCONFIG   := 1;
  Genero    := Dmod.QrControleCtaCVVenda.Value;
  Cod       := QrMPV2Codigo.Value;
  Terceiro  := QrMPV2Cliente.Value;
  UPagtos.Pagto(QrEmissC, tpCred, Cod, Terceiro, VAR_FATID_1013, Genero, 0(*GenCtb*),
  stIns, 'Venda de Couro n�o Curtido', FFat1, VAR_USUARIO, 0, 0, mmNenhum, 0, 0,
  True, False, 0, 0, 0, 0, 0, FTabLctA);
  //AtualizaMPV2(QrMPInCodigo.Value);
  // N�o precisa
  //ReopenEmissC(?);
  if FDireto then
  begin
    BtTravaClick(Self);
    BtAlteraClick(Self);
  end;
end;

procedure TFmMPV2.Alterapagamentoatual1Click(Sender: TObject);
var
  CliInt, Terceiro, Cod, Genero: Integer;
  Valor: Double;
begin
  IC3_ED_Controle := QrEmissCControle.Value;
  Valor     := QrEmissCCredito.Value;
  CliInt     := QrEmissCCliInt.Value;
  /////////////////
  DiferencasEmPagtos;
  DefineVarDup;
  Genero    := Dmod.QrControleCtaCVVenda.Value;
  Cod       := QrMPV2Codigo.Value;
  Terceiro  := QrMPV2Cliente.Value;
  UPagtos.Pagto(QrEmissC, tpCred, Cod, Terceiro, VAR_FATID_1013, Genero, 0(*GenCtb*),
  stUpd, 'Venda de Couro n�o Curtido', Valor, VAR_USUARIO, 0, CliInt, mmNenhum, 0, 0,
  True, False, 0, 0, 0, 0, 0, FTabLctA);
  //
  AtualizaTotais;
  LocCod(QrMPV2Codigo.Value, QrMPV2Codigo.Value);
end;

procedure TFmMPV2.Excluipagamentoatual1Click(Sender: TObject);
begin
  ExcluiPagto;
end;

procedure TFmMPV2.Excluitodospagamentos1Click(Sender: TObject);
begin
  ExcluiTodosPagtos;
end;

procedure TFmMPV2.PMItemPopup(Sender: TObject);
begin
  if QrMPV2Its.RecordCount > 0 then
  begin
    Alteradadosdamercadoriaatual1.Enabled := True;
    Excluimercadoriaatual1.Enabled := True;
  end else begin
    Alteradadosdamercadoriaatual1.Enabled := False;
    Excluimercadoriaatual1.Enabled := False;
  end;
end;

procedure TFmMPV2.PMPagtosPopup(Sender: TObject);
begin
  if QrEmissC.RecordCount > 0 then
  begin
    Alterapagamentoatual1.Enabled := True;
    Excluipagamentoatual1.Enabled := True;
    if QrEmissC.RecordCount > 1 then
      Excluitodospagamentos1.Enabled := True
    else
      Excluitodospagamentos1.Enabled := False;
  end else begin
    Alterapagamentoatual1.Enabled := False;
    Excluipagamentoatual1.Enabled := False;
    Excluitodospagamentos1.Enabled := False;
  end;
end;

procedure TFmMPV2.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if Application.MessageBox('Confirma a EXCLUS�O DE TODO pedido?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Codigo := QrMPV2Codigo.Value;
    UFinanceiro.ExcluiLct_FatNum(QrEmissC, QrEmissCFatID.Value,
      QrEmissCFatNum.Value, 0, QrEmissCCarteira.Value,
      dmkPF.MotivDel_ValidaCodigo(318), False, FTabLctA);
    //
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mpv2its WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mpv2 WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    QrMPV2.Close;
    Va(vpNext);
  end;
end;

procedure TFmMPV2.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprimeDireto, Btimprime);
end;

procedure TFmMPV2.Pedido1Click(Sender: TObject);
begin
  ImpressaoOculta;
end;

procedure TFmMPV2.Ordemdeproduo1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxOS, 'Ordem de produ��o n� ' + IntToStr(QrMPV2ItsControle.Value));
end;

procedure TFmMPV2.EdMPChange(Sender: TObject);
var
  Preco: Double;
begin
  if ImgTipo.SQLType = stIns then
  begin
    if QrMPsPrecoICM.Value = 0 then
      Preco := QrMPsPreco.Value / (1 - (QrMPsICMS.Value / 100))
    else
      Preco := QrMPsPreco.Value;
    dmkEdPreco.ValueVariant    := Preco;
    dmkEdICMS_Per.ValueVariant := QrMPsICMS.Value;
    dmkEdIPI_Per.ValueVariant  := QrMPsIPI.Value;
    CalculaTotal;
  end;
end;

procedure TFmMPV2.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.CadastroDeDefPecas();
  QrDefPecas.Close;
  UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
  if VAR_CADASTRO <> 0 then
  begin
    EdUnidade.Text := IntToStr(VAR_CADASTRO);
    CBUnidade.KeyValue := VAR_CADASTRO;
  end;
end;

procedure TFmMPV2.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.CadastroDeArtigosGrupos();
  QrMPs.Close;
  UnDmkDAC_PF.AbreQuery(QrMPs, Dmod.MyDB);
  if VAR_CADASTRO <> 0 then
  begin
    //  Rever pre�o
    EdMP.Text := '';
    CBMP.KeyValue := NULL;
    //
    EdMP.Text := IntToStr(VAR_CADASTRO);
    CBMP.KeyValue := VAR_CADASTRO;
  end;
end;

procedure TFmMPV2.RGGrandezaClick(Sender: TObject);
begin
  CalculaTotal;
end;

procedure TFmMPV2.dmkEdPecasChange(Sender: TObject);
begin
  CalculaTotal;
end;

procedure TFmMPV2.dmkEdPesoChange(Sender: TObject);
begin
  CalculaTotal;
end;

procedure TFmMPV2.dmkEdM2Change(Sender: TObject);
begin
  CalculaTotal;
end;

procedure TFmMPV2.dmkEdP2Change(Sender: TObject);
begin
  CalculaTotal;
end;

procedure TFmMPV2.dmkEdPrecoChange(Sender: TObject);
begin
  CalculaTotal;
end;

procedure TFmMPV2.dmkEdICMS_PerChange(Sender: TObject);
begin
  CalculaTotal;
end;

procedure TFmMPV2.dmkEdIPI_PerChange(Sender: TObject);
begin
  CalculaTotal;
end;

procedure TFmMPV2.CalculaTotal;
var
  Preco, SubTo, Valor, ICMS_Val, IPI_Val: Double;
begin
  Preco := dmkEdPreco.ValueVariant;
  case RGGrandeza.ItemIndex of
    1: FQtde := dmkEdPecas.ValueVariant;
    2: FQtde := dmkEdPeso.ValueVariant;
    3: FQtde := dmkEdPeso.ValueVariant / 1000;
    4: FQtde := dmkEdM2.ValueVariant;
    5: FQtde := dmkEdP2.ValueVariant;
    else FQtde := 0;
  end;
  SubTo := Preco * FQtde;
  {
  Desco := Geral.DMV(EdDesco.Text);
  if Desco > SubTo then
  begin
    Desco := 0;
    EdDesco.Text := '0,00';
  end;
  Valor := SubTo - Desco;
  EdSubTo.Text := Geral.FFT(SubTo, 2, siNegativo);
  }
  Valor := SubTo;
  ICMS_Val := Trunc(Valor * dmkEdICMS_Per.ValueVariant) / 100;
  IPI_Val  := Trunc(Valor * dmkEdIPI_Per.ValueVariant) / 100;
  //
  dmkEdTotal.Text    := Geral.FFT(Valor,    2, siNegativo);
  dmkEdICMS_Val.Text := Geral.FFT(ICMS_Val, 2, siNegativo);
  dmkEdIPI_Val.Text  := Geral.FFT(IPI_Val,  2, siNegativo);
end;

procedure TFmMPV2.SpeedButton7Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.CadastroDeMPEstagios();
  QrMPEstagios.Close;
  UnDmkDAC_PF.AbreQuery(QrMPEstagios, Dmod.MyDB);
  if VAR_CADASTRO <> 0 then
  begin
    EdEstagio.Text := IntToStr(VAR_CADASTRO);
    CBEstagio.KeyValue := VAR_CADASTRO;
  end;
end;

procedure TFmMPV2.QrMPV2ItsBeforeClose(DataSet: TDataSet);
begin
  QrMPV2Bxa.Close;
end;

procedure TFmMPV2.QrMPV2ItsAfterScroll(DataSet: TDataSet);
begin
  ReopenMPV2Bxa(0);
end;

procedure TFmMPV2.ReopenMPV2Bxa(Conta: Integer);
begin
  QrMPV2Bxa.Close;
  QrMPV2Bxa.Params[0].AsInteger := QrMPV2ItsControle.Value;
  UnDmkDAC_PF.AbreQuery(QrMPV2Bxa, Dmod.MyDB);
  //
  if Conta > 0 then QrMPV2Bxa.Locate('Conta', Conta, []);
end;

procedure TFmMPV2.Incluibaixadecouro1Click(Sender: TObject);
begin
  Application.CreateForm(TFmMPV2Bxa, FmMPV2Bxa);
  FmMPV2Bxa.ShowModal;
  FmMPV2Bxa.Destroy;
end;

procedure TFmMPV2.BtBaixaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBaixa, BtBaixa);
end;

procedure TFmMPV2.Excluiaixadecouro1Click(Sender: TObject);
var
  Marca, Prox: Integer;
begin
  Marca := QrMPV2BxaMarca_ID.Value;
  if Application.MessageBox('Confirma a exclus�o da baixa selecionada?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mpv2bxa ');
    Dmod.QrUpd.SQL.Add('WHERE Conta=:P0 ');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[0].AsInteger := QrMPV2BxaConta.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.AtualizaEstoqueMarca(Marca);
    //
    Prox := UmyMod.ProximoRegistro(QrMPV2Bxa, 'Conta', 0);
    //
    ReopenMPV2Bxa(Prox);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMPV2.PMBaixaPopup(Sender: TObject);
begin
  Excluiaixadecouro1.Enabled := QrMPV2Bxa.RecordCount > 0;
end;

procedure TFmMPV2.Notafiscalmatricial1Click(Sender: TObject);
var
  Desc_per: Double;
begin
(*
  //colocar valor de despesas acess�rias etc.
  UCriar.GerenciaTabelaLocal('NF1Fat', acCreate);
  UCriar.GerenciaTabelaLocal('NF1Pro', acCreate);
  UCriar.GerenciaTabelaLocal('Imprimir1', acCreate);
  Application.CreateForm(TFmNF1, FmNF1);
  //
  FmNF1.LimpaVars;
  FmNF1.FRecalculaNF          := False;
  FmNF1.EdNF.Text             := Geral.FFT(QrMPV2NF.Value, 0, siNegativo);
  FmNF1.EdCliente.Text        := IntToStr(QrMPV2Cliente.Value);
  FmNF1.CBCliente.KeyValue    := QrMPV2Cliente.Value;
  FmNF1.EdBaseICMS.Text       := Geral.FFT(QrMPV2Valor.Value, 2, siNegativo);
  FmNF1.EdValorICMS.Text      := Geral.FFT(QrMPV2ICMS_Val.Value, 2, siNegativo);
  FmNF1.EdBaseICMS_Sub.Text   := '0,00';
  FmNF1.EdValorICMS_Sub.Text  := '0,00';
  FmNF1.EdTotProd.Text        := Geral.FFT(QrMPV2Valor.Value, 2, siNegativo);
  FmNF1.EdFrete.Text          := Geral.FFT(QrMPV2FreteNF.Value, 2, siNegativo);
  FmNF1.EdSeguro.Text         := Geral.FFT(QrMPV2Seguro.Value, 2, siNegativo);
  FmNF1.EdOutros.Text         := Geral.FFT(QrMPV2Desp_Acess.Value, 2, siNegativo);
  FmNF1.EdValorIPI.Text       := Geral.FFT(QrMPV2IPI_Val.Value, 2, siNegativo);
  FmNF1.EdTotalNota.Text      := Geral.FFT(QrMPV2TOTALNOTA.Value, 2, siNegativo);
  FmNF1.EdTransp.Text         := IntToStr(QrMPV2Transp.Value);
  FmNF1.CBTransp.KeyValue     := QrMPV2Transp.Value;
  FmNF1.EdFPC.Text            := Geral.FFT(QrMPV2CobraFrete.Value + 1, 0, siPositivo);
  FmNF1.EdUFPlaca.Text        := QrMPV2UFPlaca.Value;
  FmNF1.EdPlaca.Text          := QrMPV2Placa.Value;
  FmNF1.EdQtde.Text           := Geral.FFT(QrMPV2Qtde.Value, 2, siNegativo);
  FmNF1.EdEspecie.Text        := QrMPV2Especie.Value;
  FmNF1.EdMarca.Text          := QrMPV2Marca.Value;
  FmNF1.EdNumero.Text         := QrMPV2Numero.Value;
  FmNF1.EdkgB.Text            := Geral.FFT(QrMPV2kgBruto.Value, 3, siNegativo);
  FmNF1.EdkgL.Text            := Geral.FFT(QrMPV2kgLiqui.Value, 3, siNegativo);
  {
  FmNF1.Ed.Text := Geral.FFT(QrMPV2.Value, 2, siNegativo);
  FmNF1.Ed.Text := Geral.FFT(QrMPV2.Value, 2, siNegativo);
  FmNF1.Ed.Text := Geral.FFT(QrMPV2.Value, 2, siNegativo);
  }

  //
  Desc_Per := 0;
  QrMPV2Its.First;
  while not QrMPV2Its.Eof do
  begin
    UMyMod.SQLInsUpdL(Dmod.QrUpdL, DmkEnums.NomeTipoSQL(stIns), 'nf1pro', True,
    [
      'Codigo', 'Produto', 'ClasFisc',
      'SitTrib', 'Unidade', 'Qtde',
      'Preco', 'Desc_per', 'Valor',
      'ICMS_Per', 'ICMS_Val',
      'IPI_Per', 'IPI_Val'
    ], ['Controle'], [
      QrMPV2ItsCodImp.Value, QrMPV2ItsNOMEMP.Value, QrMPV2ItsCF.Value,
      QrMPV2ItsST.Value, QrMPV2ItsNOMEGRANDEZA.Value, QrMPV2ItsQtde.Value,
      QrMPV2Itspreco.Value, Desc_per, QrMPV2ItsTotal.Value,
      QrMPV2ItsICMS_Per.Value,QrMPV2ItsICMS_Val.Value,
      QrMPV2ItsIPI_Per.Value, QrMPV2ItsIPI_Val.Value
    ], [0]);
    //
    QrMPV2Its.Next;
  end;
  FmNF1.TbNF1Pro.Close;
  UnDmkDAC_PF.AbreTable(FmNF1.TbNF1Pro);

  //FmNF1.RecalculaTotalNF;

  FmNF1.ShowModal;
  FmNF1.Destroy;
*)
end;

procedure TFmMPV2.Incluinovoitemdefrete1Click(Sender: TObject);
var
  Terceiro, Cod, Genero: Integer;
begin
  if FmPrincipal.AvisaFaltaDeContaDoPlano(VAR_FATID_1008) then Exit;
  DiferencasEmPagtos;
  DefineVarDup;
  Genero    := Dmod.QrControleCtaCVFrete.Value;
  Cod       := QrMPV2Codigo.Value;
  Terceiro  := QrMPV2Transp.Value;
  UPagtos.Pagto(QrEmissT, tpDeb, Cod, Terceiro, VAR_FATID_1014, Genero, 0(*GenCtb*),
  stIns, 'Frete de Venda de couro', FFat2, VAR_USUARIO, 0, 0, mmNenhum, 0, 0,
  True, False, 0, 0, 0, 0, 0, FTabLctA);
  //AtualizaMPV2(QrMPInCodigo.Value);
  // N�o precisa
  //ReopenPagtos;
  if FDireto then
  begin
    BtTravaClick(Self);
    BtAlteraClick(Self);
  end;
end;

procedure TFmMPV2.QrMPV2BeforeClose(DataSet: TDataSet);
begin
  QrEmissT.Close;
  QrEmissC.Close;
  QrMPV2Its.Close;
end;

procedure TFmMPV2.Alteraitemdefrete1Click(Sender: TObject);
var
  CliInt, Terceiro, Cod, Genero: Integer;
  Valor: Double;
begin
  IC3_ED_Controle := QrEmissTControle.Value;
  Valor     := QrEmissTCredito.Value;
  CliInt     := QrEmissTCliInt.Value;
  /////////////////
  DiferencasEmPagtos;
  DefineVarDup;
  Genero    := Dmod.QrControleCtaCVFrete.Value;
  Cod       := QrMPV2Codigo.Value;
  Terceiro  := QrMPV2Transp.Value;
  UPagtos.Pagto(QrEmissT, tpDeb, Cod, Terceiro, VAR_FATID_1014, Genero, 0(*GenCtb*),
  stUpd, 'Frete de Venda de Mercadorias', Valor, VAR_USUARIO, 0,
  CliInt, mmNenhum, 0, 0, True, False, 0, 0, 0, 0, 0, FTabLctA);
  //
  AtualizaTotais;
  LocCod(QrMPV2Codigo.Value, QrMPV2Codigo.Value);
end;

procedure TFmMPV2.ReopenEmissC(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissC, Dmod.MyDB, [
    'SELECT la.Controle+0.000 Controle, la.*, ca.Nome NOMECARTEIRA ',
    'FROM ' + FTabLctA + ' la, Carteiras ca ',
    'WHERE ca.Codigo=la.Carteira ',
    'AND la.FatID=' + Geral.FF0(VAR_FATID_1013),
    'AND la.FatNum=' + Geral.FF0(QrMPV2Codigo.Value),
    'AND la.ID_Pgto = 0 ',
    'ORDER BY la.Vencimento ',
    '']);
  (*
  QrEmissC.Close;
  QrEmissC.Params[0].AsInteger := QrMPV2Codigo.Value;
  QrEmissC.O p e n;
  *)
  if Controle > 0 then
    QrEmissC.Locate('Controle', Controle, [])
  //else
    //QrEmissC.Locate('FatParcela', FatParcela, []);
end;

procedure TFmMPV2.ReopenEmissT(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissT, Dmod.MyDB, [
  'SELECT la.*, ca.Nome NOMECARTEIRA ',
  'FROM ' + FTabLctA + ' la, Carteiras ca ',
  'WHERE ca.Codigo=la.Carteira ',
  'AND FatID=' + Geral.FF0(VAR_FATID_1014),
  'AND FatNum=' + Geral.FF0(QrMPV2Codigo.Value),
  'ORDER BY Vencimento ',
  '']);
  (*
  QrEmissT.Close;
  QrEmissT.Params[0].AsInteger := QrMPV2Codigo.Value;
  QrEmissT.O p e n;
  *)
  if Controle > 0 then
    QrEmissT.Locate('Controle', Controle, [])
  //else
    //QrEmissT.Locate('FatParcela', FatParcela, []);
end;

procedure TFmMPV2.ReopenFornecedor(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFornecedor, Dmod.MyDB, [
  'SELECT  ',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOME, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ERua ',
  'ELSE fo.PRua END RUA, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ENumero ',
  'ELSE fo.PNumero END + 0.000 NUMERO, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECompl ',
  'ELSE fo.PCompl END COMPL, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EBairro ',
  'ELSE fo.PBairro END BAIRRO, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECidade ',
  'ELSE fo.PCidade END CIDADE, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EPais ',
  'ELSE fo.PPais END PAIS, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ETe1 ',
  'ELSE fo.PTe1 END TELEFONE, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EFax ',
  'ELSE fo.PPais END FAX, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECel ',
  'ELSE fo.PCel END Celular, ',
  'CASE WHEN fo.Tipo=0 THEN fo.CNPJ ',
  'ELSE fo.CPF END CNPJ, ',
  'CASE WHEN fo.Tipo=0 THEN fo.IE ',
  'ELSE fo.RG END IE, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECEP ',
  'ELSE fo.PCEP END + 0.000 CEP, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EContato ',
  'ELSE fo.PContato END Contato, ',
  ' ',
  'CASE WHEN fo.Tipo=0 THEN fo.EUF ',
  'ELSE fo.PUF END + 0.000 UF, ',
  'CASE WHEN fo.Tipo=0 THEN uf1.Nome ',
  'ELSE uf2.Nome END NOMEUF, ',
  'EEMail, PEmail, ',
  ' ',
  'CASE WHEN fo.Tipo=0 THEN ll1.Nome ',
  'ELSE ll2.Nome END LOGRAD ',
  ' ',
  'FROM entidades fo ',
  'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf ',
  'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf ',
  'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd ',
  'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd ',
  'WHERE fo.Codigo=' + Geral.FF0(Qry.FieldByName('Cliente').AsInteger),
  '']);
end;

procedure TFmMPV2.BtFreteClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFrete, BtFrete);
end;

end.

