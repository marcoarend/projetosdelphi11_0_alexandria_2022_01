object FmCECTOut: TFmCECTOut
  Left = 339
  Top = 185
  Caption = 
    'PST-_CECT-002 :: Controle de Estoque de Couros de Terceiros - De' +
    'volu'#231#227'o'
  ClientHeight = 496
  ClientWidth = 904
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 904
    Height = 276
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnPesquisa: TPanel
      Left = 0
      Top = 0
      Width = 904
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 904
        Height = 60
        Align = alClient
        Caption = ' Filtros de pesquisa: '
        TabOrder = 0
        object Panel3: TPanel
          Left = 2
          Top = 15
          Width = 419
          Height = 43
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object CBClientePesq: TdmkDBLookupComboBox
            Left = 60
            Top = 18
            Width = 353
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECLI'
            ListSource = DsClientesPesq
            TabOrder = 0
            dmkEditCB = EdClientePesq
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdClientePesq: TdmkEditCB
            Left = 4
            Top = 18
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClientePesqChange
            DBLookupComboBox = CBClientePesq
            IgnoraDBLookupComboBox = False
          end
        end
        object TPIni: TdmkEditDateTimePicker
          Left = 672
          Top = 32
          Width = 110
          Height = 21
          Date = 39750.896049618060000000
          Time = 39750.896049618060000000
          TabOrder = 1
          OnClick = TPIniClick
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPFim: TdmkEditDateTimePicker
          Left = 784
          Top = 32
          Width = 110
          Height = 21
          Date = 39750.896066076390000000
          Time = 39750.896066076390000000
          TabOrder = 2
          OnClick = TPFimClick
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object CkIni: TCheckBox
          Left = 672
          Top = 12
          Width = 110
          Height = 17
          Caption = 'Data inicial:'
          TabOrder = 3
          OnClick = CkIniClick
        end
        object CkFim: TCheckBox
          Left = 784
          Top = 12
          Width = 110
          Height = 17
          Caption = 'Data final:'
          TabOrder = 4
          OnClick = CkFimClick
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 60
      Width = 904
      Height = 216
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object PnEdita: TPanel
        Left = 0
        Top = 162
        Width = 904
        Height = 54
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object Label2: TLabel
          Left = 12
          Top = 8
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label3: TLabel
          Left = 540
          Top = 8
          Width = 17
          Height = 13
          Caption = 'NF:'
        end
        object Label5: TLabel
          Left = 128
          Top = 8
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object TPDataS: TdmkEditDateTimePicker
          Left = 12
          Top = 24
          Width = 112
          Height = 21
          Date = 39749.934630706020000000
          Time = 39749.934630706020000000
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdNFOut: TdmkEdit
          Left = 540
          Top = 24
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdClienteEdit: TdmkEditCB
          Left = 128
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBClienteEdit
          IgnoraDBLookupComboBox = False
        end
        object CBClienteEdit: TdmkDBLookupComboBox
          Left = 184
          Top = 24
          Width = 352
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMECLI'
          ListSource = DsClientesEdit
          TabOrder = 2
          dmkEditCB = EdClienteEdit
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object PnGrades: TPanel
        Left = 0
        Top = 0
        Width = 904
        Height = 162
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Splitter1: TSplitter
          Left = 0
          Top = 79
          Width = 904
          Height = 5
          Cursor = crVSplit
          Align = alBottom
          ExplicitLeft = 1
          ExplicitTop = 202
          ExplicitWidth = 900
        end
        object GradeOut: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 904
          Height = 79
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Title.Caption = 'Cliente'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataS'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFOut'
              Title.Caption = 'NF '
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotQtdkg'
              Title.Caption = 'Peso (kg )'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotQtdPc'
              Title.Caption = 'Pe'#231'as'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotQtdVal'
              Title.Caption = 'Valor total'
              Width = 72
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCECTOut
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Title.Caption = 'Cliente'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataS'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFOut'
              Title.Caption = 'NF '
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotQtdkg'
              Title.Caption = 'Peso (kg )'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotQtdPc'
              Title.Caption = 'Pe'#231'as'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotQtdVal'
              Title.Caption = 'Valor total'
              Width = 72
              Visible = True
            end>
        end
        object GradeIts: TDBGrid
          Left = 0
          Top = 84
          Width = 904
          Height = 78
          Align = alBottom
          DataSource = DsCECTOutIts
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 904
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 856
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 808
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 689
        Height = 32
        Caption = 'Controle de Estoque de Couros de Terceiros - Devolu'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 689
        Height = 32
        Caption = 'Controle de Estoque de Couros de Terceiros - Devolu'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 689
        Height = 32
        Caption = 'Controle de Estoque de Couros de Terceiros - Devolu'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 904
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 900
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBControla: TGroupBox
    Left = 0
    Top = 432
    Width = 904
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 900
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 756
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtNF: TBitBtn
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&NF'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtNFClick
      end
      object BtItens: TBitBtn
        Left = 140
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Itens'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtItensClick
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 368
    Width = 904
    Height = 64
    Align = alBottom
    TabOrder = 4
    Visible = False
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 900
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel7: TPanel
        Left = 756
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn1: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn1Click
        end
      end
      object BitBtn2: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn2Click
      end
    end
  end
  object QrCECTOut: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCECTOutBeforeClose
    AfterScroll = QrCECTOutAfterScroll
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, RazaoSocial, cli.Nome) NOMECLI, Out.*'
      'FROM cectout Out'
      'LEFT JOIN entidades cli ON Out.Cliente=cli.Codigo')
    Left = 16
    Top = 208
    object QrCECTOutNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCECTOutCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCECTOutCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrCECTOutDataS: TDateField
      FieldName = 'DataS'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCECTOutNFOut: TIntegerField
      FieldName = 'NFOut'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCECTOutTotQtdkg: TFloatField
      FieldName = 'TotQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCECTOutTotQtdPc: TFloatField
      FieldName = 'TotQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCECTOutTotQtdVal: TFloatField
      FieldName = 'TotQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsCECTOut: TDataSource
    DataSet = QrCECTOut
    Left = 44
    Top = 208
  end
  object QrClientesPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLI'
      'FROM entidades en '
      'WHERE en.Cliente2="V"'
      '')
    Left = 16
    Top = 264
    object QrClientesPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClientesPesqNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsClientesPesq: TDataSource
    DataSet = QrClientesPesq
    Left = 44
    Top = 264
  end
  object QrCECTOutIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT inn.DataE, inn.NFInn, inn.NFRef, its.*'
      'FROM cectoutits its'
      'LEFT JOIN cectinn inn ON inn.Codigo=its.CECTInn'
      'WHERE its.Codigo=:P0')
    Left = 16
    Top = 236
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCECTOutItsDataE: TDateField
      FieldName = 'DataE'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCECTOutItsNFInn: TIntegerField
      FieldName = 'NFInn'
      DisplayFormat = '000000;-000000; '
    end
    object QrCECTOutItsNFRef: TIntegerField
      FieldName = 'NFRef'
      DisplayFormat = '000000;-000000; '
    end
    object QrCECTOutItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCECTOutItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCECTOutItsCECTInn: TIntegerField
      FieldName = 'CECTInn'
      Required = True
    end
    object QrCECTOutItsOutQtdkg: TFloatField
      FieldName = 'OutQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;#,###,##0.000; '
    end
    object QrCECTOutItsOutQtdPc: TFloatField
      FieldName = 'OutQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;#,###,##0.0; '
    end
    object QrCECTOutItsOutQtdVal: TFloatField
      FieldName = 'OutQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;#,###,##0.00; '
    end
  end
  object DsCECTOutIts: TDataSource
    DataSet = QrCECTOutIts
    Left = 44
    Top = 236
  end
  object QrClientesEdit: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLI'
      'FROM entidades en '
      'WHERE en.Cliente2="V"'
      '')
    Left = 72
    Top = 264
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsClientesEdit: TDataSource
    DataSet = QrClientesEdit
    Left = 100
    Top = 264
  end
  object PMNF: TPopupMenu
    Left = 24
    Top = 460
    object Inclui1: TMenuItem
      Caption = '&Inclui nova NF'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera NF atual'
      OnClick = Altera1Click
    end
  end
  object PMItens: TPopupMenu
    Left = 116
    Top = 460
    object Incluiitemdedevoluo1: TMenuItem
      Caption = '&Inclui item de devolu'#231#227'o'
      OnClick = Incluiitemdedevoluo1Click
    end
    object Retiraitemdedevoluo1: TMenuItem
      Caption = '&Retira item de devolu'#231#227'o'
      OnClick = Retiraitemdedevoluo1Click
    end
  end
end
