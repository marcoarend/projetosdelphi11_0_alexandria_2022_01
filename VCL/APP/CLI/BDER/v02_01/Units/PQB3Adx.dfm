object FmPQB3Adx: TFmPQB3Adx
  Left = 426
  Top = 332
  Caption = 'QUI-BALAN-007 :: Ajuste a Positivo (Re-Entrada)'
  ClientHeight = 437
  ClientWidth = 482
  Color = clBtnFace
  Constraints.MaxHeight = 535
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 482
    Height = 240
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object LaInsumo: TLabel
      Left = 12
      Top = 96
      Width = 37
      Height = 13
      Caption = 'Insumo:'
    end
    object Label4: TLabel
      Left = 12
      Top = 176
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label10: TLabel
      Left = 92
      Top = 176
      Width = 67
      Height = 13
      Caption = 'Custo unit'#225'rio:'
    end
    object Label6: TLabel
      Left = 168
      Top = 176
      Width = 53
      Height = 13
      Caption = 'Custo total:'
    end
    object LaCliente: TLabel
      Left = 12
      Top = 56
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object LaEmpresa: TLabel
      Left = 12
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object Label12: TLabel
      Left = 122
      Top = 136
      Width = 24
      Height = 13
      Caption = 'Lote:'
    end
    object Label1: TLabel
      Left = 12
      Top = 136
      Width = 76
      Height = 13
      Caption = 'S'#233'rie e NF (VP):'
    end
    object Label3: TLabel
      Left = 264
      Top = 176
      Width = 94
      Height = 13
      Caption = 'Data de fabrica'#231#227'o:'
    end
    object Label5: TLabel
      Left = 368
      Top = 176
      Width = 84
      Height = 13
      Caption = 'Data de validade:'
    end
    object SpeedButton1: TSpeedButton
      Left = 444
      Top = 152
      Width = 23
      Height = 22
      Caption = '?'
      OnClick = SpeedButton1Click
    end
    object Label7: TLabel
      Left = 356
      Top = 16
      Width = 26
      Height = 13
      Caption = 'Data:'
      Enabled = False
    end
    object EdQtde: TdmkEdit
      Left = 12
      Top = 192
      Width = 78
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdQtdeExit
      OnRedefinido = EdQtdeRedefinido
    end
    object EdPreco: TdmkEdit
      Left = 92
      Top = 192
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdPrecoExit
    end
    object EdVTota: TdmkEdit
      Left = 168
      Top = 192
      Width = 89
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdVTotaExit
    end
    object EdInsumo: TdmkEditCB
      Left = 12
      Top = 112
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdInsumoChange
      OnEnter = EdInsumoEnter
      OnExit = EdInsumoExit
      OnRedefinido = EdInsumoRedefinido
      DBLookupComboBox = CBInsumo
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBInsumo: TdmkDBLookupComboBox
      Left = 68
      Top = 112
      Width = 400
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPQ
      TabOrder = 5
      dmkEditCB = EdInsumo
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCliente: TdmkEditCB
      Left = 12
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdClienteChange
      OnEnter = EdClienteEnter
      OnExit = EdClienteExit
      OnRedefinido = EdClienteRedefinido
      DBLookupComboBox = CBCliente
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 68
      Top = 72
      Width = 400
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCI
      TabOrder = 3
      dmkEditCB = EdCliente
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object CkContinuar: TCheckBox
      Left = 12
      Top = 216
      Width = 113
      Height = 17
      Caption = 'Continuar inserindo'
      Checked = True
      State = cbChecked
      TabOrder = 14
    end
    object EdEmpresa: TdmkEditCB
      Left = 12
      Top = 32
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Empresa'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdEmpresaRedefinido
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 68
      Top = 32
      Width = 281
      Height = 21
      DropDownRows = 2
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 1
      TabStop = False
      dmkEditCB = EdEmpresa
      QryCampo = 'Empresa'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdxLote: TdmkEdit
      Left = 121
      Top = 152
      Width = 316
      Height = 21
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object Edide_nNF: TdmkEdit
      Left = 52
      Top = 152
      Width = 66
      Height = 21
      Alignment = taRightJustify
      MaxLength = 9
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object Edide_serie: TdmkEdit
      Left = 12
      Top = 152
      Width = 37
      Height = 21
      Alignment = taRightJustify
      MaxLength = 9
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object TPdFab: TdmkEditDateTimePicker
      Left = 264
      Top = 192
      Width = 100
      Height = 21
      Date = 45219.000000000000000000
      Time = 0.383424131941865200
      TabOrder = 12
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object TPdVal: TdmkEditDateTimePicker
      Left = 368
      Top = 192
      Width = 100
      Height = 21
      Date = 45219.000000000000000000
      Time = 0.383424131941865200
      TabOrder = 13
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object TPDataX: TdmkEditDateTimePicker
      Left = 356
      Top = 32
      Width = 112
      Height = 21
      Date = 45233.000000000000000000
      Time = 0.748196608794387400
      Enabled = False
      TabOrder = 15
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 482
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 434
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 386
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 366
        Height = 32
        Caption = 'Ajuste a Positivo (Re-Entrada)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 366
        Height = 32
        Caption = 'Ajuste a Positivo (Re-Entrada)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 366
        Height = 32
        Caption = 'Ajuste a Positivo (Re-Entrada)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 288
    Width = 482
    Height = 85
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 478
      Height = 68
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label20: TLabel
        Left = 364
        Top = 24
        Width = 93
        Height = 13
        Caption = 'Saldo futuro quant.:'
        Visible = False
      end
      object Label19: TLabel
        Left = 228
        Top = 24
        Width = 76
        Height = 13
        Caption = 'Custo unit'#225'rio $:'
        FocusControl = DBEdit9
        Visible = False
      end
      object Label18: TLabel
        Left = 120
        Top = 24
        Width = 51
        Height = 13
        Caption = 'Estoque $:'
        FocusControl = DBEdit8
        Visible = False
      end
      object Label17: TLabel
        Left = 12
        Top = 24
        Width = 98
        Height = 13
        Caption = 'Estoque quantidade:'
        FocusControl = DBEdit7
        Visible = False
      end
      object Label2: TLabel
        Left = 92
        Top = 0
        Width = 381
        Height = 13
        Caption = 
          'Estoque oculto para n'#227'o causar confus'#227'o do inicial para o atual!' +
          '!!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 4227327
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object DBEdit9: TDBEdit
        Left = 228
        Top = 40
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'CUSTO'
        DataSource = DsSaldo
        TabOrder = 0
        Visible = False
      end
      object EdSaldoFut: TdmkEdit
        Left = 364
        Top = 40
        Width = 100
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 1
        Visible = False
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object DBEdit8: TDBEdit
        Left = 120
        Top = 40
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'Valor'
        DataSource = DsSaldo
        TabOrder = 2
        Visible = False
      end
      object DBEdit7: TDBEdit
        Left = 12
        Top = 40
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'Peso'
        DataSource = DsSaldo
        TabOrder = 3
        Visible = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 373
    Width = 482
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 478
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 334
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 280
    Top = 52
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 280
    Top = 100
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END Nome'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 348
    Top = 52
    object QrCINome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.artigosgrupos.Nome'
      Size = 32
    end
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.artigosgrupos.Codigo'
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 348
    Top = 104
  end
  object QrSaldo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO '
      'FROM pqcli'
      'WHERE CI=:P0'
      'AND PQ=:P1')
    Left = 212
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSaldoPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSaldoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSaldoCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
    object QrSaldoCI: TIntegerField
      FieldName = 'CI'
    end
    object QrSaldoPQ: TIntegerField
      FieldName = 'PQ'
    end
  end
  object DsSaldo: TDataSource
    DataSet = QrSaldo
    Left = 212
    Top = 100
  end
  object Qry: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 408
    Top = 52
  end
end
