object Dmod: TDmod
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 602
  Width = 1007
  PixelsPerInch = 96
  object QrFields: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SHOW FIELDS FROM :p0')
    Left = 428
    Top = 159
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrMaster: TMySQLQuery
    Database = MyDB
    AfterOpen = QrMasterAfterOpen
    SQL.Strings = (
      'SELECT cm.Dono, cm.Versao, ma.CNPJ, ma.SolicitaSenha, '
      'ma.Em, ma.UsaAccMngr'
      'FROM controle cm'
      'LEFT JOIN entidades te ON te.Codigo=cm.Dono'
      'LEFT JOIN master ma ON ma.CNPJ=te.CNPJ')
    Left = 12
    Top = 143
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
      Required = True
    end
    object QrMasterSolicitaSenha: TSmallintField
      FieldName = 'SolicitaSenha'
      Origin = 'master.SolicitaSenha'
    end
    object QrMasterUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
    end
    object QrMasterCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 30
    end
    object QrMasterEm: TWideStringField
      FieldName = 'Em'
      Size = 100
    end
  end
  object QrLivreY_D: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Controle Codigo FROM ???')
    Left = 320
    Top = 59
    object QrLivreY_DCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrRecCountX: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  ArtigosGrupos')
    Left = 392
    Top = 251
    object QrRecCountXRecord: TIntegerField
      FieldName = 'Record'
    end
  end
  object QrUser: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Login FROM senhas'
      'WHERE Numero=:Usuario')
    Left = 12
    Top = 95
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Usuario'
        ParamType = ptUnknown
      end>
    object QrUserLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'DNTEACH.senhas.Login'
      Size = 128
    end
  end
  object QrSelX: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Lk FROM ???')
    Left = 272
    Top = 55
    object QrSelXLk: TIntegerField
      FieldName = 'Lk'
    end
  end
  object QrSB: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 204
    Top = 7
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSBCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrSBNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.pq.Nome'
      Size = 41
    end
  end
  object DsSB: TDataSource
    DataSet = QrSB
    Left = 232
    Top = 7
  end
  object QrSB2: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 204
    Top = 51
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSB2Codigo: TFloatField
      FieldName = 'Codigo'
    end
    object QrSB2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 128
    end
  end
  object DsSB2: TDataSource
    DataSet = QrSB2
    Left = 232
    Top = 51
  end
  object QrSB3: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT fa.Data Codigo, ca.Nome Nome'
      'FROM faturas fa, Carteiras ca'
      'WHERE ca.Codigo=fa.Emissao'
      'AND fa.Emissao=15')
    Left = 204
    Top = 95
    object QrSB3Codigo: TDateField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.faturas.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSB3Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
  end
  object DsSB3: TDataSource
    DataSet = QrSB3
    Left = 232
    Top = 95
  end
  object QrDuplicIntX: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT NF INTEIRO1, Cliente INTEIRO2, Codigo CODIGO'
      'FROM pqpse'
      'WHERE NF=1'
      'AND Cliente=1')
    Left = 392
    Top = 159
    object QrDuplicIntXINTEIRO1: TIntegerField
      FieldName = 'INTEIRO1'
      Origin = 'DBMBWET.pqpse.NF'
    end
    object QrDuplicIntXINTEIRO2: TIntegerField
      FieldName = 'INTEIRO2'
      Origin = 'DBMBWET.pqpse.Cliente'
    end
    object QrDuplicIntXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pqpse.Codigo'
    end
  end
  object QrDuplicStrX: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Nome NOME, Codigo CODIGO, IDCodigo ANTERIOR'
      'FROM pq'
      'WHERE Nome=:Nome')
    Left = 364
    Top = 159
    ParamData = <
      item
        DataType = ftString
        Name = 'Nome'
        ParamType = ptUnknown
      end>
    object QrDuplicStrXNOME: TWideStringField
      FieldName = 'NOME'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
    object QrDuplicStrXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrDuplicStrXANTERIOR: TIntegerField
      FieldName = 'ANTERIOR'
      Origin = 'DBMBWET.pq.IDCodigo'
    end
  end
  object QrInsLogX: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'INSERT INTO logs SET'
      'Data=NOW(),'
      'Tipo=:P0,'
      'Usuario=:P1,'
      'ID=:P2')
    Left = 392
    Top = 203
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDelLogX: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'DELETE FROM logs'
      'WHERE Tipo=:P0'
      'AND Usuario=:P1'
      'AND ID=:P2')
    Left = 364
    Top = 203
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDataBalY: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT MAX(Data) Data FROM pesagem'
      'WHERE Tipo=4')
    Left = 272
    Top = 7
    object QrDataBalYData: TDateField
      FieldName = 'Data'
      Origin = 'DBMBWET.pesagem.Data'
    end
  end
  object QrSenha: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM senhas'
      'WHERE Login=:P0'
      'AND Senha=:P1')
    Left = 8
    Top = 239
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhaLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'DNTEACH.senhas.Login'
      Size = 128
    end
    object QrSenhaNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'DNTEACH.senhas.Numero'
    end
    object QrSenhaSenha: TWideStringField
      FieldName = 'Senha'
      Origin = 'DNTEACH.senhas.Senha'
      Size = 128
    end
    object QrSenhaPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'DNTEACH.senhas.Perfil'
    end
    object QrSenhaLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DNTEACH.senhas.Lk'
    end
    object QrSenhaFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
  end
  object QrMaster2: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM master')
    Left = 8
    Top = 195
    object QrMaster2Em: TWideStringField
      FieldName = 'Em'
      Required = True
      Size = 100
    end
    object QrMaster2CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 30
    end
    object QrMaster2Monitorar: TSmallintField
      FieldName = 'Monitorar'
      Required = True
    end
    object QrMaster2Distorcao: TIntegerField
      FieldName = 'Distorcao'
      Required = True
    end
    object QrMaster2DataI: TDateField
      FieldName = 'DataI'
    end
    object QrMaster2DataF: TDateField
      FieldName = 'DataF'
    end
    object QrMaster2Hoje: TDateField
      FieldName = 'Hoje'
    end
    object QrMaster2Hora: TTimeField
      FieldName = 'Hora'
    end
    object QrMaster2MasLogin: TWideStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrMaster2MasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrMaster2MasAtivar: TWideStringField
      FieldName = 'MasAtivar'
      Size = 1
    end
    object QrMaster2Limite: TSmallintField
      FieldName = 'Limite'
      Required = True
    end
    object QrMaster2Licenca: TWideStringField
      FieldName = 'Licenca'
      Size = 255
    end
  end
  object QrProduto: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Controla'
      'FROM produtos'
      'WHERE Codigo=:P0')
    Left = 132
    Top = 275
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdutoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutoControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
  end
  object QrVendas: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Custo) Valor'
      'FROM produtossits'
      'WHERE Produto=:P0')
    Left = 92
    Top = 287
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVendasPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'DNSTORE001.produtossits.Pecas'
    end
    object QrVendasValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNSTORE001.produtossits.Total'
    end
  end
  object QrEntrada: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Custo) Valor'
      'FROM produtoscits'
      'WHERE Produto=:P0'
      '')
    Left = 64
    Top = 287
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntradaPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'DNSTORE001.produtoscits.Pecas'
    end
    object QrEntradaValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNSTORE001.produtoscits.Total'
    end
  end
  object QrUpdU: TMySQLQuery
    Database = MyDB
    Left = 12
    Top = 47
  end
  object QrUpdY: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 64
    Top = 239
  end
  object QrLivreY: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo FROM livres')
    Left = 60
    Top = 191
    object QrLivreYCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrCountY: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  Entidades')
    Left = 464
    Top = 247
    object QrCountYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrLocY: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT MIN(Codigo) Record FROM carteiras'
      '')
    Left = 464
    Top = 203
    object QrLocYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrLocDataY: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT MIN(Data) Record FROM ???'
      '')
    Left = 320
    Top = 7
    object QrLocDataYRecord: TDateField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrIdx: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 120
    Top = 320
  end
  object QrMas: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 152
    Top = 320
  end
  object QrUpd: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 176
    Top = 320
  end
  object QrAux: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 176
    Top = 364
  end
  object QrSQL: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 120
    Top = 364
  end
  object QrSB4: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 200
    Top = 143
    object QrSB4Codigo: TWideStringField
      FieldName = 'Codigo'
      Size = 255
    end
    object QrSB4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsSB4: TDataSource
    DataSet = QrSB4
    Left = 232
    Top = 143
  end
  object QrPriorNext: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 148
    Top = 364
  end
  object QrSoma1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT '
      'SUM(TotalC*Tipo) TOTAL, '
      'SUM(Qtde*Tipo) Qtde '
      'FROM mov'
      'WHERE Produto=:P0')
    Left = 216
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSoma1TOTAL: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'TOTAL'
    end
    object QrSoma1Qtde: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Qtde'
    end
  end
  object QrControle: TMySQLQuery
    Database = MyDB
    AfterOpen = QrControleAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM controle')
    Left = 16
    Top = 547
    object QrControlePQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrControlePQE: TIntegerField
      FieldName = 'PQE'
    end
    object QrControleControlaNeg: TSmallintField
      FieldName = 'ControlaNeg'
    end
    object QrControleFamilias: TSmallintField
      FieldName = 'Familias'
    end
    object QrControleFamiliasIts: TSmallintField
      FieldName = 'FamiliasIts'
    end
    object QrControleAskNFOrca: TSmallintField
      FieldName = 'AskNFOrca'
    end
    object QrControlePreviewNF: TSmallintField
      FieldName = 'PreviewNF'
    end
    object QrControleOrcaRapido: TSmallintField
      FieldName = 'OrcaRapido'
    end
    object QrControleDistriDescoItens: TSmallintField
      FieldName = 'DistriDescoItens'
    end
    object QrControleEntraSemValor: TSmallintField
      FieldName = 'EntraSemValor'
    end
    object QrControleMensalSempre: TSmallintField
      FieldName = 'MensalSempre'
    end
    object QrControleBalType: TSmallintField
      FieldName = 'BalType'
    end
    object QrControleOrcaOrdem: TSmallintField
      FieldName = 'OrcaOrdem'
    end
    object QrControleOrcaLinhas: TSmallintField
      FieldName = 'OrcaLinhas'
    end
    object QrControleOrcaLFeed: TIntegerField
      FieldName = 'OrcaLFeed'
    end
    object QrControleOrcaModelo: TSmallintField
      FieldName = 'OrcaModelo'
    end
    object QrControleOrcaRodaPos: TSmallintField
      FieldName = 'OrcaRodaPos'
    end
    object QrControleOrcaRodape: TSmallintField
      FieldName = 'OrcaRodape'
    end
    object QrControleOrcaCabecalho: TSmallintField
      FieldName = 'OrcaCabecalho'
    end
    object QrControleCoresRel: TSmallintField
      FieldName = 'CoresRel'
    end
    object QrControleMoraDD: TFloatField
      FieldName = 'MoraDD'
    end
    object QrControleMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrControleAvisosCxaEdit: TSmallintField
      FieldName = 'AvisosCxaEdit'
    end
    object QrControleTravaCidade: TSmallintField
      FieldName = 'TravaCidade'
    end
    object QrControleChConfCab: TIntegerField
      FieldName = 'ChConfCab'
    end
    object QrControleImpDOS: TIntegerField
      FieldName = 'ImpDOS'
    end
    object QrControleUnidadePadrao: TIntegerField
      FieldName = 'UnidadePadrao'
    end
    object QrControleProdutosV: TIntegerField
      FieldName = 'ProdutosV'
    end
    object QrControleCartDespesas: TIntegerField
      FieldName = 'CartDespesas'
    end
    object QrControleReserva: TSmallintField
      FieldName = 'Reserva'
    end
    object QrControleUFPadrao: TIntegerField
      FieldName = 'UFPadrao'
    end
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
    end
    object QrControleErroHora: TIntegerField
      FieldName = 'ErroHora'
    end
    object QrControleSenhas: TIntegerField
      FieldName = 'Senhas'
    end
    object QrControleSalarios: TIntegerField
      FieldName = 'Salarios'
    end
    object QrControleEntidades: TIntegerField
      FieldName = 'Entidades'
    end
    object QrControleUFs: TIntegerField
      FieldName = 'UFs'
    end
    object QrControleListaECivil: TIntegerField
      FieldName = 'ListaECivil'
    end
    object QrControlePerfis: TIntegerField
      FieldName = 'Perfis'
    end
    object QrControleUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrControleContas: TIntegerField
      FieldName = 'Contas'
    end
    object QrControleCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrControleDepartamentos: TIntegerField
      FieldName = 'Departamentos'
    end
    object QrControleDividas: TIntegerField
      FieldName = 'Dividas'
    end
    object QrControleDividasIts: TIntegerField
      FieldName = 'DividasIts'
    end
    object QrControleDividasPgs: TIntegerField
      FieldName = 'DividasPgs'
    end
    object QrControleCarteiras: TIntegerField
      FieldName = 'Carteiras'
    end
    object QrControleCartas: TIntegerField
      FieldName = 'Cartas'
    end
    object QrControleConsignacao: TIntegerField
      FieldName = 'Consignacao'
    end
    object QrControleGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrControleSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
    end
    object QrControleConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrControleInflacao: TIntegerField
      FieldName = 'Inflacao'
    end
    object QrControlekm: TIntegerField
      FieldName = 'km'
    end
    object QrControlekmMedia: TIntegerField
      FieldName = 'kmMedia'
    end
    object QrControlekmIts: TIntegerField
      FieldName = 'kmIts'
    end
    object QrControleFatura: TIntegerField
      FieldName = 'Fatura'
    end
    object QrControleLanctos: TLargeintField
      FieldName = 'Lanctos'
    end
    object QrControleEntiGrupos: TIntegerField
      FieldName = 'EntiGrupos'
    end
    object QrControleAparencias: TIntegerField
      FieldName = 'Aparencias'
    end
    object QrControlePages: TIntegerField
      FieldName = 'Pages'
    end
    object QrControleMultiEtq: TIntegerField
      FieldName = 'MultiEtq'
    end
    object QrControleEntiTransp: TIntegerField
      FieldName = 'EntiTransp'
    end
    object QrControleMediaCH: TIntegerField
      FieldName = 'MediaCH'
    end
    object QrControleImprime: TIntegerField
      FieldName = 'Imprime'
    end
    object QrControleImprimeBand: TIntegerField
      FieldName = 'ImprimeBand'
    end
    object QrControleImprimeView: TIntegerField
      FieldName = 'ImprimeView'
    end
    object QrControlePaperLef: TIntegerField
      FieldName = 'PaperLef'
    end
    object QrControlePaperTop: TIntegerField
      FieldName = 'PaperTop'
    end
    object QrControlePaperHei: TIntegerField
      FieldName = 'PaperHei'
    end
    object QrControlePaperWid: TIntegerField
      FieldName = 'PaperWid'
    end
    object QrControlePaperFcl: TIntegerField
      FieldName = 'PaperFcl'
    end
    object QrControleCartVen: TIntegerField
      FieldName = 'CartVen'
    end
    object QrControleCartCom: TIntegerField
      FieldName = 'CartCom'
    end
    object QrControleCartDeS: TIntegerField
      FieldName = 'CartDeS'
    end
    object QrControleCartReS: TIntegerField
      FieldName = 'CartReS'
    end
    object QrControleCartDeG: TIntegerField
      FieldName = 'CartDeG'
    end
    object QrControleCartReG: TIntegerField
      FieldName = 'CartReG'
    end
    object QrControleCartCoE: TIntegerField
      FieldName = 'CartCoE'
    end
    object QrControleCartCoC: TIntegerField
      FieldName = 'CartCoC'
    end
    object QrControleCartEmD: TIntegerField
      FieldName = 'CartEmD'
    end
    object QrControleCartEmA: TIntegerField
      FieldName = 'CartEmA'
    end
    object QrControleMoedaVal: TFloatField
      FieldName = 'MoedaVal'
    end
    object QrControleTela1: TIntegerField
      FieldName = 'Tela1'
    end
    object QrControleChamarPgtoServ: TIntegerField
      FieldName = 'ChamarPgtoServ'
    end
    object QrControleFormUsaTam: TIntegerField
      FieldName = 'FormUsaTam'
    end
    object QrControleFormHeight: TIntegerField
      FieldName = 'FormHeight'
    end
    object QrControleFormWidth: TIntegerField
      FieldName = 'FormWidth'
    end
    object QrControleFormPixEsq: TIntegerField
      FieldName = 'FormPixEsq'
    end
    object QrControleFormPixDir: TIntegerField
      FieldName = 'FormPixDir'
    end
    object QrControleFormPixTop: TIntegerField
      FieldName = 'FormPixTop'
    end
    object QrControleFormPixBot: TIntegerField
      FieldName = 'FormPixBot'
    end
    object QrControleFormFoAlt: TIntegerField
      FieldName = 'FormFoAlt'
    end
    object QrControleFormFoPro: TFloatField
      FieldName = 'FormFoPro'
    end
    object QrControleFormUsaPro: TIntegerField
      FieldName = 'FormUsaPro'
    end
    object QrControleFormSlides: TIntegerField
      FieldName = 'FormSlides'
    end
    object QrControleFormNeg: TIntegerField
      FieldName = 'FormNeg'
    end
    object QrControleFormIta: TIntegerField
      FieldName = 'FormIta'
    end
    object QrControleFormSub: TIntegerField
      FieldName = 'FormSub'
    end
    object QrControleFormExt: TIntegerField
      FieldName = 'FormExt'
    end
    object QrControleFormFundoTipo: TIntegerField
      FieldName = 'FormFundoTipo'
    end
    object QrControleServInterv: TIntegerField
      FieldName = 'ServInterv'
    end
    object QrControleServAntecip: TIntegerField
      FieldName = 'ServAntecip'
    end
    object QrControleAdiLancto: TIntegerField
      FieldName = 'AdiLancto'
    end
    object QrControleContaSal: TIntegerField
      FieldName = 'ContaSal'
    end
    object QrControleContaVal: TIntegerField
      FieldName = 'ContaVal'
    end
    object QrControleNiver: TSmallintField
      FieldName = 'Niver'
    end
    object QrControleNiverddA: TSmallintField
      FieldName = 'NiverddA'
    end
    object QrControleNiverddD: TSmallintField
      FieldName = 'NiverddD'
    end
    object QrControleLastPassD: TDateTimeField
      FieldName = 'LastPassD'
    end
    object QrControleMultiPass: TIntegerField
      FieldName = 'MultiPass'
    end
    object QrControleMPV: TIntegerField
      FieldName = 'MPV'
    end
    object QrControleMPVIts: TIntegerField
      FieldName = 'MPVIts'
    end
    object QrControleMyPagTip: TSmallintField
      FieldName = 'MyPagTip'
    end
    object QrControleMyPagCar: TSmallintField
      FieldName = 'MyPagCar'
    end
    object QrControlePQEIts: TIntegerField
      FieldName = 'PQEIts'
    end
    object QrControlereg10: TSmallintField
      FieldName = 'reg10'
    end
    object QrControlereg11: TSmallintField
      FieldName = 'reg11'
    end
    object QrControlereg50: TSmallintField
      FieldName = 'reg50'
    end
    object QrControlereg51: TSmallintField
      FieldName = 'reg51'
    end
    object QrControlereg53: TSmallintField
      FieldName = 'reg53'
    end
    object QrControlereg54: TSmallintField
      FieldName = 'reg54'
    end
    object QrControlereg56: TSmallintField
      FieldName = 'reg56'
    end
    object QrControlereg60: TSmallintField
      FieldName = 'reg60'
    end
    object QrControlereg75: TSmallintField
      FieldName = 'reg75'
    end
    object QrControlereg88: TSmallintField
      FieldName = 'reg88'
    end
    object QrControlereg90: TSmallintField
      FieldName = 'reg90'
    end
    object QrControleNumSerieNF: TSmallintField
      FieldName = 'NumSerieNF'
    end
    object QrControleSerieNF: TIntegerField
      FieldName = 'SerieNF'
    end
    object QrControleModeloNF: TSmallintField
      FieldName = 'ModeloNF'
    end
    object QrControleDOSDotLinTop: TSmallintField
      FieldName = 'DOSDotLinTop'
      Required = True
    end
    object QrControleDOSDotLinBot: TSmallintField
      FieldName = 'DOSDotLinBot'
      Required = True
    end
    object QrControleCambiosData: TDateField
      FieldName = 'CambiosData'
    end
    object QrControleCambiosUsuario: TIntegerField
      FieldName = 'CambiosUsuario'
    end
    object QrControleFormulas: TIntegerField
      FieldName = 'Formulas'
      Required = True
    end
    object QrControleFormulasIts: TIntegerField
      FieldName = 'FormulasIts'
      Required = True
    end
    object QrControleDOSGrades: TSmallintField
      FieldName = 'DOSGrades'
      Required = True
    end
    object QrControleMPIn: TIntegerField
      FieldName = 'MPIn'
    end
    object QrControleCorRecibo: TIntegerField
      FieldName = 'CorRecibo'
    end
    object QrControleIdleMinutos: TIntegerField
      FieldName = 'IdleMinutos'
    end
    object QrControleCartaG: TIntegerField
      FieldName = 'CartaG'
    end
    object QrControleMyPgParc: TSmallintField
      FieldName = 'MyPgParc'
    end
    object QrControleMyPgQtdP: TSmallintField
      FieldName = 'MyPgQtdP'
    end
    object QrControleMyPgPeri: TSmallintField
      FieldName = 'MyPgPeri'
    end
    object QrControleMyPgDias: TSmallintField
      FieldName = 'MyPgDias'
    end
    object QrControleConfJanela: TIntegerField
      FieldName = 'ConfJanela'
    end
    object QrControleDataPesqAuto: TSmallintField
      FieldName = 'DataPesqAuto'
    end
    object QrControleAtividad: TSmallintField
      FieldName = 'Atividad'
    end
    object QrControleCidades: TSmallintField
      FieldName = 'Cidades'
    end
    object QrControlePaises: TSmallintField
      FieldName = 'Paises'
    end
    object QrControleFluxos: TIntegerField
      FieldName = 'Fluxos'
    end
    object QrControleFluxosIts: TIntegerField
      FieldName = 'FluxosIts'
    end
    object QrControleOperacoes: TIntegerField
      FieldName = 'Operacoes'
    end
    object QrControlePallets: TIntegerField
      FieldName = 'Pallets'
    end
    object QrControlePalletsIts: TIntegerField
      FieldName = 'PalletsIts'
    end
    object QrControleMPClas: TIntegerField
      FieldName = 'MPClas'
    end
    object QrControleMPGrup: TIntegerField
      FieldName = 'MPGrup'
    end
    object QrControleMPArti: TIntegerField
      FieldName = 'MPArti'
    end
    object QrControleMPInDefeitos: TIntegerField
      FieldName = 'MPInDefeitos'
    end
    object QrControleDefeitosloc: TIntegerField
      FieldName = 'Defeitosloc'
      Required = True
    end
    object QrControleDefeitostip: TIntegerField
      FieldName = 'Defeitostip'
      Required = True
    end
    object QrControleDefPecas: TIntegerField
      FieldName = 'DefPecas'
      Required = True
    end
    object QrControleBalancosIts: TIntegerField
      FieldName = 'BalancosIts'
      Required = True
    end
    object QrControleEmbalagens: TIntegerField
      FieldName = 'Embalagens'
    end
    object QrControleEmit: TIntegerField
      FieldName = 'Emit'
    end
    object QrControleEmitCus: TIntegerField
      FieldName = 'EmitCus'
      Required = True
    end
    object QrControleEmitIts: TIntegerField
      FieldName = 'EmitIts'
    end
    object QrControleEquipes: TIntegerField
      FieldName = 'Equipes'
    end
    object QrControleEspessuras: TIntegerField
      FieldName = 'Espessuras'
    end
    object QrControleGruposQuimicos: TIntegerField
      FieldName = 'GruposQuimicos'
    end
    object QrControleListaSetores: TIntegerField
      FieldName = 'ListaSetores'
    end
    object QrControlePQFor: TIntegerField
      FieldName = 'PQFor'
    end
    object QrControlePQCot: TIntegerField
      FieldName = 'PQCot'
    end
    object QrControlePQCli: TIntegerField
      FieldName = 'PQCli'
    end
    object QrControlePQx: TIntegerField
      FieldName = 'PQx'
    end
    object QrControlePQG: TIntegerField
      FieldName = 'PQG'
    end
    object QrControlePQO: TIntegerField
      FieldName = 'PQO'
    end
    object QrControlePQT: TIntegerField
      FieldName = 'PQT'
    end
    object QrControleLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrControleDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrControleDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrControleUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrControleUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrControlePlano: TIntegerField
      FieldName = 'Plano'
    end
    object QrControleArtigosGrupos: TIntegerField
      FieldName = 'ArtigosGrupos'
    end
    object QrControleVendaCartPg: TIntegerField
      FieldName = 'VendaCartPg'
    end
    object QrControleVendaParcPg: TIntegerField
      FieldName = 'VendaParcPg'
    end
    object QrControleVendaPeriPg: TIntegerField
      FieldName = 'VendaPeriPg'
    end
    object QrControleVendaDiasPg: TIntegerField
      FieldName = 'VendaDiasPg'
    end
    object QrControleChequez: TSmallintField
      FieldName = 'Chequez'
    end
    object QrControlePQPed: TIntegerField
      FieldName = 'PQPed'
    end
    object QrControlePQPedIts: TIntegerField
      FieldName = 'PQPedIts'
    end
    object QrControleProLaINSSr: TFloatField
      FieldName = 'ProLaINSSr'
    end
    object QrControleProLaINSSp: TFloatField
      FieldName = 'ProLaINSSp'
    end
    object QrControleVerSalTabs: TIntegerField
      FieldName = 'VerSalTabs'
    end
    object QrControleVerBcoTabs: TIntegerField
      FieldName = 'VerBcoTabs'
    end
    object QrControleCNABCtaTar: TIntegerField
      FieldName = 'CNABCtaTar'
    end
    object QrControleCNABCtaJur: TIntegerField
      FieldName = 'CNABCtaJur'
    end
    object QrControleCNABCtaMul: TIntegerField
      FieldName = 'CNABCtaMul'
    end
    object QrControleCNAB_CaD: TIntegerField
      FieldName = 'CNAB_CaD'
    end
    object QrControleCNAB_CaG: TIntegerField
      FieldName = 'CNAB_CaG'
    end
    object QrControleAtzCritic: TIntegerField
      FieldName = 'AtzCritic'
    end
    object QrControleContasTrf: TIntegerField
      FieldName = 'ContasTrf'
    end
    object QrControleSomaIts: TIntegerField
      FieldName = 'SomaIts'
    end
    object QrControleContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object QrControleExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrControleExcelGruImp: TIntegerField
      FieldName = 'ExcelGruImp'
    end
    object QrControleComProdPerc: TIntegerField
      FieldName = 'ComProdPerc'
    end
    object QrControleComProdEdit: TIntegerField
      FieldName = 'ComProdEdit'
    end
    object QrControleComServPerc: TIntegerField
      FieldName = 'ComServPerc'
    end
    object QrControleComServEdit: TIntegerField
      FieldName = 'ComServEdit'
    end
    object QrControlePQNegBaixa: TSmallintField
      FieldName = 'PQNegBaixa'
      Required = True
    end
    object QrControlePQNegEntra: TSmallintField
      FieldName = 'PQNegEntra'
      Required = True
    end
    object QrControleMPP: TIntegerField
      FieldName = 'MPP'
    end
    object QrControleMPPIts: TIntegerField
      FieldName = 'MPPIts'
    end
    object QrControleVerWeb: TIntegerField
      FieldName = 'VerWeb'
    end
    object QrControleAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrControleVendaCouro: TSmallintField
      FieldName = 'VendaCouro'
      Required = True
    end
    object QrControlePortSerBal: TIntegerField
      FieldName = 'PortSerBal'
      Required = True
    end
    object QrControlePortSerLei: TIntegerField
      FieldName = 'PortSerLei'
      Required = True
    end
    object QrControlePortSerIts: TIntegerField
      FieldName = 'PortSerIts'
      Required = True
    end
    object QrControleCtaPQCompr: TIntegerField
      FieldName = 'CtaPQCompr'
    end
    object QrControleCtaPQFrete: TIntegerField
      FieldName = 'CtaPQFrete'
    end
    object QrControleCtaMPCompr: TIntegerField
      FieldName = 'CtaMPCompr'
    end
    object QrControleCtaMPFrete: TIntegerField
      FieldName = 'CtaMPFrete'
    end
    object QrControleCtaCVVenda: TIntegerField
      FieldName = 'CtaCVVenda'
    end
    object QrControleCtaCVFrete: TIntegerField
      FieldName = 'CtaCVFrete'
    end
    object QrControleImpRecRib: TSmallintField
      FieldName = 'ImpRecRib'
      Required = True
    end
    object QrControlePercula: TSmallintField
      FieldName = 'Percula'
      Required = True
    end
    object QrControleMPV2: TIntegerField
      FieldName = 'MPV2'
    end
    object QrControleMPV2Its: TIntegerField
      FieldName = 'MPV2Its'
    end
    object QrControleMPV2Bxa: TIntegerField
      FieldName = 'MPV2Bxa'
    end
    object QrControleMPEstagios: TIntegerField
      FieldName = 'MPEstagios'
    end
    object QrControleMPInIts: TIntegerField
      FieldName = 'MPInIts'
    end
    object QrControleCNAB_Rem: TIntegerField
      FieldName = 'CNAB_Rem'
    end
    object QrControleCNAB_Rem_I: TIntegerField
      FieldName = 'CNAB_Rem_I'
    end
    object QrControlePreEmMsgIm: TIntegerField
      FieldName = 'PreEmMsgIm'
    end
    object QrControlePreEmMsg: TIntegerField
      FieldName = 'PreEmMsg'
    end
    object QrControlePreEmail: TIntegerField
      FieldName = 'PreEmail'
    end
    object QrControleContasMes: TIntegerField
      FieldName = 'ContasMes'
    end
    object QrControleMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrControleImpObs: TIntegerField
      FieldName = 'ImpObs'
    end
    object QrControleCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrControleAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrControleMyPerJuros: TFloatField
      FieldName = 'MyPerJuros'
    end
    object QrControleMyPerMulta: TFloatField
      FieldName = 'MyPerMulta'
    end
    object QrControleEquiGru: TIntegerField
      FieldName = 'EquiGru'
    end
    object QrControleEntiCtas: TIntegerField
      FieldName = 'EntiCtas'
    end
    object QrControleContVen: TIntegerField
      FieldName = 'ContVen'
    end
    object QrControleContCom: TIntegerField
      FieldName = 'ContCom'
    end
    object QrControleCECTInn: TIntegerField
      FieldName = 'CECTInn'
      Required = True
    end
    object QrControleCECTOut: TIntegerField
      FieldName = 'CECTOut'
      Required = True
    end
    object QrControleCECTOutIts: TIntegerField
      FieldName = 'CECTOutIts'
      Required = True
    end
    object QrControleEquiCom: TIntegerField
      FieldName = 'EquiCom'
    end
    object QrControleContasLnk: TIntegerField
      FieldName = 'ContasLnk'
    end
    object QrControleLastBco: TIntegerField
      FieldName = 'LastBco'
    end
    object QrControleTintasCab: TIntegerField
      FieldName = 'TintasCab'
      Required = True
    end
    object QrControleTintasTin: TIntegerField
      FieldName = 'TintasTin'
      Required = True
    end
    object QrControleTintasIts: TIntegerField
      FieldName = 'TintasIts'
      Required = True
    end
    object QrControleTintasFlu: TIntegerField
      FieldName = 'TintasFlu'
      Required = True
    end
    object QrControleSetorCal: TIntegerField
      FieldName = 'SetorCal'
      Required = True
    end
    object QrControleSetorCur: TIntegerField
      FieldName = 'SetorCur'
      Required = True
    end
    object QrControleMPInExp: TIntegerField
      FieldName = 'MPInExp'
    end
    object QrControleMPInPerdas: TIntegerField
      FieldName = 'MPInPerdas'
    end
    object QrControleRolPerdas: TIntegerField
      FieldName = 'RolPerdas'
    end
    object QrControleCambioCot: TIntegerField
      FieldName = 'CambioCot'
    end
    object QrControleCambioMda: TIntegerField
      FieldName = 'CambioMda'
    end
    object QrControleMoedaBr: TIntegerField
      FieldName = 'MoedaBr'
    end
    object QrControleSenhasIts: TIntegerField
      FieldName = 'SenhasIts'
    end
    object QrControleCanAltBalA: TSmallintField
      FieldName = 'CanAltBalA'
      Required = True
    end
    object QrControleContasU: TIntegerField
      FieldName = 'ContasU'
    end
    object QrControleEntDefAtr: TIntegerField
      FieldName = 'EntDefAtr'
    end
    object QrControleEntAtrCad: TIntegerField
      FieldName = 'EntAtrCad'
    end
    object QrControleEntAtrIts: TIntegerField
      FieldName = 'EntAtrIts'
    end
    object QrControleBalTopoNom: TIntegerField
      FieldName = 'BalTopoNom'
    end
    object QrControleBalTopoTit: TIntegerField
      FieldName = 'BalTopoTit'
    end
    object QrControleBalTopoPer: TIntegerField
      FieldName = 'BalTopoPer'
    end
    object QrControleCasasProd: TSmallintField
      FieldName = 'CasasProd'
    end
    object QrControleNCMs: TIntegerField
      FieldName = 'NCMs'
    end
    object QrControleParamsNFs: TIntegerField
      FieldName = 'ParamsNFs'
    end
    object QrControleCarteirasU: TIntegerField
      FieldName = 'CarteirasU'
    end
    object QrControleLctoEndoss: TIntegerField
      FieldName = 'LctoEndoss'
    end
    object QrControleEntiContat: TIntegerField
      FieldName = 'EntiContat'
    end
    object QrControleEntiCargos: TIntegerField
      FieldName = 'EntiCargos'
    end
    object QrControleEntiMail: TIntegerField
      FieldName = 'EntiMail'
    end
    object QrControleEntiTel: TIntegerField
      FieldName = 'EntiTel'
    end
    object QrControleEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
    end
    object QrControleEntiRespon: TIntegerField
      FieldName = 'EntiRespon'
    end
    object QrControleEntiCfgRel: TIntegerField
      FieldName = 'EntiCfgRel'
    end
    object QrControleNFeMPInn: TIntegerField
      FieldName = 'NFeMPInn'
      Required = True
    end
    object QrControleNFeMPIts: TIntegerField
      FieldName = 'NFeMPIts'
      Required = True
    end
    object QrControleCMPTInn: TIntegerField
      FieldName = 'CMPTInn'
      Required = True
    end
    object QrControleCMPTOut: TIntegerField
      FieldName = 'CMPTOut'
      Required = True
    end
    object QrControleCMPTOutIts: TIntegerField
      FieldName = 'CMPTOutIts'
      Required = True
    end
    object QrControlePQRet: TIntegerField
      FieldName = 'PQRet'
    end
    object QrControlePQRetIts: TIntegerField
      FieldName = 'PQRetIts'
    end
    object QrControlePQ_StqCenCad: TIntegerField
      FieldName = 'PQ_StqCenCad'
      Required = True
    end
    object QrControleMP_StqCenCad: TIntegerField
      FieldName = 'MP_StqCenCad'
      Required = True
    end
    object QrControleCustomFR3: TIntegerField
      FieldName = 'CustomFR3'
    end
    object QrControleContasLnkIts: TIntegerField
      FieldName = 'ContasLnkIts'
    end
    object QrControleEventosCad: TSmallintField
      FieldName = 'EventosCad'
    end
    object QrControleGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object QrControleGraGru2: TIntegerField
      FieldName = 'GraGru2'
      Required = True
    end
    object QrControleGraGru3: TIntegerField
      FieldName = 'GraGru3'
      Required = True
    end
    object QrControleGraGruAtr: TIntegerField
      FieldName = 'GraGruAtr'
      Required = True
    end
    object QrControleGraGruC: TIntegerField
      FieldName = 'GraGruC'
      Required = True
    end
    object QrControleGraGruVal: TIntegerField
      FieldName = 'GraGruVal'
      Required = True
    end
    object QrControleGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrControleGraMatIts: TIntegerField
      FieldName = 'GraMatIts'
      Required = True
    end
    object QrControleGraSrv1: TIntegerField
      FieldName = 'GraSrv1'
      Required = True
    end
    object QrControleGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Required = True
    end
    object QrControleGraTamIts: TIntegerField
      FieldName = 'GraTamIts'
      Required = True
    end
    object QrControleEstqCen: TIntegerField
      FieldName = 'EstqCen'
      Required = True
    end
    object QrControleEtqGeraLot: TIntegerField
      FieldName = 'EtqGeraLot'
      Required = True
    end
    object QrControleEtqPrinCad: TIntegerField
      FieldName = 'EtqPrinCad'
      Required = True
    end
    object QrControleEtqPrinCmd: TIntegerField
      FieldName = 'EtqPrinCmd'
      Required = True
    end
    object QrControleFatPedCab: TIntegerField
      FieldName = 'FatPedCab'
      Required = True
    end
    object QrControleFatPedVol: TIntegerField
      FieldName = 'FatPedVol'
      Required = True
    end
    object QrControleFisAgrCad: TIntegerField
      FieldName = 'FisAgrCad'
      Required = True
    end
    object QrControleFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
      Required = True
    end
    object QrControleFisRegMvt: TIntegerField
      FieldName = 'FisRegMvt'
      Required = True
    end
    object QrControleGeosite: TIntegerField
      FieldName = 'Geosite'
      Required = True
    end
    object QrControleGraAtrCad: TIntegerField
      FieldName = 'GraAtrCad'
      Required = True
    end
    object QrControleGraAtrIts: TIntegerField
      FieldName = 'GraAtrIts'
      Required = True
    end
    object QrControleGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Required = True
    end
    object QrControleGraCorFam: TIntegerField
      FieldName = 'GraCorFam'
      Required = True
    end
    object QrControleGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
      Required = True
    end
    object QrControleGraCusPrcU: TIntegerField
      FieldName = 'GraCusPrcU'
      Required = True
    end
    object QrControleMedOrdem: TIntegerField
      FieldName = 'MedOrdem'
      Required = True
    end
    object QrControleMotivos: TIntegerField
      FieldName = 'Motivos'
      Required = True
    end
    object QrControlePediAccTab: TIntegerField
      FieldName = 'PediAccTab'
      Required = True
    end
    object QrControlePediVda: TIntegerField
      FieldName = 'PediVda'
      Required = True
    end
    object QrControlePediVdaIts: TIntegerField
      FieldName = 'PediVdaIts'
      Required = True
    end
    object QrControlePediVdaCuz: TIntegerField
      FieldName = 'PediVdaCuz'
      Required = True
    end
    object QrControlePediPrzCab: TIntegerField
      FieldName = 'PediPrzCab'
      Required = True
    end
    object QrControlePediPrzIts: TIntegerField
      FieldName = 'PediPrzIts'
      Required = True
    end
    object QrControlePediPrzEmp: TIntegerField
      FieldName = 'PediPrzEmp'
      Required = True
    end
    object QrControlePrdGrupAtr: TIntegerField
      FieldName = 'PrdGrupAtr'
      Required = True
    end
    object QrControlePrdGrupJan: TIntegerField
      FieldName = 'PrdGrupJan'
      Required = True
    end
    object QrControlePrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrControleRegioes: TIntegerField
      FieldName = 'Regioes'
      Required = True
    end
    object QrControleRegioesIts: TIntegerField
      FieldName = 'RegioesIts'
      Required = True
    end
    object QrControleFatConCad: TIntegerField
      FieldName = 'FatConCad'
      Required = True
    end
    object QrControleFatConIts: TIntegerField
      FieldName = 'FatConIts'
      Required = True
    end
    object QrControleFatConRet: TIntegerField
      FieldName = 'FatConRet'
      Required = True
    end
    object QrControleStqManCad: TIntegerField
      FieldName = 'StqManCad'
      Required = True
    end
    object QrControleStqManIts: TIntegerField
      FieldName = 'StqManIts'
      Required = True
    end
    object QrControleStqBalCad: TIntegerField
      FieldName = 'StqBalCad'
      Required = True
    end
    object QrControleStqBalIts: TIntegerField
      FieldName = 'StqBalIts'
      Required = True
    end
    object QrControleStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Required = True
    end
    object QrControleStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrControleStqMovItsA: TIntegerField
      FieldName = 'StqMovItsA'
      Required = True
    end
    object QrControleStqMovNFsA: TIntegerField
      FieldName = 'StqMovNFsA'
      Required = True
    end
    object QrControleStqMovValA: TIntegerField
      FieldName = 'StqMovValA'
      Required = True
    end
    object QrControleStqMovUsu: TIntegerField
      FieldName = 'StqMovUsu'
      Required = True
    end
    object QrControleStqMov001: TIntegerField
      FieldName = 'StqMov001'
      Required = True
    end
    object QrControleStqMov002: TIntegerField
      FieldName = 'StqMov002'
      Required = True
    end
    object QrControleStqMov003: TIntegerField
      FieldName = 'StqMov003'
      Required = True
    end
    object QrControleStqMov004: TIntegerField
      FieldName = 'StqMov004'
      Required = True
    end
    object QrControleStqMov099: TIntegerField
      FieldName = 'StqMov099'
      Required = True
    end
    object QrControleStqMov102: TIntegerField
      FieldName = 'StqMov102'
      Required = True
    end
    object QrControleStqMov103: TIntegerField
      FieldName = 'StqMov103'
      Required = True
    end
    object QrControleTabePrcCab: TIntegerField
      FieldName = 'TabePrcCab'
      Required = True
    end
    object QrControleTabePrcGrI: TIntegerField
      FieldName = 'TabePrcGrI'
      Required = True
    end
    object QrControleTabePrcPzG: TIntegerField
      FieldName = 'TabePrcPzG'
      Required = True
    end
    object QrControleUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrControleAnotacoes: TIntegerField
      FieldName = 'Anotacoes'
    end
    object QrControleSMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
      Required = True
    end
    object QrControleIDCtrl_NFe: TIntegerField
      FieldName = 'IDCtrl_NFe'
      Required = True
    end
    object QrControleCMPPVai: TIntegerField
      FieldName = 'CMPPVai'
      Required = True
    end
    object QrControleCMPPVaiIts: TIntegerField
      FieldName = 'CMPPVaiIts'
      Required = True
    end
    object QrControleCMPPVem: TIntegerField
      FieldName = 'CMPPVem'
      Required = True
    end
    object QrControleStqInnCad: TIntegerField
      FieldName = 'StqInnCad'
      Required = True
    end
    object QrControleImprimeEmpr: TIntegerField
      FieldName = 'ImprimeEmpr'
    end
    object QrControleGrupoBal: TIntegerField
      FieldName = 'GrupoBal'
      Required = True
    end
    object QrControleGraGruCST: TIntegerField
      FieldName = 'GraGruCST'
      Required = True
    end
    object QrControleFatVenCab: TIntegerField
      FieldName = 'FatVenCab'
      Required = True
    end
    object QrControleFatVenVol: TIntegerField
      FieldName = 'FatVenVol'
      Required = True
    end
    object QrControleFatVenIts: TIntegerField
      FieldName = 'FatVenIts'
      Required = True
    end
    object QrControleFatVenCuz: TIntegerField
      FieldName = 'FatVenCuz'
      Required = True
    end
    object QrControleCorrGrad01: TSmallintField
      FieldName = 'CorrGrad01'
      Required = True
    end
    object QrControleCorrGrad02: TSmallintField
      FieldName = 'CorrGrad02'
      Required = True
    end
    object QrControleCartTalCH: TIntegerField
      FieldName = 'CartTalCH'
    end
    object QrControleFabricas: TIntegerField
      FieldName = 'Fabricas'
    end
    object QrControleClasFisc: TIntegerField
      FieldName = 'ClasFisc'
      Required = True
    end
    object QrControleMPSubProd: TIntegerField
      FieldName = 'MPSubProd'
    end
    object QrControleXcelCfgIts: TIntegerField
      FieldName = 'XcelCfgIts'
    end
    object QrControleXcelCfgCab: TIntegerField
      FieldName = 'XcelCfgCab'
    end
    object QrControlePlanRelCab: TIntegerField
      FieldName = 'PlanRelCab'
    end
    object QrControlePlanRelIts: TIntegerField
      FieldName = 'PlanRelIts'
    end
    object QrControleEntiSrvPro: TIntegerField
      FieldName = 'EntiSrvPro'
    end
    object QrControleGraFabCad: TIntegerField
      FieldName = 'GraFabCad'
      Required = True
    end
    object QrControleGraFabMar: TIntegerField
      FieldName = 'GraFabMar'
      Required = True
    end
    object QrControleGraGru4: TIntegerField
      FieldName = 'GraGru4'
      Required = True
    end
    object QrControleGraGru5: TIntegerField
      FieldName = 'GraGru5'
      Required = True
    end
    object QrControleEntiImgs: TIntegerField
      FieldName = 'EntiImgs'
    end
    object QrControleEmitFlu: TIntegerField
      FieldName = 'EmitFlu'
    end
    object QrControleEmailConta: TIntegerField
      FieldName = 'EmailConta'
    end
    object QrControleWBNivGer: TSmallintField
      FieldName = 'WBNivGer'
      Required = True
    end
    object QrControleOSImpInfWB: TSmallintField
      FieldName = 'OSImpInfWB'
      Required = True
    end
    object QrControleUsaBRLMedM2: TSmallintField
      FieldName = 'UsaBRLMedM2'
      Required = True
    end
    object QrControleUsaM2Medio: TSmallintField
      FieldName = 'UsaM2Medio'
      Required = True
    end
    object QrControleUsaEmitGru: TSmallintField
      FieldName = 'UsaEmitGru'
      Required = True
    end
    object QrControleCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
      Required = True
    end
    object QrControleCNAB_Lei: TIntegerField
      FieldName = 'CNAB_Lei'
      Required = True
    end
    object QrControleCNAB_Lot: TIntegerField
      FieldName = 'CNAB_Lot'
      Required = True
    end
    object QrControleAtzEntiContat: TSmallintField
      FieldName = 'AtzEntiContat'
    end
    object QrControleCambioMdaAtz: TSmallintField
      FieldName = 'CambioMdaAtz'
    end
    object QrControleMPVImpOpc: TIntegerField
      FieldName = 'MPVImpOpc'
      Required = True
    end
    object QrControleVerImprRecRib: TIntegerField
      FieldName = 'VerImprRecRib'
      Required = True
    end
    object QrControleCanAltBalVS: TSmallintField
      FieldName = 'CanAltBalVS'
      Required = True
    end
    object QrControleCorrigeFisRegMvt: TIntegerField
      FieldName = 'CorrigeFisRegMvt'
      Required = True
    end
    object QrControleInfoMulFrnImpVS: TSmallintField
      FieldName = 'InfoMulFrnImpVS'
      Required = True
    end
    object QrControleQtdBoxClas: TSmallintField
      FieldName = 'QtdBoxClas'
      Required = True
    end
    object QrControleVSPwdDdData: TDateField
      FieldName = 'VSPwdDdData'
      Required = True
    end
    object QrControleVSPwdDdUser: TIntegerField
      FieldName = 'VSPwdDdUser'
      Required = True
    end
    object QrControleCons: TIntegerField
      FieldName = 'Cons'
      Required = True
    end
    object QrControleConsPrc: TIntegerField
      FieldName = 'ConsPrc'
      Required = True
    end
    object QrControleLeiGer: TIntegerField
      FieldName = 'LeiGer'
    end
    object QrControleLeiGerIts: TIntegerField
      FieldName = 'LeiGerIts'
      Required = True
    end
    object QrControleFatPedFis: TIntegerField
      FieldName = 'FatPedFis'
      Required = True
    end
    object QrControleEntiAssina: TIntegerField
      FieldName = 'EntiAssina'
    end
    object QrControleEntiGruIts: TIntegerField
      FieldName = 'EntiGruIts'
    end
    object QrControleNObrigNFeVS: TIntegerField
      FieldName = 'NObrigNFeVS'
      Required = True
    end
    object QrControleVSWarSemNF: TSmallintField
      FieldName = 'VSWarSemNF'
      Required = True
    end
    object QrControleVSWarNoFrn: TSmallintField
      FieldName = 'VSWarNoFrn'
      Required = True
    end
    object QrControlePediPrzEnt: TIntegerField
      FieldName = 'PediPrzEnt'
      Required = True
    end
    object QrControleSPED_II_PDA: TSmallintField
      FieldName = 'SPED_II_PDA'
      Required = True
    end
    object QrControleSPED_II_EmCal: TSmallintField
      FieldName = 'SPED_II_EmCal'
      Required = True
    end
    object QrControleSPED_II_Caleado: TSmallintField
      FieldName = 'SPED_II_Caleado'
      Required = True
    end
    object QrControleSPED_II_DTA: TSmallintField
      FieldName = 'SPED_II_DTA'
      Required = True
    end
    object QrControleSPED_II_EmCur: TSmallintField
      FieldName = 'SPED_II_EmCur'
      Required = True
    end
    object QrControleSPED_II_Curtido: TSmallintField
      FieldName = 'SPED_II_Curtido'
      Required = True
    end
    object QrControleSPED_II_ArtCur: TSmallintField
      FieldName = 'SPED_II_ArtCur'
      Required = True
    end
    object QrControleSPED_II_Operacao: TSmallintField
      FieldName = 'SPED_II_Operacao'
      Required = True
    end
    object QrControleSPED_II_Recurt: TSmallintField
      FieldName = 'SPED_II_Recurt'
      Required = True
    end
    object QrControleSPED_II_SubProd: TSmallintField
      FieldName = 'SPED_II_SubProd'
      Required = True
    end
    object QrControleSPED_II_Reparo: TSmallintField
      FieldName = 'SPED_II_Reparo'
      Required = True
    end
    object QrControleVSInsPalManu: TSmallintField
      FieldName = 'VSInsPalManu'
      Required = True
    end
    object QrControleImpRecAcabto: TSmallintField
      FieldName = 'ImpRecAcabto'
      Required = True
    end
    object QrControleAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrControleAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrControleVSImpMediaPall: TSmallintField
      FieldName = 'VSImpMediaPall'
      Required = True
    end
    object QrControlePQD: TIntegerField
      FieldName = 'PQD'
    end
    object QrControleCFiscalPadr: TWideStringField
      FieldName = 'CFiscalPadr'
    end
    object QrControleSitTribPadr: TWideStringField
      FieldName = 'SitTribPadr'
    end
    object QrControleCFOPPadr: TWideStringField
      FieldName = 'CFOPPadr'
    end
    object QrControleCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 100
    end
    object QrControleCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrControleSoMaiusculas: TWideStringField
      FieldName = 'SoMaiusculas'
      Size = 1
    end
    object QrControleMoeda: TWideStringField
      FieldName = 'Moeda'
      Size = 4
    end
    object QrControleContraSenha: TWideStringField
      FieldName = 'ContraSenha'
      Size = 50
    end
    object QrControlePadrPlacaCar: TWideStringField
      FieldName = 'PadrPlacaCar'
      Size = 100
    end
    object QrControleServSMTP: TWideStringField
      FieldName = 'ServSMTP'
      Size = 50
    end
    object QrControleNomeMailOC: TWideStringField
      FieldName = 'NomeMailOC'
      Size = 50
    end
    object QrControleDonoMailOC: TWideStringField
      FieldName = 'DonoMailOC'
      Size = 50
    end
    object QrControleMailOC: TWideStringField
      FieldName = 'MailOC'
      Size = 80
    end
    object QrControleCorpoMailOC: TWideMemoField
      FieldName = 'CorpoMailOC'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrControleConexaoDialUp: TWideStringField
      FieldName = 'ConexaoDialUp'
      Size = 50
    end
    object QrControleMailCCCega: TWideStringField
      FieldName = 'MailCCCega'
      Size = 80
    end
    object QrControleFormFundoBMP: TWideStringField
      FieldName = 'FormFundoBMP'
      Size = 255
    end
    object QrControlePronomeE: TWideStringField
      FieldName = 'PronomeE'
    end
    object QrControlePronomeM: TWideStringField
      FieldName = 'PronomeM'
    end
    object QrControlePronomeF: TWideStringField
      FieldName = 'PronomeF'
    end
    object QrControlePronomeA: TWideStringField
      FieldName = 'PronomeA'
    end
    object QrControleSaudacaoE: TWideStringField
      FieldName = 'SaudacaoE'
      Size = 50
    end
    object QrControleSaudacaoM: TWideStringField
      FieldName = 'SaudacaoM'
      Size = 50
    end
    object QrControleSaudacaoF: TWideStringField
      FieldName = 'SaudacaoF'
      Size = 50
    end
    object QrControleSaudacaoA: TWideStringField
      FieldName = 'SaudacaoA'
      Size = 50
    end
    object QrControleMeuLogoPath: TWideStringField
      FieldName = 'MeuLogoPath'
      Size = 255
    end
    object QrControleLogoNF: TWideStringField
      FieldName = 'LogoNF'
      Size = 255
    end
    object QrControlePathLogoDupl: TWideStringField
      FieldName = 'PathLogoDupl'
      Size = 255
    end
    object QrControlePathLogoRece: TWideStringField
      FieldName = 'PathLogoRece'
      Size = 255
    end
    object QrControleLogoBig1: TWideStringField
      FieldName = 'LogoBig1'
      Size = 255
    end
    object QrControleSecuritStr: TWideStringField
      FieldName = 'SecuritStr'
      Size = 32
    end
    object QrControleVSPwdDdClas: TWideStringField
      FieldName = 'VSPwdDdClas'
      Required = True
      Size = 30
    end
    object QrControleVSImpRandStr: TWideStringField
      FieldName = 'VSImpRandStr'
      Size = 255
    end
    object QrControleDBClarecoCDR: TWideStringField
      FieldName = 'DBClarecoCDR'
      Size = 255
    end
    object QrControleM2DiaAcabPCP: TFloatField
      FieldName = 'M2DiaAcabPCP'
    end
    object QrControleCtaComisCMP: TIntegerField
      FieldName = 'CtaComisCMP'
    end
    object QrControleNaoRecupImposPQ: TSmallintField
      FieldName = 'NaoRecupImposPQ'
    end
    object QrControleCreTrib_MP_CFOP_DE: TIntegerField
      FieldName = 'CreTrib_MP_CFOP_DE'
      Required = True
    end
    object QrControleCreTrib_MP_CFOP_FE: TIntegerField
      FieldName = 'CreTrib_MP_CFOP_FE'
      Required = True
    end
    object QrControleCreTrib_MP_ICMS_CST: TWideStringField
      FieldName = 'CreTrib_MP_ICMS_CST'
      Required = True
      Size = 3
    end
    object QrControleCreTrib_MP_IPI_CST: TWideStringField
      FieldName = 'CreTrib_MP_IPI_CST'
      Required = True
      Size = 2
    end
    object QrControleCreTrib_MP_PIS_CST: TWideStringField
      FieldName = 'CreTrib_MP_PIS_CST'
      Required = True
      Size = 2
    end
    object QrControleCreTrib_MP_COFINS_CST: TWideStringField
      FieldName = 'CreTrib_MP_COFINS_CST'
      Required = True
      Size = 2
    end
    object QrControleCreTrib_MP_ICMS_ALIQ: TFloatField
      FieldName = 'CreTrib_MP_ICMS_ALIQ'
      Required = True
    end
    object QrControleCreTrib_MP_ICMS_ALIQ_ST: TFloatField
      FieldName = 'CreTrib_MP_ICMS_ALIQ_ST'
      Required = True
    end
    object QrControleCreTrib_MP_IPI_ALIQ: TFloatField
      FieldName = 'CreTrib_MP_IPI_ALIQ'
      Required = True
    end
    object QrControleCreTrib_MP_PIS_ALIQ: TFloatField
      FieldName = 'CreTrib_MP_PIS_ALIQ'
      Required = True
    end
    object QrControleCreTrib_MP_COFINS_ALIQ: TFloatField
      FieldName = 'CreTrib_MP_COFINS_ALIQ'
      Required = True
    end
    object QrControleRemTrib_MP_CFOP_DE: TIntegerField
      FieldName = 'RemTrib_MP_CFOP_DE'
      Required = True
    end
    object QrControleRemTrib_MP_CFOP_FE: TIntegerField
      FieldName = 'RemTrib_MP_CFOP_FE'
      Required = True
    end
    object QrControleRemTrib_MP_ICMS_CST: TWideStringField
      FieldName = 'RemTrib_MP_ICMS_CST'
      Required = True
      Size = 3
    end
    object QrControleRemTrib_MP_IPI_CST: TWideStringField
      FieldName = 'RemTrib_MP_IPI_CST'
      Required = True
      Size = 2
    end
    object QrControleRemTrib_MP_PIS_CST: TWideStringField
      FieldName = 'RemTrib_MP_PIS_CST'
      Required = True
      Size = 2
    end
    object QrControleRemTrib_MP_COFINS_CST: TWideStringField
      FieldName = 'RemTrib_MP_COFINS_CST'
      Required = True
      Size = 2
    end
    object QrControleRemTrib_MP_ICMS_ALIQ: TFloatField
      FieldName = 'RemTrib_MP_ICMS_ALIQ'
      Required = True
    end
    object QrControleRemTrib_MP_ICMS_ALIQ_ST: TFloatField
      FieldName = 'RemTrib_MP_ICMS_ALIQ_ST'
      Required = True
    end
    object QrControleRemTrib_MP_IPI_ALIQ: TFloatField
      FieldName = 'RemTrib_MP_IPI_ALIQ'
      Required = True
    end
    object QrControleRemTrib_MP_PIS_ALIQ: TFloatField
      FieldName = 'RemTrib_MP_PIS_ALIQ'
      Required = True
    end
    object QrControleRemTrib_MP_COFINS_ALIQ: TFloatField
      FieldName = 'RemTrib_MP_COFINS_ALIQ'
      Required = True
    end
    object QrControleCreTrib_MP_TES_ICMS: TSmallintField
      FieldName = 'CreTrib_MP_TES_ICMS'
      Required = True
    end
    object QrControleCreTrib_MP_TES_IPI: TSmallintField
      FieldName = 'CreTrib_MP_TES_IPI'
      Required = True
    end
    object QrControleCreTrib_MP_TES_PIS: TSmallintField
      FieldName = 'CreTrib_MP_TES_PIS'
      Required = True
    end
    object QrControleCreTrib_MP_TES_COFINS: TSmallintField
      FieldName = 'CreTrib_MP_TES_COFINS'
      Required = True
    end
    object QrControleStqCenCadEstqPall: TIntegerField
      FieldName = 'StqCenCadEstqPall'
    end
    object QrControleGruUsrEdtRecei: TSmallintField
      FieldName = 'GruUsrEdtRecei'
    end
  end
  object QrEstoque: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT pm.Tipo, SUM(pmi.Qtde) Qtde,'
      'SUM(pmi.ValorCus) ValorCus'
      'FROM produtosm pm, ProdutosMIts pmi'
      'WHERE pm.Codigo=pmi.Codigo'
      'AND pmi.Produto=:P0'
      'AND pm.DataM>=:P1'
      'GROUP BY pm.Tipo')
    Left = 428
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEstoqueTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstoqueValorCus: TFloatField
      FieldName = 'ValorCus'
    end
  end
  object QrPeriodoBal: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM balancos')
    Left = 344
    Top = 324
    object QrPeriodoBalPeriodo: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Periodo'
    end
  end
  object QrMin: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Min(Periodo) Periodo'
      'FROM balancos')
    Left = 372
    Top = 325
    object QrMinPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'DBMBWET.balancos.Periodo'
    end
  end
  object QrBalancosIts: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT '
      'SUM(EstqQ) EstqQ, SUM(EstqV) EstqV,'
      'SUM(EstqQ_G) EstqQ_G, SUM(EstqV_G) EstqV_G'
      'FROM balancosits'
      'WHERE Produto=:P0'
      'AND Periodo=:P1'
      '')
    Left = 400
    Top = 324
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBalancosItsEstqQ: TFloatField
      FieldName = 'EstqQ'
      Origin = 'DBMBWET.balancosits.EstqQ'
    end
    object QrBalancosItsEstqV: TFloatField
      FieldName = 'EstqV'
      Origin = 'DBMBWET.balancosits.EstqV'
    end
    object QrBalancosItsEstqQ_G: TFloatField
      FieldName = 'EstqQ_G'
    end
    object QrBalancosItsEstqV_G: TFloatField
      FieldName = 'EstqV_G'
    end
  end
  object QrEstqperiodo: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT pm.Tipo, SUM(pmi.Qtde) Qtde,'
      'SUM(pmi.ValorCus) ValorCus'
      'FROM produtosm pm, ProdutosMIts pmi'
      'WHERE pm.Codigo=pmi.Codigo'
      'AND pmi.Produto=:P0'
      'AND pm.DataM BETWEEN :P1 AND :P2'
      'GROUP BY pm.Tipo')
    Left = 456
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEstqperiodoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstqperiodoQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstqperiodoValorCus: TFloatField
      FieldName = 'ValorCus'
    end
  end
  object QrTerminal: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM terminais'
      'WHERE IP=:P0')
    Left = 292
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrNTV: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 60
    Top = 4
  end
  object QrNTI: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 64
    Top = 48
  end
  object ZZDB: TMySQLDatabase
    ConnectOptions = []
    ConnectionTimeout = 5
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=5')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 100
    Top = 3
  end
  object QrPerfis: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      '/*SELECT pip.Libera, pit.Janela '
      'FROM perfisits pit'
      'LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela '
      'WHERE pip.Codigo=:P0'
      '*/'
      'SELECT pip.Libera, pit.Janela '
      'FROM perfisits pit'
      
        'LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0'
      ''
      '')
    Left = 280
    Top = 292
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfisLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrTerceiro: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT ufp.Nome NOMEpUF, ufe.Nome NOMEeUF, en.* '
      'FROM entidades en'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'WHERE en.Codigo=:P0')
    Left = 196
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerceiroNOMEpUF: TWideStringField
      FieldName = 'NOMEpUF'
      Required = True
      Size = 2
    end
    object QrTerceiroNOMEeUF: TWideStringField
      FieldName = 'NOMEeUF'
      Required = True
      Size = 2
    end
    object QrTerceiroCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTerceiroRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrTerceiroFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrTerceiroPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrTerceiroMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrTerceiroCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTerceiroIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTerceiroIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrTerceiroNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrTerceiroApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrTerceiroCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTerceiroRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrTerceiroELograd: TSmallintField
      FieldName = 'ELograd'
      Required = True
    end
    object QrTerceiroERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrTerceiroENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrTerceiroECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrTerceiroEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrTerceiroECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrTerceiroEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrTerceiroECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrTerceiroEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrTerceiroETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrTerceiroEte2: TWideStringField
      FieldName = 'Ete2'
    end
    object QrTerceiroEte3: TWideStringField
      FieldName = 'Ete3'
    end
    object QrTerceiroECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrTerceiroEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrTerceiroEEmail: TWideStringField
      FieldName = 'EEmail'
      Size = 100
    end
    object QrTerceiroEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrTerceiroENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrTerceiroPLograd: TSmallintField
      FieldName = 'PLograd'
      Required = True
    end
    object QrTerceiroPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrTerceiroPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrTerceiroPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrTerceiroPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrTerceiroPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrTerceiroPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrTerceiroPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrTerceiroPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrTerceiroPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrTerceiroPte2: TWideStringField
      FieldName = 'Pte2'
    end
    object QrTerceiroPte3: TWideStringField
      FieldName = 'Pte3'
    end
    object QrTerceiroPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrTerceiroPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrTerceiroPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrTerceiroPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrTerceiroPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrTerceiroSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrTerceiroResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrTerceiroProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrTerceiroCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrTerceiroRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrTerceiroDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrTerceiroAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrTerceiroAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrTerceiroCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrTerceiroCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrTerceiroFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrTerceiroFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrTerceiroFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrTerceiroFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrTerceiroTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrTerceiroCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrTerceiroInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrTerceiroLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrTerceiroVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrTerceiroMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrTerceiroObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTerceiroTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTerceiroCLograd: TSmallintField
      FieldName = 'CLograd'
      Required = True
    end
    object QrTerceiroCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrTerceiroCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrTerceiroCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrTerceiroCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrTerceiroCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 25
    end
    object QrTerceiroCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrTerceiroCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrTerceiroCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrTerceiroCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrTerceiroCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrTerceiroCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrTerceiroCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrTerceiroLLograd: TSmallintField
      FieldName = 'LLograd'
      Required = True
    end
    object QrTerceiroLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrTerceiroLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrTerceiroLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrTerceiroLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrTerceiroLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrTerceiroLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrTerceiroLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrTerceiroLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrTerceiroLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrTerceiroLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrTerceiroLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrTerceiroLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrTerceiroComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrTerceiroSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrTerceiroNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrTerceiroGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrTerceiroAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrTerceiroLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrTerceiroConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrTerceiroConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
    end
    object QrTerceiroNome1: TWideStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrTerceiroNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrTerceiroNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrTerceiroNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrTerceiroNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrTerceiroNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrTerceiroNome4: TWideStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrTerceiroNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrTerceiroCreditosI: TIntegerField
      FieldName = 'CreditosI'
    end
    object QrTerceiroCreditosL: TIntegerField
      FieldName = 'CreditosL'
    end
    object QrTerceiroCreditosF2: TFloatField
      FieldName = 'CreditosF2'
    end
    object QrTerceiroCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrTerceiroCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrTerceiroCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrTerceiroMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrTerceiroQuantI1: TIntegerField
      FieldName = 'QuantI1'
    end
    object QrTerceiroQuantI2: TIntegerField
      FieldName = 'QuantI2'
    end
    object QrTerceiroQuantI3: TIntegerField
      FieldName = 'QuantI3'
    end
    object QrTerceiroQuantI4: TIntegerField
      FieldName = 'QuantI4'
    end
    object QrTerceiroQuantN1: TFloatField
      FieldName = 'QuantN1'
    end
    object QrTerceiroQuantN2: TFloatField
      FieldName = 'QuantN2'
    end
    object QrTerceiroAgenda: TWideStringField
      FieldName = 'Agenda'
      Required = True
      Size = 1
    end
    object QrTerceiroSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Required = True
      Size = 1
    end
    object QrTerceiroSenha1: TWideStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrTerceiroLimiCred: TFloatField
      FieldName = 'LimiCred'
      Required = True
    end
    object QrTerceiroDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrTerceiroCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Required = True
    end
    object QrTerceiroTempD: TFloatField
      FieldName = 'TempD'
    end
    object QrTerceiroLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTerceiroDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTerceiroDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTerceiroUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTerceiroUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTerceiroCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Size = 18
    end
    object QrTerceiroSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrTerceiroCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrTerceiroUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Required = True
    end
  end
  object QrAgora: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 60
    Top = 95
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrTerminais: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT * FROM terminais')
    Left = 292
    Top = 228
    object QrTerminaisIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminaisTerminal: TIntegerField
      FieldName = 'Terminal'
    end
    object QrTerminaisLicenca: TWideStringField
      FieldName = 'Licenca'
      Size = 255
    end
  end
  object QrUserSets: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT * FROM usersets'
      'WHERE NomeForm=:P0'
      'AND Definicao=:P1')
    Left = 132
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrUserSetsDefinicao: TWideStringField
      FieldName = 'Definicao'
      Size = 255
    end
    object QrUserSetsAbilitado: TSmallintField
      FieldName = 'Abilitado'
    end
    object QrUserSetsVisivel: TSmallintField
      FieldName = 'Visivel'
    end
    object QrUserSetsNomeForm: TWideStringField
      FieldName = 'NomeForm'
      Size = 255
    end
  end
  object QrLct: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR'
      'FROM lanctos la'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'ORDER BY la.Data, la.Controle')
    Left = 568
    Top = 68
    object QrLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 25
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctCONTA: TIntegerField
      FieldName = 'CONTA'
    end
    object QrLctNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      FixedChar = True
      Size = 128
    end
    object QrLctNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 128
    end
    object QrLctNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 4
    end
    object QrLctNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 4
    end
    object QrLctNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 4
    end
    object QrLctNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLctUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLctCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLctQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 596
    Top = 68
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 596
    Top = 21
  end
  object QrCarteiras: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT DISTINCT ca.*, '
      'CASE WHEN ba.Codigo=0 THEN "" ELSE ba.Nome END NOMEDOBANCO, '
      
        'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END NOMEFOR' +
        'NECEI'
      'FROM carteiras ca'
      'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco'
      'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI'
      'WHERE ca.Codigo>0')
    Left = 568
    Top = 21
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'carteiras.Codigo'
      DisplayFormat = '000'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'carteiras.Tipo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'carteiras.Nome'
      Size = 128
    end
    object QrCarteirasInicial: TFloatField
      FieldName = 'Inicial'
      Origin = 'carteiras.Inicial'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'carteiras.Banco'
    end
    object QrCarteirasID: TWideStringField
      FieldName = 'ID'
      Origin = 'carteiras.ID'
      Size = 128
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'carteiras.Fatura'
      FixedChar = True
      Size = 128
    end
    object QrCarteirasID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Origin = 'carteiras.ID_Fat'
      Size = 128
    end
    object QrCarteirasSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'carteiras.Saldo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'carteiras.Lk'
    end
    object QrCarteirasNOMEDOBANCO: TWideStringField
      FieldName = 'NOMEDOBANCO'
      Origin = 'carteiras.Nome'
      FixedChar = True
      Size = 100
    end
    object QrCarteirasEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      Origin = 'carteiras.EmCaixa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasDIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIFERENCA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
      Origin = 'carteiras.Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrCarteirasTIPOPRAZO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOPRAZO'
      Size = 1
      Calculated = True
    end
    object QrCarteirasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'carteiras.DataCad'
    end
    object QrCarteirasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'carteiras.DataAlt'
    end
    object QrCarteirasUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'carteiras.UserCad'
    end
    object QrCarteirasUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'carteiras.UserAlt'
    end
    object QrCarteirasNOMEPAGREC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEPAGREC'
      Size = 50
      Calculated = True
    end
    object QrCarteirasPagRec: TSmallintField
      FieldName = 'PagRec'
      Origin = 'carteiras.PagRec'
    end
    object QrCarteirasDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
      Required = True
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
      Required = True
    end
    object QrCarteirasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 100
    end
    object QrCarteirasNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
      Required = True
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
      DisplayFormat = '000'
    end
    object QrCarteirasAgencia1: TIntegerField
      FieldName = 'Agencia1'
      DisplayFormat = '0000'
    end
    object QrCarteirasConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCarteirasCheque1: TIntegerField
      FieldName = 'Cheque1'
      DisplayFormat = '000000'
    end
    object QrCarteirasContato1: TWideStringField
      FieldName = 'Contato1'
      Required = True
      Size = 100
    end
  end
  object QrTransf: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      
        'SELECT Data, Tipo, Carteira, Controle, Genero, Debito, Credito, ' +
        'Documento FROM lanctos'
      'WHERE Controle=:P0'
      '')
    Left = 96
    Top = 140
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTransfData: TDateField
      FieldName = 'Data'
    end
    object QrTransfTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTransfCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrTransfControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTransfGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrTransfDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrTransfCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrTransfDocumento: TFloatField
      FieldName = 'Documento'
    end
  end
  object QrUpdW: TMySQLQuery
    Database = ZZDB
    Left = 492
    Top = 47
  end
  object QrSumPQ_H: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(Peso) Peso, SUM(Valor) Valor '
      'FROM pqx'
      'WHERE CliOrig=:P0'
      'AND Insumo=:P1'
      'AND DataX>=:P2'
      '')
    Left = 568
    Top = 116
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSumPQ_HPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSumPQ_HValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrLocPerf: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM perfisits pit'
      
        'LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0')
    Left = 284
    Top = 340
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object SmallintField1: TSmallintField
      FieldName = 'Libera'
    end
    object StringField1: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrUpdZ: TMySQLQuery
    Database = ZZDB
    Left = 404
    Top = 51
  end
  object QrErrQuit: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT lan.Controle Controle1, la2.Controle Controle2, '
      'la2.Data, lan.Sit, lan.Descricao'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN lanctos   la2 ON la2.ID_pgto=lan.Controle'
      ''
      'WHERE lan.Tipo=2'
      'AND lan.Sit=2'
      'AND lan.Credito-lan.Pago = 0'
      'AND lan.Controle<>la2.Controle'
      'AND lan.ID_Quit=0'
      '')
    Left = 568
    Top = 212
    object QrErrQuitControle1: TIntegerField
      FieldName = 'Controle1'
      Required = True
    end
    object QrErrQuitControle2: TIntegerField
      FieldName = 'Controle2'
      Required = True
    end
    object QrErrQuitData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrErrQuitSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrErrQuitDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object QrAPL: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito, SUM(Debito) Debito, '
      'SUM(MoraVal) MoraVal, SUM(MultaVal) MultaVal,'
      'MAX(Data) Data '
      'FROM lanctos WHERE ID_Pgto=:P0')
    Left = 552
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAPLCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrAPLDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrAPLData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrAPLMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrAPLMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
  end
  object QrVLA: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT Credito, Debito '
      'FROM lanctos '
      'WHERE Sub=0 '
      'AND Controle=:P0')
    Left = 584
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVLACredito: TFloatField
      FieldName = 'Credito'
    end
    object QrVLADebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrMPBxa: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(Peso) Peso, SUM(Pecas) Pecas, '
      'SUM(M2) M2, SUM(P2) P2 '
      'FROM mpv2bxa'
      'WHERE Marca_ID=:P0'
      '')
    Left = 560
    Top = 356
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPBxaPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrMPBxaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrMPBxaM2: TFloatField
      FieldName = 'M2'
    end
    object QrMPBxaP2: TFloatField
      FieldName = 'P2'
    end
  end
  object QrSumPQ_D: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(Peso) Peso, SUM(Valor) Valor '
      'FROM pqx'
      'WHERE CliOrig=:P0'
      'AND Insumo=:P1'
      'AND DataX BETWEEN :P2 AND :P3')
    Left = 568
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSumPQ_DPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSumPQ_DValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrBoss: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT MasSenha MasZero, '
      'AES_DECRYPT(MasSenha, :P0) MasSenha, MasLogin, Em, CNPJ'
      'FROM master')
    Left = 340
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBossMasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrBossMasLogin: TWideStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrBossEm: TWideStringField
      FieldName = 'Em'
      Size = 100
    end
    object QrBossCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 30
    end
    object QrBossMasZero: TWideStringField
      FieldName = 'MasZero'
      Size = 30
    end
  end
  object QrBSit: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM master')
    Left = 368
    Top = 376
    object QrBSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrSenhas: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT AES_DECRYPT(se.Senha, :P0) SenhaNew, se.Senha SenhaOld, '
      'se.login, se.Numero, se.Perfil, se.Funcionario, se.IP_Default'
      'FROM senhas se'
      'WHERE se.Login=:P1')
    Left = 396
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhasSenhaNew: TWideStringField
      FieldName = 'SenhaNew'
      Size = 30
    end
    object QrSenhasSenhaOld: TWideStringField
      FieldName = 'SenhaOld'
      Size = 30
    end
    object QrSenhaslogin: TWideStringField
      FieldName = 'login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
    end
    object QrSenhasFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrSenhasIP_Default: TWideStringField
      FieldName = 'IP_Default'
      Size = 50
    end
  end
  object QrSSit: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM senhas'
      'WHERE login=:P0'
      '')
    Left = 424
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrSCMPTOut: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(OutQtdVal) OutQtdVal,'
      'SUM(OutQtdkg) OutQtdkg, '
      'SUM(OutQtdPc) OutQtdPc,'
      'SUM(SrvValTot) SrvValTot'
      'FROM CMPToutits'
      'WHERE Codigo=:P0')
    Left = 688
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCMPTOutOutQtdVal: TFloatField
      FieldName = 'OutQtdVal'
    end
    object QrSCMPTOutOutQtdkg: TFloatField
      FieldName = 'OutQtdkg'
    end
    object QrSCMPTOutOutQtdPc: TFloatField
      FieldName = 'OutQtdPc'
    end
    object QrSCMPTOutOutQtdM2: TFloatField
      FieldName = 'OutQtdM2'
    end
    object QrSCMPTOutMOValor: TFloatField
      FieldName = 'MOValor'
    end
    object QrSCMPTOutMOPesoKg: TFloatField
      FieldName = 'MOPesoKg'
    end
  end
  object QrSCMPTInn: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT '
      
        'IF(SUM(its.OutQtdVal) > 0, inn.InnQtdVal - SUM(its.OutQtdVal), i' +
        'nn.InnQtdVal) Valr,'
      
        'IF(SUM(its.OutQtdkg)  > 0, inn.InnQtdkg  - SUM(its.OutQtdkg),  i' +
        'nn.InnQtdkg)  Peso,'
      
        'IF(SUM(its.OutQtdPc)  > 0, inn.InnQtdPc  - SUM(its.OutQtdPc),  i' +
        'nn.InnQtdPc)  Peca'
      'FROM CMPTinn inn '
      'LEFT JOIN CMPToutits its ON inn.Codigo=its.CMPTInn'
      'WHERE inn.Codigo=:P0'
      'GROUP BY inn.Codigo')
    Left = 692
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCMPTInnValr: TFloatField
      FieldName = 'Valr'
    end
    object QrSCMPTInnPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSCMPTInnPeca: TFloatField
      FieldName = 'Peca'
    end
    object QrSCMPTInnSdoFMOVal: TFloatField
      FieldName = 'SdoFMOVal'
    end
    object QrSCMPTInnSdoFMOKg: TFloatField
      FieldName = 'SdoFMOKg'
    end
  end
  object QrSumCus: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(Custo) Custo'
      'FROM emitcus'
      'WHERE MPIn=:P0')
    Left = 541
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumCusCusto: TFloatField
      FieldName = 'Custo'
    end
  end
  object QrSCus2: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT emc.MPIn, SUM(emc.Custo) Custo,'
      'SUM(emc.Pecas) Pecas, SUM(emc.Peso) Peso,'
      'MIN(emc.DataEmis) MinDta, MAX(emc.DataEmis) MaxDta,'
      'COUNT(emc.MPIn) Fuloes, rec.Setor'
      'FROM emitcus emc'
      'LEFT JOIN formulas rec ON rec.Numero=emc.Formula'
      'WHERE emc.MPIn=:P0'
      'GROUP BY emc.MPIn, rec.Setor')
    Left = 584
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCus2MPIn: TIntegerField
      FieldName = 'MPIn'
      Required = True
    end
    object QrSCus2Custo: TFloatField
      FieldName = 'Custo'
    end
    object QrSCus2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSCus2Peso: TFloatField
      FieldName = 'Peso'
    end
    object QrSCus2MinDta: TDateTimeField
      FieldName = 'MinDta'
      Required = True
    end
    object QrSCus2MaxDta: TDateTimeField
      FieldName = 'MaxDta'
      Required = True
    end
    object QrSCus2Fuloes: TLargeintField
      FieldName = 'Fuloes'
      Required = True
    end
    object QrSCus2Setor: TIntegerField
      FieldName = 'Setor'
    end
  end
  object QrSPerdas: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(mip.Pecas) Pecas'
      'FROM mpinperdas mip'
      'WHERE MPInCtrl=:P0')
    Left = 628
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSPerdasPecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrMPIn: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT ForcaEncer, PecasNeg, '
      'Pecas, M2, P2, PLE'
      'FROM mpin'
      'WHERE Controle=:P0')
    Left = 668
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPInForcaEncer: TSmallintField
      FieldName = 'ForcaEncer'
    end
    object QrMPInPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrMPInPecasNeg: TFloatField
      FieldName = 'PecasNeg'
    end
    object QrMPInM2: TFloatField
      FieldName = 'M2'
    end
    object QrMPInP2: TFloatField
      FieldName = 'P2'
    end
    object QrMPInPLE: TFloatField
      FieldName = 'PLE'
    end
  end
  object QrUsoPQ: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor '
      'FROM pqx'
      'WHERE Valor < 0 '
      'AND CliDest=:P0'
      'AND DataX BETWEEN :P1 AND :P2')
    Left = 772
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrUsoPQValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrPQCompr: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT cn.CtaPQCompr,'
      'co.Subgrupo, sg.Grupo, gr.Conjunto, '
      'co.Nome NOMECONTA, co.Nome2 NOMECONTA2,'
      'sg.Nome NOMESUBGRUPO, gr.Nome NOMEGRUPO, '
      'cj.Nome NOMECONJUNTO, cj.OrdemLista cjOrdemLista, '
      'gr.OrdemLista grOrdemLista, sg.OrdemLista sgOrdemLista, '
      'co.OrdemLista coOrdemLista '
      'FROM controle cn, Contas co, Subgrupos sg,'
      'Grupos gr, Conjuntos cj'
      'WHERE cn.CtaPQCompr>0'
      'AND co.Codigo=cn.CtaPQCompr'
      'AND sg.Codigo=co.Subgrupo'
      'AND gr.Codigo=sg.Grupo'
      'AND cj.Codigo=gr.Conjunto')
    Left = 772
    Top = 52
    object QrPQComprCtaPQCompr: TIntegerField
      FieldName = 'CtaPQCompr'
    end
    object QrPQComprSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrPQComprGrupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrPQComprConjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrPQComprNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Required = True
      Size = 50
    end
    object QrPQComprNOMECONTA2: TWideStringField
      FieldName = 'NOMECONTA2'
      Required = True
      Size = 50
    end
    object QrPQComprNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrPQComprNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrPQComprNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrPQComprcjOrdemLista: TIntegerField
      FieldName = 'cjOrdemLista'
      Required = True
    end
    object QrPQComprgrOrdemLista: TIntegerField
      FieldName = 'grOrdemLista'
      Required = True
    end
    object QrPQComprsgOrdemLista: TIntegerField
      FieldName = 'sgOrdemLista'
      Required = True
    end
    object QrPQComprcoOrdemLista: TIntegerField
      FieldName = 'coOrdemLista'
      Required = True
    end
  end
  object QrSumKgsRib: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(Peso) Peso'
      'FROM emitcus'
      'WHERE Codigo=:P0'
      '')
    Left = 772
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumKgsRibPeso: TFloatField
      FieldName = 'Peso'
    end
  end
  object QrSumCusRib1: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor'
      'FROM pqx'
      'WHERE Tipo=110'
      'AND OrigemCodi=:P0')
    Left = 772
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumCusRib1Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrGGX: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT ggx.Controle'
      'FROM pqx pqx'
      
        'LEFT JOIN gragrux ggx ON ggx.GraGru1=pqx.Insumo AND ggx.GraTamI=' +
        '1'
      '/*LEFT JOIN gragruc ggc ON ggx.GraGruC=ggc.Controle */'
      'WHERE pqx.Tipo< :P0'
      '/*AND ggc.GraCorCad=pqx.CliOrig */'
      'AND pqx.Insumo=:P1'
      'ORDER BY Controle')
    Left = 744
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGGXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPQG: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT DISTINCT caba.ide_mod, pqx.* '
      'FROM pqx pqx'
      'LEFT JOIN nfecaba caba'
      '  ON caba.FatNum=pqx.OrigemCodi'
      'WHERE pqx.StqMovIts=0'
      'ORDER BY DataX, Tipo')
    Left = 804
    Top = 336
    object QrPQGDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQGOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQGOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQGTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQGCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQGCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQGInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQGPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPQGValor: TFloatField
      FieldName = 'Valor'
    end
    object QrPQGAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQGAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQGRetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrPQGStqMovIts: TIntegerField
      FieldName = 'StqMovIts'
    end
    object QrPQGide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
  end
  object QrSMIA: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT IDCtrl, ParCodi'
      'FROM stqmovitsa'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1'
      'AND OriCtrl=:P2')
    Left = 800
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSMIAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrSMIAParCodi: TIntegerField
      FieldName = 'ParCodi'
    end
  end
  object QrMPI: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT mpi.* '
      'FROM mpin mpi'
      'WHERE mpi.StqMovIts=0'
      'ORDER BY mpi.Data, mpi.Controle')
    Left = 740
    Top = 332
    object QrMPICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMPIFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrMPIData: TDateField
      FieldName = 'Data'
    end
    object QrMPIClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrMPIProcedencia: TIntegerField
      FieldName = 'Procedencia'
    end
    object QrMPITransportadora: TIntegerField
      FieldName = 'Transportadora'
    end
    object QrMPIMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrMPIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrMPIPecasNF: TFloatField
      FieldName = 'PecasNF'
    end
    object QrMPIM2: TFloatField
      FieldName = 'M2'
    end
    object QrMPIP2: TFloatField
      FieldName = 'P2'
    end
    object QrMPIFimM2: TFloatField
      FieldName = 'FimM2'
    end
    object QrMPIFimP2: TFloatField
      FieldName = 'FimP2'
    end
    object QrMPIPNF: TFloatField
      FieldName = 'PNF'
    end
    object QrMPIPLE: TFloatField
      FieldName = 'PLE'
    end
    object QrMPIPDA: TFloatField
      FieldName = 'PDA'
    end
    object QrMPIRecorte_PDA: TFloatField
      FieldName = 'Recorte_PDA'
    end
    object QrMPIPTA: TFloatField
      FieldName = 'PTA'
    end
    object QrMPIRecorte_PTA: TFloatField
      FieldName = 'Recorte_PTA'
    end
    object QrMPIRaspa_PTA: TFloatField
      FieldName = 'Raspa_PTA'
    end
    object QrMPITipificacao: TIntegerField
      FieldName = 'Tipificacao'
    end
    object QrMPIAparasCabelo: TSmallintField
      FieldName = 'AparasCabelo'
    end
    object QrMPISeboPreDescarne: TSmallintField
      FieldName = 'SeboPreDescarne'
    end
    object QrMPICustoInfo: TFloatField
      FieldName = 'CustoInfo'
    end
    object QrMPIFreteInfo: TFloatField
      FieldName = 'FreteInfo'
    end
    object QrMPIValor: TFloatField
      FieldName = 'Valor'
    end
    object QrMPICustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrMPIFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrMPIAbateTipo: TIntegerField
      FieldName = 'AbateTipo'
    end
    object QrMPILote: TWideStringField
      FieldName = 'Lote'
      Size = 11
    end
    object QrMPIPecasOut: TFloatField
      FieldName = 'PecasOut'
    end
    object QrMPIPecasSal: TFloatField
      FieldName = 'PecasSal'
    end
    object QrMPICaminhoes: TIntegerField
      FieldName = 'Caminhoes'
    end
    object QrMPIQuemAssina: TIntegerField
      FieldName = 'QuemAssina'
    end
    object QrMPICorretor: TIntegerField
      FieldName = 'Corretor'
    end
    object QrMPIComisPer: TFloatField
      FieldName = 'ComisPer'
    end
    object QrMPIComisVal: TFloatField
      FieldName = 'ComisVal'
    end
    object QrMPIDescAdiant: TFloatField
      FieldName = 'DescAdiant'
    end
    object QrMPICondPagto: TWideStringField
      FieldName = 'CondPagto'
      Size = 50
    end
    object QrMPICondComis: TWideStringField
      FieldName = 'CondComis'
      Size = 50
    end
    object QrMPILocalEntrg: TWideStringField
      FieldName = 'LocalEntrg'
      Size = 50
    end
    object QrMPIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrMPIAnimal: TSmallintField
      FieldName = 'Animal'
    end
    object QrMPICMPValor: TFloatField
      FieldName = 'CMPValor'
    end
    object QrMPICMPFrete: TFloatField
      FieldName = 'CMPFrete'
    end
    object QrMPIQuebrVReal: TFloatField
      FieldName = 'QuebrVReal'
    end
    object QrMPIQuebrVCobr: TFloatField
      FieldName = 'QuebrVCobr'
    end
    object QrMPICMPDesQV: TFloatField
      FieldName = 'CMPDesQV'
    end
    object QrMPICMPPagar: TFloatField
      FieldName = 'CMPPagar'
    end
    object QrMPIPcBxa: TFloatField
      FieldName = 'PcBxa'
    end
    object QrMPIkgBxa: TFloatField
      FieldName = 'kgBxa'
    end
    object QrMPIM2Bxa: TFloatField
      FieldName = 'M2Bxa'
    end
    object QrMPIP2Bxa: TFloatField
      FieldName = 'P2Bxa'
    end
    object QrMPIPecasCal: TFloatField
      FieldName = 'PecasCal'
    end
    object QrMPIPecasCur: TFloatField
      FieldName = 'PecasCur'
    end
    object QrMPIPecasExp: TFloatField
      FieldName = 'PecasExp'
    end
    object QrMPIM2Exp: TFloatField
      FieldName = 'M2Exp'
    end
    object QrMPIP2Exp: TFloatField
      FieldName = 'P2Exp'
    end
    object QrMPIPecasNeg: TFloatField
      FieldName = 'PecasNeg'
    end
    object QrMPIPesoCal: TFloatField
      FieldName = 'PesoCal'
    end
    object QrMPIPesoCur: TFloatField
      FieldName = 'PesoCur'
    end
    object QrMPICusPQCal: TFloatField
      FieldName = 'CusPQCal'
    end
    object QrMPICusPQCur: TFloatField
      FieldName = 'CusPQCur'
    end
    object QrMPIFuloesCal: TIntegerField
      FieldName = 'FuloesCal'
    end
    object QrMPIFuloesCur: TIntegerField
      FieldName = 'FuloesCur'
    end
    object QrMPIMinDtaCal: TDateField
      FieldName = 'MinDtaCal'
    end
    object QrMPIMinDtaCur: TDateField
      FieldName = 'MinDtaCur'
    end
    object QrMPIMinDtaEnx: TDateField
      FieldName = 'MinDtaEnx'
    end
    object QrMPIMaxDtaCal: TDateField
      FieldName = 'MaxDtaCal'
    end
    object QrMPIMaxDtaCur: TDateField
      FieldName = 'MaxDtaCur'
    end
    object QrMPIMaxDtaEnx: TDateField
      FieldName = 'MaxDtaEnx'
    end
    object QrMPIForcaEncer: TSmallintField
      FieldName = 'ForcaEncer'
    end
    object QrMPIEncerrado: TSmallintField
      FieldName = 'Encerrado'
    end
    object QrMPIPS_ValUni: TFloatField
      FieldName = 'PS_ValUni'
    end
    object QrMPIPS_QtdTot: TFloatField
      FieldName = 'PS_QtdTot'
    end
    object QrMPIPS_ValTot: TFloatField
      FieldName = 'PS_ValTot'
    end
    object QrMPILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMPIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMPIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMPIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMPIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMPIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrMPIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMPIEmClassif: TSmallintField
      FieldName = 'EmClassif'
    end
    object QrMPIPesoExp: TFloatField
      FieldName = 'PesoExp'
    end
    object QrMPIEstq_Pecas: TFloatField
      FieldName = 'Estq_Pecas'
    end
    object QrMPIEstq_M2: TFloatField
      FieldName = 'Estq_M2'
    end
    object QrMPIEstq_P2: TFloatField
      FieldName = 'Estq_P2'
    end
    object QrMPIEstq_Peso: TFloatField
      FieldName = 'Estq_Peso'
    end
    object QrMPIStqMovIts: TIntegerField
      FieldName = 'StqMovIts'
    end
  end
  object QrCWB: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT DISTINCT mpi.Codigo, mpi.Controle, mpi.PLE,'
      'mpi.CMPValor, mpi.CMPFrete, mpi.CustoPQ,'
      'smia.IDCtrl, smia.DataHora, smia.OriCodi, '
      'smia.OriCnta, smia.OriPart, smia.Empresa, smia.Qtde, '
      'smia.Pecas, smia.Peso, smia.AreaM2, smia.AreaP2, '
      'smia.FatorClas, smia.DebCtrl'
      'FROM stqmovitsa smia'
      'LEFT JOIN mpin mpi ON mpi.Controle=smia.OriCodi'
      'WHERE Tipo=101'
      'AND DebCtrl=-1'
      'ORDER BY DataHora')
    Left = 744
    Top = 380
    object QrCWBCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCWBControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCWBIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCWBDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrCWBOriCodi: TIntegerField
      FieldName = 'OriCodi'
    end
    object QrCWBOriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrCWBOriPart: TIntegerField
      FieldName = 'OriPart'
    end
    object QrCWBEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCWBQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrCWBPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrCWBPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrCWBAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrCWBAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrCWBFatorClas: TFloatField
      FieldName = 'FatorClas'
    end
    object QrCWBDebCtrl: TIntegerField
      FieldName = 'DebCtrl'
    end
    object QrCWBPLE: TFloatField
      FieldName = 'PLE'
    end
    object QrCWBCMPValor: TFloatField
      FieldName = 'CMPValor'
    end
    object QrCWBCMPFrete: TFloatField
      FieldName = 'CMPFrete'
    end
    object QrCWBCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
  end
  object QrSMI: TMySQLQuery
    Database = ZZDB
    Left = 800
    Top = 380
    object QrSMIStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrSMIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSMIIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrSMIDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
  end
  object QrSMIAGGX: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT smia.IDCtrl'
      'FROM stqmovitsa smia'
      'LEFT JOIN pqx pqx ON pqx.StqMovIts=smia.ParCodi'
      'WHERE smia.ParTipo=1'
      'AND smia.ParCodi <> 0'
      'AND pqx.StqMovIts IS NULL')
    Left = 684
    Top = 288
    object QrSMIAGGXIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object QrPQ_GG1: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT pq.Codigo, pq.Ativo, pq.Nome NO_PQ, '
      'gg1.Nome NO_GG1, gg1.PrdGrupTip, gg1.Nivel2,'
      'gg1.Nivel1'
      'FROM pq'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pq.Codigo'
      'WHERE pq.Codigo> 0'
      'AND (pq.Ativo<>-gg1.Nivel2'
      'OR pq.Nome <> gg1.Nome'
      'OR gg1.PrdGrupTip<>-2)'
      '')
    Left = 744
    Top = 432
    object QrPQ_GG1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQ_GG1Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQ_GG1NO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrPQ_GG1NO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 30
    end
    object QrPQ_GG1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrPQ_GG1Nivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrPQ_GG1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
  end
  object QrPQCli: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT Controle, GraGru1'
      'FROM pqcli'
      'WHERE PQ=:P0'
      'AND CI=:P1')
    Left = 632
    Top = 116
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPQCliControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrInPQ: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT pqe.CI, pqi.TotalCusto, pqi.TotalPeso'
      'FROM pqeits pqi'
      'LEFT JOIN pqe pqe ON pqe.Codigo=pqi.Codigo'
      'WHERE pqi.Insumo=:P0'
      'GROUP BY pqe.CI'
      'ORDER BY Controle DESC')
    Left = 384
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrInPQTotalCusto: TFloatField
      FieldName = 'TotalCusto'
    end
    object QrInPQTotalPeso: TFloatField
      FieldName = 'TotalPeso'
    end
    object QrInPQCI: TIntegerField
      FieldName = 'CI'
    end
  end
  object QrNiv1: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT smia.GraGruX, GraTamI, GraGruC, SUM(smia.Qtde) QTDE '
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX '
      'WHERE smia.Ativo=1'
      'AND ggx.GraGru1=:P0'
      'GROUP BY smia.GraGruX')
    Left = 412
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNiv1GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNiv1GraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrNiv1GraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrNiv1QTDE: TFloatField
      FieldName = 'QTDE'
    end
  end
  object QrGraGruVal: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT Controle '
      'FROM gragruval'
      'WHERE GraGruX=:P0'
      'AND GraCusPrc=:P1'
      'AND Entidade=:P2')
    Left = 440
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGraGruValControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPQEMed: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT DISTINCT pqi.Insumo '
      'FROM PQEIts pqi')
    Left = 800
    Top = 432
    object QrPQEMedInsumo: TIntegerField
      FieldName = 'Insumo'
    end
  end
  object QrPQxCus: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT pqx.Valor, smia.IDCtrl'
      'FROM pqx pqx'
      'LEFT JOIN stqmovitsa smia ON smia.IDCtrl=pqx.StqMovIts'
      'WHERE smia.CustoAll <> pqx.Valor'
      'AND pqx.Peso > 0')
    Left = 684
    Top = 336
    object QrPQxCusIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object QrPQxCusValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrMPInCus: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT mpi.CMPValor, smia.IDCtrl'
      'FROM MPIn mpi'
      'LEFT JOIN stqmovitsa smia ON smia.IDCtrl=mpi.StqMovIts'
      'WHERE smia.CustoAll <> mpi.CMPValor'
      '')
    Left = 688
    Top = 384
    object QrMPInCusCMPValor: TFloatField
      FieldName = 'CMPValor'
    end
    object QrMPInCusIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
  end
  object QrMaxPerBal: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT GrupoBal'
      'FROM balancos'
      'WHERE Periodo=(SELECT Max(Periodo) FROM balancos)')
    Left = 340
    Top = 444
    object QrMaxPerBalGrupoBal: TIntegerField
      FieldName = 'GrupoBal'
    end
  end
  object QrSCECTOut: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(OutQtdVal) OutQtdVal,'
      'SUM(OutQtdkg) OutQtdkg, SUM(OutQtdPc) OutQtdPc'
      'FROM cectoutits'
      'WHERE Codigo=:P0')
    Left = 688
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCECTOutOutQtdVal: TFloatField
      FieldName = 'OutQtdVal'
    end
    object QrSCECTOutOutQtdkg: TFloatField
      FieldName = 'OutQtdkg'
    end
    object QrSCECTOutOutQtdPc: TFloatField
      FieldName = 'OutQtdPc'
    end
  end
  object QrSCECTInn: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT '
      
        'IF(SUM(its.OutQtdVal) > 0, inn.InnQtdVal - SUM(its.OutQtdVal), i' +
        'nn.InnQtdVal) Valr,'
      
        'IF(SUM(its.OutQtdkg)  > 0, inn.InnQtdkg  - SUM(its.OutQtdkg),  i' +
        'nn.InnQtdkg)  Peso,'
      
        'IF(SUM(its.OutQtdPc)  > 0, inn.InnQtdPc  - SUM(its.OutQtdPc),  i' +
        'nn.InnQtdPc)  Peca'
      'FROM cectinn inn '
      'LEFT JOIN cectoutits its ON inn.Codigo=its.CECTInn'
      'WHERE inn.Codigo=:P0'
      'GROUP BY inn.Codigo')
    Left = 692
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCECTInnValr: TFloatField
      FieldName = 'Valr'
    end
    object QrSCECTInnPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSCECTInnPeca: TFloatField
      FieldName = 'Peca'
    end
  end
  object QrGGX_MP: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT gg1.GerBxaEstq'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggx.Controle =:P0')
    Left = 624
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGGX_MPGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
  end
  object QrSMPIBxa: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT gg1.Nome NO_PRD, med.Sigla SIGLA,'
      'smia.* '
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE smia.Tipo=104'
      'AND smia.OriCodi=:P0')
    Left = 624
    Top = 387
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSMPIBxaNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 120
    end
    object QrSMPIBxaSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrSMPIBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrSMPIBxaIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrSMPIBxaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSMPIBxaQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSMPIBxaPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSMPIBxaPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object QrFormulas_EhGGX: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT EhGGX, COUNT(*) Itens'
      'FROM formulasits'
      'GROUP BY EhGGX'
      'ORDER BY EhGGX')
    Left = 132
    Top = 444
    object QrFormulas_EhGGXEhGGX: TSmallintField
      FieldName = 'EhGGX'
    end
    object QrFormulas_EhGGXItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrSumCusRib2: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(CustoAll) CustoAll'
      'FROM stqmovitsa'
      'WHERE Tipo IN (:P0, :P1)'
      'AND OriCodi=:P2')
    Left = 772
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSumCusRib2CustoAll: TFloatField
      FieldName = 'CustoAll'
    end
  end
  object QrOX1: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM gragrux'
      'WHERE GraGru1=:P0'
      'ORDER BY Controle')
    Left = 192
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOX1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrProds: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT ac.Produto1,ac.Produto2,'
      'ac.Produto3,ac.Produto4,ac.Produto5'
      'FROM itensrec ir'
      'LEFT JOIN analogoscups ac ON ac.Codigo=ir.Produto'
      'WHERE ir.Numero=:P0'
      'AND ac.Ordem=1')
    Left = 689
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdsProduto1: TIntegerField
      FieldName = 'Produto1'
    end
    object QrProdsProduto2: TIntegerField
      FieldName = 'Produto2'
    end
    object QrProdsProduto3: TIntegerField
      FieldName = 'Produto3'
    end
    object QrProdsProduto4: TIntegerField
      FieldName = 'Produto4'
    end
    object QrProdsProduto5: TIntegerField
      FieldName = 'Produto5'
    end
  end
  object QrLotes: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT ec.Controle, ec.MPIn, ec.Peso, ec.Pecas, '
      'mi.Ficha, mi.Marca, mi.Lote'
      'FROM emitcus ec'
      'LEFT JOIN mpin mi ON mi.Controle=ec.MPIn'
      'WHERE ec.Codigo=:P0')
    Left = 688
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotesControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLotesMPIn: TIntegerField
      FieldName = 'MPIn'
      Required = True
    end
    object QrLotesPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrLotesPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrLotesFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrLotesMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrLotesLote: TWideStringField
      FieldName = 'Lote'
      Size = 11
    end
  end
  object frxDsMaster: TfrxDBDataset
    UserName = 'frxDsMaster'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Dono=Dono'
      'Versao=Versao'
      'CNPJ=CNPJ'
      'SolicitaSenha=SolicitaSenha'
      'Em=Em'
      'UsaAccMngr=UsaAccMngr')
    DataSet = QrMaster
    BCDToCurrency = False
    DataSetOptions = []
    Left = 56
    Top = 144
  end
  object Qr3Bal: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor'
      'FROM pqx'
      'WHERE Tipo=0'
      'AND DataX BETWEEN :P0 AND :P1'
      'AND CliOrig=:P2'
      'GROUP BY Insumo'
      '')
    Left = 620
    Top = 496
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr3BalInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object Qr3BalPeso: TFloatField
      FieldName = 'Peso'
    end
    object Qr3BalValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object Qr3Inn: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor'
      'FROM pqx'
      'WHERE Tipo>0 AND Tipo<=100 '
      'AND DataX BETWEEN :P0 AND :P1'
      'AND CliOrig=:P2'
      'GROUP BY Insumo')
    Left = 648
    Top = 496
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr3InnInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object Qr3InnPeso: TFloatField
      FieldName = 'Peso'
    end
    object Qr3InnValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object Qr3Out: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor'
      'FROM pqx'
      'WHERE Tipo>100'
      'AND DataX BETWEEN :P0 AND :P1'
      'AND CliOrig=:P2'
      'GROUP BY Insumo'
      '')
    Left = 676
    Top = 496
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr3OutInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object Qr3OutPeso: TFloatField
      FieldName = 'Peso'
    end
    object Qr3OutValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object Qr3PQ: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT DISTINCT Insumo, pq_.Nome NOMEPQ, '
      'CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial'
      'ELSE frn.Nome END NOMEIQ, lse.Nome NOMESETOR'
      'FROM pqx pqx '
      'LEFT JOIN pq           pq_ ON pq_.Codigo=pqx.Insumo'
      'LEFT JOIN entidades    frn ON frn.Codigo=pq_.IQ'
      'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor')
    Left = 704
    Top = 496
    object Qr3PQInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object Qr3PQNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object Qr3PQINNPESO: TFloatField
      FieldKind = fkLookup
      FieldName = 'INNPESO'
      LookupDataSet = Qr3Inn
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Peso'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQINNVALOR: TFloatField
      FieldKind = fkLookup
      FieldName = 'INNVALOR'
      LookupDataSet = Qr3Inn
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Valor'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQOUTPESO: TFloatField
      FieldKind = fkLookup
      FieldName = 'OUTPESO'
      LookupDataSet = Qr3Out
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Peso'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQOUTVALOR: TFloatField
      FieldKind = fkLookup
      FieldName = 'OUTVALOR'
      LookupDataSet = Qr3Out
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Valor'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQNOMEIQ: TWideStringField
      FieldName = 'NOMEIQ'
      Size = 100
    end
    object Qr3PQNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object Qr3PQBALPESO: TFloatField
      FieldKind = fkLookup
      FieldName = 'BALPESO'
      LookupDataSet = Qr3Bal
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Peso'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQBALVALOR: TFloatField
      FieldKind = fkLookup
      FieldName = 'BALVALOR'
      LookupDataSet = Qr3Bal
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Valor'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQDVLPESO: TFloatField
      FieldKind = fkLookup
      FieldName = 'DVLPESO'
      LookupDataSet = Qr3Dvl
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Peso'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQDVLVALOR: TFloatField
      FieldKind = fkLookup
      FieldName = 'DVLVALOR'
      LookupDataSet = Qr3Dvl
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Valor'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
    end
  end
  object QrPQRx: TMySQLQuery
    Database = ZZDB
    Left = 216
    Top = 532
    object QrPQRxPeso: TFloatField
      FieldName = 'Peso'
    end
  end
  object QrVSMovIts: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT ggx.GraGruY, vmi.*'
      'FROM vsmovits vmi   '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX '
      'WHERE vmi.Pallet=434 '
      'AND vmi.SdoVrtPeca > 0 '
      'ORDER BY vmi.Controle DESC ')
    Left = 948
    Top = 4
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVSMovItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVSMovItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVSMovItsVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
  end
  object QrSumSMIA: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(smia.Qtde) Qtde'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE Baixa <> 0'
      'AND smia.ParTipo=0'
      'AND ggx.Controle=:P0'
      'AND smia.DataHora >= :P1')
    Left = 632
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumSMIAQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSumSMIACustoAll: TFloatField
      FieldName = 'CustoAll'
    end
  end
  object Qr3Dvl: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor'
      'FROM pqx'
      'WHERE Tipo>100'
      'AND DataX BETWEEN :P0 AND :P1'
      'AND CliOrig=:P2'
      'GROUP BY Insumo'
      '')
    Left = 748
    Top = 496
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr3DvlInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object Qr3DvlPeso: TFloatField
      FieldName = 'Peso'
    end
    object Qr3DvlValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object MyDB: TMySQLDatabase
    ConnectOptions = []
    ConnectionTimeout = 5
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=5')
    AfterConnect = MyDBAfterConnect
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 52
    Top = 384
  end
  object QrSumAreas: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT emi.Peso, 110.000 Tipo,  '
      
        'Codigo, CAST(DataEmis + 0 AS DATETIME) Data, "Pesagem" TipoTXT, ' +
        ' '
      'VSMovCod, AreaM2, Qtde Pecas, 999999999.99 AreaPeso  '
      'FROM emit emi '
      'LEFT JOIN pqx pqx ON emi.Codigo=pqx.OrigemCodi '
      'WHERE EmitGru=1035 '
      'AND pqx.CliOrig=-11 '
      'AND emi.Numero=822 ')
    Left = 841
    Top = 57
    object QrSumAreasArM2ComRend: TFloatField
      FieldName = 'ArM2ComRend'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumAreasArP2ComRend: TFloatField
      FieldName = 'ArP2ComRend'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumAreasPesoComRend: TFloatField
      FieldName = 'PesoComRend'
    end
  end
  object QrAreas: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT emi.Peso, 110.000 Tipo,  '
      
        'Codigo, CAST(DataEmis + 0 AS DATETIME) Data, "Pesagem" TipoTXT, ' +
        ' '
      'VSMovCod, AreaM2, Qtde Pecas, 999999999.99 AreaPeso  '
      'FROM emit emi '
      'LEFT JOIN pqx pqx ON emi.Codigo=pqx.OrigemCodi '
      'WHERE EmitGru=1035 '
      'AND pqx.CliOrig=-11 '
      'AND emi.Numero=822 ')
    Left = 841
    Top = 9
    object QrAreasPeso: TFloatField
      FieldName = 'Peso'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrAreasTipo: TFloatField
      FieldName = 'Tipo'
      Required = True
    end
    object QrAreasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrAreasData: TDateTimeField
      FieldName = 'Data'
    end
    object QrAreasTipoTXT: TWideStringField
      FieldName = 'TipoTXT'
      Required = True
      Size = 7
    end
    object QrAreasVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
      Required = True
    end
    object QrAreasAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrAreasPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
      DisplayFormat = '#,###,##0'
    end
    object QrAreasAreaPeso: TFloatField
      FieldName = 'AreaPeso'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrAreasPesoComRend: TFloatField
      FieldName = 'PesoComRend'
      DisplayFormat = '#,###,##0.000'
    end
    object QrAreasArM2ComRend: TFloatField
      FieldName = 'ArM2ComRend'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAreasArP2ComRend: TFloatField
      FieldName = 'ArP2ComRend'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrEmitSbtQtd: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(CouIntros) CouIntros, SUM(AreaM2) AreaM2,  '
      'SUM(AreaP2) AreaP2, SUM(PesoKg) PesoKg,  '
      'SUM(FatorUmidade * PesoKg) fUmiKg, '
      'SUM(LinRebMed * AreaM2) / SUM(AreaM2) LinRebMed, '
      'SUM(PesoKg / FatorUmidade) / SUM(AreaM2) * 10 MedLinWB, '
      ' '
      '(SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)  '
      '*  '
      '(1 + ((  '
      '   (SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)  '
      '    - 32) / 32 / 10  '
      '   )  '
      ') MedLinTrpFator,  '
      '  '
      '/**/  '
      '  '
      '(8.00 +  '
      '  ((  '
      '    ((SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)  '
      '    *  '
      '    (1 + ((  '
      '    (SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)  '
      '    - 32) / 32 / 10  '
      '    ))  '
      '  -32)*0.14) / LinRebMed * 16.3  '
      '  '
      ')  '
      ') MinPrcRend,  '
      '/**/  '
      ' '
      ' '
      ' '
      '1 +  '
      ' (  '
      '   (  '
      '     (SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)  '
      '      -  '
      '      32  '
      '    )  '
      '  / 32 / 10  '
      ') fator  '
      ' '
      ' '
      'FROM emitsbtqtd '
      'WHERE Codigo=1149 ')
    Left = 948
    Top = 56
    object QrEmitSbtQtdCouIntros: TFloatField
      FieldName = 'CouIntros'
    end
    object QrEmitSbtQtdAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEmitSbtQtdAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrEmitSbtQtdPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrEmitSbtQtdfUmiKg: TFloatField
      FieldName = 'fUmiKg'
    end
    object QrEmitSbtQtdLinRebMed: TFloatField
      FieldName = 'LinRebMed'
    end
    object QrEmitSbtQtdMedLinWB: TFloatField
      FieldName = 'MedLinWB'
    end
    object QrEmitSbtQtdMedLinTrpFator: TFloatField
      FieldName = 'MedLinTrpFator'
    end
    object QrEmitSbtQtdMinPrcRend: TFloatField
      FieldName = 'MinPrcRend'
    end
    object QrEmitSbtQtdfator: TFloatField
      FieldName = 'fator'
    end
  end
  object QrSumUsoSbt: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(CouIntros) CouIntros, SUM(AreaM2) AreaM2,  '
      'SUM(AreaP2) AreaP2, SUM(PesoKg) PesoKg,  '
      'SUM(FatorUmidade * PesoKg) fUmiKg, '
      'SUM(LinRebMed * AreaM2) / SUM(AreaM2) LinRebMed, '
      'SUM(PesoKg / FatorUmidade) / SUM(AreaM2) * 10 MedLinWB, '
      ' '
      '(SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)  '
      '*  '
      '(1 + ((  '
      '   (SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)  '
      '    - 32) / 32 / 10  '
      '   )  '
      ') MedLinTrpFator,  '
      '  '
      '/**/  '
      '  '
      '(8.00 +  '
      '  ((  '
      '    ((SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)  '
      '    *  '
      '    (1 + ((  '
      '    (SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)  '
      '    - 32) / 32 / 10  '
      '    ))  '
      '  -32)*0.14) / LinRebMed * 16.3  '
      '  '
      ')  '
      ') MinPrcRend,  '
      '/**/  '
      ' '
      ' '
      ' '
      '1 +  '
      ' (  '
      '   (  '
      '     (SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)  '
      '      -  '
      '      32  '
      '    )  '
      '  / 32 / 10  '
      ') fator  '
      ' '
      ' '
      'FROM emitsbtqtd '
      'WHERE Codigo=1149 ')
    Left = 948
    Top = 104
    object QrSumUsoSbtCouIntros: TFloatField
      FieldName = 'CouIntros'
    end
    object QrSumUsoSbtAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumUsoSbtAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSumUsoSbtPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object QrMedUsoSbt: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(CouIntros) CouIntros, '
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, '
      'SUM(PesoKg) PesoKg,'
      'SUM(AreaM2 * PercEsperRend) / SUM(AreaM2) PercEsperRend   '
      'FROM emitusosbt '
      'WHERE Codigo=32488 ')
    Left = 948
    Top = 156
    object QrMedUsoSbtCouIntros: TFloatField
      FieldName = 'CouIntros'
    end
    object QrMedUsoSbtAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrMedUsoSbtAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrMedUsoSbtPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrMedUsoSbtPercEsperRend: TFloatField
      FieldName = 'PercEsperRend'
    end
  end
  object QrUpdM: TMySQLQuery
    Database = MyDB
    Left = 404
    Top = 99
  end
  object QrDuplCli: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM pqcli'
      'WHERE PQ=:P0 '
      'AND CI=:P1')
    Left = 937
    Top = 241
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object QrLocGGX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM GraGruX'
      'WHERE GraGru1=:P0')
    Left = 940
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocGGXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
end
