unit CMPTOutIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, Mask, DBCtrls,
  dmkLabel, Grids, DBGrids, dmkDBGrid, mySQLDbTables, dmkEdit, dmkRadioGroup,
  dmkDBLookupComboBox, dmkEditCB, dmkImage, UnDmkEnums;

type
  TTipoMedidaAlterada = (tmaAuto, tmaPeso, tmaPeca, tmaArea, tmaValor);
  TFmCMPTOutIts = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    PnAdd: TPanel;
    GradeCMPTInn: TDBGrid;
    QrCMPTInn: TmySQLQuery;
    DsCMPTInn: TDataSource;
    QrCMPTInnCodigo: TIntegerField;
    QrCMPTInnCliente: TIntegerField;
    QrCMPTInnDataE: TDateField;
    QrCMPTInnNFInn: TIntegerField;
    QrCMPTInnNFRef: TIntegerField;
    QrCMPTInnInnQtdkg: TFloatField;
    QrCMPTInnInnQtdPc: TFloatField;
    QrCMPTInnInnQtdVal: TFloatField;
    QrCMPTInnSdoQtdkg: TFloatField;
    QrCMPTInnSdoQtdPc: TFloatField;
    QrCMPTInnSdoQtdVal: TFloatField;
    GroupBox1: TGroupBox;
    EdOutQtdkg: TdmkEdit;
    Label4: TLabel;
    Label6: TLabel;
    EdOutQtdPc: TdmkEdit;
    EdOutQtdVal: TdmkEdit;
    Label7: TLabel;
    GroupBox2: TGroupBox;
    EdSdoQtdkg: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdSdoQtdPc: TdmkEdit;
    EdSdoQtdVal: TdmkEdit;
    Label5: TLabel;
    QrCMPTInnEmpresa: TIntegerField;
    QrCMPTInnGraGruX: TIntegerField;
    QrCMPTInnTipoNF: TSmallintField;
    QrCMPTInnrefNFe: TWideStringField;
    QrCMPTInnmodNF: TSmallintField;
    QrCMPTInnSerie: TIntegerField;
    QrCMPTInnInnQtdM2: TFloatField;
    QrCMPTInnSdoQtdM2: TFloatField;
    Label8: TLabel;
    EdOutQtdM2: TdmkEdit;
    EdSdoQtdM2: TdmkEdit;
    Label9: TLabel;
    Label10: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    GroupBox3: TGroupBox;
    EdMOPrcKg: TdmkEdit;
    Label11: TLabel;
    EdMOValor: TdmkEdit;
    Label12: TLabel;
    GroupBox4: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    EdBenefikg: TdmkEdit;
    EdBenefiPc: TdmkEdit;
    EdBenefiM2: TdmkEdit;
    RGSitDevolu: TdmkRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    BtSaida: TBitBtn;
    Label14: TLabel;
    EdMOPesoKg: TdmkEdit;
    RGHowCobrMO: TdmkRadioGroup;
    QrCMPTInnLk: TIntegerField;
    QrCMPTInnDataCad: TDateField;
    QrCMPTInnDataAlt: TDateField;
    QrCMPTInnUserCad: TIntegerField;
    QrCMPTInnUserAlt: TIntegerField;
    QrCMPTInnAlterWeb: TSmallintField;
    QrCMPTInnAtivo: TSmallintField;
    QrCMPTInnjaAtz: TSmallintField;
    QrCMPTInnProcedencia: TIntegerField;
    QrCMPTInnValMaoObra: TFloatField;
    QrCMPTInnNF_CC: TIntegerField;
    Label13: TLabel;
    EdNFCMO: TdmkEdit;
    Label15: TLabel;
    EdNFDMP: TdmkEdit;
    Label18: TLabel;
    EdNFDIQ: TdmkEdit;
    QrCMPTInnGerBxaEstq: TSmallintField;
    QrCMPTInnHowCobrMO: TSmallintField;
    QrCMPTInnPLEKg: TFloatField;
    QrCMPTInnPrcUnitMO: TFloatField;
    QrCMPTInnSdoFMOKg: TFloatField;
    QrCMPTInnSdoFMOVal: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeCMPTInnDblClick(Sender: TObject);
    procedure EdOutQtdkgEnter(Sender: TObject);
    procedure EdOutQtdkgChange(Sender: TObject);
    procedure EdOutQtdPcEnter(Sender: TObject);
    procedure EdOutQtdPcChange(Sender: TObject);
    procedure EdOutQtdValEnter(Sender: TObject);
    procedure EdOutQtdValChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdOutQtdM2Change(Sender: TObject);
    procedure EdOutQtdM2Enter(Sender: TObject);
    //procedure EdSrvCodigoChange(Sender: TObject);
    //procedure EdSrvValUniChange(Sender: TObject);
    procedure RGComoCobraClick(Sender: TObject);
    procedure EdBenefikgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMOPesoKgRedefinido(Sender: TObject);
    procedure EdMOPrcKgRedefinido(Sender: TObject);
  private
    { Private declarations }
    FTipoMedidaAlterada: TTipoMedidaAlterada;
    procedure ReopenCMPTInn(Codigo: Integer);
    procedure AtualizaEstoque(Fonte: TTipoMedidaAlterada);
    //procedure CalculaServico();
    procedure CalculaMOProporcional();
    procedure CalculaValorMO();

  public
    { Public declarations }
  end;

  var
  FmCMPTOutIts: TFmCMPTOutIts;

implementation

uses UnMyObjects, Module, CMPTOut, UmySQLModule, ModuleGeral, dmkGeral,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmCMPTOutIts.AtualizaEstoque(Fonte: TTipoMedidaAlterada);
var
  Peso, Peca, Valr, M2: Double;
begin
  if Fonte = tmaAuto then Fonte := FTipoMedidaAlterada;
  //
  Peso := EdOutQtdkg.ValueVariant;
  Peca := EdOutQtdPc.ValueVariant;
  Valr := EdOutQtdVal.ValueVariant;
  M2   := EdOutQtdM2.ValueVariant;
  //
  case Fonte of
    tmaPeso:
    begin
      if Peso >= QrCMPTInnSdoQtdkg.Value then
      begin
        Peca := QrCMPTInnSdoQtdPc.Value;
        Valr := QrCMPTInnSdoQtdVal.Value;
        M2   := QrCMPTInnSdoQtdM2.Value;
      end else begin
        if QrCMPTInnSdoQtdkg.Value <= 0 then
        begin
          Peca := 0;
          Valr := 0;
          M2   := 0;
        end else begin
          M2   := Trunc(Peso / QrCMPTInnSdoQtdkg.Value * QrCMPTInnSdoQtdM2.Value * 100) / 100;
          Peca := Trunc(Peso / QrCMPTInnSdoQtdkg.Value * QrCMPTInnSdoQtdPc.Value);
          Valr := Trunc(Peso / QrCMPTInnSdoQtdkg.Value * QrCMPTInnSdoQtdVal.Value * 100) / 100;
        end;
      end;
    end;
    tmaPeca:
    begin
      if Peca >= QrCMPTInnSdoQtdPc.Value then
      begin
        Peso := QrCMPTInnSdoQtdkg.Value;
        Valr := QrCMPTInnSdoQtdVal.Value;
        M2   := QrCMPTInnSdoQtdM2.Value;
      end else begin
        if QrCMPTInnSdoQtdPc.Value <= 0 then
        begin
          Peso := 0;
          Valr := 0;
          M2   := 0;
        end else begin
          M2   := Trunc(Peca / QrCMPTInnSdoQtdPc.Value * QrCMPTInnSdoQtdM2.Value * 100) / 100;
          Peso := Trunc(Peca / QrCMPTInnSdoQtdPc.Value * QrCMPTInnSdoQtdkg.Value * 1000) / 1000;
          Valr := Trunc(Peca / QrCMPTInnSdoQtdPc.Value * QrCMPTInnSdoQtdVal.Value * 100) / 100;
        end;
      end;
    end;
    tmaArea:
    begin
      if M2 >= QrCMPTInnSdoQtdM2.Value then
      begin
        Peso := QrCMPTInnSdoQtdkg.Value;
        Valr := QrCMPTInnSdoQtdVal.Value;
        Peca := QrCMPTInnSdoQtdPc.Value;
      end else begin
        if QrCMPTInnSdoQtdM2.Value <= 0 then
        begin
          Peso := 0;
          Valr := 0;
          Peca := 0;
        end else begin
          Peca := Trunc(M2 / QrCMPTInnSdoQtdM2.Value * QrCMPTInnSdoQtdPc.Value);
          Peso := Trunc(M2 / QrCMPTInnSdoQtdM2.Value * QrCMPTInnSdoQtdkg.Value * 1000) / 1000;
          Valr := Trunc(M2 / QrCMPTInnSdoQtdM2.Value * QrCMPTInnSdoQtdVal.Value * 100) / 100;
        end;
      end;
    end;
    tmaValor:
    begin
      if Valr >= QrCMPTInnSdoQtdVal.Value then
      begin
        Peso := QrCMPTInnSdoQtdkg.Value;
        Peca := QrCMPTInnSdoQtdPc.Value;
        M2   := QrCMPTInnSdoQtdM2.Value;
      end else begin
        if QrCMPTInnSdoQtdVal.Value <= 0 then
        begin
          Peso := 0;
          Peca := 0;
          M2   := 0;
        end else begin
          M2   := Trunc(Valr / QrCMPTInnSdoQtdVal.Value * QrCMPTInnSdoQtdM2.Value * 100) / 100;
          Peso := Trunc(Valr / QrCMPTInnSdoQtdVal.Value * QrCMPTInnSdoQtdkg.Value * 1000) / 1000;
          Peca := Trunc(Valr / QrCMPTInnSdoQtdVal.Value * QrCMPTInnSdoQtdPc.Value);
        end;
      end;
    end;
  end;
  if Fonte <> tmaPeso then
    EdOutQtdkg.ValueVariant  := Peso;
  if Fonte <> tmaPeca then
    EdOutQtdPc.ValueVariant  := Peca;
  if Fonte <> tmaValor then
    EdOutQtdVal.ValueVariant := Valr;
  if Fonte <> tmaArea then
    EdOutQtdM2.ValueVariant := M2;
  //
  Peso := QrCMPTInnSdoQtdkg.Value  - Peso;
  Peca := QrCMPTInnSdoQtdPc.Value  - Peca;
  Valr := QrCMPTInnSdoQtdVal.Value - Valr;
  M2   := QrCMPTInnSdoQtdM2.Value  - M2;
  //
  EdSdoQtdkg.ValueVariant  := Peso;
  EdSdoQtdPc.ValueVariant  := Peca;
  EdSdoQtdval.ValueVariant := Valr;
  EdSdoQtdM2.ValueVariant  := M2;
end;

procedure TFmCMPTOutIts.BitBtn1Click(Sender: TObject);
begin
  GradeCMPTInn.Enabled := True;
  PnAdd.Visible        := False;
  //
  EdOutQtdPc.ValueVariant  := 0;
  AtualizaEstoque(tmaPeca);
end;

procedure TFmCMPTOutIts.BtOKClick(Sender: TObject);
(*
var
  SrvValUni, SrvValTot, OutQtdkg, OutQtdPc, OutQtdVal, OutQtdM2: Double;
  Controle, Codigo, CMPTInn, SrvCodigo, SitDevolu: Integer;
  Benefikg, BenefiPc, BenefiM2, MDOValAFat: Double;
  ComoCobra, GerBxaEstq: Integer;
begin
  OutQtdkg   := EdOutQtdkg.ValueVariant;
  OutQtdPc   := EdOutQtdPc.ValueVariant;
  OutQtdM2   := EdOutQtdM2.ValueVariant;
  OutQtdVal  := EdOutQtdVal.ValueVariant;
  SrvCodigo  := EdSrvCodigo.ValueVariant;
  SrvValUni  := EdSrvValUni.ValueVariant;
  SrvValTot  := EdSrvValTot.ValueVariant;
  SitDevolu  := RGSitDevolu.ItemIndex;
  Benefikg   := EdOutQtdkg.ValueVariant;
  BenefiPc   := EdOutQtdPc.ValueVariant;
  BenefiM2   := EdOutQtdM2.ValueVariant;
  ComoCobra  := RGComoCobra.ItemIndex;
  GerBxaEstq := RGGerBxaEstq.ItemIndex;
  MDOValAFat := EdMDOValAFat.ValueVariant;
*)
var
  //Erro: String;
  //
  Codigo, Controle, CMPTInn, SitDevolu, HowCobrMO, NFCMO, NFDMP,
  NFDIQ: Integer;
  OutQtdkg, OutQtdPc, OutQtdM2, OutQtdVal, Benefikg, BenefiPc, BenefiM2,
  MOPesoKg, MOPrcKg, MOValor: Double;
begin
  Codigo         := FmCMPTOut.QrCMPTOutCodigo.Value;
  Controle       := QrCMPTInnCodigo.Value;
  CMPTInn        := QrCMPTInnCodigo.Value;
  OutQtdkg       := EdOutQtdkg.ValueVariant;
  OutQtdPc       := EdOutQtdPc.ValueVariant;
  OutQtdM2       := EdOutQtdM2.ValueVariant;
  OutQtdVal      := EdOutQtdVal.ValueVariant;
  Benefikg       := EdOutQtdkg.ValueVariant;
  BenefiPc       := EdOutQtdPc.ValueVariant;
  BenefiM2       := EdOutQtdM2.ValueVariant;
  SitDevolu      := RGSitDevolu.ItemIndex;
  MOPesoKg       := EdMOPesoKg.ValueVariant;
  MOPrcKg        := EdMOPrcKg.ValueVariant;
  MOValor        := EdMOValor.ValueVariant;
  HowCobrMO      := RGHowCobrMO.ItemIndex;
  NFCMO          := EdNFCMO.ValueVariant;
  NFDMP          := EdNFDMP.ValueVariant;
  NFDIQ          := EdNFDIQ.ValueVariant;
  //
  (*
  Erro := '';
  case GerBxaEstq of
    0: Erro := '? ? ?';
    1: if OutQtdPc  = 0 then Erro := 'pe�as';
    2: if OutQtdM2  = 0 then Erro := '�rea (m�)';
    3: if OutQtdkg  = 0 then Erro := 'Peso (kg)';
  end;
  if Erro <> '' then
  begin
    Geral.MB_Aviso('O item "Dados de baixa: ' + Erro + '" deve ser informado!');
    Exit;
  end;

  case GerBxaEstq of
    0: Erro := '? ? ?';
    1: if BenefiPc  = 0 then Erro := 'pe�as';
    2: if BenefiM2  = 0 then Erro := '�rea (m�)';
    3: if Benefikg  = 0 then Erro := 'Peso (kg)';
  end;
  if Erro <> '' then
  begin
    Geral.MB_Aviso(
    'O item " Dados de beneficiamento: ' + Erro + '" deve ser informado!');
    Exit;
  end;

  //
  if (SrvValTot <> 0) or (SrvCodigo <> 0) then
  if (SrvValTot = 0) or (SrvCodigo = 0) then
  begin
    Geral.MB_Aviso('C�digo ou valor do servi�o indefinido!');
    Exit;
  end;
  //
  if (SrvValTot = 0) and (SrvCodigo = 0) then
  begin
    if Geral.MB_Pergunta('Servi�o indefinido! Deseja continuar assim ' +
    'mesmo?')) <> ID_YES then
    Exit;
  end;
  //

  Controle := UMyMod.BuscaEmLivreY_Def('CMPToutits', 'Controle', ImgTipo.SQLType,
    Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cmptoutits', False, [
  'Codigo', 'CMPTInn', 'OutQtdkg',
  'OutQtdPc', 'OutQtdM2', 'OutQtdVal',
  'Benefikg', 'BenefiPc', 'BenefiM2',
  'SrvCodigo', 'SrvValUni', 'ComoCobra',
  'SrvValTot', 'SitDevolu', 'GerBxaEstq',
  'MDOValAFat'], [
  'Controle'], [
  Codigo, CMPTInn, OutQtdkg,
  OutQtdPc, OutQtdM2, OutQtdVal,
  Benefikg, BenefiPc, BenefiM2,
  SrvCodigo, SrvValUni, ComoCobra,
  SrvValTot, SitDevolu, GerBxaEstq,
  MDOValAFat], [
  Controle], True) then
  begin
    Dmod.AtualizaEstoqueCMPT(CMPTInn, Codigo);
    FmCMPTOut.ReopenCMPTOut(Codigo);
    //
    Close;
  end;
O campo "cmptoutits.GerBxaEstq" deveria ser exclu�do!
O campo "cmptoutits.SrvCodigo" deveria ser exclu�do!
O campo "cmptoutits.SrvValUni" deveria ser exclu�do!
O campo "cmptoutits.ComoCobra" deveria ser exclu�do!
O campo "cmptoutits.SrvValTot" deveria ser exclu�do!
O campo "cmptoutits.MDOValAFat" deveria ser exclu�do!
*)


  Controle := UMyMod.BuscaEmLivreY_Def('CMPToutits', 'Controle', ImgTipo.SQLType,
    Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cmptoutits', False, [
  'Codigo', 'CMPTInn', 'OutQtdkg',
  'OutQtdPc', 'OutQtdM2', 'OutQtdVal',
  'Benefikg', 'BenefiPc', 'BenefiM2',
  'SitDevolu', 'MOPesoKg', 'MOPrcKg',
  'MOValor', 'HowCobrMO', 'NFCMO',
  'NFDMP', 'NFDIQ'], [
  'Controle'], [
  Codigo, CMPTInn, OutQtdkg,
  OutQtdPc, OutQtdM2, OutQtdVal,
  Benefikg, BenefiPc, BenefiM2,
  SitDevolu, MOPesoKg, MOPrcKg,
  MOValor, HowCobrMO, NFCMO,
  NFDMP, NFDIQ], [
  Controle], True) then
  begin
    Dmod.AtualizaEstoqueCMPT(CMPTInn, Codigo);
    FmCMPTOut.ReopenCMPTOut(Codigo);
    //
    Close;
  end;

end;

procedure TFmCMPTOutIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCMPTOutIts.CalculaMOProporcional();
var
  Fator, MOPesoKg, MOPrcKg, OutQtdkg, OutQtdPc: Double;
  SitDevolu: Integer;
begin
/// SitDevolu
  OutQtdPc := EdOutQtdPc.ValueVariant;
  if OutQtdPc = QrCMPTInnInnQtdPc.Value then
    SitDevolu := 2
  else
  if OutQtdPc = QrCMPTInnSdoQtdPc.Value then
    SitDevolu := 1
  else
    SitDevolu := 0;
  //
  RGSitDevolu.ItemIndex := SitDevolu;

////////////////////////////////////////////////////////////////////////////////

/// MOPesoKg
  RGHowCobrMO.ItemIndex := QrCMPTInnHowCobrMO.Value;
  OutQtdkg := EdOutQtdkg.ValueVariant;
  MOPesoKg := 0;
  MOPrcKg  := QrCMPTInnPrcUnitMO.Value;
  //
  case RGHowCobrMO.ItemIndex of
    0: Fator := 1;
    1:
    begin
      if QrCMPTInnInnQtdkg.Value <> 0 then
        Fator := QrCMPTInnPLEKg.Value / QrCMPTInnInnQtdkg.Value
      else
        Fator := 0;
    end;
    else
    begin
      Fator := 0;
      Geral.MB_Aviso(
        'Tipo de m�o de obra n�o definido em  "CalculaMOProporcional()"');
    end;
  end;
  //
  MOPesoKg := OutQtdkg * Fator;
  //
  EdMOPesoKg.ValueVariant := MOPesoKg;
  EdMOPrcKg.ValueVariant  := MOPrcKg;
end;

procedure TFmCMPTOutIts.CalculaValorMO();
var
  MOValor, MOPesoKg, MOPrcKg: Double;
begin
  MOPesoKg := EdMOPesoKg.ValueVariant;
  MOPrcKg  := EdMOPrcKg.ValueVariant;
  //
  MOValor  := MOPesoKg * MOPrcKg;
  //
  EdMOValor.ValueVariant := MOValor;
end;

{
procedure TFmCMPTOutIts.CalculaServico;
var
  Preco, Qtde, Total: Double;
begin
  Preco := EdSrvValUni.ValueVariant;
  case RGGerBxaEstq.ItemIndex of
    0: Qtde := 0;
    1: if RGComoCobra.ItemIndex = 0 then
         Qtde := EdOutQtdPc.ValueVariant
       else
         Qtde := EdBenefiPc.ValueVariant;
    2: if RGComoCobra.ItemIndex = 0 then
         Qtde := EdOutQtdM2.ValueVariant
       else
         Qtde := EdBenefiM2.ValueVariant;
    3: if RGComoCobra.ItemIndex = 0 then
         Qtde := EdOutQtdkg.ValueVariant
       else
         Qtde := EdBenefikg.ValueVariant;
    else Qtde := 0;
  end;
  Total := Preco * Qtde;
  EdSrvValTot.ValueVariant := Total;
end;

procedure TFmCMPTOutIts.EdSrvCodigoChange(Sender: TObject);
begin
  case RGGerBxaEstq.ItemIndex of
    0: EdSrvValUni.ValueVariant := 0;
    1: EdSrvValUni.ValueVariant := QrGraSrv1PrecoPc.Value;
    2: EdSrvValUni.ValueVariant := QrGraSrv1PrecoM2.Value;
    3: EdSrvValUni.ValueVariant := QrGraSrv1Precokg.Value;
  end;
end;

procedure TFmCMPTOutIts.EdSrvValUniChange(Sender: TObject);
begin
  CalculaServico();
end;
}

procedure TFmCMPTOutIts.EdBenefikgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  PerTxt: String;
  PerVal, Peso: Double;
begin
  if Key = VK_F6 then
  begin
    PerTxt := '100,00';
    if InputQuery('Percentual de Quebra', 'Informe o % de quebra', PerTxt) then
    begin
      PerVal := Geral.DMV(PerTxt);
      Peso := EdOutQtdkg.ValueVariant;
      Peso := Peso - (Peso * PerVal / 100);
      EdBenefikg.ValueVariant := Peso;
    end;
  end;
end;

procedure TFmCMPTOutIts.EdMOPesoKgRedefinido(Sender: TObject);
begin
  CalculaValorMO();
end;

procedure TFmCMPTOutIts.EdMOPrcKgRedefinido(Sender: TObject);
begin
  CalculaValorMO();
end;

procedure TFmCMPTOutIts.EdOutQtdkgChange(Sender: TObject);
begin
  AtualizaEstoque(tmaAuto);
  if ImgTipo.SQLType = stIns then
  begin
    EdBenefikg.ValueVariant := EdOutQtdkg.ValueVariant;
    //
    CalculaMOProporcional();
  end;
  //CalculaServico();
end;

procedure TFmCMPTOutIts.EdOutQtdkgEnter(Sender: TObject);
begin
  FTipoMedidaAlterada := tmaPeso;
end;

procedure TFmCMPTOutIts.EdOutQtdM2Change(Sender: TObject);
begin
  AtualizaEstoque(tmaAuto);
  if ImgTipo.SQLType = stIns then
    EdBenefiM2.ValueVariant := EdOutQtdM2.ValueVariant;
  //CalculaServico();
end;

procedure TFmCMPTOutIts.EdOutQtdM2Enter(Sender: TObject);
begin
  FTipoMedidaAlterada := tmaArea;
end;

procedure TFmCMPTOutIts.EdOutQtdPcChange(Sender: TObject);
begin
  AtualizaEstoque(tmaAuto);
  if ImgTipo.SQLType = stIns then
  begin
    EdBenefiPc.ValueVariant := EdOutQtdPc.ValueVariant;
    CalculaMOProporcional();
  end;
  //CalculaServico();
end;

procedure TFmCMPTOutIts.EdOutQtdPcEnter(Sender: TObject);
begin
  FTipoMedidaAlterada := tmaPeca;
end;

procedure TFmCMPTOutIts.EdOutQtdValChange(Sender: TObject);
begin
  AtualizaEstoque(tmaAuto);
end;

procedure TFmCMPTOutIts.EdOutQtdValEnter(Sender: TObject);
begin
  FTipoMedidaAlterada := tmaValor;
end;

procedure TFmCMPTOutIts.GradeCMPTInnDblClick(Sender: TObject);
begin
  GradeCMPTInn.Enabled := False;
  PnAdd.Visible        := True;
  //
  EdOutQtdkg.ValueVariant  := QrCMPTInnInnQtdkg.Value;
  EdOutQtdPc.ValueVariant  := QrCMPTInnInnQtdPc.Value;
  EdOutQtdVal.ValueVariant := QrCMPTInnInnQtdVal.Value;
  //
  EdOutQtdkg.SetFocus;
end;

procedure TFmCMPTOutIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCMPTOutIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //UnDmkDAC_PF.AbreQuery(QrGraSrv1, Dmod.MyDB);
  ReopenCMPTInn(0);
end;

procedure TFmCMPTOutIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCMPTOutIts.RGComoCobraClick(Sender: TObject);
begin
  //CalculaServico();
end;

procedure TFmCMPTOutIts.ReopenCMPTInn(Codigo: Integer);
begin
(*
  QrCMPTInn.Close;
  QrCMPTInn.Params[00].AsInteger := FmCMPTOut.QrCMPTOutEmpresa.Value;
  QrCMPTInn.Params[01].AsInteger := FmCMPTOut.QrCMPTOutCliente.Value;
  QrCMPTInn . O p e n ;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrCMPTInn, Dmod.MyDB, [
  'SELECT gg1.GerBxaEstq, inn.* ',
  'FROM cmptinn inn ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=inn.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE inn.Empresa=' + Geral.FF0(FmCMPTOut.QrCMPTOutEmpresa.Value),
  'AND inn.Cliente=' + Geral.FF0(FmCMPTOut.QrCMPTOutCliente.Value),
  'AND IF(gg1.GerBxaEstq=1, inn.SdoQtdPc, ',
  '  IF(gg1.GerBxaEstq=2, inn.SdoQtdM2,inn.SdoQtdkg) )> 0 ',
  '']);
end;

end.
