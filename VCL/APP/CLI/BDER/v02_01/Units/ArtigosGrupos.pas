unit ArtigosGrupos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, dmkGeral,
   UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa,  
     
      dmkEdit, dmkImage, UnDmkEnums;

type
  TFmArtigosGrupos = class(TForm)
    PainelDados: TPanel;
    DsArtigosGrupos: TDataSource;
    QrArtigosGrupos: TmySQLQuery;
    QrArtigosGruposLk: TIntegerField;
    QrArtigosGruposDataCad: TDateField;
    QrArtigosGruposDataAlt: TDateField;
    QrArtigosGruposUserCad: TIntegerField;
    QrArtigosGruposUserAlt: TIntegerField;
    QrArtigosGruposCodigo: TSmallintField;
    QrArtigosGruposNome: TWideStringField;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdCF: TEdit;
    EdST: TEdit;
    dmkEdICMS: TdmkEdit;
    dmkEdIPI: TdmkEdit;
    Label7: TLabel;
    EdCodImp: TEdit;
    dmkEdPreco: TdmkEdit;
    Label8: TLabel;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    QrArtigosGruposFluxo: TIntegerField;
    QrArtigosGruposReceitaR: TIntegerField;
    QrArtigosGruposReceitaA: TIntegerField;
    QrArtigosGruposGrandeza: TSmallintField;
    QrArtigosGruposAlterWeb: TSmallintField;
    QrArtigosGruposAtivo: TSmallintField;
    QrArtigosGruposCF: TWideStringField;
    QrArtigosGruposST: TWideStringField;
    QrArtigosGruposICMS: TFloatField;
    QrArtigosGruposIPI: TFloatField;
    QrArtigosGruposPreco: TFloatField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    QrArtigosGruposCodImp: TWideStringField;
    DBEdit6: TDBEdit;
    QrArtigosGruposPrecoICM: TSmallintField;
    CkPrecoICM: TCheckBox;
    DBCheckBox1: TDBCheckBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrArtigosGruposAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrArtigosGruposAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrArtigosGruposBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    FSeq: Integer;
  end;

var
  FmArtigosGrupos: TFmArtigosGrupos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmArtigosGrupos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmArtigosGrupos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrArtigosGruposCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmArtigosGrupos.DefParams;
begin
  VAR_GOTOTABELA := 'ArtigosGrupos';
  VAR_GOTOMYSQLTABLE := QrArtigosGrupos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM artigosgrupos');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmArtigosGrupos.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      if SQLType = stIns then
      begin
        EdCodigo.Text := '';
        EdNome.Text   := '';
        EdCodImp.Text           := '';
        EdCF.Text               := '';
        EdST.Text               := '';
        dmkEdPreco.ValueVariant := 0;
        dmkEdICMS.ValueVariant  := 0;
        dmkEdIPI.ValueVariant   := 0;
        CkPrecoICM.Checked      := False;
      end else begin
        EdCodigo.Text           := IntToStr(QrArtigosGruposCodigo.Value);
        EdNome.Text             := QrArtigosGruposNome.Value;
        EdCodImp.Text           := QrArtigosGruposCodImp.Value;
        EdCF.Text               := QrArtigosGruposCF.Value;
        EdST.Text               := QrArtigosGruposST.Value;
        dmkEdPreco.ValueVariant := QrArtigosGruposPreco.Value;
        dmkEdICMS.ValueVariant  := QrArtigosGruposICMS.Value;
        dmkEdIPI.ValueVariant   := QrArtigosGruposIPI.Value;
        CkPrecoICM.Checked      := Geral.IntToBool_0(QrArtigosGruposPrecoICM.Value);
      end;
      EdNome.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmArtigosGrupos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmArtigosGrupos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmArtigosGrupos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmArtigosGrupos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmArtigosGrupos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmArtigosGrupos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmArtigosGrupos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmArtigosGrupos.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmArtigosGrupos.BtAlteraClick(Sender: TObject);
var
  ArtigosGrupos : Integer;
begin
  ArtigosGrupos := QrArtigosGruposCodigo.Value;
  if not UMyMod.SelLockY(ArtigosGrupos, Dmod.MyDB, 'ArtigosGrupos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(ArtigosGrupos, Dmod.MyDB, 'ArtigosGrupos', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmArtigosGrupos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrArtigosGruposCodigo.Value;
  Close;
end;

procedure TFmArtigosGrupos.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descrição.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('artigosgrupos', 'Codigo', ImgTipo.SQLType,
    QrArtigosGruposCodigo.Value);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'artigosgrupos', False,
  [
    'Nome', 'CF', 'ST', 'ICMS',
    'IPI', 'Preco', 'CodImp',
    'PrecoICM'
  ], ['Codigo'], [
    Nome, EdCF.Text, EdST.Text, dmkEdICMS.ValueVariant,
    dmkEdIPI.ValueVariant, dmkEdPreco.ValueVariant, EdCodImp.Text,
    Geral.BoolToInt(CkPrecoICM.Checked)
  ], [Codigo], true) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'artigosgrupos', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmArtigosGrupos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ArtigosGrupos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ArtigosGrupos', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ArtigosGrupos', 'Codigo');
end;

procedure TFmArtigosGrupos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FSeq := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmArtigosGrupos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrArtigosGruposCodigo.Value,LaRegistro.Caption);
end;

procedure TFmArtigosGrupos.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmArtigosGrupos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmArtigosGrupos.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmArtigosGrupos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrArtigosGruposCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmArtigosGrupos.QrArtigosGruposAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmArtigosGrupos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'ArtigosGrupos', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, stIns, 0);
end;

procedure TFmArtigosGrupos.QrArtigosGruposAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrArtigosGruposCodigo.Value, False);
  //BtExclui.Enabled := GOTOy.BtEnabled(QrArtigosGruposCodigo.Value, False);
end;

procedure TFmArtigosGrupos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrArtigosGruposCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ArtigosGrupos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmArtigosGrupos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmArtigosGrupos.QrArtigosGruposBeforeOpen(DataSet: TDataSet);
begin
  QrArtigosGruposCodigo.DisplayFormat := FFormatFloat;
end;

end.

