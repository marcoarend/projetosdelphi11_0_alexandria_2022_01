unit FormulasImpBHInsLote;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, Db, Grids, DBGrids, AppListas,
  DBCtrls, Menus, dmkGeral, dmkEdit, dmkEditCB, dmkDBLookupComboBox,
  mySQLDbTables, dmkImage, UnDmkEnums, UnDmkProcFunc, DmkDAC_PF;

type
  TFmFormulasImpBHInsLote = class(TForm)
    PainelDados: TPanel;
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrLotes: TmySQLQuery;
    DsLotes: TDataSource;
    DBGrid1: TDBGrid;
    Panel2: TPanel;
    QrMarcas: TmySQLQuery;
    DsMarcas: TDataSource;
    Label6: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    QrCliInt: TmySQLQuery;
    QrCliIntNOMECI: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    DsCliInt: TDataSource;
    QrLotesNOMECLIINT: TWideStringField;
    QrLotesNOMEPROCEDE: TWideStringField;
    QrLotesCodigo: TIntegerField;
    QrLotesControle: TIntegerField;
    QrLotesFicha: TIntegerField;
    QrLotesData: TDateField;
    QrLotesClienteI: TIntegerField;
    QrLotesProcedencia: TIntegerField;
    QrLotesTransportadora: TIntegerField;
    QrLotesMarca: TWideStringField;
    QrLotesPecas: TFloatField;
    QrLotesPecasNF: TFloatField;
    QrLotesM2: TFloatField;
    QrLotesPNF: TFloatField;
    QrLotesPLE: TFloatField;
    QrLotesPDA: TFloatField;
    QrLotesRecorte_PDA: TFloatField;
    QrLotesPTA: TFloatField;
    QrLotesRecorte_PTA: TFloatField;
    QrLotesRaspa_PTA: TFloatField;
    QrLotesTipificacao: TIntegerField;
    QrLotesAparasCabelo: TSmallintField;
    QrLotesSeboPreDescarne: TSmallintField;
    QrLotesValor: TFloatField;
    QrLotesCustoPQ: TFloatField;
    QrLotesFrete: TFloatField;
    QrLotesAbateTipo: TIntegerField;
    QrLotesLk: TIntegerField;
    QrLotesDataCad: TDateField;
    QrLotesDataAlt: TDateField;
    QrLotesUserCad: TIntegerField;
    QrLotesUserAlt: TIntegerField;
    QrLotesNOMETIPO: TWideStringField;
    DBGrid2: TDBGrid;
    QrMarcasNOMECLIINT: TWideStringField;
    QrMarcasNOMEPROCEDE: TWideStringField;
    QrMarcasCodigo: TIntegerField;
    QrMarcasControle: TIntegerField;
    QrMarcasFicha: TIntegerField;
    QrMarcasData: TDateField;
    QrMarcasClienteI: TIntegerField;
    QrMarcasProcedencia: TIntegerField;
    QrMarcasTransportadora: TIntegerField;
    QrMarcasMarca: TWideStringField;
    QrMarcasPecas: TFloatField;
    QrMarcasPecasNF: TFloatField;
    QrMarcasM2: TFloatField;
    QrMarcasPNF: TFloatField;
    QrMarcasPLE: TFloatField;
    QrMarcasPDA: TFloatField;
    QrMarcasRecorte_PDA: TFloatField;
    QrMarcasPTA: TFloatField;
    QrMarcasRecorte_PTA: TFloatField;
    QrMarcasRaspa_PTA: TFloatField;
    QrMarcasTipificacao: TIntegerField;
    QrMarcasAparasCabelo: TSmallintField;
    QrMarcasSeboPreDescarne: TSmallintField;
    QrMarcasValor: TFloatField;
    QrMarcasCustoPQ: TFloatField;
    QrMarcasFrete: TFloatField;
    QrMarcasAbateTipo: TIntegerField;
    QrMarcasLk: TIntegerField;
    QrMarcasDataCad: TDateField;
    QrMarcasDataAlt: TDateField;
    QrMarcasUserCad: TIntegerField;
    QrMarcasUserAlt: TIntegerField;
    QrMarcasNOMETIPO: TWideStringField;
    EdLote: TdmkEdit;
    Label2: TLabel;
    QrLotesLote: TWideStringField;
    QrMarcasLote: TWideStringField;
    Panel3: TPanel;
    Label3: TLabel;
    EdMarca: TdmkEdit;
    DBGrid3: TDBGrid;
    Splitter1: TSplitter;
    StaticText1: TStaticText;
    Label4: TLabel;
    EdPecas_M: TdmkEdit;
    EdPesoF_M: TdmkEdit;
    Label1: TLabel;
    Label5: TLabel;
    EdPecas_L: TdmkEdit;
    EdPesoF_L: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrLotesCalcFields(DataSet: TDataSet);
    procedure QrMarcasCalcFields(DataSet: TDataSet);
    procedure EdLoteExit(Sender: TObject);
    procedure EdMarcaExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtOKClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdPecas_LExit(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrMarcas();
    procedure ReopenQrLotes();
  public
    { Public declarations }
    FEmit: Integer;
  end;

  var
  FmFormulasImpBHInsLote: TFmFormulasImpBHInsLote;

implementation

uses UnMyObjects, Module, UMySQLModule, FormulasImpBH;

{$R *.DFM}

procedure TFmFormulasImpBHInsLote.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulasImpBHInsLote.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormulasImpBHInsLote.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulasImpBHInsLote.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  dmkPF.ObtemInfoRegEdit('PageControle', Caption, 0, ktInteger);
end;

procedure TFmFormulasImpBHInsLote.QrLotesCalcFields(DataSet: TDataSet);
begin
  QrLotesNOMETIPO.Value := UnAppListas.DefineNOMETipificacaoCouro(
    -3, QrLotesTipificacao.Value);
end;

procedure TFmFormulasImpBHInsLote.QrMarcasCalcFields(DataSet: TDataSet);
begin
  QrMarcasNOMETIPO.Value := UnAppListas.DefineNOMETipificacaoCouro(
    -3, QrMarcasTipificacao.Value);
end;

procedure TFmFormulasImpBHInsLote.EdLoteExit(Sender: TObject);
begin
  ReopenQrLotes;
end;

procedure TFmFormulasImpBHInsLote.EdMarcaExit(Sender: TObject);
begin
  //EdMarca.Text := Geral.TFT(EdMarca.Text, 0, siPositivo);
  //QrMarcas.Locate('Marca', EdMarca.Text, []);
  ReopenQrMarcas;
end;

procedure TFmFormulasImpBHInsLote.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dmkPF.SalvaInfoRegEdit('PageControl', Caption,
    PageControl1.ActivePageIndex, KtInteger);
end;

procedure TFmFormulasImpBHInsLote.BtOKClick(Sender: TObject);
var
  Controle, MPIn: Integer;
  Pecas, PesoF, Custo: Double;
begin
  if FEmit = 0 then
  begin
    Application.MessageBox('O n�mero de pesagem n�o foi definido!',
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //
  case PageControl1.ActivePageIndex of
    0:
    begin
      PesoF   := Geral.DMV(EdPesoF_L.Text);
      Pecas   := Geral.DMV(EdPecas_L.Text);
      MPIn    := QrLotesControle.Value;
      EdLote.SetFocus;
    end;
    1:
    begin
      PesoF   := Geral.DMV(EdPesoF_M.Text);
      Pecas   := Geral.DMV(EdPecas_M.Text);
      MPIn    := QrMarcasControle.Value;
      EdMarca.SetFocus;
    end;
    else
    begin
      PesoF   := 0;
      Pecas   := 0;
      MPIn    := 0;
    end;
  end;
  if MPIn = 0 then
  begin
    Application.MessageBox('Mat�ria-prima n�o definida!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if PesoF = 0 then
  begin
    Application.MessageBox('Peso n�o definido!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Pecas = 0 then
  begin
    Application.MessageBox('Pe�as n�o definido!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    Custo   := 0; // Calcula Depois ?
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'EmitCus', 'EmitCus', 'Controle');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO emitcus SET Peso=:P0, Pecas=:P1, ');
    Dmod.QrUpd.SQL.Add('Custo=:P2, Formula=:P3, DataEmis=:P4, MPIn=:P5, ');
    Dmod.QrUpd.SQL.Add('Codigo=:Pa, Controle=:Pb');
    Dmod.QrUpd.Params[00].AsFloat    := PesoF;
    Dmod.QrUpd.Params[01].AsFloat    := Pecas;
    Dmod.QrUpd.Params[02].AsFloat    := Custo;
    Dmod.QrUpd.Params[03].AsInteger  := 0;// Depois? QrEmitNumero.Value;
    Dmod.QrUpd.Params[04].AsDateTime := 0;// Depois? QrEmitDataEmis.Value;
    Dmod.QrUpd.Params[05].AsInteger  := MPIn;
    //
    Dmod.QrUpd.Params[06].AsInteger  := FEmit;
    Dmod.QrUpd.Params[07].AsInteger  := Controle;
    Dmod.QrUpd.ExecSQL;
    //RecalculaCustos(QrMPInControle.Value);
    EdPecas_M.Text := '';
    EdPesoF_M.Text := '';
    EdPecas_L.Text := '';
    EdPesoF_L.Text := '';
    EdMarca.Text := '';
    EdLote.Text  := '';
    QrLotes.Close;
    QrMarcas.Close;
    FmFormulasImpBH.AtualizaPesoEPecas;
    FmFormulasImpBH.ReopenLotes;
  finally
    Screen.Cursor := crDefault;
  end;  
end;

procedure TFmFormulasImpBHInsLote.ReopenQrMarcas();
var
  CliInt: Integer;
  Marca: String;
begin
  CliInt := Geral.IMV(EdCliInt.Text);
  Marca  := EdMarca.Text;
  //////////////////////////////////////////////////////////////////////////////
  QrMarcas.Close;
  QrMarcas.SQL.Clear;
  QrMarcas.SQL.Add('SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial');
  QrMarcas.SQL.Add('ELSE cli.Nome END NOMECLIINT, CASE WHEN pro.Tipo=0');
  QrMarcas.SQL.Add('THEN pro.RazaoSocial ELSE pro.Nome END NOMEPROCEDE, mpi.*');
  QrMarcas.SQL.Add('FROM mpin mpi');
  QrMarcas.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=mpi.ClienteI');
  QrMarcas.SQL.Add('LEFT JOIN entidades pro ON pro.Codigo=mpi.Procedencia');
  QrMarcas.SQL.Add('WHERE mpi.ClienteI = '+IntToStr(CliInt));
  if Marca <> '' then
    QrMarcas.SQL.Add('AND mpi.Marca = "' + Marca + '"');
  QrMarcas.SQL.Add('ORDER BY mpi.Marca DESC, mpi.Controle DESC');
  QrMarcas.SQL.Add('LIMIT 1000');
  UnDmkDAC_PF.AbreQuery(QrMarcas, Dmod.MyDB);
  //////////////////////////////////////////////////////////////////////////////
end;

procedure TFmFormulasImpBHInsLote.ReopenQrLotes();
var
  CliInt: Integer;
  Lote: String;
begin
  CliInt := Geral.IMV(EdCliInt.Text);
  Lote   := EdLote.Text;
  //
  QrLotes.Close;
  QrLotes.SQL.Clear;
  QrLotes.SQL.Add('SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial');
  QrLotes.SQL.Add('ELSE cli.Nome END NOMECLIINT, CASE WHEN pro.Tipo=0');
  QrLotes.SQL.Add('THEN pro.RazaoSocial ELSE pro.Nome END NOMEPROCEDE, mpi.*');
  QrLotes.SQL.Add('FROM mpin mpi');
  QrLotes.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=mpi.ClienteI');
  QrLotes.SQL.Add('LEFT JOIN entidades pro ON pro.Codigo=mpi.Procedencia');
  QrLotes.SQL.Add('WHERE mpi.ClienteI = '+IntToStr(CliInt));
  if Trim(Lote) <> '' then
    QrLotes.SQL.Add('AND mpi.Lote = "' + Lote + '"');

  QrLotes.SQL.Add('ORDER BY mpi.Lote DESC, mpi.Controle DESC');
  QrLotes.SQL.Add('LIMIT 1000');
  UnDmkDAC_PF.AbreQuery(QrLotes, Dmod.MyDB);
end;

procedure TFmFormulasImpBHInsLote.BtExcluiClick(Sender: TObject);
begin
  if Application.MessageBox('Confirma a retirada do item selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM emitcus ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P0 ');
    Dmod.QrUpd.Params[0].AsInteger := FmFormulasImpBH.QrLotesControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    FmFormulasImpBH.AtualizaPesoEPecas;
    FmFormulasImpBH.ReopenLotes;
  end;
end;

procedure TFmFormulasImpBHInsLote.EdPecas_LExit(Sender: TObject);
begin
  BtOK.SetFocus;
end;

end.

