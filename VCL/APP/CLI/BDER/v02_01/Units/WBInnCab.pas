unit WBInnCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, frxCross,
  UnAppEnums;

type
  TFmWBInnCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrWBInnCab: TmySQLQuery;
    DsWBInnCab: TDataSource;
    QrWBInnIts: TmySQLQuery;
    DsWBInnIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtCompra: TdmkEditDateTimePicker;
    EdDtCompra: TdmkEdit;
    Label53: TLabel;
    TPDtViagem: TdmkEditDateTimePicker;
    EdDtViagem: TdmkEdit;
    Label3: TLabel;
    TPDtEntrada: TdmkEditDateTimePicker;
    EdDtEntrada: TdmkEdit;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrTransporta: TmySQLQuery;
    DsTransporta: TDataSource;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    Label4: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrWBInnCabCodigo: TIntegerField;
    QrWBInnCabMovimCod: TIntegerField;
    QrWBInnCabEmpresa: TIntegerField;
    QrWBInnCabDtCompra: TDateTimeField;
    QrWBInnCabDtViagem: TDateTimeField;
    QrWBInnCabDtEntrada: TDateTimeField;
    QrWBInnCabFornecedor: TIntegerField;
    QrWBInnCabTransporta: TIntegerField;
    QrWBInnCabPecas: TFloatField;
    QrWBInnCabPesoKg: TFloatField;
    QrWBInnCabAreaM2: TFloatField;
    QrWBInnCabAreaP2: TFloatField;
    QrWBInnCabLk: TIntegerField;
    QrWBInnCabDataCad: TDateField;
    QrWBInnCabDataAlt: TDateField;
    QrWBInnCabUserCad: TIntegerField;
    QrWBInnCabUserAlt: TIntegerField;
    QrWBInnCabAlterWeb: TSmallintField;
    QrWBInnCabAtivo: TSmallintField;
    QrWBInnCabNO_EMPRESA: TWideStringField;
    QrWBInnCabNO_FORNECE: TWideStringField;
    QrWBInnCabNO_TRANSPORTA: TWideStringField;
    QrWBInnItsCodigo: TIntegerField;
    QrWBInnItsControle: TIntegerField;
    QrWBInnItsMovimCod: TIntegerField;
    QrWBInnItsEmpresa: TIntegerField;
    QrWBInnItsMovimID: TIntegerField;
    QrWBInnItsGraGruX: TIntegerField;
    QrWBInnItsPecas: TFloatField;
    QrWBInnItsPesoKg: TFloatField;
    QrWBInnItsAreaM2: TFloatField;
    QrWBInnItsAreaP2: TFloatField;
    QrWBInnItsLk: TIntegerField;
    QrWBInnItsDataCad: TDateField;
    QrWBInnItsDataAlt: TDateField;
    QrWBInnItsUserCad: TIntegerField;
    QrWBInnItsUserAlt: TIntegerField;
    QrWBInnItsAlterWeb: TSmallintField;
    QrWBInnItsAtivo: TSmallintField;
    QrWBInnItsNO_PRD_TAM_COR: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TDBGrid;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    Splitter1: TSplitter;
    QrReclasif: TmySQLQuery;
    DsReclasif: TDataSource;
    QrReclasifCodigo: TIntegerField;
    QrReclasifControle: TIntegerField;
    QrReclasifMovimCod: TIntegerField;
    QrReclasifEmpresa: TIntegerField;
    QrReclasifMovimID: TIntegerField;
    QrReclasifGraGruX: TIntegerField;
    QrReclasifPecas: TFloatField;
    QrReclasifPesoKg: TFloatField;
    QrReclasifAreaM2: TFloatField;
    QrReclasifAreaP2: TFloatField;
    QrReclasifLk: TIntegerField;
    QrReclasifDataCad: TDateField;
    QrReclasifDataAlt: TDateField;
    QrReclasifUserCad: TIntegerField;
    QrReclasifUserAlt: TIntegerField;
    QrReclasifAlterWeb: TSmallintField;
    QrReclasifAtivo: TSmallintField;
    QrReclasifNO_PRD_TAM_COR: TWideStringField;
    QrWBInnItsSrcMovID: TIntegerField;
    QrWBInnItsSrcNivel1: TIntegerField;
    QrWBInnItsSrcNivel2: TIntegerField;
    QrSumWII: TmySQLQuery;
    QrSumWIIPecas: TFloatField;
    QrSumWIISrcNivel2: TIntegerField;
    QrWBInnItsPC_RECLAS: TFloatField;
    QrWBInnItsPallet: TIntegerField;
    QrWBInnItsNO_PALLET: TWideStringField;
    QrReclasifPallet: TIntegerField;
    QrReclasifNO_Pallet: TWideStringField;
    QrWBInnItsSdoVrtArM2: TFloatField;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    Classificao1: TMenuItem;
    QrResulClas: TmySQLQuery;
    QrResulClasCodigo: TIntegerField;
    QrResulClasControle: TIntegerField;
    QrResulClasMovimCod: TIntegerField;
    QrResulClasEmpresa: TIntegerField;
    QrResulClasMovimID: TIntegerField;
    QrResulClasGraGruX: TIntegerField;
    QrResulClasPecas: TFloatField;
    QrResulClasPesoKg: TFloatField;
    QrResulClasAreaM2: TFloatField;
    QrResulClasAreaP2: TFloatField;
    QrResulClasLk: TIntegerField;
    QrResulClasDataCad: TDateField;
    QrResulClasDataAlt: TDateField;
    QrResulClasUserCad: TIntegerField;
    QrResulClasUserAlt: TIntegerField;
    QrResulClasAlterWeb: TSmallintField;
    QrResulClasAtivo: TSmallintField;
    QrResulClasNO_PRD_TAM_COR: TWideStringField;
    QrResulClasPallet: TIntegerField;
    QrResulClasNO_Pallet: TWideStringField;
    frxWET_RECUR_001_01: TfrxReport;
    frxDsResulClas: TfrxDBDataset;
    QrResulClasSrcNivel2: TIntegerField;
    QrResulClasGGX_REFER: TWideStringField;
    QrResulClasNO_ORI_PALLET: TWideStringField;
    QrResulClasPALLET_M2: TFloatField;
    frxDsResulTota: TfrxDBDataset;
    QrResulTota: TmySQLQuery;
    QrResulTotaNO_Pallet: TWideStringField;
    QrResulTotaPecas: TFloatField;
    QrResulTotaGGX_REFER: TWideStringField;
    QrResulTotaGraGruX: TIntegerField;
    QrPalletInn: TmySQLQuery;
    QrResulTotaPERCENT_CLASS: TFloatField;
    QrResulClasPERCENT_CLASS: TFloatField;
    QrResulTotaCalcM2: TFloatField;
    QrWBInnSum: TmySQLQuery;
    QrWBInnSumAreaM2: TFloatField;
    QrWBInnSumPecas: TFloatField;
    frxDsWBInnIts: TfrxDBDataset;
    frxDsWBInnCab: TfrxDBDataset;
    QrWBInnItsObserv: TWideStringField;
    N2: TMenuItem;
    Fichas1: TMenuItem;
    Entradascomestoquevirtual1: TMenuItem;
    Palletsdosclassificados1: TMenuItem;
    frxWET_RECUR_001_02: TfrxReport;
    QrPalletInnCodigo: TIntegerField;
    QrPalletInnControle: TIntegerField;
    QrPalletInnMovimCod: TIntegerField;
    QrPalletInnMovimNiv: TIntegerField;
    QrPalletInnEmpresa: TIntegerField;
    QrPalletInnTerceiro: TIntegerField;
    QrPalletInnMovimID: TIntegerField;
    QrPalletInnDataHora: TDateTimeField;
    QrPalletInnPallet: TIntegerField;
    QrPalletInnGraGruX: TIntegerField;
    QrPalletInnPecas: TFloatField;
    QrPalletInnPesoKg: TFloatField;
    QrPalletInnAreaM2: TFloatField;
    QrPalletInnAreaP2: TFloatField;
    QrPalletInnSrcMovID: TIntegerField;
    QrPalletInnSrcNivel1: TIntegerField;
    QrPalletInnSrcNivel2: TIntegerField;
    QrPalletInnLk: TIntegerField;
    QrPalletInnDataCad: TDateField;
    QrPalletInnDataAlt: TDateField;
    QrPalletInnUserCad: TIntegerField;
    QrPalletInnUserAlt: TIntegerField;
    QrPalletInnAlterWeb: TSmallintField;
    QrPalletInnAtivo: TSmallintField;
    QrPalletInnSdoVrtPeca: TFloatField;
    QrPalletInnObserv: TWideStringField;
    QrPalletInnNO_PRD_TAM_COR: TWideStringField;
    QrPalletInnNO_Pallet: TWideStringField;
    QrPalletInnNO_EMPRESA: TWideStringField;
    frxDsPalletInn: TfrxDBDataset;
    QrPalletInnNO_FORNECE: TWideStringField;
    QrPalletCla: TmySQLQuery;
    QrPalletClaPallet: TIntegerField;
    QrPalletClaDataHora: TDateTimeField;
    QrPalletClaPecas: TFloatField;
    QrPalletClaAreaM2: TFloatField;
    QrPalletClaAreaP2: TFloatField;
    QrPalletClaPesoKg: TFloatField;
    QrPalletClaNO_PRD_TAM_COR: TWideStringField;
    QrPalletClaNO_Pallet: TWideStringField;
    QrPalletClaNO_EMPRESA: TWideStringField;
    QrPalletClaNO_FORNECE: TWideStringField;
    frxWET_RECUR_001_03: TfrxReport;
    frxDsPalletCla: TfrxDBDataset;
    QrWBInnCabValorT: TFloatField;
    QrWBInnItsValorT: TFloatField;
    QrReclasifValorT: TFloatField;
    Label9: TLabel;
    DBEdit15: TDBEdit;
    BtReclasif: TBitBtn;
    PMReclasif: TPopupMenu;
    Incluireclassificao1: TMenuItem;
    Alterareclassificao1: TMenuItem;
    Excluireclassificao1: TMenuItem;
    QrReclasifSrcMovID: TIntegerField;
    QrReclasifSrcNivel1: TIntegerField;
    QrReclasifSrcNivel2: TIntegerField;
    QrResulTotaAreaM2: TFloatField;
    QrWBInnItsSdoVrtPeca: TFloatField;
    QrPalletClaTerceiro: TIntegerField;
    QrPalletClaGraGruX: TIntegerField;
    QrWBInnItsMovimTwn: TIntegerField;
    QrReclasifMovimTwn: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWBInnCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWBInnCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrWBInnCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrWBInnCabBeforeClose(DataSet: TDataSet);
    procedure QrWBInnItsBeforeClose(DataSet: TDataSet);
    procedure QrWBInnItsAfterScroll(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure Classificao1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrResulTotaCalcFields(DataSet: TDataSet);
    procedure frxWET_RECUR_001_01GetValue(const VarName: string;
      var Value: Variant);
    procedure Entradascomestoquevirtual1Click(Sender: TObject);
    procedure Palletsdosclassificados1Click(Sender: TObject);
    procedure Incluireclassificao1Click(Sender: TObject);
    procedure PMReclasifPopup(Sender: TObject);
    procedure Alterareclassificao1Click(Sender: TObject);
    procedure Excluireclassificao1Click(Sender: TObject);
    procedure BtReclasifClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormWBInnIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenWBInnIts(Controle: Integer);
    procedure ReopenReclasif(Controle: Integer);

  end;

var
  FmWBInnCab: TFmWBInnCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, WBInnIts, ModuleGeral,
Principal, WBMovImp;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWBInnCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWBInnCab.MostraFormWBInnIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmWBInnIts, FmWBInnIts, afmoNegarComAviso) then
  begin
    FmWBInnIts.ImgTipo.SQLType := SQLType;
    FmWBInnIts.FQrCab := QrWBInnCab;
    FmWBInnIts.FDsCab := DsWBInnCab;
    FmWBInnIts.FQrIts := QrWBInnIts;
    FmWBInnIts.FDataHora := QrWBInnCabDtEntrada.Value;
    FmWBInnIts.FEmpresa := QrWBInnCabEmpresa.Value;
    FmWBInnIts.FFornece := QrWBInnCabFornecedor.Value;
    if SQLType = stIns then
    begin
      //FmWBInnIts.EdCPF1.ReadOnly := False
    end else
    begin
      FmWBInnIts.EdControle.ValueVariant := QrWBInnItsControle.Value;
      //
      FmWBInnIts.EdGragruX.ValueVariant  := QrWBInnItsGraGruX.Value;
      FmWBInnIts.CBGragruX.KeyValue      := QrWBInnItsGraGruX.Value;
      FmWBInnIts.EdPecas.ValueVariant    := QrWBInnItsPecas.Value;
      FmWBInnIts.EdPesoKg.ValueVariant   := QrWBInnItsPesoKg.Value;
      FmWBInnIts.EdAreaM2.ValueVariant   := QrWBInnItsAreaM2.Value;
      FmWBInnIts.EdAreaP2.ValueVariant   := QrWBInnItsAreaP2.Value;
      FmWBInnIts.EdPallet.ValueVariant   := QrWBInnItsPallet.Value;
      FmWBInnIts.CBPallet.KeyValue       := QrWBInnItsPallet.Value;

      FmWBInnIts.EdValorT.ValueVariant   := QrWBInnItsValorT.Value;

      FmWBInnIts.EdObserv.ValueVariant   := QrWBInnItsObserv.Value;
      //
    end;
    FmWBInnIts.ShowModal;
    FmWBInnIts.Destroy;
  end;
end;

procedure TFmWBInnCab.Palletsdosclassificados1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Pallets: String;
begin
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT Pallet ',
    'FROM wbmovits wmi ',
    'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBInnItsMovimCod.Value),
    'AND wmi.SrcNivel1=' + Geral.FF0(QrWBInnItsCodigo.Value),
    'AND wmi.SrcNivel2=' + Geral.FF0(QrWBInnItsControle.Value),
    'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
    'ORDER BY wmi.Controle ',
    '']);
    Pallets := '';
    Qry.First;
    while not Qry.Eof do
    begin
      Pallets := Pallets + ', ' +
        Geral.FF0(Qry.FieldByName('Pallet').AsInteger);
      //
      Qry.next;
    end;
    Pallets := Copy(Pallets, 3);
    //
    if Pallets <> '' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPalletCla, Dmod.MyDB, [
      'SELECT wmi.GragruX, wmi.Pallet, MAX(DataHora) DataHora,  ',
      'SUM(wmi.Pecas) Pecas,  ',
      'IF(MIN(AreaM2) > 0.01, SUM(wmi.AreaM2), 0) AreaM2,  ',
      'IF(MIN(AreaP2) > 0.01, SUM(wmi.AreaP2), 0) AreaP2,  ',
      'IF(MIN(PesoKg) > 0.001, SUM(wmi.PesoKg), 0) PesoKg,  ',
      'CONCAT(gg1.Nome,   ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet, wmi.Terceiro,  ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,  ',
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE  ',
      'FROM wbmovits wmi   ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX   ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet   ',
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa ',
      'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro  ',
      'WHERE wmi.Pallet IN (' + Pallets + ')  ',
      'GROUP BY wmi.Pallet, wmi.GraGruX, wmi.Empresa, wmi.Terceiro  ',
      '']);
      //Geral.MB_Aviso(QrPalletCla.SQL.Text);
      //
      MyObjects.frxDefineDataSets(frxWET_RECUR_001_03, [
        //DModG.frxDsDono,
        frxDsPalletCla
      ]);
      //
      MyObjects.frxMostra(frxWET_RECUR_001_03,
        'Pallets - (Re)Classificados');
    end else
      Geral.MB_Aviso('Nenhum pallet foi encontrado!');
  finally
    Qry.Free;
  end;
end;

procedure TFmWBInnCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrWBInnCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrWBInnCab, QrWBInnIts);
end;

procedure TFmWBInnCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrWBInnCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrWBInnIts);
  MyObjects.HabilitaMenuItemCabDel(ItsExclui1, QrWBInnIts, QrReclasif);
end;

procedure TFmWBInnCab.PMReclasifPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluireclassificao1, QrWBInnIts);
  MyObjects.HabilitaMenuItemItsUpd(Alterareclassificao1, QrReclasif);
  MyObjects.HabilitaMenuItemItsDel(Excluireclassificao1, QrReclasif);
end;

procedure TFmWBInnCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWBInnCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWBInnCab.DefParams;
begin
  VAR_GOTOTABELA := 'wbinncab';
  VAR_GOTOMYSQLTABLE := QrWBInnCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA');
  VAR_SQLx.Add('FROM wbinncab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor');
  VAR_SQLx.Add('LEFT JOIN entidades trn ON trn.Codigo=wic.transporta');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmWBInnCab.Entradascomestoquevirtual1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPalletInn, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa',
  'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrWBInnCabMovimCod.Value),
  'AND SdoVrtPeca>=0.001',
  'ORDER BY wmi.Controle ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_RECUR_001_02, [
    //DModG.frxDsDono,
    frxDsPalletInn
  ]);
  //
  MyObjects.frxMostra(frxWET_RECUR_001_02,
    'Pallets - Entrada com estoque virtual');
end;

procedure TFmWBInnCab.Estoque1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmWBMovImp, FmWBMovImp, afmoNegarComAviso) then
  begin
    FmWBMovImp.ShowModal;
    FmWBMovImp.Destroy;
  end;
end;

procedure TFmWBInnCab.Excluireclassificao1Click(Sender: TObject);
var
  WBInnCab, WBInnIts, MovimCod, SrcNivel2: Integer;
begin
  WBInnCab    := QrWBInnCabCodigo.Value;
  WBInnIts    := QrWBInnItsControle.Value;
  //
  MovimCod  := QrReclasifMovimCod.Value;
  SrcNivel2 := QrReclasifSrcNivel2.Value;
  //
  if Dmod.ExcluiMovimCodWBMovIts(QrReclasif, QrReclasifMovimCod,
  QrReclasifMovimCod.Value, QrReclasifControle.Value,
  QrReclasifSrcNivel2.Value) then
  begin
    Dmod.AtualizaTotaisWBXxxCab('wbinncab', MovimCod);
    //Dmod.AtualizaSaldoVirtualWBMovIts(SrcNivel2);
    FmWBInnCab.LocCod(WBInnCab, WBInnCab);
    FmWBInnCab.QrWBInnIts.Locate('Controle', WBInnIts, []);
  end;
end;

procedure TFmWBInnCab.Incluireclassificao1Click(Sender: TObject);
var
  Filial, MovimCod, MovimTwn, Codigo, Controle: Integer;
  ValorT, AreaM2: Double;
begin
  Filial   := QrWBInnCabEmpresa.Value;
  MovimCod := QrWBInnItsMovimCod.Value;
  Codigo   := QrWBInnItsCodigo.Value;
  Controle := QrWBInnItsControle.Value;
  ValorT   := QrWBInnItsValorT.Value;
  AreaM2   := QrWBInnItsAreaM2.Value;
  MovimTwn := QrWBInnItsMovimTwn.Value;
  //
  FmPrincipal.MostraFormWBRclIns(stIns, Filial, MovimCod, MovimTwn, Codigo, Controle,
    ValorT, AreaM2, QrReclasif, QrWBInnItsSdoVrtPeca.Value, wbrEntrada);
  ReopenWBInnIts(QrWBInnItsControle.Value);
end;

procedure TFmWBInnCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormWBInnIts(stUpd);
end;

procedure TFmWBInnCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmWBInnCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWBInnCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWBInnCab.ItsExclui1Click(Sender: TObject);
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrWBInnItsCodigo.Value;
  MovimCod := QrWBInnItsMovimCod.Value;
  //
  if Dmod.ExcluiControleWBMovIts(QrWBInnIts, QrWBInnItsControle,
  QrWBInnItsControle.Value, QrWBInnItsSrcNivel2.Value) then
  begin
    Dmod.AtualizaTotaisWBXxxCab('wbinncab', MovimCod);
    FmWBInnCab.LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWBInnCab.ReopenReclasif(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrReclasif, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBInnItsMovimCod.Value),
  'AND wmi.SrcNivel1=' + Geral.FF0(QrWBInnItsCodigo.Value),
  'AND wmi.SrcNivel2=' + Geral.FF0(QrWBInnItsControle.Value),
  'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
  'ORDER BY wmi.Controle ',
  '']);
  //
  QrReclasif.Locate('Controle', Controle, []);
end;

procedure TFmWBInnCab.ReopenWBInnIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumWII, Dmod.MyDB, [
  'SELECT wmi.SrcNivel2, SUM(wmi.Pecas) Pecas ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBInnCabMovimCod.Value),
  'AND wmi.SrcNivel1=' + Geral.FF0(QrWBInnCabCodigo.Value),
  'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
  'GROUP BY wmi.SrcNivel2 ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBInnIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrWBInnCabMovimCod.Value),
  'ORDER BY NO_Pallet, wmi.Controle ',
  '']);
  //
  QrWBInnIts.Locate('Controle', Controle, []);
end;


procedure TFmWBInnCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWBInnCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWBInnCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWBInnCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWBInnCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWBInnCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWBInnCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWBInnCabCodigo.Value;
  Close;
end;

procedure TFmWBInnCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormWBInnIts(stIns);
end;

procedure TFmWBInnCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWBInnCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'wbinncab');
  Empresa := DModG.ObtemFilialDeEntidade(QrWBInnCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmWBInnCab.BtConfirmaClick(Sender: TObject);
var
  DtCompra, DtViagem, DtEntrada, DataHora: String;
  Codigo, MovimCod, Empresa, Fornecedor, Transporta, Terceiro: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
begin
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtCompra       := Geral.FDT_TP_Ed(TPDtCompra.Date, EdDtCompra.Text);
  DtViagem       := Geral.FDT_TP_Ed(TPDtViagem.Date, EdDtViagem.Text);
  DtEntrada      := Geral.FDT_TP_Ed(TPDtEntrada.Date, EdDtEntrada.Text);
  Fornecedor     := EdFornecedor.ValueVariant;
  Transporta     := EdTransporta.ValueVariant;
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(TPDtCompra.DateTime < 2, TPDtCompra, 'Defina uma data de compra!') then Exit;
  DataChegadaInvalida := (TPDtEntrada.Date < TPDtViagem.Date);
  if MyObjects.FIC(DataChegadaInvalida, TPDtViagem, 'Defina uma data de viagem v�lida!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('wbinncab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('wbmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'wbinncab', False, [
  'MovimCod', 'Empresa', 'DtCompra',
  'DtViagem', 'DtEntrada', 'Fornecedor',
  'Transporta'(*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'*)], [
  'Codigo'], [
  MovimCod, Empresa, DtCompra,
  DtViagem, DtEntrada, Fornecedor,
  Transporta(* (*Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*)], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      Dmod.InsereWBMovCab(MovimCod, emidCompra, Codigo)
    else
    begin
      DataHora := DtEntrada;
      Terceiro := Fornecedor;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'wbmovits', False, [
      'Empresa', 'Terceiro', 'DataHora'], ['MovimCod'], [
      Empresa, Terceiro, DataHora], [MovimCod], True);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmWBInnCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'wbinncab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'wbinncab', 'Codigo');
end;

procedure TFmWBInnCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmWBInnCab.Alterareclassificao1Click(Sender: TObject);
var
  Filial, MovimCod, Codigo, Controle, MovimTwn: Integer;
  ValorT, AreaM2: Double;
begin
  Filial   := QrWBInnCabEmpresa.Value;
  MovimCod := QrReclasifMovimCod.Value;
  Codigo   := QrReclasifCodigo.Value;
  Controle := QrReclasifControle.Value;
  ValorT   := QrReclasifValorT.Value;
  AreaM2   := QrReclasifAreaM2.Value;
  MovimTwn := QrReclasifMovimTwn.Value;
  //
  FmPrincipal.MostraFormWBRclIns(stUpd, Filial, MovimCod, MovimTwn,
    Codigo, Controle, ValorT, AreaM2, QrReclasif, 0, wbrEntrada);
  ReopenWBInnIts(QrWBInnItsControle.Value);
end;

procedure TFmWBInnCab.BtReclasifClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMReclasif, BtReclasif);
end;

procedure TFmWBInnCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmWBInnCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
end;

procedure TFmWBInnCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWBInnCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWBInnCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmWBInnCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWBInnCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrWBInnCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWBInnCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWBInnCab.QrResulTotaCalcFields(DataSet: TDataSet);
var
  Total, Item: Double;
begin
  if Dmod.QrControleWBNivGer.Value = Integer(encPecas) then
  begin
    Total := QrWBInnSumPecas.Value;
    Item  := QrResulTotaPecas.Value;
  end else
  begin
    Total := QrWBInnSumAreaM2.Value;
    Item  := QrResulTotaAreaM2.Value;
  end;
  if Total = 0 then
    QrResulTotaPERCENT_CLASS.Value := 0
  else
    QrResulTotaPERCENT_CLASS.Value := 100 * Item / Total;
  QrResulTotaCalcM2.Value := QrWBInnSumAreaM2.Value;
end;

procedure TFmWBInnCab.QrWBInnCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWBInnCab.QrWBInnCabAfterScroll(DataSet: TDataSet);
begin
  ReopenWBInnIts(0);
end;

procedure TFmWBInnCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrWBInnCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmWBInnCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWBInnCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wbinncab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmWBInnCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBInnCab.frxWET_RECUR_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmWBInnCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWBInnCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'wbinncab');
end;

procedure TFmWBInnCab.Classificao1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Niveis2: String;
  //DBCross1ColumnTotal0: TfrxMemoView;
  DBCross1: TfrxDBCrossView;
begin
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT SrcNivel2 ',
    'FROM wbmovits wmi',
    'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBInnItsMovimCod.Value),
    'AND wmi.SrcNivel1=' + Geral.FF0(QrWBInnItsCodigo.Value),
    //'AND wmi.SrcNivel2=' + Geral.FF0(QrWBInnItsControle.Value),
    'AND wmi.MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
    '']);
    Niveis2 := '';
    Qry.First;
    while not Qry.Eof do
    begin
      Niveis2 := Niveis2 + ', ' +
        Geral.FF0(Qry.FieldByName('SrcNivel2').AsInteger);
      //
      Qry.next;
    end;
    Niveis2 := Copy(Niveis2, 3);
    //
    if Niveis2 <> '' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrWBInnSum, Dmod.MyDB, [
      'SELECT SUM(wmi.Pecas) Pecas, SUM(wmi.AreaM2) AreaM2 ',
      'FROM wbmovits wmi',
      'WHERE wmi.Controle IN (' + Niveis2 + ') ',
      '']);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrResulTota, Dmod.MyDB, [
      'SELECT "TOTAL" NO_Pallet, SUM(wmi.Pecas) Pecas, ',
      'SUM(wmi.AreaM2) AreaM2, ',
      'IF(gg1.Referencia <> "", gg1.Referencia, CONCAT("#", ',
      'ggx.Controle)) GGX_REFER, wmi.GraGruX',
      'FROM wbmovits wmi ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
      'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBInnItsMovimCod.Value),
      'AND wmi.SrcNivel1=' + Geral.FF0(QrWBInnItsCodigo.Value),
      //'AND wmi.SrcNivel2=' + Geral.FF0(QrWBInnItsControle.Value),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
      'GROUP BY GraGruX',
      '']);
      //
      // SQL copiado do QrReclasif
      UnDmkDAC_PF.AbreMySQLQuery0(QrResulClas, Dmod.MyDB, [
      'SELECT IF(gg1.Referencia <> "", gg1.Referencia, CONCAT("#", ',
      'ggx.Controle)) GGX_REFER, ',
      'wmi.*, CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
      'FROM wbmovits wmi ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
      'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBInnItsMovimCod.Value),
      'AND wmi.SrcNivel1=' + Geral.FF0(QrWBInnItsCodigo.Value),
      //'AND wmi.SrcNivel2=' + Geral.FF0(QrWBInnItsControle.Value),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
      'ORDER BY wmi.Controle ',
      '']);
      //
      //DBCross1ColumnTotal0 := frxWET_RECUR_001_01.FindObject('DBCross1ColumnTotal0') as TfrxMemoView;
      DBCross1 := frxWET_RECUR_001_01.FindObject('DBCross1') as TfrxDBCrossView;
      DBCross1.CellFields.Clear;
      if Dmod.QrControleWBNivGer.Value = Integer(encPecas) then
      begin
        //DBCross1ColumnTotal0.Memo.Text := 'Pe�as';
        DBCross1.CellFields.Add('Pecas');
      end else
      begin
        //DBCross1ColumnTotal0.Memo.Text := '�rea m�';
        DBCross1.CellFields.Add('AreaM2');
      end;
      //
      MyObjects.frxDefineDataSets(frxWET_RECUR_001_01, [
        DModG.frxDsDono,
        frxDsWBInnCab,
        frxDsWBInnIts,
        frxDsResulClas,
        frxDsResulTota
      ]);
      //
      MyObjects.frxMostra(frxWET_RECUR_001_01, 'Controle Couros');
    end else
      Geral.MB_Aviso('Nenhum item foi encontrado!');
  finally
    Qry.Free;
  end;
  // Erro itens DBGrid!
  // FastReport esta gerando erro no dataset?
  LocCod(QrWBInnCabCodigo.Value,QrWBInnCabCodigo.Value);
end;

procedure TFmWBInnCab.QrWBInnCabBeforeClose(
  DataSet: TDataSet);
begin
  QrWBInnIts.Close;
end;

procedure TFmWBInnCab.QrWBInnCabBeforeOpen(DataSet: TDataSet);
begin
  QrWBInnCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmWBInnCab.QrWBInnItsAfterScroll(DataSet: TDataSet);
begin
  ReopenReclasif(0);
end;

procedure TFmWBInnCab.QrWBInnItsBeforeClose(DataSet: TDataSet);
begin
  QrReclasif.Close;
end;

end.

