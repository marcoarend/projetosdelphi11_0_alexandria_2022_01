unit FormulasImpWE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs, ZCF2,
  UnInternalConsts, UnMsgInt, UnInternalConsts2, Grids, DBGrids, AppListas,
  mySQLDbTables, ComCtrls, Menus, Variants, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkGeral, dmkImage, MyListas, UnDmkProcFunc, dmkEditDateTimePicker,
  UnDmkEnums, frxClass, frxDBSet, dmkCheckBox, dmkEditCalc;

type
  TFmFormulasImpWE = class(TForm)
    QrEspessura1: TmySQLQuery;
    QrEspessura1EMCM: TFloatField;
    QrEspessuras: TmySQLQuery;
    QrDefPecas: TmySQLQuery;
    QrEspessurasCodigo: TIntegerField;
    QrEspessurasLinhas: TWideStringField;
    QrEspessurasEMCM: TFloatField;
    QrEspessurasLk: TIntegerField;
    DsEspessuras: TDataSource;
    DsDefPecas: TDataSource;
    DsFormulas: TDataSource;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    QrDefPecasLk: TIntegerField;
    Panel2: TPanel;
    QrFormulas: TmySQLQuery;
    QrFormulasNumero: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrFormulasClienteI: TIntegerField;
    QrFormulasTipificacao: TIntegerField;
    QrFormulasDataI: TDateField;
    QrFormulasDataA: TDateField;
    QrFormulasTecnico: TWideStringField;
    QrFormulasTempoR: TIntegerField;
    QrFormulasTempoP: TIntegerField;
    QrFormulasTempoT: TIntegerField;
    QrFormulasHorasR: TIntegerField;
    QrFormulasHorasP: TIntegerField;
    QrFormulasHorasT: TIntegerField;
    QrFormulasHidrica: TIntegerField;
    QrFormulasLinhaTE: TIntegerField;
    QrFormulasCaldeira: TIntegerField;
    QrFormulasSetor: TIntegerField;
    QrFormulasEspessura: TIntegerField;
    QrFormulasPeso: TFloatField;
    QrFormulasQtde: TFloatField;
    QrFormulasLk: TIntegerField;
    QrFormulasDataCad: TDateField;
    QrFormulasDataAlt: TDateField;
    QrFormulasUserCad: TIntegerField;
    QrFormulasUserAlt: TIntegerField;
    QrFormulasHHMM_P: TWideStringField;
    QrFormulasHHMM_R: TWideStringField;
    QrFormulasHHMM_T: TWideStringField;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntNOMECI: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    Panel3: TPanel;
    RGImpRecRib: TRadioGroup;
    GBkgTon: TGroupBox;
    RBTon: TRadioButton;
    RBkg: TRadioButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PainelEscolhe: TPanel;
    TabSheet3: TTabSheet;
    PainelConfig: TPanel;
    RGTipoPreco: TRadioGroup;
    RGImprime: TRadioGroup;
    QrEmitCus: TmySQLQuery;
    DsEmitCus: TDataSource;
    QrFormulasAtivo: TSmallintField;
    CkMatricial: TCheckBox;
    CkGrade: TCheckBox;
    CkContinua: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtCancela: TBitBtn;
    ProgressBar1: TProgressBar;
    LaSP_A: TLabel;
    LaSP_B: TLabel;
    LaSP_C: TLabel;
    QrEmitCusTextoECor: TWideStringField;
    QrEmitCusNOME_CLI: TWideStringField;
    QrEmitCusCodigo: TIntegerField;
    QrEmitCusControle: TIntegerField;
    QrEmitCusMPIn: TIntegerField;
    QrEmitCusFormula: TIntegerField;
    QrEmitCusDataEmis: TDateTimeField;
    QrEmitCusPeso: TFloatField;
    QrEmitCusCusto: TFloatField;
    QrEmitCusPecas: TFloatField;
    QrEmitCusAtivo: TSmallintField;
    QrEmitCusMPVIts: TIntegerField;
    QrEmitCusAreaM2: TFloatField;
    QrEmitCusAreaP2: TFloatField;
    QrWBMovIts: TmySQLQuery;
    QrWBMovItsCodigo: TIntegerField;
    QrWBMovItsControle: TIntegerField;
    QrWBMovItsMovimCod: TIntegerField;
    QrWBMovItsEmpresa: TIntegerField;
    QrWBMovItsMovimID: TIntegerField;
    QrWBMovItsGraGruX: TIntegerField;
    QrWBMovItsPecas: TFloatField;
    QrWBMovItsPesoKg: TFloatField;
    QrWBMovItsAreaM2: TFloatField;
    QrWBMovItsAreaP2: TFloatField;
    QrWBMovItsLk: TIntegerField;
    QrWBMovItsDataCad: TDateField;
    QrWBMovItsDataAlt: TDateField;
    QrWBMovItsUserCad: TIntegerField;
    QrWBMovItsUserAlt: TIntegerField;
    QrWBMovItsAlterWeb: TSmallintField;
    QrWBMovItsAtivo: TSmallintField;
    QrWBMovItsNO_PRD_TAM_COR: TWideStringField;
    QrWBMovItsSrcMovID: TIntegerField;
    QrWBMovItsSrcNivel1: TIntegerField;
    QrWBMovItsSrcNivel2: TIntegerField;
    QrWBMovItsPallet: TIntegerField;
    QrWBMovItsNO_PALLET: TWideStringField;
    QrWBMovItsSdoVrtArM2: TFloatField;
    QrWBMovItsSdoVrtPeca: TFloatField;
    QrWBMovItsObserv: TWideStringField;
    QrWBMovItsValorT: TFloatField;
    DsWBMovIts: TDataSource;
    Label13: TLabel;
    PainelEscolhas: TPanel;
    Label10: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    SpeedButton1: TSpeedButton;
    Label12: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPeso: TdmkEdit;
    EdPecas: TdmkEdit;
    CBPeca: TdmkDBLookupComboBox;
    EdMedia: TdmkEdit;
    EdFulao: TdmkEdit;
    EdPeca: TdmkEditCB;
    CBEspessura: TdmkDBLookupComboBox;
    EdAreaM2: TdmkEdit;
    EdAreaP2: TdmkEdit;
    PainelReceita: TPanel;
    Label1: TLabel;
    Label11: TLabel;
    LaData: TLabel;
    EdReceita: TdmkEditCB;
    CBReceita: TdmkDBLookupComboBox;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    TPDataP: TdmkEditDateTimePicker;
    EdEmitGru: TdmkEditCB;
    CBEmitGru: TdmkDBLookupComboBox;
    QrEmitGru: TmySQLQuery;
    DsEmitGru: TDataSource;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    Label14: TLabel;
    CkRetrabalho: TdmkCheckBox;
    QrFormulasRetrabalho: TSmallintField;
    EdEspessura: TdmkEditCB;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Label21: TLabel;
    EdSemiAreaM2: TdmkEditCalc;
    Label22: TLabel;
    EdSemiAreaP2: TdmkEditCalc;
    GroupBox2: TGroupBox;
    Panel9: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    TPDtaCambio: TdmkEditDateTimePicker;
    Label17: TLabel;
    EdBRL_USD: TdmkEdit;
    EdBRL_EUR: TdmkEdit;
    SpeedButton2: TSpeedButton;
    PnMateriaPrima: TPanel;
    Panel10: TPanel;
    Label8: TLabel;
    BtInsWBMovIts: TBitBtn;
    BtDelWBMovIts: TBitBtn;
    DGDados: TDBGrid;
    Panel5: TPanel;
    PnPedido: TPanel;
    Panel11: TPanel;
    Label6: TLabel;
    BtAdiciona: TBitBtn;
    BtExclui: TBitBtn;
    DBGrid1: TDBGrid;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    EdMemo: TMemo;
    procedure BtCancelaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdAreaM2Change(Sender: TObject);
    procedure EdAreaP2Change(Sender: TObject);
    procedure EdAreaPAfterExit(Sender: TObject);
    procedure EdAreaMAfterExit(Sender: TObject);
    procedure CBEspessuraClick(Sender: TObject);
    procedure CBEspessuraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPesoChange(Sender: TObject);
    procedure EdPecasChange(Sender: TObject);
    procedure EdReceitaChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFormulasCalcFields(DataSet: TDataSet);
    procedure BtAdicionaClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrEmitCusBeforeClose(DataSet: TDataSet);
    procedure QrEmitCusAfterScroll(DataSet: TDataSet);
    procedure QrEmitCusAfterOpen(DataSet: TDataSet);
    procedure BtInsWBMovItsClick(Sender: TObject);
    procedure BtDelWBMovItsClick(Sender: TObject);
    procedure QrWBMovItsAfterOpen(DataSet: TDataSet);
    procedure QrWBMovItsBeforeClose(DataSet: TDataSet);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    FAlturaInicial: Integer;
    //
    procedure ConfiguraCambio();
  public
    { Public declarations }
    FEmit: Integer;
    FNomeForm: String;
    //procedure CalculaArea;
    procedure ReopenEmitCus(Controle: Integer);
    procedure ReopenWBMovIts(Controle: Integer);
  end;

var
  FmFormulasImpWE: TFmFormulasImpWE;
  REICalArea : Boolean;
  REISetor : ShortInt;

implementation

uses UnMyObjects, Principal, Formulas, Module, FormulasImpShow, DmkDAC_PF,
  BlueDermConsts, FormulasImpOSs, UMySQLModule, ModEmit, ModuleGeral,
  FormulasImpShowNew;

{$R *.DFM}

{
procedure TFmFormulasImpWE.CalculaArea;
begin
  if CBEspessura.KeyValue = NULL then CBEspessura.KeyValue := 0;
  QrEspessura1.Close;
  QrEspessura1.Params[0].AsInteger := CBEspessura.KeyValue;
  UnDmkDAC_PF.AbreQuery(QrEspessura1, Dmod.MyDB);
  EdAreaM2.ValueVariant :=
    DmModEmit.CalculaAreaPelokg(EdPeso.ValueVariant,
    QrEspessurasEMCM.Value, IC2_PadroesPECouro,  1);
  QrEspessura1.Close;
end;
}

procedure TFmFormulasImpWE.BtCancelaClick(Sender: TObject);
begin
  if FEmit <> 0 then
  begin
    DmModEmit.ExcluiLoteCus(FEmit, emidIndsWE);
    if ImgTipo.SQLType <> stUpd then
       UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Emit', FEmit);
  end;
  Close;
end;

procedure TFmFormulasImpWE.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  MyObjects.PreencheComponente(RGImpRecRib, sListaRecImpApresenta, 5);
  RGImpRecRib.ItemIndex := Dmod.QrControleImpRecRib.Value;
  //
  //STSP.Caption := '';
  MyObjects.Entitula(GB_L, [LaSP_A, LaSP_B, LaSP_C],
  [], '', False, taCenter, 2, 10, 20);

  PageControl1.ActivePageIndex := 0;
  TPDataP.Date := Date;
  FAlturaInicial := Height;
  if VAR_NOMEFORMIMP_ANCESTRAL = 'FmPrincipal' then
  begin
    PageControl2.Visible := IC2_WopcoesImpRecShowLotes;
    EdMemo.Visible := IC2_WopcoesImpRecShowLotes;

    Label6.Visible := IC2_WopcoesImpRecShowMedia;
    EdMedia.Visible := IC2_WopcoesImpRecShowMedia;

    Label2.Visible := IC2_WopcoesImpRecShowEspessura;
    CBEspessura.Visible := IC2_WopcoesImpRecShowEspessura;

    Label4.Visible := IC2_WopcoesImpRecShowM2;
    EdAreaM2.Visible := IC2_WopcoesImpRecShowM2;

    Label5.Visible := IC2_WopcoesImpRecShowP2;
    EdAreaP2.Visible := IC2_WopcoesImpRecShowP2;

  end;

  REICalArea := False;
  UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEspessuras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  if VAR_SETOR = CO_CALEIRO then REISetor := -3;
  if VAR_SETOR = CO_CURTIM then REISetor := -2;
  if VAR_SETOR = CO_RECURT then REISetor := -1;
  if VAR_SETOR = CO_ACABTO then REISetor := -4;
  case REISetor of
    -3 : CBPeca.KeyValue := -10;
    -2 : CBPeca.KeyValue := -10;
    -1 : CBPeca.KeyValue := -9;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
  'SELECT * FROM formulas ',
  'WHERE Ativo=1 ',
  Geral.ATS_If(VAR_SETOR <> CO_VAZIO, ['AND Setor=' + Geral.FF0(REISetor)]),
  'ORDER BY Nome ',
  '']);
  UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
  ConfiguraCambio();
end;

procedure TFmFormulasImpWE.BtConfirmaClick(Sender: TObject);
var
  Codigo, Formula, EmitGru: Integer;
  DataEmis: String;
begin
  //Parei Aqui
  //Tipificacao em vez de setor
  //arrumar Custo
  //
  EmitGru := EdEmitGru.ValueVariant;
  if Dmod.ObrigaInformarEmitGru(FEmit <> 0, EmitGru, EdEmitGru) then
    Exit;
  Formula := EdReceita.ValueVariant;
  if RGTipoPreco.ItemIndex = 2 then
    if not DmModEmit.MostraPrecosAlternativos(dmktfrmSetrEmi_MOLHADO, Formula) then
      Exit;
  //
  if Geral.MB_Pergunta('Confirma que a receita a ser utilizada �:' +
  sLineBreak + QrFormulasNome.Value + sLineBreak + 'E o peso �: ' +
  EdPeso.Text + ' ?') = ID_YES then
  begin
    if FEmit > 0 then
    begin
      DataEmis := Geral.FDT(TPDataP.Date, 1);
      Codigo := FEmit;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'emitcus', False, [
      'Formula', 'DataEmis'], ['Codigo'], [
      Formula, DataEmis], [Codigo], True);
    end;
    BDC_APRESENTACAO_IMPRESSAO := RGImpRecRib.ItemIndex;
    //
    // Configuracoes
    case Dmod.QrControleVerImprRecRib.Value of
      0:
      begin
        FrFormulasImpShow.FReImprime    := False;
        FrFormulasImpShow.FEmit         := FEmit;
        FrFormulasImpShow.FEmitGru      := EmitGru;
        FrFormulasImpShow.FEmitGru_TXT  := CBEmitGru.Text;
        FrFormulasImpShow.FRetrabalho   := Geral.BoolToInt(CkRetrabalho.Checked);
        FrFormulasImpShow.FSourcMP      := CO_SourcMP_WetSome; // Recurtimento!
        FrFormulasImpShow.FDataP        := TPDataP.Date;
        FrFormulasImpShow.FProgressBar1 := ProgressBar1;
        FrFormulasImpShow.FTipoPreco    := RGTipoPreco.ItemIndex;
        FrFormulasImpShow.FCliIntCod    := EdCliInt.ValueVariant;
        FrFormulasImpShow.FCliIntTxt    := CBCliInt.Text;
        FrFormulasImpShow.FFormula      := Formula;
        FrFormulasImpShow.FTipoImprime  := RGImprime.ItemIndex;
        FrFormulasImpShow.FPeso         := EdPeso.ValueVariant;
        FrFormulasImpShow.FQtde         := EdPecas.ValueVariant;
        FrFormulasImpShow.FArea         := EdAreaM2.ValueVariant;
        FrFormulasImpShow.FLinhas       := CBEspessura.Text;
        FrFormulasImpShow.FFulao        := EdFulao.Text;
        FrFormulasImpShow.FDefPeca      := CBPeca.Text;
        FrFormulasImpShow.FHHMM_P       := QrFormulasHHMM_P.Value;
        FrFormulasImpShow.FHHMM_R       := QrFormulasHHMM_R.Value;
        FrFormulasImpShow.FHHMM_T       := QrFormulasHHMM_T.Value;
        FrFormulasImpShow.FSetor        := QrFormulasSetor.Value;
        FrFormulasImpShow.FTipific      := QrFormulasTipificacao.Value;
        FrFormulasImpShow.FCod_Espess   := EdEspessura.ValueVariant;
        FrFormulasImpShow.FCodDefPeca   := EdPeca.ValueVariant;
        FrFormulasImpShow.FSemiAreaM2   := EdSemiAreaM2.ValueVariant;
        FrFormulasImpShow.FSemiRendim   := EdSemiAreaP2.ValueVariant;
        FrFormulasImpShow.FBRL_USD      := EdBRL_USD.ValueVariant;
        FrFormulasImpShow.FBRL_EUR      := EdBRL_EUR.ValueVariant;
        FrFormulasImpShow.FDtaCambio    := TPDtaCambio.Date;
        //
        FrFormulasImpShow.FObs := FmFormulasImpWE.EdMemo.Lines[0]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmFormulasImpWE.EdMemo.Lines[1]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmFormulasImpWE.EdMemo.Lines[2]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmFormulasImpWE.EdMemo.Lines[3]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmFormulasImpWE.EdMemo.Lines[4]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmFormulasImpWE.EdMemo.Lines[5]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmFormulasImpWE.EdMemo.Lines[6];
        //
        if FmFormulasImpWE.CkMatricial.Checked then FrFormulasImpShow.FMatricialNovo := True
          else FrFormulasImpShow.FMatricialNovo := False;
        if FmFormulasImpWE.CkGrade.Checked then FrFormulasImpShow.FGrade := True
          else FrFormulasImpShow.FGrade := False;
        //
        if RGImpRecRib.ItemIndex = 1 then FrFormulasImpShow.FCalcCustos := True
          else FrFormulasImpShow.FCalcCustos := False;
        if RGImpRecRib.ItemIndex = 2 then FrFormulasImpShow.FCalcPrecos := True
          else FrFormulasImpShow.FCalcPrecos := False;
        if FmFormulasImpWE.RBkg.Checked then FrFormulasImpShow.FPesoCalc := 0.01
          else FrFormulasImpShow.FPesoCalc := 10;
        //
        { Desabilitado em 2013-12-09 :: Errado!!!
        if (CBEspessura.KeyValue <> NULL) and (CBEspessura.KeyValue <> 0) then
          FrFormulasImpShow.FMedia := QrEspessurasEMCM.Value
        else begin
        }
          if FrFormulasImpShow.FQtde > 0 then FrFormulasImpShow.FMedia :=
            FrFormulasImpShow.FPeso / FrFormulasImpShow.FQtde
          else FrFormulasImpShow.FMedia := 0;
        //end;
        //
        if MyObjects.FIC(FrFormulasImpShow.FFormula <= 0, EdReceita,
        'Defina uma receita!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShow.FCliIntCod = 0, EdCliInt,
        'Defina um cliente interno!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShow.FPeso <= 0, EdPeso, 'Defina o peso!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShow.FQtde <= 0, EdPecas,
        'Defina a quantidade!') then
          Exit;
        //
        //////////////////////////////////////////////////////////////////////////////
        //
        FrFormulasImpShow.CalculaReceita(QrEmitCus);
        Application.ProcessMessages;
        Hide;
        if CkGrade.Checked then FrFormulasImpShow.Show;
      end;
      1:
      begin
        FrFormulasImpShowNew.FReImprime    := False;
        FrFormulasImpShowNew.FEmit         := FEmit;
        FrFormulasImpShowNew.FEmitGru      := EmitGru;
        FrFormulasImpShowNew.FEmitGru_TXT  := CBEmitGru.Text;
        FrFormulasImpShowNew.FRetrabalho   := Geral.BoolToInt(CkRetrabalho.Checked);
        FrFormulasImpShowNew.FSourcMP      := CO_SourcMP_WetSome; // Recurtimento!
        FrFormulasImpShowNew.FDataP        := TPDataP.Date;
        FrFormulasImpShowNew.FProgressBar1 := ProgressBar1;
        FrFormulasImpShowNew.FTipoPreco    := RGTipoPreco.ItemIndex;
        FrFormulasImpShowNew.FCliIntCod    := EdCliInt.ValueVariant;
        FrFormulasImpShowNew.FCliIntTxt    := CBCliInt.Text;
        FrFormulasImpShowNew.FFormula      := Formula;
        FrFormulasImpShowNew.FTipoImprime  := RGImprime.ItemIndex;
        FrFormulasImpShowNew.FPeso         := EdPeso.ValueVariant;
        FrFormulasImpShowNew.FQtde         := EdPecas.ValueVariant;
        FrFormulasImpShowNew.FArea         := EdAreaM2.ValueVariant;
        FrFormulasImpShowNew.FLinhas       := CBEspessura.Text;
        FrFormulasImpShowNew.FFulao        := EdFulao.Text;
        FrFormulasImpShowNew.FDefPeca      := CBPeca.Text;
        FrFormulasImpShowNew.FHHMM_P       := QrFormulasHHMM_P.Value;
        FrFormulasImpShowNew.FHHMM_R       := QrFormulasHHMM_R.Value;
        FrFormulasImpShowNew.FHHMM_T       := QrFormulasHHMM_T.Value;
        FrFormulasImpShowNew.FSetor        := QrFormulasSetor.Value;
        FrFormulasImpShowNew.FTipific      := QrFormulasTipificacao.Value;
        FrFormulasImpShowNew.FCod_Espess   := EdEspessura.ValueVariant;
        FrFormulasImpShowNew.FCodDefPeca   := EdPeca.ValueVariant;
        FrFormulasImpShowNew.FSemiAreaM2   := EdSemiAreaM2.ValueVariant;
        FrFormulasImpShowNew.FSemiRendim   := EdSemiAreaP2.ValueVariant;
        FrFormulasImpShowNew.FBRL_USD      := EdBRL_USD.ValueVariant;
        FrFormulasImpShowNew.FBRL_EUR      := EdBRL_EUR.ValueVariant;
        FrFormulasImpShowNew.FDtaCambio    := TPDtaCambio.Date;
        //
        FrFormulasImpShowNew.FObs := FmFormulasImpWE.EdMemo.Lines[0]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmFormulasImpWE.EdMemo.Lines[1]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmFormulasImpWE.EdMemo.Lines[2]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmFormulasImpWE.EdMemo.Lines[3]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmFormulasImpWE.EdMemo.Lines[4]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmFormulasImpWE.EdMemo.Lines[5]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmFormulasImpWE.EdMemo.Lines[6];
        //
        if FmFormulasImpWE.CkMatricial.Checked then FrFormulasImpShowNew.FMatricialNovo := True
          else FrFormulasImpShowNew.FMatricialNovo := False;
        if FmFormulasImpWE.CkGrade.Checked then FrFormulasImpShowNew.FGrade := True
          else FrFormulasImpShowNew.FGrade := False;
        //
        if RGImpRecRib.ItemIndex = 1 then FrFormulasImpShowNew.FCalcCustos := True
          else FrFormulasImpShowNew.FCalcCustos := False;
        if RGImpRecRib.ItemIndex = 2 then FrFormulasImpShowNew.FCalcPrecos := True
          else FrFormulasImpShowNew.FCalcPrecos := False;
        if FmFormulasImpWE.RBkg.Checked then FrFormulasImpShowNew.FPesoCalc := 0.01
          else FrFormulasImpShowNew.FPesoCalc := 10;
        //
        { Desabilitado em 2013-12-09 :: Errado!!!
        if (CBEspessura.KeyValue <> NULL) and (CBEspessura.KeyValue <> 0) then
          FrFormulasImpShowNew.FMedia := QrEspessurasEMCM.Value
        else begin
        }
          if FrFormulasImpShowNew.FQtde > 0 then FrFormulasImpShowNew.FMedia :=
            FrFormulasImpShowNew.FPeso / FrFormulasImpShowNew.FQtde
          else FrFormulasImpShowNew.FMedia := 0;
        //end;
        //
        if MyObjects.FIC(FrFormulasImpShowNew.FFormula <= 0, EdReceita,
        'Defina uma receita!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShowNew.FCliIntCod = 0, EdCliInt,
        'Defina um cliente interno!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShowNew.FPeso <= 0, EdPeso, 'Defina o peso!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShowNew.FQtde <= 0, EdPecas,
        'Defina a quantidade!') then
          Exit;
        //
        //////////////////////////////////////////////////////////////////////////////
        //
        FrFormulasImpShowNew.CalculaReceita(QrEmitCus);
        Application.ProcessMessages;
        Hide;
        if CkGrade.Checked then FrFormulasImpShowNew.Show;
      end;
      else
      begin
        Geral.MB_Erro('Vers�o de impress�o de receita de ribeira n�o implementada [1]!');
        Exit;
      end;
    end;








    if CkContinua.Checked = False then Close else
    begin
    if FEmit > 0 then FEmit := UMyMod.BuscaEmLivreY(
      Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
      //STSP.Caption := IntToStr(FEmit);
      MyObjects.Entitula(GB_L, [LaSP_A, LaSP_B, LaSP_C],
      [], Geral.FF0(FEmit), False, taCenter, 2, 10, 20);
      Show;
    end;
  end;
end;

procedure TFmFormulasImpWE.BtDelWBMovItsClick(Sender: TObject);
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrWBMovItsCodigo.Value;
  MovimCod := QrWBMovItsMovimCod.Value;
  //
  if Dmod.ExcluiControleWBMovIts(QrWBMovIts, QrWBMovItsControle,
  QrWBMovItsControle.Value, QrWBMovItsSrcNivel2.Value) then
  begin
    //Dmod.AtualizaTotaisWBXxxCab('wbinncab', MovimCod);
    ReopenWBMovIts(0);
  end;
end;

procedure TFmFormulasImpWE.EdAreaM2Change(Sender: TObject);
var
  Area : Double;
begin
  if REICalArea then exit;
  REICalArea := True;
  Area := EdAreaM2.ValueVariant;
  EdAreaP2.ValueVariant := Area / CO_M2toFT2;
  REICalArea := False;
end;

procedure TFmFormulasImpWE.EdAreaP2Change(Sender: TObject);
var
  Area : Double;
begin
  if REICalArea then exit;
  REICalArea := True;
  Area := EdAreaP2.ValueVariant;
  EdAreaM2.ValueVariant := Area * CO_M2toFT2;
  REICalArea := False;
end;

procedure TFmFormulasImpWE.EdAreaPAfterExit(Sender: TObject);
var
  Area : Double;
begin
  if REICalArea then exit;
  REICalArea := True;
  Area := EdAreaP2.ValueVariant;
  EdAreaM2.ValueVariant := Area * CO_M2toFT2;
  REICalArea := False;
end;

procedure TFmFormulasImpWE.EdAreaMAfterExit(Sender: TObject);
var
  Area : Double;
begin
  if REICalArea then exit;
  REICalArea := True;
  Area := EdAreaM2.ValueVariant;
  EdAreaP2.ValueVariant := Area / CO_M2toFT2;
  REICalArea := False;
end;

procedure TFmFormulasImpWE.CBEspessuraClick(Sender: TObject);
begin
  //CalculaArea;
end;

procedure TFmFormulasImpWE.CBEspessuraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=VK_DELETE then
    CBEspessura.KeyValue := 0;
end;

procedure TFmFormulasImpWE.ConfiguraCambio();
begin
  if VAR_CAMBIO_DATA <> Trunc(Now()) then
    DModG.ReopenCambio();
  //
  EdBRL_USD.ValueVariant := VAR_CAMBIO_USD;
  EdBRL_EUR.ValueVariant := VAR_CAMBIO_EUR;
  TPDtaCambio.Date       := VAR_CAMBIO_DATA;
end;

procedure TFmFormulasImpWE.EdPesoChange(Sender: TObject);
var
  Peso, Qtde, Media : Double;
begin
  Peso := EdPeso.ValueVariant;
  Qtde := EdPecas.ValueVariant;
  if Qtde > 0 then Media := Peso / Qtde else Media := 0;
  EdMedia.ValueVariant := Media;
  //CalculaArea;
end;

procedure TFmFormulasImpWE.EdPecasChange(Sender: TObject);
var
  Peso, Qtde, Media : Double;
begin
  Peso := EdPeso.ValueVariant;
  Qtde := EdPecas.ValueVariant;
  if Qtde > 0 then Media := Peso / Qtde else Media := 0;
  EdMedia.ValueVariant := Media;
end;

procedure TFmFormulasImpWE.EdReceitaChange(Sender: TObject);
begin
  EdCliInt.ValueVariant := QrFormulasClienteI.Value;
  CBCliInt.KeyValue := QrFormulasClienteI.Value;
  CkRetrabalho.Checked := QrFormulasRetrabalho.Value > 0;
end;

procedure TFmFormulasImpWE.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if FEmit = 0 then
  begin
    BtAdiciona.Enabled := False;
    BtExclui.Enabled := False;
  end;
  //
  MyObjects.Entitula(GB_L, [LaSP_A, LaSP_B, LaSP_C],
  [], Geral.FF0(FEmit), False, taCenter, 2, 10, 20);
end;

procedure TFmFormulasImpWE.FormDestroy(Sender: TObject);
begin
  VAR_NOMEFORMIMP_ANCESTRAL := CO_VAZIO;
end;

procedure TFmFormulasImpWE.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulasImpWE.QrEmitCusAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrEmitCus.RecordCount > 0;
  //
  BtInsWBMovIts.Enabled := Habilita;
end;

procedure TFmFormulasImpWE.QrEmitCusAfterScroll(DataSet: TDataSet);
begin
  ReopenWBMovIts(0)
end;

procedure TFmFormulasImpWE.QrEmitCusBeforeClose(DataSet: TDataSet);
begin
  BtInsWBMovIts.Enabled := False;
  QrWBMovIts.Close;
end;

procedure TFmFormulasImpWE.QrFormulasCalcFields(DataSet: TDataSet);
begin
  QrFormulasHHMM_P.Value := dmkPF.HorasMH(QrFormulasTempoP.Value, False);
  QrFormulasHHMM_R.Value := dmkPF.HorasMH(QrFormulasTempoR.Value, False);
  QrFormulasHHMM_T.Value := dmkPf.HorasMH(QrFormulasTempoT.Value, False);
end;

procedure TFmFormulasImpWE.QrWBMovItsAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrWBMovIts.RecordCount > 0;
  BtDelWBMovIts.Enabled := Habilita;
end;

procedure TFmFormulasImpWE.QrWBMovItsBeforeClose(DataSet: TDataSet);
begin
  BtDelWBMovIts.Enabled := False;
end;

procedure TFmFormulasImpWE.ReopenEmitCus(Controle: Integer);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrEmitCus, Dmod.MyDB, [
  'SELECT CONCAT(mpi.Texto, " ", mpi.CorTxt) TextoECor,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_CLI,',
  'ecu.* ',
  'FROM emitcus ecu',
  'LEFT JOIN mpvits mpi ON mpi.Controle=ecu.MPVIts',
  'LEFT JOIN mpp mpp ON mpp.Codigo=mpi.Pedido ',
  'LEFT JOIN entidades ent ON ent.Codigo=mpp.Cliente ',
  'WHERE ecu.Codigo=' + Geral.FF0(FEmit),
  '']);
  //
  QrEmitCus.Locate('Controle', controle, []);
end;

procedure TFmFormulasImpWE.ReopenWBMovIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBMovIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'WHERE wmi.MovimID=' + Geral.FF0(Integer(emidIndsWE)),
  'AND wmi.Codigo=' + Geral.FF0(QrEmitCusMPVIts.Value),
  'AND wmi.LnkNivXtr1=' + Geral.FF0(QrEmitCusCodigo.Value),
  'AND wmi.LnkNivXtr2=' + Geral.FF0(QrEmitCusControle.Value),
  //'ORDER BY NO_Pallet, wmi.Controle ',
  'ORDER BY wmi.Controle ',
  '']);
  //
  QrWBMovIts.Locate('Controle', Controle, []);
end;

procedure TFmFormulasImpWE.BtAdicionaClick(Sender: TObject);
var
  Controle: Integer;
begin
  if FEmit > 0 then
  begin
    Application.CreateForm(TFmFormulasImpOSs, FmFormulasImpOSs);
    FmFormulasImpOSs.ImgTipo.SQLType := stIns;
    FmFormulasImpOSs.FTipoReceitas := dmktfrmSetrEmi_MOLHADO;
    //
    FmFormulasImpOSs.FEmit := FEmit <> 0;
    FmFormulasImpOSs.F_EmitCus := 'emitcus';
    FmFormulasImpOSs.FCodigo := FEmit;
    FmFormulasImpOSs.FControle := 0;
    FmFormulasImpOSs.FQryLote := QrEmitCus;
    FmFormulasImpOSs.FQryExe := Dmod.QrUpd;
    //
    FmFormulasImpOSs.ShowModal;
    Controle := FmFormulasImpOSs.FControle;
    FmFormulasImpOSs.Destroy;
    DmModEmit.AtualizaQuantidadesSourcMP_WetSome('emitcus', Dmod.MyDB,
      FEmit, EdPeso, EdPecas, EdAreaM2);
    ReopenEmitCus(Controle);
  end;
end;

procedure TFmFormulasImpWE.SpeedButton1Click(Sender: TObject);
begin
  FmPrincipal.CadastrodeDefPecas();
  QrDefPecas.Close;
  UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
end;

procedure TFmFormulasImpWE.SpeedButton2Click(Sender: TObject);
begin
  FmPrincipal.CadastroDeCambio();
  ConfiguraCambio();
end;

procedure TFmFormulasImpWE.BtExcluiClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a retirada do item selecionado?') = ID_YES then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDb, [
    'DELETE FROM emitcus ',
    'WHERE Controle=' + Geral.FF0(QrEmitCusControle.Value),
    '']);
    //
    DmModEmit.AtualizaQuantidadesSourcMP_WetSome('emitcus', Dmod.MyDB,
      FEmit, EdPeso, EdPecas, EdAreaM2);
    ReopenEmitCus(0);
  end;
end;

procedure TFmFormulasImpWE.BtInsWBMovItsClick(Sender: TObject);
const
  Controle = 0;
var
  Emit, EmitCus, MPVIts, Empresa: Integer;
begin
  Emit    := QrEmitCusCodigo.Value;
  EmitCus := QrEmitCusControle.Value;
  MPVIts  := QrEmitCusMPVIts.Value;
  Empresa := DModG.ObtemFilialDeEntidade(EdCliInt.ValueVariant);
  FmPrincipal.MostraFormWBIndsWE(stIns, Controle, MPVIts, Emit, EmitCus,
  Empresa, QrWBMovIts, fiwPesagem);
end;

//Setor inv�lido para caleiro ou curtimento na defini��o estoque de couros na emiss�o de pesagem!

end.

