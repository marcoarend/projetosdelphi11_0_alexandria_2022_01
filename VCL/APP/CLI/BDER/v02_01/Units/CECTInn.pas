unit CECTInn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkDBGrid, DB, mySQLDbTables,
  dmkEdit, dmkEditCB, DBCtrls, dmkDBLookupComboBox, ComCtrls,
  dmkEditDateTimePicker, dmkLabel, dmkGeral, MyDBCheck, frxClass, frxDBSet,
  Menus, UnDmkProcFunc, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmCECTInn = class(TForm)
    Panel1: TPanel;
    PnPesquisa: TPanel;
    Panel4: TPanel;
    PnEdita: TPanel;
    PnGrades: TPanel;
    GradeInn: TdmkDBGrid;
    QrCECTInn: TmySQLQuery;
    DsCECTInn: TDataSource;
    QrCECTInnNOMECLI: TWideStringField;
    QrCECTInnCodigo: TIntegerField;
    QrCECTInnCliente: TIntegerField;
    QrCECTInnDataE: TDateField;
    QrCECTInnNFInn: TIntegerField;
    QrCECTInnNFRef: TIntegerField;
    QrCECTInnInnQtdkg: TFloatField;
    QrCECTInnInnQtdPc: TFloatField;
    QrCECTInnInnQtdVal: TFloatField;
    QrCECTInnSdoQtdkg: TFloatField;
    QrCECTInnSdoQtdPc: TFloatField;
    QrCECTInnSdoQtdVal: TFloatField;
    GroupBox1: TGroupBox;
    QrClientesPesq: TmySQLQuery;
    DsClientesPesq: TDataSource;
    QrClientesPesqCodigo: TIntegerField;
    QrClientesPesqNOMECLI: TWideStringField;
    TPDataE: TdmkEditDateTimePicker;
    EdNFInn: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdInnQtdkg: TdmkEdit;
    EdClienteEdit: TdmkEditCB;
    Label5: TLabel;
    CBClienteEdit: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdInnQtdPc: TdmkEdit;
    Label7: TLabel;
    EdInnQtdVal: TdmkEdit;
    EdNFRef: TdmkEdit;
    Label8: TLabel;
    GradeIts: TDBGrid;
    Splitter1: TSplitter;
    QrCECTInnIts: TmySQLQuery;
    DsCECTInnIts: TDataSource;
    QrCECTInnItsCodigo: TIntegerField;
    QrCECTInnItsControle: TIntegerField;
    QrCECTInnItsCECTInn: TIntegerField;
    QrCECTInnItsOutQtdkg: TFloatField;
    QrCECTInnItsOutQtdPc: TFloatField;
    QrCECTInnItsOutQtdVal: TFloatField;
    QrCECTInnItsDataS: TDateField;
    QrCECTInnItsNFOut: TIntegerField;
    QrClientesEdit: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientesEdit: TDataSource;
    CkContinuar: TCheckBox;
    RGAbertos: TRadioGroup;
    Panel3: TPanel;
    CBClientePesq: TdmkDBLookupComboBox;
    EdClientePesq: TdmkEditCB;
    Label1: TLabel;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    CkIni: TCheckBox;
    CkFim: TCheckBox;
    frxCECTInn: TfrxReport;
    frxDsCECTInn: TfrxDBDataset;
    PMImpressao: TPopupMenu;
    Imprimeestoquepesquisado1: TMenuItem;
    Removeordenao1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBControla: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtDevolve: TBitBtn;
    BtImpressao: TBitBtn;
    BtSaida: TBitBtn;
    GBConfirma: TGroupBox;
    Panel2: TPanel;
    Panel7: TPanel;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdClientePesqChange(Sender: TObject);
    procedure CkAbertosClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrCECTInnBeforeClose(DataSet: TDataSet);
    procedure QrCECTInnAfterScroll(DataSet: TDataSet);
    procedure BtDevolveClick(Sender: TObject);
    procedure RGAbertosClick(Sender: TObject);
    procedure CkIniClick(Sender: TObject);
    procedure CkFimClick(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure BtImpressaoClick(Sender: TObject);
    procedure Removeordenao1Click(Sender: TObject);
    procedure Imprimeestoquepesquisado1Click(Sender: TObject);
    procedure frxCECTInnGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FCriando: Boolean;
  public
    { Public declarations }
    procedure MostraEdicao(Acao: TSQLType);
    procedure ReopenCECTInn(Codigo: Integer);
    //procedure ReopenCECTOut(Codigo: Integer);
    procedure ReopenCECTInnIts(Controle: Integer);
  end;

  var
  FmCECTInn: TFmCECTInn;

implementation

uses UnMyObjects, Module, UMySQLModule, CECTOut;

{$R *.DFM}

procedure TFmCECTInn.BitBtn1Click(Sender: TObject);
begin
  MostraEdicao(stLok);
end;

procedure TFmCECTInn.BitBtn2Click(Sender: TObject);
var
  Cliente, Codigo: Integer;
  DataE: String;
  SdoQtdkg, SdoQtdPc, SdoQtdVal,
  InnQtdkg, InnQtdPc, InnQtdVal: Double;
begin
  Cliente   := Geral.IMV(EdClienteEdit.Text);
  if Cliente = 0 then
  begin
    Application.MessageBox('Defina o cliente!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  DataE     := Geral.FDT(TPDataE.Date, 1);
  InnQtdkg  := EdInnQtdkg.ValueVariant;
  InnQtdPc  := EdInnQtdPc.ValueVariant;
  InnQtdVal := EdInnQtdVal.ValueVariant;
  if ImgTipo.SQLType = stUpd then
  begin
    SdoQtdkg  := QrCECTInnSdoQtdkg.Value;
    SdoQtdPc  := QrCECTInnSdoQtdPc.Value;
    SdoQtdVal := QrCECTInnSdoQtdVal.Value;
  end else begin
    SdoQtdkg  := InnQtdkg;
    SdoQtdPc  := InnQtdPc;
    SdoQtdVal := InnQtdVal;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('cectinn', 'Codigo', ImgTipo.SQLType,
    QrCECTInnCodigo.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cectinn', False, [
    'Cliente', 'DataE',
    'NFInn', 'NFRef',
    'InnQtdkg', 'InnQtdPc', 'InnQtdVal',
    'SdoQtdkg', 'SdoQtdPc', 'SdoQtdVal'
  ], ['Codigo'], [
    Cliente, DataE,
    EdNFInn.ValueVariant, EdNFRef.ValueVariant,
    InnQtdkg, InnQtdPc, InnQtdVal,
    SdoQtdkg, SdoQtdPc, SdoQtdVal
  ], [Codigo], True) then
  begin
    Dmod.AtualizaEstoqueCECT(Codigo, 0);
    ReopenCECTInn(Codigo);
    if CkContinuar.Checked  and CkContinuar.Visible then
    begin
      EdNFInn.ValueVariant      := 0;//EdNFInn.ValueVariant + 1;
      EdNFRef.ValueVariant      := 0;//EdNFRef.ValueVariant + 1;
      EdInnQtdkg.ValueVariant   := 0;
      EdInnQtdPc.ValueVariant   := 0;
      EdInnQtdVal.ValueVariant  := 0;
      //
      TPDataE.SetFocus;
    end else MostraEdicao(stLok);
  end;
end;

procedure TFmCECTInn.BtImpressaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImpressao, BtImpressao);
end;

procedure TFmCECTInn.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(stUpd);
end;

procedure TFmCECTInn.BtDevolveClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCECTOut, FmCECTOut, afmoNegarComAviso) then
  begin
    FmCECTOut.ShowModal;
    FmCECTOut.Destroy;
  end;
end;

procedure TFmCECTInn.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(stIns);
end;

procedure TFmCECTInn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCECTInn.CkAbertosClick(Sender: TObject);
begin
  ReopenCECTInn(QrCECTInnCodigo.Value);
end;

procedure TFmCECTInn.CkFimClick(Sender: TObject);
begin
  ReopenCECTInn(QrCECTInnCodigo.Value);
end;

procedure TFmCECTInn.CkIniClick(Sender: TObject);
begin
  ReopenCECTInn(QrCECTInnCodigo.Value);
end;

procedure TFmCECTInn.EdClientePesqChange(Sender: TObject);
begin
  ReopenCECTInn(QrCECTInnCodigo.Value);
end;

procedure TFmCECTInn.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCECTInn.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCriando := True;
  //
  GBConfirma.Align := alClient;
  UnDmkDAC_PF.AbreQuery(QrClientesEdit, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientesPesq, Dmod.MyDB);
  TPIni.date := Date - 30;
  TPFim.date := Date;
  //
  FCriando := False;
end;

procedure TFmCECTInn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCECTInn.frxCECTInnGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VFR_ORDENADO') = 0 then
    Value := Trim(QrCECTInn.SortFieldNames) <> ''
end;

procedure TFmCECTInn.Imprimeestoquepesquisado1Click(Sender: TObject);
const
  SubTotal = 'Sub-total ';
var
  Campo, Titulo: String;
  k, i: Integer;
begin
  Campo := QrCECTInn.SortFieldNames;
  k := pos(' ', Campo);
  if k > 2 then
    Campo := Copy(Campo, 1, k);
  Campo := Trim(Campo);
  if Campo = '' then
    Campo := 'NOMECLI';
  Titulo := '? ? ?';
  for i := 0 to QrCECTInn.FieldCount - 1 do
  begin
    if QrCECTInn.Fields[i].FieldName = Campo then
      Titulo := QrCECTInn.Fields[i].DisplayLabel;
  end;
  //
  frxCECTInn.Variables['VARF_GRUPO1']   := ''''+'frxDsCECTInn."'+Campo+'"''';
  frxCECTInn.Variables['VARF_HEADR1']   := ''''+Titulo+': [frxDsCECTInn."'+Campo+'"]''';
  frxCECTInn.Variables['VARF_FOOTR1']   := ''''+SubTotal+Titulo+': [frxDSCECTInn."'+Campo+'"]''';

  MyObjects.frxMostra(frxCECTInn, 'Estoque de couros de terceiros');
end;

procedure TFmCECTInn.MostraEdicao(Acao: TSQLType);
begin
  case Acao of
    stIns, stUpd:
    begin
      PnEdita.Visible    := True;
      GBConfirma.Visible := True;
      GBControla.Visible := False;
      PnPesquisa.Enabled := False;
      PnGrades.Enabled   := False;
      if Acao = stUpd then
      begin
        TPDataE.Date               := QrCECTInnDataE.Value;
        EdClienteEdit.ValueVariant := QrCECTInnCliente.Value;
        CBClienteEdit.KeyValue     := QrCECTInnCliente.Value;
        EdNFInn.ValueVariant       := QrCECTInnNFInn.Value;
        EdNFRef.ValueVariant       := QrCECTInnNFRef.Value;
        EdInnQtdkg.ValueVariant    := QrCECTInnInnQtdkg.Value;
        EdInnQtdPc.ValueVariant    := QrCECTInnInnQtdPc.Value;
        EdInnQtdVal.ValueVariant   := QrCECTInnInnQtdVal.Value;
        //
        if QrCECTInnIts.RecordCount > 0 then
        begin
          EdClienteEdit.Enabled := False;
          CBClienteEdit.Enabled := False;
          Application.MessageBox(PChar('N�o ser� poss�vel alterar o cliente, ' +
          'pois j� existe devolu��o para esta NF!'), 'Aviso', MB_OK+MB_ICONWARNING);
        end else begin
          EdClienteEdit.Enabled := True;
          CBClienteEdit.Enabled := True;
        end;
        CkContinuar.Visible := False;
      end else CkContinuar.Visible := True;
      TPDataE.SetFocus;
    end;
    //stDel: ;
    //stCpy: ;
    stLok:
    begin
      GBControla.Visible := True;
      PnPesquisa.Enabled := True;
      PnGrades.Enabled   := True;
      GBConfirma.Visible := False;
      PnEdita.Visible    := False;
    end;
  end;
  ImgTipo.SQLType := Acao;
end;

procedure TFmCECTInn.QrCECTInnAfterScroll(DataSet: TDataSet);
begin
  ReopenCECTInnIts(QrCECTInnItsControle.Value);
end;

procedure TFmCECTInn.QrCECTInnBeforeClose(DataSet: TDataSet);
begin
  QrCECTInnIts.Close;
end;

procedure TFmCECTInn.Removeordenao1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  QrCECTInn.SortFieldNames := '';
  if QrCECTInn.State <> dsInactive then
  begin
    Codigo := QrCECTInnCodigo.Value;
    QrCECTInn.Close;
    UnDmkDAC_PF.AbreQuery(QrCECTInn, Dmod.MyDB);
    QrCECTInn.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmCECTInn.ReopenCECTInn(Codigo: Integer);
var
  Cliente: Integer;
begin
  if FCriando then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    Cliente := Geral.IMV(EdClientePesq.Text);
    //
    QrCECTInn.Close;
    QrCECTInn.SQL.Clear;
    QrCECTInn.SQL.Add('SELECT IF(cli.Tipo=0, RazaoSocial, cli.Nome) NOMECLI, inn.*');
    QrCECTInn.SQL.Add('FROM cectinn inn');
    QrCECTInn.SQL.Add('LEFT JOIN entidades cli ON inn.Cliente=cli.Codigo');
    QrCECTInn.SQL.Add(dmkPF.SQL_Periodo('WHERE inn.DataE ',
      TPIni.date, TPFim.Date, CkIni.Checked, CkFim.Checked));
    //QrCECTInn.SQL.Add('WHERE inn.Codigo>-1000');
    if Cliente <> 0 then
      QrCECTInn.SQL.Add(' AND inn.Cliente=' + FormatFloat('0', Cliente));
    case RGAbertos.ItemIndex of
      0: QrCECTInn.SQL.Add('AND inn.SdoQtdPC > 0');
      1: QrCECTInn.SQL.Add('AND inn.SdoQtdPC = 0');
    end;
    UnDmkDAC_PF.AbreQuery(QrCECTInn, Dmod.MyDB);
    //
    if Codigo > 0 then
      QrCECTInn.Locate('Codigo', Codigo, []);
  finally
    Screen.Cursor := crDefault;
  end;
  //
end;

procedure TFmCECTInn.ReopenCECTInnIts(Controle: Integer);
begin
  QrCECTInnIts.Close;
  QrCECTInnIts.Params[0].AsInteger := QrCECTInnCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrCECTInnIts, Dmod.MyDB);
  //
  if Controle > 0 then
    QrCECTInnIts.Locate('Controle', Controle, []);
end;

procedure TFmCECTInn.RGAbertosClick(Sender: TObject);
begin
  ReopenCECTInn(QrCECTInnCodigo.Value);
end;

procedure TFmCECTInn.TPFimClick(Sender: TObject);
begin
  ReopenCECTInn(QrCECTInnCodigo.Value);
end;

procedure TFmCECTInn.TPIniClick(Sender: TObject);
begin
  ReopenCECTInn(QrCECTInnCodigo.Value);
end;

end.

