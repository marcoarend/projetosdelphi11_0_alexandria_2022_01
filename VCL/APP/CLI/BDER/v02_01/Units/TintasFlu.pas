unit TintasFlu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, dmkLabel, Mask, DBCtrls,
  dmkEdit, dmkDBEdit, dmkDBLookupComboBox, dmkEditCB, DB, mySQLDbTables,
  dmkImage, UnDmkEnums, DmkDAC_PF, dmkRadioGroup;

type
  TFmTintasFlu = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    DBEdNumero: TdmkDBEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    DBEdit15: TDBEdit;
    Label27: TLabel;
    Panel4: TPanel;
    EdOrdem: TdmkEdit;
    Label4: TLabel;
    EdNome: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdCodigo: TdmkEdit;
    CkContinuar: TCheckBox;
    EdGramasM2: TdmkEdit;
    Label7: TLabel;
    EdTintasTin: TdmkEditCB;
    CBTintasTin: TdmkDBLookupComboBox;
    Label8: TLabel;
    QrTintasTin: TmySQLQuery;
    QrTintasTinCodigo: TIntegerField;
    QrTintasTinNome: TWideStringField;
    DsTintasTin: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Label9: TLabel;
    EdInfoCargM2: TdmkEdit;
    EdDescri: TdmkEdit;
    Label10: TLabel;
    RGOpProc: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGOpProcClick(Sender: TObject);
  private
    { Private declarations }
    procedure VerificaOrdem(ZeraAntes: Boolean);
  public
    { Public declarations }
  end;

  var
  FmTintasFlu: TFmTintasFlu;

implementation

uses UnMyObjects, TintasCab, Module, UMySQLModule;

{$R *.DFM}

procedure TFmTintasFlu.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := UMyMod.BuscaEmLivreY_Def('TintasFlu', 'Codigo', ImgTipo.SQLType,
              FmTintasCab.QrTintasFluCodigo.Value);
  //
  EdCodigo.ValueVariant := Codigo;
  //
  if EdInfoCargM2.Enabled = False then
    EdInfoCargM2.ValueVariant := 0;
  //
  if EdGramasM2.Enabled = False then
    EdGramasM2.ValueVariant := 0;
  //
  if UMyMod.ExecSQLInsUpdFm(FmTintasFlu, ImgTipo.SQLType, 'TintasFlu', Codigo,
    Dmod.QrUpd) then
  begin
    FmTintasCab.ReopenTintasFlu(Codigo);
    //
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdTintasTin.ValueVariant := 0;
      CBTintasTin.KeyValue     := 0;
      EdNome.Text              := '';
      VerificaOrdem(True);
      EdOrdem.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmTintasFlu.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTintasFlu.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  VerificaOrdem(False);
end;

procedure TFmTintasFlu.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrTintasTin.Close;
  QrTintasTin.Params[0].AsInteger := FmTintasCab.QrTintasCabNumero.Value;
  UnDmkDAC_PF.AbreQuery(QrTintasTin, Dmod.MyDB);
  //
  RGOpProc.Columns := FmTintasCab.FOpProc.Count;
  RGOpProc.Items   := FmTintasCab.FOpProc;
end;

procedure TFmTintasFlu.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTintasFlu.RGOpProcClick(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := RGOpProc.ItemIndex <> 1;
  //
  EdInfoCargM2.Enabled := Enab;
  EdGramasM2.Enabled   := Enab;
end;

procedure TFmTintasFlu.VerificaOrdem(ZeraAntes: Boolean);
begin
  if ZeraAntes then
    EdOrdem.ValueVariant := 0;
  if EdOrdem.ValueVariant = 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT MAX(Ordem) Ordem');
    Dmod.QrAux.SQL.Add('FROM tintasflu');
    Dmod.QrAux.SQL.Add('WHERE Numero=:P0');
    Dmod.QrAux.Params[0].AsInteger := FmTintasCab.QrTintasCabNumero.Value;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    EdOrdem.ValueVariant := Dmod.QrAux.FieldByName('Ordem').AsInteger + 1;
  end;
end;

end.

