unit EmitMopp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  UnAppPF;

type
  TFmEmitMopp = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label6: TLabel;
    EdCodigo: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    RGOperPosStatus: TRadioGroup;
    Label12: TLabel;
    TPIni: TdmkEditDateTimePicker;
    EdIni: TdmkEdit;
    TPFim: TdmkEditDateTimePicker;
    EdFim: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdQtdDon: TdmkEdit;
    EdControle: TdmkEdit;
    Label4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEmitMopp: TFmEmitMopp;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmEmitMopp.BtOKClick(Sender: TObject);
var
  DthIni, DthFim: String;
  Codigo, Controle, Status, Grandz: Integer;
  QtdDon: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  Status         := RGOperPosStatus.ItemIndex;
  Grandz         := 0;
  QtdDon         := EdQtdDon.ValueVariant;
  DthIni         := Geral.FDT(TPIni.Date, 1) + ' ' + EdIni.Text;
  DthFim         := Geral.FDT(TPFim.Date, 1) + ' ' + EdFim.Text;
  //
  Controle := UMyMod.BPGS1I32('emitmopp', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'emitmopp', False, [
  'Codigo', 'Status', 'Grandz',
  'QtdDon', 'DthIni', 'DthFim'], [
  'Controle'], [
  Codigo, Status, Grandz,
  QtdDon, DthIni, DthFim], [
  Controle], True) then
  begin
    Close;
  end;
end;

procedure TFmEmitMopp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmitMopp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmitMopp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  AppPF.ConfiguraRGMapaOperPosProc(RGOperPosStatus, (*Habilita*) True, (*Default*) 1);
  TPIni.Date := Now();
  TPFim.Date := Now();
end;

procedure TFmEmitMopp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
