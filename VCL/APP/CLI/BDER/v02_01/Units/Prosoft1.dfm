object FmProsoft1: TFmProsoft1
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: [Exporta Prosoft]'
  ClientHeight = 646
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 229
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 465
      Height = 229
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 8
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label2: TLabel
        Left = 380
        Top = 56
        Width = 63
        Height = 13
        Caption = 'NCM padr'#227'o:'
      end
      object Label3: TLabel
        Left = 8
        Top = 108
        Width = 298
        Height = 13
        Caption = 'CFOP padr'#227'o produtos qu'#237'micos comprados no mesmo estado:'
      end
      object Label5: TLabel
        Left = 8
        Top = 132
        Width = 291
        Height = 13
        Caption = 'CFOP padr'#227'o produtos qu'#237'micos comprados em outro estado:'
      end
      object Label8: TLabel
        Left = 8
        Top = 156
        Width = 252
        Height = 13
        Caption = 'CFOP padr'#227'o materiais comprados no mesmo estado:'
      end
      object Label9: TLabel
        Left = 8
        Top = 180
        Width = 245
        Height = 13
        Caption = 'CFOP padr'#227'o materiais comprados em outro estado:'
      end
      object Label10: TLabel
        Left = 8
        Top = 204
        Width = 109
        Height = 13
        Caption = 'CFOP padr'#227'o servi'#231'os:'
      end
      object Label11: TLabel
        Left = 252
        Top = 204
        Width = 120
        Height = 13
        Caption = 'Modelo padr'#227'o para NFs:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 20
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 400
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object PnGraCusPrc: TPanel
        Left = 361
        Top = 6
        Width = 466
        Height = 48
        BevelOuter = bvNone
        TabOrder = 2
        Visible = False
        object Label7: TLabel
          Left = 8
          Top = 3
          Width = 76
          Height = 13
          Caption = 'Lista de Pre'#231'os:'
        end
        object EdGraCusPrc: TdmkEditCB
          Left = 8
          Top = 19
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBGraCusPrc
          IgnoraDBLookupComboBox = False
        end
        object CBGraCusPrc: TdmkDBLookupComboBox
          Left = 64
          Top = 19
          Width = 399
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          TabOrder = 1
          dmkEditCB = EdGraCusPrc
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object CGExportar: TdmkCheckGroup
        Left = 8
        Top = 44
        Width = 349
        Height = 53
        Caption = ' O que exportar: '
        Columns = 2
        ItemIndex = 2
        Items.Strings = (
          'Notas de entrada e seus itens'
          'Notas de sa'#237'da e seus itens'
          'Invent'#225'rio (n'#227'o dispon'#237'vel aqui)'
          'Produtos')
        TabOrder = 3
        UpdType = utYes
        Value = 4
        OldValor = 0
      end
      object EdNCM_Padrao: TdmkEdit
        Left = 380
        Top = 72
        Width = 80
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNCM
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCFOPa: TdmkEdit
        Left = 380
        Top = 104
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1101'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1101
        ValWarn = False
      end
      object EdCFOPb: TdmkEdit
        Left = 380
        Top = 128
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2101'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2101
        ValWarn = False
      end
      object EdCFOPc: TdmkEdit
        Left = 380
        Top = 152
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1556'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1556
        ValWarn = False
      end
      object EdCFOPd: TdmkEdit
        Left = 380
        Top = 176
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2556'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2556
        ValWarn = False
      end
      object EdCFOPe: TdmkEdit
        Left = 136
        Top = 200
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '9999'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 9999
        ValWarn = False
      end
      object EdModeloNF: TdmkEdit
        Left = 380
        Top = 200
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
      end
    end
    object Panel21: TPanel
      Left = 465
      Top = 0
      Width = 319
      Height = 229
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object RGGeraRegistro88: TRadioGroup
        Left = 0
        Top = 61
        Width = 319
        Height = 44
        Align = alTop
        Caption = ' Gera registro 88?:  '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 0
      end
      object RGQuebraDeLinha: TRadioGroup
        Left = 0
        Top = 105
        Width = 319
        Height = 44
        Align = alTop
        Caption = ' Quebra de Linha: '
        Columns = 5
        ItemIndex = 3
        Items.Strings = (
          'N'#227'o'
          'CR'
          'LF'
          'CR/LF'
          'LF/CR')
        TabOrder = 1
        OnClick = RGQuebraDeLinhaClick
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 319
        Height = 61
        Align = alTop
        Caption = ' Per'#237'odo (data fiscal): '
        TabOrder = 2
        object Label6: TLabel
          Left = 12
          Top = 16
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
        end
        object Label1: TLabel
          Left = 128
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Data final:'
        end
        object TPDataIni: TdmkEditDateTimePicker
          Left = 12
          Top = 32
          Width = 112
          Height = 21
          Date = 40179.408699699070000000
          Time = 40179.408699699070000000
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPDataFim: TdmkEditDateTimePicker
          Left = 128
          Top = 32
          Width = 112
          Height = 21
          Date = 40209.408699699070000000
          Time = 40209.408699699070000000
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
      end
      object CkRecriarMP: TCheckBox
        Left = 8
        Top = 152
        Width = 293
        Height = 17
        Caption = 'Recriar notas de entrada de mat'#233'ria-prima criadas a for'#231'a.'
        TabOrder = 3
      end
      object CkConsertaNome: TCheckBox
        Left = 8
        Top = 192
        Width = 193
        Height = 17
        Caption = 'Conserta nome de uso e consumo.'
        Checked = True
        State = cbChecked
        TabOrder = 4
      end
      object CkRecriarPQ: TCheckBox
        Left = 8
        Top = 172
        Width = 301
        Height = 17
        Caption = 'Recriar notas de entrada de uso e consumo criadas a for'#231'a.'
        TabOrder = 5
      end
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 277
    Width = 784
    Height = 252
    ActivePage = TabSheet7
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = ' NFs de Entrada '
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 175
      object RECabE: TRichEdit
        Left = 0
        Top = 0
        Width = 776
        Height = 224
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WantReturns = False
        WordWrap = False
      end
    end
    object TabSheet2: TTabSheet
      Caption = ' Itens de Entrada '
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 175
      object REItsE: TRichEdit
        Left = 0
        Top = 0
        Width = 776
        Height = 224
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WantReturns = False
        WordWrap = False
      end
    end
    object TabSheet3: TTabSheet
      Caption = ' NFs de Sa'#237'da '
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 175
      object RECabS: TRichEdit
        Left = 0
        Top = 0
        Width = 776
        Height = 224
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WantReturns = False
        WordWrap = False
      end
    end
    object TabSheet4: TTabSheet
      Caption = ' Itens de sa'#237'da '
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 175
      object REItsS: TRichEdit
        Left = 0
        Top = 0
        Width = 776
        Height = 224
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WantReturns = False
        WordWrap = False
      end
    end
    object TabSheet5: TTabSheet
      Caption = ' Invent'#225'rio '
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 175
      object REInventario: TRichEdit
        Left = 0
        Top = 0
        Width = 776
        Height = 224
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WantReturns = False
        WordWrap = False
      end
    end
    object TabSheet6: TTabSheet
      Caption = ' Produtos '
      ImageIndex = 5
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 175
      object REProdutos: TRichEdit
        Left = 0
        Top = 0
        Width = 776
        Height = 224
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WantReturns = False
        WordWrap = False
      end
    end
    object TabSheet7: TTabSheet
      Caption = ' Notas '
      ImageIndex = 6
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 776
        Height = 224
        Align = alClient
        DataSource = DsCabA
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CNPJ_CPF_dest'
            Title.Caption = 'CNPJ destinat'#225'rio'
            Width = 112
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dest_xNome'
            Title.Caption = 'Nome destinat'#225'rio'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_nNF'
            Title.Caption = 'N'#186' NF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_dEmi_TXT'
            Title.Caption = 'Data emiss'#227'o'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_dSaiEnt_TXT'
            Title.Caption = 'Sa'#237'da/entrada'
            Width = 80
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 185
        Height = 32
        Caption = 'Exporta Prosoft'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 185
        Height = 32
        Caption = 'Exporta Prosoft'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 185
        Height = 32
        Caption = 'Exporta Prosoft'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 529
    Width = 784
    Height = 53
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 582
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object EdPasta: TEdit
        Left = 144
        Top = 12
        Width = 485
        Height = 21
        TabOrder = 2
        Text = 
          'Os arquivos ser'#227'o salvos na pasta: ?????????????????????????????' +
          '??????????'
      end
    end
  end
  object QrCabA: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCabABeforeClose
    AfterScroll = QrCabAAfterScroll
    OnCalcFields = QrCabACalcFields
    SQL.Strings = (
      'SELECT * FROM nfecaba'
      'WHERE empresa=:P0'
      'AND dest_CNPJ=:P1'
      'AND DataFiscal BETWEEN :P2 AND :P3')
    Left = 8
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrCabACNPJ_CPF_emit: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_emit'
      Size = 15
      Calculated = True
    end
    object QrCabACNPJ_CPF_dest: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_dest'
      Size = 14
      Calculated = True
    end
    object QrCabAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrCabAemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrCabAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrCabAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrCabAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrCabAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrCabAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrCabAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrCabAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrCabAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrCabAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrCabAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrCabAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrCabAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrCabAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrCabAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrCabAide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrCabAInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 255
    end
    object QrCabAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrCabAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrCabAModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrCabAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrCabAide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrCabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrCabAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrCabAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrCabAide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrCabAide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrCabAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrCabAide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrCabAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrCabAide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrCabAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrCabAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrCabAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrCabAemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrCabAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrCabAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrCabAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrCabAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrCabAemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrCabAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrCabAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrCabAemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
    end
    object QrCabAemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrCabAemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 10
    end
    object QrCabAemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrCabAemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Size = 15
    end
    object QrCabAemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Size = 7
    end
    object QrCabAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrCabAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrCabAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrCabAdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrCabAdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrCabAdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrCabAdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrCabAdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrCabAdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrCabAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrCabAdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrCabAdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrCabAdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrCabAdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 10
    end
    object QrCabAdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrCabAdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrCabANotaCancelada: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'NotaCancelada'
      Calculated = True
    end
    object QrCabASigla: TWideStringField
      FieldName = 'Sigla'
      Size = 50
    end
    object QrCabAide_dEmi_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_dEmi_TXT'
      Size = 10
      Calculated = True
    end
    object QrCabAide_dSaiEnt_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_dSaiEnt_TXT'
      Size = 10
      Calculated = True
    end
  end
  object QrPQE: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pqe.* '
      'FROM pqe pqe'
      'LEFT JOIN nfecaba nca ON nca.FatNum=pqe.Codigo'
      'AND nca.FatID in (:P0,:P1)'
      'WHERE nca.FatID IS NULL'
      'AND pqe.CI=:P2')
    Left = 296
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPQECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQEData: TDateField
      FieldName = 'Data'
    end
    object QrPQEIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQECI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQETransportadora: TIntegerField
      FieldName = 'Transportadora'
    end
    object QrPQENF: TIntegerField
      FieldName = 'NF'
    end
    object QrPQEFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrPQEPesoB: TFloatField
      FieldName = 'PesoB'
    end
    object QrPQEPesoL: TFloatField
      FieldName = 'PesoL'
    end
    object QrPQEValorNF: TFloatField
      FieldName = 'ValorNF'
    end
    object QrPQERICMS: TFloatField
      FieldName = 'RICMS'
    end
    object QrPQERICMSF: TFloatField
      FieldName = 'RICMSF'
    end
    object QrPQEConhecimento: TIntegerField
      FieldName = 'Conhecimento'
    end
    object QrPQEPedido: TIntegerField
      FieldName = 'Pedido'
    end
    object QrPQEDataE: TDateField
      FieldName = 'DataE'
    end
    object QrPQEJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrPQEICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrPQECancelado: TWideStringField
      FieldName = 'Cancelado'
      Size = 1
    end
    object QrPQELk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQEDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQEDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQEUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQEUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQEAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQEAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQETipoNF: TSmallintField
      FieldName = 'TipoNF'
    end
    object QrPQErefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrPQEmodNF: TSmallintField
      FieldName = 'modNF'
    end
    object QrPQESerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrPQEValProd: TFloatField
      FieldName = 'ValProd'
    end
    object QrPQEVolumes: TIntegerField
      FieldName = 'Volumes'
    end
    object QrPQEIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrPQEPIS: TFloatField
      FieldName = 'PIS'
    end
    object QrPQECOFINS: TFloatField
      FieldName = 'COFINS'
    end
    object QrPQESeguro: TFloatField
      FieldName = 'Seguro'
    end
    object QrPQEDesconto: TFloatField
      FieldName = 'Desconto'
    end
    object QrPQEOutros: TFloatField
      FieldName = 'Outros'
    end
    object QrPQEDataS: TDateField
      FieldName = 'DataS'
    end
  end
  object QrEnti: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, IE, '
      'IF(Tipo=0, CNPJ, CPF) CNPJ_CPF, '
      'IF(Tipo=0, EUF, PUF) UF_Cod'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 324
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntiCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntiUF_Cod: TLargeintField
      FieldName = 'UF_Cod'
      Required = True
    end
  end
  object QrPQEIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Nome NOMEPQ, ei.*'
      'FROM pqeits ei'
      'LEFT JOIN pq ON pq.Codigo=ei.Insumo'
      'WHERE ei.Codigo=:P0'
      'ORDER BY Conta'
      ''
      ''
      '')
    Left = 296
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQEItsCUSTOITEM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOITEM'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object QrPQEItsVALORKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALORKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEItsTOTALKGBRUTO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALKGBRUTO'
      Calculated = True
    end
    object QrPQEItsCUSTOKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEItsPRECOKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECOKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPQEItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQEItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrPQEItsVolumes: TIntegerField
      FieldName = 'Volumes'
      Required = True
    end
    object QrPQEItsPesoVB: TFloatField
      FieldName = 'PesoVB'
      Required = True
      DisplayFormat = '#,##0.000'
    end
    object QrPQEItsPesoVL: TFloatField
      FieldName = 'PesoVL'
      Required = True
      DisplayFormat = '#,##0.000'
    end
    object QrPQEItsValorItem: TFloatField
      FieldName = 'ValorItem'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQEItsIPI: TFloatField
      FieldName = 'IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsRIPI: TFloatField
      FieldName = 'RIPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsCFin: TFloatField
      FieldName = 'CFin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsICMS: TFloatField
      FieldName = 'ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsRICMS: TFloatField
      FieldName = 'RICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsTotalCusto: TFloatField
      FieldName = 'TotalCusto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEItsInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPQEItsTotalPeso: TFloatField
      FieldName = 'TotalPeso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQEItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQEItsprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrPQEItsprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrPQEItsprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrPQEItsprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrPQEItsprod_EX_TIPI: TWideStringField
      FieldName = 'prod_EX_TIPI'
      Size = 3
    end
    object QrPQEItsprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrPQEItsprod_CFOP: TWideStringField
      FieldName = 'prod_CFOP'
      Size = 4
    end
    object QrPQEItsprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 4
    end
    object QrPQEItsprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrPQEItsprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrPQEItsprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrPQEItsprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrPQEItsprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 4
    end
    object QrPQEItsprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrPQEItsprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrPQEItsprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrPQEItsprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrPQEItsprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrPQEItsICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrPQEItsICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrPQEItsICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrPQEItsICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrPQEItsICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrPQEItsIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrPQEItsIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrPQEItsPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsPISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsPISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrPQEItsCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrPQEItsCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEItsCOFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsCOFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEItsCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object QrItsI: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrItsICalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 36
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrItsIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrItsIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrItsIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrItsIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrItsIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrItsIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrItsIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrItsIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrItsIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrItsIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrItsIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrItsIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrItsIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrItsIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrItsITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
    end
    object QrItsI_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrItsIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrItsIEhServico: TIntegerField
      FieldName = 'EhServico'
    end
    object QrItsILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrItsIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrItsIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrItsIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrItsIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrItsIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrItsIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrItsIICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrItsIICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
    end
    object QrItsIICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrItsIICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
    end
    object QrItsIIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrItsIIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
    end
    object QrItsIIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrItsIIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
    end
    object QrItsIPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrItsIPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
    end
    object QrItsIPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrItsIPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
    end
    object QrItsICOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrItsICOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
    end
    object QrItsICOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrItsICOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
    end
    object QrItsIMeuID: TIntegerField
      FieldName = 'MeuID'
    end
    object QrItsINivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrItsIICMS_CST: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ICMS_CST'
      Size = 3
      Calculated = True
    end
  end
  object QrItsO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 92
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsOnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrItsOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrItsOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrItsOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrItsOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrItsOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
  end
  object QrItsN: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 64
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsNnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrItsNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrItsNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrItsNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrItsNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrItsNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrItsNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrItsNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
  end
  object QrItsQ: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsq'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 120
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
  end
  object QrItsS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitss'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 148
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
  end
  object QrProd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX, ggx.GraGru1, '
      'pgt.Tipo_Item, pgt.Genero, unm.Sigla, gg1.*'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed unm ON unm.Codigo=gg1.UnidMed')
    Left = 380
    Top = 308
    object QrProdGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrProdGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrProdNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrProdNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrProdNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrProdCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrProdNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrProdPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrProdGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrProdUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrProdCST_A: TSmallintField
      FieldName = 'CST_A'
    end
    object QrProdCST_B: TSmallintField
      FieldName = 'CST_B'
    end
    object QrProdNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrProdPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrProdIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrProdIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrProdIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrProdIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrProdIPI_TpTrib: TSmallintField
      FieldName = 'IPI_TpTrib'
    end
    object QrProdTipDimens: TSmallintField
      FieldName = 'TipDimens'
    end
    object QrProdPerCuztMin: TFloatField
      FieldName = 'PerCuztMin'
    end
    object QrProdPerCuztMax: TFloatField
      FieldName = 'PerCuztMax'
    end
    object QrProdMedOrdem: TIntegerField
      FieldName = 'MedOrdem'
    end
    object QrProdPartePrinc: TIntegerField
      FieldName = 'PartePrinc'
    end
    object QrProdInfAdProd: TWideStringField
      FieldName = 'InfAdProd'
      Size = 255
    end
    object QrProdSiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Size = 15
    end
    object QrProdHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrProdGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrProdPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrProdPIS_AlqP: TFloatField
      FieldName = 'PIS_AlqP'
    end
    object QrProdPIS_AlqV: TFloatField
      FieldName = 'PIS_AlqV'
    end
    object QrProdPISST_AlqP: TFloatField
      FieldName = 'PISST_AlqP'
    end
    object QrProdPISST_AlqV: TFloatField
      FieldName = 'PISST_AlqV'
    end
    object QrProdCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrProdCOFINS_AlqP: TFloatField
      FieldName = 'COFINS_AlqP'
    end
    object QrProdCOFINS_AlqV: TFloatField
      FieldName = 'COFINS_AlqV'
    end
    object QrProdCOFINSST_AlqP: TFloatField
      FieldName = 'COFINSST_AlqP'
    end
    object QrProdCOFINSST_AlqV: TFloatField
      FieldName = 'COFINSST_AlqV'
    end
    object QrProdICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrProdICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrProdICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrProdICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrProdICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrProdICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrProdICMS_Pauta: TFloatField
      FieldName = 'ICMS_Pauta'
    end
    object QrProdICMS_MaxTab: TFloatField
      FieldName = 'ICMS_MaxTab'
    end
    object QrProdIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrProdLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProdDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProdUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProdAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrProdAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrProdcGTIN_EAN: TWideStringField
      FieldName = 'cGTIN_EAN'
      Size = 14
    end
    object QrProdEX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object QrProdPIS_pRedBC: TFloatField
      FieldName = 'PIS_pRedBC'
    end
    object QrProdPISST_pRedBCST: TFloatField
      FieldName = 'PISST_pRedBCST'
    end
    object QrProdCOFINS_pRedBC: TFloatField
      FieldName = 'COFINS_pRedBC'
    end
    object QrProdCOFINSST_pRedBCST: TFloatField
      FieldName = 'COFINSST_pRedBCST'
    end
    object QrProdICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrProdIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrProdPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrProdCOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrProdICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrProdIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrProdPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrProdCOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrProdICMSRec_tCalc: TSmallintField
      FieldName = 'ICMSRec_tCalc'
    end
    object QrProdIPIRec_tCalc: TSmallintField
      FieldName = 'IPIRec_tCalc'
    end
    object QrProdPISRec_tCalc: TSmallintField
      FieldName = 'PISRec_tCalc'
    end
    object QrProdCOFINSRec_tCalc: TSmallintField
      FieldName = 'COFINSRec_tCalc'
    end
    object QrProdICMSAliqSINTEGRA: TFloatField
      FieldName = 'ICMSAliqSINTEGRA'
    end
    object QrProdICMSST_BaseSINTEGRA: TFloatField
      FieldName = 'ICMSST_BaseSINTEGRA'
    end
    object QrProdFatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
    object QrProdTipo_Item: TSmallintField
      FieldName = 'Tipo_Item'
    end
    object QrProdGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrProdSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
  end
  object QrEntiSemDoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _cod_;'
      'CREATE TABLE _cod_'
      'SELECT DISTINCT CodInfoEmit Codigo'
      'FROM bluederm.nfecaba'
      'WHERE Empresa=:P0'
      'AND emit_CNPJ=""'
      'AND emit_CPF=""'
      'AND DataFiscal BETWEEN :P1 AND :P2'
      'ORDER BY Codigo;'
      ''
      'SELECT cod.Codigo,'
      'IF(ent.Tipo=0, RazaoSocial, Nome) Nome'
      'FROM _cod_ cod'
      'LEFT JOIN bluederm.entidades ent ON ent.Codigo=cod.Codigo')
    Left = 224
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEntiSemDocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiSemDocNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object frxEntiSemDoc: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40358.813132314810000000
    ReportOptions.LastChange = 40358.813132314810000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxEntiSemDocGetValue
    Left = 196
    Top = 48
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEntiSemDoc
        DataSetName = 'frxDsEntiSemDoc'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 154.960666540000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          Left = 109.606301650000000000
          Top = 136.063028740000000000
          Width = 574.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 3.779183390000000000
          Top = 136.063028740000000000
          Width = 105.826771650000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo211: TfrxMemoView
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ENTIDADES SEM CNPJ / CPF')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 86.929190000000000000
          Width = 680.315400000000000000
          Height = 49.133890000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'ATEN'#199#195'O: As entidades relacionadas abaixo devem ter seu CNPJ (ou' +
              ' CPF se for pessoa f'#237'sica) regularizados para que a exporta'#231#227'o s' +
              'eja feita sem erros!')
          ParentFont = False
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 313.700990000000000000
        Width = 680.315400000000000000
        object Memo120: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897637800000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEntiSemDoc
        DataSetName = 'frxDsEntiSemDoc'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 105.827118260000000000
          Width = 574.488188980000000000
          Height = 18.897637795275590000
          DataField = 'Nome'
          DataSet = frxDsEntiSemDoc
          DataSetName = 'frxDsEntiSemDoc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntiSemDoc."Nome"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Width = 105.826771650000000000
          Height = 18.897637795275590000
          DataField = 'Codigo'
          DataSet = frxDsEntiSemDoc
          DataSetName = 'frxDsEntiSemDoc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntiSemDoc."Codigo"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEntiSemDoc: TfrxDBDataset
    UserName = 'frxDsEntiSemDoc'
    CloseDataSource = False
    DataSet = QrEntiSemDoc
    BCDToCurrency = False
    Left = 252
    Top = 48
  end
  object QrExcl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Status, Empresa'
      'FROM nfecaba nca '
      'WHERE nca.FatID=:P0'
      'AND nca.Empresa=:P1'
      'AND CriAForca=1'
      'AND DataFiscal BETWEEN :P2 AND :P3')
    Left = 408
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrExclFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrExclFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrExclStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrExclEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrNivel1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, NCM '
      'FROM gragru1'
      'WHERE Nivel1=:P0')
    Left = 100
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNivel1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNivel1NCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
  end
  object QrGraGruX_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX, ggx.GraGru1, gg1.*'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE Controle=:P0'
      '')
    Left = 124
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField4: TIntegerField
      FieldName = 'GraGruX'
    end
    object IntegerField5: TIntegerField
      FieldName = 'GraGru1'
    end
    object IntegerField6: TIntegerField
      FieldName = 'Nivel3'
    end
    object IntegerField7: TIntegerField
      FieldName = 'Nivel2'
    end
    object IntegerField8: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object IntegerField9: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object IntegerField10: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object IntegerField11: TIntegerField
      FieldName = 'GraTamCad'
    end
    object IntegerField12: TIntegerField
      FieldName = 'UnidMed'
    end
    object SmallintField1: TSmallintField
      FieldName = 'CST_A'
    end
    object SmallintField2: TSmallintField
      FieldName = 'CST_B'
    end
    object StringField4: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object FloatField1: TFloatField
      FieldName = 'Peso'
    end
    object FloatField2: TFloatField
      FieldName = 'IPI_Alq'
    end
    object SmallintField3: TSmallintField
      FieldName = 'IPI_CST'
    end
    object StringField5: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object FloatField3: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object SmallintField4: TSmallintField
      FieldName = 'IPI_TpTrib'
    end
    object SmallintField5: TSmallintField
      FieldName = 'TipDimens'
    end
    object FloatField4: TFloatField
      FieldName = 'PerCuztMin'
    end
    object FloatField5: TFloatField
      FieldName = 'PerCuztMax'
    end
    object IntegerField13: TIntegerField
      FieldName = 'MedOrdem'
    end
    object IntegerField14: TIntegerField
      FieldName = 'PartePrinc'
    end
    object StringField6: TWideStringField
      FieldName = 'InfAdProd'
      Size = 255
    end
    object StringField7: TWideStringField
      FieldName = 'SiglaCustm'
      Size = 15
    end
    object SmallintField6: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object SmallintField7: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object SmallintField8: TSmallintField
      FieldName = 'PIS_CST'
    end
    object FloatField6: TFloatField
      FieldName = 'PIS_AlqP'
    end
    object FloatField7: TFloatField
      FieldName = 'PIS_AlqV'
    end
    object FloatField8: TFloatField
      FieldName = 'PISST_AlqP'
    end
    object FloatField9: TFloatField
      FieldName = 'PISST_AlqV'
    end
    object SmallintField9: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object FloatField10: TFloatField
      FieldName = 'COFINS_AlqP'
    end
    object FloatField11: TFloatField
      FieldName = 'COFINS_AlqV'
    end
    object FloatField12: TFloatField
      FieldName = 'COFINSST_AlqP'
    end
    object FloatField13: TFloatField
      FieldName = 'COFINSST_AlqV'
    end
    object SmallintField10: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object SmallintField11: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object FloatField14: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object FloatField15: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object FloatField16: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object FloatField17: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object FloatField18: TFloatField
      FieldName = 'ICMS_Pauta'
    end
    object FloatField19: TFloatField
      FieldName = 'ICMS_MaxTab'
    end
    object FloatField20: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object IntegerField15: TIntegerField
      FieldName = 'Lk'
    end
    object DateField1: TDateField
      FieldName = 'DataCad'
    end
    object DateField2: TDateField
      FieldName = 'DataAlt'
    end
    object IntegerField16: TIntegerField
      FieldName = 'UserCad'
    end
    object IntegerField17: TIntegerField
      FieldName = 'UserAlt'
    end
    object SmallintField12: TSmallintField
      FieldName = 'AlterWeb'
    end
    object SmallintField13: TSmallintField
      FieldName = 'Ativo'
    end
    object StringField8: TWideStringField
      FieldName = 'cGTIN_EAN'
      Size = 14
    end
    object StringField9: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object FloatField21: TFloatField
      FieldName = 'PIS_pRedBC'
    end
    object FloatField22: TFloatField
      FieldName = 'PISST_pRedBCST'
    end
    object FloatField23: TFloatField
      FieldName = 'COFINS_pRedBC'
    end
    object FloatField24: TFloatField
      FieldName = 'COFINSST_pRedBCST'
    end
    object FloatField25: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object FloatField26: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object FloatField27: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object FloatField28: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object FloatField29: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object FloatField30: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object FloatField31: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object FloatField32: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object SmallintField14: TSmallintField
      FieldName = 'ICMSRec_tCalc'
    end
    object SmallintField15: TSmallintField
      FieldName = 'IPIRec_tCalc'
    end
    object SmallintField16: TSmallintField
      FieldName = 'PISRec_tCalc'
    end
    object SmallintField17: TSmallintField
      FieldName = 'COFINSRec_tCalc'
    end
    object FloatField33: TFloatField
      FieldName = 'ICMSAliqSINTEGRA'
    end
    object FloatField34: TFloatField
      FieldName = 'ICMSST_BaseSINTEGRA'
    end
    object IntegerField18: TIntegerField
      FieldName = 'FatorClas'
    end
  end
  object QrPQsemCFOP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Ativo, IF(ci.Tipo=0, ci.EUF, ci.PUF) ciUF,'
      'IF(iq.Tipo=0, iq.EUF, iq.PUF) iqUF,'
      'pqe.CI, pqe.IQ, pqi.Controle '
      'FROM pqeits pqi'
      'LEFT JOIN pqe pqe ON pqe.Codigo=pqi.Codigo'
      'LEFT JOIN pq  pq  ON pq.Codigo=pqi.Insumo'
      'LEFT JOIN entidades ci ON ci.Codigo=pqe.CI'
      'LEFT JOIN entidades iq ON iq.Codigo=pqe.IQ'
      'WHERE (pqi.prod_CFOP="" OR pqi.prod_CFOP IS NULL)')
    Left = 8
    Top = 308
    object QrPQsemCFOPAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQsemCFOPciUF: TLargeintField
      FieldName = 'ciUF'
    end
    object QrPQsemCFOPiqUF: TLargeintField
      FieldName = 'iqUF'
    end
    object QrPQsemCFOPCI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQsemCFOPIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQsemCFOPControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSemNF_MP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mpe.Data, mpe.Procedencia, mpe.Marca, mpe.Lote,'
      'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,'
      'IF(pr.Tipo=0, pr.RazaoSocial, pr.Nome) NOMEPROCEDENCIA,'
      'IF(pr.Tipo=0, pr.CNPJ, pr.CPF) CNPJ_CPF, mpi.NF'
      'FROM mpinits mpi'
      'LEFT JOIN mpin mpe ON mpe.Controle=mpi.Controle'
      'LEFT JOIN entidades fo ON fo.Codigo=mpe.Procedencia'
      
        'LEFT JOIN entidades pr ON pr.Codigo=IF(mpi.EmitNFAvul<>0, mpi.Em' +
        'itNFAvul, mpi.Fornece)'
      'LEFT JOIN nfecaba nca ON nca.FatNum=mpi.Conta'
      'AND nca.FatID in (:P0,:P1,:P2)'
      'WHERE nca.FatID IS NULL'
      'AND (mpi.NF=0 OR'
      '    IF(pr.Tipo=0, pr.CNPJ, pr.CPF)="")     '
      'AND mpe.ClienteI=:P3'
      'AND mpe.Data BETWEEN :P4 AND :P5'
      'ORDER BY mpe.Data, mpe.Controle, mpi.Conta')
    Left = 508
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end>
    object QrSemNF_MPData: TDateField
      FieldName = 'Data'
    end
    object QrSemNF_MPProcedencia: TIntegerField
      FieldName = 'Procedencia'
    end
    object QrSemNF_MPMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrSemNF_MPLote: TWideStringField
      FieldName = 'Lote'
      Size = 11
    end
    object QrSemNF_MPNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Size = 100
    end
    object QrSemNF_MPNOMEPROCEDENCIA: TWideStringField
      FieldName = 'NOMEPROCEDENCIA'
      Size = 100
    end
    object QrSemNF_MPCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrSemNF_MPNF: TIntegerField
      FieldName = 'NF'
    end
  end
  object frxMPSemNF: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40358.813132314810000000
    ReportOptions.LastChange = 40358.813132314810000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxEntiSemDocGetValue
    Left = 564
    Top = 248
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxSemNF_MP
        DataSetName = 'frxSemNF_MP'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 147.401662680000000000
        Top = 98.267780000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          Left = 45.354291650000000000
          Top = 136.063028740000000000
          Width = 94.487878980000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Top = 136.063028740000000000
          Width = 45.354330708661420000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo211: TfrxMemoView
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ENTRADAS DE MAT'#201'RIA-PRIMA (COURO) SEM NF')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 139.842610000000000000
          Top = 136.063080000000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 343.937230000000000000
          Top = 136.063080000000000000
          Width = 185.196872360000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 529.134200000000000000
          Top = 136.063080000000000000
          Width = 151.181102360000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Proced'#234'ncia ou Emitente')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 185.196970000000000000
          Top = 136.063080000000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NF')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 230.551330000000000000
          Top = 136.063080000000000000
          Width = 113.385826770000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ/CPF')
          ParentFont = False
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        object Memo120: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        DataSet = frxSemNF_MP
        DataSetName = 'frxSemNF_MP'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataField = 'Data'
          DataSet = frxSemNF_MP
          DataSetName = 'frxSemNF_MP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxSemNF_MP."Data"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 45.354360000000000000
          Width = 94.488250000000000000
          Height = 11.338582680000000000
          DataField = 'Marca'
          DataSet = frxSemNF_MP
          DataSetName = 'frxSemNF_MP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxSemNF_MP."Marca"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 343.937230000000000000
          Width = 185.196872360000000000
          Height = 11.338582680000000000
          DataField = 'NOMEFORNECEDOR'
          DataSet = frxSemNF_MP
          DataSetName = 'frxSemNF_MP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxSemNF_MP."NOMEFORNECEDOR"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 529.134200000000000000
          Width = 151.181102360000000000
          Height = 11.338582680000000000
          DataField = 'NOMEPROCEDENCIA'
          DataSet = frxSemNF_MP
          DataSetName = 'frxSemNF_MP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxSemNF_MP."NOMEPROCEDENCIA"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 139.842610000000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataField = 'Lote'
          DataSet = frxSemNF_MP
          DataSetName = 'frxSemNF_MP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxSemNF_MP."Lote"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 185.196970000000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataField = 'NF'
          DataSet = frxSemNF_MP
          DataSetName = 'frxSemNF_MP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxSemNF_MP."NF"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 230.551330000000000000
          Width = 113.385826770000000000
          Height = 11.338582680000000000
          DataField = 'CNPJ_CPF'
          DataSet = frxSemNF_MP
          DataSetName = 'frxSemNF_MP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxSemNF_MP."CNPJ_CPF"]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 56.692950000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo3: TfrxMemoView
          Width = 680.315400000000000000
          Height = 56.692950000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'ATEN'#199#195'O: As entradas de mat'#233'ria-prima abaixo devem possuir NF e ' +
              'seu emitente deve possuir CNPJ para que a exporta'#231#227'o seja feita ' +
              'sem erros!'
            
              'Caso o emissor seja diferente da proced'#234'ncia informe no item de ' +
              'entrada!')
          ParentFont = False
        end
      end
    end
  end
  object frxSemNF_MP: TfrxDBDataset
    UserName = 'frxSemNF_MP'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'Procedencia=Procedencia'
      'Marca=Marca'
      'Lote=Lote'
      'NOMEFORNECEDOR=NOMEFORNECEDOR'
      'NOMEPROCEDENCIA=NOMEPROCEDENCIA'
      'CNPJ_CPF=CNPJ_CPF'
      'NF=NF')
    DataSet = QrSemNF_MP
    BCDToCurrency = False
    Left = 536
    Top = 248
  end
  object QrMPI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mpe.ClienteI, mpe.Data, mpi.Conta, mpi.Emissao, '
      'IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece) QuemEmitiu, '
      'mpi.NF_modelo, mpi.NF_Serie, mpi.NF, SUM(mpi.CMPValor) CMPValor,'
      'SUM(mpi.PecasNF) PecasNF'
      'FROM mpinits mpi'
      'LEFT JOIN mpin mpe ON mpe.Controle=mpi.Controle'
      'LEFT JOIN nfecaba nca ON nca.FatNum=mpi.Conta'
      'AND nca.FatID in (:P0, :P1, :P2)'
      'WHERE nca.FatID IS NULL'
      'AND mpe.ClienteI=:P3'
      'AND mpe.Data BETWEEN :P4 AND :P5'
      
        'GROUP BY mpe.ClienteI, QuemEmitiu, mpi.NF_modelo, mpi.NF_Serie, ' +
        'mpi.NF'
      'ORDER BY mpe.Data, mpe.Controle, mpi.Conta')
    Left = 352
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end>
    object QrMPIClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrMPIData: TDateField
      FieldName = 'Data'
    end
    object QrMPIQuemEmitiu: TLargeintField
      FieldName = 'QuemEmitiu'
      Required = True
    end
    object QrMPINF_modelo: TWideStringField
      FieldName = 'NF_modelo'
      Size = 6
    end
    object QrMPINF_Serie: TWideStringField
      FieldName = 'NF_Serie'
      Size = 6
    end
    object QrMPINF: TIntegerField
      FieldName = 'NF'
    end
    object QrMPICMPValor: TFloatField
      FieldName = 'CMPValor'
    end
    object QrMPIPecasNF: TFloatField
      FieldName = 'PecasNF'
    end
    object QrMPIConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMPIEmissao: TDateField
      FieldName = 'Emissao'
    end
  end
  object QrMPIIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mpe.ClienteI, mpe.Data, mpe.Tipificacao, mpe.Animal,'
      'mpi.CFOP, mpi.PecasNF, mpi.CMPValor, mpi.Conta'
      'FROM mpinits mpi'
      'LEFT JOIN mpin mpe ON mpe.Controle=mpi.Controle'
      'LEFT JOIN nfecaba nca ON nca.FatNum=mpi.Conta'
      'AND nca.FatID in (:P0,:P1)'
      'WHERE nca.FatID IS NULL'
      'AND mpe.ClienteI=:P2'
      'AND IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece)=:P3 '
      'AND mpi.NF_modelo=:P4'
      'AND mpi.NF_Serie=:P5'
      'AND mpi.NF=:P6'
      'AND mpe.Data BETWEEN :P7 AND :P8'
      'ORDER BY mpe.Data, mpe.Controle, mpi.Conta'
      '')
    Left = 352
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end>
    object QrMPIItsClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrMPIItsData: TDateField
      FieldName = 'Data'
    end
    object QrMPIItsTipificacao: TIntegerField
      FieldName = 'Tipificacao'
    end
    object QrMPIItsAnimal: TSmallintField
      FieldName = 'Animal'
    end
    object QrMPIItsCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrMPIItsPecasNF: TFloatField
      FieldName = 'PecasNF'
    end
    object QrMPIItsCMPValor: TFloatField
      FieldName = 'CMPValor'
    end
    object QrMPIItsConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object DsCabA: TDataSource
    DataSet = QrCabA
    Left = 8
    Top = 32
  end
end
