unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UMySQLModule,
  mySQLDbTables,
  ExtCtrls, Menus, Grids, DBGrids, Db,
  TypInfo, StdCtrls, ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg,
  Buttons, frxClass, frxDBSet,
  dmkEditDateTimePicker, Variants, dmkEdit, dmkGeral, UrlMon, UnFinanceiro,
  UnDmkProcFunc, xmldom, XMLIntf,
  msxmldom, XMLDoc, dmkImage, Vcl.Imaging.pngimage, UnDmkEnums, IdBaseComponent,
  IdComponent, IdIPWatch, ShellApi, dmkPageControl,
  // ver!!! Delphi Alexandria
  //DmkBase64, DmkCoding,
  // ver!!! Delphi Alexandria
  ZLibExGZ, UnMyPrinters,
  IdZLibCompressorBase, IdCompressorZLib, ZipForge, frxGZip,
  System.ZLib(*IdZLib*), UnMyVCLRef,
  UnProjGroup_Vars, UnProjGroup_Consts, UMySQLDB, UnComps_Vars, UnVS_CRC_PF,
  UnAppEnums, UnGrl_Vars, UnMyLinguas, Vcl.DBCtrls, Vcl.Mask,
  // Parei aqui! 2022-02-11 remover!
  //UnXXe_PF,
  // fim 2022-02-11
  Feriados, Math;

type
(*
  TTipoMaterialNF = (tnfMateriaPrima, tnfUsoEConsumo);
  TcpCalc = (cpJurosMes, cpMulta);
*)
  TFmPrincipal = class(TForm)
    OpenPictureDialog1: TOpenPictureDialog;
    Timer1: TTimer;
    PMProdImp: TPopupMenu;
    Estoque1: TMenuItem;
    Histrico1: TMenuItem;
    Vendas1: TMenuItem;
    StatusBar: TStatusBar;
    PMGeral: TPopupMenu;
    Button1: TButton;
    Memo3: TMemo;
    OpenDialog1: TOpenDialog;
    AdvToolBarPagerNovo: TdmkPageControl;
    AdvToolBarPager11: TTabSheet;
    AdvToolBarPager12: TTabSheet;
    AdvToolBarPager13: TTabSheet;
    AdvPage1: TTabSheet;
    AdvPage2: TTabSheet;
    AdvPage4: TTabSheet;
    AdvToolBar1: TPanel;
    AdvGlowButton1: TBitBtn;
    AdvGlowButton3: TBitBtn;
    AdvGlowButton5: TBitBtn;
    AdvGlowButton7: TBitBtn;
    AdvToolBar5: TPanel;
    AdvGlowButton17: TBitBtn;
    AdvGlowButton18: TBitBtn;
    AGBEntraUC: TBitBtn;
    AdvGlowButton21: TBitBtn;
    AdvToolBar6: TPanel;
    AdvGlowButton32: TBitBtn;
    AdvGlowButton4: TBitBtn;
    GBBaixaETE: TBitBtn;
    GBBaixaOutros: TBitBtn;
    AGBRelatUC: TBitBtn;
    AdvGlowButton38: TBitBtn;
    AdvGlowButton28: TBitBtn;
    AdvGlowButton29: TBitBtn;
    AdvToolBar10: TPanel;
    AdvGlowButton46: TBitBtn;
    AdvGlowButton47: TBitBtn;
    AdvToolBar11: TPanel;
    AdvGlowButton30: TBitBtn;
    AdvPage5: TTabSheet;
    AdvPage6: TTabSheet;
    AdvPage7: TTabSheet;
    AdvPage8: TTabSheet;
    AdvPage9: TTabSheet;
    AdvToolBar2: TPanel;
    AdvGlowButton9: TBitBtn;
    AdvGlowButton11: TBitBtn;
    AdvGlowButton12: TBitBtn;
    AdvGlowButton13: TBitBtn;
    AdvGlowButton6: TBitBtn;
    AdvGlowButton8: TBitBtn;
    AdvToolBar4: TPanel;
    AdvToolBar15: TPanel;
    AdvGlowButton58: TBitBtn;
    AdvGlowButton59: TBitBtn;
    AdvGlowButton16: TBitBtn;
    AdvGlowButton60: TBitBtn;
    AdvToolBar3: TPanel;
    AdvGlowButton14: TBitBtn;
    AdvGlowButton62: TBitBtn;
    AdvGlowButton63: TBitBtn;
    AdvToolBar16: TPanel;
    AdvGlowButton64: TBitBtn;
    AdvGlowButton65: TBitBtn;
    AdvGlowButton66: TBitBtn;
    AdvGlowButton68: TBitBtn;
    AdvGlowButton69: TBitBtn;
    AdvToolBar17: TPanel;
    AdvGlowButton70: TBitBtn;
    AdvGlowButton71: TBitBtn;
    AdvToolBar18: TPanel;
    AdvGlowButton72: TBitBtn;
    BtPediVda1: TBitBtn;
    AdvToolBar13: TPanel;
    AdvGlowButton56: TBitBtn;
    AdvGlowButton75: TBitBtn;
    AdvGlowButton76: TBitBtn;
    AdvGlowButton77: TBitBtn;
    AdvToolBar20: TPanel;
    AdvToolBar19: TPanel;
    AdvGlowButton78: TBitBtn;
    AdvGlowButton79: TBitBtn;
    AdvGlowButton86: TBitBtn;
    AdvGlowButton87: TBitBtn;
    AdvToolBar21: TPanel;
    AdvGlowButton51: TBitBtn;
    AdvGlowButton25: TBitBtn;
    AdvPage3: TTabSheet;
    AdvToolBar7: TPanel;
    AdvGlowButton48: TBitBtn;
    AdvGlowButton89: TBitBtn;
    AdvPMImp: TPopupMenu;
    Escolher2: TMenuItem;
    AGMBSkin: TBitBtn;
    AdvPMGeral: TPopupMenu;
    Setores2: TMenuItem;
    Unidadesdemedida2: TMenuItem;
    Embalagens1: TMenuItem;
    CFOP2: TMenuItem;
    Perdasdecouros1: TMenuItem;
    Gruposqumicos1: TMenuItem;
    AdvPMFluxos: TPopupMenu;
    Fluxos3: TMenuItem;
    Operaes1: TMenuItem;
    AdvPMDefeitos: TPopupMenu;
    ipos1: TMenuItem;
    Locais2: TMenuItem;
    AdvPMVendas: TPopupMenu;
    Gerencia2: TMenuItem;
    Relatrios1: TMenuItem;
    AdvPMPedidos: TPopupMenu;
    Gerenciar2: TMenuItem;
    Relatrios3: TMenuItem;
    OSsAbertas1: TMenuItem;
    AdvPMMenuCor: TPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvPMImagem: TPopupMenu;
    MenuItem1: TMenuItem;
    Padro1: TMenuItem;
    N3: TMenuItem;
    Estilo2: TMenuItem;
    Automtico1: TMenuItem;
    Centralizado1: TMenuItem;
    ManterProporo1: TMenuItem;
    ManterAltura1: TMenuItem;
    ManterLargura1: TMenuItem;
    Nenhum1: TMenuItem;
    Espichar1: TMenuItem;
    Repetido1: TMenuItem;
    PMOSsEmAberto: TPopupMenu;
    Setorial2: TMenuItem;
    Geral1: TMenuItem;
    Geral2: TMenuItem;
    Setorial1: TMenuItem;
    AdvPMCMPT: TPopupMenu;
    Entradas1: TMenuItem;
    Devolues1: TMenuItem;
    AdvGlowButton121: TBitBtn;
    AdvToolBar29: TPanel;
    AdvGlowButton27: TBitBtn;
    AdvGlowButton23: TBitBtn;
    AdvGlowButton26: TBitBtn;
    AdvGlowButton50: TBitBtn;
    AdvGlowButton125: TBitBtn;
    AdvToolBar28: TPanel;
    AdvGlowButton119: TBitBtn;
    AdvGlowButton120: TBitBtn;
    AdvGlowButton110: TBitBtn;
    AdvGlowButton22: TBitBtn;
    AdvGlowButton128: TBitBtn;
    AdvGlowButton73: TBitBtn;
    AdvToolBar25: TPanel;
    AdvGlowButton10: TBitBtn;
    ATBFormulasImpXX: TBitBtn;
    GBGerenciaPesagem: TBitBtn;
    Button2: TButton;
    BtEntiProSoft: TBitBtn;
    AdvGlowButton132: TBitBtn;
    AdvToolBar31: TPanel;
    AdvGlowButton133: TBitBtn;
    N1: TMenuItem;
    ListaEstoqueHistrico1: TMenuItem;
    AdvGlowButton135: TBitBtn;
    AdvGlowButton136: TBitBtn;
    AdvGlowButton40: TBitBtn;
    AdvGlowButton137: TBitBtn;
    AdvPMCMPP: TPopupMenu;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    AdvToolBar32: TPanel;
    AdvGlowMenuButton1: TBitBtn;
    GBMPReclas: TBitBtn;
    AdvGlowButton139: TBitBtn;
    AdvGlowButton140: TBitBtn;
    AdvGlowButton143: TBitBtn;
    AdvGlowButton144: TBitBtn;
    AdvGlowButton146: TBitBtn;
    Panel1: TPanel;
    DBGPQ_GGX: TDBGrid;
    DsPQ_GGX: TDataSource;
    AdvGlowButton145: TBitBtn;
    AdvPMCECT: TPopupMenu;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    AdvGlowButton147: TBitBtn;
    AdvPMResultados: TPopupMenu;
    Emitidos1: TMenuItem;
    Quitados1: TMenuItem;
    AdvGlowButton149: TBitBtn;
    AdvGlowButton151: TBitBtn;
    Button3: TButton;
    Edversao: TdmkEdit;
    Label191: TLabel;
    Label1: TLabel;
    Button5: TButton;
    AdvToolBar33: TPanel;
    AdvGlowButton154: TBitBtn;
    BitBtn1: TBitBtn;
    AdvToolBar34: TPanel;
    Button6: TButton;
    Edit1: TEdit;
    AdvGlowButton157: TBitBtn;
    AdvGlowButton159: TBitBtn;
    DataSource1: TDataSource;
    AdvToolBar35: TPanel;
    AdvGlowButton88: TBitBtn;
    GBFabricas: TBitBtn;
    AdvGlowButton158: TBitBtn;
    AdvGlowButton156: TBitBtn;
    Label2: TLabel;
    Button7: TButton;
    EdICMS_BC: TdmkEdit;
    EdICMS_Alq: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdICMS_Val: TdmkEdit;
    AdvToolBar36: TPanel;
    GBUsoConsCab: TBitBtn;
    frxReport1: TfrxReport;
    GBImportaXML: TBitBtn;
    GBImportaNFes: TBitBtn;
    GBAbreXML: TBitBtn;
    AdvGlowButton160: TBitBtn;
    GBTipoMov: TBitBtn;
    PMMPReclas: TPopupMenu;
    Gerencia1: TMenuItem;
    Inclui1: TMenuItem;
    GBConflitos: TBitBtn;
    BitBtn2: TBitBtn;
    GBMPSubProd: TBitBtn;
    AdvGlowButton33: TBitBtn;
    AdvGlowButton142: TBitBtn;
    AdvSPED_EFD: TPopupMenu;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    AGB_SPED_EFD_Exp: TBitBtn;
    PMEntraUC: TPopupMenu;
    Antigo1: TMenuItem;
    Novo1: TMenuItem;
    AGBEntraMP: TBitBtn;
    AGBCorrigeFatID: TBitBtn;
    AGBConfEstq: TBitBtn;
    Button8: TButton;
    Button9: TButton;
    EdGGXExluido: TdmkEdit;
    Button10: TButton;
    EdGGXSubstituto: TdmkEdit;
    EdNivelExclusao: TdmkEdit;
    PMRelatUC: TPopupMenu;
    NovoGrade1: TMenuItem;
    AntigoPQ1: TMenuItem;
    PMSintegra: TPopupMenu;
    Geral3: TMenuItem;
    SPED20101: TMenuItem;
    AGBApurICMSIPI: TBitBtn;
    PMEntrada: TPopupMenu;
    Usoeconsumo1: TMenuItem;
    Energiaeltricaguags1: TMenuItem;
    ComunicaoTelecomunicao1: TMenuItem;
    UsoeconsumoSPED1: TMenuItem;
    N2: TMenuItem;
    D100Frete07088B0910112627571: TMenuItem;
    AdvToolBar37: TPanel;
    AGBCartNiv2: TBitBtn;
    AGBCarteiras: TBitBtn;
    AGBReceDesp: TBitBtn;
    AdvPage11: TTabSheet;
    AdvToolBar39: TPanel;
    AdvToolBar38: TPanel;
    AGBFatPedNFs: TBitBtn;
    AdvGlowButton85: TBitBtn;
    AdvGlowButton80: TBitBtn;
    AdvGlowButton81: TBitBtn;
    AdvGlowButton126: TBitBtn;
    AdvGlowButton124: TBitBtn;
    AdvToolBar40: TPanel;
    AdvGlowButton35: TBitBtn;
    AdvGlowButton36: TBitBtn;
    AdvGlowButton61: TBitBtn;
    AdvGlowButton131: TBitBtn;
    AdvGlowButton148: TBitBtn;
    AdvGlowButton83: TBitBtn;
    AdvGlowButton152: TBitBtn;
    GBValidaXML: TBitBtn;
    GBClasFisc: TBitBtn;
    AdvGlowButton84: TBitBtn;
    AdvGlowButton155: TBitBtn;
    GBNatOper: TBitBtn;
    AdvToolBar30: TPanel;
    AdvGlowButton122: TBitBtn;
    AdvGlowButton123: TBitBtn;
    AGBContingencia: TBitBtn;
    AGBImportaNFeWeb: TBitBtn;
    Button11: TButton;
    AGBNFeEventos: TBitBtn;
    AGBConsultaNFe: TBitBtn;
    AGBNFeExportaXML_B: TBitBtn;
    AdvPage10: TTabSheet;
    AdvToolBar27: TPanel;
    AdvGlowButton117: TBitBtn;
    AdvGlowButton118: TBitBtn;
    AdvGlowButton105: TBitBtn;
    AdvGlowButton113: TBitBtn;
    GBReduzido: TBitBtn;
    AdvGlowButton34: TBitBtn;
    AdvToolBar26: TPanel;
    AdvGlowButton114: TBitBtn;
    AdvGlowButton115: TBitBtn;
    AdvGlowButton116: TBitBtn;
    AdvToolBar8: TPanel;
    AdvGlowButton31: TBitBtn;
    AdvGlowButton41: TBitBtn;
    AdvGlowButton42: TBitBtn;
    AdvGlowButton43: TBitBtn;
    AdvGlowButton44: TBitBtn;
    AdvGlowButton49: TBitBtn;
    AGBOpcoesGrad: TBitBtn;
    AdvToolBar12: TPanel;
    BtTema: TBitBtn;
    AdvGlowButton53: TBitBtn;
    AdvGlowButton54: TBitBtn;
    TySuporte: TTrayIcon;
    TmSuporte: TTimer;
    AGBNFeConsCad: TBitBtn;
    AdvGlowButton55: TBitBtn;
    AGBMPRecebImp: TBitBtn;
    Label6: TLabel;
    AdvGlowButton24: TBitBtn;
    AGBNFeDest: TBitBtn;
    AGBNFeLoad_Inn: TBitBtn;
    AGBNFeDesDowC: TBitBtn;
    AdvToolBar41: TPanel;
    AdvGlowButton96: TBitBtn;
    APMPesagem: TPopupMenu;
    Caleirocurtimento1: TMenuItem;
    Recurtimento1: TMenuItem;
    Acabamento1: TMenuItem;
    AdvPage12: TTabSheet;
    AdvToolBar22: TPanel;
    AGBWBInnCab: TBitBtn;
    AGBWBOutCab: TBitBtn;
    AdvGlowButton20: TBitBtn;
    AGBWBAjsCab: TBitBtn;
    AGBWBRclCab: TBitBtn;
    IdIPWatch1: TIdIPWatch;
    AdvToolBar42: TPanel;
    AGBWBPallets: TBitBtn;
    AGBWBArtCab: TBitBtn;
    AGBWBMPrCab: TBitBtn;
    AdvToolBar43: TPanel;
    AGBWBMovImp: TBitBtn;
    AGBWBIndsWB: TBitBtn;
    AGBNewFinMigra: TBitBtn;
    BalloonHint1: TBalloonHint;
    AdvGlowButton19: TBitBtn;
    APRibeira: TTabSheet;
    AdvToolBar44: TPanel;
    AGBVSNatCad: TBitBtn;
    AGBVSRibCad: TBitBtn;
    AGBVSRibCla: TBitBtn;
    AGBVSPallet: TBitBtn;
    AGBVSPalSta: TBitBtn;
    APMVSClaArt: TPopupMenu;
    ComearnovaOC1: TMenuItem;
    RecomearclkassificaodeOCjconfigurada1: TMenuItem;
    AGBVSSerFch: TBitBtn;
    ReabreTabelas1: TMenuItem;
    APMVSRclArt: TPopupMenu;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    AGBNfeNewVer: TBitBtn;
    AGBFatParcelaNfeCabY: TBitBtn;
    AdvGlowButton52: TBitBtn;
    AdvPopupMenu1: TPopupMenu;
    APMMigrarVersaoNFe: TPopupMenu;
    CamposdaNFe1: TMenuItem;
    IdCompressorZLib1: TIdCompressorZLib;
    ZipForge1: TZipForge;
    AGBVSMrtCab: TBitBtn;
    AGBVSRibOpe: TBitBtn;
    RecomearclkassificaodeOCjconfigurada2: TMenuItem;
    N4: TMenuItem;
    Gerenciamento1: TMenuItem;
    N5: TMenuItem;
    Gerenciamento2: TMenuItem;
    AdvGlowButton82: TBitBtn;
    ATBVSGerencia: TPanel;
    AGBVSMovIts: TBitBtn;
    AGBVSPallet2: TBitBtn;
    AGBVSBoxes: TBitBtn;
    AdvGlowButton67: TBitBtn;
    AGBVSDsnCab: TBitBtn;
    AGBVSFichas: TBitBtn;
    N6: TMenuItem;
    N7: TMenuItem;
    ClasificaoMassiva1: TMenuItem;
    Prepara1: TMenuItem;
    Novo2: TMenuItem;
    Antigo2: TMenuItem;
    NovaConfiguraodeOC1: TMenuItem;
    AGBOCs: TBitBtn;
    AGBVSEqzCab: TBitBtn;
    AGBVSCfgEqz: TBitBtn;
    AGBVSMotivBxa: TBitBtn;
    AGBGraGruY: TBitBtn;
    AGBVSEntiMP: TBitBtn;
    AdvToolBar47: TPanel;
    AdvGlowButton102: TBitBtn;
    AdvGlowButton106: TBitBtn;
    AdvGlowButton129: TBitBtn;
    AdvGlowButton130: TBitBtn;
    AGBVSCGICab: TBitBtn;
    AGBVSWetEnd: TBitBtn;
    AGBVSFinCla: TBitBtn;
    APMVSDvlRtb: TPopupMenu;
    DevoluoDefinitivaAoEstoque1: TMenuItem;
    RetornoParaRetrabalhoEPosteriorReenvio1: TMenuItem;
    AGBVSSubPrd: TBitBtn;
    N8: TMenuItem;
    N9: TMenuItem;
    ReclassificaoMassiva1: TMenuItem;
    AGBTribDefCad: TBitBtn;
    AGBTribDefCab: TBitBtn;
    GerenciamentodePrReclasse1: TMenuItem;
    APMCorrigeIMEIs: TPopupMenu;
    IMEIssemFornecedor1: TMenuItem;
    IMEIsGmeosorfos1: TMenuItem;
    APMVSCadDiversos: TPopupMenu;
    iposdeArtigo1: TMenuItem;
    APMResultadoFichas: TPopupMenu;
    nica1: TMenuItem;
    Vrias1: TMenuItem;
    AdvToolBar49: TPanel;
    AGBVSCalCab: TBitBtn;
    AGBVSCurCab: TBitBtn;
    TmVersao: TTimer;
    AdvToolBar50: TPanel;
    AGBPQRAjuInn: TBitBtn;
    AGBPQRAjuOut: TBitBtn;
    AdvGlowButton127: TBitBtn;
    AdvToolBar51: TPanel;
    AGBConsLeit: TBitBtn;
    AdvGlowButton161: TBitBtn;
    AGBImprimeVSEmProcBH: TBitBtn;
    AdvGlowButton162: TBitBtn;
    AdvToolBar52: TPanel;
    AGBCorrigeNFesIMIs: TBitBtn;
    AdvGlowButton163: TBitBtn;
    AGBVSCOPCab: TBitBtn;
    AdvToolBar53: TPanel;
    AdvGlowMenuButton5: TBitBtn;
    AdvPage13: TTabSheet;
    AdvToolBar45: TPanel;
    AGBVSInnCab: TBitBtn;
    AGBVSOutCab: TBitBtn;
    AGBVSOutIts: TBitBtn;
    AGBVSAjsCab: TBitBtn;
    AGBVSGeraArtCab: TBitBtn;
    AGBVSClaArt: TBitBtn;
    AGBVSRclArt: TBitBtn;
    AGBVSOpeCab: TBitBtn;
    AGBVSPlCCab: TBitBtn;
    AGBVSExBCab: TBitBtn;
    AGBVSPWECab: TBitBtn;
    AGBVSDvlRtb: TBitBtn;
    AGBVSPedCab: TBitBtn;
    AGBVSMovItb: TBitBtn;
    AGBVSTrfLocCab: TBitBtn;
    AdvToolBar46: TPanel;
    AGBVSMovImp: TBitBtn;
    AGBVSFchRslCab: TBitBtn;
    AGBSPEDEFDEnce: TBitBtn;
    AGBVSCfgEFD: TBitBtn;
    Localizarcadastrode1: TMenuItem;
    AGBGraGruEGer: TBitBtn;
    AGBCadDiversos: TBitBtn;
    AGBVSPSPCab: TBitBtn;
    AdvGlowMenuButton4: TBitBtn;
    AGBVSPwdDd: TBitBtn;
    AGBVSRRMCab: TBitBtn;
    AGBAtzArtigosPallets: TBitBtn;
    AdvPage14: TTabSheet;
    AdvToolBar54: TPanel;
    AdvGlowButton164: TBitBtn;
    AdvGlowButton165: TBitBtn;
    AdvToolBar55: TPanel;
    AdvGlowButton166: TBitBtn;
    AdvGlowButton167: TBitBtn;
    AGBVSArtCab: TBitBtn;
    AGBVSCPMRSBCb: TBitBtn;
    AGBVSReqMov: TBitBtn;
    AGBVSMovCab: TBitBtn;
    AdvGlowButton45: TBitBtn;
    APMVSInfXxx: TPopupMenu;
    IECInformaodeEntradadeCouros1: TMenuItem;
    Inconsistnciadeinformao1: TMenuItem;
    AGBInfoIncCab: TBitBtn;
    EdSelCod: TdmkEdit;
    PMLocaliza: TPopupMenu;
    IMEI1: TMenuItem;
    IMEC1: TMenuItem;
    Reduzido1: TMenuItem;
    Entidade1: TMenuItem;
    reaVSm1: TMenuItem;
    PesoVSkg1: TMenuItem;
    NFeemitida1: TMenuItem;
    Nmeroainformar1: TMenuItem;
    Componenteativo1: TMenuItem;
    NFeRecebida1: TMenuItem;
    APMVSImprime: TPopupMenu;
    N26EstoqueCustoIntegrado1: TMenuItem;
    AGBVSMovimID: TBitBtn;
    AGBClaReCo: TBitBtn;
    AdvGlowButton57: TBitBtn;
    AdvPMBaixaPQ: TPopupMenu;
    Consumo1: TMenuItem;
    Devoluonovo1: TMenuItem;
    AGBGraGruGruXCou: TBitBtn;
    AdvToolBar14: TPanel;
    AdvGlowButton90: TBitBtn;
    AdvGlowButton138: TBitBtn;
    AdvGlowButton168: TBitBtn;
    BtVSSifDipoa: TBitBtn;
    APMVSCfgEFD: TPopupMenu;
    Movimentaes1: TMenuItem;
    BaixaxestoquedeInNatura1: TMenuItem;
    Outrasbaixasxestoque1: TMenuItem;
    Geraoxestoque1: TMenuItem;
    VSxPesagemPQ1: TMenuItem;
    AGBVSMOEnvXXX: TBitBtn;
    APMVSMOEnvXXX: TPopupMenu;
    Fretesemretorno1: TMenuItem;
    Envioparaposteriorretorno1: TMenuItem;
    Retornodeenvio1: TMenuItem;
    N10: TMenuItem;
    CTes1: TMenuItem;
    N21RendimentoSemi1: TMenuItem;
    N24RendimentoeMO1: TMenuItem;
    AdvGlowButton39: TBitBtn;
    sPanel5: TPanel;
    SbLogin: TSpeedButton;
    SbAtualizaERP: TSpeedButton;
    SbFavoritos: TSpeedButton;
    SbVerificaDB: TSpeedButton;
    SbBackup: TSpeedButton;
    SbWSuport: TSpeedButton;
    ImgLogo: TdmkImage;
    PMLogo: TPopupMenu;
    Opes1: TMenuItem;
    Opesespecficas1: TMenuItem;
    Matriz1: TMenuItem;
    Filiais1: TMenuItem;
    N11: TMenuItem;
    SairdoERP1: TMenuItem;
    PMVerificaDB: TPopupMenu;
    VerificaBDServidor2: TMenuItem;
    VerificaTabelasTerceiros2: TMenuItem;
    SbPopupGeral: TSpeedButton;
    SbLastWork1: TSpeedButton;
    SbLastWork2: TSpeedButton;
    SbVSPesqSeqPeca: TSpeedButton;
    SbMinimizaMenu: TSpeedButton;
    LaTopWarn1: TLabel;
    LaTopWarn2: TLabel;
    sPanel1: TPanel;
    BtReceitaRibeira: TBitBtn;
    BtPesagemRecurt: TBitBtn;
    BtPesagemCalCur: TBitBtn;
    BtEntidades: TBitBtn;
    BtEmiteDuplicata: TBitBtn;
    BtCambio: TBitBtn;
    BtEmiteNotaPromissoria: TBitBtn;
    BtCadastroInsumos: TBitBtn;
    BtRelatorioInsumos: TBitBtn;
    BtTrocaDeProduto: TBitBtn;
    BtPesagemAcabto: TBitBtn;
    BtReceitaAcabto: TBitBtn;
    sPanel60: TPanel;
    PnVendaCouro: TPanel;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BtOSsEmAberto: TBitBtn;
    BtCouroTerceiros: TBitBtn;
    BitBtn23: TBitBtn;
    BtControleVendas: TBitBtn;
    BtMpvImp: TBitBtn;
    BitBtn21: TBitBtn;
    PanToolBar23: TPanel;
    sPanel3: TPanel;
    BitBtn24: TBitBtn;
    BitBtn25: TBitBtn;
    BitBtn26: TBitBtn;
    Panel3: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    PageControl1: TdmkPageControl;
    TsDescanso: TTabSheet;
    ImgPrincipal: TImage;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    SG1: TStringGrid;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel2: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    N00Geral1: TMenuItem;
    N12: TMenuItem;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel23: TPanel;
    Panel24: TPanel;
    Panel25: TPanel;
    Panel26: TPanel;
    Panel27: TPanel;
    Panel28: TPanel;
    Panel29: TPanel;
    Panel30: TPanel;
    Panel31: TPanel;
    Panel32: TPanel;
    Panel33: TPanel;
    Panel34: TPanel;
    Panel35: TPanel;
    Panel36: TPanel;
    Panel37: TPanel;
    Panel39: TPanel;
    Panel40: TPanel;
    Panel41: TPanel;
    Panel42: TPanel;
    Panel43: TPanel;
    Panel44: TPanel;
    Panel45: TPanel;
    Panel46: TPanel;
    Panel47: TPanel;
    Panel48: TPanel;
    Panel49: TPanel;
    Panel50: TPanel;
    Panel51: TPanel;
    Panel52: TPanel;
    Panel54: TPanel;
    Panel55: TPanel;
    Panel56: TPanel;
    Panel57: TPanel;
    Panel58: TPanel;
    Panel59: TPanel;
    Button4: TButton;
    DsParamsEmp: TDataSource;
    BitBtn3: TBitBtn;
    DataSource2: TDataSource;
    PMStyles: TPopupMenu;
    Feriadosfuturos1: TMenuItem;
    PCP1: TMenuItem;
    PCPFluxos1: TMenuItem;
    Estgios1: TMenuItem;
    Aproduzirnodiaporsetor1: TMenuItem;
    BtPURelatPQ: TBitBtn;
    PMRelatPQ: TPopupMenu;
    Previsodeconsumo1: TMenuItem;
    N101Produo1: TMenuItem;
    N05ClassesporMartelo1: TMenuItem;
    SbFinancas: TSpeedButton;
    SbEntidade: TSpeedButton;
    N13: TMenuItem;
    Marcaodedefeitosteste1: TMenuItem;
    BtGraGruYPrecos: TBitBtn;
    BitBtn4: TBitBtn;
    TabSheet1: TTabSheet;
    Panel8: TPanel;
    BitBtn5: TBitBtn;
    BtCtbBalPCab1: TBitBtn;
    BitBtn7: TBitBtn;
    Panel60: TPanel;
    Panel61: TPanel;
    AGBFinEncerMes: TBitBtn;
    Panel62: TPanel;
    SubstratosdeRendimento1: TMenuItem;
    BtAjustePQw: TBitBtn;
    AGBParar: TBitBtn;
    Gruposdereceitas1: TMenuItem;
    ConsumoGrupodeFrmulas1: TMenuItem;
    CustoSimuladodeArtigo1: TMenuItem;
    BtPQN: TBitBtn;
    BtPQI: TBitBtn;
    Panel63: TPanel;
    BtMovimentacaoFiscalNoContabil: TBitBtn;
    Panel64: TPanel;
    BtGruposContabeis: TBitBtn;
    BtCtbBalPCab2: TBitBtn;
    TabSheet2: TTabSheet;
    Panel65: TPanel;
    BtSrvTomTaxCad: TBitBtn;
    Panel66: TPanel;
    Panel67: TPanel;
    Panel68: TPanel;
    BtSrvTomRetCab: TBitBtn;
    BtSrvTomInnCab: TBitBtn;
    Caleiro1: TMenuItem;
    Curtimento1: TMenuItem;
    N14: TMenuItem;
    Panel69: TPanel;
    Panel70: TPanel;
    AGB_SPED_EFD_Imp: TBitBtn;
    AdvGlowButton150: TBitBtn;
    AGB_SPED_Compara: TBitBtn;
    AGBSintegra: TBitBtn;
    BtUsoConsCab: TBitBtn;
    Panel71: TPanel;
    BtCentroCust4: TBitBtn;
    BtCentroCust3: TBitBtn;
    BtCentroCust2: TBitBtn;
    BtCentroCusto: TBitBtn;
    BtCentroCust5: TBitBtn;
    BtCentroCustAll: TBitBtn;
    Panel72: TPanel;
    BtSoliComprCab1: TBitBtn;
    BtSoliComprCab2: TBitBtn;
    BtPediVda2: TBitBtn;
    AdvGlowButton37: TBitBtn;
    BtSPED_Contribui��es: TBitBtn;
    AdvToolBar9: TPanel;
    AGBLaySPEDEFD: TBitBtn;
    AGBTabsSPEDEFD: TBitBtn;
    Panel53: TPanel;
    AGBCFOPCFOP: TBitBtn;
    BitBtn6: TBitBtn;
    BtEfdInnCTsGer: TBitBtn;
    BtLe2: TBitBtn;
    BtCtSub1: TBitBtn;
    BtDRE: TBitBtn;
    BtDreCfgCab: TBitBtn;
    BitBtn8: TBitBtn;
    Memo1: TMemo;
    BitBtn9: TBitBtn;
    BtVSConCab: TBitBtn;
    PMAjusteEstqPQ: TPopupMenu;
    SaldosPositivos1: TMenuItem;
    Gerencia3: TMenuItem;
    BtImpETE: TBitBtn;
    PMImpETE: TPopupMenu;
    Slidossedimentveis1: TMenuItem;
    N102InNaturaPendente1: TMenuItem;
    N103Teste1: TMenuItem;
    Artigosgeradoscommodity1: TMenuItem;
    OrigemBarraca1: TMenuItem;
    OrigemCurtimento1: TMenuItem;
    OrigemAmbos1: TMenuItem;
    PorDia1: TMenuItem;
    PorMarca1: TMenuItem;
    AdvGlowMenuButton6: TBitBtn;
    BtVSExcCab: TBitBtn;
    N104Saldodemovimentaocorretiva1: TMenuItem;
    Corrigecustoreclasse1: TMenuItem;
    BtVSFerramentas: TBitBtn;
    PMVSFerramentas: TPopupMenu;
    Divergnciadecustoentreorigemedestino1: TMenuItem;
    Divergnciadecustodesaldovirtual1: TMenuItem;
    ReduzidosdePrClasseReclasseemConflito1: TMenuItem;
    IMEIscomcampospositivosenegativosnomesmoregistro1: TMenuItem;
    BtAtzSdos: TBitBtn;
    N105CustodeProduo1: TMenuItem;
    ReduzidosdeGeraodeArtigoemConflito1: TMenuItem;
    ReduzidosdeBaixaemConflitocomaOrigem1: TMenuItem;
    N12MovimentodecompraevendaMPAG1: TMenuItem;
    Custosdivergentesdeprreclasse1: TMenuItem;
    Custosdivergentesdereclasse1: TMenuItem;
    N15: TMenuItem;
    CorreodecustosporIMECs1: TMenuItem;
    N16: TMenuItem;
    Linkdebaixasdeartigosempreocessoprocessos1: TMenuItem;
    PeaspositivasnegativasXValortotalnegativopositivos1: TMenuItem;
    Executartodasferramentasemsequncia1: TMenuItem;
    N17: TMenuItem;
    N02FichaKardexagrupado1: TMenuItem;
    N02FichaKardexcorrido1: TMenuItem;
    N106Tabeladerendimentoinnaturacurtido1: TMenuItem;
    IMEIscomValoresPositivoseNegativosnoMesmoregistro1: TMenuItem;
    IDJanela1: TMenuItem;
    N107Estoquedereduzido1: TMenuItem;
    Gruposdeusurios1: TMenuItem;
    LoadWebServices1: TMenuItem;
    GerenciarWebServices1: TMenuItem;
    Veculos1: TMenuItem;
    Veculos2: TMenuItem;
    Marcas1: TMenuItem;
    Modelos1: TMenuItem;
    AdvGlowButton2: TBitBtn;
    BtMapaOperPosProc: TBitBtn;
    PMMpvImp: TPopupMenu;
    Faturados1: TMenuItem;
    Expedidos1: TMenuItem;
    BtControleVendasNovo: TBitBtn;
    PQporNF1: TMenuItem;
    N18: TMenuItem;
    RecriartodasbaixasporNF1: TMenuItem;
    N19: TMenuItem;
    IncongrunciasdeestoquedeUsoeConsumo1: TMenuItem;
    N21: TMenuItem;
    CorrigeGrupodocadastrodeInsumosemMassa1: TMenuItem;
    RecalcularsaldodeNFs1: TMenuItem;
    BtMarcas: TBitBtn;
    BtBanhosImp: TBitBtn;
    BitBtn10: TBitBtn;
    BitBtn11: TBitBtn;
    N108SIFDIPOA1: TMenuItem;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SBCadEntidadesClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtImagemClick(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure PQCadastro1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AdvGlowButton5Click(Sender: TObject);
    procedure AdvGlowButton3Click(Sender: TObject);
    procedure AdvGlowButton17Click(Sender: TObject);
    procedure AGBEntraUCClick(Sender: TObject);
    procedure AdvGlowButton18Click(Sender: TObject);
    procedure AdvGlowButton21Click(Sender: TObject);
    procedure AdvGlowButton10Click(Sender: TObject);
    procedure GBGerenciaPesagemClick(Sender: TObject);
    procedure GBBaixaETEClick(Sender: TObject);
    procedure AGBRelatUCClick(Sender: TObject);
    procedure AdvGlowButton37Click(Sender: TObject);
    procedure AdvGlowButton38Click(Sender: TObject);
    procedure AdvGlowButton24Click(Sender: TObject);
    procedure AdvGlowButton23Click(Sender: TObject);
    procedure AdvGlowButton26Click(Sender: TObject);
    procedure AdvGlowButton22Click(Sender: TObject);
    procedure AdvGlowButton27Click(Sender: TObject);
    procedure AdvGlowButton40Click(Sender: TObject);
    procedure AdvGlowButton43Click(Sender: TObject);
    procedure AdvGlowButton44Click(Sender: TObject);
    procedure AdvGlowButton46Click(Sender: TObject);
    procedure AdvGlowButton49Click(Sender: TObject);
    procedure AdvGlowButton31Click(Sender: TObject);
    procedure AdvGlowButton41Click(Sender: TObject);
    procedure AdvGlowButton8Click(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure AdvGlowButton11Click(Sender: TObject);
    procedure AdvGlowButton12Click(Sender: TObject);
    procedure AdvGlowButton13Click(Sender: TObject);
    procedure AdvGlowButton25Click(Sender: TObject);
    procedure AdvGlowButton16Click(Sender: TObject);
    procedure AdvGlowButton78Click(Sender: TObject);
    procedure AdvGlowButton79Click(Sender: TObject);
    procedure AdvGlowButton86Click(Sender: TObject);
    procedure AdvGlowButton87Click(Sender: TObject);
    procedure AdvGlowButton85Click(Sender: TObject);
    procedure AdvGlowButton80Click(Sender: TObject);
    procedure AdvGlowButton81Click(Sender: TObject);
    procedure AdvGlowButton83Click(Sender: TObject);
    procedure BtPediVda1Click(Sender: TObject);
    procedure AdvGlowButton73Click(Sender: TObject);
    procedure AdvGlowButton72Click(Sender: TObject);
    procedure AdvGlowButton56Click(Sender: TObject);
    procedure AdvGlowButton75Click(Sender: TObject);
    procedure AdvGlowButton76Click(Sender: TObject);
    procedure AdvGlowButton77Click(Sender: TObject);
    procedure AdvGlowButton69Click(Sender: TObject);
    procedure AdvGlowButton66Click(Sender: TObject);
    procedure Gfg(Sender: TObject);
    procedure AdvGlowButton70Click(Sender: TObject);
    procedure AdvGlowButton63Click(Sender: TObject);
    procedure AdvGlowButton62Click(Sender: TObject);
    procedure AdvGlowButton14Click(Sender: TObject);
    procedure AdvGlowButton52Click(Sender: TObject);
    procedure AdvGlowButton51Click(Sender: TObject);
    procedure AdvGlowButton58Click(Sender: TObject);
    procedure AdvGlowButton59Click(Sender: TObject);
    procedure AdvGlowButton60Click(Sender: TObject);
    procedure AdvGlowButton89Click(Sender: TObject);
    procedure GBValidaXMLClick(Sender: TObject);
    procedure Escolher2Click(Sender: TObject);
    procedure Setores2Click(Sender: TObject);
    procedure Unidadesdemedida2Click(Sender: TObject);
    procedure Embalagens1Click(Sender: TObject);
    procedure CFOP2Click(Sender: TObject);
    procedure Perdasdecouros1Click(Sender: TObject);
    procedure Gruposqumicos1Click(Sender: TObject);
    procedure Fluxos3Click(Sender: TObject);
    procedure Operaes1Click(Sender: TObject);
    procedure ipos1Click(Sender: TObject);
    procedure Locais2Click(Sender: TObject);
    procedure Gerencia2Click(Sender: TObject);
    procedure Relatrios1Click(Sender: TObject);
    procedure Gerenciar2Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Padro1Click(Sender: TObject);
    procedure Automtico1Click(Sender: TObject);
(*
    procedure AdvPreviewMenu1Buttons0Click(Sender: TObject;
      Button: TButtonCollectionItem);
    procedure AdvPreviewMenu1Buttons1Click(Sender: TObject;
      Button: TButtonCollectionItem);
    procedure AdvPreviewMenu1Buttons2Click(Sender: TObject;
      Button: TButtonCollectionItem);
    procedure AdvPreviewMenu1Buttons3Click(Sender: TObject;
      Button: TButtonCollectionItem);
*)
    procedure AGBEntidadesClick(Sender: TObject);
    procedure AdvGlowButton91Click(Sender: TObject);
    procedure AdvGlowButton110Click(Sender: TObject);
    procedure AdvGlowButton96Click(Sender: TObject);
    procedure Geral1Click(Sender: TObject);
    procedure Geral2Click(Sender: TObject);
    procedure Entradas1Click(Sender: TObject);
    procedure Devolues1Click(Sender: TObject);
    procedure Outros2Click(Sender: TObject);
    procedure Textosdeobservaes1Click(Sender: TObject);
    procedure Setores1Click(Sender: TObject);
    procedure UnidadesdeMedida1Click(Sender: TObject);
    procedure CFOP1Click(Sender: TObject);
    procedure EquipesClick(Sender: TObject);
    procedure Operacoes1Click(Sender: TObject);
    procedure FornecedoresMP1Click(Sender: TObject);
    procedure Balanascomunicveis1Click(Sender: TObject);
    procedure Todosnveis1Click(Sender: TObject);
    procedure Planos1Click(Sender: TObject);
    procedure Conjuntos1Click(Sender: TObject);
    procedure Grupos1Click(Sender: TObject);
    procedure Subgrupos1Click(Sender: TObject);
    procedure Contas1Click(Sender: TObject);
    procedure Carteiras1Click(Sender: TObject);
    procedure Bancos1Click(Sender: TObject);
    procedure AdvGlowButton105Click(Sender: TObject);
    procedure AdvGlowButton113Click(Sender: TObject);
    procedure AdvGlowButton115Click(Sender: TObject);
    procedure AdvGlowButton116Click(Sender: TObject);
    procedure AdvGlowButton114Click(Sender: TObject);
    procedure AdvGlowButton117Click(Sender: TObject);
    procedure AdvGlowButton118Click(Sender: TObject);
    procedure AdvGlowButton119Click(Sender: TObject);
    procedure AdvGlowButton120Click(Sender: TObject);
    procedure AdvGlowButton121Click(Sender: TObject);
    procedure AdvGlowButton123Click(Sender: TObject);
    procedure AdvGlowButton122Click(Sender: TObject);
    procedure AdvGlowButton124Click(Sender: TObject);
    procedure AdvGlowButton125Click(Sender: TObject);
    procedure AdvGlowButton126Click(Sender: TObject);
    procedure AdvGlowButton127Click(Sender: TObject);
    procedure AdvGlowButton128Click(Sender: TObject);
    procedure AdvGlowButton129Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BtEntiProSoftClick(Sender: TObject);
    procedure AGBSintegraClick(Sender: TObject);
    procedure AdvGlowButton131Click(Sender: TObject);
    procedure AdvGlowButton132Click(Sender: TObject);
    procedure AdvGlowButton133Click(Sender: TObject);
    procedure ListaEstoqueHistrico1Click(Sender: TObject);
    procedure AdvGlowButton135Click(Sender: TObject);
    procedure AdvGlowButton136Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure GBMPReclasClick(Sender: TObject);
    procedure AdvGlowButton139Click(Sender: TObject);
    procedure AdvGlowButton140Click(Sender: TObject);
    procedure AdvGlowButton142Click(Sender: TObject);
    procedure AdvGlowButton143Click(Sender: TObject);
    procedure AdvGlowButton144Click(Sender: TObject);
    procedure AdvGlowButton146Click(Sender: TObject);
    procedure FormatoA1Click(Sender: TObject);
    procedure FormatoB1Click(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
    procedure AdvGlowButton147Click(Sender: TObject);
    procedure AdvGlowButton68Click(Sender: TObject);
    procedure Emitidos1Click(Sender: TObject);
    procedure Quitados1Click(Sender: TObject);
    procedure AdvGlowButton30Click(Sender: TObject);
    procedure AdvGlowButton149Click(Sender: TObject);
    procedure AdvGlowButton148Click(Sender: TObject);
    procedure AdvGlowButton150Click(Sender: TObject);
    procedure AdvGlowButton151Click(Sender: TObject);
    procedure AdvGlowButton152Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure AdvGlowButton154Click(Sender: TObject);
    procedure AdvGlowButton155Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure AdvGlowButton156Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure AdvGlowButton157Click(Sender: TObject);
    procedure AdvGlowButton158Click(Sender: TObject);
    procedure AdvGlowButton159Click(Sender: TObject);
    procedure GBInfCplClick(Sender: TObject);
    procedure AdvGlowButton88Click(Sender: TObject);
    procedure GBFabricasClick(Sender: TObject);
    procedure GBClasFiscClick(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure AdvGlowButton84Click(Sender: TObject);
    procedure GBUsoConsCabClick(Sender: TObject);
    procedure GBNatOperClick(Sender: TObject);
    procedure GBImportaXMLClick(Sender: TObject);
    procedure GBImportaNFesClick(Sender: TObject);
    procedure GBAbreXMLClick(Sender: TObject);
    procedure AdvGlowButton160Click(Sender: TObject);
    procedure GBTipoMovClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Gerencia1Click(Sender: TObject);
    procedure GBConflitosClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure GBMPSubProdClick(Sender: TObject);
    procedure AdvGlowButton42Click(Sender: TObject);
    procedure AdvGlowButton33Click(Sender: TObject);
    procedure GBReduzidoClick(Sender: TObject);
    procedure AGB_SPED_EFD_ImpClick(Sender: TObject);
    procedure AGB_SPED_EFD_ExpClick(Sender: TObject);
    procedure Novo1Click(Sender: TObject);
    procedure Antigo1Click(Sender: TObject);
    procedure AGBEntraMPClick(Sender: TObject);
    procedure AGBCorrigeFatIDClick(Sender: TObject);
    procedure AGBConfEstqClick(Sender: TObject);
    procedure AGBLaySPEDEFDClick(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure AGBTabsSPEDEFDClick(Sender: TObject);
    procedure AGB_SPED_ComparaClick(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure NovoGrade1Click(Sender: TObject);
    procedure AntigoPQ1Click(Sender: TObject);
    procedure SPED20101Click(Sender: TObject);
    procedure Geral3Click(Sender: TObject);
    procedure BtTemaClick(Sender: TObject);
    procedure AGBApurICMSIPIClick(Sender: TObject);
    procedure Usoeconsumo1Click(Sender: TObject);
    procedure Energiaeltricaguags1Click(Sender: TObject);
    procedure ComunicaoTelecomunicao1Click(Sender: TObject);
    procedure UsoeconsumoSPED1Click(Sender: TObject);
    procedure D100Frete07088B0910112627571Click(Sender: TObject);
    procedure AGBCarteirasClick(Sender: TObject);
    procedure AGBCartNiv2Click(Sender: TObject);
    procedure AGBReceDespClick(Sender: TObject);
    procedure AGBContingenciaClick(Sender: TObject);
    procedure AGBImportaNFeWebClick(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure AGBNFeEventosClick(Sender: TObject);
    procedure AGBConsultaNFeClick(Sender: TObject);
    procedure AGBNFeExportaXML_BClick(Sender: TObject);
    procedure AGBOpcoesGradClick(Sender: TObject);
    procedure AdvToolBarButton7Click(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure AGBNFeConsCadClick(Sender: TObject);
    procedure AdvGlowButton55Click(Sender: TObject);
    procedure AGBMPRecebImpClick(Sender: TObject);
    procedure AGBNFeDestClick(Sender: TObject);
    procedure AGBNFeDesDowCClick(Sender: TObject);
    procedure Caleirocurtimento1Click(Sender: TObject);
    procedure Recurtimento1Click(Sender: TObject);
    procedure Acabamento1Click(Sender: TObject);
    procedure AGBWBInnCabClick(Sender: TObject);
    procedure AGBWBMovImpClick(Sender: TObject);
    procedure AGBWBOutCabClick(Sender: TObject);
    procedure AGBWBArtCabClick(Sender: TObject);
    procedure AdvGlowButton20Click(Sender: TObject);
    procedure AdvGlowButton35Click(Sender: TObject);
    procedure AGBWBAjsCabClick(Sender: TObject);
    procedure AGBWBPalletsClick(Sender: TObject);
    procedure AGBWBMPrCabClick(Sender: TObject);
    procedure AGBWBRclCabClick(Sender: TObject);
    procedure IdIPWatch1Status(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure AGBPQRAjuInnClick(Sender: TObject);
    procedure AGBPQRAjuOutClick(Sender: TObject);
    procedure AdvGlowButton2Click(Sender: TObject);
    procedure AGBNewFinMigraClick(Sender: TObject);
    procedure PageControl1Enter(Sender: TObject);
    procedure PageControl1MouseEnter(Sender: TObject);
    procedure PageControl1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AGBVSNatCadClick(Sender: TObject);
    procedure AGBVSRibCadClick(Sender: TObject);
    procedure AGBVSInnCabClick(Sender: TObject);
    procedure AGBVSGeraArtCabClick(Sender: TObject);
    procedure AGBVSPalletClick(Sender: TObject);
    procedure AGBVSPalStaClick(Sender: TObject);
    procedure ComearnovaOC1Click(Sender: TObject);
    procedure AGBVSMovImpClick(Sender: TObject);
    procedure AGBVSOutCabClick(Sender: TObject);
    procedure AGBVSOutItsClick(Sender: TObject);
    procedure AGBVSRibClaClick(Sender: TObject);
    procedure AGBVSSerFchClick(Sender: TObject);
    procedure ReabreTabelas1Click(Sender: TObject);
    procedure MenuItem13Click(Sender: TObject);
    procedure AGBFatParcelaNfeCabYClick(Sender: TObject);
    procedure CamposdaNFe1Click(Sender: TObject);
    procedure AGBVSMrtCabClick(Sender: TObject);
    procedure AGBVSOpeCabClick(Sender: TObject);
    procedure AGBVSRibOpeClick(Sender: TObject);
    procedure RecomearclkassificaodeOCjconfigurada2Click(Sender: TObject);
    procedure RecomearclkassificaodeOCjconfigurada1Click(Sender: TObject);
    procedure Gerenciamento1Click(Sender: TObject);
    procedure AGBVSMovItsClick(Sender: TObject);
    procedure Gerenciamento2Click(Sender: TObject);
    procedure AGBVSDsnCabClick(Sender: TObject);
    procedure AdvGlowButton67Click(Sender: TObject);
    procedure AdvGlowButton82Click(Sender: TObject);
    procedure AGBVSBoxesClick(Sender: TObject);
    procedure AGBVSFichasClick(Sender: TObject);
    procedure ClasificaoMassiva1Click(Sender: TObject);
    procedure Prepara1Click(Sender: TObject);
    procedure Antigo2Click(Sender: TObject);
    procedure Novo2Click(Sender: TObject);
    procedure NovaConfiguraodeOC1Click(Sender: TObject);
    procedure AGBOCsClick(Sender: TObject);
    procedure AGBVSEqzCabClick(Sender: TObject);
    procedure AdvGlowButton102Click(Sender: TObject);
    procedure AdvGlowButton106Click(Sender: TObject);
    procedure AGBVSCfgEqzClick(Sender: TObject);
    procedure AGBVSPlCCabClick(Sender: TObject);
    procedure AGBVSExBCabClick(Sender: TObject);
    procedure AGBVSMotivBxaClick(Sender: TObject);
    procedure AGBGraGruYClick(Sender: TObject);
    procedure AGBVSEntiMPClick(Sender: TObject);
    procedure AdvGlowButton130Click(Sender: TObject);
    procedure AGBVSCGICabClick(Sender: TObject);
    procedure AGBVSWetEndClick(Sender: TObject);
    procedure AGBVSPedCabClick(Sender: TObject);
    procedure AGBVSPWECabClick(Sender: TObject);
    procedure AGBVSFinClaClick(Sender: TObject);
    procedure DevoluoDefinitivaAoEstoque1Click(Sender: TObject);
    procedure RetornoParaRetrabalhoEPosteriorReenvio1Click(Sender: TObject);
    procedure AGBVSSubPrdClick(Sender: TObject);
    procedure ReclassificaoMassiva1Click(Sender: TObject);
    procedure AGBVSReqMovClick(Sender: TObject);
    procedure AGBTribDefCadClick(Sender: TObject);
    procedure AGBTribDefCabClick(Sender: TObject);
    procedure AGBVSMovItbClick(Sender: TObject);
    procedure AdvToolBarPagerNovoChange(Sender: TObject);
    procedure GerenciamentodePrReclasse1Click(Sender: TObject);
    procedure IMEIssemFornecedor1Click(Sender: TObject);
    procedure IMEIsGmeosorfos1Click(Sender: TObject);
    procedure AGBVSTrfLocCabClick(Sender: TObject);
    procedure iposdeArtigo1Click(Sender: TObject);
    procedure nica1Click(Sender: TObject);
    procedure Vrias1Click(Sender: TObject);
    procedure AGBVSFchRslCabClick(Sender: TObject);
    procedure AGBVSPwdDdClick(Sender: TObject);
    procedure AGBVSCalCabClick(Sender: TObject);
    procedure AGBVSCurCabClick(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure BtAjustePQwClick(Sender: TObject);
    procedure AGBPararClick(Sender: TObject);
    procedure AGBConsLeitClick(Sender: TObject);
    procedure AdvGlowButton161Click(Sender: TObject);
    procedure AGBImprimeVSEmProcBHClick(Sender: TObject);
    procedure AdvGlowButton162Click(Sender: TObject);
    procedure AGBCorrigeNFesIMIsClick(Sender: TObject);
    procedure AdvGlowButton163Click(Sender: TObject);
    procedure AGBVSCOPCabClick(Sender: TObject);
    procedure AdvGlowMenuButton5Click(Sender: TObject);
    procedure IMEI1Click(Sender: TObject);
    procedure IMEC1Click(Sender: TObject);
    procedure Reduzido1Click(Sender: TObject);
    procedure Entidade1Click(Sender: TObject);
    procedure AGBGraGruEGerClick(Sender: TObject);
    procedure reaVSm21Click(Sender: TObject);
    procedure AGBVSPSPCabClick(Sender: TObject);
    procedure AGBVSRRMCabClick(Sender: TObject);
    procedure AGBAtzArtigosPalletsClick(Sender: TObject);
    procedure AGBSPEDEFDEnceClick(Sender: TObject);
    procedure AdvGlowButton164Click(Sender: TObject);
    procedure AdvGlowButton166Click(Sender: TObject);
    procedure AdvGlowButton165Click(Sender: TObject);
    procedure AdvGlowButton167Click(Sender: TObject);
    procedure AGBVSArtCabClick(Sender: TObject);
    procedure PesoVSkg1Click(Sender: TObject);
    procedure AGBVSCPMRSBCbClick(Sender: TObject);
    procedure AGBVSMovCabClick(Sender: TObject);
    procedure AdvGlowButton45Click(Sender: TObject);
    procedure IECInformaodeEntradadeCouros1Click(Sender: TObject);
    procedure Inconsistnciadeinformao1Click(Sender: TObject);
    procedure AGBInfoIncCabClick(Sender: TObject);
    procedure NFe1Click(Sender: TObject);
    procedure IDAntes1Click(Sender: TObject);
    procedure reaVSm1Click(Sender: TObject);
    procedure Nmeroainformar1Click(Sender: TObject);
    procedure Componenteativo1Click(Sender: TObject);
    procedure NFeRecebida1Click(Sender: TObject);
    procedure N26EstoqueCustoIntegrado1Click(Sender: TObject);
    procedure AGBVSMovimIDClick(Sender: TObject);
    procedure AGBClaReCoClick(Sender: TObject);
    procedure AdvGlowButton57Click(Sender: TObject);
    procedure Consumo1Click(Sender: TObject);
    procedure Devoluonovo1Click(Sender: TObject);
    procedure AGBGraGruGruXCouClick(Sender: TObject);
    procedure AdvGlowButton90Click(Sender: TObject);
    procedure AdvGlowButton138Click(Sender: TObject);
    procedure AdvGlowButton168Click(Sender: TObject);
    procedure Movimentaes1Click(Sender: TObject);
    procedure BaixaxestoquedeInNatura1Click(Sender: TObject);
    procedure Outrasbaixasxestoque1Click(Sender: TObject);
    procedure Geraoxestoque1Click(Sender: TObject);
    procedure VSxPesagemPQ1Click(Sender: TObject);
    procedure Fretesemretorno1Click(Sender: TObject);
    procedure Envioparaposteriorretorno1Click(Sender: TObject);
    procedure CTes1Click(Sender: TObject);
    procedure N21RendimentoSemi1Click(Sender: TObject);
    procedure N24RendimentoeMO1Click(Sender: TObject);
    procedure ImgLogoClick(Sender: TObject);
    procedure SairdoERP1Click(Sender: TObject);
    procedure Opes1Click(Sender: TObject);
    procedure Opesespecficas1Click(Sender: TObject);
    procedure Matriz1Click(Sender: TObject);
    procedure Filiais1Click(Sender: TObject);
    procedure SbLoginClick(Sender: TObject);
    procedure SbAtualizaERPClick(Sender: TObject);
    procedure SbBackupClick(Sender: TObject);
    procedure SbFavoritosClick(Sender: TObject);
    procedure SbWSuportClick(Sender: TObject);
    procedure VerificaBDServidor2Click(Sender: TObject);
    procedure VerificaTabelasTerceiros2Click(Sender: TObject);
    procedure SbVerificaDBClick(Sender: TObject);
    procedure sd1FormSkin(Sender: TObject; aName: string; var DoSkin: Boolean);
    procedure SbPopupGeralClick(Sender: TObject);
    procedure SbLastWork1Click(Sender: TObject);
    procedure SbLastWork2Click(Sender: TObject);
    procedure SbVSPesqSeqPecaClick(Sender: TObject);
    procedure SbMinimizaMenuClick(Sender: TObject);
    procedure BtEntidadesClick(Sender: TObject);
    procedure BtCambioClick(Sender: TObject);
    procedure BtCadastroInsumosClick(Sender: TObject);
    procedure BtEmiteDuplicataClick(Sender: TObject);
    procedure BtEmiteNotaPromissoriaClick(Sender: TObject);
    procedure BtRelatorioInsumosClick(Sender: TObject);
    procedure BtPesagemCalCurClick(Sender: TObject);
    procedure BtPesagemRecurtClick(Sender: TObject);
    procedure BtReceitaRibeiraClick(Sender: TObject);
    procedure BtTrocaDeProdutoClick(Sender: TObject);
    procedure BtPesagemAcabtoClick(Sender: TObject);
    procedure BtReceitaAcabtoClick(Sender: TObject);
    procedure BtControleVendasClick(Sender: TObject);
    procedure BtMpvImpClick(Sender: TObject);
    procedure BitBtn21Click(Sender: TObject);
    procedure BtOSsEmAbertoClick(Sender: TObject);
    procedure BitBtn17Click(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure BitBtn15Click(Sender: TObject);
    procedure BtCouroTerceirosClick(Sender: TObject);
    procedure BitBtn23Click(Sender: TObject);
    procedure BitBtn25Click(Sender: TObject);
    procedure BitBtn24Click(Sender: TObject);
    procedure BitBtn26Click(Sender: TObject);
    procedure AdvGlowButton100Click(Sender: TObject);
    procedure AdvGlowButton95Click(Sender: TObject);
    procedure AdvGlowButton93Click(Sender: TObject);
    procedure AdvGlowButton101Click(Sender: TObject);
    procedure AdvGlowButton32Click(Sender: TObject);
    procedure AGMBSkinClick(Sender: TObject);
    procedure AdvGlowButton4Click(Sender: TObject);
    procedure GBBaixaOutrosClick(Sender: TObject);
    procedure ATBFormulasImpXXClick(Sender: TObject);
    procedure AdvGlowButton39Click(Sender: TObject);
    procedure AGBVSMOEnvXXXClick(Sender: TObject);
    procedure AGBVSClaArtClick(Sender: TObject);
    procedure AGBVSRclArtClick(Sender: TObject);
    procedure AGBVSDvlRtbClick(Sender: TObject);
    procedure AdvGlowMenuButton6Click(Sender: TObject);
    procedure AGBCadDiversosClick(Sender: TObject);
    procedure AGBVSCfgEFDClick(Sender: TObject);
    procedure AdvGlowMenuButton4Click(Sender: TObject);
    procedure AdvGlowButton137Click(Sender: TObject);
    procedure AdvGlowButton50Click(Sender: TObject);
    procedure AdvGlowButton28Click(Sender: TObject);
    procedure AdvGlowButton29Click(Sender: TObject);
    procedure AdvGlowMenuButton1Click(Sender: TObject);
    procedure AGBNfeNewVerClick(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure AdvGlowButton48Click(Sender: TObject);
    procedure AdvGlowButton54Click(Sender: TObject);
    procedure N00Geral1Click(Sender: TObject);
    procedure AdvGlowButton47Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Feriadosfuturos1Click(Sender: TObject);
    procedure Estgios1Click(Sender: TObject);
    procedure PCPFluxos1Click(Sender: TObject);
    procedure Aproduzirnodiaporsetor1Click(Sender: TObject);
    procedure Previsodeconsumo1Click(Sender: TObject);
    procedure BtPURelatPQClick(Sender: TObject);
    procedure N101Produo1Click(Sender: TObject);
    procedure N05ClassesporMartelo1Click(Sender: TObject);
    procedure SbFinancasClick(Sender: TObject);
    procedure SbEntidadeClick(Sender: TObject);
    procedure Marcaodedefeitosteste1Click(Sender: TObject);
    procedure BtGraGruYPrecosClick(Sender: TObject);
    procedure AGBFinEncerMesClick(Sender: TObject);
    procedure SubstratosdeRendimento1Click(Sender: TObject);
    procedure BtMapaOperPosProcClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure Gruposdereceitas1Click(Sender: TObject);
    procedure ConsumoGrupodeFrmulas1Click(Sender: TObject);
    procedure CustoSimuladodeArtigo1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtPQIClick(Sender: TObject);
    procedure BtPQNClick(Sender: TObject);
    procedure BtMovimentacaoFiscalNoContabilClick(Sender: TObject);
    procedure BtGruposContabeisClick(Sender: TObject);
    procedure BtCtbBalPCab1Click(Sender: TObject);
    procedure BtCtbBalPCab2Click(Sender: TObject);
    procedure BtSrvTomInnCabClick(Sender: TObject);
    procedure BtSrvTomTaxCadClick(Sender: TObject);
    procedure BtSrvTomRetCabClick(Sender: TObject);
    procedure Caleiro1Click(Sender: TObject);
    procedure Curtimento1Click(Sender: TObject);
    procedure BtCentroCustoClick(Sender: TObject);
    procedure BtCentroCust2Click(Sender: TObject);
    procedure BtCentroCust3Click(Sender: TObject);
    procedure BtCentroCust4Click(Sender: TObject);
    procedure BtCentroCust5Click(Sender: TObject);
    procedure BtCentroCustAllClick(Sender: TObject);
    procedure BtSoliComprCab1Click(Sender: TObject);
    procedure BtSPED_Contribui��esClick(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BtEfdInnCTsGerClick(Sender: TObject);
    procedure BtLe2Click(Sender: TObject);
    procedure BtCtSub1Click(Sender: TObject);
    procedure BtDREClick(Sender: TObject);
    procedure BtDreCfgCabClick(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BtVSConCabClick(Sender: TObject);
    procedure SaldosPositivos1Click(Sender: TObject);
    procedure Gerencia3Click(Sender: TObject);
    procedure BtImpETEClick(Sender: TObject);
    procedure Slidossedimentveis1Click(Sender: TObject);
    procedure N102InNaturaPendente1Click(Sender: TObject);
    procedure Artigosgeradoscommodity1Click(Sender: TObject);
    procedure OrigemBarraca1Click(Sender: TObject);
    procedure OrigemCurtimento1Click(Sender: TObject);
    procedure OrigemAmbos1Click(Sender: TObject);
    procedure BtVSExcCabClick(Sender: TObject);
    procedure N104Saldodemovimentaocorretiva1Click(Sender: TObject);
    procedure Corrigecustoreclasse1Click(Sender: TObject);
    procedure BtVSFerramentasClick(Sender: TObject);
    procedure Divergnciadecustoentreorigemedestino1Click(Sender: TObject);
    procedure Divergnciadecustodesaldovirtual1Click(Sender: TObject);
    procedure ReduzidosdePrClasseReclasseemConflito1Click(Sender: TObject);
    procedure IMEIscomcampospositivosenegativosnomesmoregistro1Click(
      Sender: TObject);
    procedure BtAtzSdosClick(Sender: TObject);
    procedure N105CustodeProduo1Click(Sender: TObject);
    procedure ReduzidosdeGeraodeArtigoemConflito1Click(Sender: TObject);
    procedure ReduzidosdeBaixaemConflitocomaOrigem1Click(Sender: TObject);
    procedure N12MovimentodecompraevendaMPAG1Click(Sender: TObject);
    procedure Custosdivergentesdeprreclasse1Click(Sender: TObject);
    procedure Custosdivergentesdereclasse1Click(Sender: TObject);
    procedure FichaKardex1Click(Sender: TObject);
    procedure CorreodecustosporIMECs1Click(Sender: TObject);
    procedure Linkdebaixasdeartigosempreocessoprocessos1Click(Sender: TObject);
    procedure PeaspositivasnegativasXValortotalnegativopositivos1Click(
      Sender: TObject);
    procedure Executartodasferramentasemsequncia1Click(Sender: TObject);
    procedure N02FichaKardexagrupado1Click(Sender: TObject);
    procedure N02FichaKardexcorrido1Click(Sender: TObject);
    procedure N106Tabeladerendimentoinnaturacurtido1Click(Sender: TObject);
    procedure IMEIscomValoresPositivoseNegativosnoMesmoregistro1Click(
      Sender: TObject);
    procedure IDJanela1Click(Sender: TObject);
    procedure N107Estoquedereduzido1Click(Sender: TObject);
    procedure Gruposdeusurios1Click(Sender: TObject);
    procedure LoadWebServices1Click(Sender: TObject);
    procedure GerenciarWebServices1Click(Sender: TObject);
    procedure Veculos2Click(Sender: TObject);
    procedure Marcas1Click(Sender: TObject);
    procedure Modelos1Click(Sender: TObject);
    procedure Faturados1Click(Sender: TObject);
    procedure Expedidos1Click(Sender: TObject);
    procedure BtControleVendasNovoClick(Sender: TObject);
    procedure PQporNF1Click(Sender: TObject);
    procedure RecriartodasbaixasporNF1Click(Sender: TObject);
    procedure IncongrunciasdeestoquedeUsoeConsumo1Click(Sender: TObject);
    procedure CorrigeGrupodocadastrodeInsumosemMassa1Click(Sender: TObject);
    procedure RecalcularsaldodeNFs1Click(Sender: TObject);
    procedure BtMarcasClick(Sender: TObject);
    procedure PorMarca1Click(Sender: TObject);
    procedure BtBanhosImpClick(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BtVSSifDipoaClick(Sender: TObject);
    procedure N108SIFDIPOA1Click(Sender: TObject);
  private
    { Private declarations }
    FALiberar: Boolean;
    FTamLinha, FFoiExpand: Integer;
    FArqPrn: TextFile;
    FPrintedLine: Boolean;
    procedure VerificaPQ_GGX();
    procedure VerificaMP_GGX();
    procedure SkinMenu(Index: integer);
    function  AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
    function  EspacoEntreTextos(QrView: TmySQLQuery; CPI: Integer): String;
    function  Formata(Texto, Prefixo, Sufixo: String; EspacosPrefixo, Formato:
              Integer; Nulo: Boolean): String;
    function  FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
              EspacosPrefixo, Substitui, Alinhamento, Comprimento, Fonte,
              Formatacao, CPI: Integer; Nulo: Boolean): String;
    procedure MostraFormVSIndsVS(SQLType: TSQLType; Controle, MPVIts, Emit,
              EmitCus, InsEmpr: Integer; QrWBMovIts: TmySQLQuery;
              FormaWBIndsWE: TFormaWBIndsWE);
    procedure AppIdle(Sender: TObject; var Done: Boolean);
  public
    { Public declarations }
    //FImportPath: String;
    FLinModErr: Integer;
    FTipoEntradTitu: String;
    FTipoEntradaDig, FTipoEntradaNFe, FTipoEntradaEFD,
    FCliIntUnico, FEntInt, FTipoNovoEnti: Integer;
    FParar: Boolean;
    //
    procedure DefineVarsCliInt(CliInt: Integer);
    procedure MyOnHint(Sender: TObject);
    procedure RetornoCNAB;
    function PreparaMinutosSQL: Boolean;
    function AdicionaMinutosSQL(HI, HF: TTime): Boolean;
    procedure AcoesIniciaisDoAplicativo();
    procedure MostraParamsEmp();
    procedure CadastroMPIn();
    procedure CadastroFluxos();
    procedure CadastroFornecedorMP();
    procedure CadastroEquipes();
    procedure CadastroDeEspessuras();
    procedure CadastroDeSetores(Codigo: Integer);
    procedure CadastroDeDefPecas();
    procedure CadastroDeCambio();
    procedure CadastroDeArtigosGrupos();
    procedure CadastroDeMPEstagios();
    procedure CadastroDeImpObs();
    procedure CadastroEntiMP();
    procedure CadastroPortSerBal();
    procedure CadastroBancos();
    // Financeiros
    function CadastroDeContasSdoSimples(Entidade, Conta: Integer): Boolean;
    procedure CadastroDeContasSdoLista();
    //procedure CadastroDeCarteiras(TipoCarteira, ItemCarteira: Integer);
    procedure CadastroDeCarteiras(TipoCarteira, ItemCarteira, x: Integer);
    procedure CadastroDeCarteira;
    procedure CadastroDeContas;
    procedure CadastroDeSubGrupos;
    procedure CadastroDeGrupos;
    procedure CadastroDeConjuntos();
    procedure CadastroDePlano();
    procedure CadastroDeNiveisPlano();
    procedure CadastroDeContasNiv();
    procedure CriaImpressaoDiversos(Indice: Integer);
    procedure SaldoDeContas;
    procedure AtzSdoContas;
    procedure ImpressaoDoPlanoDeContas;
    procedure CadastroCFOP2003();

(*
    procedure ImprimeChequeAtual(ChAtu, ChMax, ChConfCab, CPI, TopoIni, Altura,
              MEsq: Integer; Dia, Mes, Ano, Vcto, Valor, Favorecido, Extenso1,
              Extenso2, Obs, Cidade: String);
*)

    procedure SelecionaImagemdefundo;
    procedure MostraBackup;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid; Entidade: Integer; Aba: Boolean);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade1: TStringGrid);
    //function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure AjustaEstiloImagem(Index: integer);
    function CartaoDeFatura: Integer;
    function CompensacaoDeFatura: String;
    procedure MostraCarteiras(Carteira: Integer);
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );

    procedure MostraFisRegCad;
    procedure MostraCFOP2003();
    procedure MostraModelosImp;
    procedure MostraPediPrzCab(Codigo: Integer);
    procedure MostraPediAcc;
    procedure MostraGeosite;
    procedure MostraRegioes();
    procedure MostraTabePrcCab;
    procedure MostraCambioMda;
    procedure CadastroFormulas(Numero, Controle: Integer);
    procedure CadastroTintas(Numero, Codigo, Controle: Integer);
    //procedure CadastroOpcoes();
    procedure CadastroGruposQuimicos();
    procedure CadastroMatriz();
    procedure CadastroPallets();

    procedure VerificaBD();
    function VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;

    //procedure RefreshInsumos;
    procedure PedidosGerencia(Direto: Boolean);
    procedure PedidosRelatorios;
    procedure CadastroPQ(Codigo: Integer);
    procedure CadastroPQImp(FormOrig: String);
    procedure StatusPedido(Controle: Integer; DataAtual: TDateTime);
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;
    function ObtemUnidadeTxt(Unidade: Integer): String;
    function ObtemMoedaTxt(Moeda: Integer): String;
    function ObtemPeriodoTxt(Periodo: Integer): String;
    function AvisaFaltaDeContaDoPlano(Item: Integer): Boolean;
    //procedure MostraCalculadoraDeramtek(Sender: TObject);
(*
    procedure AtualizaBalancoDePQNoSMI(PB: TProgressBar);
    function  CriaFormEntradaCab(MaterialNF: TTipoMaterialNF;
              ShowForm: Boolean; IDCtrl: Integer): Boolean;
*)
    procedure ReCaptionComponentesDeForm(Form: TForm);
    //
    procedure MostraDefeitostip(Codigo: Integer);
    procedure MostraDefeitosloc(Codigo: Integer);
    procedure MostraFormWBRclIns(SQLType: TSQLType; Filial, MovimCod, MovimTwn,
              Codigo, Controle: Integer; ValorT, AreaM2: Double;
              QrReopen: TmySQLQuery; SaldoPecas: Double; WBRclass: TWBRclass);
    procedure MostraFormWBAjsCab(Codigo, Controle: Integer);
    procedure MostraFormWBPallet();
    procedure MostraFormWBInnCab(Codigo, WBInnIts, WBReclas: Integer);
    procedure MostraFormWBOutCab(Codigo, Controle: Integer);
    procedure MostraFormWBOutIts(SQLType: TSQLType; Controle: Integer;
              QrCab, QrIts: TmySQLQuery);
    procedure MostraFormWBIndsWE(SQLType: TSQLType; Controle, MPVIts,
              Emit, EmitCus, InsEmpr: Integer; QrWBMovIts: TmySQLQuery;
              FormaWBIndsWE: TFormaWBIndsWE);
    procedure MostraFormWBRclCab(Codigo, WBRclIts, WBReclas: Integer);
    procedure MostraFormBalancoPQ();
    procedure ExtratosFinanceirosEmpresaUnica(TabLctA: String);
    function  FixBugAtualizacoesApp(FixBug: Integer; FixBugStr: String): Boolean;
    // Provis�rio
    procedure Mover_PrdGrupTip_do_GraGruY_Para_GraGrYPGT();
    procedure InfoSeqIni(Msg: String);

    // Fim provis�rio

  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses UnMyObjects, Module, Entidades, MyGlyfs, MyDBCheck, BlueDerm_Dmk, UnEiia_PF,
  //Opcoes,
  MyListas, MyVCLSkin, UCreate, PQ, Embalagens, UnGFat_Jan, UnALL_Jan,
  ListaSetores, GruposQuimicos, Formulas, CalcPercent, PQx, PQE,
  (*PQB,*) PQB2, PQB3,
  DefPecas, PQImp, MPIn, Defeitosloc, Defeitostip, About, UnApp_Jan,
  MPClas, MPGrup, Pallets, MPVn,
  Equipes, FluxosCab, Senha, ArtigosGrupos, Duplicata1, Imprime,
  PQPed, MPP, MPPImp, Espessuras, EntiMP, UnMsgInt, PortSerBal,
  (*NF1,*) CFOP2003, MPEstagios, MPV2, Contas, SubGrupos, Grupos, PlaCtas, Plano,
  Carteiras, Conjuntos, ImpObs, Promissoria, NovaVersao, Backup3, ContasNiv,
  CMPTInn, CMPTOut, RolPerdas, TintasCab, Bancos, ServicoManager, Entidade2,
  Matriz, ParamsEmp, ModuleGeral, ContasSdoImp, CambioMda, CambioCot, StqManCad,
  ModPediVda, PediVdaImp2, PediAcc, PediPrzCab1, (*PediPrzCab2,*) (*PediVda,*)
{$IfDef cDbTable}
  Regioes, IBGE_DTB,
{$EndIf}
  TabePrcCab, Geosite, (*SugVPedCab,*) NCMs, UnidMed, FisRegCad,
  PrdGrupTip, GraGruN, GraCorPan, GraCorCad, GraCorFam, GraTamCad, GraCusPrc,
  StqCenCad, StqBalCad, UFs, ListServ, GraSrv1, CNAE21, ModProd,
  EntiLoad02, GraImpLista, (*CMPPVai,*) MPClasMulGer,
  StqInnCad, PreEmail, ModuleFin, CECTInn, CECTOut, CashBal,
  Prosoft1, LeDataArq, LoadXML, Email_SSL, Restaura2, NFMoDocFis,
  PQCorrigeFatID, Fabricas, ClasFisc, NFaEditCSOSNAlq, (*EntradaCab,*) NatOper,
  TesteXML, PrdGruNewU, MPReclasGer, MPReclasIns, GraImpConflitos, MPSubProd,
  GraOptEnt, PQxExcl2, Maladireta, GraGruReduzido, GetValor,
  (* PQRet, PQRCab, PQRAjuInn, PQRAjuOut, *)
(*
  SPED_EFD_Importa, EfdIcmsIpiExporta, SPED_EFD_Compara,
  EFD_E001, EFD_C001, EFD_D001, ImportaSintegraGer, Sintegra_Arq,
*)
  SPED_EFD_DownTabs, SPEDEstqGer,
  //
  UnGrade_Tabs, ReceDesp, MPRecebImp,
  UnDmkWeb, PQSubstitui, WBInnCab, WBMovImp, WBRclIns,
  DmkDAC_PF, WBOutCab, WBArtCab, CfgCadLista, WBOutIts, WBAjsCab, WBIndsWE,
  WBMPrCab, WBRclCab, WBPallet,
  Resultados1, Resultados2, LctAjustesB, CartNiv2, SelfGer2, ModuleLct2,

  // Provisorio
  TesteChart, Unit1, VSCorrigeMulFrn, UnAppPF, GerlShowGrid, TesteThread,
  //Principal3, Principal2,
  CoresVCLSkin, CNPJaConsulta,
  UnSPED_PF, NFeImporta_0400,
  // Fim provisorio

  (* Removido
  LinkRankSkin,
  Fim Removido!!!*)

  ContasHistSdo3, UnVS_PF, ModuleNFe_0000, NFe_PF, UnGrade_Jan, FavoritosG,
  UnTributos_PF, ModVS, VSCorrigeMovimTwn, (*EFD_RegObrig,*) UnEfdIcmsIPI_PF,
  UnEfdIcmsIpi_Jan, UnPQ_PF, UnConsumoGerlJan, UnConsumoJan, UnFixBugs,
  SPED_EFD_Importa, WBInnIts, UnUMedi_PF, VSInfIEC, ModVS_CRC, UnVS_Jan,
  VSClassifOneDefei, LinkRankSkin,
  UnEntities, UnFinanceiroJan, UnContabil_Jan, UnSrvTom_Jan, UnCeCuRe_Jan,
  UnEfdIcmsIpi_Jan_v03_0_2_a, UnEfdIcmsIpi_Jan_v03_0_9,
  UnEfdPisCofins_Jan_v01_35, EFD_RegObrig, PQWRefaz;

const
  FAltLin = CO_POLEGADA / 2.16;

{$R *.DFM}

procedure TFmPrincipal.Linkdebaixasdeartigosempreocessoprocessos1Click(
  Sender: TObject);
const
 //Empresa = 0;
 //GraGruX = 0;
 Avisa = False;
 ForcaMostrarForm = True;
 SelfCall = False;
 TemIMEIMrt = 0;
begin
  DmModVS.VmiPaiDifProcessos(ForcaMostrarForm, SelfCall, TemIMEIMrt, nil, nil);
end;

procedure TFmPrincipal.ListaEstoqueHistrico1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraImpLista, FmGraImpLista, afmoNegarComAviso) then
  begin
    FmGraImpLista.ShowModal;
    FmGraImpLista.Destroy;
  end;
end;

procedure TFmPrincipal.LoadWebServices1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeWebservices();
end;

procedure TFmPrincipal.Locais2Click(Sender: TObject);
begin
  MostraDefeitosloc(0);
end;

procedure TFmPrincipal.Faturados1Click(Sender: TObject);
begin
  App_Jan.MostraFormMpvImp(TMPVImp.mpvimpFatur);
end;

procedure TFmPrincipal.Feriadosfuturos1Click(Sender: TObject);
begin
  UMyMod.VerificaFeriadosFuturos(TFmFeriados, FmFeriados, False);
end;

procedure TFmPrincipal.FichaKardex1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSImpKardex4();
end;

procedure TFmPrincipal.Filiais1Click(Sender: TObject);
begin
  MostraParamsEmp();
end;

function TFmPrincipal.FixBugAtualizacoesApp(FixBug: Integer;
  FixBugStr: String): Boolean;
begin
  Result := True;
  try
    if FixBug = 0 then
      Result := True
    else if FixBug = 1 then
      Result := AppPF.CorrigeCamposNFemPQe()
    else
      Result := False;
  except
    Result := False;
  end;
end;

procedure TFmPrincipal.Fluxos3Click(Sender: TObject);
begin
  CadastroFluxos();
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
procedure ShowParamStr();
var
  j: integer;
  x: String;
  begin
    for j := 1 to ParamCount do
    begin
      if Lowercase(ParamStr(j)) = 'msgstart' then
        VAR_MSG_START := True;
    end;
  end;
  //
 var
  MenuStyle: Integer;
  Imagem(*, Avisa*): String;
  //i, j: Integer;
begin
  ShowParamStr();
  VAR_CAMBIO_DATA       := 0;
  VAR_MULTIPLAS_TAB_LCT := True;
  VAR_TYPE_LOG          := ttlFiliLog;
  //VAR_ADMIN             := 'owemqOQWD';
  VAR_USA_PED_SIT       := True;
  //
  {$IfDef cAdvToolx} //Tokyo
  VAR_AdvToolBarPagerPrincipal := AdvToolBarPager2;
  {$Else}
  VAR_PageControlMenuPrincipal := AdvToolBarPagerNovo;
  {$EndIf}
  AdvToolBarPagerNovo.ActivePageIndex := 0;
  VAR_PageControlFormsTabPrincipal := PageControl1;
  //
  //VAR_CALCULADORA_COMPONENTCLASS := TFmCalculadora;
  //VAR_CALCULADORA_REFERENCE      := FmCalculadora;
  //
  PnVendaCouro.Visible := False;
  //
  Geral.DefineFormatacoes;
  Application.OnException := MyObjects.MostraErro;
  VAR_USA_TAG_BITBTN := True;
  VAR_CAMINHOSKINPADRAO := 'C:\Dermatek\Skins\VCLSkin\Office 2007.skn';
  FTipoNovoEnti := 0;
  VAR_STLOGIN       := StatusBar.Panels[01];
  //
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  //
  VAR_STEMPRESA     := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  //VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[09];
  VAR_SKINUSANDO    := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  Application.OnMessage := MyObjects.FormMsg;
  Application.OnIdle    := AppIdle;
  //
  Imagem := Geral.ReadAppKey(
    'ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg', HKEY_LOCAL_MACHINE);
  //
  PageControl1.Align := alClient;
  //
  if FileExists(Imagem) then
  begin
    ImgPrincipal.Picture.LoadFromFile(Imagem);
    AjustaEstiloImagem(ImgPrincipal.Tag);
  end;
  AjustaEstiloImagem(-1);
  UnPQx.SetaVariaveisIniciais;
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  SkinMenu(MenuStyle);
  VAR_CAD_POPUP := PMGeral;
  VAR_LOC_POPUP := PMLocaliza;
  AdvToolBarPagerNovo.ActivePageIndex := 0;
  // M L A G e r a l .CopiaItensDeMenu(PMGeral, Cadastros1, FmPrincipal);
  MyObjects.CopiaItensDeMenu(PMGeral, FmPrincipal);
  //
  MyObjects.MaximizaPageMenu(AdvToolBarPagerNovo, deftfTrue);
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  //
  dmkPF.ConfereVersaoApp(True);
  if not FALiberar then
    Timer1.Enabled := True;
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
  // Esse � antigo!!!
  if OpenPictureDialog1.Execute then
  begin
    ImgPrincipal.Picture.LoadFromFile(OpenpictureDialog1.FileName);
    Geral.WriteAppKey('ImagemFundo', Application.Title, OpenPictureDialog1.FileName, ktString, HKEY_LOCAL_MACHINE);
  end;
  { se for copiar para projeto novo, usar este:
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso,
    PageControl1, False);
  }
end;

procedure TFmPrincipal.Setores1Click(Sender: TObject);
begin
  CadastroDeSetores(0);
end;

procedure TFmPrincipal.Setores2Click(Sender: TObject);
begin
  CadastroDeSetores(0);
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if DModG <> nil then
    DModG.MostraBackup3(False);
  Application.Terminate;
end;

procedure TFmPrincipal.Prepara1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSReclassPrePalCac(stIns, 0, 0, 0, 0, 0, 0, 0, 0);
end;

function TFmPrincipal.PreparaMinutosSQL: Boolean;
//var
//  i: Integer;
begin
  Result := True;
  try
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Clear;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('DELETE FROM ocupacao');
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.ExecSQL;
    ///
{    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Clear;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('INSERT INTO ocupacao SET Min=:P0');
    for i := 1 to 1440 do
    begin
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[0].AsInteger := I;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.ExecSQL;
    end;}
  except
    Result := False
  end;
end;

procedure TFmPrincipal.Previsodeconsumo1Click(Sender: TObject);
begin
  App_Jan.MostraFormPQUGer();
end;

procedure TFmPrincipal.Quitados1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmResultados2, FmResultados2, afmoNegarComAviso) then
  begin
    FmResultados2.ShowModal;
    FmResultados2.Destroy;
  end;
end;

function TFmPrincipal.AdicionaMinutosSQL(HI, HF: TTime): Boolean;
var
  Hour, Min, Sec, MSec: Word;
  i, Ini, Fim: Integer;
begin
  Result := True;
  if (HI=0) and (HF=0) then Exit;
  DecodeTime(HI, Hour, Min, Sec, MSec);
  Ini := (Hour * 60) + Min;
  DecodeTime(HF, Hour, Min, Sec, MSec);
  Fim := (Hour * 60) + Min - 1;
  if Fim < Ini then Fim := Fim + 1440;
  (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Clear;
  (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('INSERT INTO ocupacao SET Qtd=1, Min=:P0');
  for i := Ini to Fim do
  begin
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[0].AsInteger := I;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.ExecSQL;
  end;
end;

procedure TFmPrincipal.AdvGlowButton100Click(Sender: TObject);
begin
  CadastroPQ(0);
end;

procedure TFmPrincipal.AdvGlowButton101Click(Sender: TObject);
begin
  CadastroPQImp('');
end;

procedure TFmPrincipal.AdvGlowButton102Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Itens, Controle: Integer;
  SubClass: String;
begin
  PageControl1.ActivePageIndex := 0;
  //
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle, SubClass ',
    'FROM vscacitsa cia  ',
    'WHERE SubClass <> "" ',
    '']);
    //
    Qry.First;
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    Itens := 0;
    while not Qry.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      SubClass := dmkPF.SoTextoLayout(Qry.FieldByName('SubClass').AsString);
      if SubClass <> Qry.FieldByName('SubClass').AsString then
      begin
        Controle := Qry.FieldByName('Controle').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
        'SubClass'], ['Controle'], [SubClass], [Controle], False) then
          Itens := Itens + 1;
      end;
      //
      Qry.Next;
    end;
    Geral.MB_Aviso(Geral.FF0(Itens) + ' itens foram corrigidos!');
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
end;

procedure TFmPrincipal.ATBFormulasImpXXClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(APMPesagem, ATBFormulasImpXX);
end;

(*
  Nota := VS_PF.NotaCouroRibeira(Pecas, Peso, AreaM2, FatorMP, FatorAR);
  Geral.MB_Info(Geral.FFT(Nota, 4, siNegativo));
*)
(*
const
  FBase64 = 'C:\Dermatek\Base64\Base64.exe';
  Caminho = 'C:\Dermatek\NFE\Temp\';
var
  Str: String;
  Comando: PChar;
  IniPath, OutPath: String;
begin
  Str :=
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwda' +
  'UcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkrayZ' +
  '4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7ezs' +
  '7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6zcrz' +
  'oplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nHDp4He' +
  '/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wrsy/Tp1' +
  '+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO9u7u9u7' +
  '+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4yaw8au6+' +
  'On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2CnrdQa/391yv2' +
  'ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
  if Str <> '' then
  begin
    if FileExists(FBase64) then
    begin
      //if FileExist(TxtPath) then
        //DeleteFile(TxtPath);
      IniPath := Caminho + 'Texto.inn';
      OutPath := dmkPF.MudaExtensaoDeArquivo(IniPath, 'out');
      Geral.SalvaTextoEmArquivo(IniPath, Str, True);
      Comando := PChar(FBase64 + ' -e ' + IniPath + ' ' + OutPath);
      //"C:\base64 -e bg2.jpg bg2.txt"
      dmkPF.WinExecAndWaitOrNot(Comando, SW_SHOW, MeEncode, True);
      //ArqExiste := FileExists(ImgPath);
    end else Geral.MB_Aviso('O aplicativo "' + FBase64 +
    '" n�o existe para descomprimir o texto "' + Str + '".');
  end;
end;
*)
(*
var
  Str: String;
  Buffer: TBytes;
  StreamInn, StreamOut: TMemoryStream;
begin
  Str :=
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwda' +
  'UcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkrayZ' +
  '4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7ezs' +
  '7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6zcrz' +
  'oplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nHDp4He' +
  '/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wrsy/Tp1' +
  '+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO9u7u9u7' +
  '+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4yaw8au6+' +
  'On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2CnrdQa/391yv2' +
  'ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
  if Str <> '' then
  begin
    StreamInn := TMemoryStream.Create;
    try
      StreamOut := TMemoryStream.Create;
      try
        StreamInn.WriteBuffer(Pointer(Str)^, Length(Str));
        StreamOut.WriteBuffer(Pointer(Str)^, Length(Str));
        //StreamInn.Position := 0;
        // Erro!
        //IdCompressorZLib1.Decompress(StreamInn, StreamOut);
        DecompressStream(StreamInn, StreamOut);
        //
        Geral.MB_Info(StreamOut.ToString);
      finally
        StreamOut.Free;
      end;
    finally
      StreamInn.Free;
    end;
  end;
end;
*)
(*
const
  cCompressedData = 1;
var
  Str: String;
  Buffer: TBytes;
  StreamInn, StreamOut: TStringStream;
  //
begin
  Str :=
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwda' +
  'UcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkrayZ' +
  '4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7ezs' +
  '7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6zcrz' +
  'oplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nHDp4He' +
  '/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wrsy/Tp1' +
  '+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO9u7u9u7' +
  '+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4yaw8au6+' +
  'On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2CnrdQa/391yv2' +
  'ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
  if Str <> '' then
  begin
    Buffer := PlatformBytesOf(Str);
SrcStream. Position: = 0;
GZStream: = TDecompressionStream. Create (SrcStream); [b]//here happens AV to 00000038 [/b address
try
DestStream. Position: = 0;
GZStream. Position: = 0;
try
repeat
bytesread: = GZStream. Read (mainbuffer, SizeOf (MainBuffer));
DestStream. Write (mainbuffer, bytesread);
until bytesread &lt;1024;
except
GZStream. Free;
exit;
    //Str := ProcessOutput(Buffer);
    //Buffer := ProcessOutput(Buffer);
    Str := ZDecompressStr(Buffer);
    //Geral.MB_Info(PlatformStringOf(Buffer));
    Geral.MB_Info(Str);
  end;
end;
*)
{
var
SrcStream, DestStream: TStringStream;
GZStream: TDecompressionStream;
BytesRead: integer;
MainBuffer: array [0..1023] of AnsiChar;
  Str: String;
begin
  Str :=
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwda' +
  'UcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkrayZ' +
  '4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7ezs' +
  '7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6zcrz' +
  'oplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nHDp4He' +
  '/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wrsy/Tp1' +
  '+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO9u7u9u7' +
  '+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4yaw8au6+' +
  'On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2CnrdQa/391yv2' +
  'ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
  if Str <> '' then
  begin
    SrcStream := TStringStream.Create;
    try
      DestStream := TStringStream.Create;
      try
        SrcStream.WriteBuffer(Pointer(Str)^, Length(Str));
        SrcStream.Position := 0;
        GZStream := TDecompressionStream.Create(SrcStream); //[b]//here happens AV to 00000038 [/b address
        try
          DestStream.Position := 0;
          GZStream.Position := 0;
        try
          repeat
          bytesread := GZStream.Read(mainbuffer, SizeOf(MainBuffer));
          DestStream.Write(mainbuffer, bytesread);
          until bytesread <> 0//&lt;1024;
        except
          GZStream.Free;
        exit;
        end;
        finally

        end;
      finally
        //
      end;
    finally
      //
    end;
  end;
end;
}
(*
var
  SrcStream, DestStream: TStringStream;
  Str: String;
begin
  Str :=
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwda' +
  'UcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkrayZ' +
  '4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7ezs' +
  '7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6zcrz' +
  'oplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nHDp4He' +
  '/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wrsy/Tp1' +
  '+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO9u7u9u7' +
  '+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4yaw8au6+' +
  'On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2CnrdQa/391yv2' +
  'ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
  if Str <> '' then
  begin
    SrcStream := TStringStream.Create;
    try
      DestStream := TStringStream.Create;
      try
        SrcStream.WriteBuffer(Pointer(Str)^, Length(Str));
        SrcStream.Position := 0;
        Str := frxDecompressStream(SrcStream, DestStream);
        Geral.MB_Aviso(Str);
      finally
        DestStream.Free;
      end;
    finally
      SrcStream.Free;
    end;
  end;
end;
*)
(*
//Op��o de Encode
var
	x, InnPath, OutPath: String;
const
  FDirXML = 'C:\Dermatek\Temp\Orig\';
  FArqXML = 'TextoXML';
  FExtB64 = '.cryp';
  FExtZip = '.gzip';
  Txt =
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwd' +
  'aUcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkra' +
  'yZ4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7' +
  'ezs7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6' +
  'zcrzoplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nH' +
  'Dp4He/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wr' +
  'sy/Tp1+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO' +
  '9u7u9u7+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4' +
  'yaw8au6+On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2Cnrd' +
  'Qa/391yv2ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
begin
  ForceDirectories(FDirXML);
  InnPath := FDirXML + FArqXML + FExtB64;
  OutPath := FDirXML + FArqXML + FExtZip;
  UnDmkCoding.DecriptaBase64(Txt, InnPath, OutPath, Memo3);
  x := UnDmkCoding.DescompactaArquivoGZipToString(OutPath);
  Geral.MB_Aviso(x);
end;
*)

procedure TFmPrincipal.AdvGlowButton105Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPrdGrupTip, FmPrdGrupTip, afmoNegarComAviso) then
  begin
    FmPrdGrupTip.ShowModal;
    FmPrdGrupTip.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton106Click(Sender: TObject);
{    ini tentativa de eliminar TmySQLQuery

  procedure InsereAtual();
  var
    DataHora, Observ, Marca: String;
    Codigo, Controle, MovimCod, MovimNiv, MovimTwn, Empresa, Terceiro, CliVenda,
    MovimID, LnkNivXtr1, LnkNivXtr2, Pallet, GraGruX, SrcMovID, SrcNivel1,
    SrcNivel2, SrcGGX, SerieFch, Ficha, Misturou, FornecMO, DstMovID, DstNivel1,
    DstNivel2, DstGGX, AptoUso, TpCalcAuto: Integer;
    Pecas, PesoKg, AreaM2, AreaP2, ValorT, SdoVrtPeca, SdoVrtPeso, SdoVrtArM2,
    CustoMOKg, CustoMOTot, ValorMP, QtdGerPeca, QtdGerPeso, QtdGerArM2,
    QtdGerArP2, QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, NotaMPAG: Double;
  begin
    Codigo         := QrVSMovItsCodigo     .Value;
    //Controle       := QrVSMovItsControle   .Value;
    MovimCod       := QrVSMovItsMovimCod   .Value;
    MovimNiv       := Integer(TEstqMovimNiv.eminSorcCurtiXX);//QrVSMovItsMovimNiv   .Value;
    //MovimTwn       := QrVSMovItsMovimTwn   .Value;
    Empresa        := QrVSMovItsEmpresa    .Value;
    Terceiro       := QrVSMovItsTerceiro   .Value;
    CliVenda       := QrVSMovItsCliVenda   .Value;
    MovimID        := QrVSMovItsMovimID    .Value;
    LnkNivXtr1     := QrVSMovItsLnkNivXtr1 .Value;
    LnkNivXtr2     := QrVSMovItsLnkNivXtr2 .Value;
    DataHora       := Geral.FDT(QrVSMovItsDataHora.Value, 109);
    Pallet         := QrVSMovItsPallet     .Value;
    GraGruX        := QrVSMovItsGraGruX    .Value;
    Pecas          := -QrVSMovItsPecas      .Value;
    PesoKg         := -QrVSMovItsPesoKg     .Value;
    AreaM2         := -QrVSMovItsAreaM2     .Value;
    AreaP2         := -QrVSMovItsAreaP2     .Value;
    ValorT         := -QrVSMovItsValorT     .Value;
    SrcMovID       := 0;//QrVSMovItsSrcMovID   .Value;
    SrcNivel1      := 0;//QrVSMovItsSrcNivel1  .Value;
    SrcNivel2      := 0;//QrVSMovItsSrcNivel2  .Value;
    SrcGGX         := 0;//QrVSMovItsSrcGGX     .Value;
    SdoVrtPeca     := QrVSMovItsSdoVrtPeca .Value;
    SdoVrtPeso     := QrVSMovItsSdoVrtPeso .Value;
    SdoVrtArM2     := QrVSMovItsSdoVrtArM2 .Value;
    Observ         := QrVSMovItsObserv     .Value;
    SerieFch       := QrVSMovItsSerieFch   .Value;
    Ficha          := QrVSMovItsFicha      .Value;
    Misturou       := QrVSMovItsMisturou   .Value;
    FornecMO       := QrVSMovItsFornecMO   .Value;
    CustoMOKg      := QrVSMovItsCustoMOKg  .Value;
    CustoMOTot     := QrVSMovItsCustoMOTot .Value;
    ValorMP        := QrVSMovItsValorMP    .Value;
    DstMovID       := QrVSMovItsDstMovID   .Value;
    DstNivel1      := QrVSMovItsDstNivel1  .Value;
    DstNivel2      := QrVSMovItsDstNivel2  .Value;
    DstGGX         := QrVSMovItsDstGGX     .Value;
    QtdGerPeca     := QrVSMovItsQtdGerPeca .Value;
    QtdGerPeso     := QrVSMovItsQtdGerPeso .Value;
    QtdGerArM2     := QrVSMovItsQtdGerArM2 .Value;
    QtdGerArP2     := QrVSMovItsQtdGerArP2 .Value;
    QtdAntPeca     := QrVSMovItsQtdAntPeca .Value;
    QtdAntPeso     := QrVSMovItsQtdAntPeso .Value;
    QtdAntArM2     := QrVSMovItsQtdAntArM2 .Value;
    QtdAntArP2     := QrVSMovItsQtdAntArP2 .Value;
    AptoUso        := QrVSMovItsAptoUso    .Value;
    NotaMPAG       := QrVSMovItsNotaMPAG   .Value;
    Marca          := QrVSMovItsMarca      .Value;
    TpCalcAuto     := QrVSMovItsTpCalcAuto .Value;

    //
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, stIns, 0);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, stIns, 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_INS_TAB_VMI, False, [
    'Codigo', 'MovimCod', 'MovimNiv',
    'MovimTwn', 'Empresa', 'Terceiro',
    'CliVenda', 'MovimID', 'LnkNivXtr1',
    'LnkNivXtr2', CO_DATA_HORA_VMI, 'Pallet',
    'GraGruX', 'Pecas', 'PesoKg',
    'AreaM2', 'AreaP2', 'ValorT',
    'SrcMovID', 'SrcNivel1', 'SrcNivel2',
    'SrcGGX', 'SdoVrtPeca', 'SdoVrtPeso',
    'SdoVrtArM2', 'Observ', 'SerieFch',
    'Ficha', 'Misturou', 'FornecMO',
    'CustoMOKg', 'CustoMOTot', 'ValorMP',
    'DstMovID', 'DstNivel1', 'DstNivel2',
    'DstGGX', 'QtdGerPeca', 'QtdGerPeso',
    'QtdGerArM2', 'QtdGerArP2', 'QtdAntPeca',
    'QtdAntPeso', 'QtdAntArM2', 'QtdAntArP2',
    'AptoUso', 'NotaMPAG', 'Marca',
    'TpCalcAuto'], [
    'Controle'], [
    Codigo, MovimCod, MovimNiv,
    MovimTwn, Empresa, Terceiro,
    CliVenda, MovimID, LnkNivXtr1,
    LnkNivXtr2, DataHora, Pallet,
    GraGruX, Pecas, PesoKg,
    AreaM2, AreaP2, ValorT,
    SrcMovID, SrcNivel1, SrcNivel2,
    SrcGGX, SdoVrtPeca, SdoVrtPeso,
    SdoVrtArM2, Observ, SerieFch,
    Ficha, Misturou, FornecMO,
    CustoMOKg, CustoMOTot, ValorMP,
    DstMovID, DstNivel1, DstNivel2,
    DstGGX, QtdGerPeca, QtdGerPeso,
    QtdGerArM2, QtdGerArP2, QtdAntPeca,
    QtdAntPeso, QtdAntArM2, QtdAntArP2,
    AptoUso, NotaMPAG, Marca,
    TpCalcAuto], [
    Controle], True) then
    begin
      Controle  := QrVSMovItsControle.Value;
      MovimNiv  := Integer(TEstqMovimNiv.eminBaixCurtiXX);
      DstMovID  := 0;
      DstNivel1 := 0;
      DstNivel2 := 0;
      DstGGX    := 0;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'DstMovID', 'DstNivel1', 'DstNivel2',
      'DstGGX', 'MovimNiv', 'MovimTwn'], ['Controle'], [
      DstMovID, DstNivel1, DstNivel2,
      DstGGX, MovimNiv, MovimTwn], [Controle], False);
    end;
  end;
}
begin
{
  PageControl1.ActivePageIndex := 0;
  //
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminSorcIndsWB)), // Antigo
    'AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
    '']);
    //
    PB1.Position := 0;
    PB1.Max := QrVSMovIts.RecordCount;
    QrVSMovIts.First;
    while not QrVSMovIts.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      InsereAtual();
      //
      QrVSMovIts.Next;
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + CO_UPD_TAB_VMI,
    'SET MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)),
    'WHERE MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestIndsWB)),
    'AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
    '']);
    //
    Geral.MB_Aviso(Geral.FF0(QrVSMovIts.RecordCount) + ' itens foram corrigidos!');
  finally
    Screen.Cursor := crDefault;
  end;
  fim tentativa de eliminar TmySQLQuery
}
end;

procedure TFmPrincipal.AdvGlowButton10Click(Sender: TObject);
begin
  CadastroFormulas(0, 0);
end;

procedure TFmPrincipal.AdvGlowButton110Click(Sender: TObject);
begin
(*
  UCriar.GerenciaTabelaLocal('NF1Fat', acCreate);
  UCriar.GerenciaTabelaLocal('NF1Pro', acCreate);
  UCriar.GerenciaTabelaLocal('Imprimir1', acCreate);
  if DBCheck.CriaFm(TFmNF1, FmNF1, afmoNegarComAviso) then
  begin
    FmNF1.ShowModal;
    FmNF1.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AdvGlowButton113Click(Sender: TObject);
const
  Nivel1 = 0;
  TabePr = 0;
  TipoLista = TTipoListaPreco.tpGraCusPrc;
  MostraSubForm = TSubForm.tsfNenhum;
begin
{
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
}
  Grade_Jan.MostraFormGraGruN(Nivel1, TabePr, TipoLista, MostraSubForm);
end;

procedure TFmPrincipal.AdvGlowButton114Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraCorPan, FmGraCorPan, afmoNegarComAviso) then
  begin
    FmGraCorPan.ShowModal;
    FmGraCorPan.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton115Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraCorCad, FmGraCorCad, afmoNegarComAviso) then
  begin
    FmGraCorCad.ShowModal;
    FmGraCorCad.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton116Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraCorFam, FmGraCorFam, afmoNegarComAviso) then
  begin
    FmGraCorFam.ShowModal;
    FmGraCorFam.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton117Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraTamCad, FmGraTamCad, afmoNegarComAviso) then
  begin
    FmGraTamCad.ShowModal;
    FmGraTamCad.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton118Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraCusPrc, FmGraCusPrc, afmoNegarComAviso) then
  begin
    FmGraCusPrc.ShowModal;
    FmGraCusPrc.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton119Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqCenCad, FmStqCenCad, afmoNegarComAviso) then
  begin
    FmStqCenCad.ShowModal;
    FmStqCenCad.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton11Click(Sender: TObject);
begin
  CadastroDeGrupos;
end;

procedure TFmPrincipal.AdvGlowButton120Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqBalCad, FmStqBalCad, afmoNegarComAviso) then
  begin
    FmStqBalCad.FMultiGrandeza := True;
    FmStqBalCad.ShowModal;
    FmStqBalCad.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton121Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmUFs, FmUFs, afmoNegarComAviso) then
  begin
    FmUFs.ShowModal;
    FmUFs.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton122Click(Sender: TObject);
begin
  {
  if DBCheck.CriaFm(TFmListServ, FmListServ, afmoNegarComAviso) then
  begin
    FmListServ.ShowModal;
    FmListServ.Destroy;
  end;
  }
end;

procedure TFmPrincipal.AdvGlowButton123Click(Sender: TObject);
begin
  {
  if DBCheck.CriaFm(TFmGraSrv1, FmGraSrv1, afmoNegarComAviso) then
  begin
    FmGraSrv1.ShowModal;
    FmGraSrv1.Destroy;
  end;
  }
end;

procedure TFmPrincipal.AdvGlowButton124Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabA();
end;

procedure TFmPrincipal.AdvGlowButton125Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAE21, FmCNAE21, afmoNegarComAviso) then
  begin
    FmCNAE21.ShowModal;
    FmCNAE21.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton126Click(Sender: TObject);
{begin
  UnNFe_PF.MostraFormStepsNFe_StatusServico();
end;}
const
  MostraForm = True;
var
  CodStatus: Integer;
  TxtStatus: String;
begin
  //try
    //UnNFe_PF.NFe_StatusServicoCod(False, CodStatus, TxtStatus);
    UnNFe_PF.NFe_StatusServicoCodMul(Self, CodStatus, TxtStatus);
    Geral.MB_Info(TxtStatus + ' (C�digo ' + Geral.FF0(CodStatus) + ')');
  //except
  //    XXe_PF.MostraMMC_SnapIn();
  //end;
end;

procedure TFmPrincipal.AdvGlowButton127Click(Sender: TObject);
begin
(*  DESABILITADO EM 2016-06-09 - Nova forma de devlver PQ pelo pqw!
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      Geral.MB_Aviso('A��o n�o implementada!' + sLineBreak + 'Solicite � DERMATEK!');
    fggxPQ:
}
      if DBCheck.CriaFm(TFmPQRCab, FmPQRCab, afmoNegarComAviso) then
      begin
        FmPQRCab.ShowModal;
        FmPQRCab.Destroy;
      end;
{
  end;
}
*)
end;

procedure TFmPrincipal.AdvGlowButton128Click(Sender: TObject);
begin
{ Desativado em 2014-10-17! Algu�m usa?
  if DBCheck.CriaFm(TFmNFeMPInn, FmNFeMPInn, afmoNegarComAviso) then
  begin
    FmNFeMPInn.ShowModal;
    FmNFeMPInn.Destroy;
  end;
}
end;

procedure TFmPrincipal.AdvGlowButton129Click(Sender: TObject);
{
var
  Controle: Integer;
  procedure InsereAtual();
  var
    DataHora, Observ, Marca: String;
    Codigo, (*Controle,*) MovimCod, MovimNiv, MovimTwn, Empresa, Terceiro, CliVenda,
    MovimID, LnkNivXtr1, LnkNivXtr2, Pallet, GraGruX, SrcMovID, SrcNivel1,
    SrcNivel2, SrcGGX, SerieFch, Ficha, Misturou, FornecMO, DstMovID, DstNivel1,
    DstNivel2, DstGGX, AptoUso, TpCalcAuto: Integer;
    Pecas, PesoKg, AreaM2, AreaP2, ValorT, SdoVrtPeca, SdoVrtPeso, SdoVrtArM2,
    CustoMOKg, CustoMOTot, ValorMP, QtdGerPeca, QtdGerPeso, QtdGerArM2,
    QtdGerArP2, QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, NotaMPAG: Double;
  begin
    Codigo         := QrVSMovItsCodigo     .Value;
    //Controle       := QrVSMovItsControle   .Value;
    MovimCod       := QrVSMovItsMovimCod   .Value;
    MovimNiv       := Integer(TEstqMovimNiv.eminSorcReclass);//QrVSMovItsMovimNiv   .Value;
    //MovimTwn       := QrVSMovItsMovimTwn   .Value;
    Empresa        := QrVSMovItsEmpresa    .Value;
    Terceiro       := QrVSMovItsTerceiro   .Value;
    CliVenda       := QrVSMovItsCliVenda   .Value;
    MovimID        := QrVSMovItsMovimID    .Value;
    LnkNivXtr1     := QrVSMovItsLnkNivXtr1 .Value;
    LnkNivXtr2     := QrVSMovItsLnkNivXtr2 .Value;
    DataHora       := Geral.FDT(QrVSMovItsDataHora.Value, 109);
    Pallet         := QrVSMovItsPallet     .Value;
    GraGruX        := QrVSMovItsGraGruX    .Value;
    Pecas          := -QrVSMovItsPecas      .Value;
    PesoKg         := -QrVSMovItsPesoKg     .Value;
    AreaM2         := -QrVSMovItsAreaM2     .Value;
    AreaP2         := -QrVSMovItsAreaP2     .Value;
    ValorT         := -QrVSMovItsValorT     .Value;
    SrcMovID       := 0;//QrVSMovItsSrcMovID   .Value;
    SrcNivel1      := 0;//QrVSMovItsSrcNivel1  .Value;
    //SrcNivel2      := ;//QrVSMovItsSrcNivel2  .Value;
    case QrVSMovItsControle.Value of
      3264, 3265, 3266, 3267, 3268, 3275, 3276: SrcNivel2 := 3231;
      3253, 3254, 3255, 3256, 3257: SrcNivel2 := 3240;
      3351, 3352, 3355: SrcNivel2 := 3250;
      3357, 3358, 3359: SrcNivel2 := 3256;
    end;
    SrcGGX         := 0;//QrVSMovItsSrcGGX     .Value;
    SdoVrtPeca     := QrVSMovItsSdoVrtPeca .Value;
    SdoVrtPeso     := QrVSMovItsSdoVrtPeso .Value;
    SdoVrtArM2     := QrVSMovItsSdoVrtArM2 .Value;
    Observ         := QrVSMovItsObserv     .Value;
    SerieFch       := QrVSMovItsSerieFch   .Value;
    Ficha          := QrVSMovItsFicha      .Value;
    Misturou       := QrVSMovItsMisturou   .Value;
    FornecMO       := QrVSMovItsFornecMO   .Value;
    CustoMOKg      := QrVSMovItsCustoMOKg  .Value;
    CustoMOTot     := QrVSMovItsCustoMOTot .Value;
    ValorMP        := QrVSMovItsValorMP    .Value;
    DstMovID       := QrVSMovItsDstMovID   .Value;
    DstNivel1      := QrVSMovItsDstNivel1  .Value;
    DstNivel2      := QrVSMovItsDstNivel2  .Value;
    DstGGX         := QrVSMovItsDstGGX     .Value;
    QtdGerPeca     := QrVSMovItsQtdGerPeca .Value;
    QtdGerPeso     := QrVSMovItsQtdGerPeso .Value;
    QtdGerArM2     := QrVSMovItsQtdGerArM2 .Value;
    QtdGerArP2     := QrVSMovItsQtdGerArP2 .Value;
    QtdAntPeca     := QrVSMovItsQtdAntPeca .Value;
    QtdAntPeso     := QrVSMovItsQtdAntPeso .Value;
    QtdAntArM2     := QrVSMovItsQtdAntArM2 .Value;
    QtdAntArP2     := QrVSMovItsQtdAntArP2 .Value;
    AptoUso        := QrVSMovItsAptoUso    .Value;
    NotaMPAG       := QrVSMovItsNotaMPAG   .Value;
    Marca          := QrVSMovItsMarca      .Value;
    TpCalcAuto     := QrVSMovItsTpCalcAuto .Value;

    //
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, stIns, 0);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, stIns, 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_INS_TAB_VMI, False, [
    'Codigo', 'MovimCod', 'MovimNiv',
    'MovimTwn', 'Empresa', 'Terceiro',
    'CliVenda', 'MovimID', 'LnkNivXtr1',
    'LnkNivXtr2', CO_DATA_HORA_VMI, 'Pallet',
    'GraGruX', 'Pecas', 'PesoKg',
    'AreaM2', 'AreaP2', 'ValorT',
    'SrcMovID', 'SrcNivel1', 'SrcNivel2',
    'SrcGGX', 'SdoVrtPeca', 'SdoVrtPeso',
    'SdoVrtArM2', 'Observ', 'SerieFch',
    'Ficha', 'Misturou', 'FornecMO',
    'CustoMOKg', 'CustoMOTot', 'ValorMP',
    'DstMovID', 'DstNivel1', 'DstNivel2',
    'DstGGX', 'QtdGerPeca', 'QtdGerPeso',
    'QtdGerArM2', 'QtdGerArP2', 'QtdAntPeca',
    'QtdAntPeso', 'QtdAntArM2', 'QtdAntArP2',
    'AptoUso', 'NotaMPAG', 'Marca',
    'TpCalcAuto'], [
    'Controle'], [
    Codigo, MovimCod, MovimNiv,
    MovimTwn, Empresa, Terceiro,
    CliVenda, MovimID, LnkNivXtr1,
    LnkNivXtr2, DataHora, Pallet,
    GraGruX, Pecas, PesoKg,
    AreaM2, AreaP2, ValorT,
    SrcMovID, SrcNivel1, SrcNivel2,
    SrcGGX, SdoVrtPeca, SdoVrtPeso,
    SdoVrtArM2, Observ, SerieFch,
    Ficha, Misturou, FornecMO,
    CustoMOKg, CustoMOTot, ValorMP,
    DstMovID, DstNivel1, DstNivel2,
    DstGGX, QtdGerPeca, QtdGerPeso,
    QtdGerArM2, QtdGerArP2, QtdAntPeca,
    QtdAntPeso, QtdAntArM2, QtdAntArP2,
    AptoUso, NotaMPAG, Marca,
    TpCalcAuto], [
    Controle], True) then
    begin
      Controle  := QrVSMovItsControle.Value;
      MovimNiv  := Integer(TEstqMovimNiv.eminBaixCurtiVS);
      DstMovID  := 0;
      DstNivel1 := 0;
      DstNivel2 := 0;
      DstGGX    := 0;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'DstMovID', 'DstNivel1', 'DstNivel2',
      'DstGGX', 'MovimNiv', 'MovimTwn'], ['Controle'], [
      DstMovID, DstNivel1, DstNivel2,
      DstGGX, MovimNiv, MovimTwn], [Controle], False);
    end;
  end;
}
var
  Itens: Integer;
begin
  PageControl1.ActivePageIndex := 0;
  //
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //EXIT;
(*
  VS_PF.AtualizaOrigensReclasseSemPallet(PB1, LaAviso1, LaAviso2);
  VS_PF.AtualizaOrigensReclassePalletErrado(PB1, LaAviso1, LaAviso2);
  VS_PF.AtualizaIMEIsPalletComGraGruXErrado(PB1, LaAviso1, LaAviso2);
*)
  Itens := 0;
  Itens := Itens + VS_PF.AtualizaReclassesSemPallet(PB1, LaAviso1, LaAviso2);
  Itens := Itens + VS_PF.AtualizaOrigensReclasseSemGragruX(PB1, LaAviso1, LaAviso2);
  Itens := Itens + VS_PF.AtualizaReclassesComPalletErrado(PB1, LaAviso1, LaAviso2);
  Itens := Itens + VS_PF.AtualizaArtigosDePallets(PB1, LaAviso1, LaAviso2);
  Itens := Itens + VS_PF.AtualizaMovimCodDeVSMovCab(PB1, LaAviso1, LaAviso2);
  Geral.MB_Info(Geral.FF0(Itens) + ' itens foram corrigidos!');
  (*
  Controle := 3195;
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
    'SELECT * ',
    'FROM v s m o v i t s ',
    'WHERE Controle IN ( ',
    //'3264, 3265, 3266, 3267, 3268, 3275, 3276, ',
    '3267, ',
    //'3253, 3254, 3255, 3256, 3257, ',
    '3256, '
    '3351, 3352, 3355, ',
    '3357, 3358, 3359  ',
    ') ',
    'ORDER BY Controle DESC ',
    '']);
    //
    PB1.Position := 0;
    PB1.Max := QrVSMovIts.RecordCount;
    QrVSMovIts.First;
    while not QrVSMovIts.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      InsereAtual();
      Controle := Controle - 1;
      //
      QrVSMovIts.Next;
    end;
    Geral.MB_Aviso(Geral.FF0(QrVSMovIts.RecordCount) + ' itens foram corrigidos!');
  finally
    Screen.Cursor := crDefault;
  end;
*)
end;

procedure TFmPrincipal.AdvGlowButton12Click(Sender: TObject);
begin
  CadastroDeSubgrupos;
end;

procedure TFmPrincipal.AGBSintegraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSintegra, AGBSintegra);
end;

procedure TFmPrincipal.AGBSPEDEFDEnceClick(Sender: TObject);
begin
  EfdIcmsIpi_Jan.MostraFormSPEDEFDEnce();
end;

procedure TFmPrincipal.AdvGlowButton130Click(Sender: TObject);
(*

SELECT * FROM v s m o v i t s
WHERE Movimcod=4

SELECT vmi.*
FROM v s m o v i t s vmi
LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX
WHERE Pallet=0
AND ggx.GraGruY=3072

*)
var
  Qry1, Qry2: TmySQLQuery;
  GraGruX, SrcGGX, SrcMovID, SrcMovNiv, SrcNivel1, SrcNivel2, MovimCod,
  Controle: Integer;
  Texto: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  Texto := '';
  Qry1 := TmySQLQuery.Create(Dmod);
  Qry2 := TmySQLQuery.Create(Dmod);
  try
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI,
    'WHERE Movimcod IN (3,4) ',
    'AND MovimNiv=1 ',
    'AND ',
    ' (SrcNivel1>SrcNivel2',
    '  OR',
    '  SrcGGx=6',
    ')',
    '']);
    Qry1.First;
    while not Qry1.Eof do
    begin
      GraGruX     := Qry1.FieldByName('MovimCod').AsInteger;
      SrcGGX      := Qry1.FieldByName('MovimCod').AsInteger;
      SrcMovID    := Qry1.FieldByName('SrcGGX').AsInteger;
      SrcMovNiv   := Qry1.FieldByName('SrcMovID').AsInteger;
      //SrcNivel1   := Qry1.FieldByName('').AsInteger;
      SrcNivel2   := Qry1.FieldByName('SrcNivel1').AsInteger;
      MovimCod    := Qry1.FieldByName('SrcNivel2').AsInteger;
      Controle    := Qry1.FieldByName('Controle').AsInteger;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
      'SELECT Codigo FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(SrcNivel2),
      '']);
      SrcNivel1   := Qry2.FieldByName('Codigo').AsInteger;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'MovimCod', 'GraGruX',
      'SrcMovID', 'SrcNivel1', 'SrcNivel2',
      'SrcGGX'], [
      'Controle'], [
      MovimCod, GraGruX,
      SrcMovID, SrcNivel1, SrcNivel2,
      SrcGGX], [
      Controle], True);
      //
      Texto := Texto + Geral.FF0(Controle) + ', ';
      Qry1.Next;
    end;
    Geral.MB_Info('IMEIS mudados: ' + Texto);
  finally
    Qry2.Free;
  end;
  finally
    Qry1.Free;
  end;
end;

procedure TFmPrincipal.AdvGlowButton131Click(Sender: TObject);
begin
  DmNFe_0000.AtualizaXML_No_BD_Tudo(True);
end;

procedure TFmPrincipal.AdvGlowButton132Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraImpLista, FmGraImpLista, afmoNegarComAviso) then
  begin
    FmGraImpLista.ShowModal;
    FmGraImpLista.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton133Click(Sender: TObject);
begin
  Dmod.AtualizaStqMovIts_PQX(PB1, LaAviso1, LaAviso2);
end;

procedure TFmPrincipal.AdvGlowButton135Click(Sender: TObject);
begin
  Dmod.AtualizaStqMovIts_MPI(PB1);
end;

procedure TFmPrincipal.AdvGlowButton136Click(Sender: TObject);
begin
  Dmod.AtualizaStqMovIts_CWB();
end;

procedure TFmPrincipal.AdvGlowButton137Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMCMPP, AdvGlowButton137);
end;

procedure TFmPrincipal.AdvGlowButton138Click(Sender: TObject);
begin
  AppPF.CorrigeReduzidosDuplicadosDeInsumo(True);
end;

procedure TFmPrincipal.GBMPReclasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMPReclas, GBMPReclas);
end;

procedure TFmPrincipal.GBMPSubProdClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPSubProd, FmMPSubProd, afmoNegarComAviso) then
  begin
    FmMPSubProd.ShowModal;
    FmMPSubProd.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton139Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPClasMulGer, FmMPClasMulGer, afmoNegarComAviso) then
  begin
    FmMPClasMulGer.ShowModal;
    FmMPClasMulGer.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton13Click(Sender: TObject);
begin
  CadastroDeContas;
end;

procedure TFmPrincipal.AdvGlowButton140Click(Sender: TObject);
var
  Nivel1, Nivel2, PrdGrupTip: Integer;
  Nome: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE gragru1');
    Dmod.QrUpd.SQL.Add('SET PrdGrupTip=-2');
    Dmod.QrUpd.SQL.Add('WHERE Nivel1 IN (');
    Dmod.QrUpd.SQL.Add('  SELECT Codigo FROM PQ');
    Dmod.QrUpd.SQL.Add(')');
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE GraGru1');
    Dmod.QrUpd.SQL.Add('SET GraTamCad=0');
    Dmod.QrUpd.SQL.Add('WHERE PrdGrupTip=-2');
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrPQ_GG1.Close;
    UnDmkDAC_PF.AbreQuery(Dmod.QrPQ_GG1, Dmod.MyDB);
    //
    while not Dmod.QrPQ_GG1.Eof do
    begin
      Nivel2     := - Dmod.QrPQ_GG1Ativo.Value;
      Nome       := Dmod.QrPQ_GG1NO_PQ.Value;
      PrdGrupTip := -2; // PQ
      Nivel1     := Dmod.QrPQ_GG1Nivel1.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
      'Nivel2', 'Nome', 'PrdGrupTip'], ['Nivel1'], [
      Nivel2, Nome, PrdGrupTip], [Nivel1], True);
      //
      Dmod.QrPQ_GG1.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.AdvGlowButton142Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqInnCad, FmStqInnCad, afmoNegarComAviso) then
  begin
    FmStqInnCad.ShowModal;
    FmStqInnCad.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton143Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton144Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o desabilitada. Avise a DERMATEK');
  //DModG.AtualizaPrecosGraGruVal2_All(PB1, Empresa?);
end;

procedure TFmPrincipal.AdvGlowButton146Click(Sender: TObject);
begin

{    ini tentativa de eliminar TmySQLQuery
  AtualizaBalancoDePQNoSMI(PB1);
}
end;

procedure TFmPrincipal.AdvGlowButton147Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE cmptinn SET ');
  DMod.QrUpd.SQL.Add('GraGruX=-201, TipoNF=1');
  DMod.QrUpd.SQL.Add('WHERE GraGruX=0;');
  DMod.QrUpd.ExecSQL;
  //
  DMod.QrAux.Close;
  DMod.QrAux.SQL.Clear;
  DMod.QrAux.SQL.Add('SELECT Codigo');
  DMod.QrAux.SQL.Add('FROM cmptout');
  DMod.QrAux.SQL.Add('WHERE JaAtz=0');
  UnDmkDAC_PF.AbreQuery(DMod.QrAux, Dmod.MyDB);
  PB1.Max := Dmod.QrAux.RecordCount;
  //
  DMod.QrAux.Close;
  DMod.QrAux.SQL.Clear;
  DMod.QrAux.SQL.Add('SELECT Codigo');
  DMod.QrAux.SQL.Add('FROM cmptinn');
  DMod.QrAux.SQL.Add('WHERE JaAtz=0');
  UnDmkDAC_PF.AbreQuery(DMod.QrAux, Dmod.MyDB);
  PB1.Max := PB1.Max + Dmod.QrAux.RecordCount;
  while not DMod.QrAux.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Dmod.AtualizaEstoqueCMPT(Dmod.QrAux.FieldByName('Codigo').AsInteger, 0);
    Dmod.QrAux.Next;
  end;
  //
  DMod.QrAux.Close;
  DMod.QrAux.SQL.Clear;
  DMod.QrAux.SQL.Add('SELECT Codigo');
  DMod.QrAux.SQL.Add('FROM cmptout');
  DMod.QrAux.SQL.Add('WHERE JaAtz=0');
  UnDmkDAC_PF.AbreQuery(DMod.QrAux, Dmod.MyDB);
  PB1.Max := PB1.Max + Dmod.QrAux.RecordCount;
  while not DMod.QrAux.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Dmod.AtualizaEstoqueCMPT(0, Dmod.QrAux.FieldByName('Codigo').AsInteger);
    Dmod.QrAux.Next;
  end;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.AdvGlowButton148Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeExportaXML();
end;

procedure TFmPrincipal.AdvGlowButton149Click(Sender: TObject);
begin
  MostraPediPrzCab(0);
end;


procedure TFmPrincipal.AdvGlowButton14Click(Sender: TObject);
begin
  ImpressaoDoPlanoDeContas;
end;

procedure TFmPrincipal.AdvGlowButton150Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmProsoft1, FmProsoft1, afmoNegarComAviso) then
  begin
    FmProsoft1.ShowModal;
    FmProsoft1.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton151Click(Sender: TObject);
begin
  Application.CreateForm(TFmLeDataArq, FmLeDataArq);
  FmLeDataArq.ShowModal;
  FmLeDataArq.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton152Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLoadXML, FmLoadXML, afmoLiberado) then
  begin
    FmLoadXML.ShowModal;
    FmLoadXML.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton154Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa smia, nfecaba nfea');
  Dmod.QrUpd.SQL.Add('SET smia.Baixa=0');
  Dmod.QrUpd.SQL.Add('WHERE nfea.FatID=smia.Tipo');
  Dmod.QrUpd.SQL.Add('AND nfea.FatNum=smia.OriCodi');
  Dmod.QrUpd.SQL.Add('AND nfea.Empresa=smia.Empresa');
  Dmod.QrUpd.SQL.Add('AND (nfea.ide_tpAmb=2');
  Dmod.QrUpd.SQL.Add('OR nfea.Status=101)');
{
  Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Baixa=0');
  Dmod.QrUpd.SQL.Add('WHERE IDCtrl IN');
  Dmod.QrUpd.SQL.Add('(');
  Dmod.QrUpd.SQL.Add('SELECT smia.IDCtrl');
  Dmod.QrUpd.SQL.Add('FROM stqmovitsa smia, nfecaba nfea');
  Dmod.QrUpd.SQL.Add('WHERE nfea.FatID=smia.Tipo');
  Dmod.QrUpd.SQL.Add('AND nfea.FatNum=smia.OriCodi');
  Dmod.QrUpd.SQL.Add('AND nfea.Empresa=smia.Empresa');
  Dmod.QrUpd.SQL.Add('AND (nfea.ide_tpAmb=2');
  Dmod.QrUpd.SQL.Add('OR nfea.Status=101)');
  Dmod.QrUpd.SQL.Add(')');
}
  Dmod.QrUpd.ExecSQL;
  //
  Geral.MB_Info('Atualiza��o realizada com sucesso!');
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.AdvGlowButton155Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFMoDocFis, FmNFMoDocFis, afmoNegarComAviso) then
  begin
    FmNFMoDocFis.ShowModal;
    FmNFMoDocFis.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton156Click(Sender: TObject);
begin
  MyObjects.CriaForm_AcessoTotal(TFmPQCorrigeFatID, FmPQCorrigeFatID);
  FmPQCorrigeFatID.ShowModal;
  FmPQCorrigeFatID.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton157Click(Sender: TObject);
begin
  Application.CreateForm(TFmLctAjustesB, FmLctAjustesB);
  FmLctAjustesB.ShowModal;
  FmLctAjustesB.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton158Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  Screen.Cursor := crHourGlass;
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE lct0001a');
  DMod.QrUpd.SQL.Add('SET FatID=FatID+1000');
  DMod.QrUpd.SQL.Add('WHERE FatID in (1,2,3,4,5,6)');
  DMod.QrUpd.SQL.Add('AND Debito > 0');
  DMod.QrUpd.ExecSQL;
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE lct0001a');
  DMod.QrUpd.SQL.Add('SET FatID=FatID+1000');
  DMod.QrUpd.SQL.Add('WHERE FatID in (7,8,13,14)');
  DMod.QrUpd.SQL.Add('AND Credito > 0');
  DMod.QrUpd.ExecSQL;
  Screen.Cursor := crDefault;
  //
  DmProd.CorrigeFatID_Venda(PB1, LaAviso1, LaAviso2);
  //
  Geral.MB_Info('Atualiza��o finalizada!');
end;

procedure TFmPrincipal.AdvGlowButton159Click(Sender: TObject);
begin
  // bt lcts. sem cli int aba ferramentas - correcoes
  if DModG.SelecionaEmpresa(sllNenhum, True) then
    DmodFin.ExisteLancamentoSemCliInt(VAR_TAB_LCT_SEL);
end;

procedure TFmPrincipal.BtTemaClick(Sender: TObject);
begin
(*
{$IfDef cSkinRank}
  FmLinkRankSkin.Show;
{$Else}
  //  TODO AlphaSkin
{$EndIf}
*)
  if PMStyles.Items.Count < 2 then
    MyObjects.StylesListRefreshMenu(PMStyles);
  //
  MyObjects.MostraPopUpDeBotao(PMStyles, BtTema);
end;

procedure TFmPrincipal.AdvGlowButton160Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLoad_Inn();
end;

procedure TFmPrincipal.AdvGlowButton161Click(Sender: TObject);
begin
  ConsumoJan.MostraLeiGer();
end;

procedure TFmPrincipal.AdvGlowButton162Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Aviso: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT Codigo ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=27 ',
    'AND QtdAntPeca > -Pecas ',
    'AND QtdAntPeca>0 ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Aviso := 'Erros foram encontrados nos seguintes IDs:' + sLineBreak;
      Qry.First;
      while not Qry.Eof do
      begin
        Aviso := Aviso + Geral.FF0(Qry.FieldByName('Codigo').AsInteger) + sLineBreak;
        //
        Qry.Next;
      end;
    end else
      Aviso := 'N�o foram encontrados erros! ';
    //
    Geral.MB_Info(Aviso);
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal.AdvGlowButton163Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPMOCab(0);
end;

procedure TFmPrincipal.AdvGlowButton164Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvGlowButton165Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton166Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AdvGlowButton167Click(Sender: TObject);
begin
  dmkWeb.AbrirAppAcessoRemoto;
end;

procedure TFmPrincipal.AdvGlowButton168Click(Sender: TObject);
begin
  Application.CreateForm(TFmTesteThread, FmTesteThread);
  FmTesteThread.ShowModal;
  FmTesteThread.Destroy;
end;

procedure TFmPrincipal.AGBAtzArtigosPalletsClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Deseja realmente atualizar os artigos de pallets?' +
    sLineBreak +
    'ATEN��O: Este procedimento poder� demorar v�rios minutos!') = ID_YES
  then
    VS_PF.AtualizaArtigosDePallets(PB1, LaAviso1, LaAviso2);
end;

procedure TFmPrincipal.AdvGlowButton16Click(Sender: TObject);
begin
  CadastroDeCambio();
end;

procedure TFmPrincipal.AdvGlowButton17Click(Sender: TObject);
begin
  CadastroPQ(0);
end;

procedure TFmPrincipal.AdvGlowButton18Click(Sender: TObject);
begin
  CadastroGruposQuimicos();
end;

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMResultados, AdvGlowButton19);
end;

procedure TFmPrincipal.AGBCorrigeFatIDClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  DmProd.CorrigeFatID_Venda(PB1, LaAviso1, LaAviso2);
end;

procedure TFmPrincipal.AGBCorrigeNFesIMIsClick(Sender: TObject);
var
  Qry: TmySQLQuery;
  //
  procedure AtualizaTabela(MovimID: TEstqMovimID; Origens, Destinos: array of TEstqMovimNiv);
  var
    TabCab, FldCabSer, FldCabNum, Erros: String;
    NFeSer1, NFeNum1, NFeSer2, NFeNum2, VSMulNFeCab, Codigo, MovimCod: Integer;

  begin
    TabCab := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
    VS_PF.ObtemNomeCamposNFeVSXxxCab(MovimID, FldCabSer, FldCabNum);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.Codigo, cab.MovimCod, ',
    'cab.' + FldCabSer + ' Ser1, cab.' + FldCabNum + ' Num1,',
    'nfe.ide_serie Ser2, nfe.ide_nNF Num2  ',
    'FROM ' + TabCab + ' cab ',
    'LEFT JOIN vsoutnfecab nfe ON nfe.MovimCod=cab.MovimCod ',
    ' AND nfe.NFeStatus=100  ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Erros := '';
    while not Qry.Eof do
    begin
      NFeSer1   := Qry.FieldByName('Ser1').AsInteger;
      NFeNum1   := Qry.FieldByName('Num1').AsInteger;
      Codigo   := Qry.FieldByName('Codigo').AsInteger;
      MovimCod := Qry.FieldByName('MovimCod').AsInteger;
      //
      if NFeNum1 = 0 then
      begin
        NFeSer2 := Qry.FieldByName('Ser2').AsInteger;
        NFeNum2:= Qry.FieldByName('Num2').AsInteger;
        //
        if NFeNum2 <> 0 then
        begin
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabCab, False, [
          FldCabSer, FldCabNum], [
          'Codigo'], [
          NFeSer2, NFeNum2], [
          Codigo], True) then ;
        end;
      end;
      if (NFeNum1 = 0) and (NFeNum2 = 0) then
        Erros := Erros + Geral.FF0(Qry.FieldByName('Codigo').AsInteger) + ' | '
      else
        DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
          MovimCod, MovimID, Origens, Destinos);
      //
      Qry.Next;
    end;
    if Trim(Erros)<> '' then
      Geral.MB_Aviso(
      'Os c�digos abaixo da tabela ' + TabCab + ' n�o tem NFe definida!' +
      sLineBreak + Erros);
    Erros := '';
  end;
begin
  Qry := tMySQLQuery.Create(Dmod);
  try
    // 1 - Entrada in natura
    AtualizaTabela(TEstqMovimID.emidCompra, [(*s� por info de NFe*)], [eminSemNiv]);
    // 2 - Saida por venda
    AtualizaTabela(TEstqMovimID.emidVenda, [(*s� por info de NFe*)], [eminSemNiv]);
    //
    // 16 - Compra WB
    AtualizaTabela(TEstqMovimID.emidEntradaPlC, [(*s� por info de NFe*)], [eminSemNiv]);
    //
    // 21 - Devolucao definitiva
    AtualizaTabela(TEstqMovimID.emidDevolucao, [(*s� por info de NFe*)], [eminSemNiv]);
    // 22 - Devolucao para retrabalho
    AtualizaTabela(TEstqMovimID.emidRetrabalho, [(*s� por info de NFe*)], [eminSemNiv]);
    // 25 - Transferencia
    AtualizaTabela(TEstqMovimID.emidTransfLoc, [eminSorcLocal], [eminDestLocal]);
    //
////////////////////////////////////////////////////////////////////////////////
    // F A S E   2
////////////////////////////////////////////////////////////////////////////////
    // 26 - Caleiro - Falta Fazer !!!!!!!!!!
    //AtualizaTabela(TEstqMovimID.emidEmProCal, [emin?], [emin?]);
    // 27 - Curtimento - Falta Fazer !!!!!!!!!!
    //AtualizaTabela(TEstqMovimID.emidEmProCur, [emin?], [emin?]);
    // 11 - Operacao - Falta Fazer !!!!!!!!!!
    AtualizaTabela(TEstqMovimID.emidEmOperacao, [eminSorcOper],
    [eminEmOperInn, eminDestOper, eminEmOperBxa]);
     //19 - Recurtimento - Falta Fazer !!!!!!!!!!
    AtualizaTabela(TEstqMovimID.emidEmProcWE, [eminSorcWEnd],
    [eminEmWEndInn, eminDestWEnd, eminEmWEndBxa]);
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal.AGBApurICMSIPIClick(Sender: TObject);
begin
  //EfdIcmsIpi_Jan.MostraFormEFD_E001();
  EfdIcmsIpi_Jan.MostraFormEfdIcmsIpiE001();
end;

procedure TFmPrincipal.AGBConfEstqClick(Sender: TObject);
begin
  if DmodG.QrDonoCNPJ_CPF.Value = '02717861000110' then //Apenas o SoftCouro at� finalizar o Sped depois remover
  begin
    if DBCheck.CriaFm(TFmSPEDEstqGer, FmSPEDEstqGer, afmoNegarComAviso) then
    begin
      FmSPEDEstqGer.FImporExpor := 2;
      FmSPEDEstqGer.FExpImpTXT := FormatFloat('0', FmSPEDEstqGer.FImporExpor);
      FmSPEDEstqGer.ShowModal;
      FmSPEDEstqGer.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.AGBConsLeitClick(Sender: TObject);
begin
  ConsumoGerlJan.MostraCons(0);
end;

procedure TFmPrincipal.AGBConsultaNFeClick(Sender: TObject);
begin
  //ConsumoJan.MostraLeiGer();
  UnNFe_PF.MostraFormNFeConsulta();
end;

procedure TFmPrincipal.AGBContingenciaClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCntngnc();
end;

procedure TFmPrincipal.AGBEntraMPClick(Sender: TObject);
begin
  Grade_Jan.CriaFormEntradaCab(tnfMateriaPrima, True, 0);
end;

procedure TFmPrincipal.AGBEntraUCClick(Sender: TObject);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
    begin
      MyObjects.MostraPopUpDeBotao(PMEntraUC, AGBEntraUC);
    end;
    fggxPQ:
    begin
}
  PQ_PF.MostraFormPQE(0);
{
    end;
  end;
}
end;

procedure TFmPrincipal.AGBImportaNFeWebClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLoad_Web();
end;

procedure TFmPrincipal.AGBImprimeVSEmProcBHClick(Sender: TObject);
begin
  // ini 2023-12-31
  //VS_PF.ImprimeVSEmProcBH();
  VS_Jan.MostraFormVSImpEmProcBH();
  // fim 2023-12-31
end;

procedure TFmPrincipal.AGBInfoIncCabClick(Sender: TObject);
begin
  Eiia_PF.MostraFormInfoIncCab(0);
end;

procedure TFmPrincipal.AdvGlowButton1Click(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal.AdvGlowButton20Click(Sender: TObject);
begin
  MostraFormWBOutIts(stIns, 0, nil, nil);
end;

procedure TFmPrincipal.AdvGlowButton21Click(Sender: TObject);
begin
  MostraFormBalancoPQ();
end;

procedure TFmPrincipal.AdvGlowButton22Click(Sender: TObject);
begin
  CadastrodeEspessuras();
end;

procedure TFmPrincipal.AdvGlowButton23Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPClas, FmMPClas, afmoNegarComAviso) then
  begin
    FmMPClas.ShowModal;
    FmMPClas.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton24Click(Sender: TObject);
begin
  CadastroDeArtigosGrupos();
end;

procedure TFmPrincipal.AdvGlowButton25Click(Sender: TObject);
begin
  CadastroBancos();
end;

procedure TFmPrincipal.AdvGlowButton26Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPGrup, FmMPGrup, afmoNegarComAviso) then
  begin
    FmMPGrup.ShowModal;
    FmMPGrup.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton27Click(Sender: TObject);
begin
  CadastroDeMPEstagios();
end;

procedure TFmPrincipal.AdvGlowButton28Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMVendas, AdvGlowButton28);
end;

procedure TFmPrincipal.AdvGlowButton29Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMPedidos, AdvGlowButton29);
end;

procedure TFmPrincipal.AdvGlowButton2Click(Sender: TObject);
begin
  App_Jan.MostraFormEmitGru(0);
end;

procedure TFmPrincipal.AdvGlowButton30Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCashBal, FmCashBal, afmoNegarComAviso) then
  begin
    FmCashBal.ShowModal;
    FmCashBal.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton31Click(Sender: TObject);
begin
  CadastroMatriz();
end;

procedure TFmPrincipal.AdvGlowButton32Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMGeral, AdvGlowButton32);
end;

procedure TFmPrincipal.AdvGlowButton33Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton35Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeJust();
end;

procedure TFmPrincipal.AGBLaySPEDEFDClick(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Ser�o baixadas e atualizadas as tabelas do layout do SPED EFD.' +
  'Tem certeza que deseja continuar?') =
  ID_YES then
  begin
    Memo3.Visible := True;
    Memo3.Lines.Clear;
    EfdIcmsIpi_PF.BaixaLayoutSPED(Memo3);
    Memo3.Visible := False;
  end;
end;

procedure TFmPrincipal.AGBMPRecebImpClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPRecebImp, FmMPRecebImp, afmoNegarComAviso) then
  begin
    FmMPRecebImp.ShowModal;
    FmMPRecebImp.Destroy;
  end;
end;

procedure TFmPrincipal.AGBNewFinMigraClick(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Deseja alterar todos "' + VAR_LCT + '" de "CliInt" > 0 para "CliInt" = -11 ?') =
  ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE ' + VAR_LCT + ' SET CliInt=-11 ',
      'WHERE CliInt > 0 ',
      'AND Controle <> 0 ',
      '']);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  DmLct2.MigraLctsParaTabLct();
end;

procedure TFmPrincipal.AGBNFeConsCadClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormStepsNFe_ConsultaCadastroEntidade('', '', True);
end;

procedure TFmPrincipal.AGBNFeDesDowCClick(Sender: TObject);

begin
  UnNFe_PF.MostraFormNFeCnfDowC_0100();
end;

procedure TFmPrincipal.AGBNFeDestClick(Sender: TObject);
begin
  // Deprecado e nunca usado!
  //UnNFe_PF.MostraFormNFeDesConC_0101();
  // Substituto
  UnNFe_PF.MostraFormNFeDistDFeInt();
end;

procedure TFmPrincipal.AGBNFeEventosClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeEveRLoE(0);
end;

procedure TFmPrincipal.AGBNFeExportaXML_BClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeExportaXML_B();
end;

procedure TFmPrincipal.AGBNfeNewVerClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(APMMigrarVersaoNFe, AGBNfeNewVer);
end;

procedure TFmPrincipal.AGBOCsClick(Sender: TObject);
begin
  VS_PF.MostraFormVSOCGerCab(0);
end;

procedure TFmPrincipal.AGBOpcoesGradClick(Sender: TObject);
begin
  Grade_Jan.MostraFormOpcoesGrad();
end;

procedure TFmPrincipal.AGBPararClick(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmPrincipal.AGBPQRAjuInnClick(Sender: TObject);
begin
(*  DESABILITADO EM 2016-06-09 - Nova forma de devlver PQ pelo pqw!
  if DBCheck.CriaFm(TFmPQRAjuInn, FmPQRAjuInn, afmoNegarComAviso) then
  begin
    FmPQRAjuInn.ShowModal;
    FmPQRAjuInn.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBPQRAjuOutClick(Sender: TObject);
begin
(*  DESABILITADO EM 2016-06-09 - Nova forma de devlver PQ pelo pqw!
  if DBCheck.CriaFm(TFmPQRAjuOut, FmPQRAjuOut, afmoNegarComAviso) then
  begin
    FmPQRAjuOut.ShowModal;
    FmPQRAjuOut.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBTabsSPEDEFDClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPED_EFD_DownTabs, FmSPED_EFD_DownTabs, afmoNegarComAviso) then
  begin
    FmSPED_EFD_DownTabs.ShowModal;
    FmSPED_EFD_DownTabs.Destroy;
  end;
end;

procedure TFmPrincipal.AGBTribDefCabClick(Sender: TObject);
begin
  Tributos_PF.MostraFormTribDefCab(0);
end;

procedure TFmPrincipal.AGBTribDefCadClick(Sender: TObject);
begin
  Tributos_PF.MostraFormTribDefCad(0);
end;

procedure TFmPrincipal.AGBVSArtCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSArtCab(0, 0);
end;

procedure TFmPrincipal.AGBVSBoxesClick(Sender: TObject);
begin
  VS_PF.MostraFormVSPaCRIts();
end;

procedure TFmPrincipal.AGBVSCalCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSCalCab(0);
end;

procedure TFmPrincipal.AGBVSCfgEFDClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(APMVSCfgEFD, AGBVSCfgEFD);
end;

procedure TFmPrincipal.AGBVSCfgEqzClick(Sender: TObject);
begin
  VS_PF.MostraFormVSCfgEqzCb(0);
end;

procedure TFmPrincipal.AGBVSCGICabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSCGICab(0);
end;

procedure TFmPrincipal.AGBVSClaArtClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(APMVSClaArt, AGBVSClaArt);
end;

procedure TFmPrincipal.AGBVSCOPCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSCOPCab(0);
end;

procedure TFmPrincipal.AGBVSCPMRSBCbClick(Sender: TObject);
begin
  VS_PF.MostraFormVSCPMRSBCb();
end;

procedure TFmPrincipal.AGBVSCurCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSCurCab(0);
end;

procedure TFmPrincipal.AGBVSDsnCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSDsnCab(0);
end;

procedure TFmPrincipal.AGBVSDvlRtbClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(APMVSDvlRtb, AGBVSDvlRtb);
end;

procedure TFmPrincipal.AGBVSEntiMPClick(Sender: TObject);
begin
  VS_PF.MostraFormVSEntiMP(0);
end;

procedure TFmPrincipal.AGBVSEqzCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSEqzCab(0);
end;

procedure TFmPrincipal.AGBVSExBCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSExBCab(0, 0);
end;

procedure TFmPrincipal.AGBVSFchRslCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSImpClaMulRMP();
end;

procedure TFmPrincipal.AGBVSFichasClick(Sender: TObject);
const
  SerieFicha = 0;
  Ficha      = 0;
begin
  VS_PF.MostraFormVSFchGerCab(SerieFicha, Ficha);
end;

procedure TFmPrincipal.AGBVSFinClaClick(Sender: TObject);
begin
  VS_PF.MostraFormVSFinCla(0, False, nil);
end;

procedure TFmPrincipal.AGBVSGeraArtCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSGeraArt(0, 0);
end;

procedure TFmPrincipal.AGBVSInnCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSInnCab(0, 0, 0);
end;

procedure TFmPrincipal.AGBVSMOEnvXXXClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(APMVSMOEnvXXX, AGBVSMOEnvXXX);
end;

procedure TFmPrincipal.AGBVSMotivBxaClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMotivBxa();
end;

procedure TFmPrincipal.AGBVSMovCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMovCab(0);
end;

procedure TFmPrincipal.AGBVSMovimIDClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMovimID(TEstqMovimID.emidAjuste);
end;

procedure TFmPrincipal.AGBVSMovImpClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(APMVSImprime, AGBVSMovImp);
end;

procedure TFmPrincipal.AGBVSMovItbClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMovItbAdd();
end;

procedure TFmPrincipal.AGBVSMovItsClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMovIts(0);
end;

procedure TFmPrincipal.AGBVSMrtCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMrtCad();
end;

procedure TFmPrincipal.AGBVSNatCadClick(Sender: TObject);
begin
  VS_PF.MostraFormVSNatCad(0, False, nil);
end;

procedure TFmPrincipal.AGBVSRibOpeClick(Sender: TObject);
begin
  VS_PF.MostraFormVSRibOpe(0, False);
end;

procedure TFmPrincipal.AGBVSRRMCabClick(Sender: TObject);
begin
(*
  Quando reabilitar aqui, criar novo GGY? e configurar ?
  TUnAppPF.VerificaGraGruYsNaoConfig();
  //
*)
  VS_PF.MostraFormVSRRMCab(0);
end;

procedure TFmPrincipal.AGBVSOutCabClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSOutCab(0, 0);
end;

procedure TFmPrincipal.AGBVSOutItsClick(Sender: TObject);
begin
  VS_PF.MostraFormVSBxaCab(0, 0);
end;

procedure TFmPrincipal.AGBVSPalletClick(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(0);
end;

procedure TFmPrincipal.AGBVSPalStaClick(Sender: TObject);
begin
  VS_PF.MostraFormVSPalSta();
end;

procedure TFmPrincipal.AGBVSPedCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSPedCab(0);
end;

procedure TFmPrincipal.AGBVSPlCCabClick(Sender: TObject);
const
  Codigo   = 0;
  Controle = 0;
begin
  VS_PF.MostraFormVSPlCCab(Codigo, Controle);
end;

procedure TFmPrincipal.AGBVSPSPCabClick(Sender: TObject);
begin
//testar!
  VS_PF.MostraFormVSPSPCab(0);
end;

procedure TFmPrincipal.AGBVSPwdDdClick(Sender: TObject);
(*
var
  Qry: TmySQLQuery;
  VSPwdDdClas: String;
  VSPwdDdData: TDateTime;
  VSPwdDdUser: Integer;
begin
  Qry := TmySQLQuery.Create;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT VSPwdDdClas, VSPwdDdData, VSPwdDdUser ',
    'FROM controle ',
    'WHERE Codigo=1 ',
    '']);
    VSPwdDdClas := Qry.FieldByName('VSPwdDdClas').AsString;
    VSPwdDdData := Qry.FieldByName('VSPwdDdData').AsDateTime;
    VSPwdDdUser := Qry.FieldByName('VSPwdDdUser').AsInteger;
    if (VSPwdDdUser = VAR_USUARIO) or (VAR_USUARIO < 0) then
*)
begin
      VS_PF.MostraFormVSPwdDd(False);
(*
    else
      Geral.MB_Aviso('Acesso restrito pelo login!');
  finally
    Qry.Free;
  end;
*)
end;

procedure TFmPrincipal.AGBVSPWECabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSPWECab(0);
end;

procedure TFmPrincipal.AGBVSRclArtClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(APMVSRclArt, AGBVSRclArt);
end;

procedure TFmPrincipal.AGBVSReqMovClick(Sender: TObject);
var
  Janela: Integer;
begin
  Janela := MyObjects.SelRadioGroup('Tipo de requisi��o',
    'Escolha o tipo de requisi��o', [
    (*00*)'RME - Requisi��o de Movimenta��o de Estoque',
    (*01*)'RDC - Requisi��o de Divis�o de Couro',
    (*02*)'Ficha de estoque - Invent�rio',
    (*03*)'IEC - Informa��o de Entrada de Couros',
    (*04*)'ICR - Informa��o de Classe/Reclasse de Couros',
    (*05*)'ISC - Informa��o de Sa�da de Couros',
    (*06*)'LSP - Lista Sequencial de Pallets',
    (*07*)'IPM - Informa��o de Pallets Movimentados'
    ], -1, 1);
  case Janela of
    0: VS_PF.MostraFormVSReqMov(0);
    1: VS_PF.MostraFormVSReqDiv(0);
    2: VS_PF.ImprimeFichaEstoque(-11, Dmod.QrMasterEm.Value, '');
    3: VS_PF.MostraFormVSInfInn(0);
    4: VS_PF.MostraFormVSInfMov(0);
    5: VS_PF.MostraFormVSInfOut(0);
    6: VS_PF.MostraFormVSLstPal(0);
    7: VS_PF.MostraFormVSInfPal(0);
  end;
end;

procedure TFmPrincipal.AGBVSRibCadClick(Sender: TObject);
begin
  VS_PF.MostraFormVSRibCad(0, False);
end;

procedure TFmPrincipal.AGBVSRibClaClick(Sender: TObject);
begin
  VS_PF.MostraFormVSRibCla(0, False, nil);
end;

procedure TFmPrincipal.AGBVSSerFchClick(Sender: TObject);
begin
  VS_PF.MostraFormVSSerFch();
end;

procedure TFmPrincipal.AGBVSSubPrdClick(Sender: TObject);
begin
  VS_PF.MostraFormVSSubPrd(0, False);
end;

procedure TFmPrincipal.AGBVSTrfLocCabClick(Sender: TObject);
begin
(*
  tirar da tabela vsmovits e colocar em outra pr�pria, alterando apenas o local
  de estoque do IME-I selecionado!
  VS_PF.MostraFormVSTrfLocCab(0);
*)
end;

procedure TFmPrincipal.AGBVSWetEndClick(Sender: TObject);
begin
  VS_PF.MostraFormVSWetEnd(0, False, nil);
end;

procedure TFmPrincipal.AGBWBAjsCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSAjsCab(0, 0);
end;

procedure TFmPrincipal.AGBWBArtCabClick(Sender: TObject);
var
  Qry: TmySQLQuery;
  Ds: TDataSource;
  I, GraGruX, Codigo, Controle: Integer;
  SQLType: TSQLType;
  //
  procedure IncluiCab();
  var
    Fluxo, ReceiRecu, ReceiRefu, ReceiAcab: Integer;
    Nome, TxtMPs, Observa: String;
  begin
    SQLType        := stIns;
    Codigo         := 0;
    Nome           := Qry.FieldByName('NO_PRD_TAM_COR').AsString + ' (?)';
    Fluxo          := Qry.FieldByName('Fluxo').AsInteger;
    ReceiRecu      := Qry.FieldByName('ReceiRecu').AsInteger;
    ReceiRefu      := Qry.FieldByName('ReceiRefu').AsInteger;
    ReceiAcab      := Qry.FieldByName('ReceiAcab').AsInteger;
    TxtMPs         := Qry.FieldByName('TxtMPs').AsString;
    Observa        := Qry.FieldByName('Observa').AsString;
    //
    Codigo := UMyMod.BPGS1I32('vsartcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsartcab', False, [
    'Nome',
    'Fluxo', 'ReceiRecu', 'ReceiRefu',
    'ReceiAcab', 'TxtMPs', 'Observa'], [
    'Codigo'], [
    Nome,
    Fluxo, ReceiRecu, ReceiRefu,
    ReceiAcab, TxtMPs, Observa], [
    Codigo], True) then
  end;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, ',
    'vag.Controle, wac.*  ',
    'FROM wbartcab wac  ',
    'LEFT JOIN vsartggx vag ON wac.GraGruX=vag.GraGruX ',
    '',
    'LEFT JOIN gragrux ggx ON ggx.Controle=wac.GraGruX',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    '',
    'WHERE vag.Controle IS NULL ',
    'ORDER BY vag.DataCad, vag.GraGruX ',
    '']);
    //
    Geral.MB_Aviso('Cadastro deprecado! Use o cadastro da guia "Ribeira 1"!');
    if Qry.RecordCount > 0 then
    begin
      Ds := TDataSource.Create(Dmod);
      Ds.DataSet := Qry;
      try
        if DBCheck.EscolheCodigoUniGrid('Aviso',
        'Importa��o de configura��o de Artigo',
        'Selecione os itens que deseja cadastrar no sistema VS!', Ds, True, False) then
        begin
          if FmGerlShowGrid.DBGSel.SelectedRows.Count > 0 then
          begin
            with FmGerlShowGrid.DBGSel.DataSource.DataSet do
            for I := 0 to FmGerlShowGrid.DBGSel.SelectedRows.Count-1 do
            begin
              //GotoBookmark(pointer(FmGerlShowGrid.DBGSel.SelectedRows.Items[I]));
              GotoBookmark(FmGerlShowGrid.DBGSel.SelectedRows.Items[I]);
              //
              IncluiCab();
              GraGruX := Qry.FieldByName('GraGruX').AsInteger;
              Controle := UMyMod.BPGS1I32('vsartggx', 'Controle', '', '', tsPos, SQLType, 0);
              //if
              UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsartggx', False, [
              'Codigo', 'GraGruX'], [
              'Controle'], [
              Codigo, GraGruX], [
              Controle], True);
            end;
          end;
          FmGerlShowGrid.Destroy;
        end;
      finally
        Ds.Free;
      end;
    end;
  finally
    Qry.Free;
  end;
  //////////////
(*
  if DBCheck.CriaFm(TFmWBArtCab, FmWBArtCab, afmoNegarComAviso) then
  begin
    FmWBArtCab.ShowModal;
    FmWBArtCab.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBWBInnCabClick(Sender: TObject);
begin
  MostraFormWBInnCab(0, 0, 0);
end;

procedure TFmPrincipal.AGBWBMovImpClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmWBMovImp, FmWBMovImp, afmoNegarComAviso) then
  begin
    FmWBMovImp.ShowModal;
    FmWBMovImp.Destroy;
  end;
end;

procedure TFmPrincipal.AGBWBMPrCabClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmWBMPrCab, FmWBMPrCab, afmoNegarComAviso) then
  begin
    FmWBMPrCab.ShowModal;
    FmWBMPrCab.Destroy;
  end;
end;

procedure TFmPrincipal.AGBWBOutCabClick(Sender: TObject);
begin
  MostraFormWBOutCab(0, 0);
end;

procedure TFmPrincipal.AGBWBPalletsClick(Sender: TObject);
begin
  MostraFormWBPallet();
end;

procedure TFmPrincipal.AGBWBRclCabClick(Sender: TObject);
begin
  //MostraFormWBRclCab(0, 0, 0);
  VS_PF.MostraFormVSRclCab(0, 0, 0);
end;

procedure TFmPrincipal.GBBaixaETEClick(Sender: TObject);
begin
  UnPQx.GerenciaBaixaETE(0);
end;

procedure TFmPrincipal.GBBaixaOutrosClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMBaixaPQ, GBBaixaOutros);
end;

procedure TFmPrincipal.GBGerenciaPesagemClick(Sender: TObject);
begin
  UnPQx.GerenciaPesagem(0);
end;

procedure TFmPrincipal.AGBReceDespClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmReceDesp, FmReceDesp, afmoNegarComAviso) then
  begin
    FmReceDesp.ShowModal;
    FmReceDesp.Destroy;
  end;
end;

procedure TFmPrincipal.AGBRelatUCClick(Sender: TObject);
begin
  CadastroPQImp('');
end;

procedure TFmPrincipal.AdvGlowButton37Click(Sender: TObject);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      Geral.MB_Aviso('A��o n�o implementada!' + sLineBreak + 'Solicite � DERMATEK!');
    fggxPQ:
}
      if DBCheck.CriaFm(TFmPQPed, FmPQPed, afmoNegarComAviso) then
      begin
        FmPQPed.ShowModal;
        FmPQPed.Destroy;
      end;
{
  end;
}
end;

procedure TFmPrincipal.AdvGlowButton38Click(Sender: TObject);
begin
  CadastroMPIn();
end;

procedure TFmPrincipal.AdvGlowButton39Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMDefeitos, AdvGlowButton39);
end;

procedure TFmPrincipal.AdvGlowButton3Click(Sender: TObject);
begin
  CadastroPortSerBal();
end;

procedure TFmPrincipal.AdvGlowButton40Click(Sender: TObject);
begin
  CadastroPallets();
end;

procedure TFmPrincipal.AdvGlowButton41Click(Sender: TObject);
begin
  MostraParamsEmp();
end;

procedure TFmPrincipal.AdvGlowButton42Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraOptEnt, FmGraOptEnt, afmoNegarComAviso) then
  begin
    FmGraOptEnt.ShowModal;
    FmGraOptEnt.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton43Click(Sender: TObject);
begin
  App_Jan.CadastroOpcoesBlueDerm();
end;

procedure TFmPrincipal.AdvGlowButton44Click(Sender: TObject);
begin
  //CadastroOpcoes;
  Entities.MostraFormOpcoes();
end;

procedure TFmPrincipal.AdvGlowButton45Click(Sender: TObject);
begin
  UnPQx.MostraFormPQMCab(0);
end;

procedure TFmPrincipal.AdvGlowButton46Click(Sender: TObject);
begin
  MostraBackup;
end;

procedure TFmPrincipal.AdvGlowButton47Click(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMVerificaDB, AdvGlowButton47);
end;

procedure TFmPrincipal.AdvGlowButton48Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMResultados, AdvGlowButton48);
end;

procedure TFmPrincipal.AdvGlowButton49Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmServicoManager, FmServicoManager, afmoNegarComAviso) then
  begin
    FmServicoManager.ShowModal;
    FmServicoManager.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton4Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMFluxos, AdvGlowButton4);
end;

procedure TFmPrincipal.AdvGlowButton50Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMCMPT, AdvGlowButton50);
end;

procedure TFmPrincipal.AdvGlowButton51Click(Sender: TObject);
begin
{$IfDef cDbTable}
  if DBCheck.CriaFm(TFmIBGE_DTB, FmIBGE_DTB, afmoSoAdmin) then
  begin
    FmIBGE_DTB.ShowModal;
    FmIBGE_DTB.Destroy;
  end;
{$EndIf}
end;

procedure TFmPrincipal.AdvGlowButton52Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormLayoutNFe();
end;

procedure TFmPrincipal.AdvGlowButton54Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMImagem, AdvGlowButton54);
end;

procedure TFmPrincipal.AdvGlowButton55Click(Sender: TObject);
var
  FatNum, Empresa, IDCtrl: Integer;
  SQL: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  //Atualiza XML DB com �spas triplas
  try
    Screen.Cursor := crHourGlass;
    //
    SQL := '';
    //
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT FatNum, Empresa, IDCtrl, XML_NFe');
    DModG.QrAux.SQL.Add('FROM nfearq');
    DModG.QrAux.SQL.Add('WHERE XML_NFe LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_NFe LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_NFe LIKE ''%'+ '""' + '%''');
    UnDmkDAC_PF.AbreQuery(DModG.QrAux, Dmod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      DModG.QrAux.First;
      while not DModG.QrAux.Eof do
      begin
        FatNum  := DModG.QrAux.FieldByName('FatNum').AsInteger;
        Empresa := DModG.QrAux.FieldByName('Empresa').AsInteger;
        IDCtrl  := DModG.QrAux.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, [
          'XML_NFe'
        ], ['FatNum', 'Empresa', 'IDCtrl'],
        [
          Geral.WideStringToSQLString(DModG.QrAux.FieldByName('XML_NFe').AsWideString)
        ], [FatNum, Empresa, IDCtrl], True) then
        begin
        end;
        DModG.QrAux.Next;
      end;
    end;
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT FatNum, Empresa, IDCtrl, XML_Aut');
    DModG.QrAux.SQL.Add('FROM nfearq');
    DModG.QrAux.SQL.Add('WHERE XML_Aut LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_Aut LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_Aut LIKE ''%'+ '""' + '%''');
    UnDmkDAC_PF.AbreQuery(DModG.QrAux, Dmod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      DModG.QrAux.First;
      while not DModG.QrAux.Eof do
      begin      
        FatNum  := DModG.QrAux.FieldByName('FatNum').AsInteger;
        Empresa := DModG.QrAux.FieldByName('Empresa').AsInteger;
        IDCtrl  := DModG.QrAux.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, [
          'XML_Aut'
        ], ['FatNum', 'Empresa', 'IDCtrl'],
        [
          Geral.WideStringToSQLString(DModG.QrAux.FieldByName('XML_Aut').AsWideString)
        ], [FatNum, Empresa, IDCtrl], True) then
        begin
        end;
        DModG.QrAux.Next;
      end;
    end;
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT FatNum, Empresa, IDCtrl, XML_Can');
    DModG.QrAux.SQL.Add('FROM nfearq');
    DModG.QrAux.SQL.Add('WHERE XML_Can LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_Can LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_Can LIKE ''%'+ '""' + '%''');
    UnDmkDAC_PF.AbreQuery(DModG.QrAux, Dmod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      DModG.QrAux.First;
      while not DModG.QrAux.Eof do
      begin
        FatNum  := DModG.QrAux.FieldByName('FatNum').AsInteger;
        Empresa := DModG.QrAux.FieldByName('Empresa').AsInteger;
        IDCtrl  := DModG.QrAux.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, [
          'XML_Can'
        ], ['FatNum', 'Empresa', 'IDCtrl'],
        [
          Geral.WideStringToSQLString(DModG.QrAux.FieldByName('XML_Can').AsWideString)
        ], [FatNum, Empresa, IDCtrl], True) then
        begin
        end;
        DModG.QrAux.Next;
      end;
    end;
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT FatNum, Empresa, IDCtrl, XML_Den');
    DModG.QrAux.SQL.Add('FROM nfearq');
    DModG.QrAux.SQL.Add('WHERE XML_Den LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_Den LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_Den LIKE ''%'+ '""' + '%''');
    UnDmkDAC_PF.AbreQuery(DModG.QrAux, Dmod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      DModG.QrAux.First;
      while not DModG.QrAux.Eof do
      begin
        FatNum  := DModG.QrAux.FieldByName('FatNum').AsInteger;
        Empresa := DModG.QrAux.FieldByName('Empresa').AsInteger;
        IDCtrl  := DModG.QrAux.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, [
          'XML_Den'
        ], ['FatNum', 'Empresa', 'IDCtrl'],
        [
          Geral.WideStringToSQLString(DModG.QrAux.FieldByName('XML_Den').AsWideString)
        ], [FatNum, Empresa, IDCtrl], True) then
        begin
        end;
        DModG.QrAux.Next;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
    Geral.MB_Info('Atualiza��o finalizada!');
  end;
end;

procedure TFmPrincipal.AdvGlowButton56Click(Sender: TObject);
begin
  MostraPediAcc;
end;

procedure TFmPrincipal.AdvGlowButton57Click(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
      'UPDATE gragru1 ',
      'SET prod_indEscala = "S" ',
      'WHERE prod_indEscala = "1" ',
      '']);
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
      'UPDATE gragru1 ',
      'SET prod_indEscala = "N" ',
      'WHERE prod_indEscala = "0" ',
      '']);
  finally
    Qry.Free;
  end;
  Geral.MB_Aviso('Atualizado com sucesso!');
end;

procedure TFmPrincipal.AdvGlowButton58Click(Sender: TObject);
var
  DmLctX: TDataModule;
begin
  if UnDmkDAC_PF.CriaDataModule(FmPrincipal, TDmLct2, DmLctX) then
    TDmLct2(DmLctX).GerenciaEmpresa(PageControl1, AdvToolBarPagerNovo);
    //AdvToolBarPager2);
end;

procedure TFmPrincipal.AdvGlowButton59Click(Sender: TObject);
const
  Cliente = 0;
  Fornecedor = 0;
var
  DmLctX: TDataModule;
begin
  if UnDmkDAC_PF.CriaDataModule(FmPrincipal, TDmLct2, DmLctX) then
    TDmLct2(DmLctX).GerenciaEmpresa2(PageControl1, AdvToolBarPagerNovo,
    //AdvToolBarPager2,
    Cliente, Fornecedor);
end;

procedure TFmPrincipal.AdvGlowButton5Click(Sender: TObject);
begin
  CadastroEquipes();
end;

procedure TFmPrincipal.AdvGlowButton60Click(Sender: TObject);
begin
  MostraCambioMda;
end;

procedure TFmPrincipal.AGBCadDiversosClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(APMVSCadDiversos, AGBCadDiversos);
end;

procedure TFmPrincipal.AGBCarteirasClick(Sender: TObject);
begin
  CadastroDeCarteira;
end;

procedure TFmPrincipal.AGBCartNiv2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCartNiv2, FmCartNiv2, afmoNegarComAviso) then
  begin
    FmCartNiv2.ShowModal;
    FmCartNiv2.Destroy;
  end;
end;

procedure TFmPrincipal.AGBClaReCoClick(Sender: TObject);
begin
  //VS_Jan.MostraFormVSLoadCRCCab1(0, 0);
  VS_Jan.MostraFormVSLoadCRCCab2(0, 0);
end;

procedure TFmPrincipal.AGBVSOpeCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSOpeCab(0);
end;

procedure TFmPrincipal.AdvGlowButton62Click(Sender: TObject);
begin
  AtzSdoContas;
end;

procedure TFmPrincipal.AdvGlowButton63Click(Sender: TObject);
begin
  SaldoDeContas;
end;

procedure TFmPrincipal.AdvGlowButton66Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqManCad, FmStqManCad, afmoNegarComAviso) then
  begin
    FmStqManCad.ShowModal;
    FmStqManCad.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton67Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTesteChart, FmTesteChart, afmoLiberado) then
  begin
    FmTesteChart.ShowModal;
    FmTesteChart.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFatParcelaNfeCabYClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  DmNFe_0000.CorrigeFatParcelaNFeCabY(PB1, LaAviso1, LaAviso2);
end;

procedure TFmPrincipal.AGBFinEncerMesClick(Sender: TObject);
begin
  FinanceiroJan.MostraLctEncerraMes();
end;

procedure TFmPrincipal.Gfg(Sender: TObject);
const
  Cliente       = 0;
  CU_PediVda    = 0;
  ForcaCriarXML = False;
var
  EMP_FILIAL: Integer;
begin
  EMP_FILIAL := DmodG.QrFiliLogFilial.Value;
  //
  UnNFe_PF.MostraFormFatPedNFs(EMP_FILIAL, Cliente, CU_PediVda, ForcaCriarXML);
end;

procedure TFmPrincipal.AGBGraGruEGerClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruEGerCab();
end;

procedure TFmPrincipal.AGBGraGruGruXCouClick(Sender: TObject);
var
  Val: Variant;
  GraGruX: Integer;
begin
  Val := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, GraGruX, 0, 0,
  '', '', True, 'Reduzido', 'Informe o Reduzido: ', 0, Val) then
  begin
    GraGruX := Val;
    VS_Jan.MostraFormGraGruXCou(GraGruX);
  end;
end;

procedure TFmPrincipal.AGBGraGruYClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruY(0, 0, '');
end;

procedure TFmPrincipal.AdvGlowButton68Click(Sender: TObject);
begin
  GFat_Jan.MostraFormFatDivGer();
end;

procedure TFmPrincipal.AdvGlowButton69Click(Sender: TObject);
begin
  GFat_Jan.MostraFormFatPedCab();
end;

procedure TFmPrincipal.AdvGlowButton6Click(Sender: TObject);
begin
  CadastroDePlano();
end;

procedure TFmPrincipal.AdvGlowButton70Click(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmSugVPedCab, FmSugVPedCab, afmoNegarComAviso) then
  begin
    FmSugVPedCab.ShowModal;
    FmSugVPedCab.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AdvGlowButton72Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  // N�o precisa todas, Mudar?
  DmPediVda.ReopenTabelasPedido();
  Screen.Cursor := crDefault;
  //
(*
  if DBCheck.CriaFm(TFmPediVdaImp, FmPediVdaImp, afmoNegarComAviso) then
  begin
    FmPediVdaImp.ShowModal;
    FmPediVdaImp.Destroy;
  end;
*)
  if DBCheck.CriaFm(TFmPediVdaImp2, FmPediVdaImp2, afmoNegarComAviso) then
  begin
    FmPediVdaImp2.ShowModal;
    FmPediVdaImp2.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton73Click(Sender: TObject);
begin
  MostraPediPrzCab(0);
end;

procedure TFmPrincipal.BtPediVda1Click(Sender: TObject);
begin
  GFat_Jan.MostraFormPediVda();
end;

procedure TFmPrincipal.AdvGlowButton75Click(Sender: TObject);
begin
  MostraGeosite;
end;

procedure TFmPrincipal.AdvGlowButton76Click(Sender: TObject);
begin
  MostraRegioes();
end;

procedure TFmPrincipal.AdvGlowButton77Click(Sender: TObject);
begin
  MostraTabePrcCab;
end;

procedure TFmPrincipal.AdvGlowButton78Click(Sender: TObject);
begin
  MostraFisRegCad;
end;

procedure TFmPrincipal.AdvGlowButton79Click(Sender: TObject);
begin
  MostraCFOP2003();
end;

procedure TFmPrincipal.AdvGlowButton80Click(Sender: TObject);
begin
  if Sender is TMenuItem then
    UnNFe_PF.MostraFormNFePesq(False, nil, nil, 0)
  else
  begin
    UnNFe_PF.MostraFormNFePesq(True, PageControl1, AdvToolBarPagerNovo,
    //AdvToolBarPager2,
    0);
    AdvToolBarPagerNovo.Visible := False;
  end;
end;

procedure TFmPrincipal.AdvGlowButton81Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeInut();
end;

procedure TFmPrincipal.AdvGlowButton82Click(Sender: TObject);
begin
  Dmod.MyDB.Execute('DELETE FROM couniv1');
  Geral.MB_Aviso('Registros CouNiv1 exclu�dos!' + sLineBreak +
  'Verifique o BD para recriar os registros CouNiv1!');
end;

procedure TFmPrincipal.AdvGlowButton83Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormStepsNFe_StepGenerico();
end;

procedure TFmPrincipal.AdvGlowButton84Click(Sender: TObject);
begin
  //DModG.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
  if DBCheck.CriaFm(TFmNFaEditCSOSNAlq, FmNFaEditCSOSNAlq, afmoNegarComAviso) then
  begin
    FmNFaEditCSOSNAlq.QrParamsEmp.Locate('Codigo', DModG.QrParamsEmpCodigo.Value, []);
    FmNFaEditCSOSNAlq.ShowModal;
    FmNFaEditCSOSNAlq.Destroy;
  end;
end;

procedure TFmPrincipal.GBValidaXMLClick(Sender: TObject);
begin
  UnNFe_PF.ValidaXML_NFe('');
end;

procedure TFmPrincipal.AdvGlowButton85Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLEnc(0);
end;

procedure TFmPrincipal.AdvGlowButton86Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNCMs, FmNCMs, afmoNegarComAviso) then
  begin
    FmNCMs.ShowModal;
    FmNCMs.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton87Click(Sender: TObject);
begin
  MostraModelosImp;
end;

procedure TFmPrincipal.AdvGlowButton88Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmImportaFoxPro, FmImportaFoxPro, afmoSoMaster) then
  begin
    FmImportaFoxPro.ShowModal;
    FmImportaFoxPro.Destroy;
  end;
}
end;

procedure TFmPrincipal.GBAbreXMLClick(Sender: TObject);
begin
  if MyObjects.CriaForm_AcessoTotal(TFmTesteXML, FmTesteXML) then
  begin
    FmTesteXML.ShowModal;
    FmTesteXML.Destroy;
  end;
end;

procedure TFmPrincipal.GBClasFiscClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmClasFisc, FmClasFisc, afmoNegarComAviso) then
  begin
    FmClasFisc.ShowModal;
    FmClasFisc.Destroy;
  end;
end;

procedure TFmPrincipal.GBConflitosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraImpConflitos, FmGraImpConflitos, afmoNegarComAviso) then
  begin
    FmGraImpConflitos.ShowModal;
    FmGraImpConflitos.Destroy;
  end;
end;

procedure TFmPrincipal.GBFabricasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFabricas, FmFabricas, afmoNegarComAviso) then
  begin
    FmFabricas.ShowModal;
    FmFabricas.Destroy;
  end;
end;

procedure TFmPrincipal.GBImportaNFesClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLoad_Dir();
end;

procedure TFmPrincipal.GBImportaXMLClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLoad_Arq();
end;

procedure TFmPrincipal.GBInfCplClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeInfCpl();
end;

procedure TFmPrincipal.GBNatOperClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNatOper, FmNatOper, afmoNegarComAviso) then
  begin
    FmNatOper.ShowModal;
    FmNatOper.Destroy;
  end;
end;

procedure TFmPrincipal.GBReduzidoClick(Sender: TObject);
var
  GraGruX: Variant;
  //Cod: Integer;
begin
  GraGruX := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, GraGruX, 0, 0,
  '', '', True, 'Reduzido', 'Informe o Reduzido: ', 0, GraGruX) then
  begin
    if DBCheck.CriaFm(TFmGraGruReduzido, FmGraGruReduzido, afmoSoAdmin) then
    begin
      FmGraGruReduzido.FGraGruX := GraGruX;
      FmGraGruReduzido.ReopenGraGruX();
      FmGraGruReduzido.ShowModal;
      FmGraGruReduzido.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.GBTipoMovClick(Sender: TObject);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  if DBCheck.CriaFm(TFmAtzIdeal, FmAtzIdeal, afmoSoMaster) then
  begin
    FmAtzIdeal.ShowModal;
    FmAtzIdeal.Destroy;
  end;
}
end;

procedure TFmPrincipal.GBUsoConsCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntrada, TBitBtn(Sender));
end;

procedure TFmPrincipal.AdvGlowButton89Click(Sender: TObject);
begin
  UMedi_PF.MostraUnidMed(0);
end;

procedure TFmPrincipal.AdvGlowButton8Click(Sender: TObject);
begin
  CadastroDeNiveisPlano();
end;

procedure TFmPrincipal.AGBEntidadesClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal.AdvGlowButton90Click(Sender: TObject);
const
  Esperado = '696bfa2de10ce17eaee3ea8123639867c82b8a0c';
var
  ChaveNFe, idCSRT, Crypt, Explaned, Crypted: String;
  I, J, N: Integer;
  X, Parte: WideString;
  teste: AnsiChar;
  IsOK: Boolean;
  CSRT: String;
begin
  CSRT := Dmod.MyDB.SelectString(Geral.ATS([
  'SELECT AES_DECRYPT(infRespTec_CSRT, ',
  '"' + CO_CRYPT_CSRT + '") CSRT ',
  'FROM paramsemp ',
  'WHERE Codigo=-11 ',
  '']), IsOK);

{
G8063VRTNDMO886SFNK5LDUDEI24XJ22YIPO41180678393592000146558900000006041028190697
Chave de Acesso: 41180678393592000146558900000006041028190697
CSRT: G8063VRTNDMO886SFNK5LDUDEI24XJ22YIPO
idCSRT: 01
}
  ChaveNFe := '41180678393592000146558900000006041028190697';
  UnNFe_PF.HashCSRT(-11, ChaveNFe);
  //CSRT     := 'G8063VRTNDMO886SFNK5LDUDEI24XJ22YIPO';
  idCSRT   := '01';
  //
  Crypted  := '';
  // 696bfa2de10ce17eaee3ea8123639867c82b8a0c  MySQL
  // 696bfa2de10ce17eaee3ea8123639867c82b8a0c
  if UnNFe_PF.CryptSHA1(CSRT + ChaveNFE, Crypted) then
  begin
    if Esperado = Crypted then
      Explaned := 'Valor coicide!'
    else
      Explaned := 'Valor N�O coicide!';
    //
    Geral.MB_Info(
    'ChaveNFe = ' + ChaveNFe + sLineBreak +
    'CSRT = ' + CSRT + sLineBreak +
    'idCSRT = ' + idCSRT + sLineBreak +
    'Esperado = ' + Esperado + sLineBreak +
    'Resultado = ' + Crypted + sLineBreak +
     Explaned);
     //
    N := Length(Crypted) div 2;
    X := '';
    for I := 0 to N - 1 do
    begin
      J := (I * 2) + 1;
      Parte := Crypted[J] +  Crypted[J + 1];
      //X := X + Char(dmkPF.HexToInt(Parte));
      X := X + Char(StrToInt64('$' + Parte));
    end;
    Geral.MB_Info(X);
(*
    X := Dmod.MyDB.SelectString(
    'SELECT TO_BASE64("' + X + '") Base64', IsOK, 'Base64');
    if IsOK then
      Geral.MB_Info(X)
    else
      Geral.MB_Info('ERRO na SQL!');
*)
    X := dmkPF.EncodeBase64(X);
    Geral.MB_Info('hashCSRT:  ' + X)
  end;
end;

procedure TFmPrincipal.AdvGlowButton91Click(Sender: TObject);
begin
  CadastroDeCambio();
end;

procedure TFmPrincipal.AdvGlowButton93Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmPromissoria, FmPromissoria, afmoNegarComAviso) then
  begin
    FmPromissoria.ShowModal;
    FmPromissoria.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton95Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmDuplicata1, FmDuplicata1, afmoNegarComAviso) then
  begin
    FmDuplicata1.ShowModal;
    FmDuplicata1.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton96Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmPontos, FmPontos, afmoNegarComAviso) then
  begin
    FmPontos.ShowModal;
    FmPontos.Destroy;
  end;
}
end;

procedure TFmPrincipal.AdvGlowButton9Click(Sender: TObject);
begin
  CadastroDeConjuntos();
end;

procedure TFmPrincipal.AdvGlowMenuButton1Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMImp, AdvGlowMenuButton1);
end;

procedure TFmPrincipal.AdvGlowMenuButton4Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(APMCorrigeIMEIs, AdvGlowMenuButton4);
end;

procedure TFmPrincipal.AdvGlowMenuButton5Click(Sender: TObject);
(*
var
  Qry1: TmySQLQuery;
  //
  DataIni: String;
  Controle, GraGruX, GraGru1, GraCusPrc, Entidade, Ano: Integer;
  CustoPreco: Double;
  SQLType: TSQLType;
*)
begin
(*
  SQLType := stIns;
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT * FROM gragrux ',
    'WHERE GraGruY > 0 ',
    '']);
    Qry1.First;
    while not Qry1.Eof do
    begin
      for GraCusPrc := 1 to 8 do
      begin
        for Ano := 0 to 6 do
        begin
          GraGruX        := Qry1.FieldByName('Controle').AsInteger;
          GraGru1        := Qry1.FieldByName('GraGru1').AsInteger;
          //CustoPreco     := GraGruX / 1000 * GraCusPrc * Ano;
          CustoPreco     := (GraGruX / 100  * Ano / GraCusPrc) + 30;
          Entidade       := -11;
          DataIni        := Geral.FDT(EncodeDate(2010 + Ano, 3, 15), 1);

          //
          Controle := UMyMod.BPGS1I32('gragruval', 'Controle', '', '', tsPos, stIns, 0);
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
          'GraGruX', 'GraGru1', 'GraCusPrc',
          'CustoPreco', 'Entidade', 'DataIni'], [
          'Controle'], [
          GraGruX, GraGru1, GraCusPrc,
          CustoPreco, Entidade, DataIni], [
          Controle], True);
       end;
       //
     end;
     Qry1.Next;
  end;
  finally
    Qry1.Free;
  end;
*)
end;

procedure TFmPrincipal.AdvGlowMenuButton6Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(APMVSInfXxx, AdvGlowMenuButton6);
end;

(*
procedure TFmPrincipal.AdvPreviewMenu1Buttons0Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  Close;
end;

procedure TFmPrincipal.AdvPreviewMenu1Buttons1Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  MostraBackup;
end;

procedure TFmPrincipal.AdvPreviewMenu1Buttons2Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvPreviewMenu1Buttons3Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  VerificaBD();
end;
*)

procedure TFmPrincipal.AdvToolBarButton7Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AdvToolBarPagerNovoChange(Sender: TObject);
begin
  {$IfDef cAdvToolx} //Tokyo
  if AdvToolBarPager2.ActivePage.Name = 'APRibeira' then
  {$Else}
  if TTabSheet(AdvToolBarPagerNovo.ActivePage).Name = 'APRibeira' then
  {$EndIf}
    AppPF.VerificaCadastroXxArtigoIncompleta();
end;

procedure TFmPrincipal.AGB_SPED_ComparaClick(Sender: TObject);
begin
  EfdIcmsIpi_Jan_v03_0_9.MostraFormSPED_EFD_Compara(22,0);
end;

procedure TFmPrincipal.AGB_SPED_EFD_ExpClick(Sender: TObject);
begin
  EfdIcmsIpi_Jan_v03_0_2_a.MostraFormEfdIcmsIpiExporta();
end;

procedure TFmPrincipal.AGB_SPED_EFD_ImpClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPED_EFD_Importa, FmSPED_EFD_Importa, afmoNegarComAviso) then
  begin
    FmSPED_EFD_Importa.ShowModal;
    FmSPED_EFD_Importa.Destroy;
  end;
end;

procedure TFmPrincipal.AGMBSkinClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMImp, AGMBSkin);
end;

procedure TFmPrincipal.AjustaEstiloImagem(Index: integer);
{
var
 Indice: Integer;
}
begin
{
  if Index >= 0 then Indice := Index else
  Indice := Geral.ReadAppKey('Estio_ImgPrincipal', Application.Title,
    ktInteger, 1, HKEY_LOCAL_MACHINE);
  case Indice of
    00: ImgPrincipal.Style := sbAutosize;
    01: ImgPrincipal.Style := sbCenter;
    02: ImgPrincipal.Style := sbKeepAspRatio;
    03: ImgPrincipal.Style := sbKeepHeight;
    04: ImgPrincipal.Style := sbKeepWidth;
    05: ImgPrincipal.Style := sbNone;
    06: ImgPrincipal.Style := sbStretch;
    07: ImgPrincipal.Style := sbTile;
    else ImgPrincipal.Style := sbCenter;
  end;
  ImgPrincipal.Tag       := Indice;
  Automtico1.Checked     := Indice = 00;
  Centralizado1.Checked  := Indice = 01;
  ManterProporo1.Checked := Indice = 02;
  ManterAltura1.Checked  := Indice = 03;
  ManterLargura1.Checked := Indice = 04;
  Nenhum1.Checked        := Indice = 05;
  Espichar1.Checked      := Indice = 06;
  Repetido1.Checked      := Indice = 07;
  //
  ImgPrincipal.Invalidate;
}
end;

procedure TFmPrincipal.Antigo1Click(Sender: TObject);
begin
  PQ_PF.MostraFormPQE(0);
end;

procedure TFmPrincipal.Antigo2Click(Sender: TObject);
begin
  //VS_PF.MostraFormVSRclArtPrpOld(stIns);
end;

procedure TFmPrincipal.AntigoPQ1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPQImp, FmPQImp, afmoNegarComAviso) then
  begin
    FmPQImp.ShowModal;
    FmPQImp.Destroy;
  end;
end;

procedure TFmPrincipal.AppIdle(Sender: TObject; var Done: Boolean);
begin
  Done := True;
  //
  if DModG <> nil then
    DmodG.ExecutaPing(FmBlueDerm_Dmk, [Dmod.MyDB, DModG.MyPID_DB, DModG.AllID_DB]);
end;

procedure TFmPrincipal.Aproduzirnodiaporsetor1Click(Sender: TObject);
begin
  App_Jan.MostraFormFluxPcpImp();
end;

procedure TFmPrincipal.Artigosgeradoscommodity1Click(Sender: TObject);
begin
  VS_Jan.MostraFormArtGeComodty(0);
end;

procedure TFmPrincipal.AtzSdoContas;
begin
  // Ver se e quando precisar
(*
  if DBCheck.CriaFm(TFmContasHistAtz2, FmContasHistAtz2, afmoNegarComAviso) then
  begin
    FmContasHistAtz2.FEntiCod := 0;
    FmContasHistAtz2.FGenero  := 0;
    FmContasHistAtz2.FPeriodo := -23999;
    //
    FmContasHistAtz2.ShowModal;
    FmContasHistAtz2.Destroy;
  end;
 *)
end;

procedure TFmPrincipal.Automtico1Click(Sender: TObject);
begin
  Geral.WriteAppKey('Estio_ImgPrincipal', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  AjustaEstiloImagem(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.SBCadEntidadesClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal.SbEntidadeClick(Sender: TObject);
begin
  Entities.CadastroDeEntidade(0, fmCadSelecionar, fmCadSelecionar, False);
end;

procedure TFmPrincipal.SbFavoritosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFavoritosG, FmFavoritosG, afmoLiberado) then
  begin
    FmFavoritosG.ShowModal;
    FmFavoritosG.Destroy;
    //
    PageControl1.ActivePageIndex := 0;
    //
  {$IfDef cAdvToolx} //Tokyo
    DModG.CriaFavoritos(AdvToolBarPager2, LaAviso2, LaAviso1, AGBEntidades, FmPrincipal);
  {$Else}
    DModG.CriaFavoritos(AdvToolBarPagerNovo, LaAviso2, LaAviso1, BtEntidades, FmPrincipal);
  {$EndIf}
  end;
end;

procedure TFmPrincipal.SbFinancasClick(Sender: TObject);
begin
  AdvToolBarPagerNovo.Visible := False;
  FinanceiroJan.MostraFinancas(PageControl1, AdvToolBarPagerNovo);
end;

procedure TFmPrincipal.SbLastWork1Click(Sender: TObject);
begin
  App_Jan.MostraFormProgramacaoEntregas();
end;

procedure TFmPrincipal.SbLastWork2Click(Sender: TObject);
(*
var
  Txt: String;
  IMEI: Integer;
begin
  Txt := '12153';
    IMEI := Geral.IMV(Txt);
    VS_PF.MostraFormVSArvoreArtigos(IMEI);
*)
//begin
  //EfdIcmsIpi_Jan.MostraFormEfdIcmsIpiExporta();
  //AppPF.MostraFormSpedEfdIcmsIpiH010_Balancos(3, 201601, -11, 1);
var
  Num_TXT: String;
  Num: Integer;
  VL_OPR, VL_BC, pRedBC: Double;
begin
(*
  VL_OPR := 1000;
  VL_BC  := 670;
  pRedBC := 33;
  Geral.MB_Info(Geral.FFT(SPED_PF.FormulaVL_RED_BC(VL_OPR, VL_BC, pRedBC), 2, siNegativo));

  VL_OPR := 1000;
  VL_BC  := 0;
  pRedBC := 100;
  Geral.MB_Info(Geral.FFT(SPED_PF.FormulaVL_RED_BC(VL_OPR, VL_BC, pRedBC), 2, siNegativo));

  VL_OPR := 1000;
  VL_BC  := 1000;
  pRedBC := 0;
  Geral.MB_Info(Geral.FFT(SPED_PF.FormulaVL_RED_BC(VL_OPR, VL_BC, pRedBC), 2, siNegativo));
*)
(*
  if InputQuery('IMEI', 'Informe o Numero do IMEI', Num_TXT) then
  begin
    Num := Geral.IMV(Num_TXT);
    DmModVS.AtualizaVSMovIts_CusEmit(Num);
  end;
*)
  Application.CreateForm(TForm1, Form1);
  Form1.ShowModal;
  Form1.Destroy;
end;

procedure TFmPrincipal.SbLoginClick(Sender: TObject);
begin
  FmPrincipal.Enabled := False;
  //
  FmBlueDerm_dmk.Show;
  FmBlueDerm_dmk.BringToFront;
  FmPrincipal.SendToBack;
  FmBlueDerm_dmk.EdLogin.Text   := '';
  FmBlueDerm_dmk.EdSenha.Text   := '';
  FmBlueDerm_dmk.EdEmpresa.Text := '';
  FmBlueDerm_dmk.EdLogin.SetFocus;
  //
  InfoSeqIni('Login em espera');
end;

procedure TFmPrincipal.SbMinimizaMenuClick(Sender: TObject);
begin
  AdvToolBarPagerNovo.Visible := not AdvToolBarPagerNovo.Visible;
  //MyObjects.MaximizaPageMenu(AdvToolBarPagerNovo, deftfInverse);
end;

procedure TFmPrincipal.SbPopupGeralClick(Sender: TObject);
begin
  MyObjects.MostraPopupGeral();
end;

procedure TFmPrincipal.SbVerificaDBClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMVerificaDB, SbVerificaDB);
end;

procedure TFmPrincipal.SbWSuportClick(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.sd1FormSkin(Sender: TObject; aName: string;
  var DoSkin: Boolean);
begin
  if aName = VAR_FORMTDI_NAME then
    DoSkin := False;
end;

procedure TFmPrincipal.Escolher2Click(Sender: TObject);
begin
  CadastroDeImpObs();
end;

procedure TFmPrincipal.Textosdeobservaes1Click(Sender: TObject);
begin
  CadastroDeImpObs();
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
var
  Conectado: Boolean;
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmBlueDerm_dmk.Show;
  FmBlueDerm_dmk.BringToFront;
  FmPrincipal.SendToBack;
  FmPrincipal.Hide;
  Enabled := False;
  FmBlueDerm_dmk.Refresh;
  FmBlueDerm_dmk.EdSenha.Refresh;
  FmBlueDerm_dmk.Refresh;
  try
    InfoSeqIni('M�dulo do App ser� criado');
    Application.CreateForm(TDmod, Dmod);
    InfoSeqIni('M�dulo do App criado!');
  except
    on E: Exception do
    begin
      Geral.MB_Erro('Imposs�vel criar Modulo de dados: ' + sLineBreak +
      'Linha: ' + Geral.FF0(FLinModErr) + sLineBreak +
      E.Message);
      Application.Terminate;
      Exit;
    end;
  end;
  try
    InfoSeqIni('M�dulo de faturamento ser� criado');
    Application.CreateForm(TDmPediVda, DmPediVda);
    InfoSeqIni('M�dulo de faturamento criado!');
  except
    Geral.MB_Erro('Imposs�vel criar M�dulo de vendas');
    Application.Terminate;
    Exit;
  end;
  FmBlueDerm_dmk.EdSenha.Refresh;
  FmBlueDerm_dmk.ReloadSkin;
  FmBlueDerm_dmk.EdLogin.Text := '';
  FmBlueDerm_dmk.EdSenha.Refresh;
  FmBlueDerm_dmk.EdLogin.ReadOnly := False;
  FmBlueDerm_dmk.EdSenha.ReadOnly := False;
  FmBlueDerm_dmk.EdLogin.SetFocus;
  //FmBlueDerm_dmk.ReloadSkin;
  FmBlueDerm_dmk.Refresh;
  // ini 2022-02-20
  Conectado := Dmod.MyDB.Connected = True;
  FmBlueDerm_dmk.LaSenha.Enabled := Conectado;
  FmBlueDerm_dmk.EdSenha.Enabled := Conectado;
  FmBlueDerm_dmk.LaLogin.Enabled := Conectado;
  FmBlueDerm_dmk.EdLogin.Enabled := Conectado;
  FmBlueDerm_dmk.LaEmpresa.Enabled := Conectado;
  FmBlueDerm_dmk.EdEmpresa.Enabled := Conectado;
  // fim 2022-02-20

(*
  FmBlueDerm_dmk.EdLogin.Text   := 'master';
  FmBlueDerm_dmk.EdSenha.Text   := '31415926536';
  FmBlueDerm_dmk.EdEmpresa.Text := '1';
  FmBlueDerm_dmk.EdEmpresa.Text := '1';
  FmBlueDerm_Dmk.BtEntraClick(FmBlueDerm_Dmk);
*)

end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  InfoSeqIni('Suporte em ativa��o');
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    (*AdvToolBarButton7*)SbWSuport, BalloonHint1);
  InfoSeqIni('Suporte ativado');
  {$ENDIF}
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
{ Desmarcar quando o site voltar}
  InfoSeqIni('Vers�o em ativa��o em ativa��o');
  TmVersao.Enabled := False;
  //
  //if Geral.MB_Pergunta('Verifica se h� nova versao?!') = ID_YES then
    if DmkWeb.RemoteConnection then
    begin
      if VerificaNovasVersoes(True) then
        DmkWeb.MostraBalloonHintMenuTopo(SbAtualizaERP, BalloonHint1,
          'H� uma nova vers�o!', 'Clique aqui para atualizar.');
    end;
  InfoSeqIni('Vers�o ativado');
end;

procedure TFmPrincipal.Todosnveis1Click(Sender: TObject);
begin
  CadastroDeNiveisPlano();
end;

procedure TFmPrincipal.FornecedoresMP1Click(Sender: TObject);
begin
  CadastroEntiMP();
end;

procedure TFmPrincipal.Fretesemretorno1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMOEnvAvuGer(0);
end;

procedure TFmPrincipal.CadastroFluxos();
begin
  if DBCheck.CriaFm(TFmFluxosCab, FmFluxosCab, afmoNegarComAviso) then
 begin
    FmFluxosCab.ShowModal;
    FmFluxosCab.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroFormulas(Numero, Controle: Integer);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmFormulas2, FmFormulas2, afmoNegarComAviso) then
      begin
        FmFormulas2.ShowModal;
        FmFormulas2.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmFormulas, FmFormulas, afmoNegarComAviso) then
      begin
        if Numero <> 0 then
        begin
          if FmFormulas.TbFormulas.Locate('Numero', Numero, []) then
            FmFormulas.TbFormulasIts.Locate('Controle', Controle, []);
        end;
        FmFormulas.ShowModal;
        FmFormulas.Destroy;
      end;
{
  end;
}
end;

procedure TFmPrincipal.Marcaodedefeitosteste1Click(Sender: TObject);
begin
  Application.CreateForm(TFmVSClassifOneDefei, FmVSClassifOneDefei);
  MyObjects.Informa2(
    FmVSClassifOneDefei.LaAviso1, FmVSClassifOneDefei.LaAviso2, False,
    'Janela de teste! Esta janela pode ser chamada em produ��o na jabela de classifica��o!');
  FmVSClassifOneDefei.ShowModal;
  FmVSClassifOneDefei.Destroy;
end;

procedure TFmPrincipal.Marcas1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'VeicMarcas', 60,
  ncGerlSeq1, 'Cadastro de Marcas de Ve�culos',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.Matriz1Click(Sender: TObject);
begin
  CadastroMatriz();
end;

procedure TFmPrincipal.MenuItem13Click(Sender: TObject);
begin
  VS_PF.MostraFormVSRclArtSel();
end;

procedure TFmPrincipal.MenuItem1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.MenuItem6Click(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmCMPPVai, FmCMPPVai, afmoNegarComAviso) then
  begin
    DmPediVda.ReopenTabelasPedido(); // ?? N�o precisa todas. Mudar?
    FmCMPPVai.ShowModal;
    FmCMPPVai.Destroy;
  end;
*)
end;

procedure TFmPrincipal.MenuItem7Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmCMPPVem, FmCMPPVem, afmoNegarComAviso) then
  begin
    DmPediVda.ReopenTabelasPedido(); // ?? N�o precisa todas Mudar?
    FmCMPPVem.ShowModal;
    FmCMPPVem.Destroy;
  end;
}
end;

procedure TFmPrincipal.MenuItem8Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCECTInn, FmCECTInn, afmoNegarComAviso) then
  begin
    FmCECTInn.ShowModal;
    FmCECTInn.Destroy;
  end;
end;

procedure TFmPrincipal.MenuItem9Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCECTOut, FmCECTOut, afmoNegarComAviso) then
  begin
    //DmPediVda.ReopenTabelasPedido(); // ?? N�o precisa todas. Mudar?
    FmCECTOut.ShowModal;
    FmCECTOut.Destroy;
  end;
end;

procedure TFmPrincipal.Modelos1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'VeicModels', 60,
  ncGerlSeq1, 'Cadastro de Modelos de Ve�culos',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.MostraBackup;
begin
  DModG.MostraBackup3();
  {
  if DBCheck.CriaFm(TFmBackup3, FmBackup3, afmoNegarComAviso) then
  begin
    FmBackup3.ShowModal;
    FmBackup3.Destroy;
  end;
  }
end;

procedure TFmPrincipal.BaixaxestoquedeInNatura1Click(Sender: TObject);
begin
  AppPF.MostraFormCfgEstqInNatEFD(
    2(*ImporExpor*),
    EfdIcmsIpi_PF.AnoMesAnterior(),
    VAR_LIB_EMPRESA_SEL,
    0);
end;

procedure TFmPrincipal.Balanascomunicveis1Click(Sender: TObject);
begin
  CadastroPortSerBal();
end;

procedure TFmPrincipal.Bancos1Click(Sender: TObject);
begin
  CadastroBancos();
end;

procedure TFmPrincipal.BtOSsEmAbertoClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(PMOSsEmAberto, BtOSsEmAberto)
end;

procedure TFmPrincipal.BtControleVendasClick(Sender: TObject);
begin
  //VendasGerencia(True, False);
  App_Jan.VendasGerencia(TMPVAcao.mpvaCabecalho);
end;

procedure TFmPrincipal.BtControleVendasNovoClick(Sender: TObject);
begin
  //VendasGerencia(True, True);
  App_Jan.VendasGerencia(TMPVAcao.mpvaEncerraOS);
end;

procedure TFmPrincipal.BitBtn10Click(Sender: TObject);
var
  I64a, I64b: Int64;
begin
  I64a := 302312230946;
  Geral.WriteAppKeyCU('TesteI64', Application.Title, I64a, ktFloat);
  I64b := Geral.ReadAppKeyCU('TesteI64', Application.Title, ktFloat, 0);
  Geral.MB_Info(
  'I64a = ' + Geral.FF0(I64a) + sLineBreak +
  'I64b = ' + Geral.FF0(I64b) + sLineBreak +
  '');
end;

procedure TFmPrincipal.BitBtn11Click(Sender: TObject);
{
var
  Lista: array of Integer;
  K, I, J, A, N: Integer;
  JaFoi: Boolean;
  T1, T2: Cardinal;
}
begin
{    ini tentativa de eliminar TmySQLQuery

  GBAvisos1.Visible := True;
  //
  Screen.Cursor := crHourGlass;
  try
    I := 0;
    K := 0;
    N := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(QrTeste, DModG.MyPID_DB, [
    'SELECT Controle, SrcNivel2  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE Controle IN ( ',
    ' SELECT DISTINCT JmpNivel2 ',
    '  FROM  _seii_imecs_ _si ',
    ') ',
    '']);
    T1 := GetTickCount;
    PB1.Max := 100;
    PB1.Position := 0;
    for J := 1 to 100 do
    begin
      SetLength(Lista, 0);
      K := 0;
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      QrTeste.First;
      while not QrTeste.Eof do
      begin
        A := QrTeste.FieldByName('SrcNivel2').AsInteger;
        JaFoi := False;
        //
        for I := 0 to K-1 do
        begin
          if Lista[I] = A then
          begin
            N := N + 1;
            JaFoi := True;
            Break;
          end;
        end;
        if JaFoi = False then
        begin
          K := K + 1;
          SetLength(Lista, K);
          Lista[K-1] := A;
        end;
        //
        QrTeste.Next;
      end;
    end;
    T2 := GetTickCount;
    Geral.MB_Info('N = ' + Geral.FF0(N) + sLineBreak +
    'Tempo: ' + FloatToStr((T2-T1) / 1000) + ' s');
  finally
    Screen.Cursor := crDefault;
  end;
    fim tentativa de eliminar TmySQLQuery}
end;

procedure TFmPrincipal.BitBtn15Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPallets, FmPallets, afmoNegarComAviso) then
  begin
    FmPallets.ShowModal;
    FmPallets.Destroy;
  end;
end;

procedure TFmPrincipal.BitBtn16Click(Sender: TObject);
begin
  CadastroMPIn();
end;

procedure TFmPrincipal.BitBtn17Click(Sender: TObject);
begin
  PedidosRelatorios;
end;

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
const
  chNFe = '15100404333952000188550010000000019456724199';
var
  IDCtrl: Integer;
  ID, Dir, Aviso: String;
begin
{    ini tentativa de eliminar TmySQLQuery
  QrCabA.Close;
  QrCabA.Params[00].AsInteger := -11;
  QrCabA.Params[01].AsString  := chNFe;
  UnDmkDAC_PF.AbreQuery(QrCabA, Dmod.MyDB);
  IDCtrl := QrCabAIDCtrl.Value;
  if IDCtrl > 0 then
  begin
    DModG.ReopenParamsEmp(-11);
    Id    := QrCabAinfProt_ID.Value;
    Dir   := DModG.QrPrmsEmpNFeDirSit.Value;
    Aviso := '';
    DmNFe_0000.AtualizaXML_No_BD_ConsultaNFe(chNFe, Id, IDCtrl, Dir, Aviso);
    if Aviso <> '' then Geral.MB_Aviso(
    'Os arquivos abaixo n�o foram localizados:' + sLineBreak + Aviso);
  end;
    fim tentativa de eliminar TmySQLQuery
}
end;

procedure TFmPrincipal.BtMpvImpClick(Sender: TObject);
begin
  //App_Jan.MostraFormOsMpvVs();
  MyObjects.MostraPopupDeBotao(PMMPVImp, BtMPVImp);
end;

procedure TFmPrincipal.BtSoliComprCab1Click(Sender: TObject);
begin
  GFat_Jan.MostraFormSoliComprCab(0);
end;

procedure TFmPrincipal.BtSPED_Contribui��esClick(Sender: TObject);
begin
  EfdPisCofins_Jan_v01_35.MostraFormEfdPisCofinsExporta();
end;

procedure TFmPrincipal.BtSrvTomInnCabClick(Sender: TObject);
begin
  SrvTom_Jan.MostraFormSrvTomInnCab(0);
end;

procedure TFmPrincipal.BtSrvTomRetCabClick(Sender: TObject);
begin
  SrvTom_Jan.MostraFormSrvTomRetCab(0);
end;

procedure TFmPrincipal.BtSrvTomTaxCadClick(Sender: TObject);
begin
  SrvTom_Jan.MostraFormSrvTomTaxCad(0);
end;

procedure TFmPrincipal.BitBtn21Click(Sender: TObject);
begin
  PedidosGerencia(False);
end;

procedure TFmPrincipal.BitBtn23Click(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMImp, BitBtn23);
end;

procedure TFmPrincipal.BitBtn24Click(Sender: TObject);
begin
  MostraBackup;
end;

procedure TFmPrincipal.BitBtn25Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.BitBtn26Click(Sender: TObject);
begin
  Geral.MB_Aviso(dmkPF.PrimeiroDiaAposPeriodo(UnPQx.VerificaBalanco, dtSystem));
end;

procedure TFmPrincipal.BtCouroTerceirosClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(AdvPMCMPT, BtCouroTerceiros);
end;

procedure TFmPrincipal.BtCtbBalPCab1Click(Sender: TObject);
begin
  Contabil_Jan.MostraFormCtbBalPCab1();
end;

procedure TFmPrincipal.BtCtbBalPCab2Click(Sender: TObject);
begin
  Contabil_Jan.MostraFormCtbBalPCab2();
end;

procedure TFmPrincipal.BtCtSub1Click(Sender: TObject);
begin
  FinanceiroJan.MostraFormCtSub1(0);
end;

procedure TFmPrincipal.BtDreCfgCabClick(Sender: TObject);
begin
  Contabil_Jan.MostraFormDreCfgCab();
end;

procedure TFmPrincipal.BtDREClick(Sender: TObject);
begin
  Contabil_Jan.MostraFormDreGerCab();
end;

procedure TFmPrincipal.BitBtn2Click(Sender: TObject);
begin
  Geral.MB_Aviso(FloatToStr(Frac(1.234567 * 1000)));
end;

procedure TFmPrincipal.BitBtn3Click(Sender: TObject);
begin
  Application.CreateForm(TFmCNPJaConsulta, FmCNPJaConsulta);
  with FmCNPJaConsulta do
  begin
    ShowModal;
    Destroy;
  end;
end;

procedure TFmPrincipal.BitBtn6Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  if DBCheck.CriaFm(TFmEfd_RegObrig, FmEfd_RegObrig, afmoNegarComAviso) then
  begin
    FmEfd_RegObrig.ShowModal;
    FmEfd_RegObrig.Destroy;
  end;
end;

procedure TFmPrincipal.BitBtn8Click(Sender: TObject);
  function LetraENum(Letra: String): Integer;
  begin
    Result := dmkPF.ColTxtToInt(Letra);
    Memo1.Lines.Add(Letra + ' -> ' + Geral.FF0(Result));
  end;
  function L(Num: Integer): String;
  begin
    if Num = 0 then
      Result := ''
    else
      Result := Char(Num + 64);
  end;
var
  N, I, i1, i2, i3, i4, iCol, Seq: Integer;
  x, xCol: String;

begin
(*  Seq := 0;
  for i1 := 0 to 26 do
  begin
    for i2 := 0 to 26 do
    begin
      if (i1> 0) and (i2= 0) then begin end
      else
      begin
        for i3 := 0 to 26 do
        begin
          if (i2 > 0) and (i3 = 0) then begin end
          else
          begin

            for i4 := 0 to 26 do
            begin
              if (i3 > 0) and (i4 = 0) then begin end
              else
              begin
                xCol := L(i1) + L(i2) + L(i3) + L(i4);
                iCol := LetraENum(xCol);
                 //
                if iCol <> Seq then
                  Geral.MB_Erro(xCol + ' = ' + Geral.FF0(iCol) + ' (' + Geral.FF0(Seq) + ')');
                //
                Seq := Seq + 1;
              end;
           end;
         end;
        end;
      end;
    end;
  end;
  EXIT;
*)
  Memo1.Lines.Clear;
  for I := 1 to 1000000 do
  //for I := 67599 to 67602 do
  begin
    x := dmkPF.IntToCODIF_Receita(-I);
    N := dmkPF.CODIFToInt_Receita(x);
    if (N / 173) = (N div 173) then
      Memo1.Lines.Add(x + ' = ' + Geral.FF0(N));
    if N <> I then
      Geral.MB_Erro(x + ' = ' + Geral.FF0(N) + ' (' + Geral.FF0(I) + ')');
  end;
(*
    N := -5100;
    x := dmkPF.IntToCODIF_Receita(N);
    Memo1.Lines.Add(x + ' = ' + Geral.FF0(N));
*)
(*
  Memo1.Lines.Clear;
  for I := 5099 to 5101 do
  begin
    x := dmkPF.IntToCODIF_Receita(-I);
    N := dmkPF.CODIFToInt_Receita(x);
    Memo1.Lines.Add(x + ' = ' + Geral.FF0(N));
  end;
*)
end;

procedure TFmPrincipal.BitBtn9Click(Sender: TObject);
begin
  App_Jan.MostraFormFormulaV(0);
end;

procedure TFmPrincipal.BtAjustePQwClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMAjusteEstqPQ, BtAjustePQw);
end;

procedure TFmPrincipal.BtAtzSdosClick(Sender: TObject);
var
  s: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM vsmovits',
  'WHERE GraGruX=1',
  'AND Pecas > 0 ',
  'AND not (MovimID IN (12,17,41))',
  //'AND SdoVrtPeca > 0',
  '']);
  s := ' de ' + Geral.FF0(Dmod.QrAux.RecordCount);
  while not Dmod.QrAux.Eof do
  begin
    MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True,
      Geral.FF0(Dmod.QrAux.RecNo) + s);
    VS_CRC_PF.AtualizaSaldoIMEI(Dmod.QrAux.Fields[0].AsInteger, False);
    //
    Dmod.QrAux.Next;
  end;
  MyObjects.Informa2(LaTopWarn1, LaTopWarn2, False, '...');
end;

procedure TFmPrincipal.BtBanhosImpClick(Sender: TObject);
begin
  UnPQx.MostraFormPQBanhosImp();
end;

procedure TFmPrincipal.BtCadastroInsumosClick(Sender: TObject);
begin
  CadastroPQ(0);
end;

procedure TFmPrincipal.BtCambioClick(Sender: TObject);
begin
  CadastroDeCambio();
end;

procedure TFmPrincipal.BtCentroCust2Click(Sender: TObject);
begin
  CeCuRe_Jan.MostraFormCentroCust2(0);
end;

procedure TFmPrincipal.BtCentroCust3Click(Sender: TObject);
begin
  CeCuRe_Jan.MostraFormCentroCust3(0);
end;

procedure TFmPrincipal.BtCentroCust4Click(Sender: TObject);
begin
  CeCuRe_Jan.MostraFormCentroCust4(0);
end;

procedure TFmPrincipal.BtCentroCust5Click(Sender: TObject);
begin
  CeCuRe_Jan.MostraFormCentroCust5(0);
end;

procedure TFmPrincipal.BtCentroCustAllClick(Sender: TObject);
begin
  CeCuRe_Jan.MostraFormCentroCustAll(0);
end;

procedure TFmPrincipal.BtCentroCustoClick(Sender: TObject);
begin
  CeCuRe_Jan.MostraFormCentroCusto(0);
end;

procedure TFmPrincipal.BtEfdInnCTsGerClick(Sender: TObject);
begin
  EfdIcmsIpi_Jan.MostraFormEfdInnCTsGer();
end;

procedure TFmPrincipal.BtEmiteDuplicataClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmDuplicata1, FmDuplicata1, afmoNegarComAviso) then
  begin
    FmDuplicata1.ShowModal;
    FmDuplicata1.Destroy;
  end;
end;

procedure TFmPrincipal.BtEmiteNotaPromissoriaClick(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmPromissoria, FmPromissoria, afmoNegarComAviso) then
  begin
    FmPromissoria.ShowModal;
    FmPromissoria.Destroy;
  end;
end;

procedure TFmPrincipal.BtEntidadesClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal.BtEntiProSoftClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiLoad02, FmEntiLoad02, afmoSoAdmin) then
  begin
    FmEntiLoad02.ShowModal;
    FmEntiLoad02.Destroy;
  end;
end;

procedure TFmPrincipal.BtGraGruYPrecosClick(Sender: TObject);
begin
  VS_Jan.MostraFormGraGruYPrecos(0);
end;

procedure TFmPrincipal.BtGruposContabeisClick(Sender: TObject);
begin
  FinanceiroJan.MostraFormCtbCadGru(0);
end;

procedure TFmPrincipal.BtImagemClick(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.BtImpETEClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImpETE, BtImpETE);
end;

procedure TFmPrincipal.BtLe2Click(Sender: TObject);
begin
  UnNFeImporta_0400.MostraFormNFe_DownXML(TtpNF.tpnfSaida, TtpEmissNF.tpemiProprio);
end;

procedure TFmPrincipal.BtMapaOperPosProcClick(Sender: TObject);
begin
  App_Jan.MostraFormTMapaOperPosProc();
end;

procedure TFmPrincipal.BtMarcasClick(Sender: TObject);
begin
  VS_Jan.MostraFormVSLocalizaMarcas();
end;

procedure TFmPrincipal.BtMovimentacaoFiscalNoContabilClick(Sender: TObject);
begin
  FinanceiroJan.MostraFormCtbCadMoF(0);
end;

procedure TFmPrincipal.BtPesagemAcabtoClick(Sender: TObject);
begin
  PQ_PF.MostraFormFormulasImpFI();
end;

procedure TFmPrincipal.BtPesagemCalCurClick(Sender: TObject);
begin
  PQ_PF.MostraFormFormulasImpBH();
end;

procedure TFmPrincipal.BtPesagemRecurtClick(Sender: TObject);
begin
  PQ_PF.MostraFormFormulasImpWE();
end;

procedure TFmPrincipal.BtPQIClick(Sender: TObject);
begin
  UnPQx.MostraFormPQI(0);
end;

procedure TFmPrincipal.BtPQNClick(Sender: TObject);
begin
  UnPQx.MostraFormPQN(0);
end;

procedure TFmPrincipal.BtPURelatPQClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRelatPQ, BtPURelatPQ);
end;

procedure TFmPrincipal.BtReceitaAcabtoClick(Sender: TObject);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmTintasCab2, FmTintasCab2, afmoNegarComAviso) then
      begin
        FmTintasCab2.ShowModal;
        FmTintasCab2.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmTintasCab, FmTintasCab, afmoNegarComAviso) then
      begin
        FmTintasCab.ShowModal;
        FmTintasCab.Destroy;
      end;
{
  end;
}
end;

procedure TFmPrincipal.BtReceitaRibeiraClick(Sender: TObject);
begin
  CadastroFormulas(0, 0);
end;

procedure TFmPrincipal.BtRelatorioInsumosClick(Sender: TObject);
begin
  CadastroPQImp('');
end;

procedure TFmPrincipal.BtTrocaDeProdutoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPQSubstitui, FmPQSubstitui, afmoNegarComAviso) then
  begin
    FmPQSubstitui.ShowModal;
    FmPQSubstitui.Destroy;
  end;
end;

procedure TFmPrincipal.BtVSConCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSConCab(0);
end;

procedure TFmPrincipal.BtVSExcCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSExcCab(0, 0);
end;

procedure TFmPrincipal.MostraParamsEmp;
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

procedure TFmPrincipal.MostraPediAcc;
begin
  if DBCheck.CriaFm(TFmPediAcc, FmPediAcc, afmoNegarComAviso) then
  begin
    FmPediAcc.ShowModal;
    FmPediAcc.Destroy;
  end;
end;

procedure TFmPrincipal.MostraPediPrzCab(Codigo: Integer);
begin
(*
  if VAR_LIB_ARRAY_EMPRESAS_CONTA > 1 then
  begin
    if DBCheck.CriaFm(TFmPediPrzCab2, FmPediPrzCab2, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmPediPrzCab2.LocCod(Codigo, Codigo);
      FmPediPrzCab2.ShowModal;
      FmPediPrzCab2.Destroy;
    end;
  end else
*)
  begin
    if DBCheck.CriaFm(TFmPediPrzCab1, FmPediPrzCab1, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmPediPrzCab1.LocCod(Codigo, Codigo);
      FmPediPrzCab1.ShowModal;
      FmPediPrzCab1.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.MostraRegioes;
begin
{$IfDef cDbTable}
  if DBCheck.CriaFm(TFmRegioes, FmRegioes, afmoNegarComAviso) then
  begin
    FmRegioes.ShowModal;
    FmRegioes.Destroy;
  end;
{$EndIf}
end;

procedure TFmPrincipal.MostraTabePrcCab;
begin
  if DBCheck.CriaFm(TFmTabePrcCab, FmTabePrcCab, afmoNegarComAviso) then
  begin
    FmTabePrcCab.ShowModal;
    FmTabePrcCab.Destroy;
  end;
end;

procedure TFmPrincipal.Mover_PrdGrupTip_do_GraGruY_Para_GraGrYPGT();
var
  Qry: TmySQLQuery;
  GraGruY, PrdGrupTip: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'DESCRIBE gragruy "PrdGrupTip"',
    '']);
    if Qry.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ggy.Codigo GraGruY, ggy.PrdGrupTip',
      'FROM gragruy ggy',
      'LEFT JOIN gragruypgt ygt ON ygt.GraGruY=ggy.Codigo',
      'WHERE ygt.PrdGrupTip IS NULL',
      'AND ggy.PrdGrupTip<>0',
      '']);
      //
      Qry.First;
      while not Qry.Eof do
      begin
        GraGruY    := Qry.FieldByName('GraGruY').AsInteger;
        PrdGrupTip := Qry.FieldByName('PrdGrupTip').AsInteger;
        //
        // Evitar erro
        Dmod.QrUpd.Database := Dmod.MyDB;
        //if
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruypgt', False, [
        ], [
        'GraGruY', 'PrdGrupTip'], [
        ], [
        GraGruY, PrdGrupTip], True);
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal.Movimentaes1Click(Sender: TObject);
begin
  AppPF.MostraFormCfgMovEFD(
    2(*ImporExpor*),
    EfdIcmsIpi_PF.AnoMesAnterior(),
    VAR_LIB_EMPRESA_SEL,
    0);
end;

procedure TFmPrincipal.N00Geral1Click(Sender: TObject);
begin
  if Screen.Height <= 768 then
    VS_PF.MostraFormVSMovImp(False, nil, nil, -1)
  else
  if Sender is TMenuItem then
    VS_PF.MostraFormVSMovImp(False, nil, nil, -1)
  else
  {$IfDef cAdvToolx} //Tokyo
    VS_PF.MostraFormVSMovImp(True, PageControl1, AdvToolBarPager2, -1);
    AdvToolBarPagerNovo
  {$Else}
    VS_CRC_PF.MostraFormVSMovImp(True, PageControl1, AdvToolBarPagerNovo, -1);
  {$EndIf}
end;

procedure TFmPrincipal.N02FichaKardexagrupado1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSImpKardex4();
end;

procedure TFmPrincipal.N02FichaKardexcorrido1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSImpKardex5();
end;

procedure TFmPrincipal.N05ClassesporMartelo1Click(Sender: TObject);
begin
  VS_PF.ImprimePesquisaMartelos2(VAR_LIB_EMPRESA_SEL, VAR_LIB_EMPRESA_SEL_TXT);
end;

procedure TFmPrincipal.N101Produo1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSImpProducao();
end;

procedure TFmPrincipal.N102InNaturaPendente1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSInnNatPend();
end;

procedure TFmPrincipal.N104Saldodemovimentaocorretiva1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSImpSdoCorret();
end;

procedure TFmPrincipal.N105CustodeProduo1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSCustosProdu();
end;

procedure TFmPrincipal.N106Tabeladerendimentoinnaturacurtido1Click(
  Sender: TObject);
begin
  VS_Jan.MostraFormVSTabMPAG();
end;

procedure TFmPrincipal.N107Estoquedereduzido1Click(Sender: TObject);
begin
  VS_Jan.MostraFormVSImpEstqReduz();
end;

procedure TFmPrincipal.N108SIFDIPOA1Click(Sender: TObject);
begin
  VS_Jan.MostraFormVSSifDipoaImp(EmptyStr);
end;

procedure TFmPrincipal.N12MovimentodecompraevendaMPAG1Click(Sender: TObject);
begin
  VS_PF.MostraRelatorioVSImpCompraVenda2();
end;

procedure TFmPrincipal.N21RendimentoSemi1Click(Sender: TObject);
begin
  VS_PF.ImprimeRendimentoIMEIS([(*ImeiIni*)0], [False], [False], [], []);
end;

procedure TFmPrincipal.N24RendimentoeMO1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSImpMOEnvRet2();
end;

procedure TFmPrincipal.N26EstoqueCustoIntegrado1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSEstqCustoIntegr(0);
end;

procedure TFmPrincipal.NFe1Click(Sender: TObject);
var
  NF, Empresa: Integer;
begin
  NF := MyVCLRef.GET_ValorDeObjeto_Inteiro();
  Empresa := 0; // Todas
  UnNFe_PF.PesquisaEMostraFormNFePesqEmitida(NF, Empresa);
end;

procedure TFmPrincipal.NFeRecebida1Click(Sender: TObject);
var
  NF, Empresa: Integer;
begin
  NF := MyVCLRef.GET_ValorDeObjeto_Inteiro();
  Empresa := 0; // Todas
  UnNFe_PF.PesquisaEMostraFormNFePesqRecebida(NF, Empresa, 0);
end;

procedure TFmPrincipal.nica1Click(Sender: TObject);
begin
  //EXIT;
  VS_PF.MostraFormVSFchRslCab(0, 0);
end;

procedure TFmPrincipal.Nmeroainformar1Click(Sender: TObject);
var
  ValVar: Variant;
  Numero: Integer;
begin
  VAR_POPUP_ACTIVE_Numero := True;
  try
    MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, 0, 0, 0,
    '', '', True, 'C�digo', 'Informe o c�digo: ', 0, ValVar);
  finally
    VAR_POPUP_ACTIVE_Numero := False;
  end;
end;

procedure TFmPrincipal.NovaConfiguraodeOC1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSRclArtPrpNew(stIns, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, 0, 0);
end;

procedure TFmPrincipal.Novo1Click(Sender: TObject);
begin
  Grade_Jan.CriaFormEntradaCab(tnfUsoEConsumo, True, 0);
end;

procedure TFmPrincipal.Novo2Click(Sender: TObject);
begin
  VS_PF.MostraFormVSRclArtPrpNew(stIns, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, 0, 0);
end;

procedure TFmPrincipal.NovoGrade1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraImpLista, FmGraImpLista, afmoNegarComAviso) then
  begin
    FmGraImpLista.ShowModal;
    FmGraImpLista.Destroy;
  end;
end;

procedure TFmPrincipal.IDAntes1Click(Sender: TObject);
begin
//
end;

procedure TFmPrincipal.IdIPWatch1Status(ASender: TObject;
  const AStatus: TIdStatus; const AStatusText: string);
begin
//Pasta nao acessivel ao usuario
//C:\Arquivos de Programas\Dermatek
//Mudar ???
  IdIPWatch1.HistoryFileName := 'C:\Dermatek\iphist.dat';
  //IdIPWatch1.HistoryEnabled := False; -> se habilitar, como fica?
  IdIPWatch1.LocalIP;
end;

procedure TFmPrincipal.IDJanela1Click(Sender: TObject);
begin
  Geral.MB_Aviso(TForm(Screen.ActiveForm).Caption + ' [' +
    TForm(Screen.ActiveForm).Name + ']');
end;

procedure TFmPrincipal.IECInformaodeEntradadeCouros1Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmVSInfIEC, FmVSInfIEC, afmoNegarComAviso) then
  begin
    FmVSInfIEC.ShowModal;
    FmVSInfIEC.Destroy;
  end;
end;

procedure TFmPrincipal.IMEC1Click(Sender: TObject);
begin
  VS_PF.MostroFormVSMovimCod(MyVCLRef.GET_ValorDeObjeto_Inteiro());
end;

procedure TFmPrincipal.IMEI1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovIts(MyVCLRef.GET_ValorDeObjeto_Inteiro());
end;

procedure TFmPrincipal.IMEIscomcampospositivosenegativosnomesmoregistro1Click(
  Sender: TObject);
const
  TemIMEIMrt = 0;
  Avisa = True;
  ForcaMostrarForm = True;
  SelfCall = False;
begin
  DmModVS.QtdPosNegSameReg(TemIMEIMrt,
    Avisa, ForcaMostrarForm, SelfCall, nil, nil);
end;

procedure TFmPrincipal.IMEIscomValoresPositivoseNegativosnoMesmoregistro1Click(
  Sender: TObject);
const
  TemIMEIMrt = 0;
  Avisa = True;
  ForcaMostrarForm = True;
  SelfCall = False;
begin
  DmModVS.VlrPosNegSameReg(TemIMEIMrt,
    Avisa, ForcaMostrarForm, SelfCall, nil, nil);
end;

procedure TFmPrincipal.IMEIsGmeosorfos1Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmVSCorrigeMovimTwn, FmVSCorrigeMovimTwn, afmoNegarComAviso) then
  begin
    FmVSCorrigeMovimTwn.ShowModal;
    FmVSCorrigeMovimTwn.Destroy;
  end;
end;

procedure TFmPrincipal.IMEIssemFornecedor1Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmVSCorrigeMulFrn, FmVSCorrigeMulFrn, afmoNegarComAviso) then
  begin
    FmVSCorrigeMulFrn.ShowModal;
    FmVSCorrigeMulFrn.Destroy;
  end;
end;

procedure TFmPrincipal.ImgLogoClick(Sender: TObject);
begin
  MyObjects.MostraPopOnControlXY(PMLogo, ImgLogo, 0, 0);
end;

procedure TFmPrincipal.ImpressaoDoPlanoDeContas;
begin
  if DBCheck.CriaFm(TFmContasSdoImp, FmContasSdoImp, afmoNegarComAviso) then
  begin
    FmContasSdoImp.ShowModal;
    FmContasSdoImp.Destroy;
  end;
end;

procedure TFmPrincipal.Inclui1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPReclasIns, FmMPReclasIns, afmoNegarComAviso) then
  begin
    FmMPReclasIns.ImgTipo.SQLType := stIns;
    FmMPReclasIns.ShowModal;
    FmMPReclasIns.Destroy;
  end;
end;

procedure TFmPrincipal.IncongrunciasdeestoquedeUsoeConsumo1Click(
  Sender: TObject);
begin
  App_Jan.MostraFormPQImpErr();
end;

procedure TFmPrincipal.Inconsistnciadeinformao1Click(Sender: TObject);
begin
  Eiia_PF.InsereNovaInconsistenciaDeInformacao();
end;

procedure TFmPrincipal.InfoSeqIni(Msg: String);
begin
  if VAR_MSG_START then
    FmBlueDerm_Dmk.MeAvisos.Text := Msg + sLineBreak + FmBlueDerm_Dmk.MeAvisos.Text;
end;

procedure TFmPrincipal.ipos1Click(Sender: TObject);
begin
  MostraDefeitostip(0);
end;

procedure TFmPrincipal.iposdeArtigo1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadRegistroSimples(Dmod.MyDB, DmModVS.TbCouNiv1,
    DmModVS.DsCouNiv1, ncIdefinido, 'Cadastro de Partes de Material VS');
end;

function TFmPrincipal.AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
var
  LinAtu: Integer;
begin
  if PosicaoAtual < NovaPosicao then
  begin
    LinAtu := Trunc((NovaPosicao - PosicaoAtual) / FAltLin);
    while LinAtu > 0 do
    begin
      if LinAtu > 216 then
      begin
        Write(FArqPrn, #27+'3'+#216);
        LinAtu := LinAtu - 216;
        WriteLn(FArqPrn,' ');
      end else begin
        Write(FArqPrn, MyPrinters.DefAlturaDotImp(3, LinAtu, ''));
        WriteLn(FArqPrn,' ');
        LinAtu := 0;
      end;
    end;
  end;
  Result := NovaPosicao;
  FPrintedLine := False;
end;

function TFmPrincipal.FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
  EspacosPrefixo, Substitui, Alinhamento, Comprimento, Fonte, Formatacao, CPI:
  Integer; Nulo: Boolean): String;
var
  i, Tam, Letras, MyCPI, Vazios: Integer;
  Esq: Boolean;
  Txt, Vaz: String;
  Increm: Double;
begin
  if Substitui = 1 then Txt := Substituicao else
  begin
    if Texto = '' then Txt := ''
    else Txt := Formata(Texto, Prefixo, Sufixo, EspacosPrefixo, Formatacao, Nulo);
  end;
  if Txt = '' then
  Txt := ' '; // evitar erro de Margem Esquerda ao imprimir ??
  if CPI = 0 then MyCPI := 10 else MyCPI := CPI;
  Tam := Fonte + (MyCPI*10);
  Letras := 0;
  case Tam of
    100: Letras := 17;
    101: Letras := 10;
    102: Letras := 05;
    120: Letras := 20;
    121: Letras := 12;
    122: Letras := 06;
    else Geral.MB_Erro('CPI indefinido!');
  end;
  Increm := (CO_POLEGADA * 100) / Letras;
  Letras := Trunc(((Comprimento / 100) / CO_POLEGADA) * Letras);
  FTamLinha := FTamLinha + Trunc(Increm * Letras);
  Vazios := Letras - Length(Txt);
  Esq := True;
  // [0: Esq] [1: Centro] [2: Direita]
  if Alinhamento = 1 then
  begin
    for i := 1 to Vazios do
    begin
      if Esq then
      begin
        Txt := Txt + ' ';
        Esq := False;
      end else begin
        Txt := ' ' + Txt;
        Esq := True;
      end;
    end;
  end else begin
    for i := 1 to Vazios do Vaz := Vaz + ' ';
    if Alinhamento = 0 then Txt := Txt + Vaz else Txt := Vaz + Txt;
  end;
  Result := Txt;
end;

procedure TFmPrincipal.FormatoA1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidades, fmcadEntidades);
end;

procedure TFmPrincipal.FormatoB1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

function TFmPrincipal.EspacoEntreTextos(QrView: TmySQLQuery; CPI: Integer): String;
var
  Texto: String;
  i, MyCPI, Letras: Integer;
  Soma: Double;
begin
  Texto := '';
  if FTamLinha > QrView.FieldByName('MEsq').AsInteger then
  begin
    // Erro !!!
  end;
  if FTamLinha < QrView.FieldByName('MEsq').AsInteger then
  begin
    Texto := #15;
    if CPI = 0 then MyCPI := 10 else MyCPI := CPI;
    MyCPI := MyCPI + FFoiExpand;
    case MyCPI of
     010: MyCPI := 17;
     012: MyCPI := 20;
     110: MyCPI := 10;
     112: MyCPI := 10;
    end;
    Soma := (CO_POLEGADA * 100)/ MyCPI;
    Letras := Trunc((QrView.FieldByName('MEsq').AsInteger - FTamLinha) /
      (CO_POLEGADA * 100) * MyCPI);
    for i := 1 to Letras do Texto := Texto + ' ';
    FTamLinha := FTamLinha + Trunc(Soma * Letras);
  end;
  Result := Texto;
end;

procedure TFmPrincipal.Estgios1Click(Sender: TObject);
begin
  App_Jan.MostraFormFluxPcpStg();
end;

procedure TFmPrincipal.Executartodasferramentasemsequncia1Click(
  Sender: TObject);
const
  Avisa = False;
  ForcaMostrarForm = False;
  SelfCall = False;
  TemIMEIMrt = 0;
var
  Empresa, GraGruX: Integer;
  MargemErro: Double;
  sItens: String;
begin
  //
  Empresa := -11;
  GraGruX := 0;
  MargemErro := 0.0005;
  //
  //if
  DmModVS.QtdPosNegSameReg(TemIMEIMrt, Avisa,
    ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2)(* then Exit*);
  //if
  DmModVS.RedConfltGerA(ForcaMostrarForm, SelfCall, False, LaAviso1, LaAviso2)(* then Exit*);
  //if
  DmModVS.RedConfltBaixa(ForcaMostrarForm, SelfCall, False, LaAviso1, LaAviso2)(* then Exit*);
  //if
  DmModVS.DstGGXDiferentesDeGraGruX(LaAviso1, LaAviso2)(* then Exit*);
  //if
  DmModVS.SrcGGXDiferentesDePreClasseParaClasseEReclasse(False, False, False,
    LaAviso1, LaAviso2)(* then Exit*);
  //if
  VS_PF.RegistrosComProblema(Empresa, GraGruX, LaAviso1, LaAviso2)(* then Exit*);
  //if
  DmModVS.VSPcPosNegXValTNegPos(Empresa, GraGruX, TemIMEIMrt, MargemErro,
    Avisa, ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2)(* then Exit*);
  // if
  DmModVS.VlrPosNegSameReg(TemIMEIMrt, Avisa,
    ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2)(* then Exit*);
  //if
  DmModVS.VmiPaiDifProcessos(ForcaMostrarForm, SelfCall, TemIMEIMrt, LaAviso1, LaAviso2)(* then Exit*);
  //if
  DmModVS.CustosDiferentesSaldosVirtuais3(Empresa, GraGruX, TemIMEiMrt,
    MargemErro, Avisa, ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2)(* then Exit*);
end;

procedure TFmPrincipal.Expedidos1Click(Sender: TObject);
begin
  App_Jan.MostraFormMpvImp(TMPVImp.mpvimpExped);
end;

procedure TFmPrincipal.ExtratosFinanceirosEmpresaUnica(TabLctA: String);
begin
  //Copiar codigo de outro projeto!!!
end;

function TFmPrincipal.Formata(Texto, Prefixo, Sufixo: String; EspacosPrefixo,
 Formato: Integer; Nulo: Boolean): String;
var
  MeuTexto, MeuPrefixo: String;
  i: Integer;
begin
  if Trim(Texto) = '' then MeuTexto := Texto else
  begin
    case Formato of
      1: MeuTexto := Geral.TFT(Texto, 0, siNegativo);
      2: MeuTexto := Geral.TFT(Texto, 1, siNegativo);
      3: MeuTexto := Geral.TFT(Texto, 2, siNegativo);
      4: MeuTexto := Geral.TFT(Texto, 3, siNegativo);
      5: MeuTexto := Geral.TFT(Texto, 4, siNegativo);
      6: MeuTexto := FormatDateTime(VAR_FORMATDATE3, StrToDate(Texto));
      7: MeuTexto := FormatDateTime(VAR_FORMATDATE2, StrToDate(Texto));
      8: MeuTexto := FormatDateTime(VAR_FORMATTIME2, StrToTime(Texto));
      9: MeuTexto := FormatDateTime(VAR_FORMATTIME , StrToTime(Texto));
      else MeuTexto := Texto;
    end;
  end;
  if (Formato in ([1,2,3,4,5])) and Nulo then
  begin
    if Geral.DMV(MeuTexto) < 0.0001 then MeuTexto := '';
  end;
  if MeuTexto = '' then Result := '' else
  begin
    MeuPrefixo := '';
    if Length(Prefixo) > 0 then
      for i := 0 to EspacosPrefixo-1 do MeuPrefixo := MeuPrefixo + ' ';
    Result := Prefixo+MeuPrefixo+MeuTexto+Sufixo;
  end;
end;

procedure TFmPrincipal.Embalagens1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEmbalagens, FmEmbalagens, afmoNegarComAviso) then
  begin
    FmEmbalagens.ShowModal;
    FmEmbalagens.Destroy;
  end;
end;

procedure TFmPrincipal.Emitidos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmResultados1, FmResultados1, afmoNegarComAviso) then
  begin
    FmResultados1.FTabLctA := LAN_CTOS;
    FmResultados1.ShowModal;
    FmResultados1.Destroy;
  end;
end;

procedure TFmPrincipal.Energiaeltricaguags1Click(Sender: TObject);
begin
  //EfdIcmsIpi_Jan.MostraFormEFD_C001();
  EfdIcmsIpi_Jan_v03_0_9.MostraFormEfdInnC001Ger();
end;

procedure TFmPrincipal.Entidade1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(
    MyVCLRef.GET_ValorDeObjeto_Inteiro(), fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal.Entidades2Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal.Entradas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCMPTInn, FmCMPTInn, afmoNegarComAviso) then
  begin
    FmCMPTInn.ShowModal;
    FmCMPTInn.Destroy;
  end;
end;

procedure TFmPrincipal.Envioparaposteriorretorno1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMOEnvEnvGer(0);
end;

procedure TFmPrincipal.EquipesClick(Sender: TObject);
begin
  CadastroEquipes();
end;

procedure TFmPrincipal.Acabamento1Click(Sender: TObject);
begin
  PQ_PF.MostraFormFormulasImpFI();
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
  Entidade: Integer; Aba: Boolean);
begin
  if Entidade > 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Codigo');
    Dmod.QrAux.SQL.Add('FROM ponto');
    Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrAux.Params[0].AsInteger := Entidade;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    if Dmod.QrAux.RecordCount = 0 then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT Codigo');
      Dmod.QrAux.SQL.Add('FROM entidades');
      Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrAux.SQL.Add('AND Fornece5="V"');
      Dmod.QrAux.Params[0].AsInteger := Entidade;
      UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
      //
      if Dmod.QrAux.RecordCount = 1 then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO ponto SET Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := Entidade;
        Dmod.QrUpd.ExecSQL;
      end;
    end;
  end;
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade1: TStringGrid);
begin
   // Compatibilidade
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
const
  AutoRegera = True;
begin
  PageControl1.ActivePageIndex := 0;
  //
  try
    Screen.Cursor := crHourGlass;
    //
    if DModG <> nil then
    begin
      // Provis�rio - Copiar o Campo "PrdGrupTip" para a tabela GraGruYPGT
      // Eliminar o Campo "PrdGrupTip" no Futuro!!!
      Mover_PrdGrupTip_do_GraGruY_Para_GraGrYPGT();
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Definindo data m�nima de lan�amentos de Insumos');
      UnPQx.DefineVAR_Data_Insum_Movim();
      if DmodG.QrDonoCNPJ_CPF.Value = '02717861000110' then //Apenas o SoftCouro at� finalizar o Sped depois remover
        AGBConfEstq.Visible := True
      else
        AGBConfEstq.Visible := False;
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Criando DB usu�rio');
      DModG.MyPID_DB_Cria();
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      StatusBar.Panels[11].Text := Dmod.MyDB.DatabaseName;
      //
      MyObjects.Informa2(LaTopWarn2, LaTopWarn1, True, 'Criando favoritos');
      if not DModG.CriaFavoritos(AdvToolBarPagerNovo, LaTopWarn2, LaTopWarn1,
        BtEntidades, FmPrincipal)
      then
        if DModG.QrCtrlGeralAbaIniApp.Value > 0 then
          //AdvToolBarPager2.ActivePageIndex := DModG.QrCtrlGeralAbaIniApp.Value;
          AdvToolBarPagerNovo.ActivePageIndex := DModG.QrCtrlGeralAbaIniApp.Value;

      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Atualizando cota��es');
      DModG.AtualizaCotacoes();
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Verificando filiais');
      if (VAR_USUARIO  = -1) or (VAR_USUARIO = -3) then
      begin
        DModG.QrFiliaisSP.Close;
        UnDmkDAC_PF.AbreQuery(DModG.QrFiliaisSP, Dmod.MyDB);
        if DModG.QrFiliaisSP.RecordCount > 0 then
          MostraParamsEmp();
      end;
      //AtzPed();
      // verifica se h� gragrux para os PQs!
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Verificando necessidade de corre��es');
      VerificaPQ_GGX();
      // corrigir cadastro errado (eliminar ap�s atualizar o BD do IDEAL)
      VerificaMP_GGX();
      //
      //if DModG.QrCtrlGeralAtualizouPreEmail.AsInteger = 0 then
        //DModG.AtualizaPreEmail;
      //
      DmNFe_0000.VerificaNFeCabA();
      //DmodG.ConfiguraIconeAplicativo;
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Verificando alertas');
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      //
      DmodG.VerificaHorVerao();
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Definindo Periodo do Balan�o VS');
      DMModVS.DefinePeriodoBalVSEmpresaLogada();
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True,
        'Definindo a data m�nima para lan�amentos SPED EFD ICMS/IPI');
      VAR_Data_SPEDEFDEnce_MovimXX :=
        EfdIcmsIpi_PF.SPEDEFDEnce_DataMinMovim('MovimXX');
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Definindo Aba inicial');
      // Temporario
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Modificando Empresas para Cliente MO');
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE entidades ',
      'SET Cliente2="V" ',
      'WHERE Codigo < -10 ',
      '']);
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, False, '...');
      // Fim temporario
      {  BERLIN desabilitei por enquanto!
      if VAR_USUARIO = -1 then
      begin
        Geral.MB_Info('Janela de selecao de DB e Server !' + sLineBreak +
          'Amostras' + sLineBreak +
          '###############' + sLineBreak +
          'Criar aviso de erro na exporta��o SPED do valor a recolher no E116 <> E110'
          + sLineBreak +
          '###############' + sLineBreak +
'Empresa e Natureza da operacao '
          + sLineBreak +
'ver materias que nao vao no K200 como carro e extintor e moldura de quadro! '
          + sLineBreak +
'Gerenciar atrelamentos de NF MO no artigo destino recurtimento sem atrelamento (-1)! '
          + sLineBreak +
'Mudar Uso e consumo em todo sistema'
          + sLineBreak +
'ver se esta importando texto extra do item nfe'
          + sLineBreak +
          'FRCampos.Field      := ?JmpMovID;'
          + sLineBreak +
          '');
      end;}
      UFixBugs.MostraFixBugs(['Outros', 'Atualiza NFs de entrada de uso e consumo']);
      //
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtMostra);
      //
      if Dmod.QrControleVSPwdDdUser.Value = VAR_USUARIO then
      begin
        VS_PF.MostraFormVSPwdDd(AutoRegera);
      end;
      //
      if VAR_USUARIO = -1 then
      begin
        if Geral.MB_Pergunta('Deseja pesquisar movimenta��es incorretas de couro?') = ID_Yes then
        begin
          //VS_PF.VerificaFornecMO(PB1, LaTopWarn1, LaTopWarn2);
          VS_PF.VerificaCliForEmpty();
          VS_PF.VerificaStqCenLocEmpty();
          //...
        end;
      end;
      // Desmarcar por enquanto!
      //VS_PF.VerificaOpeSemOperacao();
      //
      VS_PF.VerificaAtzCaleados(PB1, LaTopWarn1, LaTopWarn2);
      // ver se realmente precisa fazer! Codigo feito em 2017-11-15 e funcionando!
      //VS_PF.VerificaAtzCurtidos(PB1, LaTopWarn1, LaTopWarn2);
      VS_PF.VerificaAtzCalPDA(PB1, LaTopWarn1, LaTopWarn2);
      VS_PF.VerificaAtzCalDTA(PB1, LaTopWarn1, LaTopWarn2);
      //
      VS_PF.VerificaGSP('GSPInnNiv2', emidCompra);
      VS_PF.VerificaGSP('GSPArtNiv2', emidIndsXX);
      //
{    ini tentativa de eliminar TmySQLQuery
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Verificando PQE sem Empresa');
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQE, Dmod.MyDB, [
      'SELECT COUNT(Codigo) Itens',
      'FROM pqe',
      'WHERE Empresa=0',
      '']);
      if QrPQE.FieldByName('Itens').AsInteger > 0 then
      begin
        if Geral.MB_Pergunta('Existem ' +
        Geral.FF0(QrPQE.FieldByName('Itens').AsInteger) +
        ' entradas de insumos sem EMPRESA.' + sLineBreak +
        'Deja corrigi-los agora?') = ID_Yes then
          Dmod.MyDB.Execute('UPDATE pqe SET Empresa=-11 WHERE Empresa=0');
      end;
}
      //
      UMyMod.VerificaFeriadosFuturos(TFmFeriados, FmFeriados, True);
      //
      // Evitar erro catastrofico!
      TmVersao.Enabled := True;
      //
    end;
    MyObjects.Informa2(LaTopWarn1, LaTopWarn2, False, '...');
    if WindowState <> wsMaximized then
      WindowState := wsMaximized;
{
    //......................................................
    if VAR_USUARIO = - 1 then
      Geral.MB_Info('Verificar lote de NFe com mais de uma NFe no ACBr!');
    //......................................................
}
  finally
    if VAR_USUARIO = -1 then
      Geral.MB_Info(
      '[   ] A. Verificar todos forms Application.CreateForm/ afmoLiberado e afmoSemVerificar' +
      '[   ] 1. Ver Lan�amento financeiro de compra!' + sLineBreak +
      '[   ] 2. Ver Lan�amento financeiro de Venda!' + sLineBreak +
      '[   ] 3. Ver Lan�amento financeiro no financeiro!' + sLineBreak +
      '[   ] 4. Ver quita��o Atom�tica!' + sLineBreak +
      '[   ] 5. Ver todos F L A N _ G e n e r o para > FLAN_GenCtbD e FLAN_GenCtb' + sLineBreak +
      '[ x ] 6. Concilia��o banc�ria: FmConcilia: [Removido!]' + sLineBreak +
      '');
    TmSuporte.Enabled := True;
    Screen.Cursor     := crDefault;
  end;
end;

procedure TFmPrincipal.VerificaPQ_GGX();
{
  function CriaPrdGrupTip(): Boolean;
  var
    Codigo: Integer;
  begin
    if Dmod.QrControlePQ_PrdGrup Tip.Value <> 0 then Result := True else
    begin
      Codigo := UMyMod.BuscaEmLivreY_Def('prdgruptip', 'Codigo', stIns, 0);
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
      'CodUsu', 'Nome', 'MadeBy',
      'Fracio', 'Gradeado', 'TipPrd',
      'NivCad', 'FaixaIni', 'FaixaFim',
      'TitNiv1', 'TitNiv2', 'TitNiv3',
      'Customizav'], [
      'Codigo'], [
      Codigo(*CodUsu*), 'Produtos Qu�micos'(*Nome*), 2(*MadeBy*),
      3(*Fracio*), 0(*Gradeado*), 3(*TipPrd*),
      1(*NivCad*), 1001(*FaixaIni*), 9999(*FaixaFim*),
      'Produto'(*TitNiv1*), ''(*TitNiv2*), ''(*TitNiv3*),
      0(*Customizav*)], [
      Codigo], True);
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE controle SET PQ_PrdGrup Tip=' + Geral.FF0(Codigo));
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrControle.Close;
      UnDmkDAC_PF.AbreQuery(Dmod.QrControle, Dmod.MyDB);
    end;
  end;
}
var
(*
  GraGru1, GraGruC, GraTamI, Controle, CodUsu, PrdGrupTip, GraTamCad: Integer;
  Nome: String;
*)
  MudouTabelas: Boolean;
begin
  // Parei aqui 09 10 08 restaurar backup para testar?
  DModG.ConsertaCST_PIS_COFINS();
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT Codigo FROM entidades WHERE Codigo=-11');
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  if Dmod.QrAux.RecordCount = 0 then
  begin
    if Geral.MB_Pergunta('Ser� feito modifica��es na estrutura de dados para a NFe!' +
    sLineBreak + 'Confirma e aceita estas altera��es?') <> ID_YES then Exit;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE entidades SET Codigo=-11, Filial=1 WHERE Codigo=-1;');
    Dmod.QrUpd.SQL.Add('UPDATE entidades SET CodUsu=-11, Filial=1 WHERE CodUsu=-1;');
    Dmod.QrUpd.SQL.Add('UPDATE controle SET Dono=-11;');
    Dmod.QrUpd.SQL.Add('UPDATE lct0001a SET CliInt=-11;');
    Dmod.QrUpd.SQL.Add('UPDATE pqcli SET CI=-11 WHERE CI=-1;');
    Dmod.QrUpd.SQL.Add('UPDATE pqx SET CliOrig=-11 WHERE CliOrig=-1;');
    Dmod.QrUpd.SQL.Add('UPDATE pqx SET CliDest=-11 WHERE CliDest=-1;');
    Dmod.QrUpd.SQL.Add('UPDATE formulas SET ClienteI=-11 WHERE ClienteI=-1;');
    Dmod.QrUpd.SQL.Add('UPDATE tintascab SET ClienteI=-11 WHERE ClienteI=-1;');
    //
    Dmod.QrUpd.SQL.Add('UPDATE entidades SET CodUsu=Codigo WHERE CodUsu=0;');
    Dmod.QrUpd.SQL.Add('DELETE FROM ufs;');
    Dmod.QrUpd.SQL.Add('DROP TABLE IF EXISTS cambios;');
    Dmod.QrUpd.ExecSQL;
    //
    MudouTabelas := True;
  end else MudouTabelas := False;
  //
{  Desabilitei 2011-05-08
  QrPQ_GGX.Close;
  QrPQ_GGX.Database := Dmod.MyDB;
  UnDmkDAC_PF.AbreQuery(QrPQ_GGX, Dmod.MyDB);
  if QrPQ_GGX.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(QrPQ_GGX.RecordCount) +
    ' insumos sem correspond�ncia em grade! Esta correspond�ncia ser� criada!');
    //
    Screen.Cursor := crHourGlass;
    try
      //Criar GraGru1, PrdGrupTip, e setar controle!
      QrPQ_GGX.First;
      while not QrPQ_GGX.Eof do
      begin
        CodUsu := QrPQ_GGXPQ.Value + 1000;
        Nome   := QrPQ_GGXNomePQ.Value;
        PrdGrupTip := -2;//Dmod.QrControlePQ_PrdGrup Tip.Value;
        GraGru1    := QrPq_GGXPQ.Value;
        GraTamCad  := 0;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragru1', False, [
          'Nivel3', 'Nivel2', 'CodUsu', 'Nome', 'PrdGrupTip', 'GraTamCad'
        ], ['Nivel1'], [
          0(*FNivel3*), 0(*FNivel2*), CodUsu, Nome, PrdGrupTip, GraTamCad
        ], [GraGru1], True);

        GraTamI := 1; // �nico
        GraGruC := DmProd.ObtemGraGruCDeIdx(GraGru1, 'PQCli', QrPQ_GGXNOMECLI.Value);
        //Controle := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
        Controle := QrPQ_GGXControle.Value;
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False, [
        'GraGruC', 'GraGru1', 'GraTamI'], [
        'Controle'], [GraGruC, GraGru1, GraTamI], [
        Controle], True) then
          Controle := 0;
        if (GraGru1 = 0) or (GraGruC = 0) or (Controle = 0) then
        begin
          Geral.MB_Erro('N�o foi poss�vel gerar o item de grade ' +
            'para o insumo ' + Geral.FF0(QrPQ_GGXPQ.Value) + ' (Controle ' +
            Geral.FF0(Controle) + ' )!');
          Screen.Cursor := crDefault;
          Exit;
        end;
        //
        QrPQ_GGX.Next;
      end;
      Geral.MB_Info('Atualiza��o realizada com sucesso!');
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  //
  QrPQ_GGX.Close;
  UnDmkDAC_PF.AbreQuery(QrPQ_GGX, Dmod.MyDB);
  DBGPQ_GGX.Visible := QrPQ_GGX.RecordCount > 0;
  //
}
  if MudouTabelas then Halt(0);
end;

procedure TFmPrincipal.VerificaTabelasTerceiros2Click(Sender: TObject);
begin
  ALL_Jan.MostraFormVerifiDBTerceiros(False);
end;

procedure TFmPrincipal.Vrias1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSImpClaMulRMP();
end;

procedure TFmPrincipal.VSxPesagemPQ1Click(Sender: TObject);
begin
  AppPF.MostraFormCfgEstqVSxPQ_EFD(
    2(*ImporExpor*),
    EfdIcmsIpi_PF.AnoMesAnterior(),
    VAR_LIB_EMPRESA_SEL,
    0);
end;

procedure TFmPrincipal.ReabreTabelas1Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirTabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.reaVSm1Click(Sender: TObject);
begin
//
end;

procedure TFmPrincipal.reaVSm21Click(Sender: TObject);
const
  TemIMEIMrt = 0;
var
  Valor: Double;
begin
  Valor := MyVCLRef.GET_ValorDeObjeto_Double();
  VS_PF.PesquisaDoubleVS('AreaM2', Valor, TemIMEIMrt);
end;

procedure TFmPrincipal.RecalcularsaldodeNFs1Click(Sender: TObject);
begin
  //Recalcular saldos das NFs do Acetato insumo 780
  //e colocar ferramentas para verifixar pqx x pqw
  Geral.MB_Info('A desenvolver');
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa ainda!
end;

procedure TFmPrincipal.ReclassificaoMassiva1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPaMulCabR(0);
end;

procedure TFmPrincipal.RecomearclkassificaodeOCjconfigurada1Click(
  Sender: TObject);
begin
  VS_PF.MostraFormVSClaArtSel_IMEI();
end;

procedure TFmPrincipal.RecomearclkassificaodeOCjconfigurada2Click(
  Sender: TObject);
begin
  VS_PF.MostraFormVSClaArtSel_FRMP();
end;

procedure TFmPrincipal.RecriartodasbaixasporNF1Click(Sender: TObject);
begin
  PQ_PF.AjustePQx_Todos_2023();
end;

{
function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
end;
}

procedure TFmPrincipal.Recurtimento1Click(Sender: TObject);
begin
  PQ_PF.MostraFormFormulasImpWE();
end;

procedure TFmPrincipal.Reduzido1Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruN_GGX(MyVCLRef.GET_ValorDeObjeto_Inteiro(), '');
end;

procedure TFmPrincipal.ReduzidosdeBaixaemConflitocomaOrigem1Click(
  Sender: TObject);
const
 Avisa = False;
 ForcaMostrarForm = True;
 SelfCall = False;
begin
  DmModVS.RedConfltBaixa(ForcaMostrarForm, SelfCall, False, nil, nil);
end;

procedure TFmPrincipal.ReduzidosdeGeraodeArtigoemConflito1Click(
  Sender: TObject);
const
 Avisa = False;
 ForcaMostrarForm = True;
 SelfCall = False;
begin
  DmModVS.RedConfltGerA(ForcaMostrarForm, SelfCall, False, nil, nil);
end;

procedure TFmPrincipal.ReduzidosdePrClasseReclasseemConflito1Click(
  Sender: TObject);
const
 //Empresa = 0;
 //GraGruX = 0;
 Avisa = False;
 ForcaMostrarForm = True;
 SelfCall = False;
begin
  DmModVS.SrcGGXDiferentesDePreClasseParaClasseEReclasse(ForcaMostrarForm,
  SelfCall, False, nil, nil);
end;

function TFmPrincipal.CartaoDeFatura: Integer;
begin
  Geral.MB_Aviso('Faturando n�o implementado');
  Result := 0;
end;

procedure TFmPrincipal.Carteiras1Click(Sender: TObject);
begin
  CadastroDeCarteira;
end;

procedure TFmPrincipal.CFOP1Click(Sender: TObject);
begin
  CadastroCFOP2003();
end;

procedure TFmPrincipal.CFOP2Click(Sender: TObject);
begin
  CadastroCFOP2003();
end;

procedure TFmPrincipal.ClasificaoMassiva1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPaMulCabA(0);
end;

procedure TFmPrincipal.ComearnovaOC1Click(Sender: TObject);
var
  Form, Max: Integer;
  Ord_Txt: String;
  Qry: TmySQLQuery;
  Pallets: TClass15Int;
begin
{
  Form := MyObjects.SelRadioGroup('Sele��o de Janela',
  'Selecione a Janela', [
  'Atual',
  'Novo (3)'], 0);
  case Form of
    0: VS_PF.MostraFormVSClaArtPrpMDz(0, 0, 0, 0, 0, 0, nil, 0, 0);
    1:
}
    begin //testar mais!
      VS_PF.LimpaArray15Int(Pallets);
      VS_PF.MostraFormVSClaArtPrpQnz(Pallets, nil, 0, 0, 0);
    end;
{
  end;
}
end;

function TFmPrincipal.CompensacaoDeFatura: String;
begin
  Geral.MB_Aviso('Faturando n�o implementado');
  Result := '';
end;

procedure TFmPrincipal.Componenteativo1Click(Sender: TObject);
begin
  VAR_POPUP_ACTIVE_CONTROL := Screen.ActiveForm.ActiveControl;
  VAR_POPUP_ACTIVE_FORM    := Screen.ActiveForm;
  MyObjects.MostraPopUpNoCentro(VAR_LOC_POPUP);
end;

procedure TFmPrincipal.ComunicaoTelecomunicao1Click(Sender: TObject);
begin
  EfdIcmsIpi_Jan_v03_0_9.MostraFormEfdInnD001Ger();
end;

procedure TFmPrincipal.Conjuntos1Click(Sender: TObject);
begin
  CadastroDeConjuntos();
end;

procedure TFmPrincipal.Consumo1Click(Sender: TObject);
begin
  UnPQx.GerenciaBaixaOutros(0);
end;

procedure TFmPrincipal.ConsumoGrupodeFrmulas1Click(Sender: TObject);
begin
  App_Jan.MostraFormFormulasGruImp();
end;

procedure TFmPrincipal.Contas1Click(Sender: TObject);
begin
  CadastroDeContas;
end;

procedure TFmPrincipal.CorreodecustosporIMECs1Click(Sender: TObject);
const
  Empresa = 0;
  GraGruX = 0;
  Avisa = False;
  ForcaMostrarForm = True;
  SelfCall = False;
  TemIMEiMrt = 0;
  MargemErro = 0.001;
  sMovimIDs = '';
begin
  DmModVS.CorreCustosIMECs(Empresa, GraGruX, 0, MargemErro, 0.0000, False,
   Avisa, ForcaMostrarForm, SelfCall, TemIMEiMrt, sMovimIDs, nil, nil);
end;

procedure TFmPrincipal.Corrigecustoreclasse1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  VS_Jan.MostraFormVSCorrigeCusReclas();
end;

procedure TFmPrincipal.CorrigeGrupodocadastrodeInsumosemMassa1Click(
  Sender: TObject);
begin
  App_Jan.MostraFormPQCorrigeGGXNiv2();
end;

procedure TFmPrincipal.MostraCambioMda;
begin
  if DBCheck.CriaFm(TFmCambioMda, FmCambioMda, afmoNegarComAviso) then
  begin
    FmCambioMda.ShowModal;
    FmCambioMda.Destroy;
  end;
end;

procedure TFmPrincipal.MostraCarteiras(Carteira: Integer);
begin
  // n�o � necess�rio
end;

procedure TFmPrincipal.MostraCFOP2003;
begin
  if DBCheck.CriaFm(TFmCFOP2003, FmCFOP2003, afmoNegarComAviso) then
  begin
    FmCFOP2003.ShowModal;
    FmCFOP2003.Destroy;
  end;
end;

procedure TFmPrincipal.MostraDefeitosloc(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmDefeitosloc, FmDefeitosloc, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmDefeitosloc.LocCod(Codigo, Codigo);
    FmDefeitosloc.ShowModal;
    FmDefeitosloc.Destroy;
  end;
end;

procedure TFmPrincipal.MostraDefeitostip(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmDefeitostip, FmDefeitostip, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmDefeitostip.LocCod(Codigo, Codigo);
    FmDefeitostip.ShowModal;
    FmDefeitostip.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFisRegCad();
begin
  if DBCheck.CriaFm(TFmFisRegCad, FmFisRegCad, afmoNegarComAviso) then
  begin
    FmFisRegCad.ShowModal;
    FmFisRegCad.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormBalancoPQ();
var
  PQBn: Integer;
begin
(*
  PQBn := MyObjects.SelRadioGroup('Balan�o de Insumos',
    'Selecione uma janela:', ['Nova (recomendada)', 'Antiga'], 2, 0);
  if PQBn > -1 then
  begin
    if PQBn = 0 then
    begin

      if DBCheck.CriaFm(TFmPQB3, FmPQB3, afmoNegarComAviso) then
      begin
        FmPQB3.ShowModal;
        FmPQB3.Destroy;
      end;
    end else
    begin
*)
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmStqBalCad, FmStqBalCad, afmoNegarComAviso) then
      begin
        FmStqBalCad.FMultiGrandeza := True;
        FmStqBalCad.ShowModal;
        FmStqBalCad.Destroy;
      end;
    fggxPQ:
}
(*
      if DBCheck.CriaFm(TFmPQB, FmPQB, afmoNegarComAviso) then
      begin
        FmPQB.ShowModal;
        FmPQB.Destroy;
      end;
*)
{
  end;
}
(*
    end;
  end;
*)

////////////////////////////////////////////////////////////////////////////////
{ }
(*
  PQBn := MyObjects.SelRadioGroup('Balan�o de Insumos',
    'Selecione uma janela:', ['Nova (recomendada)', 'Antiga'], 2, 0);
  if PQBn > -1 then
  begin
    if PQBn = 0 then
    begin
*)
      if DBCheck.CriaFm(TFmPQB3, FmPQB3, afmoNegarComAviso) then
      begin
        FmPQB3.ShowModal;
        FmPQB3.Destroy;
      end;
(*
    end else
    begin
    *)(*
      if DBCheck.CriaFm(TFmPQB2, FmPQB2, afmoNegarComAviso) then
      begin
        FmPQB2.ShowModal;
        FmPQB2.Destroy;
      end;
    (*
    end;
  end;
*)
{}
////////////////////////////////////////////////////////////////////////////////
{
  PQBn := MyObjects.SelRadioGroup('Balan�o de Insumos',
    'Selecione uma janela:', ['Nova (recomendada)', 'Antiga'], 2, 0);
  if PQBn > -1 then
  begin
    if PQBn = 0 then
    begin
      if DBCheck.CriaFm(TFmPQB3, FmPQB3, afmoNegarComAviso) then
      begin
        FmPQB3.ShowModal;
        FmPQB3.Destroy;
      end;
    end else
    begin
      if DBCheck.CriaFm(TFmPQB2, FmPQB2, afmoNegarComAviso) then
      begin
        FmPQB2.ShowModal;
        FmPQB2.Destroy;
      end;
    end;
  end;
}
end;

procedure TFmPrincipal.MostraFormVSIndsVS(SQLType: TSQLType; Controle, MPVIts,
  Emit, EmitCus, InsEmpr: Integer; QrWBMovIts: TmySQLQuery;
  FormaWBIndsWE: TFormaWBIndsWE);
begin
;
end;

procedure TFmPrincipal.MostraFormWBAjsCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmWBAjsCab, FmWBAjsCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmWBAjsCab.LocCod(Codigo, Codigo);
      FmWBAjsCab.QrWBAjsIts.Locate('Controle', Controle, []);
    end;
    FmWBAjsCab.ShowModal;
    FmWBAjsCab.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormWBIndsWE(SQLType: TSQLType; Controle, MPVIts,
Emit, EmitCus, InsEmpr: Integer; QrWBMovIts: TmySQLQuery; FormaWBIndsWE:
  TFormaWBIndsWE);
var
  Qry: TmySQLQuery;
  Empresa: Integer;
begin
  if DBCheck.CriaFm(TFmWBIndsWE, FmWBIndsWE, afmoNegarComAviso) then
  begin
    FmWBIndsWE.FFormaWBIndsWE := FormaWBIndsWE;
    //fiwNone, fiwPesagem, fiwBaixaPrevia);
    FmWBIndsWE.ImgTipo.SQLType := SQLType;
    FmWBIndsWE.FQrWBMovIts := QrWBMovIts;
    if SQLType = stIns then
    begin
      FmWBIndsWE.EdCodigo.ValueVariant := MPVIts;
      FmWBIndsWE.EdLnkNivXtr1.ValueVariant := Emit;
      FmWBIndsWE.EdLnkNivXtr2.ValueVariant := EmitCus;
      //
      FmWBIndsWE.EdEmpresa.ValueVariant := InsEmpr;
      FmWBIndsWE.CBEmpresa.KeyValue     := InsEmpr;
    end else
    begin
      Qry := TmySQLQuery.Create(Self);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM wbmovits ',
        'WHERE Controle=' + Geral.FF0(Controle),
        '']);
        if TEstqMovimID(Qry.FieldByName('MovimID').AsInteger) <> emidIndsWE then
        begin
          FmWBIndsWE.Destroy;
          Geral.MB_Erro('ID Item ' + Geral.FF0(Controle) + ' n�o � de baixa for�ada!');
          Exit;
        end;
        //
        //Codigo         :=
        FmWBIndsWE.EdCodigo.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
        //Controle       :=
        FmWBIndsWE.EdControle.ValueVariant := Qry.FieldByName('Controle').AsInteger;
        //MovimCod       :=
        FmWBIndsWE.EdMovimCod.ValueVariant := Qry.FieldByName('MovimCod').AsInteger;
        (*Empresa*)
        Empresa := DModG.ObtemFilialDeEntidade(Qry.FieldByName('Empresa').AsInteger);
        FmWBIndsWE.EdEmpresa.ValueVariant := Empresa;
        FmWBIndsWE.CBEmpresa.KeyValue     := Empresa;
        //Fornecedor     :=
        FmWBIndsWE.EdFornecedor.ValueVariant := Qry.FieldByName('Terceiro').AsInteger;
        FmWBIndsWE.CBFornecedor.KeyValue     := Qry.FieldByName('Terceiro').AsInteger;
        //DataHora       := Geral.FDT(
        FmWBIndsWE.TPDataHora.Date := Qry.FieldByName(CO_DATA_HORA_GRL).AsDateTime;
        FmWBIndsWE.EdDataHora.ValueVariant  := Qry.FieldByName(CO_DATA_HORA_GRL).AsDateTime;
        //MovimID        := emid Baixa;
        //MovimNiv       := emin SemNiv;
        FmWBIndsWE.EdLnkNivXtr1.ValueVariant := Qry.FieldByName('LnkNivXtr1').AsInteger;
        FmWBIndsWE.EdLnkNivXtr2.ValueVariant := Qry.FieldByName('LnkNivXtr2').AsInteger;
        //Pallet         :=
        FmWBIndsWE.EdPallet.ValueVariant := Qry.FieldByName('Pallet').AsInteger;
        FmWBIndsWE.CBPallet.KeyValue     := Qry.FieldByName('Pallet').AsInteger;
        //GraGruX        :=
        FmWBIndsWE.EdGragruX.ValueVariant := Qry.FieldByName('GragruX').AsInteger;
        FmWBIndsWE.CBGragruX.KeyValue     := Qry.FieldByName('GragruX').AsInteger;
        //Pecas          := -
        FmWBIndsWE.EdPecas.ValueVariant  := -Qry.FieldByName('Pecas').AsFloat;
        //PesoKg         := -
        FmWBIndsWE.EdPesoKg.ValueVariant := -Qry.FieldByName('PesoKg').AsFloat;
        //AreaM2         := -
        FmWBIndsWE.EdAreaM2.ValueVariant := -Qry.FieldByName('AreaM2').AsFloat;
        //AreaP2         := -
        FmWBIndsWE.EdAreaP2.ValueVariant := -Qry.FieldByName('AreaP2').AsFloat;
        //ValorT         := -
        FmWBIndsWE.EdValorT.ValueVariant := -Qry.FieldByName('ValorT').AsFloat;
        //Observ         :=
        FmWBIndsWE.EdObserv.Text          := Qry.FieldByName('Observ').AsString;
      finally
        Qry.Free;
      end;
    end;
    FmWBIndsWE.ShowModal;
    FmWBIndsWE.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormWBInnCab(Codigo, WBInnIts, WBReclas: Integer);
begin
  if DBCheck.CriaFm(TFmWBInnCab, FmWBInnCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmWBInnCab.LocCod(Codigo, Codigo);
      FmWBInnCab.QrWBInnIts.Locate('Controle', WBInnIts, []);
      if not FmWBInnCab.QrReclasif.Locate('Controle', WBreclas, []) then
        // tenta o item par
        FmWBInnCab.QrReclasif.Locate('Controle', WBreclas + 1, []);
    end;
    FmWBInnCab.ShowModal;
    FmWBInnCab.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormWBOutCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmWBOutCab, FmWBOutCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmWBOutCab.LocCod(Codigo, Codigo);
      FmWBOutCab.QrWBOutIts.Locate('Controle', Controle, []);
    end;
    FmWBOutCab.ShowModal;
    FmWBOutCab.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormWBOutIts(SQLType: TSQLType; Controle: Integer;
QrCab, QrIts: TmySQLQuery);
var
  Qry: TmySQLQuery;
  Empresa: Integer;
begin
  if DBCheck.CriaFm(TFmWBOutIts, FmWBOutIts, afmoNegarComAviso) then
  begin
    FmWBOutIts.ImgTipo.SQLType := SQLType;
    FmWBOutIts.FQrCab := QrCab;
    //FmWBOutIts.FDsCab := DsCab;
    FmWBOutIts.FQrIts := QrIts;
    if SQLType = stIns then
    begin
      //
    end else
    begin
      Qry := TmySQLQuery.Create(Self);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM wbmovits ',
        'WHERE Controle=' + Geral.FF0(Controle),
        '']);
        if TEstqMovimID(Qry.FieldByName('MovimID').AsInteger) <> emidBaixa then
        begin
          FmWBOutIts.Destroy;
          Geral.MB_Erro('ID Item ' + Geral.FF0(Controle) + ' n�o � de baixa for�ada!');
          Exit;
        end;
        //
        //Codigo         :=
        FmWBOutIts.EdCodigo.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
        //Controle       :=
        FmWBOutIts.EdControle.ValueVariant := Qry.FieldByName('Controle').AsInteger;
        //MovimCod       :=
        FmWBOutIts.EdMovimCod.ValueVariant := Qry.FieldByName('MovimCod').AsInteger;
        (*Empresa*)
        Empresa := DModG.ObtemFilialDeEntidade(Qry.FieldByName('Empresa').AsInteger);
        FmWBOutIts.EdEmpresa.ValueVariant := Empresa;
        FmWBOutIts.CBEmpresa.KeyValue     := Empresa;
        //Fornecedor     :=
        FmWBOutIts.EdFornecedor.ValueVariant := Qry.FieldByName('Terceiro').AsInteger;
        FmWBOutIts.CBFornecedor.KeyValue     := Qry.FieldByName('Terceiro').AsInteger;
        //DataHora       := Geral.FDT(
        FmWBOutIts.TPDataHora.Date := Qry.FieldByName(CO_DATA_HORA_GRL).AsDateTime;
        FmWBOutIts.EdDataHora.ValueVariant  := Qry.FieldByName(CO_DATA_HORA_GRL).AsDateTime;
        //MovimID        := emid Baixa;
        //MovimNiv       := emin SemNiv;
        //Pallet         :=
        FmWBOutIts.EdPallet.ValueVariant := Qry.FieldByName('Pallet').AsInteger;
        FmWBOutIts.CBPallet.KeyValue     := Qry.FieldByName('Pallet').AsInteger;
        //GraGruX        :=
        FmWBOutIts.EdGragruX.ValueVariant := Qry.FieldByName('GragruX').AsInteger;
        FmWBOutIts.CBGragruX.KeyValue     := Qry.FieldByName('GragruX').AsInteger;
        //Pecas          := -
        FmWBOutIts.EdPecas.ValueVariant  := -Qry.FieldByName('Pecas').AsFloat;
        //PesoKg         := -
        FmWBOutIts.EdPesoKg.ValueVariant := -Qry.FieldByName('PesoKg').AsFloat;
        //AreaM2         := -
        FmWBOutIts.EdAreaM2.ValueVariant := -Qry.FieldByName('AreaM2').AsFloat;
        //AreaP2         := -
        FmWBOutIts.EdAreaP2.ValueVariant := -Qry.FieldByName('AreaP2').AsFloat;
        //ValorT         := -
        FmWBOutIts.EdValorT.ValueVariant := -Qry.FieldByName('ValorT').AsFloat;
        //Observ         :=
        FmWBOutIts.EdObserv.Text          := Qry.FieldByName('Observ').AsString;
      finally
        Qry.Free;
      end;
    end;
    FmWBOutIts.ShowModal;
    FmWBOutIts.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormWBPallet();
begin
{
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'wbpallet', 60,
  ncGerlSeq1, 'Cadastro de Pallet',
  [], False, Null, [], [], False);
}
  if DBCheck.CriaFm(TFmWBPallet, FmWBPallet, afmoNegarComAviso) then
  begin
    FmWBPallet.ShowModal;
    FmWBPallet.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormWBRclCab(Codigo, WBRclIts, WBReclas: Integer);
begin
  if DBCheck.CriaFm(TFmWBRclCab, FmWBRclCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmWBRclCab.LocCod(Codigo, Codigo);
      FmWBRclCab.QrWBRclIts.Locate('Controle', WBRclIts, []);
      if not FmWBRclCab.QrReclasif.Locate('Controle', WBreclas, []) then
        // tenta o item par
        FmWBRclCab.QrReclasif.Locate('Controle', WBreclas + 1, []);
    end;
    FmWBRclCab.ShowModal;
    FmWBRclCab.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormWBRclIns(SQLType: TSQLType; Filial, MovimCod,
  MovimTwn, Codigo, Controle: Integer; ValorT, AreaM2: Double;
  QrReopen: TmySQLQuery; SaldoPecas: Double; WBRclass: TWBRclass);
var
  //Filial,
  Empresa, ForneceA, ReduzidoA, GraGruXA: Integer;
  PalletA, SQLIns: String;
  Qry: TmySQLQuery;
  //
  //Tabela: String;
begin
  if (MovimTwn = 0) and (SQLType = stUpd) then
  begin
    Geral.MB_Erro('"MovimTwn" n�o definido!. Altera��o abortada!');
    Exit;
  end;
  Qry := TmySQLQuery.Create(Self);
  try
    if DBCheck.CriaFm(TFmWBRclIns, FmWBRclIns, afmoNegarComAviso) then
    begin
      FmWBRclIns.ImgTipo.SQLType := SQLType;
      FmWBRclIns.FWBRclass := WBRclass;
      FmWBRclIns.FSaldoPecas := SaldoPecas;
      FmWBRclIns.EdMovimTwn.ValueVariant := MovimTwn;
      //
      //
      FmWBRclIns.FValorT := ValorT;
      FmWBRclIns.FAreaM2 := AreaM2;
      if AreaM2 = 0 then
        FmWBRclIns.FPrecoM2 := 0
      else
        FmWBRclIns.FPrecoM2 := ValorT / AreaM2;
      //
      FmWBRclIns.FQrReopen := QrReopen;
      //
      if SQLType = stIns then
        SQLIns := 'AND Controle=' + Geral.FF0(Controle)
      else
        SQLIns := '';
      //

      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM wbmovits ' ,
      'WHERE Codigo=' + Geral.FF0(Codigo),
      SQLIns,
      'AND MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimTwn=' + Geral.FF0(MovimTwn),
      'ORDER BY Controle']);
      //Filial    := Qry.FieldByName('Empresa').AsInteger;
      Empresa   := DModG.ObtemFilialDeEntidade(Filial);
      ForneceA  := Qry.FieldByName('Terceiro').AsInteger;
      GraGruXA  := Qry.FieldByName('GraGruX').AsInteger;
      PalletA   := Qry.FieldByName('Pallet').AsString;
      // Devem ser os primeiros
      FmWBRclIns.EdEmpresa.ValueVariant  := Empresa;
      FmWBRclIns.CBEmpresa.KeyValue      := Empresa;
      //
      FmWBRclIns.EdForneceA.ValueVariant := ForneceA;
      FmWBRclIns.CBForneceA.KeyValue     := ForneceA;
      FmWBRclIns.EdGraGruXA.ValueVariant := GraGruXA;
      FmWBRclIns.CBGraGruXA.KeyValue     := GraGruXA;
      //
      // Devem ser os �ltimos pois dependem dos outros
      FmWBRclIns.EdPalletA.ValueVariant  := PalletA;
      FmWBRclIns.CBPalletA.KeyValue      := PalletA;
      //
      FmWBRclIns.EdEmpresa.Enabled  := False;
      FmWBRclIns.CBEmpresa.Enabled  := False;
      //TWBRclass = (wbrEntrada, wbrEstoque);
      if WBRclass <> wbrEstoque then
      begin
        FmWBRclIns.EdForneceA.Enabled := False;
        FmWBRclIns.CBForneceA.Enabled := False;
        FmWBRclIns.EdGraGruXA.Enabled := False;
        FmWBRclIns.CBGraGruXA.Enabled := False;
        FmWBRclIns.EdPalletA.Enabled  := False;
        FmWBRclIns.CBPalletA.Enabled  := False;
      end;
      FmWBRclIns.EdForneceB.Enabled := False;
      FmWBRclIns.CBForneceB.Enabled := False;
      //
      FmWBRclIns.EdForneceB.ValueVariant := ForneceA;
      FmWBRclIns.CBForneceB.KeyValue     := ForneceA;
      //FmWBRclIns.EdGraGruXB.ValueVariant := GraGruXA;
      //FmWBRclIns.CBGraGruXB.KeyValue     := GraGruXA;
      //
      if SQLType = stIns then
      begin
        FmWBRclIns.FSrcMovID  := MovimCod;
        FmWBRclIns.FSrcNivel1 := Codigo;
        FmWBRclIns.FSrcNivel2 := Controle;
      end else
      begin
        FmWBRclIns.FSrcMovID  := Qry.FieldByName('SrcMovID').AsInteger;
        FmWBRclIns.FSrcNivel1 := Qry.FieldByName('SrcNivel1').AsInteger;
        FmWBRclIns.FSrcNivel2 := Qry.FieldByName('SrcNivel2').AsInteger;
        //
        FmWBRclIns.TPData.Date := Qry.FieldByName(CO_DATA_HORA_GRL).AsDateTime;
        FmWBRclIns.EdHora.ValueVariant := Qry.FieldByName(CO_DATA_HORA_GRL).AsDateTime;
        FmWBRclIns.EdCodigo.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
        FmWBRclIns.EdMovimCod.ValueVariant := Qry.FieldByName('MovimCod').AsInteger;
        //
        FmWBRclIns.EdControleA.ValueVariant := Qry.FieldByName('Controle').AsInteger;
        FmWBRclIns.EdPecasA.ValueVariant := -Qry.FieldByName('Pecas').AsFloat;
        FmWBRclIns.EdPesoKgA.ValueVariant := -Qry.FieldByName('PesoKg').AsFloat;
        FmWBRclIns.EdAreaM2A.ValueVariant := -Qry.FieldByName('AreaM2').AsFloat;
        FmWBRclIns.EdAreaP2A.ValueVariant := -Qry.FieldByName('AreaP2').AsFloat;
        FmWBRclIns.EdValorTA.ValueVariant := -Qry.FieldByName('ValorT').AsFloat;
        //
        Qry.Next;
        //
        FmWBRclIns.EdControleB.ValueVariant := Qry.FieldByName('Controle').AsInteger;
        FmWBRclIns.EdGraGruXB.ValueVariant  := Qry.FieldByName('GraGruX').AsInteger;
        FmWBRclIns.CBGraGruXB.KeyValue      := Qry.FieldByName('GraGruX').AsInteger;
        FmWBRclIns.EdPecasB.ValueVariant    := Qry.FieldByName('Pecas').AsFloat;
        FmWBRclIns.EdPesoKgB.ValueVariant   := Qry.FieldByName('PesoKg').AsFloat;
        FmWBRclIns.EdAreaM2B.ValueVariant   := Qry.FieldByName('AreaM2').AsFloat;
        FmWBRclIns.EdAreaP2B.ValueVariant   := Qry.FieldByName('AreaP2').AsFloat;
        FmWBRclIns.EdValorTB.ValueVariant   := Qry.FieldByName('ValorT').AsFloat;
        // Devem ser os �ltimos pois dependem dos outros
        FmWBRclIns.EdPalletB.ValueVariant   := Qry.FieldByName('Pallet').AsInteger;
        FmWBRclIns.CBPalletB.KeyValue       := Qry.FieldByName('Pallet').AsInteger;
      end;
      FmWBRclIns.ShowModal;
      FmWBRclIns.Destroy;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal.MostraGeosite;
begin
  if DBCheck.CriaFm(TFmGeosite, FmGeosite, afmoNegarComAviso) then
  begin
    FmGeosite.ShowModal;
    FmGeosite.Destroy;
  end;
end;

procedure TFmPrincipal.MostraModelosImp;
begin
  if DBCheck.CriaFm(TFmImprime, FmImprime, afmoNegarComAviso) then
  begin
    FmImprime.ShowModal;
    FmImprime.Destroy;
  end;
end;

procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  Application.CreateForm(TFmCalcPercent, FmCalcPercent);
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;

procedure TFmPrincipal.Relatrios1Click(Sender: TObject);
begin
  PedidosRelatorios;
end;

procedure TFmPrincipal.PQCadastro1Click(Sender: TObject);
begin
  CadastroPQ(0);
end;

procedure TFmPrincipal.PQporNF1Click(Sender: TObject);
begin
  App_Jan.MostraFormPQporNF();
end;

procedure TFmPrincipal.VerificaBD;
begin
  ALL_Jan.MostraFormVerifiDB(False);
end;

procedure TFmPrincipal.VerificaBDServidor2Click(Sender: TObject);
begin
  VerificaBD();
end;

procedure TFmPrincipal.VerificaMP_GGX;
begin
{ DESABILITEI EM 2001-05-21 - esta causando erros?
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM gragrux');
  Dmod.QrUpd.SQL.Add('WHERE Controle < -100');
  Dmod.QrUpd.SQL.Add('AND (GraGruC = 0');
  Dmod.QrUpd.SQL.Add('OR GraTamI = 0)');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM gragru1');
  Dmod.QrUpd.SQL.Add('WHERE Nivel1 < -100');
  Dmod.QrUpd.SQL.Add('AND GraTamCad = -1');
  Dmod.QrUpd.ExecSQL;
}
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
var
  Versao: Int64;
  Arq: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'BlueDerm',
    'BlueDerm', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO,
    CO_DMKID_APP, DModG.ObtemAgora(), Memo3, dtExec, Versao, Arq, False,
    ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal.PedidosRelatorios;
begin
  if DBCheck.CriaFm(TFmMPPImp, FmMPPImp, afmoNegarComAviso) then
  begin
    FmMPPImp.ShowModal;
    FmMPPImp.Destroy;
  end;
end;

procedure TFmPrincipal.Perdasdecouros1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRolPerdas, FmRolPerdas, afmoNegarComAviso) then
  begin
    FmRolPerdas.ShowModal;
    FmRolPerdas.Destroy;
  end;
end;

procedure TFmPrincipal.PesoVSkg1Click(Sender: TObject);
const
  TemIMEIMrt = 0;
var
  Valor: Double;
begin
  Valor := MyVCLRef.GET_ValorDeObjeto_Double();
  VS_PF.PesquisaDoubleVS('PesoKg', Valor, TemIMEIMrt);
end;

procedure TFmPrincipal.Planos1Click(Sender: TObject);
begin
  CadastroDePlano();
end;

procedure TFmPrincipal.PorMarca1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSGerArtMarcaImpBar();
end;

procedure TFmPrincipal.Veculos2Click(Sender: TObject);
begin
  App_Jan.MostraFormVeiculos(0);
end;

procedure TFmPrincipal.StatusPedido(Controle: Integer; DataAtual: TDateTime);
begin
  VAR_DATARESULT := DataAtual;
  MyObjects.Senha(TFmSenha, FmSenha, 0, '', '', '', '', '', True,
    'Data do t�rmino', Date, 'ItemPedidoOK');
  if VAR_SENHARESULT > -1 then 
  begin
    Screen.Cursor := crHourGlass;
    try
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE mpvits SET Pronto=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[0].AsString  := Geral.FDT(VAR_DATARESULT, 1);
      Dmod.QrUpd.Params[1].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPrincipal.Subgrupos1Click(Sender: TObject);
begin
  CadastroDeSubgrupos;
end;

procedure TFmPrincipal.SubstratosdeRendimento1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'EmitSbtCad', 60, ncGerlSeq1,
  'Cadastro de Substratos de Rendimento',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.SairdoERP1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.SaldoDeContas;
begin
  if DBCheck.CriaFm(TFmContasHistSdo3, FmContasHistSdo3, afmoNegarComAviso) then
  begin
    FmContasHistSdo3.ShowModal;
    FmContasHistSdo3.Destroy;
  end;
end;

procedure TFmPrincipal.SaldosPositivos1Click(Sender: TObject);
begin
  PQ_PF.MostraFormPQwPQEItsPos(False);
end;

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

procedure TFmPrincipal.SbAtualizaERPClick(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.SbBackupClick(Sender: TObject);
begin
  MostraBackup;
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.Padro1Click(Sender: TObject);
begin
  // Esse � antigo!!!
  ImgPrincipal.Picture := nil;
  Geral.WriteAppKey('ImagemFundo', Application.Title, '', ktString, HKEY_LOCAL_MACHINE);
  { se for copiar para projeto novo, usar este:
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso,
    PageControl1, True);
  }
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKey('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  SkinMenu(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.PageControl1Enter(Sender: TObject);
begin
(*
  if Dmod.QrOpcoesBugsATBAutoExp.Value = 1 then
  begin
    AdvToolBarPager2.Collaps;
    AdvToolBarPagerNovo
  end;
*)
end;

procedure TFmPrincipal.PageControl1MouseEnter(Sender: TObject);
begin
(*
  if Dmod.QrOpcoesBugsATBAutoExp.Value = 1 then
  begin
    AdvToolBarPager2.Collaps;
    AdvToolBarPagerNovo
  end;
*)
end;

procedure TFmPrincipal.PageControl1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbRight) then
  begin
    //Geral.MB_Aviso('X=' + Geral.FF0(X) + ' Y=' + Geral.FF0(Y));
    //Geral.MB_Aviso('Qtd Compos: ' + Geral.FF0(PageControl1.ActivePage.ComponentCount));
    if PageControl1.ActivePage.ComponentCount = 0 then
    begin
      if Geral.MB_Pergunta('Deseja excluir a aba "' +
        PageControl1.ActivePage.Caption + '"?') = ID_YES
      then
        PageControl1.ActivePage.Free;
    end;
  end;
end;

procedure TFmPrincipal.PCPFluxos1Click(Sender: TObject);
begin
  App_Jan.MostraFormFluxPcpCab(0);
end;

procedure TFmPrincipal.PeaspositivasnegativasXValortotalnegativopositivos1Click(
  Sender: TObject);
const
  Avisa = False;
  ForcaMostrarForm = True;
  SelfCall = False;
  TemIMEIMrt = 0;
  LaAviso1 = nil;
  LaAviso2 = nil;
  Empresa = 0;
  GraGruX = 0;
  MargemErro = 0.00;
begin
  DmModVS.VSPcPosNegXValTNegPos(Empresa, GraGruX, TemIMEIMrt, MargemErro,
    Avisa, ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2);
end;

procedure TFmPrincipal.PedidosGerencia(Direto: Boolean);
begin
  if DBCheck.CriaFm(TFmMPP, FmMPP, afmoNegarComAviso) then
  begin
    FmMPP.FDireto := Direto;
    FmMPP.ShowModal;
    FmMPP.Destroy;
  end;
end;

procedure TFmPrincipal.Geral1Click(Sender: TObject);
begin
  App_Jan.MostraFormOSsAbertaMPV(False);
end;

procedure TFmPrincipal.Geral2Click(Sender: TObject);
begin
  App_Jan.MostraFormOSsAbertaMPV(False);
end;

procedure TFmPrincipal.Geral3Click(Sender: TObject);
begin
(*
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if DBCheck.CriaFm(TFmSintegra_Arq, FmSintegra_Arq, afmoNegarComAviso) then
  begin
    FmSintegra_Arq.ShowModal;
    FmSintegra_Arq.Destroy;
  end;
*)
end;

procedure TFmPrincipal.Geraoxestoque1Click(Sender: TObject);
begin
  AppPF.MostraFormCfgEstqGeradoEFD(
    2(*ImporExpor*),
    EfdIcmsIpi_PF.AnoMesAnterior(),
    VAR_LIB_EMPRESA_SEL,
    0);
end;

procedure TFmPrincipal.Gerencia1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPReclasGer, FmMPReclasGer, afmoNegarComAviso) then
  begin
    FmMPReclasGer.ShowModal;
    FmMPReclasGer.Destroy;
  end;
end;

procedure TFmPrincipal.Gerencia2Click(Sender: TObject);
begin
  //VendasGerencia(False, False);
  App_Jan.VendasGerencia(TMPVAcao.mpvaNenhuma);
end;

procedure TFmPrincipal.Gerencia3Click(Sender: TObject);
begin
  PQ_PF.MostraFormPQwPQEItsNeg(False);
end;

procedure TFmPrincipal.Gerenciamento1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSGerRclCab(0, 0);
end;

procedure TFmPrincipal.Gerenciamento2Click(Sender: TObject);
begin
  VS_PF.MostraFormVSGerClaCab(0);
end;

procedure TFmPrincipal.GerenciamentodePrReclasse1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPrePalCab(0);
end;

procedure TFmPrincipal.Gerenciar2Click(Sender: TObject);
begin
  PedidosGerencia(False);
end;

procedure TFmPrincipal.GerenciarWebServices1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeWebservicesGer();
end;

procedure TFmPrincipal.Grupos1Click(Sender: TObject);
begin
  CadastroDeGrupos;
end;

procedure TFmPrincipal.Gruposdereceitas1Click(Sender: TObject);
begin
  App_Jan.MostraFormFormulasGru();
end;

procedure TFmPrincipal.Gruposdeusurios1Click(Sender: TObject);
begin
  App_Jan.MostraFormGruUsrEdtCab();
end;

procedure TFmPrincipal.Gruposqumicos1Click(Sender: TObject);
begin
  CadastroGruposQuimicos();
end;

procedure TFmPrincipal.CadastrodeEspessuras();
begin
  if DBCheck.CriaFm(TFmEspessuras, FmEspessuras, afmoNegarComAviso) then
  begin
    FmEspessuras.ShowModal;
    FmEspessuras.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeSetores(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmListaSetores, FmListaSetores, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmListaSetores.LocCod(Codigo, Codigo);
    FmListaSetores.ShowModal;
    FmListaSetores.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeCambio();
begin
  if DBCheck.CriaFm(TFmCambioCot, FmCambioCot, afmoNegarComAviso) then
  begin
    FmCambioCot.ShowModal;
    FmCambioCot.Destroy;
  end;
end;

function TFmPrincipal.ObtemUnidadeTxt(Unidade: Integer): String;
begin
  case Unidade of
    0: Result := 'kg';
    1: Result := 'pe�a';
    2: Result := 'm�';
    else Result := '???';
  end;
end;

procedure TFmPrincipal.Operacoes1Click(Sender: TObject);
begin
  VS_PF.MostraFormOperacoes(0);
end;

procedure TFmPrincipal.Operaes1Click(Sender: TObject);
begin
  VS_PF.MostraFormOperacoes(0);
end;

procedure TFmPrincipal.Opes1Click(Sender: TObject);
begin
  //CadastroOpcoes();
  Entities.MostraFormOpcoes();
end;

procedure TFmPrincipal.Opesespecficas1Click(Sender: TObject);
begin
  App_Jan.CadastroOpcoesBlueDerm();
end;

procedure TFmPrincipal.OrigemAmbos1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSGerArtDdImpAll();
end;

procedure TFmPrincipal.OrigemBarraca1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSGerArtDdImpBar();
end;

procedure TFmPrincipal.OrigemCurtimento1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSGerArtDdImpCur();
end;

procedure TFmPrincipal.Outrasbaixasxestoque1Click(Sender: TObject);
begin
  AppPF.MostraFormCfgEstqOthersEFD(
    2(*ImporExpor*),
    EfdIcmsIpi_PF.AnoMesAnterior(),
    VAR_LIB_EMPRESA_SEL, 0);
end;

procedure TFmPrincipal.Outros2Click(Sender: TObject);
begin
  MostraModelosImp;
end;

function TFmPrincipal.ObtemMoedaTxt(Moeda: Integer): String;
begin
  case Moeda of
    0: Result := 'R$';
    1: Result := 'U$';
    2: Result := '�$';
    3: Result := 'IDX';
    else Result := '???';
  end;
end;

function TFmPrincipal.ObtemPeriodoTxt(Periodo: Integer): String;
begin
  case Periodo of
    0: Result := 'Di�rio';
    1: Result := 'Semanal';
    2: Result := 'Decendial';
    3: Result := 'Quinzenal';
    4: Result := 'Mensal';
    else Result := '???';
  end;
end;

function TFmPrincipal.AvisaFaltaDeContaDoPlano(Item: Integer): Boolean;
var
  Descricao: String;
  Conta: Integer;
begin
  // Permitir sempre
  Result := False;
  //
  case Item of
     VAR_FATID_1001 : Conta := Dmod.QrControleCtaPQCompr.Value;
     VAR_FATID_1002 : Conta := Dmod.QrControleCtaPQFrete.Value;
     VAR_FATID_1003 : Conta := Dmod.QrControleCtaMPCompr.Value;
     VAR_FATID_1004 : Conta := Dmod.QrControleCtaMPFrete.Value;
     VAR_FATID_1007 : Conta := Dmod.QrControleCtaCVVenda.Value;
     VAR_FATID_1008 : Conta := Dmod.QrControleCtaCVFrete.Value;
     //VAR_FATID_1009 : Conta := Dmod.QrControleCta.Value;
     VAR_FATID_1010 : Conta := Dmod.QrControleCtaComisCMP.Value;
     else Conta := 0;
  end;
  if Conta = 0 then
  begin
    //Result := True;
    case Item of
      VAR_FATID_1001 : Descricao := VAR_MSG_INSUMOQUIMICO;
      VAR_FATID_1002 : Descricao := VAR_MSG_FRETEPQ;
      VAR_FATID_1003 : Descricao := VAR_MSG_MATERIAPRIMA;
      VAR_FATID_1004 : Descricao := VAR_MSG_FRETEMP;
      VAR_FATID_1005 : Descricao := VAR_MSG_INSUMOQUIMICOPS;
      VAR_FATID_1006 : Descricao := VAR_MSG_FRETEPQPS;
      VAR_FATID_1007 : Descricao := 'Venda de Couro Verde';
      VAR_FATID_1008 : Descricao := 'Frete de venda de couro verde';
      //VAR_FATID_1009 : Descricao := 'Venda';
      VAR_FATID_1010 : Descricao := 'Pagamento de comiss�o na compra de MP';
      else Descricao := '??';
    end;
    if Geral.MB_Pergunta('� necess�rio cadastrar a conta do plano ' +
      'de contas em "Ferramentas > Op��es > Espec�ficos" para a conta"' +
      Descricao + '". Deseja faz�-lo agora?') = ID_YES then
        App_Jan.CadastroOpcoesBlueDerm();
  end //else Result := False;
end;

procedure TFmPrincipal.Button10Click(Sender: TObject);
var
  GGXSubstituto, NivelExclusao: Integer;
begin
  DmProd.ObtemNovoGraGruXdeExcluido(EdGGXExluido.ValueVariant,
  GGXSubstituto, NivelExclusao);
  EdGGXSubstituto.ValueVariant := GGXSubstituto;
  EdNivelExclusao.ValueVariant := NivelExclusao;
  GGXSubstituto := EdGGXExluido.ValueVariant div 100;
  NivelExclusao := EdGGXExluido.ValueVariant mod 100;
  Geral.MB_Aviso(FormatFloat('0', GGXSubstituto) + '-' + FormatFloat('0', NivelExclusao));
end;

procedure TFmPrincipal.Button11Click(Sender: TObject);
//var
  //Teste: LongWord;
begin
  //Teste := dmkPF.TestePorta();
  //Geral.MB_Aviso(Geral.FF0(Teste));
end;

procedure TFmPrincipal.Button1Click(Sender: TObject);
begin
  {
  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE mpin mpi, mpinits its');
  Dmod.QrUpdM.SQL.Add('SET its.Fornece=mpi.Procedencia');
  Dmod.QrUpdM.SQL.Add('WHERE its.Controle=mpi.Controle');
  Dmod.QrUpdM.SQL.Add('AND its.Fornece=0');
  Dmod.QrUpdM.ExecSQL;
  }
  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE mpinits its');
  Dmod.QrUpdM.SQL.Add('SET its.PNF_Fat=its.PNF');
  Dmod.QrUpdM.SQL.Add('WHERE its.PNF_Fat=0');
  Dmod.QrUpdM.ExecSQL;
end;

procedure TFmPrincipal.Button2Click(Sender: TObject);
const
  Endereco = 'R. DO NASCENTE, 679';
var
  Numero, Compl, Bairro, xLogr, nLogr, xRua: String;
begin
  Geral.SeparaEndereco(0, Endereco, Numero, Compl, Bairro, xLogr, nLogr, xRua);
  Geral.MB_Info(
  'Numero: ' + Numero + sLineBreak +
  'Compl: ' + Compl + sLineBreak +
  'Bairro: ' + Bairro + sLineBreak +
  'xLogr: ' + xLogr + sLineBreak +
  'nLogr: ' + nLogr + sLineBreak +
  'xRua: ' + xRua + sLineBreak + Endereco);
end;

procedure TFmPrincipal.Button3Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEMail_SSL, FmEMail_SSL, afmoNegarComAviso) then
  begin
    FmEMail_SSL.ShowModal;
    FmEMail_SSL.Destroy;
  end;
end;

procedure TFmPrincipal.Button4Click(Sender: TObject);
var
  I: Integer;
begin
{    ini tentativa de eliminar TmySQLQuery
  QrParamsEmp.Close;
  QrParamsEmp.SQL.Clear;
  QrParamsEmp.SQL.Text :=
  'SELECT * ' +
  'FROM paramsemp '+
  '';
  QrParamsEmp. O p e n;
    fim tentativa de eliminar TmySQLQuery}

(*
        I := System.TypInfo.GetEnumValue(TypeInfo(TAbc), 'abcSomeOption');
        I := System.Classes.IdentToInt(
*)

end;

procedure TFmPrincipal.Button5Click(Sender: TObject);
begin
  Geral.MB_Info(FormatFloat('#,###,###,##0.0000000', CO_VERSAO mod 10000));
end;

procedure TFmPrincipal.Button6Click(Sender: TObject);
var
  Texto: WideString;
  I : Integer;
begin
  Texto := dmkPF.LoadFileToText(Edit1.Text);
  for I := 1 to Length(Texto) do
    if Ord(Texto[I]) < 32 then
      Geral.MB_Aviso(Geral.FF0(I) + ' de ' + Geral.FF0(Length(Texto)) +
        sLineBreak + 'Caracter = ' + Geral.FF0(Ord(Texto[I])));
end;

procedure TFmPrincipal.Button7Click(Sender: TObject);
var
  ICMS_vCredICMSSN, ICMS_pCredSN, ICMS_VBC: Double;
begin
  ICMS_pCredSN := EdICMS_Alq.ValueVariant;
  ICMS_VBC     := EdICMS_BC.ValueVariant;
  ICMS_vCredICMSSN := Geral.RoundC(ICMS_pCredSN * ICMS_VBC / 100, 2);
  EdICMS_Val.ValueVariant := ICMS_vCredICMSSN;
end;

procedure TFmPrincipal.Button8Click(Sender: TObject);
begin
{
  while not QrFlds.Eof do
  begin
    //? := UMyMod.BuscaEmLivreY_Def('spedefdicmsipiflds', ''Bloco', 'Registro', 'Numero', 'VersaoIni', 'VersaoFim', ImgTipo.SQLType, CodAtual);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipiflds', False, [
'Campo', 'Tipo', 'Tam',
'TObrig', 'Decimais', 'CObrig',
'EObrig', 'SObrig', 'DescrLin1',
'DescrLin2', 'DescrLin3', 'DescrLin4',
'DescrLin5', 'DescrLin6', 'DescrLin7',
'DescrLin8'], [
'Bloco', 'Registro', 'Numero', 'VersaoIni', 'VersaoFim'], [
Campo, Tipo, Tam,
TObrig, Decimais, CObrig,
EObrig, SObrig, DescrLin1,
DescrLin2, DescrLin3, DescrLin4,
DescrLin5, DescrLin6, DescrLin7,
DescrLin8], [
Bloco, Registro, Numero, VersaoIni, VersaoFim], UserDataAlterweb?, IGNORE?
    //
    QrFlds.Next;
  end;
}
end;

procedure TFmPrincipal.Button9Click(Sender: TObject);
begin
{    ini tentativa de eliminar TmySQLQuery
  QrFlds.Close;
  UnDmkDAC_PF.AbreQuery(QrFlds, Dmod.MyDB);
}
end;

procedure TFmPrincipal.CadastroDeMPEstagios();
begin
  if DBCheck.CriaFm(TFmMPEstagios, FmMPEstagios, afmoNegarComAviso) then
  begin
    FmMPEstagios.ShowModal;
    FmMPEstagios.Destroy;
  end;
end;

procedure TFmPrincipal.UnidadesdeMedida1Click(Sender: TObject);
begin
  CadastroDeDefPecas();
end;

procedure TFmPrincipal.Unidadesdemedida2Click(Sender: TObject);
begin
  CadastroDeDefPecas();
end;

procedure TFmPrincipal.Usoeconsumo1Click(Sender: TObject);
begin
  Grade_Jan.CriaFormEntradaCab(tnfUsoEConsumo, True, 0);
end;

procedure TFmPrincipal.UsoeconsumoSPED1Click(Sender: TObject);
begin
  EfdIcmsIpi_Jan_v03_0_9.MostraFormEFD_C001_v03_0_9();
end;

procedure TFmPrincipal.CadastroDeDefPecas();
begin
  if DBCheck.CriaFm(TFmDefPecas, FmDefPecas, afmoNegarComAviso) then
  begin
    FmDefPecas.ShowModal;
    FmDefPecas.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroBancos;
begin
  if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
  begin
    FmBancos.ShowModal;
    FmBancos.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroCFOP2003;
begin
  if DBCheck.CriaFm(TFmCFOP2003, FmCFOP2003, afmoNegarComAviso) then
  begin
    FmCFOP2003.ShowModal;
    FmCFOP2003.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeArtigosGrupos();
begin
  if DBCheck.CriaFm(TFmArtigosGrupos, FmArtigosGrupos, afmoNegarComAviso) then
  begin
    FmArtigosGrupos.ShowModal;
    FmArtigosGrupos.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeCarteiras(TipoCarteira, ItemCarteira, x: Integer);
begin
  VAR_CARTEIRA := TipoCarteira;
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    if x <> 0 then
      FmCarteiras.LocCod(x, x);
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeContas;
begin
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.ShowModal;
    FmContas.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeSubGrupos;
begin
  if DBCheck.CriaFm(TFmSubgrupos, FmSubgrupos, afmoNegarComAviso) then
  begin
    FmSubgrupos.ShowModal;
    FmSubgrupos.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroEntiMP;
begin
  if DBCheck.CriaFm(TFmEntiMP, FmEntiMP, afmoNegarComAviso) then
  begin
    FmEntiMP.ShowModal;
    FmEntiMP.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroEquipes;
begin
  if DBCheck.CriaFm(TFmEquipes, FmEquipes, afmoNegarComAviso) then
  begin
    FmEquipes.ShowModal;
    FmEquipes.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroFornecedorMP;
begin
  if DBCheck.CriaFm(TFmEntiMP, FmEntiMP, afmoNegarComAviso) then
  begin
    FmEntiMP.ShowModal;
    FmEntiMP.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroGruposQuimicos;
begin
  if DBCheck.CriaFm(TFmGruposQuimicos, FmGruposQuimicos, afmoNegarComAviso) then
  begin
    FmGruposQuimicos.ShowModal;
    FmGruposQuimicos.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroMatriz;
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoAdmin) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroMPIn;
begin
  if DBCheck.CriaFm(TFmMPIn, FmMPIn, afmoNegarComAviso) then
  begin
    FmMPIn.ShowModal;
    FmMPIn.Destroy;
  end;
end;

{
procedure TFmPrincipal.CadastroOpcoes;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoSoMaster) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;
}

procedure TFmPrincipal.CadastroPallets;
begin
  if DBCheck.CriaFm(TFmPallets, FmPallets, afmoNegarComAviso) then
  begin
    FmPallets.ShowModal;
    FmPallets.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroPortSerBal;
begin
  if DBCheck.CriaFm(TFmPortSerBal, FmPortSerBal, afmoNegarComAviso) then
  begin
    FmPortSerBal.ShowModal;
    FmPortSerBal.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroPQ(Codigo: Integer);
begin
  AppPF.CadastroInsumo(0, '', Codigo);
end;

procedure TFmPrincipal.CadastroPQImp(FormOrig: String);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
    begin
      if LowerCase(FormOrig) = 'fmpq' then
        MyObjects.MostraPopUpDeBotao(PMRelatUC, FmPQ.SbImprime)
      else if LowerCase(FormOrig) = 'fmpqb' then
        MyObjects.MostraPopUpDeBotao(PMRelatUC, FmPQB.SbImprime)
      else
        MyObjects.MostraPopUpDeBotao(PMRelatUC, AGBRelatUC);
    end;
    fggxPQ:
    begin
}
      if DBCheck.CriaFm(TFmPQImp, FmPQImp, afmoNegarComAviso) then
      begin
        FmPQImp.ShowModal;
        FmPQImp.Destroy;
      end;
{
    end;
  end;
}
end;

procedure TFmPrincipal.CadastroTintas(Numero, Codigo, Controle: Integer);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmTintasCab2, FmTintasCab2, afmoNegarComAviso) then
      begin
        FmTintasCab2.ShowModal;
        FmTintasCab2.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmTintasCab, FmTintasCab, afmoNegarComAviso) then
      begin
        if Numero <> 0 then
        begin
          FmTintasCab.LocCod(Numero, Numero);
          FmTintasCab.QrTintasTin.Locate('Codigo', Codigo, []);
          FmTintasCab.QrTintasIts.Locate('Controle', Controle, []);
        end;
        FmTintasCab.ShowModal;
        FmTintasCab.Destroy;
      end;
{
  end;
}
end;

procedure TFmPrincipal.Caleiro1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSCalCab(0);
end;

procedure TFmPrincipal.Caleirocurtimento1Click(Sender: TObject);
begin
  PQ_PF.MostraFormFormulasImpBH();
end;

procedure TFmPrincipal.CamposdaNFe1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeNewVer();
end;

procedure TFmPrincipal.CadastroDeGrupos;
begin
  if DBCheck.CriaFm(TFmGrupos, FmGrupos, afmoNegarComAviso) then
  begin
    FmGrupos.ShowModal;
    FmGrupos.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeCarteira;
begin
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
  end;
end;

procedure TFmPrincipal.CriaImpressaoDiversos(Indice: Integer);
begin
  {
  if DBCheck.CriaFm(TFmFormularios, FmFormularios, afmoNegarComAviso) then
  begin
    FmFormularios.ShowModal;
    FmFormularios.Destroy;
  end;
  }
end;

procedure TFmPrincipal.CTes1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMOEnvCTeGer(0, 0, 0, 0);
end;

procedure TFmPrincipal.Curtimento1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSCurCab(0);
end;

procedure TFmPrincipal.Custosdivergentesdeprreclasse1Click(Sender: TObject);
const
  Empresa = 0;
  GraGruX = 0;
  Avisa = False;
  ForcaMostrarForm = True;
  SelfCall = False;
  TemIMEiMrt = 0;
  MargemErro = 0.001;
begin
  DmModVS.CustosDifPreRecl(Empresa, GraGruX, MargemErro,
    Avisa, ForcaMostrarForm, SelfCall, TemIMEiMrt, nil, nil);
end;

procedure TFmPrincipal.Custosdivergentesdereclasse1Click(Sender: TObject);
const
  Empresa = 0;
  GraGruX = 0;
  Avisa = False;
  ForcaMostrarForm = True;
  SelfCall = False;
  TemIMEiMrt = 0;
  MargemErro = 0.001;
begin
  DmModVS.CustosDifReclasse(Empresa, GraGruX, MargemErro,
   Avisa, ForcaMostrarForm, SelfCall, TemIMEiMrt, nil, nil);
end;

procedure TFmPrincipal.CustoSimuladodeArtigo1Click(Sender: TObject);
begin
  App_Jan.MostraFormFrmlRibCusCab();
end;

procedure TFmPrincipal.D100Frete07088B0910112627571Click(Sender: TObject);
begin
  EfdIcmsIpi_Jan.MostraFormEFD_D001();
end;

procedure TFmPrincipal.DefineVarsCliInt(CliInt: Integer);
begin
  DmodG.QrCliIntUni.Close;
  DmodG.QrCliIntUni.Params[0].AsInteger := CliInt;
  UnDmkDAC_PF.AbreQuery(DmodG.QrCliIntUni, Dmod.MyDB);
  //
  FEntInt := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntUniCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
end;

procedure TFmPrincipal.Devolues1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCMPTOut, FmCMPTOut, afmoNegarComAviso) then
  begin
    DmPediVda.ReopenTabelasPedido(); // ?? N�o precisa todas. Mudar?
    FmCMPTOut.ShowModal;
    FmCMPTOut.Destroy;
  end;
end;

procedure TFmPrincipal.DevoluoDefinitivaAoEstoque1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSDvlCab(0);
end;

procedure TFmPrincipal.Devoluonovo1Click(Sender: TObject);
begin
  UnPQx.GerenciaBaixaDevolucao(0);
end;

procedure TFmPrincipal.Divergnciadecustodesaldovirtual1Click(Sender: TObject);
const
 Empresa = 0;
 GraGruX = 0;
 Avisa = False;
 ForcaMostrarForm = True;
 SelfCall = False;
 TemIMEiMrt = 0;
 MargemErro = 0.0005;
begin
  DmModVS.CustosDiferentesSaldosVirtuais3(Empresa, GraGruX, TemIMEiMrt,
    MargemErro, Avisa, ForcaMostrarForm, SelfCall, nil, nil);
end;

procedure TFmPrincipal.Divergnciadecustoentreorigemedestino1Click(
  Sender: TObject);
const
  Empresa = 0;
  GraGruX = 0;
  Avisa = False;
  ForcaMostrarForm = True;
  SelfCall = False;
  TemIMEiMrt = 0;
  MargemErro = 0.001;
begin
  DmModVS.CustosDiferentesOrigemParaDestino2(Empresa, GraGruX, MargemErro,
    Avisa, ForcaMostrarForm, SelfCall, TemIMEiMrt, nil, nil);
end;

procedure TFmPrincipal.CadastroDeContasSdoLista();
begin
  {
  if DBCheck.CriaFm(TFmContasSdoAll, FmContasSdoAll, afmoNegarComAviso) then
  begin
    FmContasSdoAll.ShowModal;
    FmContasSdoAll.Destroy;
  end;
  }
end;

function TFmPrincipal.CadastroDeContasSdoSimples(Entidade, Conta: Integer): Boolean;
begin
{
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    if Entidade <> 0 then
    begin
      FmContasSdoUni.EdEntidade.Text := Geral.FF0(Entidade);
      FmContasSdoUni.CBEntidade.KeyValue := Entidade;
    end;
    if Conta <> 0 then
    begin
      FmContasSdoUni.EdCodigo.Text := Geral.FF0(Conta);
      FmContasSdoUni.CBCodigo.KeyValue := Conta;
    end;
    FmContas.ShowModal;
    Result := FmContasSdoUni.FExecutou;
    FmContas.Destroy;
  end;
  }
  Result := True;
end;

procedure TFmPrincipal.CadastroDeConjuntos();
begin
  if DBCheck.CriaFm(TFmConjuntos, FmConjuntos, afmoNegarComAviso) then
  begin
    FmConjuntos.ShowModal;
    FmConjuntos.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeNiveisPlano();
begin
  if DBCheck.CriaFm(TFmPlaCtas, FmPlaCtas, afmoNegarComAviso) then
  begin
    FmPlaCtas.ShowModal;
    FmPlaCtas.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDePlano();
begin
  if DBCheck.CriaFm(TFmPlano, FmPlano, afmoNegarComAviso) then
  begin
    FmPlano.ShowModal;
    FmPlano.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeImpObs();
begin
  if DBCheck.CriaFm(TFmImpObs, FmImpObs, afmoNegarComAviso) then
  begin
    FmImpObs.ShowModal;
    FmImpObs.Destroy;
  end;
end;

procedure TFmPrincipal.RetornoCNAB;
begin
  // Compatibilidade
end;

procedure TFmPrincipal.RetornoParaRetrabalhoEPosteriorReenvio1Click(
  Sender: TObject);
begin
  VS_PF.MostraFormVSRtbCab(0);
end;

procedure TFmPrincipal.MyOnHint(Sender: TObject);
begin
  {
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
  }
end;

procedure TFmPrincipal.SkinMenu(Index: integer);
begin
  {$IfDef cAdvToolx} //Tokyo
case Index of
    0:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2003Blue;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2003Blue;
    end;
    1:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2003Classic;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2003Classic;
    end;
    2:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2003Olive;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2003Olive;
    end;
    3:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2003Silver;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2003Silver;
    end;
    4:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2007Luna;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2007Luna;
    end;
    5:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2007Obsidian;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2007Obsidian;
    end;
    6:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2007Silver;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2007Silver;
    end;
    7:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOfficeXP;
      AdvPreviewMenuOfficeStyler1.Style := psOfficeXP;
    end;
    8:
    begin
      AdvToolBarOfficeStyler1.Style     := bsWhidbeyStyle;
      AdvPreviewMenuOfficeStyler1.Style := psWhidbeyStyle;
    end;
    9:
    begin
      AdvToolBarOfficeStyler1.Style     := bsWindowsXP;
      AdvPreviewMenuOfficeStyler1.Style := psWindowsXP;
    end;
  end;
  {$Else}
  //
  {$EndIf}
end;

procedure TFmPrincipal.Slidossedimentveis1Click(Sender: TObject);
begin
  App_Jan.ImprimeMonitoramentoSolidos();
end;

procedure TFmPrincipal.SPED20101Click(Sender: TObject);
begin
(*
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if MyObjects.CriaForm_AcessoTotal(TFmImportaSintegraGer, FmImportaSintegraGer) then
  begin
    FmImportaSintegraGer.ShowModal;
    FmImportaSintegraGer.Destroy;
  end;
*)
end;

procedure TFmPrincipal.BtVSFerramentasClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(PMVSFerramentas, BtVSFerramentas);
end;

procedure TFmPrincipal.BtVSSifDipoaClick(Sender: TObject);
begin
  VS_PF.MostraFormOpcoesVSSifDipoa();
end;

procedure TFmPrincipal.SbVSPesqSeqPecaClick(Sender: TObject);
begin
  VS_PF.MostraFormVSPesqSeqPeca();
end;

procedure TFmPrincipal.CadastroDeContasNiv();
begin
  if DBCheck.CriaFm(TFmContasNiv, FmContasNiv, afmoNegarComAviso) then
  begin
    FmContasNiv.ShowModal;
    FmContasNiv.Destroy;
  end;
end;


{
 Parei aqui!

 #####
 Ver hist�rico do estoque do Couro pronto:
SELECT smia.GraGruX, GraTamI, GraGruC, smia.Qtde
FROM stqmovitsa smia
LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX
WHERE smia.Ativo=1
AND ggx.GraGru1=365
 #######
 Tirar units e forms da NFe antiga
 #######
 Estoque de PQ pelo pre�o m�dio
 #######

 }

 {
 - Recep��o:
https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeRecepcao2
- Cancelamento:
https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeCancelamento2
- Consulta Recibo:
https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeRetRecepcao2
- Consulta Chave Acesso:
https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeConsulta2
- Inutiliza��o:
https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeInutilizacao2
- Consulta Status do Servi�o:
https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeStatusServico2
}

{ TODO : A��es para testar EFD no IDEAL }
{
UPDATE gragru1
SET NCM="12345678"
WHERE NCM=0
OR NCM=""
OR LEFT(NCM, 1) = "0"
OR LEFT(NCM, 2) = "00";
UPDATE entidades
SET PCodMunici=1501402
WHERE Codigo=690;
UPDATE entidades
SET Fantasia="CURTUME IDEAL"
WHERE Codigo IN (-1,-11)

}


{ TODO : Guia de Manifesta��o do usui�rio na janela de adi��o de evento na janela de pesquisa de NF-e }
{
Tabela[gratamcad].RegistroObrigatorio[1] n�o existe.
/*' + TMeuDB + '.QrSQL*/
/*********** SQL ***********/
INSERT INTO gratamcad SET
Codigo=1,
CodUsu=1,
Nome="�NICO",
PrintTam=0

/*****Query sem parametros*******/

/********* FIM SQL *********/
Lista de valores:
Codigo|CodUsu|Nome|PrintTam
1|1|"�NICO"|0
INSERT INTO gratamcad SET
Codigo=1,
CodUsu=1,
Nome="�NICO",
PrintTam=0

Tabela[gratamcad].RegistroObrigatorio[1] ERRO ao incluir.
/*' + TMeuDB + '.QrSQL*/
/*********** SQL ***********/
INSERT INTO gratamits SET
Codigo=1,
Controle=1,
Nome="�NICO",
PrintTam=0

/*****Query sem parametros*******/

/********* FIM SQL *********/
Lista de valores:
Codigo|Controle|Nome|PrintTam
0|0|"�NICO"|0
1|1|"�NICO"|0
INSERT INTO gratamits SET
Codigo=1,
Controle=1,
Nome="�NICO",
PrintTam=0

Tabela[gratamits].RegistroObrigatorio[1] ERRO ao incluir.
Tabela[nfecsitcab].Campo[chNFe]  Diferen�a:"KEY"
}

// Unable to create process: A opera��o solicitada requer eleva��o.

//O hor�rio de ver�o come�ou a ser vigorado a partir de um decreto da Presid�ncia da Rep�blica, fundamentado em informa��es dadas ao Minist�rio das Minas e Energia que adotam o hor�rio de ver�o a partir de estudos t�cnicos e indicando que Estados dever�o adota este hor�rio de ver�o. Este hor�rio foi devidamente regulado em 2008 pela Casa Civil da Presid�ncia da Rep�blica pelo decreto n� 6558 que inclusive definiu a data de in�cio e t�rmino do hor�rio de ver�o. O in�cio ser� sempre no terceiro domingo de outubro e o t�rmino do hor�rio no terceiro domingo de fevereiro com exce��o se a data coincidir com o carnaval e ai o t�rmino ser� no domingo seguinte, o que ocorrer� em 2015 e, por isso, o termino ser� dia 22 de fevereiro.

(*
http://www.rfidsystems.com.br/dura-3000
http://www.automacaomaringa.com.br/novo/rfid/
*)







////////////////////////////////////////////////////////////////////////////////







{
    procedure InjetaDadosMovimCod(const IMEI, MovimID, MovimNiv: Integer;
              const NodePai: TTreeNode);
    procedure InjetaDadosMovimTwn(const IMEI, MovimID, MovimNiv: Integer;
              const NodePai: TTreeNode);
    procedure InjetaItensDeQryIts(const TwnOuCod: String; const QryIts:
              TmySQLQuery; const TvPai: TTreeNode);
    procedure InjetaMovimTwn(QryCab: TmySQLQuery; NodePai: TTreeNode);
procedure TFmVSArvoreArtigos.InjetaDadosMovimCod(const IMEI, MovimID, MovimNiv:
  Integer; const NodePai: TTreeNode);
var
  QryCab, QryIts, QryMix: TmySQLQuery;
  MovimCod, Controle, IDFilho, NivFilho, MovimTwn, Indice, SerieFch, Ficha,
  Terceiro: Integer;
  Texto, Marca: String;
  TvTwn_07_or_08_to_08: TTreeNode;
begin
  QryCab := TmySQLQuery.Create(Dmod);
  try
  QryIts := TmySQLQuery.Create(Dmod);
  try
  QryMix := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryCab, Dmod.MyDB, [
    'SELECT DISTINCT MovimCod, MovimTwn',
    'FROM v s m o v i t s ',
    'WHERE SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    QryCab.First;
    while not QryCab.Eof do
    begin
      MovimCod := QryCab.FieldByName('MovimCod').AsInteger;
      MovimTwn := QryCab.FieldByName('MovimTwn').AsInteger;
      IDFilho  := QryCab.FieldByName('MovimID').AsInteger;
      NivFilho := QryCab.FieldByName('MovimNiv').AsInteger;
      //
      DefineImageIndexETextoMovimA(IDFilho, NivFilho, Indice, Texto);
      TvTwn_07_or_08_to_08 := InjetaNodeAtual('IME-C ', Texto, MovimCod,
        Indice, NodePai, emitIME_C);
      //
      if MovimTwn = 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QryIts, Dmod.MyDB, [
        'SELECT *  ',
        'FROM v s m o v i t s ',
        'WHERE MovimCod=' + Geral.FF0(MovimCod),
        'AND MovimTwn = 0',
        '']);
        QryIts.First;
        //
        InjetaItensDeQryIts('MovimCod', QryIts, TvTwn_07_or_08_to_08);
      end else
      begin
        InjetaMovimTwn(QryCab, NodePai);
      end;
      //
      QryCab.Next;
    end;
  finally
    QryMix.Free;
  end;
  finally
    QryIts.Free;
  end;
  finally
    QryCab.Free;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaDadosMovimTwn(const IMEI, MovimID,
  MovimNiv: Integer; const NodePai: TTreeNode);
var
  QryCab, QryIts, QryMix: TmySQLQuery;
  MovimTwn, SerieFch, Ficha, Terceiro: Integer;
  Marca: String;
  TvTwn_07_or_08_to_08: TTreeNode;
begin
  QryCab := TmySQLQuery.Create(Dmod);
  try
  QryIts := TmySQLQuery.Create(Dmod);
  try
  QryMix := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryCab, Dmod.MyDB, [
    'SELECT DISTINCT MovimTwn',
    'FROM v s m o v i t s ',
    'WHERE Controle=' + Geral.FF0(IMEI),
    '']);
    QryCab.First;
    while not QryCab.Eof do
    begin
      InjetaMovimTwn(QryCab, NodePai);
      //
      QryCab.Next;
    end;
  finally
    QryMix.Free;
  end;
  finally
    QryIts.Free;
  end;
  finally
    QryCab.Free;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaItensDeQryIts(const TwnOuCod: String; const
  QryIts: TmySQLQuery; const TvPai: TTreeNode);
var
  Controle, IDFilho, NivFilho, MovimTwn, Indice: Integer;
  TvAtual: TTreeNode;
  Texto: String;
begin
  QryIts.First;
  while not QryIts.Eof do
  begin
    Controle := QryIts.FieldByName('Controle').AsInteger;
    IDFilho  := QryIts.FieldByName('MovimID').AsInteger;
    NivFilho := QryIts.FieldByName('MovimNiv').AsInteger;
    MovimTwn := QryIts.FieldByName('MovimTwn').AsInteger;
    //
    DefineImageIndexETextoMovimA(IDFilho, NivFilho, Indice, Texto);
    TvAtual := InjetaNodeAtual('IME-I ', Texto, Controle, Indice, TvPai, emitIME_I);
    //
    case TEstqMovimNiv(NivFilho) of
(*
      eminSorcClass: ;       // 1
        // Nada
      eminDestClass:         // 2
      begin
        InjetaDadosMovimCod(Controle, IDFilho, NivFilho, TvAtual);
          if MovimTwn <> 0 then
           // Nunca passou por "MovimTwn <> 0" !!!!  testar se precisar quando passar!!!
            //InjetaDadosMovimTwn(Controle, IDFilho, NivFilho, TvAtual)
            InjetaDadosMovimCod(Controle, IDFilho, NivFilho, TvAtual)
          else
            InjetaDadosMovimCod(Controle, IDFilho, NivFilho, TvAtual);
      end;
*)
      eminSorcClass: ;       // 1
        // Nada
      eminDestClass,         // 2
      eminSorcPreReclas:     // 11
        InjetaDadosMovimCod(Controle, IDFilho, NivFilho, TvAtual);
      eminDestPreReclas:     // 12
      begin
        if MovimTwn <> 0 then
         // Nunca passou por "MovimTwn <> 0" !!!!  testar se precisar quando passar!!!
          //InjetaDadosMovimTwn(Controle, IDFilho, NivFilho, TvAtual)
          InjetaDadosMovimCod(Controle, IDFilho, NivFilho, TvAtual)
        else
          InjetaDadosMovimCod(Controle, IDFilho, NivFilho, TvAtual);
      end;

      else Geral.MB_Info('"MovimNiv" sem inje��o definida em "' + TwnOuCod +
        '"!' + sLineBreak +
        'MovimID: ' + Geral.FF0(IDFilho) + sLineBreak +
        'MovimNiv: ' + Geral.FF0(NivFilho) + sLineBreak +
        'IMEI: ' + Geral.FF0(Controle));
    end;
    //
    QryIts.Next;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaMovimTwn(QryCab: TmySQLQuery; NodePai:
  TTreeNode);
var
  MovimTwn, Indice: Integer;
  TvTwn_07_or_08_to_08: TTreeNode;
  QryIts: TmySQLQuery;
  Texto: String;
begin
  QryIts := TmySQLQuery.Create(Dmod);
  try
    MovimTwn := QryCab.FieldByName('MovimTwn').AsInteger;
    //
    Indice := CO_IDX_IMG_GEMEOS;
    Texto  := 'Par de G�meos';
    TvTwn_07_or_08_to_08 := InjetaNodeAtual('IME-P ', Texto, MovimTwn,
      Indice, NodePai, emitIME_P);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QryIts, Dmod.MyDB, [
    'SELECT *  ',
    'FROM v s m o v i t s ',
    'WHERE MovimTwn<> 0',
    'AND MovimTwn=' + Geral.FF0(MovimTwn),
    '']);
    //
    InjetaItensDeQryIts('MovimTwn', QryIts, TvTwn_07_or_08_to_08);
  finally
    QryIts.Free;
  end;
end;
}

{ Meza 2015-09-03
SELECT Controle, DataHora, ValorT, AreaM2,
ValorT/AreaM2 Custo
FROM v s m o v i t s
WHERE ValorT/AreaM2 BETWEEN 18 AND 19
OR DataHora >= "2015-09-01"
}

(*

�rea financeira trata operacionalmente das finan�as das empresas, sendo que seu
setor esta normalmente dividido em duas se��es chaves que s�o o contas a pagar
e a receber. Contas a pagar como o pr�prio nome j� diz, a pessoa que cuida
dessa carteira deve ter o controle sobre as quest�es de pagamento da empresa
deve trabalhar em conjunto com o contas a receber que � onde os recursos s�o
devidamente captados para poss�veis despesas, investimentos entre outros.
Essas duas fun��es est�o intimamente ligadas com o gestor de contas chamado
controller.
A �rea fiscal trabalha de maneira a cumprir as obriga��es com o Estado em todas
as esferas (municipal, estadual e federal) trabalha diretamente ligago com a
contabilidade isso quando ao inv�s do um advogado n�o � o pr�prio contador quem
o faz pos condiz a sua �rea.
�rea cont�bil � onde as informa��es de todas as �reas da empresa s�o
processadas resultando no devido controle da empresa se portando como material
de an�lise para a tomada de decis�es dos gestores.

Todas essas �reas expostas nesta quest�o est�o ligadas ao controller, podendo
ter um s� departamento chamado de Administrativo.


Obs>: Lembrando que apesar de todos do setor serem subordinados ao controller ou gerente administrativo. O contador trabalha de forma independente, pois em legisla��o prev� que ele responda solidariamente com o respons�vel legal da empresa. Pois, muitos administradores querem administrar a empresa de modo contrario a legisla��o. Quando isso acontece que o contado n�o � ouvido ele tem imediatamente que comunicar ao conselho de contabilidade para que venha a se proteger de problemas futuros mediante a sua classe.


espero ter lhe ajudado
*)

(*
object SkinStore1: TSkinStore
  Store = <>
  Left = 296
  Top = 776
end
object sd1: TSkinData
  Active = True
  DisableTag = 99
  SkinControls = [xcMainMenu, xcPopupMenu, xcToolbar, xcControlbar, xcCombo, xcCheckBox, xcRadioButton, xcProgress, xcScrollbar, xcEdit, xcButton, xcBitBtn, xcSpeedButton, xcPanel, xcGroupBox, xcStatusBar, xcTab, xcSystemMenu]
  Options = [xoPreview]
  Skin3rd.Strings = (
    'TComboboxex=combobox'
    'TTBDock=Panel'
    'TTBToolbar=Panel'
    'TImageEnMView=scrollbar'
    'TImageEnView=scrollbar'
    'TAdvMemo=scrollbar'
    'TDBAdvMemo=scrollbar'
    'TRzButton=button'
    'TRzBitbtn=bitbtn'
    'TRzMenuButton=bitbtn'
    'TRzCheckGroup=CheckGroup'
    'TRzRadioGroup=Radiogroup'
    'TRzRadioButton=Radiobutton'
    'TRzCheckBox=Checkbox'
    'TRzButtonEdit=Edit'
    'TRzDBRadioGroup=Radiogroup'
    'TRzDBRadioButton=Radiobutton'
    'TRzDBCheckBox=Checkbox'
    'TRzDateTimeEdit=combobox'
    'TRzColorEdit=combobox'
    'TRzDateTimePicker=combobox'
    'TRzDBDateTimeEdit=combobox'
    'TRzDbColorEdit=combobox'
    'TRzDBDateTimePicker=combobox'
    'TLMDGroupBox=Groupbox'
    'TDBCheckboxEh=Checkbox'
    'TDBCheckboxEh=Checkbox'
    'TLMDCHECKBOX=Checkbox'
    'TLMDDBCHECKBOX=Checkbox'
    'TLMDRadiobutton=Radiobutton'
    'TLMDCalculator=panel'
    'TLMDGROUPBOX=Panel'
    'TLMDSIMPLEPANEL=Panel'
    'TLMDDBCalendar=Panel'
    'TLMDButtonPanel=Panel'
    'TLMDLMDCalculator=Panel'
    'TLMDHeaderPanel=Panel'
    'TLMDTechnicalLine=Panel'
    'TLMDLMDClock=Panel'
    'TLMDTrackbar=panel'
    'TLMDListCombobox=combobox'
    'TLMDCheckListCombobox=combobox'
    'TLMDHeaderListCombobox=combobox'
    'TLMDImageCombobox=combobox'
    'TLMDColorCombobox=combobox'
    'TLMDFontCombobox=combobox'
    'TLMDFontSizeCombobox=combobox'
    'TLMDFontSizeCombobox=combobox'
    'TLMDPrinterCombobox=combobox'
    'TLMDDriveCombobox=combobox'
    'TLMDCalculatorComboBox=combobox'
    'TLMDTrackBarComboBox=combobox'
    'TLMDCalendarComboBox=combobox'
    'TLMDTreeComboBox=combobox'
    'TLMDRADIOGROUP=radiogroup'
    'TLMDCheckGroup=CheckGroup'
    'TLMDDBRADIOGROUP=radiogroup'
    'TLMDDBCheckGroup=CheckGroup'
    'TLMDCalculatorEdit=edit'
    'TLMDEDIT=Edit'
    'TLMDMASKEDIT=Edit'
    'TLMDBROWSEEDIT=Edit'
    'TLMDEXTSPINEDIT=Edit'
    'TLMDCALENDAREDIT=Edit'
    'TLMDFILEOPENEDIT=Edit'
    'TLMDFILESAVEEDIT=Edit'
    'TLMDCOLOREDIT=Edit'
    'TLMDDBEDIT=Edit'
    'TLMDDBMASKEDIT=Edit'
    'TLMDDBEXTSPINEDIT=Edit'
    'TLMDDBSPINEDIT=Edit'
    'TLMDDBEDITDBLookup=Edit'
    'TLMDEDITDBLookup=Edit'
    'TDBLookupCombobox=Combobox'
    'TWWDBCombobox=Combobox'
    'TWWDBLookupCombo=Combobox'
    'TWWDBCombobox=Combobox'
    'TWWKeyCombo=Combobox'
    'TWWTempKeyCombo=combobox'
    'TWWDBDateTimePicker=Combobox'
    'TWWRADIOGROUP=radiogroup'
    'TWWDBEDIT=Edit'
    'TcxButton=bitbtn'
    'TcxDBCheckBox=checkbox'
    'TcxDBRadioGroup=radiogroup'
    'TcxRadioGroup=radiogroup'
    'TcxCheckBox=checkbox'
    'TOVCPICTUREFIELD=Edit'
    'TOVCDBPICTUREFIELD=Edit'
    'TOVCSLIDEREDIT=Edit'
    'TOVCDBSLIDEREDIT=Edit'
    'TOVCSIMPLEFIELD=Edit'
    'TOVCDBSIMPLEFIELD=Edit'
    'TO32DBFLEXEDIT=Edit'
    'TOVCNUMERICFIELD=Edit'
    'TOVCDBNUMERICFIELD=Edit')
  SkinStore = '(Good)'
  SkinFormtype = sfMainform
  Version = '5.70.02.28'
  MenuUpdate = True
  MenuMerge = False
  OnFormSkin = sd1FormSkin
  Left = 328
  Top = 776
  SkinStream = {
    A5CE00009FB70B00D676B1986978DC180E7E5D5B906300601FBFE6EBDDF9B7CC
    E5AD6E2E69D4B48F05FE3E37B62B413FEFAFCFE9200A9B57FA01A01EF854FB6B
    2BD4530DA8AE86D474017EFA79707F782F82451D4F4FCA7DC0545AA923359DFF
    88709369DCC6248AE9856B00E96186CD4379A2A0B6A9A4CAC3D7EF9A4D310A33
    5520C6E545D8481B22A32EA8DE465A02E83D7109D97AD858A54A954055608D64
    46D19A462209227A031E256820A530A95A40901067524117FBE18280A5282820
    37D53522DA10A09687747C43EE827850D6C7763FE2CCE0AACC5F4AAB90F12519
    22015541AF8C0CA2AF62C22C0034B55E508B821A149F2B27F1937E54CCBE859D
    80439A48AE03E08CB5D0035800EB9260C2E0673B874EB752737393E72FBC58E0
    FFCF8E3FFC4D72181D13480EF0B858DE60391E00B896A0753763B424043FF1C8
    AFE62C57994D7651F400DA858F0766CAA6B056798BCA003D0A557C9E01C6EF01
    F5620FD1A989A60BDDA55410C7C40760A30006E52A5518B7776BA18DD9D4164D
    F288729A95A4CB8A80FC2E0E1C64726A031B44C4CF118990B8E73715C0E6F68F
    771DDE3DC6A862DC3FA305131B71E6E444DDCA5CE46B59E3F695E7E5C06EECC1
    CC84A58CD943A3276BD4952DE2DC4F5AE27B65AA944E96A64B184DDC4EF9DFEB
    D3721EA015D86BE203874AC1831040745029080E9A00DAD72C77CB993231BA6F
    4401C84094BD200E85309456A03AE828AD335916CB8CB1A89E4513D0A27A184E
    C074D80EE03AE600E08E8949454B4021715C4E179CC6AA8584E171FB80C685C3
    73162E663238F2047F9A97812F38549509C062426960069B003A929047516E03
    18F46E3997D51811B46A14AD10E2476083FE657808ACEA040495E02EC10039C3
    1B65E82FF9C9F701C201EFA036ECC6D0DAA6434060F3AFA23FA1BE3437A03F9B
    157807EDEBC3EF4CFE0FFDC0FE01AA196DC035BA9331968A114D4B98C7C992EA
    6E081B60FDD2C1F6E018107806030E0FE7C1FD4BA2AB63E6FF07FF00F5EF1B84
    E891E82A634B96E0DF05FF0A504C7E09A4502FA85AF8069875BC1FC0E8AD7C1B
    BF83799D44F9F12FB1C747B706FD2BDE01AF31705FDEE9C5731909C5A8EB7DBD
    1C2310F03E9949C380EBB807A3C83C0FE01E9108ACE39CFF80C6D1020FE07786
    03EE0785C12356450585E0B0E0D79F97D7F5C48B7F07E5D936C0769B5E035F3A
    C7058AF05BB9B817C07D9E0BA967C17E03118A63780DCDAC3E07D3355D4F03F3
    83F03F80E96F00FAB0640C6086801BEE03D000C08062801B3AF8019C00340010
    7DAAEF59EF00A2C04E800020067C01C33028354363618C01569E6C28003D25CB
    8D87E903668542003360180A01098F0DD801040F00043804A4A8025E7AEEEBBD
    76EB26BBC98FB48968068E025A0343419C20E252C606274113BE8CC26F78D0F9
    33800DE9C81587B8C0200711E12F154152ECFE92F31CCF1E5108A4A401FF4F49
    2B87898001A18000B38E47803B6E003F010F2483EA0255F35B913A859EB82779
    EBAB257B49583AEFAF54B54029DCA82305F0052C06701E2A2BBD6B7963FF3AD3
    0417DEB79638DEEF83BFF1EEFC326D803F00A8C27C0E7653353D94285001FD85
    93757DD3B37B92FDCBBFDB3D2F04B80EE4DA0E7BCFC2B558B7D2401A1279EC83
    0AF939665BAE33F0FB8A8FF003966EEB9175A65F8A005E4429D91C8F03EF3EBC
    5D217BC22B9B71C437C04FD9CEDBDA05D26C118001220175014F035C41D78002
    3CC00C69F59DE31738C3C34A84C6DD96CC2006C56D7D800BF54F50003FE1E345
    66E5CC0F6C3674DAFADEC46000BB3DDAED0C5424DA800683A0EBE29E9FCF213E
    DF8F76F5C32AE799DCE381A840FCF54F2CDB4D8A5D0611E83DB3D9BDADAD85D0
    599072B8F0D744F2DD38199CB999E9CD42E8BAD92FDC8B078BCD2E7D6C4A178F
    585CFDE8466F0D8473D0BBF1F8D29E633B58A3A556B9F6BB95BE8975658554DB
    AE6231926F6E3F750C4F17AE1178C5C8C44F418A1DDB715374C2D5DCAE85D7B9
    9CA38DD145CB189F8E200817395D394B63D6214E6253F0043A92BA2758595A42
    4703C928C4A9598965B1C001C00EA015402F40299AE27F914E419FC6AE29B4D3
    F17E611B9098A1142B8E6001546574E175D0586535D1A3A8CDCDCCE60CE7644A
    8B323C14B9EF57904851194C63D2FDDBDF482FBAEE305625F63751F69657578B
    A2AAD9B312F24004A2BB2E6F1D67E2873168AE552521EAE4A3054AAF76836EC0
    A70B5CFDDC17B77C7C5DD235C4ECB86F8ED746491F4AAC03DAE92BA9DAB81D91
    7A58A92472E7144B0BCE6D512E5A3769BC86E9282AC725C25D26A56EB4BBDC66
    5E2E031622A3BD202A3AE3968EADD13F5AEA9A14A573CC8FD98607E87D3544CC
    9685CB05CA34AB075D5AD80973A229E2EE0BA6481E430068D2E092165B398A54
    2E7451DD596E5003AD73D1919F8092FF63172A973E16F239E4843C503B1608A5
    3062B8055368F7608836DF696015D1E57A8B7C3729B973FEB9ED90FF8F39CF28
    E710D710996188CC960EB59C0B9FDFB78E014F4075315EAC48CC30F314D6B9A7
    A3CCE1759AE54203875CF1D55C079C0E631DAE58EE7F253C8917532F56F73A03
    07F88FD7BE6F344368DF769477F7C8628BB00D46A5299969E6D3C5CE6258975D
    C7781642A765CFF2C9304738B431DC90154088CAD10ECE123A43535734FD8008
    01D2823802CBBD46320B00F1C8EF0CF05CBC938972B3A6F4B7918C50E23225DB
    EA7E68F1EB000DE89E919BC60F182ECE37B765001BD3C77FDF7731906CB62BD8
    DDD684F654775FC8524D6B54AB9DABA4FF8AF169E9186C79E9C81E8FE31D3C90
    5E0174063899533C7BC10F3976C257C6B1965D2B3F6410FD907990F8EDF0CE2C
    3A9FF7C6FA00E92C5A733CA746BA1ECDE44AC5B89003F00E353F59F4E4717A84
    7FCF44C0ECE5C518F2F2EFC55C8F6B059B23F12D7385D2C230B7B6171344BBB4
    9C434B5CC1AF997257F98001A00000253C04B80415FAEC3F3DDFF67B0844B7F4
    B8E435449AA06F2A07CA7E5BC80ED02B514DF97352B4E9D5CED76831E00F0D40
    21002D40069A8AE5D1BCAAA9D61C4E2656D73D17498A751EA4F610AFB074A002
    8BA54801410B8D3393A755C74A912EAC7F698972451B899E42B3D429B88A9FC7
    2B55BE6E252B8AECA75342001566CA95C4DBC9791ACD13376BAC4D1331BD78F6
    6B4A403B80881D906E4048216C0021F71197F1AE4BA1FF9D8800064318C73D99
    927995A003FB817008E80BD00B50934636E1BE44B7D2DF003A66F6989F001E76
    FBE012B00790DE004D865359CE41653400AF5CA407C1282C6E56F881D07DDC04
    1C8D2065F8FEB5EFC03D813006EC0B2DF6CEF970BB373DE39FD060024137C929
    73A79D23FD00FA02DC08A00DF0056921000EC008812380083F012C812403F211
    470D446B7D1EC23EA3022200E48019E41D2582CC648CA035F94CAC0CB10AD3B9
    74A006E655C861C588D0882DC01459DCC8E72879FDCB5E55BC400AC7A52A5801
    64000785F40A03C30135801C600EE96F337360D3F0008A79DB801E513137E744
    498149B08A5BCC2FBE3B148453A54AE51C4EF58A07E1AEF2E070D4136D9008D9
    9AC100786779890EE6EF401EE80B5C12879AC812BD665D99D6F83CB4D055D1F9
    DF62A257FA994079B9FE018097EC300600BE4140265E5D2D12DACA2E4170B933
    A9B9D8A5764FC49FC39B39CAE06B4D70274794B8F4EDB7F9D5EC2CBA14DE3340
    CF3463008C06B9802EB00808262A0D1C15B05D4B3BA6F492607DFC57FC8809DD
    8E72CAFF5DDEFB19F7B0B3E76CA367544DA3417A4F79F5B9757B9A57B3CEEFF1
    3AC6B3BB536E2668D1400D09E80FF0347F014B401B61AE01EAF90A22D01B91C9
    48C92571648A2CFFFD9E3F50C665D72E4591FAAEF237C0C02EFBC77D731E4E6D
    C6BD134BA6A0260EEE29528034928BF53C87E1BA25105F5A98C0B5777DC9A4C8
    0395F619D5EFD15AA51017368BA6407248CDBA707E9876005FE622FBE6469986
    01A1BD5D7BBAA85D38D447DF0EEA6A2AB1E01FDF61E5781CDF8FE079DEB3D947
    6557EDBE2E632E2A527F3857943B1A6D2F6472803AFDFD5ECDCA48138A0D534A
    68005E4567355946FE4DDDD04CDEEFEAAB9FD623368F19262AA7349535B20472
    4F4DD9605D4A02E9E6B9F3735677385F2D5F72A87D681F61E262868A58A9F1DD
    DAF93CF0EA5999397D65E1CE7DAB1B5707FE7B759E2EABBEDA90EBB69B1A182D
    FB65F53BA852FAD0225644B7DDF53297D7AB7E9B1EFF6DE8F55BADA624BAE9EF
    93705DAE7C1E9C2E61149F0A1534049B15ED73F62F53BF1CDABF82908A2C3090
    BAD73FA1E434D1DE63D67A19EC9A18B164B693A7F561E574A2D14B16D8BAFA33
    A8A1390F71E6A7315FD728E3BA07AB630D96FAB6494F71176848A50EA14D66C4
    350CEDC98A1B5D1EAEFE8EFE563C044F5CDE00F227F602D0A6E25AA23D4FB0ED
    9E40E6EE7E6FC976FD14FA95C248160A36558D4F380EB9F8D1DF9447C6295D13
    2F172E99A5B3ED3F772B52B927B5CA7B9460AC1CEAD6B2173EE298F5350BA57E
    74AC1C2E8D6B043B5D2280D11482C5FD14EFC5C913E54A92B597F33172ED7391
    1E4636C5620A6B7E9C2DE1900BA37CCC452F229E85D2E231EC02F51C7190253B
    3C7EC79756F64B8B54B059CC92CD6B12173EBDF334F43D152A8B8AEEC07C28EC
    2EA4452E4554458FEAEC1127D51994E4E3EDCB9395CA9B9F319E425753CD4F8E
    76B6ACC1AB5FDFE95BECAE7E5A80DE4CAF54B930C9D375ECF2EE25B8544B3CC7
    7C0E42A24B9FA2755679C5D1F9CD34FECB7953E2E9ED70FBCB9DC6379463C144
    059FAB4DD03C33ED72F77E65CB9E9BD15E4768311F9F53F67A85D28393012CB9
    527802DFC81E77F99EE7010ECEED741C51C89FE12FC64C71DF7B160792D7F200
    555CF7E428956B7C0B9F6BA768F5E3C3D09FADD4FFF410D63520EDBE921C0078
    02675EEB30079D9EBDA0D7EC1A6939E72B3095BA1517FD3259B209F8C83E3EF8
    EEB99C5E753F77229FEB37716594E947BDB5E0443F002F4059E006D1F07A6000
    27D23213610F3898E5C518BBE5DEF17272AC1355AD642E70BA754616D985C754
    7A685A7C5EC93167D7955EF3AC718963C020067A8000780930F492926992F87E
    8967D31A3FDB996EF291794EE4F231740B9C53F65CDF2999CA68C2CCE65A163D
    4D49AC75DF9F1D40097CB93D69BBD69B06F0D475FA847DE53A74B0F071332AB9
    FCBA4453ECF507B1AD69EA278012A2D5520023548BAC7E51871FF12FBC7F1112
    E67A37913C8D17A8324385FFF1DB7DEF99837A11297F00536F800E2415BE46F3
    27BCA89B4F5D732899FDEBD97E7E3E04581F206F604E81610060F880FF1B5BDF
    7377F007C8639AA6F0493095BFE21654F8705300410964633436DAA2493D4F86
    803BAE4B31270076C7EE138096402D0D88081195C654F6592B00181A7D8B6C44
    763E801E0741A227E0EEF781B27952442002D020819A05A14E7EF9457614F704
    FE5A0033134BE0C1E21D247B402D011408E80D6F0A9A5F00301017813A803C1E
    212102660728440403618653D907D66046A01F50073E3ADC3C9B82448033029A
    001B281539131F0001E872F3E7D2A343777700D272B84702F9D1EC174AB58000
    DADD747666A00247C07B6BC079FC0FA9FC07333961417C95E1F88409C03C3F0B
    880F756347DE036F7088704073912716C9928579EF82F18935E2D25FE1F023F9
    93926CDA8B495D7CB469D65373B6F5C6014F45EE350069779466FF34F6FCEDE6
    9D21776AA91B8808DA0111E002077A178090000EF8FE55E0001FF8FF332C00D8
    2F0F9F7CC9000D56F0FC8D26293E1FD53A80003F0FE5E5C8036B787F27EFC017
    F16E1F207EDD8FE031FD763FC0C7FB98F8D63F9383F02F355F21B36BD1F34813
    6B8B9F32BB9079B568630FC2FB4DAFCE51C06136320335A5C66C7E6B77F80E57
    1907CE4A0784D9D48EFF9BD9F24BDE0072E78019BFCEAE389D5D654F4B757CE8
    F480BF92C636027BDF14B801FE7B4A801CD631AB1CD4F3599B458B0A1EA36EC0
    09DA02BB8D779F6017F8B1D504DA8001E21C2EC8C007357256B2007D67556ABD
    EC00DDB01BBACBCBB400D801D5D4F6400F9803D661F004C800F25FA2729EBD8B
    8E998E7791174BE7D9D8EFC00737C0359B62D001F6003A0ABC8D50B52DDE8977
    5963BD5D10F9E3B8B55DDC3C91321ECAA51673528A6C75806EBC649000E0074E
    00E9DCBA00F7C01F262AF728C4BA1423933548FFD95A256A15AA007427E700F2
    03B0D3190406F440EFDFCE20FD841FC600A7C624080D772F589F4AA103DF87A2
    03DD68E3158AF067C187B074A2ADEC48B21E9FD0880C1438BE4CC70E6A9A28EE
    F2481C80FF2371DBAA1BF1617A0CC48B41020302031C0352036C03700378037A
    0396036201D82DC34B6FB54C1B59AF2BB9EC04B56D5AD5740D46CD3B1A1DDA09
    1595E0A9669F95414ED37FDA09F957C2B4794FA86BEF556ABE9E409E32C68A44
    E76F9FFA73503E77204F44E1CE93B94546B27F609175E87C4CEAFC747D83CF54
    37C04813D419CB76BCBF53BE824C42D42D8DAACD75F41203B52C9376EA866CE0
    904F485A372A462366D9609FE67811E012534825CD989C183AB0137075018F48
    5991313FBE1D6A04823E715261D11FD02783AD3CEAA326656E8CBFA8120859B5
    A84FE027E46217A4CA009AD89CF7D827B813961394D554E98289EB2315AAC13E
    1BE0865C1270EFEC126E6945A6A2A8BA1E5C12741E1D8CD34AF5C129354D6F86
    A6009EADD8B356A2F1695EE64120B7A39539E55D71BD11641294BC86B1964B98
    56413D5BA04C000D0266FBB45FD21409E104ADAD5325EBC4F7827AF5C11C0878
    4120D5E03791FD027AA5FA46ED7698413E3F7909FA23D7C3CD51E3801BF8027C
    47ABC88F28252429818800508271647986FCA7C28047E609491ECAF80320252B
    9D26B4809F305CD4BE609FC8AAE32FAB79013B841D2E1D26B4A09F91F247D88F
    2827E6FCC052BF61F9824387B56C6A382904C1AEE549CCDB2F802A04B2404852
    C9EF59B0E28DF554099F1956385EDD17D97096B04E7F4E1C7940158275213ACF
    797ABD8300B8DAF57BB7ED070D5E8D423B0F80C0B3ADE3FFF5A234707582EE74
    48755A497F6822378A2333E4DF3E4FAB5E3C35EBFF2BF3D7CADD7AD18B0046E4
    064B8FEC053F80C981FD84B74D01D7CA12332033B15FEC67270D8CD2A13E7FD8
    BA3B39C7233D07D5677FB6CB626EE13B4913D9A9D9CD989F19DC7B6D4ECB4ED0
    6C9492D9DA0FF68B0B9DA2DA9F895EABE1C6D9E91DE5602A00369B60ED3476A3
    9ED3476823B3D1D9CEFDFFECC68D706C26C6086761356BF6F42AB5F23AF5C592
    AD78E39D768FB7E3AE5BE75CB84EB677704256B278080FD6F875A39FBBFDFC08
    0FD3FBE758A3EF78EB255AC5EB582DF18300070407340740074C075007740778
    071607E3C401B24E7935998EA705CA7580299902E212F880540076A0B88A2008
    E1C83DB76F40FFEE5B82E8C200C87DF4AD02A4ADD402E4FD230EE08C773F4122
    7D16C6B9B14005AEDED42C637F4AC6BFAFE26BFDFB528166B8215D2FC8BF5A5F
    2AFEFD222602CEB066DE772DEEBC6D23CEEC9E0B543CA933B8B73D8FB79AFDF2
    F6E802C20420BF2B22F12857BFEE4D805E3521750117C8F156017988F12FAD62
    F105ED3AB5422F9BC3708ADD7D902E7A20BEFBBF412EC88F3417BCFC93488EA0
    5A8768346059C38261160BEFA4FDFEC27F405E82F75DF02FF3FF6187D0DB51E6
    4C3E87FF20B279E7EAE9275A1FFF82D028DBACA242D714953F681686C536C29B
    B264AF23C111F005870E9C13533281E401AAEF5E05C15CCC810421D8C2806162
    762D76F282F7BB98E8804405D201CA0B92E001BD65D84FDF482F686F200015B4
    2FF61DD20BE82398F217905914FE517611A69DA0BCC9FAD51F5BA13E00B8BD42
    4AFD406845FCC17512750BC01643E933B6EFA5E08BEA05E64F7333D20B172242
    01714AE567EE05F049683D19EC05930098A952A8E45FC8164FF84480743C1FFE
    C17ECFF017FB05FC59D513EC05E6B0C493E40BD5A9FE0FF613FEC1762CFB5978
    82C4F2118F067FE05F07FCB50F00BBE67FB4162BCC44797D91E8D5FF982C454D
    17EF5FFC17C82FD5D802091794167C82F97B3898005B1266159D8C191FA05FFF
    2E66001000B0DC791F0177FA8033F80F53F02F3FCF1E0DF7B752031DBAC03B55
    7DDBB5C1DBFFA00FDB75EC1EEDEB8BFD520156015F0162016401640167016B01
    6D016E0174017501750178017D016E0013DCFA0020004800B0020005400B0018
    0034006800D001F003E008401080230046008C0120026004E009C01400280050
    00A8015002E006000C00180031006200C40188032006600D001A8035006C00D8
    01B0039007200E401C803B007600EC01D803D007A00F801F003F007E00FC0040
    010007001C007001C00A002800A002C00B002C00B002C00C003000C003800E00
    3C00F003C00F0040010004001000400110044011004401100480120054A006E1
    90024009802600980260098026009802600A002800A002800B0030D0034E700D
    0875001CA59018021606EC259800073220DE459EC0BE05C118B20BC03E60DBF1
    4F247FA2BFA4B662496F4AE0467A40FD012CE8EC5BDF697D0A8D0B321F6A5B74
    7C591DB23B647683476C4838849AEA72EA72EA71AED4E2CAEBA297452E8A6144
    F831B802E00B802E00C003000C003000C003000C003000C003200C803BF00387
    B3F7C939F003B950060032011039A0FF41002DE028D05B459E004440FA17700F
    A17D805F88CF2427425F442781C7830FC90FA4136A58EE737F357A417B0BF623
    CA3B947728EAD3477F284D8A93654E654E654E2B8539F15DA45348A6D14FCDC5
    79080105C00BF82FE007E0BA026844C8CF1257F3C022FECE1B70D2243E105F80
    8E745FD9155C00B9046E400123E5442368EED1DDA3ACBC8EF312F616936D4E6D
    4E6D4E336A7368A6D14DA29AE82B1847DB3DFE017803A608442E54117A8D5508
    72BC17C7A0293180A0017028B68EED1DE23B9723B6C327893714E714E714E484
    A719C817114E228B6914454B4BD700B19EC087F767BD8E01240E703CB0708A6E
    0237C3EC9232C516109CB0A2F24122F4417AE5720264C2E1BEF95C94404D1E1A
    E881F003C9F9C47788EF11D5911D9BD26E29CE29CE29CCB2A70493E229C453A8
    A6AB7161049097DC025E0440A9280911C56EE011E004984C41D7157607532D0C
    3403703235686D8197C027169341BDD477A8EF51D4A51DEA4DD539E539E538AB
    A9C04501140452C1DAB81645A5CC027C7B20ECE00985B462CD429E006E0E987C
    84272E606F03BCDF442386260E8100811D047411DC7E76E4A49814E0A9C15384
    A709143450D14E004B8B1EAFDC02C8F042D8700B5005E0070510C1074426708F
    C1C201384D2C7A1F220E5884F0776844645795DE4E3C81B05FD04582888C0EF2
    7132DEC10585769BBCF72D47CA8E0D1D3474D1D4391DD4A84352635386A70D4E
    4C6A715384D143450D14E9B8D455D9B6017151CDA0BE0015CCCD85F0E0173EE0
    7CD045CC586025219E68E9A3A68E9A4D8E12645388A7114E19D009114445234C
    05A34191B00B1E9D7BC105029428211CEE780B481AED4B03A02311DAE38F08C0
    0A505C04C508C071B120E0DA085C7768CB9828EE5130357D134C476A23A88EA2
    3B89D1D649F96A4CAA7154E2A9C5538A8A2A28A8A74571F2D9C90C59DD367600
    597805301DCA04580754410744517821A2729C99CF93EA8EAA3AA8E9808EAA4C
    AA7154E329CAC54E3228C8A32284C9F393B49300280A2A4890537816002108D4
    8029E082846E0E8034FD9410A8ED51A8029B02048A293871C6C08BCB7AB0B9C2
    2B926A96EB99E8058945766C5884CF41517A10C8EB23AC8ECAC8EEC4815324CC
    A7194E329CC414E3228C8A3228A10E483A0CED00298D41DDD3C0B004201C102C
    02E21110159F80BC230104011B09922348682487008489824CBC0858B7492092
    4E02F08A540E2174421DE0EF05590CF7810A2A0832647591D6474BC11D315164
    5710693329C6538CA703129C3204C8A3228C8A248A15273A66539EA5BE8900BC
    1CA08AC05C680B7238C010F781AE48D7182DF28DBCF1399D355D1D7475D1DBED
    1D0A24354499D4E3A9C753817D4E3A28E8A3A2971E05A1CE5AB809C04E027013
    809C05002801400A005002801400A405202D540455B200900480240120090048
    02401200900480240120090048024012009004802401400A005002801400A005
    002801400A005002801400A005002801400A005002801400A005002801400A00
    5002801400A005002801400A005802C01600B005802C01600B005802C01600B0
    05802C01600B005802C01800C006003001800C006003001800C006003001800C
    006003001800C0060030018002E800816A0023AA40B682376142803826114200
    24AB8185E5F3CF11DC47711DA7C8EE24D8A7314E629CA78A73114C4531141204
    2FC8C4204ECF01CED881BC10F032425C003C39B3069858C015417F231E412C0F
    C03438A4C47AC56C12DC090AB96577918B2058013CE84CB7467BD9D151D96623
    EECB639F188EE23B88ED768EDA906A893629CC5398A72C2A9C795D88A622988A
    1689E4F34944760001104AC1DE0A6C2F6846D152835A6003226D215B64B324E0
    028C16201EB081E0510008721F6638EF10F88EE23B88EBBE8EB88C624D8A7314
    E629C9CA9CC453114C450AC704D9239DD288FC07DD1E06F01800801D107AC11E
    165429105C458A808481585E20338BED02AC8C548438956211D1C4261C243A28
    9805880E664D53A0A70B9C4788EE23B88EB991D85426C093629CC539AA73BB29
    C595DA8A6A29A8A34DC49A8803BA4001480B5801A0BB82684D84F7C52B840023
    B8667B035210E6C2E208E7C5E722ABB017688D360080F98108D47751DD476974
    770D2F78926D539AA7354E63CA73514D45351442828B7F5C0B23402F00658224
    0BF60444C2E9EE98808A3461F3458721EFAE2C2FE1131179C8A531654649D308
    22004481670D2D47751DD474648EE312C1BA936A9CD539AA71B6539D64E3514D
    4529F2294FB00880C00439240465AB3344A002901D007B40E014F604470FB011
    916162C9CB4A2F24242F40169CAE784D285ECDA82BBA92884EFC347207C01D27
    E6A3BA8EEA3AD823BD7A4DAA7354E6A9CA9D4E0127A8A6A29A8A196E3D94C566
    20027A7C0B61809F1C5087047803A8130076055DA1D88B400D786CBC8D3C9B7E
    32580A05A6226F6A3BA8EEA3AA7A3BA936A9CD539AA73B9539A8A6A29A8A5A3B
    EEC97042AA01539881DC897C3B705011D88B3A48162680E883BE26B17A059097
    1510267C2D1156688428A09F4187E1EB61BD79016A3BA8EEA3B82A3BE4B4A540
    04936A9CD539CA7394E7229C8A722881F53B7C31500135F8237B0805CA02F004
    0295FC21D589A023F07300511349C40F9E073C4278397045A45795DE4E4481B8
    5E808BE510980E89C80B7B841715D8DB5E05847D08E391DE47791D8423A12A28
    A49B94E729CE539B1A9C98E1E453914F4512DC6AAB267E1004B5A2DC0B400258
    D185CC0027A6D07CF844FC59E023319FE8EFA3BE8EFA4D4724DEA73D4E7A9CA3
    402F453D14D2402A595EF6AC802D855ED8821A0568500232931013C068F6981D
    11180EE41C7046205682E22668464382120E5DC08887770C92850594538D4709
    8C676FA3BE8EFA3BA423B113F4949BD4E7A9CF539EA73D14F453D14415D45F17
    5213EA683EF202D16010851839F1DA02E508DD5892C83AF0DC290043428948A3
    92C5040C871193922400B4702104FC05469CCFF477D1DF475E28EA9294C49305
    3814E05385C53811408A0451B44FD8FD5AD70096A8D2BEB9E02C805F08E3A80A
    7021C11C03A90D5BCE044E3B9C6A90A1011F229C4E3870404605B950BB823512
    7316F519EE05FCA2D48858A4CF7151821011D08E84763C8E9090D9293053814E
    05385953811408A0452121E020A9CE012A4CFD3ADAE02C810F07240B20BFC23F
    0114B805C1190826082099E2378696481010F133099A811216EF241E49CC5EC1
    4C01CC2E9843D41D2156C33EA0440A860C823A11D08EDBB4758541B2BAF24982
    9C0A7029C4954E350208A0450228FA295AB6A362C463E02F46D5BBF847DAB42F
    8075956626206A97A01D4115503A008210F80BCD06B4162709C6049F1C2D2DE6
    4E4CF060422581C11D08E84775A874CF380B14293053814E0539B0D4E410E08A
    045022941E19CBCB1516F8D473F0024700EDAE01C9C49800CCF97B985FE1B75D
    0E003283F6E1A55BA00400400400400400400400400400400400400400400400
    4004004004004004004004004004004004004004404404404404404404404404
    4044044044044044044044044044044044044044044044044044044044044044
    0440440440440440440440440440440440440440440440440440440480480480
    4804804804804804804804804804804804804804804804804804804804804804
    8048048048048048048048048048048048048048048048048048048048048048
    0480480480480480480480480480480480480480480480480480480480480480
    4804C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04
    C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C
    04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C04C0500
    5005005005005005005005005005005005005005005005005005005005005005
    0050050050050050050050050050050050050050050050050050050050050050
    0500500500500500500500500500500500500500500500500500500500500500
    5005005005005005005005005005005005005005005005005005005005005405
    4054054054054054054054054054054054054054054054054054054054054054
    0540540540540540540540540540540540540540540540540540540540540540
    5405405405405405405405405405405405405405405405405405405405405405
    4054054054054054054054054054054054054054054054054054054054054054
    0540540540540540540540540540540540540540540540540540540540540540
    5405405405405405405405405405405405405405405405405405405805805805
    8058058058058058058058058058058058058058058058058058058058058058
    0580580580580580580580580580580580580580580580580580580580580580
    5805805805805805805805805805805805805805805805805805805805805805
    805805805805805805805805805805805805C05C05C05C05C05C05C05C05C05C
    05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C0
    5C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05
    C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C
    05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C0
    5C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05
    C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C
    05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C05C0
    5C05C05C05C05C05C05C05C05C05C05C05C05C05C04008010020040080100200
    4008010020040080100200400801002004008010020040080100200400801002
    0040080100200400801002004008010020040080100200400801002004008010
    0200400801002004008010020040080100200400801002004008010020040080
    1002004008010020040080100200400801002004008010020040080100200400
    8010020040080100200400801002004008010020040080100200400801002004
    0080100200400801002004008010020040080100200400801002004008010020
    0400801002004008010020040080100200400801002004008010020040080100
    2004008010020040080100200400801002004008010020040080100200400801
    0020040080100200400801002004008010020040080100200400801002004008
    0100200400801002004008010020040080100200400801002004008010020040
    0801002004008012024048090120240480901202404809012024048090120240
    4809012024048090120240480901202404809012024048090120240480901202
    4048090120240480901202404809012024048090120240480901202404809012
    0240480901202404809012024048090120240480901202404809012024048090
    1202404809012024048090120240480901202404809012024048090120240480
    9012024048090120240480901202404809012024048090120240480901202404
    8090120240480901202404809012024048090120240480901202404809012024
    0480901202404809012024048090120240480901202404809012024048090120
    2404809012024048090120240480901202404809012024048090120240480901
    2024048090120240480901202404809012024048090120240480901202404809
    0120240480901202404809012024048090120240480901202404809012024048
    0901202404809012024048090120240480901202404809012024048090120240
    4809012024048090120240480901202404809012024048090120240480901202
    4048090120240480901202404809012024048090120240480901202404809012
    0240480901202404809012024048090120240480901202404809012024048090
    120240480901202404809012024048090120240500A0140280500A0140280500
    A0140280500A0140280500A0140280500A0140280500A0140280500A01402805
    00A0140280500A0140280500A0140280500A0140280500A0140280500A014028
    0500A0140280500A0140280500A0140280500A0140280500A0140280500A0140
    280500A0140280500A0140280500A0140280500A0140280500A0140280500A01
    40280500A0140280500A0140280500A0140280500A0140280500A0140280500A
    0140280500A0140280500A0140280500A0140280500A0140280500A014028050
    0A0140280500A0140280500A0140280500A0140280500A0140280500A0140280
    500A0140280500A0140280500A0140280500A0140280500A0140280500A01402
    80500A0140280500A0140280500A0140280500A0140280500A0140280500A014
    0280500A0140280500A0140280500A0140280500A0140280500A0140280500A0
    140280500A0140280500A0140280500A0140280500A0140280500A0140280500
    A0140280500A0140280500A0140280500A0140280500A0140280500A01402805
    00A0140280500A0140280500A0140280500A0140280500A0140280500A014028
    0500A0140280500A0140280500A0140280500A0140280500A0140280500A0140
    280500A0140280500A0140280500A0140280500A0140280500A0140280500A01
    40280500A0140280500A0140280500A0140280500A0140280500A0140280500A
    0140280500A0140280500A0140280500A0140280500A0140280500A014028050
    0A0140280500A0140280500A0140280500A0140280500A0140280500A0140280
    500A0140280500A0140280500A0140280500A0140280500A0140280500A01402
    80500A0140280500B01602C0580B01602C0580B01602C0580B01602C0580B016
    02C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B0
    1602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580
    B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C05
    80B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C
    0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B0160
    2C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01
    602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B
    01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C058
    0B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0
    580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602
    C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B016
    02C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B0
    1602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580
    B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C05
    80B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C
    0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B0160
    2C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01
    602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B
    01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C058
    0B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0
    580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602
    C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B016
    02C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B0
    1602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580
    B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C05
    80B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C
    0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B0160
    2C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01
    602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B
    01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C058
    0B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0
    580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602
    C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B016
    02C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B0
    1602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580
    B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C05
    80B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C
    0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B0160
    2C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01
    602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B
    01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C058
    0B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0
    580B01602C0580B01602C0580B01602C0580B01602C0580B01602C0580B01602
    C0580B01602C0401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    0401004010040100401004010040100401004010040100401004010040100401
    0040100401004010040100401004010040100401004010040100401004010040
    1004010040100401004010040100401004010040100401004010040100401004
    0100401004010040100401004010040100401004010040100401004010040100
    4010040100401004010040100401004010040100401004010040100401004010
    040100401004010040100401599017BEE674F86DA7DDC017D801FCD801EAB17F
    7DFA5401F2BF789A062017A9F49A00E907F5FF7E56C2DB7E66CFBDF40ED3B9F9
    3617A26C85AD7AE536573A6D9802800006A00830FF0DEFA6217E9678FE220D88
    37A0E4839FEED939C0FB25DBD593FF3139F174DDF3C26138A8E3792F05005000
    00D401061D12A557B52FACBA0D8837B539F3D3E128F7DEFE7607D226AF88E0E1
    CF6F22FFBD31CF3E04014000035004187F086CBAEDAB2BEEA0D8837B559E6EE0
    068E6EE39C00E220FF44F3DFBEAEBAE74A6CDEEBF0D45B38599BDBB9E82869F8
    800000D801309A53207D0637E5FE4C60D28363E3EFD7F2120E4E89D9E64D3CF3
    4A557FE65BA4C4C4FFE3FD7FFE6799F9FF600001B0033E6F3F1303E831BF87F8
    AC20D2836224E20E485B713CDBEF23D8E6E6E9D455DBF32F29F8FEF00000D801
    06DC0FA0C6CF5E59FED834A0D8F8B53CF6D44283770039D3771CE0079283F9E1
    BD743AD6DADB7ACEBE6607D2E49FD8256A26EEF7C2D7B29700500000E05EEA08
    CC0F42A6936D078241BD66DAB9645F259B75633BCB947AECEFA77AD8A20028C2
    38B30F7B22D2D06F66B47BAF47D6FAF9DECBD5CEB55E2FD6FA64D99F927A8E49
    DE7EEC01400003817BA023E7C915042C0837AD168B9E8DDF2F98B80080294C05
    AB771CE014A873D9767E550B57DD37E5B86790004921D83621BA41C995683C4C
    6A67D45F4C53888D0029D0EC1B106F41C9EA2E2A646E46F56E7D5A2F98947F00
    2FB0EC1B106F41C9A377DCAB177A010054F80B46EE39C02A78E7C45C73D17A9C
    89B27981D3D470F1400C9B13DEE9F2F7A60DEF889CD87B1591CEED2E31E83D8C
    DA358F0900036C0EFB13F57E4041BDF06A61EFCF8A339D6B5DF8CF2F0D232688
    DA0071789EA76C5C5A0DE8E90C377E3FD8EB200802DB0050DDC73805501CF0C6
    B173BE28A77E7EF79BF2FCED261676DB68F577015B0D7C0212790660CC834A0D
    8C82EF0FB9D651E0E7A5B8E364B8F20077B3DF06C41CFACA3CEA36D176C253D6
    760FEBBDFD0DF36DA3E8DC058035F7484A54A2499926949B13FD9EB28E6EE6EF
    C93771CE39EEF39EFF82C3E3FF0CD20EF3FD6FBB2712B276D36C0DB161B91002
    6D9A401BC1D7B85FEC195EC424981263499926949B1368BACA3E4FDD5DD3BFFF
    D3CC79392712A94D002BB93193324EA9363BE3DDF126B28FA8D46F7FB42F634F
    CF7389598753F3C4FBD1B003BEDA003703B140BC606589909260498D2664FE1D
    26C71FD8793B4A39BB9BBF84DDC738E7B18E7A697EB3CF993E388F993ABD8BF9
    6E0CED2A4F0D8013E4F4803703B3E0BD18659E909260498D26649A526C4C1FDD
    28F273F991831875959F2B325484C43D8013E4F4803783B800BD6865AF9092CA
    65A9CB5C934A4E7958DBE51E7F1DE29E33DBCC37A6759F764ED2A621EC009F27
    A801BC1DD685EC232EB084A3028C6A3328D2A363D5FC3F8DF28E6EE6EF28B771
    CE39F1839EA0FD64EB07657F0DECFECF7CA9C951DB6EE800AD55DC801320BDF4
    3588109400A30E8CCA34B6D486D28F57B1473D1C9461ECB330E3A002AA8EF600
    9C05F0A1ACE084A005329A3B68EAA39EB017B4A3D7A515BDA9437AB27E3A6B6C
    52F55FB23AA881ADBA0024940C016D0BE5035BA109400A30E8CCA34A8D8AFEE7
    6947377377F21BB8E71CF9D1CF53DF5D51AD4CFBD44A9EB41DAE45D1DB1E00A1
    C6C00F5C35BC109842068C3AA7FA34ABBD546F72C47D551D2CFA387655D55352
    200CB5B96E06BA82132440D31DA3B69B151B146F72C47D6CEED8C13239E977B7
    0543DAF24AA536680151636007EA1AF0884CC903461D6232CD2B362CDEE588F3
    77377C15BB8E71CFB91CFB41FBE415CAFDA0BB1D400001600B302CC6B332FA2B
    B41FE588FBE955ACFDAA5EAD4F20068000BB15B63576497E8E5B6BBF21B9623E
    F6777E44DDFFDC3DDE52800002C01660598D6665FA3966C59BDCF11E6EE006AA
    DDC73801D49CF7C82F54FD199FE17ABF9DCF96E750000070B32CC6B332CD2B83
    F9651EFD22B59F752FC54A7196400000E326F6755B77B5879651EFD22D66BF7B
    B8F79FB5BC497129400001C4A2598D66659A5788CCB28E6EE0073CDDC7380039
    C73800947000004A38009474A38006EE39C001CFD0E71D40070B334ACF9ADEBC
    ECA3A52800E25134CA3D2EF5EAA51D2940071289AA51EAB7AE6EE0075EDDC738
    01E01CFD38FFD0E7D19D5ECA055614E949CEA5A7539EFA19A7947EBE8FEAF9B2
    B3193F290E8186AB7EABE3DCDD6CA3F5F47FBCA3EBA52A372C58114EE89CEE5A
    77947BF47F7728E6EE6EF326EE39C73E0739EE0FF37269E7E03ED05F506B0029
    F1484C76F6B57B8FE651EFE6CEDEF53C3E600E6618B80BEA0D64C53E8909A4DE
    D5ABAECCA3EC3BD765E3DEA63730076B0C5C05F506B4E29FC084DCEF6AE76BC5
    28E6EE6EFEE3771CE39F8F39F2AD1F7190A72AEBCC1F93D3C00E5D8D7853C85F
    686B842983109E9197307EE7CACD8B37B9C47DBE9B59F6A8AE46B5AF1AF0F82A
    8C0183D98031429E42FB435C714C7884F70CACC6B95FB66B766171039DEBE974
    DB3F67EC865E232CF0A4EB8C8A52A800483CC018A14F217DA1AE38A662427DC6
    566359BD7892B362CDEE711E6EE6EF116EE39C73F6273DC1FE60CC93CC0129A5
    FE8AB7F7C8179047905FC82F3714E00213C8CACC0EA3B5999D425B707D4A3DFF
    7365A71CC5CAB9DEF98038D7FDA84F1DFA1AEFFF405E811F017F50BD3C53A448
    4F832B303A07599966959F377DBD4A3F0DC578D83311EE5B6FB85CC019468DB1
    FE88D25DE80BD023E02FEA17AD0A79B109F065660740EB332CD2B362DB8F528E
    6EE6EF1C6EE39C73F9073DF727ABB7B2EBA53591A0BD673005823B05F608C605
    FF014FDA427C0BF608EC01DE00AFF1D8D93571CDCA3D194DE17CF5E27583B91A
    E4C000021A00B94DB363476F9F3194E85B0DCA3F347EF4B3F3113501DC8D576C
    B5CAAF7DF600B04760BEC118F0BC245308213E05FB047600B94DB362BBE13DBB
    3333366EE6EFD33771CE39FD239F575F5E62727AF8EDDD087738B600EC53F011
    8C0EC60BE54533C213B54B9346F7D0FB37BB392FBAB79D6F3011E6197E4A8654
    ACFA02FA014D1C84F02F978A4BD37DDF259C9F3FD41F4CF53D58BF23FCC047B8
    65B007B127E8233C1DAA17D58A6AE427B38BAD8A4BF4A2CDEB390DDCDDFCA6EE
    39C73C10E7B5B7767F7D9CBE4A5F7B73AF28F25793DBF047DCA1EC803C017968
    A6CC4E7DF1DF3A4F9DDBCB3FC7019A90974EA959DE1CF47F368F94E19B50CC02
    FBBE3DD0078C2F3B14DA89CC047BBD1CF47FC2F1A05927D9B35AF969755F3444
    895FE078BF8E1D59B47E7FE2201CDA866017DDF1F2003C817A28A6F44E6023DD
    E8E7A3F99D9D3F62DFF0E43773778A3771CE39E5473F2FC75961DBF1D2F9940C
    3B73BB159362F103353FE4EE005A57A007E42F6314C5835E8CB4DA9DC6181863
    66742994FB0E410C37B0E4C39F3BDB9B38C94DAD582CD39D496561E1CA8DC00C
    3DB7C01FF0BDA053280D7A32F558E18D9C70D1DA8FAAE5F0E6F17D39F2D0B367
    1929B817E51FF2A1D820CF4DF001C9D7A004205EDC299586BD19737E5CB7B30C
    0C31B38E12B354120AA257FCB3CF5679BB9BBCE1BB8E71CF5483FC7163AC8BB7
    3E2E004EF007E0B00FCAB807BFF00769F166F43180FB6B72F407F81FE4C7A2F0
    05BE028E801A700454C9ED03F7F607DD7BFB191AB7D9F1BFA847990DE4B8293D
    B7FDA6F903BE7A93F22BE6C4D242D3B40F38A74641BE40F9C47EA24481FAD03F
    665A7681E6C7EA0837E81EA11C756F435F4246F42070E897E81FE5A615D0202F
    46DF681C49983646BA837E81FA11FC217A106EAD03F92612081FE5A48207F0B4
    ED03E5FD7A10B0A0DFA079989CE68F4911B106F3903EA9F18540E48B4FD03FA5
    A7681DA56E69D6A41BF40EA65A6DE19A50FCFA9361BBD1039D61BFA07B0B4EA4
    0FE969DA0736D2D5CDD506FD03C1061238807BDEA78081C03D4FD03FA5A47207
    F4B4ED03CC3068D06FD03F1FED646FD06FDEA6D91BFA952A5E3DF3405A502070
    2DFED03CFDB75C837E81E87D17CEFEE834095103EA633FA07F9694C81C4B7E44
    0F6C834481C6A0D125440E9EA5E632D35207AE41B6A07C124481F0D03E6968D9
    7B65ED97EC765F531C2BFFA1D9D11CB6FA9770B6772A1A54E83F41F823CDAA25
    CCD0F9D14C7D110762016616BE6FB225F89B00B826F8FE58CCA09A9AAFACFBA7
    B2B99E7D555F5C9A0E7EDB90E73EAD951367D85964701367C7D50EB5E8DCDF74
    D96985167F4D86AC0B4CED6ED2D2440EA259AD8D2D51D60728F4CC63AB6BAB2F
    3E96E05CBFD8DF4D33D2B023860ED22E2137E7FFC7724E2213603B696598D0E5
    B9CEA332E5999AEA5E77FED39B81B33273E56A52B1E9529826805304D949599C
    7C4DB04D9A6FDA5A7C78F271C36EF4BF77195CCE939F34F88FA1FEF01785A619
    9A903D76BF758BE31D676E75B109DD966D3E4DC0EC445E283C457C776D013607
    6256794659745BF7C1C09D9B1B70E5ACD6EC712BFBB7A1791E09B121CCBDC03B
    80BC02E29AC136076256B87EFC32863723B7F0FE6E1DD93DF67CCF9F9B67D0BC
    BE676302F041B6C9F3C5B59E80556A77106C5CD848E2CBC137B3F27F555C7D80
    9B0DFCA11C60303372E970FD82018CD3115C41B03B8D9F4E8D0469DDA7EEA070
    006E8877139A05D19689B67AB5E169B74DAB8DF7DFD9F7D60D54DB863D5A7BF0
    14106816996FB0B4A2C563AE16D935D6EC0BBEB08CC3ECFE6330A45D912A8FEF
    FC361E08F5AA7DF1226D76C106D136C26D767B1EA0B6A084C100D42A0B8F6463
    640363BE89B52AD7D3B2C1B213BF0AEC9E6AACD1DEB318644B640F5766CCCECC
    0C33270006CB4C106D0EEFC9194265F8FB64D615B299AF1748886A77F0F1BBFC
    8BFA7EFF7DD2F8542C777637FEBC186D92026D31F62E456EC4F7D05C411A11D7
    C1A3ED3C7679753C893FA806FDA1100685E2CEB5826CF477BF71FAF5E16ABED4
    7A04024836B0DD7A9B237B3D196CF4EEA8F93B64D9E2DAA9D136F14A590FCC54
    79E178FC5BEAF6972FC3506B1FB7FBB28E3B3C5A456C96C9009487E99D047180
    01B505A2693C8D076FF350A0781818BAA3A4FEF44DA486FF65B10367AFC503D6
    4BB237B3C5A6DE5AB8EDEDEBE6DCEDB26D54EB2B932E9350E57E6FC3F2BFA3EE
    7506D61BAF524956D54ECF8EAE0DA8BD3EA5395B207A00D41B512D8F5A6FEB07
    DB6A932E94806AEF8503DB5E94ECC84DA5A6B07D9B8A7EAEB5BCD03D91BD9E2E
    AA9D983F487FEFFDC775DB64EB709C11EA0A9F0AFA14F20DCC379EA7791334AB
    84DD26DD21BD7C738A7378B3004CEEE5F7D213C40F4897487F06399171DDC837
    30DE4FF88D9C04E936E8E9390FA3F68B1E75E2753DC4F27B0A5D3C5D553D21FD
    26797636BA7EFDB7DF906B353BD964E9FBE2D6744580C563AB753BCDDE757DD1
    1FAF82765739C49BCEA0479D27077B1AFD8DAEABEA325A3E6A4EFC52854AB5E8
    6FC6DF30AA20DCC369937E5A1D553D3A7054BF5FC73F9FAAFF1AC32C8375B874
    D3478E6EC234DEDA8EB2CC20DD0BF9E2C7846A5FC52712917288079DFC72D7A6
    11BAD328C9694014C23478E6E845D535441B9EA7157D3C5CE5E95294CADC19A7
    E8E9D90BE7A9D237D3C54B70EC85F66DA6334FCF53A46F92DE51FD6E1C268D00
    8D06ECD3D34BFCB3A6ED43C4DD7AFCEABB59DEBA41ECD3F673BB531D42BD85BE
    9BBEC0CFAE69E8D92884F4F151E143F34F1EA4FA46E4D97F45CAFD8475E7760B
    A5D97FB6AEA52EFB191D8049765FD20DD8DA9F1371EF2ECBF7C26B8D7E75871B
    74D97F5B87169954759C2BC835848F0AF417B837E564EE15CDE4000000000316
    009F6700100401005CA1237259EF4FD0000000000500070601004016A5C1DE7F
    AB00000003C800EECF4AE0E55039317335577B010C900000D86E706000000063
    455ECE0E72BB27280401004011C1800000017B7079E9D1A00802008023830000
    00026F0E0E1AD5C008020080238300000002F88E0F717340200802008E0C0000
    00038300802008E0C000000038338300802008E0C0000000C86E0E1AD7360176
    CA25016507503B5011CC0512D9446213EC36CE0C0000000D4FB83D63BBE5DB25
    A00BE81740767104A023FE0444B084D09087060000000621C1D6FD0DC8010040
    10047060000000553D03FE1EC40200802008E0C0000000FA164EB3BDA5F6E0FD
    0CA7005AFC7E0012C212A8007DB830000000000A0033E02EDDE83D1C04000000
    074FC48706000000000130028E0C020080200BF005CE0C0000000003F00057A0
    100401540A72CAA30D84C37958008C700FD0F01FD6800773676DA0638177FE37
    401EF07FFB14916B4000000000128C018E689AA6B3DCFF477279CFBDACC00000
    00003CE001F77E50DE1F72AEE02B1B8000000078208E55DCF74AAC0000000F48
    0BCF5FA714AAC00000002AB2AB00000000AACAAC00000002AB2AB00000000AAC
    AAC00000002AB2AB00000000AACAAC00000002AB2AB00000000AACAAC0000000
    2AB2AB00000000AACAAC00000002AB2AB00000000AACAAC00000002AB2AB0000
    0000AACAAC00000002AB2AB00000000AACAAC00000002AB2AB00000000AACAAC
    00000002AB2AB00000000AACAAC00000002AB2AB00000000AACAAC00000002AB
    2AB00000000AACAAC00000002AB2AB00000000AACAAC00000002AB2AB0000000
    0AACAAC00000002AB2AB00000000AACAAC00000002AB2AB00000000AACAAC000
    00002AB2AB00000000AACAAC00000002AB2AB00000000AACAAC00000002AB2AB
    00000000AACAAC00000002AB2AB00000000AACAAC00000002AB2AB00000000AA
    CAAC00000002AB2AB00000000AACAAC00000002AB2AB00000000AACAAC000000
    02AB2AB00000000AACAAC00000002AB2AB00000000AACAAC00000002AB2AB000
    00000AACAAC00000002AB2AB00000000AACAAC00000002AB2AB00000000AACAA
    C00000002AB2AB00000000AACAAC00000002AB2AB00000000AACAAC00000002A
    B2AB00000000AACAAC00000002AB2AB00000000AACAAC00000002AB2AB000000
    00AACAAC00000002AB2AB00000000AACAAC00000002AB2AB00000000AACAAC00
    000002AB2AB00000000AACAAC00000002AB2AB00000000AACAAC00000002AB2A
    B00000000AACAAC00000002AB2AB00000000AACAAC00000002AB2AB00000000A
    ACAAC00000002AB2AB00000000AACAAC00000002AB2AB00000000AACAAC00000
    002AB2AB00000000AACAAC00000002AB2AB00000000AACAAC00000002AB2AB00
    000000AACAAC00000002AB2AB00000000AACAAC00000002AB2AB00000000AACA
    AC00000002ABE0280000000007C60090E73AD73CEA30DAF3139D80091F00FF87
    01FDB7AE79CBEF02F10CEE940C702F11D16803DE0FC980000000000000000000
    000000000000000000007A100679FC40000000F9C017B80000001BC004E00000
    000000000000000000CF3B8F73F626D00000000378038FFC4F90F76A77C00003
    F277F90530B00000A9617C82D740000000F000990528A00000FC917B277F0C00
    000007800277C00003CB3BE514000010985F48CFD4000000078004D3BE514000
    010A45EC9DFF9C0000000F0004EF8000079677CA28000021B0BE9DFC56800000
    00F0009A77CA2800002248BD93BF2600000003C0013BE00001EF9DF28A000008
    AC2F988CE80000000F0009A77CA28000022C8BD93BFC8680000001400277C000
    03E33BE514000011D85F311DE80000000F2009A77CA28000023C8BDB3BFE3000
    00000F4004EF800007CE77CA2800002530B63E400002BB50A514000012E45ED9
    DFDA600000007A00277C00003EB3BE514000012D859050000020A000016048BD
    B3BF7F00000001EC009DF00000FFCEF945000004B617C82F9100000001EC0132
    0A514000013A45D3BE000009DF000004EF80000277C00000004EF800005889DF
    28C000009CC353BE000015D3BE5140000157233F840000000F600552800007D2
    45ED9DF9D800000007B00277C000021A77CA28000027B0BE9DFFC60B11F29F06
    11E028000000134EF945000004F917B677EE600000007B00277C000022677CA2
    800002A30BE9DF8100000000F6009A77CA2800002AC8BDB3BF2280000001EC00
    9DF000008C9DF28A00000AAC2FA77E0600000007C004D3BE514000015645D3BE
    000009DF000004EF80000277C00000004EF80000277C000024542197F7FB0F72
    EC700401005401277C00003E8C2F905F2BF75C5496100000E1C792F4F1A277C0
    00020245F4D2C1DD9D81BFBDE43C23000003871C7CF1FFCC3711869E377DCF86
    7184C8A0080200B8F9E3FF90530B00000AF617F7BDF7F41480040170E38F930B
    0000082617A6990C967BC17AD814850000063F9DEE5C7CF1FFCC363E9367FBD2
    388BF85C81E64D6C02DA2028D5E09F1F8F9E3FF90530B00000B861786C672270
    89613C40146FB688915878F930B0000084617A69665CD817215D3DBDA9600000
    FE4018439E3E7FE6107243F30AA003562DF42B2E632900802F3602E3CFC3E6F5
    510530B000008861781E0F15F3F6589201005000171E66160000122C2F4E004F
    E3649996A07D3492F00E36E08DEBC078D7E01E67A5E6818E05E75B1D007EC1F5
    F800000019D4012D1780000001220089400B4019CCE6CFBEA010221E3C87AE40
    0B40021C43887BE02F329E558438879321FE480105495886399B08710F9A21DB
    71E00694D97EB843887D110F378015558BBFFCBC31344EB7208710F2643F0200
    0D25BD3E24D4F88410F1E43E0F00317ED58F4928E21C43EC487E04001A2B797A
    A10E21F6843D020053D61E2BE86F93F81E77C021C43D210FC080037A7A6FBF3C
    8388410F2A43EC0002D0008710FBB21FC1000B40021C43887C0C2F39A710221C
    4388710E21E2B002D0008710FBC21EE5002D004F56E20443CA90EF0440000322
    0BBF6B00000D0801B2000000037B0033CE9727A6B79CED0050780740701F04E0
    3C6B3807A3E77D9863817B86DB301FB07D5EBF1906ED1B801F33E9857FD7FEB3
    FFD01438AB8D0D6A7B06F6C96B880386DA67CEF4FFB29D76B7564F850AAF861E
    D2C8CBF95EE3CD76C5BA83F9C0BBE3BEDA119853811E1E80BCB5B8FDB60D55D1
    39178F957BE06BD4EB56007700268BE1DBF414A380ED776DA02B4115A2E046E7
    E014C29200B83BBFE3796A68BEF055FB21AE09C3FB82588BCB540D73C8BD61D4
    70AEA4EB0DCD4D6B3C82DF7F9A00AB781702FCEDC0BFE57A703B82FE72E0A45F
    8291C6ECC7376CD17F18AB0D7CC45E20DADBEA9B05ED5ADAF08B1C7F18B74B7C
    5D005F41DC0BCA6F16F85E80EF21785186AAC8BD186B8EE4FABD75CDF766E68B
    CA14D81F02946D8C880A38C3351529A05F7AFC06B00D9409345FF42AE031AC48
    8A7627726F3FDDB8B2400A10A4FE2F9A2FEC7D9605A52F7A94E0143382F6F89F
    F3E04B3922BEE02D7FB63C84777F8F30EDD3C14DB2BABE1DF5323365F002AE42
    1AF6BB351EABF41D03708036F2D0070239A6F002614E4FAA3B02E89A2F8CD2BF
    7EC327C6BE80142F7CFDF4D1789157450D7BDFCBD78FB6A5D0FD0039A2700280
    5393EC471ED63D022F09B405468BDEA4D178B1575D0D7BF76E453D7F9007ABFC
    510C60BA000E6A771B95CF3AD5B68B951795157820D70181C6F74FE3F3DF99F4
    A7A6D22CBD54D49B8D8D3F822FA8203D08ABC6C6B1749B5E7217CCD7801D3553
    047003B22D3004611EF457D9FD587C8BD4CABD506BFE62892F95D46238C01C45
    4C011847A1153F5C5C245EBA55ECC3585A1D96B0B9B91C549F5CC8E6FA61B716
    E7FD9F117F1359DA7FD37DB5C7767BC80B30E077B08E80BDC6395A1AACBE2BBC
    7FBF0DC5FC4C42701ECE55EF435E07757D744F9803A75E404480BA047AFE64B0
    FCD72B32D5ECCF376CEA87F62FEB33C7FB01702F823682FD3B358BFD118DA118
    67C08C1A651BCEC3EE055F8035EA9486765B1561C5CCF0894C26C800EC34766E
    FA63288BD6F00FE15AD9D8A58017FAEB7401480BC21ABD714AD7C52E1298BFBD
    9561AE68CEFBB6411D324B20225007047B2572A2F15CDA6ABD61DE57E801451F
    8ABA545E0E5586BCA366D2A4CC764A8DE2F70B2FABBF6170E7F0730698058C6C
    01500B921AFE2F9AC470B2F8D157F38D7B3B9CA7396370EF801C08982E608C20
    0EDD9F63B339CE759E45E21E5F9D96B9DB9EA20282CF5E76B7CB7B9B2FBE957F
    78D6D8DA600728AE164FE57A8669BA173CCE4095FF9D9202BBF00B47AAB75D26
    DBA62FF0455C206B0A00795FB10D722F0E0144B0CB72951572F3871C65353BFF
    C8E560023DC0395B80FE474BCB2B96E05EB9960A818E05ED1B35007EC1FDC240
    000000000000000000000000000000000000000000000000000000003B31A000
    0000000006C0002D800000000002D80000000000000B600000000000002D8000
    0000000000B600000000000B600000000000002D80000000000000B600000000
    000002D800000000002D80000000000000B600000000000002D8000000000000
    0B600000000000B600000000000002D80000000000000B600000000000002D80
    0000000002D80000000000000B600000000000002D80000000000000B6000000
    00000B600000000000002D80000000000000B600000000000002D80000000000
    2D80000000000000B600000000000002D80000000000000B600000000000B600
    000000000002D80000000000000B600000000000002D800000000002D8000000
    0000000B600000000000002D80000000000000B600000000000B600000000000
    002D80000000000000B600000000000002D800000000002D80000000000000B6
    00000000000002D80000000000000B600000000000B600000000000002D80000
    000000000B600000000000002D800000000002D80000000000000B6000000000
    00002D80000000000000B600000000000B600000000000002D80000000000000
    B600000000000002D800000000002D80000000000000B600000000000002D800
    00000000000B600000000000B600000000000002D80000000000000B60000000
    0000002D800000000002D80000000000000B600000000000002D800000000000
    00B600000000000B600000000000002D80000000000000B600000000000002D8
    00000000002D80000000000000B600000000000002D80000000000000B600000
    000000B600000000000002D80000000000002A16C00000000000000000000000
    0000000000000000000000000000000043C0196539AFBBF90CAC0047B8073AF0
    1F68F01E33C03DCB3C15031C0FDC770A00FD83E119C2C0200802008020080200
    80200802008020080200AA0058FEE3C7F0BC02AA136F8D771E03EB81F904867F
    2D69328B8BF8D8F1AD6D45204B3E07E7D40995D8FCA00B6F4CA382FA2594015A
    12CA008995DCFCDD532968BF0259E7265812C26580653C42605329F8BE5359D0
    265852CE450995789B09F918110BE4920DAA658D2C8D0880543C346110BE4D1A
    DB4529A468960040100401004010040100401004010040100401004010040170
    E32FCDF45D1FFBF9BDE5BDE79DC66FCFCDE1E00000005B8004FC00000005FE00
    4EC0000000D120086744F10004010040100401005A7017AE00000006CB0074C9
    7F282623D36AB63E27C802D9BA6FEC60E00000002884789E3FC3D4E754000000
    03DB803A33EC2D86B720080200BF2015272040804014D9239080000000848015
    F4E6000000045D39EFF00000002300032B6010040100401005F400BF78000000
    1318014E6000015DA73FEE000000055E008A000000000000000001BF0009CB00
    000539DD800000006ED0029CC000014E60000000000000000029CC0000000539
    80000000A7300000539800000000000000000A73000000014E6000000029CC00
    0014E60000000000000000029CC000000053980000000A730000053980000000
    0000000000A73000000014E6000000029CC000014E60000000000000000029CC
    000000053980000000A7300000539800000000000000000A73000000014E6000
    000029CC000014E60000000000000000029CC000000053980000000A73000005
    39800000000000000000A73000000014E6000000029CC000014E600000000000
    00000029CC000000053980000000A7300000539800000000000000000A730000
    00014E6000000029CC000014E60000000000000000029CC00000005398000000
    0A7300000539800000000000000000A73000000014E6000000029CC000014E60
    000000000000000029CC0000000D5D3980000001B90029CC000014E600000000
    00000000029CC00000008BD73800000011600D5DE800A7300000539800000000
    000000000A7300000002BE9CC0000000DD003E73D0014E600000A73000000000
    00000000244419E739B3BC459CEC004D78076A701E0DEC679C77817EF678BA06
    3817BD6F3401FA07D50007BF005B5745F98562DF560202E14F0B0558003FC7FD
    F37CE36178FFAF3FC6785E3FC7F800004C29FBFFA835FEADB26E5FC7FCF5AB7C
    FBA822DFB9AFAFCFFE61FBFB29D1E1F99AB6E0BFD081A26C93E817C085FADB6F
    6FFDCF8754A7F43D002138923FE077016408E40EF0838CFB4D5A4CFFA78BFA14
    408593A5219E178FF1FFABB62FC0C8D7E3198AC4E6B9AB370795E7317D72AEB7
    E74817E0CFF19E178FF1FECE18011A239A9EFB753E17C145F83E1BA75C0FCBCE
    F8CA131F0C17F14B11C317647271486785E3FC7FF4C7F4BBF6167A64B71EFBFD
    7247F9471F6517DB4FF19E178FF1FE9FB0025549F21FF9287ED85B3D312943E1
    7CB8BFA88A436E25D4EA8535615BD480B527DC663E4EA5D5002788652785E3FC
    7FE471F923FD1B0BF0D17E1CFF19E178FF869FE9FB0035B49E59E17C753FE5FB
    5FC4A82EE3F6517FA94AA817C2C9CC7E5A94983A9190EFB3044C56FD805F4A25
    D5B59C83C8028F31F324FC5CAA96BE35982DEA999F332651C05E3FE574333E6A
    8CF617F5E2FC39FE33E87FF3C20BC7FE814FDB3EFB671F50200BEDC5F1F7DFD3
    0D2005E4DF785F762FDC7BF7F3DE6C57859D31FD0D240025326485E937F6017C
    D38FED904F38FA827E9499C02AB19F3E664F05F5A7FC93E76A7FD61F929FF2FD
    9F778CEAE98FB48BD31FD3DF5BFAFF9E87FBF8F96F482F1FF647FD1815E13F24
    CFD6B65713402F052505CB81A17DB05FDED7D0A7FA8968F8FF96C567C7E17ED8
    5FB2ED3E0FD2B700136A99C17C2CE6BFD5BF9C01A2B66906A6E3ED42FBC71FD4
    996749F489FA29326E48678FFB33F07FE895FB3C2E59E0BEE02F4C7E85F3A405
    6343FD8F59D266ABA2B33FEEA2FB277F76B0FE92FEA4FD4BFED09FEADCD4EE66
    05E0A500BA4D342FC20BE95B7DA9C785F642F832158D696DDC01DE7076563A58
    B502F859D31FDD5AA0014F653A835CC6A731F7217D94C7D9D27EB27E9C52F099
    FC5A5BA0548178FF990970815067EBC2FD5A1FEC86FAE63FC22FD67FD0BE7600
    3F131F6F4A56D43FB2932DEB72A42FD0AFFBA9FF5E264DC7CC1740168695B1AC
    85F6C2F9E17E8F23F67B5FDBD7E71F83F7F85FFB17FC7FD30D595E2003FE4275
    77A94B602FB5C9C2CEA4F9FF613D7B705364C6570D4E93F122FAF31F71126E69
    3EC13FB6948678FFC5B0FDA810FFB0E3E895F8838FFE8BEDD7FDB9FF609FDD9D
    FFF19F8BB5F0BC7FF13F8EDAFD0CB39D271F3371F305D205A9A56E1605F702F8
    3C2FC269BC2FB7D4FE8DD53E8B45BA401B5D000DC0EE16747F9FE4161389CAFD
    80D4E63F162FD3E77EFC9FAA2966CCFDE6605E3FEF4FCC659FBF8FF3FB3E78E6
    CCFB3EDA2F677EA27E6815B8CFA63BC2F6D7F344FEF7496686782F550FD4000B
    F402FC4F6BE921DA559E17DFBEFB1AD8BCE08F8C00CF74ED8B83B0E489395061
    C2CE9BFE30F9DB5A005258AD06A949F9F17DFFDFF7B49F984FD2296A0CFACC9A
    0002FBD98FC2FEFED13F59F39C67C0306C5FBDAFD263FB215DD5DCE6BF6D17E2
    2FFC73FC6785FA43FF08050F3FF7B14EA13F005A79376A0505FAF17DAC2FC637
    DE17C2F13E352096EB10D401DE767488F3BFE1677A9FD5ADDEA4D001582F811E
    42F40D53D7FD30BF1DC7E1FDFE33C2F909FF854983FFA63FA81ED7D9FD2BF9B8
    2FC00BF5331E33C2F1FF163FED3A78577F92170BF445FE9DF7C550F85FA22F9E
    A9AA9263E8024593ED975297023859D2BFC4270039AFD366B405D0117435498F
    D68BF0785F19E178FF1FF560BFCCAFEC3C2F960BF587F8CF0BC7FC78FFCA6E99
    0E3F342817DD75459E17E82FFEA22B83EF803B575E4114A4FA199C505306BE41
    17435498FD78BE931F8365D9FE05C78CF0BC7F8FF617FD22FD71FE33C2F1FF2A
    3FF31BA6438FD00A05F75D537DE17E88BFE6FCBF986212006F1EA7D7E43BC2CA
    B5FA8038A8A017740150D50E3F022FE35B9C6760E22805D361E70BE33C2F1FE3
    FEB2BFDCF1EFDAE3F96283DD617E5C2FA59FE33C2F1FF4B3FF31BA6538FDD850
    2FE7E5DF07FA59F44F7F2A8D2A1F0BFEC2FD66B36722410008A4725150833CB6
    5A1074C7D400EABBE007C08C586B7E2F886B2C1BE3A18D0074DC8CF0BC7F8FFD
    C802931FB9178FF19E178FF19E77E17DD22A4777F9773E17EAEA7ED88E80195F
    536E1EA6DA016B0A638C5E92CFEE63300235C03B0F80F0D380F15E01FED8E068
    18E07FDDB8D007BC1F70800000000058600B0800000000000000000000000000
    00000000308006080000000001658035B0000000000000000000000202007820
    0000000017A803580000000000C9801FC000000000068400AB80000000003560
    043E0000000001B4803EE0000000000F08016B80000000000000000000001038
    03D90000000000FB4018E319AAFE93E63300249C03B3380F1A701FF5E01FED8E
    16818B81FE45007BC1FC59D58B74EE757FF608BB28BDA68EDC5D795DDE7A53EF
    260183E7A1785E1785E1785E1785E1785E1785E1785E1785E1785E1785E1785E
    1785E1785E1785E1785E1785E1785E1785E1785E1785E1785E1785E1785E1785
    E1785E1785E1785E1785F9017CDEDBFBBEFF6DEDFF7B6717F3FEDBCD737D9132
    00000505628BD0005E8002F400CE14E67A269D12002150005E8002F40017A000
    BD0005E8002F40017A000BD0005E8002F4408002F400BE5EF92FC9FFFE062907
    702350359C20C89082F45E8BD17A2F45EEA17B6DEDC00B5A7006785FE06BE083
    3C59F82105E8BD17A2F45E8BDD42F7D75BC01ECE7006805FE06BE0833C59F821
    05E8BD17A2F45E8BDD72F682F1006539AE9BF01D400D61C53F0342F45E8BD17A
    2F45EEB97B9F6F007FC9C01583BE80330358714DF0D0BD17A2F45E8BD17BB45E
    D9B6F60BA44B7052800CC0D5620FC0A744342F45E8BD17A2F45EED17BD403535
    E001C14A00334355883FC29D10D0BD17A2F45E8BD17BBA5EC1B0FC9D0AEF0056
    0BE047D0BE886BE882F8B2A4420BD17A2F45E8BD17BBA5ED3BE1B6CE00AC17F7
    C05CE0D5F007808417A2F45E8BD17A2F774BDA05F801CF3AF823CA1ACB8A7006
    85E8BD17A2F45E8BDDD2F69F0C57F0DE900340359A14E00D0BD17A2F45E8BD17
    BEA5ED60097E7007D08FAC359F14E30D5E78A65A2F45E8BD17A2F45EE897B861
    21AAB3C0709C0129E01DEBC0794380F92E01C02702D00105F8E803DE0FE1D73B
    5D08002F0BFD85FCB2FA52695FFDB43EEAB53F2BEE99705FB29115F10005E17C
    90BFA81AB9B7EDD485E17CB45F5AEDA93F4A5FC5158D6CE241785FB217E365F5
    27F943DBF7BE531447B4005E17EAC5F4B2FEF79336CEF62FA2FCF53E1785F601
    7FFDAF85E1799785E17FB9DFA57C2F0BD5BE1785E65E1785EA7F0BC2F0BD43E1
    7A9FCD7C2F0BD43E1785E9EF0BC2F72BC2F0BFE42FF39EAFFCD307FEA5FD5714
    351F6E750385E17D845F07449267DB80A19E27F9AFF7F830821ED80E165A56D5
    EBC08A85E916F4B405E17E78BCA7C2F0BCFBC2F0BD3DE1785EF8FD73E17ECC5F
    B200683AAEDA1785FEC2FC3090D8591F16138026BC03C4F80FE7B80FE2B80704
    C32B385ECEE0FC25F46A03DC008FE32100802008020080200802008020080200
    80200802008020080200802008020080200A9016C53803DFF87FAA823817242F
    C97BE803D8530AC89367DDEC004918D81F3A6D6A4C3803180200802008029980
    B988D600836208E4E6F093913C7D3C781CDD600A3A48014D6F2F85F0876C06CB
    7EAD5E930033D1AAE267E3E7E478424AB689C92E2FD2974F7986D339F4CF9D65
    B72EB000200A7B13252F680271CDEDCABE5C38499E749B731B4DCDD962591EBC
    3452D4076A39BB1CF4EC55B282B25D7188804016A6413CAE6F65B4DB642A3568
    EB55CDD4D1BFA5CDFE73D0F5D787B9FFCE7BDD9BEFC0030E00802F451BEBCDB7
    F6FFD07E42AFB0B9ECE87DA8DF6660FDA8DFEF90DB9CD3DCDCC6AB9EF46920C2
    C0200BE847DB14C91DB59CDFDF4F28E8766E7326C9A9A87FBB9CD8D37EBD621C
    D9CD9CD804013A79EE737D475D859B1CC528C769CDFD72B4E475551BEE7AB7FA
    E6FF1CB8614FF5853F19590080200BD7A87FC8B323CA3F5E3CB5DE45E791797C
    AAFF722ECE9B5569CB37C0D028342DC8B008028CB15CDF64C6241D47BF6903FF
    FBD59F665598D6017FA61B628A2BD88EC792B70E6F37A9F1DE53BEC300802D73
    9BFAE86E090A6172C73DB0E6EC725D865ED366D7D934C2B76C0BEBA122E01004
    01004010040100401004010040100401004010040100568CA3B55F4C9B8CC009
    FF00F25E01C8DC03DC5C0384E393A06381708F6A803D80F163F3070AB156010A
    B008558042AC021562AC02156010AB008558042AC558042AC02156010AB00855
    8AB008558042AC02156010AB156010AB0085580598ABC718FD6C0EC07866A398
    7EDB81DB87ADE61FB5C27F23FAC29C028B738700EC047BC056076C0D6C017046
    D13D81DE08DA1D984D68B262375A79DB423E21A9887A08F492730E387D710338
    02705CE626E33270D69046927E814A0FA20A7E82F4919105FA4C9DE785C60693
    4B796FA8CC9C05151AD479D14BCE1AD47FC530F312152D3C10FF493D44CF296F
    05BE90A8D43E9CF6AA90B853E5695552D8636FA59553F6047C94B189F9D3EF20
    8B17CF966B1871E57B55A87924FC14CC60ED8CCBF2D8B8A5B5E2EA6A1E8DB8CF
    16ADF75284AB8F92AC75B5CF4756D24373DADCCBF7ADEA95DBC41EA06EF845E8
    53BCFF9301FD3B0B61154776A71291DDF3DA5022AB26AE1BC371944CCEB92A4F
    D51DF27016B0796B6FD9BDEE1BF859FED2FF8AF0F28152DBD5AFEFC302BA4A6E
    99EF0DA2597DEE57FC4C1F839E6927E10ED4320D4326AC85CD31BF4D6F840DF1
    92FF64FFB477D8B3E35FAD00FA9CFEAC8F7151D59059CB63786AFAA87F326BEC
    02A0E93CB23A0637C992B887F2C8FEA663E353F5D777D395BA91B7E5A5C4E79A
    A0AAB4F3247F731F3D4D7AA64A0EABF9C35D4FFE908BEB8282DBBEC6E052DD1B
    729EE1E701681770478005C00DD05C00EF10384C77E6F479C3DF829A7D465F0B
    19C41BA4B7B405E8E1B49CA993693F7933798F85387643DA7387D11BE890DD4E
    3CA38BD5152327D0FB72077A4EE990318A7DAE5EEE7C86DEBE7473573145F081
    806AD490DF7D8230E2FA6547C635BCD5B0660BC4B6D999EB5DBE55A5065579F9
    84105D9C105EF2CCAC07AA99FC05F440DA0DED9357695A6FC4E612F0B05163FE
    5AE7E4A60BE9DE5B7A8D787DA1D417D9BF349BC0C6F6F2FE31AF733D972129CA
    CCA4F77EA0BFF335F18330122335AE1F7397B6FD6FA5BFB50A20658101277FD8
    75EE8EA14E2E599E0FA3AA3F54762E9F2CB5CAC3A04F26087512C1802A293344
    17A2EE2D92CBC4745E3DE1DD1A425D4A5DC2AE4090A54B7F3CAB869C663C2994
    B02A9CFC5BE8914FC35B716D194C300AC52FFCAD3EF0A7D52FD4739E09905EA9
    645BBED1B7462034E6774DF1D3809F45527380CCDB6D8B6C31B3377C192D7BEC
    D58BF2A517AA2366C5EE80A5C74EA7F7469358C37D0E654E19AB2FF95A193B21
    AE8DACECDB6AB733A539614556DBFC66B7FD4A5753E9E0040F8D74C7D7EC0580
    2FF05280BC0213680BFC14B479C00A8822B680B0207FE3EF623A01789274121F
    01DA52660CBDA0E1A8C37726739270D02A9B1EC6BF0FC8314E946BCA4E836311
    DFB296A8D58333710EF89DD8599749570D38FCA37EAEA888750BDB60A8B38EE1
    45FF50BF50B9EC151CDEA11BBEE1A59C87D99DB656DE27CC33A1A581EE120FCD
    D2ABAA1D8D5936E029059629FA12F889BD23CEFCCFAE443FAB93C5CC7D30E1AA
    8DC61DBA6950D0888C47437FDF151476410F223F155C6B50F04B2DC839E0B898
    E97FC212D914F9193CD14E7743F997C8B62FB2A7F1667951EF8545692B32996E
    8E7FD609FD743F28A3F05ED19634CFFA7A7CA1BC88BD9E73B69BEC89F8FA73FF
    69F286290E4A775476E47093685E73FD9EC33E25375A0F8DDE68A7F956A3D4F9
    ACEC9BB3805C00B411502DA06B405F835488EA13B435FC1DFC41F405F06BC32B
    459C058A167F21292663BFC59225D7F2966187062AFF9BD52A292DFE1F9012DA
    07102531608230E7FAA431533F91A98575543C0847C303E16F5521F8DEB972DB
    5B189FF50B49302C076D477F493C532802955D589A5711DD9288D36F145F13C3
    F02E03CFD143C785F15C94975027D0187F9299755D0B2731447DD28A9515F230
    EDC362976DBEE4A97E555271776D31607D6CBA18762B3E4505B967CB1FF32453
    02E1FAE5690DCC6292F2B98B29819DDCCE269CC68950FCD73F4C64B9638B89FA
    91FE71F353AFFCA8A42F4CD0CC96BAD026FABABF6E3A0178240B036EC691B1A0
    1FD45331636829705354BAE19930192A8C323B85FF50344FB4E13E1164B0240B
    93ECF7E89C59E179769BFC013B13205BD3E567CD3DFF6D8FF1219380A0713424
    57C1779A129FD0D3C0FC295E57DAF100B7BF2B807405DE0AFC1AE0DD483AF085
    614F7A7384AB02CB04D008EAFF8BF151012D78CA81DC432A11988B1C46DFE1AD
    E58D843C285341DF06BB0E3183DA0302A3BA44BE00BD8604F2BB151B8A473A6D
    C7DFCF9A9694864E4A2B94B6F9930AE4BD05A65C30A5942A6C55D01AA2C44E62
    36FD297DE6F4F60BFC8A9E6C57ACC9E7B586C624768A5BF350C914FE8164A362
    7733C32A882FF605F932FECCBFB8CCCB8733D78F0CA2386A2A8FE905588DFA21
    E48BC4FD74C5BA357AA97FF33FDCC528B0F24CA79DE66899475B6392CC5D6440
    E4EFE005D5707D4E150FE32165227C51CF2296D9904A9485647734E8E4FD7328
    7FC875E5454C6D95882264D5A75F7E7BF3437B674F94FF560BDBA1D14C355C37
    CEAC7DF3DF6F20BE152F84724114A29D2C39CB96CAFFE635382D2CB47C391AF1
    B33B955902DF9DC22B8D732B21B09FBC35E137AC18AD831EE7EF14FCDFAB8064
    02C26F02FD7782F1BAC0BA805C1FB314FE0ED06B5017C248508DBC1B9C778E40
    FC0AEC8ABB832F111E73C3D4EDA10911157999F438739B7EC52C6E2FCE4B50A3
    F2105E6BE748393EAAA6B4107D108F548212525333EE2C6A23B716AE75E7A046
    DFC5602C73924C68F911B09EDF935BF155F99C648BF35FCEE6EF4BE72533AC6B
    66F2EE896BA54512CCBD68911519D3B85B146F17CE147DD5BFB076385DAE2366
    45BF622B4A8A865061507EB277E8FBFF33BD658E98878324DEC43C261DC236CA
    1DDB9299454FC2B9F5B77A3D78BE8424D55D369217264BDE88B531DD95A6E129
    FB93F7FA7E3FC97D9E6FE955F29139565474FF60DA2538F5AE8861D65043D1AE
    7BDB96A6413A3DBC9F9B953ED4D7535E03D0472016803F83BD0290883346BC81
    DC82FD614AC69D505F023ECD356B3DA608CB350EF5195505E7B1A5E4517E1115
    44727CAD2A417E6A1FFA5F405C62FBD10EB533884BFA1DBEEE3EDC75B4707729
    3F93C5EE9E149EB9304139382F3A886592E1C5599D1118A297B2EAC6C6AF4719
    75A255FA3BEC38C09F1DBA7CF35923F10B7AED0E52165D569D224E2C16E432B9
    DA3E49C45037D53E39490BBADC66329F7C3A01E894D5485E392C6EA6BD7D30FD
    E09F521F752EFD0CDA805A00AC11555D05D611B5B9883D81D78CBC523600A8C0
    3A0E3F94BEE2FBA26BC6719931157784799B159334006065D4DC1ACD515F288E
    3091DB0778AAF7850AE8D70F3551885ABCD358250391A56D205756F60C01DA8E
    CF26C7D955D5ADEFED4FCCC46D7D6973509FE1EE4A7EB795E592D89C4F02AD05
    F952DFCABAB6E31187DFD20CC9B2FED54B2247949048B86C70756CAFF671ABE6
    81551E73A461DC4CBEFCB74943FCE27F9A93FF1C3CDBD791551C14DB7CCB15A5
    657D29CBD9D3FA69A7BD1872581FE7CC6800D03DC0AFDFE9EC087C3A00A2D9B3
    40B7052E106816F0DE882F015832D08DE02B01D78763047305E4455E50465558
    068C3B4096052D832B0A2B0C98C9F90DD8C87A15178BDC50BD04D919960171D0
    8B50EC893D0A5096C728BA082427322671CA88C99E22068816464C8875E21E35
    BD2235430E4593C691E01AEA14C942507D216FD4A29114EA7CFF267233E46A72
    344FE9C756B97CCCEC55F66B90284FA51546956A8F8DEAE4A4E454F6882F9ED9
    31AECC0B5C6467871B02C9F061815721064A96551B2B989570F6267124E33915
    CD99614B2907ED0FC606ED04256C5290119638621C6D3B3857433FB49F618F3F
    F6471AD2C522F58B79B6EC875B8390BE09A195A3D60266C1665BD89DE7352DDD
    6072B95825BB801D0383FFBC5E6E7BBCA0F2D21CC82CCEE62A42F8A6B7B88F15
    6266BFE09A4EEADF13A7FA27F3447355D0407EE4540A2A6545E264B1C67ADB67
    2C40E2912C1CAB4D3B55B8A8EE4F99DB4CFB2AF46C2E16E2A02C0D016CB76097
    81B65DEF00460BB431781711B415983A4104C239071520741B6822410541D780
    AF28A6096852422731DDE2AF058DA2FB4B9BC1750D486DDA6C5A7B402FCCE7B4
    F9A95D203A62BA53AAF333C22FE11694E1B4A8B54B5526E44E260154D5B4DB94
    F9CD11CD2FF0A1262DEAA8AD3DAA08E0C29567E302F0B0F83B71AC3B9604CA66
    E4BE65B1558728A6630F1AE7E18B5C269579D56DCA4CCE1194C3B920955EDCA7
    32B92E123F0BFE52D2733F2963C2BD9CC09D8A5C8BF8623E151722BACA64D5E8
    F0F7B846A76E3C3449D9F5CAD2E05CE8BE57F1C90EA76A7390F28D783D6A43F4
    B4DE176C2B62771F5713E13DA85277DF58E1C42595E5CEF96173D2C8A72277C3
    569DFAD4DE9C93AAA8F0487941657DFD0A728932ACAFD017F3241E66CFC85E89
    599A8EAABE664EE8707C38999B773A6DE1D6F96CBCDD6714C34D4A4C94CCB629
    42E16A7E973F11B7D1AB4AC6F470FA5FF3317E090F71E789BCCA9042ABBA59C7
    B48228493548CD53BC59552A12955FC458B9667B51B3488F2CCA9A04E2AD3DDB
    35F98965D800B3F9805A09981D504440EE40B513522CB81B305391F4C43D4237
    0E2E10E700BCF80598BDE4A2C092BC9CFC951C4E7E45F1083AC5993EDCFE639F
    933F943B93ABD99332AEE7333A4CBE96074A8AF28ECE5ED252FB17F420C0C7B3
    FB0A5E4659ECC0CC637414E906F84B729E11121389A76414B95ED60B8277F1B1
    7338FE2BCE187D78E61AE088E08BE622BB896EE6E372F08771FCAB71F5515CE6
    07C797990ECED07447A91D526CF22FDB422B847C58BE756F3A87F8DAE817E063
    53073F164F435870D6AB1B96076359896C450D7A791DF307D90BB3C8A7C533DF
    01FA9CECF07FE22FFAB63A13728E7DAF3B10D962356ED97E224D628758A3CEC9
    48EC4DA3F603F7A77E73B0F04B38199EB4E7FCAF62A0189B778563F7AE924667
    BD8B1EAD0B4467F740DA65AF8EE762790F35B005500740E8C2FD017F811FC05F
    1207C81C017239FF8FFE801025EF11F9C4A5B8841CC9DC2A90A55ABE4877A0D7
    FD4CD3127EA91FAB838561DEC32F13AD865A54E78414D79F08BC6B4B9865E783
    FE7944820E84358517BD99C5F263FA28BD281E19997B9FAC20A7A20B66DBBD94
    87C8EDFC4D27360243E24C5C67FD520472CB959F8C526AD4012BCDFC8E6EA015
    9A59950A67F7F2F0E72A0BD01E01DA85E8135238CC5808E9185484D1C521CB07
    D80EF0207447D127D103611160130303A2BB8802A1AA8EEE242C13605E7449DC
    58D08BE64A28C81A126A105E81D71C3E020B8D2AA7D825940ABE0E1A11AB081F
    623E91AE945740EB9607052D72EDF6B4A8649561DD03AAB0EA26E014E073DEA4
    286235539D0BE830EF4E28476C45F80D6C51B6290A170F02BBE10F5785600A84
    9A854549EDD0B3A54B63FBE987D832AB41E938B5B455545538ABF5E9FD54E7EE
    14561EDB156D60B6AE7FB127B4D6D902A496B4F9EC81ECEAA636F7271F555E66
    2AF90A729932BD7B52BF235031C2EB58EDABC2F4FAAA73B58D6BC8F0911E0B26
    AA37B2AFE85EB36D4B74ECCCED69705AB1D087D92F8D3E916ADD4D4235DB6CD9
    F066D6C6DFB32159628E7FB914BE2E4BED43471ABC9EA720176DD2D9F2A4711D
    805AF83B8A5556FE54CF6B9EABE0ED6C5A2FEEBEBF2F0FC6F7E78BF911EDCADB
    5956DA45B6EBE55894CC67E10724CBF12184FF51DD3F5F800AF0359F7670ED04
    7602D005435803AA26A8A6D0A542550ED208B423811B692D787AD213908FF133
    D903D93FF8D2FE23ECC4F31DC8725C43FA8E3D502BD2F845BF204484FFA84791
    A5DA3B4AB1E28EDE0091457317E78FCBD35B97B48A7E9217D5724239FC951BD4
    514864C20D489DE7DAB47115C2266E7674A617157C158FC305E6A67FBB0C3107
    C717DECB7FC1CE0EB6C53F1CCA642A3B7A3021D6C7A9F3006F611F49DDCC8E05
    5710B491C7F437AAE6F2C920E8EAA4953CC15BC9193B5C92494F18A5D0D5FA6A
    7DA17D270A59CE888BF19298AEAB8C79B68D7C040C17AA6ABE9697537AE328F6
    109D6DA3BA55EB5F9F629E71CF20BFE57978BEFFB7192362B275EFD71C20393B
    3C3A5A17A74F3DD1ECB6D3C9C1CB487F321D02CFB8FCE93E540F0DD00651700B
    B005417804689EC3B97D3D88B790B301C5823943BD85F900390473BB5FF07128
    83914BD8BFF80180FA534AA5BD808B83B268A453B6C1940A37B1DF428AC52771
    E7D0E4FE1F7322A55AB625FE249C656F9E290A9A5C1A65D10BB9517343B9AE4E
    8A72653BF7805E6EAA3083E08AE8F5F9BC8EDAA58EC3E96EF90749CB689AB6FC
    627FC29FFF8A5E31039AC68C577F2A2B06A1FF22FF079C6297268758CE7E4EA8
    04DD24CB9CA84B1E6585734BC3A8CEEA5CFE340AFFE4E78FFB22AE527F226F7C
    F6E5DEEE59F9414A51D46F9AD8E74751E66E39C3F3319E1D72F6BABC5A0F7457
    DC60913D9213ECB49C62BE7C8FCE75BFE4AA2F94DCBA99D00E754EE205DBBAC4
    0B8003008E202C4198823010E882011E226E53608300FDE46D457502DE11A909
    5296A2FEC0B5267B1B6214F0B61638022C356F30FFCD280E1D206A23C505A9CF
    546AF18C03894BDC502C0AEC52FEC84B106C4F3C135ED12EC90BC4752DEF3C3B
    599537AF51B510550B814855784064C85758B20E8815E76F9AC394AEBD5D4043
    E09E7682D8B4B05ED816F8AB1B179F6494A42FFA23838FEDD6D8AEBFD02ED9AE
    0C82ABC2C3FE519787E60BFF13FE17A9FEAA3C76DD142646AC1653056DCB11BC
    8ABE3E681E5C067CA20E655F363B910FE371C56973641CD62F62F3F05F0C43C9
    26B24CAAC82695DC99664C83C7B5345A670EDF2777AA96B1A3F8B1A75690B47D
    A24C2E8BA94B3A47E3E9DA4C76965F448EAA1209090BF5D967D92BF930DB2D28
    4F09DD55645E25AB15296ABB7AD18C44CD9385B2E78A2AC51ABC3E76A07F1607
    5839D5C35897F9499B205E4B178E7A208CF28FD640211B7E99F3933641FF6D5F
    69CBFF7500591801F405A27505C402E823D86F68F7BD196A17EA268811E8FBB0
    B220BCE4CF414F41AEF0B4E823F4FB8295DE44854ED8685CD876F53B24F3D2F0
    01C056369393ADEA82FB0161623E1046D599B17BEA55D170F92ABF25A54EAA2A
    68C92EBA8A7EF3DA25333B74EE5B1F4C53B3EEF37E5A989E975E6FB3A34D89EA
    5A35EA2A8EBC1092FB2D82FF50C790C23BC76D0BE3F6387C69F7B26112F38A47
    F9E3541C67EE94DF10DD3B49246BE7C38B1ECE935713B4FAC3A07A5CBA9E39F2
    5E8CC63BECFB5393C13C41FE23E7032FE937FF618D3279BF60A7E3E09E8BB94F
    A6433431D45FB519739EDE3917F32A2F54300C29F4FF740F0489F00BE80AF006
    0460FA82A2550EDA16D40D78CB023505D41D25FA2CB47D38BED1D54828C2378B
    240A9395237D2EAD33EA20A8D22F346A0EB8C2D835B119B1FFE98D79BD70CBD1
    7FC02F5236A49DC51C6424E9343413550F7193E9C3B28AB8771A84F4FCF80046
    93760455425C6362F98C848664DE8A6C64EA905453D95EF42C7649BB2F68C9CD
    DAB4358DD07742805C3EF4F0D5B5EA7CD42379C3D890DDDE6EB13D0EEA7E7A0B
    8C99D1B5DCE2F47F7DC8D86C23C1A1AA87DCBDDC235476E6D71ADBF061C714BD
    6EFEC4CB87D1C4CDDDCEB16232A3D617B94BEBCBDC99BA57F79268DFAF95DCC6
    9C46F82E52121CD3963E01D3AB1B96DC7A1D48A43E566EEA3775873CC7FB217E
    11D9E30F8A31B8FFAD1BDC05AA173969750CFABF3D42251AFDC529550DD11B79
    FBDE753D90EF508F0ABE5A1FBE8377303C1F92B95F1096FA82A047D255B7AFA8
    93DB12BDEF581BCAB511C3778B3AD73D902D292657432C91CA43F9863A29CF9D
    264C4C3F1BCEB780A9BA81674D4017847E067908E808D405508790BDE31F232B
    1AA40EE1C9871528F51AE08E206B72926303701686F5017F2664C1DF7387E1DE
    86DCC7E7942E282E82A278C6F8F8D0DBD0E19929A52EBE3CFC46DEE426865F51
    DEE6A790BDA0DB8E388E2384014539FF1AFFE805BA075663D846D0B56520532A
    EA13B8ABFFD10F28CFE1B1FE2DB8A5BF31F34A69541C1E8B796D1EB09BD45EF6
    487E9BBD082FC4848950F949CCF83AB4D9A12FCB0A859FFA8B74491CC4E7EE63
    E95A6CCD3FA99F41F100D4FDD08708768D2BE3AF408FB875D69225C8A4B43A98
    F0ABF0D1FFFB932E7C64F6D8BA85E6AAFE7D2651481EEB67FA4B5CB3FFD945D6
    1D774C7D061CB43B4B1D99D6F0F0BA43EE2205133D2951F61DB7780F00943600
    AEAC014B3B350B508E817A82EF0D6A20E02351BE0B3507540151068425417C10
    9C0BEF0D6A455235D465C09F0ABD405E929783BE939C127C082A1DA92D53DB43
    9F42CAA5D5E485E4F54BDA8CB4007E12DF42FA143C10F48035272A85D8B1D099
    A48AF4EDD172522099457D44F85F34A5979EDC2E4F4C899457E022645FD689C1
    1BEB53F63BE07163FFE2BBE483D993F18166B9A05EF69BC2EAD81D44A9FE14FC
    A7E645F85C9A981A877F1813382E035D496988ED8C6A5D0ECF2FF38CF67F1FDA
    87C119DC4C9CB7F74EF530F846A72A3F4C3FE876D01FF94BB4AB5964CEDEB853
    FF488FF8C9C4A2767A3FB235B422E26F3F576FD2DD4957D88D9D964F11B54BED
    2A1DA12FD307C02E77313CA05A96E4053FB9BFE76CF23368FFFE1BCF4EDB4ACD
    9FAFF33FE9329E36F0FE3E8D521B1488E13B14DAA3BFCD1496E3FB02ED5BFF42
    8E247786B3F45E9E963FD69FFB789EC5A7B4EA6387CA4FFD12A4D6B69F50D6B5
    CDC174DD1802D16F007C05C08EC08D43FF053608EA10D423784F820F80D72484
    032BC4F007E9017234B163FC3B805F013162D6036FB9BD41F3616C90A6C55D07
    84C166C9DD8F3F89CD8B4A152C061D8A8A4A2F881FE2D2480B64F3605FC35F94
    3EC4B52B27555FCAF998484D05FC85FF10F28F9D969D8BAF983C0C5A84E26969
    35A849CC3FE53F4AB4E553D0D828711F28FF8D2A57CF4E14594F7E270FC00E60
    5FCB46C12E4C2A58A505AC2D4E98CF4133411B43E3FF3A1826472A48F676F41B
    9CB85A5C952F7765574BF2B3B4E600FCB47BA7DFD1339581095BF2C1FE55D4C4
    981A54B642252714AA4F9A0C1417049F4AA04FA993EA38A8FC859FC2C225251F
    4FF550DEAAD3E60972AD3660BCBFF814E72A70A5A8AF8A085D32F41B3EA42A7F
    6EA675B295310B4C5D9CF5E86EAE9EF9B692BB6BE21800138A0526D8185091FC
    C0D4882007D0C604C2380452C0B0A4394A598542400722AF1C583471DD408716
    9307653AAF2D126A9C6A92A600FA60EE8361C52D8E1BCE5796F348749B7410E7
    4BD23282965905494573C2A96221095C28DB2113E5B157B1D91A57CC27B10A71
    7552DBA0BA2964727EC2F6056E9288EC9E150870A42AA0EBCECE0C335A999140
    509E0D5B59CD55CB7BD87850F0B778075678275B613B355D6924C3482EF78509
    C0154CA63525781D9F508425FF56E9E2641448A1CD2896F2636BCF38881B38F8
    9CAD0BE282C6B21ED0593240569B1133113EF24E953271DD30C637913EFBECE8
    4AAF614A24713E39164932A968A09BCB524BD69466F2A3E3E4EEB9695B2A138F
    F2CB2BD91BCB153DA8E30D2A2A9902AE14287D0A451EA2AB2421D4D28CF8E8D1
    4BCE0A6FED3D2F88C7AD07DBF2A451F45D4C648E5EA31DAEAD1ED7E9622C1000
    261B6F01800304006F05DE0DBC31786CA1E2851701286A5163C41619148650E0
    A15A1C81514CF2DCC4CD87F482E4270E6D2E4218C2380D69C8F1F3CD83162F2A
    CE5648903C58F2D94903CC930FA70BE4154854292991543C7752A7CE7B527856
    089C65BD9F4E5D50C715B473A868350934C6B29AB274B90F0DCAC8A42C38AC8D
    33D6885B74AD178B2A0445688F6435B8653258C5D29C254CF9ED2A48035BB9B1
    CB390E79B913089EFD551658635317EB210B1A23489A9D5754C466BFE7C00A96
    95694D2B9E54C7A89EACA5913B9F21723267A65249A54CED53B753C590DE91C9
    4669AA586A80F364C9538123DA8CF5E79FF248DA92C7C5B92AE18A0A3A115CC3
    2A05CB2ACF21229FABD6BD511A7C526B21752A57C357AD39675259C28F4455D7
    BAE93EA9929569479FBAA4C4B00043400560849AF0428B7856D00184C4B699C4
    D7881B07125E3BA880E1AA8F9E408242A1ADE4E1CB194547270E1606020C3795
    7299F79C2B2E6A0C20CC60F659DA6265052A0AEB45353B5E8D5A82A13531691C
    2F61E73D52CF4E5E7E213541E0B56F322B1E829169BD88D551058D2AB9408256
    1CB62F6AAB8E84AD46D817B1A82D96475DAB60AB34A72B91ECE2993A7B595821
    EDACC8EC5AC69532B2054754A67EB608EC808B5F844FA321DBD2301A5A9C6181
    2A58F64108B169AB091495F4DC7805269452293B109A52D207E0A57052712C58
    A31C025705BA0555C8619567626234624D13B446C49971144C0BF510444F3956
    53589D866B614570A79831498965A03D57FC953EBCC4B07CBA8613D113D40C79
    82F95E20527FA258619805E446AD4C35C417433473B815340D5B62987CCD91C2
    C3C35114F68A89FE5D3234ADB39D6027A1CF301CC0930210164038414A86CC1A
    30C8C33408100B4114401083A61DA090085C24E20B44188B27EA6B0405000544
    B30ED88484FF21AAC2F291DA841D80B41E283D8C35A86A0A11751AC50283AD84
    73152B0BA6926613D32AB62011AB258B3EA51B10E9950ABA9F8222D0AE9192C2
    5E07CD547B137121148D861510234493536F4264905690623052C00BA83EC831
    48D7F70854D1B01585328741281766120668B611A856785F3057FB24998F6982
    09D36B6A3E31AAB0DC848A053B02F9A12354D0D642B0B9C13C48E25A45A0C268
    42E8E067C2600301045F324D04A01B253021D888125CC095753730055BD11CAB
    5F5C99299465E3996948E129487098A99A08E5826A32A416F3309988D6206CD0
    BA6850D034BD8C0143870F41B8F850B5352CC28A25951DCEB3CC7CD80E218A23
    456231B02901640448C4099810405AEC40738761DA83D015A832C6563BA83602
    040E023B0845A2F410860820054C9B188182E8C7510F3416F30C019023005313
    110E6964145E06F34F681F2D538D39503D82AE42C9AA8732BA4055986DB53610
    9AA573989C6109492C35CF243A91934522142104138494CA4252BEB6B93126C2
    D822C09CEA4B861B44982F8CF5EA3233F280F9AF3AA79A4E412AF607E0D6F4C1
    E73A774DF7A428D82D68A362DB5690E350E42C533292A2AA40840B0947A60C82
    ADE085C20B012712642679296AC092D89AA9C2498055C97E53A76BDC68D900E5
    729A2814BB6B48CE46FA978AD3C199741497B5CC4E73841619710D54C20684FC
    A92725A5CA77E3ABE11B6014635690294661A56C4266C555EDA31A95C954B077
    865BCD45007613D8DD164544432472314544BED6EBA95A61DD89817600B020C1
    1191810603D839A083864E1A181C6062071A29A8F522068D4E4900480C04322E
    04224A1698CD2825968732AA7310DB217C9256002A91D041523029E0D35CA43D
    4C200CF4838493140A221E299505BC52268FC491C4FC68ECAA28A98154E44FD6
    A714266255D1452594B41740B35896E95B156AE0B224ABD496743868B3F4AA31
    121131229ED5E1825D2597B5388B53896224D5AA5416AC2331310D30B2307675
    1A8AB86BB589B4C9F942332C10095744989463404415F510032481576C4E88B2
    A9640C4FC2A1614D695E846745B2649796E8A41176594D8B30971C23162324B4
    2DD08CF0A3CD5B9458804BC4236C54A632A392F51AD30C556ECD2AD0AB634C93
    B45D2EA8288AE39810AB4453F7456396CCD93FC8B2211BD459115BA461B330D2
    8A2B8D4EB95DD061C68F507A791F5BDAA070C096000207EA073850400185EC47
    10B44160D88EDC3538E9C43A47162388468005042D4BA7144905D24808B4B272
    92D2CDB20EDA83A7CAE0F4FA22418CC875C8A14848844E841C04251C90D15325
    08C808C446A5C8A4E89247356732919D3B730652D5A434405E7B676E5BC02AF4
    646C4613511AB35A4E8715CB8D88AAD0608901F284D170142313980202185C48
    32E0290A0CEC2AB7ABA1A62852CEE2CE0B1CCEFE192A1A0964E69ABE51CAB31D
    1CE847C74746F5D4FD6333D42CC52AF9A158602DA0AA4E533E72A220CF5CA2E6
    573CAA42710317EACC2271CE581B15E6B8812E7D20B2AC7874485D4B3D4B9E24
    F8E949AA40188E832D293D1CED89ADC6A540842EA3A4C4ACC54C45514A335430
    444AB35BC8CDA0920DCC08701581EA08500AC2620BD862C004851402A80441B3
    8B2206A91A7109C41127CA39388EA3AA940E1E38B1A477500281C725A226A4AB
    8811C58B8C051FF53C2A91A917524054BD472B8E67174D162328A260554CB8A8
    1ABD50614791540C4516675391771EEA3D1C43C55EA8D0B52C28BED648E33627
    6B90553638983128D462A9974E54AE0C44E18B281B0E68AB1C1713A6CDCF3890
    18C6819A2926914BA8AAE2C0B24C98C26B60F8DAE45845AEF0B23A3730376715
    6E6271CA6A2898CD19CA58CBD0CAE271E71807183E693E31F3172A3CA315306E
    465A99E33E1299F3A6562ADCA2ADCB3D4A08C961257ED66F26B2693E9F276D12
    455465A8B115BCA866D5F8480F79129EA88C7549531547EEAFA33BE9ECB1A5AD
    69F929141B5836D45B4B24A31A4DE6D62068E0492195212A97174EDA4EC08C00
    1CED02D85078D0D50444144094206A800E3D90EE1F6C57450DA20309A78BE797
    573312C517A505000F9782E876C43AA48C867C6466D4AD643C6444EA7AD22950
    A4148902637831AA75F845765AF32081C5B894F912C226B68C9C754EAA4C0AF9
    393960B6C2CE40C451DA0C4080B5E2D0AD08824420499D6AD72495EDA86827D8
    5B8672963C4C6305813772E4155552B323B4A5F24F4471B6E6EEC3DA4821F99B
    1D31328E127371981498B90B4047CDD65B36D580CF8D8BE694550B53AA093246
    76EAEEF8F92ACA78B6E90743828C05CB12C42DA11975384EEA2E0D5594D29A05
    9EF65D65C97BC0353D662A37085002808703940020BD076F059C18701C86B400
    58B6418585EC4C947D6119C4CB14B88C3120B210C05390AB27E44F3850B1E18F
    6B19971C18D9396CB33CE523930B88EE52ACBF5A2EB1298417439C57AD25318F
    3D623D6918F09222F580B4FED52D691CB418C0A320CF610F47ED5516AA231F32
    4AC8D896BA64C7647CACFEB585992E923EF55922A0CA64CE10C6F99DCDABA245
    CC8B142A602E17BDA22AA910926417634F0510B46CED9E7F8678C85FB27425B2
    D43ED02C4436591DCB63AB6F607F73CFC796086D93CED7B3A91AC43D90C864AF
    512C32660829905500672480B95A2E74406AC894930B44E70F993941CB03704E
    41736C46BA868282DC9059E904A5E90D4DB756B808887806AB29CB91420A90B8
    FB0E9C504167CD4DC5A27452252902B5C0C4484749A8806405D8072C700514FA
    7D12D48EC27CE5A96F0150008089016842411900060CC8163046D8BCC510808C
    718083C8AB06D02691487998D8BE418E02A08442840A408C38DC91F263EDB557
    7016DA36B0736B9202A80A0806206C2C12CAC14234B2F406E09A65C55C1EA294
    879498970C952E0A68306A5375D5057961548B24259AC5C6B2C6DDFCD36195CD
    8E1F36B5D0BE4A0600E4865791E17044D9B34839EA3C6CA52D1E8B0567741420
    88DA4A0724C936B6D6971E902D2E8345B22BA01F9220588B14B231E6AF750B1D
    2D0560215A7C1CA15A75BE7CFAAB7969A44A6EC9C36B7758E94AC983D6B3331B
    A785C566C60800406A03981660458056072C36B049E0067098F0EC17400B01AC
    105856C1A796922A2E95C2103CDEB1B23CC509E0B08048A2A7AD8C8871D08295
    B241A13A21DB641449204C44081EB4162679C8451561DA17504EA710E9A4D849
    441F921DB817712322B89075D5C28113CE723053CDB42F93C880A9E12AF74EF8
    82EA7BF862D04F647755B91057B11111679AA50BEB9457273C9375623392A112
    AE2416BCEA88CEA4E9089F1405DE7BA265E5CF79BD148B4489538825E668753E
    221178BA144F2F23CEB1A8A08AF47743B4153E3A873AC04922E88C847CD79841
    020204CDFA88A79851D028A7447EA509586495F2F1AA640BB07254E2DACFAF90
    22A2224AFBE3FCBD56F3708403760406605841D607843410FAC1F60BD85C1184
    C23611986061C9894411AB24160B79446082C8259622002CB45869647D865311
    4CC3799F611A0C710EDE4988F0B1F1336D6672C58B44660E7A9A9A8D484CF51C
    823A69DCD5B90D87A3D619211D426B6364EAF9120AF0EDF5AA8546F200A235B4
    B992D34AAFA6D31CAD0599F4F4A986E248F4A3CC03BD9C14D5994D34A5270CE0
    14D3499E6935A6DC92CB91632268F37FAC3AE651255D24AA45CF4DAF09C43A28
    1222253136DC23D1F292F90F4C0A9A657E46A70F4ACA1312E6A65EB366EB0509
    0E2BD14B71625B19D15E14CEF7B9D4CDFA57A3DE44C97116539192CAC8A67D30
    D4967413185347E98A439D52C192BFD498335B2717AA5DC3DCC254D0EB16738B
    367D7E985364499BF7AC09B1D1CFE5FA019B262D132550A42C92FC6B6D366D3B
    3D9DB919E7B70DCD00AC009821E0B22661D3068C009875E439E4E914E8F9AC48
    04A8BF29651BCB8A27410B91456424396EB93E5EADCF49C38AD14B2DE872DD79
    C27C148A27D120B4321225AE2B4996EA96F4E131091620488B835D69512E250C
    D288002F458C4E2048CC604CBD6490445EBA85023031B9C8903839245860EE59
    675DA0EA86B47A86C7CA00D8005049418B08CC1D611D82AF12BC5691D3C72321
    D22198F511AAC465010C81794E62DC0003CA7597132725091661BD1274A54A3C
    F2240179C3FD2998D3F329C328D62BF328449EEDB461E3638A340604E992C51B
    34BC00D504829122BE9ED326D4D7996E8BE68BBD572C1AAF5E7378453306E686
    5EBA6AA24ACD0228209DCA2D670633D310E046011CD2D5602345564D9D024CB1
    AB02F78CAAC17308812CC8179539E6BE8C4005FD198EA330A0B3246654709419
    963D1B52CE1E8E8219635484F3DAD11183FE114A9A83654EAA3372BEFF5EA9E7
    80AB63522D186C7C73AE366235B088584B4873D914AA7B7A49C6A3CCA7A6554D
    F6A32AE81A1C9A5665DDD6B82015807400A0450102030804217B05421A585AC2
    0780861F7858A410494B032C46C0546164050527D64B2C9F98350156C045C170
    105CDD81DB30BC05693ECC4ADCE8BA5AC326E6430F949910249418997D302C14
    E4D5DCCE68168C000D276084ED4E30936207036A094C0A698FA622505D89AA9A
    A5D4CCD9E0CC4516302DD8BA067DB08D83039A9E9B4282B681FAA2446A48C121
    1A841FF4DA14DF64C33062D36D6648E6B9E6D46111A69C4226A2BAA6E09449C0
    F05283103A38063464C22393837C93695363A38F0383C780876E6A72B0824A73
    C244F84C39DDCC06C21A46266A7366A2FDE1E29181D0524B3950E54AFD534D0A
    64997422084858A6ED050EA6E2721C44D4E4789428A6B33590E125AA8DCA49D5
    1F0EDEA758462C8929612380231A219EA8EAA9A63CA3C289208D4DB612635495
    92B2DF17E44A888A03300A082800A0D2038A09609A605989A800903A63160926
    2E08A084D28A2393F717D0A265A288A08FA0511C9A4889484429DB32A82720A8
    5DC26C4B05514A280A801441015B0D1982D8434104991E2A5EA13117EA676A95
    9A6B0A0A0D8A78E25530CBA83631681AB004993266BE4E1E810AA5D4C4E1A295
    537642BA14B554D8CD986456E7730F15B74C9E81446EAA66EF2BB4D840A758D0
    954C2610993884454AA6C7761278A329EC9DC291C95CCF4DC11DD81C5AC9139B
    58793A432D4C82C9CC61DADB8BF44D074D5B2681BCEC90B709BAD4D3C7A64101
    8D0C630D1660DCA6BB4DB9A195F3C931DF5D8F1509651F886965384885ABA931
    4DB48CD5936A6E456DA1DAA5E492DB6A46B18CAB0D591C65B0A9EE8A473220F5
    8009045817481D4800D43A8428E624B1509C0A1119442736DA0C3A6550483280
    DE272E5F22A41033E2944D659A414E08B892264808130BF6950E8230762B2AED
    32A38A015E881245BB11A7C9D46A44B0625D39F67844D5DB019C61CED502C044
    320150622504E12AC053B679F83B5CF383745C710398D5356EF4A7D5A0C9AB95
    EB2BF1E9CED66491C5E34FF328C2392FA6FE049420B575D3754F550006DC8F61
    F7A85A65742C191A98ECFEDA9C900E8AD5282ED21C0090589D2809607B500530
    BBA8A42452E2044F89AE6FA314E73C6B06C34666AC4AAF2EA95C28A217444068
    2C09B34348ECC213085E3A385D32786B6F3E572FCBA997E28B8C36BAE0AD59A1
    B28C82F16E931B2C9D5674A76EACB1E90662A05A4B14DE4670696A312876972A
    094802047C6EBECFF7624CEBE58635311BEC7691409B926A24CF3813571A9F7F
    AF0A02E0A54B51968815DEBA559242895E145B6372252BFC9B345BA240088840
    8F0014134553E0830A044C0F840E501A61E8036035B894A4326401C85609EE32
    92CEC33A6693721258C16E24015F88562EC25C84B4B1F0747082388194BC2F4A
    3ED65106C7D629AEBE1E2AC81A04589D5C311A78C5822EB307787A6C3C9084A5
    96BA43F0E1819419CCB5274F3B020E160B26C1C53EF79EB9625D899A8F414AD9
    E4157A706E0F21740C80F4A3AA124CB0EC770952C9759543A5164139939BCE06
    872A1E218AF2E5637C66CD1B129DAE54738B39BA79CF6A5E23393CD83642AA30
    E828D83A724C7929615552A2B2AA48E219EF224C1A7C4B6AC9CA99EB79510152
    A50200AC4A050819809B002B6086ABD902DC4500F284C5105C78515146550284
    1B583EF15612A94641C150366062902EA0940BD584373F586AAC58DCEC3922F9
    70FA4E5168400B1BCC52E9EAE081DB1CA70794D5781A2CA70C2DA0AFECABD881
    8CB78EC598523EBC080C62F2B11101ABECBD4D86E48DC902A8D59CD2C4C0ECE2
    0D06A3583AA97D5C11DCDA0429168FBAB4A097D3A23A3610C76A49A40878FC48
    56C1CD9E35D478EB5AA49259025D3F24973851889B30F318D450BC2244A41A9E
    34416699698C994CF8C92FAA494B535A61E17543526FCC94F8361789185AA9DA
    B56F0614D361AA971D52B20E58C1368B1222EB6D581A75B6C65B414B57CDA63D
    9316FD77988723A8B3D0AB166A145015C0085A4F20E6036C142040209288C835
    80C8E421C0121F204080D8042E07B03584F9C8A613E618CC1AC801F3A1C9E704
    64C17C0FFB850A69344758A939DD74F9E48B39EAFABEB03E041B0016364C42EC
    103165297C3012C2FA82903AFAAA4030CC0EC21444BEAF2E541CC661908DCD3E
    DC8C1A180F3961662E8CA9462F31C198170A672A566A6990D9AEA718DC5172C1
    DB4C0A5115D1B60E3B884CEA55035F24DB8F65928AAB20EA75E84EA8F699CAB8
    254C68163DD3940FBCF2C1C2C94B88BDC6841F74A571155EFA43C1B7836A5751
    FAA958361F1E939F998ACBECAE45254F219C36A2AE619AD2CB59423AF2842535
    ECBF3B34FD915ABC9DD781ED5D635BD33B00C6831E8284032817704C90E2D000
    46012998551454D6599A4ABA066500B986C03252E9E881B016E3B81BF61A3E43
    A7211474DC7358DE294D29C595590EB506487DA75A96319ED57912FA119AC50C
    C35DB6A86363646350312309761A511B2DB7A77636342C1B5FF112158EE34B18
    1CF05AD136922E862EBB28889EB25718D96055CF61C2E7A5446DA85448530693
    2C5DB13E7539E8C5F85832A616562A6C47C9161C64AB864D97816094166DE051
    E67C62654BE6B5DE68EB3B83A821C59D1D556BA054539C5592A71CE4E09E9244
    E3264A6536A6F72559AE4550E76F53753DA7A417D2143C000ECBB6F4580F7089
    86003062701810A07E3AAF8A1901AF28541A8F2F6E2E70471C5143A3002E39EA
    309D8933A4AE44A955D2C38C6701F5842E1C8EE400DAB84A460DA3D6C6708DA0
    8D25CF52A3E8F7AC002015968BB815B956F0352B64E13E1F49C7E68A45E56201
    D0C694C4FAABAA0A4851CF5344FD54B38A64DFF64494591D528466F5257A900A
    D810803202838D9721E5721A803CC2602E05799E472A9D236038A11182331B18
    41AF8611F5CB128494A19476C00B10E1C4397AE9790C5988A6219B1E07648C57
    370383202079A979DCB6893674E3979F20D2DAEAADA02CADA493FD566CC69D07
    6083B8B6EA8A3630A3A3660D1A5DDC75B36947258F2F0E9D1E5D48D268C268D0
    15C3813B73AABDC10D01AF97A479784CEBDE6A254D356B5B894AC618CB59CC4A
    2D69756A1055913F595D77B853B9A753AD1C9639730D550ADB2274590F47A101
    C80282900900C108401840FB03D00258010119041201B82EE12811AE06EC4AA8
    826042C651CC8B13F01D58E52891455DC2363B88793060435988AC0587405483
    A8E7BA58A4E581E29EEB473637AE8E10C651F6C3F200881BA55E28351C1B9949
    418898CEBA4706657566C09199B5C8B5306B90173AEEAF54475D47DCB230A981
    8313486CF71CC426B29F62A64316613F0552A25E263ED5A529E941094186A677
    771668A706B8A85C860266A885846758C5F484ED8C9189DA969B199AA5518EAA
    0A46102CE975834F5530066048676B5150738B682B9A181747484B5672B16C19
    942772EE05C9465C2333A263E73128E4725B4043FDBBE872775C571137954D7C
    CE26372B7AB98E5EEF0A0E8167016F2F6D5C619213AC89D6B8D4C4BB927F8B8E
    D7F1D644B4AAEFF6F2A57AA77808012001E37E600A201E41A10EB02610490177
    12DC2CC179067604582AC093014B22EC4EB470D2B6E48B4A7219AD31AE50183B
    6379A59DCD36945026208C2CD1B97CB0D1813708593181F06543754D02D2E93A
    C9D82CBB90D03DEEB496A356702D0A322AD1E5D62C1495D782D642D81354C187
    8650ED5C9654B1348C11BB448884E72716A5161D141562D39CF459F2D5FBA1D5
    A3CB0FC2CA50F0508F3A3EAED2E2CA6EE556293992998FDCB26C7875ADBACF17
    1B96FB5AA29B156F11111C56F216D98E4ACE3FE3245765AB1EA22CDCA75BFF33
    C5BBBA5B7DBA4E870ECE2F5E670F5C714819B4D5C61864D573CA7CC32E8F10E7
    10915E0D1A9A4D2B91A6B33565D5C9F4B0B7186A70646B416AECC822B75D9D5B
    C19BD53B1F83026E908C71E86F46486F6DD79828A4F79B465263B5DCEC583581
    E00C0180300600C0180300600C0180300600C0180300600C0180300600C01803
    00092002698B3EE9A790990003D901AE3207AAE40EF2520B05A6B2B0624175BD
    2D601C41E2F4C6496F006005600933333F00667E00CCFC0180333F001D99F205
    118571DE01C01800C00931AC6AC908F4E00C00600498D633F53F801529FCCFC0
    199F80333F00600E27E00E27E00D4FE67E00CCFC0199F80300713F00713F006A
    7F33F00667E00CCFC0180389F80389F80353F99F80333F00667E00C01C4FC01C
    4FC01A9FCCFC0199F80333F00600E27E00E27E00D4FE67E00CCFC0199F803007
    13F00713F006A7F33F00667E00CCFC0180389F80389F80353F99F80333F00667
    E00C01C4FC01C4FC01A9FCCFC0199F80333F00600E27E00E27E00D4FE67E00CC
    FC010133C6A3A1EA9B59005004016004114E016C2B24C6B29F929AAFAD001F52
    00B00208A700B61592636A7F53F02A7F33F33F33F00414CF1DCBE4907701002C
    24016004114E016C2B24C6B44F929A940100010802C008229C02D856498DA9FD
    4FC2A9FCCFCCFCCFC010F33E6F9287A28661E24074BD20017862C0F4056B82D5
    88CBC92824A25435088AF9DF9D1201014210BD20315520017862C0F4056B82D5
    88C7392824A25435088A4C6D4FEA7E154FE67E67E67E67D10CF99369CE514B39
    6BC31607A02B5C16AC470AC9412512A1A84457B0F9B7618C76A4EA69019754A0
    05E18B03D015AE0B5823ACA4A0928950D4222931B53FA9F8653F99F99F99F99F
    4433E6CD839B978C2027E8805B02218680C8100802C146009A328E7DAB882E23
    B00DA0B613F71C841733BF689F366D4E480144101288805B0214ED0190201005
    828C01346411E10805820B88EC03682D84FDC72205CE07F53FA9F8753F99F99F
    99F99F4833E6CDAD97CB65FEE08B02CD9A65F9C768C843C2E6F5FF3B8D11A090
    741CD1C88173EF5DA27CD9B2A320084840284CA05B022C0BB022851002E08682
    DA3210F0A403058C0158468241D073472205CE07F53FA9FA053F99F99F99F99F
    4B33E6536D97C2E5FC60011008408B02EC082E606E086E71DA3210F24016132F
    CEF948DB10EC19301C071C080452AE6746D13E6E35EC90055020155A00A51F31
    008408B02EC08060801704482E4411190879200B0984174008A46D88760C980E
    85AA41008A55CE04753FA9FA053F99F99F99F99F4F33E72B02CBE51020151F4E
    9DC501341152B41CC089411200902C464DCD2DC3AE80A481B009032303AE1D94
    59B9DF9B75074802A5100AA9ADC305013410176839811282240120588C843C90
    05C3AE80A481B009032303AE1D98B8B7EA7F53F40A7F33F33F33F33EA267C914
    F13F0A98E5AD755856D20210200804DCD1C82220B28D44381009838283AB8F8A
    02B95CC072EE52CB44F98144C26568736802B54A5429010810080290D0805208
    882CA3510E040260E0A0EAE3E280AE57301C03992CD4FEA7E814FE67E67E67E6
    7D4CCF9CAC172CEEE699B979AE028E682DA068E77441A0F8804C1C141D5C7C50
    15CAE603A73BF57DA27858361E5D5C7561F1680174795F69010810140290D080
    5208882CA3510E040260E0A0EAE3E280AE57301CF00D8720BD4FEA7E894FE67E
    67E67E67D58CF9E354E6746B455152029000E08A2015801103C8006EC0908543
    7901345322FA81A3E28BE4522011828E42F25408771005881B14A0A09509689F
    310EB1AC5FF012E8E2C80A400110C8805600440F20087D81210AA90901345322
    F7D1A3E30BE4522011828E42F25408771005881B14AFABFB0A6A7F53F44A7F33
    F33F33F33EB267C987351760104E20885E0210B8F043487259404E4281A1009F
    88A94C6D13E5EBBD8F68D6C021B44110E4042171E08690E342809C850862013F
    1153263A9FD4FD129FCCFCCFCCFCCFAC99E4624CF282A2B00AA8300AE0E57805
    6172010D21D20501390A5602013F1152A26D13E1086E8701810DC3B009C6300A
    E0E5CC056172010D21E812809C851F21009FC8A9949D4FEA7E8D4FE67E67E67E
    678019F2450F9094FB404DE24441D10BDB98F1015B968783ADCC428B103888B1
    A384120627E1D951896B44F9814B982E173A8F5B4056005C1D10B9C2700522F1
    8C9C0708B0529A901145881C4458D1C2090313E42CCA650DA9FD4FD2A9FCCFCC
    FCCFCCF0233E4BA6F0DD1CA92844DCC11F1A9802B72DCF071798961620711164
    250CA992027490B72CAA3940AD13E61D2B0B95457015800808982F0D3A029100
    C46E83AA670A83901145881C44595756D546404E920627C1B4E4C816A7F53F4A
    A7F33F33F33F33C1CCF539719E57AEF101128C00B83A940291020466E5C1B911
    3CA061146E5DDC174238B8989EF4CC64B44FAB60C86280A08488808523002E0E
    A580A4408119064E8C6F49C0120241146015C174238B8992B2426326A7F53F4C
    A7F33F33F33F33C1CCFA5A978260B4AAA39E181C05511C05107485EC02685EE0
    27C58F8D6AB18081C655625C70130A288827DF7B44FA90986B09CA7554471336
    01134E02883A42F601342F7013E2C7C6A6C180824655DAF070130A2888263FEA
    7F53F4CA7F33F33F33F33C28CF5316700B9637A3EB9A4ABEC06A052029041802
    A52F7292B70C0470E980218A6E0B883A0C52D2897E380A24B42F725A4C6B44F7
    21EC49D5CA72595C824E025B601088824052083004238597B7946023874D010C
    53705C41D08296945021C0512594AB92D2636A7F53F4CA7F33F33F33F33C28CF
    92A19E868863E9A088E0228205E467B640509EADDC533ED948F9A0B7FD7F5802
    612C3014FB892635A27CC2885214203404238561EA464052083808A08171121D
    9014453113205321491F3416688EB00984B0C05226931B53FA9FA653F99F99F9
    9F99E1E67CFA56672504B68092D821515AE4052083808A089E28480A22988990
    2990A48F9A0B34475804C258602913498D653F0641D4B8681D929A02098705B2
    7A4052083808A08090DA2E405114C44C814C85247CD059A23AC02612C301489A
    4C6D4FEA7E9D4FE67E67E67E678799F2542AAB52F8D0102D509F5F0E08380970
    206298899D0B9428AE0E893F6F74246B2635A27CC284BD4548DA02A981CB42F5
    2029041C04B810314C44CE85CA145707449FAA20F07C80A46B2636A7F53F4EA7
    F33F33F33F33C3CCF4AA857B2FA1AB004AC380B102EF76624B2B56C51480A47D
    263594FC0A836F483E1A8744101075380B1034774A8E02704B10521ABC480A47
    D2636A7F53F52A7F33F33F33F100799EB7ACD850FD20F090804453809F10318E
    02AC20405EA40512588E70148EE5605A279269B4114FA81312400A0A1C04F881
    57A005584070BD480A24B436400A47731C353FA9FA953F99F99F99F99E2A67D2
    549B327D82B80AA94009611AADA02B05C0F2085611188CE40A046D14DC932838
    646C487295CE7B44F8361A79B47530A1B808CA40096114D1A02B05D489042B08
    8C46720502368A6E49941C323624394AE843A9FD4FD5A9FCCFCCFCCFCCF1533E
    12864C143D2D51DC0462400961133340560BA6641030899E4057088C46319490
    E322AC234F488244D724CA0E191AD21CE174C5A27AA26394BE00430A8AE02863
    4009611AC0680AC170CD2081846B0C80AE11188C632921C64558464E2441226B
    9265070C8D710E70B835A9FD4FD529FCCFCCFCCFCCF1733D4E54EA128093F101
    3E0EB845017104009B1C28206E143874A082915689EF178D192809394404F83A
    E11405EB5A004D8E1410370A1C3A504148AD4FEA7EA94FE67E67E67E679019F0
    14174E1404C9C404F83AE11405C048013A385240DC2870E9410522AD13D46B28
    6773009EB1013E0EB84501786C4009D1C29206E143874C08748AD4FEA7D8153F
    99F99F99F99E446781418F09804291013E0EB84501717D0027470A481B850E1D
    30208914180924B5A27D1E82D7B98040BC404F83AE11485C9CA404E8E1490370
    A1C3A60417D0BC6024915A9FD4FD5A9FCCFCCFCCFCCF0BC679CB4112026C04C0
    5064546279E1A09B0136026028322B53FA9F00A7F21301667E67E67E02C05C4F
    C040005A9FC04014FE40E02CCFC044EC67F2538095CD9B6E7FB69C04AEEA353F
    8080A9FCCFC0599F80B33F01602E27E02E27E02D4FE67E02CCFC045CE67B8EDB
    E02C047F83F815809DF9BF53F8080A9FCCFC0599F8083F19F80B013BF6D9A27E
    02C04E80B53F808129FC81C0599F80A866786500D59E2E00C00700499999F803
    33F00667E00C0199F80098CF41087D0019B33432C2F2203A73B4756107C1A86D
    A64836719BBC74E92903C92045D54AA02625E0FE1CC0C9965B0298364A830588
    20320B45314A8DBD7736944E528A751A4EEBF7335C322D0285507A83D34C3D5D
    D47491CAA8AF503E48106F5AF41235EC9C543050BCF1846059085715820CE34C
    428E5C874E0DAED1953770F0282C472621785D614431A44D10B966C9B0E3A1A2
    4A8812A12F085D14360544AB05025814C804914454E1A9040E19841A2C5841A3
    28D58C6A814689C50494C7094615824115215310B4CCB0D24EB8032823830210
    9257D42D7723009475498B4119544F4D2F29638B204B03908E83F52880B60F84
    83A5588503F22D006B04A2F2831F8540A4969F69155515D0401EAE48A938C444
    DC18854B70D5884B87246B20223ABD4156464260E11A564A768407245EC45AC4
    55315C7627562360C845051182441FA6096C5D2C8B95196335626B20F4B64CEB
    E0627E4F550420117A50720A8287AC491440A2E103A1C73DEF02E9E079223250
    72463C98F2C9C680AA7C369A5E40C41695C832A481A4DABE2D1D52D0D86E4988
    6A5103D8AA60E2795414610892502645310CB663009E788368248C70CA9B30B8
    35B32F40447632626632B920A210A5E80BD9A356DA495DD0A411B99480123904
    B827495C2F71252C3325D49989301089C14025A51542060CC6A3F73523775C23
    704AB50700E6E7621572021070F325A90488A33192946225AE574338FB28E1C2
    928D21B89853EA6D5F65E360689D72033669B60CCCD027EC2A41289C9A84AD24
    3E0C6360630B9078A27AC1B78A6796890D8F8AD5C074D14BC4B18E99B9D2BD10
    4D2BA18838B498611194E8108AF20934B5715BB269587F316964926EF4B6EA26
    440B6112691B653DE07FD2A98686520205B65552EEE9A1FE05BB8CF2064D5956
    893340577C4B078D4EB12D49163FACF8C18853B606B8F305CD72ED4560753A5E
    3F742968A4E8DD8102AD4B1A63EA801F3066B7641B19D3CAC77BED8148A01D31
    1C1157BF50F9601E102CC571ED1180302963BBD449EF5A44700DDA1940219335
    C4CDDBBC9854975E055F9A426C8E5D90EF214ECE5646528EF16EAF43AD08D015
    FCD2E155F8EA307840A096C389C3B2D19AB31A5B616D06A2CE5B34177C534B72
    F1D223A5D80699A41B97BBC4BB65A818F642F38B36476DAA8D60F59DB0AE68D7
    006A0ED1C89812A41E45E4BB550FC223ECB97D22A3F808A663F1D4032AC42B8D
    9B30565C6CB9D5BE71870AF9C78B571F39F928A4A572B66D4EFA02BAE5AC988C
    77D47950E2ED75CB07AD6EC29A6C0D26E61AE6267A4F07DBA1F2DCB1B35489A2
    8807B048E9F8196A55C5A41A93853AB18ED2F3E3B710562877D1E12001D4244A
    9C0B112B436430D1E656FB11BBC31EBB1E4B22AD32595A981087F74B20B52F24
    EB29C94B165647A92B2676664AE09614B6509633EADDF45CD451F20C86BD4793
    A1840D94489E6D2CA0BB2BAFB8599090F9FD0B04823A14547FE880E7D1D42B28
    C53AE66A5999D2EDB160CA54AA5DD604BE9A6D4427FA367C7FA3805C899A4F28
    72E2AB8293EF8E9E7499B655B1A90833A420C5C849ECE29B6E772CE08972126B
    20C27B2F0C9184E21BF532E562C42530A56742F3C59555C3DC953D2209CD6150
    786018A884C8BB96463E58B287133FEBE2CE05FD916815E1303C6CE80E3C580F
    5557D642FC92702C8E4B23C9C0EA3EA9D82F7E4FD831DC580B8BBC477D0202CA
    3AD8989B5842112D611467E4A20BE7ED61FB67F56239B6F9E392A957047BFE22
    B478845695C8BBC4AD83A7CF1E63FB44F665A56A2573937CB5C49D96DEE45DC1
    79A083619E8E4046C61CF0D6B16FE352628E46D72AD70B2FD628BBFBBFA12CD5
    A357C73F0C4A88BA950E83D79A2A451BD765E3AAC6CCF4E59F4A39F621ED4018
    6AFE728E3CAC263F511011694FDF970923DA8B84D0EEC6139F63A0371AE59EB2
    4DCE5558DBBD2291C8E9E0FCA4A50435850EB65BE6B58391F36AC1C8F9BEE0E8
    80FC3F31E7EB38757089BEC165050EC8C9536403AA808BE57BB46921AAD15F29
    00A1124A40C87C9F23C7D19D5B45498D2E1084D44A2B5744F2CBE7B617BEE90D
    C485936620AED4091AC8B3C01429BA52849FC5D7E789F33D1E4CA52B0F790ACB
    A493FCD0B6CAC27FBA58307A606314E9650DF079031A03A7F0F5014F5DB4FC53
    CDA79E71F079D8FE9FB2E6B62C78051195619A738782D72462DD3C1A2967FD0A
    832A7160CA838FE5A70EDC69F1ACFCA20BAED435A7E3E6CFE8908E827B2D73AA
    51675CF3E2D52239B2A456736B064AEA583129752A43F8E6DD4D99E9A50F9397
    83BB128BDFD3408719967DB933A19D4ECAFF2D5DFDAC36739C91173DED47A0FE
    ED6EBCD380588019EA6F1E2F6B88EBE703D71709A0371BB5F0CFEA1E6172FB33
    9CE40E2600CBFDA328C501AEA5502BBD21D003DEDD664ECFA3E6F15577C8773C
    CE492F459A7F0676F9EBD5130612FC268E02E4CD7924D00FC708AD6EBDE1A9DD
    E1D15E71D9FC095152CC1220B0F894C5A7CE8670B141D3921B7D6291729E759F
    6A6E39C122E02DBA9D7538400CEE3B8EE7AEDE39699B80F81C201E5F7BBC80C7
    BEEBC981474E3FECBD5A148208873353D6C01E02FF90DD2D8AA2F28E80DF1121
    483A9C8A45517EEFCC597C5E64ADB5C9F180634F90DE40A03C14183A5B51DF34
    CF45360BD7C7992FFA006B1B655F1A719D3C3A8EA4456D8E60ED9FF5FA2D9285
    70D0004CC78C756CF4FC5D79209FCCC250EE6F267B4D08B3BD13C3FFB62CDF8B
    DC0ACDA8E2BFCD5EE23D4F347A824F540766F6DC91B6F7B204043E66E0095D6F
    93B120AB579A007F0C17F191771A15EB362B0C2FE47F0A28793F776441EF071A
    9EE303D92CA6AFA280CC51FA54D1A10340AD453E063C7C0CE6426ADC23F0D0D8
    41458FA2C64F59A713634FC5FC4E77DBCF8546BBB28AB6537E4CB1D178506B2D
    36384C4A180A6C69E05797D8B06010C5C6AA8357A6B4BD178B507BE6C056C1B6
    0C44219D63FDF258FAB5A41A8FB7E29E3BDB75D99CE1DF393EE1CCA10CA97430
    695436C04DDB7CCAF100DD9566E67E6267C15B72FDA1FDCB7F703B28E2F573C3
    1419C2D069BD00DAE3CD673D0CF168A18C42CFEF253F923EFB8E1F2E0CB2185C
    23D2520B805F7444A0B8D0BBFA5CA91C174B9331761726BE574B9396FDE7265E
    2E1E82B00E933DD6F17466D2D64D34DDEC0BBB7C52380264DA17A6770C6D7BA8
    1FA71B05189C1B2F2C4E37803952706F1BB6751EA9CB3A9FF76F77A92FF1BC42
    3AFE203F593679CBEB21B4B45F5911981C7AC9B5465F590D444DFAC9A4CF2FCA
    268F0B4E1C3FD22CF4F915168BD143A2BC1A711F7F647981096FB69D5C84800C
    201082501D03F4F416287A03E4C9F86043D0BE7EF7FC07A14F80F447C2149248
    50A7BE1266E602249B3406F4E98804AB4308127A667D0986B3D24366AE009562
    5F40609E709EA07CC4486680D2DCF3B68ACF3A234A92A31429BD0FAA095303F4
    37A463F0388C4F4D5257D0B9D7466782D741C272DF8B028684CAA58782E56D13
    9D0B73354EE584F406260DF44B5E643C45ADDABE73B36D01B9BD1EA382DA0369
    ADBD1A0BD01B4EF41C1A400C44EC989570739332D1384EE4699CB9F4BACECF5D
    ABACEC90F2131D9ACEE7CE79BEF8C7953D9CD3DA055F01FEE037D9F9979881A4
    D02D8324D0344212693ABCDA4F274E80F99071209CBC7A4935A037560340F81C
    524E977E292C760313A07C4A5E0180FE76353680C03747D74F04EEE1E9893106
    01E21312148C032A260C0C5A4C34B82EACBB080A58317E18B35C43A03743B561
    0945DEB063406532C97340E7C58665048AC7C290CB4065859DF4A18069B5B0EC
    7B3158100B0940380700E01C0380700E01C0380619C07E7FC40E7814B1355F7D
    C3001A0A6B92E84B693D2B0F92A4C64A3EB9E3CF999498C1A657CD6F9C8BCF68
    BE1DA6175F212DA731BCFFC58281F7B80B41673FF3F7A00F551C63E8120ADDC6
    04BDF94C102A94DB1341220976ABD3ACC3CE82C03056BE270AE291B56739EAC0
    9F934048A5879B01FC3DDA6490557F261E848612ED85F3D860E1E0A84FCD84E9
    FA09C63850963AE57EFE12397D00E773B0E08EC9812704BA5B81C6012E413BB7
    763A4919D4D9A79A74261D79BCFFF1608888B2EF33F84938974B7CBC7F684E8E
    E8D08E4C5D5E9E47B0033F704A8F25E38DFD2509211294B7E10613E3476A0A02
    3E1F9850F9967078EFD6067D71425A25F4FC00C1294B71F8184EA3E94883889E
    1A1286D960F851A299D1D6B1E9A6A49825A46BC77F12E96EED8E09D780495E59
    A9C8E115DBE5805B69B6E9CE99B72EF441B8F4EABDDE84B8894C5BB8272AD155
    AF6C13E598CB19BB2E7E09238370E3C0D3425C44A64BDC13A84072EA0381DB1A
    66BCD43C85F1FEB0795FF0E41D7F87F1AECDAE84B8894C5BB8275DE3D6C3E1E5
    F800830EDD37EE5E97170806054AAD097112EB5F5E9B13AEB0F952E0BA183728
    021C279A402FC5810C919CA5A12E22532E75D0C2734A4AFDC08D38266AFBF8BB
    FAE51073950360387A3CE124225325E055A13A82461431D0B393B5C0E48AFC9C
    3124F250EFED386E8A9FCBFC124225316F867C4213E70E739FE1BA02FAFF9002
    03EE72393ED84008F2BE56F092D1299AFAFD81F33F44E8EAF168601E64E40100
    83C66A0FB7533F424B44A694746A05618CEDC8713B931464E60602CE12780041
    2204BB37E8141009A01300B400F040404B63FDAE5B5CA46B9705FA5EB970A0DF
    5D383AE5FC0D70DADD1EB77075AA15BAD573DAD9B5AB00E01C0380700E01C038
    0700E0196047B6282D5F1F57EEEC25EB2571F00C581DFA64B92FCDD2F4C2613A
    936AFF25848D73BB6F6D6B97C5C94C0E6662803A5129BF2E9E30934253605711
    C8C2CFDAB139B5CA7D9CE84BEC7D5FC6038730F9A39E7499074244094F81543B
    E1489F9971A06F9A6DBF2523E767310FF5FE811926630421B77BF8CB95849B12
    9B02A2EBB4627C899759A506848BAC5342420E34E270E64311B8B89F0498094F
    AE5684EBD72F18CAEBD402AA8AF0F44C6D72A01298B7771027E326D3682A24E4
    0A8BB9667A4CBB39F45C57E6735FF3425A25FA8F00784A6CBEB62736B9791447
    8DFD0AA9DF93125BFC8FE49E545E4B01684B48DFE8B237FFF84A66BED89DD399
    B9271FE62E2006CC2A2790A2712A5AB83C5E8EF95D09689444589483F094DAE5
    6C4E7D72FC6E6511504FEA3E67EF89007FE2A979FD7F0DD72E91A4D2C8DD1D98
    4A6CFAB627FA505329CBF3722DDDFF0AFEED00310EB814AE1A4243A12D12974B
    12A85E129B5CAD89CFAE552B9CE538EC07C7A8AAEEF5F75CBA46A9EB235A6F09
    7720FCAB6276839E6F133E2C2840EAFBC7409F9AD096894E3BD12B85E129973B
    4B144E7936E654DFCFA1350D4636B9744BD647E129F5CAC89DD6EE91CDA6C463
    4E41135C649820DD40D04C4AF04985FF7A3BF097DAE5089FE20F3786D72A831E
    4535EFC0184120C416E09291299AFFF0E2BEEE89F75CBB39A609680007303ECC
    042A6D72AC24D89774A7BAB105E62108C27C9DBACE36B80E123800509322536B
    95893004C04F027C0BFE212B5CA9D72A3FE66BA4DAE5237DB86BA487766C3D72
    A89AE1C9117D4BEB9513EB86D6EDADDE1ADD12DAE180700E01C0380700E01C03
    80700F2DEB95157403987C39CA19F47E28E680F74AB9F71D097B5CA01F2C2722
    19FC04315F9BF9F046D71244D6EB9B9E84B6A8F95D2D01AA200A3DB6019719C7
    BA44C20579A5DAC24FBA1DF9447D4456424312FB5CA4A16C027C274772D637F9
    DADA307D69168A91005BD16B5827102442791FEBD090A25316FFE16C913EEB94
    41D290008692256FEDFF816FC086419748F28262C24D894F9F545F9E0C4FC2BE
    52B28CE8491BF2CE3B63E800B82E731E092A129CB702B2277817FF3591FF624D
    12A99DC6BE0CF969F28A31853FACE122C253A5F82144FF70BF341CFE0936C1A5
    25456A29FAEEBFD8FF84EE6CE12D12BC06461C4BCE1F5BE129D73B627E73A527
    3A9C2B243F5618A53654F50F4C3424623448103FF55448EC456253E415B13E19
    DDE1031FF411C1291F4FAC0E9729391DAB7C127425E12E02D00384A72DDB13AB
    275CE9785823097B9567C4512296A960A66849646E972E129F3EAD89F265AA56
    3E4FB32AC5C1CF802E14512ADD0780D8962DA8D09784A72DDC25396ED89CE977
    C4C1C8490FACAAA5412F0AC556E3A1A965E9926237247024D8884266612EE4AF
    896C4FF7594FDB8FE6FE166B453A007BE5CD0815EB68915A12409792FC14097F
    A01BE12A173FF5104E89480307446AA1B7322F3CA607F0054F44501824584A6D
    72B027DD5FA34B63BA918B2500B1769A64494A6EDAFA7824584A82DDBCFA8044
    FFF889CCD0975BF98C863808C50E7A493E021D2E092712FBCA56DB8276DFED72
    E84B754A9B19F4DAF374779E70934254695FF480CF058E8F6984E43F5D7F20E1
    E00A0246000C12604BBA93FE581396BB22CEE80D002D03980780C256B94FAE50
    27D70F5775C28D72BF6B87A49636B970240EDD74BF6B95F3D6FC812D74BE1F56
    BB6D76846BB6D78C0380700E01C0380700E01C038067199BDE90C32A191C19E3
    000DF40E1F093B5CBF3AEE523F8DA5D4F6DBEF1C93BABEF6B948C248FA0C3DF5
    3C8BE0F3D159F0E91E3A00288DEFB81468FBA5012EE4BD7F57B877D03D0824C0
    97DAE55E5E8509F1F57DFF39587AA093D097FC7912DEEC1F335F83619C7FC090
    C25316EF97CE713F5E4BF0FE54A13EFAF3E7FDE0F079860F6C0E400E0F897282
    4D0951815FC524109FE9851B9F5FDD29C772DEE90116EC1BE17AA1FF8249C4A8
    2DF0C604E1F92CFCBC5C7793E77826138D2D8E65594E3161F4A0C60E9B02025A
    25F7F5AE12FB5CAC09C36C78F7FF78229533AF1E2CF1125EAEF96ED890E119D1
    31012D12E22812E2CE12A173B4275DB4A41EE584F0E87D61D91C9A8D60647225
    6D09208DCA1046E6CE12A35CAD09FB8DF1FF0EC48FC7CC721DD5D230CC5BD1D9
    7B424812F46812E9EBE12A0B7684ECD678E96E93C4E66FBF0ED95D43929A1241
    1BE4BD75A3B40951AE5684E1DF0509FFA124BB124CB644522AC44BFB25C116F9
    17173424812F876DEEA6C812A0B7684FFB19CD8A2CBDD86E84FC7AFD9B4704BC
    46EE882377CF097B26FF6B4275E02E16FCC2010CD121FD8E1DFE9B8A58D09784
    AA3D4C2542E7658313A2273744A662FFFD89658234B882F4DD3E259CE6E60918
    8DF263E129B5CAC09F75A4895946BEB188D28F1758A2FF763EF824544A82DFEC
    0B004E4BCD39DF90C06C395EF612F783B2293D3D906670493095137E5D8C42BC
    13BE6FB5A5A12D001455FFDD92C97A28E12684BBA572148BA01C4CC277A08930
    33C0546C8F22CC43093025DD3797DE04005004F027C0BFCA1236B97A4C955AE5
    D22501109B2B596C00DAD07FFB59DA6EF35CAA07A6BAE0140280500A01402805
    00A0140280500A0140280500A0140218802466CAC8C4B0C3A0517C64500A0140
    28050091A02D5B086DEACEE5E700A0140280500BA979C0280500A0140280500A
    0140280500A0140280500A01402804421AE8A7F0C3D97F27A2BFB8D7CC7AAF17
    DB411A02C6C97630C0280500A014021E02D59836FBDF1B48164A7B669D5C92F3
    80500A014029798F5E700A0140280500A0140280500A0140280500A014028050
    0A01108687066A097A6C6384B31D3E258CF678C332CC20F8F160F6CA57CCE59F
    3D92E80500A014028044C05AB1B56676E979E356AA74970C18A3BF1F799C60BE
    72F380500A01402979E45E700A0140280500A017064C85BD91E0140280500A01
    402E00A65F00A01402805009A1632BCBCF48BCAE1963D3973EFB7DA9FA42F4A4
    0F8BEE3BE2CAE40280500A01402C6016ACC7A0818BB198B58FD016FCE5CFA2A5
    E700A014028052F312F380500A0140280500A0140280500A0140280500A01402
    80500BFA1718E0A638E6DF58EF7D10C7241B3B6AEF7B7B867FB1040280500A01
    402E9012AF967FB724B86E5BD432AEB3BFAAF85CBF2F380500A01402979E45E7
    00A0140280500A0140280500A0140280500A0140280500A012AB286DBF579D7A
    F3F6FC2ABB80500A01402B2872F380500A01402979D94380500A0140280500A0
    140280500A0140280500A01402805F5CA1F73F631BBE89BC4AD31CC2E2F84900
    6A12C0280500A0140222016FCCDBF3631C14575ACA1CBCE0140280500A5E7650
    E0140280500A0140280500A0140280500A0140280500A012A3E871DE2C14CEDD
    2CF0FACF66FBDC380500A01402804440277DD186EAD867DD3E9826C70DDCFA1E
    5E700A014028052F3A4760140280500A0140280500A0140280500A0140280500
    A013B72879C7DE42FD9DD741DCEF4F9316C4E51B7E700A014028050089805AB4
    860CD1DDB92ECC44329072872F380500A01402979D94380500A0140280500A01
    40280500A0140280500A01402804A9CA1F79F73E2ECBE6EC6FA2DB7A500A0140
    2805008E005AB4B7E618FFD92EC9943979C0280500A014BCE91D80500A014028
    0500A0140280500A0140280500A01402804EDCA1ADCFB6E9DE8B39834723BD16
    EBD280500A014028047402EFA1396C764B886351283943979C0280500A014BCE
    CA1C0280500A0140280500A0140280500A0140280500A01402FAE50CF0F07F3D
    7A5B3FC79B77A2DD7E700A01402805008EC05DF4272D879F7BDCB65C63750E50
    E5E700A014028052F3B28700A0140280500A0140280500A0140280500A014028
    0500BED9437CF63F9780422C1DEFEF45BAF4A0140280500A011E00B9697CCEBF
    D5D92E72764100E50E5E700A014028052F3B28700A0140280500A0140280500A
    0140280500A0140280500BF394377D83C13FC1E770FA7F5C4CE58F8F71B7C373
    80500A014028047A02E5A5CB63B25C7E66AC5072872F380500A01402979D9438
    0500A0140280500A0140280500A0140280500A01402804B1CA1E26F29F7BB6F8
    2EF599777020140280500A011E80B9696F0223A764B9CDD8B19672872F380500
    A01402979D23B00A0140280500A0140280500A0140280500A0140280500BF394
    3DF9FFF035162C27C8BE8F8FAF4A0140280500A011F00B96972D8EC97610E8C9
    99CA1CBCE0140280500A5E748EC0280500A0140280500A0140280500A0140280
    500A014027A650FBD10C2762C93BB6FD7A5DDC080500A01402804A402EF9A7B2
    5CB6E5D80299E4DBC3943979C0280500A014BCECA1C0280500A0140280500A01
    40280500A0140280500A01402FDE50FBD10C2984DB780BDE8E629F27E300A014
    028050094805EF9BF96F3D247CEC3943979C0280500A014BCECA1C0280500A01
    40280500A0140280500A0140280500A0140260650DCEC4312EF45BBD1CEF4660
    140280500A012900BDF37ADF8AEFF7B9D5DB99CA1CBCE0140280500A5E748EC0
    280500A0140280500A0140280500A0140280500A01402FE650FEF2DF9077A2CE
    2E7388F7A3300A014028050096005CB4B96C72DEFACA6BA59CA1CBCE01402805
    00A5E748EC0280500A0140280500A0140280500A0140280500A014027AE50FA9
    CF7CFE7713BD16EBD2EF4660140280500A012D00B96972D8E5BCCE50E5E700A0
    14028052F3B28700A0140280500A0140280500A0140280500A0140280500BFD9
    43EA765A07AF85EBD27372DFD900A014028050096C05CB4BBE6F203EE8A5833A
    8F4072872F380500A01402979D94380500A0140280500A0140280500A0140280
    500A01402805FECA1F76DF265E58FEBD2EA76EBD580500A01402804BE02E5A5C
    B6396F6EF89EE21CA1CBCE0140280500A5E7650E0140280500A0140280500A01
    40280500A0140280500A013072860382FA4F5E7FAF4A0140280500A012F80BCB
    5796F63CA1CBCE0140280500A5E748EC0280500A0140280500A0140280500A01
    40280500A014027F650DE6D7031EBCFF7A2D00A014028050097002E5B18F2872
    F380500A01402979D94380500A0140280500A0140280500A0140280500A01402
    805650E0140280500A015943979C0280500A014BCECA1C0280500A0140280500
    A0140280500A0140280500A01402B28700A0140280500ACA1CBCE0140280500A
    5E7650E0140280500A0140280500A0140280500A0140280500A01594380500A0
    1402805650E5E700A014028052F3B28700A0140280500A0140280500A0140280
    500A014028050081B287A63A1BAF538753E12A180C42C8FEFD40280500A01402
    620172D2F65199CA1CBCE0140280500A5E7650E0140280500A0140280500A014
    0280500A0140280500A013F81C40C95824300C7A0F53875E61C9856ACE8ED3B5
    77A4500A0140280500B5805CB4BAF5796E105FE603379F2F380500A014029798
    579C0280500A0140280500A0140280500A0140280500A014028040417D90C34E
    3AF7800377A27DDF9EBCFAAEE01402805008082FD91FEBCFEC973CBEA686E5FD
    5DAECBCE0140280500A5E78D79C0280500A0140280500A0140280500A0140280
    500A0140280408177B587607F5FF1DAFD82E8AD23D7A5DE8B6EF560140280500
    A016100B96672D2EF9BFD4792309FB614EAE5CF2F380500A01402979E35E700A
    0140280500A0140280500A0140280500A0140280500A012EC28EF38B9AEBD226
    618E10CB51C0A0140280500A012F4058082D8AF64D39724BCE0140280500A010
    D5E700A0140280500A0140280500A0140280500A0140280500A013042840E91A
    F0F705FCD8682C7160140280500A0131C04E8B0113BECB858092F380500A0140
    28044579AAAA9609863C54C0060F00DCFE0393F480F5655F3B0C702C8601D80D
    A0FE21A0140280500A014021A02F47DFF2F602F49E4F8584058A7B63EB8C04F1
    FB5F1304047A68DEFC02C6021F8FFD4E01402690136BFE611305982CC1601402
    695FF3059844C2261100A016B2FF9844C1660B3058052FF7DBCEC59C1814C166
    113089844026A2FFD8081182CC2260B305982C02075FF308982CC22014C22013
    82FF982CC2260B00A60B00B5D7FCC2260B30880530897F9363E695C02982D21D
    305982CC16010D5FEE460B308A43A61100A610D75FED7611305A43A60B00A60A
    E35FF487521D3089844C225FFCBFFD7D0D7184C0A90EA43A60B305982EA6BFF6
    02982CC2260B3088053081CBFE6113059844C1660B305705FF3059844C226113
    089846BAFF9844C1660B305982F90C176BFC8B5CFCDC02982D21D3089844C226
    10397FE405308A43A60B305982CC1472FF982D21D3089844C22611AEBFE61148
    74C16E5ACC235D016D7FEA2D4695C02982CC16611308982CBCC7AFF0460B308A
    43A60B30898479ABFDD8C2260B4874C2260B305F617FD21D308982CC16611E63
    08DAFFD4BE903E1B02A43A60B3089844C145182E57FE405305984521D3059844
    C207AFF0660B4874C16611308982F83051EBFE90E9844C2260B4874C21C17FD2
    1D305982CC2290E75B05DAFF5D6A034746054874C22611305A439C4C232BFF20
    2982D21D305A43A611305D80B1AFF74305A43A611308982D21DE2BFDAEC2BE9C
    6C0A90EA43A60B3059847AAC172BFF602982D21D3089844C1690E20BFE611487
    4C1660B308A439D57FCC1690E9844C2260B487792FF984521D305982CC23DF90
    EDAFF000E0F1B014C166114874C2290E982E404DAFF0760B308A43A60B305984
    3AB081EBFE90EA43A90E9844C2260AEABFE90EA43A90E982CC17E861028BFE90
    EA43A90E9844C205582E017FD21D487521C2AC145584521CD80B6BFF5AFBFE7E
    0160182D21D308A43A90EA439DCBFF202982CC2290E982CC2290E24BFE90E166
    0B308A43A90E9843B17FD21D487521D305A43BDE90EDAFF0380355E014C1690E
    984521D308A4382020AFF1560B487521D4874C1661030C171AFF7BB08A43A90E
    A43A611305C3611B5FF85FCFE1F009DAC1690EA43A90EF9984521C4982857FE1
    B05A43A90E9844C166118490ED0BFFC984521D4874C166113051961188BFE90E
    A43A90EA43A90EFB182BD2FFA43A90EA43A90EA439E6C21ECBFE90EA43A90EA4
    3A90E7C305DAFF226B383EC05308A43A90E982CC2260B9610DAFF982D21D308A
    43A90EA43B2021EBFE6114874C1690EA43A90E7A5FF305A43A611487521DF748
    70DAFF984521D305A43A90E18611865FF305A43A611487521CFA60BB5FE05753
    E8E0163182CC2290EA43A60B308C561015FF9014C1690E982D21D308D0B087F2
    FF984521D308E43B60BC873DAFFD82F21DB05E43B6100290E9AFF3C58C70B260
    5B08E43B611B08D82C9E43A2BFE202D82EC1790ED82F21DB0824BFDFEC17611C
    876C23610FE60B27E5A925FFC876C1790ED82F21DC873DAFFE43B611C876C239
    0E7CB089AFFBB6EBB0A804FCC1790ED82F21DB05E43A06C142BFE202D82EC239
    0ED846C2390E2CBFF90ED82EC1790ED82F21D68BFF90ED846C2390ED8400D828
    DAFFE43B60BB05E43B60A30C226BFEC57FAD7980B611C876C23611B08E438DAF
    FD82F21DB05D82EC1790EB45FFB08E43B611B08D84721CFCBFF60BC876C2360B
    B05043051D5FFB08E43B60BB08D840D3089AFF7A8E4E74C05B05E43B60BC877C
    B5D82C58436BFDFCC17611B08D82EC2390E8501185FFC876C1760BC87721DB05
    7E5FFC876C23611C87721C266103ABFF90ED82EC1790EE4386982CD7FDAD620B
    D402D82EC23611C876C2360A101057FCAD82F21DB05D82F21DC874B17F8E308E
    43B611B08E43B90EB02FFE43B60BB05E43B6100590E7E5FFC876C23611C876C1
    5F721D015FFC876C1760BC876C220721D35FF053F707D00A12C1790ED846C239
    0EE43AD182857FC405B05D846C1760BB08E43A804657FF21DB05D846C2360BC8
    75A2FFE43B611B05D82EC204B21C62BFF90ED82EC23611B051E90E84AFFE43B6
    11B08D82EC224CC226BFC00E929EC2C0B90ED82EC17611B05B560B15FF301193
    05E43B611B08D82EC20C3086D7F8A184721DB05D82EC2360A1011B5FFC876C17
    611B08D82EC22D97FF21DB08D82EC176102960B045FFC876C17611B08D82D998
    459D7FF21DB08D82EC176110760A2E5FFC876C23611B08D82DB00A6BFE0519A4
    7A010C982F21DB05D82EC176116CBFF60BC876C23611B08D82DC2FFD84721DB0
    5D82EC17610FEBFF60BC876C23611B0817B05805FFB08E43B60A31611B051BB0
    8932FFD82F21DB08E43B6112A60A64BFF611C876C1790ED8299B081B2FFD82EC
    23611C876C221182EBFF611B05D82EC2360A69846BFF60BB08D846C176106982
    EBFF611B05D82EC2360A6C05AFFD82EC23611C876C235FF80B611B05D82F21C6
    8C175FFB05D846C23611C871E611AFFD846C1760BB05E438FB05D7FE02D82EC2
    3611C8723016BFF016C2360BB05D846BFF016C17611B08E438D184557FE02C05
    80B01602C043C04A28}
end
*)




















////    Alexandria ini
///       fim tentativa de eliminar TmySQLQuery
///
{
procedure TFmPrincipal.ImprimeChequeAtual(ChAtu, ChMax, ChConfCab, CPI, TopoIni,
  Altura, MEsq: Integer; Dia, Mes, Ano, Vcto, Valor, Favorecido, Extenso1,
  Extenso2, Obs, Cidade: String);
var
  LinhaAtual: Integer;
  MyCPI, Texto, Espaco, Linha, FormataA, FormataI, FormataNI, FormataNF: String;
begin
  Screen.Cursor := crHourGlass;
  try
    LinhaAtual := 0;
    if ChAtu = 1 then
    begin
      AssignFile(FArqPrn, 'LPT1:');
      ReWrite(FArqPrn);
      Write(FArqPrn, #13);
      if CPI = 12 then MyCPI := 'M' else MyCPI := 'P';
      Write(FArqPrn, #27+MyCPI);
      FormataA  := #15;
      FormataI  := #15;
      FormataNI := #15;
      FormataNF := #15;
      //if (ChAtu < ChMax) then Substitui := 1 else Substitui := 0;
      if TopoIni < 0 then LinhaAtual := LinhaAtual - TopoIni else
        LinhaAtual := AvancaCarro(LinhaAtual, TopoIni);
    end;
    //////////////////////////////////////////////////////////////////////////
    QrTopos1.Close;
    QrTopos1.Params[0].AsInteger := ChConfCab;
    UnDmkDAC_PF.AbreQuery(QrTopos1, Dmod.MyDB);
    QrTopos1.First;
    while not QrTopos1.Eof do
    begin
      LinhaAtual := AvancaCarro(LinhaAtual, QrTopos1TopR.Value);
      if not FPrintedLine then
      begin
        QrView1.Close;
        QrView1.Params[0].AsInteger := ChConfCab;
        QrView1.Params[1].AsInteger := QrTopos1TopR.Value;
        UnDmkDAC_PF.AbreQuery(QrView1, Dmod.MyDB);
        QrView1.First;
        Linha := '';
        FTamLinha  := -MEsq;
        FFoiExpand := 0;
        while not QrView1.Eof do
          begin
          case QrView1FTam.Value of
            0: FormataA := #15;
            1: FormataA := #18;
            2: FormataA := #18#14;
          end;
          Espaco := EspacoEntreTextos(QrView1, CPI);
          //if QrView1Nulo.Value = 1 then Nulo := True else Nulo := False;
          //
          case QrView1Campo.Value of
            01: Texto := Valor;
            02: Texto := Extenso1;
            03: Texto := Extenso2;
            04: Texto := Favorecido;
            05: Texto := Dia;
            06: Texto := Mes;
            07: Texto := Ano;
            08: Texto := Vcto;
            09: Texto := Obs;
            10: Texto := Cidade;
            else Texto := '';
          end;
          Texto := FormataTexto(Texto, QrView1Pref.Value, QrView1Sufi.Value,
          '', QrView1PrEs.Value, 0, QrView1AliH.Value, QrView1Larg.Value,
          QrView1FTam.Value, 0(*Texto*) , CPI, True(*Nulo*));
          Linha := Linha + Espaco + FormataA + Texto;
          // J� usou expandido na mesma linha
          if QrView1FTam.Value = 2 then FFoiExpand := 100;
          QrView1.Next;
        end;
        Write(FarqPrn, #27+'3'+#0);
        //Write(FarqPrn, #27+'F');
        WriteLn(FArqPrn, Linha);
        FPrintedLine := True;
      end;
      QrTopos1.Next;
    end;
    //LinhaAtual :=
    AvancaCarro(LinhaAtual, Altura);
    //Write(FArqPrn, '_____________-------------------_________________---------------____________');
    if ChAtu = ChMax then
    begin
      Write(FArqPrn, #13);
      WriteLn(FArqPrn, #27+'0');
      //if CkEjeta.Checked then
      //Write(FArqPrn, #12);
      CloseFile(FArqPrn);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

{
procedure TFmPrincipal.AtualizaBalancoDePQNoSMI(PB: TProgressBar);
var
  GrupoBal: Integer;
  DataHora_I, DataHora_F: String;
  //
  Codigo, CodUsu, Empresa, PrdGrupTip, StqCenCad, CasasProd: Integer;
  Nome, Abertura, Encerrou, Data: String;
begin
  Screen.Cursor := crHourGlass;
  try
    QrBalancos.Close;
    UnDmkDAC_PF.AbreQuery(QrBalancos, Dmod.MyDB);
    if PB <> nil then
    begin
      PB.Position := 0;
      PB.Max := QrBalancos.RecordCount;
    end;
    DataHora_I := Geral.FDT(0, 1);
    while not QrBalancos.Eof do
    begin
      if PB <> nil then
      begin
        PB.Position := PB.Position + 1;
        PB.Update;
      end;
      Application.ProcessMessages;
      //
      DataHora_F := dmkPF.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtSystem);
      if QrBalancosGrupoBal.Value = 0 then
      begin
        if not DModG.BuscaProximoCodigoInt_Novo('Controle', 'GrupoBal', '', 0, '', GrupoBal) then
        begin
          Screen.Cursor := crDefault;
          Exit;
        end;
      end else GrupoBal := QrBalancosGrupoBal.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE balancos SET grupobal=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE Periodo=:P1');
      Dmod.QrUpd.Params[00].AsInteger := GrupoBal;
      Dmod.QrUpd.Params[01].AsInteger := QrBalancosPeriodo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa smia, GraGruX ggx, GraGru1 gg1');
      Dmod.QrUpd.SQL.Add('SET smia.GrupoBal=:P0');
      Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smia.GraGruX');
      Dmod.QrUpd.SQL.Add('AND gg1.Nivel1=ggx.GraGru1');
      Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
      Dmod.QrUpd.SQL.Add('AND (smia.DataHora >= :P1 AND smia.DataHora < :P2)');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := GrupoBal;
      Dmod.QrUpd.Params[01].AsString  := DataHora_I;
      Dmod.QrUpd.Params[02].AsString  := DataHora_F;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsb smia, GraGruX ggx, GraGru1 gg1');
      Dmod.QrUpd.SQL.Add('SET smia.GrupoBal=:P0');
      Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smia.GraGruX');
      Dmod.QrUpd.SQL.Add('AND gg1.Nivel1=ggx.GraGru1');
      Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
      Dmod.QrUpd.SQL.Add('AND (smia.DataHora >= :P1 AND smia.DataHora < :P2)');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := GrupoBal;
      Dmod.QrUpd.Params[01].AsString  := DataHora_I;
      Dmod.QrUpd.Params[02].AsString  := DataHora_F;
      Dmod.QrUpd.ExecSQL;
      //

      //  Cadastro da balanco em StqBalCad (Grade)
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT * FROM stqbalcad');
      Dmod.QrAux.SQL.Add('WHERE Empresa=-11');
      Dmod.QrAux.SQL.Add('AND PrdGrupTip=-2');
      Dmod.QrAux.SQL.Add('AND StqCenCad=1');
      Dmod.QrAux.SQL.Add('AND Abertura=:P0');
      Dmod.QrAux.SQL.Add('');
      Dmod.QrAux.Params[0].AsString := DataHora_F;
      UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
      if Dmod.QrAux.RecordCount = 0 then
      begin
        Nome := 'Balan�o de Uso e Consumo de ' + Geral.Maiusculas(
          dmkPF.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtTexto), False);
        Empresa    := -11;
        PrdGrupTip := -2; // Produtos qu�micos
        StqCenCad  := 1;
        CasasProd  := 3;
        Abertura   := dmkPF.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtSystem);
        Encerrou   := Abertura;
        Codigo     := UMyMod.BuscaEmLivreY_Def('stqbalcad', 'Codigo', stIns, 0);
        CodUsu     := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux,
          'StqBalCad', 'CodUsu', [], [], stIns, 0, siPositivo, nil);
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqbalcad', False, [
        'CodUsu', 'Nome', 'Empresa',
        'PrdGrupTip', 'StqCenCad', 'CasasProd',
        'Abertura', 'Encerrou', 'GrupoBal'], [
        'Codigo'], [
        CodUsu, Nome, Empresa,
        PrdGrupTip, StqCenCad, CasasProd,
        Abertura, Encerrou, GrupoBal], [
        Codigo], True);
      end;
      //
      DataHora_I := DataHora_F;
      QrBalancos.Next;
    end;
    //
    //  Colocar itens do StqMovItsA no StqMovItsB
    Data := dmkPF.PrimeiroDiaAposPeriodo(UnPQx.VerificaBalanco, dtSystem);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('REPLACE INTO stqmovitsb');
  (*
    Dmod.QrUpd.SQL.Add('SELECT smia.*');
    Dmod.QrUpd.SQL.Add('FROM pqx pqx');
    Dmod.QrUpd.SQL.Add('LEFT JOIN stqmovitsa smia');
    Dmod.QrUpd.SQL.Add('  ON smia.ParCodi=pqx.StqMovIts');
    Dmod.QrUpd.SQL.Add('  AND smia.ParTipo=1');
    Dmod.QrUpd.SQL.Add('WHERE pqx.Insumo > 0');
    Dmod.QrUpd.SQL.Add('AND smia.IDCtrl IS NOT NULL');
    Dmod.QrUpd.SQL.Add('AND pqx.DataX < :P0');
  *)
    Dmod.QrUpd.SQL.Add('SELECT smia.*');
    Dmod.QrUpd.SQL.Add('FROM stqmovitsa smia');
    Dmod.QrUpd.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX');
    Dmod.QrUpd.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
    Dmod.QrUpd.SQL.Add('WHERE gg1.PrdGrupTip=-2');
    Dmod.QrUpd.SQL.Add('AND smia.DataHora < :P0');
    Dmod.QrUpd.Params[0].AsString := Data;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
  (*
    Dmod.QrUpd.SQL.Add('DELETE smia FROM stqmovitsa smia, pqx pqx');
    Dmod.QrUpd.SQL.Add('WHERE smia.ParCodi=pqx.StqMovIts');
    Dmod.QrUpd.SQL.Add('AND smia.ParTipo=1');
    Dmod.QrUpd.SQL.Add('AND smia.IDCtrl IS NOT NULL');
    Dmod.QrUpd.SQL.Add('AND pqx.DataX < :P0');
  *)
    Dmod.QrUpd.SQL.Add('DELETE smia ');
    Dmod.QrUpd.SQL.Add('FROM stqmovitsa smia, gragrux ggx, gragru1 gg1');
    Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smia.GraGruX');
    Dmod.QrUpd.SQL.Add('AND gg1.Nivel1=ggx.GraGru1');
    Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
    Dmod.QrUpd.SQL.Add('AND smia.DataHora < :P0');
    Dmod.QrUpd.Params[0].AsString := Data;
    Dmod.QrUpd.ExecSQL;
    //
  (*
    Dmod.QrMaxPerBal.Close;
    UnDmkDAC_PF.AbreQuery(Dmod.QrMaxPerBal, Dmod.MyDB);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa smib, gragrux ggx, gragru1 gg1');
    Dmod.QrUpd.SQL.Add('SET smib.GrupoBal=:P0');
    Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smib.GraGruX');
    Dmod.QrUpd.SQL.Add('AND smib.GrupoBal=0');
    Dmod.QrUpd.SQL.Add('AND gg1.Nivel1=ggx.GraGru1');
    Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
    Dmod.QrUpd.SQL.Add('AND smib.DataHora < :P1');
    Dmod.QrUpd.Params[00].AsInteger := Dmod.QrMaxPerBalGrupoBal.Value;
    Dmod.QrUpd.Params[01].AsString  := Data;
    Dmod.QrUpd.ExecSQL;
  *)
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;
}




(*
object QrTopos1: TMySQLQuery

  SQL.Strings = (
    'SELECT DISTINCT TopR'
    'FROM chconfval'
    'WHERE Codigo=:P0'
    'ORDER BY Topo')
  Left = 212
  Top = 776
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end>
  object QrTopos1TopR: TIntegerField
    FieldName = 'TopR'
  end
end
object QrView1: TMySQLQuery

  SQL.Strings = (
    'SELECT *'
    'FROM chconfval'
    'WHERE Codigo=:P0'
    'AND TopR=:P1'
    'ORDER BY MEsq')
  Left = 240
  Top = 776
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end
    item
      DataType = ftUnknown
      Name = 'P1'
      ParamType = ptUnknown
    end>
  object QrView1Codigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrView1Campo: TIntegerField
    FieldName = 'Campo'
  end
  object QrView1Topo: TIntegerField
    FieldName = 'Topo'
  end
  object QrView1MEsq: TIntegerField
    FieldName = 'MEsq'
  end
  object QrView1Larg: TIntegerField
    FieldName = 'Larg'
  end
  object QrView1FTam: TIntegerField
    FieldName = 'FTam'
  end
  object QrView1Negr: TIntegerField
    FieldName = 'Negr'
  end
  object QrView1Ital: TIntegerField
    FieldName = 'Ital'
  end
  object QrView1Lk: TIntegerField
    FieldName = 'Lk'
  end
  object QrView1DataCad: TDateField
    FieldName = 'DataCad'
  end
  object QrView1DataAlt: TDateField
    FieldName = 'DataAlt'
  end
  object QrView1UserCad: TIntegerField
    FieldName = 'UserCad'
  end
  object QrView1UserAlt: TIntegerField
    FieldName = 'UserAlt'
  end
  object QrView1Altu: TIntegerField
    FieldName = 'Altu'
  end
  object QrView1Pref: TWideStringField
    FieldName = 'Pref'
    Size = 255
  end
  object QrView1Sufi: TWideStringField
    FieldName = 'Sufi'
    Size = 255
  end
  object QrView1Padr: TWideStringField
    FieldName = 'Padr'
    Size = 255
  end
  object QrView1AliV: TIntegerField
    FieldName = 'AliV'
  end
  object QrView1AliH: TIntegerField
    FieldName = 'AliH'
  end
  object QrView1TopR: TIntegerField
    FieldName = 'TopR'
  end
  object QrView1PrEs: TIntegerField
    FieldName = 'PrEs'
  end
end









object mySQLQuery1: TMySQLQuery

  Left = 40
  Top = 520
end
object QrPQE: TMySQLQuery

  Left = 64
  Top = 652
end
object QrCabA: TMySQLQuery

  SQL.Strings = (
    'SELECT IDCtrl, infProt_ID'
    'FROM nfecaba '
    'WHERE Empresa=:P0'
    'AND id=:P1')
  Left = 436
  Top = 776
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end
    item
      DataType = ftUnknown
      Name = 'P1'
      ParamType = ptUnknown
    end>
  object QrCabAIDCtrl: TIntegerField
    FieldName = 'IDCtrl'
  end
  object QrCabAinfProt_ID: TWideStringField
    FieldName = 'infProt_ID'
    Size = 30
  end
end
object QrBalancos: TMySQLQuery

  SQL.Strings = (
    'SELECT Periodo, GrupoBal '
    'FROM Balancos'
    'ORDER BY Periodo')
  Left = 488
  Top = 776
  object QrBalancosPeriodo: TIntegerField
    FieldName = 'Periodo'
  end
  object QrBalancosGrupoBal: TIntegerField
    FieldName = 'GrupoBal'
  end
end
object QrVSMovIts: TMySQLQuery

  SQL.Strings = (
    'SELECT *'
    'FROM vsmovits'
    'WHERE MovimNiv=3'
    'AND MovimID=6')
  Left = 848
  Top = 776
  object QrVSMovItsCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrVSMovItsControle: TIntegerField
    FieldName = 'Controle'
  end
  object QrVSMovItsMovimCod: TIntegerField
    FieldName = 'MovimCod'
  end
  object QrVSMovItsMovimNiv: TIntegerField
    FieldName = 'MovimNiv'
  end
  object QrVSMovItsMovimTwn: TIntegerField
    FieldName = 'MovimTwn'
  end
  object QrVSMovItsEmpresa: TIntegerField
    FieldName = 'Empresa'
  end
  object QrVSMovItsTerceiro: TIntegerField
    FieldName = 'Terceiro'
  end
  object QrVSMovItsCliVenda: TIntegerField
    FieldName = 'CliVenda'
  end
  object QrVSMovItsMovimID: TIntegerField
    FieldName = 'MovimID'
  end
  object QrVSMovItsLnkNivXtr1: TIntegerField
    FieldName = 'LnkNivXtr1'
  end
  object QrVSMovItsLnkNivXtr2: TIntegerField
    FieldName = 'LnkNivXtr2'
  end
  object QrVSMovItsDataHora: TDateTimeField
    FieldName = 'DataHora'
  end
  object QrVSMovItsPallet: TIntegerField
    FieldName = 'Pallet'
  end
  object QrVSMovItsGraGruX: TIntegerField
    FieldName = 'GraGruX'
  end
  object QrVSMovItsPecas: TFloatField
    FieldName = 'Pecas'
  end
  object QrVSMovItsPesoKg: TFloatField
    FieldName = 'PesoKg'
  end
  object QrVSMovItsAreaM2: TFloatField
    FieldName = 'AreaM2'
  end
  object QrVSMovItsAreaP2: TFloatField
    FieldName = 'AreaP2'
  end
  object QrVSMovItsValorT: TFloatField
    FieldName = 'ValorT'
  end
  object QrVSMovItsSrcMovID: TIntegerField
    FieldName = 'SrcMovID'
  end
  object QrVSMovItsSrcNivel1: TIntegerField
    FieldName = 'SrcNivel1'
  end
  object QrVSMovItsSrcNivel2: TIntegerField
    FieldName = 'SrcNivel2'
  end
  object QrVSMovItsSrcGGX: TIntegerField
    FieldName = 'SrcGGX'
  end
  object QrVSMovItsSdoVrtPeca: TFloatField
    FieldName = 'SdoVrtPeca'
  end
  object QrVSMovItsSdoVrtPeso: TFloatField
    FieldName = 'SdoVrtPeso'
  end
  object QrVSMovItsSdoVrtArM2: TFloatField
    FieldName = 'SdoVrtArM2'
  end
  object QrVSMovItsObserv: TWideStringField
    FieldName = 'Observ'
    Required = True
    Size = 255
  end
  object QrVSMovItsSerieFch: TIntegerField
    FieldName = 'SerieFch'
  end
  object QrVSMovItsFicha: TIntegerField
    FieldName = 'Ficha'
  end
  object QrVSMovItsMisturou: TSmallintField
    FieldName = 'Misturou'
  end
  object QrVSMovItsFornecMO: TIntegerField
    FieldName = 'FornecMO'
  end
  object QrVSMovItsCustoMOKg: TFloatField
    FieldName = 'CustoMOKg'
  end
  object QrVSMovItsCustoMOTot: TFloatField
    FieldName = 'CustoMOTot'
  end
  object QrVSMovItsValorMP: TFloatField
    FieldName = 'ValorMP'
  end
  object QrVSMovItsDstMovID: TIntegerField
    FieldName = 'DstMovID'
  end
  object QrVSMovItsDstNivel1: TIntegerField
    FieldName = 'DstNivel1'
  end
  object QrVSMovItsDstNivel2: TIntegerField
    FieldName = 'DstNivel2'
  end
  object QrVSMovItsDstGGX: TIntegerField
    FieldName = 'DstGGX'
  end
  object QrVSMovItsQtdGerPeca: TFloatField
    FieldName = 'QtdGerPeca'
  end
  object QrVSMovItsQtdGerPeso: TFloatField
    FieldName = 'QtdGerPeso'
  end
  object QrVSMovItsQtdGerArM2: TFloatField
    FieldName = 'QtdGerArM2'
  end
  object QrVSMovItsQtdGerArP2: TFloatField
    FieldName = 'QtdGerArP2'
  end
  object QrVSMovItsQtdAntPeca: TFloatField
    FieldName = 'QtdAntPeca'
  end
  object QrVSMovItsQtdAntPeso: TFloatField
    FieldName = 'QtdAntPeso'
  end
  object QrVSMovItsQtdAntArM2: TFloatField
    FieldName = 'QtdAntArM2'
  end
  object QrVSMovItsQtdAntArP2: TFloatField
    FieldName = 'QtdAntArP2'
  end
  object QrVSMovItsAptoUso: TSmallintField
    FieldName = 'AptoUso'
  end
  object QrVSMovItsNotaMPAG: TFloatField
    FieldName = 'NotaMPAG'
  end
  object QrVSMovItsMarca: TWideStringField
    FieldName = 'Marca'
  end
  object QrVSMovItsLk: TIntegerField
    FieldName = 'Lk'
  end
  object QrVSMovItsDataCad: TDateField
    FieldName = 'DataCad'
  end
  object QrVSMovItsDataAlt: TDateField
    FieldName = 'DataAlt'
  end
  object QrVSMovItsUserCad: TIntegerField
    FieldName = 'UserCad'
  end
  object QrVSMovItsUserAlt: TIntegerField
    FieldName = 'UserAlt'
  end
  object QrVSMovItsAlterWeb: TSmallintField
    FieldName = 'AlterWeb'
  end
  object QrVSMovItsAtivo: TSmallintField
    FieldName = 'Ativo'
  end
  object QrVSMovItsTpCalcAuto: TIntegerField
    FieldName = 'TpCalcAuto'
  end
end
object QrPQ_GGX: TMySQLQuery

  SQL.Strings = (
    'SELECT pqc.Controle, pqc.PQ, pqc.CI, pq.Nome NOMEPQ,'
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI'
    'FROM pqcli pqc'
    'LEFT JOIN pq pq ON pqc.PQ=pq.Codigo'
    'LEFT JOIN gragrux ggx ON ggx.Controle=pqc.Controle'
    'LEFT JOIN entidades cli ON cli.Codigo=pqc.CI'
    'WHERE ggx.Controle IS NULL')
  Left = 1004
  Top = 640
  object QrPQ_GGXControle: TIntegerField
    FieldName = 'Controle'
  end
  object QrPQ_GGXPQ: TIntegerField
    FieldName = 'PQ'
  end
  object QrPQ_GGXCI: TIntegerField
    FieldName = 'CI'
  end
  object QrPQ_GGXNOMECLI: TWideStringField
    FieldName = 'NOMECLI'
    Size = 100
  end
  object QrPQ_GGXNOMEPQ: TWideStringField
    FieldName = 'NOMEPQ'
    Size = 50
  end
end
object QrFlds: TMySQLQuery

  SQL.Strings = (
    'SELECT * '
    'FROM spedefdflds')
  Left = 936
  Top = 556
  object QrFldsBloco: TWideStringField
    FieldName = 'Bloco'
    Origin = 'spedefdflds.Bloco'
    Size = 1
  end
  object QrFldsRegistro: TWideStringField
    FieldName = 'Registro'
    Origin = 'spedefdflds.Registro'
    Size = 4
  end
  object QrFldsNumero: TIntegerField
    FieldName = 'Numero'
    Origin = 'spedefdflds.Numero'
  end
  object QrFldsVersaoIni: TIntegerField
    FieldName = 'VersaoIni'
    Origin = 'spedefdflds.VersaoIni'
  end
  object QrFldsVersaoFim: TIntegerField
    FieldName = 'VersaoFim'
    Origin = 'spedefdflds.VersaoFim'
  end
  object QrFldsCampo: TWideStringField
    FieldName = 'Campo'
    Origin = 'spedefdflds.Campo'
    Size = 50
  end
  object QrFldsTipo: TWideStringField
    FieldName = 'Tipo'
    Origin = 'spedefdflds.Tipo'
    Size = 1
  end
  object QrFldsTam: TSmallintField
    FieldName = 'Tam'
    Origin = 'spedefdflds.Tam'
  end
  object QrFldsTObrig: TSmallintField
    FieldName = 'TObrig'
    Origin = 'spedefdflds.TObrig'
  end
  object QrFldsDecimais: TSmallintField
    FieldName = 'Decimais'
    Origin = 'spedefdflds.Decimais'
  end
  object QrFldsCObrig: TWideStringField
    FieldName = 'CObrig'
    Origin = 'spedefdflds.CObrig'
    Size = 2
  end
  object QrFldsEObrig: TWideStringField
    FieldName = 'EObrig'
    Origin = 'spedefdflds.EObrig'
    Size = 2
  end
  object QrFldsSObrig: TWideStringField
    FieldName = 'SObrig'
    Origin = 'spedefdflds.SObrig'
    Size = 2
  end
  object QrFldsDescrLin1: TWideStringField
    FieldName = 'DescrLin1'
    Origin = 'spedefdflds.DescrLin1'
    Size = 80
  end
  object QrFldsDescrLin2: TWideStringField
    FieldName = 'DescrLin2'
    Origin = 'spedefdflds.DescrLin2'
    Size = 80
  end
  object QrFldsDescrLin3: TWideStringField
    FieldName = 'DescrLin3'
    Origin = 'spedefdflds.DescrLin3'
    Size = 80
  end
  object QrFldsDescrLin4: TWideStringField
    FieldName = 'DescrLin4'
    Origin = 'spedefdflds.DescrLin4'
    Size = 80
  end
  object QrFldsDescrLin5: TWideStringField
    FieldName = 'DescrLin5'
    Origin = 'spedefdflds.DescrLin5'
    Size = 80
  end
  object QrFldsDescrLin6: TWideStringField
    FieldName = 'DescrLin6'
    Origin = 'spedefdflds.DescrLin6'
    Size = 80
  end
  object QrFldsDescrLin7: TWideStringField
    FieldName = 'DescrLin7'
    Origin = 'spedefdflds.DescrLin7'
    Size = 80
  end
  object QrFldsDescrLin8: TWideStringField
    FieldName = 'DescrLin8'
    Origin = 'spedefdflds.DescrLin8'
    Size = 80
  end
  object QrFldsLk: TIntegerField
    FieldName = 'Lk'
    Origin = 'spedefdflds.Lk'
  end
  object QrFldsDataCad: TDateField
    FieldName = 'DataCad'
    Origin = 'spedefdflds.DataCad'
  end
  object QrFldsDataAlt: TDateField
    FieldName = 'DataAlt'
    Origin = 'spedefdflds.DataAlt'
  end
  object QrFldsUserCad: TIntegerField
    FieldName = 'UserCad'
    Origin = 'spedefdflds.UserCad'
  end
  object QrFldsUserAlt: TIntegerField
    FieldName = 'UserAlt'
    Origin = 'spedefdflds.UserAlt'
  end
  object QrFldsAlterWeb: TSmallintField
    FieldName = 'AlterWeb'
    Origin = 'spedefdflds.AlterWeb'
  end
  object QrFldsAtivo: TSmallintField
    FieldName = 'Ativo'
    Origin = 'spedefdflds.Ativo'
  end
end
object QrErr103: TMySQLQuery

  SQL.Strings = (
    '/*'
    'UPDATE nfecaba SET FatID=1 WHERE FatID=103 AND CodInfoEmit < 0;'
    'UPDATE nfecabamsg SET FatID=1 WHERE FatID=103;'
    'UPDATE nfecabb SET FatID=1 WHERE FatID=103;'
    'UPDATE nfecabf SET FatID=1 WHERE FatID=103;'
    'UPDATE nfecabg SET FatID=1 WHERE FatID=103;'
    'UPDATE nfecabxlac SET FatID=1 WHERE FatID=103;'
    'UPDATE nfecabxreb SET FatID=1 WHERE FatID=103;'
    'UPDATE nfecabxvol SET FatID=1 WHERE FatID=103;'
    'UPDATE nfecaby SET FatID=1 WHERE FatID=103;'
    'UPDATE nfecabzcon SET FatID=1 WHERE FatID=103;'
    'UPDATE nfecabzfis SET FatID=1 WHERE FatID=103;'
    'UPDATE nfecabzpro SET FatID=1 WHERE FatID=103;'
    ''
    'UPDATE nfeitsi SET FatID=1 WHERE FatID=103;'
    'UPDATE nfeitsidi SET FatID=1 WHERE FatID=103;'
    'UPDATE nfeitsidia SET FatID=1 WHERE FatID=103;'
    'UPDATE nfeitsn SET FatID=1 WHERE FatID=103;'
    'UPDATE nfeitso SET FatID=1 WHERE FatID=103;'
    'UPDATE nfeitsp SET FatID=1 WHERE FatID=103;'
    'UPDATE nfeitsq SET FatID=1 WHERE FatID=103;'
    'UPDATE nfeitsr SET FatID=1 WHERE FatID=103;'
    'UPDATE nfeitss SET FatID=1 WHERE FatID=103;'
    'UPDATE nfeitst SET FatID=1 WHERE FatID=103;'
    'UPDATE nfeitsu SET FatID=1 WHERE FatID=103;'
    'UPDATE nfeitsv SET FatID=1 WHERE FatID=103;'
    '*/'
    'SELECT nfea.FatID, nfea.FatNum, nfea.Empresa'
    'FROM nfecaba nfea'
    'WHERE nfea.FatID=103 '
    'AND nfea.CodInfoEmit < 0;'
    '')
  Left = 1168
  Top = 28
  object QrErr103FatID: TIntegerField
    FieldName = 'FatID'
  end
  object QrErr103FatNum: TIntegerField
    FieldName = 'FatNum'
  end
  object QrErr103Empresa: TIntegerField
    FieldName = 'Empresa'
  end
end
object QrParamsEmp: TMySQLQuery

  SQL.Strings = (
    'SELECT * FROM paramsemp')
  Left = 856
  Top = 172
  object QrParamsEmpCodigo: TIntegerField
    FieldName = 'Codigo'
    Required = True
  end
  object QrParamsEmpNFSeLogoPref: TWideStringField
    FieldName = 'NFSeLogoPref'
    Size = 255
  end
  object QrParamsEmpNFSeLogoFili: TWideStringField
    FieldName = 'NFSeLogoFili'
    Size = 255
  end
  object QrParamsEmpNFSeUserWeb: TWideStringField
    FieldName = 'NFSeUserWeb'
    Size = 60
  end
  object QrParamsEmpNFSeSenhaWeb: TWideStringField
    FieldName = 'NFSeSenhaWeb'
    Size = 60
  end
  object QrParamsEmpNFSeCodMunici: TIntegerField
    FieldName = 'NFSeCodMunici'
    Required = True
  end
  object QrParamsEmpNFSePrefeitura1: TWideStringField
    FieldName = 'NFSePrefeitura1'
    Size = 100
  end
  object QrParamsEmpNFSePrefeitura2: TWideStringField
    FieldName = 'NFSePrefeitura2'
    Size = 100
  end
  object QrParamsEmpNFSePrefeitura3: TWideStringField
    FieldName = 'NFSePrefeitura3'
    Size = 100
  end
  object QrParamsEmpNFSeMsgVisu: TSmallintField
    FieldName = 'NFSeMsgVisu'
    Required = True
  end
end
object QrTeste: TMySQLQuery

  Left = 56
  Top = 280
end
object MySQLQuery2: TMySQLQuery

  Left = 624
  Top = 392
end


*)

//fim   fim tentativa de eliminar TmySQLQuery



end.


