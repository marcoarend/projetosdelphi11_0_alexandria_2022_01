unit PQUCaH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, dmkEditDateTimePicker, mySQLDbTables,
  dmkDBGridZTO;

type
  TFmPQUCaH = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrCliInt: TMySQLQuery;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMECI: TWideStringField;
    DsCliInt: TDataSource;
    QrEmitGru: TMySQLQuery;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    DsEmitGru: TDataSource;
    DBGProdz: TdmkDBGridZTO;
    Panel5: TPanel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    LaCI: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdOrdem: TdmkEdit;
    TPDtPedido: TdmkEditDateTimePicker;
    TPDtIniProd: TdmkEditDateTimePicker;
    TPDtEntrega: TdmkEditDateTimePicker;
    EdEmitGru: TdmkEditCB;
    CBEmitGru: TdmkDBLookupComboBox;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    QrProdz: TMySQLQuery;
    DsProdz: TDataSource;
    QrProdzPeso: TFloatField;
    QrProdzQtde: TFloatField;
    QrProdzAreaM2: TFloatField;
    QrProdzNome: TWideStringField;
    QrProdzNumero: TIntegerField;
    Panel6: TPanel;
    Label8: TLabel;
    TPProdzIni: TdmkEditDateTimePicker;
    Label9: TLabel;
    TPProdzFim: TdmkEditDateTimePicker;
    BtPesquisar: TBitBtn;
    Panel7: TPanel;
    RGPedGrandeza: TRadioGroup;
    Label23: TLabel;
    LaQtPedido: TLabel;
    EdPedQtPdAll: TdmkEdit;
    Label10: TLabel;
    LaQtProdDia: TLabel;
    EdQtProdDia: TdmkEdit;
    SbDtEntrega: TSpeedButton;
    EdDtIniProd: TdmkEdit;
    EdDtEntrega: TdmkEdit;
    QrProdzMin_DataEmis: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtPesquisarClick(Sender: TObject);
    procedure RGPedGrandezaClick(Sender: TObject);
    procedure SbDtEntregaClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCab(Codigo: Integer);
  public
    { Public declarations }
    FQrPQUCab: TmySQLQuery;
    FAplicacao, FGrandezaOld: Integer;
    FM2Dia, FCourosDia, FKgDia: Double;
  end;

  var
  FmPQUCaH: TFmPQUCaH;

implementation

uses UnMyObjects, DmkDAC_PF, Module, UnDMkProcFunc, UMySQLModule, UnPQ_PF;

{$R *.DFM}

procedure TFmPQUCaH.BtOKClick(Sender: TObject);
var
  Nome, DtPedido, DtIniProd, DtEntrega: String;
  Codigo, Aplicacao, Ordem, EmitGru, CliInt: Integer;
  SQLType: TSQLType;
  //
  I: Integer;
  PesoTot: Double;
  //
  Controle, Receita, DefPeca, Pecas1, Pecas2, Pecas3, Pecas4, DMais2,
  DMais3, DMais4, PedGrandeza: Integer;
  PedQtPdAll,
  Media, Base1, Base2, Base3, Base4, PedQtdeGdz, PedLinhasCul, PedLinhasCab,
  PedAreaM2, PedAreaP2, PedPesoKg, PedQtPdGdz, PedQtPrGdz, PedPdArM2, PedPdArP2,
  PedPdPeKg, PedPrArM2, PedPrArP2, PedPrPeKg, RendEsperado, MargemDBI: Double;
  //
  PdzM2, PdzP2, PdzKg, ItmM2, ItmP2, ItmKg, Fator, Linhas: Double;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Aplicacao      := FAplicacao;
  Ordem          := EdOrdem.ValueVariant;
  DtPedido       := Geral.FDT(TPDtPedido.Date, 1);
  DtIniProd      := Geral.FDT_TP_Ed(TPDtIniProd.Date, EdDtIniProd.Text);
  DtEntrega      := Geral.FDT_TP_Ed(TPDtEntrega.Date, EdDtEntrega.Text);
  EmitGru        := EdEmitGru.ValueVariant;
  CliInt         := EdCliInt.ValueVariant;
  //
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  //
  if MyObjects.FIC(TpDtPedido.DateTime < 2, TPDtPedido, 'Informe a data do pedido!') then
    Exit;
  //
  if MyObjects.FIC(TPDtPedido.DateTime < 2, TPDtIniProd, 'Informe a data prevista do in�cio da produ��o!') then
    Exit;
  //
  if MyObjects.FIC(CliInt = 0, EdCliInt, 'Informe o cliente interno!') then
    Exit;
  //
  PedGrandeza := RGPedGrandeza.ItemIndex;
  PedQtPdAll  := EdPedQtPdAll.ValueVariant;
  //
  if MyObjects.FIC(PedGrandeza = 0, RGPedGrandeza, 'Informe a grandeza!') then
    Exit;
  if MyObjects.FIC(PedQtPdAll = 0, EdPedQtPdAll, 'Informe a quantidade!') then
    Exit;
  if MyObjects.FIC(QrProdz.State = dsInactive, nil, 'Realize a pesquisa!') then
    Exit;
  if MyObjects.FIC(QrProdz.RecordCount = 0, nil, 'Nenhum item produzido foi selecionado!') then
    Exit;
  //
  if Ordem = 0 then
    if Geral.MB_Pergunta('Confirma Ordem = 0?') <> ID_YES then
      Exit;
  //
  //
  PdzM2 := 0;
  PdzP2 := 0;
  PdzKg := 0;
  with DBGProdz.DataSource.DataSet do
  for I := 0 to DBGProdz.SelectedRows.Count-1 do
  begin
    GotoBookmark(DBGProdz.SelectedRows.Items[I]);
    //
    PdzM2 := PdzM2 + QrProdzAreaM2.Value;
    PdzKg := PdzKg + QrProdzPeso.Value;
  end;
  PdzP2 := Geral.ConverteArea(PdzM2, ctM2toP2, cfQuarto);
  //
  case PedGrandeza of
    1: if MyObjects.FIC(PdzM2 <= 0, nil, '�rea produzida n�o definida!') then Exit;
    2: if MyObjects.FIC(PdzP2 <= 0, nil, '�rea produzida n�o definida!') then Exit;
    3: if MyObjects.FIC(PdzKg <= 0, nil, 'Peso produzido n�o definido!') then Exit;
  end;
  //
  Codigo := UMyMod.BPGS1I32('pqucab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqucab', False, [
  'Nome', 'Aplicacao', 'Ordem',
  'DtPedido', 'DtIniProd', 'DtEntrega',
  'EmitGru', 'CliInt'], [
  'Codigo'], [
  Nome, Aplicacao, Ordem,
  DtPedido, DtIniProd, DtEntrega,
  EmitGru, CliInt], [
  Codigo], True) then
  begin
    with DBGProdz.DataSource.DataSet do
    for I := 0 to DBGProdz.SelectedRows.Count-1 do
    begin
      GotoBookmark(DBGProdz.SelectedRows.Items[I]);
      //
      case PedGrandeza of
        1: Fator := PedQtPdAll / PdzM2;
        2: Fator := PedQtPdAll / PdzP2;
        3: Fator := PedQtPdAll / PdzKg;
      end;
      //
      ItmM2 := QrProdzAreaM2.Value * Fator;
      ItmP2 := Geral.ConverteArea(ItmM2, ctM2toP2, cfQuarto);
      ItmKg := QrProdzPeso.Value * Fator;
      //
      if ItmM2 > 0 then
        Linhas := ItmKg / ItmM2 / 1.15 * 10
      else
        Linhas := 0;
      //SQLType        := ImgTipo.SQLType;
      //Codigo         := Geral.IMV(DbEdCodigo.Text); //QrPQUCabCodigo.Value;
      Controle       := 0; //EdControle.ValueVariant;
      Receita        := QrProdzNumero.Value;
      DefPeca        := 0;
      Pecas1         := 0;
      Pecas2         := 0;
      Pecas3         := 0;
      Pecas4         := 0;
      Media          := 0;
      Base1          := 0;
      Base2          := 0;
      Base3          := 0;
      Base4          := 0;
      DMais2         := 0;
      DMais3         := 0;
      DMais4         := 0;
      //PedGrandeza    := RGPedGrandeza.ItemIndex;
      case PedGrandeza of
        1: PedQtPdGdz := ItmM2;
        2: PedQtPdGdz := ItmP2;
        3: PedQtPdGdz := ItmKg;
        4:
        begin
          PedGrandeza := 2; // p2
          PedQtPdGdz  := ItmP2;
        end;
      end;
      PedQtPrGdz     := 0;//EdPedQtPrGdz.ValueVariant;
      PedQtdeGdz     := PedQtPdGdz; //EdPedQtdeGdz.ValueVariant;
      PedLinhasCul   := Linhas;
      PedLinhasCab   := Linhas;
      PedAreaM2      := ItmM2;
      PedAreaP2      := ItmP2;
      PedPesoKg      := ItmKg;
      PedPdArM2      := ItmM2;
      PedPdArP2      := ItmP2;
      PedPdPeKg      := ItmKg;
      PedPrArM2      := 0;
      PedPrArP2      := 0;
      PedPrPeKg      := 0;
      RendEsperado   := 10;
      MargemDBI      := 30;
      //
      Controle := UMyMod.BPGS1I32('pqufrm', 'Controle', '', '', tsPos, SQLType, Controle);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqufrm', False, [
      'Codigo', 'Receita', 'DefPeca',
      'Pecas1', 'Pecas2', 'Pecas3',
      'Pecas4', 'Media', 'Base1',
      'Base2', 'Base3', 'Base4',
      'DMais2', 'DMais3', 'DMais4',
      'PedGrandeza', 'PedQtdeGdz', 'PedLinhasCul',
      'PedLinhasCab', 'PedAreaM2', 'PedAreaP2',
      'PedPesoKg', 'PedQtPdGdz', 'PedQtPrGdz',
      'PedPdArM2', 'PedPdArP2', 'PedPdPeKg',
      'PedPrArM2', 'PedPrArP2', 'PedPrPeKg',
      'RendEsperado', 'MargemDBI'], [
      'Controle'], [
      Codigo, Receita, DefPeca,
      Pecas1, Pecas2, Pecas3,
      Pecas4, Media, Base1,
      Base2, Base3, Base4,
      DMais2, DMais3, DMais4,
      PedGrandeza, PedQtdeGdz, PedLinhasCul,
      PedLinhasCab, PedAreaM2, PedAreaP2,
      PedPesoKg, PedQtPdGdz, PedQtPrGdz,
      PedPdArM2, PedPdArP2, PedPdPeKg,
      PedPrArM2, PedPrArP2, PedPrPeKg,
      RendEsperado, MargemDBI], [
      Controle], True) then
      begin
        //
      end;
    end;
    begin
      ReopenCab(Codigo);
      Close;
    end;
  end;
end;

procedure TFmPQUCaH.BtPesquisarClick(Sender: TObject);
var
  SQL_Periodo: String;
begin
  SQL_Periodo := dmkPF.SQL_Periodo('AND DataEmis ',
    TPProdzIni.Date, TPProdzFim.Date, True, True);
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrProdz, Dmod.MyDB, [
  'SELECT emi.Numero, SUM(emi.Peso) Peso,  ',
  'SUM(emi.Qtde) Qtde, SUM(emi.AreaM2) AreaM2,  ',
  'rec.Nome, DATE_FORMAT(MIN(DataEmis), "%Y/%m/%d") Min_DataEmis  ',
  'FROM ' + TMeuDB + '.emit emi ',
  'LEFT JOIN ' + TMeuDB + '.listasetores lse ON lse.Codigo=emi.Setor ',
  'LEFT JOIN ' + TMeuDB + '.formulas rec ON rec.Numero=emi.Numero ',
  'WHERE lse.TpReceita=2 ',
  //'AND DataEmis BETWEEN "2020-06-29" AND "2020-08-29" ',
  SQL_Periodo,
  'GROUP BY emi.Numero ',
  'ORDER BY Nome ',
  '']);
  //
end;

procedure TFmPQUCaH.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQUCaH.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQUCaH.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FGrandezaOld   := 0;
  FM2Dia         := 0;
  FCourosDia     := 0;
  FKgDia         := 0;
  //
  TPProdzIni.Date := Date - 92;
  TPProdzFim.Date := Date;
  //
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitGru, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM emitgru ',
  'ORDER BY Codigo DESC ',
  '']);
end;

procedure TFmPQUCaH.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQUCaH.ReopenCab(Codigo: Integer);
begin
  if FQrPQUCab <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrPQUCab, Dmod.MyDB);
    //
    if Codigo <> 0 then
      FQrPQUCab.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmPQUCaH.RGPedGrandezaClick(Sender: TObject);
var
  QtProdDia, QtProdDiaOld, M2: Double;
begin
  QtProdDiaOld := EdQtProdDia.ValueVariant;
  QtProdDia    := -1;
  //
  if QtProdDiaOld > 0 then
  begin
    if RGPedGrandeza.ItemIndex <> FGrandezaOld then
    begin
      case RGPedGrandeza.ItemIndex of
        // M2
        1:
        begin
          case FGrandezaOld of
            1: ; // Nada. � o mesmo.
            2: QtProdDia := Geral.ConverteArea(QtProdDiaOld, ctP2toM2, cfCento);
            3:
            begin
              if FKgDia > 0 then
                QtProdDia := QtProdDiaOld / FKgDia * FM2Dia
              else
                QtProdDia := 0;
            end;
            4:
            begin
              if FCourosDia > 0 then
                QtProdDia := QtProdDiaOld / FCourosDia * FM2Dia
              else
                QtProdDia := 0;
            end;
          end;
        end;
        // P2
        2:
        begin
          case FGrandezaOld of
            1: QtProdDia := Geral.ConverteArea(QtProdDiaOld, ctM2toP2, cfQuarto);
            2: ; // Nada. � o mesmo.
            3:
            begin
              if FKgDia > 0 then
                QtProdDia := Geral.ConverteArea(QtProdDiaOld / FKgDia * FM2Dia, ctM2toP2, cfQuarto)
              else
                QtProdDia := 0;
            end;
            4:
            begin
              if FCourosDia > 0 then
                QtProdDia := Geral.ConverteArea(QtProdDiaOld / FCourosDia * FM2Dia, ctM2toP2, cfQuarto)
              else
                QtProdDia := 0;
            end;
          end;
        end;
        // Kg
        3:
        begin
          case FGrandezaOld of
            1:
            begin
              if FM2Dia > 0 then
                QtProdDia := QtProdDiaOld / FM2Dia * FKgDia
              else
                QtProdDia := 0;
            end;
            2:
            begin
              if FM2Dia > 0 then
                QtProdDia := Geral.ConverteArea(QtProdDiaOld, ctP2toM2, cfCento)/ FM2Dia * FKgDia
              else
                QtProdDia := 0;
            end;
            3: ; // Nada. � o mesmo.
            4:
            begin
              if FCourosDia > 0 then
                QtProdDia := QtProdDiaOld / FCourosDia * FKgDia
              else
                QtProdDia := 0;
            end;
          end;
        end;
        // Couros
        4:
        begin
          case FGrandezaOld of
            1:
            begin
              if FM2Dia > 0 then
                QtProdDia := QtProdDiaOld / FM2Dia * FCourosDia
              else
                QtProdDia := 0;
            end;
            2:
            begin
              if FM2Dia > 0 then
                QtProdDia := Geral.ConverteArea(QtProdDiaOld, ctP2toM2, cfCento)/ FM2Dia * FCourosDia
              else
                QtProdDia := 0;
            end;
            3:
            begin
              if FKgDia > 0 then
                QtProdDia := QtProdDiaOld / FKgDia * FCourosDia
              else
                QtProdDia := 0;
            end;
            4: ; // Nada. � o mesmo.
          end;
        end;
      end;
    end;
  end;
  LaQtPedido.Caption  := RGPedGrandeza.Items[RGPedGrandeza.ItemIndex];
  LaQtProdDia.Caption := RGPedGrandeza.Items[RGPedGrandeza.ItemIndex];
  //
  FGrandezaOld := RGPedGrandeza.ItemIndex;
  if QtProdDia >= 0 then
    EdQtProdDia.ValueVariant := QtProdDia;
end;

procedure TFmPQUCaH.SbDtEntregaClick(Sender: TObject);
var
  PedQtPdAll, QtProdDia: Double;
  DtIniProd, HrIniProd, DtFImProd, HrFimProd: TDateTime;
begin
  PedQtPdAll := EdPedQtPdAll.ValueVariant;
  QtProdDia  := EdQtProdDia.ValueVariant;
  DtIniProd  := TPDtIniProd.Date;
  HrIniProd  := EdDtIniProd.ValueVariant;
  //
  PQ_PF.CalculaDataFimProducao(PedQtPdAll, QtProdDia, DtIniProd, HrIniProd,
  DtFImProd, HrFimProd);
  //
  TPDtEntrega.Date := DtFImProd;
  EdDtEntrega.ValueVariant := HrFimProd;
{
const
  // Horario >> 7:30 �s 11:30 e das 13:00 �s 17:00
  //OitoHoras = 8 / 24; // 8 horas
  IniTurno = 7.5 / 24;
  IniIntervalo = 11.5 / 24;
  IntervaloAlmoco = 1.5 / 24;
  MeioTurno = 4 / 24;
  Turno = 3;  // um ter�o do dia
  DiasSemana = 7;
  Domingo = 1; // Primeiro dia da semana
  Sabado = 7; // S�timo dia da semana
  DiasFimDeSemana = 2; // S�bado e domingo
var
  DataIni, DataFim, DtEntrega, HrEntrega, Hora: TDateTime;
  Dias, PedQtPdGdz, QtProdDia, Semanas: Double;
begin
  Hora := EdDtIniProd.ValueVariant;
  //
  //Ini Hora: de 8 para 24
  //testar aqui!
  if Hora >= IniIntervalo then
    Hora := (MeioTurno + (Hora - IniIntervalo)) * Turno
  else
  if (Hora < IniIntervalo) and (Hora > IniTurno) then
    Hora := (Hora - IniTurno) * Turno
  else
    Hora := 0;
  //
  DataIni := Trunc(TPDtIniProd.Date) + Hora;
  PedQtPdGdz := EdPedQtPdGdz.ValueVariant;
  QtProdDia  := EdQtProdDia.ValueVariant;
  if QtProdDia > 0 then
    Dias := PedQtPdGdz / QtProdDia
  else
    Dias := 0;
  //
  Semanas   := Trunc(Dias / DiasSemana);
  Dias      := Dias + (Semanas * DiasFimDeSemana); // adi��o de s�bado e domingo!
  DataFim   := DataIni + Dias;
  DtEntrega := Trunc(DataFim);
  HrEntrega := DataFim - DtEntrega;
  HrEntrega := (HrEntrega / Turno) + IniTurno;
  if HrEntrega > IniIntervalo then
    HrEntrega := HrEntrega + IntervaloAlmoco;
  //
  case DayOfWeek(DtEntrega) of
    Domingo: DtEntrega := DtEntrega + Domingo;
    Sabado : DtEntrega := DtEntrega + DiasFimDeSemana;
  end;
  TPDtEntrega.Date := DtEntrega;
  EdDtEntrega.ValueVariant := HrEntrega;
}
end;

end.
