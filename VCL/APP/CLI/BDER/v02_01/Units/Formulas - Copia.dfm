object FmFormulas: TFmFormulas
  Left = 371
  Top = 169
  Caption = 'QUI-RECEI-001 :: Receitas de Ribeira'
  ClientHeight = 529
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LaAviso: TLabel
    Left = 0
    Top = 417
    Width = 1008
    Height = 16
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alBottom
    Caption = 'CTRL + Delete para excluir item da grade'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ExplicitWidth = 287
  end
  object PainelDados: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 137
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object PainelDadosReceita: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 137
      Align = alClient
      Alignment = taLeftJustify
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object Label16: TLabel
        Left = 6
        Top = 4
        Width = 40
        Height = 13
        Caption = 'N'#250'mero:'
        FocusControl = DBEdit12
      end
      object Label17: TLabel
        Left = 57
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Cria'#231#227'o:'
        FocusControl = DBEdit13
      end
      object Label18: TLabel
        Left = 121
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Altera'#231#227'o:'
        FocusControl = DBEdit14
      end
      object Label30: TLabel
        Left = 185
        Top = 4
        Width = 81
        Height = 13
        Caption = 'Nome da receita:'
        FocusControl = DBEdit26
      end
      object Label22: TLabel
        Left = 867
        Top = 3
        Width = 28
        Height = 13
        Caption = 'Setor:'
      end
      object Label23: TLabel
        Left = 796
        Top = 42
        Width = 102
        Height = 13
        Caption = 'T'#233'cnico respons'#225'vel:'
        FocusControl = DBEdit19
      end
      object Label26: TLabel
        Left = 378
        Top = 42
        Width = 59
        Height = 13
        Caption = 'Tempo total:'
        FocusControl = DBEdit22
      end
      object Label25: TLabel
        Left = 270
        Top = 42
        Width = 101
        Height = 13
        Caption = 'Tempo em opera'#231#227'o:'
        FocusControl = DBEdit21
      end
      object Label24: TLabel
        Left = 162
        Top = 42
        Width = 72
        Height = 13
        Caption = 'Tempo parado:'
        FocusControl = DBEdit20
      end
      object Label5: TLabel
        Left = 490
        Top = 42
        Width = 19
        Height = 13
        Caption = 'mm:'
      end
      object Label6: TLabel
        Left = 767
        Top = 3
        Width = 52
        Height = 13
        Caption = 'Espessura:'
      end
      object Label1: TLabel
        Left = 71
        Top = 42
        Width = 27
        Height = 13
        Caption = 'Peso:'
      end
      object Label2: TLabel
        Left = 6
        Top = 42
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label3: TLabel
        Left = 530
        Top = 42
        Width = 29
        Height = 13
        Caption = 'm. kg:'
      end
      object Label4: TLabel
        Left = 570
        Top = 42
        Width = 166
        Height = 13
        Caption = 'Cliente interno (Dono dos insumos):'
      end
      object SpeedButton5: TSpeedButton
        Left = 840
        Top = 18
        Width = 23
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object SpeedButton6: TSpeedButton
        Left = 972
        Top = 18
        Width = 23
        Height = 21
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object Label7: TLabel
        Left = 610
        Top = 90
        Width = 306
        Height = 13
        Caption = 
          'Artigo padr'#227'o em processo (obrigat'#243'rio para caleiro e curtimento' +
          '):'
      end
      object Label8: TLabel
        Left = 454
        Top = 2
        Width = 122
        Height = 13
        Caption = 'Cor padr'#227'o (recurtimento):'
      end
      object Label9: TLabel
        Left = 667
        Top = 3
        Width = 42
        Height = 13
        Caption = 'Rebaixe:'
      end
      object SpeedButton7: TSpeedButton
        Left = 740
        Top = 18
        Width = 23
        Height = 21
        Caption = '...'
        OnClick = SpeedButton7Click
      end
      object DBEdit12: TDBEdit
        Left = 6
        Top = 18
        Width = 47
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'Numero'
        DataSource = DsFormulas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 17
      end
      object DBEdit13: TDBEdit
        Left = 57
        Top = 18
        Width = 60
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'DataCad'
        DataSource = DsFormulas
        ReadOnly = True
        TabOrder = 18
      end
      object DBEdit14: TdmkEdit
        Left = 121
        Top = 18
        Width = 60
        Height = 19
        TabStop = False
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 19
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object DBEdit26: TDBEdit
        Left = 185
        Top = 18
        Width = 264
        Height = 21
        DataField = 'Nome'
        DataSource = DsFormulas
        TabOrder = 0
      end
      object DBEdit19: TDBEdit
        Left = 796
        Top = 57
        Width = 197
        Height = 21
        DataField = 'Tecnico'
        DataSource = DsFormulas
        TabOrder = 10
      end
      object DBEdit22: TDBEdit
        Left = 378
        Top = 56
        Width = 108
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'HHMM_T'
        DataSource = DsFormulas
        ReadOnly = True
        TabOrder = 22
      end
      object DBEdit21: TDBEdit
        Left = 270
        Top = 56
        Width = 108
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'HHMM_R'
        DataSource = DsFormulas
        ReadOnly = True
        TabOrder = 21
      end
      object DBEdit20: TDBEdit
        Left = 162
        Top = 56
        Width = 107
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'HHMM_P'
        DataSource = DsFormulas
        ReadOnly = True
        TabOrder = 20
      end
      object DBEdit1: TDBEdit
        Left = 71
        Top = 56
        Width = 86
        Height = 21
        DataField = 'Peso'
        DataSource = DsFormulas
        TabOrder = 7
      end
      object DBEdit2: TDBEdit
        Left = 6
        Top = 56
        Width = 61
        Height = 21
        DataField = 'Qtde'
        DataSource = DsFormulas
        TabOrder = 6
      end
      object DBEdit3: TDBEdit
        Left = 489
        Top = 56
        Width = 36
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'EMCM'
        DataSource = DsFormulas
        ReadOnly = True
        TabOrder = 23
      end
      object DBEdit5: TDBEdit
        Left = 530
        Top = 56
        Width = 36
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'MEDIAPESO'
        DataSource = DsFormulas
        ReadOnly = True
        TabOrder = 24
      end
      object DBRGTipificacao: TDBRadioGroup
        Left = 6
        Top = 80
        Width = 459
        Height = 57
        Caption = ' Tipifica'#231#227'o: '
        Columns = 5
        DataField = 'Tipificacao'
        DataSource = DsFormulas
        Items.Strings = (
          'N/D'
          'Integral'
          'Laminado'
          'Repartido'
          'Dividido'
          'Rebaixado'
          'Raspa'
          'Apara'
          'Tapete')
        TabOrder = 11
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8')
      end
      object EdDBClienteI: TDBEdit
        Left = 570
        Top = 56
        Width = 35
        Height = 21
        DataField = 'ClienteI'
        DataSource = DsFormulas
        TabOrder = 8
      end
      object DBCheckBox1: TDBCheckBox
        Left = 472
        Top = 116
        Width = 49
        Height = 17
        Caption = 'Ativa.'
        DataField = 'Ativo'
        DataSource = DsFormulas
        TabOrder = 13
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object CBClienteI: TDBLookupComboBox
        Left = 608
        Top = 56
        Width = 183
        Height = 21
        DataField = 'ClienteI'
        DataSource = DsFormulas
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsEntidades
        TabOrder = 9
      end
      object CBEspessura: TDBLookupComboBox
        Left = 766
        Top = 18
        Width = 75
        Height = 21
        DataField = 'Espessura'
        DataSource = DsFormulas
        KeyField = 'Codigo'
        ListField = 'Linhas'
        ListSource = DsEspessuras
        TabOrder = 4
      end
      object CBSetor: TDBLookupComboBox
        Left = 866
        Top = 18
        Width = 107
        Height = 21
        DataField = 'Setor'
        DataSource = DsFormulas
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsListaSetores
        TabOrder = 5
        OnClick = CBSetorClick
      end
      object DBCheckBox2: TDBCheckBox
        Left = 520
        Top = 116
        Width = 81
        Height = 17
        Caption = #201' retrabalho.'
        DataField = 'Retrabalho'
        DataSource = DsFormulas
        TabOrder = 14
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 468
        Top = 80
        Width = 133
        Height = 33
        Caption = ' Baixa estq VS In Natura: '
        Columns = 3
        DataField = 'BxaEstqVS'
        DataSource = DsFormulas
        Items.Strings = (
          '???'
          'N'#227'o'
          'Sim')
        TabOrder = 12
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8')
      end
      object EdDBGraGruX: TDBEdit
        Left = 610
        Top = 106
        Width = 56
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsFormulas
        TabOrder = 15
      end
      object CBGraGruX: TDBLookupComboBox
        Left = 668
        Top = 106
        Width = 325
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsFormulas
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 16
      end
      object EdDBGraCorCad: TDBEdit
        Left = 454
        Top = 18
        Width = 40
        Height = 21
        DataField = 'GraCorCad'
        DataSource = DsFormulas
        TabOrder = 1
      end
      object CBGraCorCad: TDBLookupComboBox
        Left = 492
        Top = 18
        Width = 169
        Height = 21
        DataField = 'NoGraCorCad'
        DataSource = DsFormulas
        ListField = 'Nome'
        TabOrder = 2
      end
      object CBRebaixe: TDBLookupComboBox
        Left = 666
        Top = 18
        Width = 75
        Height = 21
        DataField = 'EspRebaixe'
        DataSource = DsFormulas
        KeyField = 'Codigo'
        ListField = 'Linhas'
        ListSource = DsRebaixe
        TabOrder = 3
      end
    end
  end
  object PainelGerencia: TPanel
    Left = 0
    Top = 433
    Width = 1008
    Height = 96
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object PainelControle: TPanel
      Left = 0
      Top = 48
      Width = 1008
      Height = 48
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 448
        Top = 1
        Width = 559
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 464
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtSaidaClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object BtDuplica: TBitBtn
          Tag = 177
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Duplica'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtDuplicaClick
        end
        object BtImportar: TBitBtn
          Tag = 19
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'I&mporta'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtImportarClick
        end
      end
    end
    object PainelConfirma: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 48
      Align = alBottom
      TabOrder = 1
      Visible = False
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confirma inclus'#245'es / altera'#231#245'es'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 900
        Top = 1
        Width = 107
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 13
          Left = 7
          Top = 3
          Width = 90
          Height = 40
          Hint = 'Desiste de inclus'#245'es / altera'#231#245'es'
          Caption = '&Desiste'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 189
    Width = 1008
    Height = 228
    Align = alClient
    Color = clWhite
    DataSource = DsFormulasIts
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDrawColumnCell = DBGrid1DrawColumnCell
    OnKeyDown = DBGrid1KeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'STATUSPQ'
        Title.Caption = 'Status do prod.'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Ordem'
        Title.Caption = 'Seq.'
        Width = 28
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Processo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Porcent'
        Title.Alignment = taRightJustify
        Title.Caption = ' % de uso'
        Width = 54
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Produto'
        Title.Alignment = taRightJustify
        Title.Caption = 'PQ [F4]'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEPQ'
        Title.Caption = 'Descri'#231#227'o (Produto ou Texto) [F7]'
        Width = 170
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Boca'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Title.Caption = 'B?'
        Width = 18
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Diluicao'
        Title.Alignment = taRightJustify
        Title.Caption = 'Dil.1:x'
        Width = 38
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Graus'
        Title.Alignment = taRightJustify
        Title.Caption = #186' C'
        Width = 20
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'TempoR'
        Title.Alignment = taRightJustify
        Title.Caption = 'Roda'
        Width = 34
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'TempoP'
        Title.Alignment = taRightJustify
        Title.Caption = 'P'#225'ra'
        Width = 35
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pH_Min'
        Title.Alignment = taRightJustify
        Title.Caption = 'pH min'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pH_Max'
        Title.Caption = 'pH m'#225'x'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Be_Min'
        Title.Alignment = taRightJustify
        Title.Caption = #186' B'#233' min'
        Width = 44
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Be_Max'
        Title.Caption = #186' B'#233' m'#225'x'
        Width = 44
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GC_Min'
        Title.Caption = #186'C Min'
        Width = 36
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GC_Max'
        Title.Caption = #186'C Max'
        Width = 36
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Obs'
        Title.Caption = 'Observa'#231#245'es'
        Width = 114
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ObsProces'
        Title.Caption = 'Observa'#231#227'o processso'
        Width = 214
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Minimo'
        Title.Caption = 'M'#237'n.'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Maximo'
        Title.Caption = 'M'#225'x.'
        Width = 30
        Visible = True
      end>
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 239
        Height = 32
        Caption = 'Receitas de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 239
        Height = 32
        Caption = 'Receitas de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 239
        Height = 32
        Caption = 'Receitas de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaAviso1: TLabel
        Left = 269
        Top = 22
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 268
        Top = 21
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object TbFormulas: TMySQLTable
    Database = Dmod.MyDB
    BeforeClose = TbFormulasBeforeClose
    AfterInsert = TbFormulasAfterInsert
    BeforePost = TbFormulasBeforePost
    AfterScroll = TbFormulasAfterScroll
    OnCalcFields = TbFormulasCalcFields
    TableName = 'formulas'
    Left = 24
    Top = 196
    object TbFormulasNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'formulas.Numero'
    end
    object TbFormulasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'formulas.Nome'
      Size = 30
    end
    object TbFormulasClienteI: TIntegerField
      FieldName = 'ClienteI'
      Origin = 'formulas.ClienteI'
    end
    object TbFormulasTipificacao: TIntegerField
      FieldName = 'Tipificacao'
      Origin = 'formulas.Tipificacao'
    end
    object TbFormulasDataI: TDateField
      FieldName = 'DataI'
      Origin = 'formulas.DataI'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbFormulasDataA: TDateField
      FieldName = 'DataA'
      Origin = 'formulas.DataA'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbFormulasTecnico: TWideStringField
      FieldName = 'Tecnico'
      Origin = 'formulas.Tecnico'
    end
    object TbFormulasTempoR: TIntegerField
      FieldName = 'TempoR'
      Origin = 'formulas.TempoR'
    end
    object TbFormulasTempoP: TIntegerField
      FieldName = 'TempoP'
      Origin = 'formulas.TempoP'
    end
    object TbFormulasTempoT: TIntegerField
      FieldName = 'TempoT'
      Origin = 'formulas.TempoT'
    end
    object TbFormulasHidrica: TIntegerField
      FieldName = 'Hidrica'
      Origin = 'formulas.Hidrica'
    end
    object TbFormulasLinhaTE: TIntegerField
      FieldName = 'LinhaTE'
      Origin = 'formulas.LinhaTE'
    end
    object TbFormulasCaldeira: TIntegerField
      FieldName = 'Caldeira'
      Origin = 'formulas.Caldeira'
    end
    object TbFormulasSetor: TIntegerField
      FieldName = 'Setor'
      Origin = 'formulas.Setor'
    end
    object TbFormulasEspessura: TIntegerField
      FieldName = 'Espessura'
      Origin = 'formulas.Espessura'
    end
    object TbFormulasPeso: TFloatField
      FieldName = 'Peso'
      Origin = 'formulas.Peso'
      DisplayFormat = '#,###,##0.0'
    end
    object TbFormulasQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'formulas.Qtde'
      DisplayFormat = '#,###,##0'
    end
    object TbFormulasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'formulas.Lk'
    end
    object TbFormulasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'formulas.DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbFormulasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'formulas.DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbFormulasUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'formulas.UserCad'
    end
    object TbFormulasUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'formulas.UserAlt'
    end
    object TbFormulasNOMECLIENTEI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECLIENTEI'
      LookupDataSet = QrEntidades
      LookupKeyFields = 'Codigo'
      LookupResultField = 'NOMEENTIDADE'
      KeyFields = 'ClienteI'
      Size = 100
      Lookup = True
    end
    object TbFormulasNOMESETOR: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMESETOR'
      LookupDataSet = QrListaSetores
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Setor'
      Size = 30
      Lookup = True
    end
    object TbFormulasLINHAS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LINHAS'
      LookupDataSet = QrEspessuras
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Linhas'
      KeyFields = 'Espessura'
      Size = 30
      Lookup = True
    end
    object TbFormulasMEDIAPESO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MEDIAPESO'
      DisplayFormat = '#,###,##0.0'
      Calculated = True
    end
    object TbFormulasEMCM: TFloatField
      FieldKind = fkLookup
      FieldName = 'EMCM'
      LookupDataSet = QrEspessuras
      LookupKeyFields = 'Codigo'
      LookupResultField = 'EMCM'
      KeyFields = 'Espessura'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object TbFormulasHorasR: TIntegerField
      FieldName = 'HorasR'
      Origin = 'formulas.HorasR'
    end
    object TbFormulasHorasP: TIntegerField
      FieldName = 'HorasP'
      Origin = 'formulas.HorasP'
    end
    object TbFormulasHorasT: TIntegerField
      FieldName = 'HorasT'
      Origin = 'formulas.HorasT'
    end
    object TbFormulasHHMM_P: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_P'
      Size = 10
      Calculated = True
    end
    object TbFormulasHHMM_R: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_R'
      Size = 10
      Calculated = True
    end
    object TbFormulasHHMM_T: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_T'
      Size = 10
      Calculated = True
    end
    object TbFormulasAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'formulas.Ativo'
    end
    object TbFormulasRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
      Origin = 'formulas.Retrabalho'
    end
    object TbFormulasBxaEstqVS: TSmallintField
      FieldName = 'BxaEstqVS'
      Origin = 'formulas.BxaEstqVS'
    end
    object TbFormulasGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'formulas.GraGruX'
    end
    object TbFormulasGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'formulas.GraCorCad'
    end
    object TbFormulasNoGraCorCad: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NoGraCorCad'
      LookupDataSet = QrGraCorCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'GraCorCad'
      Size = 60
      Lookup = True
    end
    object TbFormulasEspRebaixe: TIntegerField
      FieldName = 'EspRebaixe'
      Origin = 'formulas.EspRebaixe'
    end
    object TbFormulasLinReb: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LinReb'
      LookupDataSet = QrRebaixe
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Linhas'
      KeyFields = 'EspRebaixe'
      Size = 7
      Lookup = True
    end
  end
  object DsFormulas: TDataSource
    DataSet = TbFormulas
    Left = 20
    Top = 244
  end
  object TbFormulasIts: TMySQLTable
    Database = Dmod.MyDB
    BeforeInsert = TbFormulasItsBeforeInsert
    BeforePost = TbFormulasItsBeforePost
    AfterPost = TbFormulasItsAfterPost
    AfterDelete = TbFormulasItsAfterDelete
    OnNewRecord = TbFormulasItsNewRecord
    IndexName = 'PRIMARY'
    MasterFields = 'Numero'
    MasterSource = DsFormulas
    readOnly = True
    TableName = 'formulasits'
    Left = 88
    Top = 196
    object TbFormulasItsNumero: TIntegerField
      FieldName = 'Numero'
    end
    object TbFormulasItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object TbFormulasItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object TbFormulasItsReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object TbFormulasItsPorcent: TFloatField
      FieldName = 'Porcent'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object TbFormulasItsProduto: TIntegerField
      FieldName = 'Produto'
      DisplayFormat = '0;-0; '
    end
    object TbFormulasItsTempoR: TIntegerField
      FieldName = 'TempoR'
      DisplayFormat = '0;-0; '
    end
    object TbFormulasItsTempoP: TIntegerField
      FieldName = 'TempoP'
      DisplayFormat = '0;-0; '
    end
    object TbFormulasItsObs: TWideStringField
      FieldName = 'Obs'
    end
    object TbFormulasItsSC: TIntegerField
      FieldName = 'SC'
      DisplayFormat = '0;-0; '
    end
    object TbFormulasItsReciclo: TIntegerField
      FieldName = 'Reciclo'
    end
    object TbFormulasItsDiluicao: TFloatField
      FieldName = 'Diluicao'
      DisplayFormat = '0;-0; '
    end
    object TbFormulasItsCProd: TFloatField
      FieldName = 'CProd'
      DisplayFormat = '#,###,##0.0000'
    end
    object TbFormulasItsCSolu: TFloatField
      FieldName = 'CSolu'
      DisplayFormat = '#,###,##0.0000'
    end
    object TbFormulasItsCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '#,###,##0.00'
    end
    object TbFormulasItsMinimo: TFloatField
      FieldName = 'Minimo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object TbFormulasItsMaximo: TFloatField
      FieldName = 'Maximo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object TbFormulasItsGraus: TFloatField
      FieldName = 'Graus'
      DisplayFormat = '0;-0; '
    end
    object TbFormulasItsBoca: TWideStringField
      FieldName = 'Boca'
      Size = 1
    end
    object TbFormulasItsNOMEPQ: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMEPQ'
      LookupDataSet = QrPQs
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Produto'
      Size = 100
      Lookup = True
    end
    object TbFormulasItsProcesso: TWideStringField
      FieldName = 'Processo'
      Size = 30
    end
    object TbFormulasItsObsProces: TWideStringField
      FieldName = 'ObsProces'
      Size = 100
    end
    object TbFormulasItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object TbFormulasItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object TbFormulasItspH_Min: TFloatField
      FieldName = 'pH_Min'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbFormulasItspH_Max: TFloatField
      FieldName = 'pH_Max'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbFormulasItsBe_Min: TFloatField
      FieldName = 'Be_Min'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbFormulasItsBe_Max: TFloatField
      FieldName = 'Be_Max'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbFormulasItsGC_Min: TFloatField
      FieldName = 'GC_Min'
      DisplayFormat = '0;-0; '
    end
    object TbFormulasItsGC_Max: TFloatField
      FieldName = 'GC_Max'
      DisplayFormat = '0;-0; '
    end
    object TbFormulasItsSTATUSPQ: TWideStringField
      FieldKind = fkLookup
      FieldName = 'STATUSPQ'
      LookupDataSet = QrPQs
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Status_TXT'
      KeyFields = 'Produto'
      Size = 30
      Lookup = True
    end
  end
  object DsFormulasIts: TDataSource
    DataSet = TbFormulasIts
    Left = 92
    Top = 244
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 88
    Top = 292
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 88
    Top = 340
  end
  object QrListaSetores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 168
    Top = 196
    object QrListaSetoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'listasetores.Codigo'
    end
    object QrListaSetoresNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'listasetores.Nome'
    end
  end
  object DsListaSetores: TDataSource
    DataSet = QrListaSetores
    Left = 168
    Top = 244
  end
  object QrEspessuras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Linhas, EMCM'
      'FROM espessuras'
      'ORDER BY EMCM')
    Left = 20
    Top = 292
    object QrEspessurasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'espessuras.Codigo'
    end
    object QrEspessurasLinhas: TWideStringField
      DisplayWidth = 20
      FieldName = 'Linhas'
      Origin = 'espessuras.Linhas'
    end
    object QrEspessurasEMCM: TFloatField
      FieldName = 'EMCM'
      Origin = 'espessuras.EMCM'
    end
  end
  object DsEspessuras: TDataSource
    DataSet = QrEspessuras
    Left = 20
    Top = 340
  end
  object QrPQs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ativo'
      'FROM pq'
      'WHERE Ativo = 1'
      'AND (GGXNiv2>0 OR Codigo<0)'
      'ORDER BY Nome'
      '')
    Left = 168
    Top = 292
    object QrPQsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPQsStatus_TXT: TWideStringField
      FieldName = 'Status_TXT'
      Size = 30
    end
  end
  object DsPQs: TDataSource
    DataSet = QrPQs
    Left = 168
    Top = 340
  end
  object QrReordem: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'UPDATE formulasits SET'
      'Ordem=Reordem'
      'WHERE Numero=:P0'
      '')
    Left = 248
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(TempoR) TempoR, SUM(TempoP) TempoP'
      'FROM formulasits'
      'WHERE Numero=:P0')
    Left = 248
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaTempoR: TFloatField
      FieldName = 'TempoR'
    end
    object QrSomaTempoP: TFloatField
      FieldName = 'TempoP'
    end
  end
  object PMImporta: TPopupMenu
    Left = 512
    Top = 432
    object Receitadeanlogos1: TMenuItem
      Caption = '&Receita de an'#225'logos'
      OnClick = Receitadeanlogos1Click
    end
  end
  object QrDup: TMySQLQuery
    Database = Dmod.MyDB
    Left = 249
    Top = 290
  end
  object QrIns: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'INSERT INTO formulasits SET '
      'Ordem=:P0,'
      'Controle=:P1,'
      'Porcent=:P2,'
      'Produto=:P3,'
      'Graus=:P4,'
      'TempoR=:P5,'
      'TempoP=:P6,'
      'pH_Min=:P7,'
      'pH_Max=:P8,'
      'Be_Min=:P9,'
      'Be_Max=:P10,'
      'Obs=:P11,'
      'SC=:P12,'
      'Reciclo=:P13,'
      'Diluicao=:P14,'
      'Minimo=:P15,'
      'Maximo=:P16,'
      'GC_Min=:P17,'
      'GC_Max=:P18,'
      'Processo=:P19,'
      'ObsProces=:P20,'
      'Numero=:P21')
    Left = 249
    Top = 338
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P9'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P10'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P11'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P12'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P13'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P14'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P15'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P16'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P17'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P18'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P19'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P20'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P21'
        ParamType = ptUnknown
      end>
  end
  object QrSetor: TMySQLQuery
    Database = Dmod.MyDB
    Left = 456
    Top = 284
    object QrSetorTpReceita: TSmallintField
      FieldName = 'TpReceita'
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 344
    Top = 232
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 344
    Top = 276
  end
  object QrGraCorCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gracorcad'
      'ORDER BY Nome')
    Left = 560
    Top = 280
    object QrGraCorCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gracorcad.Codigo'
    end
    object QrGraCorCadNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
  end
  object DsGraCorCad: TDataSource
    DataSet = QrGraCorCad
    Left = 556
    Top = 328
  end
  object QrRebaixe: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Linhas, EMCM'
      'FROM espessuras'
      'ORDER BY EMCM')
    Left = 340
    Top = 324
    object QrRebaixeCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'espessuras.Codigo'
    end
    object QrRebaixeLinhas: TWideStringField
      FieldName = 'Linhas'
      Origin = 'espessuras.Linhas'
      Size = 7
    end
    object QrRebaixeEMCM: TFloatField
      FieldName = 'EMCM'
      Origin = 'espessuras.EMCM'
    end
  end
  object DsRebaixe: TDataSource
    DataSet = QrRebaixe
    Left = 340
    Top = 372
  end
  object QrSeq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 684
    Top = 340
    object QrSeqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSeqOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrSeqReordem: TIntegerField
      FieldName = 'Reordem'
    end
  end
end
