unit WBMovImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables, frxClass, frxDBSet, dmkEditDateTimePicker, Variants,
  dmkDBGridDAC, Vcl.Menus, UnAppEnums;

type
  TFmWBMovImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label7: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    PCRelatorio: TPageControl;
    TabSheet1: TTabSheet;
    Panel6: TPanel;
    QrEstqR1: TmySQLQuery;
    QrEstqR1GraGruX: TIntegerField;
    QrEstqR1Pecas: TFloatField;
    QrEstqR1PesoKg: TFloatField;
    QrEstqR1AreaM2: TFloatField;
    QrEstqR1AreaP2: TFloatField;
    QrEstqR1GraGru1: TIntegerField;
    QrEstqR1NO_PRD_TAM_COR: TWideStringField;
    frxWET_RECUR_004_00_A: TfrxReport;
    frxDsEstqR1: TfrxDBDataset;
    RG00_Ordem1: TRadioGroup;
    RG00_Ordem2: TRadioGroup;
    RG00_Ordem3: TRadioGroup;
    RG00_AgrupaWB: TRadioGroup;
    QrEstqR1Terceiro: TIntegerField;
    QrEstqR1NO_TERCEIRO: TWideStringField;
    QrEstqR1Pallet: TIntegerField;
    QrEstqR1NO_PALLET: TWideStringField;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    Qr01Fornecedor: TmySQLQuery;
    Qr01FornecedorCodigo: TIntegerField;
    Qr01FornecedorNOMEENTIDADE: TWideStringField;
    Ds01Fornecedor: TDataSource;
    Qr01WBPallet: TmySQLQuery;
    Qr01WBPalletCodigo: TIntegerField;
    Qr01WBPalletNome: TWideStringField;
    Ds01WBPallet: TDataSource;
    Qr01GraGruX: TmySQLQuery;
    Qr01GraGruXGraGru1: TIntegerField;
    Qr01GraGruXControle: TIntegerField;
    Qr01GraGruXNO_PRD_TAM_COR: TWideStringField;
    Qr01GraGruXSIGLAUNIDMED: TWideStringField;
    Qr01GraGruXCODUSUUNIDMED: TIntegerField;
    Qr01GraGruXNOMEUNIDMED: TWideStringField;
    Ds01GraGruX: TDataSource;
    Label1: TLabel;
    Ed01GraGruX: TdmkEditCB;
    CB01GraGruX: TdmkDBLookupComboBox;
    Label4: TLabel;
    Ed01Pallet: TdmkEditCB;
    CB01Pallet: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    TP01DataIni: TdmkEditDateTimePicker;
    Ck01DataIni: TCheckBox;
    Ck01DataFim: TCheckBox;
    TP01DataFim: TdmkEditDateTimePicker;
    QrEstqR2: TmySQLQuery;
    QrEstqR1ValorT: TFloatField;
    frxWET_RECUR_004_01_A: TfrxReport;
    frxDsEstqR2: TfrxDBDataset;
    QrEstqR2OrdGrupSeq: TIntegerField;
    QrEstqR2Codigo: TIntegerField;
    QrEstqR2Controle: TIntegerField;
    QrEstqR2MovimCod: TIntegerField;
    QrEstqR2MovimNiv: TIntegerField;
    QrEstqR2Empresa: TIntegerField;
    QrEstqR2Terceiro: TIntegerField;
    QrEstqR2MovimID: TIntegerField;
    QrEstqR2DataHora: TDateTimeField;
    QrEstqR2Pallet: TIntegerField;
    QrEstqR2GraGruX: TIntegerField;
    QrEstqR2Pecas: TFloatField;
    QrEstqR2PesoKg: TFloatField;
    QrEstqR2AreaM2: TFloatField;
    QrEstqR2AreaP2: TFloatField;
    QrEstqR2SrcMovID: TIntegerField;
    QrEstqR2SrcNivel1: TIntegerField;
    QrEstqR2SrcNivel2: TIntegerField;
    QrEstqR2SdoVrtPeca: TFloatField;
    QrEstqR2SdoVrtArM2: TFloatField;
    QrEstqR2Observ: TWideStringField;
    QrEstqR2ValorT: TFloatField;
    QrEstqR2GraGru1: TIntegerField;
    QrEstqR2NO_PRD_TAM_COR: TWideStringField;
    QrEstqR2NO_PALLET: TWideStringField;
    QrEstqR2NO_TERCEIRO: TWideStringField;
    QrEstqR2AcumPecas: TFloatField;
    QrEstqR2AcumPesoKg: TFloatField;
    QrEstqR2AcumAreaM2: TFloatField;
    QrEstqR2AcumAreaP2: TFloatField;
    QrEstqR2AcumValorT: TFloatField;
    QrEstqR2Ativo: TSmallintField;
    QrSumIR2: TmySQLQuery;
    QrSumIR2Pecas: TFloatField;
    QrSumIR2PesoKg: TFloatField;
    QrSumIR2AreaM2: TFloatField;
    QrSumIR2AreaP2: TFloatField;
    QrSumIR2ValorT: TFloatField;
    PB1: TProgressBar;
    Label10: TLabel;
    Ed01Terceiro: TdmkEditCB;
    CB01Terceiro: TdmkDBLookupComboBox;
    QrEstqR2NO_MovimID: TWideStringField;
    QrEstqR2CustoM: TFloatField;
    QrEstqR2AcumCustoM: TFloatField;
    TabSheet3: TTabSheet;
    Ck00DataCompra: TCheckBox;
    RG00ZeroNegat: TRadioGroup;
    Panel8: TPanel;
    Qr02Fornecedor: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    Ds02Fornecedor: TDataSource;
    Qr02WBPallet: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    Ds02WBPallet: TDataSource;
    Qr02GraGruX: TmySQLQuery;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    StringField3: TWideStringField;
    StringField4: TWideStringField;
    IntegerField5: TIntegerField;
    StringField5: TWideStringField;
    Ds02GraGruX: TDataSource;
    Panel9: TPanel;
    Label2: TLabel;
    Ed02GraGruX: TdmkEditCB;
    CB02GraGruX: TdmkDBLookupComboBox;
    Label3: TLabel;
    Ed02Pallet: TdmkEditCB;
    CB02Pallet: TdmkDBLookupComboBox;
    Label5: TLabel;
    Ed02Terceiro: TdmkEditCB;
    CB02Terceiro: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    TP02DataIni: TdmkEditDateTimePicker;
    Ck02DataIni: TCheckBox;
    Ck02DataFim: TCheckBox;
    TP02DataFim: TdmkEditDateTimePicker;
    Panel10: TPanel;
    BitBtn1: TBitBtn;
    QrWBMovIts: TmySQLQuery;
    DsWBMovIts: TDataSource;
    DG02WBMovIts: TDBGrid;
    QrWBMovItsCodigo: TIntegerField;
    QrWBMovItsControle: TIntegerField;
    QrWBMovItsMovimCod: TIntegerField;
    QrWBMovItsMovimNiv: TIntegerField;
    QrWBMovItsEmpresa: TIntegerField;
    QrWBMovItsTerceiro: TIntegerField;
    QrWBMovItsMovimID: TIntegerField;
    QrWBMovItsDataHora: TDateTimeField;
    QrWBMovItsPallet: TIntegerField;
    QrWBMovItsGraGruX: TIntegerField;
    QrWBMovItsPecas: TFloatField;
    QrWBMovItsPesoKg: TFloatField;
    QrWBMovItsAreaM2: TFloatField;
    QrWBMovItsAreaP2: TFloatField;
    QrWBMovItsValorT: TFloatField;
    QrWBMovItsSrcMovID: TIntegerField;
    QrWBMovItsSrcNivel1: TIntegerField;
    QrWBMovItsSrcNivel2: TIntegerField;
    QrWBMovItsSdoVrtPeca: TFloatField;
    QrWBMovItsSdoVrtArM2: TFloatField;
    QrWBMovItsObserv: TWideStringField;
    QrWBMovItsLk: TIntegerField;
    QrWBMovItsDataCad: TDateField;
    QrWBMovItsDataAlt: TDateField;
    QrWBMovItsUserCad: TIntegerField;
    QrWBMovItsUserAlt: TIntegerField;
    QrWBMovItsAlterWeb: TSmallintField;
    QrWBMovItsAtivo: TSmallintField;
    QrWBMovItsNO_PRD_TAM_COR: TWideStringField;
    QrWBMovItsNO_MovimID: TWideStringField;
    QrWBMovItsNO_Pallet: TWideStringField;
    QrWBMovItsNO_TERCEIRO: TWideStringField;
    Label6: TLabel;
    Ed02Controle: TdmkEdit;
    TabSheet4: TTabSheet;
    Panel11: TPanel;
    QrEstqR3: TmySQLQuery;
    frxWET_RECUR_004_03_A: TfrxReport;
    DBG03Estq: TdmkDBGridDAC;
    DsEstqR3: TDataSource;
    QrGGXPalTer: TmySQLQuery;
    frxDsGGXPalTer: TfrxDBDataset;
    Panel12: TPanel;
    Bt03Imprime: TBitBtn;
    Bt03Tudo: TBitBtn;
    Bt03Nenhum: TBitBtn;
    QrEstqR3GraGruX: TIntegerField;
    QrEstqR3Pecas: TFloatField;
    QrEstqR3PesoKg: TFloatField;
    QrEstqR3AreaM2: TFloatField;
    QrEstqR3AreaP2: TFloatField;
    QrEstqR3GraGru1: TIntegerField;
    QrEstqR3NO_PRD_TAM_COR: TWideStringField;
    QrEstqR3Terceiro: TIntegerField;
    QrEstqR3NO_TERCEIRO: TWideStringField;
    QrEstqR3Pallet: TIntegerField;
    QrEstqR3NO_PALLET: TWideStringField;
    QrEstqR3ValorT: TFloatField;
    QrGGXPalTerGraGruX: TIntegerField;
    QrGGXPalTerPecas: TFloatField;
    QrGGXPalTerPesoKg: TFloatField;
    QrGGXPalTerAreaM2: TFloatField;
    QrGGXPalTerAreaP2: TFloatField;
    QrGGXPalTerGraGru1: TIntegerField;
    QrGGXPalTerNO_PRD_TAM_COR: TWideStringField;
    QrGGXPalTerTerceiro: TIntegerField;
    QrGGXPalTerNO_TERCEIRO: TWideStringField;
    QrGGXPalTerPallet: TIntegerField;
    QrGGXPalTerNO_PALLET: TWideStringField;
    QrGGXPalTerValorT: TFloatField;
    QrEstqR3Ativo: TSmallintField;
    QrGGXPalTerEmpresa: TIntegerField;
    QrEstqR3Empresa: TIntegerField;
    QrGGXPalTerNO_EMPRESA: TWideStringField;
    QrGGXPalTerDataHora: TDateTimeField;
    frxWET_RECUR_004_03_B: TfrxReport;
    PM03Imprime: TPopupMenu;
    Fichas1: TMenuItem;
    Lista1: TMenuItem;
    QrEstqR3OrdGGX: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_RECUR_004_00_AGetValue(const VarName: string;
      var Value: Variant);
    procedure QrEstqR2CalcFields(DataSet: TDataSet);
    procedure DG02WBMovItsDblClick(Sender: TObject);
    procedure QrEstqR3BeforeClose(DataSet: TDataSet);
    procedure QrEstqR3AfterOpen(DataSet: TDataSet);
    procedure Bt03TudoClick(Sender: TObject);
    procedure Bt03NenhumClick(Sender: TObject);
    procedure Bt03ImprimeClick(Sender: TObject);
    procedure Fichas1Click(Sender: TObject);
    procedure Lista1Click(Sender: TObject);
  private
    { Private declarations }
    FWBMovImp1, FWBMovImp2, FWBMovImp3: String;
    //
    function  DefineSQLTerceiro(const Obrigatorio: Boolean; const EdTerceiro:
              TdmkEditCB; var SQL: String): Boolean;
    function  DefineSQLPallet(const Obrigatorio: Boolean; Const EdPallet:
              TdmkEditCB; var SQL: String): Boolean;
    //
    procedure PesquisaFichasPallets();
    procedure ImprimeEstoqueReal();
    procedure ImprimeHistorico();
    procedure ImprimePallets(frxReport: TfrxReport; Titulo: String);
    //
    procedure InsereWBMovImp2(DataHora: String; AcumPecas, AcumPesoKg,
              AcumAreaM2, AcumAreaP2, AcumValorT: Double);
    procedure PesquisaLancamentos();

    procedure ReopenGraGruX(Qry: TmySQLQuery);
    procedure ReopenPallet(Qry: TmySQLQuery);
    procedure ReopenWBMovImp3(Empresa, GraGruX, Pallet, Terceiro: Integer);

    procedure SetHabilitado03(Habilitado: Boolean);
    procedure SetaTodosItens03(Ativo: Byte);

  public
    { Public declarations }
  end;

  var
  FmWBMovImp: TFmWBMovImp;

implementation

uses UnMyObjects, Module, ModuleGeral, UnDmkProcFunc, DmkDAC_PF, UMySQLModule,
  CreateBlueDerm, AppListas, Principal;

{$R *.DFM}

procedure TFmWBMovImp.Bt03ImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PM03Imprime, Bt03Imprime);
end;

procedure TFmWBMovImp.Bt03NenhumClick(Sender: TObject);
begin
  SetaTodosItens03(0);
end;

procedure TFmWBMovImp.Bt03TudoClick(Sender: TObject);
begin
  SetaTodosItens03(1);
end;

procedure TFmWBMovImp.BtOKClick(Sender: TObject);
begin
  case PCRelatorio.ActivePageIndex of
    0: ImprimeEstoqueReal();
    1: ImprimeHistorico();
    2: PesquisaLancamentos();
    3: PesquisaFichasPallets();
    //
    else Geral.MB_Erro('Relat�rio n�o implementado!');
  end;
end;

procedure TFmWBMovImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmWBMovImp.DefineSQLPallet(const Obrigatorio: Boolean;
  const EdPallet: TdmkEditCB; var SQL: String): Boolean;
var
  Pallet: Integer;
begin
  Result := False;
  SQL := '';
  Pallet := EdPallet.ValueVariant;
  if MyObjects.FIC((Pallet = 0) and Obrigatorio, EdPallet,
  'Informe o Pallet!') then
    Exit;
  if Pallet <> 0 then
    SQL := 'AND wmi.Pallet=' + Geral.FF0(Pallet);
  Result := True;
end;

function TFmWBMovImp.DefineSQLTerceiro(const Obrigatorio: Boolean; const
EdTerceiro: TdmkEditCB; var SQL:
String): Boolean;
var
  Terceiro: Integer;
begin
  Result := False;
  SQL := '';
  Terceiro := EdTerceiro.ValueVariant;
  if MyObjects.FIC((Terceiro = 0) and Obrigatorio, EdTerceiro,
  'Informe o terceiro!') then
    Exit;
  if Terceiro <> 0 then
    SQL := 'AND wmi.Terceiro=' + Geral.FF0(Terceiro);
  Result := True;
end;

procedure TFmWBMovImp.DG02WBMovItsDblClick(Sender: TObject);
  procedure ObtemCodigoControleDeOrigem(const SrcMovID, SrcNivel1, SrcNivel2:
  Integer; var Codigo, Controle: Integer; var MovimID: TEstqMovimID);
  var
    Qry: TmySQLQuery;
  begin
    Qry := TmySQLQuery.Create(Self);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo, Controle, MovimID ',
      'FROM wbmovits ',
      'WHERE MovimCod=' + Geral.FF0(SrcMovID),
      'AND Codigo=' + Geral.FF0(SrcNivel1),
      'AND Controle=' + Geral.FF0(SrcNivel2),
      '']);
      //
      Codigo   := Qry.FieldByName('Codigo').AsInteger;
      Controle := Qry.FieldByName('Controle').AsInteger;
      MovimID  := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    finally
      Qry.Free;
    end;
  end;
var
  Codigo, Controle, SrcMovID, SrcNivel1, SrcNivel2, Reclasif, LocCtrl: Integer;
  Qry: TmySQLQuery;
  MovimID: TEstqMovimID;
begin
  Codigo    := QrWBMovItsCodigo.Value;
  Controle  := QrWBMovItsControle.Value;
  SrcMovID  := QrWBMovItsSrcMovID.Value;
  SrcNivel1 := QrWBMovItsSrcNivel1.Value;
  SrcNivel2 := QrWBMovItsSrcNivel2.Value;
  Reclasif  := QrWBMovItsControle.Value;
  LocCtrl   := QrWBMovItsControle.Value;
  //
  MovimID := TEstqMovimID(QrWBMovItsMovimID.Value);
  if MovimID = emidReclasWE then
    ObtemCodigoControleDeOrigem(
        SrcMovID, SrcNivel1, SrcNivel2, Codigo, Controle, MovimID);
  case MovimID of
    emidAjuste: FmPrincipal.MostraFormWBAjsCab(Codigo, Controle);
    emidCompra: FmPrincipal.MostraFormWBInnCab(Codigo, Controle, Reclasif);
    emidVenda:  FmPrincipal.MostraFormWBOutCab(Codigo, Controle);
    //emidReclasWE: FmPrincipal.MostraFormWBInnCab(Codigo, Controle); redirecionado acima!!!
    emidBaixa:  FmPrincipal.MostraFormWBOutIts(stUpd, Controle, nil, nil);
    emidIndsWE: FmPrincipal.MostraFormWBIndsWE(stUpd, Controle, 0, 0, 0, 0, nil, fiwNone);
    else
      Geral.MB_Aviso('Tipo de movimento de estoque n�o implementado! [1]');
  end;
  //
  UnDmkDAC_PF.AbreQuery(QrWBMovIts, Dmod.MyDB);
  QrWBMovIts.Locate('Controle', LocCtrl, []);
end;

procedure TFmWBMovImp.Fichas1Click(Sender: TObject);
begin
  ImprimePallets(frxWET_RECUR_004_03_A, 'Fichas de Pallets');
end;

procedure TFmWBMovImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWBMovImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  PCRelatorio.ActivePageIndex := 0;
  //
  ReopenGraGruX(Qr01GraGruX);
  ReopenGraGruX(Qr02GraGruX);
  //
  ReopenPallet(Qr01WBPallet);
  ReopenPallet(Qr02WBPallet);
  //
  UnDmkDAC_PF.AbreQuery(Qr01Fornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(Qr02Fornecedor, Dmod.MyDB);
  //
  TP01DataIni.Date := Date - 90;
  TP01DataFim.Date := Date;
  //
  TP02DataIni.Date := Date - 90;
  TP02DataFim.Date := Date;
end;

procedure TFmWBMovImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBMovImp.frxWET_RECUR_004_00_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_WBNIVGER' then
    Value := Dmod.QrControleWBNivGer.Value
  else
  if VarName = 'VARF_TERCEIRO_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01Terceiro.Text, Ed01Terceiro.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_PALLET_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01Pallet.Text, Ed01Pallet.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_GRAGRUX_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01GraGruX.Text, Ed01GraGruX.ValueVariant, 'TODAS')
  else
  if VarName = 'VARF_PERIODO_1' then
    Value := dmkPF.PeriodoImp1(TP01DataIni.Date, TP01DataFim.Date,
    Ck01DataIni.Checked, Ck01DataFim.Checked, '', 'at�', '')
  else
end;

procedure TFmWBMovImp.ImprimeEstoqueReal();
const
  Ordens: array[0..2] of String = ('NO_PRD_TAM_COR', 'NO_TERCEIRO', 'NO_PALLET');
  Agrups: array[0..2] of String = ('wmi.GraGruX', 'wmi.Terceiro', 'wmi.Pallet');
var
  Empresa, Index: Integer;
  SQL_Empresa, SQL_DtHr, SQL_ZeroNegat, (*SQL_Terceiro,*) Ordem, GroupBy: String;
  Grupo1, Grupo2: TfrxGroupHeader;
  Futer1, Futer2: TfrxGroupFooter;
  Me_GH1, Me_FT1, Me_GH2, Me_FT2, MeValNome, MeTitNome, MeValCodi: TfrxMemoView;
begin
  FWBMovImp1 :=
    UnCreateBlueDerm.RecriaTempTableNovo(ntrttWBMovImp1, DModG.QrUpdPID1, False);
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  if Empresa <> 0 then
    SQL_Empresa := 'AND wmi.Empresa=' + Geral.FF0(Empresa)
  else
    SQL_Empresa := '';
  //
  if Ck00DataCompra.Checked then
    SQL_DtHr := 'AND DataHora > "1900-01-01"'
  else
    SQL_DtHr := '';
  //
  case RG00ZeroNegat.ItemIndex of
    0: SQL_ZeroNegat := 'WHERE Pecas >= 0.001 ';
    1: SQL_ZeroNegat := 'WHERE (Pecas >= 0.001 OR Pecas <= -0.001) ';
    2: SQL_ZeroNegat := '';
    else
    begin
      SQL_ZeroNegat := 'WHERE Pecas=???';
      Geral.MB_Erro('"Estoques a serem considerados" n�o implementado!');
    end;
  end;
  //DefineSQLTerceiro(False, SQL_Terceiro);
  GroupBy := 'GROUP BY ' + Agrups[RG00_Ordem1.ItemIndex];
  if RG00_AgrupaWB.ItemIndex > 0 then
    GroupBy := GroupBy + ', ' + Agrups[RG00_Ordem2.ItemIndex];
  if RG00_AgrupaWB.ItemIndex > 1 then
    GroupBy := GroupBy + ', ' + Agrups[RG00_Ordem3.ItemIndex];
  GroupBy := GroupBy + ';';
  //
  Ordem := 'ORDER BY ' +
    Ordens[RG00_Ordem1.ItemIndex] + ', ' +
    Ordens[RG00_Ordem2.ItemIndex] + ', ' +
    Ordens[RG00_Ordem3.ItemIndex];

  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR1, DModG.MyPID_DB, [
  'DELETE FROM ' + FWBMovImp1 + ';',
  '',
  'INSERT INTO ' + FWBMovImp1 + '',
  'SELECT wmi.Empresa, wmi.GraGruX, SUM(wmi.Pecas) Pecas, ',
  'SUM(wmi.PesoKg) PesoKg, SUM(wmi.AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, SUM(ValorT) ValorT, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wmi.Pallet, wbp.Nome NO_Pallet, ',
  'wmi.Terceiro, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  '"0000-00-00 00:00:00" DataHora, wmp.Ordem, 1 Ativo ',
  'FROM ' + TMeuDB + '.wbmovits wmi ',
  'LEFT JOIN ' + TMeuDB + '.wbmprcab   wmp ON wmp.GraGruX=wmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.wbpallet   wbp ON wbp.Codigo=wmi.Pallet   ',
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=wmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=wmi.Empresa ',
  'WHERE wmi.Controle <> 0 ',
  SQL_Empresa,
  SQL_DtHr,
  //SQL_Terceiro,
  GroupBy,
  '',
  'SELECT * FROM ' + FWBMovImp1,
  SQL_ZeroNegat,
  Ordem,
  '']);
  Grupo1 := frxWET_RECUR_004_00_A.FindObject('GH_01') as TfrxGroupHeader;
  Grupo1.Visible := RG00_AgrupaWB.ItemIndex > 0;
  Futer1 := frxWET_RECUR_004_00_A.FindObject('FT_01') as TfrxGroupFooter;
  Futer1.Visible := Grupo1.Visible;
  Me_GH1 := frxWET_RECUR_004_00_A.FindObject('Me_GH1') as TfrxMemoView;
  Me_FT1 := frxWET_RECUR_004_00_A.FindObject('Me_FT1') as TfrxMemoView;
  case RG00_Ordem1.ItemIndex of
    0:
    begin
      Grupo1.Condition := 'frxDsEstqR1."GraGruX"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."GraGruX"] - [frxDsEstqR1."NO_PRD_TAM_COR"]';
      Me_FT1.Memo.Text := '[frxDsEstqR1."GraGruX"] - [frxDsEstqR1."NO_PRD_TAM_COR"]: ';
    end;
    1:
    begin
      Grupo1.Condition := 'frxDsEstqR1."Terceiro"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."Terceiro"] - [frxDsEstqR1."NO_TERCEIRO"]';
      Me_FT1.Memo.Text := '[frxDsEstqR1."Terceiro"] - [frxDsEstqR1."NO_TERCEIRO"]: ';
    end;
    2:
    begin
      Grupo1.Condition := 'frxDsEstqR1."Pallet"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."Pallet"] - [frxDsEstqR1."NO_PALLET"]';
      Me_FT1.Memo.Text := '[frxDsEstqR1."Pallet"] - [frxDsEstqR1."NO_PALLET"]: ';
    end
    else Grupo1.Condition := '***ERRO***';
  end;
  //
  Grupo2 := frxWET_RECUR_004_00_A.FindObject('GH_02') as TfrxGroupHeader;
  Grupo2.Visible := RG00_AgrupaWB.ItemIndex > 1;
  Futer2 := frxWET_RECUR_004_00_A.FindObject('FT_02') as TfrxGroupFooter;
  Futer2.Visible := Grupo2.Visible;
  Me_GH2 := frxWET_RECUR_004_00_A.FindObject('Me_GH2') as TfrxMemoView;
  Me_FT2 := frxWET_RECUR_004_00_A.FindObject('Me_FT2') as TfrxMemoView;
  case RG00_Ordem2.ItemIndex of
    0:
    begin
      Grupo2.Condition := 'frxDsEstqR1."GraGruX"';
      Me_GH2.Memo.Text := '[frxDsEstqR1."GraGruX"] - [frxDsEstqR1."NO_PRD_TAM_COR"]';
      Me_FT2.Memo.Text := '[frxDsEstqR1."GraGruX"] - [frxDsEstqR1."NO_PRD_TAM_COR"]: ';
    end;
    1:
    begin
      Grupo2.Condition := 'frxDsEstqR1."Terceiro"';
      Me_GH2.Memo.Text := '[frxDsEstqR1."Terceiro"] - [frxDsEstqR1."NO_TERCEIRO"]';
      Me_FT2.Memo.Text := '[frxDsEstqR1."Terceiro"] - [frxDsEstqR1."NO_TERCEIRO"]: ';
    end;
    2:
    begin
      Grupo2.Condition := 'frxDsEstqR1."Pallet"';
      Me_GH2.Memo.Text := '[frxDsEstqR1."Pallet"] - [frxDsEstqR1."NO_PALLET"]';
      Me_FT2.Memo.Text := '[frxDsEstqR1."Pallet"] - [frxDsEstqR1."NO_PALLET"]: ';
    end
    else Grupo2.Condition := '***ERRO***';
  end;
  //
  MeValNome := frxWET_RECUR_004_00_A.FindObject('MeValNome') as TfrxMemoView;
  MeTitNome := frxWET_RECUR_004_00_A.FindObject('MeTitNome') as TfrxMemoView;
  MeValCodi := frxWET_RECUR_004_00_A.FindObject('MeValCodi') as TfrxMemoView;
  case RG00_AgrupaWB.Itemindex of
    0: Index := RG00_Ordem1.ItemIndex;
    1: Index := RG00_Ordem2.ItemIndex;
    2: Index := RG00_Ordem3.ItemIndex;
    else Index := -1;
  end;
  case Index of
    0:
    begin
      MeTitNome.Memo.Text := 'Nome da mat�ria-prima';
      MeValCodi.Memo.Text := '[frxDsEstqR1."GraGruX"]';
      MeValNome.Memo.Text := '[frxDsEstqR1."NO_PRD_TAM_COR"]';
    end;
    1:
    begin
      MeTitNome.Memo.Text := 'Nome do fornecedor';
      MeValCodi.Memo.Text := '[frxDsEstqR1."Terceiro"]';
      MeValNome.Memo.Text := '[frxDsEstqR1."NO_TERCEIRO"]';
    end;
    2:
    begin
      MeTitNome.Memo.Text := 'Nome do pallet';
      MeValCodi.Memo.Text := '[frxDsEstqR1."Pallet"]';
      MeValNome.Memo.Text := '[frxDsEstqR1."NO_PALLET"]';
    end;
    else
    begin
      MeTitNome.Memo.Text := '?????';
      MeValCodi.Memo.Text := '[frxDsEstqR1."???"]';
      MeValNome.Memo.Text := '[frxDsEstqR1."???"]'
    end;
  end;
  MyObjects.frxDefineDataSets(frxWET_RECUR_004_00_A, [
    DModG.frxDsDono,
    frxDsEstqR1
  ]);
  MyObjects.frxMostra(frxWET_RECUR_004_00_A, 'Estoque de MP para semi/acab.');
end;

procedure TFmWBMovImp.PesquisaFichasPallets();
var
  Empresa: Integer;
  SQL_Empresa: String;
begin
  FWBMovImp3 :=
    UnCreateBlueDerm.RecriaTempTableNovo(ntrttWBMovImp3, DModG.QrUpdPID1, False);
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  if Empresa <> 0 then
    SQL_Empresa := 'AND wmi.Empresa=' + Geral.FF0(Empresa)
  else
    SQL_Empresa := '';
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrEstqR3, DModG.MyPID_DB, [
  'DELETE FROM  ' + FWBMovImp3 + '; ',
  'INSERT INTO  ' + FWBMovImp3,
  'SELECT wmi.Empresa, wmi.GraGruX, SUM(wmi.Pecas) Pecas, ',
  'SUM(wmi.PesoKg) PesoKg, SUM(wmi.AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, SUM(ValorT) ValorT, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wmi.Pallet, wbp.Nome NO_Pallet, ',
  'wmi.Terceiro, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'MAX(wmi.DataHora) DataHora, wmp.Ordem, 1 Ativo ',
  'FROM ' + TMeuDB + '.wbmovits wmi ',
  'LEFT JOIN ' + TMeuDB + '.wbmprcab   wmp ON wmp.GraGruX=wmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=wmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=wmi.Empresa ',
  'WHERE wmi.Controle <> 0 ',
  SQL_Empresa,
  'GROUP BY wmi.Empresa, wmi.Pallet, wmi.Terceiro, wmi.GraGruX; ',
  '']);
  //
  ReopenWBMovImp3(0, 0, 0, 0);
end;

procedure TFmWBMovImp.ImprimeHistorico();
const
  TxtCalc = 'Calculando evolu��o do estoque. ';
var
  DataI, SQL_Periodo, SQL_Terceiro, SQL_Pallet, ATT_MovimID: String;
  Empresa, GraGruX, Pallet: Integer;
  //
  DataHora, TxtItem: String;
  AcumPecas, AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT: Double;
  Controle: Integer;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('wmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  //
  GragruX := Ed01GraGruX.ValueVariant;
  if MyObjects.FIC(GraGruX = 0, Ed01GraGruX, 'Informe a mat�ria-prima') then
    Exit;
  (*Empresa*) DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  FWBMovImp2 :=
    UnCreateBlueDerm.RecriaTempTableNovo(ntrttWBMovImp2, DModG.QrUpdPID1, False);
  //
  DataI := Geral.FDT(TP01DataIni.Date, 1);
  SQL_Periodo := dmkPF.SQL_Periodo('AND wmi.DataHora ',
    TP01DataIni.Date, TP01DataFim.Date, Ck01DataIni.Checked, Ck01DataFim.Checked);
  //
  DefineSQLTerceiro(False, Ed01Terceiro, SQL_Terceiro);
  DefineSQLPallet(False, Ed01Pallet, SQL_Pallet);

  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR2, DModG.MyPID_DB, [
  'DELETE FROM ' + FWBMovImp2,
  '; ',
  'INSERT INTO ' + FWBMovImp2,
  'SELECT 1 OrdGrupSeq, ',
  'wmi.Codigo, wmi.Controle, wmi.MovimCod, ',
  'wmi.MovimNiv, wmi.Empresa, wmi.Terceiro, ',
  'wmi.MovimID, wmi.DataHora, wmi.Pallet, ',
  'wmi.GraGruX, wmi.Pecas, wmi.PesoKg, ',
  'wmi.AreaM2, wmi.AreaP2, wmi.SrcMovID, ',
  'wmi.SrcNivel1, wmi.SrcNivel2, ',
  'wmi.SdoVrtPeca, wmi.SdoVrtArM2, ',
  'wmi.Observ, wmi.ValorT, ggx.GraGru1, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_PALLET, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, ',
  ATT_MovimID,
  '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, ',
  '0 AcumAreaP2, 0 AcumValorT, 1 Ativo ',
  'FROM ' + TMeuDB + '.wbmovits wmi ',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=wmi.Terceiro ',
  'WHERE wmi.Empresa=' + Geral.FF0(Empresa),
  'AND wmi.GraGruX=' + Geral.FF0(GraGruX),
  SQL_Periodo,
  SQL_Terceiro,
  SQL_Pallet,
  '; ',
  'SELECT * FROM ' + FWBMovImp2,
  'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; ',
  ' ']);
  TxtItem := TxtCalc + 'Item %d de ' + Geral.FF0(QrEstqR2.RecordCount) + '.';
  PB1.Position := 0;
  PB1.Max := QrEstqR2.RecordCount;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumIR2, Dmod.MyDB, [
  'SELECT SUM(wmi.Pecas) Pecas, SUM(wmi.PesoKg) PesoKg, ',
  'SUM(wmi.AreaM2) AreaM2, SUM(wmi.AreaP2) AreaP2, ',
  'SUM(wmi.ValorT) ValorT ',
  'FROM wbmovits wmi ',
  'WHERE wmi.Empresa=' + Geral.FF0(Empresa),
  'AND wmi.GraGruX=' + Geral.FF0(GraGruX),
  'AND wmi.DataHora  < "' + DataI + '" ',
  '']);
  DataHora   := Geral.FDT(TP01DataIni.Date, 1);
  AcumPecas  := QrSumIR2Pecas.Value;
  AcumPesoKg := QrSumIR2PesoKg.Value;
  AcumAreaM2 := QrSumIR2AreaM2.Value;
  AcumAreaP2 := QrSumIR2AreaP2.Value;
  AcumValorT := QrSumIR2ValorT.Value;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TxtCalc);
  InsereWBMovImp2(
    DataHora, AcumPecas, AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT);
  QrEstqR2.First;
  while not QrEstqR2.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      Format(TxtItem, [QrEstqR2.RecNo]));
    //
    Controle := QrEstqR2Controle.Value;
    //
    AcumPecas  := AcumPecas  + QrEstqR2Pecas.Value;
    AcumPesoKg := AcumPesoKg + QrEstqR2PesoKg.Value;
    AcumAreaM2 := AcumAreaM2 + QrEstqR2AreaM2.Value;
    AcumAreaP2 := AcumAreaP2 + QrEstqR2AreaP2.Value;
    AcumValorT := AcumValorT + QrEstqR2ValorT.Value;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FWBMovImp2, False, [
    'AcumPecas',
    'AcumPesoKg', 'AcumAreaM2', 'AcumAreaP2',
    'AcumValorT'], [
    'Controle'], [
    AcumPecas,
    AcumPesoKg, AcumAreaM2, AcumAreaP2,
    AcumValorT], [
    Controle], False);
    //
    QrEstqR2.Next;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR2, DModG.MyPID_DB, [
  'SELECT * FROM ' + FWBMovImp2,
  'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; ',
  ' ']);
  MyObjects.frxDefineDataSets(frxWET_RECUR_004_01_A, [
    DModG.frxDsDono,
    frxDsEstqR2
  ]);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  MyObjects.frxMostra(frxWET_RECUR_004_01_A, 'Movimento de MP para semi/acab.');
  PB1.Position := 0;
end;

procedure TFmWBMovImp.ImprimePallets(frxReport: TfrxReport; Titulo: String);
var
  Ordem: String;
begin
  Ordem := DBG03Estq.GetSortField;
  if Ordem <> '' then
    Ordem := 'ORDER BY ' + Ordem
  else
    Ordem := 'ORDER BY NO_PALLET, NO_TERCEIRO, OrdGGX, NO_PRD_TAM_COR ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGGXPalTer, DModG.MyPID_DB, [
  'SELECT * FROM  ' + FWBMovImp3,
  'WHERE (Pecas >= 0.001 OR Pecas <= -0.001) ',
  'AND Ativo=1 ',
  Ordem,
  '']);
  //
  MyObjects.frxDefineDataSets(frxReport, [
    DModG.frxDsDono,
    frxDsGGXPalTer
  ]);
  MyObjects.frxMostra(frxReport, Titulo);
end;

procedure TFmWBMovImp.InsereWBMovImp2(DataHora: String; AcumPecas, AcumPesoKg,
  AcumAreaM2, AcumAreaP2, AcumValorT: Double);
var
  //DataHora,
  Observ, NO_PRD_TAM_COR, NO_PALLET, NO_TERCEIRO: String;
  OrdGrupSeq, Codigo, Controle, MovimCod, MovimNiv, Empresa, Terceiro, MovimID,
  Pallet, GraGruX, SrcMovID, SrcNivel1, SrcNivel2, GraGru1: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, SdoVrtPeca, SdoVrtArM2, ValorT(*, AcumPecas,
  AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT*): Double;
begin
  OrdGrupSeq     := 0;
  Codigo         := 0;
  Controle       := 0;
  MovimCod       := 0;
  MovimNiv       := 0;
  Empresa        := 0;
  Terceiro       := 0;
  MovimID        := 0;
  //DataHora       := ;
  Pallet         := 0;
  GraGruX        := 0;
  Pecas          := 0;
  PesoKg         := 0;
  AreaM2         := 0;
  AreaP2         := 0;
  SrcMovID       := 0;
  SrcNivel1      := 0;
  SrcNivel2      := 0;
  SdoVrtPeca     := 0;
  SdoVrtArM2     := 0;
  Observ         := '';
  ValorT         := 0;
  GraGru1        := 0;
  NO_PRD_TAM_COR := '';
  NO_PALLET      := '';
  NO_TERCEIRO    := '';
{
  AcumPecas      := ;
  AcumPesoKg     := ;
  AcumAreaM2     := ;
  AcumAreaP2     := ;
  AcumValorT     := ;
}
  //
  //if
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FWBMovImp2, False, [
  'OrdGrupSeq', 'Codigo', 'Controle',
  'MovimCod', 'MovimNiv', 'Empresa',
  'Terceiro', 'MovimID', 'DataHora',
  'Pallet', 'GraGruX', 'Pecas',
  'PesoKg', 'AreaM2', 'AreaP2',
  'SrcMovID', 'SrcNivel1', 'SrcNivel2',
  'SdoVrtPeca', 'SdoVrtArM2', 'Observ',
  'ValorT', 'GraGru1', 'NO_PRD_TAM_COR',
  'NO_PALLET', 'NO_TERCEIRO', 'AcumPecas',
  'AcumPesoKg', 'AcumAreaM2', 'AcumAreaP2',
  'AcumValorT'], [
  ], [
  OrdGrupSeq, Codigo, Controle,
  MovimCod, MovimNiv, Empresa,
  Terceiro, MovimID, DataHora,
  Pallet, GraGruX, Pecas,
  PesoKg, AreaM2, AreaP2,
  SrcMovID, SrcNivel1, SrcNivel2,
  SdoVrtPeca, SdoVrtArM2, Observ,
  ValorT, GraGru1, NO_PRD_TAM_COR,
  NO_PALLET, NO_TERCEIRO, AcumPecas,
  AcumPesoKg, AcumAreaM2, AcumAreaP2,
  AcumValorT], [
  ], False);
end;

procedure TFmWBMovImp.Lista1Click(Sender: TObject);
begin
  ImprimePallets(frxWET_RECUR_004_03_B, 'Lista de Pallets');
end;

procedure TFmWBMovImp.PesquisaLancamentos();
var
  DataI, SQL_Periodo, SQL_Terceiro, SQL_GraGruX, SQL_Pallet, SQL_Controle,
  ATT_MovimID: String;
  Empresa, GraGruX, Pallet: Integer;
  //
  DataHora, TxtItem: String;
  AcumPecas, AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT: Double;
  Controle: Integer;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('wmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  //
  GraGruX := Ed02GraGruX.ValueVariant;
  if GraGruX = 0 then
    SQL_GraGruX := ''
  else
    SQL_GraGruX := 'AND wmi.GraGruX=' + Geral.FF0(GraGruX);
  //
  (*Empresa*) DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  DataI := Geral.FDT(TP02DataIni.Date, 1);
  SQL_Periodo := dmkPF.SQL_Periodo('AND wmi.DataHora ',
    TP02DataIni.Date, TP02DataFim.Date, Ck02DataIni.Checked, Ck02DataFim.Checked);
  //
  DefineSQLTerceiro(False, Ed02Terceiro, SQL_Terceiro);
  DefineSQLPallet(False, Ed02Pallet, SQL_Pallet);
  //
  Controle := Ed02Controle.ValueVariant;
  if Controle = 0 then
    SQL_Controle := ''
  else
    SQL_Controle := 'AND wmi.Controle=' + Geral.FF0(Controle);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBMovIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  ATT_MovimID,
  'wbp.Nome NO_Pallet, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro ',
  'WHERE wmi.Empresa=' + Geral.FF0(Empresa),
  SQL_GraGruX,
  SQL_Periodo,
  SQL_Terceiro,
  SQL_Pallet,
  SQL_Controle,
  'ORDER BY wmi.DataHora, wmi.Pecas DESC; ',
  ' ']);
  //
end;

procedure TFmWBMovImp.QrEstqR2CalcFields(DataSet: TDataSet);
var
  Valor: Double;
begin
  Valor := 0;
  case TEstqNivCtrl(Dmod.QrControleWBNivGer.Value) of
    encPecas: ;//Valor := 0;
    encArea: ;//Valor := 0;
    encValor:
    begin
      if (QrEstqR2AreaM2.Value >=  0.01)
      or (QrEstqR2AreaM2.Value <= -0.01) then
        Valor := QrEstqR2ValorT.Value / QrEstqR2AreaM2.Value
      else
      if QrEstqR2Pecas.Value > 0 then
        Valor := QrEstqR2ValorT.Value / QrEstqR2Pecas.Value;
    end;
    else
    begin
      //Valor := 0;
      Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [2]');
    end;
  end;
  QrEstqR2CustoM.Value := Valor;
  //
  Valor := 0;
  case TEstqNivCtrl(Dmod.QrControleWBNivGer.Value) of
    encPecas: ;//Valor := 0;
    encArea: ;//Valor := 0;
    encValor:
    begin
      if (QrEstqR2AcumAreaM2.Value >=  0.01)
      or (QrEstqR2AcumAreaM2.Value <= -0.01) then
        Valor := QrEstqR2AcumValorT.Value / QrEstqR2AcumAreaM2.Value
      else
      if QrEstqR2AcumPecas.Value > 0 then
        Valor := QrEstqR2AcumValorT.Value / QrEstqR2AcumPecas.Value;
    end;
    else
    begin
      //Valor := 0;
      Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [3]');
    end;
  end;
  QrEstqR2AcumCustoM.Value := Valor;
end;

procedure TFmWBMovImp.QrEstqR3AfterOpen(DataSet: TDataSet);
begin
  SetHabilitado03(QrEstqR3.RecordCount > 0);
end;

procedure TFmWBMovImp.QrEstqR3BeforeClose(DataSet: TDataSet);
begin
  SetHabilitado03(False);
end;

procedure TFmWBMovImp.ReopenGraGruX(Qry: TmySQLQuery);
begin
  if Qry.State = dsInactive then
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED ',
  'FROM wbmprcab wmp ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);
end;

procedure TFmWBMovImp.ReopenPallet(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM wbpallet ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmWBMovImp.ReopenWBMovImp3(Empresa, GraGruX, Pallet,
  Terceiro: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR3, DModG.MyPID_DB, [
  'SELECT * FROM  ' + FWBMovImp3,
  'WHERE (Pecas >= 0.001 OR Pecas <= -0.001) ',
  'ORDER BY NO_PALLET, NO_TERCEIRO, OrdGGX, NO_PRD_TAM_COR ',
  '']);
  //
  QrEstqR3.Locate('Empresa;GraGruX;Pallet;Terceiro',
  VarArrayOf([Empresa, GraGruX, Pallet, Terceiro]), []);
end;

procedure TFmWBMovImp.SetaTodosItens03(Ativo: Byte);
var
  Empresa, GraGruX, Pallet, Terceiro: Integer;
begin
  GraGruX  := QrEstqR3GraGruX.Value;
  Empresa  := QrEstqR3Empresa.Value;
  Terceiro := QrEstqR3Terceiro.Value;
  Pallet   := QrEstqR3Pallet.Value;
  //
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FWBMovImp3, False, [
  'Ativo'], [
  (*'Empresa', 'GraGruX', 'Pallet',
  'Terceiro'*)], [
  Ativo], [
  (*Empresa, GraGruX, Pallet,
  Terceiro*)], False) then
    ReopenWBMovImp3(Empresa, GraGruX, Pallet, Terceiro);
end;

procedure TFmWBMovImp.SetHabilitado03(Habilitado: Boolean);
begin
  Bt03Imprime.Enabled := Habilitado;
  Bt03Nenhum.Enabled := Habilitado;
  Bt03Tudo.Enabled := Habilitado;
end;

end.
