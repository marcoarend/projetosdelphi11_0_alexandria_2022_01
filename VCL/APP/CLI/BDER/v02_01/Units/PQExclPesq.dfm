object FmPQExclPesq: TFmPQExclPesq
  Left = 339
  Top = 185
  Caption = 'QUI-RECEI-009 :: Pesquisa em Gerenciamento de Pesagem '
  ClientHeight = 631
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 475
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 245
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 272
        Top = 4
        Width = 171
        Height = 13
        Caption = 'Receita de ribeira (F4 para amostra):'
      end
      object Label5: TLabel
        Left = 12
        Top = 4
        Width = 28
        Height = 13
        Caption = 'Setor:'
      end
      object EdReceita: TdmkEditCB
        Left = 272
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdReceitaChange
        OnKeyDown = EdReceitaKeyDown
        DBLookupComboBox = CBReceita
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBReceita: TdmkDBLookupComboBox
        Left = 328
        Top = 20
        Width = 657
        Height = 21
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsFormulas
        TabOrder = 3
        OnKeyDown = CBReceitaKeyDown
        dmkEditCB = EdReceita
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object GroupBox1: TGroupBox
        Left = 140
        Top = 44
        Width = 261
        Height = 69
        Caption = ' Per'#237'odo:'
        TabOrder = 5
        object Label2: TLabel
          Left = 8
          Top = 20
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
        end
        object Label3: TLabel
          Left = 132
          Top = 20
          Width = 48
          Height = 13
          Caption = 'Data final:'
        end
        object TPDataIni: TdmkEditDateTimePicker
          Left = 8
          Top = 36
          Width = 120
          Height = 21
          Date = 39851.000000000000000000
          Time = 0.643366516196692800
          TabOrder = 0
          OnChange = TPDataIniChange
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object TPDataFim: TdmkEditDateTimePicker
          Left = 132
          Top = 36
          Width = 120
          Height = 21
          Date = 39851.000000000000000000
          Time = 0.643413645819237000
          TabOrder = 1
          OnChange = TPDataFimChange
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
      end
      object EdSetor: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdReceitaChange
        DBLookupComboBox = CBSetor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSetor: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 201
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSetores
        TabOrder = 1
        dmkEditCB = EdSetor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CGStatus: TdmkCheckGroup
        Left = 404
        Top = 44
        Width = 129
        Height = 69
        Caption = '                                    '
        ItemIndex = 0
        Items.Strings = (
          'Custo n'#227'o rateado'
          'Custo rateado'
          'Custo alterado')
        TabOrder = 7
        OnClick = CGStatusClick
        UpdType = utYes
        Value = 1
        OldValor = 0
      end
      object CkStatus: TdmkCheckBox
        Left = 412
        Top = 42
        Width = 101
        Height = 17
        Caption = ' Status pesagem: '
        TabOrder = 6
        OnClick = CkStatusClick
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object GroupBox2: TGroupBox
        Left = 536
        Top = 44
        Width = 77
        Height = 69
        Caption = '                  '
        TabOrder = 8
        object Label8: TLabel
          Left = 8
          Top = 20
          Width = 43
          Height = 13
          Caption = 'N'#250'mero: '
        end
        object EdFulao: TdmkEdit
          Left = 8
          Top = 36
          Width = 65
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdFulaoChange
        end
        object CkFulao: TCheckBox
          Left = 12
          Top = -2
          Width = 53
          Height = 17
          Caption = 'Ful'#227'o:'
          TabOrder = 0
          OnClick = CkFulaoClick
        end
      end
      object GroupBox3: TGroupBox
        Left = 616
        Top = 44
        Width = 181
        Height = 69
        Caption = '                                  '
        TabOrder = 9
        object Label6: TLabel
          Left = 8
          Top = 20
          Width = 64
          Height = 13
          Caption = 'Peso m'#237'nimo:'
        end
        object Label7: TLabel
          Left = 92
          Top = 20
          Width = 65
          Height = 13
          Caption = 'Peso m'#225'ximo:'
        end
        object EdPesoMin: TdmkEdit
          Left = 8
          Top = 36
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPesoMinChange
          OnExit = EdPesoMinExit
        end
        object EdPesoMax: TdmkEdit
          Left = 92
          Top = 36
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPesoMaxChange
        end
        object CkCarga: TCheckBox
          Left = 12
          Top = -2
          Width = 97
          Height = 17
          Caption = 'Carga do ful'#227'o: '
          TabOrder = 0
          OnClick = CkCargaClick
        end
      end
      object GroupBox4: TGroupBox
        Left = 800
        Top = 44
        Width = 185
        Height = 69
        Caption = '                                            '
        TabOrder = 10
        object Label9: TLabel
          Left = 8
          Top = 20
          Width = 63
          Height = 13
          Caption = 'Qtde m'#237'nima:'
        end
        object Label10: TLabel
          Left = 92
          Top = 20
          Width = 64
          Height = 13
          Caption = 'Qtde m'#225'xima:'
        end
        object EdQtdeMin: TdmkEdit
          Left = 8
          Top = 36
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 1
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQtdeMinChange
          OnExit = EdQtdeMinExit
        end
        object EdQtdeMax: TdmkEdit
          Left = 92
          Top = 36
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 1
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQtdeMaxChange
        end
        object CkQtde: TCheckBox
          Left = 12
          Top = -2
          Width = 121
          Height = 17
          Caption = 'Quantidade no ful'#227'o: '
          TabOrder = 0
          OnClick = CkQtdeClick
        end
      end
      object GroupBox5: TGroupBox
        Left = 12
        Top = 116
        Width = 973
        Height = 61
        Caption = ' Produto qu'#237'mico: '
        TabOrder = 11
        object Label11: TLabel
          Left = 312
          Top = 16
          Width = 81
          Height = 13
          Caption = 'Produto qu'#237'mico:'
        end
        object EdPQ: TdmkEditCB
          Left = 312
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdReceitaChange
          DBLookupComboBox = CBPQ
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPQ: TdmkDBLookupComboBox
          Left = 368
          Top = 32
          Width = 401
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsPQ
          TabOrder = 1
          dmkEditCB = EdPQ
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object RGTipoProd: TRadioGroup
          Left = 2
          Top = 15
          Width = 307
          Height = 44
          Align = alLeft
          Caption = ' Onde pesquisar: '
          Columns = 2
          ItemIndex = 1
          Items.Strings = (
            'Receita espelho'
            'Baixa de insumo')
          TabOrder = 2
          OnClick = RGTipoProdClick
        end
        object CkPesoZero: TCheckBox
          Left = 772
          Top = 32
          Width = 197
          Height = 17
          Caption = 'Somente com peso diferente de zero.'
          Checked = True
          State = cbChecked
          TabOrder = 3
          OnClick = CkPesoZeroClick
        end
      end
      object GroupBox6: TGroupBox
        Left = 12
        Top = 176
        Width = 197
        Height = 65
        Caption = ' Lotes VMI: '
        TabOrder = 12
        object Panel2: TPanel
          Left = 2
          Top = 15
          Width = 193
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label12: TLabel
            Left = 8
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Marca: '
          end
          object EdMarca: TdmkEdit
            Left = 8
            Top = 20
            Width = 161
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdFulaoChange
          end
        end
      end
      object RGPeriodo: TRadioGroup
        Left = 12
        Top = 44
        Width = 125
        Height = 69
        Caption = ' Data: '
        ItemIndex = 0
        Items.Strings = (
          'Baixa'
          'Emiss'#227'o')
        TabOrder = 4
      end
      object CGFinalidrec: TdmkCheckGroup
        Left = 216
        Top = 176
        Width = 133
        Height = 65
        Caption = ' Finalidade da Receita: '
        Items.Strings = (
          'Amostra'
          'Produ'#231#227'o')
        TabOrder = 13
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
    end
    object dmkDBGridDAC1: TdmkDBGridDAC
      Left = 0
      Top = 245
      Width = 1008
      Height = 230
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Pesagem'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataEmis'
          Title.Caption = 'Emiss'#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Numero'
          Title.Caption = 'Receita'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME'
          Title.Caption = 'Nome receita'
          Width = 270
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'STATUS_TXT'
          Title.Caption = 'Status pesagem'
          Width = 145
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESETOR'
          Title.Caption = 'Setor'
          Width = 93
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fulao'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DefPeca'
          Title.Caption = 'Pe'#231'a'
          Width = 70
          Visible = True
        end>
      Color = clWindow
      DataSource = DsPesq
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = dmkDBGridDAC1DblClick
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Pesagem'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataEmis'
          Title.Caption = 'Emiss'#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Numero'
          Title.Caption = 'Receita'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME'
          Title.Caption = 'Nome receita'
          Width = 270
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'STATUS_TXT'
          Title.Caption = 'Status pesagem'
          Width = 145
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESETOR'
          Title.Caption = 'Setor'
          Width = 93
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fulao'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DefPeca'
          Title.Caption = 'Pe'#231'a'
          Width = 70
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 516
        Height = 32
        Caption = 'Pesquisa em Gerenciamento de Pesagem '
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 516
        Height = 32
        Caption = 'Pesquisa em Gerenciamento de Pesagem '
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 516
        Height = 32
        Caption = 'Pesquisa em Gerenciamento de Pesagem '
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 523
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 567
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 272
        Top = 20
        Width = 110
        Height = 13
        Caption = 'Receitas encontradas: '
      end
      object LaQtde: TLabel
        Left = 384
        Top = 20
        Width = 8
        Height = 13
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtPesquisa: TBitBtn
        Tag = 40
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtPesquisaClick
      end
      object BtLocaliza: TBitBtn
        Tag = 94
        Left = 140
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Localiza'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtLocalizaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 444
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Imprime'
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtImprimeClick
      end
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 60
    Top = 320
  end
  object QrFormulas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome'
      'FROM formulas'
      'ORDER BY Nome')
    Left = 60
    Top = 272
    object QrFormulasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    OnCalcFields = QrPesqCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM emit'
      'WHERE Codigo > 0'
      'AND DataEmis >= "2008-05-13" AND DataEmis < "2008-05-15" '
      'AND Numero = 4')
    Left = 136
    Top = 272
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrPesqStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrPesqNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrPesqNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrPesqNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrPesqTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrPesqNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrPesqClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrPesqTipificacao: TSmallintField
      FieldName = 'Tipificacao'
    end
    object QrPesqTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrPesqTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrPesqSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrPesqTipific: TSmallintField
      FieldName = 'Tipific'
    end
    object QrPesqEspessura: TWideStringField
      FieldName = 'Espessura'
      Size = 15
    end
    object QrPesqDefPeca: TWideStringField
      FieldName = 'DefPeca'
      Size = 15
    end
    object QrPesqPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0'
    end
    object QrPesqCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrPesqQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0'
    end
    object QrPesqAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrPesqFulao: TWideStringField
      FieldName = 'Fulao'
      Size = 5
    end
    object QrPesqObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrPesqLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPesqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPesqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPesqUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPesqUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPesqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPesqAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPesqSTATUS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS_TXT'
      Size = 30
      Calculated = True
    end
    object QrPesqNO_Grupo: TWideStringField
      FieldName = 'NO_Grupo'
      Size = 60
    end
    object QrPesqGrupo: TIntegerField
      FieldName = 'Grupo'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 136
    Top = 320
  end
  object QrSetores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 200
    Top = 272
  end
  object DsSetores: TDataSource
    DataSet = QrSetores
    Left = 200
    Top = 320
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM pq'
      'ORDER BY Nome')
    Left = 264
    Top = 272
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 264
    Top = 320
  end
  object frxQUI_RECEI_009_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '    '
      'end.')
    OnGetValue = frxQUI_RECEI_009_AGetValue
    Left = 556
    Top = 316
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 71.811060240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692784020000010000
          Width = 56.692686380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pesagem')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 328.818841500000000000
          Top = 56.692784020000010000
          Width = 79.370071420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Setor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 464.881921500000000000
          Top = 56.692784020000010000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590641499999900000
          Top = 56.692784020000010000
          Width = 124.724431420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LISTA DE PESAGENS PESQUISADAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Top = 56.692949999999990000
          Width = 207.874115830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Top = 56.692949999999990000
          Width = 64.251746380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 56.692949999999990000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ful'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 185.196970000000000000
          Top = 37.795300000000000000
          Width = 279.684780630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_REC_NOME]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Top = 37.795321970000010000
          Width = 44.881880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 511.984540000000000000
          Top = 37.795321970000010000
          Width = 164.440630000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Top = 37.795300000000000000
          Width = 44.881880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita:')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 34.015770000000010000
          Top = 37.795300000000000000
          Width = 105.826400630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SET_NOME]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 33.543290000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setor:')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118276220000000000
        Top = 192.756030000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
        RowCount = 0
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692686380000000000
          Height = 15.118110240000000000
          DataField = 'Codigo'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Top = 0.000165979999999996
          Width = 34.015735830000000000
          Height = 15.118110240000000000
          DataField = 'Numero'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."Numero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Top = 0.000165979999906085
          Width = 64.251746380000000000
          Height = 15.118110240000000000
          DataField = 'DataEmis'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."DataEmis"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Width = 173.858345830000000000
          Height = 15.118110240000000000
          DataField = 'NOME'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NOME"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 79.370071420000000000
          Height = 15.118110240000000000
          DataField = 'NOMESETOR'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NOMESETOR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'Peso'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '###,###,###.###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Width = 79.370071420000000000
          Height = 15.118110240000000000
          DataField = 'Qtde'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '###,###,###.###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189508500000000000
          Top = 0.000165979999906085
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          DataField = 'Fulao'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."Fulao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Width = 45.354301420000000000
          Height = 15.118110240000000000
          DataField = 'DefPeca'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."DefPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 26.456710000000000000
        ParentFont = False
        Top = 313.700990000000000000
        Width = 680.315400000000000000
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Top = 7.559060000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '###,###,###.###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."Peso">,Band4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Top = 7.559060000000000000
          Width = 79.370071420000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '###,###,###.###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."Qtde">,Band4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Top = 7.559060000000000000
          Width = 45.354301420000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 464.882155830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total do per'#237'odo: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 151.181200000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPesq."Grupo"'
        object frxDsPesqNO_Grupo: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPesq."NO_Grupo"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '###,###,###.###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."Peso">,Band4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Width = 79.370071420000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '###,###,###.###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."Qtde">,Band4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Width = 45.354301420000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Width = 464.882155830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total do grupo [frxDsPesq."NO_Grupo"]: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPesq: TfrxDBDataset
    UserName = 'frxDsPesq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'DataEmis=DataEmis'
      'Status=Status'
      'Numero=Numero'
      'NOMECI=NOMECI'
      'NOMESETOR=NOMESETOR'
      'Tecnico=Tecnico'
      'NOME=NOME'
      'ClienteI=ClienteI'
      'Tipificacao=Tipificacao'
      'TempoR=TempoR'
      'TempoP=TempoP'
      'Setor=Setor'
      'Tipific=Tipific'
      'Espessura=Espessura'
      'DefPeca=DefPeca'
      'Peso=Peso'
      'Custo=Custo'
      'Qtde=Qtde'
      'AreaM2=AreaM2'
      'Fulao=Fulao'
      'Obs=Obs'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'STATUS_TXT=STATUS_TXT'
      'NO_Grupo=NO_Grupo'
      'Grupo=Grupo')
    DataSet = QrPesq
    BCDToCurrency = False
    DataSetOptions = []
    Left = 132
    Top = 372
  end
end
