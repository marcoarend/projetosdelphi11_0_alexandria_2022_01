object FmPQPedNew: TFmPQPedNew
  Left = 339
  Top = 167
  Caption = 'QUI-PEDID-002 :: Edi'#231#227'o de Pedido de PQ'
  ClientHeight = 347
  ClientWidth = 795
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 795
    Height = 191
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 76
    ExplicitTop = 56
    object Label2: TLabel
      Left = 12
      Top = 4
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label1: TLabel
      Left = 68
      Top = 4
      Width = 49
      Height = 13
      Caption = 'Solicitado:'
    end
    object Label5: TLabel
      Left = 160
      Top = 4
      Width = 40
      Height = 13
      Caption = 'Entrega:'
    end
    object Label4: TLabel
      Left = 252
      Top = 4
      Width = 40
      Height = 13
      Caption = 'Contato:'
    end
    object Label7: TLabel
      Left = 400
      Top = 4
      Width = 33
      Height = 13
      Caption = 'kg l'#237'q.:'
    end
    object Label8: TLabel
      Left = 496
      Top = 4
      Width = 42
      Height = 13
      Caption = 'kg bruto:'
    end
    object Label9: TLabel
      Left = 594
      Top = 4
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object Label10: TLabel
      Left = 690
      Top = 4
      Width = 55
      Height = 13
      Caption = 'kg Entrada:'
    end
    object Label3: TLabel
      Left = 12
      Top = 44
      Width = 57
      Height = 13
      Caption = 'Fornecedor:'
    end
    object Label19: TLabel
      Left = 12
      Top = 84
      Width = 75
      Height = 13
      Caption = 'Transportadora:'
    end
    object Label6: TLabel
      Left = 284
      Top = 84
      Width = 28
      Height = 13
      Caption = 'Setor:'
    end
    object Label11: TLabel
      Left = 284
      Top = 44
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object RGSit: TRadioGroup
      Left = 556
      Top = 45
      Width = 229
      Height = 77
      Caption = ' Situa'#231#227'o do Pedido: '
      Enabled = False
      ItemIndex = 0
      Items.Strings = (
        'Solicitado'
        'Entrada parcial'
        'Encerrado')
      TabOrder = 16
    end
    object EdCodigo: TdmkEdit
      Left = 12
      Top = 20
      Width = 51
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object TPSolicitado: TDateTimePicker
      Left = 68
      Top = 20
      Width = 90
      Height = 21
      Date = 37500.947893460600000000
      Time = 37500.947893460600000000
      Color = clWhite
      TabOrder = 1
    end
    object TPEntrega: TDateTimePicker
      Left = 160
      Top = 20
      Width = 90
      Height = 21
      Date = 37500.948137442100000000
      Time = 37500.948137442100000000
      Color = clWhite
      TabOrder = 2
    end
    object EdContato: TEdit
      Left = 252
      Top = 20
      Width = 146
      Height = 21
      Color = clWhite
      TabOrder = 3
    end
    object EdLiquido: TdmkEdit
      Left = 400
      Top = 20
      Width = 93
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdBruto: TdmkEdit
      Left = 496
      Top = 20
      Width = 93
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdValor: TdmkEdit
      Left = 594
      Top = 20
      Width = 93
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdEntrada: TdmkEdit
      Left = 692
      Top = 20
      Width = 93
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdFornecedor: TdmkEditCB
      Left = 12
      Top = 60
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBFornecedor
      IgnoraDBLookupComboBox = False
    end
    object CBFornecedor: TdmkDBLookupComboBox
      Left = 78
      Top = 60
      Width = 200
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsFornecedor
      TabOrder = 9
      dmkEditCB = EdFornecedor
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdTransportador: TdmkEditCB
      Left = 12
      Top = 100
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 12
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBTransportador
      IgnoraDBLookupComboBox = False
    end
    object CBTransportador: TdmkDBLookupComboBox
      Left = 78
      Top = 100
      Width = 200
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsTransportador
      TabOrder = 13
      dmkEditCB = EdTransportador
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdSetor: TdmkEditCB
      Left = 284
      Top = 100
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBSetor
      IgnoraDBLookupComboBox = False
    end
    object CBSetor: TdmkDBLookupComboBox
      Left = 350
      Top = 100
      Width = 200
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsSetores
      TabOrder = 15
      dmkEditCB = EdSetor
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCliInt: TdmkEditCB
      Left = 284
      Top = 60
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCliInt
      IgnoraDBLookupComboBox = False
    end
    object CBCliInt: TdmkDBLookupComboBox
      Left = 350
      Top = 60
      Width = 200
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMECI'
      ListSource = DsCI
      TabOrder = 11
      dmkEditCB = EdCliInt
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 795
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 747
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 699
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 296
        Height = 32
        Caption = 'Edi'#231#227'o de Pedido de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 296
        Height = 32
        Caption = 'Edi'#231#227'o de Pedido de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 296
        Height = 32
        Caption = 'Edi'#231#227'o de Pedido de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 239
    Width = 795
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 180
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 791
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 283
    Width = 795
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = 152
    ExplicitTop = 287
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 791
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 647
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Desiste das altera'#231#245'es'
          Caption = '&Desiste'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confirma as altera'#231#245'es'
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrFornecedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fo.*, '
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEENTIDADE'
      'FROM entidades fo'
      'WHERE fo.Fornece2='#39'V'#39
      'ORDER BY NOMEENTIDADE')
    Left = 196
    Top = 140
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Size = 50
    end
    object QrFornecedorFantasia: TWideStringField
      FieldName = 'Fantasia'
      Size = 50
    end
    object QrFornecedorRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 128
    end
    object QrFornecedorRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 128
    end
    object QrFornecedorPai: TWideStringField
      FieldName = 'Pai'
      Size = 128
    end
    object QrFornecedorMae: TWideStringField
      FieldName = 'Mae'
      Size = 128
    end
    object QrFornecedorCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrFornecedorIE: TWideStringField
      FieldName = 'IE'
      Size = 15
    end
    object QrFornecedorNome: TWideStringField
      FieldName = 'Nome'
      Size = 128
    end
    object QrFornecedorApelido: TWideStringField
      FieldName = 'Apelido'
      Size = 128
    end
    object QrFornecedorCPF: TWideStringField
      FieldName = 'CPF'
      Size = 128
    end
    object QrFornecedorRG: TWideStringField
      FieldName = 'RG'
      Size = 128
    end
    object QrFornecedorERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrFornecedorENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrFornecedorECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 128
    end
    object QrFornecedorEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 21
    end
    object QrFornecedorECIDADE: TWideStringField
      FieldName = 'ECIDADE'
      Size = 15
    end
    object QrFornecedorEUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrFornecedorECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrFornecedorEPais: TWideStringField
      FieldName = 'EPais'
      Size = 15
    end
    object QrFornecedorETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrFornecedorETe2: TWideStringField
      FieldName = 'ETe2'
      Size = 128
    end
    object QrFornecedorETe3: TWideStringField
      FieldName = 'ETe3'
      Size = 128
    end
    object QrFornecedorECel: TWideStringField
      FieldName = 'ECel'
      Size = 17
    end
    object QrFornecedorEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrFornecedorEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 26
    end
    object QrFornecedorEContato: TWideStringField
      FieldName = 'EContato'
      Size = 19
    end
    object QrFornecedorENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrFornecedorPRua: TWideStringField
      FieldName = 'PRua'
      Size = 128
    end
    object QrFornecedorPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrFornecedorPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 128
    end
    object QrFornecedorPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 128
    end
    object QrFornecedorPCIDADE: TWideStringField
      FieldName = 'PCIDADE'
      Size = 128
    end
    object QrFornecedorPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrFornecedorPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrFornecedorPPais: TWideStringField
      FieldName = 'PPais'
      Size = 128
    end
    object QrFornecedorPTe1: TWideStringField
      FieldName = 'PTe1'
      Size = 128
    end
    object QrFornecedorPTe2: TWideStringField
      FieldName = 'PTe2'
      Size = 128
    end
    object QrFornecedorPTe3: TWideStringField
      FieldName = 'PTe3'
      Size = 128
    end
    object QrFornecedorPCel: TWideStringField
      FieldName = 'PCel'
      Size = 128
    end
    object QrFornecedorPFax: TWideStringField
      FieldName = 'PFax'
      Size = 128
    end
    object QrFornecedorPEMail: TWideStringField
      FieldName = 'PEMail'
      Size = 128
    end
    object QrFornecedorPContato: TWideStringField
      FieldName = 'PContato'
      Size = 128
    end
    object QrFornecedorPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrFornecedorSexo: TWideStringField
      FieldName = 'Sexo'
      FixedChar = True
      Size = 1
    end
    object QrFornecedorResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 128
    end
    object QrFornecedorProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 128
    end
    object QrFornecedorCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 128
    end
    object QrFornecedorRecibo: TSmallintField
      FieldName = 'Recibo'
    end
    object QrFornecedorDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
    end
    object QrFornecedorAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
    end
    object QrFornecedorAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
    end
    object QrFornecedorCliente1: TWideStringField
      FieldName = 'Cliente1'
      FixedChar = True
      Size = 1
    end
    object QrFornecedorCliente2: TWideStringField
      FieldName = 'Cliente2'
      FixedChar = True
      Size = 1
    end
    object QrFornecedorFornece1: TWideStringField
      FieldName = 'Fornece1'
      FixedChar = True
      Size = 1
    end
    object QrFornecedorFornece2: TWideStringField
      FieldName = 'Fornece2'
      FixedChar = True
      Size = 1
    end
    object QrFornecedorFornece3: TWideStringField
      FieldName = 'Fornece3'
      FixedChar = True
      Size = 1
    end
    object QrFornecedorFornece4: TWideStringField
      FieldName = 'Fornece4'
      FixedChar = True
      Size = 1
    end
    object QrFornecedorTerceiro: TWideStringField
      FieldName = 'Terceiro'
      FixedChar = True
      Size = 1
    end
    object QrFornecedorCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrFornecedorInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 128
    end
    object QrFornecedorLogo: TBlobField
      FieldName = 'Logo'
      Size = 1
    end
    object QrFornecedorVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrFornecedorMensal: TWideStringField
      FieldName = 'Mensal'
      FixedChar = True
      Size = 1
    end
    object QrFornecedorObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 1
    end
    object QrFornecedorTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrFornecedorLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFornecedorGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrFornecedorDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFornecedorDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFornecedorUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrFornecedorUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrFornecedorCRua: TWideStringField
      FieldName = 'CRua'
      Size = 128
    end
    object QrFornecedorCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrFornecedorCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 128
    end
    object QrFornecedorCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 128
    end
    object QrFornecedorCCIDADE: TWideStringField
      FieldName = 'CCIDADE'
      Size = 128
    end
    object QrFornecedorCUF: TSmallintField
      FieldName = 'CUF'
    end
    object QrFornecedorCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrFornecedorCPais: TWideStringField
      FieldName = 'CPais'
      Size = 128
    end
    object QrFornecedorCTel: TWideStringField
      FieldName = 'CTel'
      Size = 128
    end
    object QrFornecedorCFax: TWideStringField
      FieldName = 'CFax'
      Size = 128
    end
    object QrFornecedorCCel: TWideStringField
      FieldName = 'CCel'
      Size = 128
    end
    object QrFornecedorCContato: TWideStringField
      FieldName = 'CContato'
      Size = 128
    end
    object QrFornecedorLRua: TWideStringField
      FieldName = 'LRua'
      Size = 128
    end
    object QrFornecedorLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrFornecedorLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 128
    end
    object QrFornecedorLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 128
    end
    object QrFornecedorLCIDADE: TWideStringField
      FieldName = 'LCIDADE'
      Size = 128
    end
    object QrFornecedorLUF: TSmallintField
      FieldName = 'LUF'
    end
    object QrFornecedorLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrFornecedorLPais: TWideStringField
      FieldName = 'LPais'
      Size = 128
    end
    object QrFornecedorLTel: TWideStringField
      FieldName = 'LTel'
      Size = 128
    end
    object QrFornecedorLFax: TWideStringField
      FieldName = 'LFax'
      Size = 128
    end
    object QrFornecedorLCel: TWideStringField
      FieldName = 'LCel'
      Size = 128
    end
    object QrFornecedorLContato: TWideStringField
      FieldName = 'LContato'
      Size = 128
    end
    object QrFornecedorComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrFornecedorSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrFornecedorNivel: TWideStringField
      FieldName = 'Nivel'
      FixedChar = True
      Size = 1
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 50
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 224
    Top = 136
  end
  object QrTransportador: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fo.*, '
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEENTIDADE'
      'FROM entidades fo'
      'WHERE fo.Fornece3='#39'V'#39
      'ORDER BY NOMEENTIDADE')
    Left = 460
    Top = 172
    object QrTransportadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportadorRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Size = 41
    end
    object QrTransportadorFantasia: TWideStringField
      FieldName = 'Fantasia'
      Size = 49
    end
    object QrTransportadorRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 128
    end
    object QrTransportadorRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 128
    end
    object QrTransportadorPai: TWideStringField
      FieldName = 'Pai'
      Size = 128
    end
    object QrTransportadorMae: TWideStringField
      FieldName = 'Mae'
      Size = 128
    end
    object QrTransportadorCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrTransportadorIE: TWideStringField
      FieldName = 'IE'
      Size = 15
    end
    object QrTransportadorNome: TWideStringField
      FieldName = 'Nome'
      Size = 128
    end
    object QrTransportadorApelido: TWideStringField
      FieldName = 'Apelido'
      Size = 128
    end
    object QrTransportadorCPF: TWideStringField
      FieldName = 'CPF'
      Size = 128
    end
    object QrTransportadorRG: TWideStringField
      FieldName = 'RG'
      Size = 128
    end
    object QrTransportadorERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrTransportadorENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrTransportadorECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 128
    end
    object QrTransportadorEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 14
    end
    object QrTransportadorECIDADE: TWideStringField
      FieldName = 'ECIDADE'
      Size = 12
    end
    object QrTransportadorEUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrTransportadorECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrTransportadorEPais: TWideStringField
      FieldName = 'EPais'
      Size = 6
    end
    object QrTransportadorETe1: TWideStringField
      FieldName = 'ETe1'
      Size = 19
    end
    object QrTransportadorETe2: TWideStringField
      FieldName = 'ETe2'
      Size = 128
    end
    object QrTransportadorETe3: TWideStringField
      FieldName = 'ETe3'
      Size = 128
    end
    object QrTransportadorECel: TWideStringField
      FieldName = 'ECel'
      Size = 12
    end
    object QrTransportadorEFax: TWideStringField
      FieldName = 'EFax'
      Size = 19
    end
    object QrTransportadorEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 18
    end
    object QrTransportadorEContato: TWideStringField
      FieldName = 'EContato'
      Size = 19
    end
    object QrTransportadorENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrTransportadorPRua: TWideStringField
      FieldName = 'PRua'
      Size = 128
    end
    object QrTransportadorPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrTransportadorPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 128
    end
    object QrTransportadorPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 128
    end
    object QrTransportadorPCIDADE: TWideStringField
      FieldName = 'PCIDADE'
      Size = 128
    end
    object QrTransportadorPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrTransportadorPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrTransportadorPPais: TWideStringField
      FieldName = 'PPais'
      Size = 128
    end
    object QrTransportadorPTe1: TWideStringField
      FieldName = 'PTe1'
      Size = 128
    end
    object QrTransportadorPTe2: TWideStringField
      FieldName = 'PTe2'
      Size = 128
    end
    object QrTransportadorPTe3: TWideStringField
      FieldName = 'PTe3'
      Size = 128
    end
    object QrTransportadorPCel: TWideStringField
      FieldName = 'PCel'
      Size = 128
    end
    object QrTransportadorPFax: TWideStringField
      FieldName = 'PFax'
      Size = 128
    end
    object QrTransportadorPEMail: TWideStringField
      FieldName = 'PEMail'
      Size = 128
    end
    object QrTransportadorPContato: TWideStringField
      FieldName = 'PContato'
      Size = 128
    end
    object QrTransportadorPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrTransportadorSexo: TWideStringField
      FieldName = 'Sexo'
      FixedChar = True
      Size = 1
    end
    object QrTransportadorResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 128
    end
    object QrTransportadorProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 128
    end
    object QrTransportadorCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 128
    end
    object QrTransportadorRecibo: TSmallintField
      FieldName = 'Recibo'
    end
    object QrTransportadorDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
    end
    object QrTransportadorAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
    end
    object QrTransportadorAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
    end
    object QrTransportadorCliente1: TWideStringField
      FieldName = 'Cliente1'
      FixedChar = True
      Size = 1
    end
    object QrTransportadorCliente2: TWideStringField
      FieldName = 'Cliente2'
      FixedChar = True
      Size = 1
    end
    object QrTransportadorFornece1: TWideStringField
      FieldName = 'Fornece1'
      FixedChar = True
      Size = 1
    end
    object QrTransportadorFornece2: TWideStringField
      FieldName = 'Fornece2'
      FixedChar = True
      Size = 1
    end
    object QrTransportadorFornece3: TWideStringField
      FieldName = 'Fornece3'
      FixedChar = True
      Size = 1
    end
    object QrTransportadorFornece4: TWideStringField
      FieldName = 'Fornece4'
      FixedChar = True
      Size = 1
    end
    object QrTransportadorTerceiro: TWideStringField
      FieldName = 'Terceiro'
      FixedChar = True
      Size = 1
    end
    object QrTransportadorCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrTransportadorInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 128
    end
    object QrTransportadorLogo: TBlobField
      FieldName = 'Logo'
      Size = 1
    end
    object QrTransportadorVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrTransportadorMensal: TWideStringField
      FieldName = 'Mensal'
      FixedChar = True
      Size = 1
    end
    object QrTransportadorObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 1
    end
    object QrTransportadorTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTransportadorLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTransportadorGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrTransportadorDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTransportadorDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTransportadorUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrTransportadorUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrTransportadorCRua: TWideStringField
      FieldName = 'CRua'
      Size = 128
    end
    object QrTransportadorCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrTransportadorCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 128
    end
    object QrTransportadorCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 128
    end
    object QrTransportadorCCIDADE: TWideStringField
      FieldName = 'CCIDADE'
      Size = 128
    end
    object QrTransportadorCUF: TSmallintField
      FieldName = 'CUF'
    end
    object QrTransportadorCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrTransportadorCPais: TWideStringField
      FieldName = 'CPais'
      Size = 128
    end
    object QrTransportadorCTel: TWideStringField
      FieldName = 'CTel'
      Size = 128
    end
    object QrTransportadorCFax: TWideStringField
      FieldName = 'CFax'
      Size = 128
    end
    object QrTransportadorCCel: TWideStringField
      FieldName = 'CCel'
      Size = 128
    end
    object QrTransportadorCContato: TWideStringField
      FieldName = 'CContato'
      Size = 128
    end
    object QrTransportadorLRua: TWideStringField
      FieldName = 'LRua'
      Size = 128
    end
    object QrTransportadorLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrTransportadorLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 128
    end
    object QrTransportadorLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 128
    end
    object QrTransportadorLCIDADE: TWideStringField
      FieldName = 'LCIDADE'
      Size = 128
    end
    object QrTransportadorLUF: TSmallintField
      FieldName = 'LUF'
    end
    object QrTransportadorLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrTransportadorLPais: TWideStringField
      FieldName = 'LPais'
      Size = 128
    end
    object QrTransportadorLTel: TWideStringField
      FieldName = 'LTel'
      Size = 128
    end
    object QrTransportadorLFax: TWideStringField
      FieldName = 'LFax'
      Size = 128
    end
    object QrTransportadorLCel: TWideStringField
      FieldName = 'LCel'
      Size = 128
    end
    object QrTransportadorLContato: TWideStringField
      FieldName = 'LContato'
      Size = 128
    end
    object QrTransportadorComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrTransportadorSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrTransportadorNivel: TWideStringField
      FieldName = 'Nivel'
      FixedChar = True
      Size = 1
    end
    object QrTransportadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 41
    end
  end
  object DsTransportador: TDataSource
    DataSet = QrTransportador
    Left = 488
    Top = 172
  end
  object QrIns: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'INSERT INTO pqped SET'
      'Data=:P0,'
      'Fornece=:P1,'
      'Entrega=:P2,'
      'Transporte=:P3,'
      'Contato=:P4,'
      'Setor=:P5,'
      'Sit=:P6,'
      'CliInt=:P7,'
      'Codigo=:P8')
    Left = 288
    Top = 180
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end>
  end
  object QrUpd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'UPDATE pqped SET'
      'Data=:P0,'
      'Fornece=:P1,'
      'Entrega=:P2,'
      'Transporte=:P3,'
      'Contato=:P4,'
      'Setor=:P5,'
      'Sit=:P6,'
      'CliInt=:P7'
      'WHERE Codigo=:P8')
    Left = 316
    Top = 180
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end>
  end
  object QrSetores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM listasetores'
      'ORDER BY Nome')
    Left = 204
    Top = 180
    object QrSetoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.listasetores.Codigo'
    end
    object QrSetoresNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.listasetores.Nome'
      Size = 12
    end
  end
  object DsSetores: TDataSource
    DataSet = QrSetores
    Left = 232
    Top = 180
  end
  object QrCI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 352
    Top = 180
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 380
    Top = 180
  end
end
