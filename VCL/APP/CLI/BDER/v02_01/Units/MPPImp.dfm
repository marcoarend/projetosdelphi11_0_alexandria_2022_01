object FmMPPImp: TFmMPPImp
  Left = 347
  Top = 195
  Caption = 'VEN-COURO-102 :: Relat'#243'rios de Pedidos'
  ClientHeight = 662
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 500
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 145
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label10: TLabel
        Left = 16
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label1: TLabel
        Left = 396
        Top = 4
        Width = 30
        Height = 13
        Caption = 'Artigo:'
      end
      object Label2: TLabel
        Left = 16
        Top = 44
        Width = 49
        Height = 13
        Caption = 'Vendedor:'
      end
      object Label4: TLabel
        Left = 396
        Top = 44
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object EdCliente: TdmkEditCB
        Left = 16
        Top = 20
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 72
        Top = 20
        Width = 321
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientes
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBMP: TdmkDBLookupComboBox
        Left = 452
        Top = 20
        Width = 321
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsMP
        TabOrder = 3
        dmkEditCB = EdMP
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdMP: TdmkEditCB
        Left = 396
        Top = 20
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdMPChange
        DBLookupComboBox = CBMP
        IgnoraDBLookupComboBox = False
      end
      object EdVendedor: TdmkEditCB
        Left = 16
        Top = 60
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdVendedorChange
        DBLookupComboBox = CBVendedor
        IgnoraDBLookupComboBox = False
      end
      object CBVendedor: TdmkDBLookupComboBox
        Left = 72
        Top = 60
        Width = 321
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsVendedores
        TabOrder = 5
        dmkEditCB = EdVendedor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTexto: TdmkEdit
        Left = 396
        Top = 60
        Width = 373
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdTextoChange
      end
      object RgFiltro: TRadioGroup
        Left = 440
        Top = 84
        Width = 329
        Height = 57
        Caption = ' Filtro da descri'#231#227'o: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Sem filtro'
          '%texto%'
          '%texto'
          'texto%')
        TabOrder = 9
        OnClick = RgFiltroClick
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 84
        Width = 209
        Height = 57
        Caption = ' Emiss'#227'o: '
        TabOrder = 7
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 205
          Height = 40
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object CkIniPed: TCheckBox
            Left = 8
            Top = -1
            Width = 89
            Height = 17
            Caption = 'Data inicial:'
            TabOrder = 0
          end
          object TPIniPed: TDateTimePicker
            Left = 8
            Top = 16
            Width = 93
            Height = 21
            Date = 38604.601629953700000000
            Time = 38604.601629953700000000
            TabOrder = 1
            OnChange = TPIniPedChange
          end
          object CkFimPed: TCheckBox
            Left = 104
            Top = -1
            Width = 81
            Height = 17
            Caption = 'Data final.'
            TabOrder = 2
          end
          object TPFimPed: TDateTimePicker
            Left = 104
            Top = 16
            Width = 93
            Height = 21
            Date = 38604.601629953700000000
            Time = 38604.601629953700000000
            TabOrder = 3
            OnChange = TPFimPedChange
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 228
        Top = 84
        Width = 209
        Height = 57
        Caption = ' Entrega: '
        TabOrder = 8
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 205
          Height = 40
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object CkIniEnt: TCheckBox
            Left = 8
            Top = -1
            Width = 89
            Height = 17
            Caption = 'Data inicial:'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
          object TPIniEnt: TDateTimePicker
            Left = 8
            Top = 16
            Width = 93
            Height = 21
            Date = 38604.601629953700000000
            Time = 38604.601629953700000000
            TabOrder = 1
            OnChange = TPIniPedChange
          end
          object CkFimEnt: TCheckBox
            Left = 104
            Top = -1
            Width = 81
            Height = 17
            Caption = 'Data final.'
            Checked = True
            State = cbChecked
            TabOrder = 2
          end
          object TPFimEnt: TDateTimePicker
            Left = 104
            Top = 16
            Width = 93
            Height = 21
            Date = 38604.601629953700000000
            Time = 38604.601629953700000000
            TabOrder = 3
            OnChange = TPFimPedChange
          end
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 145
      Width = 784
      Height = 355
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object DBGrid1: TDBGrid
        Left = 0
        Top = 128
        Width = 784
        Height = 227
        Align = alClient
        DataSource = DsMPVIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DataF'
            Title.Alignment = taCenter
            Title.Caption = 'Emiss'#227'o'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Entrega'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEMP'
            Title.Caption = 'Artigo'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Texto'
            Title.Caption = 'Descri'#231#227'o'
            Width = 251
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CorTxt'
            Title.Caption = 'Cor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EspesTxt'
            Title.Caption = 'Espessura'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Classe'
            Width = 91
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Alignment = taRightJustify
            Title.Caption = 'Quantidade'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Title.Alignment = taRightJustify
            Title.Caption = 'Pre'#231'o'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Title.Alignment = taRightJustify
            Width = 68
            Visible = True
          end>
      end
      object PnImprime: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 128
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object RGAgrupa: TRadioGroup
          Left = 690
          Top = 0
          Width = 97
          Height = 128
          Align = alLeft
          Caption = ' Agrupamentos: '
          ItemIndex = 0
          Items.Strings = (
            '0'
            '1'
            '2'
            '3')
          TabOrder = 0
        end
        object RGOrdem4: TRadioGroup
          Left = 414
          Top = 0
          Width = 138
          Height = 128
          Align = alLeft
          Caption = ' Ordem 4: '
          ItemIndex = 3
          Items.Strings = (
            'Cliente'
            'Artigo'
            'Vendedor'
            'Descri'#231#227'o'
            'Emiss'#227'o'
            'Entrega')
          TabOrder = 1
          OnClick = RGOrdem4Click
        end
        object RGOrdem3: TRadioGroup
          Left = 276
          Top = 0
          Width = 138
          Height = 128
          Align = alLeft
          Caption = ' Ordem 3: '
          ItemIndex = 2
          Items.Strings = (
            'Cliente'
            'Artigo'
            'Vendedor'
            'Descri'#231#227'o'
            'Emiss'#227'o'
            'Entrega')
          TabOrder = 2
          OnClick = RGOrdem3Click
        end
        object RGOrdem2: TRadioGroup
          Left = 138
          Top = 0
          Width = 138
          Height = 128
          Align = alLeft
          Caption = ' Ordem 2: '
          ItemIndex = 5
          Items.Strings = (
            'Cliente'
            'Artigo'
            'Vendedor'
            'Descri'#231#227'o'
            'Emiss'#227'o'
            'Entrega')
          TabOrder = 3
          OnClick = RGOrdem2Click
        end
        object RGOrdem1: TRadioGroup
          Left = 0
          Top = 0
          Width = 138
          Height = 128
          Align = alLeft
          Caption = ' Ordem 1: '
          ItemIndex = 0
          Items.Strings = (
            'Cliente'
            'Artigo'
            'Vendedor'
            'Descri'#231#227'o'
            'Emiss'#227'o'
            'Entrega')
          TabOrder = 4
          OnClick = RGOrdem1Click
        end
        object RGOrdem5: TRadioGroup
          Left = 552
          Top = 0
          Width = 138
          Height = 128
          Align = alLeft
          Caption = ' Ordem 5: '
          ItemIndex = 4
          Items.Strings = (
            'Cliente'
            'Artigo'
            'Vendedor'
            'Descri'#231#227'o'
            'Emiss'#227'o'
            'Entrega')
          TabOrder = 5
          OnClick = RGOrdem5Click
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 265
        Height = 32
        Caption = 'Relat'#243'rios de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 265
        Height = 32
        Caption = 'Relat'#243'rios de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 265
        Height = 32
        Caption = 'Relat'#243'rios de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 548
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 592
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 235
        Left = 12
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Gera'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 108
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtEntrega: TBitBtn
        Tag = 407
        Left = 204
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Entrega'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtEntregaClick
      end
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 289
    Top = 69
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 317
    Top = 69
  end
  object QrMPVIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMPVItsAfterOpen
    BeforeClose = QrMPVItsBeforeClose
    SQL.Strings = (
      'SELECT mpp.Vendedor, mpp.Cliente, art.Nome NOMEMP, mpp.DataF,'
      'mpp.Valor VALORMPP, mvi.*,'
      
        'CASE WHEN ven.Tipo=0 THEN ven.RazaoSocial ELSE ven.Nome END NOME' +
        'VENDEDOR,'
      
        'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOME' +
        'CLIENTE'
      'FROM mpvits mvi'
      'LEFT JOIN mpp       mpp ON mpp.Codigo=mvi.Pedido'
      'LEFT JOIN entidades ven ON ven.Codigo=mpp.Vendedor'
      'LEFT JOIN entidades cli ON cli.Codigo=mpp.Cliente'
      'LEFT JOIN artigosgrupos art ON art.Codigo=mvi.MP'
      'WHERE mpp.DataF  BETWEEN "2000-03-02" AND "2008-04-01"'
      ''
      'ORDER BY NOMECLIENTE, mvi.MP, NOMEVENDEDOR, mvi.Texto')
    Left = 8
    Top = 409
    object QrMPVItsVendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'mpp.Vendedor'
    end
    object QrMPVItsCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'mpp.Cliente'
    end
    object QrMPVItsNOMEMP: TWideStringField
      FieldName = 'NOMEMP'
      Origin = 'artigosgrupos.Nome'
      Size = 50
    end
    object QrMPVItsDataF: TDateField
      FieldName = 'DataF'
      Origin = 'mpp.DataF'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPVItsVALORMPP: TFloatField
      FieldName = 'VALORMPP'
      Origin = 'mpp.Valor'
    end
    object QrMPVItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpvits.Codigo'
      Required = True
    end
    object QrMPVItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'mpvits.Controle'
      Required = True
    end
    object QrMPVItsMP: TIntegerField
      FieldName = 'MP'
      Origin = 'mpvits.MP'
      Required = True
    end
    object QrMPVItsQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'mpvits.Qtde'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVItsPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'mpvits.Preco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVItsValor: TFloatField
      FieldName = 'Valor'
      Origin = 'mpvits.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVItsTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'mpvits.Texto'
      Required = True
      Size = 50
    end
    object QrMPVItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'mpvits.Lk'
    end
    object QrMPVItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'mpvits.DataCad'
    end
    object QrMPVItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'mpvits.DataAlt'
    end
    object QrMPVItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'mpvits.UserCad'
    end
    object QrMPVItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'mpvits.UserAlt'
    end
    object QrMPVItsDesco: TFloatField
      FieldName = 'Desco'
      Origin = 'mpvits.Desco'
      Required = True
    end
    object QrMPVItsEntrega: TDateField
      FieldName = 'Entrega'
      Origin = 'mpvits.Entrega'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPVItsPronto: TDateField
      FieldName = 'Pronto'
      Origin = 'mpvits.Pronto'
      Required = True
    end
    object QrMPVItsStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'mpvits.Status'
      Required = True
    end
    object QrMPVItsFluxo: TIntegerField
      FieldName = 'Fluxo'
      Origin = 'mpvits.Fluxo'
      Required = True
    end
    object QrMPVItsClasse: TWideStringField
      FieldName = 'Classe'
      Origin = 'mpvits.Classe'
      Size = 15
    end
    object QrMPVItsPedido: TIntegerField
      FieldName = 'Pedido'
      Origin = 'mpvits.Pedido'
      Required = True
    end
    object QrMPVItsEspesTxt: TWideStringField
      FieldName = 'EspesTxt'
      Origin = 'mpvits.EspesTxt'
      Size = 10
    end
    object QrMPVItsCorTxt: TWideStringField
      FieldName = 'CorTxt'
      Origin = 'mpvits.CorTxt'
      Size = 30
    end
    object QrMPVItsM2Pedido: TFloatField
      FieldName = 'M2Pedido'
      Origin = 'mpvits.M2Pedido'
      Required = True
    end
    object QrMPVItsPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'mpvits.Pecas'
      Required = True
    end
    object QrMPVItsUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'mpvits.Unidade'
      Required = True
    end
    object QrMPVItsObserv: TWideMemoField
      FieldName = 'Observ'
      Origin = 'mpvits.Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPVItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'mpvits.AlterWeb'
      Required = True
    end
    object QrMPVItsPrecoPed: TFloatField
      FieldName = 'PrecoPed'
      Origin = 'mpvits.PrecoPed'
      Required = True
    end
    object QrMPVItsValorPed: TFloatField
      FieldName = 'ValorPed'
      Origin = 'mpvits.ValorPed'
      Required = True
    end
    object QrMPVItsNOMEVENDEDOR: TWideStringField
      FieldName = 'NOMEVENDEDOR'
      Origin = 'NOMEVENDEDOR'
      Size = 100
    end
    object QrMPVItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Origin = 'NOMECLIENTE'
      Size = 100
    end
  end
  object DsMPVIts: TDataSource
    DataSet = QrMPVIts
    Left = 36
    Top = 409
  end
  object QrMP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM artigosgrupos'
      'ORDER BY Nome')
    Left = 689
    Top = 65
    object QrMPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMP: TDataSource
    DataSet = QrMP
    Left = 717
    Top = 65
  end
  object QrVendedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece6="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 289
    Top = 105
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 317
    Top = 105
  end
  object PMEntrega: TPopupMenu
    Left = 324
    Top = 456
    object Alteraentrega1: TMenuItem
      Caption = '&Altera entrega'
      OnClick = Alteraentrega1Click
    end
    object Desfazentrega1: TMenuItem
      Caption = '&Desfaz entrega'
      OnClick = Desfazentrega1Click
    end
  end
  object frxMPVIts: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38008.523706979200000000
    ReportOptions.LastChange = 39540.579984618110000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  CabealhodeGrupo1.Visible := <VARF_VISI1>;'
      '  CabealhodeGrupo2.Visible := <VARF_VISI2>;'
      '  CabealhodeGrupo3.Visible := <VARF_VISI3>;'
      '  //'
      '  CabealhodeGrupo1.Condition := <VARF_COND1>;'
      '  CabealhodeGrupo2.Condition := <VARF_COND2>;'
      '  CabealhodeGrupo3.Condition := <VARF_COND3>;'
      '  //'
      '  GroupFooter3.Visible := <VARF_VISI3>;'
      '  GroupFooter2.Visible := <VARF_VISI2>;'
      '  GroupFooter1.Visible := <VARF_VISI1>;'
      '  //'
      '  MemoCG1.Memo.Text := <VARF_MEMO1>;'
      '  MemoCG2.Memo.Text := <VARF_MEMO2>;'
      '  MemoCG3.Memo.Text := <VARF_MEMO3>;'
      ''
      ''
      'end.')
    OnGetValue = frxMPVItsGetValue
    Left = 65
    Top = 409
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsMPVIts
        DataSetName = 'frxDsMPVIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -7
      Font.Name = 'Arial'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object DadosMestre1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMPVIts
        DataSetName = 'frxDsMPVIts'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 521.574952050000000000
          Width = 56.692913385826770000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataField = 'Qtde'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Qtde"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 578.267716540000000000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Preco"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 627.401574803149600000
          Width = 52.913385826771650000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Valor"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 196.535447720000000000
          Width = 325.039323700000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NOMEMP"] - [frxDsMPVIts."Texto"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."DataF"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 83.149660000000000000
          Width = 56.692913390000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataField = 'Pedido'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Pedido"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 139.842610000000000000
          Width = 56.692913390000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataField = 'Controle'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Controle"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 41.574830000000000000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataField = 'Entrega'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Entrega"]')
          ParentFont = False
        end
      end
      object CabealhoDePgina1: TfrxPageHeader
        Height = 15.000000000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        object Memo13: TfrxMemoView
          Left = 196.535447720000000000
          Width = 325.039323700000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 521.574952050000000000
          Width = 56.692913385826770000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 578.267716535433100000
          Width = 49.133858267716540000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 627.401574803149600000
          Width = 52.913385826771650000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 83.149660000000000000
          Width = 56.692913390000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 139.842610000000000000
          Width = 56.692913390000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'OS')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 41.574830000000000000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Entrega')
          ParentFont = False
        end
      end
      object CabealhoDeGrupo1: TfrxGroupHeader
        Height = 20.000000000000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'CabealhoDeGrupo1OnBeforePrint'
        Condition = 'frxDsMPVIts."NOMEMP"'
        object MemoCG1: TfrxMemoView
          Left = 2.000000000000000000
          Width = 680.314960629921300000
          Height = 17.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object CabealhoDeGrupo2: TfrxGroupHeader
        Height = 20.000000000000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'CabealhoDeGrupo2OnBeforePrint'
        object MemoCG2: TfrxMemoView
          Left = 2.000000000000000000
          Width = 680.314960629921300000
          Height = 17.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object CabealhoDeGrupo3: TfrxGroupHeader
        Height = 20.000000000000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'CabealhoDeGrupo3OnBeforePrint'
        object MemoCG3: TfrxMemoView
          Left = 2.000000000000000000
          Width = 680.314960629921300000
          Height = 17.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object TfrxReportSummary
        Height = 36.000000000000000000
        Top = 570.709030000000000000
        Width = 680.315400000000000000
        object Memo6: TfrxMemoView
          Left = 521.575140000000000000
          Top = 3.779530000000000000
          Width = 56.692913385826770000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Qtde">,DadosMestre1)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 627.401574803149600000
          Top = 3.779530000000000000
          Width = 52.913385826771650000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Valor">,DadosMestre1)]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 578.267716540000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsMPVIts."Qtde">,DadosMestre1) > 0,SUM(<frxDsMPVIts' +
              '."Valor">,DadosMestre1) / SUM(<frxDsMPVIts."Qtde">,DadosMestre1)' +
              ',0)]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 487.559370000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupFooter1OnBeforePrint'
        object Memo29: TfrxMemoView
          Left = 521.575140000000000000
          Top = 3.779530000000000000
          Width = 56.692913385826770000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Qtde">,DadosMestre1)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 627.401574803149600000
          Top = 3.779530000000000000
          Width = 52.913385826771650000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Valor">,DadosMestre1)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 578.267716540000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsMPVIts."Qtde">,DadosMestre1) > 0,SUM(<frxDsMPVIts' +
              '."Valor">,DadosMestre1) / SUM(<frxDsMPVIts."Qtde">,DadosMestre1)' +
              ',0)]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 442.205010000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupFooter2OnBeforePrint'
        object Memo15: TfrxMemoView
          Left = 521.575140000000000000
          Width = 56.692913385826770000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Qtde">,DadosMestre1)]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 627.401574803149600000
          Width = 52.913385826771650000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Valor">,DadosMestre1)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 578.267716540000000000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsMPVIts."Qtde">,DadosMestre1) > 0,SUM(<frxDsMPVIts' +
              '."Valor">,DadosMestre1) / SUM(<frxDsMPVIts."Qtde">,DadosMestre1)' +
              ',0)]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 396.850650000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupFooter3OnBeforePrint'
        object Memo22: TfrxMemoView
          Left = 521.575140000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Qtde">,DadosMestre1)]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 578.267716540000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsMPVIts."Qtde">,DadosMestre1) > 0,SUM(<frxDsMPVIts' +
              '."Valor">,DadosMestre1) / SUM(<frxDsMPVIts."Qtde">,DadosMestre1)' +
              ',0)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 627.401574800000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPVIts."Valor">,DadosMestre1)]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 45.354360000000000000
        Top = 631.181510000000000000
        Width = 680.315400000000000000
      end
      object ReportTitle1: TfrxReportTitle
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 117.165430000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE VENDAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 340.157480310000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente: [VARF_CLIENTE]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 340.157700000000000000
          Top = 41.574830000000000000
          Width = 340.157480310000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Vendedor: [VARF_VENDEDOR]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Top = 60.472480000000000000
          Width = 340.157480310000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Artigo:  [VARF_MP]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 340.157480310000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Descri'#231#227'o: [VARF_DESCRICAO]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsMPVIts: TfrxDBDataset
    UserName = 'frxDsMPVIts'
    CloseDataSource = False
    DataSet = QrMPVIts
    BCDToCurrency = False
    Left = 93
    Top = 409
  end
end
