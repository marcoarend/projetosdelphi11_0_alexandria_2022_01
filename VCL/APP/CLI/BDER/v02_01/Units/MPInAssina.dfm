object FmMPInAssina: TFmMPInAssina
  Left = 463
  Top = 225
  Caption = 'COU-MP_IN-005 :: Quem Assina?'
  ClientHeight = 232
  ClientWidth = 654
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 654
    Height = 76
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label5: TLabel
      Left = 32
      Top = 36
      Width = 168
      Height = 13
      Caption = 'Respons'#225'vel compra mat'#233'ria-prima:'
    end
    object SpeedButton1: TSpeedButton
      Left = 600
      Top = 52
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object StaticText1: TStaticText
      Left = 0
      Top = 0
      Width = 654
      Height = 26
      Align = alTop
      Alignment = taCenter
      Caption = 'Todos itens pesquisados ser'#227'o alterados!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object EdAssina: TdmkEditCB
      Left = 32
      Top = 52
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBAssina
      IgnoraDBLookupComboBox = False
    end
    object CBAssina: TdmkDBLookupComboBox
      Left = 88
      Top = 52
      Width = 509
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsAssinam
      TabOrder = 2
      dmkEditCB = EdAssina
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 654
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 606
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 558
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 177
        Height = 32
        Caption = 'Quem Assina?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 177
        Height = 32
        Caption = 'Quem Assina?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 177
        Height = 32
        Caption = 'Quem Assina?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 124
    Width = 654
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 650
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 168
    Width = 654
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 650
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 506
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 5
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&S'#243' imprime'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          Spacing = 3
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 11
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera e imprime'
        NumGlyphs = 2
        Spacing = 3
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrAssinam: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTI'
      'FROM entidades '
      'WHERE Fornece5="V"'
      'ORDER BY NOMEENTI')
    Left = 16
    Top = 12
    object QrAssinamCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAssinamNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Required = True
      Size = 100
    end
  end
  object DsAssinam: TDataSource
    DataSet = QrAssinam
    Left = 44
    Top = 12
  end
end
