unit PQx;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Math, Db, DbCtrls, 
  UnInternalConsts2, Mask, ComCtrls,(* DbTables,*) (*DBIProcs,*) Registry,
  mySQLDbTables, dmkGeral, UnDmkEnums, UnDmkProcFunc, UnProjGroup_Vars,
  UnComps_Vars;

type
{
  epPesoValor = (epPeso, epValor, epNenhum);
  epHistTipo  = (epSintetico, epAnalitico);
  aeAviso     = (aeMsg, aeMemo, aeNenhum);
  MyArrayD2   = array[1..2] of Real;
  MyArrayD22  = array[1..22] of Real;
}

  TArrEstqPQs = array of array [0..1] of Double;
  TUnPQx   = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  AtualizaEstoquePQ(Cliente, Insumo, Empresa: Integer; TipoAviso:
              TTipoAviso; NomeCompo: String; AtzSMI_PQX: Boolean = True): Boolean;
    procedure DefineVAR_Data_Insum_Movim();
    function  ExcluiPQemPesagemComSeguranca(Pesagem: Integer; Conta: Double;
              Confirma, AvisaSucesso: Boolean): Boolean;
    procedure GerenciaBaixaETE(Codigo: Integer);
    procedure GerenciaBaixaPQ(Tipo, Codigo: Integer);
    procedure GerenciaBaixaDevolucao(Codigo: Integer);
    procedure GerenciaBaixaOutros(Codigo: Integer);
    procedure GerenciaPesagem(Pesagem: Integer);
    function  ImpedePeloBalanco(Data: TDateTime; VerificaHoje: Boolean = False;
              Pergunta: String = ''): Boolean;
    procedure MostraFormEmitCab(SQLType: TSQLType; Pesagem: Integer);
    procedure MostraFormEmitUsoSbt(SQLType: TSQLType; Pesagem, EmitGru: Integer);
    procedure MostraFormPQMCab(Codigo: Integer);
    function  ObtemGGXdeInsumo(Insumo: Integer): Integer;
    function  ObtemGGXdeInsumo_Fast(Insumo: Integer): Integer;
    procedure SetaVariaveisIniciais();
    function  SQLAtualizaEstoquePQ(PesoEstq, ValorEstq: Double; Cliente,
              Insumo, Empresa: Integer; TipoAviso: TTipoAviso; NomeCompo:
              String): Boolean;
    function  VerificaBalanco(): Integer;
    function  VerificaBalancoPelaData(Data: TDateTime): Integer;
    function  VerificaEstoquePQ(const Cliente, Insumo: Integer;
              const Data: TDateTime; var Peso, Valor: Double): Boolean;
    function  VerificaEstoquePQs(const Cliente: Integer;
              const Data: TDateTime; var ArrEstqPQs: TArrEstqPQs): Boolean;
    function  VerificaEstoquePeriodo(Insumo, Periodo: Integer; Resultado:
              epPesoValor): MyArrayD2;
    procedure AtualizaDadosPQ(CliInt, Insumo, MoedaPadrao: Integer;
              CustoPadrao, Cotacao: Double);
    function  ObtemInsumoDeGraGruX(GraGruX: Integer): Integer;
    procedure MostraFormPQI(Codigo: Integer);
    procedure MostraFormPQN(Codigo: Integer);
    procedure MostraFormPQBanhosImp();

    function  PQ_IncluiClienteInterno(const SQLType: TSQLType; const PQ,
              Empresa, CI, MoedaPadrao, CustoPadraoAtz, MoedaMan, EstSegur,
              EdtCtrl: Integer; const MinEstq, CustoPadrao, CustoMan, KgLiqEmb:
              Double; const NO_CI, CodProprio: String; var Controle: Integer):
              Boolean;
    function  ObtemDataBal(): TDateTime;

  end;

var
  UnPQx: TUnPQx;

implementation

uses UnMyObjects, Module, UCreate, BlueDermConsts, DmkDAC_PF, UMySQLModule,
  ModuleGeral, MyDBCheck, PQxExcl, PQO, PQD, PQT, PQMCab, EmitCab, EmitUsoSbt,
  PQI, PQN, ModProd, PQBanhosImp;

procedure TUnPQx.AtualizaDadosPQ(CliInt, Insumo, MoedaPadrao: Integer;
  CustoPadrao, Cotacao: Double);
var
  Custo: Double;
begin
  if MoedaPadrao <> 0 then
  begin
    if Cotacao <> 0 then
      Custo := CustoPadrao / Cotacao
    else
      Custo := 0;
  end else
    Custo := CustoPadrao;
  //
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE pqcli SET CustoPadrao=:P0, MoedaPadrao=:P1 ');
  Dmod.QrUpd.SQL.Add('WHERE PQ=:P2 AND CI=:P3');
  Dmod.QrUpd.Params[0].AsFloat   := Custo;
  Dmod.QrUpd.Params[1].AsInteger := MoedaPadrao;
  Dmod.QrUpd.Params[2].AsInteger := Insumo;
  Dmod.QrUpd.Params[3].AsInteger := CliInt;
  Dmod.QrUpd.ExecSQL;
end;

function TUnPQx.SQLAtualizaEstoquePQ(PesoEstq, ValorEstq: Double; Cliente,
  Insumo, Empresa: Integer; TipoAviso: TTipoAviso; NomeCompo: String): Boolean;
var
  Custo, CA, CB, CUSTOMEDIO: Double;
  Erro: Boolean;
  Aviso: String;
  i: Integer;
begin
  //N�o calcula desperd�cio
  CB := 0;
(*
  //////////////////////////////////////////////////////////////////////////////
  C�digo parcial desabilitado em 2023-10-17 por causa do estoque de itens
  com menos de 10 gramas no novo invent�rio para controlar melhor o estoque
  de insumos por NF (e lote)
  //////////////////////////////////////////////////////////////////////////////
  if (PesoEstq < 0.01)  and (PesoEstq > -0.01)  then PesoEstq  := 0;
  if PesoEstq = 0 then
  begin
    if (ValorEstq < 0.10) and (ValorEstq > -0.10) then ValorEstq := 0;
  end;
*)
  Erro := False;
  if PesoEstq <> 0 then
    CUSTOMEDIO := ValorEstq / PesoEstq else CUSTOMEDIO := 0;
  CA := CUSTOMEDIO;
  if CA > 0 then Custo := CA else Custo := CB;
  Aviso := '';
  if (Insumo > 0) and (Dmod.QrControlePQNegBaixa.Value = 0) then
  begin
    if Cliente < 1 then
    begin
      if Custo < -0.00009 then
      begin
        Aviso := 'ERRO!!!! O custo do insumo n� '+IntToStr(Insumo)+' ficou negativo.'+
        ' Pre�o m�dio do estoque = '+Geral.TFT(FloatToStr(Insumo), 4, siNegativo);
        Erro := True;
      end
      //else if (Custo = 0) and (PesoEstq>0.0009) then
      else if (Custo = 0) and (PesoEstq>0.0009) and (Cliente < 0) then
      begin
        Aviso := 'ERRO!!! O custo do insumo n� '+IntToStr(Insumo)+' ficou nulo.'+
        ' Pre�o m�dio do estoque = '+Geral.TFT(FloatToStr(Custo), 4, siNegativo)+
        ' Peso  do estoque = '+Geral.TFT(FloatToStr(PesoEstq), 3, siNegativo);
        Erro := True;
      //end else if (Custo = 0) and (ValorEstq>0.00009) then
      end else if (Custo = 0) and (ValorEstq>0.00009) and (Cliente < 0) then
      begin
        Aviso := 'ERRO!!! O custo do insumo n� '+IntToStr(Insumo)+' ficou nulo.'+
        ' Pre�o m�dio do estoque = '+Geral.TFT(FloatToStr(Custo), 4, siNegativo)+
        ' Valor  do estoque = '+Geral.TFT(FloatToStr(ValorEstq), 4, siNegativo);
        Erro := True;
      end else if (ValorEstq < -0.00009) and (PesoEstq < -0.0009) then
      begin
        Aviso := 'ERRO!!! O peso e o custo do insumo n� '+IntToStr(Insumo)+' ficaram negativo.'+
        ' Valor do estoque = '+Geral.TFT(FloatToStr(ValorEstq), 4, siNegativo)+
        ' Peso  do estoque = '+Geral.TFT(FloatToStr(PesoEstq), 3, siNegativo);
        Erro := True;
      end else if ValorEstq < -0.00009 then
      begin
        Aviso := 'ERRO!!! O valor do insumo n� '+IntToStr(Insumo)+' ficou negativo.'+
        ' Valor do estoque = '+Geral.TFT(FloatToStr(ValorEstq), 4, siNegativo)+
        ' Peso  do estoque = '+Geral.TFT(FloatToStr(PesoEstq), 3, siNegativo);
        Erro := True;
      end else if PesoEstq < -0.0009 then
      begin
        Aviso := 'ERRO!!! O peso do insumo n� '+IntToStr(Insumo)+' ficou negativo.'+
        ' Valor do estoque = '+Geral.TFT(FloatToStr(ValorEstq), 4, siNegativo)+
        ' Peso  do estoque = '+Geral.TFT(FloatToStr(PesoEstq), 3, siNegativo);
        Erro := True;
      end;
      if Aviso <> '' then
      begin
        case TipoAviso of
          aeMemo:
          begin
            for i := 0 to Screen.ActiveForm.ComponentCount -1 do
            begin
              if (Screen.ActiveForm.Components[i] is TMemo) then
              if TMemo(Screen.ActiveForm.Components[i]).Name = NomeCompo then
              begin
                TMemo(Screen.ActiveForm.Components[i]).Lines.Add(Aviso);
                TMemo(Screen.ActiveForm.Components[i]).Refresh;
                Screen.ActiveForm.Update;
                Break;
              end;
            end;
          end;
          aeMsg: Geral.MB_Aviso(Aviso);
          aeVAR: VAR_PQS_ESTQ_NEG := VAR_PQS_ESTQ_NEG + sLineBreak + Aviso;
        end;
      end;
    end;
(*
    Dmod.QrUpdW.Close;
    Dmod.QrUpdW.SQL.Clear;
    Dmod.QrUpdW.SQL.Add('UPDATE pqcli SET Peso=:P0, Valor=:P1, Custo=:P2');
    Dmod.QrUpdW.SQL.Add('WHERE PQ=:P3 AND CI=:P4');
    Dmod.QrUpdW.Params[0].AsFloat := PesoEstq;
    Dmod.QrUpdW.Params[1].AsFloat := ValorEstq;
    Dmod.QrUpdW.Params[2].AsFloat := Custo;
    Dmod.QrUpdW.Params[3].AsInteger := Insumo;
    Dmod.QrUpdW.Params[4].AsInteger := Cliente;
    Dmod.QrUpdW.ExecSQL;
*)
    //if    Jurubeba
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pqcli', False, [
    'Peso', 'Valor', 'Custo'], [
    'PQ', 'CI', 'Empresa'], [
    PesoEstq, ValorEstq, Custo], [
    Insumo, Cliente, Empresa], False);
  end;
  Result := Erro;
end;

function TUnPQx.VerificaEstoquePeriodo(Insumo, Periodo: Integer; Resultado:
epPesoValor): MyArrayD2;
{var
  Peso, Valor: Double;
  DataI, DataF: String;}
begin
  (*if Periodo = 0 then
  begin
    DataI  := FormatDateTime(VAR_FORMATDATE, 2);
    Dmod.QrMin.Close;
    UnDmkDAC_PF.AbreQuery(Dmod.QrMin, Dmod.MyDB);
    if Dmod.QrMinPeriodo.Value = 0 then
      DataF := FormatDateTime(VAR_FORMATDATE, Date)
    else DataF := dmkPF.UltimoDiaDoPeriodoAnterior(
      Dmod.QrMinPeriodo.Value, dtSystem);
    //
    Dmod.QrPQ1.Close;
    Dmod.QrPQ1.Params[0].AsInteger := Insumo;
    UnDmkDAC_PF.AbreQuery(Dmod.QrPQ1, Dmod.MyDB);
    Peso  := Dmod.QrPQ1SAkg.Value;
    Valor := Dmod.QrPQ1SAVlr.Value;
  end else begin
    DataI := dmkPF.PrimeiroDiaDoPeriodo(Periodo, dtSystem3);
    DataF := dmkPF.UltimoDiaDoPeriodo(Periodo, dtSystem3);
    //
    Dmod.QrBalancosIts.Close;
    Dmod.QrBalancosIts.Params[0].AsInteger  := Insumo;
    Dmod.QrBalancosIts.Params[1].AsInteger := Periodo;
    UnDmkDAC_PF.AbreQuery(Dmod.QrBalancosIts, Dmod.MyDB);
    Peso  := Dmod.QrBalancosItsEstqQ.Value;
    Valor := Dmod.QrBalancosItsEstqV.Value;
  end;
  //
  QvEntraPQ3.Close;
  QvEntraPQ3.Params[0].AsInteger := Insumo;
  QvEntraPQ3.Params[1].AsString := DataI;
  QvEntraPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvEntraPQ3, Dmod.MyDB);
  //
  QvVendaPQ3.Close;
  QvVendaPQ3.Params[0].AsInteger := Insumo;
  QvVendaPQ3.Params[1].AsString := DataI;
  QvVendaPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvVendaPQ3, Dmod.MyDB);
  //
  QvDevolPQ3.Close;
  QvDevolPQ3.Params[0].AsInteger := Insumo;
  QvDevolPQ3.Params[1].AsString := DataI;
  QvDevolPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvDevolPQ3, Dmod.MyDB);
  //
  QvRecomPQ3.Close;
  QvRecomPQ3.Params[0].AsInteger := Insumo;
  QvRecomPQ3.Params[1].AsString := DataI;
  QvRecomPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvRecomPQ3, Dmod.MyDB);
  //
  QvPesagPQ3.Close;
  QvPesagPQ3.Params[0].AsInteger := Insumo;
  QvPesagPQ3.Params[1].AsString := DataI;
  QvPesagPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvPesagPQ3, Dmod.MyDB);
  //
  QvMistuPQ3.Close;
  QvMistuPQ3.Params[0].AsInteger := Insumo;
  QvMistuPQ3.Params[1].AsString := DataI;
  QvMistuPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvMistuPQ3, Dmod.MyDB);
  //
  QvConsEPQ3.Close;
  QvConsEPQ3.Params[0].AsInteger := Insumo;
  QvConsEPQ3.Params[1].AsString := DataI;
  QvConsEPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvConsEPQ3, Dmod.MyDB);
  //
  QvConsSPQ3.Close;
  QvConsSPQ3.Params[0].AsInteger := Insumo;
  QvConsSPQ3.Params[1].AsString := DataI;
  QvConsSPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvConsSPQ3, Dmod.MyDB);
  //
  QvHistoPQ3.Close;
  QvHistoPQ3.Params[0].AsInteger := Insumo;
  QvHistoPQ3.Params[1].AsString := DataI;
  QvHistoPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvHistoPQ3, Dmod.MyDB);

  ///////////////////////

  Peso := Peso + QvEntraPQ3.FieldByName('Peso').AsFloat
               - QvVendaPQ3.FieldByName('Peso').AsFloat
               - QvDevolPQ3.FieldByName('Peso').AsFloat
               - QvPesagPQ3.FieldByName('Peso').AsFloat
               + QvRecomPQ3.FieldByName('Peso').AsFloat
               + QvMistuPQ3.FieldByName('Peso').AsFloat
               - QvHistoPQ3.FieldByName('Peso').AsFloat
               + QvConsEPQ3.FieldByName('Peso').AsFloat
               - QvConsSPQ3.FieldByName('Peso').AsFloat;

  Valor := Valor + QvEntraPQ3.FieldByName('Valor').AsFloat
                 - QvVendaPQ3.FieldByName('Valor').AsFloat
                 - QvDevolPQ3.FieldByName('Valor').AsFloat
                 - QvPesagPQ3.FieldByName('Valor').AsFloat
                 + QvRecomPQ3.FieldByName('Valor').AsFloat
                 + QvMistuPQ3.FieldByName('Valor').AsFloat
                 - QvHistoPQ3.FieldByName('Valor').AsFloat
                 + QvConsEPQ3.FieldByName('Valor').AsFloat
                 - QvConsSPQ3.FieldByName('Valor').AsFloat;

  Result[0] := Peso;
  Result[2] := Valor;
  ////////////////
  Dmod.QrPQ1.Close;
  QvEntraPQ3.Close;
  QvVendaPQ3.Close;
  QvPesagPQ3.Close;
  QvMistuPQ3.Close;
  QvHistoPQ3.Close;
  QvConsEPQ3.Close;
  QvConsSPQ3.Close;*)
end;

function TUnPQx.AtualizaEstoquePQ(Cliente, Insumo, Empresa: Integer; TipoAviso:
TTipoAviso; NomeCompo: String; AtzSMI_PQX: Boolean = True): Boolean;
var
  Peso, Valor, PrMed: Double;
  Data: String;
  PeriodoBal: Integer;
begin
(* ini 2023-10-19
  ////////////////  False quando faz multi atualiza��es e faz uma vez antes!
  if AtzSMI_PQX then
    DMod.AtualizaStqMovIts_PQX(nil, nil, nil);
  ////////////////
*) // fim 2023-10-19
  Peso  := 0;
  Valor := 0;
  PeriodoBal := VerificaBalanco();
  if PeriodoBal = 0 then
  begin
    Data  := FormatDateTime(VAR_FORMATDATE, 2);
  end else begin
    Data := dmkPF.PrimeiroDiaAposPeriodo(PeriodoBal, dtSystem); //2?
    // Nao usa mais! Compatibilidade? Ate quando?
(* ini 2023-10-19
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrBalancosIts, Dmod.MyDB, [
    'SELECT ',
    'SUM(EstqQ) EstqQ, SUM(EstqV) EstqV, ',
    'SUM(EstqQ_G) EstqQ_G, SUM(EstqV_G) EstqV_G ',
    'FROM balancosits ',
    'WHERE Produto=' + Geral.FF0(Insumo),
    'AND Periodo=' + Geral.FF0(PeriodoBal),
    '']);
    Peso  := Dmod.QrBalancosItsEstqQ.Value;
    Valor := Dmod.QrBalancosItsEstqV.Value;
*) // fim 2023-10-19
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrSumPQ_H, Dmod.MyDB, [
  'SELECT SUM(Peso) Peso, SUM(Valor) Valor ',
  'FROM pqx ',
  'WHERE CliOrig=' + Geral.FF0(Cliente),
  'AND Insumo=' + Geral.FF0(Insumo),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND DataX>="' + Data + '"',
  'AND Tipo>=0 ',
  // 2016-02-13
  'AND HowLoad<>2 ',
  '']);
  //
  Peso  := Peso  + Dmod.QrSumPQ_HPeso.Value;
  Valor := Valor + Dmod.QrSumPQ_HValor.Value;
  ////////////////
(* ini 2023-10-19
  // Obtem pqcli.controle
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrPQCli, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM gragrux ',
  'WHERE GraGru1=' + Geral.FF0(Insumo),
  'ORDER BY Controle ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrSumSMIA, Dmod.MyDB, [
  'SELECT SUM(smia.Qtde) Qtde, SUM(CustoAll) CustoAll ',
  'FROM stqmovitsa smia ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE Baixa <> 0 ',
  'AND smia.ParTipo=0 ',
  'AND ggx.Controle=' + Geral.FF0(Dmod.QrPQCliControle.Value),
  'AND smia.DataHora >= "' + Data + '" ',
  '']);
  if Dmod.QrSumSMIAQtde.Value <> 0 then
  begin
    Peso  := Peso  + Dmod.QrSumSMIAQtde.Value;
    Valor := Valor + Dmod.QrSumSMIACustoAll.Value;
  end;
  //
*) // fim 2023-10-19
  Result := SQLAtualizaEstoquePQ(Peso, Valor, Cliente, Insumo, Empresa, TipoAviso, NomeCompo);
end;

{
function TUnPQx.AtualizaEstoquePQ(Cliente, Insumo: Integer; TipoAviso:
TTipoAviso; NomeCompo: String; AtzSMI_PQX: Boolean = True): Boolean;
var
  Peso, Valor, PrMed: Double;
  Data: String;
  PeriodoBal: Integer;
begin
  ////////////////  False quando faz multi atualiza��es e faz uma vez antes!
  if AtzSMI_PQX then
    DMod.AtualizaStqMovIts_PQX(nil);
  ////////////////
  PeriodoBal := VerificaBalanco;
  if PeriodoBal = 0 then
  begin
    Data  := FormatDateTime(VAR_FORMATDATE, 2);
    Peso  := 0;
    Valor := 0;
  end else begin
    Data := dmkPF.PrimeiroDiaAposPeriodo(PeriodoBal, dtSystem); //2?
    Dmod.QrBalancosIts.Close;
(*
SELECT
SUM(EstqQ) EstqQ, SUM(EstqV) EstqV,
SUM(EstqQ_G) EstqQ_G, SUM(EstqV_G) EstqV_G
FROM balancosits
WHERE Produto=:P0
AND Periodo=:P1
*)
    Dmod.QrBalancosIts.Params[0].AsInteger := Insumo;
    Dmod.QrBalancosIts.Params[1].AsInteger := PeriodoBal;
    UnDmkDAC_PF.AbreQuery(Dmod.QrBalancosIts, Dmod.MyDB);
    Peso  := Dmod.QrBalancosItsEstqQ.Value;
    Valor := Dmod.QrBalancosItsEstqV.Value;
  end;
(*
  Dmod.QrSumPQ_H.Close;
  Dmod.QrSumPQ_H.Params[0].AsInteger := Cliente;
  Dmod.QrSumPQ_H.Params[1].AsInteger := Insumo;
  Dmod.QrSumPQ_H.Params[2].AsString  := Data;
  UnDmkDAC_PF.AbreQuery(Dmod.QrSumPQ_H, Dmod.MyDB);
*)
(*
SELECT SUM(Peso) Peso, SUM(Valor) Valor
FROM pqx
WHERE CliOrig=:P0
AND Insumo=:P1
AND DataX>=:P2
*)
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrSumPQ_H, Dmod.MyDB, [
  'SELECT SUM(Peso) Peso, SUM(Valor) Valor ',
  'FROM pqx ',
  'WHERE CliOrig=' + Geral.FF0(Cliente),
  'AND Insumo=' + Geral.FF0(Insumo),
  'AND DataX>="' + Data + '"',
  'AND Tipo>=0 ',
  '']);
  //
  Peso  := Peso  + Dmod.QrSumPQ_HPeso.Value;
  Valor := Valor + Dmod.QrSumPQ_HValor.Value;
  ////////////////
  // Obtem pqcli.controle
  Dmod.QrPQCli.Close;
  Dmod.QrPQCli.Params[00].AsInteger := Insumo;
  Dmod.QrPQCli.Params[01].AsInteger := Cliente;
  UnDmkDAC_PF.AbreQuery(Dmod.QrPQCli, Dmod.MyDB);
  //
  Dmod.QrSumSMIA.Close;
(*
SELECT SUM(smia.Qtde) Qtde
FROM stqmovitsa smia
LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX
LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1
WHERE Baixa <> 0
AND smia.ParTipo=0
AND ggx.Controle=:P0
AND smia.DataHora >= :P1
*)
  Dmod.QrSumSMIA.Params[00].AsInteger := Dmod.QrPQCliControle.Value;
  Dmod.QrSumSMIA.Params[01].AsString  := Data;
  UnDmkDAC_PF.AbreQuery(Dmod.QrSumSMIA, Dmod.MyDB);

  if Dmod.QrSumSMIAQtde.Value <> 0 then
  begin
    if Peso = 0 then
      PrMed := 0
    else
      PrMed := Valor / Peso;
    if PrMed < 0 then
      PrMed := PrMed * -1;
    Peso  := Peso  + Dmod.QrSumSMIAQtde.Value;
    Valor := Valor + (Dmod.QrSumSMIAQtde.Value * PrMed);
  end;
  //
  Result := SQLAtualizaEstoquePQ(Peso, Valor, Cliente, Insumo, TipoAviso, NomeCompo);
end;
}

function TUnPQx.VerificaEstoquePQ(const Cliente, Insumo: Integer;
const Data: TDateTime; var Peso, Valor: Double): Boolean;
var
  PeriodoBal: Integer;
  DataI, DataF: String;
begin
  PeriodoBal := VerificaBalancoPelaData(Data);
  // 2014-08-30
(*
  if PeriodoBal = 0 then
  begin
    Peso  := 0;
    Valor := 0;
  end else begin
    Dmod.QrBalancosIts.Close;
    Dmod.QrBalancosIts.Params[0].AsInteger := Insumo;
    Dmod.QrBalancosIts.Params[1].AsInteger := PeriodoBal;
    UnDmkDAC_PF.AbreQuery(Dmod.QrBalancosIts, Dmod.MyDB);
    Peso  := Dmod.QrBalancosItsEstqQ.Value;
    Valor := Dmod.QrBalancosItsEstqV.Value;
  end;
  Dmod.QrSumPQ_D.Close;
  Dmod.QrSumPQ_D.Params[0].AsInteger := Cliente;
  Dmod.QrSumPQ_D.Params[1].AsInteger := Insumo;
  Dmod.QrSumPQ_D.Params[2].AsString  := dmkPF.PrimeiroDiaDoPeriodo(PeriodoBal, dtSystem);
  Dmod.QrSumPQ_D.Params[3].AsString  := Geral.FDT(Data,1);
  UnDmkDAC_PF.AbreQuery(Dmod.QrSumPQ_D, Dmod.MyDB);
  Peso  := Peso  + Dmod.QrSumPQ_DPeso.Value;
  Valor := Valor + Dmod.QrSumPQ_DValor.Value;
*)
  Peso  := 0;
  Valor := 0;
  DataI := dmkPF.PrimeiroDiaDoPeriodo(PeriodoBal, dtSystem);
  DataF := Geral.FDT(Data,1);
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrSumPQ_D, Dmod.MyDB, [
  'SELECT SUM(Peso) Peso, SUM(Valor) Valor ',
  'FROM pqx ',
  'WHERE CliOrig=' + Geral.FF0(Cliente),
  'AND Insumo=' + Geral.FF0(Insumo),
  'AND Tipo>=' + Geral.FF0(VAR_FATID_0000),
  'AND DataX BETWEEN "' + DataI + '" AND "' + DataF + '" ',
  '']);
  //Geral.MB_SQL(nil, Dmod.QrSumPQ_D);
  Peso  := Dmod.QrSumPQ_DPeso.Value;
  Valor := Dmod.QrSumPQ_DValor.Value;
  // FIM 2014-08-30
  ////////////////
  Result := True;
  ////////////////
end;

function TUnPQx.VerificaEstoquePQs(const Cliente: Integer;
  const Data: TDateTime; var ArrEstqPQs: TArrEstqPQs): Boolean;
var
  PeriodoBal, I, Insumo: Integer;
  DataI, DataF: String;
  Qry: TmySQLQuery;
begin
  Result := False;
  PeriodoBal := VerificaBalancoPelaData(Data);
  DataI := dmkPF.PrimeiroDiaDoPeriodo(PeriodoBal, dtSystem);
  DataF := Geral.FDT(Data,1);
  SetLength(ArrEstqPQs, 0);
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Insumo,  ',
    'SUM(Peso) Peso, SUM(Valor) Valor  ',
    'FROM pqx  ',
    'WHERE CliOrig=' + Geral.FF0(Cliente),
    'AND Tipo>=' + Geral.FF0(VAR_FATID_0000),
    'AND DataX BETWEEN "' + DataI + '" AND "' + DataF + '" ',
    'GROUP BY Insumo ',
    'ORDER BY Insumo DESC ',
    '']);
    Qry.First;
    SetLength(ArrEstqPQs, Qry.FieldByName('Insumo').AsInteger + 1);
    for I := Low(ArrEstqPQs) to High(ArrEstqPQs) do
    begin
      ArrEstqPQs[I,0] := 0.000;
      ArrEstqPQs[I,1] := 0.00;
    end;
    while not Qry.Eof do
    begin
      Insumo := Qry.FieldByName('Insumo').AsInteger;
      if Insumo > 0 then
      begin
        ArrEstqPQs[Insumo, 0] := Qry.FieldByName('Peso').AsFloat;
        ArrEstqPQs[Insumo, 1] := Qry.FieldByName('Valor').AsFloat;
      end;
      //
      Qry.Next;
    end;
    //
    Result := True;
  finally
    Qry.Free;
  end;
end;

{
function TUnPQx.VerificaEstoquePQ(Insumo: Integer; Data: TDate;
 Tipo: epHistTipo; IncBalDoDia: Boolean): MyArrayD22;
var
  Peso, Valor, PesoI, ValorI, BalQ, BalV: Double;
  DataI, DataF: String;
  PeriodoBal: Integer;
  Ano, Mes, Dia: Word;
begin
  (*Result[01] := 0;
  Result[02] := 0;
  Result[03] := 0;
  Result[04] := 0;
  Result[05] := 0;
  Result[06] := 0;
  Result[07] := 0;
  Result[08] := 0;
  Result[09] := 0;
  Result[10] := 0;
  Result[11] := 0;
  Result[12] := 0;
  Result[13] := 0;
  Result[14] := 0;
  Result[15] := 0;
  Result[16] := 0;
  Result[17] := 0;
  Result[18] := 0;
  Result[19] := 0;
  Result[20] := 0;
  Result[21] := 0;
  Result[22] := 0;
  //
  Dmod.Query2.Close;
  Dmod.Query2.SQL.Clear;
  Dmod.Query2.SQL.Add('SELECT MAX(Periodo) Periodo FROM balancos');
  Dmod.Query2.SQL.Add('WHERE Periodo<:P0');
  Dmod.Query2.Params[0].AsInteger := Geral.Periodo2000(Data);
  UnDmkDAC_PF.AbreQuery(Dmod.Query2, Dmod.MyDB);
  PeriodoBal := Dmod.Query2.FieldByName('Periodo').AsInteger;
  //
  Dmod.QrPQ1.Close;
  Dmod.QrPQ1.Params[0].AsInteger := Insumo;
  UnDmkDAC_PF.AbreQuery(Dmod.QrPQ1, Dmod.MyDB);
  if PeriodoBal = 0 then
  begin
    DataI  := FormatDateTime(VAR_FORMATDATE, 0);
    DataF  := FormatDateTime(VAR_FORMATDATE, Data);
    PesoI  := Dmod.QrPQ1SAkg.Value;
    ValorI := Dmod.QrPQ1SAVlr.Value;
  end else begin
    DataI := dmkPF.PrimeiroDiaAposPeriodo(PeriodoBal, dtSystem);  //2?
    DataF := FormatDateTime(VAR_FORMATDATE, Data);
    Dmod.QrBalancosIts.Close;
    Dmod.QrBalancosIts.Params[0].AsInteger  := Insumo;
    Dmod.QrBalancosIts.Params[1].AsInteger := PeriodoBal;
    UnDmkDAC_PF.AbreQuery(Dmod.QrBalancosIts, Dmod.MyDB);
    PesoI  := Dmod.QrBalancosItsEstqQ.Value;
    ValorI := Dmod.QrBalancosItsEstqV.Value;
  end;
  //
  QvEntraPQ3.Close;
  QvEntraPQ3.Params[0].AsInteger := Insumo;
  QvEntraPQ3.Params[1].AsString := DataI;
  QvEntraPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvEntraPQ3, Dmod.MyDB);
  //
  QvVendaPQ3.Close;
  QvVendaPQ3.Params[0].AsInteger := Insumo;
  QvVendaPQ3.Params[1].AsString := DataI;
  QvVendaPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvVendaPQ3, Dmod.MyDB);
  //
  QvDevolPQ3.Close;
  QvDevolPQ3.Params[0].AsInteger := Insumo;
  QvDevolPQ3.Params[1].AsString := DataI;
  QvDevolPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvDevolPQ3, Dmod.MyDB);
  //
  QvRecomPQ3.Close;
  QvRecomPQ3.Params[0].AsInteger := Insumo;
  QvRecomPQ3.Params[1].AsString := DataI;
  QvRecomPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvRecomPQ3, Dmod.MyDB);
  //
  QvPesagPQ3.Close;
  QvPesagPQ3.Params[0].AsInteger := Insumo;
  QvPesagPQ3.Params[1].AsString := DataI;
  QvPesagPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvPesagPQ3, Dmod.MyDB);
  //
  QvMistuPQ3.Close;
  QvMistuPQ3.Params[0].AsInteger := Insumo;
  QvMistuPQ3.Params[1].AsString := DataI;
  QvMistuPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvMistuPQ3, Dmod.MyDB);
  //
  QvConsEPQ3.Close;
  QvConsEPQ3.Params[0].AsInteger := Insumo;
  QvConsEPQ3.Params[1].AsString := DataI;
  QvConsEPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvConsEPQ3, Dmod.MyDB);
  //
  QvConsSPQ3.Close;
  QvConsSPQ3.Params[0].AsInteger := Insumo;
  QvConsSPQ3.Params[1].AsString := DataI;
  QvConsSPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvConsSPQ3, Dmod.MyDB);
  //
  QvHistoPQ3.Close;
  QvHistoPQ3.Params[0].AsInteger := Insumo;
  QvHistoPQ3.Params[1].AsString := DataI;
  QvHistoPQ3.Params[2].AsString := DataF;
  UnDmkDAC_PF.AbreQuery(QvHistoPQ3, Dmod.MyDB);
  ////////////////////////
  Peso := PesoI + QvEntraPQ3.FieldByName('Peso').AsFloat
               - QvVendaPQ3.FieldByName('Peso').AsFloat
               - QvDevolPQ3.FieldByName('Peso').AsFloat
               - QvPesagPQ3.FieldByName('Peso').AsFloat
               + QvRecomPQ3.FieldByName('Peso').AsFloat
               + QvMistuPQ3.FieldByName('Peso').AsFloat
               - QvHistoPQ3.FieldByName('Peso').AsFloat
               + QvConsEPQ3.FieldByName('Peso').AsFloat
               - QvConsSPQ3.FieldByName('Peso').AsFloat;

  Valor := ValorI + QvEntraPQ3.FieldByName('Valor').AsFloat
                 - QvVendaPQ3.FieldByName('Valor').AsFloat
                 - QvDevolPQ3.FieldByName('Valor').AsFloat
                 - QvPesagPQ3.FieldByName('Valor').AsFloat
                 + QvRecomPQ3.FieldByName('Valor').AsFloat
                 + QvMistuPQ3.FieldByName('Valor').AsFloat
                 - QvHistoPQ3.FieldByName('Valor').AsFloat
                 + QvConsEPQ3.FieldByName('Valor').AsFloat
                 - QvConsSPQ3.FieldByName('Valor').AsFloat;
  ///////////////////////
  BalQ := 0;
  BalV := 0;
  if IncBalDoDia then
  begin
    DecodeDate(Data + 1, Ano, Mes, Dia);
    if Dia = 1 then
    begin
      Dmod.QrBalancosIts.Close;
      Dmod.QrBalancosIts.Params[0].AsInteger  := Insumo;
      Dmod.QrBalancosIts.Params[1].AsInteger := Geral.Periodo2000(Data);
      UnDmkDAC_PF.AbreQuery(Dmod.QrBalancosIts, Dmod.MyDB);
      BalQ  := Dmod.QrBalancosItsEstqQ.Value - Peso;
      BalV  := Dmod.QrBalancosItsEstqV.Value - Valor;
      Dmod.QrBalancosIts.Close;
    end;
  end;
  ///////////////////////
  if Tipo = epSintetico then
  begin
    Result[1] := Peso  + BalQ;
    Result[2] := Valor + BalV;
  end;
  if Tipo = epAnalitico then
  begin
    Result[01] := PesoI;
    Result[03] := QvEntraPQ3.FieldByName('Peso').AsFloat;
    Result[05] := QvVendaPQ3.FieldByName('Peso').AsFloat;
    Result[07] := QvDevolPQ3.FieldByName('Peso').AsFloat;
    Result[09] := QvPesagPQ3.FieldByName('Peso').AsFloat;
    Result[11] := QvRecomPQ3.FieldByName('Peso').AsFloat;
    Result[13] := QvMistuPQ3.FieldByName('Peso').AsFloat;
    Result[15] := QvHistoPQ3.FieldByName('Peso').AsFloat;
    Result[17] := QvConsEPQ3.FieldByName('Peso').AsFloat;
    Result[19] := QvConsSPQ3.FieldByName('Peso').AsFloat;
    Result[21] := BalQ;

    Result[02] := ValorI;
    Result[04] := QvEntraPQ3.FieldByName('Valor').AsFloat;
    Result[06] := QvVendaPQ3.FieldByName('Valor').AsFloat;
    Result[08] := QvDevolPQ3.FieldByName('Valor').AsFloat;
    Result[10] := QvPesagPQ3.FieldByName('Valor').AsFloat;
    Result[12] := QvRecomPQ3.FieldByName('Valor').AsFloat;
    Result[14] := QvMistuPQ3.FieldByName('Valor').AsFloat;
    Result[16] := QvHistoPQ3.FieldByName('Valor').AsFloat;
    Result[18] := QvConsEPQ3.FieldByName('Valor').AsFloat;
    Result[20] := QvConsSPQ3.FieldByName('Valor').AsFloat;
    Result[22] := BalV;
  end;
  ////////////////
  Dmod.QrPQ1.Close;
  QvEntraPQ3.Close;
  QvVendaPQ3.Close;
  QvPesagPQ3.Close;
  QvMistuPQ3.Close;
  QvHistoPQ3.Close;
  QvConsEPQ3.Close;
  QvConsSPQ3.Close;*)
end;
}

procedure TUnPQx.DefineVAR_Data_Insum_Movim();
var
  Qry: TmySQLQuery;
begin

  VAR_Data_Insum_Movim := StrToDate(dmkPF.PrimeiroDiaAposPeriodo(VerificaBalanco(), dtSystem3));
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(DataAtu) DataAtu ',
    'FROM pqbcicab ',
    '']);
    if Qry.Fields[0].AsDateTime > VAR_Data_Insum_Movim then
      VAR_Data_Insum_Movim := Qry.Fields[0].AsDateTime;
  finally
    Qry.Free;
  end;
end;

function TUnPQx.ExcluiPQemPesagemComSeguranca(Pesagem: Integer; Conta:
Double; Confirma, AvisaSucesso: Boolean): Boolean;
{var
  Qr1, Qr2: TmySQLQuery;
  Txt: String;}
begin
  Result := True;
  (*Result := False;
  Qr1:= TmySQLQuery.Create(Dmod);
  //TmySQLQuery.Create(Qr1);
  Qr1.Database := Dmod.MyDB;
  Qr1.SQL.Add('SELECT * FROM pesagem');
  Qr1.SQL.Add('WHERE Pesagem='+IntToStr(Pesagem));
  Qr1.SQL.Add('AND Conta='+FloatToStr(Conta));
  UnDmkDAC_PF.AbreQuery(Qr1, Dmod.MyDB);
  if DermPQ.ImpedePeloBalanco(Qr1.FieldByName('Data').AsDateTime) then Exit;
  Txt :=
    sLineBreak+'Pesagem     = '+IntToStr(Pesagem)+
    sLineBreak+'Insumo      = '+IntToStr(Qr1.FieldByName('Insumo').AsInteger)+
    sLineBreak+'Peso        = '+FormatFloat('0.000', Qr1.FieldByName('Peso').AsFloat)+
    sLineBreak+'Valor       = '+FormatFloat('0.00', Qr1.FieldByName('Valor').AsFloat)+
    sLineBreak+'Peso extra  = '+FormatFloat('0.000', Qr1.FieldByName('PesoExtra').AsFloat)+
    sLineBreak+'Valor extra = '+FormatFloat('0.00', Qr1.FieldByName('ValorExtra').AsFloat);
  if Confirma then
  if Application.MessageBox(PChar('Confirma a exclus�o de:'+Txt),
  'Exclus�o de Item de Pesagem', MB_YESNOCANCEL+MB_ICONQUESTION+
  MB_DEFBUTTON2) = ID_YES then
  begin
    Qr2:= TmySQLQuery.Create(Dmod);
    //TmySQLQuery.Create(Qr2);
    Qr2.Database := Dmod.MyDB;
    Qr2.SQL.Add('DELETE FROM pesagem');
    Qr2.SQL.Add('WHERE Pesagem='+IntToStr(Pesagem));
    Qr2.SQL.Add('AND Conta='+FloatToStr(Conta));
    Qr2.ExecSQL;
    //
    if AtualizaEstoquePQ(Qr1.FieldByName('Insumo').AsInteger, aeNenhum, CO_VAZIO) then
    begin
      Qr2.SQL.Add('INSERT INTO pesagem SET');
      Qr2.SQL.Add('Conta=:P0, Pesagem=:P1, Tipo=:P2, Data=:P3, Insumo=:P4, ');
      Qr2.SQL.Add('Peso=:P5, Valor=:P6, PesoExtra=:P7, ValorExtra=:P8, Mistura=:P9');
      Qr2.Params[0].AsFloat   := Conta;
      Qr2.Params[1].AsInteger := Pesagem;
      Qr2.Params[2].AsInteger := Qr1.FieldByName('Tipo').AsInteger;
      Qr2.Params[3].AsString  := FormatDateTime(VAR_FORMATDATE, Qr1.FieldByName('Data').AsDateTime);
      Qr2.Params[4].AsInteger := Qr1.FieldByName('Insumo').AsInteger;
      Qr2.Params[5].AsFloat   := Qr1.FieldByName('Peso').AsFloat;
      Qr2.Params[6].AsFloat   := Qr1.FieldByName('Valor').AsFloat;
      Qr2.Params[7].AsFloat   := Qr1.FieldByName('PesoExtra').AsFloat;
      Qr2.Params[8].AsFloat   := Qr1.FieldByName('ValorExtra').AsFloat;
      Qr2.Params[9].AsInteger := Qr1.FieldByName('Mistura').AsInteger;
      Qr2.ExecSQL;
      if AtualizaEstoquePQ(Qr1.FieldByName('Insumo').AsInteger, aeMsg, CO_VAZIO) then
        Application.MessageBox(PChar('N�o foi poss�vel cancelar a exclus�o!'+Txt),
        'Erro', MB_OK+MB_ICONERROR)
    end else begin
      Result := True;
      if AvisaSucesso then
      Application.MessageBox(PChar('Exclus�o conclu�da!'+Txt),
      'Exclus�o de Item de Pesagem', MB_OK+MB_ICONWARNING);
    end;
    Qr2.Free;
  end;
  Qr1.Close;
  Qr1.Free;*)
end;

procedure TUnPQx.GerenciaBaixaDevolucao(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPQD, FmPQD, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPQD.LocCod(Codigo, Codigo);
    FmPQD.ShowModal;
    FmPQD.Destroy;
  end;
end;

procedure TUnPQx.GerenciaBaixaETE(Codigo: Integer);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmPQT2, FmPQT2, afmoNegarComAviso) then
      begin
        FmPQT2.ShowModal;
        FmPQT2.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmPQT, FmPQT, afmoNegarComAviso) then
      begin
        if Codigo <> 0 then
          FmPQT.LocCod(Codigo, Codigo);
        FmPQT.ShowModal;
        FmPQT.Destroy;
      end;
{
  end;
}
end;

procedure TUnPQx.GerenciaBaixaOutros(Codigo: Integer);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmPQO2, FmPQO2, afmoNegarComAviso) then
      begin
        FmPQO2.ShowModal;
        FmPQO2.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmPQO, FmPQO, afmoNegarComAviso) then
      begin
        if Codigo <> 0 then
          FmPQO.LocCod(Codigo, Codigo);
        FmPQO.ShowModal;
        FmPQO.Destroy;
      end;
{
  end;
}
end;

procedure TUnPQx.GerenciaBaixaPQ(Tipo, Codigo: Integer);
begin
  case Tipo of
    110: GerenciaPesagem(Codigo);
    150: GerenciaBaixaETE(Codigo);
    190: GerenciaBaixaOutros(Codigo);
    else Geral.MB_Erro(
    'Tipo de baixa de PQ n�o definido em  "UnPQx.GerenciaBaixaPQ()"');
  end;
end;

procedure TUnPQx.GerenciaPesagem(Pesagem: Integer);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmPQxExcl2, FmPQxExcl2, afmoNegarComAviso) then
      begin
        if Pesagem <> 0 then
        begin
          FmPQxExcl2.EdPesagem.ValueVariant := Pesagem;
          FmPQxExcl2.ReopenEmit(True);
        end;
        FmPQxExcl2.ShowModal;
        FmPQxExcl2.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmPQxExcl, FmPQxExcl, afmoNegarComAviso) then
      begin
        if Pesagem <> 0 then
        begin
          FmPQxExcl.EdPesagem.ValueVariant := Pesagem;
          FmPQxExcl.ReopenEmit(True);
        end;
        FmPQxExcl.ShowModal;
        FmPQxExcl.Destroy;
      end;
{
  end;
}
end;

function TUnPQx.VerificaBalanco(): Integer;
begin
  //ini 2023-10-19 tentativa de deixar mais r�pido, mas ficou apenas 4,76% mais r�pido
(*
  if VAR_PERIODO_BAL = -2 then
  begin
*)
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrPeriodoBal, Dmod.MyDB, [
    'SELECT MAX(Periodo) Periodo',
    'FROM balancos',
    'WHERE Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
    '']);
    //
    Result := Dmod.QrPeriodoBalPeriodo.Value-1;
    Dmod.QrPeriodoBal.Close;
(*
  end;
*)
  //VAR_PERIODO_BAL := Result;
end;

function TUnPQx.VerificaBalancoPelaData(Data: TDateTime): Integer;
var
  Periodo: Integer;
begin
  Periodo := Geral.Periodo2000(Data);
(*
  Dmod.QrPeriodoBal.Close;
  Dmod.QrPeriodoBal.SQL.Clear;
  Dmod.QrPeriodoBal.SQL.Add('SELECT Max(Periodo) Periodo');
  Dmod.QrPeriodoBal.SQL.Add('FROM balancos');
  Dmod.QrPeriodoBal.SQL.Add('WHERE Periodo <= :P0');
  Dmod.QrPeriodoBal.Params[0].AsInteger := Periodo;
  UnDmkDAC_PF.AbreQuery(Dmod.QrPeriodoBal, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrPeriodoBal, Dmod.MyDB, [
  'SELECT MAX(Periodo) Periodo',
  'FROM balancos',
  'WHERE Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
  'AND Periodo<=' + Geral.FF0(Periodo),
  '']);
  //
  Result := Dmod.QrPeriodoBalPeriodo.Value;
  Dmod.QrPeriodoBal.Close;
end;

function TUnPQx.ImpedePeloBalanco(Data: TDateTime; VerificaHoje: Boolean = False;
  Pergunta: String = ''): Boolean;
var
  Dia: TDateTime;
begin
  (* ini 2023-10-19
  // Novo balan�o n�o pode mais permitir altera��es!!!
  if Dmod.QrControleCanAltBalA.Value = 1 then
    Result := False
  else
  *) // fim 2023-10-19
  begin
    Result := True;
    // ini 2023-11-04
    // se houver balan�o intermedi�rio obedecer este pois � data posterior ao balan�o principal
    //Dia    := StrToDate(dmkPF.PrimeiroDiaAposPeriodo(VerificaBalanco(), dtSystem3));
    DefineVAR_Data_Insum_Movim();
    Dia    := VAR_Data_Insum_Movim;
    // fim 2023-11-04
    if Data >= Dia then
      Result := False
    else
      if Pergunta <> EmptyStr then
        Result := Geral.MB_Pergunta(Pergunta) <> ID_Yes
      else
  (* ini 2023-10-19
  // Novo balan�o n�o pode mais permitir altera��es!!!
        Geral.MB_Aviso('Data inv�lida. J� existe balan�o com data ' +
        'posterior.' + sLineBreak + 'Solicite ao seu superior o desbloqueio em: '
        + sLineBreak + 'Ferramentas > Op��es > Espec�ficos.' + sLineBreak +
        'Na aba "Miscel�nea" marque a op��o:' + sLineBreak +
        '"Permite alterar estoque de insumos ap�s balancete (invent�rio)".');
  *)
        Geral.MB_Aviso('Data inv�lida. J� existe balan�o com data posterior.' +
        sLineBreak + 'Data m�nima para lan�amento: ' + Geral.FDT(Dia, 2));

  // fim 2023-10-19
  end;
  // ini 2022-05-17
  if VerificaHoje then
  begin
    if Trunc(Data) > Trunc(Date()) then
    begin
      Result := True;
      Geral.MB_Aviso('Data inv�lida. Data n�o pode ser maior que a data de hoje: ' + Geral.FDT(Trunc(Date()), 2));
    end;
  end;
  // Fim 2022-05-17
end;

procedure TUnPQx.MostraFormEmitCab(SQLType: TSQLType; Pesagem: Integer);
begin
  if DBCheck.CriaFm(TFmEmitCab, FmEmitCab, afmoNegarComAviso) then
  begin
    FmEmitCab.ImgTipo.SQLType := SQLTYpe;
    FmEmitCab.ReopenEmit(Pesagem);
    FmEmitCab.ShowModal;
    //
    FmEmitCab.Destroy;
  end;
end;

procedure TUnPQx.MostraFormEmitUsoSbt(SQLType: TSQLType; Pesagem, EmitGru: Integer);
begin
end;

procedure TUnPQx.MostraFormPQBanhosImp;
begin
  if DBCheck.CriaFm(TFmPQBanhosImp, FmPQBanhosImp, afmoNegarComAviso) then
  begin
    FmPQBanhosImp.ShowModal;
    FmPQBanhosImp.Destroy;
  end;
end;

procedure TUnPQx.MostraFormPQI(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPQI, FmPQI, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPQI.LocCod(Codigo, Codigo);
    FmPQI.ShowModal;
    FmPQI.Destroy;
  end;
end;

procedure TUnPQx.MostraFormPQMCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPQMCab, FmPQMCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPQMCab.LocCod(Codigo, Codigo);
    FmPQMCab.ShowModal;
    FmPQMCab.Destroy;
  end;
end;

procedure TUnPQx.MostraFormPQN(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPQN, FmPQN, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPQN.LocCod(Codigo, Codigo);
    FmPQN.ShowModal;
    FmPQN.Destroy;
  end;
end;

function TUnPQx.ObtemDataBal(): TDateTime;
begin
  UnPQx.DefineVAR_Data_Insum_Movim();
  Result := VAR_Data_Insum_Movim;
end;

function TUnPQx.ObtemGGXdeInsumo(Insumo: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrPQCli, Dmod.MyDB, [
  'SELECT Controle ',
(* ERRO!
  'FROM pqcli ',
  'WHERE PQ=' + Geral.FF0(Insumo),
  'AND CI=' + Geral.FF0(Cliente),
*)
  'FROM gragrux ',
  'WHERE GraGru1=' + Geral.FF0(Insumo),
  'ORDER BY Controle ',
  '']);
  //
  Result := Dmod.QrPQCliControle.Value;
end;

function TUnPQx.ObtemGGXdeInsumo_Fast(Insumo: Integer): Integer;
const
  sProcName = 'TUnPQx.ObtemGGXdeInsumo_Fast()';
var
  IsOk: Boolean;
begin
  Result := DModG.MyCompressDB.SelectInteger(//aSQL : string; var IsOk : boolean; aFieldName : string):integer;
    ' SELECT Controle ' +
    ' FROM gragrux ' +
    ' WHERE GraGru1=' + Geral.FF0(Insumo) +
    ' ORDER BY Controle ',
    IsOK, 'Controle');
  if IsOk = False then
  begin
    Result := 0;
    Geral.MB_Erro('Reduzido nao encontrado para o Insumo ' +
    Geral.FF0(Insumo) + ' em ' + sProcName);
  end;
end;

function TUnPQx.ObtemInsumoDeGraGruX(GraGruX: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT GraGru1 ',
    'FROM gragrux ',
    'WHERE Controle=' + Geral.FF0(GraGruX),
    '']);
    Result := Qry.FieldByName('GraGru1').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnPQx.PQ_IncluiClienteInterno(const SQLType: TSQLType; const PQ,
  Empresa, CI, MoedaPadrao, CustoPadraoAtz, MoedaMan, EstSegur, EdtCtrl: Integer;
  const MinEstq, CustoPadrao, CustoMan, KgLiqEmb: Double; const NO_CI, CodProprio: String;
  var Controle: Integer): Boolean;
var
  GraGru1, GraGruC, GraTamI: Integer;
begin
  Result := False;
  if SQLType = stIns then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrDuplCli, Dmod.MyDB, [
    'SELECT * ',
    'FROM pqcli ',
    'WHERE PQ=' + Geral.FF0(PQ),
    'AND CI=' + Geral.FF0(CI),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
    if MyObjects.FIC(Dmod.QrDuplCli.RecordCount > 0, nil,
    'Cliente interno j� cadastrado para o insumo na empresa logada!') then
      Exit;
    //
    GraGru1 := PQ; //QrPqCodigo.Value;
    // 2011-05-07
    Controle := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrLocGGX, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM GraGruX ',
    'WHERE GraGru1=' + Geral.FF0(GraGru1),
    '']);
    (*Dmod.QrLocGGX.Close;
    Dmod.QrLocGGX.Params[0].AsInteger := GraGru1;
    UnDmkDAC_PF.AbreQuery(Dmod.QrLocGGX, Dmod.MyDB);
    *)
    if Dmod.QrLocGGX.RecordCount = 0 then
    begin
      GraGruC := DmProd.ObtemGraGruCDeIdx(GraGru1, 'PQCli', NO_CI(*CBCI.Text*));
      //
       //GraTamI := 1; // �nico
      // 2011-04-28
      GraTamI := 0; // �nico
      //Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'PQCli',
      if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False,
        ['GraGruC', 'GraGru1', 'GraTamI'], ['Controle'],
        [GraGruC, GraGru1, GraTamI], [Controle], True)
      then
        Controle := 0;
      if (GraGru1 = 0) or (GraGruC = 0) or (Controle = 0) then
      begin
        Geral.MB_Erro('N�o foi poss�vel gerar o item de grade!');
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    // FIM 2011-05-07
    // 2014-07-24
    //Dmod.QrUpd.SQL.Clear;
    //Dmod.QrUpd.SQL.Add('INSERT INTO pqcli SET ');
  end else begin
    //Dmod.QrUpd.SQL.Clear;
    //Dmod.QrUpd.SQL.Add('UPDATE pqcli SET ');
    Controle := EdtCtrl; //QrPQCliControle.Value;
  end;
  {
  Dmod.QrUpd.SQL.Add('PQ=:P0, CI=:P1, MinEstq=:P2, EstSegur=:P3, ');
  Dmod.QrUpd.SQL.Add('CustoPadrao=:P4, MoedaPadrao=:P5, ');
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpd.SQL.Add('DataCad=:Px, UserCad=:Py, Controle=:Pz')
  else Dmod.QrUpd.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Controle=:Pz');
  Dmod.QrUpd.Params[00].AsInteger := PQ;
  Dmod.QrUpd.Params[01].AsInteger := CI;
  Dmod.QrUpd.Params[02].AsFloat   := MinEstq;
  Dmod.QrUpd.Params[03].AsInteger := EstSegur;
  Dmod.QrUpd.Params[04].AsFloat   := CustoPadrao;
  Dmod.QrUpd.Params[05].AsInteger := MoedaPadrao;
  //
  Dmod.QrUpd.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[07].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[08].AsInteger := Controle;
  UMyMod.ExecutaQuery(Dmod.QrUpd);
  }
  //
  //if
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqcli', False, [
    'PQ', 'CI', 'MinEstq',
    'EstSegur', 'CustoPadrao', 'CustoPadraoAtz', 'MoedaPadrao',
    //'Peso', 'Valor', 'Custo'
    'CodProprio', 'Empresa', 'KgLiqEmb',
    'CustoMan', 'MoedaMan'], [
    'Controle'], [
    PQ, CI, MinEstq,
    EstSegur, CustoPadrao, CustoPadraoAtz, MoedaPadrao,
    //Peso, Valor, Custo
    CodProprio, Empresa, KgLiqEmb,
    CustoMan, MoedaMan], [
    Controle], True);
  // FIM 2014-07-24
  //FCli := Controle;
end;

procedure TUnPQx.SetaVariaveisIniciais;
var
  Caminho: String;
begin
  BDC_CAMINHO_REGEDIT := 'Dermatek\BlueDerm';
  //
  Caminho := BDC_CAMINHO_REGEDIT+'\PQ';
  BDC_ATUALIZA_PRECO_PQ := Geral.ReadAppKey('AlteraPrecoEntrada', Caminho,
  ktBoolean, True, HKEY_LOCAL_MACHINE);
  //
  BDC_CUSTO_PQ_CONSIGNA := Geral.ReadAppKey('CustoPQConsigna', Caminho,
  ktInteger, True, HKEY_LOCAL_MACHINE);
  //
  BDC_PROCESSAMENTO_COURO := Geral.ReadAppKey('ProcessamentoCouro', Caminho,
  ktInteger, True, HKEY_LOCAL_MACHINE);
  //
end;

end.


