unit WBOutCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, UnAppEnums;

type
  TFmWBOutCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrWBoutCab: TmySQLQuery;
    DsWBOutCab: TDataSource;
    QrWBOutIts: TmySQLQuery;
    DsWBOutIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtVenda: TdmkEditDateTimePicker;
    EdDtVenda: TdmkEdit;
    Label53: TLabel;
    TPDtViagem: TdmkEditDateTimePicker;
    EdDtViagem: TdmkEdit;
    Label3: TLabel;
    TPDtEntrega: TdmkEditDateTimePicker;
    EdDtEntrega: TdmkEdit;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    QrTransporta: TmySQLQuery;
    DsTransporta: TDataSource;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    Label4: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrWBoutCabCodigo: TIntegerField;
    QrWBoutCabMovimCod: TIntegerField;
    QrWBoutCabEmpresa: TIntegerField;
    QrWBoutCabDtVenda: TDateTimeField;
    QrWBoutCabDtViagem: TDateTimeField;
    QrWBoutCabDtEntrega: TDateTimeField;
    QrWBoutCabCliente: TIntegerField;
    QrWBoutCabTransporta: TIntegerField;
    QrWBoutCabPecas: TFloatField;
    QrWBoutCabPesoKg: TFloatField;
    QrWBoutCabAreaM2: TFloatField;
    QrWBoutCabAreaP2: TFloatField;
    QrWBoutCabLk: TIntegerField;
    QrWBoutCabDataCad: TDateField;
    QrWBoutCabDataAlt: TDateField;
    QrWBoutCabUserCad: TIntegerField;
    QrWBoutCabUserAlt: TIntegerField;
    QrWBoutCabAlterWeb: TSmallintField;
    QrWBoutCabAtivo: TSmallintField;
    QrWBoutCabNO_EMPRESA: TWideStringField;
    QrWBoutCabNO_CLIENTE: TWideStringField;
    QrWBoutCabNO_TRANSPORTA: TWideStringField;
    QrWBOutItsCodigo: TIntegerField;
    QrWBOutItsControle: TIntegerField;
    QrWBOutItsMovimCod: TIntegerField;
    QrWBOutItsEmpresa: TIntegerField;
    QrWBOutItsMovimID: TIntegerField;
    QrWBOutItsGraGruX: TIntegerField;
    QrWBOutItsPecas: TFloatField;
    QrWBOutItsPesoKg: TFloatField;
    QrWBOutItsAreaM2: TFloatField;
    QrWBOutItsAreaP2: TFloatField;
    QrWBOutItsLk: TIntegerField;
    QrWBOutItsDataCad: TDateField;
    QrWBOutItsDataAlt: TDateField;
    QrWBOutItsUserCad: TIntegerField;
    QrWBOutItsUserAlt: TIntegerField;
    QrWBOutItsAlterWeb: TSmallintField;
    QrWBOutItsAtivo: TSmallintField;
    QrWBOutItsNO_PRD_TAM_COR: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TDBGrid;
    QrWBOutItsSrcMovID: TIntegerField;
    QrWBOutItsSrcNivel1: TIntegerField;
    QrWBOutItsSrcNivel2: TIntegerField;
    QrWBOutItsPallet: TIntegerField;
    QrWBOutItsNO_Pallet: TWideStringField;
    ItemComrastreio1: TMenuItem;
    ItemSemrastreio1: TMenuItem;
    QrWBOutItsMovimNiv: TIntegerField;
    QrWBOutItsTerceiro: TIntegerField;
    QrWBOutItsDataHora: TDateTimeField;
    QrWBOutItsValorT: TFloatField;
    QrWBOutItsSdoVrtPeca: TFloatField;
    QrWBOutItsSdoVrtArM2: TFloatField;
    QrWBOutItsObserv: TWideStringField;
    QrWBOutItsLnkNivXtr1: TIntegerField;
    QrWBOutItsLnkNivXtr2: TIntegerField;
    QrWBOutItsCliVenda: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWBoutCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWBoutCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrWBoutCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrWBoutCabBeforeClose(DataSet: TDataSet);
    procedure ItemComrastreio1Click(Sender: TObject);
    procedure ItemSemrastreio1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormWBOutUnk(SQLType: TSQLType);
    procedure MostraFormWBOutKno(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenWBOutIts(Controle: Integer);

  end;

var
  FmWBOutCab: TFmWBOutCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, WBOutUnk, ModuleGeral,
Principal, WBOutKno;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWBOutCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWBOutCab.MostraFormWBOutKno(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmWBOutKno, FmWBOutKno, afmoNegarComAviso) then
  begin
    FmWBOutKno.ImgTipo.SQLType := SQLType;
    FmWBOutKno.FQrCab := QrWBOutCab;
    FmWBOutKno.FDsCab := DsWBOutCab;
    FmWBOutKno.FQrIts := QrWBOutIts;
    FmWBOutKno.FDataHora := QrWBoutCabDtVenda.Value;
    if SQLType = stIns then
    begin
      //FmWBOutKno.EdCPF1.ReadOnly := False
{
    end else
    begin
      FmWBOutKno.FDataHora := QrWBOutCabDtVenda.Value;
      FmWBOutKno.EdControle.ValueVariant := QrWBOutItsControle.Value;
      //
      FmWBOutKno.EdGragruX.ValueVariant  := QrWBOutItsGraGruX.Value;
      FmWBOutKno.CBGragruX.KeyValue      := QrWBOutItsGraGruX.Value;
      FmWBOutKno.EdPecas.ValueVariant    := -QrWBOutItsPecas.Value;
      FmWBOutKno.EdPesoKg.ValueVariant   := -QrWBOutItsPesoKg.Value;
      FmWBOutKno.EdAreaM2.ValueVariant   := -QrWBOutItsAreaM2.Value;
      FmWBOutKno.EdAreaP2.ValueVariant   := -QrWBOutItsAreaP2.Value;
      //FmWBOutKno.EdValorT.ValueVariant   := -QrWBOutItsValorT.Value;
      FmWBOutKno.EdPallet.ValueVariant   := QrWBOutItsPallet.Value;
      //
    end;
}
    FmWBOutKno.ShowModal;
    FmWBOutKno.Destroy;
    end;
  end;
end;

procedure TFmWBOutCab.MostraFormWBOutUnk(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmWBOutUnk, FmWBOutUnk, afmoNegarComAviso) then
  begin
    FmWBOutUnk.ImgTipo.SQLType := SQLType;
    FmWBOutUnk.FEmpresa := QrWBoutCabEmpresa.Value;
    FmWBOutUnk.FQrCab := QrWBOutCab;
    FmWBOutUnk.FDsCab := DsWBOutCab;
    FmWBOutUnk.FQrIts := QrWBOutIts;
    FmWBOutUnk.FDataHora := QrWBoutCabDtVenda.Value;
    if SQLType = stIns then
    begin
      //FmWBOutUnk.EdCPF1.ReadOnly := False
    end else
    begin
      FmWBOutUnk.FDataHora := QrWBOutCabDtVenda.Value;
      FmWBOutUnk.EdControle.ValueVariant := QrWBOutItsControle.Value;
      //
      FmWBOutUnk.EdGragruX.ValueVariant    := QrWBOutItsGraGruX.Value;
      FmWBOutUnk.CBGragruX.KeyValue        := QrWBOutItsGraGruX.Value;
      FmWBOutUnk.EdFornecedor.ValueVariant := QrWBOutItsTerceiro.Value;
      FmWBOutUnk.CBFornecedor.KeyValue     := QrWBOutItsTerceiro.Value;
      FmWBOutUnk.EdPecas.ValueVariant      := -QrWBOutItsPecas.Value;
      FmWBOutUnk.EdPesoKg.ValueVariant     := -QrWBOutItsPesoKg.Value;
      FmWBOutUnk.EdAreaM2.ValueVariant     := -QrWBOutItsAreaM2.Value;
      FmWBOutUnk.EdAreaP2.ValueVariant     := -QrWBOutItsAreaP2.Value;
      FmWBOutUnk.EdValorT.ValueVariant     := -QrWBOutItsValorT.Value;
      // Deve ser por ultimo
      FmWBOutUnk.EdPallet.ValueVariant     := QrWBOutItsPallet.Value;
      FmWBOutUnk.CBPallet.KeyValue         := QrWBOutItsPallet.Value;
      //
    end;
    FmWBOutUnk.ShowModal;
    FmWBOutUnk.Destroy;
  end;
end;

procedure TFmWBOutCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrWBOutCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrWBOutCab, QrWBOutIts);
end;

procedure TFmWBOutCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrWBOutCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrWBOutIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrWBOutIts);
end;

procedure TFmWBOutCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWBOutCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWBOutCab.DefParams;
begin
  VAR_GOTOTABELA := 'wboutcab';
  VAR_GOTOMYSQLTABLE := QrWBOutCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_CLIENTE,');
  VAR_SQLx.Add('IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA');
  VAR_SQLx.Add('FROM wboutcab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=wic.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades trn ON trn.Codigo=wic.transporta');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmWBOutCab.ItemComrastreio1Click(Sender: TObject);
begin
  MostraFormWBOutKno(stIns);
end;

procedure TFmWBOutCab.ItemSemrastreio1Click(Sender: TObject);
begin
  MostraFormWBOutUnk(stIns);
end;

procedure TFmWBOutCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormWBOutUnk(stUpd);
end;

procedure TFmWBOutCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmWBOutCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWBOutCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWBOutCab.ItsExclui1Click(Sender: TObject);
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrWBOutItsCodigo.Value;
  MovimCod := QrWBOutItsMovimCod.Value;
  //
  if Dmod.ExcluiControleWBMovIts(QrWBOutIts, QrWBOutItsControle,
  QrWBOutItsControle.Value, QrWBOutItsSrcNivel2.Value) then
  begin
    Dmod.AtualizaTotaisWBXxxCab('wboutcab', MovimCod);
    FmWBOutCab.LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWBOutCab.ReopenWBOutIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBOutIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrWBOutCabMovimCod.Value),
  'ORDER BY wmi.Controle ',
  '']);
  //
  QrWBOutIts.Locate('Controle', Controle, []);
end;


procedure TFmWBOutCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWBOutCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWBOutCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWBOutCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWBOutCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWBOutCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWBOutCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWBOutCabCodigo.Value;
  Close;
end;

procedure TFmWBOutCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWBOutCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'wboutcab');
  Empresa := DModG.ObtemFilialDeEntidade(QrWBOutCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
  // Evitar recalculo P2
  //EdAreaM2.ValueVariant := QrWBoutCabAreaM2.Value;
end;

procedure TFmWBOutCab.BtConfirmaClick(Sender: TObject);
var
  DtVenda, DtViagem, DtEntrega, DataHora: String;
  Codigo, MovimCod, Empresa, Cliente, Transporta, CliVenda: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
begin
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtVenda        := Geral.FDT_TP_Ed(TPDtVenda.Date, EdDtVenda.Text);
  DtViagem       := Geral.FDT_TP_Ed(TPDtViagem.Date, EdDtViagem.Text);
  DtEntrega      := Geral.FDT_TP_Ed(TPDtEntrega.Date, EdDtEntrega.Text);
  Cliente        := EdCliente.ValueVariant;
  Transporta     := EdTransporta.ValueVariant;
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(TPDtVenda.DateTime < 2, TPDtVenda, 'Defina uma data de Venda!') then Exit;
  DataChegadaInvalida := TPDtEntrega.Date < TPDtViagem.Date;
  if MyObjects.FIC(DataChegadaInvalida, TPDtViagem, 'Defina uma data de viagem v�lida!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('wboutcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('wbmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'wboutcab', False, [
  'MovimCod', 'Empresa', 'DtVenda',
  'DtViagem', 'DtEntrega', 'Cliente',
  'Transporta'(*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'*)], [
  'Codigo'], [
  MovimCod, Empresa, DtVenda,
  DtViagem, DtEntrega, Cliente,
  Transporta(* (*Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*)], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      Dmod.InsereWBMovCab(MovimCod, emidVenda, Codigo)
    else
    begin
      DataHora := DtVenda;
      CliVenda := Cliente;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'wbmovits', False, [
      'Empresa', 'CliVenda', 'DataHora'], ['MovimCod'], [
      Empresa, CliVenda, DataHora], [MovimCod], True);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmWBOutCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'wboutcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'wboutcab', 'Codigo');
end;

procedure TFmWBOutCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmWBOutCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmWBOutCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
end;

procedure TFmWBOutCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWBOutCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWBOutCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWBOutCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrWBOutCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWBOutCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWBOutCab.QrWBoutCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWBOutCab.QrWBoutCabAfterScroll(DataSet: TDataSet);
begin
  ReopenWBOutIts(0);
end;

procedure TFmWBOutCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrWBOutCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmWBOutCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWBOutCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wboutcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmWBOutCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBOutCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWBOutCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'wboutcab');
end;

procedure TFmWBOutCab.QrWBoutCabBeforeClose(
  DataSet: TDataSet);
begin
  QrWBOutIts.Close;
end;

procedure TFmWBOutCab.QrWBoutCabBeforeOpen(DataSet: TDataSet);
begin
  QrWBOutCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

