object FmPQEPed2: TFmPQEPed2
  Left = 366
  Top = 203
  Caption = 'QUI-ENTRA-008 :: Entrada de PQ por Pedido'
  ClientHeight = 467
  ClientWidth = 950
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 950
    Height = 300
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 950
      Height = 300
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Pesquisa '
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 942
          Height = 272
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitHeight = 283
          object PainelDados: TPanel
            Left = 0
            Top = 0
            Width = 942
            Height = 49
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label1: TLabel
              Left = 12
              Top = 4
              Width = 40
              Height = 13
              Caption = 'Numero:'
            end
            object Label2: TLabel
              Left = 104
              Top = 4
              Width = 55
              Height = 13
              Caption = 'Refer'#234'ncia:'
            end
            object SpeedButton1: TSpeedButton
              Left = 70
              Top = 18
              Width = 25
              Height = 25
              Caption = '>'
              OnClick = SpeedButton1Click
            end
            object SpeedButton2: TSpeedButton
              Left = 428
              Top = 18
              Width = 25
              Height = 25
              Caption = '>'
              OnClick = SpeedButton2Click
            end
            object EdCodUsu: TdmkEdit
              Left = 12
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdReferencia: TdmkEdit
              Left = 104
              Top = 20
              Width = 321
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '%%'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = '%%'
              ValWarn = False
            end
          end
          object DBGrid1: TDBGrid
            Left = 0
            Top = 49
            Width = 733
            Height = 223
            Align = alLeft
            DataSource = DsPediVda
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid1DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ReferenPedi'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaEntra'
                Title.Caption = 'Dta cadastro'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaPrevi'
                Title.Caption = 'Dta previs'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Cliente'
                Title.Caption = 'Fornecedor'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ENT'
                Title.Caption = 'Nome do Fornecedor'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantP'
                Title.Caption = 'Itens'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValLiq'
                Title.Caption = 'Val. Liq.'
                Visible = True
              end>
          end
          object DBMemo1: TDBMemo
            Left = 733
            Top = 49
            Width = 209
            Height = 223
            Align = alClient
            DataField = 'Observa'
            DataSource = DsPediVda
            TabOrder = 2
            ExplicitHeight = 234
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Verifica e confirma'
        ImageIndex = 1
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 942
          Height = 272
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGGru: TDBGrid
            Left = 0
            Top = 0
            Width = 942
            Height = 272
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 403
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SaldoQtd'
                Title.Caption = 'Saldo pedido'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantP'
                Title.Caption = 'Qtd pedido'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrecoF'
                Title.Caption = 'Pre'#231'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValBru'
                Title.Caption = 'Val. Bru.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValLiq'
                Title.Caption = 'Val. liq.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsPediVdaGru
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 403
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SaldoQtd'
                Title.Caption = 'Saldo pedido'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantP'
                Title.Caption = 'Qtd pedido'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrecoF'
                Title.Caption = 'Pre'#231'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValBru'
                Title.Caption = 'Val. Bru.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValLiq'
                Title.Caption = 'Val. liq.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 950
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 902
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 854
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 317
        Height = 32
        Caption = 'Entrada de PQ por Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 317
        Height = 32
        Caption = 'Entrada de PQ por Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 317
        Height = 32
        Caption = 'Entrada de PQ por Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 348
    Width = 950
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 946
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 946
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 403
    Width = 950
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 946
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 802
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
      object BtAtzSdos: TBitBtn
        Tag = 18
        Left = 136
        Top = 5
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Atz Saldos'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAtzSdosClick
      end
    end
  end
  object QrFornece: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT fo.Codigo,'
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOMEENTIDADE'
      'FROM entidades fo, PQPed pp'
      'WHERE pp.Fornece=fo.Codigo'
      'AND pp.Sit <:P0'
      'AND fo.Fornece2='#39'V'#39
      'ORDER BY NOMEENTIDADE')
    Left = 136
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      FixedChar = True
      Size = 128
    end
  end
  object QrPedidos: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPedidosAfterScroll
    SQL.Strings = (
      'SELECT Codigo'
      'FROM pqped'
      'WHERE Fornece=:P0'
      'AND Sit<:P1'
      '')
    Left = 236
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPedidosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pqped.Codigo'
      DisplayFormat = '000'
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 168
    Top = 4
  end
  object DsPedidos: TDataSource
    DataSet = QrPedidos
    Left = 264
  end
  object QrPediVda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvd.Codigo, pvd.CodUsu, pvd.Cliente, '
      'pvd.DtaEntra, pvd.DtaPrevi, pvd.Observa, '
      'pvd.QuantP, pvd.ValLiq, pvd.ReferenPedi,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT '
      'FROM pedivda pvd'
      'LEFT JOIN entidades ent ON ent.Codigo=pvd.Cliente'
      'WHERE pvd.EntSai=0'
      'AND pvd.CodUsu=2'
      'OR ReferenPedi LIKE "%74%"')
    Left = 292
    Top = 208
    object QrPediVdaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediVdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPediVdaCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPediVdaDtaEntra: TDateField
      FieldName = 'DtaEntra'
      Required = True
    end
    object QrPediVdaDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
      Required = True
    end
    object QrPediVdaObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPediVdaQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrPediVdaValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrPediVdaReferenPedi: TWideStringField
      FieldName = 'ReferenPedi'
      Size = 60
    end
    object QrPediVdaNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsPediVda: TDataSource
    DataSet = QrPediVda
    Left = 292
    Top = 260
  end
  object QrPediVdaGru: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPediVdaGruAfterOpen
    BeforeClose = QrPediVdaGruBeforeClose
    SQL.Strings = (
      'SELECT pvi.Controle, SUM(QuantP) QuantP, SUM(ValLiq) ValLiq, '
      'SUM(Customizad) ItensCustomizados,'
      'gti.Codigo GRATAMCAD,'
      'gg1.CodUsu, gg1.Nome, gg1.Nivel1, pgt.Fracio'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pvi.Codigo=:P0'
      'GROUP BY gg1.Nivel1'
      'ORDER BY pci.Controle')
    Left = 392
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaGruCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediVdaGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPediVdaGruNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPediVdaGruGRATAMCAD: TIntegerField
      FieldName = 'GRATAMCAD'
    end
    object QrPediVdaGruQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrPediVdaGruValLiq: TFloatField
      FieldName = 'ValLiq'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPediVdaGruItensCustomizados: TFloatField
      FieldName = 'ItensCustomizados'
    end
    object QrPediVdaGruFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrPediVdaGruControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPediVdaGruValBru: TFloatField
      FieldName = 'ValBru'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPediVdaGruPrecoF: TFloatField
      FieldName = 'PrecoF'
      DisplayFormat = '#,###,###,##0.000000'
    end
    object QrPediVdaGruSaldoQtd: TFloatField
      FieldName = 'SaldoQtd'
    end
  end
  object DsPediVdaGru: TDataSource
    DataSet = QrPediVdaGru
    Left = 392
    Top = 260
  end
  object QrPediVdaIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pedivdaits')
    Left = 492
    Top = 205
    object QrPediVdaItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediVdaItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediVdaItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrPediVdaItsPrecoO: TFloatField
      FieldName = 'PrecoO'
      Required = True
    end
    object QrPediVdaItsPrecoR: TFloatField
      FieldName = 'PrecoR'
      Required = True
    end
    object QrPediVdaItsQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrPediVdaItsQuantC: TFloatField
      FieldName = 'QuantC'
      Required = True
    end
    object QrPediVdaItsQuantV: TFloatField
      FieldName = 'QuantV'
      Required = True
    end
    object QrPediVdaItsValBru: TFloatField
      FieldName = 'ValBru'
      Required = True
    end
    object QrPediVdaItsDescoP: TFloatField
      FieldName = 'DescoP'
      Required = True
    end
    object QrPediVdaItsDescoV: TFloatField
      FieldName = 'DescoV'
      Required = True
    end
    object QrPediVdaItsValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrPediVdaItsPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrPediVdaItsMedidaC: TFloatField
      FieldName = 'MedidaC'
      Required = True
    end
    object QrPediVdaItsMedidaL: TFloatField
      FieldName = 'MedidaL'
      Required = True
    end
    object QrPediVdaItsMedidaA: TFloatField
      FieldName = 'MedidaA'
      Required = True
    end
    object QrPediVdaItsMedidaE: TFloatField
      FieldName = 'MedidaE'
      Required = True
    end
    object QrPediVdaItsPercCustom: TFloatField
      FieldName = 'PercCustom'
      Required = True
    end
    object QrPediVdaItsCustomizad: TSmallintField
      FieldName = 'Customizad'
      Required = True
    end
    object QrPediVdaItsInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrPediVdaItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrPediVdaItsReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 11
    end
    object QrPediVdaItsMulti: TIntegerField
      FieldName = 'Multi'
      Required = True
    end
    object QrPediVdaItsvProd: TFloatField
      FieldName = 'vProd'
      Required = True
    end
    object QrPediVdaItsvFrete: TFloatField
      FieldName = 'vFrete'
      Required = True
    end
    object QrPediVdaItsvSeg: TFloatField
      FieldName = 'vSeg'
      Required = True
    end
    object QrPediVdaItsvOutro: TFloatField
      FieldName = 'vOutro'
      Required = True
    end
    object QrPediVdaItsvDesc: TFloatField
      FieldName = 'vDesc'
      Required = True
    end
    object QrPediVdaItsvBC: TFloatField
      FieldName = 'vBC'
      Required = True
    end
  end
end
