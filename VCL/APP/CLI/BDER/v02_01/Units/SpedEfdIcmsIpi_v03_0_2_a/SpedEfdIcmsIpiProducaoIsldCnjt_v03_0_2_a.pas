unit SpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  mySQLDbTables, UMySQLDB,
  //
  StdCtrls, ExtCtrls, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums,
  UnMsgInt,(* DbTables,*) DmkCoding, dmkEdit, dmkRadioGroup, dmkMemo,
  dmkCheckGroup, UnMyObjects, MyDBCheck, UnDmkProcFunc, UnProjGroup_Consts,
  AppListas, dmkLabel, dmkImage, DBCtrls, dmkDBLookupComboBox, dmkEditCB,
  dmkEditDateTimePicker, dmkDBGridZTO, SPED_Listas, TypInfo, UnEfdIcmsIpi_PF,
  mySQLDirectQuery, UnAppEnums;

type
  TOpenCliForSite = (ocfsIndef=0, ocfsControle=1, ocfsMovIdNivCod=2, ocfsSrcNivel2=3);
  TFormaInsercaoItemProducao = (fipIndef=0, fipIsolada=1, fipConjunta=2);
  TInsercaoItemProducaoConjunta = (iipcIndef=0, iipcSimples=1,
    iipcGemeos=2, iipcIndireta=3, iipcUniAnt=4, iipcUniGer=5, iipcDuplicado=6);
  //
  TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a = class(TForm)
    QrDatas: TmySQLQuery;
    QrDatasMinDH: TDateTimeField;
    QrDatasMaxDH: TDateTimeField;
    QrOri: TmySQLQuery;
    QrMae: TmySQLQuery;
    QrMC: TmySQLQuery;
    QrMCMovimCod: TIntegerField;
    QrProdRib: TmySQLQuery;
    QrProdRibData: TDateField;
    QrProdRibGGX_Dst: TIntegerField;
    QrProdRibGGX_Src: TIntegerField;
    QrProdRibPecas: TFloatField;
    QrProdRibAreaM2: TFloatField;
    QrProdRibPesoKg: TFloatField;
    QrProdRibMovimID: TIntegerField;
    QrProdRibCodigo: TIntegerField;
    QrProdRibMovimCod: TIntegerField;
    QrProdRibControle: TIntegerField;
    QrProdRibEmpresa: TIntegerField;
    QrProdRibQtdAntPeca: TFloatField;
    QrProdRibQtdAntArM2: TFloatField;
    QrProdRibQtdAntPeso: TFloatField;
    QrProdRibsTipoEstq: TFloatField;
    QrProdRibdTipoEstq: TFloatField;
    QrProdRibMovimNiv: TIntegerField;
    QrProdRibDtCorrApo: TDateTimeField;
    QrProdRibClientMO: TIntegerField;
    QrProdRibFornecMO: TIntegerField;
    QrProdRibEntiSitio: TIntegerField;
    QrObtCliForSite: TmySQLQuery;
    QrIts: TmySQLQuery;
    QrCalToCur: TmySQLQuery;
    QrConsParc: TmySQLQuery;
    QrConsParcPecas: TFloatField;
    QrConsParcAreaM2: TFloatField;
    QrConsParcPesoKg: TFloatField;
    QrConsParcAno: TFloatField;
    QrConsParcMes: TFloatField;
    QrProdParc: TmySQLQuery;
    QrProdParcPecas: TFloatField;
    QrProdParcAreaM2: TFloatField;
    QrProdParcPesoKg: TFloatField;
    QrProdParcClientMO: TIntegerField;
    QrProdParcFornecMO: TIntegerField;
    QrProdParcControle: TIntegerField;
    QrProdParcAno: TFloatField;
    QrProdParcMes: TFloatField;
    QrPeriodos: TmySQLQuery;
    QrPeriodosAno: TFloatField;
    QrPeriodosMes: TFloatField;
    QrPeriodosPeriodo: TIntegerField;
    QrVmiGArCab: TmySQLQuery;
    QrVmiGArCabData: TDateField;
    QrVmiGArCabCodigo: TIntegerField;
    QrVmiGArCabMovimCod: TIntegerField;
    QrVmiGArCabGraGruX: TIntegerField;
    QrVmiGArCabPecas: TFloatField;
    QrVmiGArCabAreaM2: TFloatField;
    QrVmiGArCabPesoKg: TFloatField;
    QrVmiGArCabMovimTwn: TIntegerField;
    QrVmiGArCabUnidMed: TIntegerField;
    QrVmiGArCabmed_grandeza: TIntegerField;
    QrVmiGArCabxco_grandeza: TIntegerField;
    QrVmiGArCabGraGruY: TIntegerField;
    QrVmiGArCabCouNiv2: TIntegerField;
    QrVmiGArCabControle: TIntegerField;
    QrVmiGArCabTipoEstq: TFloatField;
    QrVmiGArCabDtCorrApo: TDateTimeField;
    QrVmiGArCabClientMO: TIntegerField;
    QrVmiGArCabFornecMO: TIntegerField;
    QrVmiGArCabEntiSitio: TIntegerField;
    QrSumGAR: TmySQLQuery;
    QrSumGARPecas: TFloatField;
    QrSumGARAreaM2: TFloatField;
    QrSumGARPesoKg: TFloatField;
    QrVmiGArIts: TmySQLQuery;
    QrVmiGArItsData: TDateField;
    QrVmiGArItsCodigo: TIntegerField;
    QrVmiGArItsMovimCod: TIntegerField;
    QrVmiGArItsGraGruX: TIntegerField;
    QrVmiGArItsPecas: TFloatField;
    QrVmiGArItsAreaM2: TFloatField;
    QrVmiGArItsPesoKg: TFloatField;
    QrVmiGArItsMovimTwn: TIntegerField;
    QrVmiGArItsUnidMed: TIntegerField;
    QrVmiGArItsmed_grandeza: TIntegerField;
    QrVmiGArItsxco_grandeza: TIntegerField;
    QrVmiGArItsGraGruY: TIntegerField;
    QrVmiGArItsCouNiv2: TIntegerField;
    QrVmiGArItsTipoEstq: TLargeintField;
    QrVmiGArItsControle: TIntegerField;
    QrVmiGArItsDstGGX: TIntegerField;
    QrVmiGArItsSrcGGX: TIntegerField;
    QrVmiGArItsSrcNivel2: TIntegerField;
    QrVmiGArItsSrcMovID: TIntegerField;
    QrVmiGArItsDtCorrApo: TDateTimeField;
    QrIMECs_: TmySQLQuery;
    QrIMECs_Codigo: TIntegerField;
    QrIMECs_MovimCod: TIntegerField;
    QrIMECs_MovimID: TIntegerField;
    QrIMECs_AnoCA: TIntegerField;
    QrIMECs_MesCA: TIntegerField;
    QrIMECs_TipoTab: TWideStringField;
    QrIMECs_Ativo: TSmallintField;
    QrVSRibAtu_: TmySQLQuery;
    QrVSRibAtu_Codigo: TLargeintField;
    QrVSRibAtu_Controle: TLargeintField;
    QrVSRibAtu_MovimCod: TLargeintField;
    QrVSRibAtu_MovimNiv: TLargeintField;
    QrVSRibAtu_MovimTwn: TLargeintField;
    QrVSRibAtu_Empresa: TLargeintField;
    QrVSRibAtu_Terceiro: TLargeintField;
    QrVSRibAtu_CliVenda: TLargeintField;
    QrVSRibAtu_MovimID: TLargeintField;
    QrVSRibAtu_DataHora: TDateTimeField;
    QrVSRibAtu_Pallet: TLargeintField;
    QrVSRibAtu_GraGruX: TLargeintField;
    QrVSRibAtu_Pecas: TFloatField;
    QrVSRibAtu_PesoKg: TFloatField;
    QrVSRibAtu_AreaM2: TFloatField;
    QrVSRibAtu_AreaP2: TFloatField;
    QrVSRibAtu_ValorT: TFloatField;
    QrVSRibAtu_SrcMovID: TLargeintField;
    QrVSRibAtu_SrcNivel1: TLargeintField;
    QrVSRibAtu_SrcNivel2: TLargeintField;
    QrVSRibAtu_SrcGGX: TLargeintField;
    QrVSRibAtu_SdoVrtPeca: TFloatField;
    QrVSRibAtu_SdoVrtPeso: TFloatField;
    QrVSRibAtu_SdoVrtArM2: TFloatField;
    QrVSRibAtu_Observ: TWideStringField;
    QrVSRibAtu_SerieFch: TLargeintField;
    QrVSRibAtu_Ficha: TLargeintField;
    QrVSRibAtu_Misturou: TLargeintField;
    QrVSRibAtu_FornecMO: TLargeintField;
    QrVSRibAtu_CustoMOKg: TFloatField;
    QrVSRibAtu_CustoMOM2: TFloatField;
    QrVSRibAtu_CustoMOTot: TFloatField;
    QrVSRibAtu_ValorMP: TFloatField;
    QrVSRibAtu_DstMovID: TLargeintField;
    QrVSRibAtu_DstNivel1: TLargeintField;
    QrVSRibAtu_DstNivel2: TLargeintField;
    QrVSRibAtu_DstGGX: TLargeintField;
    QrVSRibAtu_QtdGerPeca: TFloatField;
    QrVSRibAtu_QtdGerPeso: TFloatField;
    QrVSRibAtu_QtdGerArM2: TFloatField;
    QrVSRibAtu_QtdGerArP2: TFloatField;
    QrVSRibAtu_QtdAntPeca: TFloatField;
    QrVSRibAtu_QtdAntPeso: TFloatField;
    QrVSRibAtu_QtdAntArM2: TFloatField;
    QrVSRibAtu_QtdAntArP2: TFloatField;
    QrVSRibAtu_NotaMPAG: TFloatField;
    QrVSRibAtu_NO_PALLET: TWideStringField;
    QrVSRibAtu_NO_PRD_TAM_COR: TWideStringField;
    QrVSRibAtu_NO_TTW: TWideStringField;
    QrVSRibAtu_ID_TTW: TLargeintField;
    QrVSRibAtu_NO_FORNECE: TWideStringField;
    QrVSRibAtu_ReqMovEstq: TLargeintField;
    QrVSRibAtu_CUSTO_M2: TFloatField;
    QrVSRibAtu_CUSTO_P2: TFloatField;
    QrVSRibAtu_NO_LOC_CEN: TWideStringField;
    QrVSRibAtu_Marca: TWideStringField;
    QrVSRibAtu_PedItsLib: TLargeintField;
    QrVSRibAtu_StqCenLoc: TLargeintField;
    QrVSRibAtu_NO_FICHA: TWideStringField;
    QrVSRibAtu_NO_FORNEC_MO: TWideStringField;
    QrVSRibAtu_ClientMO: TLargeintField;
    QrVSRibAtu_CUSTO_KG: TFloatField;
    QrVSRibAtu_CustoPQ: TFloatField;
    QrVSRibDst_: TmySQLQuery;
    QrVSRibDst_Codigo: TLargeintField;
    QrVSRibDst_Controle: TLargeintField;
    QrVSRibDst_MovimCod: TLargeintField;
    QrVSRibDst_MovimNiv: TLargeintField;
    QrVSRibDst_MovimTwn: TLargeintField;
    QrVSRibDst_Empresa: TLargeintField;
    QrVSRibDst_Terceiro: TLargeintField;
    QrVSRibDst_CliVenda: TLargeintField;
    QrVSRibDst_MovimID: TLargeintField;
    QrVSRibDst_DataHora: TDateTimeField;
    QrVSRibDst_Pallet: TLargeintField;
    QrVSRibDst_GraGruX: TLargeintField;
    QrVSRibDst_Pecas: TFloatField;
    QrVSRibDst_PesoKg: TFloatField;
    QrVSRibDst_AreaM2: TFloatField;
    QrVSRibDst_AreaP2: TFloatField;
    QrVSRibDst_ValorT: TFloatField;
    QrVSRibDst_SrcMovID: TLargeintField;
    QrVSRibDst_SrcNivel1: TLargeintField;
    QrVSRibDst_SrcNivel2: TLargeintField;
    QrVSRibDst_SrcGGX: TLargeintField;
    QrVSRibDst_SdoVrtPeca: TFloatField;
    QrVSRibDst_SdoVrtPeso: TFloatField;
    QrVSRibDst_SdoVrtArM2: TFloatField;
    QrVSRibDst_Observ: TWideStringField;
    QrVSRibDst_SerieFch: TLargeintField;
    QrVSRibDst_Ficha: TLargeintField;
    QrVSRibDst_Misturou: TLargeintField;
    QrVSRibDst_FornecMO: TLargeintField;
    QrVSRibDst_CustoMOKg: TFloatField;
    QrVSRibDst_CustoMOM2: TFloatField;
    QrVSRibDst_CustoMOTot: TFloatField;
    QrVSRibDst_ValorMP: TFloatField;
    QrVSRibDst_DstMovID: TLargeintField;
    QrVSRibDst_DstNivel1: TLargeintField;
    QrVSRibDst_DstNivel2: TLargeintField;
    QrVSRibDst_DstGGX: TLargeintField;
    QrVSRibDst_QtdGerPeca: TFloatField;
    QrVSRibDst_QtdGerPeso: TFloatField;
    QrVSRibDst_QtdGerArM2: TFloatField;
    QrVSRibDst_QtdGerArP2: TFloatField;
    QrVSRibDst_QtdAntPeca: TFloatField;
    QrVSRibDst_QtdAntPeso: TFloatField;
    QrVSRibDst_QtdAntArM2: TFloatField;
    QrVSRibDst_QtdAntArP2: TFloatField;
    QrVSRibDst_NotaMPAG: TFloatField;
    QrVSRibDst_NO_PALLET: TWideStringField;
    QrVSRibDst_NO_PRD_TAM_COR: TWideStringField;
    QrVSRibDst_NO_TTW: TWideStringField;
    QrVSRibDst_ID_TTW: TLargeintField;
    QrVSRibDst_NO_FORNECE: TWideStringField;
    QrVSRibDst_NO_SerieFch: TWideStringField;
    QrVSRibDst_ReqMovEstq: TLargeintField;
    QrVSRibDst_PedItsFin: TLargeintField;
    QrVSRibDst_Marca: TWideStringField;
    QrVSRibDst_StqCenLoc: TLargeintField;
    QrVSRibDst_NO_MovimNiv: TWideStringField;
    QrVSRibDst_NO_MovimID: TWideStringField;
    QrProc_: TmySQLQuery;
    QrProc_DtHrAberto: TDateTimeField;
    QrProc_DtHrFimOpe: TDateTimeField;
    QrVSRibDst_DtCorrApo: TDateTimeField;
    QrVmiTwn: TmySQLQuery;
    QrIMECs_JmpMovID: TIntegerField;
    QrIMECs_JmpNivel1: TIntegerField;
    QrIMECs_JmpNivel2: TIntegerField;
    QrJmpA_: TmySQLQuery;
    QrJmpA_SrcNivel2: TIntegerField;
    QrJmpA_MovimID: TIntegerField;
    QrJmpA_Codigo: TIntegerField;
    QrJmpA_MovimCod: TIntegerField;
    QrJmpA_Controle: TIntegerField;
    QrJmpB_: TmySQLQuery;
    QrJmpB_MovimID: TIntegerField;
    QrJmpB_Codigo: TIntegerField;
    QrJmpB_MovimCod: TIntegerField;
    QrJmpB_Controle: TIntegerField;
    QrVSRibInd_: TmySQLQuery;
    QrVSRibInd_Codigo: TLargeintField;
    QrVSRibInd_Controle: TLargeintField;
    QrVSRibInd_MovimCod: TLargeintField;
    QrVSRibInd_MovimNiv: TLargeintField;
    QrVSRibInd_MovimTwn: TLargeintField;
    QrVSRibInd_Empresa: TLargeintField;
    QrVSRibInd_Terceiro: TLargeintField;
    QrVSRibInd_CliVenda: TLargeintField;
    QrVSRibInd_MovimID: TLargeintField;
    QrVSRibInd_DataHora: TDateTimeField;
    QrVSRibInd_Pallet: TLargeintField;
    QrVSRibInd_GraGruX: TLargeintField;
    QrVSRibInd_Pecas: TFloatField;
    QrVSRibInd_PesoKg: TFloatField;
    QrVSRibInd_AreaM2: TFloatField;
    QrVSRibInd_AreaP2: TFloatField;
    QrVSRibInd_ValorT: TFloatField;
    QrVSRibInd_SrcMovID: TLargeintField;
    QrVSRibInd_SrcNivel1: TLargeintField;
    QrVSRibInd_SrcNivel2: TLargeintField;
    QrVSRibInd_SrcGGX: TLargeintField;
    QrVSRibInd_SdoVrtPeca: TFloatField;
    QrVSRibInd_SdoVrtPeso: TFloatField;
    QrVSRibInd_SdoVrtArM2: TFloatField;
    QrVSRibInd_Observ: TWideStringField;
    QrVSRibInd_SerieFch: TLargeintField;
    QrVSRibInd_Ficha: TLargeintField;
    QrVSRibInd_Misturou: TLargeintField;
    QrVSRibInd_FornecMO: TLargeintField;
    QrVSRibInd_CustoMOKg: TFloatField;
    QrVSRibInd_CustoMOM2: TFloatField;
    QrVSRibInd_CustoMOTot: TFloatField;
    QrVSRibInd_ValorMP: TFloatField;
    QrVSRibInd_DstMovID: TLargeintField;
    QrVSRibInd_DstNivel1: TLargeintField;
    QrVSRibInd_DstNivel2: TLargeintField;
    QrVSRibInd_DstGGX: TLargeintField;
    QrVSRibInd_QtdGerPeca: TFloatField;
    QrVSRibInd_QtdGerPeso: TFloatField;
    QrVSRibInd_QtdGerArM2: TFloatField;
    QrVSRibInd_QtdGerArP2: TFloatField;
    QrVSRibInd_QtdAntPeca: TFloatField;
    QrVSRibInd_QtdAntPeso: TFloatField;
    QrVSRibInd_QtdAntArM2: TFloatField;
    QrVSRibInd_QtdAntArP2: TFloatField;
    QrVSRibInd_NotaMPAG: TFloatField;
    QrVSRibInd_NO_PALLET: TWideStringField;
    QrVSRibInd_NO_PRD_TAM_COR: TWideStringField;
    QrVSRibInd_NO_TTW: TWideStringField;
    QrVSRibInd_ID_TTW: TLargeintField;
    QrVSRibInd_NO_FORNECE: TWideStringField;
    QrVSRibInd_NO_SerieFch: TWideStringField;
    QrVSRibInd_ReqMovEstq: TLargeintField;
    QrVSRibInd_PedItsFin: TLargeintField;
    QrVSRibInd_Marca: TWideStringField;
    QrVSRibInd_StqCenLoc: TLargeintField;
    QrVSRibInd_NO_MovimNiv: TWideStringField;
    QrVSRibInd_NO_MovimID: TWideStringField;
    QrVSRibInd_DtCorrApo: TDateTimeField;
    QrVSRibInd_JmpMovID: TLargeintField;
    QrVSRibInd_JmpNivel1: TLargeintField;
    QrVSRibInd_JmpNivel2: TLargeintField;
    QrVSRibInd_JmpGGX: TLargeintField;
    QrFather_: TmySQLQuery;
    QrFather_MovCodPai: TIntegerField;
    QrIMECs_MovCodPai: TIntegerField;
    QrVSRibOriIMEI_: TmySQLQuery;
    QrVSRibOriIMEI_Codigo: TLargeintField;
    QrVSRibOriIMEI_Controle: TLargeintField;
    QrVSRibOriIMEI_MovimCod: TLargeintField;
    QrVSRibOriIMEI_MovimNiv: TLargeintField;
    QrVSRibOriIMEI_MovimTwn: TLargeintField;
    QrVSRibOriIMEI_Empresa: TLargeintField;
    QrVSRibOriIMEI_Terceiro: TLargeintField;
    QrVSRibOriIMEI_CliVenda: TLargeintField;
    QrVSRibOriIMEI_MovimID: TLargeintField;
    QrVSRibOriIMEI_DataHora: TDateTimeField;
    QrVSRibOriIMEI_Pallet: TLargeintField;
    QrVSRibOriIMEI_GraGruX: TLargeintField;
    QrVSRibOriIMEI_Pecas: TFloatField;
    QrVSRibOriIMEI_PesoKg: TFloatField;
    QrVSRibOriIMEI_AreaM2: TFloatField;
    QrVSRibOriIMEI_AreaP2: TFloatField;
    QrVSRibOriIMEI_ValorT: TFloatField;
    QrVSRibOriIMEI_SrcMovID: TLargeintField;
    QrVSRibOriIMEI_SrcNivel1: TLargeintField;
    QrVSRibOriIMEI_SrcNivel2: TLargeintField;
    QrVSRibOriIMEI_SrcGGX: TLargeintField;
    QrVSRibOriIMEI_SdoVrtPeca: TFloatField;
    QrVSRibOriIMEI_SdoVrtPeso: TFloatField;
    QrVSRibOriIMEI_SdoVrtArM2: TFloatField;
    QrVSRibOriIMEI_Observ: TWideStringField;
    QrVSRibOriIMEI_SerieFch: TLargeintField;
    QrVSRibOriIMEI_Ficha: TLargeintField;
    QrVSRibOriIMEI_Misturou: TLargeintField;
    QrVSRibOriIMEI_FornecMO: TLargeintField;
    QrVSRibOriIMEI_CustoMOKg: TFloatField;
    QrVSRibOriIMEI_CustoMOM2: TFloatField;
    QrVSRibOriIMEI_CustoMOTot: TFloatField;
    QrVSRibOriIMEI_ValorMP: TFloatField;
    QrVSRibOriIMEI_DstMovID: TLargeintField;
    QrVSRibOriIMEI_DstNivel1: TLargeintField;
    QrVSRibOriIMEI_DstNivel2: TLargeintField;
    QrVSRibOriIMEI_DstGGX: TLargeintField;
    QrVSRibOriIMEI_QtdGerPeca: TFloatField;
    QrVSRibOriIMEI_QtdGerPeso: TFloatField;
    QrVSRibOriIMEI_QtdGerArM2: TFloatField;
    QrVSRibOriIMEI_QtdGerArP2: TFloatField;
    QrVSRibOriIMEI_QtdAntPeca: TFloatField;
    QrVSRibOriIMEI_QtdAntPeso: TFloatField;
    QrVSRibOriIMEI_QtdAntArM2: TFloatField;
    QrVSRibOriIMEI_QtdAntArP2: TFloatField;
    QrVSRibOriIMEI_NotaMPAG: TFloatField;
    QrVSRibOriIMEI_NO_PALLET: TWideStringField;
    QrVSRibOriIMEI_NO_PRD_TAM_COR: TWideStringField;
    QrVSRibOriIMEI_NO_TTW: TWideStringField;
    QrVSRibOriIMEI_ID_TTW: TLargeintField;
    QrVSRibOriIMEI_NO_FORNECE: TWideStringField;
    QrVSRibOriIMEI_NO_SerieFch: TWideStringField;
    QrVSRibOriIMEI_ReqMovEstq: TLargeintField;
    QrVSRibOriIMEI_CustoPQ: TFloatField;
    QrVSRibOriIMEI_VSMulFrnCab: TLargeintField;
    QrVSRibOriIMEI_ClientMO: TLargeintField;
    QrVSRibOriIMEI_RmsMovID: TLargeintField;
    QrVSRibOriIMEI_RmsNivel1: TLargeintField;
    QrVSRibOriIMEI_RmsNivel2: TLargeintField;
    QrVSRibOriIMEI_DtCorrApo: TDateTimeField;
    QrVSRibOriIMEI_RmsGGX: TLargeintField;
    QrVSRibInd_RmsGGX: TLargeintField;
    QrCab34: TmySQLQuery;
    QrCab34MovimCod: TIntegerField;
    QrCalToCurGraGruX: TIntegerField;
    QrVmiGArItsJmpGGX: TIntegerField;
    QrVmiGArCabQtdGerArM2: TFloatField;
    QrPQxMixOP: TmySQLQuery;
    QrPQxMixOPOrigemCodi: TIntegerField;
    QrPQxMixOPCliDest: TIntegerField;
    QrPQMCab: TmySQLQuery;
    QrPQMBxa: TmySQLQuery;
    QrPQMBxaDataX: TDateField;
    QrPQMBxaOrigemCtrl: TIntegerField;
    QrPQMBxaInsumo: TIntegerField;
    QrPQMBxaPeso: TFloatField;
    QrPQMGer: TmySQLQuery;
    QrPQxMixOPDtCorrApo: TDateField;
    QrPQMCabDataB: TDateField;
    QrPQMCabVSMovCod: TIntegerField;
    QrPQMGerDataX: TDateField;
    QrPQMGerOrigemCtrl: TIntegerField;
    QrPQMGerInsumo: TIntegerField;
    QrPQMGerPeso: TFloatField;
    Qry: TmySQLQuery;
    QrVSRibAtu_DtCorrApo: TDateTimeField;
    DqFather: TmySQLDirectQuery;
    DqIMECs: TmySQLDirectQuery;
    DqProc: TmySQLDirectQuery;
    DqVSRibAtu: TmySQLDirectQuery;
    DqVSRibOriIMEI: TmySQLDirectQuery;
    DqJmpA: TmySQLDirectQuery;
    DqJmpB: TmySQLDirectQuery;
    DqVSRibDst: TmySQLDirectQuery;
    DqVSRibInd: TmySQLDirectQuery;
    QrMinData: TmySQLQuery;
    DqOri: TmySQLDirectQuery;
    DqIts: TmySQLDirectQuery;
    DqVmiGArCab: TmySQLDirectQuery;
    DqVmiGArIts: TmySQLDirectQuery;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FSEII_IMEcs: String;
    F_BNCI_IDSeq1_K210, F_BNCI_IDSeq2_K215,
    F_BNCI_IDSeq1_K230, F_BNCI_IDSeq2_K235,
    F_BNCI_IDSeq1_K250, F_BNCI_IDSeq2_K255,
    F_BNCI_IDSeq1_K290, F_BNCI_IDSeq2_K291, F_BNCI_IDSeq2_K292,
    F_BNCI_IDSeq1_K300, F_BNCI_IDSeq2_K301, F_BNCI_IDSeq2_K302: Integer;
    //
    function  ImportaOpeProcB_Isolada(QrCab: TMySQLQuery; PsqMovimNiv:
              TEstqMovimNiv; SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ,
              SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ImportaOpeProcB_Conjunta(QrCab: TMySQLQuery; MovimID,
              PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; MovimNiv,
              PsqMovimNiv_VMI: TEstqMovimNiv; SQL_PeriodoComplexo,
              SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ReopenVmiTwn(MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv;
              MovimCod, MovimTwn: Integer): Boolean;
              // GAr
    function  FiltroRegGAR(TipoRegSPEDProd: TTipoRegSPEDProd): String;
    function  FiltroPeriodoGAR(TipoRegSPEDProd: TTipoRegSPEDProd; SQL_Periodo:
              String): String;
    function  GeraGAR_Fast(SQL_Periodo: String): Boolean;
    function  GeraGAR(SQL_Periodo: String): Boolean;
    procedure ReopenObtCliForSite(Forma: TOpenCliForSite; Controle, MovimCod,
              MovimNiv: Integer);
    procedure ReopenVmiGArCab_Fast(TipoRegSPEDProd: TTipoRegSPEDProd;
              SQL_Periodo: String);
    procedure ReopenVmiGArCab(TipoRegSPEDProd: TTipoRegSPEDProd;
              SQL_Periodo: String);
    procedure ReopenVmiGArIts_Fast(MovimCod, MovimTwn: Integer; SQL_Periodo: String;
              TipoRegSPEDProd: TTipoRegSPEDProd);
    procedure ReopenVmiGArIts(MovimCod, MovimTwn: Integer; SQL_Periodo: String;
              TipoRegSPEDProd: TTipoRegSPEDProd);
              // Fim GAr
    procedure VerificaTipoEstqQtde(TipoEstq: Integer; AreaM2, PesoKg, Pecas:
              Double; GraGruX, MovimCod, Controle: Integer; ProcName: String);
  public
    { Public declarations }
    FSQL_PeriodoPQ: String;
    FImporExpor, FEmpresa, FAnoMes, FPeriApu, (*FSPED_EFD_Producao*)
    FVersaoIsldCnjt: Integer;
    FDiaIni, FDiaFim, FDiaPos(*, FData*): TDateTime;
    FTipoPeriodoFiscal: TTipoPeriodoFiscal;
    PB2: TProgressBar;
    LaAviso1, LaAviso2, LaAviso3, LaAviso4: TLabel;
    MeAviso: TMemo;
    FParar, FBaseDadosDiminuida: Boolean;
    FTabIMEI: String;
    FDB_IMEI: TmySQLDatabase;
    //
    function  DefineGGXCalToCur(Campo: String; Controle: Integer): Integer;
    function  ImportaDiluicaoMistura(SQL_PeriodoComplexo, SQL_PeriodoVS,
              SQL_PeriodoPQ, SQL_DtHrFimOpe: String; OrigemOpeProc:
              TOrigemOpeProc): Boolean;
    function  ImportaGerArtIsolada_Fast(SQL_PeriodoComplexo, SQL_Periodo, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ImportaGerArtIsolada_Slow(SQL_PeriodoComplexo, SQL_Periodo, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ImportaGerArtConjunta_Fast(SQL_PeriodoComplexo, SQL_Periodo, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ImportaGerArtConjunta(SQL_PeriodoComplexo, SQL_Periodo, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ImportaOpeProcA(QrCab: TMySQLQuery; MovimID: TEstqMovimID;
              SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ImportaOpeProcB(QrCab: TMySQLQuery; MovimID, PsqMovimID_VMI,
              PsqMovimID_PQX: TEstqMovimID; MovimNiv, PsqMovimNiv_VMI:
              TEstqMovimNiv; SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ,
              SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  Insere_OrigeProcPQ(TipoRegSPEDProd: TTipoRegSPEDProd; MovimID:
              TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod: Integer;
              IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
              TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer): Boolean;
    function  Insere_OrigeRibDTA_210_Fast(const TipoRegSPEDProd:
              TTipoRegSPEDProd; const MovimID: TEstqMovimID; const MovimNiv:
              TEstqMovimNiv; const SQL_PeriodoVS: String; const OrigemOpeProc:
              TOrigemOpeProc): Boolean;
    function  Insere_OrigeRibDTA_210(const TipoRegSPEDProd:
              TTipoRegSPEDProd; const MovimID: TEstqMovimID; const MovimNiv:
              TEstqMovimNiv; const SQL_PeriodoVS: String; const OrigemOpeProc:
              TOrigemOpeProc): Boolean;
    function  Insere_OrigeRibPDA_210_Fast(const TipoRegSPEDProd:
              TTipoRegSPEDProd; const MovimID: TEstqMovimID; const MovimNiv:
              TEstqMovimNiv; const SQL_PeriodoVS: String; const OrigemOpeProc:
              TOrigemOpeProc): Boolean;
    function  Insere_OrigeRibPDA_210_Slow(const TipoRegSPEDProd:
              TTipoRegSPEDProd; const MovimID: TEstqMovimID; const MovimNiv:
              TEstqMovimNiv; const SQL_PeriodoVS: String; const OrigemOpeProc:
              TOrigemOpeProc): Boolean;
    function  Insere_OrigeProcVS_210(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; const MovimNivInn, MovimNivBxa:
              TEstqMovimNiv; const QrCab: TmySQLQuery; var IDSeq1: Integer;
              const SQL_PeriodoVS: String; const OrigemOpeProc: TOrigemOpeProc):
              Boolean;
    function  Insere_EmOpeProc_215(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; const QrCab:
              TmySQLQuery; (*const Fator: Double;*) const SQL_Periodo: String;
              const OrigemOpeProc: TOrigemOpeProc; var IDSeq1: Integer):
              Boolean;
    procedure Insere_EmOpeProc_Prd(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; const Qry: TmySQLQuery; const Fator:
              Double; const SQL_Periodo, SQL_PeriodoPQ: String; const
              OrigemOpeProc: TOrigemOpeProc; const MovimNiv: TEstqMovimNiv);
    //
    function  GrandezasInconsistentes(): Boolean;
    procedure Mensagem(ProcName: String; MovimCod, MovimID: Integer; TextoBase:
              String; Query: TComponent; Parar: Boolean = True);
    procedure PesquisaIMECsRelacionados(TargetMovimID, PsqMovimID_VMI,
              PsqMovimID_PQX: TEstqMovimID; PsqMovimNiv_VMI: TEstqMovimNiv;
              SQL_PeriodoVS, SQL_PeriodoPQ: String; IncluiPQx: Boolean);
    procedure ReopenOriPQx(MovimCod: Integer; SQL_PeriodoPQ: String;
              ShowSQL: Boolean);
    //
(*
    procedure InsereB_ProducaoVer1(SQLx, SQL_PeriodoPQ: String; PsqMovimID:
              TEstqMovimID; PsqMovimNiv: TEstqMovimNiv; OrigemOpeProc:
              TOrigemOpeProc);
*)
    procedure InsereB_ProducaoVer2_Fast(TabCab: String; TargetMovimID,
              PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; PsqMovimNiv:
              TEstqMovimNiv; OrigemOpeProc: TOrigemOpeProc;
              FormaInsercaoItemProducao: TFormaInsercaoItemProducao;
              IncluiPQx: Boolean);
    procedure InsereB_ProducaoVer2_Slow(TabCab: String; TargetMovimID,
              PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; PsqMovimNiv:
              TEstqMovimNiv; OrigemOpeProc: TOrigemOpeProc;
              FormaInsercaoItemProducao: TFormaInsercaoItemProducao;
              IncluiPQx: Boolean);
    function  InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd: TTipoRegSPEDProd;
              MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod: Integer;
              IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
              TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer): Boolean;
    function  InsereB_OrigeProcPQ_Conjunta_Fast(TipoRegSPEDProd: TTipoRegSPEDProd;
              MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod: Integer;
              IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
              TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer): Boolean;
    function  InsereB_OrigeProcPQ_Isolada_Fast(TipoRegSPEDProd: TTipoRegSPEDProd;
              MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod: Integer;
              IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
              TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer;
              DtHrAberto: TDateTime): Boolean;
    function  InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd: TTipoRegSPEDProd;
              MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod: Integer;
              IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
              TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer;
              DtHrAberto: TDateTime): Boolean;
    procedure InsereItemProducaoConjuntaGemeos(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GraGruX: Integer;
              const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer; const
              _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc: TOrigemOpeProc;
              const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
              MovCodPai, PaiNivel1, PaiNivel2: Integer; var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ:
              Boolean; const Produziu: TProduziuVS);
    procedure InsereItemProducaoIsoladaGemeos(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, Tipo_Item, GraGruX: Integer;
              const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer; const
              _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc: TOrigemOpeProc;
              const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
              MovCodPai, PaiNivel1, PaiNivel2: Integer; var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
    procedure InsereItemProducaoConjuntaIndireta_Fast(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GraGruX: Integer;
              const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer; const
              _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc: TOrigemOpeProc;
              const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
              MovCodPai, PaiNivel1, PaiNivel2, GerMovID, GerNivel1, GerNivel2,
              GerGGX: Integer; const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
              const FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstqDst: Integer;
              var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoConjuntaIndireta_Slow(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GraGruX: Integer;
              const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer; const
              _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc: TOrigemOpeProc;
              const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
              MovCodPai, PaiNivel1, PaiNivel2, GerMovID, GerNivel1, GerNivel2,
              GerGGX: Integer; const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
              var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoIsoladaIndireta_Fast(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GraGruX: Integer;
              const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer; const
              _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc: TOrigemOpeProc;
              const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
              MovCodPai, PaiNivel1, PaiNivel2, GerMovID, GerNivel1, GerNivel2,
              GerGGX: Integer; const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
              const FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstqDst: Integer;
              var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoIsoladaIndireta_Slow(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GraGruX: Integer;
              const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer; const
              _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc: TOrigemOpeProc;
              const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
              MovCodPai, PaiNivel1, PaiNivel2, GerMovID, GerNivel1, GerNivel2,
              GerGGX: Integer; const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
              var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoConjuntaUniAnt_Fast(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GGXOri, GGXDst:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
              PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer; const
              QtdAntPeca, QtdAntPeso, QtdAntArM2: Double;
              const FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstqDst: Integer;
              var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoConjuntaUniAnt_Slow(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GGXOri, GGXDst:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
              PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer; const
              QtdAntPeca, QtdAntPeso, QtdAntArM2: Double;
              var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoIsoladaUniAnt_Fast(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GGXOri, GGXDst:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
              PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer; const
              QtdAntPeca, QtdAntPeso, QtdAntArM2: Double;
              const FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstqDst: Integer;
              var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoIsoladaUniAnt_Slow(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GGXOri, GGXDst:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
              PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer; const
              QtdAntPeca, QtdAntPeso, QtdAntArM2: Double;
              var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoConjuntaUniGer_Fast(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GGXOri, GGXDst:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
              PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer; const
              QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
              const FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstqDst: Integer;
              var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoConjuntaUniGer_Slow(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GGXOri, GGXDst:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
              PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer; const
              QtdGerPeca, QtdGerPeso, QtdGerArM2: Double; var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoIsoladaUniGer_Fast(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GGXOri, GGXDst:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
              PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
              const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
              const FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstqDst: Integer;
              var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoIsoladaUniGer_Slow(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GGXOri, GGXDst:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
              PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer; const
              QtdGerPeca, QtdGerPeso, QtdGerArM2: Double; var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure DefineCliForSiteOnly(const Controle: Integer; var ClientMO, FornecMO,
              EntiSitio: Integer);
    procedure DefinePsqIMEI(const MovimCod, MovimID, MovimNiv: Integer; var
              IMEI: Integer);
    procedure DefineCliForSiteTipEstq_Fast(const Controle, ClientMO, FornecMO,
              EntiSitio, TipoEstq: Integer);
    procedure DefineCliForSiteTipEstq(const Controle: Integer; var ClientMO, FornecMO,
              EntiSitio, TipoEstq: Integer);
    procedure InsereItemProducaoConjuntaSimples(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GraGruX:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const AreaM2, PesoKg, Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
              Fator: Integer; const OriESTSTabSorc: TEstqSPEDTabSorc;
              const SPED_EFD_GerBxa: TSPED_EFD_GerBxa; const IncluiPQx: Boolean;
              var IDSeq1_K290, IDSeq1_K300: Integer);
    procedure VerificaSeTemGraGruX(IDLinPas, GraGruX: Integer; ProcName: String;
              Qry: TmySQLQuery); overload;
    procedure VerificaSeTemGraGruX(IDLinPas, GraGruX: Integer; ProcName: String;
              Qry: TmySQLDirectQuery); overload;
  end;

var
  FmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a: TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a;

implementation
uses
  Module, UMySQLModule, ModuleGeral, UnVS_PF, ModAppGraG1, PQx, DmkDAC_PF,
  UnEfdIcmsIpi_PF_v03_0_2_a, UnGrade_PF, UnSPED_Create, UnVS_EFD_ICMS_IPI, UnPQ_PF,
  GraGruX;


{$R *.dfm}

{ TFmSpedEfdIcmsIpiProducaoConjunta }

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.DefineCliForSiteOnly(
  const Controle: Integer; var ClientMO, FornecMO, EntiSitio: Integer);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.DefineCliForSiteOnly()';
var
  MovimID, PsqMovimNiv, MovimCod: Integer;
  //
  procedure Msg(Texto: String);
  begin
    Geral.MB_Erro(Texto + ' em ' + sprocName + sLineBreak +
    'IME-I = ' + Geral.FF0(Controle));
  end;
begin
  ReopenObtCliForSite(ocfsControle, Controle, 0, 0);
  //N�o fechar QrObtCliForSite!!!
  ClientMO    := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
  FornecMO    := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
  EntiSitio   := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
  //
  if (FornecMO = 0) or (ClientMO = 0) or (EntiSitio = 0) then
  begin
    MovimCod    := QrObtCliForSite.FieldByName('MovimCod').AsInteger;
    MovimID     := QrObtCliForSite.FieldByName('MovimID').AsInteger;
    PsqMovimNiv := Integer(VS_PF.ObtemMovimNivCliForLocDeMovimID(TEstqMovimID(MovimID)));
    //
    ReopenObtCliForSite(ocfsMovIdNivCod, 0, MovimCod, PsqMovimNiv);
    if ClientMO = 0 then
      ClientMO  := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
    if FornecMO = 0 then
      FornecMO  := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
    if EntiSitio = 0 then
      EntiSitio := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
  end;
  if ClientMO = 0 then
    Msg('ClientMO nao definido');
  if (FornecMO = 0) and (EntiSitio = 0) then
    Msg('FornecMO e EntiSitio nao definidos');
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.DefineCliForSiteTipEstq(const Controle:
  Integer; var ClientMO, FornecMO, EntiSitio, TipoEstq: Integer);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.DefineCliForSiteTipEstq()';
var
  MovimID, PsqMovimNiv, MovimCod: Integer;
  //
  procedure Msg(Texto: String);
  begin
    Geral.MB_Erro(Texto + ' em ' + sprocName + sLineBreak +
    'IME-I = ' + Geral.FF0(Controle));
  end;
begin
  ReopenObtCliForSite(ocfsControle, Controle, 0, 0);
  //N�o fechar QrObtCliForSite!!!
  ClientMO    := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
  FornecMO    := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
  EntiSitio   := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
  TipoEstq := Trunc(QrObtCliForSite.FieldByName('TipoEstq').AsFloat);
  //
  if (FornecMO = 0) or (ClientMO = 0) or (EntiSitio = 0) then
  begin
    MovimCod    := QrObtCliForSite.FieldByName('MovimCod').AsInteger;
    MovimID     := QrObtCliForSite.FieldByName('MovimID').AsInteger;
    PsqMovimNiv := Integer(VS_PF.ObtemMovimNivCliForLocDeMovimID(TEstqMovimID(MovimID)));
    //
    ReopenObtCliForSite(ocfsMovIdNivCod, 0, MovimCod, PsqMovimNiv);
    if ClientMO = 0 then
      ClientMO  := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
    if FornecMO = 0 then
      FornecMO  := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
    if EntiSitio = 0 then
      EntiSitio := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
  end;
  if ClientMO = 0 then
    Msg('ClientMO nao definido');
  if (FornecMO = 0) and (EntiSitio = 0) then
    Msg('FornecMO e EntiSitio nao definidos');
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.DefineCliForSiteTipEstq_Fast(
  const Controle, ClientMO, FornecMO, EntiSitio, TipoEstq: Integer);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.DefineCliForSiteTipEstq_Fast()';
  //
  procedure Msg(Texto: String);
  begin
    Geral.MB_Erro(Texto + ' em ' + sprocName + sLineBreak +
    'IME-I = ' + Geral.FF0(Controle));
  end;
begin
  if ClientMO = 0 then
    Msg('ClientMO nao definido');
  if (FornecMO = 0) and (EntiSitio = 0) then
    Msg('FornecMO e EntiSitio nao definidos');
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.DefineGGXCalToCur(Campo: String;
  Controle: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCalToCur, Dmod.MyDB, [
  //'SELECT DstGGX ',
  'SELECT ' + Campo + ' GraGruX ',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  //Geral.MB_SQL(Self, QrCalToCur);
  Result := QrCalToCurGraGruX.Value;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.DefinePsqIMEI(
  const MovimCod, MovimID, MovimNiv: Integer; var IMEI: Integer);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.DefinePsqIMEI()';
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimID=' + Geral.FF0(MovimID),
  'AND MovimNiv=' + Geral.FF0(MovimNiv),
  '']);
  //Geral.MB_SQL(Self, QrCalToCur);
  IMEI := Qry.FieldByName('Controle').AsInteger;
  if IMEI = 0 then
    Geral.MB_Erro('N�o foi poss�vel obter o IMEI em ' + sProcName);
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.FiltroPeriodoGAR(
  TipoRegSPEDProd: TTipoRegSPEDProd; SQL_Periodo: String): String;
begin
  case TipoRegSPEDProd of
    trsp23X, trsp29X: Result := SQL_Periodo;
    trsp25X, trsp30X: Result := '';
    else
    begin
      Geral.MB_Erro('"FiltroReg" indefinido!');
      Result := '#ERR_PERIODO_';
    end;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.FiltroRegGAR(
  TipoRegSPEDProd: TTipoRegSPEDProd): String;
begin
  case TipoRegSPEDProd of
    trsp23X, trsp29X: Result := '=';
    trsp25X, trsp30X: Result := '<>';
    else
    begin
      Geral.MB_Erro('"FiltroPeriodoGAR" indefinido: ' +
        Geral.FF0(Integer(TipoRegSPEDProd)) + '!');
      Result := '#ERR';
    end;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.FormCreate(Sender: TObject);
begin
  FBaseDadosDiminuida := False;
  FParar := False;
  //
  F_BNCI_IDSeq1_K210 := 0;
  F_BNCI_IDSeq2_K215 := 0;
  F_BNCI_IDSeq1_K230 := 0;
  F_BNCI_IDSeq2_K235 := 0;
  F_BNCI_IDSeq1_K250 := 0;
  F_BNCI_IDSeq2_K255 := 0;
  F_BNCI_IDSeq1_K290 := 0;
  F_BNCI_IDSeq2_K291 := 0;
  F_BNCI_IDSeq2_K292 := 0;
  F_BNCI_IDSeq1_K300 := 0;
  F_BNCI_IDSeq2_K301 := 0;
  F_BNCI_IDSeq2_K302 := 0;
  //
  FSEII_IMEcs := SPED_Create.RecriaTempTableNovo(ntrttSPEDEFD_IMECs, DModG.QrUpdPID1, False);
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.GeraGAR_Fast(
  SQL_Periodo: String): Boolean;
var
  SQL: String;
begin
  //UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  SQL := Geral.ATS([
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_GAR;',
  'CREATE TABLE _SPED_EFD_K2XX_GAR',
  'SELECT vmi.DstGGX, vmi.SrcGGX, ',
  'gg1.UnidMed, med.Grandeza med_grandeza, ',
  'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'DATE(vmi.DataHora) Data, vmi.Codigo, vmi.MovimCod,  ',
  'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg,  ',
  'vmi.MovimTwn  ',
  //'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '        vmi   ',
  'FROM ' + FTabIMEI + '        vmi   ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiXX)),      //15
  SQL_Periodo,
  'ORDER BY Data, vmi.MovimCod, vmi.GraGruX ;',
  '']);
  DModG.MyPID_CompressDB.Execute(SQL);
  //Geral.MB_SQL(Self, DModG.QrUpdPID1);
  Result :=
    not EfdIcmsIpi_PF.ErrGrandeza('_SPED_EFD_K2XX_GAR', 'SrcGGX', 'DstGGX');
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.GeraGAR(
  SQL_Periodo: String): Boolean;
var
  SQL: String;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_GAR;',
  'CREATE TABLE _SPED_EFD_K2XX_GAR',
  'SELECT vmi.DstGGX, vmi.SrcGGX, ',
  'gg1.UnidMed, med.Grandeza med_grandeza, ',
  'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'DATE(vmi.DataHora) Data, vmi.Codigo, vmi.MovimCod,  ',
  'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg,  ',
  'vmi.MovimTwn  ',
  //'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '        vmi   ',
  'FROM ' + FTabIMEI + '        vmi   ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiXX)),      //15
  SQL_Periodo,
  'ORDER BY Data, vmi.MovimCod, vmi.GraGruX ;',
  '']);
  //Geral.MB_SQL(Self, DModG.QrUpdPID1);
  Result :=
    not EfdIcmsIpi_PF.ErrGrandeza('_SPED_EFD_K2XX_GAR', 'SrcGGX', 'DstGGX');
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.GrandezasInconsistentes: Boolean;
var
  SQL_PeriodoVMI: String;
begin
  Result := False;
  SQL_PeriodoVMI := dmkPF.SQL_Periodo('WHERE vmi.DataHora ', FDiaIni, FDiaFim, True, True);
  //
  if DfModAppGraG1.GrandesasInconsistentes([
  'DROP TABLE IF EXISTS _TESTE_; ',
  'CREATE TABLE _TESTE_ ',
  'SELECT DISTINCT ggx.GraGru1 Nivel1, ggx.Controle Reduzido,   ',
  'gg1.Nome NO_GG1, med.Sigla,  ',
  'CAST(med.Grandeza AS DECIMAL(11,0)) Grandeza, ',
  'CAST(xco.Grandeza AS DECIMAL(11,0)) xco_Grandeza,   ',
  'CAST(IF(xco.Grandeza > 0, xco.Grandeza + 0.000,   ',
  '  IF(ggx.GraGruY=1024 AND med.Grandeza = 0, 0.000,  ',
  '  IF((ggx.GraGruY<2048 OR   ',
  '  xco.CouNiv2<>1), 2.000,   ',
  '  IF(xco.CouNiv2=1, 1.000, -1.000)))) AS DECIMAL(11,0)) dif_Grandeza  ',
  '  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle   ',
  'LEFT JOIN ' + TMeuDB + '.couniv1    nv1 ON nv1.Codigo=xco.CouNiv1   ',
  'LEFT JOIN ' + TMeuDB + '.couniv2    nv2 ON nv2.Codigo=xco.CouNiv2   ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed  ',
  SQL_PeriodoVMI,
  'AND vmi.GraGruX <> 0  ',
  'AND ((IF(xco.Grandeza > 0, xco.Grandeza + 0.000,   ',
  '  IF(ggx.GraGruY=1024 AND med.Grandeza = 0, 0.000,  ',
  '  IF((ggx.GraGruY<2048 OR   ',
  '  xco.CouNiv2<>1), 2.000,   ',
  '  IF(xco.CouNiv2=1, 1.000, -1.000))) <> med.Grandeza + 0.000))) ; ',
  ' ',
  '/**/ ',
  ' ',
  'SELECT * FROM _TESTE_ ',
  'WHERE dif_Grandeza <> Grandeza ',
  '; ',
  ''], DModG.MyPID_DB) then
  begin
    Result := True;
    Close;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaDiluicaoMistura(
  SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe: String;
  OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaDiluicaoMistura()';
  MovimID = TEstqMovimID.emidMixInsum; // 35
  OriESTSTabSorc = estsPQx;
  JmpMovID  = 0;
  JmpMovCod = 0;
  JmpNivel1 = 0;
  JmpNivel2 = 0;
  PaiMovID  = 0;
  MovCodPai = 0;
  PaiNivel1 = 0;
  PaiNivel2 = 0;
var
  FornecMO, ClientMO, IDSeq1_K290, IDSeq1_K300, Codigo, MovimCod, CodDocOP,
  EntiSitio, Tipo_Item, GraGruX, UnidMed, Controle: Integer;
  Qtde: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  DtCorrApo_Txt, Sigla, NCM: String;
  //
  procedure ReopenPQMIts(Qry: TmySQLQuery; Tipo: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT OrigemCtrl, DataX, Insumo, Peso ',
    'FROM pqx',
    'WHERE Tipo=' + Geral.FF0(Tipo),
    'AND OrigemCodi=' + Geral.FF0(Codigo),
    'AND DtCorrApo="' + DtCorrApo_Txt + '" ',
    'AND Insumo > 0', // N�o buscar �gua, reciclo e textos!
    'AND Peso <> 0', // N�o textos!
    '']);
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQxMixOP, Dmod.MyDB, [
  'SELECT DISTINCT OrigemCodi, CliDest, DtCorrApo',
  'FROM pqx ',
  'WHERE Tipo IN (' + Geral.FF0(VAR_FATID_0030) + ',' + Geral.FF0(VAR_FATID_0130) + ')',
  'AND Empresa=' + Geral.FF0(FEmpresa),
  SQL_PeriodoPQ,
  '']);
  //Geral.MB_SQL(Self, QrPQxMixOP);
  QrPQxMixOP.First;
  while not QrPQxMixOP.Eof do
  begin
    if FParar then
      Exit;
    Codigo        := QrPQxMixOPOrigemCodi.Value;
    DtCorrApo_Txt := Geral.FDT(QrPQxMixOPDtCorrApo.Value, 1);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPQMCab, Dmod.MyDB, [
    'SELECT DataB, VSMovCod ',
    'FROM pqm',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    MovimCod   := QrPQMCabVSMovCod.Value;
    CodDocOP   := MovimCod;
    DtHrAberto := QrPQMCabDataB.Value;
    DtHrFimOpe := QrPQMCabDataB.Value;
    DtCorrApo  := QrPQxMixOPDtCorrApo.Value;
    //
    ReopenPQMIts(QrPQMGer, VAR_FATID_0030);
    ReopenPQMIts(QrPQMBxa, VAR_FATID_0130);
    //
    //
    IDSeq1_K290 := -1;
    IDSeq1_K300 := -1;
    FornecMO    := FEmpresa;
    ClientMO    := QrPQxMixOPCliDest.Value;
    EntiSitio   := FEmpresa;
    //
    if ClientMO = FornecMO then
      TipoRegSPEDProd := trsp29X
    else
      TipoRegSPEDProd := trsp30X;
     //
    case TipoRegSPEDProd of
      trsp29X:
      begin
        // K290 - Cabe�alho
        if IDSeq1_K290 = -1 then
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
          (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
          DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsPQx, MeAviso, IDSeq1_K290);
        end;
        QrPQMGer.First;
        while not QrPQMGer.Eof do
        begin
          DtMovim    := QrPQMGerDataX.Value;
          //DtCorrApo  := Acima!;
          //GraGruX    :=
          Qtde       := QrPQMGerPeso.Value;
          Controle   := QrPQMGerOrigemCtrl.Value;
          PQ_PF.ObtemDadosGradeDePQ(QrPQMGerInsumo.Value, Tipo_Item, GraGruX,
          UnidMed, Sigla, NCM, True);
          // K291 - Itens Produzidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsPQx, MeAviso);
          //
          QrPQMGer.Next;
        end;
        QrPQMBxa.First;
        while not QrPQMBxa.Eof do
        begin
          DtMovim    := QrPQMBxaDataX.Value;
          //DtCorrApo  := Acima!;
          //GraGruX    :=
          Qtde       := -QrPQMBxaPeso.Value;
          Controle   := QrPQMBxaOrigemCtrl.Value;
          PQ_PF.ObtemDadosGradeDePQ(QrPQMBxaInsumo.Value, Tipo_Item, GraGruX,
          UnidMed, Sigla, NCM, True);
          // K292 - Itens consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsPQx, MeAviso);
          //
          QrPQMBxa.Next;
        end;
      end;
      trsp30X:
      begin
        // K230 - Cabe�alho produ��o em terceiros
        if IDSeq1_K300 = -1 then
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
          (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
          DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsPQx, MeAviso, IDSeq1_K300);
        end;
        QrPQMGer.First;
        while not QrPQMGer.Eof do
        begin
          DtMovim    := QrPQMGerDataX.Value;
          //DtCorrApo  := Acima!;
          //GraGruX    :=
          Qtde       := QrPQMGerPeso.Value;
          Controle   := QrPQMGerOrigemCtrl.Value;
          PQ_PF.ObtemDadosGradeDePQ(QrPQMGerInsumo.Value, Tipo_Item, GraGruX,
          UnidMed, Sigla, NCM, True);
          // K301 - Itens Produzidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsPQx, MeAviso);
          //
          QrPQMGer.Next;
        end;
        QrPQMBxa.First;
        while not QrPQMBxa.Eof do
        begin
          DtMovim    := QrPQMBxaDataX.Value;
          //DtCorrApo  := Acima!;
          //GraGruX    :=
          Qtde       := -QrPQMBxaPeso.Value;
          Controle   := QrPQMBxaOrigemCtrl.Value;
          PQ_PF.ObtemDadosGradeDePQ(QrPQMBxaInsumo.Value, Tipo_Item, GraGruX,
          UnidMed, Sigla, NCM, True);
          // K302 - Itens Consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsPQx, MeAviso);
          //
          QrPQMBxa.Next;
        end;
        //
      end;
      else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(20)');
    end;
    //
    QrPQxMixOP.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaGerArtConjunta_Fast(
  SQL_PeriodoComplexo, SQL_Periodo, SQL_DtHrFimOpe: String;
  OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  MovimID = TEstqMovimID.emidIndsXX;
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaGerArtConjunta()';
  //
  JmpMovID  = 0;
  JmpMovCod = 0;
  JmpNivel1 = 0;
  JmpNivel2 = 0;
  PaiMovID  = 0;
  MovCodPai = 0;
  PaiNivel1 = 0;
  PaiNivel2 = 0;
  //
  function InsereRegistros_Fast(TipoRegSPEDProd: TTipoRegSPEDProd): Boolean;
  var
    MovimCod, MovimTwn, Codigo, GraGruX, TipoEstq, MyFatorCab, MyFAtorIts,
    IDSeq1, ID_Item, Controle, ClientMO, FornecMO, EntiSitio, IDSeq1_K290,
    IDSeq1_K300, CodDocOP: Integer;
    DtHrAberto, DtHrFimOpe: TDateTime;
    AreaM2, PesoKg, Pecas, Qtde, QtdGerArM2: Double;
    COD_INS_SUBST: String;
    //
    DtMovim, DtCorrApo: TDateTime;
    OriESTSTabSorc: TEstqSPEDTabSorc;
  begin
    COD_INS_SUBST := '';
    MyFatorCab := 1;
    MyFatorIts := -1;
    //
////////////////////////////////////////////////////////////////////////////////
///  Cabecalho > Registro K230 ou K250
////////////////////////////////////////////////////////////////////////////////
    ReopenVmiGArCab_Fast(TipoRegSPEDProd, SQL_Periodo);
    //Geral.MB_SQL(Self, DqVmiGArCab);
    PB2.Position := 0;
    PB2.Max := DqVmiGArCab.RecordCount;
    DqVmiGArCab.First;
    while not DqVmiGArCab.Eof do
    begin
      MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
      //
      IDSeq1_K290 := -1;
      IDSeq1_K300 := -1;
      CodDocOP    := USQLDB.Int(DqVmiGArCab, 'MovimCod');
      //
      Codigo      := USQLDB.Int(DqVmiGArCab, 'Codigo');
      MovimCod    := USQLDB.Int(DqVmiGArCab, 'MovimCod');
      MovimTwn    := USQLDB.Int(DqVmiGArCab, 'MovimTwn');
      DtHrAberto  := USQLDB.DtH(DqVmiGArCab, 'Data');
      DtHrFimOpe  := USQLDB.DtH(DqVmiGArCab, 'Data');
      DtMovim     := USQLDB.DtH(DqVmiGArCab, 'Data');
      DtCorrApo   := USQLDB.DtH(DqVmiGArCab, 'DtCorrApo');
      GraGruX     := USQLDB.Int(DqVmiGArCab, 'GraGruX');
      TipoEstq    := Trunc(USQLDB.Flu(DqVmiGArCab, 'TipoEstq'));
      ClientMO    := USQLDB.Int(DqVmiGArCab, 'ClientMO');
      FornecMO    := USQLDB.Int(DqVmiGArCab, 'FornecMO');
      EntiSitio   := USQLDB.Int(DqVmiGArCab, 'EntiSitio');
      Controle    := USQLDB.Int(DqVmiGArCab, 'Controle');
      //
(*
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumGAR, Dmod.MyDB, [
      'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,  ',
      'SUM(AreaM2) AreaM2 ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestCurtiVS)),
      'AND GraGruX=' + Geral.FF0(GraGruX),
      '']);
      AreaM2     := QrSumGARAreaM2.Value;
      PesoKg     := QrSumGARPesoKg.Value;
      Pecas      := QrSumGARPecas.Value;
*)
      AreaM2     := USQLDB.Flu(DqVmiGArCab, 'AreaM2');
      QtdGerArM2 := USQLDB.Flu(DqVmiGArCab, 'QtdGerArM2');
      PesoKg     := USQLDB.Flu(DqVmiGArCab, 'PesoKg');
      Pecas      := USQLDB.Flu(DqVmiGArCab, 'Pecas');
      //
      if ((Pecas <> 0) or (TipoEstq = 2(*kg*))
      or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
      begin
        case TipoEstq of
          0: Qtde := Pecas;
          1:
          begin
            if AreaM2 <> 0 then
              Qtde := AreaM2
            else
              Qtde := QtdGerArM2;
          end;
          2: Qtde := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
            //
            Qtde := Pecas;
          end;
        end;
        Qtde := Qtde * MyFatorCab;
      end else
        Qtde := 0;
      //
      if (*TemTwn and*) (Qtde = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
        GraGruX, MovimCod, Controle, sProcName);
      //
      if Qtde < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (1)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), DqVmiGArCab)
      else if (Qtde = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidIndsXX: DtHrFimOpe := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade zerada para OP encerrada!!! (1)', DqVmiGArCab);
        end;
      end;
      //
      case TipoRegSPEDProd of
        trsp29X:
        begin
          if IDSeq1_K290 = -1 then
          begin
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290_Fast(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
            (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
            DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
            FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290,
            F_BNCI_IDSeq1_K290);
          end;
          // K291 - Itens Produzidos
          OriESTSTabSorc := estsVMI;
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291_Fast(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso,
          F_BNCI_IDSeq2_K291);
        end;
        trsp30X:
        begin
          // K230 - Cabe�alho produ��o em terceiros
          if IDSeq1_K300 = -1 then
          begin
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300_Fast(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
            (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
            DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
            FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300,
            F_BNCI_IDSeq1_K300);
          end;
          // K301 - Itens Produzidos
          OriESTSTabSorc := estsVMI;
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301_Fast(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso,
          F_BNCI_IDSeq2_K301);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(1)');
      end;
////////////////////////////////////////////////////////////////////////////////
///  Item > Registro K235 ou K255
////////////////////////////////////////////////////////////////////////////////
      //
      ReopenVmiGArIts_Fast(MovimCod, MovimTwn, SQL_Periodo, TipoRegSPEDProd);
      DqVmiGArIts.First;
      while not DqVmiGArIts.Eof do
      begin
        if TEstqMovimID(USQLDB.Int(DqVmiGArIts, 'SrcMovID')) = emidEmProcCur then
        begin
          //GraGruX := DefineGGXCalToCur('JmpGGX', USQLDB.Int(DqVmiGArIts, 'SrcNivel2));
          GraGruX := USQLDB.Int(DqVmiGArIts, 'JmpGGX');
        end
        else
          GraGruX := USQLDB.Int(DqVmiGArIts, 'GraGruX');
        //
        Codigo     := USQLDB.Int(DqVmiGArIts, 'Codigo');
        MovimCod   := USQLDB.Int(DqVmiGArIts, 'MovimCod');
        MovimTwn   := USQLDB.Int(DqVmiGArIts, 'MovimTwn');
        DtHrAberto := USQLDB.DtH(DqVmiGArIts, 'Data');
        DtHrFimOpe := USQLDB.DtH(DqVmiGArIts, 'Data');
        TipoEstq   := USQLDB.Int(DqVmiGArIts, 'TipoEstq');
        Controle   := USQLDB.Int(DqVmiGArIts, 'Controle');
        ID_Item    := Controle;
        //
        AreaM2     := USQLDB.Flu(DqVmiGArIts, 'AreaM2');
        PesoKg     := USQLDB.FLu(DqVmiGArIts, 'PesoKg');
        Pecas      := USQLDB.Flu(DqVmiGArIts, 'Pecas');
        //
        DtCorrApo  := USQLDB.Int(DqVmiGArIts, 'DtCorrApo');
        //
        if ((Pecas <> 0) or (TipoEstq = 2(*kg*))
        or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
        begin
          case TipoEstq of
            0: Qtde := Pecas;
            1: Qtde := AreaM2;
            2: Qtde := PesoKg;
            else
            begin
              Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
              + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
              //
              Qtde := Pecas;
            end;
          end;
          Qtde := Qtde * MyFatorIts;
        end else
          Qtde := 0;
        if (*TemTwn and*) (Qtde = 0) then
          VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
          GraGruX, MovimCod, Controle, sProcName);
        //
        if Qtde < 0 then
          Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade n�o pode ser negativa!!! (2)' + sLineBreak +
          'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), DqVmiGArIts)
        else if (Qtde = 0) and (DtHrFimOpe > 1) then
        begin
          case MovimID of
            emidIndsXX: DtHrFimOpe := 0;
            else Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade zerada para OP encerrada!!! (2)', DqVmiGArIts);
          end;
        end;
        //
        case TipoRegSPEDProd of
          //trsp21X:
          trsp29X:
          begin
            // K292 - Itens consumidos
            OriESTSTabSorc := estsVMI;
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292_Fast(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
            PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
            GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
            ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
            FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso,
            F_BNCI_IDSeq2_K292);
          end;
          trsp30X:
          begin
            // K302 - Itens Consumidos
            OriESTSTabSorc := estsVMI;
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302_Fast(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
            PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
            GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
            ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
            FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso,
            F_BNCI_IDSeq2_K302);
          end;
          else
            Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
            TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(2)');
        end;
        //
        DqVmiGArIts.Next;
      end;
      //
      DqVmiGArCab.Next;
    end;
  end;
begin
  if not GeraGAR_Fast(SQL_Periodo) then
    Exit;
  //
  InsereRegistros_Fast(trsp29X);
  InsereRegistros_Fast(trsp30X);
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaGerArtConjunta(
  SQL_PeriodoComplexo, SQL_Periodo, SQL_DtHrFimOpe: String;
  OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  MovimID = TEstqMovimID.emidIndsXX;
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaGerArtConjunta()';
  //
  JmpMovID  = 0;
  JmpMovCod = 0;
  JmpNivel1 = 0;
  JmpNivel2 = 0;
  PaiMovID  = 0;
  MovCodPai = 0;
  PaiNivel1 = 0;
  PaiNivel2 = 0;
  //
  function InsereRegistros(TipoRegSPEDProd: TTipoRegSPEDProd): Boolean;
  var
    MovimCod, MovimTwn, Codigo, GraGruX, TipoEstq, MyFatorCab, MyFAtorIts,
    IDSeq1, ID_Item, Controle, ClientMO, FornecMO, EntiSitio, IDSeq1_K290,
    IDSeq1_K300, CodDocOP: Integer;
    DtHrAberto, DtHrFimOpe: TDateTime;
    AreaM2, PesoKg, Pecas, Qtde, QtdGerArM2: Double;
    COD_INS_SUBST: String;
    //
    DtMovim, DtCorrApo: TDateTime;
    OriESTSTabSorc: TEstqSPEDTabSorc;
  begin
    COD_INS_SUBST := '';
    MyFatorCab := 1;
    MyFatorIts := -1;
    //
////////////////////////////////////////////////////////////////////////////////
///  Cabecalho > Registro K230 ou K250
////////////////////////////////////////////////////////////////////////////////
    ReopenVmiGArCab(TipoRegSPEDProd, SQL_Periodo);
    //Geral.MB_SQL(Self, QrVmiGArCab);
    PB2.Position := 0;
    PB2.Max := QrVmiGArCab.RecordCount;
    QrVmiGArCab.First;
    while not QrVmiGArCab.Eof do
    begin
      MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
      //
      IDSeq1_K290 := -1;
      IDSeq1_K300 := -1;
      CodDocOP    := QrVmiGArCabMovimCod.Value;
      //
      Codigo      := QrVmiGArCabCodigo.Value;
      MovimCod    := QrVmiGArCabMovimCod.Value;
      MovimTwn    := QrVmiGArCabMovimTwn.Value;
      DtHrAberto  := QrVmiGArCabData.Value;
      DtHrFimOpe  := QrVmiGArCabData.Value;
      DtMovim     := QrVmiGArCabData.Value;
      DtCorrApo   := QrVmiGArCabDtCorrApo.Value;
      GraGruX     := QrVmiGArCabGraGruX.Value;
      TipoEstq    := Trunc(QrVmiGArCabTipoEstq.Value);
      ClientMO    := QrVmiGArCabClientMO.Value;
      FornecMO    := QrVmiGArCabFornecMO.Value;
      EntiSitio   := QrVmiGArCabEntiSitio.Value;
      Controle    := QrVmiGArCabControle.Value;
      //
(*
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumGAR, Dmod.MyDB, [
      'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,  ',
      'SUM(AreaM2) AreaM2 ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestCurtiVS)),
      'AND GraGruX=' + Geral.FF0(GraGruX),
      '']);
      AreaM2     := QrSumGARAreaM2.Value;
      PesoKg     := QrSumGARPesoKg.Value;
      Pecas      := QrSumGARPecas.Value;
*)
      AreaM2     := QrVmiGArCabAreaM2.Value;
      QtdGerArM2 := QrVmiGArCabQtdGerArM2.Value;
      PesoKg     := QrVmiGArCabPesoKg.Value;
      Pecas      := QrVmiGArCabPecas.Value;
      //
      if ((Pecas <> 0) or (TipoEstq = 2(*kg*))
      or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
      begin
        case TipoEstq of
          0: Qtde := Pecas;
          1:
          begin
            if AreaM2 <> 0 then
              Qtde := AreaM2
            else
              Qtde := QtdGerArM2;
          end;
          2: Qtde := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
            //
            Qtde := Pecas;
          end;
        end;
        Qtde := Qtde * MyFatorCab;
      end else
        Qtde := 0;
      //
      if (*TemTwn and*) (Qtde = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
        GraGruX, MovimCod, Controle, sProcName);
      //
      if Qtde < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (1)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrVmiGArCab)
      else if (Qtde = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidIndsXX: DtHrFimOpe := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade zerada para OP encerrada!!! (1)', QrVmiGArCab);
        end;
      end;
      //
      case TipoRegSPEDProd of
        trsp29X:
        begin
          if IDSeq1_K290 = -1 then
          begin
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
            (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
            DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
            FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290);
          end;
          // K291 - Itens Produzidos
          OriESTSTabSorc := estsVMI;
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
        end;
        trsp30X:
        begin
          // K230 - Cabe�alho produ��o em terceiros
          if IDSeq1_K300 = -1 then
          begin
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
            (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
            DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
            FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300);
          end;
          // K301 - Itens Produzidos
          OriESTSTabSorc := estsVMI;
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(1)');
      end;
////////////////////////////////////////////////////////////////////////////////
///  Item > Registro K235 ou K255
////////////////////////////////////////////////////////////////////////////////
      //
      ReopenVmiGArIts(MovimCod, MovimTwn, SQL_Periodo, TipoRegSPEDProd);
      QrVmiGArIts.First;
      while not QrVmiGArIts.Eof do
      begin
        if TEstqMovimID(QrVmiGArItsSrcMovID.Value) = emidEmProcCur then
        begin
          //GraGruX := DefineGGXCalToCur('JmpGGX', QrVmiGArItsSrcNivel2.Value);
          GraGruX := QrVmiGArItsJmpGGX.Value;
        end
        else
          GraGruX := QrVmiGArItsGraGruX.Value;
        //
        Codigo     := QrVmiGArItsCodigo.Value;
        MovimCod   := QrVmiGArItsMovimCod.Value;
        MovimTwn   := QrVmiGArItsMovimTwn.Value;
        DtHrAberto := QrVmiGArItsData.Value;
        DtHrFimOpe := QrVmiGArItsData.Value;
        TipoEstq   := QrVmiGArItsTipoEstq.Value;
        Controle   := QrVmiGArItsControle.Value;
        ID_Item    := Controle;
        //
        AreaM2     := QrVmiGArItsAreaM2.Value;
        PesoKg     := QrVmiGArItsPesoKg.Value;
        Pecas      := QrVmiGArItsPecas.Value;
        //
        DtCorrApo  := QrVmiGArItsDtCorrApo.Value;
        //
        if ((Pecas <> 0) or (TipoEstq = 2(*kg*))
        or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
        begin
          case TipoEstq of
            0: Qtde := Pecas;
            1: Qtde := AreaM2;
            2: Qtde := PesoKg;
            else
            begin
              Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
              + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
              //
              Qtde := Pecas;
            end;
          end;
          Qtde := Qtde * MyFatorIts;
        end else
          Qtde := 0;
        if (*TemTwn and*) (Qtde = 0) then
          VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
          GraGruX, MovimCod, Controle, sProcName);
        //
        if Qtde < 0 then
          Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade n�o pode ser negativa!!! (2)' + sLineBreak +
          'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrVmiGArIts)
        else if (Qtde = 0) and (DtHrFimOpe > 1) then
        begin
          case MovimID of
            emidIndsXX: DtHrFimOpe := 0;
            else Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade zerada para OP encerrada!!! (2)', QrVmiGArIts);
          end;
        end;
        //
        case TipoRegSPEDProd of
          //trsp21X:
          trsp29X:
          begin
            // K292 - Itens consumidos
            OriESTSTabSorc := estsVMI;
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
            PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
            GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
            ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
            FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
          end;
          trsp30X:
          begin
            // K302 - Itens Consumidos
            OriESTSTabSorc := estsVMI;
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
            PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
            GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
            ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
            FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
          end;
          else
            Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
            TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(2)');
        end;
        //
        QrVmiGArIts.Next;
      end;
      //
      QrVmiGArCab.Next;
    end;
  end;
begin
  if not GeraGAR(SQL_Periodo) then
    Exit;
  //
  InsereRegistros(trsp29X);
  InsereRegistros(trsp30X);
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaGerArtIsolada_Fast(SQL_PeriodoComplexo,
  SQL_Periodo, SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  MovimID = TEstqMovimID.emidIndsXX;
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaGerArtIsolada_Fast()';
  //
  function FiltroReg(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := '=';
      trsp25X: Result := '<>';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (1)');
        Result := '#ERR';
      end;
    end;
  end;
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := SQL_Periodo;
      //trsp25X: Result := SQL_DtHrFimOpe;
      //trsp25X: Result := SQL_Periodo;
      trsp25X: Result := '';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido!');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
  //
  function InsereRegistros_Fast(TipoRegSPEDProd: TTipoRegSPEDProd): Boolean;
  var
    MovimCod, MovimTwn, Codigo, GraGruX, TipoEstq, MyFatorCab, MyFAtorIts,
    IDSeq1, ID_Item, Controle, ClientMO, FornecMO, EntiSitio, Tipo_Item: Integer;
    DtHrAberto, DtHrFimOpe: TDateTime;
    AreaM2, PesoKg, Pecas, Qtde, QtdGerArM2: Double;
    COD_INS_SUBST: String;
    //
    DtMovim, DtCorrApo: TDateTime;
  begin
    COD_INS_SUBST := '';
    MyFatorCab := 1;
    MyFatorIts := -1;
    //
////////////////////////////////////////////////////////////////////////////////
///  Cabecalho > Registro K230 ou K250
////////////////////////////////////////////////////////////////////////////////
    ReopenVmiGArCab_Fast(TipoRegSPEDProd, SQL_Periodo);
    PB2.Position := 0;
    PB2.Max := DqVmiGArCab.RecordCount;
    DqVmiGArCab.First;
    while not DqVmiGArCab.Eof do
    begin
      if FParar then
        Exit;
      MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
      //
      Codigo     := USQLDB.Int(DqVmiGArCab, 'Codigo');
      MovimCod   := USQLDB.Int(DqVmiGArCab, 'MovimCod');
      MovimTwn   := USQLDB.Int(DqVmiGArCab, 'MovimTwn');
      DtHrAberto := USQLDB.DtH(DqVmiGArCab, 'Data');
      DtHrFimOpe := USQLDB.DtH(DqVmiGArCab, 'Data');
      DtMovim    := USQLDB.DtH(DqVmiGArCab, 'Data');
      DtCorrApo  := USQLDB.DtH(DqVmiGArCab, 'DtCorrApo');
      GraGruX    := USQLDB.Int(DqVmiGArCab, 'GraGruX');
      TipoEstq   := Trunc(USQLDB.Int(DqVmiGArCab, 'TipoEstq'));
      ClientMO   := USQLDB.Int(DqVmiGArCab, 'ClientMO');
      FornecMO   := USQLDB.Int(DqVmiGArCab, 'FornecMO');
      EntiSitio  := USQLDB.Int(DqVmiGArCab, 'EntiSitio');
      Controle   := USQLDB.Int(DqVmiGArCab, 'Controle');
      //
(*
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumGAR, Dmod.MyDB, [
      'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,  ',
      'SUM(AreaM2) AreaM2 ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestCurtiVS)),
      'AND GraGruX=' + Geral.FF0(GraGruX),
      '']);
      AreaM2     := QrSumGARAreaM2.Value;
      PesoKg     := QrSumGARPesoKg.Value;
      Pecas      := QrSumGARPecas.Value;
*)
      AreaM2     := USQLDB.Flu(DqVmiGArCab, 'AreaM2');
      PesoKg     := USQLDB.Flu(DqVmiGArCab, 'PesoKg');
      Pecas      := USQLDB.Flu(DqVmiGArCab, 'Pecas');
      QtdGerArM2 := USQLDB.Flu(DqVmiGArCab, 'QtdGerArM2');
      //
      if ((Pecas <> 0) or (TipoEstq = 2(*kg*))
      or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
      begin
        case TipoEstq of
          0: Qtde := Pecas;
          1:
          begin
            if AreaM2 <> 0 then
              Qtde := AreaM2
            else
              Qtde := QtdGerArM2;
          end;
          2: Qtde := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
            //
            Qtde := Pecas;
          end;
        end;
        Qtde := Qtde * MyFatorCab;
      end else
        Qtde := 0;
      if (*TemTwn and*) (Qtde = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
        GraGruX, MovimCod, Controle, sProcName);
      //
      if Qtde < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (3)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), DqVmiGArCab)
      else if (Qtde = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidIndsXX: DtHrFimOpe := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade zerada para OP encerrada!!! (3)', DqVmiGArCab);
        end;
      end;
      //
      case TipoRegSPEDProd of
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230_Fast(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod,
          Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GraGruX, ClientMO,
          FornecMO, EntiSitio, Qtde, OrigemOpeProc,
          FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1, F_BNCI_IDSeq1_K230);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250_Fast(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod,
          Controle,
          DtHrFimOpe,
          DtMovim, DtCorrApo, GraGruX, ClientMO, FornecMO, EntiSitio,
          Qtde, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso,
          IDSeq1, F_BNCI_IDSeq1_K250);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(3)');
      end;
////////////////////////////////////////////////////////////////////////////////
///  Item > Registro K235 ou K255
////////////////////////////////////////////////////////////////////////////////
      //
      ReopenVmiGArIts_Fast(MovimCod, MovimTwn, SQL_Periodo, TipoRegSPEDProd);
      DqVmiGArIts.First;
      while not DqVmiGArIts.Eof do
      begin
        if TEstqMovimID(USQLDB.Int(DqVmiGArIts, 'SrcMovID')) = emidEmProcCur then
        begin
          //GraGruX := DefineGGXCalToCur('JmpGGX', DqVmiGArIts, 'SrcNivel2.Value);
          GraGruX := USQLDB.Int(DqVmiGArIts, 'JmpGGX');
        end
        else
          GraGruX := USQLDB.Int(DqVmiGArIts, 'GraGruX');
        //
        Codigo     := USQLDB.Int(DqVmiGArIts, 'Codigo');
        MovimCod   := USQLDB.Int(DqVmiGArIts, 'MovimCod');
        MovimTwn   := USQLDB.Int(DqVmiGArIts, 'MovimTwn');
        DtHrAberto := USQLDB.DtH(DqVmiGArIts, 'Data');
        DtHrFimOpe := USQLDB.DtH(DqVmiGArIts, 'Data');
        TipoEstq   := USQLDB.Int(DqVmiGArIts, 'TipoEstq');
        Controle   := USQLDB.Int(DqVmiGArIts, 'Controle');
        ID_Item    := Controle;
        //
        AreaM2     := USQLDB.Flu(DqVmiGArIts, 'AreaM2');
        PesoKg     := USQLDB.Flu(DqVmiGArIts, 'PesoKg');
        Pecas      := USQLDB.Flu(DqVmiGArIts, 'Pecas');
        //
        DtCorrApo  := USQLDB.DtH(DqVmiGArIts, 'DtCorrApo');
        //
        if ((Pecas <> 0) or (TipoEstq = 2(*kg*))
        or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
        begin
          case TipoEstq of
            0: Qtde := Pecas;
            1: Qtde := AreaM2;
            2: Qtde := PesoKg;
            else
            begin
              Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
              + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
              //
              Qtde := Pecas;
            end;
          end;
          Qtde := Qtde * MyFatorIts;
        end else
          Qtde := 0;
        if (*TemTwn and*) (Qtde = 0) then
          VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
          GraGruX, MovimCod, Controle, sProcName);
        //
        if Qtde < 0 then
          Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade n�o pode ser negativa!!! (4)' + sLineBreak +
          'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), DqVmiGArIts)
        else if (Qtde = 0) and (DtHrFimOpe > 1) then
        begin
          case MovimID of
            emidIndsXX: DtHrFimOpe := 0;
            else Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade zerada para OP encerrada!!! (4)', DqVmiGArIts);
          end;
        end;
        //
        case TipoRegSPEDProd of
          //trsp21X:
          trsp23X:
          begin
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235_Fast(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GraGruX,
            Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
            Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
            ESTSTabSorc, FTipoPeriodoFiscal, MeAviso, F_BNCI_IDSeq2_K235);
          end;
          trsp25X:
          begin
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255_Fast(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GraGruX,
            Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
            Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
            ESTSTabSorc, FTipoPeriodoFiscal, MeAviso, F_BNCI_IDSeq2_K255);
          end;
          else
            Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
            TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(4)');
        end;
        //
        DqVmiGArIts.Next;
      end;
      //
      DqVmiGArCab.Next;
    end;
  end;
begin
  if not GeraGAR_Fast(SQL_Periodo) then
    Exit;
  //
  InsereRegistros_Fast(trsp23X);
  InsereRegistros_Fast(trsp25X);
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaGerArtIsolada_Slow(SQL_PeriodoComplexo,
  SQL_Periodo, SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  MovimID = TEstqMovimID.emidIndsXX;
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaGerArtIsoladaSlow()';
  //
  function FiltroReg(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := '=';
      trsp25X: Result := '<>';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (1)');
        Result := '#ERR';
      end;
    end;
  end;
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := SQL_Periodo;
      //trsp25X: Result := SQL_DtHrFimOpe;
      //trsp25X: Result := SQL_Periodo;
      trsp25X: Result := '';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido!');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
  //
  function InsereRegistros(TipoRegSPEDProd: TTipoRegSPEDProd): Boolean;
  var
    MovimCod, MovimTwn, Codigo, GraGruX, TipoEstq, MyFatorCab, MyFAtorIts,
    IDSeq1, ID_Item, Controle, ClientMO, FornecMO, EntiSitio, Tipo_Item: Integer;
    DtHrAberto, DtHrFimOpe: TDateTime;
    AreaM2, PesoKg, Pecas, Qtde, QtdGerArM2: Double;
    COD_INS_SUBST: String;
    //
    DtMovim, DtCorrApo: TDateTime;
  begin
    COD_INS_SUBST := '';
    MyFatorCab := 1;
    MyFatorIts := -1;
    //
////////////////////////////////////////////////////////////////////////////////
///  Cabecalho > Registro K230 ou K250
////////////////////////////////////////////////////////////////////////////////
    ReopenVmiGArCab(TipoRegSPEDProd, SQL_Periodo);
    PB2.Position := 0;
    PB2.Max := QrVmiGArCab.RecordCount;
    QrVmiGArCab.First;
    while not QrVmiGArCab.Eof do
    begin
      if FParar then
        Exit;
      MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
      //
      Codigo     := QrVmiGArCabCodigo.Value;
      MovimCod   := QrVmiGArCabMovimCod.Value;
      MovimTwn   := QrVmiGArCabMovimTwn.Value;
      DtHrAberto := QrVmiGArCabData.Value;
      DtHrFimOpe := QrVmiGArCabData.Value;
      DtMovim    := QrVmiGArCabData.Value;
      DtCorrApo  := QrVmiGArCabDtCorrApo.Value;
      GraGruX    := QrVmiGArCabGraGruX.Value;
      TipoEstq   := Trunc(QrVmiGArCabTipoEstq.Value);
      ClientMO   := QrVmiGArCabClientMO.Value;
      FornecMO   := QrVmiGArCabFornecMO.Value;
      EntiSitio  := QrVmiGArCabEntiSitio.Value;
      Controle   := QrVmiGArCabControle.Value;
      //
(*
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumGAR, Dmod.MyDB, [
      'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,  ',
      'SUM(AreaM2) AreaM2 ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestCurtiVS)),
      'AND GraGruX=' + Geral.FF0(GraGruX),
      '']);
      AreaM2     := QrSumGARAreaM2.Value;
      PesoKg     := QrSumGARPesoKg.Value;
      Pecas      := QrSumGARPecas.Value;
*)
      AreaM2     := QrVmiGArCabAreaM2.Value;
      PesoKg     := QrVmiGArCabPesoKg.Value;
      Pecas      := QrVmiGArCabPecas.Value;
      QtdGerArM2 := QrVmiGArCabQtdGerArM2.Value;
      //
      if ((Pecas <> 0) or (TipoEstq = 2(*kg*))
      or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
      begin
        case TipoEstq of
          0: Qtde := Pecas;
          1:
          begin
            if AreaM2 <> 0 then
              Qtde := AreaM2
            else
              Qtde := QtdGerArM2;
          end;
          2: Qtde := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
            //
            Qtde := Pecas;
          end;
        end;
        Qtde := Qtde * MyFatorCab;
      end else
        Qtde := 0;
      if (*TemTwn and*) (Qtde = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
        GraGruX, MovimCod, Controle, sProcName);
      //
      if Qtde < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (3)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrVmiGArCab)
      else if (Qtde = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidIndsXX: DtHrFimOpe := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade zerada para OP encerrada!!! (3)', QrVmiGArCab);
        end;
      end;
      //
      case TipoRegSPEDProd of
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod,
          Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GraGruX, ClientMO,
          FornecMO, EntiSitio, Qtde, OrigemOpeProc,
          FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod,
          Controle,
          DtHrFimOpe,
          DtMovim, DtCorrApo, GraGruX, ClientMO, FornecMO, EntiSitio,
          Qtde, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(3)');
      end;
////////////////////////////////////////////////////////////////////////////////
///  Item > Registro K235 ou K255
////////////////////////////////////////////////////////////////////////////////
      //
      ReopenVmiGArIts(MovimCod, MovimTwn, SQL_Periodo, TipoRegSPEDProd);
      QrVmiGArIts.First;
      while not QrVmiGArIts.Eof do
      begin
        if TEstqMovimID(QrVmiGArItsSrcMovID.Value) = emidEmProcCur then
        begin
          //GraGruX := DefineGGXCalToCur('JmpGGX', QrVmiGArItsSrcNivel2.Value);
          GraGruX := QrVmiGArItsJmpGGX.Value;
        end
        else
          GraGruX := QrVmiGArItsGraGruX.Value;
        //
        Codigo     := QrVmiGArItsCodigo.Value;
        MovimCod   := QrVmiGArItsMovimCod.Value;
        MovimTwn   := QrVmiGArItsMovimTwn.Value;
        DtHrAberto := QrVmiGArItsData.Value;
        DtHrFimOpe := QrVmiGArItsData.Value;
        TipoEstq   := QrVmiGArItsTipoEstq.Value;
        Controle   := QrVmiGArItsControle.Value;
        ID_Item    := Controle;
        //
        AreaM2     := QrVmiGArItsAreaM2.Value;
        PesoKg     := QrVmiGArItsPesoKg.Value;
        Pecas      := QrVmiGArItsPecas.Value;
        //
        DtCorrApo  := QrVmiGArItsDtCorrApo.Value;
        //
        if ((Pecas <> 0) or (TipoEstq = 2(*kg*))
        or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
        begin
          case TipoEstq of
            0: Qtde := Pecas;
            1: Qtde := AreaM2;
            2: Qtde := PesoKg;
            else
            begin
              Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
              + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
              //
              Qtde := Pecas;
            end;
          end;
          Qtde := Qtde * MyFatorIts;
        end else
          Qtde := 0;
        if (*TemTwn and*) (Qtde = 0) then
          VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
          GraGruX, MovimCod, Controle, sProcName);
        //
        if Qtde < 0 then
          Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade n�o pode ser negativa!!! (4)' + sLineBreak +
          'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrVmiGArIts)
        else if (Qtde = 0) and (DtHrFimOpe > 1) then
        begin
          case MovimID of
            emidIndsXX: DtHrFimOpe := 0;
            else Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade zerada para OP encerrada!!! (4)', QrVmiGArIts);
          end;
        end;
        //
        case TipoRegSPEDProd of
          //trsp21X:
          trsp23X:
          begin
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GraGruX,
            Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
            Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
            ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
          end;
          trsp25X:
          begin
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GraGruX,
            Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
            Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
            ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
          end;
          else
            Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
            TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(4)');
        end;
        //
        QrVmiGArIts.Next;
      end;
      //
      QrVmiGArCab.Next;
    end;
  end;
begin
  if not GeraGAR(SQL_Periodo) then
    Exit;
  //
  InsereRegistros(trsp23X);
  InsereRegistros(trsp25X);
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaOpeProcA(QrCab: TMySQLQuery;
  MovimID: TEstqMovimID; SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ,
  SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
var
  TabSrc, SQL_FLD, SQL_AND: String;
  MovNivInn, MovNivBxa: TEstqMovimNiv;
  //
  function FiltroReg(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := '=';
      trsp25X: Result := '<>';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (2)');
        Result := '#ERR';
      end;
    end;
  end;
  //
  function FiltroCond(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := ' OR ';
      trsp25X: Result := ' AND ';
      else
      begin
        Geral.MB_Erro('"FiltroCond" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (2a)');
        Result := '#ERR';
      end;
    end;
  end;
  //
  function ReabreEmProcesso21X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    SQL_PeriodoComplexo,
    'OR MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    '  ' + SQL_PeriodoVS,
    ')) ',
    SQL_AND,

    ' ',
    'AND MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv=' + Geral.FF0(Integer(MovNivInn)),
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    //'  AND FornecMO' + FiltroReg(trsp21X) + Geral.FF0(FEmpresa) +
    '  )',
    ') ',
    ' ',
    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function ReabreEmProcesso23X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    //'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    SQL_PeriodoComplexo,
    'OR MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    '  ' + SQL_PeriodoVS,
    ')) ',
    SQL_AND,

    ' ',
    'AND MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv=' + Geral.FF0(Integer(MovNivInn)),
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    '  AND (FornecMO' + FiltroReg(trsp23X) + Geral.FF0(FEmpresa),
    '  ' + FiltroCond(trsp23X) + ' FornecMO' + FiltroReg(trsp23X) + '0) ) ',
    ') ',
    ' ',

    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
(*
    if Query.RecordCount > 0 then Geral.MB_SQL(Self, Query);
*)
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function ReabreEmProcesso25X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    'WHERE  MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv IN (' + Geral.FF0(Integer(MovNivInn)) + ',' +
    Geral.FF0(Integer(MovNivBxa)) + ') ',
    //
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    '  AND (FornecMO' + FiltroReg(trsp25X) + Geral.FF0(FEmpresa),
    '  ' + FiltroCond(trsp25X) + ' FornecMO' + FiltroReg(trsp25X) + '0) ) ',
    ' ' + SQL_PeriodoVS,
    ') ',
    ' ',
    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
(*
    if Query.RecordCount > 0 then Geral.MB_SQL(Self, Query);
*)
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function ReabreEmProcesso26X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    SQL_PeriodoComplexo,
    'OR MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    '  ' + SQL_PeriodoVS,
    ')) ',
    SQL_AND,

    ' ',
    'AND MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv=' + Geral.FF0(Integer(MovNivInn)),
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    '  )',
    ') ',
    ' ',
    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
(*
    if Query.RecordCount > 0 then Geral.MB_SQL(Self, Query);
*)
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function InsereRegistros(TipoRegSPEDProd: TTipoRegSPEDProd): Boolean;
  var
    LinArqPai, MovimCod: Integer;
    Fator: Double;
    Abriu: Boolean;
  begin
    Result := False;
    case TipoRegSPEDProd of
      trsp21X: Abriu := ReabreEmProcesso21X(QrCab);
      trsp23X: Abriu := ReabreEmProcesso23X(QrCab);
      trsp25X: Abriu := ReabreEmProcesso25X(QrCab);
      trsp26X: Abriu := ReabreEmProcesso26X(QrCab);
      else
      begin
        Geral.MB_Aviso('Tipo de registro indefinido em "InsereRegistros()"');
        Abriu := False;
      end;
    end;
    if not Abriu then
      Exit;
    //
    PB2.Max := QrCab.RecordCount;
    PB2.Position := 0;
    QrCab.First;
    while not QrCab.Eof do
    begin
      if FParar then
        Exit;
      MyObjects.UpdPB(PB2, nil, nil);
      MovimCod  := QrCab.FieldByName('MovimCod').AsInteger;
      if TipoRegSPEDProd = trsp21X then
      begin
        case MovimID of
(*         Ja tem proprio: Insere_OrigeRibPDA_210()
          emidEmRibPDA:
          begin
            Fator := 1;
            Insere_OrigeRibPDA_210(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminDestPDA, QrCab, LinArqPai, SQL_PeriodoVS, OrigemOpeProc);
          end;
*)
          emidEmProcCal: ; // nada!!
          emidEmProcCur: ; // nada!!
          emidEmOperacao:
          begin
            Fator := 1;
            Insere_OrigeProcVS_210(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminEmOperInn, TEstqMovimNiv.eminEmOperBxa, QrCab, LinArqPai, SQL_PeriodoVS, OrigemOpeProc);
            Insere_EmOpeProc_215(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminDestOper, QrCab, (*Fator,*) SQL_PeriodoVS, OrigemOpeProc, LinArqPai);
          end;
          emidEmProcWE: ; // nada!!
          else
          begin
            Result := False;
            Geral.MB_Erro('MovimID ' + Geral.FF0(Integer(MovimID)) + ' n�o implementado em "ImportaOpeProc().21x"');
            Exit;
          end;
        end;
      end else
      begin
        case MovimID of
          emidEmProcCal:
          begin
            Fator := -1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmCalBxa);
          end;
          emidEmProcCur:
          begin
            Fator := 1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmCurBxa);
          end;
          emidEmProcWE:
          begin
            Fator := 1;

            //criar query que separa periodos + Periodos de correcao!
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmWEndBxa);
          end;
          emidEmProcSP:
          begin
            Fator := 1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmPSPBxa);
          end;
          emidEmReprRM:
          begin
            Fator := 1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmRRMBxa);
           // Geral.MB_SQL(Self, QrCab);
          end;
          else
          begin
            Result := False;
            // MovimID 30 n�o implementado em "ImportaOpeProc().2xx"
            Geral.MB_Erro('MovimID ' + Geral.FF0(Integer(MovimID)) + ' n�o implementado em "ImportaOpeProc().2xx"');
            Exit;
          end;
        end;
      end;
      QrCab.Next;
    end;
    Result := True;
  end;
begin
//fazer novo aqui!
  //PB1.Position := PB1.Position + 1;
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando "DtHrFimOpe" do MovimID: ' +
    Geral.FF0(Integer(MovimID)));
  VS_PF.AtualizaDtHrFimOpe_MovimID(MovimID);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Importando MovimID: ' +
    Geral.FF0(Integer(MovimID)));
  TabSrc    := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
  // Recurtimento!!!
  MovNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
  MovNivBxa := VS_PF.ObtemMovimNivBxaDeMovimID(MovimID);
  // ?????
  //MovNivInn := VS_PF.ObtemMovimNivBxaDeMovimID(MovimID);
  if MovNivInn = eminSemNiv then
    Exit;
  //
  SQL_FLD := Geral.ATS([
  'SELECT DISTINCT ggx.GraGru1 Nivel1, ggx.Controle Reduzido, ',
  'gg1.Nome NO_GG1, med.Sigla, med.Grandeza ',
  '']);
  SQL_AND := Geral.ATS([
  'AND ((IF(xco.Grandeza > 0, xco.Grandeza, ',
  '  IF((ggx.GraGruY<2048 OR ',
  '  xco.CouNiv2<>1), 2, ',
  '  IF(xco.CouNiv2=1, 1, -1))) <> med.Grandeza))',
  '']);
  if GrandezasInconsistentes() then
    Exit;
  //
  SQL_FLD := Geral.ATS([
  'SELECT  ',
  //
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vsx.AreaDstM2', 'vsx.PesoKgDst', 'vsx.PecasDst'),
  //
  'vsx.* ',
  '']);
  SQL_AND := '';
  //
  case MovimID of
    //emidEmRibPDA, Ja tem proprio: Insere_OrigeRibPDA_210()
    //emidEmRibDTA,
    emidEmOperacao:
    begin
      InsereRegistros(trsp21X);
    end;
    emidEmProcCal,
    emidEmProcCur,
    emidEmProcWE,
    emidEmProcSP:
    begin
      InsereRegistros(trsp23X);
      InsereRegistros(trsp25X);
    end;
    emidEmReprRM:
    begin
      InsereRegistros(trsp26X);
    end;
    else Geral.MB_Erro('"MovimID" n�o implementado em "ImportaOpeProcA()"');
  end;
  //
  Result := True;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaOpeProcB(QrCab: TMySQLQuery;
  MovimID, PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; MovimNiv,
  PsqMovimNiv_VMI: TEstqMovimNiv; SQL_PeriodoComplexo, SQL_PeriodoVS,
  SQL_PeriodoPQ, SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc):
  Boolean;
const
  sProcName = 'TTFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaOpeProcB()"';
begin
//
  //case FSPED_EFD_Producao of
  case FVersaoIsldCnjt of
    0: ImportaOpeProcB_Isolada(QrCab, PsqMovimNiv_VMI, SQL_PeriodoComplexo,
       SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe, OrigemOpeProc);
    1: ImportaOpeProcB_Conjunta(QrCab, MovimID, PsqMovimID_VMI, PsqMovimID_PQX,    //isolada tambem??
       MovimNiv, PsqMovimNiv_VMI, SQL_PeriodoComplexo, SQL_PeriodoVS,
       SQL_PeriodoPQ, SQL_DtHrFimOpe, OrigemOpeProc);
    //else Geral.MB_Erro('"FSPED_EFD_Producao" indefinido em ' + sProcName);
    else Geral.MB_Erro('"FVersaoIsldCnjt" indefinido em ' + sProcName);
  end
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaOpeProcB_Conjunta(QrCab:
  TMySQLQuery; MovimID, PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; MovimNiv,
  PsqMovimNiv_VMI: TEstqMovimNiv; SQL_PeriodoComplexo, SQL_PeriodoVS,
  SQL_PeriodoPQ, SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc):
  Boolean;
const
  sProcName = 'TTFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaOpeProcB_Conjunta()"';
  //
  function ParteSQL(Tabela, FldSrc, FldDst, sArea, sPeso, sPeca, dArea, dPeso, dPeca: String; MovimNiv:
  TEstqmovimNiv): String;
  var
    SQL_IF: String;
  begin
    SQL_IF := 'IF(' + Copy(SQL_PeriodoVS, 4) + ', ';
    //
    Result := Geral.ATS([
    'SELECT vmi.Empresa, vmi.MovimID, vmi.Codigo, vmi.MovimCod,  ',
    'vmi.MovimNiv, vmi.Controle, ' +
    SQL_IF + sPeca + ', 0.000) Pecas, ' +
    SQL_IF + sArea + ', 0.000) AreaM2, ' +
    SQL_IF + sPeso + ', 0.000) PesoKg, ',
    'DATE(vmi.DataHora) Data,  ' + FldDst + ' GGX_Dst, ', //+ FldOri + ', ',
    FldSrc + ' GGX_Src, ',
    '  ',
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'sTipoEstq', True,
    sArea, sPeso, sPeca, 's'),
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'dTipoEstq', True,
    dArea, dPeso, dPeca, 'd'),
    //
    ' ',
    SQL_IF + dPeca + ', 0.000) QtdAntPeca, ' +
    SQL_IF + dArea + ', 0.000) QtdAntArM2, ' +
    SQL_IF + dPeso + ', 0.000) QtdAntPeso, ',
    'vmi.DtCorrApo, vmi.ClientMO, vmi.FornecMO, scc.EntiSitio ',
////////////////////////////////////////////////////////////////////////////////
    //'FROM ' + TMeuDB + '.' + Tabela + ' cab ',
    'FROM ' + FSEII_IMEcs + ' imc ',
    'LEFT JOIN ' + TMeuDB + '.' + Tabela + ' cab ON cab.MovimCod=imc.MovimCod ',
////////////////////////////////////////////////////////////////////////////////

    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '   vmi ON vmi.MovimCod=cab.MovimCod ',
    'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    sggx ON sggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    sgg1 ON sgg1.Nivel1=sggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    smed ON smed.Codigo=sgg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    sggc ON sggc.Controle=sggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  sgcc ON sgcc.Codigo=sggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  sgti ON sgti.Controle=sggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou sxco ON sxco.GraGruX=sggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    snv1 ON snv1.Codigo=sxco.CouNiv1   ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    snv2 ON snv2.Codigo=sxco.CouNiv2   ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    dggx ON dggx.Controle=cab.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    dgg1 ON dgg1.Nivel1=dggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    dmed ON dmed.Codigo=dgg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    dggc ON dggc.Controle=dggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  dgcc ON dgcc.Codigo=dggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  dgti ON dgti.Controle=dggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou dxco ON dxco.GraGruX=dggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    dnv1 ON dnv1.Codigo=dxco.CouNiv1   ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    dnv2 ON dnv2.Codigo=dxco.CouNiv2   ',
    ' ',
    //'WHERE imc.MovimID=' + Geral.FF0(Integer(PsqMovimID)),
  '']);
  end;
  procedure AvisoOpcEspecif(Txt: String);
  begin
    Geral.MB_Aviso('Configura��o inv�lida em op��es espec�ficas para ' +
    sProcName + ' para o item: ' + Geral.FF0(Integer(MovimNiv)) +
    ' - ' + sEstqMovimNiv[Integer(MovimNiv)] + Txt);
  end;
var
  SQLx: String;
  IncluiPQx: Boolean;
begin
  case MovimNiv of
    //  Couro em caleiro (origem: Couro PDA)
    TEstqmovimNiv.eminSorcCal:
    begin
      IncluiPQx := True;
      SQLx := ParteSQL('vscalcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCal);
      //
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      case Dmod.QrControleSPED_II_EmCal.Value of
(*
        2: InsereB_ProducaoVer1(SQLx, SQL_PeriodoPQ, PsqMovimID_VMI, PsqMovimNiv_VMI,
           OrigemOpeProc);
*)
        2: InsereB_ProducaoVer2_Fast('vscalcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2_Fast('vscalcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
      //
    end;
    TEstqmovimNiv.eminDestCal:
    begin
(*
26.31 n�o acha!
29.31 � o certo para Couro caleado!
*)
      IncluiPQx := False;
      SQLx := ParteSQL('vscaljmp', 'vmi.JmpGGX', 'vmi.GraGruX', '-vmi.QtdGerArM2',
      '-vmi.QtdGerPeso', '-vmi.QtdGerPeca',  'vmi.QtdGerArM2',
      'vmi.QtdGerPeso', 'vmi.QtdGerPeca', TEstqmovimNiv.eminDestCal);
      //
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //
      case Dmod.QrControleSPED_II_Caleado.Value of
(*
        2: InsereB_ProducaoVer1(SQLx, SQL_PeriodoPQ, PsqMovimID_VMI, PsqMovimNiv_VMI,
           OrigemOpeProc);
*)
        2: InsereB_ProducaoVer2_Fast('vscalcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2_Fast('vscalcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    TEstqmovimNiv.eminSorcCur:
    begin
      IncluiPQx := True;
      SQLx := ParteSQL('vscurcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCur);
      //
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //InsereB_ProducaoVer2('vscurcab', PsqMovimID, PsqMovimNiv, OrigemOpeProc);
      //testar
      case Dmod.QrControleSPED_II_EmCur.Value of
(*
        2: InsereB_ProducaoVer1(SQLx, SQL_PeriodoPQ, PsqMovimID_VMI, PsqMovimNiv_VMI,
           OrigemOpeProc);
*)
        2: InsereB_ProducaoVer2_Fast('vscurcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2_Fast('vscurcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    TEstqmovimNiv.eminDestCur:
    begin
      IncluiPQx := False;
(*
      SQLx := ParteSQL('vscaljmp', 'vmi.JmpGGX', 'vmi.GraGruX', '-vmi.QtdGerArM2',
      '-vmi.QtdGerPeso', '-vmi.QtdGerPeca',  'vmi.QtdGerArM2',
      'vmi.QtdGerPeso', 'vmi.QtdGerPeca', TEstqmovimNiv.eminDestCal);
*)
      SQLx := ParteSQL('vscurcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCur);
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //
      case Dmod.QrControleSPED_II_Caleado.Value of
(*
        2: InsereB_ProducaoVer1(SQLx, SQL_PeriodoPQ, PsqMovimID_VMI, PsqMovimNiv_VMI,
           OrigemOpeProc);
*)
        2: InsereB_ProducaoVer2_Fast('vscurcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2_Fast('vscurcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    TEstqmovimNiv.eminDestOper:
    begin
      IncluiPQx := True;
      SQLx := 'SELECT ???? FROM ????';
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //
      case Dmod.QrControleSPED_II_Operacao.Value of
        2: InsereB_ProducaoVer2_Fast('vsopecab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2_Fast('vsopecab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    TEstqmovimNiv.eminDestWEnd:
    begin
      IncluiPQx := True;
      SQLx := 'SELECT ???? FROM ????';
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //
      case Dmod.QrControleSPED_II_Recurt.Value of
        2: InsereB_ProducaoVer2_Fast('vspwecab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2_Fast('vspwecab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    TEstqmovimNiv.eminDestPSP:
    begin
      IncluiPQx := True;
      SQLx := 'SELECT ???? FROM ????';
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //
      case Dmod.QrControleSPED_II_SubProd.Value of
        2: InsereB_ProducaoVer2_Fast('vspspcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2_Fast('vspspcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    TEstqmovimNiv.eminDestRRM:
    begin
      IncluiPQx := True;
      SQLx := 'SELECT ???? FROM ????';
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //
      case Dmod.QrControleSPED_II_Reparo.Value of
        2: InsereB_ProducaoVer2_Fast('vsrrmcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2_Fast('vsrrmcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    //
    else
    begin
      IncluiPQx := False;
      SQLx := 'SELECT ???? FROM ????';
      Geral.MB_Erro('"MovimNiv" n�o implementado em ' + sProcName);
    end;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaOpeProcB_Isolada(QrCab: TMySQLQuery;
  PsqMovimNiv: TEstqMovimNiv; SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ,
  SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
  //
  function ParteSQL(Tabela, FldSrc, FldDst, sArea, sPeso, sPeca, dArea, dPeso, dPeca: String; MovimNiv:
  TEstqmovimNiv): String;
  var
    SQL_IF: String;
  begin
    SQL_IF := 'IF(' + Copy(SQL_PeriodoVS, 4) + ', ';
    //
    Result := Geral.ATS([
    'SELECT vmi.Empresa, vmi.MovimID, vmi.Codigo, vmi.MovimCod,  ',
    'vmi.MovimNiv, vmi.Controle, ' +
    SQL_IF + sPeca + ', 0.000) Pecas, ' +
    SQL_IF + sArea + ', 0.000) AreaM2, ' +
    SQL_IF + sPeso + ', 0.000) PesoKg, ',
    'DATE(vmi.DataHora) Data,  ' + FldDst + ' GGX_Dst, ', //+ FldOri + ', ',
    FldSrc + ' GGX_Src, ',
    '  ',
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'sTipoEstq', True,
    sArea, sPeso, sPeca, 's'),
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'dTipoEstq', True,
    dArea, dPeso, dPeca, 'd'),
    //
    ' ',
    SQL_IF + dPeca + ', 0.000) QtdAntPeca, ' +
    SQL_IF + dArea + ', 0.000) QtdAntArM2, ' +
    SQL_IF + dPeso + ', 0.000) QtdAntPeso, ',
    'vmi.DtCorrApo, vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
    'IF(gg1.Nivel2 <> 0 AND gg2.Tipo_Item>-1, gg2.Tipo_Item, pgt.Tipo_Item) Tipo_Item  ',
    'FROM ' + TMeuDB + '.' + Tabela + ' cab ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '   vmi ON vmi.MovimCod=cab.MovimCod ',
    'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    sggx ON sggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    sgg1 ON sgg1.Nivel1=sggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.gragru2    sgg2 ON sgg2.Nivel2=sgg1.Nivel1 ',
    'LEFT JOIN ' + TMeuDB + '.prdgruptip spgt ON spgt.Codigo=sgg1.PrdGrupTip ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    smed ON smed.Codigo=sgg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    sggc ON sggc.Controle=sggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  sgcc ON sgcc.Codigo=sggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  sgti ON sgti.Controle=sggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou sxco ON sxco.GraGruX=sggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    snv1 ON snv1.Codigo=sxco.CouNiv1   ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    snv2 ON snv2.Codigo=sxco.CouNiv2   ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    dggx ON dggx.Controle=cab.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    dgg1 ON dgg1.Nivel1=dggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.gragru2    dgg2 ON dgg2.Nivel2=dgg1.Nivel1 ',
    'LEFT JOIN ' + TMeuDB + '.prdgruptip dpgt ON dpgt.Codigo=dgg1.PrdGrupTip ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    dmed ON dmed.Codigo=dgg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    dggc ON dggc.Controle=dggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  dgcc ON dgcc.Codigo=dggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  dgti ON dgti.Controle=dggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou dxco ON dxco.GraGruX=dggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    dnv1 ON dnv1.Codigo=dxco.CouNiv1   ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    dnv2 ON dnv2.Codigo=dxco.CouNiv2   ',
    ' ',
    'WHERE (vmi.Empresa=' + Geral.FF0(FEmpresa),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    SQL_PeriodoVS,
  ') OR ',
  '( ',
  'vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  'AND cab.MovimCod IN ( ',
  'SELECT DISTINCT VSMovCod  ',
  'FROM ' + TMeuDB + '.emit  ',
  'WHERE Codigo IN (   ',
  '  SELECT OrigemCodi  ',
  '  FROM ' + TMeuDB + '.pqx  ',
  '  WHERE Tipo=110     ',
  '  ' + SQL_PeriodoPQ,
  ')   ',
  'AND VSMovCod <> 0  ',
  '  ',
  'UNION   ',
  '  ',
  'SELECT DISTINCT VSMovCod  ',
  'FROM ' + TMeuDB + '.pqo  ',
  'WHERE Codigo IN (   ',
  '  SELECT OrigemCodi  ',
  '  FROM ' + TMeuDB + '.pqx  ',
  '  WHERE Tipo=110     ',
  '  ' + SQL_PeriodoPQ,
  ')   ',
  'AND VSMovCod <> 0  ',
  ////
  ') ',
  ') ',
  '']);
  end;
const
  sProcName = '"TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaOpeProcB_Isolada()"';
  //SrcFator = -1;
  DstFator = 1;
  ESTSTabSorc = estsVMI;
var
  Pecas, AreaM2, PesoKg, QtdeSrc, QtdeDst, QtdAntArM2, QtdAntPeso, QtdAntPeca:
  Double;
  Data, DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  SrcTipoEstq, DstTipoEstq, GGXSrc, GGXDst, Codigo, MovimCod, IDSeq1,
  ID_Item, Controle, SrcFator, ClientMO, FornecMO, EntiSitio, Tipo_Item: Integer;
  MovimNiv: TEstqMovimNiv;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  COD_INS_SUBST, SQLx: String;
  MovimID: TEstqMovimID;
  MovimNivInn: TEstqMovimNiv;
begin
  COD_INS_SUBST := '';
  //
  case PsqMovimNiv of
    TEstqmovimNiv.eminSorcCal: SrcFator := 1;
    TEstqmovimNiv.eminDestCal: SrcFator := -1;
    TEstqmovimNiv.eminSorcCur: SrcFator := 1;
    TEstqmovimNiv.eminBaixCurtiXX: SrcFator := -1;
    TEstqmovimNiv.eminDestWEnd: SrcFator := -1; //???
    else
    begin
      SrcFator := -1;
      Geral.MB_Erro('"SrcFator" n�o implementado em ' + sProcName + sLineBreak
      + 'PsqMovimNiv = ' + GetEnumName(TypeInfo(TEstqmovimNiv), Integer(PsqMovimNiv)));
    end;
  end;
  case PsqMovimNiv of
    TEstqmovimNiv.eminSorcCal: SQLx :=
      ParteSQL('vscalcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCal);
    TEstqmovimNiv.eminDestCal: SQLx :=
      ParteSQL('vscaljmp', 'vmi.JmpGGX', 'vmi.GraGruX', '-vmi.QtdGerArM2',
      '-vmi.QtdGerPeso', '-vmi.QtdGerPeca',  'vmi.QtdGerArM2',
      'vmi.QtdGerPeso', 'vmi.QtdGerPeca', TEstqmovimNiv.eminDestCal);
    TEstqmovimNiv.eminSorcCur: SQLx :=
      ParteSQL('vscurcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCur);
    TEstqmovimNiv.eminBaixCurtiXX:
    begin
      // N�o fazer?
      // Vai duplicar a baixa?
      Exit;
    end;
    TEstqmovimNiv.eminDestWEnd:
    begin
      // N�o fazer?
      // Vai duplicar algo?
      Exit;
    end;
    else
    begin
      SQLx := 'SELECT ???? FROM ????';
      Geral.MB_Erro('"MovimNiv" n�o implementado em ' + sProcName);
      //Exit;
    end;
  end;

  UnDmkDAC_PF.AbreMySQLQuery0(QrMC, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_O_P;  ',
  'CREATE TABLE _SPED_EFD_K2XX_O_P  ',
  //
  SQLx,
  '; ',
  ' ',
  'SELECT DISTINCT MovimCod  ',
  'FROM _SPED_EFD_K2XX_O_P ',
  'ORDER BY MovimCod;  ',
  '']);
  //Geral.MB_SQL(Self, QrMC); //Parei aqui
  //
  if EfdIcmsIpi_PF.ErrGrandeza('_SPED_EFD_K2XX_O_P', 'GGx_Src', 'GGX_Dst' ) then
    Exit;
  //
  PB2.Position := 0;
  PB2.Max := QrMC.RecordCount;
  QrMC.First;
  //if QrMC.RecordCount > 0 then
    //Geral.MB_Aviso('DEPRRCAR!!! OpeProcA - 1');
  while not QrMC.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    UnDmkDAC_PF.AbreMySQLQuery0(QrProdRib, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM _SPED_EFD_K2XX_O_P ',
    'WHERE MovimCod=' + Geral.FF0(QrMCMovimCod.Value),
    '']);
    //Geral.MB_SQL(Self, QrProdRib); //Parei aqui!
    QrProdRib.First;
    while not QrProdRib.Eof do
    begin
      MovimID     := TEstqMovimID(QrProdRibMovimID.Value);
      AreaM2      := QrProdRibAreaM2.Value;
      PesoKg      := QrProdRibPesoKg.Value;
      Pecas       := QrProdRibPecas.Value;
      QtdAntArM2  := QrProdRibQtdAntArM2.Value;
      QtdAntPeso  := QrProdRibQtdAntPeso.Value;
      QtdAntPeca  := QrProdRibQtdAntPeca.Value;
      Data        := QrProdRibData.Value;
      SrcTipoEstq := Trunc(QrProdRibsTipoEstq.Value);
      DstTipoEstq := Trunc(QrProdRibdTipoEstq.Value);
      GGXSrc      := QrProdRibGGX_Src.Value;
      GGXDst      := QrProdRibGGX_Dst.Value;
      MovimCod    := QrProdRibMovimCod.Value;
      MovimNiv    := TEstqMovimNiv(QrProdRibMovimNiv.Value);
      Codigo      := QrProdRibCodigo.Value;
      Controle    := QrProdRibControle.Value;
      ID_Item     := Controle;
      //
      DtMovim     := Data;
      DtCorrApo   := QrProdRibDtCorrApo.Value;
      //
      ClientMO    := QrProdRibClientMO.Value;
      FornecMO    := QrProdRibFornecMO.Value;
      EntiSitio   := QrProdRibEntiSitio.Value;
      //
      if (ClientMO =0) or (FornecMO=0) or (EntiSitio=0) then
      begin
        MovimNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
        UnDmkDAC_PF.AbreMySQLQuery0(QrObtCliForSite, Dmod.MyDB, [
        'SELECT vmi.ClientMO, vmi.FornecMO, scc.EntiSitio  ',
        'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
        'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc  ',
        'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo  ',
        'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
        'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
        '']);
        ClientMO  := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
        FornecMO  := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
        EntiSitio  := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
      end;
      //
      //if (Pecas <> 0) and (DtHrFimOpe > 1) then
      if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
      begin
        case SrcTipoEstq of
          0: QtdeSrc := Pecas;
          1: QtdeSrc := AreaM2;
          2: QtdeSrc := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(SrcTipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[SrcTipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrProdRib.SQL.Text);
            //
            QtdeDst := Pecas;
          end;
        end;
        QtdeSrc := QtdeSrc * SrcFator;

        //
        case DstTipoEstq of
          1: QtdeDst := QtdAntArM2;
          2: QtdeDst := QtdAntPeso;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(SrcTipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[SrcTipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrProdRib.SQL.Text);
            //
            QtdeDst := QtdAntPeca;
          end;
        end;
        QtdeDst := QtdeDst * DstFator;
      end else
      begin
        QtdeSrc := 0;
        QtdeDst := 0;
      end;
      if (*TemTwn and*) (QtdeSrc = 0) then
        VerificaTipoEstqQtde(SrcTipoEstq, AreaM2, PesoKg, Pecas,
        GGXSrc, MovimCod, Controle, sProcName);
      if (*TemTwn and*) (QtdeDst = 0) then
        VerificaTipoEstqQtde(DstTipoEstq, AreaM2, PesoKg, Pecas,
        GGXDst, MovimCod, Controle, sProcName);
      //
      if QtdeSrc < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade "Src" n�o pode ser negativa!!! (5)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo), QrProdRib);
      if QtdeDst < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade "Dst" n�o pode ser negativa!!! (6)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), QrProdRib)
      else if (QtdeDst = 0) and (Data > 1) then
      begin
        case MovimID of
          emidEmProcCal: Data := 0;
          emidEmProcCur: Data := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade zerada para OP encerrada!!! (5)', QrProdRib);
        end;
      end;
      //
      //if QrSubPrdOPEmpresa.Value = QrFMOFornecMO.Value then

      if FEmpresa = FornecMO then
        TipoRegSPEDProd := trsp23X
      else
        TipoRegSPEDProd := trsp25X;
      //
      DtHrAberto := Data;
      DtHrAberto := Data;
      case TipoRegSPEDProd of
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod,
          Controle, DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst,
          ClientMO, FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
          FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
          //
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, DtHrAberto,
          DtCorrApo, GGXSrc, QtdeSrc, COD_INS_SUBST, ID_Item, Integer(MovimID),
          Codigo, MovimCod, Controle, ClientMO, FornecMO, EntiSitio,
          OrigemOpeProc, ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
          //
(*        Desabilitar?. PQ Ser� incluido no couro processado e n�o em processo!*)
          Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv, MovimCod,
          IDSeq1, SQL_PeriodoPQ, OrigemOpeProc, ClientMO, FornecMO, EntiSitio);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
          QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
          //
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXSrc,
          QtdeSrc, COD_INS_SUBST, ID_Item,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
          //
(*        Desabilitar?. PQ Ser� incluido no couro processado e n�o em processo!*)
          Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv, MovimCod,
          IDSeq1, SQL_PeriodoPQ, OrigemOpeProc, ClientMO, FornecMO, EntiSitio);

        end;
        else Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(6)');
      end;
      QrProdRib.Next;
    end;
    QrMC.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_OrigeProcPQ_Conjunta_Fast(
  TipoRegSPEDProd: TTipoRegSPEDProd; MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv; MovimCod, IDSeq1: Integer; SQL_PeriodoPQ: String;
  OrigemOpeProc: TOrigemOpeProc; ClientMO, FornecMO,
  EntiSitio: Integer): Boolean;
const
  Fator = -1;
  OriESTSTabSorc = estsPQx;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_OrigeProcPQ_Conjunta_Fast()';
  JmpMovID = 0;
  JmpMovCod = 0;
  JmpNivel1 = 0;
  JmpNivel2 = 0;
  PaiMovID = 0;
  MovCodPai = 0;
  PaiNivel1 = 0;
  PaiNivel2 = 0;
var
  DtMovim, DtCorrApo: TDateTime;
  Codigo, Controle, GraGruX(*, ClientMO, FornecMO, EntiSitio*): Integer;
  MyQtd, Qtde: Double;
begin
  Result := True;
  VS_EFD_ICMS_IPI.ReopenVSOriPQx_Fast(DqOri, MovimCod, SQL_PeriodoPQ, False);
  //
  //Geral.MB_SQL(Self, QrOri);
  DqOri.First;
  while not DqOri.Eof do
  begin
    Codigo    := USQLDB.Int(DqOri, 'OrigemCodi');
    Controle  := USQLDB.Int(DqOri, 'OrigemCtrl');
    GraGruX   := UnPQx.ObtemGGXdeInsumo_Fast(USQLDB.Int(DqOri, 'Insumo'));
    MyQtd     := USQLDB.Flu(DqOri, 'Peso');
    Qtde      := MyQtd * Fator;
    DtMovim   := USQLDB.DtH(DqOri, 'DataX');
    DtCorrApo := USQLDB.DtH(DqOri, 'DtCorrApo');
    //
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (5)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      'ID Item: ' + Geral.FF0(Controle) + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Peso: ' + Geral.FFT(MyQtd, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    if Qtde > 0 then
    begin
      case TipoRegSPEDProd of
        trsp29X:
        begin
          // K292 - Insumos consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292_Fast(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
          MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          TEstqSPEDTabSorc.estsPQX, MeAviso, F_BNCI_IDSeq2_K292);
        end;
        trsp30X:
        begin
          // K302 - Insumos consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302_Fast(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
          MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          TEstqSPEDTabSorc.estsPQX, MeAviso, F_BNCI_IDSeq2_K302);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(7)');
      end;
    end;
    //
    DqOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_OrigeProcPQ_Conjunta(
  TipoRegSPEDProd: TTipoRegSPEDProd; MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv; MovimCod, IDSeq1: Integer; SQL_PeriodoPQ: String;
  OrigemOpeProc: TOrigemOpeProc; ClientMO, FornecMO,
  EntiSitio: Integer): Boolean;
const
  Fator = -1;
  OriESTSTabSorc = estsPQx;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_OrigeProcPQ_Conjunta()';
  JmpMovID = 0;
  JmpMovCod = 0;
  JmpNivel1 = 0;
  JmpNivel2 = 0;
  PaiMovID = 0;
  MovCodPai = 0;
  PaiNivel1 = 0;
  PaiNivel2 = 0;
var
  DtMovim, DtCorrApo: TDateTime;
  Codigo, Controle, GraGruX(*, ClientMO, FornecMO, EntiSitio*): Integer;
  MyQtd, Qtde: Double;
begin
  Result := True;
  VS_EFD_ICMS_IPI.ReopenVSOriPQx(QrOri, MovimCod, SQL_PeriodoPQ, False);
  //
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  while not QrOri.Eof do
  begin
    Codigo    := QrOri.FieldByName('OrigemCodi').AsInteger;
    Controle  := QrOri.FieldByName('OrigemCtrl').AsInteger;
    GraGruX   := UnPQx.ObtemGGXdeInsumo(QrOri.FieldByName('Insumo').AsInteger);
    MyQtd     := QrOri.FieldByName('Peso').AsFloat;
    Qtde      := MyQtd * Fator;
    DtMovim   := QrOri.FieldByName('DataX').AsDateTime;
    DtCorrApo := QrOri.FieldByName('DtCorrApo').AsDateTime;
    //
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (5)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      //'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'ID Item: ' + Geral.FF0(Controle) + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Peso: ' + Geral.FFT(MyQtd, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    if Qtde > 0 then
    begin
      case TipoRegSPEDProd of
        trsp29X:
        begin
          // K292 - Insumos consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
          MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          TEstqSPEDTabSorc.estsPQX, MeAviso);
        end;
        trsp30X:
        begin
          // K302 - Insumos consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
          MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          TEstqSPEDTabSorc.estsPQX, MeAviso);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(7)');
      end;
    end;
    //
    QrOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_OrigeProcPQ_Isolada_Fast(
  TipoRegSPEDProd: TTipoRegSPEDProd; MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv; MovimCod, IDSeq1: Integer; SQL_PeriodoPQ: String;
  OrigemOpeProc: TOrigemOpeProc; ClientMO, FornecMO,
  EntiSitio: Integer; DtHrAberto: TDateTime): Boolean;
const
  Fator = -1;
  OriESTSTabSorc = estsPQx;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_OrigeProcPQ_Isolada_Fast()';
  JmpMovID = 0;
  JmpMovCod = 0;
  JmpNivel1 = 0;
  JmpNivel2 = 0;
  PaiMovID = 0;
  MovCodPai = 0;
  PaiNivel1 = 0;
  PaiNivel2 = 0;
  COD_INS_SUBST = '';
var
  DtMovim, DtCorrApo: TDateTime;
  Codigo, Controle, GraGruX(*, ClientMO, FornecMO, EntiSitio*): Integer;
  MyQtd, Qtde: Double;
begin
  Result := True;
  //
  VS_EFD_ICMS_IPI.ReopenVSOriPQx_Fast(DqOri, MovimCod, SQL_PeriodoPQ, False);
  //
  //Geral.MB_SQL(Self, DqOri);
  DqOri.First;
  while not DqOri.Eof do
  begin
    Codigo    := USQLDB.Int(DqOri, 'OrigemCodi');
    Controle  := USQLDB.Int(DqOri, 'OrigemCtrl');
    GraGruX   := UnPQx.ObtemGGXdeInsumo_Fast(USQLDB.Int(DqOri, 'Insumo'));
    MyQtd     := USQLDB.Flu(DqOri, 'Peso');
    Qtde      := MyQtd * Fator;
    DtMovim   := USQLDB.DtH(DqOri, 'DataX');
    DtCorrApo := USQLDB.DtH(DqOri, 'DtCorrApo');
    //
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (6)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      //'Grandeza: ' + sGrandezaUnidMed[DqOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'ID Item: ' + Geral.FF0(Controle) + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Peso: ' + Geral.FFT(MyQtd, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    if Qtde > 0 then
    begin
      case TipoRegSPEDProd of
        trsp23X:
        begin
(*
          // K292 - Insumos consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
          MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          TEstqSPEDTabSorc.estsPQX, MeAviso);
*)
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235_Fast(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, DtHrAberto,
          DtCorrApo, GraGruX, Qtde, COD_INS_SUBST, Controle, Integer(MovimID),
          Codigo, MovimCod, Controle, ClientMO, FornecMO, EntiSitio,
          OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso,
          F_BNCI_IDSeq2_K235);
        end;
        trsp25X:
        begin
(*
          // K302 - Insumos consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
          MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          TEstqSPEDTabSorc.estsPQX, MeAviso);
*)
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255_Fast(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GraGruX, Qtde, COD_INS_SUBST,
          Controle, Integer(MovimID), Codigo, MovimCod, Controle, ClientMO,
          FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          MeAviso, F_BNCI_IDSeq2_K255);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(8)');
      end;
    end;
    //
    DqOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_OrigeProcPQ_Isolada(
  TipoRegSPEDProd: TTipoRegSPEDProd; MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv; MovimCod, IDSeq1: Integer; SQL_PeriodoPQ: String;
  OrigemOpeProc: TOrigemOpeProc; ClientMO, FornecMO,
  EntiSitio: Integer; DtHrAberto: TDateTime): Boolean;
const
  Fator = -1;
  OriESTSTabSorc = estsPQx;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_OrigeProcPQ_Isolada()';
  JmpMovID = 0;
  JmpMovCod = 0;
  JmpNivel1 = 0;
  JmpNivel2 = 0;
  PaiMovID = 0;
  MovCodPai = 0;
  PaiNivel1 = 0;
  PaiNivel2 = 0;
  COD_INS_SUBST = '';
var
  DtMovim, DtCorrApo: TDateTime;
  Codigo, Controle, GraGruX(*, ClientMO, FornecMO, EntiSitio*): Integer;
  MyQtd, Qtde: Double;
begin
  Result := True;
  VS_EFD_ICMS_IPI.ReopenVSOriPQx(QrOri, MovimCod, SQL_PeriodoPQ, False);
  //
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  while not QrOri.Eof do
  begin
    Codigo    := QrOri.FieldByName('OrigemCodi').AsInteger;
    Controle  := QrOri.FieldByName('OrigemCtrl').AsInteger;
    GraGruX   := UnPQx.ObtemGGXdeInsumo(QrOri.FieldByName('Insumo').AsInteger);
    MyQtd     := QrOri.FieldByName('Peso').AsFloat;
    Qtde      := MyQtd * Fator;
    DtMovim   := QrOri.FieldByName('DataX').AsDateTime;
    DtCorrApo := QrOri.FieldByName('DtCorrApo').AsDateTime;
    //
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (6)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      //'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'ID Item: ' + Geral.FF0(Controle) + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Peso: ' + Geral.FFT(MyQtd, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    if Qtde > 0 then
    begin
      case TipoRegSPEDProd of
        trsp23X:
        begin
(*
          // K292 - Insumos consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
          MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          TEstqSPEDTabSorc.estsPQX, MeAviso);
*)
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, DtHrAberto,
          DtCorrApo, GraGruX, Qtde, COD_INS_SUBST, Controle, Integer(MovimID),
          Codigo, MovimCod, Controle, ClientMO, FornecMO, EntiSitio,
          OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
        trsp25X:
        begin
(*
          // K302 - Insumos consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
          MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          TEstqSPEDTabSorc.estsPQX, MeAviso);
*)
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GraGruX, Qtde, COD_INS_SUBST,
          Controle, Integer(MovimID), Codigo, MovimCod, Controle, ClientMO,
          FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          MeAviso);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(8)');
      end;
    end;
    //
    QrOri.Next;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_ProducaoVer2_Fast(TabCab:
  String; TargetMovimID, PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; PsqMovimNiv:
   TEstqMovimNiv; OrigemOpeProc: TOrigemOpeProc; FormaInsercaoItemProducao:
   TFormaInsercaoItemProducao; IncluiPQX: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_ProducaoVer2_Fast()';
  DstFator  = 1;
  OriFator  = 1;
var
  IDSeq1_K290, IDSeq1_K300: Integer;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  //
  MovimID: TEstqMovimID;
  Codigo, MovimCod, ClientMO, FornecMO, EntiSitio, TipoEstq, CtrlDst, GGXDst,
  TipoEstqDst, TipoEstqOri, CtrlOri, GGXOri, CodDocOP, StqCenLoc: Integer;
  DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  OriESTSTabSorc: TEstqSPEDTabSorc;
  //
  MovimNiv: TEstqMovimNiv;
  MovimTwn, CtrlPsq, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
  MovCodPai, PaiNivel1, PaiNivel2, Empresa, Controle, GraGruX,
  GerMovID, GerNivel1, GerNivel2, GerGGX, Tipo_Item: Integer;
  InseriuPQ, InsumoSemProducao: Boolean;
  Produziu: TProduziuVS;
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdAntPeca, QtdAntPeso, QtdAntArM2: Double;
  InsercaoItemProducaoConjunta: TInsercaoItemProducaoConjunta;
  //
  procedure AvisoFip(ProcNome: String);
  begin
    Geral.MB_Aviso('FormaInsercaoItemProducao n�o definida em ' + sProcName +
    '.' + ProcNome);
  end;
var
  FatherMovCodPai, IMECsCodigo, IMECsMovimID, IMECsMovCodPai, IMECsJmpNivel2,
  I, K: Integer;
  JaFoi: Boolean;
const
  DqIMECs_Flds        = 'SELECT DISTINCT Codigo, MovimID, MovCodPai';
  DqIMECs_FldsCaleado = 'SELECT DISTINCT Codigo, MovimID, MovCodPai, JmpNivel2';
begin
{
  Fazer!
  Acrescenatar:
OR  DtCorrApo BETWEEN
"2018-09-01" AND "2018-10-31 23:59:59"
  Acrescenatar:
  vsXXXcab.DtHrAberto e DtHrFimOpe e PesoKgSrc e PcKgSrc <> 0
  Acrescentar....
}

{


DROP TABLE IF EXISTS _IMEIS_SPED_DIRECT_PART1;

CREATE TABLE _IMEIS_SPED_DIRECT_PART1
SELECT vmi.*
FROM _IMEIS_SPED_DIRECT isd
LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=isd.Controle
;

/******************************************/
/******************************************/
/******************************************/
/******************************************/
/******************************************/

DROP TABLE IF EXISTS _IMCES_SPED_INVOLVED_;

CREATE TABLE _IMCES_SPED_INVOLVED_

SELECT DISTINCT vmi.MovimCod
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi
LEFT JOIN _IMEIS_SPED_DIRECT_PART1 isd ON isd.Controle=vmi.Controle
WHERE isd.SrcNivel1<>0
AND isd.SrcMovID<>0
AND isd.SrcNivel1=vmi.SrcNivel1
AND isd.SrcMovID=vmi.SrcMovID

UNION

SELECT DISTINCT vmi.MovimCod
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi
LEFT JOIN _IMEIS_SPED_DIRECT_PART1 isd ON isd.Controle=vmi.Controle
WHERE isd.DstNivel1<>0
AND isd.DstMovID<>0
AND isd.DstNivel1=vmi.DstNivel1
AND isd.DstMovID=vmi.DstMovID

UNION

SELECT DISTINCT vmi.MovimCod
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi
LEFT JOIN _IMEIS_SPED_DIRECT_PART1 isd ON isd.Controle=vmi.Controle
WHERE isd.JmpNivel1<>0
AND isd.JmpMovID<>0
AND isd.JmpNivel1=vmi.JmpNivel1
AND isd.JmpMovID=vmi.JmpMovID

UNION

SELECT DISTINCT vmi.MovimCod
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi
LEFT JOIN _IMEIS_SPED_DIRECT_PART1 isd ON isd.Controle=vmi.Controle
WHERE isd.RmsNivel1<>0
AND isd.RmsMovID<>0
AND isd.RmsNivel1=vmi.RmsNivel1
AND isd.RmsMovID=vmi.RmsMovID
;

/******************************************/
/******************************************/
/******************************************/
/******************************************/
/******************************************/

DROP TABLE IF EXISTS _IMEIS_SPED_DIRECT_PART2;

CREATE TABLE _IMEIS_SPED_DIRECT_PART2

SELECT Controle
FROM _IMEIS_SPED_DIRECT_PART1

UNION

SELECT vmi.Controle
FROM _IMCES_SPED_INVOLVED_ isi
LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON vmi.MovimCod=isi.MovimCod
;

SELECT vmi.*
FROM _IMEIS_SPED_DIRECT_PART1 isd
LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON isd.Controle=vmi.Controle

}

{
DROP TABLE IF EXISTS _IMEIS_SPED_DIRECT;

CREATE TABLE _IMEIS_SPED_DIRECT

SELECT Controle
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE DataHora BETWEEN
"2018-09-01" AND "2018-10-31 23:59:59"

UNION

SELECT SrcNivel2 Controle
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE DataHora BETWEEN
"2018-09-01" AND "2018-10-31 23:59:59"
AND SrcNivel2 <> 0

UNION

SELECT DstNivel2 Controle
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE DataHora BETWEEN
"2018-09-01" AND "2018-10-31 23:59:59"
AND DstNivel2 <> 0

UNION

SELECT GSPArtNiv2 Controle
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE DataHora BETWEEN
"2018-09-01" AND "2018-10-31 23:59:59"
AND GSPArtNiv2 <> 0

UNION

SELECT JmpNivel2 Controle
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE DataHora BETWEEN
"2018-09-01" AND "2018-10-31 23:59:59"
AND JmpNivel2 <> 0

UNION

SELECT RmsNivel2 Controle
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE DataHora BETWEEN
"2018-09-01" AND "2018-10-31 23:59:59"
AND RmsNivel2 <> 0

UNION

SELECT GSPJmpNiv2 Controle
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE DataHora BETWEEN
"2018-09-01" AND "2018-10-31 23:59:59"
AND GSPJmpNiv2 <> 0;

DROP TABLE IF EXISTS _IMEIS_SPED_DIRECT_ALL;

CREATE TABLE _IMEIS_SPED_DIRECT_ALL
SELECT vmi.*
FROM _IMEIS_SPED_DIRECT isd
LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=isd.Controle
}

  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqFather, DModG.MyPID_CompressDB, [
  'SELECT DISTINCT MovCodPai ',
  'FROM  ' + FSEII_IMEcs,
  'ORDER BY MovCodPai',
  '']);
  //Geral.MB_SQL(Self, DqFather);
  PB2.Position := 0;
  PB2.Max := DqFather.RecordCount;
  DqFather.First;
  while not DqFather.Eof do
  begin
    //DqFatherMovCodPai_Value := Geral.IMV(DqFather.FieldValues[00]);
    //FatherMovCodPai := Geral.IMV(DqFather.FieldValueByFieldName('MovCodPai'));
    FatherMovCodPai := USQLDB.Int(DqFather, 'MovCodPai');
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    //
    InseriuPQ := False;
    IDSeq1_K290 := -1;
    IDSeq1_K300 := -1;
    //
    case TargetMovimID of
      TEStqMovimID.emidEmOperacao,
      TEStqMovimID.emidEmProcWE,
      TEStqMovimID.emidEmProcSP,
      TEStqMovimID.emidEmReprRM:
      begin
        UnDmkDAC_PF.AbreMySQLDirectQuery0(DqIMECs, DModG.MyPID_CompressDB, [
        DqIMECs_Flds,
        'FROM  ' + FSEII_IMEcs,
        'WHERE MovCodPai=' + Geral.FF0(FatherMovCodPai),
        //'ORDER BY '
        'LIMIT 1 ',
        '']);
      end;
      TEStqMovimID.emidEmProcCal,
      TEStqMovimID.emidEmProcCur,
      TEStqMovimID.emidCurtido:
      begin
        UnDmkDAC_PF.AbreMySQLDirectQuery0(DqIMECs, DModG.MyPID_CompressDB, [
        DqIMECs_Flds,
        'FROM  ' + FSEII_IMEcs,
        'WHERE MovCodPai=' + Geral.FF0(FatherMovCodPai),
        '']);
        //*Geral.MB_SQL(Self, DqIMECs);
      end;
      TEStqMovimID.emidCaleado:
      begin
        UnDmkDAC_PF.AbreMySQLDirectQuery0(DqIMECs, DModG.MyPID_CompressDB, [
        DqIMECs_FldsCaleado,
        'FROM  ' + FSEII_IMEcs + '',
        'WHERE MovCodPai=' + Geral.FF0(FatherMovCodPai),
        '']);
        //Geral.MB_SQL(Self, DqIMECs);
      end;
      else begin
        Geral.MB_Erro(
        GetEnumName(TypeInfo(TEStqMovimID), Integer(TargetMovimID)) +
        ' indefinido em ' + sprocName + ' (1)');
        //
        UnDmkDAC_PF.AbreMySQLDirectQuery0(DqIMECs, DModG.MyPID_CompressDB, [
        DqIMECs_Flds,
        'FROM  ' + FSEII_IMEcs,
        'WHERE MovCodPai=' + Geral.FF0(FatherMovCodPai),
        '']);
      end;
    end;
    //Geral.MB_SQL(Self, DqIMECs);
//  At� aqui � super r�pido!
    while not DqIMECs.Eof do
    begin
      //DqIMECs_Flds = 'SELECT Codigo, Controle, MovimID, MovCodPai, JmpNivel2';
      //IMECsCodigo    := Geral.IMV(DqIMECs.FieldValueByFieldName('Codigo'));
      IMECsCodigo    := USQLDB.Int(DqIMECs, 'Codigo');
      //IMECsMovimID   := Geral.IMV(DqIMECs.FieldValueByFieldName('MovimID'));
      IMECsMovimID   := USQLDB.Int(DqIMECs, 'MovimID');
      //IMECsMovCodPai := Geral.IMV(DqIMECs.FieldValueByFieldName('MovCodPai'));
      IMECsMovCodPai := USQLDB.Int(DqIMECs, 'MovCodPai');
      if TargetMovimID = TEStqMovimID.emidCaleado then
        //IMECsJmpNivel2 := Geral.IMV(DqIMECs.FieldValueByFieldName('JmpNivel2'));
        IMECsJmpNivel2 := USQLDB.Int(DqIMECs, 'JmpNivel2')
      else
        IMECsJmpNivel2 := 0;
      //
      InsercaoItemProducaoConjunta := iipcIndef;
      CodDocOP  := IMECsMovCodPai;
      UnDmkDAC_PF.AbreMySQLDirectQuery0(DqProc, DModG.MyCompressDB, [
      'SELECT DtHrAberto, DtHrFimOpe ',
      'FROM ' + LowerCase(TabCab),
      'WHERE MovimCod=' + Geral.FF0(CodDocOP),
      '']);
      //
{} // A
      //case PsqMovimID of
      case TargetMovimID of
        TEStqMovimID.emidEmProcCal:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := IMECsMovimID;
          MovCodPai := IMECsMovCodPai;
          PaiNivel1 := IMECsCodigo;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss_Fast(DqVSRibAtu, DqVSRibOriIMEI,
          (*DqVSRibOriPallet*)nil, (*DqVSRibDst*)nil, (*DqVSRibInd*)nil,
          (*DqSubPrd*)nil, (*DqDescl*)nil, (*DqForcados*)nil,
          (*DqEmit*)nil, (*DqPQO*)nil, (*IMECsCodigo*)PaiNivel1,
          (*IMECsMovimCod*) MovCodPai, (*IMECsTemIMEIMrt*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto,
          emidEmProcCal, emidCaleado, emidCaleado,
          //MovNivInn, MovNivSrc, MovNivDst:           DB_IMEI
          eminEmCalInn, eminSorcCal, eminDestCal, FTabIMEI, FDB_IMEI,
          FBaseDadosDiminuida);
          InsercaoItemProducaoConjunta := iipcUniAnt;
        end;
        TEStqMovimID.emidCaleado:
        begin
          //  Couro caleado tirado direto do caleiro
          if IMECsJmpNivel2 = 0 then
          begin
            JmpMovID  := 0;
            JmpMovCod := 0;
            JmpNivel1 := 0;
            JmpNivel2 := 0;
            PaiMovID  := IMECsMovimID;
            MovCodPai := IMECsMovCodPai;
            PaiNivel1 := IMECsCodigo;
            PaiNivel2 := 0;
            InsercaoItemProducaoConjunta := iipcGemeos;
          end else
          begin
            CtrlPsq := IMECsJmpNivel2;
            //
            UnDmkDAC_PF.AbreMySQLDirectQuery0(DqJmpA, DModG.MyCompressDB, [
            'SELECT *  ',
            'FROM ' + CO_SEL_TAB_VMI + ' ',
            'WHERE Controle=' + Geral.FF0(CtrlPsq),
            '']);
            //
(*
            CtrlPsq   := Geral.IMV(DqJmpA.FieldValueByFieldName('SrcNivel2'));
            JmpMovID  := Geral.IMV(DqJmpA.FieldValueByFieldName('MovimID'));
            JmpMovCod := Geral.IMV(DqJmpA.FieldValueByFieldName('MovimCod'));
            JmpNivel1 := Geral.IMV(DqJmpA.FieldValueByFieldName('Codigo'));
            JmpNivel2 := Geral.IMV(DqJmpA.FieldValueByFieldName('Controle'));
            //
*)
            CtrlPsq   := USQLDB.Int(DqJmpA, 'SrcNivel2');
            JmpMovID  := USQLDB.Int(DqJmpA, 'MovimID');
            JmpMovCod := USQLDB.Int(DqJmpA, 'MovimCod');
            JmpNivel1 := USQLDB.Int(DqJmpA, 'Codigo');
            JmpNivel2 := USQLDB.Int(DqJmpA, 'Controle');
            //
            UnDmkDAC_PF.AbreMySQLDirectQuery0(DqJmpB, DModG.MyCompressDB, [
            'SELECT *  ',
            'FROM ' + CO_SEL_TAB_VMI + ' ',
            'WHERE Controle=' + Geral.FF0(CtrlPsq),
            '']);
            //
(*
            PaiMovID  := Geral.IMV(DqJmpB.FieldValueByFieldName('MovimID'));
            MovCodPai := Geral.IMV(DqJmpB.FieldValueByFieldName('MovimCod'));
            PaiNivel1 := Geral.IMV(DqJmpB.FieldValueByFieldName('Codigo'));
            PaiNivel2 := Geral.IMV(DqJmpB.FieldValueByFieldName('Controle'));
*)
            PaiMovID  := USQLDB.Int(DqJmpB, 'MovimID');
            MovCodPai := USQLDB.Int(DqJmpB, 'MovimCod');
            PaiNivel1 := USQLDB.Int(DqJmpB, 'Codigo');
            PaiNivel2 := USQLDB.Int(DqJmpB, 'Controle');
            //
            InsercaoItemProducaoConjunta := iipcIndireta;
          end;
          //
          JaFoi := False;
          K := Length(FListaExec);
          for I := 0 to K - 1 do
          begin
            if (FListaExec[I][0] = PaiNivel1)
            and (FListaExec[I][1] = MovCodPai) then
            begin
              JaFoi := True;
              Break;
            end;
          end;
          if JaFoi = False then
          begin
            K := K + 1;
            SetLength(FListaExec, K);
            FListaExec[K-1][0] := PaiNivel1;
            FListaExec[K-1][1] := MovCodPai;
          end;
          if JaFoi then
            InsercaoItemProducaoConjunta := iipcDuplicado;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss_Fast(DqVSRibAtu, (*DqVSRibOriIMEI*)nil,
          (*DqVSRibOriPallet*)nil, DqVSRibDst, DqVSRibInd,
          (*DqSubPrd*)nil, (*DqDescl*)nil, (*DqForcados*)nil,
          (*DqEmit*)nil, (*DqPQO*)nil, (*IMECsCodigo*)PaiNivel1,
          (*IMECsMovimCod*) MovCodPai, (*IMECsTemIMEIMrt*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto,
          emidEmProcCal, emidCaleado, emidCaleado,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCalInn, eminSorcCal, eminDestCal, FTabIMEI, FDB_IMEI,
          FBaseDadosDiminuida);
        end;
        TEStqMovimID.emidEmProcCur:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := IMECsMovimID;
          MovCodPai := IMECsMovCodPai;
          PaiNivel1 := IMECsCodigo;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss_Fast(DqVSRibAtu, DqVSRibOriIMEI,
          (*DqVSRibOriPallet*)nil, (*DqVSRibDst*)nil, (*DqVSRibInd*)nil,
          (*DqSubPrd*)nil, (*DqDescl*)nil, (*DqForcados*)nil,
          (*DqEmit*)nil, (*DqPQO*)nil, (*IMECsCodigo*)PaiNivel1,
          (*IMECsMovimCod*) MovCodPai, (*IMECsTemIMEIMrt*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto,
          emidEmProcCur, emidIndsXX, emidEmProcCur,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCurInn, eminSorcCur, eminDestCur, FTabIMEI, FDB_IMEI,
          FBaseDadosDiminuida);
          InsercaoItemProducaoConjunta := iipcUniAnt;
        end;
        TEStqMovimID.emidCurtido:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := IMECsMovimID;
          MovCodPai := IMECsMovCodPai;
          PaiNivel1 := IMECsCodigo;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss_Fast(DqVSRibAtu, (*DqVSRibOriIMEI*)nil,
          (*DqVSRibOriPallet*)nil, DqVSRibDst, DqVSRibInd,
          (*DqSubPrd*)nil, (*DqDescl*)nil, (*DqForcados*)nil,
          (*DqEmit*)nil, (*DqPQO*)nil, (*IMECsCodigo*)PaiNivel1,
          (*IMECsMovimCod*) MovCodPai, (*IMECsTemIMEIMrt*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmProcCur, emidIndsXX, emidEmProcCur,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCurInn, eminSorcCur, eminDestCur, FTabIMEI, FDB_IMEI,
          FBaseDadosDiminuida);
          InsercaoItemProducaoConjunta := iipcUniGer;
          //Geral.MB_SQL(Self, DqVSRibAtu);
          //Geral.MB_SQL(Self, DqVSRibDst);
          //Geral.MB_SQL(Self, DqVSRibInd);
        end;
        TEStqMovimID.emidEmOperacao:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := IMECsMovimID;
          MovCodPai := IMECsMovCodPai;
          PaiNivel1 := IMECsCodigo;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss_Fast(DqVSRibAtu, (*DqVSRibOriIMEI*)nil,
          (*DqVSRibOriPallet*)nil, DqVSRibDst, (*DqVSRibInd*)nil,
          (*DqSubPrd*)nil, (*DqDescl*)nil, (*DqForcados*)nil,
          (*DqEmit*)nil, (*DqPQO*)nil, (*DqVSRibCabCodigo*)PaiNivel1,
          (*DqVSRibCabMovimCod*)MovCodPai,(*DqVSRibCabTemIMEIMrt*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmOperacao, emidEmOperacao, emidEmOperacao,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmOperInn, eminSorcOper, eminDestOper, FTabIMEI, FDB_IMEI,
          FBaseDadosDiminuida);
          InsercaoItemProducaoConjunta := iipcGemeos;
        end;
        TEStqMovimID.emidEmProcWE:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := IMECsMovimID;
          MovCodPai := IMECsMovCodPai;
          PaiNivel1 := IMECsCodigo;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss_Fast(DqVSRibAtu, (*DqVSRibOriIMEI*)nil,
          (*DqVSRibOriPallet*)nil, DqVSRibDst, (*DqVSRibInd*)nil,
          (*DqSubPrd*)nil, (*DqDescl*)nil, (*DqForcados*)nil,
          (*DqEmit*)nil, (*DqPQO*)nil, (*DqVSRibCabCodigo*)PaiNivel1,
          (*DqVSRibCabMovimCod*)MovCodPai,(*DqVSRibCabTemIMEIMrt*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmProcWE, emidFinished, emidEmProcWE,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmWEndInn, eminSorcWEnd, eminDestWEnd, FTabIMEI, FDB_IMEI,
          FBaseDadosDiminuida);
          InsercaoItemProducaoConjunta := iipcGemeos;
          //Geral.MB_SQL(Self, DqVSRibAtu);
          //Geral.MB_SQL(Self, DqVSRibDst);
          //Geral.MB_SQL(Self, DqVSRibInd);
        end;
        TEStqMovimID.emidEmProcSP:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := IMECsMovimID;
          MovCodPai := IMECsMovCodPai;
          PaiNivel1 := IMECsCodigo;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss_Fast(DqVSRibAtu, (*DqVSRibOriIMEI*)nil,
          (*DqVSRibOriPallet*)nil, DqVSRibDst, (*DqVSRibInd*)nil,
          (*DqSubPrd*)nil, (*DqDescl*)nil, (*DqForcados*)nil,
          (*DqEmit*)nil, (*DqPQO*)nil, (*DqVSRibCabCodigo*)PaiNivel1,
          (*DqVSRibCabMovimCod*)MovCodPai,(*DqVSRibCabTemIMEIMrt*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmProcSP, emidEmProcSP, emidEmProcSP,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmPSPInn, eminSorcPSP, eminDestPSP, FTabIMEI, FDB_IMEI,
          FBaseDadosDiminuida);
          InsercaoItemProducaoConjunta := iipcGemeos;
        end;
        TEStqMovimID.emidEmReprRM:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := IMECsMovimID;
          MovCodPai := IMECsMovCodPai;
          PaiNivel1 := IMECsCodigo;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss_Fast(DqVSRibAtu, (*DqVSRibOriIMEI*)nil,
          (*DqVSRibOriPallet*)nil, DqVSRibDst, (*DqVSRibInd*)nil,
          (*DqSubPrd*)nil, (*DqDescl*)nil, (*DqForcados*)nil,
          (*DqEmit*)nil, (*DqPQO*)nil, (*DqVSRibCabCodigo*)PaiNivel1,
          (*DqVSRibCabMovimCod*)MovCodPai,(*DqVSRibCabTemIMEIMrt*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmReprRM, emidEmReprRM, emidEmReprRM,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmRRMInn, eminSorcRRM, eminDestRRM, FTabIMEI, FDB_IMEI,
          FBaseDadosDiminuida);
          InsercaoItemProducaoConjunta := iipcGemeos;
        end;
        else Geral.MB_Erro('MovimID ' + Geral.FF0(Integer(TargetMovimID)) +
        ' n�o implementado em ' + sProcName + ' (2)');
      end;
{ // B }
      //
      //Geral.MB_SQL(Self, QrVSRibDst);
      case InsercaoItemProducaoConjunta of
        iipcDuplicado:
        begin
          // Duplicado!!!
          // N�o faz nada!
            //
          VAR_INTENS_EXECUTADOS_Count := VAR_INTENS_EXECUTADOS_Count + 1;
          VAR_INTENS_EXECUTADOS_Texto := VAR_INTENS_EXECUTADOS_Texto +
            Geral.FF0(VAR_INTENS_EXECUTADOS_Count) + ' - Codigo ' +
            Geral.FF0(PaiNivel1) + ' - MovimCod = ' + Geral.FF0(MovCodPai) + sLineBreak;
        end;
        //iipcIndef=0,
        iipcGemeos:
        begin
(*
          2019-01-27 - Fazer aqui!!
          Se QrVSRibDst n�o tem itens, quer dizer que tem um processo em aberto,
          mas sem nenhum artigo pronto ainda! Nesse caso tem que ver se j� tem
          consumo de insumos! Tem que criar o 290 sem 291 mas com 292 de couro
          e de insumos!
*)
          if (DqVSRibAtu.RecordCount > 0) and (DqVSRibDst.RecordCount = 0) then
          begin
            MovimID     := TEstqMovimID(USQLDB.Int(DqVSRibAtu, 'MovimID'));
            MovimCod    := USQLDB.Int(DqVSRibAtu, 'MovimCod');
            Codigo      := USQLDB.Int(DqVSRibAtu, 'Codigo');
            DtHrAberto  := Trunc(USQLDB.DtH(DqProc, 'DtHrAberto'));
            DtHrFimOpe  := Trunc(USQLDB.DtH(DqProc, 'DtHrFimOpe'));
            DtMovim     := Trunc(USQLDB.DtH(DqVSRibAtu, 'DataHora'));
            DtCorrApo   := Trunc(USQLDB.DtH(DqVSRibAtu, 'DtCorrApo'));
            Empresa     := USQLDB.Int(DqVSRibAtu, 'Empresa');
            FornecMO    := USQLDB.Int(DqVSRibAtu, 'FornecMO');
            Controle    := USQLDB.Int(DqVSRibAtu, 'Controle');
            GraGruX     := USQLDB.Int(DqVSRibAtu, 'GraGruX');
            MovimNiv    := TEstqMovimNiv(USQLDB.Int(DqVSRibAtu, 'MovimNiv'));
            MovimTwn    := USQLDB.Int(DqVSRibAtu, 'MovimTwn');
            AreaM2      := 0; // N�o produziu nada!
            PesoKg      := 0; // N�o produziu nada!
            Pecas       := 0; // N�o produziu nada!
            Produziu    := TProduziuVS.przvsNao;
            // Ver se tem consumo de insumos...
            // Se tiver, lan�ar.
            ReopenOriPQx(MovimCod, FSQL_PeriodoPQ, False);
            InsumoSemProducao := QrOri.RecordCount > 0;
            if InsumoSemProducao then
            begin
              MeAviso.Lines.Add('MovimCod ' + Geral.FF0(USQLDB.Int(DqVSRibAtu, 'MovimCod')) +
              ' sem artigos destino mas com insumos consumidos!');
              //
              VerificaSeTemGraGruX(3299, GraGruX, sProcName, DqVSRibAtu);
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaGemeos(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, Tipo_Item, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaGemeos(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ, Produziu);
                else
                  AvisoFip('Gemeos');
              end;
            end;
          end else
          //// Fim Processo sem produ��o, mas com consumo de insumos
          ////
          ///////////////////////    Houve produ��o    /////////////////////////
          begin
            DqVSRibDst.First;
            while not DqVSRibDst.Eof do
            begin
              if (USQLDB.Dth(DqVSRibDst, 'DataHora') >= FDiaIni)
              and (USQLDB.DtH(DqVSRibDst, 'DataHora') < FDiaPos) then
              begin
                MovimID     := TEstqMovimID(USQLDB.Int(DqVSRibDst, 'MovimID'));
                MovimCod    := USQLDB.Int(DqVSRibDst, 'MovimCod');
                Codigo      := USQLDB.Int(DqVSRibDst, 'Codigo');
                DtHrAberto  := Trunc(USQLDB.DtH(DqProc, 'DtHrAberto'));
                DtHrFimOpe  := Trunc(USQLDB.DtH(DqProc, 'DtHrFimOpe'));
                DtMovim     := Trunc(USQLDB.DtH(DqVSRibDst, 'DataHora'));
                DtCorrApo   := Trunc(USQLDB.DtH(DqVSRibDst, 'DtCorrApo'));
                Empresa     := USQLDB.Int(DqVSRibDst, 'Empresa');
                FornecMO    := USQLDB.Int(DqVSRibDst, 'FornecMO');
                Controle    := USQLDB.Int(DqVSRibDst, 'Controle');
                GraGruX     := USQLDB.Int(DqVSRibDst, 'GraGruX');
                MovimNiv    := TEstqMovimNiv(USQLDB.Int(DqVSRibDst, 'MovimNiv'));
                MovimTwn    := USQLDB.Int(DqVSRibDst, 'MovimTwn');
                AreaM2      := USQLDB.Flu(DqVSRibDst, 'AreaM2');
                PesoKg      := USQLDB.Flu(DqVSRibDst, 'PesoKg');
                Pecas       := USQLDB.Flu(DqVSRibDst, 'Pecas');
                Produziu    := TProduziuVS.przvsSim;
                VerificaSeTemGraGruX(3343, GraGruX, sProcName, DqVSRibDst);
                //
                case FormaInsercaoItemProducao of
                  fipIsolada:
                    InsereItemProducaoIsoladaGemeos(DtHrAberto, DtHrFimOpe,
                    DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                    Codigo, Controle, Tipo_Item, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                    Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                    JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                    IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ);
                  fipConjunta:
                    InsereItemProducaoConjuntaGemeos(DtHrAberto, DtHrFimOpe,
                    DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                    Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                    Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                    JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                    IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ, Produziu);
                  else
                    AvisoFip('Gemeos');
                end;
              end;
              //
              DqVSRibDst.Next;
            end;
          end;
        end;
        iipcIndireta:
        begin
          DqVSRibInd.First;
          while not DqVSRibInd.Eof do
          begin
            if (USQLDB.DtH(DqVSRibInd, 'DataHora') >= FDiaIni)
            and (USQLDB.DtH(DqVSRibInd, 'DataHora') < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(USQLDB.Int(DqVSRibInd, 'MovimID'));
              MovimCod    := USQLDB.Int(DqVSRibInd, 'MovimCod');
              Codigo      := USQLDB.Int(DqVSRibInd, 'Codigo');
              DtHrAberto  := Trunc(USQLDB.DtH(DqProc, 'DtHrAberto'));
              DtHrFimOpe  := Trunc(USQLDB.DtH(DqProc, 'DtHrFimOpe'));
              DtMovim     := Trunc(USQLDB.DtH(DqVSRibInd, 'DataHora'));
              DtCorrApo   := Trunc(USQLDB.DtH(DqVSRibInd, 'DtCorrApo'));
              Empresa     := USQLDB.Int(DqVSRibInd, 'Empresa');
              Controle    := USQLDB.Int(DqVSRibInd, 'Controle');
              GraGruX     := USQLDB.Int(DqVSRibInd, 'GraGruX');
              MovimNiv    := TEstqMovimNiv(USQLDB.Int(DqVSRibInd, 'MovimNiv'));
              MovimTwn    := USQLDB.Int(DqVSRibInd, 'MovimTwn');
              AreaM2      := USQLDB.Flu(DqVSRibInd, 'AreaM2');
              PesoKg      := USQLDB.Flu(DqVSRibInd, 'PesoKg');
              Pecas       := USQLDB.Flu(DqVSRibInd, 'Pecas');
              GerMovID    := USQLDB.Int(DqVSRibInd, 'SrcMovID');
              GerNivel1   := USQLDB.Int(DqVSRibInd, 'SrcNivel1');
              GerNivel2   := USQLDB.Int(DqVSRibInd, 'SrcNivel2');
              GerGGX      := USQLDB.Int(DqVSRibInd, 'JmpGGX');
              QtdGerPeca  := USQLDB.Flu(DqVSRibInd, 'QtdGerPeca');
              QtdGerPeso  := USQLDB.Flu(DqVSRibInd, 'QtdGerPeso');
              QtdGerArM2  := USQLDB.Flu(DqVSRibInd, 'QtdGerArM2');
              //
              FornecMO    := USQLDB.Int(DqVSRibInd, 'FornecMO');
              ClientMO    := USQLDB.Int(DqVSRibInd, 'ClientMO');
              StqCenLoc   := USQLDB.Int(DqVSRibInd, 'StqCenLoc');
              EntiSitio   := USQLDB.Int(DqVSRibInd, 'scc_EntiSitio');
              TipoEstq    := USQLDB.Int(DqVSRibInd, 'TipoEstq');
              //
              if GerGGX = 0 then
              begin
                //Geral.MB_Erro('MovCodPai = ' + Geral.FF0(MovCodPai));
                GerGGX := VS_PF.ObtemJmpGGXdeMovCodPai_Fast(MovCodPai, sProcName);
                if MeAviso <> nil then
                  MeAviso.Lines.Add('"GerGGX" obtido do MovCodPai = ' +
                    Geral.FF0(MovCodPai) + ' (MovimCod = ' + Geral.FF0(MovimCod)
                    + ')(MovimID = ' + Geral.FF0(Integer(MovimID)) +
                    ')(MovimNiv = ' +  Geral.FF0(Integer(MovimNiv)) +
                    ')(Controle = ' + Geral.FF0(Controle) + ')');
(*
                if MeAviso <> nil then
                  MeAviso.Lines.Add('"GerGGX" indefinido para GerMovID = ' +
                  Geral.FF0(GerMovID) + ' e GerNivel1 = ' + Geral.FF0(GerNivel1));
                VS_PF.AtualizaJmpGGX(TEstqMovimID(GerMovID), GerNivel1);
                UnDmkDAC_PF.AbreQuery(USQLDB.?(DqVSRibInd, , Dmod.MyDB);
                GerGGX := USQLDB.?(DqVSRibInd, JmpGGX;
                if MeAviso <> nil then
                begin
                  if GerGGX = 0 then
                    MeAviso.Lines.Add('"GerGGX" nao corrigido para GerMovID = ' +
                    Geral.FF0(GerMovID) + ' e GerNivel1 = ' + Geral.FF0(GerNivel1))
                  else
                    MeAviso.Lines.Add('"GerGGX" corrigido para GerMovID = ' +
                    Geral.FF0(GerMovID) + ' e GerNivel1 = ' + Geral.FF0(GerNivel1));
                end;
*)
              end;
              VerificaSeTemGraGruX(3424, GraGruX, sProcName, DqVSRibInd);
              VerificaSeTemGraGruX(3425, GerGGX, sProcName, DqVSRibInd);
              //fazer pelo JmpGGX! sem
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaIndireta_Fast(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  GerMovID, GerNivel1, GerNivel2, GerGGX, QtdGerPeca,
                  QtdGerPeso, QtdGerArM2,
                  FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstq,
                  IDSeq1_K290, IDSeq1_K300, IncluiPQX,
                  InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaIndireta_Fast(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  GerMovID, GerNivel1, GerNivel2, GerGGX, QtdGerPeca,
                  QtdGerPeso, QtdGerArM2,
                  FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstq,
                  IDSeq1_K290, IDSeq1_K300, IncluiPQX,
                  InseriuPQ);
                else
                  AvisoFip('Indireta');
              end;
            end;
            //
            DqVSRibInd.Next;
          end;
        end;
        iipcUniAnt:
        begin
          //Geral.MB_SQL(Self, DqVSRibOriIMEI);
          DqVSRibOriIMEI.First;
          while not DqVSRibOriIMEI.Eof do
          begin
            if (USQLDB.DtH(DqVSRibOriIMEI, 'DataHora') >= FDiaIni)
            and (USQLDB.DtH(DqVSRibOriIMEI, 'DataHora') < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(USQLDB.Int(DqVSRibOriIMEI, 'MovimID'));
              MovimCod    := USQLDB.Int(DqVSRibOriIMEI, 'MovimCod');
              Codigo      := USQLDB.Int(DqVSRibOriIMEI, 'Codigo');
              DtHrAberto  := USQLDB.DtH(DqProc, 'DtHrAberto');
              DtHrFimOpe  := USQLDB.DtH(DqProc, 'DtHrFimOpe');
              DtMovim     := Trunc(USQLDB.DtH(DqVSRibOriIMEI, 'DataHora'));
              DtCorrApo   := Trunc(USQLDB.DtH(DqVSRibOriIMEI, 'DtCorrApo'));
              Empresa     := USQLDB.Int(DqVSRibOriIMEI, 'Empresa');
              Controle    := USQLDB.Int(DqVSRibOriIMEI, 'Controle');
              GGXOri      := USQLDB.Int(DqVSRibOriIMEI, 'RmsGGX');
              GGXDst      := USQLDB.Int(DqVSRibOriIMEI, 'DstGGX');
              MovimNiv    := TEstqMovimNiv(USQLDB.Int(DqVSRibOriIMEI, 'MovimNiv'));
              MovimTwn    := USQLDB.Int(DqVSRibOriIMEI, 'MovimTwn');
              AreaM2      := USQLDB.Flu(DqVSRibOriIMEI, 'AreaM2');
              PesoKg      := USQLDB.Flu(DqVSRibOriIMEI, 'PesoKg');
              Pecas       := USQLDB.Flu(DqVSRibOriIMEI, 'Pecas');
              QtdAntPeca  := USQLDB.Flu(DqVSRibOriIMEI, 'QtdAntPeca');
              QtdAntPeso  := USQLDB.Flu(DqVSRibOriIMEI, 'QtdAntPeso');
              QtdAntArM2  := USQLDB.Flu(DqVSRibOriIMEI, 'QtdAntArM2');
              FornecMO    := USQLDB.Int(DqVSRibOriIMEI, 'FornecMO');
              ClientMO    := USQLDB.Int(DqVSRibOriIMEI, 'ClientMO');
              StqCenLoc   := USQLDB.Int(DqVSRibOriIMEI, 'StqCenLoc');
              EntiSitio   := USQLDB.Int(DqVSRibOriIMEI, 'scc_EntiSitio');
              TipoEstq    := USQLDB.Int(DqVSRibOriIMEI, 'TipoEstq');
              //
              VerificaSeTemGraGruX(3485, GGXOri, sProcName, DqVSRibOriIMEI);
              VerificaSeTemGraGruX(3486, GGXDst, sProcName, DqVSRibOriIMEI);
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaUniAnt_Fast(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdAntPeca, QtdAntPeso, QtdAntArM2,
                  FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstq,
                  IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaUniAnt_Fast(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdAntPeca, QtdAntPeso, QtdAntArM2,
                  FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstq,
                  IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                else
                  AvisoFip('UnidAnt');
              end;
            end;
            //
            DqVSRibOriIMEI.Next;
          end;
        end;
        iipcUniGer:
        begin
          DqVSRibInd.First;
          while not DqVSRibInd.Eof do
          begin
            if (USQLDB.DtH(DqVSRibInd, 'DataHora') >= FDiaIni)
            and (USQLDB.DtH(DqVSRibInd, 'DataHora') < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(USQLDB.Int(DqVSRibInd, 'MovimID'));
              MovimCod    := USQLDB.Int(DqVSRibInd, 'MovimCod');
              Codigo      := USQLDB.Int(DqVSRibInd, 'Codigo');
              DtHrAberto  := USQLDB.DtH(DqProc, 'DtHrAberto');
              DtHrFimOpe  := USQLDB.DtH(DqProc, 'DtHrFimOpe');
              DtMovim     := Trunc(USQLDB.DtH(DqVSRibInd, 'DataHora'));
              DtCorrApo   := Trunc(USQLDB.DtH(DqVSRibInd, 'DtCorrApo'));
              Empresa     := USQLDB.Int(DqVSRibInd, 'Empresa');
              FornecMO    := USQLDB.Int(DqVSRibInd, 'FornecMO');
              Controle    := USQLDB.Int(DqVSRibInd, 'Controle');
              GGXOri      := USQLDB.Int(DqVSRibInd, 'RmsGGX');
              GGXDst      := USQLDB.Int(DqVSRibInd, 'JmpGGX');
              MovimNiv    := TEstqMovimNiv(USQLDB.Int(DqVSRibInd, 'MovimNiv'));
              MovimTwn    := USQLDB.Int(DqVSRibInd, 'MovimTwn');
              AreaM2      := USQLDB.Flu(DqVSRibInd, 'AreaM2');
              PesoKg      := USQLDB.Flu(DqVSRibInd, 'PesoKg');
              Pecas       := USQLDB.Flu(DqVSRibInd, 'Pecas');
              QtdGerPeca  := Pecas;
              QtdGerPeso  := PesoKg;
              QtdGerArM2  := AreaM2;
              //
              FornecMO    := USQLDB.Int(DqVSRibInd, 'FornecMO');
              ClientMO    := USQLDB.Int(DqVSRibInd, 'ClientMO');
              StqCenLoc   := USQLDB.Int(DqVSRibInd, 'StqCenLoc');
              EntiSitio   := USQLDB.Int(DqVSRibInd, 'scc_EntiSitio');
              TipoEstq    := USQLDB.Int(DqVSRibInd, 'TipoEstq');
              //
              VerificaSeTemGraGruX(3542, GGXOri, sProcName, DqVSRibInd);
              VerificaSeTemGraGruX(3543, GGXDst, sProcName, DqVSRibInd);
              //
              JmpMovID    := USQLDB.Int(DqVSRibInd, 'JmpMovID');
              JmpNivel1   := USQLDB.Int(DqVSRibInd, 'JmpNivel1');
              //if MovimID = TEstqMovimID.emidCurtido then
              if (MovimID = TEstqMovimID.emidCurtido) and
              (TEstqMovimID(JmpMovID) = TEstqMovimID.emidCurtido) then
              begin
                MovimID := TEstqMovimID(JmpMovID);
                if JmpNivel1 <> 0 then
                begin
                  UnDmkDAC_PF.AbreMySQLQuery0(QrCab34, Dmod.MyDB, [
                  'SELECT MovimCod',
                  'FROM vscurjmp',
                  'WHERE Codigo=' + Geral.FF0(JmpNivel1),
                  '']);
                  MovimCod := QrCab34MovimCod.Value;
                end else
                begin
                  MovimCod := 0;
                  Geral.MB_Aviso('MovimCod indefinido em ' + sProcName + ' (3)');
                end;
              end;
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaUniGer_Fast(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdGerPeca, QtdGerPeso, QtdGerArM2,
                  FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstq,
                  IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaUniGer_Fast(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdAntPeca, QtdAntPeso, QtdAntArM2,
                  FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstq,
                  IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                else
                  AvisoFip('UnidGer');
              end;
            end;
            //
            DqVSRibInd.Next;
          end;
        end;
        iipcSimples:
        begin
          // Gera��o!
          DqVSRibDst.First;
          while not DqVSRibDst.Eof do
          begin
            if (USQLDB.DtH(DqVSRibDst, 'DataHora') >= FDiaIni)
            and (USQLDB.DtH(DqVSRibDst, 'DataHora') < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(USQLDB.Int(DqVSRibDst, 'MovimID'));
              MovimCod    := USQLDB.Int(DqVSRibDst, 'MovimCod');
              Codigo      := USQLDB.Int(DqVSRibDst, 'Codigo');
              DtHrAberto  := USQLDB.DtH(DqProc, 'DtHrAberto');
              DtHrFimOpe  := USQLDB.DtH(DqProc, 'DtHrFimOpe');
              DtMovim     := Trunc(USQLDB.DtH(DqVSRibDst, 'DataHora'));
              DtCorrApo   := Trunc(USQLDB.DtH(DqVSRibDst, 'DtCorrApo'));
              Empresa     := USQLDB.Int(DqVSRibDst, 'Empresa');
              FornecMO    := USQLDB.Int(DqVSRibDst, 'FornecMO');
              Controle    := USQLDB.Int(DqVSRibDst, 'Controle');
              GraGruX     := USQLDB.Int(DqVSRibDst, 'GraGruX');
              MovimNiv    := TEstqMovimNiv(USQLDB.Int(DqVSRibDst, 'MovimNiv'));
              MovimTwn    := USQLDB.Int(DqVSRibDst, 'MovimTwn');
              AreaM2      := USQLDB.Flu(DqVSRibDst, 'AreaM2');
              PesoKg      := USQLDB.Flu(DqVSRibDst, 'PesoKg');
              Pecas       := USQLDB.Flu(DqVSRibDst, 'Pecas');
              //
              VerificaSeTemGraGruX(3618, GraGruX, sProcName, DqVSRibDst);
              case FormaInsercaoItemProducao of
                (*
                fipIsolada:
                  InsereItemProducaoIsoladaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdGerPeca, QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300,
                  InseriuPQ);
                *)
                fipConjunta:
                  InsereItemProducaoConjuntaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, PaiMovID, MovCodPai, PaiNivel1,
                  PaiNivel2, (*Fator*)1, TEstqSPEDTabSorc.estsVMI,
                  TSPED_EFD_GerBxa.segbGera, IncluiPQX, IDSeq1_K290, IDSeq1_K300);
                else
                  AvisoFip('Simples');
              end;
            end;
            //
            DqVSRibDst.Next;
          end;
          //
          // Baixa!
          DqVSRibOriIMEI.First;
          while not DqVSRibOriIMEI.Eof do
          begin
            if (USQLDB.DtH(DqVSRibOriIMEI, 'DataHora') >= FDiaIni)
            and (USQLDB.DtH(DqVSRibOriIMEI, 'DataHora') < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(USQLDB.Int(DqVSRibOriIMEI, 'MovimID'));
              MovimCod    := USQLDB.Int(DqVSRibOriIMEI, 'MovimCod');
              Codigo      := USQLDB.Int(DqVSRibOriIMEI, 'Codigo');
              DtHrAberto  := USQLDB.DtH(DqProc, 'DtHrAberto');
              DtHrFimOpe  := USQLDB.DtH(DqProc, 'DtHrFimOpe');
              DtMovim     := Trunc(USQLDB.DtH(DqVSRibOriIMEI, 'DataHora'));
              DtCorrApo   := Trunc(USQLDB.DtH(DqVSRibOriIMEI, 'DtCorrApo'));
              Empresa     := USQLDB.Int(DqVSRibOriIMEI, 'Empresa');
              FornecMO    := USQLDB.Int(DqVSRibOriIMEI, 'FornecMO');
              Controle    := USQLDB.Int(DqVSRibOriIMEI, 'Controle');
              GraGruX     := USQLDB.Int(DqVSRibAtu, 'GraGruX');
              MovimNiv    := TEstqMovimNiv(USQLDB.Int(DqVSRibOriIMEI, 'MovimNiv'));
              MovimTwn    := USQLDB.Int(DqVSRibOriIMEI, 'MovimTwn');
              AreaM2      := USQLDB.Flu(DqVSRibOriIMEI, 'AreaM2');
              PesoKg      := USQLDB.Flu(DqVSRibOriIMEI, 'PesoKg');
              Pecas       := USQLDB.Flu(DqVSRibOriIMEI, 'Pecas');
              //
              VerificaSeTemGraGruX(3669, GraGruX, sProcName, DqVSRibOriIMEI);
              //
              case FormaInsercaoItemProducao of
                (*
                fipIsolada:
                  InsereItemProducaoIsoladaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdGerPeca, QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300,
                  InseriuPQ);
                *)
                fipConjunta:
                  InsereItemProducaoConjuntaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, PaiMovID, MovCodPai, PaiNivel1,
                  PaiNivel2, (*Fator*)-1, TEstqSPEDTabSorc.estsVMI,
                  TSPED_EFD_GerBxa.segbBaixa, IncluiPQX, IDSeq1_K290, IDSeq1_K300);
                else
                  AvisoFip('Simples');
              end;
            end;
            //
            DqVSRibOriIMEI.Next;
          end;
          //baixa !!!!
(*
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        //MovimCod
        MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
*)
        //
        end;
        else begin
          Geral.MB_Erro(GetEnumName(TypeInfo(TInsercaoItemProducaoConjunta),
          Integer(InsercaoItemProducaoConjunta)) + ' indefinido em ' +
          sprocName + ' (4)');
        end;
      end;
{} // Fim
      DqIMECs.Next;
    end;
    DqFather.Next;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_ProducaoVer2_Slow(
  TabCab: String; TargetMovimID, PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID;
  PsqMovimNiv: TEstqMovimNiv; OrigemOpeProc: TOrigemOpeProc;
  FormaInsercaoItemProducao: TFormaInsercaoItemProducao; IncluiPQx: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_ProducaoVer2_Fast()';
  DstFator  = 1;
  OriFator  = 1;
var
  IDSeq1_K290, IDSeq1_K300: Integer;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  //
  MovimID: TEstqMovimID;
  Codigo, MovimCod, ClientMO, FornecMO, StqCenLoc, EntiSitio, CtrlDst, GGXDst,
  TipoEstqDst, TipoEstqOri, CtrlOri, GGXOri, CodDocOP: Integer;
  DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  OriESTSTabSorc: TEstqSPEDTabSorc;
  //
  MovimNiv: TEstqMovimNiv;
  MovimTwn, CtrlPsq, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
  MovCodPai, PaiNivel1, PaiNivel2, Empresa, Controle, GraGruX,
  GerMovID, GerNivel1, GerNivel2, GerGGX, Tipo_Item: Integer;
  InseriuPQ, InsumoSemProducao: Boolean;
  Produziu: TProduziuVS;
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdAntPeca, QtdAntPeso, QtdAntArM2: Double;
  InsercaoItemProducaoConjunta: TInsercaoItemProducaoConjunta;
  //
  procedure AvisoFip(ProcNome: String);
  begin
    Geral.MB_Aviso('FormaInsercaoItemProducao n�o definida em ' + sProcName +
    '.' + ProcNome);
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFather_, DModG.MyPID_DB, [
  'SELECT DISTINCT MovCodPai ',
  'FROM  ' + FSEII_IMEcs,
  'ORDER BY MovCodPai',
  '']);
  //*Geral.MB_SQL(Self, QrFather_);
  PB2.Position := 0;
  PB2.Max := QrFather_.RecordCount;
  QrFather_.First;
  while not QrFather_.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    //
    InseriuPQ := False;
    IDSeq1_K290 := -1;
    IDSeq1_K300 := -1;
    //
    case TargetMovimID of
      TEStqMovimID.emidEmOperacao,
      TEStqMovimID.emidEmProcWE,
      TEStqMovimID.emidEmProcSP,
      TEStqMovimID.emidEmReprRM:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrIMECs_, DModG.MyPID_DB, [
        'SELECT * ',
        'FROM  ' + FSEII_IMEcs,
        'WHERE MovCodPai=' + Geral.FF0(QrFather_MovCodPai.Value),
        //'ORDER BY '
        'LIMIT 1 ',
        '']);
      end;
      TEStqMovimID.emidEmProcCal,
      TEStqMovimID.emidCaleado,
      TEStqMovimID.emidEmProcCur,
      TEStqMovimID.emidCurtido:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrIMECs_, DModG.MyPID_DB, [
        'SELECT * ',
        'FROM  ' + FSEII_IMEcs,
        'WHERE MovCodPai=' + Geral.FF0(QrFather_MovCodPai.Value),
        '']);
        //*Geral.MB_SQL(Self, QrIMECs_);
      end;
      else begin
        Geral.MB_Erro(
        GetEnumName(TypeInfo(TEStqMovimID), Integer(TargetMovimID)) +
        ' indefinido em ' + sprocName + ' (1)');
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrIMECs_, DModG.MyPID_DB, [
        'SELECT * ',
        'FROM  ' + FSEII_IMEcs,
        'WHERE MovCodPai=' + Geral.FF0(QrFather_MovCodPai.Value),
        '']);
      end;
    end;
    //Geral.MB_SQL(Self, QrIMECs_);
//  At� aqui � super r�pido!
    while not QrIMECs_.Eof do
    begin
      InsercaoItemProducaoConjunta := iipcIndef;
      CodDocOP  := QrIMECs_MovCodPai.Value;
      UnDmkDAC_PF.AbreMySQLQuery0(QrProc_, Dmod.MyDB, [
      'SELECT DtHrAberto, DtHrFimOpe ',
      'FROM ' + LowerCase(TabCab),
      'WHERE MovimCod=' + Geral.FF0(CodDocOP),
      '']);
      //
      //case PsqMovimID of
      case TargetMovimID of
        TEStqMovimID.emidEmProcCal:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECs_MovimID.Value;
          MovCodPai := QrIMECs_MovCodPai.Value;
          PaiNivel1 := QrIMECs_Codigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu_, QrVSRibOriIMEI_,
          (*QrVSRibOriPallet*)nil, (*QrVSRibDst*)nil, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrIMECs_Codigo.Value*)PaiNivel1,
          (*QrIMECs_MovimCod.Value*) MovCodPai, (*QrIMECs_TemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto,
          emidEmProcCal, emidCaleado, emidCaleado,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCalInn, eminSorcCal, eminDestCal);
          InsercaoItemProducaoConjunta := iipcUniAnt;
        end;
        TEStqMovimID.emidCaleado:
        begin
          //  Couro caleado tirado direto do caleiro
          if QrIMECs_JmpNivel2.Value = 0 then
          begin
            JmpMovID  := 0;
            JmpMovCod := 0;
            JmpNivel1 := 0;
            JmpNivel2 := 0;
            PaiMovID  := QrIMECs_MovimID.Value;
            MovCodPai := QrIMECs_MovCodPai.Value;
            PaiNivel1 := QrIMECs_Codigo.Value;
            PaiNivel2 := 0;
            InsercaoItemProducaoConjunta := iipcGemeos;
          end else
          begin
            CtrlPsq := QrIMECs_JmpNivel2.Value;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrJmpA_, Dmod.MyDB, [
            'SELECT *  ',
            'FROM ' + CO_SEL_TAB_VMI + ' ',
            'WHERE Controle=' + Geral.FF0(CtrlPsq),
            '']);
            //
            CtrlPsq   := QrJmpA_SrcNivel2.Value;
            JmpMovID  := QrJmpA_MovimID.Value;
            JmpMovCod := QrJmpA_MovimCod.Value;
            JmpNivel1 := QrJmpA_Codigo.Value;
            JmpNivel2 := QrJmpA_Controle.Value;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrJmpB_, Dmod.MyDB, [
            'SELECT *  ',
            'FROM ' + CO_SEL_TAB_VMI + ' ',
            'WHERE Controle=' + Geral.FF0(CtrlPsq),
            '']);
            //
            PaiMovID  := QrJmpB_MovimID.Value;
            MovCodPai := QrJmpB_MovimCod.Value;
            PaiNivel1 := QrJmpB_Codigo.Value;
            PaiNivel2 := QrJmpB_Controle.Value;
            InsercaoItemProducaoConjunta := iipcIndireta;
          end;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu_, (*QrVSRibOriIMEI_*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst_, QrVSRibInd_,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrIMECs_Codigo.Value*)PaiNivel1,
          (*QrIMECs_MovimCod.Value*) MovCodPai, (*QrIMECs_TemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto,
          emidEmProcCal, emidCaleado, emidCaleado,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCalInn, eminSorcCal, eminDestCal);
        end;
        TEStqMovimID.emidEmProcCur:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECs_MovimID.Value;
          MovCodPai := QrIMECs_MovCodPai.Value;
          PaiNivel1 := QrIMECs_Codigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu_, QrVSRibOriIMEI_,
          (*QrVSRibOriPallet*)nil, (*QrVSRibDst_*)nil, (*QrVSRibInd_*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrIMECs_Codigo.Value*)PaiNivel1,
          (*QrIMECs_MovimCod.Value*) MovCodPai, (*QrIMECs_TemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto,
          emidEmProcCur, emidIndsXX, emidEmProcCur,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCurInn, eminSorcCur, eminDestCur);
          InsercaoItemProducaoConjunta := iipcUniAnt;
        end;
        TEStqMovimID.emidCurtido:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECs_MovimID.Value;
          MovCodPai := QrIMECs_MovCodPai.Value;
          PaiNivel1 := QrIMECs_Codigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu_, (*QrVSRibOriIMEI_*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst_, QrVSRibInd_,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrIMECs_Codigo.Value*)PaiNivel1,
          (*QrIMECs_MovimCod.Value*) MovCodPai, (*QrIMECs_TemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmProcCur, emidIndsXX, emidEmProcCur,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCurInn, eminSorcCur, eminDestCur);
          InsercaoItemProducaoConjunta := iipcUniGer;
          //Geral.MB_SQL(Self, QrVSRibAtu_);
          //Geral.MB_SQL(Self, QrVSRibDst_);
          //Geral.MB_SQL(Self, QrVSRibInd_);
        end;
        TEStqMovimID.emidEmOperacao:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECs_MovimID.Value;
          MovCodPai := QrIMECs_MovCodPai.Value;
          PaiNivel1 := QrIMECs_Codigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu_, (*QrVSRibOriIMEI_*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst_, (*QrVSRibInd_*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrVSRibCabCodigo.Value*)PaiNivel1,
          (*QrVSRibCabMovimCod.Value*)MovCodPai,(*QrVSRibCabTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmOperacao, emidEmOperacao, emidEmOperacao,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmOperInn, eminSorcOper, eminDestOper);
          InsercaoItemProducaoConjunta := iipcGemeos;
        end;
        TEStqMovimID.emidEmProcWE:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECs_MovimID.Value;
          MovCodPai := QrIMECs_MovCodPai.Value;
          PaiNivel1 := QrIMECs_Codigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu_, (*QrVSRibOriIMEI_*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst_, (*QrVSRibInd_*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrVSRibCabCodigo.Value*)PaiNivel1,
          (*QrVSRibCabMovimCod.Value*)MovCodPai,(*QrVSRibCabTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmProcWE, emidFinished, emidEmProcWE,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmWEndInn, eminSorcWEnd, eminDestWEnd);
          InsercaoItemProducaoConjunta := iipcGemeos;
          //Geral.MB_SQL(Self, QrVSRibAtu_);
          //Geral.MB_SQL(Self, QrVSRibDst_);
          //Geral.MB_SQL(Self, QrVSRibInd_);
        end;
        TEStqMovimID.emidEmProcSP:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECs_MovimID.Value;
          MovCodPai := QrIMECs_MovCodPai.Value;
          PaiNivel1 := QrIMECs_Codigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu_, (*QrVSRibOriIMEI_*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst_, (*QrVSRibInd_*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrVSRibCabCodigo.Value*)PaiNivel1,
          (*QrVSRibCabMovimCod.Value*)MovCodPai,(*QrVSRibCabTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmProcSP, emidEmProcSP, emidEmProcSP,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmPSPInn, eminSorcPSP, eminDestPSP);
          InsercaoItemProducaoConjunta := iipcGemeos;
        end;
        TEStqMovimID.emidEmReprRM:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECs_MovimID.Value;
          MovCodPai := QrIMECs_MovCodPai.Value;
          PaiNivel1 := QrIMECs_Codigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu_, (*QrVSRibOriIMEI_*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst_, (*QrVSRibInd_*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrVSRibCabCodigo.Value*)PaiNivel1,
          (*QrVSRibCabMovimCod.Value*)MovCodPai,(*QrVSRibCabTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmReprRM, emidEmReprRM, emidEmReprRM,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmRRMInn, eminSorcRRM, eminDestRRM);
          InsercaoItemProducaoConjunta := iipcGemeos;
        end;
        else Geral.MB_Erro('MovimID ' + Geral.FF0(Integer(TargetMovimID)) +
        ' n�o implementado em ' + sProcName + ' (2)');
      end;
      //
      //Geral.MB_SQL(Self, QrVSRibDst_);
      case InsercaoItemProducaoConjunta of
        //iipcIndef=0,
        iipcGemeos:
        begin
(*
          2019-01-27 - Fazer aqui!!
          Se QrVSRibDst_ n�o tem itens, quer dizer que tem um processo em aberto,
          mas sem nenhum artigo pronto ainda! Nesse caso tem que ver se j� tem
          consumo de insumos! Tem que criar o 290 sem 291 mas com 292 de couro
          e de insumos!
*)
          if (QrVSRibAtu_.RecordCount > 0) and (QrVSRibDst_.RecordCount = 0) then
          begin
            MovimID     := TEstqMovimID(QrVSRibAtu_MovimID.Value);
            MovimCod    := QrVSRibAtu_MovimCod.Value;
            Codigo      := QrVSRibAtu_Codigo.Value;
            DtHrAberto  := Trunc(QrProc_DtHrAberto.Value);
            DtHrFimOpe  := Trunc(QrProc_DtHrFimOpe.Value);
            DtMovim     := Trunc(QrVSRibAtu_DataHora.Value);
            DtCorrApo   := Trunc(QrVSRibAtu_DtCorrApo.Value);
            Empresa     := QrVSRibAtu_Empresa.Value;
            FornecMO    := QrVSRibAtu_FornecMO.Value;
            Controle    := QrVSRibAtu_Controle.Value;
            GraGruX     := QrVSRibAtu_GraGruX.Value;
            MovimNiv    := TEstqMovimNiv(QrVSRibAtu_MovimNiv.Value);
            MovimTwn    := QrVSRibAtu_MovimTwn.Value;
            AreaM2      := 0; // N�o produziu nada!
            PesoKg      := 0; // N�o produziu nada!
            Pecas       := 0; // N�o produziu nada!
            Produziu    := TProduziuVS.przvsNao;
            // Ver se tem consumo de insumos...
            // Se tiver, lan�ar.
            ReopenOriPQx(MovimCod, FSQL_PeriodoPQ, False);
            InsumoSemProducao := QrOri.RecordCount > 0;
            if InsumoSemProducao then
            begin
              MeAviso.Lines.Add('MovimCod ' + Geral.FF0(QrVSRibAtu_MovimCod.Value) +
              ' sem artigos destino mas com insumos consumidos!');
              //
              VerificaSeTemGraGruX(3299, GraGruX, sProcName, QrVSRibAtu_);
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaGemeos(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, Tipo_Item, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaGemeos(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ, Produziu);
                else
                  AvisoFip('Gemeos');
              end;
            end;
          end else
          //// Fim Processo sem produ��o, mas com consumo de insumos
          ////
          ///////////////////////    Houve produ��o    /////////////////////////
          begin
            QrVSRibDst_.First;
            while not QrVSRibDst_.Eof do
            begin
              if (QrVSRibDst_DataHora.Value >= FDiaIni)
              and (QrVSRibDst_DataHora.Value < FDiaPos) then
              begin
                MovimID     := TEstqMovimID(QrVSRibDst_MovimID.Value);
                MovimCod    := QrVSRibDst_MovimCod.Value;
                Codigo      := QrVSRibDst_Codigo.Value;
                DtHrAberto  := Trunc(QrProc_DtHrAberto.Value);
                DtHrFimOpe  := Trunc(QrProc_DtHrFimOpe.Value);
                DtMovim     := Trunc(QrVSRibDst_DataHora.Value);
                DtCorrApo   := Trunc(QrVSRibDst_DtCorrApo.Value);
                Empresa     := QrVSRibDst_Empresa.Value;
                FornecMO    := QrVSRibDst_FornecMO.Value;
                Controle    := QrVSRibDst_Controle.Value;
                GraGruX     := QrVSRibDst_GraGruX.Value;
                MovimNiv    := TEstqMovimNiv(QrVSRibDst_MovimNiv.Value);
                MovimTwn    := QrVSRibDst_MovimTwn.Value;
                AreaM2      := QrVSRibDst_AreaM2.Value;
                PesoKg      := QrVSRibDst_PesoKg.Value;
                Pecas       := QrVSRibDst_Pecas.Value;
                Produziu    := TProduziuVS.przvsSim;
                VerificaSeTemGraGruX(3343, GraGruX, sProcName, QrVSRibDst_);
                //
                case FormaInsercaoItemProducao of
                  fipIsolada:
                    InsereItemProducaoIsoladaGemeos(DtHrAberto, DtHrFimOpe,
                    DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                    Codigo, Controle, Tipo_Item, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                    Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                    JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                    IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ);
                  fipConjunta:
                    InsereItemProducaoConjuntaGemeos(DtHrAberto, DtHrFimOpe,
                    DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                    Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                    Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                    JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                    IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ, Produziu);
                  else
                    AvisoFip('Gemeos');
                end;
              end;
              //
              QrVSRibDst_.Next;
            end;
          end;
        end;
        iipcIndireta:
        begin
          QrVSRibInd_.First;
          while not QrVSRibInd_.Eof do
          begin
            if (QrVSRibInd_DataHora.Value >= FDiaIni)
            and (QrVSRibInd_DataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibInd_MovimID.Value);
              MovimCod    := QrVSRibInd_MovimCod.Value;
              Codigo      := QrVSRibInd_Codigo.Value;
              DtHrAberto  := Trunc(QrProc_DtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProc_DtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibInd_DataHora.Value);
              DtCorrApo   := Trunc(QrVSRibInd_DtCorrApo.Value);
              Empresa     := QrVSRibInd_Empresa.Value;
              FornecMO    := QrVSRibInd_FornecMO.Value;
              Controle    := QrVSRibInd_Controle.Value;
              GraGruX     := QrVSRibInd_GraGruX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibInd_MovimNiv.Value);
              MovimTwn    := QrVSRibInd_MovimTwn.Value;
              AreaM2      := QrVSRibInd_AreaM2.Value;
              PesoKg      := QrVSRibInd_PesoKg.Value;
              Pecas       := QrVSRibInd_Pecas.Value;
              GerMovID    := QrVSRibInd_SrcMovID.Value;
              GerNivel1   := QrVSRibInd_SrcNivel1.Value;
              GerNivel2   := QrVSRibInd_SrcNivel2.Value;
              GerGGX      := QrVSRibInd_JmpGGX.Value;
              QtdGerPeca  := QrVSRibInd_QtdGerPeca.Value;
              QtdGerPeso  := QrVSRibInd_QtdGerPeso.Value;
              QtdGerArM2  := QrVSRibInd_QtdGerArM2.Value;
              if GerGGX = 0 then
              begin
                //Geral.MB_Erro('MovCodPai = ' + Geral.FF0(MovCodPai));
                GerGGX := VS_PF.ObtemJmpGGXdeMovCodPai_Slow(MovCodPai);
                if MeAviso <> nil then
                  MeAviso.Lines.Add('"GerGGX" obtido do MovCodPai = ' +
                    Geral.FF0(MovCodPai));
(*
                if MeAviso <> nil then
                  MeAviso.Lines.Add('"GerGGX" indefinido para GerMovID = ' +
                  Geral.FF0(GerMovID) + ' e GerNivel1 = ' + Geral.FF0(GerNivel1));
                VS_PF.AtualizaJmpGGX(TEstqMovimID(GerMovID), GerNivel1);
                UnDmkDAC_PF.AbreQuery(QrVSRibInd_, Dmod.MyDB);
                GerGGX := QrVSRibInd_JmpGGX.Value;
                if MeAviso <> nil then
                begin
                  if GerGGX = 0 then
                    MeAviso.Lines.Add('"GerGGX" nao corrigido para GerMovID = ' +
                    Geral.FF0(GerMovID) + ' e GerNivel1 = ' + Geral.FF0(GerNivel1))
                  else
                    MeAviso.Lines.Add('"GerGGX" corrigido para GerMovID = ' +
                    Geral.FF0(GerMovID) + ' e GerNivel1 = ' + Geral.FF0(GerNivel1));
                end;
*)
              end;
              VerificaSeTemGraGruX(3424, GraGruX, sProcName, QrVSRibInd_);
              VerificaSeTemGraGruX(3425, GerGGX, sProcName, QrVSRibInd_);
              //fazer pelo JmpGGX! sem
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaIndireta_Slow(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  GerMovID, GerNivel1, GerNivel2, GerGGX, QtdGerPeca,
                  QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300, IncluiPQX,
                  InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaIndireta_Slow(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  GerMovID, GerNivel1, GerNivel2, GerGGX, QtdGerPeca,
                  QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300, IncluiPQX,
                  InseriuPQ);
                else
                  AvisoFip('Indireta');
              end;
            end;
            //
            QrVSRibInd_.Next;
          end;
        end;
        iipcUniAnt:
        begin
          //Geral.MB_SQL(Self, QrVSRibOriIMEI_);
          QrVSRibOriIMEI_.First;
          while not QrVSRibOriIMEI_.Eof do
          begin
            if (QrVSRibOriIMEI_DataHora.Value >= FDiaIni)
            and (QrVSRibOriIMEI_DataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibOriIMEI_MovimID.Value);
              MovimCod    := QrVSRibOriIMEI_MovimCod.Value;
              Codigo      := QrVSRibOriIMEI_Codigo.Value;
              DtHrAberto  := Trunc(QrProc_DtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProc_DtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibOriIMEI_DataHora.Value);
              DtCorrApo   := Trunc(QrVSRibOriIMEI_DtCorrApo.Value);
              Empresa     := QrVSRibOriIMEI_Empresa.Value;
              Controle    := QrVSRibOriIMEI_Controle.Value;
              GGXOri      := QrVSRibOriIMEI_RmsGGX.Value;
              GGXDst      := QrVSRibOriIMEI_DstGGX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibOriIMEI_MovimNiv.Value);
              MovimTwn    := QrVSRibOriIMEI_MovimTwn.Value;
              AreaM2      := QrVSRibOriIMEI_AreaM2.Value;
              PesoKg      := QrVSRibOriIMEI_PesoKg.Value;
              Pecas       := QrVSRibOriIMEI_Pecas.Value;
              QtdAntPeca  := QrVSRibOriIMEI_QtdAntPeca.Value;
              QtdAntPeso  := QrVSRibOriIMEI_QtdAntPeso.Value;
              QtdAntArM2  := QrVSRibOriIMEI_QtdAntArM2.Value;
              FornecMO    := QrVSRibOriIMEI_FornecMO.Value;
              ClientMO    := QrVSRibOriIMEI_ClientMO.Value;
              //
              VerificaSeTemGraGruX(3485, GGXOri, sProcName, QrVSRibOriIMEI_);
              VerificaSeTemGraGruX(3486, GGXDst, sProcName, QrVSRibOriIMEI_);
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaUniAnt_Slow(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdAntPeca, QtdAntPeso, QtdAntArM2,
                  IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaUniAnt_Slow(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdAntPeca, QtdAntPeso, QtdAntArM2,
                  IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                else
                  AvisoFip('UnidAnt');
              end;
            end;
            //
            QrVSRibOriIMEI_.Next;
          end;
        end;
        iipcUniGer:
        begin
          QrVSRibInd_.First;
          while not QrVSRibInd_.Eof do
          begin
            if (QrVSRibInd_DataHora.Value >= FDiaIni)
            and (QrVSRibInd_DataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibInd_MovimID.Value);
              MovimCod    := QrVSRibInd_MovimCod.Value;
              Codigo      := QrVSRibInd_Codigo.Value;
              DtHrAberto  := Trunc(QrProc_DtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProc_DtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibInd_DataHora.Value);
              DtCorrApo   := Trunc(QrVSRibInd_DtCorrApo.Value);
              Empresa     := QrVSRibInd_Empresa.Value;
              FornecMO    := QrVSRibInd_FornecMO.Value;
              Controle    := QrVSRibInd_Controle.Value;
              GGXOri      := QrVSRibInd_RmsGGX.Value;
              GGXDst      := QrVSRibInd_JmpGGX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibInd_MovimNiv.Value);
              MovimTwn    := QrVSRibInd_MovimTwn.Value;
              AreaM2      := QrVSRibInd_AreaM2.Value;
              PesoKg      := QrVSRibInd_PesoKg.Value;
              Pecas       := QrVSRibInd_Pecas.Value;
              QtdGerPeca  := Pecas;
              QtdGerPeso  := PesoKg;
              QtdGerArM2  := AreaM2;
              //
              VerificaSeTemGraGruX(3542, GGXOri, sProcName, QrVSRibInd_);
              VerificaSeTemGraGruX(3543, GGXDst, sProcName, QrVSRibInd_);
              //
              JmpMovID    := QrVSRibInd_JmpMovID.Value;
              JmpNivel1   := QrVSRibInd_JmpNivel1.Value;
              //if MovimID = TEstqMovimID.emidCurtido then
              if (MovimID = TEstqMovimID.emidCurtido) and
              (TEstqMovimID(JmpMovID) = TEstqMovimID.emidCurtido) then
              begin
                MovimID := TEstqMovimID(JmpMovID);
                if JmpNivel1 <> 0 then
                begin
                  UnDmkDAC_PF.AbreMySQLQuery0(QrCab34, Dmod.MyDB, [
                  'SELECT MovimCod',
                  'FROM vscurjmp',
                  'WHERE Codigo=' + Geral.FF0(JmpNivel1),
                  '']);
                  MovimCod := QrCab34MovimCod.Value;
                end else
                begin
                  MovimCod := 0;
                  Geral.MB_Aviso('MovimCod indefinido em ' + sProcName + ' (3)');
                end;
              end;
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaUniGer_Slow(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdGerPeca, QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaUniGer_Slow(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdAntPeca, QtdAntPeso, QtdAntArM2, IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                else
                  AvisoFip('UnidGer');
              end;
            end;
            //
            QrVSRibInd_.Next;
          end;
        end;
        iipcSimples:
        begin
          // Gera��o!
          QrVSRibDst_.First;
          while not QrVSRibDst_.Eof do
          begin
            if (QrVSRibDst_DataHora.Value >= FDiaIni)
            and (QrVSRibDst_DataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibDst_MovimID.Value);
              MovimCod    := QrVSRibDst_MovimCod.Value;
              Codigo      := QrVSRibDst_Codigo.Value;
              DtHrAberto  := Trunc(QrProc_DtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProc_DtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibDst_DataHora.Value);
              DtCorrApo   := Trunc(QrVSRibDst_DtCorrApo.Value);
              Empresa     := QrVSRibDst_Empresa.Value;
              FornecMO    := QrVSRibDst_FornecMO.Value;
              Controle    := QrVSRibDst_Controle.Value;
              GraGruX     := QrVSRibDst_GraGruX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibDst_MovimNiv.Value);
              MovimTwn    := QrVSRibDst_MovimTwn.Value;
              AreaM2      := QrVSRibDst_AreaM2.Value;
              PesoKg      := QrVSRibDst_PesoKg.Value;
              Pecas       := QrVSRibDst_Pecas.Value;
              //
              VerificaSeTemGraGruX(3618, GraGruX, sProcName, QrVSRibDst_);
              case FormaInsercaoItemProducao of
                (*
                fipIsolada:
                  InsereItemProducaoIsoladaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdGerPeca, QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300,
                  InseriuPQ);
                *)
                fipConjunta:
                  InsereItemProducaoConjuntaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, PaiMovID, MovCodPai, PaiNivel1,
                  PaiNivel2, (*Fator*)1, TEstqSPEDTabSorc.estsVMI,
                  TSPED_EFD_GerBxa.segbGera, IncluiPQX, IDSeq1_K290, IDSeq1_K300);
                else
                  AvisoFip('Simples');
              end;
            end;
            //
            QrVSRibDst_.Next;
          end;
          //
          // Baixa!
          QrVSRibOriIMEI_.First;
          while not QrVSRibOriIMEI_.Eof do
          begin
            if (QrVSRibOriIMEI_DataHora.Value >= FDiaIni)
            and (QrVSRibOriIMEI_DataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibOriIMEI_MovimID.Value);
              MovimCod    := QrVSRibOriIMEI_MovimCod.Value;
              Codigo      := QrVSRibOriIMEI_Codigo.Value;
              DtHrAberto  := Trunc(QrProc_DtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProc_DtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibOriIMEI_DataHora.Value);
              DtCorrApo   := Trunc(QrVSRibOriIMEI_DtCorrApo.Value);
              Empresa     := QrVSRibOriIMEI_Empresa.Value;
              FornecMO    := QrVSRibOriIMEI_FornecMO.Value;
              Controle    := QrVSRibOriIMEI_Controle.Value;
              GraGruX     := QrVSRibAtu_GraGruX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibOriIMEI_MovimNiv.Value);
              MovimTwn    := QrVSRibOriIMEI_MovimTwn.Value;
              AreaM2      := QrVSRibOriIMEI_AreaM2.Value;
              PesoKg      := QrVSRibOriIMEI_PesoKg.Value;
              Pecas       := QrVSRibOriIMEI_Pecas.Value;
              //
              VerificaSeTemGraGruX(3669, GraGruX, sProcName, QrVSRibOriIMEI_);
              //
              case FormaInsercaoItemProducao of
                (*
                fipIsolada:
                  InsereItemProducaoIsoladaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdGerPeca, QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300,
                  InseriuPQ);
                *)
                fipConjunta:
                  InsereItemProducaoConjuntaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, PaiMovID, MovCodPai, PaiNivel1,
                  PaiNivel2, (*Fator*)-1, TEstqSPEDTabSorc.estsVMI,
                  TSPED_EFD_GerBxa.segbBaixa, IncluiPQX, IDSeq1_K290, IDSeq1_K300);
                else
                  AvisoFip('Simples');
              end;
            end;
            //
            QrVSRibOriIMEI_.Next;
          end;
          //baixa !!!!
(*
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        //MovimCod
        MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
*)
        //
        end;
        else begin
          Geral.MB_Erro(GetEnumName(TypeInfo(TInsercaoItemProducaoConjunta),
          Integer(InsercaoItemProducaoConjunta)) + ' indefinido em ' +
          sprocName + ' (4)');
        end;
      end;
      QrIMECs_.Next;
    end;
    QrFather_.Next;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaGemeos(const
  DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime; const
  MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo, Controle,
  GraGruX: Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
  const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc: TOrigemOpeProc; const
  JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, MovCodPai, PaiNivel1,
  PaiNivel2: Integer; var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx:
  Boolean; var InseriuPQ: Boolean; const Produziu: TProduziuVS);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaGemeos()';
  DstFator = 1;
  OriFator = 1;
var
  ClientMO, FornecMO, EntiSitio, CtrlDst, GGXDst, TipoEstqDst, CtrlOri, GGXOri,
  TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
  TemTwn: Boolean;
begin
  //
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  //
  CtrlDst     := Controle;
  GGXDst      := GraGruX;
  AreaM2      := _AreaM2;
  PesoKg      := _PesoKg;
  Pecas       := _Pecas;
  //if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
  if Produziu <> TProduziuVS.przvsNao then
  begin
    if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
    begin
      case TipoEstqDst of
        0: QtdeDst := Pecas;
        1: QtdeDst := AreaM2;
        2: QtdeDst := PesoKg;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrProdRib.SQL.Text);
          //
          QtdeDst := Pecas;
        end;
      end;
      QtdeDst := QtdeDst * DstFator;
    end else
      QtdeDst := 0;
    if (QtdeDst = 0) then
    begin
      VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
      GGXDst, MovimCod, CtrlDst, sProcName);
      //Geral.MB_SQL(self, QrObtCliForSite);
    end;
  end;
  //
  //
  TemTwn := ReopenVmiTwn(MovimID, MovimNiv, MovimCod, MovimTwn);
  if TemTwn then
  begin
    CtrlOri     := QrVmiTwn.FieldByName('Controle').AsInteger;
    GGXOri      := QrVmiTwn.FieldByName('GraGruX').AsInteger;
    AreaM2      := -QrVmiTwn.FieldByName('AreaM2').AsFloat;
    PesoKg      := -QrVmiTwn.FieldByName('PesoKg').AsFloat;
    Pecas       := -QrVmiTwn.FieldByName('Pecas').AsFloat;
    TipoEstqOri := VS_PF.ObtemTipoEstqIMEI(CtrlOri,
    'vmi.AreaM2',  'vmi.PesoKg',  'vmi.Pecas',  'vmi.GraGruX', 'vmi.Controle');
    if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
    begin
      case TipoEstqOri of
        0: QtdeOri := Pecas;
        1: QtdeOri := AreaM2;
        2: QtdeOri := PesoKg;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrProdRib.SQL.Text);
          //
          QtdeOri := Pecas;
        end;
      end;
      QtdeOri := QtdeOri * OriFator;
    end else
      QtdeOri := 0;
  end;
  if TemTwn and (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, CtrlOri, sProcName);
  //
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      if TemTwn then
      begin
        // K292 - Itens consumidos
        OriESTSTabSorc := estsVMI;
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
        PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
        GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
        ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      end;
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      if TemTwn then
      begin
        // K302 - Itens Consumidos
        OriESTSTabSorc := estsVMI;
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
        PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
        GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
        ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      end;
      //
      // K302 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(10)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaIndireta_Fast(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GraGruX: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, GerMovID, GerNivel1,
  GerNivel2, GerGGX: Integer; const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
  const FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstqDst: Integer;
  var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean; var
  InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaIndireta_Fast()';
  DstFator = -1;
  OriFator = -1;
var
  //ClientMO, FornecMO, EntiSitio, TipoEstqDst,
  CtrlDst, GGXDst, CtrlOri, GGXOri, TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq_Fast(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  CtrlDst     := GerNivel2;
  //GGXDst      := GerGGX;
  GGXDst      := GraGruX;
  AreaM2      := _AreaM2;
  PesoKg      := _PesoKg;
  Pecas       := _Pecas;
  //if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0:
      begin
        //QtdeDst := AreaM2;
        QtdeDst := QtdGerPeca;
        QtdeOri := QtdGerPeca;
      end;
      1:
      begin
        //QtdeDst := AreaM2;
        QtdeDst := QtdGerArM2;
        QtdeOri := QtdGerArM2;
      end;
      2:
      begin
        //QtdeDst := PesoKg;
        QtdeDst := QtdGerPeso;
        QtdeOri := QtdGerPeso;
      end
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod));
        //
        //QtdeDst := Pecas;
        QtdeDst := QtdGerPeca;
        QtdeOri := QtdGerPeca;
      end;
    end;
    //QtdeDst := QtdeDst * DstFator;
    //QtdeOri := QtdeOri * DstFator;
  end else
    QtdeDst := 0;
  //
  CtrlOri     := Controle;
  //GGXOri      := GraGruX;
  GGXOri      := GerGGX;
  //
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290_Fast(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290,
        F_BNCI_IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso,
      F_BNCI_IDSeq2_K291);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, F_BNCI_IDSeq2_K292);
{
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
}
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300_Fast(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300,
        F_BNCI_IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, F_BNCI_IDSeq2_K301);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, F_BNCI_IDSeq2_K302);
      //
      // K302 - Insumos consumidos
{
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
}
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(11)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaIndireta_Slow(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GraGruX: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, GerMovID, GerNivel1,
  GerNivel2, GerGGX: Integer; const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
  var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
  var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaIndireta_Fast()';
  DstFator = -1;
  OriFator = -1;
var
  ClientMO, FornecMO, EntiSitio, CtrlDst, GGXDst, TipoEstqDst, CtrlOri, GGXOri,
  TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  CtrlDst     := GerNivel2;
  //GGXDst      := GerGGX;
  GGXDst      := GraGruX;
  AreaM2      := _AreaM2;
  PesoKg      := _PesoKg;
  Pecas       := _Pecas;
  //if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0:
      begin
        //QtdeDst := AreaM2;
        QtdeDst := QtdGerPeca;
        QtdeOri := QtdGerPeca;
      end;
      1:
      begin
        //QtdeDst := AreaM2;
        QtdeDst := QtdGerArM2;
        QtdeOri := QtdGerArM2;
      end;
      2:
      begin
        //QtdeDst := PesoKg;
        QtdeDst := QtdGerPeso;
        QtdeOri := QtdGerPeso;
      end
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        //QtdeDst := Pecas;
        QtdeDst := QtdGerPeca;
        QtdeOri := QtdGerPeca;
      end;
    end;
    //QtdeDst := QtdeDst * DstFator;
    //QtdeOri := QtdeOri * DstFator;
  end else
    QtdeDst := 0;
  //
  CtrlOri     := Controle;
  //GGXOri      := GraGruX;
  GGXOri      := GerGGX;
  //
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
(*
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
*)
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Insumos consumidos
(*
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovCodPai, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
*)
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(11)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaSimples(
  const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GraGruX:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const AreaM2, PesoKg, Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
              Fator: Integer; const OriESTSTabSorc: TEstqSPEDTabSorc; const
              SPED_EFD_GerBxa: TSPED_EFD_GerBxa; const IncluiPQx: Boolean;
              var IDSeq1_K290, IDSeq1_K300: Integer);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaSimples()';
  //
  JmpMovID     = 0;
  JmpMovCod    = 0;
  JmpNivel1    = 0;
  JmpNivel2    = 0;
var
  ClientMO, FornecMO, EntiSitio, TipoEstq: Integer;
  Qtde: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstq);
  //
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstq of
      0: Qtde := Pecas;
      1: Qtde := AreaM2;
      2: Qtde := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstq) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GraGruX) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        Qtde := Pecas;
      end;
    end;
    Qtde := Qtde * Fator;
  end else
    Qtde := 0;
  if (*TemTwn and*) (Qtde = 0) then
    VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
    GraGruX, MovimCod, Controle, sProcName);
  //
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290);
      end;
      case SPED_EFD_GerBxa of
        // K291 - Itens Produzidos
        TSPED_EFD_GerBxa.segbGera:
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
        // K292 - Itens consumidos
        TSPED_EFD_GerBxa.segbBaixa:
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
        else Geral.MB_Erro('SPED_EFD_GerBxa indefinido em ' + sProcName);
      end;
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300);
      end;
      case SPED_EFD_GerBxa of
      // K301 - Itens Produzidos
        TSPED_EFD_GerBxa.segbGera:
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Itens Consumidos
        TSPED_EFD_GerBxa.segbBaixa:
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
        //
        else Geral.MB_Erro('SPED_EFD_GerBxa indefinido em ' + sProcName);
      end;
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(12)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaUniAnt_Fast(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GGXOri, GGXDst: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  const QtdAntPeca, QtdAntPeso, QtdAntArM2: Double;
  const FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstqDst: Integer;
  var IDSeq1_K290, IDSeq1_K300:
  Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaRmsDst()';
  DstFator = 1;
  OriFator = 1;
var
  //ClientMO, FornecMO, EntiSitio,
  TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq_Fast(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  AreaM2      := QtdAntArM2;
  PesoKg      := QtdAntPeso;
  Pecas       := QtdAntPeca;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstqDst" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod));
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
  AreaM2      := QtdAntArM2;
  PesoKg      := QtdAntPeso;
  Pecas       := QtdAntPeca;
  TipoEstqOri := TipoEstqDst;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqOri of
      0: QtdeOri := Pecas;
      1: QtdeOri := AreaM2;
      2: QtdeOri := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod));
        //
        QtdeOri := Pecas;
      end;
    end;
    QtdeOri := QtdeOri * OriFator;
  end else
    QtdeOri := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290_Fast(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290,
        F_BNCI_IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, F_BNCI_IDSeq2_K291);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, F_BNCI_IDSeq2_K292);
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta_Fast(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300_Fast(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300,
        F_BNCI_IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, F_BNCI_IDSeq2_K301);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, F_BNCI_IDSeq2_K302);
      //
      // K302 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta_Fast(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(13)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaUniAnt_Slow(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GGXOri, GGXDst: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  const QtdAntPeca, QtdAntPeso, QtdAntArM2: Double;
  var IDSeq1_K290, IDSeq1_K300:
  Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaRmsDst()';
  DstFator = 1;
  OriFator = 1;
var
  ClientMO, FornecMO, EntiSitio, TipoEstqDst, TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  AreaM2      := QtdAntArM2;
  PesoKg      := QtdAntPeso;
  Pecas       := QtdAntPeca;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstqDst" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
  AreaM2      := QtdAntArM2;
  PesoKg      := QtdAntPeso;
  Pecas       := QtdAntPeca;
  TipoEstqOri := TipoEstqDst;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqOri of
      0: QtdeOri := Pecas;
      1: QtdeOri := AreaM2;
      2: QtdeOri := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeOri := Pecas;
      end;
    end;
    QtdeOri := QtdeOri * OriFator;
  end else
    QtdeOri := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(13)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaUniGer_Fast(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GGXOri, GGXDst: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
  const FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstqDst: Integer;
  var IDSeq1_K290,
  IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaUniReg()';
  DstFator = 1;
  OriFator = 1;
var
  //ClientMO, FornecMO, EntiSitio, TipoEstqDst,
  TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq_Fast(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  AreaM2      := -_AreaM2;
  PesoKg      := -_PesoKg;
  Pecas       := -_Pecas;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstqDst" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod));
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
  TipoEstqOri := VS_PF.ObtemTipoEstqIMEI_Fast(Controle,
  'vmi.QtdGerArM2',  'vmi.QtdGerPeso',  'vmi.QtdGerPeca',  'vmi.RmsGGX', 'vmi.Controle');
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqOri of
      0: QtdeOri := Pecas;
      1: QtdeOri := AreaM2;
      2: QtdeOri := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod));
        //
        QtdeOri := Pecas;
      end;
    end;
    QtdeOri := QtdeOri * OriFator;
  end else
    QtdeOri := 0;
  //
  if (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290_Fast(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290,
        F_BNCI_IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, F_BNCI_IDSeq2_K291);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, F_BNCI_IDSeq2_K292);
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta_Fast(TipoRegSPEDProd, MovimID, MovimNiv,
        MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300_Fast(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300,
        F_BNCI_IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, F_BNCI_IDSeq2_K301);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, F_BNCI_IDSeq2_K302);
      //
      // K302 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta_Fast(TipoRegSPEDProd, MovimID, MovimNiv,
        MovCodPai, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(14)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaUniGer_Slow(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GGXOri, GGXDst: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double; var IDSeq1_K290,
  IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaUniReg()';
  DstFator = 1;
  OriFator = 1;
var
  ClientMO, FornecMO, EntiSitio, TipoEstqDst, TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  AreaM2      := -_AreaM2;
  PesoKg      := -_PesoKg;
  Pecas       := -_Pecas;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstqDst" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
(*
  AreaM2      := -QtdGerArM2;
  PesoKg      := -QtdGerPeso;
  Pecas       := -QtdGerPeca;
*)
  TipoEstqOri := VS_PF.ObtemTipoEstqIMEI(Controle,
  'vmi.QtdGerArM2',  'vmi.QtdGerPeso',  'vmi.QtdGerPeca',  'vmi.RmsGGX', 'vmi.Controle');
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqOri of
      0: QtdeOri := Pecas;
      1: QtdeOri := AreaM2;
      2: QtdeOri := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeOri := Pecas;
      end;
    end;
    QtdeOri := QtdeOri * OriFator;
  end else
    QtdeOri := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(14)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaGemeos(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, Tipo_Item, GraGruX: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
  var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaGemeos()';
  DstFator = 1;
  OriFator = 1;
  COD_INS_SUBST = '';
var
  ClientMO, FornecMO, EntiSitio, CtrlDst, GGXDst, TipoEstqDst, CtrlOri, GGXOri,
  TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
  TemTwn: Boolean;
begin
  //
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  //
  CtrlDst     := Controle;
  GGXDst      := GraGruX;
  AreaM2      := _AreaM2;
  PesoKg      := _PesoKg;
  Pecas       := _Pecas;
  //if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
  //
  TemTwn := ReopenVmiTwn(MovimID, MovimNiv, MovimCod, MovimTwn);
  //
  if TemTwn then
  begin
    CtrlOri     := QrVmiTwn.FieldByName('Controle').AsInteger;
    GGXOri      := QrVmiTwn.FieldByName('GraGruX').AsInteger;
    AreaM2      := -QrVmiTwn.FieldByName('AreaM2').AsFloat;
    PesoKg      := -QrVmiTwn.FieldByName('PesoKg').AsFloat;
    Pecas       := -QrVmiTwn.FieldByName('Pecas').AsFloat;
    TipoEstqOri := TipoEstqDst;
    if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
    begin
      case TipoEstqOri of
        0: QtdeOri := Pecas;
        1: QtdeOri := AreaM2;
        2: QtdeOri := PesoKg;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrProdRib.SQL.Text);
          //
          QtdeOri := Pecas;
        end;
      end;
      QtdeOri := QtdeOri * OriFator;
    end else
      QtdeOri := 0;
    //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
  if QtdeOri = 0 then
    Geral.MB_Aviso('Qtde 0');
  end;
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp23X
  else
    TipoRegSPEDProd := trsp25X;
  //
  case TipoRegSPEDProd of
    trsp23X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO,
      FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
      FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1_K290);
      //
      if TemTwn then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, IDSeq1_K290, DtHrAberto, DtCorrApo, GGXOri,
        QtdeOri, COD_INS_SUBST, (*ID_Item*)CtrlOri, Integer(MovimID), Codigo, MovimCod,
        Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
        OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      end;
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    trsp25X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrFimOpe,
      DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
      QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1_K300);
      //
      if TemTwn then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, IDSeq1_K300, DtHrAberto, DtCorrApo, GGXOri,
        QtdeOri, COD_INS_SUBST, (*ID_Item*)CtrlOri, Integer(MovimID), Codigo, MovimCod,
        Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
        OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      end;
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(15)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaIndireta_Fast(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GraGruX: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, GerMovID, GerNivel1,
  GerNivel2, GerGGX: Integer; const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
  const FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstqDst: Integer;
  var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
  var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaIndireta_Fast()';
  DstFator = -1;
  OriFator = -1;
  COD_INS_SUBST = '';
var
  //ClientMO, FornecMO, EntiSitio, TipoEstqDst,
  CtrlDst, GGXDst, CtrlOri, GGXOri,
  TipoEstqOri, IDSeq1: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq_Fast(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  CtrlDst     := GerNivel2;
  //GGXDst      := GerGGX;
  GGXDst      := GraGruX;
  AreaM2      := _AreaM2;
  PesoKg      := _PesoKg;
  Pecas       := _Pecas;
  //if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0:
      begin
        //QtdeDst := AreaM2;
        QtdeDst := QtdGerPeca;
        QtdeOri := QtdGerPeca;
      end;
      1:
      begin
        //QtdeDst := AreaM2;
        QtdeDst := QtdGerArM2;
        QtdeOri := QtdGerArM2;
      end;
      2:
      begin
        //QtdeDst := PesoKg;
        QtdeDst := QtdGerPeso;
        QtdeOri := QtdGerPeso;
      end
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        //QtdeDst := Pecas;
        QtdeDst := QtdGerPeca;
        QtdeOri := QtdGerPeca;
      end;
    end;
    //QtdeDst := QtdeDst * DstFator;
    //QtdeOri := QtdeOri * DstFator;
  end else
    QtdeDst := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, CtrlDst, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, CtrlOri, sProcName);
  CtrlOri     := Controle;
  //GGXOri      := GraGruX;
  GGXOri      := GerGGX;
  //
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp23X
  else
    TipoRegSPEDProd := trsp25X;
  //
  case TipoRegSPEDProd of
    trsp23X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO,
      FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
      FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1, F_BNCI_IDSeq1_K230);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)CtrlOri, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso, F_BNCI_IDSeq2_K235);
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada_Fast(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    trsp25X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrFimOpe,
      DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
      QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1,
      F_BNCI_IDSeq1_K250);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)CtrlOri, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso, F_BNCI_IDSeq2_K255);
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada_Fast(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(16)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaIndireta_Slow(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GraGruX: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, GerMovID, GerNivel1,
  GerNivel2, GerGGX: Integer; const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
  var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
  var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaIndireta_Slow()';
  DstFator = -1;
  OriFator = -1;
  COD_INS_SUBST = '';
var
  ClientMO, FornecMO, EntiSitio, CtrlDst, GGXDst, TipoEstqDst, CtrlOri, GGXOri,
  TipoEstqOri, IDSeq1: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  CtrlDst     := GerNivel2;
  //GGXDst      := GerGGX;
  GGXDst      := GraGruX;
  AreaM2      := _AreaM2;
  PesoKg      := _PesoKg;
  Pecas       := _Pecas;
  //if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0:
      begin
        //QtdeDst := AreaM2;
        QtdeDst := QtdGerPeca;
        QtdeOri := QtdGerPeca;
      end;
      1:
      begin
        //QtdeDst := AreaM2;
        QtdeDst := QtdGerArM2;
        QtdeOri := QtdGerArM2;
      end;
      2:
      begin
        //QtdeDst := PesoKg;
        QtdeDst := QtdGerPeso;
        QtdeOri := QtdGerPeso;
      end
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        //QtdeDst := Pecas;
        QtdeDst := QtdGerPeca;
        QtdeOri := QtdGerPeca;
      end;
    end;
    //QtdeDst := QtdeDst * DstFator;
    //QtdeOri := QtdeOri * DstFator;
  end else
    QtdeDst := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, CtrlDst, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, CtrlOri, sProcName);
  CtrlOri     := Controle;
  //GGXOri      := GraGruX;
  GGXOri      := GerGGX;
  //
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp23X
  else
    TipoRegSPEDProd := trsp25X;
  //
  case TipoRegSPEDProd of
    trsp23X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO,
      FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
      FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)CtrlOri, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    trsp25X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrFimOpe,
      DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
      QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)CtrlOri, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
{
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, MeAviso);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Insumos consumidos
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, MeAviso);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Insumos consumidos
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
}
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(16)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaUniAnt_Fast(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GGXOri, GGXDst: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  const QtdAntPeca, QtdAntPeso, QtdAntArM2: Double;
  const FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstqDst: Integer;
  var IDSeq1_K290,
  IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaUniAnt_Fast()';
  DstFator = 1;
  OriFator = 1;
  COD_INS_SUBST = '';
var
  //ClientMO, FornecMO, EntiSitio, TipoEstqDst,
  TipoEstqOri, IDSeq1: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq_Fast(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  AreaM2      := QtdAntArM2;
  PesoKg      := QtdAntPeso;
  Pecas       := QtdAntPeca;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstqDst" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod));
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
  AreaM2      := QtdAntArM2;
  PesoKg      := QtdAntPeso;
  Pecas       := QtdAntPeca;
  TipoEstqOri := TipoEstqDst;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqOri of
      0: QtdeOri := Pecas;
      1: QtdeOri := AreaM2;
      2: QtdeOri := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod));
        //
        QtdeOri := Pecas;
      end;
    end;
    QtdeOri := QtdeOri * OriFator;
  end else
    QtdeOri := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp23X
  else
    TipoRegSPEDProd := trsp25X;
   //
  case TipoRegSPEDProd of
    trsp23X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO,
      FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
      FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1, F_BNCI_IDSeq1_K230);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)Controle, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso, F_BNCI_IDSeq2_K235);
      //
{
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
}
      // Ini 2019-01-23
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada_Fast(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
      // FIm 2019-01-23
    end;
    trsp25X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrFimOpe,
      DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
      QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1,
      F_BNCI_IDSeq1_K250);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)Controle, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso, F_BNCI_IDSeq2_K255);
      //
{
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
}
      // Ini 2019-01-23
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada_Fast(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
      // FIm 2019-01-23
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(17)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaUniAnt_Slow(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GGXOri, GGXDst: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  const QtdAntPeca, QtdAntPeso, QtdAntArM2: Double;
  var IDSeq1_K290,
  IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaUniAnt_Slow()';
  DstFator = 1;
  OriFator = 1;
  COD_INS_SUBST = '';
var
  ClientMO, FornecMO, EntiSitio, TipoEstqDst, TipoEstqOri, IDSeq1: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  AreaM2      := QtdAntArM2;
  PesoKg      := QtdAntPeso;
  Pecas       := QtdAntPeca;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstqDst" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
  AreaM2      := QtdAntArM2;
  PesoKg      := QtdAntPeso;
  Pecas       := QtdAntPeca;
  TipoEstqOri := TipoEstqDst;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqOri of
      0: QtdeOri := Pecas;
      1: QtdeOri := AreaM2;
      2: QtdeOri := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeOri := Pecas;
      end;
    end;
    QtdeOri := QtdeOri * OriFator;
  end else
    QtdeOri := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
{
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, MeAviso);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Insumos consumidos
(*
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
*)
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, MeAviso);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Insumos consumidos
(*
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
*)
    end;
}
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp23X
  else
    TipoRegSPEDProd := trsp25X;
   //
  case TipoRegSPEDProd of
    trsp23X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO,
      FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
      FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)Controle, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      //
{
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
}
      // Ini 2019-01-23
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
      // FIm 2019-01-23
    end;
    trsp25X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrFimOpe,
      DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
      QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)Controle, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      //
{
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
}
      // Ini 2019-01-23
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
      // FIm 2019-01-23
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(17)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaUniGer_Fast(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GGXOri, GGXDst: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
  const FornecMO, ClientMO, StqCenLoc, EntiSitio, TipoEstqDst: Integer;
  var IDSeq1_K290,
  IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaUniReg()';
  DstFator = 1;
  OriFator = 1;
  COD_INS_SUBST = '';
var
  //ClientMO, FornecMO, EntiSitio, TipoEstqDst,
  TipoEstqOri, IDSeq1: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq_Fast(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  AreaM2      := -_AreaM2;
  PesoKg      := -_PesoKg;
  Pecas       := -_Pecas;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstqDst" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
  AreaM2      := -QtdGerArM2;
  PesoKg      := -QtdGerPeso;
  Pecas       := -QtdGerPeca;
  TipoEstqOri := VS_PF.ObtemTipoEstqIMEI_Fast(Controle,
  'vmi.QtdGerArM2',  'vmi.QtdGerPeso',  'vmi.QtdGerPeca',  'vmi.RmsGGX', 'vmi.Controle');
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqOri of
      0: QtdeOri := Pecas;
      1: QtdeOri := AreaM2;
      2: QtdeOri := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeOri := Pecas;
      end;
    end;
    QtdeOri := QtdeOri * OriFator;
  end else
    QtdeOri := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp23X
  else
    TipoRegSPEDProd := trsp25X;
   //
  case TipoRegSPEDProd of
    trsp23X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO,
      FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
      FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1, F_BNCI_IDSeq1_K230);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)Controle, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso, F_BNCI_IDSeq2_K235);
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada_Fast(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    trsp25X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrFimOpe,
      DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
      QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso,
      IDSeq1, F_BNCI_IDSeq1_K250);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255_Fast(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)Controle, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso, F_BNCI_IDSeq2_K255);
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada_Fast(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, (*IDSeq1*)IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(18)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaUniGer_Slow(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GGXOri, GGXDst: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double; var IDSeq1_K290,
  IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaUniReg()';
  DstFator = 1;
  OriFator = 1;
  COD_INS_SUBST = '';
var
  ClientMO, FornecMO, EntiSitio, TipoEstqDst, TipoEstqOri, IDSeq1: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  AreaM2      := -_AreaM2;
  PesoKg      := -_PesoKg;
  Pecas       := -_Pecas;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstqDst" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
  AreaM2      := -QtdGerArM2;
  PesoKg      := -QtdGerPeso;
  Pecas       := -QtdGerPeca;
  TipoEstqOri := VS_PF.ObtemTipoEstqIMEI(Controle,
  'vmi.QtdGerArM2',  'vmi.QtdGerPeso',  'vmi.QtdGerPeca',  'vmi.RmsGGX', 'vmi.Controle');
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqOri of
      0: QtdeOri := Pecas;
      1: QtdeOri := AreaM2;
      2: QtdeOri := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeOri := Pecas;
      end;
    end;
    QtdeOri := QtdeOri * OriFator;
  end else
    QtdeOri := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
{
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, MeAviso);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, MeAviso);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
}
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp23X
  else
    TipoRegSPEDProd := trsp25X;
   //
  case TipoRegSPEDProd of
    trsp23X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO,
      FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
      FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)Controle, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    trsp25X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrFimOpe,
      DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
      QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)Controle, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, (*IDSeq1*)IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(18)');
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_EmOpeProc_215(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv; const QrCab: TmySQLQuery; const SQL_Periodo: String;
  const OrigemOpeProc: TOrigemOpeProc; var IDSeq1: Integer): Boolean;
const
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeProc_215';
var
  DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
  GraGruX, GGXCabDst, ID_Item, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer;
  MyQtd, Qtde, Fator: Double;
  COD_INS_SUBST, TabCab: String;
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_Periodo;
      //trsp23X: Result := SQL_Periodo;
      //trsp25X: Result := SQL_Periodo;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (3)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
begin
  Result := True;
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT cab.GraGruX GGXSrc, cab.GGXDst,  ',
  'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
  'Pecas, PesoKg, AreaM2, DATE(DataHora) DATA, vmi.DtCorrApo, ',
      VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
      'AreaM2', 'PesoKg', 'Pecas'),
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
  'LEFT JOIN vsopecab   cab ON cab.MovimCod=vmi.MovimCod  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  //'LEFT JOIN gragrux ggx ON ggx.Controle=cab.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
  SQL_Periodo,
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  while not QrOri.Eof do
  begin
    Codigo    := QrOri.FieldByName('Codigo').AsInteger;
    MovimCod  := QrOri.FieldByName('MovimCod').AsInteger;
    Controle  := QrOri.FieldByName('Controle').AsInteger;
    ID_Item   := Controle;
    //
    Fator         := 1;
    Data          := QrOri.FieldByName('DATA').AsDateTime;
    GGXCabDst     := QrOri.FieldByName('GGXDst').AsInteger;
    DtMovim       := Data;
    DtCorrApo     := QrOri.FieldByName('DtCorrApo').AsDateTime;
    ClientMO      := QrOri.FieldByName('ClientMO').AsInteger;
    FornecMO      := QrOri.FieldByName('FornecMO').AsInteger;
    EntiSitio     := QrOri.FieldByName('EntiSitio').AsInteger;
    if MovimID = emidEmProcCur then
    begin
(*
      UnDmkDAC_PF.AbreMySQLQuery0(QrCalToCur, Dmod.MyDB, [
      'SELECT DstGGX ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(QrOri.FieldByName('SrcNivel2').AsInteger),
      '']);
      GraGruX := QrCalToCurDstGGX.Value;
*)
      GraGruX := DefineGGXCalToCur('DstGGX', QrOri.FieldByName('SrcNivel2').AsInteger);
    end
    else
      GraGruX     := QrOri.FieldByName('GraGruX').AsInteger;
    COD_INS_SUBST := '';
    case MovimNiv of
      //eminEmOperBxa,
      //eminEmWEndBxa,
      eminDestOper:
      begin
        Fator := 1;
        if GGXCabDst <> GraGruX then
          if GGXCabDst <> 0 then
            COD_INS_SUBST := Geral.FF0(GGXCabDst);
      end;
(*
      eminDestCal,
      eminDestCur:
      begin
       COD_INS_SUBST := ''; // Nada??? Eh o GraGruX!
       Fator := ?;
      end;
*)
      else
      begin
        Result := False;
        Geral.MB_Erro('"MovimNiv": ' + Geral.FF0(Integer(MovimNiv)) +
        ' n�o implemetado em "Insere_OrigeProc()" (1)');
      end;
    end;
    //qual usar? area ou peso?
    MyQtd := 0;
    case  QrOri.FieldByName('Grandeza').AsInteger of
      0: MyQtd := QrOri.FieldByName('Pecas').AsFloat;
      1: MyQtd := QrOri.FieldByName('AreaM2').AsFloat;
      2: MyQtd := QrOri.FieldByName('PesoKg').AsFloat;
      else
      begin
        Geral.MB_Aviso('Grandeza n�o implementada: ' +
        sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak);
      end;
    end;
    //
    Qtde := MyQtd * Fator;
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (7)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
      '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
      'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) +
      sLineBreak + 'Avise a DERMATEK!!!');
      ID_Item := QrOri.FieldByName('Controle').AsInteger;
    if Qtde < 0.001 then
      Geral.MB_ERRO('Quantidade n�o pode ser zero!!! (3)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak + sLineBreak +
      'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
      '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
      'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    //
    case TipoRegSPEDProd of
      trsp21X:
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, IDSeq1, Data, DtMovim, DtCorrApo, GraGruX,
        Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
        Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210ProcVS,
        ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      end;
      else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(19)');
    end;
    //
    QrOri.Next;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_EmOpeProc_Prd(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const Qry: TmySQLQuery; const Fator: Double; const SQL_Periodo,
  SQL_PeriodoPQ: String; const OrigemOpeProc: TOrigemOpeProc;
  const MovimNiv: TEstqMovimNiv);
  //
  procedure Insere_EmOpeProc(const TipoRegSPEDProd: TTipoRegSPEDProd; const
            MovimID: TEstqMovimID; const Qry: TmySQLQuery; const Fator:
            Double; const SQL_Periodo, SQL_CorrApo: String;
            const OrigemOpeProc: TOrigemOpeProc;
            const InsumoSemProducao: Boolean; var IDSeq1: Integer);
  const
    sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_EmOpeProc_Prd > Insere_EmOpeProc()';
  var
    DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd, DtCorrApoCons: TDateTime;
    QtdeProd, QtdeCons, Pecas, PesoKg, AreaM2: Double;
    MyFator, TipoEstq, Codigo, MovimCod, GGXDst, ClientMO, FornecMO,
    EntiSitio: Integer; ESTSTabSorc: TEstqSPEDTabSorc; ID_Item, Controle: Integer;
    //
    function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
    begin
      case TipoRegSPEDProd of
        trsp21X: Result := '';
        trsp23X: Result := SQL_Periodo;
        //trsp25X: Result := '';
        trsp25X: Result := SQL_Periodo;
        trsp26X: Result := SQL_Periodo;
        else
        begin
          Geral.MB_Erro('"FiltroReg" indefinido: ' +
            Geral.FF0(Integer(TipoRegSPEDProd)) + '! (4)');
          Result := '#ERR_PERIODO_';
        end;
      end;
    end;

    procedure InsereAtual_2X0();
    begin
      DtMovim    := FDiaIni; // ver aqui quando periodo nao for mensal!
      //Parei aqui !!!  � asimm???
      if (Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: QtdeProd := Pecas;
          1: QtdeProd := AreaM2;
          2: QtdeProd := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            Qry.SQL.Text);
            //
            QtdeProd := Pecas;
          end;
        end;
        QtdeProd := QtdeProd * MyFator;
      end else
        QtdeProd := 0;
      //
      if QtdeProd < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (8)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeProd, 3, siNegativo), nil)
      else if (QtdeProd = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidEmProcCal: DtHrFimOpe := 0;
          emidEmProcCur: DtHrFimOpe := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade zerada para OP encerrada!!! (6)', nil);
        end;
      end;
      if (*TemTwn and*) (QtdeProd = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
        GGXDst, MovimCod, Controle, sProcName);
      //
      case TipoRegSPEDProd of
    {
        trsp21X:
        begin
      ESTSTabSorc := TEstqSPEDTabSorc.estsND;
      ID_Item     := 0;
      Controle    := 0;
          InsereItemAtual_K215(LinArqPai, DtHrAberto(*Data*), GGXDst(*GraGruX*),
          Qtde, ''(*COD_INS_SUBST*), ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ESTSTabSorc);
          //
        end;
    }
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd, GGXDst, ClientMO,
          FornecMO, EntiSitio, QtdeProd, OrigemOpeProc,
          FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrFimOpe, DtMovim, DtCorrApoProd, GGXDst, ClientMO, FornecMO,
          EntiSitio, QtdeProd, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal,
          MeAviso, IDSeq1);
        end;
        trsp26X:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrConsParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE MovimCod=' + Geral.FF0(MovimCod),
          'AND MovimNiv IN (57) ', //eminEmRRMBxa=57,
          //'AND MovimTwn=' + Geral.FF0(), /
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          'GROUP BY Ano, Mes', // MovimNiv >>> 54 ou 56 para RRM!!!
          '']);
         // Geral.MB_SQL(Self, QrConsParc);
          //
          if QrConsParc.RecordCount > 1 then
            Geral.MB_Erro('ERRO! Avise a Dermatek! QrConsParc.RecordCount > 1!!!');
          //while not QrConsParc.Eof do
          begin
            if QrConsParcAno.Value = 0 then
              DtCorrApoCons := 0
            else
              DtCorrApoCons := EncodeDate(Trunc(QrConsParcAno.Value), Trunc(QrConsParcMes.Value), 1);
            //
            AreaM2 := QrConsParcAreaM2.Value;
            PesoKg := QrConsParcPesoKg.Value;
            Pecas  := QrConsParcPecas.Value;
            //
            //Parei aqui !!!  � asimm???
            if (Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP) (*and (DtHrFimOpe > 1)*) then
            begin
              case TipoEstq of
                0: QtdeCons := Pecas;
                1: QtdeCons := AreaM2;
                2: QtdeCons := PesoKg;
                else
                begin
                  Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                  + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                  'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                  'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                  Qry.SQL.Text);
                  //
                  QtdeCons := Pecas;
                end;
              end;
              QtdeCons := QtdeCons * (*MyFator*) -1;
            end else
              QtdeCons := 0;
            if (*TemTwn and*) (QtdeCons = 0) then
              VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
              GGXDst, MovimCod, Controle, sProcName);
            //
            if QtdeCons < 0 then
              Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (9)' + sLineBreak +
              'Qtde: ' + Geral.FFT(QtdeCons, 3, siNegativo), QrConsParc)
          end;
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K260(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd, DtCorrApoCons, GGXDst,
          ClientMO, FornecMO, EntiSitio,
          //CtrlDst, CtrlBxa,
          QtdeProd, QtdeCons, OrigemOpeProc, FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(20)');
      end;
    end;
  begin
    MyFator    := -1;
    QtdeProd   := 0;
    QtdeCons   := 0;
    TipoEstq   := Qry.FieldByName('Tipoestq').AsInteger;
    Codigo     := Qry.FieldByName('Codigo').AsInteger;
    MovimCod   := Qry.FieldByName('MovimCod').AsInteger;
    DtHrAberto := Qry.FieldByName('DtHrAberto').AsDateTime;
    DtHrFimOpe := Qry.FieldByName('DtHrFimOpe').AsDateTime;
    GGXDst     := Qry.FieldByName('GGXDst').AsInteger;
    //
    if InsumoSemProducao then
    begin
      DtCorrApoProd := 0;
      AreaM2 := 0;
      PesoKg := 0;
      Pecas  := 0;
      //
      InsereAtual_2X0();
      //
    end else
    begin
      case MovimID of
        emidEmProcCal:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE SrcMovID=' + Geral.FF0(Integer(MovimID)),
          'AND SrcNivel1 <> 0 ',
          'AND SrcNivel1=' + Geral.FF0(Codigo),
          // Entradas no estoque pela producao ( sem doc fiscal de compra, remessa, devolucao, etc)
          'AND MovimID IN (' + CO_ALL_CODS_NO_INN_SPED_VS + ')',
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          '']);
        end;
        emidEmProcCur:
        begin
          // Igual a emidEmProcCal!!!
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE SrcMovID=' + Geral.FF0(Integer(MovimID)),
          'AND SrcNivel1 <> 0 ',
          'AND SrcNivel1=' + Geral.FF0(Codigo),
          // Entradas no estoque pela producao ( sem doc fiscal de compra, remessa, devolucao, etc)
          'AND MovimID IN (' + CO_ALL_CODS_NO_INN_SPED_VS + ')',
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          '']);
        end;
        emidEmOperacao, emidEmProcWE, emidEmProcSP, emidEmReprRM:
        begin
          MyFator := 1;
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE MovimCod=' + Geral.FF0(MovimCod),
          'AND MovimNiv IN (9,22,51,56) ', //eminDestOper=9, //eminDestWEnd=22, //eminDestPSP=51,//eminDestRRM=56,
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          '']);
          //Geral.MB_SQL(Self, QrProdParc);
        end;
        else
        begin
          //Result := False;
          Geral.MB_ERRO(
          '"MovimID" n�o implementado em ' + sProcName);
          //Close;
          Exit;
        end;
      end;
      //Geral.MB_SQL(self, QrProdParc);
      //
      QrProdParc.First;
      while not QrProdParc.Eof do
      begin
        //
        if QrProdParcAno.Value = 0 then
          DtCorrApoProd := 0
        else
          DtCorrApoProd := EncodeDate(Trunc(QrProdParcAno.Value), Trunc(QrProdParcMes.Value), 1);
        //
        AreaM2   := QrProdParcAreaM2.Value;
        PesoKg   := QrProdParcPesoKg.Value;
        Pecas    := QrProdParcPecas.Value;
        ClientMO := QrProdParcClientMO.Value;
        FornecMO := QrProdParcFornecMO.Value;
        Controle := QrProdParcControle.Value;
        //
        InsereAtual_2X0();
        //
        QrProdParc.Next;
      end;
    end;
  end;
  //
  function  Insere_OrigeProcVS(TipoRegSPEDProd: TTipoRegSPEDProd; MovimID:
            TEstqMovimID; MovimNiv: TEstqMovimNiv; QrCab: TmySQLQuery;
            IDSeq1: Integer; SQL_PeriodoVS, SQL_CorrApo: String;
            OrigemOpeProc: TOrigemOpeProc; InsumoSemProducao: Boolean): Boolean;
  const
    ESTSTabSorc = estsVMI;
    sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_EmOpeProc_Prd > Insere_OrigeProc()';
    //
    procedure InsereAtual_2X5(Data, DtCorrApo: TDateTime; GraGruX: Integer;
    Qtde: Double; COD_INS_SUBST: String; ID_Item, Codigo, Controle, ClientMO,
    FornecMO, EntiSitio, MovimCod: Integer);
    begin
      case TipoRegSPEDProd of
        //trsp21X:
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtCorrApo, GraGruX, Qtde,
          COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc,
          FTipoPeriodoFiscal, MeAviso);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtCorrApo, GraGruX, Qtde,
          COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc,
          FTipoPeriodoFiscal, MeAviso);
        end;
        trsp26X:
        begin
          //
(*        N�o tem como retornar nada ao estoque, pois a mercadoria foi transfor-
          mada e n�o montada (com um motor por exemplo)!
          //
          InsereItemAtual_K265(LinArqPai, Data, DtCorrApo, GraGruX, QtdeCons,
          QtdeRet, COD_INS_SUBST, ID_Item, MovimID, Codigo, MovimCod, Controle,
          OrigemOpeProc, ESTSTabSorc);
*)
          Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv, MovimCod,
          IDSeq1, SQL_PeriodoPQ, OrigemOpeProc, ClientMO, FornecMO, EntiSitio);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(21)');
      end;
    end;
  var
    Data, DtCorrApo, DtHrAberto, DtHrFimOpe: TDateTime;
    GraGruX, GGXCabDst, ID_Item, Codigo, MovimCod, Controle, ClientMO, FornecMO,
    EntiSitio: Integer;
    MyQtd, Qtde, Fator: Double;
    COD_INS_SUBST, TabCab: String;
    MovimNivInn: TEstqMovimNiv;
    //
  var
    SQL_PeriodoApo: String;
    DtCorrIni, DtCorrFim: TDateTime;
  begin
    Result := True;
    TabCab := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
    'SELECT cab.GraGruX GGXSrc, cab.GGXDst,  ',
    'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
    //'SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, SUM(AreaM2) AreaM2, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(DataHora) DATA, vmi.DtCorrApo, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
    'med.Grandeza  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
    'LEFT JOIN vsopecab  cab ON cab.MovimCod=vmi.MovimCod  ',
    'LEFT JOIN gragrux   ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    SQL_PeriodoVS,
    SQL_CorrApo,
    //'GROUP BY vmi.Controle ', // Sem Soma!
    '']);
    //Geral.MB_SQL(Self, QrOri);
    if InsumoSemProducao then
    begin
      //Geral.MB_SQL(Self, QrCab);
      case TipoRegSPEDProd of
        //trsp21X:
        trsp23X,
        //trsp25X:
        trsp26X:
        begin
          Data      := 0;
          DtCorrApo := 0;
          GraGruX   := QrCab.FieldByName('GraGruX').AsInteger;
          Qtde      := 0;
          COD_INS_SUBST := '';
          ID_Item   := 0;
          Codigo    := QrCab.FieldByName('Codigo').AsInteger;
          Controle  := 0;
          MovimCod  := QrCab.FieldByName('MovimCod').AsInteger;
          // N�o testado!!
          MovimNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
          UnDmkDAC_PF.AbreMySQLQuery0(QrObtCliForSite, Dmod.MyDB, [
          'SELECT vmi.ClientMO, vmi.FornecMO, scc.EntiSitio  ',
          'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
          'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc  ',
          'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo  ',
          'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
          'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
          '']);
          ClientMO  := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
          FornecMO  := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
          EntiSitio  := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
          //
          InsereAtual_2X5(Data, DtCorrApo, GraGruX, Qtde, COD_INS_SUBST,
          ID_Item, Codigo, Controle, ClientMO, FornecMO, EntiSitio, MovimCod);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(22)');
      end;
    end else
    begin
      if QrOri.RecordCount > 0 then
      begin
        QrOri.First;
        while not QrOri.Eof do
        begin
          Codigo    := QrOri.FieldByName('Codigo').AsInteger;
          MovimCod  := QrOri.FieldByName('MovimCod').AsInteger;
          Controle  := QrOri.FieldByName('Controle').AsInteger;
          ID_Item   := Controle;
          //
          Fator         := 1;
          Data          := QrOri.FieldByName('DATA').AsDateTime;
          DtCorrApo     := QrOri.FieldByName('DtCorrApo').AsDateTime;
          GGXCabDst     := QrOri.FieldByName('GGXDst').AsInteger;
          // J� testado!!
          ClientMO      := QrOri.FieldByName('ClientMO').AsInteger;
          FornecMO      := QrOri.FieldByName('FornecMO').AsInteger;
          if MovimID = emidEmProcCur then
          begin
(*
            UnDmkDAC_PF.AbreMySQLQuery0(QrCalToCur, Dmod.MyDB, [
            'SELECT DstGGX ',
            'FROM ' + CO_SEL_TAB_VMI,
            'WHERE Controle=' + Geral.FF0(QrOri.FieldByName('SrcNivel2').AsInteger),
            '']);
            GraGruX := QrCalToCurDstGGX.Value;
*)
            GraGruX := DefineGGXCalToCur('DstGGX', QrOri.FieldByName('SrcNivel2').AsInteger);
          end
          else
            GraGruX     := QrOri.FieldByName('GraGruX').AsInteger;
          COD_INS_SUBST := '';
          case MovimNiv of
            eminEmOperBxa,
            eminEmWEndBxa,
            eminEmPSPBxa:
            begin
              Fator := -1;
              if GGXCabDst <> GraGruX then
                if GGXCabDst <> 0 then
                  COD_INS_SUBST := Geral.FF0(GGXCabDst);
            end;
            eminEmCalBxa,
            eminEmCurBxa:
            begin
             COD_INS_SUBST := ''; // Nada??? Eh o GraGruX!
             Fator := -1;
            end;
            eminEmRRMBxa:
            begin
             COD_INS_SUBST := ''; // N�o existe no manual
             Fator := -1;
            end;
            else
            begin
              Result := False;
              Geral.MB_Erro('"MovimNiv": ' + Geral.FF0(Integer(MovimNiv)) +
              ' n�o implemetado em "Insere_OrigeProc()" (3)');
            end;
          end;
          //qual usar? area ou peso?
          MyQtd := 0;
          case  QrOri.FieldByName('Grandeza').AsInteger of
            0: MyQtd := QrOri.FieldByName('Pecas').AsFloat;
            1: MyQtd := QrOri.FieldByName('AreaM2').AsFloat;
            2: MyQtd := QrOri.FieldByName('PesoKg').AsFloat;
            else
            begin
              Geral.MB_Aviso('Grandeza n�o implementada: ' +
              sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
            'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak);
            end;
          end;
          //
          Qtde := MyQtd * Fator;
          if Qtde < 0 then
            Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (10)' + sLineBreak +
            'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
            sprocName + sLineBreak + sLineBreak +
            'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
            'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
            'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
            '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
            'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
            'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
            Geral.FFT(Fator, 3, siNegativo) + ' = ' +
            Geral.FFT(Qtde, 3, siNegativo) +
            sLineBreak + 'Avise a DERMATEK!!!');
            ID_Item := QrOri.FieldByName('Controle').AsInteger;
          if Qtde < 0.001 then
            Geral.MB_ERRO('Quantidade n�o pode ser zero!!! (3)' + sLineBreak +
            'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak + sLineBreak +
            'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
            'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
            'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
            '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
            'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
            'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
            Geral.FFT(Fator, 3, siNegativo) + ' = ' +
            Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
            sprocName + sLineBreak +
            sLineBreak + 'Avise a DERMATEK!!!');
            //
            //InsereAtual_2X5();
            InsereAtual_2X5(Data, DtCorrApo, GraGruX, Qtde, COD_INS_SUBST,
            ID_Item, Codigo, Controle, ClientMO, FornecMO, EntiSitio, MovimCod);
          //
          QrOri.Next;
        end;
      end else
      // ver se gastou insumo sem produzir produto acabado!
      begin
        // ?!
      end;
    end;
  end;
  //
  var
    SQL_PeriodoApo: String;
    DtCorrIni, DtCorrFim: TDateTime;
  procedure InsereAtualOpeProc_Prd(DtApoIni, DtApoFim: TDateTime);
  var
    DtHrAberto: TDateTime;
    SQL_PeriodoApo: String;
    IDSeq1, MovimCod, MovCodPai, ClientMO, FornecMO, EntiSitio, PsqIMEI,
    PsqMovimNiv: Integer;
    InsumoSemProducao: Boolean;

  begin
    SQL_PeriodoApo := dmkPF.SQL_Periodo('AND DtCorrApo ', DtApoIni, DtApoFim,
      True, True);
    //
    IDSeq1 := 0;
    InsumoSemProducao := False;
    Insere_EmOpeProc(TipoRegSPEDProd, MovimID, Qry, Fator,
    SQL_Periodo, SQL_PeriodoApo, OrigemOpeProc, InsumoSemProducao, IDSeq1);
    //
    MovimCod  := Qry.FieldByName('MovimCod').AsInteger;
    MovCodPai := Qry.FieldByName('MovimCod').AsInteger;
    InsumoSemProducao := IDSeq1 = 0;
    if InsumoSemProducao then
    begin
      // CUIDADO!!! Aqui esta duplicando!!!
      //Geral.MB_Info(Geral.FF0(Qry.FieldByName('MovimCod').AsInteger));
      ReopenOriPQx(MovimCod, SQL_PeriodoPQ, False);
      InsumoSemProducao := QrOri.RecordCount > 0;
      if InsumoSemProducao then
      begin
        //Geral.MB_Info(Geral.FF0(Qry.FieldByName('MovimCod').AsInteger));
        //
        Insere_EmOpeProc(TipoRegSPEDProd, MovimID, Qry, Fator, SQL_Periodo,
        SQL_PeriodoApo, OrigemOpeProc, InsumoSemProducao, IDSeq1);
      end;
      //
    end;
    Insere_OrigeProcVS(TipoRegSPEDProd, MovimID, MovimNiv, Qry, IDSeq1,
    SQL_Periodo, SQL_PeriodoApo, OrigemOpeProc, InsumoSemProducao);
    // Como fazer??
    // ini 2019-01-26
    //   Parei aqui
    //Geral.MB_SQL(Self, Qry);
    //PsqIMEI := Qry.FieldByName('Controle').AsInteger;
    //Geral.MB_Info('Ver se n�o duplica quando tem gra��o de couro prontono recurtimento/acabamento!');
    PsqMovimNiv := Integer(VS_PF.ObtemMovimNivCliForLocDeMovimID(TEstqMovimID(MovimID)));
    DefinePsqIMEI(MovimCod, Integer(MovimID), PsqMovimNiv, PsqIMEI);
    if PsqIMEI <> 0 then
    begin
      DtHrAberto := Qry.FieldByName('DtHrAberto').AsDateTime;
      //
      DefineCliForSiteOnly(PsqIMEI, ClientMO, FornecMO, EntiSitio);
      //
      (*InseriuPQ :=*) InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
      (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
      FornecMO, EntiSitio, DtHrAberto);
    end;
    //Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminEmWEndBxa, MovimCod, LinArqPai, SQL_PeriodoPQ);
    // fim 2019-01-26
  end;
begin
 // Criar query que separa periodos + Periodos de correcao!
   UnDmkDAC_PF.AbreMySQLQuery0(QrPeriodos, Dmod.MyDB, [
  'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
  'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          //'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, ',
   EfdIcmsIpi_PF.SQLPeriodoFiscal(
     FTipoPeriodoFiscal, 'DtCorrApo', 'Periodo', False),
   'FROM ' + CO_TAB_VMI,
   'WHERE MovimCod>0 ',
   'AND DtCorrApo > 0 ',
   'GROUP BY Ano, Mes, Periodo ',
   '']);
   //
   DtCorrIni  := 0;
   DtCorrFim  := 0;
   InsereAtualOpeProc_Prd(0, 0); // Registro normal sem Correcao de apontamento!
   QrPeriodos.First;
   while not QrPeriodos.Eof do
   begin
     EfdIcmsIpi_PF.ObtemDatasDePeriodoSPED(Trunc(QrPeriodosAno.Value),
     Trunc(QrPeriodosMes.Value), QrPeriodosPeriodo.Value, FTipoPeriodoFiscal,
     DtCorrIni, DtCorrFim);
     // Registros com Correcao de apontamento!
     InsereAtualOpeProc_Prd(DtCorrIni, DtCorrFim);
     QrPeriodos.Next
   end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeProcPQ(TipoRegSPEDProd:
  TTipoRegSPEDProd; MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod:
  Integer; IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
  TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer): Boolean;
const
  Fator = -1;
  ESTSTabSorc = estsPQx;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeProcPQ';
var
  Data, DtCorrApo: TDateTime;
  Codigo, Controle, ID_Item, GraGruX(*, ClientMO, FornecMO, EntiSitio*): Integer;
  MyQtd, Qtde: Double;
  //
  COD_INS_SUBST: String;
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := SQL_PeriodoPQ;
      trsp25X: Result := '';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (6)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
begin
  Result := True;
  ReopenOriPQx(MovimCod, SQL_PeriodoPQ, False);
  //
  //Geral.MB_SQL(Self, QrOri); //Parei Aqui!
  QrOri.First;
  while not QrOri.Eof do
  begin
    COD_INS_SUBST := '';
    Codigo   := QrOri.FieldByName('OrigemCodi').AsInteger;
    Controle := QrOri.FieldByName('OrigemCtrl').AsInteger;
    GraGruX  := UnPQx.ObtemGGXdeInsumo(QrOri.FieldByName('Insumo').AsInteger);
    //qual usar? area ou peso?
    MyQtd     := QrOri.FieldByName('Peso').AsFloat;
    Qtde      := MyQtd * Fator;
    Data      := QrOri.FieldByName('DataX').AsDateTime;
    DtCorrApo := QrOri.FieldByName('DtCorrApo').AsDateTime;
    ID_Item   := QrOri.FieldByName('OrigemCtrl').AsInteger;
    //
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (11)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      //'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'ID Item: ' + Geral.FF0(ID_Item) + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Peso: ' + Geral.FFT(MyQtd, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    if Qtde > 0 then
    begin
      case TipoRegSPEDProd of
        //trsp21X:
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtCorrApo, GraGruX, Qtde,
          COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtCorrApo, GraGruX,
          Qtde, COD_INS_SUBST, ID_Item,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
        trsp26X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K265(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtCorrApo, GraGruX,
          (*QtdeCons*) Qtde, (*QtdeRet*)0, COD_INS_SUBST, ID_Item,
          Integer(MovimID), Codigo, MovimCod, Controle, OrigemOpeProc,
          ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(23)');
      end;
    end;
    //
    QrOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeProcVS_210(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const MovimNivInn, MovimNivBxa: TEstqMovimNiv; const QrCab: TmySQLQuery;
  var IDSeq1: Integer; const SQL_PeriodoVS: String;
  const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeProcVS_210';
var
  DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  Qtde, Pecas, PesoKg, AreaM2: Double;
  MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, ClientMO, FornecMO, EntiSitio,
  ID_Item, Controle: Integer;
  //
(*
  procedure Mensagem(TextoBase: String);
  begin
    Geral.MB_ERRO(TextoBase + sLineBreak +
    'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
    'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
    sProcName + sLineBreak +
    sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
    QrProdParc.SQL.Text);
  end;
*)
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_PeriodoVS;
      //trsp23X: Result := SQL_PeriodoVS;
      //trsp25X: Result := SQL_PeriodoVS;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (7)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
begin
  Result := True;
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT cab.GraGruX GGXSrc, cab.GGXDst, ',
  'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
  //'SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, SUM(AreaM2) AreaM2, ',
  'DATE(DataHora) DATA, vmi.DtCorrApo, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, ',
      VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
      //'SUM(AreaM2)', 'SUM(PesoKg)', 'SUM(Pecas)'),
      'AreaM2', 'PesoKg', 'Pecas'),
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
  'LEFT JOIN vsopecab   cab ON cab.MovimCod=vmi.MovimCod  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=cab.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
  SQL_PeriodoVS,
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
  'GROUP BY DATA, Ano, Mes, GGXSrc ',
  '']);
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  while not QrOri.Eof do
  begin
    MyFator := -1;
    Qtde := 0;
    TipoEstq   := QrOri.FieldByName('Tipoestq').AsInteger;
    Codigo     := QrCab.FieldByName('Codigo').AsInteger;
    MovimCod   := QrCab.FieldByName('MovimCod').AsInteger;
    DtHrAberto := QrCab.FieldByName('DtHrAberto').AsDateTime;
    DtHrFimOpe := QrCab.FieldByName('DtHrFimOpe').AsDateTime;
    GGXSrc     := QrOri.FieldByName('GGXSrc').AsInteger;
    ClientMO   := QrOri.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrOri.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrOri.FieldByName('EntiSitio').AsInteger;
    //
    DtMovim    := QrOri.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrOri.FieldByName('DtCorrApo').AsDateTime;
    case MovimID of
      //emidEmProcCal:
      //emidEmProcCur:
      emidEmOperacao:
      begin
        MyFator := 1;
        UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
        'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
        'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
        'Pecas, AreaM2, PesoKg, ',
        'ClientMO, FornecMO, Controle ',
        'FROM ' + CO_SEL_TAB_VMI + ' ',
        'WHERE MovimCod=' + Geral.FF0(MovimCod),
        'AND MovimNiv=' + Geral.FF0(Integer(MovimNivBxa)),
        FiltroPeriodo(TipoRegSPEDProd),
        '']);
        //Geral.MB_SQL(Self, QrProdParc);
      end;
      //emidEmProcWE:
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //Geral.MB_SQL(self, QrProdParc);
    //
    QrProdParc.First;
    while not QrProdParc.Eof do
    begin
      AreaM2 := -QrProdParcAreaM2.Value;
      PesoKg := -QrProdParcPesoKg.Value;
      Pecas  := -QrProdParcPecas.Value;
      Controle := QrProdParcControle.Value;
      //
      //Parei aqui !!!  � asimm???
      if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: Qtde := Pecas;
          1: Qtde := AreaM2;
          2: Qtde := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrOri.SQL.Text);
            //
            Qtde := Pecas;
          end;
        end;
        Qtde := Qtde * MyFator;
      end else
        Qtde := 0;
      if (*TemTwn and*) (Qtde = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
        GGXSrc, MovimCod, Controle, sProcName);
      //
      if Qtde < 0 then
        (*
        Mensagem('Quantidade n�o pode ser negativa!!! (12)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo))
        *)
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (12)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrOri)
      else if (Qtde = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidEmProcCal: DtHrFimOpe := 0;
          emidEmProcCur: DtHrFimOpe := 0;
          else //Mensagem('Quantidade zerada para OP encerrada!!! (7)');
          Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade zerada para OP encerrada!!! (7)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrOri)
        end;
      end;
      //
      case TipoRegSPEDProd of
        trsp21X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K210(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO,
          FornecMO, EntiSitio, Qtde, OrigemOpeProc, oidk210ProcVS, FDiaFim,
          FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
  (*
        trsp23X:
        begin
          InsereItemAtual_K230(Integer(MovimID), Codigo, MovimCod, DtHrAberto,
          DtHrFimOpe, GGXDst, Qtde, LinArqPai);
        end;
        trsp25X:
        begin
          InsereItemAtual_K250(Integer(MovimID), Codigo, MovimCod, DtHrFimOpe,
          GGXDst, Qtde, LinArqPai);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(24)');
  *)
      end;
      QrProdParc.Next;
    end;
    //
    QrOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibDTA_210_Fast(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const MovimNiv: TEstqMovimNiv; const SQL_PeriodoVS: String;
  const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  COD_INS_SUBST = '';
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibDTA_210';
  //
var
  FListaSeq210: TMyArrOf2ArrOfInt;
  //
  function RegistroK210(const OriMovID: TEstqMovimID; const DtIniOS, DtFimOS:
  TDateTime; const SrcNivel1: Integer; var IDSeq1: Integer): Boolean;
  var
  DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
  QtdeSrc, QtdeDst, PecasSrc, PesoKgSrc, AreaM2Src, PecasDst, PesoKgDst,
  AreaM2Dst: Double;
  MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, GGXDst, ID_Item, Controle,
  ClientMO, FornecMO, EntiSitio: Integer;
  EhDoMes: Boolean;
  begin
    IDSeq1     := 0;
    AreaM2Src  := 0;
    PesoKgSrc  := 0;
    PecasSrc   := 0;
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeSrc    := 0;
    QtdeDst    := 0;
    TipoEstq   := USQLDB.Int(DqIts, 'TipoEstq');
    Codigo     := USQLDB.Int(DqIts, 'Codigo');
    MovimCod   := USQLDB.Int(DqIts, 'MovimCod');
    DtHrAberto := DtIniOS;
    DtHrFimOpe := DtFimOS;
    GGXSrc     := USQLDB.Int(DqIts, 'GGXSrc');
    // 215
    Data       := USQLDB.DtH(DqIts, 'DATA');
    GGXDst     := USQLDB.Int(DqIts, 'GGXDst');
    Controle   := USQLDB.Int(DqIts, 'RmsNivel2');
    ID_Item    := Controle;
    // 270
    DtMovim    := USQLDB.DtH(DqIts, 'DATA');
    DtCorrApo  := USQLDB.DtH(DqIts, 'DtCorrApo');
    //
    ClientMO   := USQLDB.Int(DqIts, 'ClientMO');
    FornecMO   := USQLDB.Int(DqIts, 'FornecMO');
    EntiSitio  := USQLDB.Int(DqIts, 'EntiSitio');
    //
    case MovimID of
      emidEmRibDTA:
      begin
        AreaM2Src := -USQLDB.Flu(DqIts, 'AreaM2');
        PesoKgSrc := -USQLDB.Flu(DqIts, 'PesoKg');
        PecasSrc  := -USQLDB.Flu(DqIts, 'Pecas');
        //
        AreaM2Dst := USQLDB.Flu(DqIts, 'QtdAntArM2');
        PesoKgDst := USQLDB.Flu(DqIts, 'QtdAntPeso');
        PecasDst  := USQLDB.Flu(DqIts, 'QtdAntPeca');
      end;
      else
      begin
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        Exit;
      end;
    end;
    EhDoMes := (Data >= FDiaIni) and (Data < FDiaPos);
    if EhDoMes then
    begin
      if (PecasSrc <> 0) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: QtdeSrc := PecasSrc;
          1: QtdeSrc := AreaM2Src;
          2: QtdeSrc := PesoKgSrc;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            DqIts.SQL.Text);
            //
            QtdeSrc := PecasSrc;
          end;
        end;
        QtdeSrc := QtdeSrc * MyFator;
      end else
        QtdeSrc := 0;
      if (*TemTwn and*) (QtdeSrc = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2Src, PesoKgSrc, PecasSrc,
        GGXSrc, MovimCod, Controle, sProcName);
    end else
      QtdeSrc := 0;
    //
    if QtdeSrc < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (13)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo), DqIts);
    //
    case TipoRegSPEDProd of
      trsp21X:
      begin
        // 2017-12-23 (a)
        if Controle <> 0 then
        // Fim 2017-12-23
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K210_Fast(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(OriMovID), Codigo, MovimCod, Controle, DtHrAberto,
          DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO, FornecMO, EntiSitio,
          QtdeSrc, OrigemOpeProc, oidk210RibDTA, FDiaFim, FTipoPeriodoFiscal,
          MeAviso, SrcNivel1, IDSeq1, F_BNCI_IDSeq1_K210, FListaSeq210);
        if EhDoMes then
        begin
          if (PecasDst <> 0) (*and (DtHrFimOpe > 1)*) then
          begin
            case TipoEstq of
              0: QtdeDst := PecasDst;
              1: QtdeDst := AreaM2Dst;
              2: QtdeDst := PesoKgDst;
              else
              begin
                Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                DqIts.SQL.Text);
                //
                QtdeDst := PecasDst;
              end;
            end;
            //QtdeDst := QtdeDst * MyFator;
          end else
            QtdeDst := 0;
          if (*TemTwn and*) (QtdeDst = 0) then
            VerificaTipoEstqQtde(TipoEstq, AreaM2Dst, PesoKgDst, PecasDst,
            GGXDst, MovimCod, Controle, sProcName);
          //
          if QtdeDst < 0 then
            Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade n�o pode ser negativa!!! (14)' + sLineBreak +
            'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), DqIts)
          else if (QtdeDst = 0) and (DtHrFimOpe > 1) then
          begin
            case MovimID of
              emidEmProcCal: DtHrFimOpe := 0;
              emidEmProcCur: DtHrFimOpe := 0;
              else Mensagem(sProcName, MovimCod, Integer(MovimID),
              'Quantidade zerada para OP encerrada!!! (8)', DqIts);
            end;
          end;
          //
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215_Fast(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtMovim, DtCorrApo, GGXDst,
          QtdeDst, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibDTA,
          ESTSTabSorc, FTipoPeriodoFiscal, MeAviso, F_BNCI_IDSeq2_K215);
        end;
      end;
      else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(25)');
    end;
  end;
  //
  //
  function RegistroK215_SubPrd(const IDSeq1: Integer): Boolean;
  var
    Data, DtMovim, DtCorrApo: TDateTime;
    QtdeDst, PecasDst, PesoKgDst, AreaM2Dst: Double;
    Codigo, MovimCod, MyFator, TipoEstq, GGXDst, ID_Item, Controle, MyMovimID,
    ClientMO, FornecMO, EntiSitio: Integer;
  begin
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeDst    := 0;
    TipoEstq   := USQLDB.Int(DqIts, 'TipoEstq');
    MyMovimID  := USQLDB.Int(DqIts, 'MovimID');
    Codigo     := USQLDB.Int(DqIts, 'Codigo');
    MovimCod   := USQLDB.Int(DqIts, 'MovimCod');
    // 215
    Data       := USQLDB.DtH(DqIts, 'DATA');
    GGXDst     := USQLDB.Int(DqIts, 'GraGruX');
    Controle   := USQLDB.Int(DqIts, 'Controle');
    ID_Item    := Controle;
    //
    DtMovim    := USQLDB.DtH(DqIts, 'DATA');
    DtCorrApo  := USQLDB.DtH(DqIts, 'DtCorrApo');
    //
    ClientMO   := USQLDB.Int(DqIts, 'ClientMO');
    FornecMO   := USQLDB.Int(DqIts, 'FornecMO');
    EntiSitio  := USQLDB.Int(DqIts, 'EntiSitio');
    //
    case MovimID of
      emidEmRibDTA:
      begin
        AreaM2Dst := USQLDB.Flu(DqIts, 'AreaM2');
        PesoKgDst := USQLDB.Flu(DqIts, 'PesoKg');
        PecasDst  := USQLDB.Flu(DqIts, 'Pecas');
      end;
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //
    //Parei aqui !!!  � asimm???
    if (PecasDst <> 0) or (PesoKgDst <> 0)(*and (DtHrFimOpe > 1)*) then
    begin
      case TipoEstq of
        0: QtdeDst := PecasDst;
        1: QtdeDst := AreaM2Dst;
        2: QtdeDst := PesoKgDst;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          DqIts.SQL.Text);
          //
          QtdeDst := PecasDst;
        end;
      end;
      //QtdeDst := QtdeDst * MyFator;
      QtdeDst := QtdeDst;
    end else
      QtdeDst := 0;
    //
    if (*TemTwn and*) (QtdeDst = 0) then
      VerificaTipoEstqQtde(TipoEstq, AreaM2Dst, PesoKgDst, PecasDst,
      GGXDst, MovimCod, Controle, sProcName);
    if QtdeDst < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (15)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), DqIts);
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215_Fast(FImporExpor, FAnoMes, FEmpresa, FPeriApu,
    IDSeq1, Data, DtMovim, DtCorrApo, GGXDst, QtdeDst,
    COD_INS_SUBST, ID_Item, MyMovimID, Codigo, MovimCod, Controle,
    ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibDTA, ESTSTabSorc,
    FTipoPeriodoFiscal, MeAviso, F_BNCI_IDSeq2_K215);
  end;
var
  SrcNivel1, LinArqK210, I, K: Integer;
  //Data
  DtIniOS, DtFimOS: TDateTime;
  SQL: String;
begin
  Result := True;
  MyObjects.Informa2(LaAviso3, LaAviso4, True, 'Gerando dados base: 1. Caleiros das entradas de couro in natura para saber o PDA(DTA)');
  FParar := False;
  Result := True;
  // 1. Codigos das entradas de materia-prima que caleirou dentro do mes
  SQL := Geral.ATS([
  'DROP TABLE IF EXISTS _PRE_GPS_; ',
  'CREATE TABLE _PRE_GPS_ ',
  'SELECT GSPJmpNiv2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE GSPJmpNiv2<>0 ',
  SQL_PeriodoVS,
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_1; ',
  'CREATE TABLE _PRE_DESC_1 ',
  ' ',
  'SELECT DISTINCT SrcNivel1, 1 Normal ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCur)),
  'AND SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  SQL_PeriodoVS,
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT vmi.Codigo, 2 Normal ',
  'FROM _PRE_GPS_ gps ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  '  ON gps.GSPJmpNiv2=vmi.Controle ',
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_2; ',
  'CREATE TABLE _PRE_DESC_2 ',
  ' ',
  'SELECT DISTINCT SrcNivel1 ',
  'FROM _PRE_DESC_1 ',
  ' ',
  ' ']);
  //
  DModG.MyPID_CompressDB.Execute(SQL);
  //
////////////////////////////////////////////////////////////////////////////////
  // 3. Datas minimas e m�ximas de cada Ordem de Produ��o
  MyObjects.Informa2(LaAviso3, LaAviso4, True, 'Gerando dados base: 3. Datas minimas e m�ximas de cada Ordem de Produ��o');
  SQL := Geral.ATS([
  'DROP TABLE IF EXISTS _PRE_DESC_ORI_MP_; ',
  'CREATE TABLE _PRE_DESC_ORI_MP_ ',
  'SELECT vmi.Controle  ',
  'FROM _pre_desc_2 pd2',
  'LEFT JOIN ' + FTabIMEI + ' vmi ON ',
  '   vmi.SrcNivel1=pd2.SrcNivel1',
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_DTIF_; ',
  ' ',
  'CREATE TABLE _PRE_DESC_DTIF_ ',
  'SELECT vmi.SrcNivel1, vmi.Controle, vmi.DataHora ',
  'FROM _pre_desc_2 pd2',
  'LEFT JOIN ' + FTabIMEI + ' vmi ON ',
  '   vmi.SrcNivel1=pd2.SrcNivel1',
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  '; ',
  ' ',
  'INSERT IGNORE INTO _PRE_DESC_DTIF_ ',
  ' ',
  'SELECT vmi.SrcNivel1, vmi.Controle, vmi.DataHora  ',
  'FROM _PRE_DESC_ORI_MP_ ori  ',
  'LEFT JOIN ' + FTabIMEI + ' vmi ',
  '  ON ori.Controle=vmi.GSPJmpNiv2; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_DT_MIN_MAX_; ',
  ' ',
  'CREATE TABLE _PRE_DESC_DT_MIN_MAX_ ',
  'SELECT SrcNivel1, MIN(DataHora) MinDH,  ',
  'MAX(DataHora) MaxDH  ',
  'FROM _PRE_DESC_DTIF_ ',
  'WHERE Controle > 0 ',
  'GROUP BY SrcNivel1',
  ' ']);
  //
  DModG.MyPID_CompressDB.Execute(SQL);
  //
  // 4. Caleiros das entradas de couro in natura do codigo de entrada atual
  //     para saber o PDA (campo RmsGGX)!
  MyObjects.Informa2(LaAviso3, LaAviso4, True, '4. Caleiros das entradas de couro in natura do codigo de entrada atual para saber o PDA(DTA) ');
  UnDmkDAC_PF.AbreMySQLDIrectQuery0(DqIts, DModG.MyPID_CompressDB, [
  'SELECT pd2.SrcNivel1, dmm.MinDH, dmm.MaxDH, ',
  //
  //'SELECT vmi.RmsGGX GGXDst, vmi.JmpGGX GGXSrc, ',
  'vmi.RmsGGX GGXDst, vmi.JmpGGX GGXSrc, ',
  'cab.Codigo, cab.MovimCod, vmi.SrcNivel2, ',
  'vmi.Controle VMICal, vmi.GraGruX GGXOriCal, ',
  'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
  'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
     VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
     'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'vmi.QtdAntPeso, vmi.QtdAntPeca, vmi.QtdAntArM2, vmi.RmsNivel2, ',
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM _PRE_DESC_2 pd2 ',
  //'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN ' + FTabIMEI + ' vmi ON vmi.SrcNivel1=pd2.SrcNivel1 ',
  'LEFT JOIN ' + TMeuDB + '.vscalcab   cab ON cab.Codigo=vmi.SrcNivel1 ',  // colorado!!!
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'LEFT JOIN _PRE_DESC_DT_MIN_MAX_     dmm ON dmm.SrcNivel1=pd2.SrcNivel1  ',
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  //
  '']);
  //Geral.MB_SQL(Self, DqIts);
  DqIts.First;
  PB2.Position := 0;
  PB2.Max := DqIts.RecordCount;
  // 5. Importando dados K210/K215 Produtos principais
  MyObjects.Informa2(LaAviso3, LaAviso4, True, 'Gerando dados base: 5. Importando dados K210');
  while not DqIts.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    //
    SrcNivel1 := USQLDB.Int(DqIts, 'SrcNivel1');
    //
    //ObtemDataIniEFim(SrcNivel1, DtIniOS, DtFimOS);
    //
    RegistroK210(TEstqMovimID.emidEmProcCal, DtIniOS, DtFimOS, SrcNivel1, LinArqK210);
      //
    DqIts.Next;
  end;
  //
  // 6. Itens de entrada de materia-prima do codigo atual
  MyObjects.Informa2(LaAviso3, LaAviso4, True, '6. Itens de entrada de materia-prima do codigo atual para saber os subprodutos');
  //UnDmkDAC_PF.AbreMySQLDIrectQuery0(DqIts, DModG.MyPID_CompressDB, [
  SQL := Geral.ATS([
  'DROP TABLE IF EXISTS _PRE_DESC_SUB_PRD_1; ',
  'CREATE TABLE _PRE_DESC_SUB_PRD_1 ',
  ' ',
  'SELECT vmi.Codigo, vmi.Controle ',
  'FROM _pre_desc_2 pd2',
  'LEFT JOIN ' + FTabIMEI + ' vmi ON ',
  '   vmi.Codigo=pd2.SrcNivel1',
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  //'AND Codigo=' + Geral.FF0(SrcNivel1),  // 1...270...n
  '; ',
  '']);
  DModG.MyPID_CompressDB.Execute(SQL);
  //
  UnDmkDAC_PF.AbreMySQLDIrectQuery0(DqIts, DModG.MyPID_CompressDB, [
  'SELECT pds.Codigo pds_SrcNivel1, vmi.GraGruX, ',
  'vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
  'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
  'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
     VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
     'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM _PRE_DESC_SUB_PRD_1  pds',
  'LEFT JOIN ' + FTabIMEI + ' vmi ON vmi.GSPSrcNiv2=pds.Controle',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad  scc ON scc.Codigo=scl.Codigo',
  'WHERE (NOT vmi.Codigo IS NULL) ',
  '']);
  //Geral.MB_SQL(Self, DqIts);
  DqIts.First;
  PB2.Position := 0;
  PB2.Max := DqIts.RecordCount;
  if DqIts.RecordCount > 0 then
    Geral.MB_Info('SubPrd "TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibDTA_210_Fast() nao testado!"');
  while not DqIts.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    //
    LinArqK210 := 0;
    //SrcNivel1  := USQLDB.Int(DqIts, 'SrcNivel1');
    SrcNivel1  := USQLDB.Int(DqIts, 'pds_SrcNivel1');
    //JaFoi := False;
    K := Length(FListaSeq210);
    for I := 0 to K -1 do
    begin
      if FListaSeq210[I][0] = SrcNivel1 then
      begin
        LinArqK210 := FListaSeq210[I][1];
        //JaFoi := True;
        Break;
      end;
    end;
    RegistroK215_SubPrd(LinArqK210);
    //
    DqIts.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibDTA_210(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const MovimNiv: TEstqMovimNiv; const SQL_PeriodoVS: String;
  const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  COD_INS_SUBST = '';
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibDTA_210';
  //
  procedure ObtemDataIniEFim(const Codigo: Integer; var Ini, Fim: TDateTime);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDatas, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _PRE_DESC_ORI_MP_; ',
    'CREATE TABLE _PRE_DESC_ORI_MP_ ',
    'SELECT Controle  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND Codigo=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _PRE_DESC_DTIF_; ',
    ' ',
    'CREATE TABLE _PRE_DESC_DTIF_ ',
    'SELECT vmi.Controle, vmi.DataHora ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND vmi.SrcNivel1=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'INSERT IGNORE INTO _PRE_DESC_DTIF_ ',
    ' ',
    'SELECT vmi.Controle, vmi.DataHora  ',
    'FROM _PRE_DESC_ORI_MP_ ori  ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    '  ON ori.Controle=vmi.GSPJmpNiv2; ',
    ' ',
    'SELECT MIN(DataHora) MinDH,  ',
    'MAX(DataHora) MaxDH  ',
    'FROM _PRE_DESC_DTIF_ ',
    'WHERE Controle > 0 ',
    '']);
    //Geral.MB_SQL(Self, QrDatas);
    //
    Ini := QrDatasMinDH.Value;
    Fim := QrDatasMaxDH.Value;
  end;
  //
  function RegistroK210(const OriMovID: TEstqMovimID; const DtIniOS, DtFimOS:
  TDateTime; var IDSeq1: Integer): Boolean;
  var
  DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
  QtdeSrc, QtdeDst, PecasSrc, PesoKgSrc, AreaM2Src, PecasDst, PesoKgDst,
  AreaM2Dst: Double;
  MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, GGXDst, ID_Item, Controle,
  ClientMO, FornecMO, EntiSitio: Integer;
  EhDoMes: Boolean;
  begin
    IDSeq1     := 0;
    AreaM2Src  := 0;
    PesoKgSrc  := 0;
    PecasSrc   := 0;
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeSrc    := 0;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    DtHrAberto := DtIniOS;
    DtHrFimOpe := DtFimOS;
    GGXSrc     := QrIts.FieldByName('GGXSrc').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GGXDst').AsInteger;
    Controle   := QrIts.FieldByName('RmsNivel2').AsInteger;
    ID_Item    := Controle;
    // 270
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibDTA:
      begin
        AreaM2Src := -QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgSrc := -QrIts.FieldByName('PesoKg').AsFloat;
        PecasSrc  := -QrIts.FieldByName('Pecas').AsFloat;
        //
        AreaM2Dst := QrIts.FieldByName('QtdAntArM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('QtdAntPeso').AsFloat;
        PecasDst  := QrIts.FieldByName('QtdAntPeca').AsFloat;
      end;
      else
      begin
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        Exit;
      end;
    end;
    EhDoMes := (Data >= FDiaIni) and (Data < FDiaPos);
    if EhDoMes then
    begin
      if (PecasSrc <> 0) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: QtdeSrc := PecasSrc;
          1: QtdeSrc := AreaM2Src;
          2: QtdeSrc := PesoKgSrc;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrIts.SQL.Text);
            //
            QtdeSrc := PecasSrc;
          end;
        end;
        QtdeSrc := QtdeSrc * MyFator;
      end else
        QtdeSrc := 0;
      if (*TemTwn and*) (QtdeSrc = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2Src, PesoKgSrc, PecasSrc,
        GGXSrc, MovimCod, Controle, sProcName);
    end else
      QtdeSrc := 0;
    //
    if QtdeSrc < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (13)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo), QrIts);
    //
    case TipoRegSPEDProd of
      trsp21X:
      begin
        // 2017-12-23 (a)
        if Controle <> 0 then
        // Fim 2017-12-23
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K210(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(OriMovID), Codigo, MovimCod, Controle, DtHrAberto,
          DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO, FornecMO, EntiSitio,
          QtdeSrc, OrigemOpeProc, oidk210RibDTA, FDiaFim, FTipoPeriodoFiscal,
          MeAviso, IDSeq1);
        if EhDoMes then
        begin
          if (PecasDst <> 0) (*and (DtHrFimOpe > 1)*) then
          begin
            case TipoEstq of
              0: QtdeDst := PecasDst;
              1: QtdeDst := AreaM2Dst;
              2: QtdeDst := PesoKgDst;
              else
              begin
                Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                QrIts.SQL.Text);
                //
                QtdeDst := PecasDst;
              end;
            end;
            //QtdeDst := QtdeDst * MyFator;
          end else
            QtdeDst := 0;
          if (*TemTwn and*) (QtdeDst = 0) then
            VerificaTipoEstqQtde(TipoEstq, AreaM2Dst, PesoKgDst, PecasDst,
            GGXDst, MovimCod, Controle, sProcName);
          //
          if QtdeDst < 0 then
            Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade n�o pode ser negativa!!! (14)' + sLineBreak +
            'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), QrIts)
          else if (QtdeDst = 0) and (DtHrFimOpe > 1) then
          begin
            case MovimID of
              emidEmProcCal: DtHrFimOpe := 0;
              emidEmProcCur: DtHrFimOpe := 0;
              else Mensagem(sProcName, MovimCod, Integer(MovimID),
              'Quantidade zerada para OP encerrada!!! (8)', QrIts);
            end;
          end;
          //
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtMovim, DtCorrApo, GGXDst,
          QtdeDst, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibDTA,
          ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
      end;
      else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(25)');
    end;
  end;
  //
  //
  function RegistroK215_SubPrd(const IDSeq1: Integer): Boolean;
  var
    Data, DtMovim, DtCorrApo: TDateTime;
    QtdeDst, PecasDst, PesoKgDst, AreaM2Dst: Double;
    Codigo, MovimCod, MyFator, TipoEstq, GGXDst, ID_Item, Controle, MyMovimID,
    ClientMO, FornecMO, EntiSitio: Integer;
  begin
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    MyMovimID  := QrIts.FieldByName('MovimID').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GraGruX').AsInteger;
    Controle   := QrIts.FieldByName('Controle').AsInteger;
    ID_Item    := Controle;
    //
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibDTA:
      begin
        AreaM2Dst := QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('PesoKg').AsFloat;
        PecasDst  := QrIts.FieldByName('Pecas').AsFloat;
      end;
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //
    //Parei aqui !!!  � asimm???
    if (PecasDst <> 0) or (PesoKgDst <> 0)(*and (DtHrFimOpe > 1)*) then
    begin
      case TipoEstq of
        0: QtdeDst := PecasDst;
        1: QtdeDst := AreaM2Dst;
        2: QtdeDst := PesoKgDst;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrIts.SQL.Text);
          //
          QtdeDst := PecasDst;
        end;
      end;
      //QtdeDst := QtdeDst * MyFator;
      QtdeDst := QtdeDst;
    end else
      QtdeDst := 0;
    //
    if (*TemTwn and*) (QtdeDst = 0) then
      VerificaTipoEstqQtde(TipoEstq, AreaM2Dst, PesoKgDst, PecasDst,
      GGXDst, MovimCod, Controle, sProcName);
    if QtdeDst < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (15)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), QrIts);
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa, FPeriApu,
    IDSeq1, Data, DtMovim, DtCorrApo, GGXDst, QtdeDst,
    COD_INS_SUBST, ID_Item, MyMovimID, Codigo, MovimCod, Controle,
    ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibDTA, ESTSTabSorc,
    FTipoPeriodoFiscal, MeAviso);
  end;
  var
    LinArqK210: Integer;
    //Data
    DtIniOS, DtFimOS: TDateTime;
begin
  Result := True;
  // 1. Codigos das entradas de materia-prima que caleirou dentro do mes
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _PRE_GPS_; ',
  'CREATE TABLE _PRE_GPS_ ',
  'SELECT GSPJmpNiv2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE GSPJmpNiv2<>0 ',
  SQL_PeriodoVS,
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_; ',
  'CREATE TABLE _PRE_DESC_ ',
  ' ',
  'SELECT DISTINCT SrcNivel1, 1 Normal ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCur)),
  'AND SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  SQL_PeriodoVS,
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT vmi.Codigo, 2 Normal ',
  'FROM _PRE_GPS_ gps ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  '  ON gps.GSPJmpNiv2=vmi.Controle ',
  '; ',
  ' ',
  'SELECT DISTINCT SrcNivel1 ',
  'FROM _PRE_DESC_ ',
  'ORDER BY SrcNivel1; ',
  ' ',
  '']);
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  PB2.Position := 0;
  PB2.Max := QrOri.RecordCount;
  while not QrOri.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    // 1a. Caleiros das entradas de couro in natura do codigo de entrada atual
    //     para saber o PDA(DTA) (campo RmsGGX)!
    UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
    'SELECT vmi.RmsGGX GGXDst, vmi.JmpGGX GGXSrc, ',
    'cab.Codigo, cab.MovimCod, vmi.SrcNivel2, ',
    'vmi.Controle VMICal, vmi.GraGruX GGXOriCal, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
       VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
       'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'vmi.QtdAntPeso, vmi.QtdAntPeca, vmi.QtdAntArM2, vmi.RmsNivel2, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN vscalcab   cab ON cab.Codigo=vmi.SrcNivel1 ',  // colorado!!!
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND vmi.SrcNivel1=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),  // 1...270...n
    '']);
    //Geral.MB_SQL(Self, QrIts);
    ObtemDataIniEFim(QrOri.FieldByName('SrcNivel1').AsInteger, DtIniOS, DtFimOS);
    //
    QrIts.First;
    while not QrIts.Eof do
    begin
      RegistroK210(TEstqMovimID.emidEmProcCal, DtIniOS, DtFimOS, LinArqK210);
      //
      QrIts.Next;
    end;
    //
    // 1b. Itens de caleiro do codigo atual
    UnDmkDAC_PF.AbreMySQLQuery0(QrMae, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND Codigo=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),
    //SQL_PeriodoVS,
    '']);
    QrMae.First;
    while not QrMae.Eof do
    begin
      // 1b1. Subprodutos gerados do item de entrada atual
      UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
      'SELECT vmi.GraGruX, ',
      'vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
      'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
         VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
         'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
      'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
      'WHERE vmi.GSPJmpNiv2=' + Geral.FF0(QrMae.FieldByName('Controle').AsInteger),
      SQL_PeriodoVS,
      '']);
      //if QrIts.RecordCount > 0 then
        //Geral.MB_SQL(Self, QrIts);
      QrIts.First;
      while not QrIts.Eof do
      begin
        RegistroK215_SubPrd(LinArqK210);
        //
        QrIts.Next;
      end;
      //
      QrMae.Next;
    end;
    //
    QrOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibPDA_210_Fast(const TipoRegSPEDProd: TTipoRegSPEDProd;
  const MovimID: TEstqMovimID; const MovimNiv: TEstqMovimNiv;
  const SQL_PeriodoVS: String; const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  COD_INS_SUBST = '';
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibPDA_210_Fast';
  //
var
  PsqIMEI: Integer;
  FListaSeq210: TMyArrOf2ArrOfInt;
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_PeriodoVS;
      //trsp23X: Result := SQL_PeriodoVS;
      //trsp25X: Result := SQL_PeriodoVS;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (7)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
  //
  // Obtem data inicial e final de movimentos de caleiro e geracao de subprodutos da entrada selecionada!
(*
  procedure ObtemDataIniEFim(const Codigo: Integer; var Ini, Fim: TDateTime);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDatas, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _PRE_DESC_ORI_MP_; ',
    'CREATE TABLE _PRE_DESC_ORI_MP_ ',
    'SELECT Controle  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND Codigo=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _PRE_DESC_DTIF_; ',
    ' ',
    'CREATE TABLE _PRE_DESC_DTIF_ ',
    'SELECT vmi.Controle, vmi.DataHora ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND vmi.SrcNivel1=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'INSERT IGNORE INTO _PRE_DESC_DTIF_ ',
    ' ',
    'SELECT vmi.Controle, vmi.DataHora  ',
    'FROM _PRE_DESC_ORI_MP_ ori  ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    '  ON ori.Controle=vmi.GSPSrcNiv2 ',
    'WHERE vmi.GSPSrcNiv2 <> 0 ',
    'AND vmi.GSPJmpNiv2=0; ',
    ' ',
    'SELECT MIN(DataHora) MinDH,  ',
    'MAX(DataHora) MaxDH  ',
    'FROM _PRE_DESC_DTIF_ ',
    'WHERE Controle > 0 ',
    '']);
    //Geral.MB_SQL(Self, QrDatas);
    //
    Ini := QrDatasMinDH.Value;
    Fim := QrDatasMaxDH.Value;
  end;
*)
  //
  function RegistroK210(const OriMovID: TEstqMovimID; const DtIniOS, DtFimOS:
  TDateTime; const SrcNivel1: Integer; var IDSeq1: Integer): Boolean;
  var
    DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
    QtdeSrc, QtdeDst, PecasSrc, PesoKgSrc, AreaM2Src, PecasDst, PesoKgDst,
    AreaM2Dst: Double;
    MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, GGXDst, ID_Item, Controle,
    ClientMO, FornecMO, EntiSitio, VMI_MovCod, TipoEstqDst: Integer;
    EhDoMes: Boolean;
  begin
    IDSeq1      := 0;
    AreaM2Src   := 0;
    PesoKgSrc   := 0;
    PecasSrc    := 0;
    AreaM2Dst   := 0;
    PesoKgDst   := 0;
    PecasDst    := 0;
    MyFator     := -1;
    QtdeSrc     := 0;
    QtdeDst     := 0;
    TipoEstq    := USQLDB.Int(DqIts, 'TipoEstq');
    Codigo      := USQLDB.Int(DqIts, 'Codigo');
    MovimCod    := USQLDB.Int(DqIts, 'MovimCod');
    VMI_MovCod  := USQLDB.Int(DqIts, 'VMI_MovCod');
    DtHrAberto  := DtIniOS;
    DtHrFimOpe  := DtFimOS;
    GGXSrc      := USQLDB.Int(DqIts, 'GGXSrc');
    // 215
    Data        := USQLDB.DtH(DqIts, 'DATA');
    GGXDst      := USQLDB.Int(DqIts, 'GGXDst');
    Controle    := USQLDB.Int(DqIts, 'RmsNivel2');
    ID_Item     := Controle;
    // 270
    DtMovim     := USQLDB.DtH(DqIts, 'DATA');
    DtCorrApo   := USQLDB.DtH(DqIts, 'DtCorrApo');
    //
    PsqIMEI     := USQLDB.Int(DqIts, 'Controle');
    //
    ClientMO    := USQLDB.Int(DqIts, 'ClientMO');
    FornecMO    := USQLDB.Int(DqIts, 'FornecMO');
    EntiSitio   := USQLDB.Int(DqIts, 'EntiSitio');
    TipoEstqDst := -999999999; // compatibilidade!
    //
    //DefineCliForSiteOnly_Fast(PsqIMEI, ClientMO, FornecMO, EntiSitio);
    DefineCliForSiteTipEstq_Fast(PsqIMEI, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
    //
    //
(*
*)
    //
    case MovimID of
      emidEmRibPDA:
      begin
        AreaM2Src := USQLDB.Flu(DqIts, 'AreaM2');
        PesoKgSrc := USQLDB.Flu(DqIts, 'PesoKg');
        PecasSrc  := USQLDB.Flu(DqIts, 'Pecas');
        //
        AreaM2Dst := USQLDB.Flu(DqIts, 'QtdAntArM2');
        PesoKgDst := USQLDB.Flu(DqIts, 'QtdAntPeso');
        PecasDst  := USQLDB.Flu(DqIts, 'QtdAntPeca');
      end;
      else
      begin
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        Exit;
      end;
    end;
    //
    EhDoMes := (Data >= FDiaIni) and (Data < FDiaPos);
    if EhDoMes then
    begin
      if (PecasSrc <> 0) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: QtdeSrc := PecasSrc;
          1: QtdeSrc := AreaM2Src;
          2: QtdeSrc := PesoKgSrc;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            DqIts.SQL.Text);
            //
            QtdeSrc := PecasSrc;
          end;
        end;
        //QtdeSrc := QtdeSrc * MyFator;
        if QtdeSrc < 0 then
          QtdeSrc := -QtdeSrc;
        if (*TemTwn and*) (QtdeSrc = 0) then
          VerificaTipoEstqQtde(TipoEstq, AreaM2Src, PesoKgSrc, PecasSrc,
          GGXSrc, MovimCod, Controle, sProcName);
      end else
        QtdeSrc := 0;
(*  ini 2019-02-07
    end else
      QtdeSrc := 0;
    //
    if ((PecasSrc <> 0) or (PesoKgSrc <> 0)) and (QtdeSrc = 0) then
      //Mensagem(sProcName, MovimCod, Integer(MovimID),
      Mensagem(sProcName, VMI_MovCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (16)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo) + sLineBreak +
      'IME-I: ' + Geral.FF0(PsqIMEI), DqIts);
    //
*)
//
      if ((PecasSrc <> 0) or (PesoKgSrc <> 0)) and (QtdeSrc = 0) then
        //Mensagem(sProcName, MovimCod, Integer(MovimID),
        Mensagem(sProcName, VMI_MovCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (16)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo) + sLineBreak +
        'IME-I: ' + Geral.FF0(PsqIMEI), DqIts);
    end else
    begin
      QtdeSrc := 0;
      //
      MeAviso.Lines.Add('MovimCod ' + Geral.FF0(VMI_MovCod) + ' IME-I ' +
      Geral.FF0(PsqIMEI) + ' QtdeSrc = ' + Geral.FFT(QtdeSrc, 3, siNegativo));
    end;
    //
//    fim 2019-02-07

    case TipoRegSPEDProd of
      trsp21X:
      begin
        // 2017-12-23 (b)
        if Controle <> 0 then
        // Fim 2017-12-23
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K210_Fast(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(OriMovID), Codigo, MovimCod, Controle, DtHrAberto,
          DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO, FornecMO, EntiSitio,
          QtdeSrc, OrigemOpeProc, oidk210RibPDA, FDiaFim, FTipoPeriodoFiscal,
          MeAviso, SrcNivel1, IDSeq1, F_BNCI_IDSeq1_K210, FListaSeq210);
        //
        if EhDoMes then
        begin
          if (PecasDst <> 0) (*and (DtHrFimOpe > 1)*) then
          begin
            case TipoEstq of
              0: QtdeDst := PecasDst;
              1: QtdeDst := AreaM2Dst;
              2: QtdeDst := PesoKgDst;
              else
              begin
                Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                DqIts.SQL.Text);
                //
                QtdeDst := PecasDst;
              end;
            end;
            //QtdeDst := QtdeDst * MyFator;
          end else
            QtdeDst := 0;
          //
          if ((PecasDst <> 0) or (PesoKgDst <> 0)) and (QtdeDst = 0) then
          begin
            if (*TemTwn and*) (QtdeSrc = 0) then
              VerificaTipoEstqQtde(TipoEstq, AreaM2Src, PesoKgSrc, PecasSrc,
              GGXSrc, MovimCod, Controle, sProcName);
            if (*TemTwn and*) (QtdeDst = 0) then
              VerificaTipoEstqQtde(TipoEstq, AreaM2Dst, PesoKgDst, PecasDst,
              GGXDst, MovimCod, Controle, sProcName);
            if QtdeDst < 0 then
              Mensagem(sProcName, MovimCod, Integer(MovimID),
              'Quantidade n�o pode ser negativa!!! (17)' + sLineBreak +
              'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), nil)
            else if (QtdeDst = 0) and (DtHrFimOpe > 1) then
            begin
              case MovimID of
                emidEmProcCal: DtHrFimOpe := 0;
                emidEmProcCur: DtHrFimOpe := 0;
                else Mensagem(sProcName, MovimCod, Integer(MovimID),
                'Quantidade zerada para OP encerrada!!! (9)', nil);
              end;
            end;
          end;
          //
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215_Fast(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtMovim, DtCorrApo, GGXDst,
          QtdeDst, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibPDA,
          ESTSTabSorc, FTipoPeriodoFiscal, MeAviso, F_BNCI_IDSeq2_K215);
        end;
      end;
      else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(26)');
    end;
  end;
  //
  //
  function RegistroK215_SubPrd(const IDSeq1: Integer): Boolean;
  var
    Data, DtMovim, DtCorrApo: TDateTime;
    QtdeDst, PecasDst, PesoKgDst, AreaM2Dst: Double;
    Codigo, MovimCod, MyFator, TipoEstq, GGXDst, ID_Item, Controle,
    ClientMO, FornecMO, EntiSitio, MyMovimID: Integer;
  begin
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeDst    := 0;
    TipoEstq   := USQLDB.Int(DqIts, 'TipoEstq');
    MyMovimID  := USQLDB.Int(DqIts, 'MovimID');
    Codigo     := USQLDB.Int(DqIts, 'Codigo');
    MovimCod   := USQLDB.Int(DqIts, 'MovimCod');
    // 215
    Data       := USQLDB.DtH(DqIts, 'DATA');
    GGXDst     := USQLDB.Int(DqIts, 'GraGruX');
    Controle   := USQLDB.Int(DqIts, 'Controle');
    ID_Item    := Controle;
    //
    DtMovim    := USQLDB.DtH(DqIts, 'DATA');
    DtCorrApo  := USQLDB.DtH(DqIts, 'DtCorrApo');
    //
    ClientMO   := USQLDB.Int(DqIts, 'ClientMO');
    FornecMO   := USQLDB.Int(DqIts, 'FornecMO');
    EntiSitio  := USQLDB.Int(DqIts, 'EntiSitio');
    //
    case MovimID of
      emidEmRibPDA:
      begin
        AreaM2Dst := USQLDB.Flu(DqIts, 'AreaM2');
        PesoKgDst := USQLDB.Flu(DqIts, 'PesoKg');
        PecasDst  := USQLDB.Flu(DqIts, 'Pecas');
      end;
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //
    //Parei aqui !!!  � asimm???
    if (PecasDst <> 0) or (PesoKgDst <> 0)(*and (DtHrFimOpe > 1)*) then
    begin
      case TipoEstq of
        0: QtdeDst := PecasDst;
        1: QtdeDst := AreaM2Dst;
        2: QtdeDst := PesoKgDst;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          DqIts.SQL.Text);
          //
          QtdeDst := PecasDst;
        end;
      end;
      //QtdeDst := QtdeDst * MyFator;
      QtdeDst := QtdeDst;
    end else
      QtdeDst := 0;
    if (*TemTwn and*) (QtdeDst = 0) then
      VerificaTipoEstqQtde(TipoEstq, AreaM2Dst, PesoKgDst, PecasDst,
      GGXDst, MovimCod, Controle, sProcName);
    //
    if QtdeDst < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (18)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), nil);
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215_Fast(FImporExpor, FAnoMes, FEmpresa, FPeriApu,
    IDSeq1, Data, DtMovim, DtCorrApo, GGXDst, QtdeDst,
    COD_INS_SUBST, ID_Item, MyMovimID, Codigo, MovimCod, Controle, ClientMO,
    FornecMO, EntiSitio, OrigemOpeProc, oidk210RibPDA, ESTSTabSorc,
    FTipoPeriodoFiscal, MeAviso, F_BNCI_IDSeq2_K215);
  end;
var
  LinArqK210, VMI_MovCod, SrcNivel1, I, K: Integer;
  //Data
  DtIniOS, DtFimOS: TDateTime;
  SQL: String;
begin
(*
Tempo: 00:02:14:656
Reaberturas de queries normais: 1367
Reaberturas de queries direct: 0
*)
  MyObjects.Informa2(LaAviso3, LaAviso4, True, 'Gerando dados base: 1. C�digos das entradas de mat�ria-prima que caleirou dentro do m�s');
  FParar := False;
  Result := True;
  // 1. Codigos das entradas de materia-prima que caleirou dentro do mes
  //UnDmkDAC_PF.AbreMySQLDirectQuery0(DqOri, DModG.MyPID_CompressDB, [
  SQL := Geral.ATS([
  'DROP TABLE IF EXISTS _PRE_GPS_; ',
  'CREATE TABLE _PRE_GPS_ ',
  'SELECT GSPSrcNiv2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE GSPSrcNiv2<>0 ',
  'AND GSPJmpNiv2=0 ',
  SQL_PeriodoVS,
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_1; ',
  'CREATE TABLE _PRE_DESC_1 ',
  ' ',
  'SELECT DISTINCT SrcNivel1, 1 Normal ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  'AND SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  SQL_PeriodoVS,
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT vmi.Codigo, 2 Normal ',
  'FROM _PRE_GPS_ gps ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  '  ON gps.GSPSrcNiv2=vmi.Controle ',
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_2; ',
  'CREATE TABLE _PRE_DESC_2 ',
  ' ',
  'SELECT DISTINCT SrcNivel1 ',
  'FROM _PRE_DESC_1 ',
  ' ',
  ' ']);
  //
  //Geral.MB_Info(SQL);
  DModG.MyPID_CompressDB.Execute(SQL);
  //
{
  // 2. IDSeq 210
  MyObjects.Informa2(LaAviso3, LaAviso4, True, 'Gerando dados base: 2. IDSeq de cada Ordem de Produ��o');
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqOri, DModG.MyPID_CompressDB, [
  'SELECT DISTINCT SrcNivel1 ',
  'FROM _PRE_DESC_2 ',
  'ORDER BY SrcNivel1; ',
  ' ',
  '']);
  DqOri.First;
  //PB2.Position := 0;
  //PB2.Max := DqOri.RecordCount;
  SetLength(FListaSeq210, DqOri.RecordCount);
  while not DqOri.Eof do
  begin
    FListaSeq210[DqOri.RecNo - 1][0] := USQLDB.Int(DqOri, 'SrcNivel1');
    FListaSeq210[DqOri.RecNo - 1][1] := GetSeq1
    /
    DqOri.Next;
  end;
  //if DqOri.RecordCount > 0 then
    //Geral.MB_Aviso('DEPRRCAR!!! 210 - 1');
  //
  //Geral.MB_SQL(Self, DqOri);
}
  //
  // 3. Datas minimas e m�ximas de cada Ordem de Produ��o
  MyObjects.Informa2(LaAviso3, LaAviso4, True, 'Gerando dados base: 3. Datas minimas e m�ximas de cada Ordem de Produ��o');
  SQL := Geral.ATS([
  'DROP TABLE IF EXISTS _PRE_DESC_ORI_MP_; ',
  'CREATE TABLE _PRE_DESC_ORI_MP_ ',
  'SELECT vmi.Controle  ',
  'FROM _pre_desc_2 pd2',
  'LEFT JOIN ' + FTabIMEI + ' vmi ON ',
  '   vmi.SrcNivel1=pd2.SrcNivel1',
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_DTIF_; ',
  ' ',
  'CREATE TABLE _PRE_DESC_DTIF_ ',
  'SELECT vmi.SrcNivel1, vmi.Controle, vmi.DataHora ',
  'FROM _pre_desc_2 pd2',
  'LEFT JOIN ' + FTabIMEI + ' vmi ON ',
  '   vmi.SrcNivel1=pd2.SrcNivel1',
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  '; ',
  ' ',
  'INSERT IGNORE INTO _PRE_DESC_DTIF_ ',
  ' ',
  'SELECT vmi.SrcNivel1, vmi.Controle, vmi.DataHora  ',
  'FROM _PRE_DESC_ORI_MP_ ori  ',
  'LEFT JOIN ' + FTabIMEI + ' vmi ',
  '  ON ori.Controle=vmi.GSPSrcNiv2 ',
  'WHERE vmi.GSPSrcNiv2 <> 0 ',
  'AND vmi.GSPJmpNiv2=0; ',
  ' ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_DT_MIN_MAX_; ',
  ' ',
  'CREATE TABLE _PRE_DESC_DT_MIN_MAX_ ',
  'SELECT SrcNivel1, MIN(DataHora) MinDH,  ',
  'MAX(DataHora) MaxDH  ',
  'FROM _PRE_DESC_DTIF_ ',
  'WHERE Controle > 0 ',
  'GROUP BY SrcNivel1',
  ' ']);
  //
  //Geral.MB_Info(SQL);
  DModG.MyPID_CompressDB.Execute(SQL);
  //
(*
  DqOri.First;
  PB2.Position := 0;
  PB2.Max := DqOri.RecordCount;
  while not DqOri.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    //
    SrcNivel1 := USQLDB.Int(DqOri, 'SrcNivel1');
    //
    // 1a. Caleiros das entradas de couro in natura do codigo de entrada atual
    //     para saber o PDA (campo RmsGGX)!
    UnDmkDAC_PF.AbreMySQLDIrectQuery0(DqIts, Dmod.MyDB, [
    'SELECT vmi.RmsGGX GGXDst, vmi.GraGruX GGXSrc, ',
    'cab.Codigo, cab.MovimCod, vmi.Controle, vmi.SrcNivel2, ',
    'vmi.Controle VMICal, vmi.GraGruX GGXOriCal, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
       VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
       'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'vmi.QtdAntPeso, vmi.QtdAntPeca, vmi.QtdAntArM2, vmi.RmsNivel2, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza, ',
    'vmi.MovimCod VMI_MovCod ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN vsinncab   cab ON cab.Codigo=vmi.SrcNivel1 ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1),  // 1...270...n
    //SQL_PeriodoVS,
    '']);
    //Geral.MB_SQL(Self, DqIts);
*)
  //
  // 4. Caleiros das entradas de couro in natura do codigo de entrada atual
  //     para saber o PDA (campo RmsGGX)!
  MyObjects.Informa2(LaAviso3, LaAviso4, True, '4. Caleiros das entradas de couro in natura do codigo de entrada atual para saber o PDA ');
  UnDmkDAC_PF.AbreMySQLDIrectQuery0(DqIts, DModG.MyPID_CompressDB, [
  'SELECT pd2.SrcNivel1, dmm.MinDH, dmm.MaxDH, ',
  'vmi.RmsGGX GGXDst, vmi.GraGruX GGXSrc,  ',
  'cab.Codigo, cab.MovimCod, vmi.Controle, vmi.SrcNivel2,  ',
  'vmi.Controle VMICal, vmi.GraGruX GGXOriCal,  ',
  'vmi.Pecas, vmi.PesoKg, vmi.AreaM2,  ',
  'DATE(vmi.DataHora) DATA, vmi.DtCorrApo,  ',
  'IF(gg1.UnidMed <> 0, med.Grandeza,  ',
  '  IF(xco.Grandeza > 0, xco.Grandeza,  ',
  '  IF((ggx.GraGruY<2048 OR  ',
  '  xco.CouNiv2<>1) AND vmi.PesoKg <> 0, 2,  ',
  '  IF(xco.CouNiv2=1 AND vmi.AreaM2 <> 0, 1,  ',
  '  IF(vmi.Pecas=0, 5, 9 ))))) TipoEstq,  ',
  ' ',
  ' ',
  'vmi.QtdAntPeso, vmi.QtdAntPeca, vmi.QtdAntArM2, vmi.RmsNivel2,  ',
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza,  ',
  'vmi.MovimCod VMI_MovCod  ',
  'FROM _PRE_DESC_2 pd2 ',
  'LEFT JOIN ' + FTabIMEI + ' vmi ON vmi.SrcNivel1=pd2.SrcNivel1 ',
  'LEFT JOIN ' + TMeuDB + '.vsinncab   cab ON cab.Codigo=vmi.SrcNivel1  ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed  ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc  scl ON scl.Controle=vmi.StqCenLoc  ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad  scc ON scc.Codigo=scl.Codigo  ',
  'LEFT JOIN _PRE_DESC_DT_MIN_MAX_     dmm ON dmm.SrcNivel1=pd2.SrcNivel1  ',
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  '']);
  //Geral.MB_SQL(Self, DqIts);
  DqIts.First;
  PB2.Position := 0;
  PB2.Max := DqIts.RecordCount;
  // 5. Importando dados K210/K215 Produtos principais
  MyObjects.Informa2(LaAviso3, LaAviso4, True, 'Gerando dados base: 5. Importando dados K210');
  while not DqIts.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    //
    SrcNivel1 := USQLDB.Int(DqIts, 'SrcNivel1');
    //
    //ObtemDataIniEFim(SrcNivel1, DtIniOS, DtFimOS);
    //
{
    DqIts.First;
    while not DqIts.Eof do
    begin
      if FParar then
        Exit;
      //Data := DqIts.FieldByName('DATA').AsDateTime;
      //if (Data >= DiaIni) and (Data < DiaPos) then
}
      RegistroK210(TEstqMovimID.emidCompra, DtIniOS, DtFimOS, SrcNivel1, LinArqK210);
{
      //
      DqIts.Next;
    end;
}
    DqIts.Next;
  end;
  //
  // 6. Itens de entrada de materia-prima do codigo atual
  MyObjects.Informa2(LaAviso3, LaAviso4, True, '6. Itens de entrada de materia-prima do codigo atual para saber os subprodutos');
  //UnDmkDAC_PF.AbreMySQLDIrectQuery0(DqIts, DModG.MyPID_CompressDB, [
  SQL := Geral.ATS([
  'DROP TABLE IF EXISTS _PRE_DESC_SUB_PRD_1; ',
  'CREATE TABLE _PRE_DESC_SUB_PRD_1 ',
  ' ',
  'SELECT vmi.Codigo, vmi.Controle ',
  'FROM _pre_desc_2 pd2',
  'LEFT JOIN ' + FTabIMEI + ' vmi ON ',
  '   vmi.Codigo=pd2.SrcNivel1',
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  //'AND Codigo=' + Geral.FF0(SrcNivel1),  // 1...270...n
  '; ',
  '']);
  //Geral.MB_Info(SQL);
  DModG.MyPID_CompressDB.Execute(SQL);
  //
  UnDmkDAC_PF.AbreMySQLDIrectQuery0(DqIts, DModG.MyPID_CompressDB, [
  'SELECT pds.Codigo pds_SrcNivel1, vmi.GraGruX, ',
  'vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
  'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
  'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
     VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
     'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM _PRE_DESC_SUB_PRD_1  pds',
  'LEFT JOIN ' + FTabIMEI + ' vmi ON vmi.GSPSrcNiv2=pds.Controle',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad  scc ON scc.Codigo=scl.Codigo',
  'WHERE vmi.GSPJmpNiv2=0 ',
  'ORDER BY vmi.Controle ',
  '']);

  //Geral.MB_SQL(Self, DqIts);
{
  QrMae.First;
  while not QrMae.Eof do
  begin
    // 1b1. Subprodutos gerados do item de entrada atual
    UnDmkDAC_PF.AbreMySQLQuery0(DqIts, Dmod.MyDB, [
    'SELECT vmi.GraGruX, ',
    'vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
       VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
       'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.GSPJmpNiv2=0 ',
    'AND vmi.GSPSrcNiv2=' + Geral.FF0(QrMae.FieldByName('Controle').AsInteger),
    SQL_PeriodoVS,
    '']);
    //Geral.MB_SQL(Self, DqIts);
}
    DqIts.First;
    PB2.Position := 0;
    PB2.Max := DqIts.RecordCount;
    while not DqIts.Eof do
    begin
      if FParar then
        Exit;
      MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
      //
      LinArqK210 := 0;
      //SrcNivel1  := USQLDB.Int(DqIts, 'SrcNivel1');
      SrcNivel1  := USQLDB.Int(DqIts, 'pds_SrcNivel1');
      //JaFoi := False;
      K := Length(FListaSeq210);
      for I := 0 to K -1 do
      begin
        if FListaSeq210[I][0] = SrcNivel1 then
        begin
          LinArqK210 := FListaSeq210[I][1];
          //JaFoi := True;
          Break;
        end;
      end;
      RegistroK215_SubPrd(LinArqK210);
      //
      DqIts.Next;
    end;
    //
(*
    QrMae.Next;
  end;
*)
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibPDA_210_Slow(const TipoRegSPEDProd: TTipoRegSPEDProd;
  const MovimID: TEstqMovimID; const MovimNiv: TEstqMovimNiv;
  const SQL_PeriodoVS: String; const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  COD_INS_SUBST = '';
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibPDA_210_Slow';
  //
var
  QrIts: TmySQLQuery;
  PsqIMEI: Integer;
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_PeriodoVS;
      //trsp23X: Result := SQL_PeriodoVS;
      //trsp25X: Result := SQL_PeriodoVS;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (7)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
  //
  // Obtem data inicial e final de movimentos de caleiro e geracao de subprodutos da entrada selecionada!
  procedure ObtemDataIniEFim(const Codigo: Integer; var Ini, Fim: TDateTime);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDatas, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _PRE_DESC_ORI_MP_; ',
    'CREATE TABLE _PRE_DESC_ORI_MP_ ',
    'SELECT Controle  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND Codigo=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _PRE_DESC_DTIF_; ',
    ' ',
    'CREATE TABLE _PRE_DESC_DTIF_ ',
    'SELECT vmi.Controle, vmi.DataHora ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND vmi.SrcNivel1=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'INSERT IGNORE INTO _PRE_DESC_DTIF_ ',
    ' ',
    'SELECT vmi.Controle, vmi.DataHora  ',
    'FROM _PRE_DESC_ORI_MP_ ori  ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    '  ON ori.Controle=vmi.GSPSrcNiv2 ',
    'WHERE vmi.GSPSrcNiv2 <> 0 ',
    'AND vmi.GSPJmpNiv2=0; ',
    ' ',
    'SELECT MIN(DataHora) MinDH,  ',
    'MAX(DataHora) MaxDH  ',
    'FROM _PRE_DESC_DTIF_ ',
    'WHERE Controle > 0 ',
    '']);
    //Geral.MB_SQL(Self, QrDatas);
    //
    Ini := QrDatasMinDH.Value;
    Fim := QrDatasMaxDH.Value;
  end;
  //
  function RegistroK210(const OriMovID: TEstqMovimID; const DtIniOS, DtFimOS:
  TDateTime; var IDSeq1: Integer): Boolean;
  var
    DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
    QtdeSrc, QtdeDst, PecasSrc, PesoKgSrc, AreaM2Src, PecasDst, PesoKgDst,
    AreaM2Dst: Double;
    MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, GGXDst, ID_Item, Controle,
    ClientMO, FornecMO, EntiSitio, VMI_MovCod: Integer;
    EhDoMes: Boolean;
  begin
    IDSeq1     := 0;
    AreaM2Src  := 0;
    PesoKgSrc  := 0;
    PecasSrc   := 0;
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := -1;
    QtdeSrc    := 0;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    VMI_MovCod := QrIts.FieldByName('VMI_MovCod').AsInteger;
    DtHrAberto := DtIniOS;
    DtHrFimOpe := DtFimOS;
    GGXSrc     := QrIts.FieldByName('GGXSrc').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GGXDst').AsInteger;
    Controle   := QrIts.FieldByName('RmsNivel2').AsInteger;
    ID_Item    := Controle;
    // 270
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    PsqIMEI    := QrIts.FieldByName('Controle').AsInteger;
    DefineCliForSiteOnly(PsqIMEI, ClientMO, FornecMO, EntiSitio);
    //
(*
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
*)
    //
    case MovimID of
      emidEmRibPDA:
      begin
        AreaM2Src := QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgSrc := QrIts.FieldByName('PesoKg').AsFloat;
        PecasSrc  := QrIts.FieldByName('Pecas').AsFloat;
        //
        AreaM2Dst := QrIts.FieldByName('QtdAntArM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('QtdAntPeso').AsFloat;
        PecasDst  := QrIts.FieldByName('QtdAntPeca').AsFloat;
      end;
      else
      begin
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        Exit;
      end;
    end;
    //
    EhDoMes := (Data >= FDiaIni) and (Data < FDiaPos);
    if EhDoMes then
    begin
      if (PecasSrc <> 0) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: QtdeSrc := PecasSrc;
          1: QtdeSrc := AreaM2Src;
          2: QtdeSrc := PesoKgSrc;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrIts.SQL.Text);
            //
            QtdeSrc := PecasSrc;
          end;
        end;
        //QtdeSrc := QtdeSrc * MyFator;
        if QtdeSrc < 0 then
          QtdeSrc := -QtdeSrc;
        if (*TemTwn and*) (QtdeSrc = 0) then
          VerificaTipoEstqQtde(TipoEstq, AreaM2Src, PesoKgSrc, PecasSrc,
          GGXSrc, MovimCod, Controle, sProcName);
      end else
        QtdeSrc := 0;
(*  ini 2019-02-07
    end else
      QtdeSrc := 0;
    //
    if ((PecasSrc <> 0) or (PesoKgSrc <> 0)) and (QtdeSrc = 0) then
      //Mensagem(sProcName, MovimCod, Integer(MovimID),
      Mensagem(sProcName, VMI_MovCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (16)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo) + sLineBreak +
      'IME-I: ' + Geral.FF0(PsqIMEI), QrIts);
    //
*)
//
      if ((PecasSrc <> 0) or (PesoKgSrc <> 0)) and (QtdeSrc = 0) then
        //Mensagem(sProcName, MovimCod, Integer(MovimID),
        Mensagem(sProcName, VMI_MovCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (16)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo) + sLineBreak +
        'IME-I: ' + Geral.FF0(PsqIMEI), QrIts);
    end else
    begin
      QtdeSrc := 0;
      //
      MeAviso.Lines.Add('MovimCod ' + Geral.FF0(VMI_MovCod) + ' IME-I ' +
      Geral.FF0(PsqIMEI) + ' QtdeSrc = ' + Geral.FFT(QtdeSrc, 3, siNegativo));
    end;
    //
//    fim 2019-02-07

    case TipoRegSPEDProd of
      trsp21X:
      begin
        // 2017-12-23 (b)
        if Controle <> 0 then
        // Fim 2017-12-23
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K210(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(OriMovID), Codigo, MovimCod, Controle, DtHrAberto,
          DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO, FornecMO, EntiSitio,
          QtdeSrc, OrigemOpeProc, oidk210RibPDA, FDiaFim, FTipoPeriodoFiscal,
          MeAviso, IDSeq1);
        //
        if EhDoMes then
        begin
          if (PecasDst <> 0) (*and (DtHrFimOpe > 1)*) then
          begin
            case TipoEstq of
              0: QtdeDst := PecasDst;
              1: QtdeDst := AreaM2Dst;
              2: QtdeDst := PesoKgDst;
              else
              begin
                Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                QrIts.SQL.Text);
                //
                QtdeDst := PecasDst;
              end;
            end;
            //QtdeDst := QtdeDst * MyFator;
          end else
            QtdeDst := 0;
          //
          if ((PecasDst <> 0) or (PesoKgDst <> 0)) and (QtdeDst = 0) then
          begin
            if (*TemTwn and*) (QtdeSrc = 0) then
              VerificaTipoEstqQtde(TipoEstq, AreaM2Src, PesoKgSrc, PecasSrc,
              GGXSrc, MovimCod, Controle, sProcName);
            if (*TemTwn and*) (QtdeDst = 0) then
              VerificaTipoEstqQtde(TipoEstq, AreaM2Dst, PesoKgDst, PecasDst,
              GGXDst, MovimCod, Controle, sProcName);
            if QtdeDst < 0 then
              Mensagem(sProcName, MovimCod, Integer(MovimID),
              'Quantidade n�o pode ser negativa!!! (17)' + sLineBreak +
              'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), nil)
            else if (QtdeDst = 0) and (DtHrFimOpe > 1) then
            begin
              case MovimID of
                emidEmProcCal: DtHrFimOpe := 0;
                emidEmProcCur: DtHrFimOpe := 0;
                else Mensagem(sProcName, MovimCod, Integer(MovimID),
                'Quantidade zerada para OP encerrada!!! (9)', nil);
              end;
            end;
          end;
          //
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtMovim, DtCorrApo, GGXDst,
          QtdeDst, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibPDA,
          ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
      end;
      else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(26)');
    end;
  end;
  //
  //
  function RegistroK215_SubPrd(const IDSeq1: Integer): Boolean;
  var
    Data, DtMovim, DtCorrApo: TDateTime;
    QtdeDst, PecasDst, PesoKgDst, AreaM2Dst: Double;
    Codigo, MovimCod, MyFator, TipoEstq, GGXDst, ID_Item, Controle,
    ClientMO, FornecMO, EntiSitio, MyMovimID: Integer;
  begin
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    MyMovimID  := QrIts.FieldByName('MovimID').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GraGruX').AsInteger;
    Controle   := QrIts.FieldByName('Controle').AsInteger;
    ID_Item    := Controle;
    //
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibPDA:
      begin
        AreaM2Dst := QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('PesoKg').AsFloat;
        PecasDst  := QrIts.FieldByName('Pecas').AsFloat;
      end;
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //
    //Parei aqui !!!  � asimm???
    if (PecasDst <> 0) or (PesoKgDst <> 0)(*and (DtHrFimOpe > 1)*) then
    begin
      case TipoEstq of
        0: QtdeDst := PecasDst;
        1: QtdeDst := AreaM2Dst;
        2: QtdeDst := PesoKgDst;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrIts.SQL.Text);
          //
          QtdeDst := PecasDst;
        end;
      end;
      //QtdeDst := QtdeDst * MyFator;
      QtdeDst := QtdeDst;
    end else
      QtdeDst := 0;
    if (*TemTwn and*) (QtdeDst = 0) then
      VerificaTipoEstqQtde(TipoEstq, AreaM2Dst, PesoKgDst, PecasDst,
      GGXDst, MovimCod, Controle, sProcName);
    //
    if QtdeDst < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (18)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), nil);
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa, FPeriApu,
    IDSeq1, Data, DtMovim, DtCorrApo, GGXDst, QtdeDst,
    COD_INS_SUBST, ID_Item, MyMovimID, Codigo, MovimCod, Controle, ClientMO,
    FornecMO, EntiSitio, OrigemOpeProc, oidk210RibPDA, ESTSTabSorc,
    FTipoPeriodoFiscal, MeAviso);
  end;
var
  LinArqK210, VMI_MovCod: Integer;
  //Data
  DtIniOS, DtFimOS: TDateTime;
begin
  FParar := False;
  QrIts := TmySQLQuery.Create(Dmod);
  try
  Result := True;
  // 1. Codigos das entradas de materia-prima que caleirou dentro do mes
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _PRE_GPS_; ',
  'CREATE TABLE _PRE_GPS_ ',
  'SELECT GSPSrcNiv2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE GSPSrcNiv2<>0 ',
  'AND GSPJmpNiv2=0 ',
  SQL_PeriodoVS,
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_; ',
  'CREATE TABLE _PRE_DESC_ ',
  ' ',
  'SELECT DISTINCT SrcNivel1, 1 Normal ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  'AND SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  SQL_PeriodoVS,
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT vmi.Codigo, 2 Normal ',
  'FROM _PRE_GPS_ gps ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  '  ON gps.GSPSrcNiv2=vmi.Controle ',
  '; ',
  ' ',
  'SELECT DISTINCT SrcNivel1 ',
  'FROM _PRE_DESC_ ',
  'ORDER BY SrcNivel1; ',
  ' ',
  '']);
  //if QrOri.RecordCount > 0 then
    //Geral.MB_Aviso('DEPRRCAR!!! 210 - 1');
  //
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  PB2.Position := 0;
  PB2.Max := QrOri.RecordCount;
  while not QrOri.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    // 1a. Caleiros das entradas de couro in natura do codigo de entrada atual
    //     para saber o PDA (campo RmsGGX)!
    UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
    'SELECT vmi.RmsGGX GGXDst, vmi.GraGruX GGXSrc, ',
    'cab.Codigo, cab.MovimCod, vmi.Controle, vmi.SrcNivel2, ',
    'vmi.Controle VMICal, vmi.GraGruX GGXOriCal, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
       VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
       'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'vmi.QtdAntPeso, vmi.QtdAntPeca, vmi.QtdAntArM2, vmi.RmsNivel2, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza, ',
    'vmi.MovimCod VMI_MovCod ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN vsinncab   cab ON cab.Codigo=vmi.SrcNivel1 ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND vmi.SrcNivel1=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),  // 1...270...n
    //SQL_PeriodoVS,
    '']);
    //Geral.MB_SQL(Self, QrIts);
    ObtemDataIniEFim(QrOri.FieldByName('SrcNivel1').AsInteger, DtIniOS, DtFimOS);
    //
    QrIts.First;
    while not QrIts.Eof do
    begin
      if FParar then
        Exit;
      //Data := QrIts.FieldByName('DATA').AsDateTime;
      //if (Data >= DiaIni) and (Data < DiaPos) then
      RegistroK210(TEstqMovimID.emidCompra, DtIniOS, DtFimOS, LinArqK210);
      //
      QrIts.Next;
    end;
    //
    // 1b. Itens de entrada de materia-prima do codigo atual
    UnDmkDAC_PF.AbreMySQLQuery0(QrMae, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND Codigo=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),  // 1...270...n
    //SQL_PeriodoVS,
    '']);
    //Geral.MB_SQL(Self, QrMae);
    QrMae.First;
    while not QrMae.Eof do
    begin
      // 1b1. Subprodutos gerados do item de entrada atual
      UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
      'SELECT vmi.GraGruX, ',
      'vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
      'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
         VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
         'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
      'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
      'WHERE vmi.GSPJmpNiv2=0 ',
      'AND vmi.GSPSrcNiv2=' + Geral.FF0(QrMae.FieldByName('Controle').AsInteger),
      SQL_PeriodoVS,
      '']);
      //Geral.MB_SQL(Self, QrIts);
      QrIts.First;
      while not QrIts.Eof do
      begin
        RegistroK215_SubPrd(LinArqK210);
        //
        QrIts.Next;
      end;
      //
      QrMae.Next;
    end;
    //
    QrOri.Next;
  end;
  finally
    QrIts.Free;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Mensagem(ProcName: String; MovimCod,
  MovimID: Integer; TextoBase: String; Query: TComponent; Parar: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Mensagem()';
var
  Qry_Text: String;
begin
  if Query <> nil then
  begin
    if Query is  TmySQLQuery then
      Qry_Text := TmySQLQuery(Query).SQL.Text
    else
    if Query is  TmySQLDirectQuery then
      Qry_Text := TmySQLDirectQuery(Query).SQL.Text
    else
      Qry_Text := 'Componente Qry n�o implementado "' + Query.Name + '.' +
      sProcName + '" em ' + sProcName;
  end else
    Qry_Text := '';
  //
  Geral.MB_ERRO(TextoBase + sLineBreak +
  'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
  'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
  ProcName + '.' + sProcName + sLineBreak +
  sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
  ProcName + sLineBreak + sLineBreak +
  Qry_Text);
  //
  if Parar then
    FParar := True;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.PesquisaIMECsRelacionados(
  TargetMovimID, PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; PsqMovimNiv_VMI:
  TEstqMovimNiv; SQL_PeriodoVS, SQL_PeriodoPQ: String; IncluiPQx: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.PesquisaIMECsRelacionados()';
  //
  procedure InsereVMI();
  var
    SQL_MovimNiv: String;
  begin
    case TargetMovimID of
      TEStqMovimID.emidEmOperacao,
      TEStqMovimID.emidEmProcWE,
      TEStqMovimID.emidEmProcSP,
      TEStqMovimID.emidEmReprRM:
        SQL_MovimNiv := '';
      TEStqMovimID.emidEmProcCal,
      TEStqMovimID.emidCaleado,
      TEStqMovimID.emidEmProcCur,
      TEStqMovimID.emidCurtido:
        SQL_MovimNiv := 'AND MovimNiv=' + Geral.FF0(Integer(PsqMovimNiv_VMI));
      else begin
        Geral.MB_Erro(
        GetEnumName(TypeInfo(TEStqMovimID), Integer(TargetMovimID)) +
        ' indefinido em ' + sprocName);
        //
        SQL_MovimNiv := 'AND MovimNiv=' + Geral.FF0(Integer(PsqMovimNiv_VMI));
      end;
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FSEII_IMEcs,
    'SELECT DISTINCT IF(MovCodPai<>0, MovCodPai, MovimCod) MovCodPai, ',
    'Codigo, MovimCod, MovimID, ',
    'JmpMovID, JmpNivel1, JmpNivel2, ',
    'SrcMovID, SrcNivel1, SrcNivel2, ',
    'YEAR(DtCorrApo) AnoCA, ',
    'MONTH(DtCorrApo) MesCA, ',
    '"VMI" TipoTab, 1 Ativo ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI ,
    'WHERE MovimID=' + Geral.FF0(Integer(PsqMovimID_VMI)),
    SQL_MovimNiv,
    SQL_PeriodoVS,
    '']);
    //Geral.MB_SQL(Self, DModG.QrUpdPID1);
  end;
  procedure InserePQX(Tabela: String; Tipo: Integer);
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FSEII_IMEcs,
    'SELECT DISTINCT VSMovCod MovCodPai, vmc.CodigoID Codigo, ',
    'cab.VSMovCod MovimCod, vmc.MovimID,  ',
    '0 JmpMovID, 0 JmpNivel1, 0 JmpNivel2, ',
    '0 SrcMovID, 0 SrcNivel1, 0 SrcNivel2, ',
    'YEAR(pqx.DtCorrApo) AnoCA, ',
    'MONTH(pqx.DtCorrApo) MesCA, ',
    '"PQX" TipoTab, 1 Ativo ',
    'FROM ' + TMeuDB + '.pqx pqx',
    'LEFT JOIN ' + TMeuDB + '.' + Tabela +' cab ON cab.Codigo=pqx.OrigemCodi ',
    '   AND pqx.Tipo=' + Geral.FF0(Tipo),
    'LEFT JOIN ' + TMeuDB + '.vsmovcab vmc ON vmc.Codigo=cab.VSMovCod',
    'WHERE vmc.MovimID=' + Geral.FF0(Integer(PsqMovimID_PQX)),
    SQL_PeriodoPQ,
    '']);
    //Geral.MB_SQL(Self, DModG.QrUpdPID1);
  end;
begin
  DModG.MyPID_DB.Execute('DELETE FROM ' + FSEII_IMEcs);
  //
  InsereVMI();
  if IncluiPQX then
  begin
    InserePQX('emit', VAR_FATID_0110);
    InserePQX('pqo', VAR_FATID_0190);
  end;
  //
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenObtCliForSite(Forma:
  TOpenCliForSite; Controle, MovimCod, MovimNiv: Integer);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenObtCliForSite()';
var
  SQL_Psq: String;
begin
  case Forma of
    //ocfsIndef
    ocfsControle:    SQL_Psq := 'WHERE vmi.Controle=' + Geral.FF0(Controle);
    ocfsMovIdNivCod: SQL_Psq := 'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod) +
                                ' AND vmi.MovimNiv=' + Geral.FF0(MovimNiv);
    ocfsSrcNivel2:   SQL_Psq := 'WHERE vmi.SrcNivel2=' + Geral.FF0(Controle);
    else
    begin
      SQL_Psq := 'WHERE vmi.???=???';
      Geral.MB_Erro('Forma indefinida em ' + sProcName)
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrObtCliForSite, Dmod.MyDB, [
  'SELECT vmi.MovimID, vmi.MovimNiv, vmi.MovimCod, ',
  'vmi.Codigo, vmi.ClientMO, vmi.FornecMO, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'scc.EntiSitio  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc  ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  //'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1  ',
  //'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2  ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  //'WHERE vmi.Controle=' + Geral.FF0(Controle),
  SQL_Psq,
  '']);
  //Geral.MB_SQL(Self, QrObtCliForSite);
  //N�o fechar!!!
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenOriPQx(MovimCod: Integer; SQL_PeriodoPQ: String;
              ShowSQL: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
  'CliDest ClientMO ',
  'FROM pqx ',
  'WHERE ( ',
  '  Tipo=110   ',
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM emit ',
  '    WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'OR ',
  '( ',
  '  Tipo=190   ',
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM PQO ',
  '    WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'AND Peso <> 0',
  SQL_PeriodoPQ,
  '']);
  //
  if ShowSQL then
    Geral.MB_Info(QrOri.SQL.Text);
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenVmiGArCab_Fast(TipoRegSPEDProd:
  TTipoRegSPEDProd; SQL_Periodo: String);
begin
  // incluir Tipo_Item dp gg2 ou pgt!
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqVmiGArCab, DModG.MyCompressDB, [
  'SELECT gg1.UnidMed, med.Grandeza med_grandeza, ',
  'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(True, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'DATE(vmi.DataHora) Data, vmi.Codigo, vmi.MovimCod, ',
  'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg, ',
  'vmi.QtdGerArM2, ',
  'vmi.MovimTwn, vmi.Controle, DtCorrApo, ',
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
  'IF(gg1.Nivel2 <> 0 AND gg2.Tipo_Item>-1, gg2.Tipo_Item, ',
  '  pgt.Tipo_Item) Tipo_Item ',
  'FROM ' + CO_SEL_TAB_VMI + '        vmi ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCurtiXX)), // 14
  SQL_Periodo,
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
(*
Desabilitado em 23/12/2017 por causa de gera��o direto do Verde/Salgado!
Ver gera��o a partir do curtido!
*)
  'AND vmi.MovimCod IN ( ',
  '  SELECT MovimCod ',
  '  FROM ' + CO_SEL_TAB_VMI + ' ',
  '  WHERE MovimNiv=13 ',
  '  AND FornecMO' + FiltroRegGAR(TipoRegSPEDProd) + Geral.FF0(FEmpresa),
  ') ',
(**)
  'ORDER BY vmi.DtCorrApo, vmi.MovimCod, vmi.GraGruX ',
  '']);
  //Geral.MB_SQL(Self, DqVmiGArCab);
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenVmiGArCab(TipoRegSPEDProd:
  TTipoRegSPEDProd; SQL_Periodo: String);
begin
  // incluir Tipo_Item dp gg2 ou pgt!
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmiGArCab, Dmod.MyDB, [
  'SELECT gg1.UnidMed, med.Grandeza med_grandeza, ',
  'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(True, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'DATE(vmi.DataHora) Data, vmi.Codigo, vmi.MovimCod, ',
  'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg, ',
  'vmi.QtdGerArM2, ',
  'vmi.MovimTwn, vmi.Controle, DtCorrApo, ',
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
  'IF(gg1.Nivel2 <> 0 AND gg2.Tipo_Item>-1, gg2.Tipo_Item, ',
  '  pgt.Tipo_Item) Tipo_Item ',
  'FROM ' + CO_SEL_TAB_VMI + '        vmi ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCurtiXX)), // 14
  SQL_Periodo,
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
(*
Desabilitado em 23/12/2017 por causa de gera��o direto do Verde/Salgado!
Ver gera��o a partir do curtido!
*)
  'AND vmi.MovimCod IN ( ',
  '  SELECT MovimCod ',
  '  FROM ' + CO_SEL_TAB_VMI + ' ',
  '  WHERE MovimNiv=13 ',
  '  AND FornecMO' + FiltroRegGAR(TipoRegSPEDProd) + Geral.FF0(FEmpresa),
  ') ',
(**)
  'ORDER BY vmi.DtCorrApo, vmi.MovimCod, vmi.GraGruX ',
  '']);
  //Geral.MB_SQL(Self, QrVmiGArCab);
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenVmiGArIts_Fast(MovimCod, MovimTwn:
  Integer; SQL_Periodo: String; TipoRegSPEDProd: TTipoRegSPEDProd);
begin
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqVmiGArIts, DModG.MyCompressDB, [
  'SELECT vmi.SrcMovID, vmi.SrcNivel2, vmi.SrcGGX, vmi.DstGGX, ',
  'vmi.JmpGGX, gg1.UnidMed, med.Grandeza med_grandeza, ',
  'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'DATE(vmi.DataHora) Data, vmi.DtCorrApo, vmi.Codigo, vmi.MovimCod,  ',
  'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg,  ',
  'vmi.MovimTwn, vmi.Controle  ',
  'FROM ' + CO_SEL_TAB_VMI + '        vmi  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiXX)), // 15
  'AND vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimTwn=' + Geral.FF0(MovimTwn),
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
  'AND (vmi.FornecMO=0 ',
  ' OR vmi.FornecMO' + FiltroRegGAR(TipoRegSPEDProd) + Geral.FF0(FEmpresa),
  ')',
  FiltroPeriodoGAr(TipoRegSPEDProd, SQL_Periodo),
  'ORDER BY Data, vmi.MovimCod, vmi.GraGruX ',
  '']);
  //if QrVmiGArIts.RecordCount > 1 then
    //Geral.MB_SQL(Self, QrVmiGArIts);
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenVmiGArIts(MovimCod, MovimTwn:
  Integer; SQL_Periodo: String; TipoRegSPEDProd: TTipoRegSPEDProd);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmiGArIts, Dmod.MyDB, [
  'SELECT vmi.SrcMovID, vmi.SrcNivel2, vmi.SrcGGX, vmi.DstGGX, ',
  'vmi.JmpGGX, gg1.UnidMed, med.Grandeza med_grandeza, ',
  'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'DATE(vmi.DataHora) Data, vmi.DtCorrApo, vmi.Codigo, vmi.MovimCod,  ',
  'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg,  ',
  'vmi.MovimTwn, vmi.Controle  ',
  'FROM ' + CO_SEL_TAB_VMI + '        vmi  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiXX)), // 15
  'AND vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimTwn=' + Geral.FF0(MovimTwn),
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
  'AND (vmi.FornecMO=0 ',
  ' OR vmi.FornecMO' + FiltroRegGAR(TipoRegSPEDProd) + Geral.FF0(FEmpresa),
  ')',
  FiltroPeriodoGAr(TipoRegSPEDProd, SQL_Periodo),
  'ORDER BY Data, vmi.MovimCod, vmi.GraGruX ',
  '']);
  //if QrVmiGArIts.RecordCount > 1 then
    //Geral.MB_SQL(Self, QrVmiGArIts);
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenVmiTwn(MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv; MovimCod, MovimTwn: Integer): Boolean;
var
  MovimIDBxa: TEstqMovimID;
  MovimNivBxa: TEstqMovimNiv;
begin
  Result := False;
  if MovimTwn <> 0 then
  begin
    MovimIDBxa := VS_PF.ObtemMovimIDBxaDeMovimID(MovimID);
    MovimNivBxa := VS_PF.ObtemMovimNivBxaDeMovimID(MovimID);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrVmiTwn, Dmod.MyDB, [
    'SELECT *  ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimID=' + Geral.FF0(Integer(MovimIDBxa)),
    'AND MovimNiv<>' + Geral.FF0(Integer(MovimNiv)),
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNivBxa)),
    'AND MovimTwn=' + Geral.FF0(MovimTwn),
    ' ']);
    Result := QrVmiTwn.RecordCount > 0;
    if not Result then
      Geral.MB_Erro('IME-I n�o localizado para MovimTwn ' + Geral.FF0(MovimTwn) +
      ' e MovimNiv ' + Geral.FF0(Integer(MovimNivBxa)) + ' !!!');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.VerificaSeTemGraGruX(
  IDLinPas, GraGruX: Integer; ProcName: String; Qry: TmySQLQuery);
var
  SQL: String;
begin
  if GraGruX = 0 then
  begin
    if Qry <> nil then
      SQL := Qry.SQL.Text
    else
      SQL := '';
    Geral.MB_Erro('Reduzido indefinido em ' + ProcName + sLineBreak +
    'IDLinPas: ' + Geral.FF0(IDLinPas) + sLineBreak + SQL);
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.VerificaSeTemGraGruX(
  IDLinPas, GraGruX: Integer; ProcName: String; Qry: TmySQLDirectQuery);
var
  SQL: String;
begin
  if GraGruX = 0 then
  begin
    if Qry <> nil then
      SQL := Qry.SQL.Text
    else
      SQL := '';
    Geral.MB_Erro('Reduzido indefinido em ' + ProcName + sLineBreak +
    'IDLinPas: ' + Geral.FF0(IDLinPas) + sLineBreak + SQL);
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.VerificaTipoEstqQtde(TipoEstq:
  Integer; AreaM2, PesoKg, Pecas: Double; GraGruX, MovimCod, Controle: Integer;
  ProcName: String);
begin
  if (AreaM2 <> 0) or (PesoKg <> 0) or (Pecas <> 0) then
  begin
    Geral.MB_Erro(
    'Quantidade zerada para  "TipoEstq" = ' + Geral.FF0(TipoEstq) + ' -> ' +
    sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
    ProcName + sLineBreak +
    'Area m�: ' + Geral.FFT(AreaM2, 2, siNegativo) + sLineBreak +
    'Peso kg: ' + Geral.FFT(PesoKg, 3, siNegativo) + sLineBreak +
    'Pe�as: ' + Geral.FFT(Pecas, 2, siNegativo) + sLineBreak +
    'Reduzido = ' + Geral.FF0(GraGruX) + sLineBreak +
    'IME-C = ' + Geral.FF0(MovimCod) + sLineBreak +
    'IME-I = ' + Geral.FF0(COntrole) + sLineBreak);
  end;
end;

(*
object QrPQMGerOrigemCodi: TIntegerField
  FieldName = 'OrigemCodi'
end


object QrPQMGerTipo: TIntegerField
  FieldName = 'Tipo'
end
object QrPQMGerCliOrig: TIntegerField
  FieldName = 'CliOrig'
end
object QrPQMGerCliDest: TIntegerField
  FieldName = 'CliDest'
end
object QrPQMGerValor: TFloatField
  FieldName = 'Valor'
  DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
end
object QrPQMGerRetorno: TSmallintField
  FieldName = 'Retorno'
end
object QrPQMGerStqMovIts: TIntegerField
  FieldName = 'StqMovIts'
end
object QrPQMGerRetQtd: TFloatField
  FieldName = 'RetQtd'
end
object QrPQMGerHowLoad: TSmallintField
  FieldName = 'HowLoad'
end
object QrPQMGerStqCenLoc: TIntegerField
  FieldName = 'StqCenLoc'
end
object QrPQMGerSdoPeso: TFloatField
  FieldName = 'SdoPeso'
end
object QrPQMGerSdoValr: TFloatField
  FieldName = 'SdoValr'
end
object QrPQMGerAcePeso: TFloatField
  FieldName = 'AcePeso'
end
object QrPQMGerAceValr: TFloatField
  FieldName = 'AceValr'
end
object QrPQMGerDtCorrApo: TDateField
  FieldName = 'DtCorrApo'
end
object QrPQMGerEmpresa: TIntegerField
  FieldName = 'Empresa'
end
object QrPQMGerAlterWeb: TSmallintField
  FieldName = 'AlterWeb'
end
object QrPQMGerAtivo: TSmallintField
  FieldName = 'Ativo'
end
object QrPQMGerNOMEPQ: TWideStringField
  FieldName = 'NOMEPQ'
  Size = 50
end
object QrPQMGerGrupoQuimico: TIntegerField
  FieldName = 'GrupoQuimico'
end
object QrPQMGerNOMEGRUPO: TWideStringField
  FieldName = 'NOMEGRUPO'
  Size = 100
end
object QrPQMGerSigla: TWideStringField
  FieldName = 'Sigla'
  Size = 6
end
*)
//Colocar opcao bderm de isolada Direta no sped? conjunta no
//classificar couro a couro

// Tags pesquisa recurtimento
//DefinePsqIMEI(
//ReopenOriPQx
//Insere_OrigeProcPQ ????

//InsereItemProducaoConjuntaGemeos(
// InsumoSemProducao

{
MovimID	MovimNiv
1	0
2	0
6	13
6	14
6	15
7	1
7	2
7	6
8	1
8	2
8	6
9	0
11	7
11	8
11	9
11	10
12	0
13	0
15	11
15	12
16	0
17	0
18	0
19	20
19	21
19	23
20	22
21	0
22	0
23	0
24	1
24	2
25	27
25	28
26	29
26	30
27	34
27	35
29	31
29	32
30	41
30	42
31	46
31	47
32	49
32	50
32	51
32	52
33	0
33	54
33	55
33	56
33	57
34	0

36	0



SELECT vmi.MovimCod, vmi.Codigo, vmi.Controle,
vmi.GraGruX, vmi.PesoKg, vmi.DataHora, e215.KndItm
FROM ' + CO_SEL_TAB_VMI + ' vmi
LEFT JOIN efdicmsipik215 e215 ON e215.KndItm=vmi.Controle
WHERE (vmi.GraGruX=787
AND vmi.DataHora BETWEEN
"2018-10-01" AND "2018-10-31 23:59:59")
ORDER BY Controle

Caleiro 4341 IMEC 33395 --> 75679



////////////////////////////////



DROP TABLE IF EXISTS _SPED_KNDITM_1_;

CREATE TABLE _SPED_KNDITM_1_

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik210

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik215

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik230

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik235

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik250

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik255

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik260

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik265

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik270

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik275

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik291

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik292

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik301

UNION

SELECT DISTINCT KndItm
FROM ' + TMeuDB + '.efdicmsipik302

;

DROP TABLE IF EXISTS _SPED_KNDITM_2_;

CREATE TABLE _SPED_KNDITM_2_
SELECT *
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE DataHora BETWEEN
"2018-10-01" AND "2018-10-31 23:59:59"
;

SELECT itm.*, vmi.*
FROM _SPED_KNDITM_2_ vmi
LEFT JOIN _SPED_KNDITM_1_ itm ON itm.KndItm=vmi.Controle
WHERE itm.KndItm IS NULL


////////////////////////////////////////////////////////////////////////////////

DROP TABLE IF EXISTS _MID_MNiv_1_;

CREATE TABLE _MID_MNiv_1_

SELECT MovimID MID, MovimNiv MNiv,
"MovimID" FldMID,
SUM(IF(Pecas>0 OR PesoKg>0, 1, 0)) Positivo,
SUM(IF(Pecas<0 OR PesoKg<0, 1, 0)) Negativo
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
GROUP BY MID, MNiv

UNION

SELECT SrcMovID MID, 0 MNiv,
"SrcMovID" FldMID,
SUM(IF(Pecas>0 OR PesoKg>0, 1, 0)) Positivo,
SUM(IF(Pecas<0 OR PesoKg<0, 1, 0)) Negativo
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE SrcMovID <> 0
GROUP BY MID, MNiv

UNION

SELECT DstMovID MID, 0 MNiv,
"DstMovID" FldMID,
SUM(IF(Pecas>0 OR PesoKg>0, 1, 0)) Positivo,
SUM(IF(Pecas<0 OR PesoKg<0, 1, 0)) Negativo
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE DstMovID <> 0
GROUP BY MID, MNiv

UNION

SELECT JmpMovID MID, 0 MNiv,
"JmpMovID" FldMID,
SUM(IF(Pecas>0 OR PesoKg>0, 1, 0)) Positivo,
SUM(IF(Pecas<0 OR PesoKg<0, 1, 0)) Negativo
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE JmpMovID <> 0
GROUP BY MID, MNiv

UNION

SELECT RmsMovID MID, 0 MNiv,
"RmsMovID" FldMID,
SUM(IF(Pecas>0 OR PesoKg>0, 1, 0)) Positivo,
SUM(IF(Pecas<0 OR PesoKg<0, 1, 0)) Negativo
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE RmsMovID <> 0
GROUP BY MID, MNiv

UNION

SELECT GSPSrcMovID MID, 0 MNiv,
"GSPSrcMovID" FldMID,
SUM(IF(Pecas>0 OR PesoKg>0, 1, 0)) Positivo,
SUM(IF(Pecas<0 OR PesoKg<0, 1, 0)) Negativo
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE GSPSrcMovID <> 0
GROUP BY MID, MNiv

UNION

SELECT GSPJmpMovID MID, 0 MNiv,
"GSPJmpMovID" FldMID,
SUM(IF(Pecas>0 OR PesoKg>0, 1, 0)) Positivo,
SUM(IF(Pecas<0 OR PesoKg<0, 1, 0)) Negativo
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '
WHERE GSPJmpMovID <> 0
GROUP BY MID, MNiv
;

DROP TABLE IF EXISTS _MID_MNiv_2_;

CREATE TABLE _MID_MNiv_2_

SELECT FldMID, MID, MNiv, SUM(Positivo) Positivo,
SUM(Negativo) Negativo
FROM _MID_MNiv_1_
GROUP BY FldMID, MID, MNiv

ORDER BY  FldMID, MID, MNiv
;

SELECT mi2.*
FROM _MID_MNiv_2_ mi2
}


////////////////////////////////////////////////////////////////////////////////

{

SELECT cab.Codigo, auu.*
FROM _imeis_sped_direct_all auu
LEFT JOIN ' + TMeuDB + '.vsmovcab cab ON
  cab.Codigo=auu.MovimCod
WHERE cab.Codigo IS NULL
}





end.

