unit SpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  mySQLDbTables,
  //
  StdCtrls, ExtCtrls, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums,
  UnMsgInt,(* DbTables,*) DmkCoding, dmkEdit, dmkRadioGroup, dmkMemo, AdvToolBar,
  dmkCheckGroup, UnMyObjects, MyDBCheck, UnDmkProcFunc, UnProjGroup_Consts,
  AppListas, dmkLabel, dmkImage, DBCtrls, dmkDBLookupComboBox, dmkEditCB,
  dmkEditDateTimePicker, dmkDBGridZTO, SPED_Listas, TypInfo, UnEfdIcmsIpi_PF;

type
  TOpenCliForSite = (ocfsIndef=0, ocfsControle=1, ocfsMovIdNivCod=2, ocfsSrcNivel2=3);
  TFormaInsercaoItemProducao = (fipIndef=0, fipIsolada=1, fipConjunta=2);
  TInsercaoItemProducaoConjunta = (iipcIndef=0, iipcSimples=1, iipcGemeos=2,
                                iipcIndireta=3, iipcUniAnt=4, iipcUniGer=5);
  TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b = class(TForm)
    QrDatas: TmySQLQuery;
    QrDatasMinDH: TDateTimeField;
    QrDatasMaxDH: TDateTimeField;
    QrOri: TmySQLQuery;
    QrMae: TmySQLQuery;
    QrMC: TmySQLQuery;
    QrMCMovimCod: TIntegerField;
    QrProdRib: TmySQLQuery;
    QrProdRibData: TDateField;
    QrProdRibGGX_Dst: TIntegerField;
    QrProdRibGGX_Src: TIntegerField;
    QrProdRibPecas: TFloatField;
    QrProdRibAreaM2: TFloatField;
    QrProdRibPesoKg: TFloatField;
    QrProdRibMovimID: TIntegerField;
    QrProdRibCodigo: TIntegerField;
    QrProdRibMovimCod: TIntegerField;
    QrProdRibControle: TIntegerField;
    QrProdRibEmpresa: TIntegerField;
    QrProdRibQtdAntPeca: TFloatField;
    QrProdRibQtdAntArM2: TFloatField;
    QrProdRibQtdAntPeso: TFloatField;
    QrProdRibsTipoEstq: TFloatField;
    QrProdRibdTipoEstq: TFloatField;
    QrProdRibMovimNiv: TIntegerField;
    QrProdRibDtCorrApo: TDateTimeField;
    QrProdRibClientMO: TIntegerField;
    QrProdRibFornecMO: TIntegerField;
    QrProdRibEntiSitio: TIntegerField;
    QrObtCliForSite: TmySQLQuery;
    QrIts: TmySQLQuery;
    QrCalToCur: TmySQLQuery;
    QrConsParc: TmySQLQuery;
    QrConsParcPecas: TFloatField;
    QrConsParcAreaM2: TFloatField;
    QrConsParcPesoKg: TFloatField;
    QrConsParcAno: TFloatField;
    QrConsParcMes: TFloatField;
    QrProdParc: TmySQLQuery;
    QrProdParcPecas: TFloatField;
    QrProdParcAreaM2: TFloatField;
    QrProdParcPesoKg: TFloatField;
    QrProdParcClientMO: TIntegerField;
    QrProdParcFornecMO: TIntegerField;
    QrProdParcControle: TIntegerField;
    QrProdParcAno: TFloatField;
    QrProdParcMes: TFloatField;
    QrPeriodos: TmySQLQuery;
    QrPeriodosAno: TFloatField;
    QrPeriodosMes: TFloatField;
    QrPeriodosPeriodo: TIntegerField;
    QrVmiGArCab: TmySQLQuery;
    QrVmiGArCabData: TDateField;
    QrVmiGArCabCodigo: TIntegerField;
    QrVmiGArCabMovimCod: TIntegerField;
    QrVmiGArCabGraGruX: TIntegerField;
    QrVmiGArCabPecas: TFloatField;
    QrVmiGArCabAreaM2: TFloatField;
    QrVmiGArCabPesoKg: TFloatField;
    QrVmiGArCabMovimTwn: TIntegerField;
    QrVmiGArCabUnidMed: TIntegerField;
    QrVmiGArCabmed_grandeza: TIntegerField;
    QrVmiGArCabxco_grandeza: TIntegerField;
    QrVmiGArCabGraGruY: TIntegerField;
    QrVmiGArCabCouNiv2: TIntegerField;
    QrVmiGArCabControle: TIntegerField;
    QrVmiGArCabTipoEstq: TFloatField;
    QrVmiGArCabDtCorrApo: TDateTimeField;
    QrVmiGArCabClientMO: TIntegerField;
    QrVmiGArCabFornecMO: TIntegerField;
    QrVmiGArCabEntiSitio: TIntegerField;
    QrSumGAR: TmySQLQuery;
    QrSumGARPecas: TFloatField;
    QrSumGARAreaM2: TFloatField;
    QrSumGARPesoKg: TFloatField;
    QrVmiGArIts: TmySQLQuery;
    QrVmiGArItsData: TDateField;
    QrVmiGArItsCodigo: TIntegerField;
    QrVmiGArItsMovimCod: TIntegerField;
    QrVmiGArItsGraGruX: TIntegerField;
    QrVmiGArItsPecas: TFloatField;
    QrVmiGArItsAreaM2: TFloatField;
    QrVmiGArItsPesoKg: TFloatField;
    QrVmiGArItsMovimTwn: TIntegerField;
    QrVmiGArItsUnidMed: TIntegerField;
    QrVmiGArItsmed_grandeza: TIntegerField;
    QrVmiGArItsxco_grandeza: TIntegerField;
    QrVmiGArItsGraGruY: TIntegerField;
    QrVmiGArItsCouNiv2: TIntegerField;
    QrVmiGArItsTipoEstq: TLargeintField;
    QrVmiGArItsControle: TIntegerField;
    QrVmiGArItsDstGGX: TIntegerField;
    QrVmiGArItsSrcGGX: TIntegerField;
    QrVmiGArItsSrcNivel2: TIntegerField;
    QrVmiGArItsSrcMovID: TIntegerField;
    QrVmiGArItsDtCorrApo: TDateTimeField;
    QrIMECs: TmySQLQuery;
    QrIMECsCodigo: TIntegerField;
    QrIMECsMovimCod: TIntegerField;
    QrIMECsMovimID: TIntegerField;
    QrIMECsAnoCA: TIntegerField;
    QrIMECsMesCA: TIntegerField;
    QrIMECsTipoTab: TWideStringField;
    QrIMECsAtivo: TSmallintField;
    QrVSRibAtu: TmySQLQuery;
    QrVSRibAtuCodigo: TLargeintField;
    QrVSRibAtuControle: TLargeintField;
    QrVSRibAtuMovimCod: TLargeintField;
    QrVSRibAtuMovimNiv: TLargeintField;
    QrVSRibAtuMovimTwn: TLargeintField;
    QrVSRibAtuEmpresa: TLargeintField;
    QrVSRibAtuTerceiro: TLargeintField;
    QrVSRibAtuCliVenda: TLargeintField;
    QrVSRibAtuMovimID: TLargeintField;
    QrVSRibAtuDataHora: TDateTimeField;
    QrVSRibAtuPallet: TLargeintField;
    QrVSRibAtuGraGruX: TLargeintField;
    QrVSRibAtuPecas: TFloatField;
    QrVSRibAtuPesoKg: TFloatField;
    QrVSRibAtuAreaM2: TFloatField;
    QrVSRibAtuAreaP2: TFloatField;
    QrVSRibAtuValorT: TFloatField;
    QrVSRibAtuSrcMovID: TLargeintField;
    QrVSRibAtuSrcNivel1: TLargeintField;
    QrVSRibAtuSrcNivel2: TLargeintField;
    QrVSRibAtuSrcGGX: TLargeintField;
    QrVSRibAtuSdoVrtPeca: TFloatField;
    QrVSRibAtuSdoVrtPeso: TFloatField;
    QrVSRibAtuSdoVrtArM2: TFloatField;
    QrVSRibAtuObserv: TWideStringField;
    QrVSRibAtuSerieFch: TLargeintField;
    QrVSRibAtuFicha: TLargeintField;
    QrVSRibAtuMisturou: TLargeintField;
    QrVSRibAtuFornecMO: TLargeintField;
    QrVSRibAtuCustoMOKg: TFloatField;
    QrVSRibAtuCustoMOM2: TFloatField;
    QrVSRibAtuCustoMOTot: TFloatField;
    QrVSRibAtuValorMP: TFloatField;
    QrVSRibAtuDstMovID: TLargeintField;
    QrVSRibAtuDstNivel1: TLargeintField;
    QrVSRibAtuDstNivel2: TLargeintField;
    QrVSRibAtuDstGGX: TLargeintField;
    QrVSRibAtuQtdGerPeca: TFloatField;
    QrVSRibAtuQtdGerPeso: TFloatField;
    QrVSRibAtuQtdGerArM2: TFloatField;
    QrVSRibAtuQtdGerArP2: TFloatField;
    QrVSRibAtuQtdAntPeca: TFloatField;
    QrVSRibAtuQtdAntPeso: TFloatField;
    QrVSRibAtuQtdAntArM2: TFloatField;
    QrVSRibAtuQtdAntArP2: TFloatField;
    QrVSRibAtuNotaMPAG: TFloatField;
    QrVSRibAtuNO_PALLET: TWideStringField;
    QrVSRibAtuNO_PRD_TAM_COR: TWideStringField;
    QrVSRibAtuNO_TTW: TWideStringField;
    QrVSRibAtuID_TTW: TLargeintField;
    QrVSRibAtuNO_FORNECE: TWideStringField;
    QrVSRibAtuReqMovEstq: TLargeintField;
    QrVSRibAtuCUSTO_M2: TFloatField;
    QrVSRibAtuCUSTO_P2: TFloatField;
    QrVSRibAtuNO_LOC_CEN: TWideStringField;
    QrVSRibAtuMarca: TWideStringField;
    QrVSRibAtuPedItsLib: TLargeintField;
    QrVSRibAtuStqCenLoc: TLargeintField;
    QrVSRibAtuNO_FICHA: TWideStringField;
    QrVSRibAtuNO_FORNEC_MO: TWideStringField;
    QrVSRibAtuClientMO: TLargeintField;
    QrVSRibAtuCUSTO_KG: TFloatField;
    QrVSRibAtuCustoPQ: TFloatField;
    QrVSRibDst: TmySQLQuery;
    QrVSRibDstCodigo: TLargeintField;
    QrVSRibDstControle: TLargeintField;
    QrVSRibDstMovimCod: TLargeintField;
    QrVSRibDstMovimNiv: TLargeintField;
    QrVSRibDstMovimTwn: TLargeintField;
    QrVSRibDstEmpresa: TLargeintField;
    QrVSRibDstTerceiro: TLargeintField;
    QrVSRibDstCliVenda: TLargeintField;
    QrVSRibDstMovimID: TLargeintField;
    QrVSRibDstDataHora: TDateTimeField;
    QrVSRibDstPallet: TLargeintField;
    QrVSRibDstGraGruX: TLargeintField;
    QrVSRibDstPecas: TFloatField;
    QrVSRibDstPesoKg: TFloatField;
    QrVSRibDstAreaM2: TFloatField;
    QrVSRibDstAreaP2: TFloatField;
    QrVSRibDstValorT: TFloatField;
    QrVSRibDstSrcMovID: TLargeintField;
    QrVSRibDstSrcNivel1: TLargeintField;
    QrVSRibDstSrcNivel2: TLargeintField;
    QrVSRibDstSrcGGX: TLargeintField;
    QrVSRibDstSdoVrtPeca: TFloatField;
    QrVSRibDstSdoVrtPeso: TFloatField;
    QrVSRibDstSdoVrtArM2: TFloatField;
    QrVSRibDstObserv: TWideStringField;
    QrVSRibDstSerieFch: TLargeintField;
    QrVSRibDstFicha: TLargeintField;
    QrVSRibDstMisturou: TLargeintField;
    QrVSRibDstFornecMO: TLargeintField;
    QrVSRibDstCustoMOKg: TFloatField;
    QrVSRibDstCustoMOM2: TFloatField;
    QrVSRibDstCustoMOTot: TFloatField;
    QrVSRibDstValorMP: TFloatField;
    QrVSRibDstDstMovID: TLargeintField;
    QrVSRibDstDstNivel1: TLargeintField;
    QrVSRibDstDstNivel2: TLargeintField;
    QrVSRibDstDstGGX: TLargeintField;
    QrVSRibDstQtdGerPeca: TFloatField;
    QrVSRibDstQtdGerPeso: TFloatField;
    QrVSRibDstQtdGerArM2: TFloatField;
    QrVSRibDstQtdGerArP2: TFloatField;
    QrVSRibDstQtdAntPeca: TFloatField;
    QrVSRibDstQtdAntPeso: TFloatField;
    QrVSRibDstQtdAntArM2: TFloatField;
    QrVSRibDstQtdAntArP2: TFloatField;
    QrVSRibDstNotaMPAG: TFloatField;
    QrVSRibDstNO_PALLET: TWideStringField;
    QrVSRibDstNO_PRD_TAM_COR: TWideStringField;
    QrVSRibDstNO_TTW: TWideStringField;
    QrVSRibDstID_TTW: TLargeintField;
    QrVSRibDstNO_FORNECE: TWideStringField;
    QrVSRibDstNO_SerieFch: TWideStringField;
    QrVSRibDstReqMovEstq: TLargeintField;
    QrVSRibDstPedItsFin: TLargeintField;
    QrVSRibDstMarca: TWideStringField;
    QrVSRibDstStqCenLoc: TLargeintField;
    QrVSRibDstNO_MovimNiv: TWideStringField;
    QrVSRibDstNO_MovimID: TWideStringField;
    QrProc: TmySQLQuery;
    QrProcDtHrAberto: TDateTimeField;
    QrProcDtHrFimOpe: TDateTimeField;
    QrVSRibDstDtCorrApo: TDateTimeField;
    QrVmiTwn: TmySQLQuery;
    QrIMECsJmpMovID: TIntegerField;
    QrIMECsJmpNivel1: TIntegerField;
    QrIMECsJmpNivel2: TIntegerField;
    QrJmpA: TmySQLQuery;
    QrJmpASrcNivel2: TIntegerField;
    QrJmpAMovimID: TIntegerField;
    QrJmpACodigo: TIntegerField;
    QrJmpAMovimCod: TIntegerField;
    QrJmpAControle: TIntegerField;
    QrJmpB: TmySQLQuery;
    QrJmpBMovimID: TIntegerField;
    QrJmpBCodigo: TIntegerField;
    QrJmpBMovimCod: TIntegerField;
    QrJmpBControle: TIntegerField;
    QrVSRibInd: TmySQLQuery;
    QrVSRibIndCodigo: TLargeintField;
    QrVSRibIndControle: TLargeintField;
    QrVSRibIndMovimCod: TLargeintField;
    QrVSRibIndMovimNiv: TLargeintField;
    QrVSRibIndMovimTwn: TLargeintField;
    QrVSRibIndEmpresa: TLargeintField;
    QrVSRibIndTerceiro: TLargeintField;
    QrVSRibIndCliVenda: TLargeintField;
    QrVSRibIndMovimID: TLargeintField;
    QrVSRibIndDataHora: TDateTimeField;
    QrVSRibIndPallet: TLargeintField;
    QrVSRibIndGraGruX: TLargeintField;
    QrVSRibIndPecas: TFloatField;
    QrVSRibIndPesoKg: TFloatField;
    QrVSRibIndAreaM2: TFloatField;
    QrVSRibIndAreaP2: TFloatField;
    QrVSRibIndValorT: TFloatField;
    QrVSRibIndSrcMovID: TLargeintField;
    QrVSRibIndSrcNivel1: TLargeintField;
    QrVSRibIndSrcNivel2: TLargeintField;
    QrVSRibIndSrcGGX: TLargeintField;
    QrVSRibIndSdoVrtPeca: TFloatField;
    QrVSRibIndSdoVrtPeso: TFloatField;
    QrVSRibIndSdoVrtArM2: TFloatField;
    QrVSRibIndObserv: TWideStringField;
    QrVSRibIndSerieFch: TLargeintField;
    QrVSRibIndFicha: TLargeintField;
    QrVSRibIndMisturou: TLargeintField;
    QrVSRibIndFornecMO: TLargeintField;
    QrVSRibIndCustoMOKg: TFloatField;
    QrVSRibIndCustoMOM2: TFloatField;
    QrVSRibIndCustoMOTot: TFloatField;
    QrVSRibIndValorMP: TFloatField;
    QrVSRibIndDstMovID: TLargeintField;
    QrVSRibIndDstNivel1: TLargeintField;
    QrVSRibIndDstNivel2: TLargeintField;
    QrVSRibIndDstGGX: TLargeintField;
    QrVSRibIndQtdGerPeca: TFloatField;
    QrVSRibIndQtdGerPeso: TFloatField;
    QrVSRibIndQtdGerArM2: TFloatField;
    QrVSRibIndQtdGerArP2: TFloatField;
    QrVSRibIndQtdAntPeca: TFloatField;
    QrVSRibIndQtdAntPeso: TFloatField;
    QrVSRibIndQtdAntArM2: TFloatField;
    QrVSRibIndQtdAntArP2: TFloatField;
    QrVSRibIndNotaMPAG: TFloatField;
    QrVSRibIndNO_PALLET: TWideStringField;
    QrVSRibIndNO_PRD_TAM_COR: TWideStringField;
    QrVSRibIndNO_TTW: TWideStringField;
    QrVSRibIndID_TTW: TLargeintField;
    QrVSRibIndNO_FORNECE: TWideStringField;
    QrVSRibIndNO_SerieFch: TWideStringField;
    QrVSRibIndReqMovEstq: TLargeintField;
    QrVSRibIndPedItsFin: TLargeintField;
    QrVSRibIndMarca: TWideStringField;
    QrVSRibIndStqCenLoc: TLargeintField;
    QrVSRibIndNO_MovimNiv: TWideStringField;
    QrVSRibIndNO_MovimID: TWideStringField;
    QrVSRibIndDtCorrApo: TDateTimeField;
    QrVSRibIndJmpMovID: TLargeintField;
    QrVSRibIndJmpNivel1: TLargeintField;
    QrVSRibIndJmpNivel2: TLargeintField;
    QrVSRibIndJmpGGX: TLargeintField;
    QrFather: TmySQLQuery;
    QrFatherMovCodPai: TIntegerField;
    QrIMECsMovCodPai: TIntegerField;
    QrVSRibOriIMEI: TmySQLQuery;
    QrVSRibOriIMEICodigo: TLargeintField;
    QrVSRibOriIMEIControle: TLargeintField;
    QrVSRibOriIMEIMovimCod: TLargeintField;
    QrVSRibOriIMEIMovimNiv: TLargeintField;
    QrVSRibOriIMEIMovimTwn: TLargeintField;
    QrVSRibOriIMEIEmpresa: TLargeintField;
    QrVSRibOriIMEITerceiro: TLargeintField;
    QrVSRibOriIMEICliVenda: TLargeintField;
    QrVSRibOriIMEIMovimID: TLargeintField;
    QrVSRibOriIMEIDataHora: TDateTimeField;
    QrVSRibOriIMEIPallet: TLargeintField;
    QrVSRibOriIMEIGraGruX: TLargeintField;
    QrVSRibOriIMEIPecas: TFloatField;
    QrVSRibOriIMEIPesoKg: TFloatField;
    QrVSRibOriIMEIAreaM2: TFloatField;
    QrVSRibOriIMEIAreaP2: TFloatField;
    QrVSRibOriIMEIValorT: TFloatField;
    QrVSRibOriIMEISrcMovID: TLargeintField;
    QrVSRibOriIMEISrcNivel1: TLargeintField;
    QrVSRibOriIMEISrcNivel2: TLargeintField;
    QrVSRibOriIMEISrcGGX: TLargeintField;
    QrVSRibOriIMEISdoVrtPeca: TFloatField;
    QrVSRibOriIMEISdoVrtPeso: TFloatField;
    QrVSRibOriIMEISdoVrtArM2: TFloatField;
    QrVSRibOriIMEIObserv: TWideStringField;
    QrVSRibOriIMEISerieFch: TLargeintField;
    QrVSRibOriIMEIFicha: TLargeintField;
    QrVSRibOriIMEIMisturou: TLargeintField;
    QrVSRibOriIMEIFornecMO: TLargeintField;
    QrVSRibOriIMEICustoMOKg: TFloatField;
    QrVSRibOriIMEICustoMOM2: TFloatField;
    QrVSRibOriIMEICustoMOTot: TFloatField;
    QrVSRibOriIMEIValorMP: TFloatField;
    QrVSRibOriIMEIDstMovID: TLargeintField;
    QrVSRibOriIMEIDstNivel1: TLargeintField;
    QrVSRibOriIMEIDstNivel2: TLargeintField;
    QrVSRibOriIMEIDstGGX: TLargeintField;
    QrVSRibOriIMEIQtdGerPeca: TFloatField;
    QrVSRibOriIMEIQtdGerPeso: TFloatField;
    QrVSRibOriIMEIQtdGerArM2: TFloatField;
    QrVSRibOriIMEIQtdGerArP2: TFloatField;
    QrVSRibOriIMEIQtdAntPeca: TFloatField;
    QrVSRibOriIMEIQtdAntPeso: TFloatField;
    QrVSRibOriIMEIQtdAntArM2: TFloatField;
    QrVSRibOriIMEIQtdAntArP2: TFloatField;
    QrVSRibOriIMEINotaMPAG: TFloatField;
    QrVSRibOriIMEINO_PALLET: TWideStringField;
    QrVSRibOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVSRibOriIMEINO_TTW: TWideStringField;
    QrVSRibOriIMEIID_TTW: TLargeintField;
    QrVSRibOriIMEINO_FORNECE: TWideStringField;
    QrVSRibOriIMEINO_SerieFch: TWideStringField;
    QrVSRibOriIMEIReqMovEstq: TLargeintField;
    QrVSRibOriIMEICustoPQ: TFloatField;
    QrVSRibOriIMEIVSMulFrnCab: TLargeintField;
    QrVSRibOriIMEIClientMO: TLargeintField;
    QrVSRibOriIMEIRmsMovID: TLargeintField;
    QrVSRibOriIMEIRmsNivel1: TLargeintField;
    QrVSRibOriIMEIRmsNivel2: TLargeintField;
    QrVSRibOriIMEIDtCorrApo: TDateTimeField;
    QrVSRibOriIMEIRmsGGX: TLargeintField;
    QrVSRibIndRmsGGX: TLargeintField;
    QrCab34: TmySQLQuery;
    QrCab34MovimCod: TIntegerField;
    QrCalToCurGraGruX: TIntegerField;
    QrVmiGArItsJmpGGX: TIntegerField;
    QrVmiGArCabQtdGerArM2: TFloatField;
    QrPQxMixOP: TmySQLQuery;
    QrPQxMixOPOrigemCodi: TIntegerField;
    QrPQxMixOPCliDest: TIntegerField;
    QrPQMCab: TmySQLQuery;
    QrPQMBxa: TmySQLQuery;
    QrPQMBxaDataX: TDateField;
    QrPQMBxaOrigemCtrl: TIntegerField;
    QrPQMBxaInsumo: TIntegerField;
    QrPQMBxaPeso: TFloatField;
    QrPQMGer: TmySQLQuery;
    QrPQxMixOPDtCorrApo: TDateField;
    QrPQMCabDataB: TDateField;
    QrPQMCabVSMovCod: TIntegerField;
    QrPQMGerDataX: TDateField;
    QrPQMGerOrigemCtrl: TIntegerField;
    QrPQMGerInsumo: TIntegerField;
    QrPQMGerPeso: TFloatField;
    Qry: TmySQLQuery;
    QrVSRibAtuDtCorrApo: TDateTimeField;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FSEII_IMEcs: String;
    //
    function  ImportaOpeProcB_Isolada(QrCab: TMySQLQuery; PsqMovimNiv:
              TEstqMovimNiv; SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ,
              SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ImportaOpeProcB_Conjunta(QrCab: TMySQLQuery; MovimID,
              PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; MovimNiv,
              PsqMovimNiv_VMI: TEstqMovimNiv; SQL_PeriodoComplexo,
              SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ReopenVmiTwn(MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv;
              MovimCod, MovimTwn: Integer): Boolean;
              // GAr
    function  FiltroRegGAR(TipoRegSPEDProd: TTipoRegSPEDProd): String;
    function  FiltroPeriodoGAR(TipoRegSPEDProd: TTipoRegSPEDProd; SQL_Periodo:
              String): String;
    function  GeraGAR(SQL_Periodo: String): Boolean;
    procedure ReopenObtCliForSite(Forma: TOpenCliForSite; Controle, MovimCod,
              MovimNiv: Integer);
    procedure ReopenVmiGArCab(TipoRegSPEDProd: TTipoRegSPEDProd; SQL_Periodo:
              String);
    procedure ReopenVmiGArIts(MovimCod, MovimTwn: Integer; SQL_Periodo: String;
              TipoRegSPEDProd: TTipoRegSPEDProd);
              // Fim GAr
    procedure VerificaTipoEstqQtde(TipoEstq: Integer; AreaM2, PesoKg, Pecas:
              Double; GraGruX, MovimCod, Controle: Integer; ProcName: String);
  public
    { Public declarations }
    FSQL_PeriodoPQ: String;
    FImporExpor, FEmpresa, FAnoMes, FPeriApu, (*FSPED_EFD_Producao*)
    FVersaoIsldCnjt: Integer;
    FDiaIni, FDiaFim, FDiaPos(*, FData*): TDateTime;
    FTipoPeriodoFiscal: TTipoPeriodoFiscal;
    PB2: TProgressBar;
    LaAviso1, LaAviso2, LaAviso3, LaAviso4: TLabel;
    MeAviso: TMemo;
    FParar: Boolean;
    //
    function  DefineGGXCalToCur(Campo: String; Controle: Integer): Integer;
    function  ImportaDiluicaoMistura(SQL_PeriodoComplexo, SQL_PeriodoVS,
              SQL_PeriodoPQ, SQL_DtHrFimOpe: String; OrigemOpeProc:
              TOrigemOpeProc): Boolean;
    function  ImportaGerArtIsolada(SQL_PeriodoComplexo, SQL_Periodo, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ImportaGerArtConjunta(SQL_PeriodoComplexo, SQL_Periodo, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ImportaOpeProcA(QrCab: TMySQLQuery; MovimID: TEstqMovimID;
              SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ImportaOpeProcB(QrCab: TMySQLQuery; MovimID, PsqMovimID_VMI,
              PsqMovimID_PQX: TEstqMovimID; MovimNiv, PsqMovimNiv_VMI:
              TEstqMovimNiv; SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ,
              SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  Insere_OrigeProcPQ(TipoRegSPEDProd: TTipoRegSPEDProd; MovimID:
              TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod: Integer;
              IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
              TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer): Boolean;
    function  Insere_OrigeRibDTA_210(const TipoRegSPEDProd:
              TTipoRegSPEDProd; const MovimID: TEstqMovimID; const MovimNiv:
              TEstqMovimNiv; const SQL_PeriodoVS: String; const OrigemOpeProc:
              TOrigemOpeProc): Boolean;
    function  Insere_OrigeRibPDA_210(const TipoRegSPEDProd:
              TTipoRegSPEDProd; const MovimID: TEstqMovimID; const MovimNiv:
              TEstqMovimNiv; const SQL_PeriodoVS: String; const OrigemOpeProc:
              TOrigemOpeProc): Boolean;
    function  Insere_OrigeProcVS_210(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; const MovimNivInn, MovimNivBxa:
              TEstqMovimNiv; const QrCab: TmySQLQuery; var IDSeq1: Integer;
              const SQL_PeriodoVS: String; const OrigemOpeProc: TOrigemOpeProc):
              Boolean;
    function  Insere_EmOpeProc_215(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; const QrCab:
              TmySQLQuery; (*const Fator: Double;*) const SQL_Periodo: String;
              const OrigemOpeProc: TOrigemOpeProc; var IDSeq1: Integer):
              Boolean;
    procedure Insere_EmOpeProc_Prd(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; const Qry: TmySQLQuery; const Fator:
              Double; const SQL_Periodo, SQL_PeriodoPQ: String; const
              OrigemOpeProc: TOrigemOpeProc; const MovimNiv: TEstqMovimNiv);
    //
    function  GrandezasInconsistentes(): Boolean;
    procedure Mensagem(ProcName: String; MovimCod, MovimID: Integer; TextoBase:
              String; Query: TmySQLQuery; Parar: Boolean = True);
    procedure PesquisaIMECsRelacionados(TargetMovimID, PsqMovimID_VMI,
              PsqMovimID_PQX: TEstqMovimID; PsqMovimNiv_VMI: TEstqMovimNiv;
              SQL_PeriodoVS, SQL_PeriodoPQ: String; IncluiPQx: Boolean);
    procedure ReopenOriPQx(MovimCod: Integer; SQL_PeriodoPQ: String;
              ShowSQL: Boolean);
    //
(*
    procedure InsereB_ProducaoVer1(SQLx, SQL_PeriodoPQ: String; PsqMovimID:
              TEstqMovimID; PsqMovimNiv: TEstqMovimNiv; OrigemOpeProc:
              TOrigemOpeProc);
*)
    procedure InsereB_ProducaoVer2_Slow(TabCab: String; TargetMovimID,
              PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; PsqMovimNiv:
              TEstqMovimNiv; OrigemOpeProc: TOrigemOpeProc;
              FormaInsercaoItemProducao: TFormaInsercaoItemProducao;
              IncluiPQx: Boolean);
    procedure InsereB_ProducaoVer2_Fast(TabCab: String; TargetMovimID,
              PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; PsqMovimNiv:
              TEstqMovimNiv; OrigemOpeProc: TOrigemOpeProc;
              FormaInsercaoItemProducao: TFormaInsercaoItemProducao;
              IncluiPQx: Boolean);
    function  InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd: TTipoRegSPEDProd;
              MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod: Integer;
              IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
              TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer): Boolean;
    function  InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd: TTipoRegSPEDProd;
              MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod: Integer;
              IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
              TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer;
              DtHrAberto: TDateTime): Boolean;
    procedure InsereItemProducaoConjuntaGemeos(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GraGruX: Integer;
              const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer; const
              _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc: TOrigemOpeProc;
              const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
              MovCodPai, PaiNivel1, PaiNivel2: Integer; var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ:
              Boolean; const Produziu: TProduziuVS);
    procedure InsereItemProducaoIsoladaGemeos(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, Tipo_Item, GraGruX: Integer;
              const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer; const
              _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc: TOrigemOpeProc;
              const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
              MovCodPai, PaiNivel1, PaiNivel2: Integer; var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
    procedure InsereItemProducaoConjuntaIndireta(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GraGruX: Integer;
              const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer; const
              _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc: TOrigemOpeProc;
              const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
              MovCodPai, PaiNivel1, PaiNivel2, GerMovID, GerNivel1, GerNivel2,
              GerGGX: Integer; const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
              var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoIsoladaIndireta(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GraGruX: Integer;
              const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer; const
              _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc: TOrigemOpeProc;
              const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
              MovCodPai, PaiNivel1, PaiNivel2, GerMovID, GerNivel1, GerNivel2,
              GerGGX: Integer; const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
              var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoConjuntaUniAnt(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GGXOri, GGXDst:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
              PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer; const
              QtdAntPeca, QtdAntPeso, QtdAntArM2: Double; var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoIsoladaUniAnt(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GGXOri, GGXDst:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
              PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer; const
              QtdAntPeca, QtdAntPeso, QtdAntArM2: Double; var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoConjuntaUniGer(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GGXOri, GGXDst:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
              PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer; const
              QtdGerPeca, QtdGerPeso, QtdGerArM2: Double; var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure InsereItemProducaoIsoladaUniGer(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GGXOri, GGXDst:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
              PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer; const
              QtdGerPeca, QtdGerPeso, QtdGerArM2: Double; var IDSeq1_K290,
              IDSeq1_K300: Integer; const IncluiPQx: Boolean;
              var InseriuPQ: Boolean);
    procedure DefineCliForSiteOnly(const Controle: Integer; var ClientMO, FornecMO,
              EntiSitio: Integer);
    procedure DefinePsqIMEI(const MovimCod, MovimID, MovimNiv: Integer; var
              IMEI: Integer);
    procedure DefineCliForSiteTipEstq(const Controle: Integer; var ClientMO, FornecMO,
              EntiSitio, TipoEstq: Integer);
    procedure InsereItemProducaoConjuntaSimples(const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GraGruX:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const AreaM2, PesoKg, Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
              Fator: Integer; const OriESTSTabSorc: TEstqSPEDTabSorc;
              const SPED_EFD_GerBxa: TSPED_EFD_GerBxa; const IncluiPQx: Boolean;
              var IDSeq1_K290, IDSeq1_K300: Integer);
    procedure VerificaSeTemGraGruX(IDLinPas, GraGruX: Integer; ProcName: String;
              Qry: TmySQLQuery);
  end;

var
  FmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b: TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b;

implementation
uses
  Module, UMySQLModule, ModuleGeral, UnVS_PF, ModAppGraG1, PQx, DmkDAC_PF,
  UnEfdIcmsIpi_PF_v03_0_2_a, UnGrade_PF, UnSPED_Create, UnVS_EFD_ICMS_IPI, UnPQ_PF;


{$R *.dfm}

{ TFmSpedEfdIcmsIpiProducaoConjunta }

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.DefineCliForSiteOnly(
  const Controle: Integer; var ClientMO, FornecMO, EntiSitio: Integer);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.DefineCliForSiteOnly()';
var
  MovimID, PsqMovimNiv, MovimCod: Integer;
  //
  procedure Msg(Texto: String);
  begin
    Geral.MB_Erro(Texto + ' em ' + sprocName + sLineBreak +
    'IME-I = ' + Geral.FF0(Controle));
  end;
begin
  ReopenObtCliForSite(ocfsControle, Controle, 0, 0);
  //N�o fechar QrObtCliForSite!!!
  ClientMO    := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
  FornecMO    := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
  EntiSitio   := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
  //
  if (FornecMO = 0) or (ClientMO = 0) or (EntiSitio = 0) then
  begin
    MovimCod    := QrObtCliForSite.FieldByName('MovimCod').AsInteger;
    MovimID     := QrObtCliForSite.FieldByName('MovimID').AsInteger;
    PsqMovimNiv := Integer(VS_PF.ObtemMovimNivCliForLocDeMovimID(TEstqMovimID(MovimID)));
    //
    ReopenObtCliForSite(ocfsMovIdNivCod, 0, MovimCod, PsqMovimNiv);
    if ClientMO = 0 then
      ClientMO  := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
    if FornecMO = 0 then
      FornecMO  := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
    if EntiSitio = 0 then
      EntiSitio := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
  end;
  if ClientMO = 0 then
    Msg('ClientMO nao definido');
  if (FornecMO = 0) and (EntiSitio = 0) then
    Msg('FornecMO e EntiSitio nao definidos');
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.DefineCliForSiteTipEstq(const Controle:
  Integer; var ClientMO, FornecMO, EntiSitio, TipoEstq: Integer);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.DefineCliForSiteTipEstq()';
var
  MovimID, PsqMovimNiv, MovimCod: Integer;
  //
  procedure Msg(Texto: String);
  begin
    Geral.MB_Erro(Texto + ' em ' + sprocName + sLineBreak +
    'IME-I = ' + Geral.FF0(Controle));
  end;
begin
  ReopenObtCliForSite(ocfsControle, Controle, 0, 0);
  //N�o fechar QrObtCliForSite!!!
  ClientMO    := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
  FornecMO    := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
  EntiSitio   := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
  TipoEstq := Trunc(QrObtCliForSite.FieldByName('TipoEstq').AsFloat);
  //
  if (FornecMO = 0) or (ClientMO = 0) or (EntiSitio = 0) then
  begin
    MovimCod    := QrObtCliForSite.FieldByName('MovimCod').AsInteger;
    MovimID     := QrObtCliForSite.FieldByName('MovimID').AsInteger;
    PsqMovimNiv := Integer(VS_PF.ObtemMovimNivCliForLocDeMovimID(TEstqMovimID(MovimID)));
    //
    ReopenObtCliForSite(ocfsMovIdNivCod, 0, MovimCod, PsqMovimNiv);
    if ClientMO = 0 then
      ClientMO  := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
    if FornecMO = 0 then
      FornecMO  := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
    if EntiSitio = 0 then
      EntiSitio := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
  end;
  if ClientMO = 0 then
    Msg('ClientMO nao definido');
  if (FornecMO = 0) and (EntiSitio = 0) then
    Msg('FornecMO e EntiSitio nao definidos');
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.DefineGGXCalToCur(Campo: String;
  Controle: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCalToCur, Dmod.MyDB, [
  //'SELECT DstGGX ',
  'SELECT ' + Campo + ' GraGruX ',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  //Geral.MB_SQL(Self, QrCalToCur);
  Result := QrCalToCurGraGruX.Value;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.DefinePsqIMEI(
  const MovimCod, MovimID, MovimNiv: Integer; var IMEI: Integer);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.DefinePsqIMEI()';
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimID=' + Geral.FF0(MovimID),
  'AND MovimNiv=' + Geral.FF0(MovimNiv),
  '']);
  //Geral.MB_SQL(Self, QrCalToCur);
  IMEI := Qry.FieldByName('Controle').AsInteger;
  if IMEI = 0 then
    Geral.MB_Erro('N�o foi poss�vel obter o IMEI em ' + sProcName);
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.FiltroPeriodoGAR(
  TipoRegSPEDProd: TTipoRegSPEDProd; SQL_Periodo: String): String;
begin
  case TipoRegSPEDProd of
    trsp23X, trsp29X: Result := SQL_Periodo;
    trsp25X, trsp30X: Result := '';
    else
    begin
      Geral.MB_Erro('"FiltroReg" indefinido!');
      Result := '#ERR_PERIODO_';
    end;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.FiltroRegGAR(
  TipoRegSPEDProd: TTipoRegSPEDProd): String;
begin
  case TipoRegSPEDProd of
    trsp23X, trsp29X: Result := '=';
    trsp25X, trsp30X: Result := '<>';
    else
    begin
      Geral.MB_Erro('"FiltroPeriodoGAR" indefinido: ' +
        Geral.FF0(Integer(TipoRegSPEDProd)) + '!');
      Result := '#ERR';
    end;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.FormCreate(Sender: TObject);
begin
  FParar := False;
  //
  FSEII_IMEcs := SPED_Create.RecriaTempTableNovo(ntrttSPEDEFD_IMECs, DModG.QrUpdPID1, False);
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.GeraGAR(
  SQL_Periodo: String): Boolean;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_GAR;',
  'CREATE TABLE _SPED_EFD_K2XX_GAR',
  'SELECT vmi.DstGGX, vmi.SrcGGX, ',
  'gg1.UnidMed, med.Grandeza med_grandeza, ',
  'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'DATE(vmi.DataHora) Data, vmi.Codigo, vmi.MovimCod,  ',
  'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg,  ',
  'vmi.MovimTwn  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '        vmi   ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiVS)),      //15
  SQL_Periodo,
  'ORDER BY Data, vmi.MovimCod, vmi.GraGruX ;',
  '']);
  //Geral.MB_SQL(Self, DModG.QrUpdPID1);
  Result :=
    not EfdIcmsIpi_PF.ErrGrandeza('_SPED_EFD_K2XX_GAR', 'SrcGGX', 'DstGGX');

end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.GrandezasInconsistentes: Boolean;
var
  SQL_PeriodoVMI: String;
begin
  Result := False;
  SQL_PeriodoVMI := dmkPF.SQL_Periodo('WHERE vmi.DataHora ', FDiaIni, FDiaFim, True, True);
  //
  if DfModAppGraG1.GrandesasInconsistentes([
  'DROP TABLE IF EXISTS _TESTE_; ',
  'CREATE TABLE _TESTE_ ',
  'SELECT DISTINCT ggx.GraGru1 Nivel1, ggx.Controle Reduzido,   ',
  'gg1.Nome NO_GG1, med.Sigla,  ',
  'CAST(med.Grandeza AS DECIMAL(11,0)) Grandeza, ',
  'CAST(xco.Grandeza AS DECIMAL(11,0)) xco_Grandeza,   ',
  'CAST(IF(xco.Grandeza > 0, xco.Grandeza + 0.000,   ',
  '  IF(ggx.GraGruY=1024 AND med.Grandeza = 0, 0.000,  ',
  '  IF((ggx.GraGruY<2048 OR   ',
  '  xco.CouNiv2<>1), 2.000,   ',
  '  IF(xco.CouNiv2=1, 1.000, -1.000)))) AS DECIMAL(11,0)) dif_Grandeza  ',
  '  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle   ',
  'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1   ',
  'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2   ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed  ',
  SQL_PeriodoVMI,
  'AND vmi.GraGruX <> 0  ',
  'AND ((IF(xco.Grandeza > 0, xco.Grandeza + 0.000,   ',
  '  IF(ggx.GraGruY=1024 AND med.Grandeza = 0, 0.000,  ',
  '  IF((ggx.GraGruY<2048 OR   ',
  '  xco.CouNiv2<>1), 2.000,   ',
  '  IF(xco.CouNiv2=1, 1.000, -1.000))) <> med.Grandeza + 0.000))) ; ',
  ' ',
  '/**/ ',
  ' ',
  'SELECT * FROM _TESTE_ ',
  'WHERE dif_Grandeza <> Grandeza ',
  '; ',
  ''], Dmod.MyDB) then
  begin
    Result := True;
    Close;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.ImportaDiluicaoMistura(
  SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe: String;
  OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaDiluicaoMistura()';
  MovimID = TEstqMovimID.emidMixInsum; // 35
  OriESTSTabSorc = estsPQx;
  JmpMovID  = 0;
  JmpMovCod = 0;
  JmpNivel1 = 0;
  JmpNivel2 = 0;
  PaiMovID  = 0;
  MovCodPai = 0;
  PaiNivel1 = 0;
  PaiNivel2 = 0;
var
  FornecMO, ClientMO, IDSeq1_K290, IDSeq1_K300, Codigo, MovimCod, CodDocOP,
  EntiSitio, Tipo_Item, GraGruX, UnidMed, Controle: Integer;
  Qtde: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  DtCorrApo_Txt, Sigla, NCM: String;
  //
  procedure ReopenPQMIts(Qry: TmySQLQuery; Tipo: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT OrigemCtrl, DataX, Insumo, Peso ',
    'FROM pqx',
    'WHERE Tipo=' + Geral.FF0(Tipo),
    'AND OrigemCodi=' + Geral.FF0(Codigo),
    'AND DtCorrApo="' + DtCorrApo_Txt + '" ',
    'AND Insumo > 0', // N�o buscar �gua, reciclo e textos!
    'AND Peso <> 0', // N�o textos!
    '']);
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQxMixOP, Dmod.MyDB, [
  'SELECT DISTINCT OrigemCodi, CliDest, DtCorrApo',
  'FROM pqx ',
  'WHERE Tipo IN (' + Geral.FF0(VAR_FATID_0030) + ',' + Geral.FF0(VAR_FATID_0130) + ')',
  'AND Empresa=' + Geral.FF0(FEmpresa),
  SQL_PeriodoPQ,
  '']);
  //Geral.MB_SQL(Self, QrPQxMixOP);
  QrPQxMixOP.First;
  while not QrPQxMixOP.Eof do
  begin
    if FParar then
      Exit;
    Codigo        := QrPQxMixOPOrigemCodi.Value;
    DtCorrApo_Txt := Geral.FDT(QrPQxMixOPDtCorrApo.Value, 1);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPQMCab, Dmod.MyDB, [
    'SELECT DataB, VSMovCod ',
    'FROM pqm',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    MovimCod   := QrPQMCabVSMovCod.Value;
    CodDocOP   := MovimCod;
    DtHrAberto := QrPQMCabDataB.Value;
    DtHrFimOpe := QrPQMCabDataB.Value;
    DtCorrApo  := QrPQxMixOPDtCorrApo.Value;
    //
    ReopenPQMIts(QrPQMGer, VAR_FATID_0030);
    ReopenPQMIts(QrPQMBxa, VAR_FATID_0130);
    //
    //
    IDSeq1_K290 := -1;
    IDSeq1_K300 := -1;
    FornecMO    := FEmpresa;
    ClientMO    := QrPQxMixOPCliDest.Value;
    EntiSitio   := FEmpresa;
    //
    if ClientMO = FornecMO then
      TipoRegSPEDProd := trsp29X
    else
      TipoRegSPEDProd := trsp30X;
     //
    case TipoRegSPEDProd of
      trsp29X:
      begin
        // K290 - Cabe�alho
        if IDSeq1_K290 = -1 then
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
          (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
          DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsPQx, MeAviso, IDSeq1_K290);
        end;
        QrPQMGer.First;
        while not QrPQMGer.Eof do
        begin
          DtMovim    := QrPQMGerDataX.Value;
          //DtCorrApo  := Acima!;
          //GraGruX    :=
          Qtde       := QrPQMGerPeso.Value;
          Controle   := QrPQMGerOrigemCtrl.Value;
          PQ_PF.ObtemDadosGradeDePQ(QrPQMGerInsumo.Value, Tipo_Item, GraGruX,
          UnidMed, Sigla, NCM, True);
          // K291 - Itens Produzidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsPQx, MeAviso);
          //
          QrPQMGer.Next;
        end;
        QrPQMBxa.First;
        while not QrPQMBxa.Eof do
        begin
          DtMovim    := QrPQMBxaDataX.Value;
          //DtCorrApo  := Acima!;
          //GraGruX    :=
          Qtde       := -QrPQMBxaPeso.Value;
          Controle   := QrPQMBxaOrigemCtrl.Value;
          PQ_PF.ObtemDadosGradeDePQ(QrPQMBxaInsumo.Value, Tipo_Item, GraGruX,
          UnidMed, Sigla, NCM, True);
          // K292 - Itens consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsPQx, MeAviso);
          //
          QrPQMBxa.Next;
        end;
      end;
      trsp30X:
      begin
        // K230 - Cabe�alho produ��o em terceiros
        if IDSeq1_K300 = -1 then
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
          (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
          DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsPQx, MeAviso, IDSeq1_K300);
        end;
        QrPQMGer.First;
        while not QrPQMGer.Eof do
        begin
          DtMovim    := QrPQMGerDataX.Value;
          //DtCorrApo  := Acima!;
          //GraGruX    :=
          Qtde       := QrPQMGerPeso.Value;
          Controle   := QrPQMGerOrigemCtrl.Value;
          PQ_PF.ObtemDadosGradeDePQ(QrPQMGerInsumo.Value, Tipo_Item, GraGruX,
          UnidMed, Sigla, NCM, True);
          // K301 - Itens Produzidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsPQx, MeAviso);
          //
          QrPQMGer.Next;
        end;
        QrPQMBxa.First;
        while not QrPQMBxa.Eof do
        begin
          DtMovim    := QrPQMBxaDataX.Value;
          //DtCorrApo  := Acima!;
          //GraGruX    :=
          Qtde       := -QrPQMBxaPeso.Value;
          Controle   := QrPQMBxaOrigemCtrl.Value;
          PQ_PF.ObtemDadosGradeDePQ(QrPQMBxaInsumo.Value, Tipo_Item, GraGruX,
          UnidMed, Sigla, NCM, True);
          // K302 - Itens Consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsPQx, MeAviso);
          //
          QrPQMBxa.Next;
        end;
        //
      end;
      else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(20)');
    end;
    //
    QrPQxMixOP.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.ImportaGerArtConjunta(
  SQL_PeriodoComplexo, SQL_Periodo, SQL_DtHrFimOpe: String;
  OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  MovimID = TEstqMovimID.emidIndsVS;
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaGerArtConjunta()';
  //
  JmpMovID  = 0;
  JmpMovCod = 0;
  JmpNivel1 = 0;
  JmpNivel2 = 0;
  PaiMovID  = 0;
  MovCodPai = 0;
  PaiNivel1 = 0;
  PaiNivel2 = 0;
  //
  function InsereRegistros(TipoRegSPEDProd: TTipoRegSPEDProd): Boolean;
  var
    MovimCod, MovimTwn, Codigo, GraGruX, TipoEstq, MyFatorCab, MyFAtorIts,
    IDSeq1, ID_Item, Controle, ClientMO, FornecMO, EntiSitio, IDSeq1_K290,
    IDSeq1_K300, CodDocOP: Integer;
    DtHrAberto, DtHrFimOpe: TDateTime;
    AreaM2, PesoKg, Pecas, Qtde, QtdGerArM2: Double;
    COD_INS_SUBST: String;
    //
    DtMovim, DtCorrApo: TDateTime;
    OriESTSTabSorc: TEstqSPEDTabSorc;
  begin
    COD_INS_SUBST := '';
    MyFatorCab := 1;
    MyFatorIts := -1;
    //
////////////////////////////////////////////////////////////////////////////////
///  Cabecalho > Registro K230 ou K250
////////////////////////////////////////////////////////////////////////////////
    ReopenVmiGArCab(TipoRegSPEDProd, SQL_Periodo);
    //Geral.MB_SQL(Self, QrVmiGArCab);
    PB2.Position := 0;
    PB2.Max := QrVmiGArCab.RecordCount;
    QrVmiGArCab.First;
    while not QrVmiGArCab.Eof do
    begin
      MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
      //
      IDSeq1_K290 := -1;
      IDSeq1_K300 := -1;
      CodDocOP    := QrVmiGArCabMovimCod.Value;
      //
      Codigo      := QrVmiGArCabCodigo.Value;
      MovimCod    := QrVmiGArCabMovimCod.Value;
      MovimTwn    := QrVmiGArCabMovimTwn.Value;
      DtHrAberto  := QrVmiGArCabData.Value;
      DtHrFimOpe  := QrVmiGArCabData.Value;
      DtMovim     := QrVmiGArCabData.Value;
      DtCorrApo   := QrVmiGArCabDtCorrApo.Value;
      GraGruX     := QrVmiGArCabGraGruX.Value;
      TipoEstq    := Trunc(QrVmiGArCabTipoEstq.Value);
      ClientMO    := QrVmiGArCabClientMO.Value;
      FornecMO    := QrVmiGArCabFornecMO.Value;
      EntiSitio   := QrVmiGArCabEntiSitio.Value;
      Controle    := QrVmiGArCabControle.Value;
      //
(*
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumGAR, Dmod.MyDB, [
      'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,  ',
      'SUM(AreaM2) AreaM2 ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestCurtiVS)),
      'AND GraGruX=' + Geral.FF0(GraGruX),
      '']);
      AreaM2     := QrSumGARAreaM2.Value;
      PesoKg     := QrSumGARPesoKg.Value;
      Pecas      := QrSumGARPecas.Value;
*)
      AreaM2     := QrVmiGArCabAreaM2.Value;
      QtdGerArM2 := QrVmiGArCabQtdGerArM2.Value;
      PesoKg     := QrVmiGArCabPesoKg.Value;
      Pecas      := QrVmiGArCabPecas.Value;
      //
      if ((Pecas <> 0) or (TipoEstq = 2(*kg*))
      or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
      begin
        case TipoEstq of
          0: Qtde := Pecas;
          1:
          begin
            if AreaM2 <> 0 then
              Qtde := AreaM2
            else
              Qtde := QtdGerArM2;
          end;
          2: Qtde := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
            //
            Qtde := Pecas;
          end;
        end;
        Qtde := Qtde * MyFatorCab;
      end else
        Qtde := 0;
      //
      if (*TemTwn and*) (Qtde = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
        GraGruX, MovimCod, Controle, sProcName);
      //
      if Qtde < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (1)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrVmiGArCab)
      else if (Qtde = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidIndsVS: DtHrFimOpe := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade zerada para OP encerrada!!! (1)', QrVmiGArCab);
        end;
      end;
      //
      case TipoRegSPEDProd of
        trsp29X:
        begin
          if IDSeq1_K290 = -1 then
          begin
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
            (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
            DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
            FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290);
          end;
          // K291 - Itens Produzidos
          OriESTSTabSorc := estsVMI;
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
        end;
        trsp30X:
        begin
          // K230 - Cabe�alho produ��o em terceiros
          if IDSeq1_K300 = -1 then
          begin
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
            (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
            DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
            FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300);
          end;
          // K301 - Itens Produzidos
          OriESTSTabSorc := estsVMI;
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(1)');
      end;
////////////////////////////////////////////////////////////////////////////////
///  Item > Registro K235 ou K255
////////////////////////////////////////////////////////////////////////////////
      //
      ReopenVmiGArIts(MovimCod, MovimTwn, SQL_Periodo, TipoRegSPEDProd);
      QrVmiGArIts.First;
      while not QrVmiGArIts.Eof do
      begin
        if TEstqMovimID(QrVmiGArItsSrcMovID.Value) = emidEmProcCur then
        begin
          //GraGruX := DefineGGXCalToCur('JmpGGX', QrVmiGArItsSrcNivel2.Value);
          GraGruX := QrVmiGArItsJmpGGX.Value;
        end
        else
          GraGruX := QrVmiGArItsGraGruX.Value;
        //
        Codigo     := QrVmiGArItsCodigo.Value;
        MovimCod   := QrVmiGArItsMovimCod.Value;
        MovimTwn   := QrVmiGArItsMovimTwn.Value;
        DtHrAberto := QrVmiGArItsData.Value;
        DtHrFimOpe := QrVmiGArItsData.Value;
        TipoEstq   := QrVmiGArItsTipoEstq.Value;
        Controle   := QrVmiGArItsControle.Value;
        ID_Item    := Controle;
        //
        AreaM2     := QrVmiGArItsAreaM2.Value;
        PesoKg     := QrVmiGArItsPesoKg.Value;
        Pecas      := QrVmiGArItsPecas.Value;
        //
        DtCorrApo  := QrVmiGArItsDtCorrApo.Value;
        //
        if ((Pecas <> 0) or (TipoEstq = 2(*kg*))
        or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
        begin
          case TipoEstq of
            0: Qtde := Pecas;
            1: Qtde := AreaM2;
            2: Qtde := PesoKg;
            else
            begin
              Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
              + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
              //
              Qtde := Pecas;
            end;
          end;
          Qtde := Qtde * MyFatorIts;
        end else
          Qtde := 0;
        if (*TemTwn and*) (Qtde = 0) then
          VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
          GraGruX, MovimCod, Controle, sProcName);
        //
        if Qtde < 0 then
          Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade n�o pode ser negativa!!! (2)' + sLineBreak +
          'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrVmiGArIts)
        else if (Qtde = 0) and (DtHrFimOpe > 1) then
        begin
          case MovimID of
            emidIndsVS: DtHrFimOpe := 0;
            else Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade zerada para OP encerrada!!! (2)', QrVmiGArIts);
          end;
        end;
        //
        case TipoRegSPEDProd of
          //trsp21X:
          trsp29X:
          begin
            // K292 - Itens consumidos
            OriESTSTabSorc := estsVMI;
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
            PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
            GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
            ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
            FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
          end;
          trsp30X:
          begin
            // K302 - Itens Consumidos
            OriESTSTabSorc := estsVMI;
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
            PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
            GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, Controle,
            ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
            FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
          end;
          else
            Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
            TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(2)');
        end;
        //
        QrVmiGArIts.Next;
      end;
      //
      QrVmiGArCab.Next;
    end;
  end;
begin
  if not GeraGAR(SQL_Periodo) then
    Exit;
  //
  InsereRegistros(trsp29X);
  InsereRegistros(trsp30X);
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.ImportaGerArtIsolada(SQL_PeriodoComplexo,
  SQL_Periodo, SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  MovimID = TEstqMovimID.emidIndsVS;
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaGerArtIsolada()';
  //
  function FiltroReg(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := '=';
      trsp25X: Result := '<>';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (1)');
        Result := '#ERR';
      end;
    end;
  end;
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := SQL_Periodo;
      //trsp25X: Result := SQL_DtHrFimOpe;
      //trsp25X: Result := SQL_Periodo;
      trsp25X: Result := '';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido!');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
  //
  function InsereRegistros(TipoRegSPEDProd: TTipoRegSPEDProd): Boolean;
  var
    MovimCod, MovimTwn, Codigo, GraGruX, TipoEstq, MyFatorCab, MyFAtorIts,
    IDSeq1, ID_Item, Controle, ClientMO, FornecMO, EntiSitio, Tipo_Item: Integer;
    DtHrAberto, DtHrFimOpe: TDateTime;
    AreaM2, PesoKg, Pecas, Qtde, QtdGerArM2: Double;
    COD_INS_SUBST: String;
    //
    DtMovim, DtCorrApo: TDateTime;
  begin
    COD_INS_SUBST := '';
    MyFatorCab := 1;
    MyFatorIts := -1;
    //
////////////////////////////////////////////////////////////////////////////////
///  Cabecalho > Registro K230 ou K250
////////////////////////////////////////////////////////////////////////////////
    ReopenVmiGArCab(TipoRegSPEDProd, SQL_Periodo);
    PB2.Position := 0;
    PB2.Max := QrVmiGArCab.RecordCount;
    QrVmiGArCab.First;
    while not QrVmiGArCab.Eof do
    begin
      if FParar then
        Exit;
      MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
      //
      Codigo     := QrVmiGArCabCodigo.Value;
      MovimCod   := QrVmiGArCabMovimCod.Value;
      MovimTwn   := QrVmiGArCabMovimTwn.Value;
      DtHrAberto := QrVmiGArCabData.Value;
      DtHrFimOpe := QrVmiGArCabData.Value;
      DtMovim    := QrVmiGArCabData.Value;
      DtCorrApo  := QrVmiGArCabDtCorrApo.Value;
      GraGruX    := QrVmiGArCabGraGruX.Value;
      TipoEstq   := Trunc(QrVmiGArCabTipoEstq.Value);
      ClientMO   := QrVmiGArCabClientMO.Value;
      FornecMO   := QrVmiGArCabFornecMO.Value;
      EntiSitio  := QrVmiGArCabEntiSitio.Value;
      Controle   := QrVmiGArCabControle.Value;
      //
(*
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumGAR, Dmod.MyDB, [
      'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,  ',
      'SUM(AreaM2) AreaM2 ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestCurtiVS)),
      'AND GraGruX=' + Geral.FF0(GraGruX),
      '']);
      AreaM2     := QrSumGARAreaM2.Value;
      PesoKg     := QrSumGARPesoKg.Value;
      Pecas      := QrSumGARPecas.Value;
*)
      AreaM2     := QrVmiGArCabAreaM2.Value;
      PesoKg     := QrVmiGArCabPesoKg.Value;
      Pecas      := QrVmiGArCabPecas.Value;
      QtdGerArM2 := QrVmiGArCabQtdGerArM2.Value;
      //
      if ((Pecas <> 0) or (TipoEstq = 2(*kg*))
      or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
      begin
        case TipoEstq of
          0: Qtde := Pecas;
          1:
          begin
            if AreaM2 <> 0 then
              Qtde := AreaM2
            else
              Qtde := QtdGerArM2;
          end;
          2: Qtde := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
            //
            Qtde := Pecas;
          end;
        end;
        Qtde := Qtde * MyFatorCab;
      end else
        Qtde := 0;
      if (*TemTwn and*) (Qtde = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
        GraGruX, MovimCod, Controle, sProcName);
      //
      if Qtde < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (3)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrVmiGArCab)
      else if (Qtde = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidIndsVS: DtHrFimOpe := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade zerada para OP encerrada!!! (3)', QrVmiGArCab);
        end;
      end;
      //
      case TipoRegSPEDProd of
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod,
          Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GraGruX, ClientMO,
          FornecMO, EntiSitio, Qtde, OrigemOpeProc,
          FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod,
          Controle,
          DtHrFimOpe,
          DtMovim, DtCorrApo, GraGruX, ClientMO, FornecMO, EntiSitio,
          Qtde, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(3)');
      end;
////////////////////////////////////////////////////////////////////////////////
///  Item > Registro K235 ou K255
////////////////////////////////////////////////////////////////////////////////
      //
      ReopenVmiGArIts(MovimCod, MovimTwn, SQL_Periodo, TipoRegSPEDProd);
      QrVmiGArIts.First;
      while not QrVmiGArIts.Eof do
      begin
        if TEstqMovimID(QrVmiGArItsSrcMovID.Value) = emidEmProcCur then
        begin
          //GraGruX := DefineGGXCalToCur('JmpGGX', QrVmiGArItsSrcNivel2.Value);
          GraGruX := QrVmiGArItsJmpGGX.Value;
        end
        else
          GraGruX := QrVmiGArItsGraGruX.Value;
        //
        Codigo     := QrVmiGArItsCodigo.Value;
        MovimCod   := QrVmiGArItsMovimCod.Value;
        MovimTwn   := QrVmiGArItsMovimTwn.Value;
        DtHrAberto := QrVmiGArItsData.Value;
        DtHrFimOpe := QrVmiGArItsData.Value;
        TipoEstq   := QrVmiGArItsTipoEstq.Value;
        Controle   := QrVmiGArItsControle.Value;
        ID_Item    := Controle;
        //
        AreaM2     := QrVmiGArItsAreaM2.Value;
        PesoKg     := QrVmiGArItsPesoKg.Value;
        Pecas      := QrVmiGArItsPecas.Value;
        //
        DtCorrApo  := QrVmiGArItsDtCorrApo.Value;
        //
        if ((Pecas <> 0) or (TipoEstq = 2(*kg*))
        or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
        begin
          case TipoEstq of
            0: Qtde := Pecas;
            1: Qtde := AreaM2;
            2: Qtde := PesoKg;
            else
            begin
              Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
              + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
              //
              Qtde := Pecas;
            end;
          end;
          Qtde := Qtde * MyFatorIts;
        end else
          Qtde := 0;
        if (*TemTwn and*) (Qtde = 0) then
          VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
          GraGruX, MovimCod, Controle, sProcName);
        //
        if Qtde < 0 then
          Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade n�o pode ser negativa!!! (4)' + sLineBreak +
          'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrVmiGArIts)
        else if (Qtde = 0) and (DtHrFimOpe > 1) then
        begin
          case MovimID of
            emidIndsVS: DtHrFimOpe := 0;
            else Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade zerada para OP encerrada!!! (4)', QrVmiGArIts);
          end;
        end;
        //
        case TipoRegSPEDProd of
          //trsp21X:
          trsp23X:
          begin
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GraGruX,
            Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
            Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
            ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
          end;
          trsp25X:
          begin
            EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GraGruX,
            Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
            Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
            ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
          end;
          else
            Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
            TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(4)');
        end;
        //
        QrVmiGArIts.Next;
      end;
      //
      QrVmiGArCab.Next;
    end;
  end;
begin
  if not GeraGAR(SQL_Periodo) then
    Exit;
  //
  InsereRegistros(trsp23X);
  InsereRegistros(trsp25X);
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.ImportaOpeProcA(QrCab: TMySQLQuery;
  MovimID: TEstqMovimID; SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ,
  SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
var
  TabSrc, SQL_FLD, SQL_AND: String;
  MovNivInn, MovNivBxa: TEstqMovimNiv;
  //
  function FiltroReg(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := '=';
      trsp25X: Result := '<>';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (2)');
        Result := '#ERR';
      end;
    end;
  end;
  //
  function FiltroCond(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := ' OR ';
      trsp25X: Result := ' AND ';
      else
      begin
        Geral.MB_Erro('"FiltroCond" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (2a)');
        Result := '#ERR';
      end;
    end;
  end;
  //
  function ReabreEmProcesso21X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    SQL_PeriodoComplexo,
    'OR MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    '  ' + SQL_PeriodoVS,
    ')) ',
    SQL_AND,

    ' ',
    'AND MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv=' + Geral.FF0(Integer(MovNivInn)),
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    //'  AND FornecMO' + FiltroReg(trsp21X) + Geral.FF0(FEmpresa) +
    '  )',
    ') ',
    ' ',
    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function ReabreEmProcesso23X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    //'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    SQL_PeriodoComplexo,
    'OR MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    '  ' + SQL_PeriodoVS,
    ')) ',
    SQL_AND,

    ' ',
    'AND MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv=' + Geral.FF0(Integer(MovNivInn)),
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    '  AND (FornecMO' + FiltroReg(trsp23X) + Geral.FF0(FEmpresa),
    '  ' + FiltroCond(trsp23X) + ' FornecMO' + FiltroReg(trsp23X) + '0) ) ',
    ') ',
    ' ',

    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
(*
    if Query.RecordCount > 0 then Geral.MB_SQL(Self, Query);
*)
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function ReabreEmProcesso25X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    'WHERE  MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv IN (' + Geral.FF0(Integer(MovNivInn)) + ',' +
    Geral.FF0(Integer(MovNivBxa)) + ') ',
    //
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    '  AND (FornecMO' + FiltroReg(trsp25X) + Geral.FF0(FEmpresa),
    '  ' + FiltroCond(trsp25X) + ' FornecMO' + FiltroReg(trsp25X) + '0) ) ',
    ' ' + SQL_PeriodoVS,
    ') ',
    ' ',
    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
(*
    if Query.RecordCount > 0 then Geral.MB_SQL(Self, Query);
*)
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function ReabreEmProcesso26X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    SQL_PeriodoComplexo,
    'OR MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    '  ' + SQL_PeriodoVS,
    ')) ',
    SQL_AND,

    ' ',
    'AND MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv=' + Geral.FF0(Integer(MovNivInn)),
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    '  )',
    ') ',
    ' ',
    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
(*
    if Query.RecordCount > 0 then Geral.MB_SQL(Self, Query);
*)
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function InsereRegistros(TipoRegSPEDProd: TTipoRegSPEDProd): Boolean;
  var
    LinArqPai, MovimCod: Integer;
    Fator: Double;
    Abriu: Boolean;
  begin
    Result := False;
    case TipoRegSPEDProd of
      trsp21X: Abriu := ReabreEmProcesso21X(QrCab);
      trsp23X: Abriu := ReabreEmProcesso23X(QrCab);
      trsp25X: Abriu := ReabreEmProcesso25X(QrCab);
      trsp26X: Abriu := ReabreEmProcesso26X(QrCab);
      else
      begin
        Geral.MB_Aviso('Tipo de registro indefinido em "InsereRegistros()"');
        Abriu := False;
      end;
    end;
    if not Abriu then
      Exit;
    //
    PB2.Max := QrCab.RecordCount;
    PB2.Position := 0;
    QrCab.First;
    while not QrCab.Eof do
    begin
      if FParar then
        Exit;
      MyObjects.UpdPB(PB2, nil, nil);
      MovimCod  := QrCab.FieldByName('MovimCod').AsInteger;
      if TipoRegSPEDProd = trsp21X then
      begin
        case MovimID of
(*         Ja tem proprio: Insere_OrigeRibPDA_210()
          emidEmRibPDA:
          begin
            Fator := 1;
            Insere_OrigeRibPDA_210(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminDestPDA, QrCab, LinArqPai, SQL_PeriodoVS, OrigemOpeProc);
          end;
*)
          emidEmProcCal: ; // nada!!
          emidEmProcCur: ; // nada!!
          emidEmOperacao:
          begin
            Fator := 1;
            Insere_OrigeProcVS_210(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminEmOperInn, TEstqMovimNiv.eminEmOperBxa, QrCab, LinArqPai, SQL_PeriodoVS, OrigemOpeProc);
            Insere_EmOpeProc_215(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminDestOper, QrCab, (*Fator,*) SQL_PeriodoVS, OrigemOpeProc, LinArqPai);
          end;
          emidEmProcWE: ; // nada!!
          else
          begin
            Result := False;
            Geral.MB_Erro('MovimID ' + Geral.FF0(Integer(MovimID)) + ' n�o implementado em "ImportaOpeProc().21x"');
            Exit;
          end;
        end;
      end else
      begin
        case MovimID of
          emidEmProcCal:
          begin
            Fator := -1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmCalBxa);
          end;
          emidEmProcCur:
          begin
            Fator := 1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmCurBxa);
          end;
          emidEmProcWE:
          begin
            Fator := 1;

            //criar query que separa periodos + Periodos de correcao!
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmWEndBxa);
          end;
          emidEmProcSP:
          begin
            Fator := 1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmPSPBxa);
          end;
          emidEmReprRM:
          begin
            Fator := 1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmRRMBxa);
           // Geral.MB_SQL(Self, QrCab);
          end;
          else
          begin
            Result := False;
            // MovimID 30 n�o implementado em "ImportaOpeProc().2xx"
            Geral.MB_Erro('MovimID ' + Geral.FF0(Integer(MovimID)) + ' n�o implementado em "ImportaOpeProc().2xx"');
            Exit;
          end;
        end;
      end;
      QrCab.Next;
    end;
    Result := True;
  end;
begin
//fazer novo aqui!
  //PB1.Position := PB1.Position + 1;
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando "DtHrFimOpe" do MovimID: ' +
    Geral.FF0(Integer(MovimID)));
  VS_PF.AtualizaDtHrFimOpe_MovimID(MovimID);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Importando MovimID: ' +
    Geral.FF0(Integer(MovimID)));
  TabSrc    := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
  // Recurtimento!!!
  MovNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
  MovNivBxa := VS_PF.ObtemMovimNivBxaDeMovimID(MovimID);
  // ?????
  //MovNivInn := VS_PF.ObtemMovimNivBxaDeMovimID(MovimID);
  if MovNivInn = eminSemNiv then
    Exit;
  //
  SQL_FLD := Geral.ATS([
  'SELECT DISTINCT ggx.GraGru1 Nivel1, ggx.Controle Reduzido, ',
  'gg1.Nome NO_GG1, med.Sigla, med.Grandeza ',
  '']);
  SQL_AND := Geral.ATS([
  'AND ((IF(xco.Grandeza > 0, xco.Grandeza, ',
  '  IF((ggx.GraGruY<2048 OR ',
  '  xco.CouNiv2<>1), 2, ',
  '  IF(xco.CouNiv2=1, 1, -1))) <> med.Grandeza))',
  '']);
  if GrandezasInconsistentes() then
    Exit;
  //
  SQL_FLD := Geral.ATS([
  'SELECT  ',
  //
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vsx.AreaDstM2', 'vsx.PesoKgDst', 'vsx.PecasDst'),
  //
  'vsx.* ',
  '']);
  SQL_AND := '';
  //
  case MovimID of
    //emidEmRibPDA, Ja tem proprio: Insere_OrigeRibPDA_210()
    //emidEmRibDTA,
    emidEmOperacao:
    begin
      InsereRegistros(trsp21X);
    end;
    emidEmProcCal,
    emidEmProcCur,
    emidEmProcWE,
    emidEmProcSP:
    begin
      InsereRegistros(trsp23X);
      InsereRegistros(trsp25X);
    end;
    emidEmReprRM:
    begin
      InsereRegistros(trsp26X);
    end;
    else Geral.MB_Erro('"MovimID" n�o implementado em "ImportaOpeProcA()"');
  end;
  //
  Result := True;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.ImportaOpeProcB(QrCab: TMySQLQuery;
  MovimID, PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; MovimNiv,
  PsqMovimNiv_VMI: TEstqMovimNiv; SQL_PeriodoComplexo, SQL_PeriodoVS,
  SQL_PeriodoPQ, SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc):
  Boolean;
const
  sProcName = 'TTFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaOpeProcB()"';
begin
//
  //case FSPED_EFD_Producao of
  case FVersaoIsldCnjt of
    0: ImportaOpeProcB_Isolada(QrCab, PsqMovimNiv_VMI, SQL_PeriodoComplexo,
       SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe, OrigemOpeProc);
    1: ImportaOpeProcB_Conjunta(QrCab, MovimID, PsqMovimID_VMI, PsqMovimID_PQX,    //isolada tambem??
       MovimNiv, PsqMovimNiv_VMI, SQL_PeriodoComplexo, SQL_PeriodoVS,
       SQL_PeriodoPQ, SQL_DtHrFimOpe, OrigemOpeProc);
    //else Geral.MB_Erro('"FSPED_EFD_Producao" indefinido em ' + sProcName);
    else Geral.MB_Erro('"FVersaoIsldCnjt" indefinido em ' + sProcName);
  end
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.ImportaOpeProcB_Conjunta(QrCab:
  TMySQLQuery; MovimID, PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; MovimNiv,
  PsqMovimNiv_VMI: TEstqMovimNiv; SQL_PeriodoComplexo, SQL_PeriodoVS,
  SQL_PeriodoPQ, SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc):
  Boolean;
const
  sProcName = 'TTFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaOpeProcB_Conjunta()"';
  //
  function ParteSQL(Tabela, FldSrc, FldDst, sArea, sPeso, sPeca, dArea, dPeso, dPeca: String; MovimNiv:
  TEstqmovimNiv): String;
  var
    SQL_IF: String;
  begin
    SQL_IF := 'IF(' + Copy(SQL_PeriodoVS, 4) + ', ';
    //
    Result := Geral.ATS([
    'SELECT vmi.Empresa, vmi.MovimID, vmi.Codigo, vmi.MovimCod,  ',
    'vmi.MovimNiv, vmi.Controle, ' +
    SQL_IF + sPeca + ', 0.000) Pecas, ' +
    SQL_IF + sArea + ', 0.000) AreaM2, ' +
    SQL_IF + sPeso + ', 0.000) PesoKg, ',
    'DATE(vmi.DataHora) Data,  ' + FldDst + ' GGX_Dst, ', //+ FldOri + ', ',
    FldSrc + ' GGX_Src, ',
    '  ',
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'sTipoEstq', True,
    sArea, sPeso, sPeca, 's'),
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'dTipoEstq', True,
    dArea, dPeso, dPeca, 'd'),
    //
    ' ',
    SQL_IF + dPeca + ', 0.000) QtdAntPeca, ' +
    SQL_IF + dArea + ', 0.000) QtdAntArM2, ' +
    SQL_IF + dPeso + ', 0.000) QtdAntPeso, ',
    'vmi.DtCorrApo, vmi.ClientMO, vmi.FornecMO, scc.EntiSitio ',
////////////////////////////////////////////////////////////////////////////////
    //'FROM ' + TMeuDB + '.' + Tabela + ' cab ',
    'FROM ' + FSEII_IMEcs + ' imc ',
    'LEFT JOIN ' + TMeuDB + '.' + Tabela + ' cab ON cab.MovimCod=imc.MovimCod ',
////////////////////////////////////////////////////////////////////////////////

    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '   vmi ON vmi.MovimCod=cab.MovimCod ',
    'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    sggx ON sggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    sgg1 ON sgg1.Nivel1=sggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    smed ON smed.Codigo=sgg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    sggc ON sggc.Controle=sggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  sgcc ON sgcc.Codigo=sggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  sgti ON sgti.Controle=sggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou sxco ON sxco.GraGruX=sggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    snv1 ON snv1.Codigo=sxco.CouNiv1   ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    snv2 ON snv2.Codigo=sxco.CouNiv2   ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    dggx ON dggx.Controle=cab.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    dgg1 ON dgg1.Nivel1=dggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    dmed ON dmed.Codigo=dgg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    dggc ON dggc.Controle=dggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  dgcc ON dgcc.Codigo=dggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  dgti ON dgti.Controle=dggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou dxco ON dxco.GraGruX=dggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    dnv1 ON dnv1.Codigo=dxco.CouNiv1   ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    dnv2 ON dnv2.Codigo=dxco.CouNiv2   ',
    ' ',
    //'WHERE imc.MovimID=' + Geral.FF0(Integer(PsqMovimID)),
  '']);
  end;
  procedure AvisoOpcEspecif(Txt: String);
  begin
    Geral.MB_Aviso('Configura��o inv�lida em op��es espec�ficas para ' +
    sProcName + ' para o item: ' + Geral.FF0(Integer(MovimNiv)) +
    ' - ' + sEstqMovimNiv[Integer(MovimNiv)] + Txt);
  end;
var
  SQLx: String;
  IncluiPQx: Boolean;
begin
  case MovimNiv of
    //  Couro em caleiro (origem: Couro PDA)
    TEstqmovimNiv.eminSorcCal:
    begin
      IncluiPQx := True;
      SQLx := ParteSQL('vscalcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCal);
      //
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      case Dmod.QrControleSPED_II_EmCal.Value of
(*
        2: InsereB_ProducaoVer1(SQLx, SQL_PeriodoPQ, PsqMovimID_VMI, PsqMovimNiv_VMI,
           OrigemOpeProc);
*)
        2: InsereB_ProducaoVer2('vscalcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2('vscalcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
      //
    end;
    TEstqmovimNiv.eminDestCal:
    begin
(*
26.31 n�o acha!
29.31 � o certo para Couro caleado!
*)
      IncluiPQx := False;
      SQLx := ParteSQL('vscaljmp', 'vmi.JmpGGX', 'vmi.GraGruX', '-vmi.QtdGerArM2',
      '-vmi.QtdGerPeso', '-vmi.QtdGerPeca',  'vmi.QtdGerArM2',
      'vmi.QtdGerPeso', 'vmi.QtdGerPeca', TEstqmovimNiv.eminDestCal);
      //
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //
      case Dmod.QrControleSPED_II_Caleado.Value of
(*
        2: InsereB_ProducaoVer1(SQLx, SQL_PeriodoPQ, PsqMovimID_VMI, PsqMovimNiv_VMI,
           OrigemOpeProc);
*)
        2: InsereB_ProducaoVer2('vscalcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2('vscalcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    TEstqmovimNiv.eminSorcCur:
    begin
      IncluiPQx := True;
      SQLx := ParteSQL('vscurcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCur);
      //
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //InsereB_ProducaoVer2('vscurcab', PsqMovimID, PsqMovimNiv, OrigemOpeProc);
      //testar
      case Dmod.QrControleSPED_II_EmCur.Value of
(*
        2: InsereB_ProducaoVer1(SQLx, SQL_PeriodoPQ, PsqMovimID_VMI, PsqMovimNiv_VMI,
           OrigemOpeProc);
*)
        2: InsereB_ProducaoVer2('vscurcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2('vscurcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    TEstqmovimNiv.eminDestCur:
    begin
      IncluiPQx := False;
(*
      SQLx := ParteSQL('vscaljmp', 'vmi.JmpGGX', 'vmi.GraGruX', '-vmi.QtdGerArM2',
      '-vmi.QtdGerPeso', '-vmi.QtdGerPeca',  'vmi.QtdGerArM2',
      'vmi.QtdGerPeso', 'vmi.QtdGerPeca', TEstqmovimNiv.eminDestCal);
*)
      SQLx := ParteSQL('vscurcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCur);
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //
      case Dmod.QrControleSPED_II_Caleado.Value of
(*
        2: InsereB_ProducaoVer1(SQLx, SQL_PeriodoPQ, PsqMovimID_VMI, PsqMovimNiv_VMI,
           OrigemOpeProc);
*)
        2: InsereB_ProducaoVer2('vscurcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2('vscurcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    TEstqmovimNiv.eminDestOper:
    begin
      IncluiPQx := True;
      SQLx := 'SELECT ???? FROM ????';
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //
      case Dmod.QrControleSPED_II_Operacao.Value of
        2: InsereB_ProducaoVer2('vsopecab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2('vsopecab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    TEstqmovimNiv.eminDestWEnd:
    begin
      IncluiPQx := True;
      SQLx := 'SELECT ???? FROM ????';
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //
      case Dmod.QrControleSPED_II_Recurt.Value of
        2: InsereB_ProducaoVer2('vspwecab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2('vspwecab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    TEstqmovimNiv.eminDestPSP:
    begin
      IncluiPQx := True;
      SQLx := 'SELECT ???? FROM ????';
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //
      case Dmod.QrControleSPED_II_SubProd.Value of
        2: InsereB_ProducaoVer2('vspspcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2('vspspcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    TEstqmovimNiv.eminDestRRM:
    begin
      IncluiPQx := True;
      SQLx := 'SELECT ???? FROM ????';
      PesquisaIMECsRelacionados(MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI, SQL_PeriodoVS,
      SQL_PeriodoPQ, IncluiPQx);
      //
      case Dmod.QrControleSPED_II_Reparo.Value of
        2: InsereB_ProducaoVer2('vsrrmcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipIsolada, IncluiPQX);
        3: InsereB_ProducaoVer2('vsrrmcab', MovimID, PsqMovimID_VMI, PsqMovimID_PQX, PsqMovimNiv_VMI,
           OrigemOpeProc, fipConjunta, IncluiPQX);
        else AvisoOpcEspecif('');
      end;
    end;
    //
    else
    begin
      IncluiPQx := False;
      SQLx := 'SELECT ???? FROM ????';
      Geral.MB_Erro('"MovimNiv" n�o implementado em ' + sProcName);
    end;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.ImportaOpeProcB_Isolada(QrCab: TMySQLQuery;
  PsqMovimNiv: TEstqMovimNiv; SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ,
  SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
  //
  function ParteSQL(Tabela, FldSrc, FldDst, sArea, sPeso, sPeca, dArea, dPeso, dPeca: String; MovimNiv:
  TEstqmovimNiv): String;
  var
    SQL_IF: String;
  begin
    SQL_IF := 'IF(' + Copy(SQL_PeriodoVS, 4) + ', ';
    //
    Result := Geral.ATS([
    'SELECT vmi.Empresa, vmi.MovimID, vmi.Codigo, vmi.MovimCod,  ',
    'vmi.MovimNiv, vmi.Controle, ' +
    SQL_IF + sPeca + ', 0.000) Pecas, ' +
    SQL_IF + sArea + ', 0.000) AreaM2, ' +
    SQL_IF + sPeso + ', 0.000) PesoKg, ',
    'DATE(vmi.DataHora) Data,  ' + FldDst + ' GGX_Dst, ', //+ FldOri + ', ',
    FldSrc + ' GGX_Src, ',
    '  ',
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'sTipoEstq', True,
    sArea, sPeso, sPeca, 's'),
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'dTipoEstq', True,
    dArea, dPeso, dPeca, 'd'),
    //
    ' ',
    SQL_IF + dPeca + ', 0.000) QtdAntPeca, ' +
    SQL_IF + dArea + ', 0.000) QtdAntArM2, ' +
    SQL_IF + dPeso + ', 0.000) QtdAntPeso, ',
    'vmi.DtCorrApo, vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
    'IF(gg1.Nivel2 <> 0 AND gg2.Tipo_Item>-1, gg2.Tipo_Item, pgt.Tipo_Item) Tipo_Item  ',
    'FROM ' + TMeuDB + '.' + Tabela + ' cab ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '   vmi ON vmi.MovimCod=cab.MovimCod ',
    'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    sggx ON sggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    sgg1 ON sgg1.Nivel1=sggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.gragru2    sgg2 ON sgg2.Nivel2=sgg1.Nivel1 ',
    'LEFT JOIN ' + TMeuDB + '.prdgruptip spgt ON spgt.Codigo=sgg1.PrdGrupTip ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    smed ON smed.Codigo=sgg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    sggc ON sggc.Controle=sggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  sgcc ON sgcc.Codigo=sggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  sgti ON sgti.Controle=sggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou sxco ON sxco.GraGruX=sggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    snv1 ON snv1.Codigo=sxco.CouNiv1   ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    snv2 ON snv2.Codigo=sxco.CouNiv2   ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    dggx ON dggx.Controle=cab.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    dgg1 ON dgg1.Nivel1=dggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.gragru2    dgg2 ON dgg2.Nivel2=dgg1.Nivel1 ',
    'LEFT JOIN ' + TMeuDB + '.prdgruptip dpgt ON dpgt.Codigo=dgg1.PrdGrupTip ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    dmed ON dmed.Codigo=dgg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    dggc ON dggc.Controle=dggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  dgcc ON dgcc.Codigo=dggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  dgti ON dgti.Controle=dggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou dxco ON dxco.GraGruX=dggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    dnv1 ON dnv1.Codigo=dxco.CouNiv1   ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    dnv2 ON dnv2.Codigo=dxco.CouNiv2   ',
    ' ',
    'WHERE (vmi.Empresa=' + Geral.FF0(FEmpresa),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    SQL_PeriodoVS,
  ') OR ',
  '( ',
  'vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  'AND cab.MovimCod IN ( ',
  'SELECT DISTINCT VSMovCod  ',
  'FROM ' + TMeuDB + '.emit  ',
  'WHERE Codigo IN (   ',
  '  SELECT OrigemCodi  ',
  '  FROM ' + TMeuDB + '.pqx  ',
  '  WHERE Tipo=110     ',
  '  ' + SQL_PeriodoPQ,
  ')   ',
  'AND VSMovCod <> 0  ',
  '  ',
  'UNION   ',
  '  ',
  'SELECT DISTINCT VSMovCod  ',
  'FROM ' + TMeuDB + '.pqo  ',
  'WHERE Codigo IN (   ',
  '  SELECT OrigemCodi  ',
  '  FROM ' + TMeuDB + '.pqx  ',
  '  WHERE Tipo=110     ',
  '  ' + SQL_PeriodoPQ,
  ')   ',
  'AND VSMovCod <> 0  ',
  ////
  ') ',
  ') ',
  '']);
  end;
const
  sProcName = '"TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ImportaOpeProcB_Isolada()"';
  //SrcFator = -1;
  DstFator = 1;
  ESTSTabSorc = estsVMI;
var
  Pecas, AreaM2, PesoKg, QtdeSrc, QtdeDst, QtdAntArM2, QtdAntPeso, QtdAntPeca:
  Double;
  Data, DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  SrcTipoEstq, DstTipoEstq, GGXSrc, GGXDst, Codigo, MovimCod, IDSeq1,
  ID_Item, Controle, SrcFator, ClientMO, FornecMO, EntiSitio, Tipo_Item: Integer;
  MovimNiv: TEstqMovimNiv;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  COD_INS_SUBST, SQLx: String;
  MovimID: TEstqMovimID;
  MovimNivInn: TEstqMovimNiv;
begin
  COD_INS_SUBST := '';
  //
  case PsqMovimNiv of
    TEstqmovimNiv.eminSorcCal: SrcFator := 1;
    TEstqmovimNiv.eminDestCal: SrcFator := -1;
    TEstqmovimNiv.eminSorcCur: SrcFator := 1;
    TEstqmovimNiv.eminBaixCurtiVS: SrcFator := -1;
    TEstqmovimNiv.eminDestWEnd: SrcFator := -1; //???
    else
    begin
      SrcFator := -1;
      Geral.MB_Erro('"SrcFator" n�o implementado em ' + sProcName + sLineBreak
      + 'PsqMovimNiv = ' + GetEnumName(TypeInfo(TEstqmovimNiv), Integer(PsqMovimNiv)));
    end;
  end;
  case PsqMovimNiv of
    TEstqmovimNiv.eminSorcCal: SQLx :=
      ParteSQL('vscalcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCal);
    TEstqmovimNiv.eminDestCal: SQLx :=
      ParteSQL('vscaljmp', 'vmi.JmpGGX', 'vmi.GraGruX', '-vmi.QtdGerArM2',
      '-vmi.QtdGerPeso', '-vmi.QtdGerPeca',  'vmi.QtdGerArM2',
      'vmi.QtdGerPeso', 'vmi.QtdGerPeca', TEstqmovimNiv.eminDestCal);
    TEstqmovimNiv.eminSorcCur: SQLx :=
      ParteSQL('vscurcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCur);
    TEstqmovimNiv.eminBaixCurtiVS:
    begin
      // N�o fazer?
      // Vai duplicar a baixa?
      Exit;
    end;
    TEstqmovimNiv.eminDestWEnd:
    begin
      // N�o fazer?
      // Vai duplicar algo?
      Exit;
    end;
    else
    begin
      SQLx := 'SELECT ???? FROM ????';
      Geral.MB_Erro('"MovimNiv" n�o implementado em ' + sProcName);
      //Exit;
    end;
  end;

  UnDmkDAC_PF.AbreMySQLQuery0(QrMC, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_O_P;  ',
  'CREATE TABLE _SPED_EFD_K2XX_O_P  ',
  //
  SQLx,
  '; ',
  ' ',
  'SELECT DISTINCT MovimCod  ',
  'FROM _SPED_EFD_K2XX_O_P ',
  'ORDER BY MovimCod;  ',
  '']);
  //Geral.MB_SQL(Self, QrMC); //Parei aqui
  //
  if EfdIcmsIpi_PF.ErrGrandeza('_SPED_EFD_K2XX_O_P', 'GGx_Src', 'GGX_Dst' ) then
    Exit;
  //
  PB2.Position := 0;
  PB2.Max := QrMC.RecordCount;
  QrMC.First;
  //if QrMC.RecordCount > 0 then
    //Geral.MB_Aviso('DEPRRCAR!!! OpeProcA - 1');
  while not QrMC.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    UnDmkDAC_PF.AbreMySQLQuery0(QrProdRib, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM _SPED_EFD_K2XX_O_P ',
    'WHERE MovimCod=' + Geral.FF0(QrMCMovimCod.Value),
    '']);
    //Geral.MB_SQL(Self, QrProdRib); //Parei aqui!
    QrProdRib.First;
    while not QrProdRib.Eof do
    begin
      MovimID     := TEstqMovimID(QrProdRibMovimID.Value);
      AreaM2      := QrProdRibAreaM2.Value;
      PesoKg      := QrProdRibPesoKg.Value;
      Pecas       := QrProdRibPecas.Value;
      QtdAntArM2  := QrProdRibQtdAntArM2.Value;
      QtdAntPeso  := QrProdRibQtdAntPeso.Value;
      QtdAntPeca  := QrProdRibQtdAntPeca.Value;
      Data        := QrProdRibData.Value;
      SrcTipoEstq := Trunc(QrProdRibsTipoEstq.Value);
      DstTipoEstq := Trunc(QrProdRibdTipoEstq.Value);
      GGXSrc      := QrProdRibGGX_Src.Value;
      GGXDst      := QrProdRibGGX_Dst.Value;
      MovimCod    := QrProdRibMovimCod.Value;
      MovimNiv    := TEstqMovimNiv(QrProdRibMovimNiv.Value);
      Codigo      := QrProdRibCodigo.Value;
      Controle    := QrProdRibControle.Value;
      ID_Item     := Controle;
      //
      DtMovim     := Data;
      DtCorrApo   := QrProdRibDtCorrApo.Value;
      //
      ClientMO    := QrProdRibClientMO.Value;
      FornecMO    := QrProdRibFornecMO.Value;
      EntiSitio   := QrProdRibEntiSitio.Value;
      //
      if (ClientMO =0) or (FornecMO=0) or (EntiSitio=0) then
      begin
        MovimNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
        UnDmkDAC_PF.AbreMySQLQuery0(QrObtCliForSite, Dmod.MyDB, [
        'SELECT vmi.ClientMO, vmi.FornecMO, scc.EntiSitio  ',
        'FROM vsmovits vmi ',
        'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc  ',
        'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo  ',
        'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
        'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
        '']);
        ClientMO  := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
        FornecMO  := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
        EntiSitio  := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
      end;
      //
      //if (Pecas <> 0) and (DtHrFimOpe > 1) then
      if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
      begin
        case SrcTipoEstq of
          0: QtdeSrc := Pecas;
          1: QtdeSrc := AreaM2;
          2: QtdeSrc := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(SrcTipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[SrcTipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrProdRib.SQL.Text);
            //
            QtdeDst := Pecas;
          end;
        end;
        QtdeSrc := QtdeSrc * SrcFator;

        //
        case DstTipoEstq of
          1: QtdeDst := QtdAntArM2;
          2: QtdeDst := QtdAntPeso;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(SrcTipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[SrcTipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrProdRib.SQL.Text);
            //
            QtdeDst := QtdAntPeca;
          end;
        end;
        QtdeDst := QtdeDst * DstFator;
      end else
      begin
        QtdeSrc := 0;
        QtdeDst := 0;
      end;
      if (*TemTwn and*) (QtdeSrc = 0) then
        VerificaTipoEstqQtde(SrcTipoEstq, AreaM2, PesoKg, Pecas,
        GGXSrc, MovimCod, Controle, sProcName);
      if (*TemTwn and*) (QtdeDst = 0) then
        VerificaTipoEstqQtde(DstTipoEstq, AreaM2, PesoKg, Pecas,
        GGXDst, MovimCod, Controle, sProcName);
      //
      if QtdeSrc < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade "Src" n�o pode ser negativa!!! (5)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo), QrProdRib);
      if QtdeDst < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade "Dst" n�o pode ser negativa!!! (6)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), QrProdRib)
      else if (QtdeDst = 0) and (Data > 1) then
      begin
        case MovimID of
          emidEmProcCal: Data := 0;
          emidEmProcCur: Data := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade zerada para OP encerrada!!! (5)', QrProdRib);
        end;
      end;
      //
      //if QrSubPrdOPEmpresa.Value = QrFMOFornecMO.Value then

      if FEmpresa = FornecMO then
        TipoRegSPEDProd := trsp23X
      else
        TipoRegSPEDProd := trsp25X;
      //
      DtHrAberto := Data;
      DtHrAberto := Data;
      case TipoRegSPEDProd of
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod,
          Controle, DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst,
          ClientMO, FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
          FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
          //
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, DtHrAberto,
          DtCorrApo, GGXSrc, QtdeSrc, COD_INS_SUBST, ID_Item, Integer(MovimID),
          Codigo, MovimCod, Controle, ClientMO, FornecMO, EntiSitio,
          OrigemOpeProc, ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
          //
(*        Desabilitar?. PQ Ser� incluido no couro processado e n�o em processo!*)
          Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv, MovimCod,
          IDSeq1, SQL_PeriodoPQ, OrigemOpeProc, ClientMO, FornecMO, EntiSitio);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
          QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
          //
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXSrc,
          QtdeSrc, COD_INS_SUBST, ID_Item,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
          //
(*        Desabilitar?. PQ Ser� incluido no couro processado e n�o em processo!*)
          Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv, MovimCod,
          IDSeq1, SQL_PeriodoPQ, OrigemOpeProc, ClientMO, FornecMO, EntiSitio);

        end;
        else Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(6)');
      end;
      QrProdRib.Next;
    end;
    QrMC.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.InsereB_OrigeProcPQ_Conjunta(
  TipoRegSPEDProd: TTipoRegSPEDProd; MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv; MovimCod, IDSeq1: Integer; SQL_PeriodoPQ: String;
  OrigemOpeProc: TOrigemOpeProc; ClientMO, FornecMO,
  EntiSitio: Integer): Boolean;
const
  Fator = -1;
  OriESTSTabSorc = estsPQx;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_OrigeProcPQ_Conjunta()';
  JmpMovID = 0;
  JmpMovCod = 0;
  JmpNivel1 = 0;
  JmpNivel2 = 0;
  PaiMovID = 0;
  MovCodPai = 0;
  PaiNivel1 = 0;
  PaiNivel2 = 0;
var
  DtMovim, DtCorrApo: TDateTime;
  Codigo, Controle, GraGruX(*, ClientMO, FornecMO, EntiSitio*): Integer;
  MyQtd, Qtde: Double;
begin
  Result := True;
  VS_EFD_ICMS_IPI.ReopenVSOriPQx(QrOri, MovimCod, SQL_PeriodoPQ, False);
  //
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  while not QrOri.Eof do
  begin
    Codigo    := QrOri.FieldByName('OrigemCodi').AsInteger;
    Controle  := QrOri.FieldByName('OrigemCtrl').AsInteger;
    GraGruX   := UnPQx.ObtemGGXdeInsumo(QrOri.FieldByName('Insumo').AsInteger);
    MyQtd     := QrOri.FieldByName('Peso').AsFloat;
    Qtde      := MyQtd * Fator;
    DtMovim   := QrOri.FieldByName('DataX').AsDateTime;
    DtCorrApo := QrOri.FieldByName('DtCorrApo').AsDateTime;
    //
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (5)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      //'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'ID Item: ' + Geral.FF0(Controle) + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Peso: ' + Geral.FFT(MyQtd, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    if Qtde > 0 then
    begin
      case TipoRegSPEDProd of
        trsp29X:
        begin
          // K292 - Insumos consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
          MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          TEstqSPEDTabSorc.estsPQX, MeAviso);
        end;
        trsp30X:
        begin
          // K302 - Insumos consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
          MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          TEstqSPEDTabSorc.estsPQX, MeAviso);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(7)');
      end;
    end;
    //
    QrOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.InsereB_OrigeProcPQ_Isolada(
  TipoRegSPEDProd: TTipoRegSPEDProd; MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv; MovimCod, IDSeq1: Integer; SQL_PeriodoPQ: String;
  OrigemOpeProc: TOrigemOpeProc; ClientMO, FornecMO,
  EntiSitio: Integer; DtHrAberto: TDateTime): Boolean;
const
  Fator = -1;
  OriESTSTabSorc = estsPQx;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_OrigeProcPQ_Isolada()';
  JmpMovID = 0;
  JmpMovCod = 0;
  JmpNivel1 = 0;
  JmpNivel2 = 0;
  PaiMovID = 0;
  MovCodPai = 0;
  PaiNivel1 = 0;
  PaiNivel2 = 0;
  COD_INS_SUBST = '';
var
  DtMovim, DtCorrApo: TDateTime;
  Codigo, Controle, GraGruX(*, ClientMO, FornecMO, EntiSitio*): Integer;
  MyQtd, Qtde: Double;
begin
  Result := True;
  VS_EFD_ICMS_IPI.ReopenVSOriPQx(QrOri, MovimCod, SQL_PeriodoPQ, False);
  //
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  while not QrOri.Eof do
  begin
    Codigo    := QrOri.FieldByName('OrigemCodi').AsInteger;
    Controle  := QrOri.FieldByName('OrigemCtrl').AsInteger;
    GraGruX   := UnPQx.ObtemGGXdeInsumo(QrOri.FieldByName('Insumo').AsInteger);
    MyQtd     := QrOri.FieldByName('Peso').AsFloat;
    Qtde      := MyQtd * Fator;
    DtMovim   := QrOri.FieldByName('DataX').AsDateTime;
    DtCorrApo := QrOri.FieldByName('DtCorrApo').AsDateTime;
    //
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (6)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      //'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'ID Item: ' + Geral.FF0(Controle) + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Peso: ' + Geral.FFT(MyQtd, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    if Qtde > 0 then
    begin
      case TipoRegSPEDProd of
        trsp23X:
        begin
(*
          // K292 - Insumos consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
          MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          TEstqSPEDTabSorc.estsPQX, MeAviso);
*)
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, DtHrAberto,
          DtCorrApo, GraGruX, Qtde, COD_INS_SUBST, Controle, Integer(MovimID),
          Codigo, MovimCod, Controle, ClientMO, FornecMO, EntiSitio,
          OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
        trsp25X:
        begin
(*
          // K302 - Insumos consumidos
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
          MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          TEstqSPEDTabSorc.estsPQX, MeAviso);
*)
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GraGruX, Qtde, COD_INS_SUBST,
          Controle, Integer(MovimID), Codigo, MovimCod, Controle, ClientMO,
          FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc, FTipoPeriodoFiscal,
          MeAviso);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(8)');
      end;
    end;
    //
    QrOri.Next;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.InsereB_ProducaoVer2_Fast(TabCab:
  String; TargetMovimID, PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; PsqMovimNiv:
   TEstqMovimNiv; OrigemOpeProc: TOrigemOpeProc; FormaInsercaoItemProducao:
   TFormaInsercaoItemProducao; IncluiPQX: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_ProducaoVer2_Fast()';
  DstFator  = 1;
  OriFator  = 1;
var
  IDSeq1_K290, IDSeq1_K300: Integer;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  //
  MovimID: TEstqMovimID;
  Codigo, MovimCod, ClientMO, FornecMO, EntiSitio, CtrlDst, GGXDst, TipoEstqDst,
  TipoEstqOri, CtrlOri, GGXOri, CodDocOP: Integer;
  DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  OriESTSTabSorc: TEstqSPEDTabSorc;
  //
  MovimNiv: TEstqMovimNiv;
  MovimTwn, CtrlPsq, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
  MovCodPai, PaiNivel1, PaiNivel2, Empresa, ForneceMO, Controle, GraGruX,
  GerMovID, GerNivel1, GerNivel2, GerGGX, Tipo_Item: Integer;
  InseriuPQ, InsumoSemProducao: Boolean;
  Produziu: TProduziuVS;
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdAntPeca, QtdAntPeso, QtdAntArM2: Double;
  InsercaoItemProducaoConjunta: TInsercaoItemProducaoConjunta;
  //
  procedure AvisoFip(ProcNome: String);
  begin
    Geral.MB_Aviso('FormaInsercaoItemProducao n�o definida em ' + sProcName +
    '.' + ProcNome);
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFather, DModG.MyPID_DB, [
  'SELECT DISTINCT MovCodPai ',
  'FROM  ' + FSEII_IMEcs,
  'ORDER BY MovCodPai',
  '']);
  //*Geral.MB_SQL(Self, QrFather);
  PB2.Position := 0;
  PB2.Max := QrFather.RecordCount;
  QrFather.First;
  while not QrFather.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    //
    InseriuPQ := False;
    IDSeq1_K290 := -1;
    IDSeq1_K300 := -1;
    //
    case TargetMovimID of
      TEStqMovimID.emidEmOperacao,
      TEStqMovimID.emidEmProcWE,
      TEStqMovimID.emidEmProcSP,
      TEStqMovimID.emidEmReprRM:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrIMECs, DModG.MyPID_DB, [
        'SELECT * ',
        'FROM  ' + FSEII_IMEcs,
        'WHERE MovCodPai=' + Geral.FF0(QrFatherMovCodPai.Value),
        //'ORDER BY '
        'LIMIT 1 ',
        '']);
      end;
      TEStqMovimID.emidEmProcCal,
      TEStqMovimID.emidCaleado,
      TEStqMovimID.emidEmProcCur,
      TEStqMovimID.emidCurtido:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrIMECs, DModG.MyPID_DB, [
        'SELECT * ',
        'FROM  ' + FSEII_IMEcs,
        'WHERE MovCodPai=' + Geral.FF0(QrFatherMovCodPai.Value),
        '']);
        //*Geral.MB_SQL(Self, QrIMECs);
      end;
      else begin
        Geral.MB_Erro(
        GetEnumName(TypeInfo(TEStqMovimID), Integer(TargetMovimID)) +
        ' indefinido em ' + sprocName + ' (1)');
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrIMECs, DModG.MyPID_DB, [
        'SELECT * ',
        'FROM  ' + FSEII_IMEcs,
        'WHERE MovCodPai=' + Geral.FF0(QrFatherMovCodPai.Value),
        '']);
      end;
    end;
    //Geral.MB_SQL(Self, QrIMECs);
//  At� aqui � super r�pido!
    while not QrIMECs.Eof do
    begin
      InsercaoItemProducaoConjunta := iipcIndef;
      CodDocOP  := QrIMECsMovCodPai.Value;
      UnDmkDAC_PF.AbreMySQLQuery0(QrProc, Dmod.MyDB, [
      'SELECT DtHrAberto, DtHrFimOpe ',
      'FROM ' + LowerCase(TabCab),
      'WHERE MovimCod=' + Geral.FF0(CodDocOP),
      '']);
      //
{ // A
      //case PsqMovimID of
      case TargetMovimID of
        TEStqMovimID.emidEmProcCal:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, QrVSRibOriIMEI,
          (*QrVSRibOriPallet*)nil, (*QrVSRibDst*)nil, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrIMECsCodigo.Value*)PaiNivel1,
          (*QrIMECsMovimCod.Value*) MovCodPai, (*QrIMECsTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto,
          emidEmProcCal, emidCaleado, emidCaleado,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCalInn, eminSorcCal, eminDestCal);
          InsercaoItemProducaoConjunta := iipcUniAnt;
        end;
        TEStqMovimID.emidCaleado:
        begin
          //  Couro caleado tirado direto do caleiro
          if QrIMECsJmpNivel2.Value = 0 then
          begin
            JmpMovID  := 0;
            JmpMovCod := 0;
            JmpNivel1 := 0;
            JmpNivel2 := 0;
            PaiMovID  := QrIMECsMovimID.Value;
            MovCodPai := QrIMECsMovCodPai.Value;
            PaiNivel1 := QrIMECsCodigo.Value;
            PaiNivel2 := 0;
            InsercaoItemProducaoConjunta := iipcGemeos;
          end else
          begin
            CtrlPsq := QrIMECsJmpNivel2.Value;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrJmpA, Dmod.MyDB, [
            'SELECT *  ',
            'FROM vsmovits ',
            'WHERE Controle=' + Geral.FF0(CtrlPsq),
            '']);
            //
            CtrlPsq   := QrJmpASrcNivel2.Value;
            JmpMovID  := QrJmpAMovimID.Value;
            JmpMovCod := QrJmpAMovimCod.Value;
            JmpNivel1 := QrJmpACodigo.Value;
            JmpNivel2 := QrJmpAControle.Value;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrJmpB, Dmod.MyDB, [
            'SELECT *  ',
            'FROM vsmovits ',
            'WHERE Controle=' + Geral.FF0(CtrlPsq),
            '']);
            //
            PaiMovID  := QrJmpBMovimID.Value;
            MovCodPai := QrJmpBMovimCod.Value;
            PaiNivel1 := QrJmpBCodigo.Value;
            PaiNivel2 := QrJmpBControle.Value;
            InsercaoItemProducaoConjunta := iipcIndireta;
          end;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, (*QrVSRibOriIMEI*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst, QrVSRibInd,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrIMECsCodigo.Value*)PaiNivel1,
          (*QrIMECsMovimCod.Value*) MovCodPai, (*QrIMECsTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto,
          emidEmProcCal, emidCaleado, emidCaleado,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCalInn, eminSorcCal, eminDestCal);
        end;
        TEStqMovimID.emidEmProcCur:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, QrVSRibOriIMEI,
          (*QrVSRibOriPallet*)nil, (*QrVSRibDst*)nil, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrIMECsCodigo.Value*)PaiNivel1,
          (*QrIMECsMovimCod.Value*) MovCodPai, (*QrIMECsTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto,
          emidEmProcCur, emidIndsVS, emidEmProcCur,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCurInn, eminSorcCur, eminDestCur);
          InsercaoItemProducaoConjunta := iipcUniAnt;
        end;
        TEStqMovimID.emidCurtido:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, (*QrVSRibOriIMEI*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst, QrVSRibInd,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrIMECsCodigo.Value*)PaiNivel1,
          (*QrIMECsMovimCod.Value*) MovCodPai, (*QrIMECsTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmProcCur, emidIndsVS, emidEmProcCur,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCurInn, eminSorcCur, eminDestCur);
          InsercaoItemProducaoConjunta := iipcUniGer;
          //Geral.MB_SQL(Self, QrVSRibAtu);
          //Geral.MB_SQL(Self, QrVSRibDst);
          //Geral.MB_SQL(Self, QrVSRibInd);
        end;
        TEStqMovimID.emidEmOperacao:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, (*QrVSRibOriIMEI*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrVSRibCabCodigo.Value*)PaiNivel1,
          (*QrVSRibCabMovimCod.Value*)MovCodPai,(*QrVSRibCabTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmOperacao, emidEmOperacao, emidEmOperacao,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmOperInn, eminSorcOper, eminDestOper);
          InsercaoItemProducaoConjunta := iipcGemeos;
        end;
        TEStqMovimID.emidEmProcWE:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, (*QrVSRibOriIMEI*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrVSRibCabCodigo.Value*)PaiNivel1,
          (*QrVSRibCabMovimCod.Value*)MovCodPai,(*QrVSRibCabTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmProcWE, emidFinished, emidEmProcWE,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmWEndInn, eminSorcWEnd, eminDestWEnd);
          InsercaoItemProducaoConjunta := iipcGemeos;
          //Geral.MB_SQL(Self, QrVSRibAtu);
          //Geral.MB_SQL(Self, QrVSRibDst);
          //Geral.MB_SQL(Self, QrVSRibInd);
        end;
        TEStqMovimID.emidEmProcSP:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, (*QrVSRibOriIMEI*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrVSRibCabCodigo.Value*)PaiNivel1,
          (*QrVSRibCabMovimCod.Value*)MovCodPai,(*QrVSRibCabTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmProcSP, emidEmProcSP, emidEmProcSP,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmPSPInn, eminSorcPSP, eminDestPSP);
          InsercaoItemProducaoConjunta := iipcGemeos;
        end;
        TEStqMovimID.emidEmReprRM:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, (*QrVSRibOriIMEI*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrVSRibCabCodigo.Value*)PaiNivel1,
          (*QrVSRibCabMovimCod.Value*)MovCodPai,(*QrVSRibCabTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmReprRM, emidEmReprRM, emidEmReprRM,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmRRMInn, eminSorcRRM, eminDestRRM);
          InsercaoItemProducaoConjunta := iipcGemeos;
        end;
        else Geral.MB_Erro('MovimID ' + Geral.FF0(Integer(TargetMovimID)) +
        ' n�o implementado em ' + sProcName + ' (2)');
      end;
{} // B
      //
      //Geral.MB_SQL(Self, QrVSRibDst);
      case InsercaoItemProducaoConjunta of
        //iipcIndef=0,
        iipcGemeos:
        begin
(*
          2019-01-27 - Fazer aqui!!
          Se QrVSRibDst n�o tem itens, quer dizer que tem um processo em aberto,
          mas sem nenhum artigo pronto ainda! Nesse caso tem que ver se j� tem
          consumo de insumos! Tem que criar o 290 sem 291 mas com 292 de couro
          e de insumos!
*)
          if (QrVSRibAtu.RecordCount > 0) and (QrVSRibDst.RecordCount = 0) then
          begin
            MovimID     := TEstqMovimID(QrVSRibAtuMovimID.Value);
            MovimCod    := QrVSRibAtuMovimCod.Value;
            Codigo      := QrVSRibAtuCodigo.Value;
            DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
            DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
            DtMovim     := Trunc(QrVSRibAtuDataHora.Value);
            DtCorrApo   := Trunc(QrVSRibAtuDtCorrApo.Value);
            Empresa     := QrVSRibAtuEmpresa.Value;
            ForneceMO   := QrVSRibAtuFornecMO.Value;
            Controle    := QrVSRibAtuControle.Value;
            GraGruX     := QrVSRibAtuGraGruX.Value;
            MovimNiv    := TEstqMovimNiv(QrVSRibAtuMovimNiv.Value);
            MovimTwn    := QrVSRibAtuMovimTwn.Value;
            AreaM2      := 0; // N�o produziu nada!
            PesoKg      := 0; // N�o produziu nada!
            Pecas       := 0; // N�o produziu nada!
            Produziu    := TProduziuVS.przvsNao;
            // Ver se tem consumo de insumos...
            // Se tiver, lan�ar.
            ReopenOriPQx(MovimCod, FSQL_PeriodoPQ, False);
            InsumoSemProducao := QrOri.RecordCount > 0;
            if InsumoSemProducao then
            begin
              MeAviso.Lines.Add('MovimCod ' + Geral.FF0(QrVSRibAtuMovimCod.Value) +
              ' sem artigos destino mas com insumos consumidos!');
              //
              VerificaSeTemGraGruX(3299, GraGruX, sProcName, QrVSRibAtu);
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaGemeos(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, Tipo_Item, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaGemeos(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ, Produziu);
                else
                  AvisoFip('Gemeos');
              end;
            end;
          end else
          //// Fim Processo sem produ��o, mas com consumo de insumos
          ////
          ///////////////////////    Houve produ��o    /////////////////////////
          begin
            QrVSRibDst.First;
            while not QrVSRibDst.Eof do
            begin
              if (QrVSRibDstDataHora.Value >= FDiaIni)
              and (QrVSRibDstDataHora.Value < FDiaPos) then
              begin
                MovimID     := TEstqMovimID(QrVSRibDstMovimID.Value);
                MovimCod    := QrVSRibDstMovimCod.Value;
                Codigo      := QrVSRibDstCodigo.Value;
                DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
                DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
                DtMovim     := Trunc(QrVSRibDstDataHora.Value);
                DtCorrApo   := Trunc(QrVSRibDstDtCorrApo.Value);
                Empresa     := QrVSRibDstEmpresa.Value;
                ForneceMO   := QrVSRibDstFornecMO.Value;
                Controle    := QrVSRibDstControle.Value;
                GraGruX     := QrVSRibDstGraGruX.Value;
                MovimNiv    := TEstqMovimNiv(QrVSRibDstMovimNiv.Value);
                MovimTwn    := QrVSRibDstMovimTwn.Value;
                AreaM2      := QrVSRibDstAreaM2.Value;
                PesoKg      := QrVSRibDstPesoKg.Value;
                Pecas       := QrVSRibDstPecas.Value;
                Produziu    := TProduziuVS.przvsSim;
                VerificaSeTemGraGruX(3343, GraGruX, sProcName, QrVSRibDst);
                //
                case FormaInsercaoItemProducao of
                  fipIsolada:
                    InsereItemProducaoIsoladaGemeos(DtHrAberto, DtHrFimOpe,
                    DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                    Codigo, Controle, Tipo_Item, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                    Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                    JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                    IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ);
                  fipConjunta:
                    InsereItemProducaoConjuntaGemeos(DtHrAberto, DtHrFimOpe,
                    DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                    Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                    Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                    JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                    IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ, Produziu);
                  else
                    AvisoFip('Gemeos');
                end;
              end;
              //
              QrVSRibDst.Next;
            end;
          end;
        end;
        iipcIndireta:
        begin
          QrVSRibInd.First;
          while not QrVSRibInd.Eof do
          begin
            if (QrVSRibIndDataHora.Value >= FDiaIni)
            and (QrVSRibIndDataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibIndMovimID.Value);
              MovimCod    := QrVSRibIndMovimCod.Value;
              Codigo      := QrVSRibIndCodigo.Value;
              DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibIndDataHora.Value);
              DtCorrApo   := Trunc(QrVSRibIndDtCorrApo.Value);
              Empresa     := QrVSRibIndEmpresa.Value;
              ForneceMO   := QrVSRibIndFornecMO.Value;
              Controle    := QrVSRibIndControle.Value;
              GraGruX     := QrVSRibIndGraGruX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibIndMovimNiv.Value);
              MovimTwn    := QrVSRibIndMovimTwn.Value;
              AreaM2      := QrVSRibIndAreaM2.Value;
              PesoKg      := QrVSRibIndPesoKg.Value;
              Pecas       := QrVSRibIndPecas.Value;
              GerMovID    := QrVSRibIndSrcMovID.Value;
              GerNivel1   := QrVSRibIndSrcNivel1.Value;
              GerNivel2   := QrVSRibIndSrcNivel2.Value;
              GerGGX      := QrVSRibIndJmpGGX.Value;
              QtdGerPeca  := QrVSRibIndQtdGerPeca.Value;
              QtdGerPeso  := QrVSRibIndQtdGerPeso.Value;
              QtdGerArM2  := QrVSRibIndQtdGerArM2.Value;
              if GerGGX = 0 then
              begin
                //Geral.MB_Erro('MovCodPai = ' + Geral.FF0(MovCodPai));
                GerGGX := VS_PF.ObtemJmpGGXdeMovCodPai(MovCodPai);
                if MeAviso <> nil then
                  MeAviso.Lines.Add('"GerGGX" obtido do MovCodPai = ' +
                    Geral.FF0(MovCodPai));
(*
                if MeAviso <> nil then
                  MeAviso.Lines.Add('"GerGGX" indefinido para GerMovID = ' +
                  Geral.FF0(GerMovID) + ' e GerNivel1 = ' + Geral.FF0(GerNivel1));
                VS_PF.AtualizaJmpGGX(TEstqMovimID(GerMovID), GerNivel1);
                UnDmkDAC_PF.AbreQuery(QrVSRibInd, Dmod.MyDB);
                GerGGX := QrVSRibIndJmpGGX.Value;
                if MeAviso <> nil then
                begin
                  if GerGGX = 0 then
                    MeAviso.Lines.Add('"GerGGX" nao corrigido para GerMovID = ' +
                    Geral.FF0(GerMovID) + ' e GerNivel1 = ' + Geral.FF0(GerNivel1))
                  else
                    MeAviso.Lines.Add('"GerGGX" corrigido para GerMovID = ' +
                    Geral.FF0(GerMovID) + ' e GerNivel1 = ' + Geral.FF0(GerNivel1));
                end;
*)
              end;
              VerificaSeTemGraGruX(3424, GraGruX, sProcName, QrVSRibInd);
              VerificaSeTemGraGruX(3425, GerGGX, sProcName, QrVSRibInd);
              //fazer pelo JmpGGX! sem
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaIndireta(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  GerMovID, GerNivel1, GerNivel2, GerGGX, QtdGerPeca,
                  QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300, IncluiPQX,
                  InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaIndireta(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  GerMovID, GerNivel1, GerNivel2, GerGGX, QtdGerPeca,
                  QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300, IncluiPQX,
                  InseriuPQ);
                else
                  AvisoFip('Indireta');
              end;
            end;
            //
            QrVSRibInd.Next;
          end;
        end;
        iipcUniAnt:
        begin
          //Geral.MB_SQL(Self, QrVSRibOriIMEI);
          QrVSRibOriIMEI.First;
          while not QrVSRibOriIMEI.Eof do
          begin
            if (QrVSRibOriIMEIDataHora.Value >= FDiaIni)
            and (QrVSRibOriIMEIDataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibOriIMEIMovimID.Value);
              MovimCod    := QrVSRibOriIMEIMovimCod.Value;
              Codigo      := QrVSRibOriIMEICodigo.Value;
              DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibOriIMEIDataHora.Value);
              DtCorrApo   := Trunc(QrVSRibOriIMEIDtCorrApo.Value);
              Empresa     := QrVSRibOriIMEIEmpresa.Value;
              ForneceMO   := QrVSRibOriIMEIFornecMO.Value;
              Controle    := QrVSRibOriIMEIControle.Value;
              GGXOri      := QrVSRibOriIMEIRmsGGX.Value;
              GGXDst      := QrVSRibOriIMEIDstGGX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibOriIMEIMovimNiv.Value);
              MovimTwn    := QrVSRibOriIMEIMovimTwn.Value;
              AreaM2      := QrVSRibOriIMEIAreaM2.Value;
              PesoKg      := QrVSRibOriIMEIPesoKg.Value;
              Pecas       := QrVSRibOriIMEIPecas.Value;
              QtdAntPeca  := QrVSRibOriIMEIQtdAntPeca.Value;
              QtdAntPeso  := QrVSRibOriIMEIQtdAntPeso.Value;
              QtdAntArM2  := QrVSRibOriIMEIQtdAntArM2.Value;
              //
              VerificaSeTemGraGruX(3485, GGXOri, sProcName, QrVSRibOriIMEI);
              VerificaSeTemGraGruX(3486, GGXDst, sProcName, QrVSRibOriIMEI);
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaUniAnt(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdAntPeca, QtdAntPeso, QtdAntArM2, IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaUniAnt(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdAntPeca, QtdAntPeso, QtdAntArM2, IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                else
                  AvisoFip('UnidAnt');
              end;
            end;
            //
            QrVSRibOriIMEI.Next;
          end;
        end;
        iipcUniGer:
        begin
          QrVSRibInd.First;
          while not QrVSRibInd.Eof do
          begin
            if (QrVSRibIndDataHora.Value >= FDiaIni)
            and (QrVSRibIndDataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibIndMovimID.Value);
              MovimCod    := QrVSRibIndMovimCod.Value;
              Codigo      := QrVSRibIndCodigo.Value;
              DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibIndDataHora.Value);
              DtCorrApo   := Trunc(QrVSRibIndDtCorrApo.Value);
              Empresa     := QrVSRibIndEmpresa.Value;
              ForneceMO   := QrVSRibIndFornecMO.Value;
              Controle    := QrVSRibIndControle.Value;
              GGXOri      := QrVSRibIndRmsGGX.Value;
              GGXDst      := QrVSRibIndJmpGGX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibIndMovimNiv.Value);
              MovimTwn    := QrVSRibIndMovimTwn.Value;
              AreaM2      := QrVSRibIndAreaM2.Value;
              PesoKg      := QrVSRibIndPesoKg.Value;
              Pecas       := QrVSRibIndPecas.Value;
              QtdGerPeca  := Pecas;
              QtdGerPeso  := PesoKg;
              QtdGerArM2  := AreaM2;
              //
              VerificaSeTemGraGruX(3542, GGXOri, sProcName, QrVSRibInd);
              VerificaSeTemGraGruX(3543, GGXDst, sProcName, QrVSRibInd);
              //
              JmpMovID    := QrVSRibIndJmpMovID.Value;
              JmpNivel1   := QrVSRibIndJmpNivel1.Value;
              //if MovimID = TEstqMovimID.emidCurtido then
              if (MovimID = TEstqMovimID.emidCurtido) and
              (TEstqMovimID(JmpMovID) = TEstqMovimID.emidCurtido) then
              begin
                MovimID := TEstqMovimID(JmpMovID);
                if JmpNivel1 <> 0 then
                begin
                  UnDmkDAC_PF.AbreMySQLQuery0(QrCab34, Dmod.MyDB, [
                  'SELECT MovimCod',
                  'FROM vscurjmp',
                  'WHERE Codigo=' + Geral.FF0(JmpNivel1),
                  '']);
                  MovimCod := QrCab34MovimCod.Value;
                end else
                begin
                  MovimCod := 0;
                  Geral.MB_Aviso('MovimCod indefinido em ' + sProcName + ' (3)');
                end;
              end;
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaUniGer(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdGerPeca, QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaUniGer(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdAntPeca, QtdAntPeso, QtdAntArM2, IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                else
                  AvisoFip('UnidGer');
              end;
            end;
            //
            QrVSRibInd.Next;
          end;
        end;
        iipcSimples:
        begin
          // Gera��o!
          QrVSRibDst.First;
          while not QrVSRibDst.Eof do
          begin
            if (QrVSRibDstDataHora.Value >= FDiaIni)
            and (QrVSRibDstDataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibDstMovimID.Value);
              MovimCod    := QrVSRibDstMovimCod.Value;
              Codigo      := QrVSRibDstCodigo.Value;
              DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibDstDataHora.Value);
              DtCorrApo   := Trunc(QrVSRibDstDtCorrApo.Value);
              Empresa     := QrVSRibDstEmpresa.Value;
              ForneceMO   := QrVSRibDstFornecMO.Value;
              Controle    := QrVSRibDstControle.Value;
              GraGruX     := QrVSRibDstGraGruX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibDstMovimNiv.Value);
              MovimTwn    := QrVSRibDstMovimTwn.Value;
              AreaM2      := QrVSRibDstAreaM2.Value;
              PesoKg      := QrVSRibDstPesoKg.Value;
              Pecas       := QrVSRibDstPecas.Value;
              //
              VerificaSeTemGraGruX(3618, GraGruX, sProcName, QrVSRibDst);
              case FormaInsercaoItemProducao of
                (*
                fipIsolada:
                  InsereItemProducaoIsoladaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdGerPeca, QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300,
                  InseriuPQ);
                *)
                fipConjunta:
                  InsereItemProducaoConjuntaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, PaiMovID, MovCodPai, PaiNivel1,
                  PaiNivel2, (*Fator*)1, TEstqSPEDTabSorc.estsVMI,
                  TSPED_EFD_GerBxa.segbGera, IncluiPQX, IDSeq1_K290, IDSeq1_K300);
                else
                  AvisoFip('Simples');
              end;
            end;
            //
            QrVSRibDst.Next;
          end;
          //
          // Baixa!
          QrVSRibOriIMEI.First;
          while not QrVSRibOriIMEI.Eof do
          begin
            if (QrVSRibOriIMEIDataHora.Value >= FDiaIni)
            and (QrVSRibOriIMEIDataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibOriIMEIMovimID.Value);
              MovimCod    := QrVSRibOriIMEIMovimCod.Value;
              Codigo      := QrVSRibOriIMEICodigo.Value;
              DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibOriIMEIDataHora.Value);
              DtCorrApo   := Trunc(QrVSRibOriIMEIDtCorrApo.Value);
              Empresa     := QrVSRibOriIMEIEmpresa.Value;
              ForneceMO   := QrVSRibOriIMEIFornecMO.Value;
              Controle    := QrVSRibOriIMEIControle.Value;
              GraGruX     := QrVSRibAtuGraGruX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibOriIMEIMovimNiv.Value);
              MovimTwn    := QrVSRibOriIMEIMovimTwn.Value;
              AreaM2      := QrVSRibOriIMEIAreaM2.Value;
              PesoKg      := QrVSRibOriIMEIPesoKg.Value;
              Pecas       := QrVSRibOriIMEIPecas.Value;
              //
              VerificaSeTemGraGruX(3669, GraGruX, sProcName, QrVSRibOriIMEI);
              //
              case FormaInsercaoItemProducao of
                (*
                fipIsolada:
                  InsereItemProducaoIsoladaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdGerPeca, QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300,
                  InseriuPQ);
                *)
                fipConjunta:
                  InsereItemProducaoConjuntaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, PaiMovID, MovCodPai, PaiNivel1,
                  PaiNivel2, (*Fator*)-1, TEstqSPEDTabSorc.estsVMI,
                  TSPED_EFD_GerBxa.segbBaixa, IncluiPQX, IDSeq1_K290, IDSeq1_K300);
                else
                  AvisoFip('Simples');
              end;
            end;
            //
            QrVSRibOriIMEI.Next;
          end;
          //baixa !!!!
(*
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        //MovimCod
        MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
*)
        //
        end;
        else begin
          Geral.MB_Erro(GetEnumName(TypeInfo(TInsercaoItemProducaoConjunta),
          Integer(InsercaoItemProducaoConjunta)) + ' indefinido em ' +
          sprocName + ' (4)');
        end;
      end;
{} // Fim
      QrIMECs.Next;
    end;
    QrFather.Next;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_b.InsereB_ProducaoVer2_Slow(
  TabCab: String; TargetMovimID, PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID;
  PsqMovimNiv: TEstqMovimNiv; OrigemOpeProc: TOrigemOpeProc;
  FormaInsercaoItemProducao: TFormaInsercaoItemProducao; IncluiPQx: Boolean);
begin
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereB_ProducaoVer2_Fast()';
  DstFator  = 1;
  OriFator  = 1;
var
  IDSeq1_K290, IDSeq1_K300: Integer;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  //
  MovimID: TEstqMovimID;
  Codigo, MovimCod, ClientMO, FornecMO, EntiSitio, CtrlDst, GGXDst, TipoEstqDst,
  TipoEstqOri, CtrlOri, GGXOri, CodDocOP: Integer;
  DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  OriESTSTabSorc: TEstqSPEDTabSorc;
  //
  MovimNiv: TEstqMovimNiv;
  MovimTwn, CtrlPsq, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
  MovCodPai, PaiNivel1, PaiNivel2, Empresa, ForneceMO, Controle, GraGruX,
  GerMovID, GerNivel1, GerNivel2, GerGGX, Tipo_Item: Integer;
  InseriuPQ, InsumoSemProducao: Boolean;
  Produziu: TProduziuVS;
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdAntPeca, QtdAntPeso, QtdAntArM2: Double;
  InsercaoItemProducaoConjunta: TInsercaoItemProducaoConjunta;
  //
  procedure AvisoFip(ProcNome: String);
  begin
    Geral.MB_Aviso('FormaInsercaoItemProducao n�o definida em ' + sProcName +
    '.' + ProcNome);
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFather, DModG.MyPID_DB, [
  'SELECT DISTINCT MovCodPai ',
  'FROM  ' + FSEII_IMEcs,
  'ORDER BY MovCodPai',
  '']);
  //*Geral.MB_SQL(Self, QrFather);
  PB2.Position := 0;
  PB2.Max := QrFather.RecordCount;
  QrFather.First;
  while not QrFather.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    //
    InseriuPQ := False;
    IDSeq1_K290 := -1;
    IDSeq1_K300 := -1;
    //
    case TargetMovimID of
      TEStqMovimID.emidEmOperacao,
      TEStqMovimID.emidEmProcWE,
      TEStqMovimID.emidEmProcSP,
      TEStqMovimID.emidEmReprRM:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrIMECs, DModG.MyPID_DB, [
        'SELECT * ',
        'FROM  ' + FSEII_IMEcs,
        'WHERE MovCodPai=' + Geral.FF0(QrFatherMovCodPai.Value),
        //'ORDER BY '
        'LIMIT 1 ',
        '']);
      end;
      TEStqMovimID.emidEmProcCal,
      TEStqMovimID.emidCaleado,
      TEStqMovimID.emidEmProcCur,
      TEStqMovimID.emidCurtido:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrIMECs, DModG.MyPID_DB, [
        'SELECT * ',
        'FROM  ' + FSEII_IMEcs,
        'WHERE MovCodPai=' + Geral.FF0(QrFatherMovCodPai.Value),
        '']);
        //*Geral.MB_SQL(Self, QrIMECs);
      end;
      else begin
        Geral.MB_Erro(
        GetEnumName(TypeInfo(TEStqMovimID), Integer(TargetMovimID)) +
        ' indefinido em ' + sprocName + ' (1)');
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrIMECs, DModG.MyPID_DB, [
        'SELECT * ',
        'FROM  ' + FSEII_IMEcs,
        'WHERE MovCodPai=' + Geral.FF0(QrFatherMovCodPai.Value),
        '']);
      end;
    end;
    //Geral.MB_SQL(Self, QrIMECs);
//  At� aqui � super r�pido!
    while not QrIMECs.Eof do
    begin
      InsercaoItemProducaoConjunta := iipcIndef;
      CodDocOP  := QrIMECsMovCodPai.Value;
      UnDmkDAC_PF.AbreMySQLQuery0(QrProc, Dmod.MyDB, [
      'SELECT DtHrAberto, DtHrFimOpe ',
      'FROM ' + LowerCase(TabCab),
      'WHERE MovimCod=' + Geral.FF0(CodDocOP),
      '']);
      //
      //case PsqMovimID of
      case TargetMovimID of
        TEStqMovimID.emidEmProcCal:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, QrVSRibOriIMEI,
          (*QrVSRibOriPallet*)nil, (*QrVSRibDst*)nil, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrIMECsCodigo.Value*)PaiNivel1,
          (*QrIMECsMovimCod.Value*) MovCodPai, (*QrIMECsTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto,
          emidEmProcCal, emidCaleado, emidCaleado,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCalInn, eminSorcCal, eminDestCal);
          InsercaoItemProducaoConjunta := iipcUniAnt;
        end;
        TEStqMovimID.emidCaleado:
        begin
          //  Couro caleado tirado direto do caleiro
          if QrIMECsJmpNivel2.Value = 0 then
          begin
            JmpMovID  := 0;
            JmpMovCod := 0;
            JmpNivel1 := 0;
            JmpNivel2 := 0;
            PaiMovID  := QrIMECsMovimID.Value;
            MovCodPai := QrIMECsMovCodPai.Value;
            PaiNivel1 := QrIMECsCodigo.Value;
            PaiNivel2 := 0;
            InsercaoItemProducaoConjunta := iipcGemeos;
          end else
          begin
            CtrlPsq := QrIMECsJmpNivel2.Value;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrJmpA, Dmod.MyDB, [
            'SELECT *  ',
            'FROM vsmovits ',
            'WHERE Controle=' + Geral.FF0(CtrlPsq),
            '']);
            //
            CtrlPsq   := QrJmpASrcNivel2.Value;
            JmpMovID  := QrJmpAMovimID.Value;
            JmpMovCod := QrJmpAMovimCod.Value;
            JmpNivel1 := QrJmpACodigo.Value;
            JmpNivel2 := QrJmpAControle.Value;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrJmpB, Dmod.MyDB, [
            'SELECT *  ',
            'FROM vsmovits ',
            'WHERE Controle=' + Geral.FF0(CtrlPsq),
            '']);
            //
            PaiMovID  := QrJmpBMovimID.Value;
            MovCodPai := QrJmpBMovimCod.Value;
            PaiNivel1 := QrJmpBCodigo.Value;
            PaiNivel2 := QrJmpBControle.Value;
            InsercaoItemProducaoConjunta := iipcIndireta;
          end;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, (*QrVSRibOriIMEI*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst, QrVSRibInd,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrIMECsCodigo.Value*)PaiNivel1,
          (*QrIMECsMovimCod.Value*) MovCodPai, (*QrIMECsTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto,
          emidEmProcCal, emidCaleado, emidCaleado,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCalInn, eminSorcCal, eminDestCal);
        end;
        TEStqMovimID.emidEmProcCur:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, QrVSRibOriIMEI,
          (*QrVSRibOriPallet*)nil, (*QrVSRibDst*)nil, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrIMECsCodigo.Value*)PaiNivel1,
          (*QrIMECsMovimCod.Value*) MovCodPai, (*QrIMECsTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto,
          emidEmProcCur, emidIndsVS, emidEmProcCur,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCurInn, eminSorcCur, eminDestCur);
          InsercaoItemProducaoConjunta := iipcUniAnt;
        end;
        TEStqMovimID.emidCurtido:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, (*QrVSRibOriIMEI*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst, QrVSRibInd,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrIMECsCodigo.Value*)PaiNivel1,
          (*QrIMECsMovimCod.Value*) MovCodPai, (*QrIMECsTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmProcCur, emidIndsVS, emidEmProcCur,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmCurInn, eminSorcCur, eminDestCur);
          InsercaoItemProducaoConjunta := iipcUniGer;
          //Geral.MB_SQL(Self, QrVSRibAtu);
          //Geral.MB_SQL(Self, QrVSRibDst);
          //Geral.MB_SQL(Self, QrVSRibInd);
        end;
        TEStqMovimID.emidEmOperacao:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, (*QrVSRibOriIMEI*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrVSRibCabCodigo.Value*)PaiNivel1,
          (*QrVSRibCabMovimCod.Value*)MovCodPai,(*QrVSRibCabTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmOperacao, emidEmOperacao, emidEmOperacao,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmOperInn, eminSorcOper, eminDestOper);
          InsercaoItemProducaoConjunta := iipcGemeos;
        end;
        TEStqMovimID.emidEmProcWE:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, (*QrVSRibOriIMEI*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrVSRibCabCodigo.Value*)PaiNivel1,
          (*QrVSRibCabMovimCod.Value*)MovCodPai,(*QrVSRibCabTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmProcWE, emidFinished, emidEmProcWE,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmWEndInn, eminSorcWEnd, eminDestWEnd);
          InsercaoItemProducaoConjunta := iipcGemeos;
          //Geral.MB_SQL(Self, QrVSRibAtu);
          //Geral.MB_SQL(Self, QrVSRibDst);
          //Geral.MB_SQL(Self, QrVSRibInd);
        end;
        TEStqMovimID.emidEmProcSP:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, (*QrVSRibOriIMEI*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrVSRibCabCodigo.Value*)PaiNivel1,
          (*QrVSRibCabMovimCod.Value*)MovCodPai,(*QrVSRibCabTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmProcSP, emidEmProcSP, emidEmProcSP,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmPSPInn, eminSorcPSP, eminDestPSP);
          InsercaoItemProducaoConjunta := iipcGemeos;
        end;
        TEStqMovimID.emidEmReprRM:
        begin
          JmpMovID  := 0;
          JmpMovCod := 0;
          JmpNivel1 := 0;
          JmpNivel2 := 0;
          PaiMovID  := QrIMECsMovimID.Value;
          MovCodPai := QrIMECsMovCodPai.Value;
          PaiNivel1 := QrIMECsCodigo.Value;
          PaiNivel2 := 0;
          //
          VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSRibAtu, (*QrVSRibOriIMEI*)nil,
          (*QrVSRibOriPallet*)nil, QrVSRibDst, (*QrVSRibInd*)nil,
          (*QrSubPrd*)nil, (*QrDescl*)nil, (*QrForcados*)nil,
          (*QrEmit*)nil, (*QrPQO*)nil, (*QrVSRibCabCodigo.Value*)PaiNivel1,
          (*QrVSRibCabMovimCod.Value*)MovCodPai,(*QrVSRibCabTemIMEIMrt.Value*)1,
          //MovIDEmProc, MovIDPronto, MovIDIndireto
          emidEmReprRM, emidEmReprRM, emidEmReprRM,
          //MovNivInn, MovNivSrc, MovNivDst:
          eminEmRRMInn, eminSorcRRM, eminDestRRM);
          InsercaoItemProducaoConjunta := iipcGemeos;
        end;
        else Geral.MB_Erro('MovimID ' + Geral.FF0(Integer(TargetMovimID)) +
        ' n�o implementado em ' + sProcName + ' (2)');
      end;
      //
      //Geral.MB_SQL(Self, QrVSRibDst);
      case InsercaoItemProducaoConjunta of
        //iipcIndef=0,
        iipcGemeos:
        begin
(*
          2019-01-27 - Fazer aqui!!
          Se QrVSRibDst n�o tem itens, quer dizer que tem um processo em aberto,
          mas sem nenhum artigo pronto ainda! Nesse caso tem que ver se j� tem
          consumo de insumos! Tem que criar o 290 sem 291 mas com 292 de couro
          e de insumos!
*)
          if (QrVSRibAtu.RecordCount > 0) and (QrVSRibDst.RecordCount = 0) then
          begin
            MovimID     := TEstqMovimID(QrVSRibAtuMovimID.Value);
            MovimCod    := QrVSRibAtuMovimCod.Value;
            Codigo      := QrVSRibAtuCodigo.Value;
            DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
            DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
            DtMovim     := Trunc(QrVSRibAtuDataHora.Value);
            DtCorrApo   := Trunc(QrVSRibAtuDtCorrApo.Value);
            Empresa     := QrVSRibAtuEmpresa.Value;
            ForneceMO   := QrVSRibAtuFornecMO.Value;
            Controle    := QrVSRibAtuControle.Value;
            GraGruX     := QrVSRibAtuGraGruX.Value;
            MovimNiv    := TEstqMovimNiv(QrVSRibAtuMovimNiv.Value);
            MovimTwn    := QrVSRibAtuMovimTwn.Value;
            AreaM2      := 0; // N�o produziu nada!
            PesoKg      := 0; // N�o produziu nada!
            Pecas       := 0; // N�o produziu nada!
            Produziu    := TProduziuVS.przvsNao;
            // Ver se tem consumo de insumos...
            // Se tiver, lan�ar.
            ReopenOriPQx(MovimCod, FSQL_PeriodoPQ, False);
            InsumoSemProducao := QrOri.RecordCount > 0;
            if InsumoSemProducao then
            begin
              MeAviso.Lines.Add('MovimCod ' + Geral.FF0(QrVSRibAtuMovimCod.Value) +
              ' sem artigos destino mas com insumos consumidos!');
              //
              VerificaSeTemGraGruX(3299, GraGruX, sProcName, QrVSRibAtu);
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaGemeos(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, Tipo_Item, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaGemeos(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ, Produziu);
                else
                  AvisoFip('Gemeos');
              end;
            end;
          end else
          //// Fim Processo sem produ��o, mas com consumo de insumos
          ////
          ///////////////////////    Houve produ��o    /////////////////////////
          begin
            QrVSRibDst.First;
            while not QrVSRibDst.Eof do
            begin
              if (QrVSRibDstDataHora.Value >= FDiaIni)
              and (QrVSRibDstDataHora.Value < FDiaPos) then
              begin
                MovimID     := TEstqMovimID(QrVSRibDstMovimID.Value);
                MovimCod    := QrVSRibDstMovimCod.Value;
                Codigo      := QrVSRibDstCodigo.Value;
                DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
                DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
                DtMovim     := Trunc(QrVSRibDstDataHora.Value);
                DtCorrApo   := Trunc(QrVSRibDstDtCorrApo.Value);
                Empresa     := QrVSRibDstEmpresa.Value;
                ForneceMO   := QrVSRibDstFornecMO.Value;
                Controle    := QrVSRibDstControle.Value;
                GraGruX     := QrVSRibDstGraGruX.Value;
                MovimNiv    := TEstqMovimNiv(QrVSRibDstMovimNiv.Value);
                MovimTwn    := QrVSRibDstMovimTwn.Value;
                AreaM2      := QrVSRibDstAreaM2.Value;
                PesoKg      := QrVSRibDstPesoKg.Value;
                Pecas       := QrVSRibDstPecas.Value;
                Produziu    := TProduziuVS.przvsSim;
                VerificaSeTemGraGruX(3343, GraGruX, sProcName, QrVSRibDst);
                //
                case FormaInsercaoItemProducao of
                  fipIsolada:
                    InsereItemProducaoIsoladaGemeos(DtHrAberto, DtHrFimOpe,
                    DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                    Codigo, Controle, Tipo_Item, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                    Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                    JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                    IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ);
                  fipConjunta:
                    InsereItemProducaoConjuntaGemeos(DtHrAberto, DtHrFimOpe,
                    DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                    Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                    Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                    JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                    IDSeq1_K290, IDSeq1_K300, IncluiPQX, InseriuPQ, Produziu);
                  else
                    AvisoFip('Gemeos');
                end;
              end;
              //
              QrVSRibDst.Next;
            end;
          end;
        end;
        iipcIndireta:
        begin
          QrVSRibInd.First;
          while not QrVSRibInd.Eof do
          begin
            if (QrVSRibIndDataHora.Value >= FDiaIni)
            and (QrVSRibIndDataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibIndMovimID.Value);
              MovimCod    := QrVSRibIndMovimCod.Value;
              Codigo      := QrVSRibIndCodigo.Value;
              DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibIndDataHora.Value);
              DtCorrApo   := Trunc(QrVSRibIndDtCorrApo.Value);
              Empresa     := QrVSRibIndEmpresa.Value;
              ForneceMO   := QrVSRibIndFornecMO.Value;
              Controle    := QrVSRibIndControle.Value;
              GraGruX     := QrVSRibIndGraGruX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibIndMovimNiv.Value);
              MovimTwn    := QrVSRibIndMovimTwn.Value;
              AreaM2      := QrVSRibIndAreaM2.Value;
              PesoKg      := QrVSRibIndPesoKg.Value;
              Pecas       := QrVSRibIndPecas.Value;
              GerMovID    := QrVSRibIndSrcMovID.Value;
              GerNivel1   := QrVSRibIndSrcNivel1.Value;
              GerNivel2   := QrVSRibIndSrcNivel2.Value;
              GerGGX      := QrVSRibIndJmpGGX.Value;
              QtdGerPeca  := QrVSRibIndQtdGerPeca.Value;
              QtdGerPeso  := QrVSRibIndQtdGerPeso.Value;
              QtdGerArM2  := QrVSRibIndQtdGerArM2.Value;
              if GerGGX = 0 then
              begin
                //Geral.MB_Erro('MovCodPai = ' + Geral.FF0(MovCodPai));
                GerGGX := VS_PF.ObtemJmpGGXdeMovCodPai(MovCodPai);
                if MeAviso <> nil then
                  MeAviso.Lines.Add('"GerGGX" obtido do MovCodPai = ' +
                    Geral.FF0(MovCodPai));
(*
                if MeAviso <> nil then
                  MeAviso.Lines.Add('"GerGGX" indefinido para GerMovID = ' +
                  Geral.FF0(GerMovID) + ' e GerNivel1 = ' + Geral.FF0(GerNivel1));
                VS_PF.AtualizaJmpGGX(TEstqMovimID(GerMovID), GerNivel1);
                UnDmkDAC_PF.AbreQuery(QrVSRibInd, Dmod.MyDB);
                GerGGX := QrVSRibIndJmpGGX.Value;
                if MeAviso <> nil then
                begin
                  if GerGGX = 0 then
                    MeAviso.Lines.Add('"GerGGX" nao corrigido para GerMovID = ' +
                    Geral.FF0(GerMovID) + ' e GerNivel1 = ' + Geral.FF0(GerNivel1))
                  else
                    MeAviso.Lines.Add('"GerGGX" corrigido para GerMovID = ' +
                    Geral.FF0(GerMovID) + ' e GerNivel1 = ' + Geral.FF0(GerNivel1));
                end;
*)
              end;
              VerificaSeTemGraGruX(3424, GraGruX, sProcName, QrVSRibInd);
              VerificaSeTemGraGruX(3425, GerGGX, sProcName, QrVSRibInd);
              //fazer pelo JmpGGX! sem
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaIndireta(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  GerMovID, GerNivel1, GerNivel2, GerGGX, QtdGerPeca,
                  QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300, IncluiPQX,
                  InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaIndireta(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2, PesoKg,
                  Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  GerMovID, GerNivel1, GerNivel2, GerGGX, QtdGerPeca,
                  QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300, IncluiPQX,
                  InseriuPQ);
                else
                  AvisoFip('Indireta');
              end;
            end;
            //
            QrVSRibInd.Next;
          end;
        end;
        iipcUniAnt:
        begin
          //Geral.MB_SQL(Self, QrVSRibOriIMEI);
          QrVSRibOriIMEI.First;
          while not QrVSRibOriIMEI.Eof do
          begin
            if (QrVSRibOriIMEIDataHora.Value >= FDiaIni)
            and (QrVSRibOriIMEIDataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibOriIMEIMovimID.Value);
              MovimCod    := QrVSRibOriIMEIMovimCod.Value;
              Codigo      := QrVSRibOriIMEICodigo.Value;
              DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibOriIMEIDataHora.Value);
              DtCorrApo   := Trunc(QrVSRibOriIMEIDtCorrApo.Value);
              Empresa     := QrVSRibOriIMEIEmpresa.Value;
              ForneceMO   := QrVSRibOriIMEIFornecMO.Value;
              Controle    := QrVSRibOriIMEIControle.Value;
              GGXOri      := QrVSRibOriIMEIRmsGGX.Value;
              GGXDst      := QrVSRibOriIMEIDstGGX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibOriIMEIMovimNiv.Value);
              MovimTwn    := QrVSRibOriIMEIMovimTwn.Value;
              AreaM2      := QrVSRibOriIMEIAreaM2.Value;
              PesoKg      := QrVSRibOriIMEIPesoKg.Value;
              Pecas       := QrVSRibOriIMEIPecas.Value;
              QtdAntPeca  := QrVSRibOriIMEIQtdAntPeca.Value;
              QtdAntPeso  := QrVSRibOriIMEIQtdAntPeso.Value;
              QtdAntArM2  := QrVSRibOriIMEIQtdAntArM2.Value;
              //
              VerificaSeTemGraGruX(3485, GGXOri, sProcName, QrVSRibOriIMEI);
              VerificaSeTemGraGruX(3486, GGXDst, sProcName, QrVSRibOriIMEI);
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaUniAnt(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdAntPeca, QtdAntPeso, QtdAntArM2, IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaUniAnt(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdAntPeca, QtdAntPeso, QtdAntArM2, IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                else
                  AvisoFip('UnidAnt');
              end;
            end;
            //
            QrVSRibOriIMEI.Next;
          end;
        end;
        iipcUniGer:
        begin
          QrVSRibInd.First;
          while not QrVSRibInd.Eof do
          begin
            if (QrVSRibIndDataHora.Value >= FDiaIni)
            and (QrVSRibIndDataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibIndMovimID.Value);
              MovimCod    := QrVSRibIndMovimCod.Value;
              Codigo      := QrVSRibIndCodigo.Value;
              DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibIndDataHora.Value);
              DtCorrApo   := Trunc(QrVSRibIndDtCorrApo.Value);
              Empresa     := QrVSRibIndEmpresa.Value;
              ForneceMO   := QrVSRibIndFornecMO.Value;
              Controle    := QrVSRibIndControle.Value;
              GGXOri      := QrVSRibIndRmsGGX.Value;
              GGXDst      := QrVSRibIndJmpGGX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibIndMovimNiv.Value);
              MovimTwn    := QrVSRibIndMovimTwn.Value;
              AreaM2      := QrVSRibIndAreaM2.Value;
              PesoKg      := QrVSRibIndPesoKg.Value;
              Pecas       := QrVSRibIndPecas.Value;
              QtdGerPeca  := Pecas;
              QtdGerPeso  := PesoKg;
              QtdGerArM2  := AreaM2;
              //
              VerificaSeTemGraGruX(3542, GGXOri, sProcName, QrVSRibInd);
              VerificaSeTemGraGruX(3543, GGXDst, sProcName, QrVSRibInd);
              //
              JmpMovID    := QrVSRibIndJmpMovID.Value;
              JmpNivel1   := QrVSRibIndJmpNivel1.Value;
              //if MovimID = TEstqMovimID.emidCurtido then
              if (MovimID = TEstqMovimID.emidCurtido) and
              (TEstqMovimID(JmpMovID) = TEstqMovimID.emidCurtido) then
              begin
                MovimID := TEstqMovimID(JmpMovID);
                if JmpNivel1 <> 0 then
                begin
                  UnDmkDAC_PF.AbreMySQLQuery0(QrCab34, Dmod.MyDB, [
                  'SELECT MovimCod',
                  'FROM vscurjmp',
                  'WHERE Codigo=' + Geral.FF0(JmpNivel1),
                  '']);
                  MovimCod := QrCab34MovimCod.Value;
                end else
                begin
                  MovimCod := 0;
                  Geral.MB_Aviso('MovimCod indefinido em ' + sProcName + ' (3)');
                end;
              end;
              //
              case FormaInsercaoItemProducao of
                fipIsolada:
                  InsereItemProducaoIsoladaUniGer(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdGerPeca, QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                fipConjunta:
                  InsereItemProducaoConjuntaUniGer(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdAntPeca, QtdAntPeso, QtdAntArM2, IDSeq1_K290, IDSeq1_K300,
                  IncluiPQX, InseriuPQ);
                else
                  AvisoFip('UnidGer');
              end;
            end;
            //
            QrVSRibInd.Next;
          end;
        end;
        iipcSimples:
        begin
          // Gera��o!
          QrVSRibDst.First;
          while not QrVSRibDst.Eof do
          begin
            if (QrVSRibDstDataHora.Value >= FDiaIni)
            and (QrVSRibDstDataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibDstMovimID.Value);
              MovimCod    := QrVSRibDstMovimCod.Value;
              Codigo      := QrVSRibDstCodigo.Value;
              DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibDstDataHora.Value);
              DtCorrApo   := Trunc(QrVSRibDstDtCorrApo.Value);
              Empresa     := QrVSRibDstEmpresa.Value;
              ForneceMO   := QrVSRibDstFornecMO.Value;
              Controle    := QrVSRibDstControle.Value;
              GraGruX     := QrVSRibDstGraGruX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibDstMovimNiv.Value);
              MovimTwn    := QrVSRibDstMovimTwn.Value;
              AreaM2      := QrVSRibDstAreaM2.Value;
              PesoKg      := QrVSRibDstPesoKg.Value;
              Pecas       := QrVSRibDstPecas.Value;
              //
              VerificaSeTemGraGruX(3618, GraGruX, sProcName, QrVSRibDst);
              case FormaInsercaoItemProducao of
                (*
                fipIsolada:
                  InsereItemProducaoIsoladaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdGerPeca, QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300,
                  InseriuPQ);
                *)
                fipConjunta:
                  InsereItemProducaoConjuntaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, PaiMovID, MovCodPai, PaiNivel1,
                  PaiNivel2, (*Fator*)1, TEstqSPEDTabSorc.estsVMI,
                  TSPED_EFD_GerBxa.segbGera, IncluiPQX, IDSeq1_K290, IDSeq1_K300);
                else
                  AvisoFip('Simples');
              end;
            end;
            //
            QrVSRibDst.Next;
          end;
          //
          // Baixa!
          QrVSRibOriIMEI.First;
          while not QrVSRibOriIMEI.Eof do
          begin
            if (QrVSRibOriIMEIDataHora.Value >= FDiaIni)
            and (QrVSRibOriIMEIDataHora.Value < FDiaPos) then
            begin
              MovimID     := TEstqMovimID(QrVSRibOriIMEIMovimID.Value);
              MovimCod    := QrVSRibOriIMEIMovimCod.Value;
              Codigo      := QrVSRibOriIMEICodigo.Value;
              DtHrAberto  := Trunc(QrProcDtHrAberto.Value);
              DtHrFimOpe  := Trunc(QrProcDtHrFimOpe.Value);
              DtMovim     := Trunc(QrVSRibOriIMEIDataHora.Value);
              DtCorrApo   := Trunc(QrVSRibOriIMEIDtCorrApo.Value);
              Empresa     := QrVSRibOriIMEIEmpresa.Value;
              ForneceMO   := QrVSRibOriIMEIFornecMO.Value;
              Controle    := QrVSRibOriIMEIControle.Value;
              GraGruX     := QrVSRibAtuGraGruX.Value;
              MovimNiv    := TEstqMovimNiv(QrVSRibOriIMEIMovimNiv.Value);
              MovimTwn    := QrVSRibOriIMEIMovimTwn.Value;
              AreaM2      := QrVSRibOriIMEIAreaM2.Value;
              PesoKg      := QrVSRibOriIMEIPesoKg.Value;
              Pecas       := QrVSRibOriIMEIPecas.Value;
              //
              VerificaSeTemGraGruX(3669, GraGruX, sProcName, QrVSRibOriIMEI);
              //
              case FormaInsercaoItemProducao of
                (*
                fipIsolada:
                  InsereItemProducaoIsoladaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GGXOri, GGXDst, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, JmpMovID, JmpMovCod, JmpNivel1,
                  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
                  QtdGerPeca, QtdGerPeso, QtdGerArM2, IDSeq1_K290, IDSeq1_K300,
                  InseriuPQ);
                *)
                fipConjunta:
                  InsereItemProducaoConjuntaSimples(DtHrAberto, DtHrFimOpe,
                  DtMovim, DtCorrApo, MovimID, Empresa, MovimCod, CodDocOP,
                  Codigo, Controle, GraGruX, MovimNiv, MovimTwn, AreaM2,
                  PesoKg, Pecas, OrigemOpeProc, PaiMovID, MovCodPai, PaiNivel1,
                  PaiNivel2, (*Fator*)-1, TEstqSPEDTabSorc.estsVMI,
                  TSPED_EFD_GerBxa.segbBaixa, IncluiPQX, IDSeq1_K290, IDSeq1_K300);
                else
                  AvisoFip('Simples');
              end;
            end;
            //
            QrVSRibOriIMEI.Next;
          end;
          //baixa !!!!
(*
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        //MovimCod
        MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
*)
        //
        end;
        else begin
          Geral.MB_Erro(GetEnumName(TypeInfo(TInsercaoItemProducaoConjunta),
          Integer(InsercaoItemProducaoConjunta)) + ' indefinido em ' +
          sprocName + ' (4)');
        end;
      end;
      QrIMECs.Next;
    end;
    QrFather.Next;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaGemeos(const
  DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime; const
  MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo, Controle,
  GraGruX: Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
  const _AreaM2, _PesoKg, _Pecas: Double; OrigemOpeProc: TOrigemOpeProc; const
  JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, MovCodPai, PaiNivel1,
  PaiNivel2: Integer; var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx:
  Boolean; var InseriuPQ: Boolean; const Produziu: TProduziuVS);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaGemeos()';
  DstFator = 1;
  OriFator = 1;
var
  ClientMO, FornecMO, EntiSitio, CtrlDst, GGXDst, TipoEstqDst, CtrlOri, GGXOri,
  TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
  TemTwn: Boolean;
begin
  //
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  //
  CtrlDst     := Controle;
  GGXDst      := GraGruX;
  AreaM2      := _AreaM2;
  PesoKg      := _PesoKg;
  Pecas       := _Pecas;
  //if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
  if Produziu <> TProduziuVS.przvsNao then
  begin
    if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
    begin
      case TipoEstqDst of
        0: QtdeDst := Pecas;
        1: QtdeDst := AreaM2;
        2: QtdeDst := PesoKg;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrProdRib.SQL.Text);
          //
          QtdeDst := Pecas;
        end;
      end;
      QtdeDst := QtdeDst * DstFator;
    end else
      QtdeDst := 0;
    if (QtdeDst = 0) then
    begin
      VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
      GGXDst, MovimCod, CtrlDst, sProcName);
      //Geral.MB_SQL(self, QrObtCliForSite);
    end;
  end;
  //
  //
  TemTwn := ReopenVmiTwn(MovimID, MovimNiv, MovimCod, MovimTwn);
  if TemTwn then
  begin
    CtrlOri     := QrVmiTwn.FieldByName('Controle').AsInteger;
    GGXOri      := QrVmiTwn.FieldByName('GraGruX').AsInteger;
    AreaM2      := -QrVmiTwn.FieldByName('AreaM2').AsFloat;
    PesoKg      := -QrVmiTwn.FieldByName('PesoKg').AsFloat;
    Pecas       := -QrVmiTwn.FieldByName('Pecas').AsFloat;
    TipoEstqOri := VS_PF.ObtemTipoEstqIMEI(CtrlOri,
    'vmi.AreaM2',  'vmi.PesoKg',  'vmi.Pecas',  'vmi.GraGruX', 'vmi.Controle');
    if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
    begin
      case TipoEstqOri of
        0: QtdeOri := Pecas;
        1: QtdeOri := AreaM2;
        2: QtdeOri := PesoKg;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrProdRib.SQL.Text);
          //
          QtdeOri := Pecas;
        end;
      end;
      QtdeOri := QtdeOri * OriFator;
    end else
      QtdeOri := 0;
  end;
  if TemTwn and (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, CtrlOri, sProcName);
  //
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      if TemTwn then
      begin
        // K292 - Itens consumidos
        OriESTSTabSorc := estsVMI;
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
        PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
        GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
        ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      end;
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      if TemTwn then
      begin
        // K302 - Itens Consumidos
        OriESTSTabSorc := estsVMI;
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
        PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
        GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
        ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      end;
      //
      // K302 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(10)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaIndireta(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GraGruX: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, GerMovID, GerNivel1,
  GerNivel2, GerGGX: Integer; const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
  var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean; var
  InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaIndireta()';
  DstFator = -1;
  OriFator = -1;
var
  ClientMO, FornecMO, EntiSitio, CtrlDst, GGXDst, TipoEstqDst, CtrlOri, GGXOri,
  TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  CtrlDst     := GerNivel2;
  //GGXDst      := GerGGX;
  GGXDst      := GraGruX;
  AreaM2      := _AreaM2;
  PesoKg      := _PesoKg;
  Pecas       := _Pecas;
  //if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0:
      begin
        //QtdeDst := AreaM2;
        QtdeDst := QtdGerPeca;
        QtdeOri := QtdGerPeca;
      end;
      1:
      begin
        //QtdeDst := AreaM2;
        QtdeDst := QtdGerArM2;
        QtdeOri := QtdGerArM2;
      end;
      2:
      begin
        //QtdeDst := PesoKg;
        QtdeDst := QtdGerPeso;
        QtdeOri := QtdGerPeso;
      end
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        //QtdeDst := Pecas;
        QtdeDst := QtdGerPeca;
        QtdeOri := QtdGerPeca;
      end;
    end;
    //QtdeDst := QtdeDst * DstFator;
    //QtdeOri := QtdeOri * DstFator;
  end else
    QtdeDst := 0;
  //
  CtrlOri     := Controle;
  //GGXOri      := GraGruX;
  GGXOri      := GerGGX;
  //
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
{
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
}
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Insumos consumidos
{
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
}
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(11)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaSimples(
  const DtHrAberto, DtHrFimOpe,
              DtMovim, DtCorrApo: TDateTime; const MovimID: TEstqMovimID; const
              Empresa, MovimCod, CodDocOP, Codigo, Controle, GraGruX:
              Integer; const MovimNiv: TEstqMovimNiv; const MovimTwn: Integer;
              const AreaM2, PesoKg, Pecas: Double; OrigemOpeProc:
              TOrigemOpeProc; const PaiMovID, MovCodPai, PaiNivel1, PaiNivel2,
              Fator: Integer; const OriESTSTabSorc: TEstqSPEDTabSorc; const
              SPED_EFD_GerBxa: TSPED_EFD_GerBxa; const IncluiPQx: Boolean;
              var IDSeq1_K290, IDSeq1_K300: Integer);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaSimples()';
  //
  JmpMovID     = 0;
  JmpMovCod    = 0;
  JmpNivel1    = 0;
  JmpNivel2    = 0;
var
  ClientMO, FornecMO, EntiSitio, TipoEstq: Integer;
  Qtde: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstq);
  //
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstq of
      0: Qtde := Pecas;
      1: Qtde := AreaM2;
      2: Qtde := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstq) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GraGruX) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        Qtde := Pecas;
      end;
    end;
    Qtde := Qtde * Fator;
  end else
    Qtde := 0;
  if (*TemTwn and*) (Qtde = 0) then
    VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
    GraGruX, MovimCod, Controle, sProcName);
  //
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290);
      end;
      case SPED_EFD_GerBxa of
        // K291 - Itens Produzidos
        TSPED_EFD_GerBxa.segbGera:
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
        // K292 - Itens consumidos
        TSPED_EFD_GerBxa.segbBaixa:
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
        else Geral.MB_Erro('SPED_EFD_GerBxa indefinido em ' + sProcName);
      end;
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300);
      end;
      case SPED_EFD_GerBxa of
      // K301 - Itens Produzidos
        TSPED_EFD_GerBxa.segbGera:
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Itens Consumidos
        TSPED_EFD_GerBxa.segbBaixa:
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
          PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
          GraGruX, Qtde, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
          FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
        //
        else Geral.MB_Erro('SPED_EFD_GerBxa indefinido em ' + sProcName);
      end;
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(12)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaUniAnt(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GGXOri, GGXDst: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  const QtdAntPeca, QtdAntPeso, QtdAntArM2: Double; var IDSeq1_K290, IDSeq1_K300:
  Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaRmsDst()';
  DstFator = 1;
  OriFator = 1;
var
  ClientMO, FornecMO, EntiSitio, TipoEstqDst, TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  AreaM2      := QtdAntArM2;
  PesoKg      := QtdAntPeso;
  Pecas       := QtdAntPeca;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstqDst" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
  AreaM2      := QtdAntArM2;
  PesoKg      := QtdAntPeso;
  Pecas       := QtdAntPeca;
  TipoEstqOri := TipoEstqDst;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqOri of
      0: QtdeOri := Pecas;
      1: QtdeOri := AreaM2;
      2: QtdeOri := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeOri := Pecas;
      end;
    end;
    QtdeOri := QtdeOri * OriFator;
  end else
    QtdeOri := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(13)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaUniGer(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GGXOri, GGXDst: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double; var IDSeq1_K290,
  IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoConjuntaUniReg()';
  DstFator = 1;
  OriFator = 1;
var
  ClientMO, FornecMO, EntiSitio, TipoEstqDst, TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  AreaM2      := -_AreaM2;
  PesoKg      := -_PesoKg;
  Pecas       := -_Pecas;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstqDst" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
(*
  AreaM2      := -QtdGerArM2;
  PesoKg      := -QtdGerPeso;
  Pecas       := -QtdGerPeca;
*)
  TipoEstqOri := VS_PF.ObtemTipoEstqIMEI(Controle,
  'vmi.QtdGerArM2',  'vmi.QtdGerPeso',  'vmi.QtdGerPeca',  'vmi.RmsGGX', 'vmi.Controle');
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqOri of
      0: QtdeOri := Pecas;
      1: QtdeOri := AreaM2;
      2: QtdeOri := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeOri := Pecas;
      end;
    end;
    QtdeOri := QtdeOri * OriFator;
  end else
    QtdeOri := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(14)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaGemeos(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, Tipo_Item, GraGruX: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
  var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaGemeos()';
  DstFator = 1;
  OriFator = 1;
  COD_INS_SUBST = '';
var
  ClientMO, FornecMO, EntiSitio, CtrlDst, GGXDst, TipoEstqDst, CtrlOri, GGXOri,
  TipoEstqOri: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
  TemTwn: Boolean;
begin
  //
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  //
  CtrlDst     := Controle;
  GGXDst      := GraGruX;
  AreaM2      := _AreaM2;
  PesoKg      := _PesoKg;
  Pecas       := _Pecas;
  //if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
  //
  TemTwn := ReopenVmiTwn(MovimID, MovimNiv, MovimCod, MovimTwn);
  //
  if TemTwn then
  begin
    CtrlOri     := QrVmiTwn.FieldByName('Controle').AsInteger;
    GGXOri      := QrVmiTwn.FieldByName('GraGruX').AsInteger;
    AreaM2      := -QrVmiTwn.FieldByName('AreaM2').AsFloat;
    PesoKg      := -QrVmiTwn.FieldByName('PesoKg').AsFloat;
    Pecas       := -QrVmiTwn.FieldByName('Pecas').AsFloat;
    TipoEstqOri := TipoEstqDst;
    if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
    begin
      case TipoEstqOri of
        0: QtdeOri := Pecas;
        1: QtdeOri := AreaM2;
        2: QtdeOri := PesoKg;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrProdRib.SQL.Text);
          //
          QtdeOri := Pecas;
        end;
      end;
      QtdeOri := QtdeOri * OriFator;
    end else
      QtdeOri := 0;
    //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
  if QtdeOri = 0 then
    Geral.MB_Aviso('Qtde 0');
  end;
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp23X
  else
    TipoRegSPEDProd := trsp25X;
  //
  case TipoRegSPEDProd of
    trsp23X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO,
      FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
      FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1_K290);
      //
      if TemTwn then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, IDSeq1_K290, DtHrAberto, DtCorrApo, GGXOri,
        QtdeOri, COD_INS_SUBST, (*ID_Item*)CtrlOri, Integer(MovimID), Codigo, MovimCod,
        Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
        OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      end;
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    trsp25X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrFimOpe,
      DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
      QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1_K300);
      //
      if TemTwn then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, IDSeq1_K300, DtHrAberto, DtCorrApo, GGXOri,
        QtdeOri, COD_INS_SUBST, (*ID_Item*)CtrlOri, Integer(MovimID), Codigo, MovimCod,
        Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
        OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      end;
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(15)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaIndireta(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GraGruX: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, GerMovID, GerNivel1,
  GerNivel2, GerGGX: Integer; const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double;
  var IDSeq1_K290, IDSeq1_K300: Integer; const IncluiPQx: Boolean;
  var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaIndireta()';
  DstFator = -1;
  OriFator = -1;
  COD_INS_SUBST = '';
var
  ClientMO, FornecMO, EntiSitio, CtrlDst, GGXDst, TipoEstqDst, CtrlOri, GGXOri,
  TipoEstqOri, IDSeq1: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  CtrlDst     := GerNivel2;
  //GGXDst      := GerGGX;
  GGXDst      := GraGruX;
  AreaM2      := _AreaM2;
  PesoKg      := _PesoKg;
  Pecas       := _Pecas;
  //if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0:
      begin
        //QtdeDst := AreaM2;
        QtdeDst := QtdGerPeca;
        QtdeOri := QtdGerPeca;
      end;
      1:
      begin
        //QtdeDst := AreaM2;
        QtdeDst := QtdGerArM2;
        QtdeOri := QtdGerArM2;
      end;
      2:
      begin
        //QtdeDst := PesoKg;
        QtdeDst := QtdGerPeso;
        QtdeOri := QtdGerPeso;
      end
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        //QtdeDst := Pecas;
        QtdeDst := QtdGerPeca;
        QtdeOri := QtdGerPeca;
      end;
    end;
    //QtdeDst := QtdeDst * DstFator;
    //QtdeOri := QtdeOri * DstFator;
  end else
    QtdeDst := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, CtrlDst, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, CtrlOri, sProcName);
  CtrlOri     := Controle;
  //GGXOri      := GraGruX;
  GGXOri      := GerGGX;
  //
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp23X
  else
    TipoRegSPEDProd := trsp25X;
  //
  case TipoRegSPEDProd of
    trsp23X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO,
      FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
      FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)CtrlOri, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    trsp25X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrFimOpe,
      DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
      QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)CtrlOri, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
{
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, MeAviso);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Insumos consumidos
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, CtrlDst,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, MeAviso);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, CtrlOri,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Insumos consumidos
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
}
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(16)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaUniAnt(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GGXOri, GGXDst: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  const QtdAntPeca, QtdAntPeso, QtdAntArM2: Double; var IDSeq1_K290,
  IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaRmsDst()';
  DstFator = 1;
  OriFator = 1;
  COD_INS_SUBST = '';
var
  ClientMO, FornecMO, EntiSitio, TipoEstqDst, TipoEstqOri, IDSeq1: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  AreaM2      := QtdAntArM2;
  PesoKg      := QtdAntPeso;
  Pecas       := QtdAntPeca;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstqDst" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
  AreaM2      := QtdAntArM2;
  PesoKg      := QtdAntPeso;
  Pecas       := QtdAntPeca;
  TipoEstqOri := TipoEstqDst;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqOri of
      0: QtdeOri := Pecas;
      1: QtdeOri := AreaM2;
      2: QtdeOri := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeOri := Pecas;
      end;
    end;
    QtdeOri := QtdeOri * OriFator;
  end else
    QtdeOri := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
{
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, MeAviso);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Insumos consumidos
(*
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
*)
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, MeAviso);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Insumos consumidos
(*
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        MovimCod, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
*)
    end;
}
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp23X
  else
    TipoRegSPEDProd := trsp25X;
   //
  case TipoRegSPEDProd of
    trsp23X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO,
      FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
      FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)Controle, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      //
{
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
}
      // Ini 2019-01-23
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
      // FIm 2019-01-23
    end;
    trsp25X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrFimOpe,
      DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
      QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)Controle, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      //
{
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
}
      // Ini 2019-01-23
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
      // FIm 2019-01-23
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(17)');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaUniGer(
  const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const MovimID: TEstqMovimID; const Empresa, MovimCod, CodDocOP, Codigo,
  Controle, GGXOri, GGXDst: Integer; const MovimNiv: TEstqMovimNiv;
  const MovimTwn: Integer; const _AreaM2, _PesoKg, _Pecas: Double;
  OrigemOpeProc: TOrigemOpeProc; const JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, MovCodPai, PaiNivel1, PaiNivel2: Integer;
  const QtdGerPeca, QtdGerPeso, QtdGerArM2: Double; var IDSeq1_K290,
  IDSeq1_K300: Integer; const IncluiPQx: Boolean; var InseriuPQ: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.InsereItemProducaoIsoladaUniReg()';
  DstFator = 1;
  OriFator = 1;
  COD_INS_SUBST = '';
var
  ClientMO, FornecMO, EntiSitio, TipoEstqDst, TipoEstqOri, IDSeq1: Integer;
  AreaM2, PesoKg, Pecas, QtdeDst, QtdeOri: Double;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  OriESTSTabSorc: TEstqSPEDTabSorc;
begin
  DefineCliForSiteTipEstq(Controle, ClientMO, FornecMO, EntiSitio, TipoEstqDst);
  //
  AreaM2      := -_AreaM2;
  PesoKg      := -_PesoKg;
  Pecas       := -_Pecas;
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqDst of
      0: QtdeDst := Pecas;
      1: QtdeDst := AreaM2;
      2: QtdeDst := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstqDst" = ' + Geral.FF0(TipoEstqDst) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqDst] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeDst := Pecas;
      end;
    end;
    QtdeDst := QtdeDst * DstFator;
  end else
    QtdeDst := 0;
  //
  AreaM2      := -QtdGerArM2;
  PesoKg      := -QtdGerPeso;
  Pecas       := -QtdGerPeca;
  TipoEstqOri := VS_PF.ObtemTipoEstqIMEI(Controle,
  'vmi.QtdGerArM2',  'vmi.QtdGerPeso',  'vmi.QtdGerPeca',  'vmi.RmsGGX', 'vmi.Controle');
  if ((Pecas <> 0) or ((Pecas=0) and (PesoKg <> 0))) and (DtMovim > 1) then
  begin
    case TipoEstqOri of
      0: QtdeOri := Pecas;
      1: QtdeOri := AreaM2;
      2: QtdeOri := PesoKg;
      else
      begin
        Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(TipoEstqOri) + ' indefinido em '
        + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstqOri] + sLineBreak +
        'Reduzido = ' + Geral.FF0(GGXOri) + sLineBreak +
        'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
        QrProdRib.SQL.Text);
        //
        QtdeOri := Pecas;
      end;
    end;
    QtdeOri := QtdeOri * OriFator;
  end else
    QtdeOri := 0;
  //
  if (*TemTwn and*) (QtdeOri = 0) then
    VerificaTipoEstqQtde(TipoEstqOri, AreaM2, PesoKg, Pecas,
    GGXOri, MovimCod, Controle, sProcName);
  if (*TemTwn and*) (QtdeDst = 0) then
    VerificaTipoEstqQtde(TipoEstqDst, AreaM2, PesoKg, Pecas,
    GGXDst, MovimCod, Controle, sProcName);
{
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp29X
  else
    TipoRegSPEDProd := trsp30X;
   //
  case TipoRegSPEDProd of
    trsp29X:
    begin
      // K290 - Cabe�alho
      if IDSeq1_K290 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, (*DtMovim,*)
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, MeAviso, IDSeq1_K290);
      end;
      // K291 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, MeAviso);
      // K292 - Itens consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K290, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      // K292 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
    trsp30X:
    begin
      // K230 - Cabe�alho produ��o em terceiros
      if IDSeq1_K300 = -1 then
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(MovimID), Codigo, MovimCod, CodDocOP, (*Controle,*)
        (*DataIni*)DtHrAberto, (*DataFim*)DtHrFimOpe, DtMovim,
        DtCorrApo, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, FDiaFim,
        FTipoPeriodoFiscal, MeAviso, IDSeq1_K300);
      end;
      // K301 - Itens Produzidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXDst, QtdeDst, Integer(MovimID), Codigo, MovimCod, (*CtrlDst*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, MeAviso);
      //
      // K302 - Itens Consumidos
      OriESTSTabSorc := estsVMI;
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1_K300, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
      PaiMovID, MovCodPai, PaiNivel1, PaiNivel2, DtMovim, DtCorrApo,
      GGXOri, QtdeOri, Integer(MovimID), Codigo, MovimCod, (*CtrlOri*)Controle,
      ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
      FTipoPeriodoFiscal, TEstqSPEDTabSorc.estsVMI, MeAviso);
      //
      // K302 - Insumos consumidos
      if IncluiPQX  and (not InseriuPQ) then
        InseriuPQ := InsereB_OrigeProcPQ_Conjunta(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1_K300, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio);
    end;
}
  if Empresa = FornecMO then
    TipoRegSPEDProd := trsp23X
  else
    TipoRegSPEDProd := trsp25X;
   //
  case TipoRegSPEDProd of
    trsp23X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO,
      FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
      FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)Controle, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    trsp25X:
    begin
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, Integer(MovimID), Codigo, MovimCod,
      Controle,
      DtHrFimOpe,
      DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
      QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
      //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
      FPeriApu, IDSeq1, DtHrAberto, DtCorrApo, GGXOri,
      QtdeOri, COD_INS_SUBST, (*ID_Item*)Controle, Integer(MovimID), Codigo, MovimCod,
      Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
      OriESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      //
      if not InseriuPQ then
        InseriuPQ := InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
        (*MovimCod*)MovCodPai, (*IDSeq1*)IDSeq1_K290, FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
        FornecMO, EntiSitio, DtHrAberto);
    end;
    else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(18)');
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_EmOpeProc_215(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv; const QrCab: TmySQLQuery; const SQL_Periodo: String;
  const OrigemOpeProc: TOrigemOpeProc; var IDSeq1: Integer): Boolean;
const
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeProc_215';
var
  DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
  GraGruX, GGXCabDst, ID_Item, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer;
  MyQtd, Qtde, Fator: Double;
  COD_INS_SUBST, TabCab: String;
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_Periodo;
      //trsp23X: Result := SQL_Periodo;
      //trsp25X: Result := SQL_Periodo;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (3)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
begin
  Result := True;
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT cab.GraGruX GGXSrc, cab.GGXDst,  ',
  'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
  'Pecas, PesoKg, AreaM2, DATE(DataHora) DATA, vmi.DtCorrApo, ',
      VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
      'AreaM2', 'PesoKg', 'Pecas'),
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
  'LEFT JOIN vsopecab   cab ON cab.MovimCod=vmi.MovimCod  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  //'LEFT JOIN gragrux ggx ON ggx.Controle=cab.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
  SQL_Periodo,
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  while not QrOri.Eof do
  begin
    Codigo    := QrOri.FieldByName('Codigo').AsInteger;
    MovimCod  := QrOri.FieldByName('MovimCod').AsInteger;
    Controle  := QrOri.FieldByName('Controle').AsInteger;
    ID_Item   := Controle;
    //
    Fator         := 1;
    Data          := QrOri.FieldByName('DATA').AsDateTime;
    GGXCabDst     := QrOri.FieldByName('GGXDst').AsInteger;
    DtMovim       := Data;
    DtCorrApo     := QrOri.FieldByName('DtCorrApo').AsDateTime;
    ClientMO      := QrOri.FieldByName('ClientMO').AsInteger;
    FornecMO      := QrOri.FieldByName('FornecMO').AsInteger;
    EntiSitio     := QrOri.FieldByName('EntiSitio').AsInteger;
    if MovimID = emidEmProcCur then
    begin
(*
      UnDmkDAC_PF.AbreMySQLQuery0(QrCalToCur, Dmod.MyDB, [
      'SELECT DstGGX ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(QrOri.FieldByName('SrcNivel2').AsInteger),
      '']);
      GraGruX := QrCalToCurDstGGX.Value;
*)
      GraGruX := DefineGGXCalToCur('DstGGX', QrOri.FieldByName('SrcNivel2').AsInteger);
    end
    else
      GraGruX     := QrOri.FieldByName('GraGruX').AsInteger;
    COD_INS_SUBST := '';
    case MovimNiv of
      //eminEmOperBxa,
      //eminEmWEndBxa,
      eminDestOper:
      begin
        Fator := 1;
        if GGXCabDst <> GraGruX then
          if GGXCabDst <> 0 then
            COD_INS_SUBST := Geral.FF0(GGXCabDst);
      end;
(*
      eminDestCal,
      eminDestCur:
      begin
       COD_INS_SUBST := ''; // Nada??? Eh o GraGruX!
       Fator := ?;
      end;
*)
      else
      begin
        Result := False;
        Geral.MB_Erro('"MovimNiv": ' + Geral.FF0(Integer(MovimNiv)) +
        ' n�o implemetado em "Insere_OrigeProc()" (1)');
      end;
    end;
    //qual usar? area ou peso?
    MyQtd := 0;
    case  QrOri.FieldByName('Grandeza').AsInteger of
      0: MyQtd := QrOri.FieldByName('Pecas').AsFloat;
      1: MyQtd := QrOri.FieldByName('AreaM2').AsFloat;
      2: MyQtd := QrOri.FieldByName('PesoKg').AsFloat;
      else
      begin
        Geral.MB_Aviso('Grandeza n�o implementada: ' +
        sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak);
      end;
    end;
    //
    Qtde := MyQtd * Fator;
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (7)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
      '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
      'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) +
      sLineBreak + 'Avise a DERMATEK!!!');
      ID_Item := QrOri.FieldByName('Controle').AsInteger;
    if Qtde < 0.001 then
      Geral.MB_ERRO('Quantidade n�o pode ser zero!!! (3)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak + sLineBreak +
      'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
      '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
      'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    //
    case TipoRegSPEDProd of
      trsp21X:
      begin
        EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, IDSeq1, Data, DtMovim, DtCorrApo, GraGruX,
        Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
        Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210ProcVS,
        ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      end;
      else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(19)');
    end;
    //
    QrOri.Next;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_EmOpeProc_Prd(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const Qry: TmySQLQuery; const Fator: Double; const SQL_Periodo,
  SQL_PeriodoPQ: String; const OrigemOpeProc: TOrigemOpeProc;
  const MovimNiv: TEstqMovimNiv);
  //
  procedure Insere_EmOpeProc(const TipoRegSPEDProd: TTipoRegSPEDProd; const
            MovimID: TEstqMovimID; const Qry: TmySQLQuery; const Fator:
            Double; const SQL_Periodo, SQL_CorrApo: String;
            const OrigemOpeProc: TOrigemOpeProc;
            const InsumoSemProducao: Boolean; var IDSeq1: Integer);
  const
    sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_EmOpeProc_Prd > Insere_EmOpeProc()';
  var
    DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd, DtCorrApoCons: TDateTime;
    QtdeProd, QtdeCons, Pecas, PesoKg, AreaM2: Double;
    MyFator, TipoEstq, Codigo, MovimCod, GGXDst, ClientMO, FornecMO,
    EntiSitio: Integer; ESTSTabSorc: TEstqSPEDTabSorc; ID_Item, Controle: Integer;
    //
    function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
    begin
      case TipoRegSPEDProd of
        trsp21X: Result := '';
        trsp23X: Result := SQL_Periodo;
        //trsp25X: Result := '';
        trsp25X: Result := SQL_Periodo;
        trsp26X: Result := SQL_Periodo;
        else
        begin
          Geral.MB_Erro('"FiltroReg" indefinido: ' +
            Geral.FF0(Integer(TipoRegSPEDProd)) + '! (4)');
          Result := '#ERR_PERIODO_';
        end;
      end;
    end;

    procedure InsereAtual_2X0();
    begin
      DtMovim    := FDiaIni; // ver aqui quando periodo nao for mensal!
      //Parei aqui !!!  � asimm???
      if (Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: QtdeProd := Pecas;
          1: QtdeProd := AreaM2;
          2: QtdeProd := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            Qry.SQL.Text);
            //
            QtdeProd := Pecas;
          end;
        end;
        QtdeProd := QtdeProd * MyFator;
      end else
        QtdeProd := 0;
      //
      if QtdeProd < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (8)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeProd, 3, siNegativo), nil)
      else if (QtdeProd = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidEmProcCal: DtHrFimOpe := 0;
          emidEmProcCur: DtHrFimOpe := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade zerada para OP encerrada!!! (6)', nil);
        end;
      end;
      if (*TemTwn and*) (QtdeProd = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
        GGXDst, MovimCod, Controle, sProcName);
      //
      case TipoRegSPEDProd of
    {
        trsp21X:
        begin
      ESTSTabSorc := TEstqSPEDTabSorc.estsND;
      ID_Item     := 0;
      Controle    := 0;
          InsereItemAtual_K215(LinArqPai, DtHrAberto(*Data*), GGXDst(*GraGruX*),
          Qtde, ''(*COD_INS_SUBST*), ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ESTSTabSorc);
          //
        end;
    }
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd, GGXDst, ClientMO,
          FornecMO, EntiSitio, QtdeProd, OrigemOpeProc,
          FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrFimOpe, DtMovim, DtCorrApoProd, GGXDst, ClientMO, FornecMO,
          EntiSitio, QtdeProd, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal,
          MeAviso, IDSeq1);
        end;
        trsp26X:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrConsParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE MovimCod=' + Geral.FF0(MovimCod),
          'AND MovimNiv IN (57) ', //eminEmRRMBxa=57,
          //'AND MovimTwn=' + Geral.FF0(), /
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          'GROUP BY Ano, Mes', // MovimNiv >>> 54 ou 56 para RRM!!!
          '']);
         // Geral.MB_SQL(Self, QrConsParc);
          //
          if QrConsParc.RecordCount > 1 then
            Geral.MB_Erro('ERRO! Avise a Dermatek! QrConsParc.RecordCount > 1!!!');
          //while not QrConsParc.Eof do
          begin
            if QrConsParcAno.Value = 0 then
              DtCorrApoCons := 0
            else
              DtCorrApoCons := EncodeDate(Trunc(QrConsParcAno.Value), Trunc(QrConsParcMes.Value), 1);
            //
            AreaM2 := QrConsParcAreaM2.Value;
            PesoKg := QrConsParcPesoKg.Value;
            Pecas  := QrConsParcPecas.Value;
            //
            //Parei aqui !!!  � asimm???
            if (Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP) (*and (DtHrFimOpe > 1)*) then
            begin
              case TipoEstq of
                0: QtdeCons := Pecas;
                1: QtdeCons := AreaM2;
                2: QtdeCons := PesoKg;
                else
                begin
                  Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                  + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                  'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                  'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                  Qry.SQL.Text);
                  //
                  QtdeCons := Pecas;
                end;
              end;
              QtdeCons := QtdeCons * (*MyFator*) -1;
            end else
              QtdeCons := 0;
            if (*TemTwn and*) (QtdeCons = 0) then
              VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
              GGXDst, MovimCod, Controle, sProcName);
            //
            if QtdeCons < 0 then
              Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (9)' + sLineBreak +
              'Qtde: ' + Geral.FFT(QtdeCons, 3, siNegativo), QrConsParc)
          end;
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K260(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd, DtCorrApoCons, GGXDst,
          ClientMO, FornecMO, EntiSitio,
          //CtrlDst, CtrlBxa,
          QtdeProd, QtdeCons, OrigemOpeProc, FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(20)');
      end;
    end;
  begin
    MyFator    := -1;
    QtdeProd   := 0;
    QtdeCons   := 0;
    TipoEstq   := Qry.FieldByName('Tipoestq').AsInteger;
    Codigo     := Qry.FieldByName('Codigo').AsInteger;
    MovimCod   := Qry.FieldByName('MovimCod').AsInteger;
    DtHrAberto := Qry.FieldByName('DtHrAberto').AsDateTime;
    DtHrFimOpe := Qry.FieldByName('DtHrFimOpe').AsDateTime;
    GGXDst     := Qry.FieldByName('GGXDst').AsInteger;
    //
    if InsumoSemProducao then
    begin
      DtCorrApoProd := 0;
      AreaM2 := 0;
      PesoKg := 0;
      Pecas  := 0;
      //
      InsereAtual_2X0();
      //
    end else
    begin
      case MovimID of
        emidEmProcCal:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE SrcMovID=' + Geral.FF0(Integer(MovimID)),
          'AND SrcNivel1 <> 0 ',
          'AND SrcNivel1=' + Geral.FF0(Codigo),
          // Entradas no estoque pela producao ( sem doc fiscal de compra, remessa, devolucao, etc)
          'AND MovimID IN (' + CO_ALL_CODS_NO_INN_SPED_VS + ')',
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          '']);
        end;
        emidEmProcCur:
        begin
          // Igual a emidEmProcCal!!!
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE SrcMovID=' + Geral.FF0(Integer(MovimID)),
          'AND SrcNivel1 <> 0 ',
          'AND SrcNivel1=' + Geral.FF0(Codigo),
          // Entradas no estoque pela producao ( sem doc fiscal de compra, remessa, devolucao, etc)
          'AND MovimID IN (' + CO_ALL_CODS_NO_INN_SPED_VS + ')',
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          '']);
        end;
        emidEmOperacao, emidEmProcWE, emidEmProcSP, emidEmReprRM:
        begin
          MyFator := 1;
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE MovimCod=' + Geral.FF0(MovimCod),
          'AND MovimNiv IN (9,22,51,56) ', //eminDestOper=9, //eminDestWEnd=22, //eminDestPSP=51,//eminDestRRM=56,
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          '']);
          //Geral.MB_SQL(Self, QrProdParc);
        end;
        else
        begin
          //Result := False;
          Geral.MB_ERRO(
          '"MovimID" n�o implementado em ' + sProcName);
          //Close;
          Exit;
        end;
      end;
      //Geral.MB_SQL(self, QrProdParc);
      //
      QrProdParc.First;
      while not QrProdParc.Eof do
      begin
        //
        if QrProdParcAno.Value = 0 then
          DtCorrApoProd := 0
        else
          DtCorrApoProd := EncodeDate(Trunc(QrProdParcAno.Value), Trunc(QrProdParcMes.Value), 1);
        //
        AreaM2   := QrProdParcAreaM2.Value;
        PesoKg   := QrProdParcPesoKg.Value;
        Pecas    := QrProdParcPecas.Value;
        ClientMO := QrProdParcClientMO.Value;
        FornecMO := QrProdParcFornecMO.Value;
        Controle := QrProdParcControle.Value;
        //
        InsereAtual_2X0();
        //
        QrProdParc.Next;
      end;
    end;
  end;
  //
  function  Insere_OrigeProcVS(TipoRegSPEDProd: TTipoRegSPEDProd; MovimID:
            TEstqMovimID; MovimNiv: TEstqMovimNiv; QrCab: TmySQLQuery;
            IDSeq1: Integer; SQL_PeriodoVS, SQL_CorrApo: String;
            OrigemOpeProc: TOrigemOpeProc; InsumoSemProducao: Boolean): Boolean;
  const
    ESTSTabSorc = estsVMI;
    sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_EmOpeProc_Prd > Insere_OrigeProc()';
    //
    procedure InsereAtual_2X5(Data, DtCorrApo: TDateTime; GraGruX: Integer;
    Qtde: Double; COD_INS_SUBST: String; ID_Item, Codigo, Controle, ClientMO,
    FornecMO, EntiSitio, MovimCod: Integer);
    begin
      case TipoRegSPEDProd of
        //trsp21X:
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtCorrApo, GraGruX, Qtde,
          COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc,
          FTipoPeriodoFiscal, MeAviso);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtCorrApo, GraGruX, Qtde,
          COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc,
          FTipoPeriodoFiscal, MeAviso);
        end;
        trsp26X:
        begin
          //
(*        N�o tem como retornar nada ao estoque, pois a mercadoria foi transfor-
          mada e n�o montada (com um motor por exemplo)!
          //
          InsereItemAtual_K265(LinArqPai, Data, DtCorrApo, GraGruX, QtdeCons,
          QtdeRet, COD_INS_SUBST, ID_Item, MovimID, Codigo, MovimCod, Controle,
          OrigemOpeProc, ESTSTabSorc);
*)
          Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv, MovimCod,
          IDSeq1, SQL_PeriodoPQ, OrigemOpeProc, ClientMO, FornecMO, EntiSitio);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(21)');
      end;
    end;
  var
    Data, DtCorrApo, DtHrAberto, DtHrFimOpe: TDateTime;
    GraGruX, GGXCabDst, ID_Item, Codigo, MovimCod, Controle, ClientMO, FornecMO,
    EntiSitio: Integer;
    MyQtd, Qtde, Fator: Double;
    COD_INS_SUBST, TabCab: String;
    MovimNivInn: TEstqMovimNiv;
    //
  var
    SQL_PeriodoApo: String;
    DtCorrIni, DtCorrFim: TDateTime;
  begin
    Result := True;
    TabCab := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
    'SELECT cab.GraGruX GGXSrc, cab.GGXDst,  ',
    'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
    //'SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, SUM(AreaM2) AreaM2, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(DataHora) DATA, vmi.DtCorrApo, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
    'med.Grandeza  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
    'LEFT JOIN vsopecab  cab ON cab.MovimCod=vmi.MovimCod  ',
    'LEFT JOIN gragrux   ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    SQL_PeriodoVS,
    SQL_CorrApo,
    //'GROUP BY vmi.Controle ', // Sem Soma!
    '']);
    //Geral.MB_SQL(Self, QrOri);
    if InsumoSemProducao then
    begin
      //Geral.MB_SQL(Self, QrCab);
      case TipoRegSPEDProd of
        //trsp21X:
        trsp23X,
        //trsp25X:
        trsp26X:
        begin
          Data      := 0;
          DtCorrApo := 0;
          GraGruX   := QrCab.FieldByName('GraGruX').AsInteger;
          Qtde      := 0;
          COD_INS_SUBST := '';
          ID_Item   := 0;
          Codigo    := QrCab.FieldByName('Codigo').AsInteger;
          Controle  := 0;
          MovimCod  := QrCab.FieldByName('MovimCod').AsInteger;
          // N�o testado!!
          MovimNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
          UnDmkDAC_PF.AbreMySQLQuery0(QrObtCliForSite, Dmod.MyDB, [
          'SELECT vmi.ClientMO, vmi.FornecMO, scc.EntiSitio  ',
          'FROM vsmovits vmi ',
          'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc  ',
          'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo  ',
          'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
          'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
          '']);
          ClientMO  := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
          FornecMO  := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
          EntiSitio  := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
          //
          InsereAtual_2X5(Data, DtCorrApo, GraGruX, Qtde, COD_INS_SUBST,
          ID_Item, Codigo, Controle, ClientMO, FornecMO, EntiSitio, MovimCod);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(22)');
      end;
    end else
    begin
      if QrOri.RecordCount > 0 then
      begin
        QrOri.First;
        while not QrOri.Eof do
        begin
          Codigo    := QrOri.FieldByName('Codigo').AsInteger;
          MovimCod  := QrOri.FieldByName('MovimCod').AsInteger;
          Controle  := QrOri.FieldByName('Controle').AsInteger;
          ID_Item   := Controle;
          //
          Fator         := 1;
          Data          := QrOri.FieldByName('DATA').AsDateTime;
          DtCorrApo     := QrOri.FieldByName('DtCorrApo').AsDateTime;
          GGXCabDst     := QrOri.FieldByName('GGXDst').AsInteger;
          // J� testado!!
          ClientMO      := QrOri.FieldByName('ClientMO').AsInteger;
          FornecMO      := QrOri.FieldByName('FornecMO').AsInteger;
          if MovimID = emidEmProcCur then
          begin
(*
            UnDmkDAC_PF.AbreMySQLQuery0(QrCalToCur, Dmod.MyDB, [
            'SELECT DstGGX ',
            'FROM ' + CO_SEL_TAB_VMI,
            'WHERE Controle=' + Geral.FF0(QrOri.FieldByName('SrcNivel2').AsInteger),
            '']);
            GraGruX := QrCalToCurDstGGX.Value;
*)
            GraGruX := DefineGGXCalToCur('DstGGX', QrOri.FieldByName('SrcNivel2').AsInteger);
          end
          else
            GraGruX     := QrOri.FieldByName('GraGruX').AsInteger;
          COD_INS_SUBST := '';
          case MovimNiv of
            eminEmOperBxa,
            eminEmWEndBxa,
            eminEmPSPBxa:
            begin
              Fator := -1;
              if GGXCabDst <> GraGruX then
                if GGXCabDst <> 0 then
                  COD_INS_SUBST := Geral.FF0(GGXCabDst);
            end;
            eminEmCalBxa,
            eminEmCurBxa:
            begin
             COD_INS_SUBST := ''; // Nada??? Eh o GraGruX!
             Fator := -1;
            end;
            eminEmRRMBxa:
            begin
             COD_INS_SUBST := ''; // N�o existe no manual
             Fator := -1;
            end;
            else
            begin
              Result := False;
              Geral.MB_Erro('"MovimNiv": ' + Geral.FF0(Integer(MovimNiv)) +
              ' n�o implemetado em "Insere_OrigeProc()" (3)');
            end;
          end;
          //qual usar? area ou peso?
          MyQtd := 0;
          case  QrOri.FieldByName('Grandeza').AsInteger of
            0: MyQtd := QrOri.FieldByName('Pecas').AsFloat;
            1: MyQtd := QrOri.FieldByName('AreaM2').AsFloat;
            2: MyQtd := QrOri.FieldByName('PesoKg').AsFloat;
            else
            begin
              Geral.MB_Aviso('Grandeza n�o implementada: ' +
              sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
            'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak);
            end;
          end;
          //
          Qtde := MyQtd * Fator;
          if Qtde < 0 then
            Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (10)' + sLineBreak +
            'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
            sprocName + sLineBreak + sLineBreak +
            'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
            'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
            'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
            '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
            'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
            'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
            Geral.FFT(Fator, 3, siNegativo) + ' = ' +
            Geral.FFT(Qtde, 3, siNegativo) +
            sLineBreak + 'Avise a DERMATEK!!!');
            ID_Item := QrOri.FieldByName('Controle').AsInteger;
          if Qtde < 0.001 then
            Geral.MB_ERRO('Quantidade n�o pode ser zero!!! (3)' + sLineBreak +
            'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak + sLineBreak +
            'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
            'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
            'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
            '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
            'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
            'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
            Geral.FFT(Fator, 3, siNegativo) + ' = ' +
            Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
            sprocName + sLineBreak +
            sLineBreak + 'Avise a DERMATEK!!!');
            //
            //InsereAtual_2X5();
            InsereAtual_2X5(Data, DtCorrApo, GraGruX, Qtde, COD_INS_SUBST,
            ID_Item, Codigo, Controle, ClientMO, FornecMO, EntiSitio, MovimCod);
          //
          QrOri.Next;
        end;
      end else
      // ver se gastou insumo sem produzir produto acabado!
      begin
        // ?!
      end;
    end;
  end;
  //
  var
    SQL_PeriodoApo: String;
    DtCorrIni, DtCorrFim: TDateTime;
  procedure InsereAtualOpeProc_Prd(DtApoIni, DtApoFim: TDateTime);
  var
    DtHrAberto: TDateTime;
    SQL_PeriodoApo: String;
    IDSeq1, MovimCod, MovCodPai, ClientMO, FornecMO, EntiSitio, PsqIMEI,
    PsqMovimNiv: Integer;
    InsumoSemProducao: Boolean;

  begin
    SQL_PeriodoApo := dmkPF.SQL_Periodo('AND DtCorrApo ', DtApoIni, DtApoFim,
      True, True);
    //
    IDSeq1 := 0;
    InsumoSemProducao := False;
    Insere_EmOpeProc(TipoRegSPEDProd, MovimID, Qry, Fator,
    SQL_Periodo, SQL_PeriodoApo, OrigemOpeProc, InsumoSemProducao, IDSeq1);
    //
    MovimCod  := Qry.FieldByName('MovimCod').AsInteger;
    MovCodPai := Qry.FieldByName('MovimCod').AsInteger;
    InsumoSemProducao := IDSeq1 = 0;
    if InsumoSemProducao then
    begin
      // CUIDADO!!! Aqui esta duplicando!!!
      //Geral.MB_Info(Geral.FF0(Qry.FieldByName('MovimCod').AsInteger));
      ReopenOriPQx(MovimCod, SQL_PeriodoPQ, False);
      InsumoSemProducao := QrOri.RecordCount > 0;
      if InsumoSemProducao then
      begin
        //Geral.MB_Info(Geral.FF0(Qry.FieldByName('MovimCod').AsInteger));
        //
        Insere_EmOpeProc(TipoRegSPEDProd, MovimID, Qry, Fator, SQL_Periodo,
        SQL_PeriodoApo, OrigemOpeProc, InsumoSemProducao, IDSeq1);
      end;
      //
    end;
    Insere_OrigeProcVS(TipoRegSPEDProd, MovimID, MovimNiv, Qry, IDSeq1,
    SQL_Periodo, SQL_PeriodoApo, OrigemOpeProc, InsumoSemProducao);
    // Como fazer??
    // ini 2019-01-26
    //   Parei aqui
    //Geral.MB_SQL(Self, Qry);
    //PsqIMEI := Qry.FieldByName('Controle').AsInteger;
    //Geral.MB_Info('Ver se n�o duplica quando tem gra��o de couro prontono recurtimento/acabamento!');
    PsqMovimNiv := Integer(VS_PF.ObtemMovimNivCliForLocDeMovimID(TEstqMovimID(MovimID)));
    DefinePsqIMEI(MovimCod, Integer(MovimID), PsqMovimNiv, PsqIMEI);
    if PsqIMEI <> 0 then
    begin
      DtHrAberto := Qry.FieldByName('DtHrAberto').AsDateTime;
      //
      DefineCliForSiteOnly(PsqIMEI, ClientMO, FornecMO, EntiSitio);
      //
      (*InseriuPQ :=*) InsereB_OrigeProcPQ_Isolada(TipoRegSPEDProd, MovimID, MovimNiv,
      (*MovimCod*)MovCodPai, IDSeq1(*IDSeq1_K290*), FSQL_PeriodoPQ, OrigemOpeProc, ClientMO,
      FornecMO, EntiSitio, DtHrAberto);
    end;
    //Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminEmWEndBxa, MovimCod, LinArqPai, SQL_PeriodoPQ);
    // fim 2019-01-26
  end;
begin
 // Criar query que separa periodos + Periodos de correcao!
   UnDmkDAC_PF.AbreMySQLQuery0(QrPeriodos, Dmod.MyDB, [
  'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
  'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          //'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, ',
   EfdIcmsIpi_PF.SQLPeriodoFiscal(
     FTipoPeriodoFiscal, 'DtCorrApo', 'Periodo', False),
   'FROM ' + CO_TAB_VMI,
   'WHERE MovimCod>0 ',
   'AND DtCorrApo > 0 ',
   'GROUP BY Ano, Mes, Periodo ',
   '']);
   //
   DtCorrIni  := 0;
   DtCorrFim  := 0;
   InsereAtualOpeProc_Prd(0, 0); // Registro normal sem Correcao de apontamento!
   QrPeriodos.First;
   while not QrPeriodos.Eof do
   begin
     EfdIcmsIpi_PF.ObtemDatasDePeriodoSPED(Trunc(QrPeriodosAno.Value),
     Trunc(QrPeriodosMes.Value), QrPeriodosPeriodo.Value, FTipoPeriodoFiscal,
     DtCorrIni, DtCorrFim);
     // Registros com Correcao de apontamento!
     InsereAtualOpeProc_Prd(DtCorrIni, DtCorrFim);
     QrPeriodos.Next
   end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeProcPQ(TipoRegSPEDProd:
  TTipoRegSPEDProd; MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod:
  Integer; IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
  TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer): Boolean;
const
  Fator = -1;
  ESTSTabSorc = estsPQx;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeProcPQ';
var
  Data, DtCorrApo: TDateTime;
  Codigo, Controle, ID_Item, GraGruX(*, ClientMO, FornecMO, EntiSitio*): Integer;
  MyQtd, Qtde: Double;
  //
  COD_INS_SUBST: String;
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := SQL_PeriodoPQ;
      trsp25X: Result := '';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (6)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
begin
  Result := True;
  ReopenOriPQx(MovimCod, SQL_PeriodoPQ, False);
  //
  //Geral.MB_SQL(Self, QrOri); //Parei Aqui!
  QrOri.First;
  while not QrOri.Eof do
  begin
    COD_INS_SUBST := '';
    Codigo   := QrOri.FieldByName('OrigemCodi').AsInteger;
    Controle := QrOri.FieldByName('OrigemCtrl').AsInteger;
    GraGruX  := UnPQx.ObtemGGXdeInsumo(QrOri.FieldByName('Insumo').AsInteger);
    //qual usar? area ou peso?
    MyQtd     := QrOri.FieldByName('Peso').AsFloat;
    Qtde      := MyQtd * Fator;
    Data      := QrOri.FieldByName('DataX').AsDateTime;
    DtCorrApo := QrOri.FieldByName('DtCorrApo').AsDateTime;
    ID_Item   := QrOri.FieldByName('OrigemCtrl').AsInteger;
    //
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (11)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      //'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'ID Item: ' + Geral.FF0(ID_Item) + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Peso: ' + Geral.FFT(MyQtd, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    if Qtde > 0 then
    begin
      case TipoRegSPEDProd of
        //trsp21X:
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtCorrApo, GraGruX, Qtde,
          COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtCorrApo, GraGruX,
          Qtde, COD_INS_SUBST, ID_Item,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
        trsp26X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K265(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtCorrApo, GraGruX,
          (*QtdeCons*) Qtde, (*QtdeRet*)0, COD_INS_SUBST, ID_Item,
          Integer(MovimID), Codigo, MovimCod, Controle, OrigemOpeProc,
          ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(23)');
      end;
    end;
    //
    QrOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeProcVS_210(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const MovimNivInn, MovimNivBxa: TEstqMovimNiv; const QrCab: TmySQLQuery;
  var IDSeq1: Integer; const SQL_PeriodoVS: String;
  const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeProcVS_210';
var
  DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  Qtde, Pecas, PesoKg, AreaM2: Double;
  MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, ClientMO, FornecMO, EntiSitio,
  ID_Item, Controle: Integer;
  //
(*
  procedure Mensagem(TextoBase: String);
  begin
    Geral.MB_ERRO(TextoBase + sLineBreak +
    'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
    'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
    sProcName + sLineBreak +
    sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
    QrProdParc.SQL.Text);
  end;
*)
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_PeriodoVS;
      //trsp23X: Result := SQL_PeriodoVS;
      //trsp25X: Result := SQL_PeriodoVS;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (7)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
begin
  Result := True;
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT cab.GraGruX GGXSrc, cab.GGXDst, ',
  'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
  //'SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, SUM(AreaM2) AreaM2, ',
  'DATE(DataHora) DATA, vmi.DtCorrApo, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, ',
      VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
      //'SUM(AreaM2)', 'SUM(PesoKg)', 'SUM(Pecas)'),
      'AreaM2', 'PesoKg', 'Pecas'),
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
  'LEFT JOIN vsopecab   cab ON cab.MovimCod=vmi.MovimCod  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=cab.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
  SQL_PeriodoVS,
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
  'GROUP BY DATA, Ano, Mes, GGXSrc ',
  '']);
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  while not QrOri.Eof do
  begin
    MyFator := -1;
    Qtde := 0;
    TipoEstq   := QrOri.FieldByName('Tipoestq').AsInteger;
    Codigo     := QrCab.FieldByName('Codigo').AsInteger;
    MovimCod   := QrCab.FieldByName('MovimCod').AsInteger;
    DtHrAberto := QrCab.FieldByName('DtHrAberto').AsDateTime;
    DtHrFimOpe := QrCab.FieldByName('DtHrFimOpe').AsDateTime;
    GGXSrc     := QrOri.FieldByName('GGXSrc').AsInteger;
    ClientMO   := QrOri.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrOri.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrOri.FieldByName('EntiSitio').AsInteger;
    //
    DtMovim    := QrOri.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrOri.FieldByName('DtCorrApo').AsDateTime;
    case MovimID of
      //emidEmProcCal:
      //emidEmProcCur:
      emidEmOperacao:
      begin
        MyFator := 1;
        UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
        'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
        'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
        'Pecas, AreaM2, PesoKg, ',
        'ClientMO, FornecMO, Controle ',
        'FROM ' + CO_SEL_TAB_VMI + ' ',
        'WHERE MovimCod=' + Geral.FF0(MovimCod),
        'AND MovimNiv=' + Geral.FF0(Integer(MovimNivBxa)),
        FiltroPeriodo(TipoRegSPEDProd),
        '']);
        //Geral.MB_SQL(Self, QrProdParc);
      end;
      //emidEmProcWE:
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //Geral.MB_SQL(self, QrProdParc);
    //
    QrProdParc.First;
    while not QrProdParc.Eof do
    begin
      AreaM2 := -QrProdParcAreaM2.Value;
      PesoKg := -QrProdParcPesoKg.Value;
      Pecas  := -QrProdParcPecas.Value;
      Controle := QrProdParcControle.Value;
      //
      //Parei aqui !!!  � asimm???
      if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: Qtde := Pecas;
          1: Qtde := AreaM2;
          2: Qtde := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrOri.SQL.Text);
            //
            Qtde := Pecas;
          end;
        end;
        Qtde := Qtde * MyFator;
      end else
        Qtde := 0;
      if (*TemTwn and*) (Qtde = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2, PesoKg, Pecas,
        GGXSrc, MovimCod, Controle, sProcName);
      //
      if Qtde < 0 then
        (*
        Mensagem('Quantidade n�o pode ser negativa!!! (12)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo))
        *)
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (12)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrOri)
      else if (Qtde = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidEmProcCal: DtHrFimOpe := 0;
          emidEmProcCur: DtHrFimOpe := 0;
          else //Mensagem('Quantidade zerada para OP encerrada!!! (7)');
          Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade zerada para OP encerrada!!! (7)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrOri)
        end;
      end;
      //
      case TipoRegSPEDProd of
        trsp21X:
        begin
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K210(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO,
          FornecMO, EntiSitio, Qtde, OrigemOpeProc, oidk210ProcVS, FDiaFim,
          FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
  (*
        trsp23X:
        begin
          InsereItemAtual_K230(Integer(MovimID), Codigo, MovimCod, DtHrAberto,
          DtHrFimOpe, GGXDst, Qtde, LinArqPai);
        end;
        trsp25X:
        begin
          InsereItemAtual_K250(Integer(MovimID), Codigo, MovimCod, DtHrFimOpe,
          GGXDst, Qtde, LinArqPai);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(24)');
  *)
      end;
      QrProdParc.Next;
    end;
    //
    QrOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibDTA_210(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const MovimNiv: TEstqMovimNiv; const SQL_PeriodoVS: String;
  const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  COD_INS_SUBST = '';
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibDTA_210';
  //
  procedure ObtemDataIniEFim(const Codigo: Integer; var Ini, Fim: TDateTime);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDatas, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _PRE_DESC_ORI_MP_; ',
    'CREATE TABLE _PRE_DESC_ORI_MP_ ',
    'SELECT Controle  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND Codigo=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _PRE_DESC_DTIF_; ',
    ' ',
    'CREATE TABLE _PRE_DESC_DTIF_ ',
    'SELECT vmi.Controle, vmi.DataHora ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND vmi.SrcNivel1=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'INSERT IGNORE INTO _PRE_DESC_DTIF_ ',
    ' ',
    'SELECT vmi.Controle, vmi.DataHora  ',
    'FROM _PRE_DESC_ORI_MP_ ori  ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    '  ON ori.Controle=vmi.GSPJmpNiv2; ',
    ' ',
    'SELECT MIN(DataHora) MinDH,  ',
    'MAX(DataHora) MaxDH  ',
    'FROM _PRE_DESC_DTIF_ ',
    'WHERE Controle > 0 ',
    '']);
    //Geral.MB_SQL(Self, QrDatas);
    //
    Ini := QrDatasMinDH.Value;
    Fim := QrDatasMaxDH.Value;
  end;
  //
  function RegistroK210(const OriMovID: TEstqMovimID; const DtIniOS, DtFimOS:
  TDateTime; var IDSeq1: Integer): Boolean;
  var
  DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
  QtdeSrc, QtdeDst, PecasSrc, PesoKgSrc, AreaM2Src, PecasDst, PesoKgDst,
  AreaM2Dst: Double;
  MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, GGXDst, ID_Item, Controle,
  ClientMO, FornecMO, EntiSitio: Integer;
  EhDoMes: Boolean;
  begin
    IDSeq1     := 0;
    AreaM2Src  := 0;
    PesoKgSrc  := 0;
    PecasSrc   := 0;
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeSrc    := 0;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    DtHrAberto := DtIniOS;
    DtHrFimOpe := DtFimOS;
    GGXSrc     := QrIts.FieldByName('GGXSrc').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GGXDst').AsInteger;
    Controle   := QrIts.FieldByName('RmsNivel2').AsInteger;
    ID_Item    := Controle;
    // 270
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibDTA:
      begin
        AreaM2Src := -QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgSrc := -QrIts.FieldByName('PesoKg').AsFloat;
        PecasSrc  := -QrIts.FieldByName('Pecas').AsFloat;
        //
        AreaM2Dst := QrIts.FieldByName('QtdAntArM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('QtdAntPeso').AsFloat;
        PecasDst  := QrIts.FieldByName('QtdAntPeca').AsFloat;
      end;
      else
      begin
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        Exit;
      end;
    end;
    EhDoMes := (Data >= FDiaIni) and (Data < FDiaPos);
    if EhDoMes then
    begin
      if (PecasSrc <> 0) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: QtdeSrc := PecasSrc;
          1: QtdeSrc := AreaM2Src;
          2: QtdeSrc := PesoKgSrc;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrIts.SQL.Text);
            //
            QtdeSrc := PecasSrc;
          end;
        end;
        QtdeSrc := QtdeSrc * MyFator;
      end else
        QtdeSrc := 0;
      if (*TemTwn and*) (QtdeSrc = 0) then
        VerificaTipoEstqQtde(TipoEstq, AreaM2Src, PesoKgSrc, PecasSrc,
        GGXSrc, MovimCod, Controle, sProcName);
    end else
      QtdeSrc := 0;
    //
    if QtdeSrc < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (13)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo), QrIts);
    //
    case TipoRegSPEDProd of
      trsp21X:
      begin
        // 2017-12-23 (a)
        if Controle <> 0 then
        // Fim 2017-12-23
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K210(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(OriMovID), Codigo, MovimCod, Controle, DtHrAberto,
          DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO, FornecMO, EntiSitio,
          QtdeSrc, OrigemOpeProc, oidk210RibDTA, FDiaFim, FTipoPeriodoFiscal,
          MeAviso, IDSeq1);
        if EhDoMes then
        begin
          if (PecasDst <> 0) (*and (DtHrFimOpe > 1)*) then
          begin
            case TipoEstq of
              0: QtdeDst := PecasDst;
              1: QtdeDst := AreaM2Dst;
              2: QtdeDst := PesoKgDst;
              else
              begin
                Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                QrIts.SQL.Text);
                //
                QtdeDst := PecasDst;
              end;
            end;
            //QtdeDst := QtdeDst * MyFator;
          end else
            QtdeDst := 0;
          if (*TemTwn and*) (QtdeDst = 0) then
            VerificaTipoEstqQtde(TipoEstq, AreaM2Dst, PesoKgDst, PecasDst,
            GGXDst, MovimCod, Controle, sProcName);
          //
          if QtdeDst < 0 then
            Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade n�o pode ser negativa!!! (14)' + sLineBreak +
            'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), QrIts)
          else if (QtdeDst = 0) and (DtHrFimOpe > 1) then
          begin
            case MovimID of
              emidEmProcCal: DtHrFimOpe := 0;
              emidEmProcCur: DtHrFimOpe := 0;
              else Mensagem(sProcName, MovimCod, Integer(MovimID),
              'Quantidade zerada para OP encerrada!!! (8)', QrIts);
            end;
          end;
          //
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtMovim, DtCorrApo, GGXDst,
          QtdeDst, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibDTA,
          ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
      end;
      else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(25)');
    end;
  end;
  //
  //
  function RegistroK215_SubPrd(const IDSeq1: Integer): Boolean;
  var
    Data, DtMovim, DtCorrApo: TDateTime;
    QtdeDst, PecasDst, PesoKgDst, AreaM2Dst: Double;
    Codigo, MovimCod, MyFator, TipoEstq, GGXDst, ID_Item, Controle, MyMovimID,
    ClientMO, FornecMO, EntiSitio: Integer;
  begin
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    MyMovimID  := QrIts.FieldByName('MovimID').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GraGruX').AsInteger;
    Controle   := QrIts.FieldByName('Controle').AsInteger;
    ID_Item    := Controle;
    //
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibDTA:
      begin
        AreaM2Dst := QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('PesoKg').AsFloat;
        PecasDst  := QrIts.FieldByName('Pecas').AsFloat;
      end;
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //
    //Parei aqui !!!  � asimm???
    if (PecasDst <> 0) or (PesoKgDst <> 0)(*and (DtHrFimOpe > 1)*) then
    begin
      case TipoEstq of
        0: QtdeDst := PecasDst;
        1: QtdeDst := AreaM2Dst;
        2: QtdeDst := PesoKgDst;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrIts.SQL.Text);
          //
          QtdeDst := PecasDst;
        end;
      end;
      //QtdeDst := QtdeDst * MyFator;
      QtdeDst := QtdeDst;
    end else
      QtdeDst := 0;
    //
    if (*TemTwn and*) (QtdeDst = 0) then
      VerificaTipoEstqQtde(TipoEstq, AreaM2Dst, PesoKgDst, PecasDst,
      GGXDst, MovimCod, Controle, sProcName);
    if QtdeDst < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (15)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), QrIts);
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa, FPeriApu,
    IDSeq1, Data, DtMovim, DtCorrApo, GGXDst, QtdeDst,
    COD_INS_SUBST, ID_Item, MyMovimID, Codigo, MovimCod, Controle,
    ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibDTA, ESTSTabSorc,
    FTipoPeriodoFiscal, MeAviso);
  end;
  var
    LinArqK210: Integer;
    //Data
    DtIniOS, DtFimOS: TDateTime;
begin
  Result := True;
  // 1. Codigos das entradas de materia-prima que caleirou dentro do mes
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _PRE_GPS_; ',
  'CREATE TABLE _PRE_GPS_ ',
  'SELECT GSPJmpNiv2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE GSPJmpNiv2<>0 ',
  SQL_PeriodoVS,
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_; ',
  'CREATE TABLE _PRE_DESC_ ',
  ' ',
  'SELECT DISTINCT SrcNivel1, 1 Normal ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCur)),
  'AND SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  SQL_PeriodoVS,
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT vmi.Codigo, 2 Normal ',
  'FROM _PRE_GPS_ gps ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  '  ON gps.GSPJmpNiv2=vmi.Controle ',
  '; ',
  ' ',
  'SELECT DISTINCT SrcNivel1 ',
  'FROM _PRE_DESC_ ',
  'ORDER BY SrcNivel1; ',
  ' ',
  '']);
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  PB2.Position := 0;
  PB2.Max := QrOri.RecordCount;
  while not QrOri.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    // 1a. Caleiros das entradas de couro in natura do codigo de entrada atual
    //     para saber o PDA(DTA) (campo RmsGGX)!
    UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
    'SELECT vmi.RmsGGX GGXDst, vmi.JmpGGX GGXSrc, ',
    'cab.Codigo, cab.MovimCod, vmi.SrcNivel2, ',
    'vmi.Controle VMICal, vmi.GraGruX GGXOriCal, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
       VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
       'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'vmi.QtdAntPeso, vmi.QtdAntPeca, vmi.QtdAntArM2, vmi.RmsNivel2, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN vscalcab   cab ON cab.Codigo=vmi.SrcNivel1 ',  // colorado!!!
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND vmi.SrcNivel1=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),  // 1...270...n
    '']);
    //Geral.MB_SQL(Self, QrIts);
    ObtemDataIniEFim(QrOri.FieldByName('SrcNivel1').AsInteger, DtIniOS, DtFimOS);
    //
    QrIts.First;
    while not QrIts.Eof do
    begin
      RegistroK210(TEstqMovimID.emidEmProcCal, DtIniOS, DtFimOS, LinArqK210);
      //
      QrIts.Next;
    end;
    //
    // 1b. Itens de caleiro do codigo atual
    UnDmkDAC_PF.AbreMySQLQuery0(QrMae, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND Codigo=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),
    //SQL_PeriodoVS,
    '']);
    QrMae.First;
    while not QrMae.Eof do
    begin
      // 1b1. Subprodutos gerados do item de entrada atual
      UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
      'SELECT vmi.GraGruX, ',
      'vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
      'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
         VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
         'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
      'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
      'WHERE vmi.GSPJmpNiv2=' + Geral.FF0(QrMae.FieldByName('Controle').AsInteger),
      SQL_PeriodoVS,
      '']);
      //if QrIts.RecordCount > 0 then
        //Geral.MB_SQL(Self, QrIts);
      QrIts.First;
      while not QrIts.Eof do
      begin
        RegistroK215_SubPrd(LinArqK210);
        //
        QrIts.Next;
      end;
      //
      QrMae.Next;
    end;
    //
    QrOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibPDA_210(const TipoRegSPEDProd: TTipoRegSPEDProd;
  const MovimID: TEstqMovimID; const MovimNiv: TEstqMovimNiv;
  const SQL_PeriodoVS: String; const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  COD_INS_SUBST = '';
  ESTSTabSorc = estsVMI;
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Insere_OrigeRibPDA_210';
  //
var
  QrIts: TmySQLQuery;
  PsqIMEI: Integer;
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_PeriodoVS;
      //trsp23X: Result := SQL_PeriodoVS;
      //trsp25X: Result := SQL_PeriodoVS;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (7)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
  //
  // Obtem data inicial e final de movimentos de caleiro e geracao de subprodutos da entrada selecionada!
  procedure ObtemDataIniEFim(const Codigo: Integer; var Ini, Fim: TDateTime);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDatas, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _PRE_DESC_ORI_MP_; ',
    'CREATE TABLE _PRE_DESC_ORI_MP_ ',
    'SELECT Controle  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND Codigo=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _PRE_DESC_DTIF_; ',
    ' ',
    'CREATE TABLE _PRE_DESC_DTIF_ ',
    'SELECT vmi.Controle, vmi.DataHora ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND vmi.SrcNivel1=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'INSERT IGNORE INTO _PRE_DESC_DTIF_ ',
    ' ',
    'SELECT vmi.Controle, vmi.DataHora  ',
    'FROM _PRE_DESC_ORI_MP_ ori  ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    '  ON ori.Controle=vmi.GSPSrcNiv2 ',
    'WHERE vmi.GSPSrcNiv2 <> 0 ',
    'AND vmi.GSPJmpNiv2=0; ',
    ' ',
    'SELECT MIN(DataHora) MinDH,  ',
    'MAX(DataHora) MaxDH  ',
    'FROM _PRE_DESC_DTIF_ ',
    'WHERE Controle > 0 ',
    '']);
    //Geral.MB_SQL(Self, QrDatas);
    //
    Ini := QrDatasMinDH.Value;
    Fim := QrDatasMaxDH.Value;
  end;
  //
  function RegistroK210(const OriMovID: TEstqMovimID; const DtIniOS, DtFimOS:
  TDateTime; var IDSeq1: Integer): Boolean;
  var
    DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
    QtdeSrc, QtdeDst, PecasSrc, PesoKgSrc, AreaM2Src, PecasDst, PesoKgDst,
    AreaM2Dst: Double;
    MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, GGXDst, ID_Item, Controle,
    ClientMO, FornecMO, EntiSitio, VMI_MovCod: Integer;
    EhDoMes: Boolean;
  begin
    IDSeq1     := 0;
    AreaM2Src  := 0;
    PesoKgSrc  := 0;
    PecasSrc   := 0;
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := -1;
    QtdeSrc    := 0;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    VMI_MovCod := QrIts.FieldByName('VMI_MovCod').AsInteger;
    DtHrAberto := DtIniOS;
    DtHrFimOpe := DtFimOS;
    GGXSrc     := QrIts.FieldByName('GGXSrc').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GGXDst').AsInteger;
    Controle   := QrIts.FieldByName('RmsNivel2').AsInteger;
    ID_Item    := Controle;
    // 270
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    PsqIMEI    := QrIts.FieldByName('Controle').AsInteger;
    DefineCliForSiteOnly(PsqIMEI, ClientMO, FornecMO, EntiSitio);
    //
(*
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
*)
    //
    case MovimID of
      emidEmRibPDA:
      begin
        AreaM2Src := QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgSrc := QrIts.FieldByName('PesoKg').AsFloat;
        PecasSrc  := QrIts.FieldByName('Pecas').AsFloat;
        //
        AreaM2Dst := QrIts.FieldByName('QtdAntArM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('QtdAntPeso').AsFloat;
        PecasDst  := QrIts.FieldByName('QtdAntPeca').AsFloat;
      end;
      else
      begin
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        Exit;
      end;
    end;
    //
    EhDoMes := (Data >= FDiaIni) and (Data < FDiaPos);
    if EhDoMes then
    begin
      if (PecasSrc <> 0) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: QtdeSrc := PecasSrc;
          1: QtdeSrc := AreaM2Src;
          2: QtdeSrc := PesoKgSrc;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrIts.SQL.Text);
            //
            QtdeSrc := PecasSrc;
          end;
        end;
        //QtdeSrc := QtdeSrc * MyFator;
        if QtdeSrc < 0 then
          QtdeSrc := -QtdeSrc;
        if (*TemTwn and*) (QtdeSrc = 0) then
          VerificaTipoEstqQtde(TipoEstq, AreaM2Src, PesoKgSrc, PecasSrc,
          GGXSrc, MovimCod, Controle, sProcName);
      end else
        QtdeSrc := 0;
(*  ini 2019-02-07
    end else
      QtdeSrc := 0;
    //
    if ((PecasSrc <> 0) or (PesoKgSrc <> 0)) and (QtdeSrc = 0) then
      //Mensagem(sProcName, MovimCod, Integer(MovimID),
      Mensagem(sProcName, VMI_MovCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (16)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo) + sLineBreak +
      'IME-I: ' + Geral.FF0(PsqIMEI), QrIts);
    //
*)
//
      if ((PecasSrc <> 0) or (PesoKgSrc <> 0)) and (QtdeSrc = 0) then
        //Mensagem(sProcName, MovimCod, Integer(MovimID),
        Mensagem(sProcName, VMI_MovCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (16)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo) + sLineBreak +
        'IME-I: ' + Geral.FF0(PsqIMEI), QrIts);
    end else
    begin
      QtdeSrc := 0;
      //
      MeAviso.Lines.Add('MovimCod ' + Geral.FF0(VMI_MovCod) + ' IME-I ' +
      Geral.FF0(PsqIMEI) + ' QtdeSrc = ' + Geral.FFT(QtdeSrc, 3, siNegativo));
    end;
    //
//    fim 2019-02-07

    case TipoRegSPEDProd of
      trsp21X:
      begin
        // 2017-12-23 (b)
        if Controle <> 0 then
        // Fim 2017-12-23
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K210(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(OriMovID), Codigo, MovimCod, Controle, DtHrAberto,
          DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO, FornecMO, EntiSitio,
          QtdeSrc, OrigemOpeProc, oidk210RibPDA, FDiaFim, FTipoPeriodoFiscal,
          MeAviso, IDSeq1);
        //
        if EhDoMes then
        begin
          if (PecasDst <> 0) (*and (DtHrFimOpe > 1)*) then
          begin
            case TipoEstq of
              0: QtdeDst := PecasDst;
              1: QtdeDst := AreaM2Dst;
              2: QtdeDst := PesoKgDst;
              else
              begin
                Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                QrIts.SQL.Text);
                //
                QtdeDst := PecasDst;
              end;
            end;
            //QtdeDst := QtdeDst * MyFator;
          end else
            QtdeDst := 0;
          //
          if ((PecasDst <> 0) or (PesoKgDst <> 0)) and (QtdeDst = 0) then
          begin
            if (*TemTwn and*) (QtdeSrc = 0) then
              VerificaTipoEstqQtde(TipoEstq, AreaM2Src, PesoKgSrc, PecasSrc,
              GGXSrc, MovimCod, Controle, sProcName);
            if (*TemTwn and*) (QtdeDst = 0) then
              VerificaTipoEstqQtde(TipoEstq, AreaM2Dst, PesoKgDst, PecasDst,
              GGXDst, MovimCod, Controle, sProcName);
            if QtdeDst < 0 then
              Mensagem(sProcName, MovimCod, Integer(MovimID),
              'Quantidade n�o pode ser negativa!!! (17)' + sLineBreak +
              'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), nil)
            else if (QtdeDst = 0) and (DtHrFimOpe > 1) then
            begin
              case MovimID of
                emidEmProcCal: DtHrFimOpe := 0;
                emidEmProcCur: DtHrFimOpe := 0;
                else Mensagem(sProcName, MovimCod, Integer(MovimID),
                'Quantidade zerada para OP encerrada!!! (9)', nil);
              end;
            end;
          end;
          //
          EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtMovim, DtCorrApo, GGXDst,
          QtdeDst, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibPDA,
          ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
      end;
      else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(26)');
    end;
  end;
  //
  //
  function RegistroK215_SubPrd(const IDSeq1: Integer): Boolean;
  var
    Data, DtMovim, DtCorrApo: TDateTime;
    QtdeDst, PecasDst, PesoKgDst, AreaM2Dst: Double;
    Codigo, MovimCod, MyFator, TipoEstq, GGXDst, ID_Item, Controle,
    ClientMO, FornecMO, EntiSitio, MyMovimID: Integer;
  begin
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    MyMovimID  := QrIts.FieldByName('MovimID').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GraGruX').AsInteger;
    Controle   := QrIts.FieldByName('Controle').AsInteger;
    ID_Item    := Controle;
    //
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibPDA:
      begin
        AreaM2Dst := QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('PesoKg').AsFloat;
        PecasDst  := QrIts.FieldByName('Pecas').AsFloat;
      end;
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //
    //Parei aqui !!!  � asimm???
    if (PecasDst <> 0) or (PesoKgDst <> 0)(*and (DtHrFimOpe > 1)*) then
    begin
      case TipoEstq of
        0: QtdeDst := PecasDst;
        1: QtdeDst := AreaM2Dst;
        2: QtdeDst := PesoKgDst;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrIts.SQL.Text);
          //
          QtdeDst := PecasDst;
        end;
      end;
      //QtdeDst := QtdeDst * MyFator;
      QtdeDst := QtdeDst;
    end else
      QtdeDst := 0;
    if (*TemTwn and*) (QtdeDst = 0) then
      VerificaTipoEstqQtde(TipoEstq, AreaM2Dst, PesoKgDst, PecasDst,
      GGXDst, MovimCod, Controle, sProcName);
    //
    if QtdeDst < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (18)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), nil);
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa, FPeriApu,
    IDSeq1, Data, DtMovim, DtCorrApo, GGXDst, QtdeDst,
    COD_INS_SUBST, ID_Item, MyMovimID, Codigo, MovimCod, Controle, ClientMO,
    FornecMO, EntiSitio, OrigemOpeProc, oidk210RibPDA, ESTSTabSorc,
    FTipoPeriodoFiscal, MeAviso);
  end;
var
  LinArqK210, VMI_MovCod: Integer;
  //Data
  DtIniOS, DtFimOS: TDateTime;
begin
  FParar := False;
  QrIts := TmySQLQuery.Create(Dmod);
  try
  Result := True;
  // 1. Codigos das entradas de materia-prima que caleirou dentro do mes
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _PRE_GPS_; ',
  'CREATE TABLE _PRE_GPS_ ',
  'SELECT GSPSrcNiv2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE GSPSrcNiv2<>0 ',
  'AND GSPJmpNiv2=0 ',
  SQL_PeriodoVS,
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_; ',
  'CREATE TABLE _PRE_DESC_ ',
  ' ',
  'SELECT DISTINCT SrcNivel1, 1 Normal ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  'AND SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  SQL_PeriodoVS,
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT vmi.Codigo, 2 Normal ',
  'FROM _PRE_GPS_ gps ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  '  ON gps.GSPSrcNiv2=vmi.Controle ',
  '; ',
  ' ',
  'SELECT DISTINCT SrcNivel1 ',
  'FROM _PRE_DESC_ ',
  'ORDER BY SrcNivel1; ',
  ' ',
  '']);
  //if QrOri.RecordCount > 0 then
    //Geral.MB_Aviso('DEPRRCAR!!! 210 - 1');
  //
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  PB2.Position := 0;
  PB2.Max := QrOri.RecordCount;
  while not QrOri.Eof do
  begin
    if FParar then
      Exit;
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    // 1a. Caleiros das entradas de couro in natura do codigo de entrada atual
    //     para saber o PDA (campo RmsGGX)!
    UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
    'SELECT vmi.RmsGGX GGXDst, vmi.GraGruX GGXSrc, ',
    'cab.Codigo, cab.MovimCod, vmi.Controle, vmi.SrcNivel2, ',
    'vmi.Controle VMICal, vmi.GraGruX GGXOriCal, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
       VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
       'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'vmi.QtdAntPeso, vmi.QtdAntPeca, vmi.QtdAntArM2, vmi.RmsNivel2, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza, ',
    'vmi.MovimCod VMI_MovCod ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN vsinncab   cab ON cab.Codigo=vmi.SrcNivel1 ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND vmi.SrcNivel1=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),  // 1...270...n
    //SQL_PeriodoVS,
    '']);
    //Geral.MB_SQL(Self, QrIts);
    ObtemDataIniEFim(QrOri.FieldByName('SrcNivel1').AsInteger, DtIniOS, DtFimOS);
    //
    QrIts.First;
    while not QrIts.Eof do
    begin
      if FParar then
        Exit;
      //Data := QrIts.FieldByName('DATA').AsDateTime;
      //if (Data >= DiaIni) and (Data < DiaPos) then
      RegistroK210(TEstqMovimID.emidCompra, DtIniOS, DtFimOS, LinArqK210);
      //
      QrIts.Next;
    end;
    //
    // 1b. Itens de entrada de materia-prima do codigo atual
    UnDmkDAC_PF.AbreMySQLQuery0(QrMae, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND Codigo=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),  // 1...270...n
    //SQL_PeriodoVS,
    '']);
    //Geral.MB_SQL(Self, QrMae);
    QrMae.First;
    while not QrMae.Eof do
    begin
      // 1b1. Subprodutos gerados do item de entrada atual
      UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
      'SELECT vmi.GraGruX, ',
      'vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
      'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
         VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
         'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
      'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
      'WHERE vmi.GSPJmpNiv2=0 ',
      'AND vmi.GSPSrcNiv2=' + Geral.FF0(QrMae.FieldByName('Controle').AsInteger),
      SQL_PeriodoVS,
      '']);
      //Geral.MB_SQL(Self, QrIts);
      QrIts.First;
      while not QrIts.Eof do
      begin
        RegistroK215_SubPrd(LinArqK210);
        //
        QrIts.Next;
      end;
      //
      QrMae.Next;
    end;
    //
    QrOri.Next;
  end;
  finally
    QrIts.Free;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.Mensagem(ProcName: String; MovimCod,
  MovimID: Integer; TextoBase: String; Query: TmySQLQuery; Parar: Boolean);
var
  Qry_Text: String;
begin
  if Query <> nil then
    Qry_Text := Query.SQL.Text
  else
    Qry_Text := '';
  //
  Geral.MB_ERRO(TextoBase + sLineBreak +
  'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
  'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
  ProcName + sLineBreak +
  sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
  ProcName + sLineBreak + sLineBreak +
  Qry_Text);
  //
  if Parar then
    FParar := True;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.PesquisaIMECsRelacionados(
  TargetMovimID, PsqMovimID_VMI, PsqMovimID_PQX: TEstqMovimID; PsqMovimNiv_VMI:
  TEstqMovimNiv; SQL_PeriodoVS, SQL_PeriodoPQ: String; IncluiPQx: Boolean);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.PesquisaIMECsRelacionados()';
  //
  procedure InsereVMI();
  var
    SQL_MovimNiv: String;
  begin
    case TargetMovimID of
      TEStqMovimID.emidEmOperacao,
      TEStqMovimID.emidEmProcWE,
      TEStqMovimID.emidEmProcSP,
      TEStqMovimID.emidEmReprRM:
        SQL_MovimNiv := '';
      TEStqMovimID.emidEmProcCal,
      TEStqMovimID.emidCaleado,
      TEStqMovimID.emidEmProcCur,
      TEStqMovimID.emidCurtido:
        SQL_MovimNiv := 'AND MovimNiv=' + Geral.FF0(Integer(PsqMovimNiv_VMI));
      else begin
        Geral.MB_Erro(
        GetEnumName(TypeInfo(TEStqMovimID), Integer(TargetMovimID)) +
        ' indefinido em ' + sprocName);
        //
        SQL_MovimNiv := 'AND MovimNiv=' + Geral.FF0(Integer(PsqMovimNiv_VMI));
      end;
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FSEII_IMEcs,
    'SELECT DISTINCT IF(MovCodPai<>0, MovCodPai, MovimCod) MovCodPai, ',
    'Codigo, MovimCod, MovimID, ',
    'JmpMovID, JmpNivel1, JmpNivel2, ',
    'SrcMovID, SrcNivel1, SrcNivel2, ',
    'YEAR(DtCorrApo) AnoCA, ',
    'MONTH(DtCorrApo) MesCA, ',
    '"VMI" TipoTab, 1 Ativo ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI ,
    'WHERE MovimID=' + Geral.FF0(Integer(PsqMovimID_VMI)),
    SQL_MovimNiv,
    SQL_PeriodoVS,
    '']);
    //Geral.MB_SQL(Self, DModG.QrUpdPID1);
  end;
  procedure InserePQX(Tabela: String; Tipo: Integer);
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FSEII_IMEcs,
    'SELECT DISTINCT VSMovCod MovCodPai, vmc.CodigoID Codigo, ',
    'cab.VSMovCod MovimCod, vmc.MovimID,  ',
    '0 JmpMovID, 0 JmpNivel1, 0 JmpNivel2, ',
    '0 SrcMovID, 0 SrcNivel1, 0 SrcNivel2, ',
    'YEAR(pqx.DtCorrApo) AnoCA, ',
    'MONTH(pqx.DtCorrApo) MesCA, ',
    '"PQX" TipoTab, 1 Ativo ',
    'FROM ' + TMeuDB + '.pqx pqx',
    'LEFT JOIN ' + TMeuDB + '.' + Tabela +' cab ON cab.Codigo=pqx.OrigemCodi ',
    '   AND pqx.Tipo=' + Geral.FF0(Tipo),
    'LEFT JOIN ' + TMeuDB + '.vsmovcab vmc ON vmc.Codigo=cab.VSMovCod',
    'WHERE vmc.MovimID=' + Geral.FF0(Integer(PsqMovimID_PQX)),
    SQL_PeriodoPQ,
    '']);
    //Geral.MB_SQL(Self, DModG.QrUpdPID1);
  end;
begin
  DModG.MyPID_DB.Execute('DELETE FROM ' + FSEII_IMEcs);
  //
  InsereVMI();
  if IncluiPQX then
  begin
    InserePQX('emit', VAR_FATID_0110);
    InserePQX('pqo', VAR_FATID_0190);
  end;
  //
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenObtCliForSite(Forma:
  TOpenCliForSite; Controle, MovimCod, MovimNiv: Integer);
const
  sProcName = 'TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenObtCliForSite()';
var
  SQL_Psq: String;
begin
  case Forma of
    //ocfsIndef
    ocfsControle:    SQL_Psq := 'WHERE vmi.Controle=' + Geral.FF0(Controle);
    ocfsMovIdNivCod: SQL_Psq := 'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod) +
                                ' AND vmi.MovimNiv=' + Geral.FF0(MovimNiv);
    ocfsSrcNivel2:   SQL_Psq := 'WHERE vmi.SrcNivel2=' + Geral.FF0(Controle);
    else
    begin
      SQL_Psq := 'WHERE vmi.???=???';
      Geral.MB_Erro('Forma indefinida em ' + sProcName)
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrObtCliForSite, Dmod.MyDB, [
  'SELECT vmi.MovimID, vmi.MovimNiv, vmi.MovimCod, ',
  'vmi.Codigo, vmi.ClientMO, vmi.FornecMO, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'scc.EntiSitio  ',
  'FROM vsmovits vmi ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc  ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  //'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1  ',
  //'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2  ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  //'WHERE vmi.Controle=' + Geral.FF0(Controle),
  SQL_Psq,
  '']);
  //Geral.MB_SQL(Self, QrObtCliForSite);
  //N�o fechar!!!
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenOriPQx(MovimCod: Integer; SQL_PeriodoPQ: String;
              ShowSQL: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
  'CliDest ClientMO ',
  'FROM pqx ',
  'WHERE ( ',
  '  Tipo=110   ',
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM emit ',
  '    WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'OR ',
  '( ',
  '  Tipo=190   ',
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM PQO ',
  '    WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'AND Peso <> 0',
  SQL_PeriodoPQ,
  '']);
  //
  if ShowSQL then
    Geral.MB_Info(QrOri.SQL.Text);
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenVmiGArCab(TipoRegSPEDProd:
  TTipoRegSPEDProd; SQL_Periodo: String);
begin
  // incluir Tipo_Item dp gg2 ou pgt!
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmiGArCab, Dmod.MyDB, [
  'SELECT gg1.UnidMed, med.Grandeza med_grandeza, ',
  'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(True, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'DATE(vmi.DataHora) Data, vmi.Codigo, vmi.MovimCod, ',
  'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg, ',
  'vmi.QtdGerArM2, ',
  'vmi.MovimTwn, vmi.Controle, DtCorrApo, ',
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
  'IF(gg1.Nivel2 <> 0 AND gg2.Tipo_Item>-1, gg2.Tipo_Item, ',
  '  pgt.Tipo_Item) Tipo_Item ',
  'FROM ' + CO_SEL_TAB_VMI + '        vmi ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCurtiVS)), // 14
  SQL_Periodo,
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
(*
Desabilitado em 23/12/2017 por causa de gera��o direto do Verde/Salgado!
Ver gera��o a partir do curtido!
*)
  'AND vmi.MovimCod IN ( ',
  '  SELECT MovimCod ',
  '  FROM ' + CO_SEL_TAB_VMI + ' ',
  '  WHERE MovimNiv=13 ',
  '  AND FornecMO' + FiltroRegGAR(TipoRegSPEDProd) + Geral.FF0(FEmpresa),
  ') ',
(**)
  'ORDER BY vmi.DtCorrApo, vmi.MovimCod, vmi.GraGruX ',
  '']);
  //Geral.MB_SQL(Self, QrVmiGArCab);
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenVmiGArIts(MovimCod, MovimTwn:
  Integer; SQL_Periodo: String; TipoRegSPEDProd: TTipoRegSPEDProd);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmiGArIts, Dmod.MyDB, [
  'SELECT vmi.SrcMovID, vmi.SrcNivel2, vmi.SrcGGX, vmi.DstGGX, ',
  'vmi.JmpGGX, gg1.UnidMed, med.Grandeza med_grandeza, ',
  'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'DATE(vmi.DataHora) Data, vmi.DtCorrApo, vmi.Codigo, vmi.MovimCod,  ',
  'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg,  ',
  'vmi.MovimTwn, vmi.Controle  ',
  'FROM ' + CO_SEL_TAB_VMI + '        vmi  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiVS)), // 15
  'AND vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimTwn=' + Geral.FF0(MovimTwn),
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
  'AND (vmi.FornecMO=0 ',
  ' OR vmi.FornecMO' + FiltroRegGAR(TipoRegSPEDProd) + Geral.FF0(FEmpresa),
  ')',
  FiltroPeriodoGAr(TipoRegSPEDProd, SQL_Periodo),
  'ORDER BY Data, vmi.MovimCod, vmi.GraGruX ',
  '']);
  //if QrVmiGArIts.RecordCount > 1 then
    //Geral.MB_SQL(Self, QrVmiGArIts);
end;

function TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.ReopenVmiTwn(MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv; MovimCod, MovimTwn: Integer): Boolean;
var
  MovimIDBxa: TEstqMovimID;
  MovimNivBxa: TEstqMovimNiv;
begin
  Result := False;
  if MovimTwn <> 0 then
  begin
    MovimIDBxa := VS_PF.ObtemMovimIDBxaDeMovimID(MovimID);
    MovimNivBxa := VS_PF.ObtemMovimNivBxaDeMovimID(MovimID);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrVmiTwn, Dmod.MyDB, [
    'SELECT *  ',
    'FROM vsmovits ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimID=' + Geral.FF0(Integer(MovimIDBxa)),
    'AND MovimNiv<>' + Geral.FF0(Integer(MovimNiv)),
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNivBxa)),
    'AND MovimTwn=' + Geral.FF0(MovimTwn),
    ' ']);
    Result := QrVmiTwn.RecordCount > 0;
    if not Result then
      Geral.MB_Erro('IME-I n�o localizado para MovimTwn ' + Geral.FF0(MovimTwn) +
      ' e MovimNiv ' + Geral.FF0(Integer(MovimNivBxa)) + ' !!!');
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.VerificaSeTemGraGruX(
  IDLinPas, GraGruX: Integer; ProcName: String; Qry: TmySQLQuery);
var
  SQL: String;
begin
  if GraGruX = 0 then
  begin
    if Qry <> nil then
      SQL := Qry.SQL.Text
    else
      SQL := '';
    Geral.MB_Erro('Reduzido indefinido em ' + ProcName + sLineBreak +
    'IDLinPas: ' + Geral.FF0(IDLinPas) + sLineBreak + SQL);
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a.VerificaTipoEstqQtde(TipoEstq:
  Integer; AreaM2, PesoKg, Pecas: Double; GraGruX, MovimCod, Controle: Integer;
  ProcName: String);
begin
  if (AreaM2 <> 0) or (PesoKg <> 0) or (Pecas <> 0) then
  begin
    Geral.MB_Erro(
    'Quantidade zerada para  "TipoEstq" = ' + Geral.FF0(TipoEstq) + ' -> ' +
    sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
    ProcName + sLineBreak +
    'Area m�: ' + Geral.FFT(AreaM2, 2, siNegativo) + sLineBreak +
    'Peso kg: ' + Geral.FFT(PesoKg, 3, siNegativo) + sLineBreak +
    'Pe�as: ' + Geral.FFT(Pecas, 2, siNegativo) + sLineBreak +
    'Reduzido = ' + Geral.FF0(GraGruX) + sLineBreak +
    'IME-C = ' + Geral.FF0(MovimCod) + sLineBreak +
    'IME-I = ' + Geral.FF0(COntrole) + sLineBreak);
  end;
end;

(*
object QrPQMGerOrigemCodi: TIntegerField
  FieldName = 'OrigemCodi'
end


object QrPQMGerTipo: TIntegerField
  FieldName = 'Tipo'
end
object QrPQMGerCliOrig: TIntegerField
  FieldName = 'CliOrig'
end
object QrPQMGerCliDest: TIntegerField
  FieldName = 'CliDest'
end
object QrPQMGerValor: TFloatField
  FieldName = 'Valor'
  DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
end
object QrPQMGerRetorno: TSmallintField
  FieldName = 'Retorno'
end
object QrPQMGerStqMovIts: TIntegerField
  FieldName = 'StqMovIts'
end
object QrPQMGerRetQtd: TFloatField
  FieldName = 'RetQtd'
end
object QrPQMGerHowLoad: TSmallintField
  FieldName = 'HowLoad'
end
object QrPQMGerStqCenLoc: TIntegerField
  FieldName = 'StqCenLoc'
end
object QrPQMGerSdoPeso: TFloatField
  FieldName = 'SdoPeso'
end
object QrPQMGerSdoValr: TFloatField
  FieldName = 'SdoValr'
end
object QrPQMGerAcePeso: TFloatField
  FieldName = 'AcePeso'
end
object QrPQMGerAceValr: TFloatField
  FieldName = 'AceValr'
end
object QrPQMGerDtCorrApo: TDateField
  FieldName = 'DtCorrApo'
end
object QrPQMGerEmpresa: TIntegerField
  FieldName = 'Empresa'
end
object QrPQMGerAlterWeb: TSmallintField
  FieldName = 'AlterWeb'
end
object QrPQMGerAtivo: TSmallintField
  FieldName = 'Ativo'
end
object QrPQMGerNOMEPQ: TWideStringField
  FieldName = 'NOMEPQ'
  Size = 50
end
object QrPQMGerGrupoQuimico: TIntegerField
  FieldName = 'GrupoQuimico'
end
object QrPQMGerNOMEGRUPO: TWideStringField
  FieldName = 'NOMEGRUPO'
  Size = 100
end
object QrPQMGerSigla: TWideStringField
  FieldName = 'Sigla'
  Size = 6
end
*)
//Colocar opcao bderm de isolada Direta no sped? conjunta no
//classificar couro a couro

// Tags pesquisa recurtimento
//DefinePsqIMEI(
//ReopenOriPQx
//Insere_OrigeProcPQ ????

//InsereItemProducaoConjuntaGemeos(
// InsumoSemProducao
end.
