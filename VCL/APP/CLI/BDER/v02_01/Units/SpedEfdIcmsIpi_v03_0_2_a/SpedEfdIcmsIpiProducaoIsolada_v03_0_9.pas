unit SpedEfdIcmsIpiProducaoIsolada_v03_0_9;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  mySQLDbTables,
  //
  StdCtrls, ExtCtrls, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt,(* DbTables,*) DmkCoding, dmkEdit,
  dmkRadioGroup, dmkMemo, dmkCheckGroup, UnMyObjects, MyDBCheck,
  UnDmkProcFunc, UnProjGroup_Consts, AppListas,
  dmkLabel, dmkImage,
  DBCtrls,
  dmkDBLookupComboBox, dmkEditCB, dmkEditDateTimePicker,
  dmkDBGridZTO, SPED_Listas, TypInfo, UnVS_EFD_ICMS_IPI, UnEfdIcmsIpi_PF,
  UnAppEnums;

type
  TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9 = class(TForm)
    QrDatas: TmySQLQuery;
    QrDatasMinDH: TDateTimeField;
    QrDatasMaxDH: TDateTimeField;
    QrOri: TmySQLQuery;
    QrMae: TmySQLQuery;
    QrMC: TmySQLQuery;
    QrMCMovimCod: TIntegerField;
    QrProdRib: TmySQLQuery;
    QrProdRibData: TDateField;
    QrProdRibGGX_Dst: TIntegerField;
    QrProdRibGGX_Src: TIntegerField;
    QrProdRibPecas: TFloatField;
    QrProdRibAreaM2: TFloatField;
    QrProdRibPesoKg: TFloatField;
    QrProdRibMovimID: TIntegerField;
    QrProdRibCodigo: TIntegerField;
    QrProdRibMovimCod: TIntegerField;
    QrProdRibControle: TIntegerField;
    QrProdRibEmpresa: TIntegerField;
    QrProdRibQtdAntPeca: TFloatField;
    QrProdRibQtdAntArM2: TFloatField;
    QrProdRibQtdAntPeso: TFloatField;
    QrProdRibsTipoEstq: TFloatField;
    QrProdRibdTipoEstq: TFloatField;
    QrProdRibMovimNiv: TIntegerField;
    QrProdRibDtCorrApo: TDateTimeField;
    QrProdRibClientMO: TIntegerField;
    QrProdRibFornecMO: TIntegerField;
    QrProdRibEntiSitio: TIntegerField;
    QrObtCliForSite: TmySQLQuery;
    QrIts: TmySQLQuery;
    QrCalToCur: TmySQLQuery;
    QrCalToCurDstGGX: TIntegerField;
    QrConsParc: TmySQLQuery;
    QrConsParcPecas: TFloatField;
    QrConsParcAreaM2: TFloatField;
    QrConsParcPesoKg: TFloatField;
    QrConsParcAno: TFloatField;
    QrConsParcMes: TFloatField;
    QrProdParc: TmySQLQuery;
    QrProdParcPecas: TFloatField;
    QrProdParcAreaM2: TFloatField;
    QrProdParcPesoKg: TFloatField;
    QrProdParcClientMO: TIntegerField;
    QrProdParcFornecMO: TIntegerField;
    QrProdParcControle: TIntegerField;
    QrProdParcAno: TFloatField;
    QrProdParcMes: TFloatField;
    QrPeriodos: TmySQLQuery;
    QrPeriodosAno: TFloatField;
    QrPeriodosMes: TFloatField;
    QrPeriodosPeriodo: TIntegerField;
    QrVmiGArCab: TmySQLQuery;
    QrVmiGArCabData: TDateField;
    QrVmiGArCabCodigo: TIntegerField;
    QrVmiGArCabMovimCod: TIntegerField;
    QrVmiGArCabGraGruX: TIntegerField;
    QrVmiGArCabPecas: TFloatField;
    QrVmiGArCabAreaM2: TFloatField;
    QrVmiGArCabPesoKg: TFloatField;
    QrVmiGArCabMovimTwn: TIntegerField;
    QrVmiGArCabUnidMed: TIntegerField;
    QrVmiGArCabmed_grandeza: TIntegerField;
    QrVmiGArCabxco_grandeza: TIntegerField;
    QrVmiGArCabGraGruY: TIntegerField;
    QrVmiGArCabCouNiv2: TIntegerField;
    QrVmiGArCabControle: TIntegerField;
    QrVmiGArCabTipoEstq: TFloatField;
    QrVmiGArCabDtCorrApo: TDateTimeField;
    QrVmiGArCabClientMO: TIntegerField;
    QrVmiGArCabFornecMO: TIntegerField;
    QrVmiGArCabEntiSitio: TIntegerField;
    QrSumGAR: TmySQLQuery;
    QrSumGARPecas: TFloatField;
    QrSumGARAreaM2: TFloatField;
    QrSumGARPesoKg: TFloatField;
    QrVmiGArIts: TmySQLQuery;
    QrVmiGArItsData: TDateField;
    QrVmiGArItsCodigo: TIntegerField;
    QrVmiGArItsMovimCod: TIntegerField;
    QrVmiGArItsGraGruX: TIntegerField;
    QrVmiGArItsPecas: TFloatField;
    QrVmiGArItsAreaM2: TFloatField;
    QrVmiGArItsPesoKg: TFloatField;
    QrVmiGArItsMovimTwn: TIntegerField;
    QrVmiGArItsUnidMed: TIntegerField;
    QrVmiGArItsmed_grandeza: TIntegerField;
    QrVmiGArItsxco_grandeza: TIntegerField;
    QrVmiGArItsGraGruY: TIntegerField;
    QrVmiGArItsCouNiv2: TIntegerField;
    QrVmiGArItsTipoEstq: TLargeintField;
    QrVmiGArItsControle: TIntegerField;
    QrVmiGArItsDstGGX: TIntegerField;
    QrVmiGArItsSrcGGX: TIntegerField;
    QrVmiGArItsSrcNivel2: TIntegerField;
    QrVmiGArItsSrcMovID: TIntegerField;
    QrVmiGArItsDtCorrApo: TDateTimeField;
  private
    { Private declarations }

  public
    { Public declarations }
    FImporExpor, FEmpresa, FAnoMes, FPeriApu: Integer;
    FDiaIni, FDiaFim, FDiaPos(*, FData*): TDateTime;
    FTipoPeriodoFiscal: TTipoPeriodoFiscal;
    PB2: TProgressBar;
    LaAviso1, LaAviso2, LaAviso3, LaAviso4: TLabel;
    MeAviso: TMemo;
    //
    function  ImportaGerArt(SQL_PeriodoComplexo, SQL_Periodo, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ImportaOpeProcA(QrCab: TMySQLQuery; MovimID: TEstqMovimID;
              SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ImportaOpeProcB(QrCab: TMySQLQuery; PsqMovimNiv: TEstqMovimNiv;
              SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  Insere_OrigeProcPQ(TipoRegSPEDProd: TTipoRegSPEDProd; MovimID:
              TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod: Integer;
              IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
              TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer): Boolean;
    function  Insere_OrigeRibDTA_210(const TipoRegSPEDProd:
              TTipoRegSPEDProd; const MovimID: TEstqMovimID; const MovimNiv:
              TEstqMovimNiv; const SQL_PeriodoVS: String; const OrigemOpeProc:
              TOrigemOpeProc): Boolean;
    function  Insere_OrigeRibPDA_210(const TipoRegSPEDProd:
              TTipoRegSPEDProd; const MovimID: TEstqMovimID; const MovimNiv:
              TEstqMovimNiv; const SQL_PeriodoVS: String; const OrigemOpeProc:
              TOrigemOpeProc): Boolean;
    function  Insere_OrigeProcVS_210(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; const MovimNivInn, MovimNivBxa:
              TEstqMovimNiv; const QrCab: TmySQLQuery; var IDSeq1: Integer;
              const SQL_PeriodoVS: String; const OrigemOpeProc: TOrigemOpeProc):
              Boolean;
    function  Insere_EmOpeProc_215(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; const QrCab:
              TmySQLQuery; (*const Fator: Double;*) const SQL_Periodo: String;
              const OrigemOpeProc: TOrigemOpeProc; var IDSeq1: Integer):
              Boolean;
    procedure Insere_EmOpeProc_Prd(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; const Qry: TmySQLQuery; const Fator:
              Double; const SQL_Periodo, SQL_PeriodoPQ: String; const
              OrigemOpeProc: TOrigemOpeProc; const MovimNiv: TEstqMovimNiv);
    //
    function  GrandezasInconsistentes(): Boolean;
    procedure Mensagem(ProcName: String; MovimCod, MovimID: Integer; TextoBase:
              String; Query: TmySQLQuery);
  end;

var
  FmSpedEfdIcmsIpiProducaoIsolada_v03_0_9: TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9;

implementation
uses
  Module, UMySQLModule, ModuleGeral, UnVS_PF, ModAppGraG1, PQx, DmkDAC_PF,
  UnEfdIcmsIpi_PF_v03_0_9, GraGruX, UnGrade_PF;


{$R *.dfm}

{ TFmSpedEfdIcmsIpiProducaoIsolada }

function TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9.GrandezasInconsistentes: Boolean;
var
  SQL_PeriodoVMI: String;
begin
  Result := False;
  SQL_PeriodoVMI := dmkPF.SQL_Periodo('WHERE vmi.DataHora ', FDiaIni, FDiaFim, True, True);
  //
  if DfModAppGraG1.GrandesasInconsistentes([
  'DROP TABLE IF EXISTS _TESTE_; ',
  'CREATE TABLE _TESTE_ ',
  'SELECT DISTINCT ggx.GraGru1 Nivel1, ggx.Controle Reduzido,   ',
  'gg1.Nome NO_GG1, med.Sigla,  ',
  'CAST(med.Grandeza AS DECIMAL(11,0)) Grandeza, ',
  'CAST(xco.Grandeza AS DECIMAL(11,0)) xco_Grandeza,   ',
  'CAST(IF(xco.Grandeza > 0, xco.Grandeza + 0.000,   ',
  '  IF(ggx.GraGruY=1024 AND med.Grandeza = 0, 0.000,  ',
  '  IF((ggx.GraGruY<2048 OR xco.CouNiv2<>1), 2.000,   ',
  '  IF((ggx.GraGruY=2048 AND vmi.AreaM2=0 AND vmi.PesoKg<>0), med.Grandeza, ',
  '  IF(xco.CouNiv2=1, 1.000, -1.000))))) AS DECIMAL(11,0)) dif_Grandeza  ',
  '  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle   ',
  'LEFT JOIN ' + TMeuDB + '.couniv1    nv1 ON nv1.Codigo=xco.CouNiv1   ',
  'LEFT JOIN ' + TMeuDB + '.couniv2    nv2 ON nv2.Codigo=xco.CouNiv2   ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed  ',
  SQL_PeriodoVMI,
  'AND vmi.GraGruX <> 0  ',
  'AND ((IF(xco.Grandeza > 0, xco.Grandeza + 0.000,   ',
  '  IF(ggx.GraGruY=1024 AND med.Grandeza = 0, 0.000,  ',
  '  IF((ggx.GraGruY<2048 OR xco.CouNiv2<>1), 2.000,   ',
  '  IF((ggx.GraGruY=2048 AND vmi.AreaM2=0 AND vmi.PesoKg<>0), med.Grandeza, ',
  '  IF(xco.CouNiv2=1, 1.000, -1.000)))) <> med.Grandeza + 0.000))) ; ',
  ' ',
  '/**/ ',
  ' ',
  'SELECT * FROM _TESTE_ ',
  'WHERE dif_Grandeza <> Grandeza ',
  '; ',
  ''], DModG.MyPID_DB) then
  begin
    Result := True;
    Close;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9.ImportaGerArt(SQL_PeriodoComplexo,
  SQL_Periodo, SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  MovimID = TEstqMovimID.emidIndsXX;
  ESTSTabSorc = estsVMI;
  sProcName = 'FmSpedEfdIcmsIpiProducaoIsolada.ProducaoSingular_ImportaGerArt()';
  //
  function FiltroReg(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := '=';
      trsp25X: Result := '<>';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (1)');
        Result := '#ERR';
      end;
    end;
  end;
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := SQL_Periodo;
      //trsp25X: Result := SQL_DtHrFimOpe;
      //trsp25X: Result := SQL_Periodo;
      trsp25X: Result := '';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido!');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
  //
  function InsereRegistros(TipoRegSPEDProd: TTipoRegSPEDProd): Boolean;
  var
    MovimCod, MovimTwn, Codigo, GraGruX, TipoEstq, MyFatorCab, MyFAtorIts,
    IDSeq1, ID_Item, Controle, ClientMO, FornecMO, EntiSitio: Integer;
    DtHrAberto, DtHrFimOpe: TDateTime;
    AreaM2, PesoKg, Pecas, Qtde: Double;
    COD_INS_SUBST: String;
    //
    DtMovim, DtCorrApo: TDateTime;
  begin
    COD_INS_SUBST := '';
    MyFatorCab := 1;
    MyFatorIts := -1;
    //
////////////////////////////////////////////////////////////////////////////////
///  Cabecalho > Registro K230 ou K250
////////////////////////////////////////////////////////////////////////////////
    UnDmkDAC_PF.AbreMySQLQuery0(QrVmiGArCab, Dmod.MyDB, [
    'SELECT gg1.UnidMed, med.Grandeza med_grandeza, ',
    'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'TipoEstq', True,
    'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'DATE(vmi.DataHora) Data, vmi.Codigo, vmi.MovimCod, ',
    'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg, ',
    'vmi.MovimTwn, vmi.Controle, DtCorrApo, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio ',
    'FROM ' + CO_SEL_TAB_VMI + '        vmi ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCurtiXX)), // 14
    SQL_Periodo,
    'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
    'AND vmi.MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimNiv=13 ',
    '  AND FornecMO' + FiltroReg(TipoRegSPEDProd) + Geral.FF0(FEmpresa),
    ') ',
    'ORDER BY vmi.DtCorrApo, vmi.MovimCod, vmi.GraGruX ',
    '']);
    //Geral.MB_SQL(Self, QrVmiGArCab);
    PB2.Position := 0;
    PB2.Max := QrVmiGArCab.RecordCount;
    QrVmiGArCab.First;
    while not QrVmiGArCab.Eof do
    begin
      MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
      //
      Codigo     := QrVmiGArCabCodigo.Value;
      MovimCod   := QrVmiGArCabMovimCod.Value;
      MovimTwn   := QrVmiGArCabMovimTwn.Value;
      DtHrAberto := QrVmiGArCabData.Value;
      DtHrFimOpe := QrVmiGArCabData.Value;
      DtMovim    := QrVmiGArCabData.Value;
      DtCorrApo  := QrVmiGArCabDtCorrApo.Value;
      GraGruX    := QrVmiGArCabGraGruX.Value;
      TipoEstq   := Trunc(QrVmiGArCabTipoEstq.Value);
      ClientMO   := QrVmiGArCabClientMO.Value;
      FornecMO   := QrVmiGArCabFornecMO.Value;
      EntiSitio  := QrVmiGArCabEntiSitio.Value;
      Controle   := QrVmiGArCabControle.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumGAR, Dmod.MyDB, [
      'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,  ',
      'SUM(AreaM2) AreaM2 ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)),
      'AND GraGruX=' + Geral.FF0(GraGruX),
      '']);
      AreaM2     := QrSumGARAreaM2.Value;
      PesoKg     := QrSumGARPesoKg.Value;
      Pecas      := QrSumGARPecas.Value;
      //
      if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
      begin
        case TipoEstq of
          0: Qtde := Pecas;
          1: Qtde := AreaM2;
          2: Qtde := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
            //
            Qtde := Pecas;
          end;
        end;
        Qtde := Qtde * MyFatorCab;
      end else
        Qtde := 0;
      //
      if Qtde < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (2)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrVmiGArCab)
      else if (Qtde = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidIndsXX: DtHrFimOpe := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade zerada para OP encerrada!!! (2)', QrVmiGArCab);
        end;
      end;
      //
      case TipoRegSPEDProd of
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod,
          Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GraGruX, ClientMO,
          FornecMO, EntiSitio, Qtde, OrigemOpeProc,
          FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod,
          Controle,
          DtHrFimOpe,
          DtMovim, DtCorrApo, GraGruX, ClientMO, FornecMO, EntiSitio,
          Qtde, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, (*LinArqPai*)IDSeq1);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(2)');
      end;
////////////////////////////////////////////////////////////////////////////////
///  Item > Registro K235 ou K255
////////////////////////////////////////////////////////////////////////////////
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrVmiGArIts, Dmod.MyDB, [
      'SELECT vmi.SrcMovID, vmi.SrcNivel2,  vmi.SrcGGX, vmi.DstGGX, ',
      'gg1.UnidMed, med.Grandeza med_grandeza, ',
      'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
      VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
      'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
      'DATE(vmi.DataHora) Data, vmi.DtCorrApo, vmi.Codigo, vmi.MovimCod,  ',
      'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg,  ',
      'vmi.MovimTwn, vmi.Controle  ',
      'FROM ' + CO_SEL_TAB_VMI + '        vmi  ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiXX)), // 15
      'AND vmi.MovimCod=' + Geral.FF0(MovimCod),
      'AND vmi.MovimTwn=' + Geral.FF0(MovimTwn),
      'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
      'AND (vmi.FornecMO=0 ',
      ' OR vmi.FornecMO' + FiltroReg(TipoRegSPEDProd) + Geral.FF0(FEmpresa),
      ')',
      FiltroPeriodo(TipoRegSPEDProd),
      'ORDER BY Data, vmi.MovimCod, vmi.GraGruX ',
      '']);
      //if QrVmiGArIts.RecordCount > 1 then
      //Geral.MB_SQL(Self, QrVmiGArIts);
      QrVmiGArIts.First;
      while not QrVmiGArIts.Eof do
      begin
        if TEstqMovimID(QrVmiGArItsSrcMovID.Value) = emidEmProcCur then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrCalToCur, Dmod.MyDB, [
          'SELECT DstGGX ',
          'FROM ' + CO_SEL_TAB_VMI,
          'WHERE Controle=' + Geral.FF0(QrVmiGArItsSrcNivel2.Value),
          '']);
          GraGruX := QrCalToCurDstGGX.Value;
        end
        else
          GraGruX := QrVmiGArItsGraGruX.Value;
        //
        Codigo     := QrVmiGArItsCodigo.Value;
        MovimCod   := QrVmiGArItsMovimCod.Value;
        MovimTwn   := QrVmiGArItsMovimTwn.Value;
        DtHrAberto := QrVmiGArItsData.Value;
        DtHrFimOpe := QrVmiGArItsData.Value;
        TipoEstq   := QrVmiGArItsTipoEstq.Value;
        Controle   := QrVmiGArItsControle.Value;
        ID_Item    := Controle;
        //
        AreaM2     := QrVmiGArItsAreaM2.Value;
        PesoKg     := QrVmiGArItsPesoKg.Value;
        Pecas      := QrVmiGArItsPecas.Value;
        //
        DtCorrApo  := QrVmiGArItsDtCorrApo.Value;
        //
        if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
        begin
          case TipoEstq of
            0: Qtde := Pecas;
            1: Qtde := AreaM2;
            2: Qtde := PesoKg;
            else
            begin
              Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
              + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
              //
              Qtde := Pecas;
            end;
          end;
          Qtde := Qtde * MyFatorIts;
        end else
          Qtde := 0;
        //
        if Qtde < 0 then
          Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade n�o pode ser negativa!!! (3)' + sLineBreak +
          'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrVmiGArIts)
        else if (Qtde = 0) and (DtHrFimOpe > 1) then
        begin
          case MovimID of
            emidIndsXX: DtHrFimOpe := 0;
            else Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade zerada para OP encerrada!!! (3)', QrVmiGArIts);
          end;
        end;
        //
        case TipoRegSPEDProd of
          //trsp21X:
          trsp23X:
          begin
            EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, (*LinArqPai*)IDSeq1, DtHrAberto, DtCorrApo, GraGruX,
            Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
            Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
            ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
          end;
          trsp25X:
          begin
            EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
            FPeriApu, (*LinArqPai*)IDSeq1, DtHrAberto, DtCorrApo, GraGruX,
            Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
            Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
            ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
          end;
          else
            Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
            TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(3)');
        end;
        //
        QrVmiGArIts.Next;
      end;
      //
      QrVmiGArCab.Next;
    end;
  end;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_GAR;',
  'CREATE TABLE _SPED_EFD_K2XX_GAR',
  'SELECT vmi.DstGGX, vmi.SrcGGX, ',
  'gg1.UnidMed, med.Grandeza med_grandeza, ',
  'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'DATE(vmi.DataHora) Data, vmi.Codigo, vmi.MovimCod,  ',
  'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg,  ',
  'vmi.MovimTwn  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '        vmi   ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiXX)),      //15
  SQL_Periodo,
  'ORDER BY Data, vmi.MovimCod, vmi.GraGruX ;',
  '']);
  if EfdIcmsIpi_PF.ErrGrandeza('_SPED_EFD_K2XX_GAR', 'SrcGGX', 'DstGGX') then
    Exit;
  //
  InsereRegistros(trsp23X);
  InsereRegistros(trsp25X);
end;

function TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9.ImportaOpeProcA(QrCab: TMySQLQuery;
  MovimID: TEstqMovimID; SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ,
  SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
var
  TabSrc, SQL_FLD, SQL_AND: String;
  MovNivInn, MovNivBxa: TEstqMovimNiv;
  //
  function FiltroReg(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := '=';
      trsp25X: Result := '<>';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (2)');
        Result := '#ERR';
      end;
    end;
  end;
  //
  function FiltroCond(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := ' OR ';
      trsp25X: Result := ' AND ';
      else
      begin
        Geral.MB_Erro('"FiltroCond" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (2a)');
        Result := '#ERR';
      end;
    end;
  end;
  //
  function ReabreEmProcesso21X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    SQL_PeriodoComplexo,
    'OR MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    '  ' + SQL_PeriodoVS,
    ')) ',
    SQL_AND,

    ' ',
    'AND MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv=' + Geral.FF0(Integer(MovNivInn)),
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    //'  AND FornecMO' + FiltroReg(trsp21X) + Geral.FF0(FEmpresa) +
    '  )',
    ') ',
    ' ',
    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function ReabreEmProcesso23X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    //'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    SQL_PeriodoComplexo,
    'OR MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    '  ' + SQL_PeriodoVS,
    ')) ',
    SQL_AND,

    ' ',
    'AND MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv=' + Geral.FF0(Integer(MovNivInn)),
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    '  AND (FornecMO' + FiltroReg(trsp23X) + Geral.FF0(FEmpresa),
    '  ' + FiltroCond(trsp23X) + ' FornecMO' + FiltroReg(trsp23X) + '0) ) ',
    ') ',
    ' ',

    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
(*
    if Query.RecordCount > 0 then Geral.MB_SQL(Self, Query);
*)
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function ReabreEmProcesso25X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    'WHERE  MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv IN (' + Geral.FF0(Integer(MovNivInn)) + ',' +
    Geral.FF0(Integer(MovNivBxa)) + ') ',
    //
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    '  AND (FornecMO' + FiltroReg(trsp25X) + Geral.FF0(FEmpresa),
    '  ' + FiltroCond(trsp25X) + ' FornecMO' + FiltroReg(trsp25X) + '0) ) ',
    ' ' + SQL_PeriodoVS,
    ') ',
    ' ',
    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
(*
    if Query.RecordCount > 0 then Geral.MB_SQL(Self, Query);
*)
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function ReabreEmProcesso26X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    SQL_PeriodoComplexo,
    'OR MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    '  ' + SQL_PeriodoVS,
    ')) ',
    SQL_AND,

    ' ',
    'AND MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv=' + Geral.FF0(Integer(MovNivInn)),
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    '  )',
    ') ',
    ' ',
    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
(*
    if Query.RecordCount > 0 then Geral.MB_SQL(Self, Query);
*)
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function InsereRegistros(TipoRegSPEDProd: TTipoRegSPEDProd): Boolean;
  var
    LinArqPai, MovimCod: Integer;
    Fator: Double;
    Abriu: Boolean;
  begin
    Result := False;
    case TipoRegSPEDProd of
      trsp21X: Abriu := ReabreEmProcesso21X(QrCab);
      trsp23X: Abriu := ReabreEmProcesso23X(QrCab);
      trsp25X: Abriu := ReabreEmProcesso25X(QrCab);
      trsp26X: Abriu := ReabreEmProcesso26X(QrCab);
      else
      begin
        Geral.MB_Aviso('Tipo de registro indefinido em "InsereRegistros()"');
        Abriu := False;
      end;
    end;
    if not Abriu then
      Exit;
    //
    PB2.Max := QrCab.RecordCount;
    PB2.Position := 0;
    QrCab.First;
    while not QrCab.Eof do
    begin
      MyObjects.UpdPB(PB2, nil, nil);
      MovimCod  := QrCab.FieldByName('MovimCod').AsInteger;
      if TipoRegSPEDProd = trsp21X then
      begin
        case MovimID of
(*         Ja tem proprio: ProducaoSingular_Insere_OrigeRibPDA_210()
          emidEmRibPDA:
          begin
            Fator := 1;
            ProducaoSingular_Insere_OrigeRibPDA_210(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminDestPDA, QrCab, LinArqPai, SQL_PeriodoVS, OrigemOpeProc);
          end;
*)
          emidEmProcCal: ; // nada!!
          emidEmProcCur: ; // nada!!
          emidEmOperacao:
          begin
            Fator := 1;
            Insere_OrigeProcVS_210(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminEmOperInn, TEstqMovimNiv.eminEmOperBxa, QrCab, LinArqPai, SQL_PeriodoVS, OrigemOpeProc);
            Insere_EmOpeProc_215(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminDestOper, QrCab, (*Fator,*) SQL_PeriodoVS, OrigemOpeProc, LinArqPai);
          end;
          emidEmProcWE: ; // nada!!
          else
          begin
            Result := False;
            Geral.MB_Erro('MovimID ' + Geral.FF0(Integer(MovimID)) + ' n�o implementado em "ImportaOpeProc().21x"');
            Exit;
          end;
        end;
      end else
      begin
        case MovimID of
          emidEmProcCal:
          begin
            Fator := -1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmCalBxa);
          end;
          emidEmProcCur:
          begin
            Fator := 1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmCurBxa);
          end;
          emidEmProcWE:
          begin
            Fator := 1;

            //criar query que separa periodos + Periodos de correcao!
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmWEndBxa);
          end;
          emidEmProcSP:
          begin
            Fator := 1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmPSPBxa);
          end;
          emidEmReprRM:
          begin
            Fator := 1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmRRMBxa);
           // Geral.MB_SQL(Self, QrCab);
          end;
          else
          begin
            Result := False;
            // MovimID 30 n�o implementado em "ImportaOpeProc().2xx"
            Geral.MB_Erro('MovimID ' + Geral.FF0(Integer(MovimID)) + ' n�o implementado em "ImportaOpeProc().2xx"');
            Exit;
          end;
        end;
      end;
      QrCab.Next;
    end;
    Result := True;
  end;
begin
  //PB1.Position := PB1.Position + 1;
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando "DtHrFimOpe" do MovimID: ' +
    Geral.FF0(Integer(MovimID)));
  VS_PF.AtualizaDtHrFimOpe_MovimID(MovimID);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Importando MovimID: ' +
    Geral.FF0(Integer(MovimID)));
  TabSrc    := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
  // Recurtimento!!!
  MovNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
  MovNivBxa := VS_PF.ObtemMovimNivBxaDeMovimID(MovimID);
  // ?????
  //MovNivInn := VS_PF.ObtemMovimNivBxaDeMovimID(MovimID);
  if MovNivInn = eminSemNiv then
    Exit;
  //
  SQL_FLD := Geral.ATS([
  'SELECT DISTINCT ggx.GraGru1 Nivel1, ggx.Controle Reduzido, ',
  'gg1.Nome NO_GG1, med.Sigla, med.Grandeza ',
  '']);
  SQL_AND := Geral.ATS([
  'AND ((IF(xco.Grandeza > 0, xco.Grandeza, ',
  '  IF((ggx.GraGruY<2048 OR ',
  '  xco.CouNiv2<>1), 2, ',
  '  IF(xco.CouNiv2=1, 1, -1))) <> med.Grandeza))',
  '']);
  if GrandezasInconsistentes() then
    Exit;
  //
  SQL_FLD := Geral.ATS([
  'SELECT  ',
  //
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vsx.AreaDstM2', 'vsx.PesoKgDst', 'vsx.PecasDst'),
  //
  'vsx.* ',
  '']);
  SQL_AND := '';
  //
  case MovimID of
    //emidEmRibPDA, Ja tem proprio: ProducaoSingular_Insere_OrigeRibPDA_210()
    //emidEmRibDTA,
    emidEmOperacao:
    begin
      InsereRegistros(trsp21X);
    end;
    emidEmProcCal,
    emidEmProcCur,
    emidEmProcWE,
    emidEmProcSP:
    begin
      InsereRegistros(trsp23X);
      InsereRegistros(trsp25X);
    end;
    emidEmReprRM:
    begin
      InsereRegistros(trsp26X);
    end;
    else Geral.MB_Erro('"MovimID" n�o implementado em "ProducaoSingular_ImportaOpeProcA()"');
  end;
  //
  Result := True;
end;

function TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9.ImportaOpeProcB(QrCab: TMySQLQuery;
  PsqMovimNiv: TEstqMovimNiv; SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ,
  SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
  //
  function ParteSQL(Tabela, FldSrc, FldDst, sArea, sPeso, sPeca, dArea, dPeso, dPeca: String; MovimNiv:
  TEstqmovimNiv): String;
  var
    SQL_IF: String;
  begin
    SQL_IF := 'IF(' + Copy(SQL_PeriodoVS, 4) + ', ';
    //
    Result := Geral.ATS([
    'SELECT vmi.Empresa, vmi.MovimID, vmi.Codigo, vmi.MovimCod,  ',
    'vmi.MovimNiv, vmi.Controle, ' +
    SQL_IF + sPeca + ', 0.000) Pecas, ' +
    SQL_IF + sArea + ', 0.000) AreaM2, ' +
    SQL_IF + sPeso + ', 0.000) PesoKg, ',
    'DATE(vmi.DataHora) Data,  ' + FldDst + ' GGX_Dst, ', //+ FldOri + ', ',
    FldSrc + ' GGX_Src, ',
    '  ',
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'sTipoEstq', True,
    sArea, sPeso, sPeca, 's'),
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'dTipoEstq', True,
    dArea, dPeso, dPeca, 'd'),
    //
    ' ',
    SQL_IF + dPeca + ', 0.000) QtdAntPeca, ' +
    SQL_IF + dArea + ', 0.000) QtdAntArM2, ' +
    SQL_IF + dPeso + ', 0.000) QtdAntPeso, ',
    'vmi.DtCorrApo, vmi.ClientMO, vmi.FornecMO, scc.EntiSitio ',
    'FROM ' + TMeuDB + '.' + Tabela + ' cab ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '   vmi ON vmi.MovimCod=cab.MovimCod ',
    'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    sggx ON sggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    sgg1 ON sgg1.Nivel1=sggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    smed ON smed.Codigo=sgg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    sggc ON sggc.Controle=sggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  sgcc ON sgcc.Codigo=sggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  sgti ON sgti.Controle=sggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou sxco ON sxco.GraGruX=sggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    snv1 ON snv1.Codigo=sxco.CouNiv1   ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    snv2 ON snv2.Codigo=sxco.CouNiv2   ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    dggx ON dggx.Controle=cab.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    dgg1 ON dgg1.Nivel1=dggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    dmed ON dmed.Codigo=dgg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    dggc ON dggc.Controle=dggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  dgcc ON dgcc.Codigo=dggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  dgti ON dgti.Controle=dggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou dxco ON dxco.GraGruX=dggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    dnv1 ON dnv1.Codigo=dxco.CouNiv1   ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    dnv2 ON dnv2.Codigo=dxco.CouNiv2   ',
    ' ',
    'WHERE (vmi.Empresa=' + Geral.FF0(FEmpresa),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    SQL_PeriodoVS,
  ') OR ',
  '( ',
  'vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  'AND cab.MovimCod IN ( ',
  'SELECT DISTINCT VSMovCod  ',
  'FROM ' + TMeuDB + '.emit  ',
  'WHERE Codigo IN (   ',
  '  SELECT OrigemCodi  ',
  '  FROM ' + TMeuDB + '.pqx  ',
  '  WHERE Tipo=110     ',
  '  ' + SQL_PeriodoPQ,
  ')   ',
  'AND VSMovCod <> 0  ',
  '  ',
  'UNION   ',
  '  ',
  'SELECT DISTINCT VSMovCod  ',
  'FROM ' + TMeuDB + '.pqo  ',
  'WHERE Codigo IN (   ',
  '  SELECT OrigemCodi  ',
  '  FROM ' + TMeuDB + '.pqx  ',
  '  WHERE Tipo=110     ',
  '  ' + SQL_PeriodoPQ,
  ')   ',
  'AND VSMovCod <> 0  ',
  ////
  ') ',
  ') ',
  '']);
  end;
const
  sProcName = 'FmSpedEfdIcmsIpiProducaoIsolada.ProducaoSingular_ImportaOpeProcB()';
  //SrcFator = -1;
  DstFator = 1;
  ESTSTabSorc = estsVMI;
var
  Pecas, AreaM2, PesoKg, QtdeSrc, QtdeDst, QtdAntArM2, QtdAntPeso, QtdAntPeca:
  Double;
  Data, DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  SrcTipoEstq, DstTipoEstq, GGXSrc, GGXDst, Codigo, MovimCod, IDSeq1,
  ID_Item, Controle, SrcFator, ClientMO, FornecMO, EntiSitio: Integer;
  MovimNiv: TEstqMovimNiv;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  COD_INS_SUBST, SQLx: String;
  MovimID: TEstqMovimID;
  MovimNivInn: TEstqMovimNiv;
begin
  COD_INS_SUBST := '';
  //
  case PsqMovimNiv of
    TEstqmovimNiv.eminSorcCal: SrcFator := 1;
    TEstqmovimNiv.eminDestCal: SrcFator := -1;
    TEstqmovimNiv.eminSorcCur: SrcFator := 1;
    else
    begin
      SrcFator := -1;
      Geral.MB_Erro('"SrcFator" n�o implementado em "ProducaoSingular_ImportaOpeProcB()"');
    end;
  end;
  case PsqMovimNiv of
    TEstqmovimNiv.eminSorcCal: SQLx :=
      ParteSQL('vscalcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCal);
    TEstqmovimNiv.eminDestCal: SQLx :=
      ParteSQL('vscaljmp', 'vmi.JmpGGX', 'vmi.GraGruX', '-vmi.QtdGerArM2',
      '-vmi.QtdGerPeso', '-vmi.QtdGerPeca',  'vmi.QtdGerArM2',
      'vmi.QtdGerPeso', 'vmi.QtdGerPeca', TEstqmovimNiv.eminDestCal);
    TEstqmovimNiv.eminSorcCur: SQLx :=
      ParteSQL('vscurcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCur);
    else
    begin
      SQLx := 'SELECT ???? FROM ????';
      Geral.MB_Erro('"MovimNiv" n�o implementado em "ProducaoSingular_ImportaOpeProcB()"');
    end;
  end;

  UnDmkDAC_PF.AbreMySQLQuery0(QrMC, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_O_P;  ',
  'CREATE TABLE _SPED_EFD_K2XX_O_P  ',
  //
  SQLx,
  '; ',
  ' ',
  'SELECT DISTINCT MovimCod  ',
  'FROM _SPED_EFD_K2XX_O_P ',
  'ORDER BY MovimCod;  ',
  '']);
  //Geral.MB_SQL(Self, QrMC); //Parei aqui
  //
  if EfdIcmsIpi_PF.ErrGrandeza('_SPED_EFD_K2XX_O_P', 'GGx_Src', 'GGX_Dst' ) then
    Exit;
  //
  PB2.Position := 0;
  PB2.Max := QrMC.RecordCount;
  QrMC.First;
  //if QrMC.RecordCount > 0 then
    //Geral.MB_Aviso('DEPRRCAR!!! OpeProcA - 1');
  while not QrMC.Eof do
  begin
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    UnDmkDAC_PF.AbreMySQLQuery0(QrProdRib, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM _SPED_EFD_K2XX_O_P ',
    'WHERE MovimCod=' + Geral.FF0(QrMCMovimCod.Value),
    '']);
    //Geral.MB_SQL(Self, QrProdRib); //Parei aqui!
    QrProdRib.First;
    while not QrProdRib.Eof do
    begin
      MovimID     := TEstqMovimID(QrProdRibMovimID.Value);
      AreaM2      := QrProdRibAreaM2.Value;
      PesoKg      := QrProdRibPesoKg.Value;
      Pecas       := QrProdRibPecas.Value;
      QtdAntArM2  := QrProdRibQtdAntArM2.Value;
      QtdAntPeso  := QrProdRibQtdAntPeso.Value;
      QtdAntPeca  := QrProdRibQtdAntPeca.Value;
      Data        := QrProdRibData.Value;
      SrcTipoEstq := Trunc(QrProdRibsTipoEstq.Value);
      DstTipoEstq := Trunc(QrProdRibdTipoEstq.Value);
      GGXSrc      := QrProdRibGGX_Src.Value;
      GGXDst      := QrProdRibGGX_Dst.Value;
      MovimCod    := QrProdRibMovimCod.Value;
      MovimNiv    := TEstqMovimNiv(QrProdRibMovimNiv.Value);
      Codigo      := QrProdRibCodigo.Value;
      Controle    := QrProdRibControle.Value;
      ID_Item     := Controle;
      //
      DtMovim     := Data;
      DtCorrApo   := QrProdRibDtCorrApo.Value;
      //
      ClientMO    := QrProdRibClientMO.Value;
      FornecMO    := QrProdRibFornecMO.Value;
      EntiSitio   := QrProdRibEntiSitio.Value;
      //
      if (ClientMO =0) or (FornecMO=0) or (EntiSitio=0) then
      begin
        MovimNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
        UnDmkDAC_PF.AbreMySQLQuery0(QrObtCliForSite, Dmod.MyDB, [
        'SELECT vmi.ClientMO, vmi.FornecMO, scc.EntiSitio  ',
        'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
        'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc  ',
        'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo  ',
        'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
        'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
        '']);
        if Integer(MovimNivInn) = 0 then
        begin
          Geral.MB_info(sProcName + sLineBreak +
          'MovimNivInn Indefinido na SQL abaixo:' + sLineBreak +
          QrObtCliForSite.SQL.Text);
        end;
        //
        ClientMO  := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
        FornecMO  := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
        EntiSitio  := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
      end;
      //
      //if (Pecas <> 0) and (DtHrFimOpe > 1) then
      if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
      begin
        case SrcTipoEstq of
          0: QtdeSrc := Pecas;
          1: QtdeSrc := AreaM2;
          2: QtdeSrc := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(SrcTipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[SrcTipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrProdRib.SQL.Text);
            //
            QtdeDst := Pecas;
          end;
        end;
        QtdeSrc := QtdeSrc * SrcFator;

        //
        case DstTipoEstq of
          0: QtdeDst := QtdAntPeca;
          1: QtdeDst := QtdAntArM2;
          2: QtdeDst := QtdAntPeso;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(SrcTipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[SrcTipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrProdRib.SQL.Text);
            //
            QtdeDst := QtdAntPeca;
          end;
        end;
        QtdeDst := QtdeDst * DstFator;
      end else
      begin
        QtdeSrc := 0;
        QtdeDst := 0;
      end;
      //
      if QtdeSrc < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade "Src" n�o pode ser negativa!!! (5)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo), QrProdRib);
      if QtdeDst < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade "Dst" n�o pode ser negativa!!! (6)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), QrProdRib)
      else if (QtdeDst = 0) and (Data > 1) then
      begin
        case MovimID of
          emidEmProcCal: Data := 0;
          emidEmProcCur: Data := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade zerada para OP encerrada!!! (4)', QrProdRib);
        end;
      end;
      //
      //if QrSubPrdOPEmpresa.Value = QrFMOFornecMO.Value then

      if FEmpresa = FornecMO then
        TipoRegSPEDProd := trsp23X
      else
        TipoRegSPEDProd := trsp25X;
      //
      DtHrAberto := Data;
      case TipoRegSPEDProd of
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod,
          Controle, DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst,
          ClientMO, FornecMO, EntiSitio, QtdeDst, OrigemOpeProc,
          FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
          //
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, (*LinArqPai*)IDSeq1, DtHrAberto,
          DtCorrApo, GGXSrc, QtdeSrc, COD_INS_SUBST, ID_Item, Integer(MovimID),
          Codigo, MovimCod, Controle, ClientMO, FornecMO, EntiSitio,
          OrigemOpeProc, ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
          //
          Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv, MovimCod,
          (*LinArqPai*)IDSeq1, SQL_PeriodoPQ, OrigemOpeProc, ClientMO, FornecMO, EntiSitio);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
          QtdeDst, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal, MeAviso, (*LinArqPai*)IDSeq1);
          //
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, (*LinArqPai*)IDSeq1, DtHrAberto, DtCorrApo, GGXSrc,
          QtdeSrc, COD_INS_SUBST, ID_Item,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
          //
          Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv, MovimCod,
          (*LinArqPai*)IDSeq1, SQL_PeriodoPQ, OrigemOpeProc, ClientMO, FornecMO, EntiSitio);
        end;
        else Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(5)');
      end;
      QrProdRib.Next;
    end;
    QrMC.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9.Insere_EmOpeProc_215(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv; const QrCab: TmySQLQuery; const SQL_Periodo: String;
  const OrigemOpeProc: TOrigemOpeProc; var IDSeq1: Integer): Boolean;
const
  ESTSTabSorc = estsVMI;
  sProcName = 'FmSpedEfdIcmsIpiProducaoIsolada.Insere_OrigeProc_215';
var
  DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
  GraGruX, GGXCabDst, ID_Item, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer;
  MyQtd, Qtde, Fator: Double;
  COD_INS_SUBST, TabCab: String;
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_Periodo;
      //trsp23X: Result := SQL_Periodo;
      //trsp25X: Result := SQL_Periodo;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (3)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
begin
  Result := True;
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT cab.GraGruX GGXSrc, cab.GGXDst,  ',
  'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
  'Pecas, PesoKg, AreaM2, DATE(DataHora) DATA, vmi.DtCorrApo, ',
      VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
      'AreaM2', 'PesoKg', 'Pecas'),
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
  'LEFT JOIN vsopecab   cab ON cab.MovimCod=vmi.MovimCod  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  //'LEFT JOIN gragrux ggx ON ggx.Controle=cab.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
  SQL_Periodo,
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  while not QrOri.Eof do
  begin
    Codigo    := QrOri.FieldByName('Codigo').AsInteger;
    MovimCod  := QrOri.FieldByName('MovimCod').AsInteger;
    Controle  := QrOri.FieldByName('Controle').AsInteger;
    ID_Item   := Controle;
    //
    Fator         := 1;
    Data          := QrOri.FieldByName('DATA').AsDateTime;
    GGXCabDst     := QrOri.FieldByName('GGXDst').AsInteger;
    DtMovim       := Data;
    DtCorrApo     := QrOri.FieldByName('DtCorrApo').AsDateTime;
    ClientMO      := QrOri.FieldByName('ClientMO').AsInteger;
    FornecMO      := QrOri.FieldByName('FornecMO').AsInteger;
    EntiSitio     := QrOri.FieldByName('EntiSitio').AsInteger;
    if MovimID = emidEmProcCur then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCalToCur, Dmod.MyDB, [
      'SELECT DstGGX ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(QrOri.FieldByName('SrcNivel2').AsInteger),
      '']);
      GraGruX := QrCalToCurDstGGX.Value;
    end
    else
      GraGruX     := QrOri.FieldByName('GraGruX').AsInteger;
    COD_INS_SUBST := '';
    case MovimNiv of
      //eminEmOperBxa,
      //eminEmWEndBxa,
      eminDestOper:
      begin
        Fator := 1;
        if GGXCabDst <> GraGruX then
          if GGXCabDst <> 0 then
            COD_INS_SUBST := Geral.FF0(GGXCabDst);
      end;
(*
      eminDestCal,
      eminDestCur:
      begin
       COD_INS_SUBST := ''; // Nada??? Eh o GraGruX!
       Fator := ?;
      end;
*)
      else
      begin
        Result := False;
        Geral.MB_Erro('"MovimNiv": ' + Geral.FF0(Integer(MovimNiv)) +
        ' n�o implemetado em "Insere_OrigeProc()" (1)');
      end;
    end;
    //qual usar? area ou peso?
    MyQtd := 0;
    case  QrOri.FieldByName('Grandeza').AsInteger of
      0: MyQtd := QrOri.FieldByName('Pecas').AsFloat;
      1: MyQtd := QrOri.FieldByName('AreaM2').AsFloat;
      2: MyQtd := QrOri.FieldByName('PesoKg').AsFloat;
      else
      begin
        Geral.MB_Aviso('Grandeza n�o implementada: ' +
        sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak);
      end;
    end;
    //
    Qtde := MyQtd * Fator;
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (5)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
      '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
      'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) +
      sLineBreak + 'Avise a DERMATEK!!!');
      ID_Item := QrOri.FieldByName('Controle').AsInteger;
    if Qtde < 0.001 then
      Geral.MB_ERRO('Quantidade n�o pode ser zero!!! (3)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak + sLineBreak +
      'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
      '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
      'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    //
    case TipoRegSPEDProd of
      trsp21X:
      begin
        EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, (*LinArqPai*)IDSeq1, Data, DtMovim, DtCorrApo, GraGruX,
        Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
        Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210ProcVS,
        ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
      end;
      else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(6)');
    end;
    //
    QrOri.Next;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9.Insere_EmOpeProc_Prd(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const Qry: TmySQLQuery; const Fator: Double; const SQL_Periodo,
  SQL_PeriodoPQ: String; const OrigemOpeProc: TOrigemOpeProc;
  const MovimNiv: TEstqMovimNiv);
  //
  procedure Insere_EmOpeProc(const TipoRegSPEDProd: TTipoRegSPEDProd; const
            MovimID: TEstqMovimID; const Qry: TmySQLQuery; const Fator:
            Double; const SQL_Periodo, SQL_CorrApo: String;
            const OrigemOpeProc: TOrigemOpeProc;
            const InsumoSemProducao: Boolean; var IDSeq1: Integer);
  const
    sProcName = 'FmSpedEfdIcmsIpiProducaoIsolada.Insere_EmOpeProc_Prd > Insere_EmOpeProc()';
  var
    DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd, DtCorrApoCons: TDateTime;
    QtdeProd, QtdeCons, Pecas, PesoKg, AreaM2: Double;
    MyFator, TipoEstq, Codigo, MovimCod, GGXDst, ClientMO, FornecMO,
    EntiSitio: Integer; ESTSTabSorc: TEstqSPEDTabSorc; ID_Item, Controle: Integer;
    //
    function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
    begin
      case TipoRegSPEDProd of
        trsp21X: Result := '';
        trsp23X: Result := SQL_Periodo;
        //trsp25X: Result := '';
        trsp25X: Result := SQL_Periodo;
        trsp26X: Result := SQL_Periodo;
        else
        begin
          Geral.MB_Erro('"FiltroReg" indefinido: ' +
            Geral.FF0(Integer(TipoRegSPEDProd)) + '! (4)');
          Result := '#ERR_PERIODO_';
        end;
      end;
    end;

    procedure InsereAtual_2X0();
    begin
      DtMovim    := FDiaIni; // ver aqui quando periodo nao for mensal!
      //Parei aqui !!!  � asimm???
      if (Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: QtdeProd := Pecas;
          1: QtdeProd := AreaM2;
          2: QtdeProd := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            Qry.SQL.Text);
            //
            QtdeProd := Pecas;
          end;
        end;
        QtdeProd := QtdeProd * MyFator;
      end else
        QtdeProd := 0;
      //
      if QtdeProd < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (8)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeProd, 3, siNegativo), nil)
      else if (QtdeProd = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidEmProcCal: DtHrFimOpe := 0;
          emidEmProcCur: DtHrFimOpe := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade zerada para OP encerrada!!! (6)', nil);
        end;
      end;
      //
      case TipoRegSPEDProd of
    {
        trsp21X:
        begin
      ESTSTabSorc := TEstqSPEDTabSorc.estsND;
      ID_Item     := 0;
      Controle    := 0;
          InsereItemAtual_K215(LinArqPai, DtHrAberto(*Data*), GGXDst(*GraGruX*),
          Qtde, ''(*COD_INS_SUBST*), ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ESTSTabSorc);
          //
        end;
    }
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K230(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd, GGXDst, ClientMO,
          FornecMO, EntiSitio, QtdeProd, OrigemOpeProc,
          FDiaFim, FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K250(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrFimOpe, DtMovim, DtCorrApoProd, GGXDst, ClientMO, FornecMO,
          EntiSitio, QtdeProd, OrigemOpeProc, FDiaFim, FTipoPeriodoFiscal,
          MeAviso, (*LinArqPai*)IDSeq1);
        end;
        trsp26X:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrConsParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE MovimCod=' + Geral.FF0(MovimCod),
          'AND MovimNiv IN (57) ', //eminEmRRMBxa=57,
          //'AND MovimTwn=' + Geral.FF0(), /
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          'GROUP BY Ano, Mes', // MovimNiv >>> 54 ou 56 para RRM!!!
          '']);
         // Geral.MB_SQL(Self, QrConsParc);
          //
          if QrConsParc.RecordCount > 1 then
            Geral.MB_Erro('ERRO! Avise a Dermatek! QrConsParc.RecordCount > 1!!!');
          //while not QrConsParc.Eof do
          begin
            if QrConsParcAno.Value = 0 then
              DtCorrApoCons := 0
            else
              DtCorrApoCons := EncodeDate(Trunc(QrConsParcAno.Value), Trunc(QrConsParcMes.Value), 1);
            //
            AreaM2 := QrConsParcAreaM2.Value;
            PesoKg := QrConsParcPesoKg.Value;
            Pecas  := QrConsParcPecas.Value;
            //
            //Parei aqui !!!  � asimm???
            if (Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP) (*and (DtHrFimOpe > 1)*) then
            begin
              case TipoEstq of
                0: QtdeCons := Pecas;
                1: QtdeCons := AreaM2;
                2: QtdeCons := PesoKg;
                else
                begin
                  Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                  + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                  'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                  'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                  Qry.SQL.Text);
                  //
                  QtdeCons := Pecas;
                end;
              end;
              QtdeCons := QtdeCons * (*MyFator*) -1;
            end else
              QtdeCons := 0;
            //
            if QtdeCons < 0 then
              Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (9)' + sLineBreak +
              'Qtde: ' + Geral.FFT(QtdeCons, 3, siNegativo), QrConsParc)
          end;
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K260(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd, DtCorrApoCons, GGXDst,
          ClientMO, FornecMO, EntiSitio,
          //CtrlDst, CtrlBxa,
          QtdeProd, QtdeCons, OrigemOpeProc, FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(9)');
      end;
    end;
  begin
    MyFator    := -1;
    QtdeProd   := 0;
    QtdeCons   := 0;
    TipoEstq   := Qry.FieldByName('Tipoestq').AsInteger;
    Codigo     := Qry.FieldByName('Codigo').AsInteger;
    MovimCod   := Qry.FieldByName('MovimCod').AsInteger;
    DtHrAberto := Qry.FieldByName('DtHrAberto').AsDateTime;
    DtHrFimOpe := Qry.FieldByName('DtHrFimOpe').AsDateTime;
    GGXDst     := Qry.FieldByName('GGXDst').AsInteger;
    //
    if InsumoSemProducao then
    begin
      DtCorrApoProd := 0;
      AreaM2 := 0;
      PesoKg := 0;
      Pecas  := 0;
      //
      InsereAtual_2X0();
      //
    end else
    begin
      case MovimID of
        emidEmProcCal:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE SrcMovID=' + Geral.FF0(Integer(MovimID)),
          'AND SrcNivel1 <> 0 ',
          'AND SrcNivel1=' + Geral.FF0(Codigo),
          // Entradas no estoque pela producao ( sem doc fiscal de compra, remessa, devolucao, etc)
          'AND MovimID IN (' + CO_ALL_CODS_NO_INN_SPED_VS + ')',
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          '']);
        end;
        emidEmProcCur:
        begin
          // Igual a emidEmProcCal!!!
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE SrcMovID=' + Geral.FF0(Integer(MovimID)),
          'AND SrcNivel1 <> 0 ',
          'AND SrcNivel1=' + Geral.FF0(Codigo),
          // Entradas no estoque pela producao ( sem doc fiscal de compra, remessa, devolucao, etc)
          'AND MovimID IN (' + CO_ALL_CODS_NO_INN_SPED_VS + ')',
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          '']);
        end;
        emidEmOperacao, emidEmProcWE, emidEmProcSP, emidEmReprRM:
        begin
          MyFator := 1;
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE MovimCod=' + Geral.FF0(MovimCod),
          'AND MovimNiv IN (9,22,51,56) ', //eminDestOper=9, //eminDestWEnd=22, //eminDestPSP=51,//eminDestRRM=56,
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          '']);
          //Geral.MB_SQL(Self, QrProdParc);
        end;
        else
        begin
          //Result := False;
          Geral.MB_ERRO(
          '"MovimID" n�o implementado em ' + sProcName);
          //Close;
          Exit;
        end;
      end;
      //Geral.MB_SQL(self, QrProdParc);
      //
      QrProdParc.First;
      while not QrProdParc.Eof do
      begin
        //
        if QrProdParcAno.Value = 0 then
          DtCorrApoProd := 0
        else
          DtCorrApoProd := EncodeDate(Trunc(QrProdParcAno.Value), Trunc(QrProdParcMes.Value), 1);
        //
        AreaM2   := QrProdParcAreaM2.Value;
        PesoKg   := QrProdParcPesoKg.Value;
        Pecas    := QrProdParcPecas.Value;
        ClientMO := QrProdParcClientMO.Value;
        FornecMO := QrProdParcFornecMO.Value;
        Controle := QrProdParcControle.Value;
        //
        InsereAtual_2X0();
        //
        QrProdParc.Next;
      end;
    end;
  end;
  //
  function  Insere_OrigeProcVS(TipoRegSPEDProd: TTipoRegSPEDProd; MovimID:
            TEstqMovimID; MovimNiv: TEstqMovimNiv; QrCab: TmySQLQuery;
            IDSeq1: Integer; SQL_PeriodoVS, SQL_CorrApo: String;
            OrigemOpeProc: TOrigemOpeProc; InsumoSemProducao: Boolean): Boolean;
  const
    ESTSTabSorc = estsVMI;
    sProcName = 'FmSpedEfdIcmsIpiProducaoIsolada.Insere_EmOpeProc_Prd > Insere_OrigeProc()';
    //
    procedure InsereAtual_2X5(Data, DtCorrApo: TDateTime; GraGruX: Integer;
    Qtde: Double; COD_INS_SUBST: String; ID_Item, Codigo, Controle, ClientMO,
    FornecMO, EntiSitio, MovimCod: Integer);
    begin
      case TipoRegSPEDProd of
        //trsp21X:
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, (*LinArqPai*)IDSeq1, Data, DtCorrApo, GraGruX, Qtde,
          COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc,
          FTipoPeriodoFiscal, MeAviso);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, (*LinArqPai*)IDSeq1, Data, DtCorrApo, GraGruX, Qtde,
          COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc,
          FTipoPeriodoFiscal, MeAviso);
        end;
        trsp26X:
        begin
          //
(*        N�o tem como retornar nada ao estoque, pois a mercadoria foi transfor-
          mada e n�o montada (com um motor por exemplo)!
          //
          InsereItemAtual_K265(LinArqPai, Data, DtCorrApo, GraGruX, QtdeCons,
          QtdeRet, COD_INS_SUBST, ID_Item, MovimID, Codigo, MovimCod, Controle,
          OrigemOpeProc, ESTSTabSorc);
*)
          Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv, MovimCod,
          IDSeq1, SQL_PeriodoPQ, OrigemOpeProc, ClientMO, FornecMO, EntiSitio);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(10)');
      end;
    end;
  var
    Data, DtCorrApo, DtHrAberto, DtHrFimOpe: TDateTime;
    GraGruX, GGXCabDst, ID_Item, Codigo, MovimCod, Controle, ClientMO, FornecMO,
    EntiSitio: Integer;
    MyQtd, Qtde, Fator: Double;
    COD_INS_SUBST, TabCab: String;
    MovimNivInn: TEstqMovimNiv;
    //
  var
    SQL_PeriodoApo: String;
    DtCorrIni, DtCorrFim: TDateTime;
  begin
    Result := True;
    TabCab := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
    'SELECT cab.GraGruX GGXSrc, cab.GGXDst,  ',
    'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
    //'SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, SUM(AreaM2) AreaM2, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(DataHora) DATA, vmi.DtCorrApo, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
    'med.Grandeza  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
    'LEFT JOIN vsopecab  cab ON cab.MovimCod=vmi.MovimCod  ',
    'LEFT JOIN gragrux   ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    SQL_PeriodoVS,
    SQL_CorrApo,
    //'GROUP BY vmi.Controle ', // Sem Soma!
    '']);
    //Geral.MB_SQL(Self, QrOri);
    if InsumoSemProducao then
    begin
      //Geral.MB_SQL(Self, QrCab);
      case TipoRegSPEDProd of
        //trsp21X:
        trsp23X,
        //trsp25X:
        trsp26X:
        begin
          Data      := 0;
          DtCorrApo := 0;
          GraGruX   := QrCab.FieldByName('GraGruX').AsInteger;
          Qtde      := 0;
          COD_INS_SUBST := '';
          ID_Item   := 0;
          Codigo    := QrCab.FieldByName('Codigo').AsInteger;
          Controle  := 0;
          MovimCod  := QrCab.FieldByName('MovimCod').AsInteger;
          // N�o testado!!
          MovimNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
          UnDmkDAC_PF.AbreMySQLQuery0(QrObtCliForSite, Dmod.MyDB, [
          'SELECT vmi.ClientMO, vmi.FornecMO, scc.EntiSitio  ',
          'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
          'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc  ',
          'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo  ',
          'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
          'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
          '']);
          ClientMO  := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
          FornecMO  := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
          EntiSitio  := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
          //
          InsereAtual_2X5(Data, DtCorrApo, GraGruX, Qtde, COD_INS_SUBST,
          ID_Item, Codigo, Controle, ClientMO, FornecMO, EntiSitio, MovimCod);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(11)');
      end;
    end else
    begin
      if QrOri.RecordCount > 0 then
      begin
        QrOri.First;
        while not QrOri.Eof do
        begin
          Codigo    := QrOri.FieldByName('Codigo').AsInteger;
          MovimCod  := QrOri.FieldByName('MovimCod').AsInteger;
          Controle  := QrOri.FieldByName('Controle').AsInteger;
          ID_Item   := Controle;
          //
          Fator         := 1;
          Data          := QrOri.FieldByName('DATA').AsDateTime;
          DtCorrApo     := QrOri.FieldByName('DtCorrApo').AsDateTime;
          GGXCabDst     := QrOri.FieldByName('GGXDst').AsInteger;
          // J� testado!!
          ClientMO      := QrOri.FieldByName('ClientMO').AsInteger;
          FornecMO      := QrOri.FieldByName('FornecMO').AsInteger;
          if MovimID = emidEmProcCur then
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrCalToCur, Dmod.MyDB, [
            'SELECT DstGGX ',
            'FROM ' + CO_SEL_TAB_VMI,
            'WHERE Controle=' + Geral.FF0(QrOri.FieldByName('SrcNivel2').AsInteger),
            '']);
            GraGruX := QrCalToCurDstGGX.Value;
          end
          else
            GraGruX     := QrOri.FieldByName('GraGruX').AsInteger;
          COD_INS_SUBST := '';
          case MovimNiv of
            eminEmOperBxa,
            eminEmWEndBxa,
            eminEmPSPBxa:
            begin
              Fator := -1;
              if GGXCabDst <> GraGruX then
                if GGXCabDst <> 0 then
                  COD_INS_SUBST := Geral.FF0(GGXCabDst);
            end;
            eminEmCalBxa,
            eminEmCurBxa:
            begin
             COD_INS_SUBST := ''; // Nada??? Eh o GraGruX!
             Fator := -1;
            end;
            eminEmRRMBxa:
            begin
             COD_INS_SUBST := ''; // N�o existe no manual
             Fator := -1;
            end;
            else
            begin
              Result := False;
              Geral.MB_Erro('"MovimNiv": ' + Geral.FF0(Integer(MovimNiv)) +
              ' n�o implemetado em "Insere_OrigeProc()" (3)');
            end;
          end;
          //qual usar? area ou peso?
          MyQtd := 0;
          case  QrOri.FieldByName('Grandeza').AsInteger of
            0: MyQtd := QrOri.FieldByName('Pecas').AsFloat;
            1: MyQtd := QrOri.FieldByName('AreaM2').AsFloat;
            2: MyQtd := QrOri.FieldByName('PesoKg').AsFloat;
            else
            begin
              Geral.MB_Aviso('Grandeza n�o implementada: ' +
              sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
            'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak);
            end;
          end;
          //
          Qtde := MyQtd * Fator;
          if Qtde < 0 then
            Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (10)' + sLineBreak +
            'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
            sprocName + sLineBreak + sLineBreak +
            'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
            'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
            'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
            '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
            'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
            'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
            Geral.FFT(Fator, 3, siNegativo) + ' = ' +
            Geral.FFT(Qtde, 3, siNegativo) +
            sLineBreak + 'Avise a DERMATEK!!!');
            ID_Item := QrOri.FieldByName('Controle').AsInteger;
          if Qtde < 0.001 then
            Geral.MB_ERRO('Quantidade n�o pode ser zero!!! (3)' + sLineBreak +
            'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak + sLineBreak +
            'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
            'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
            'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
            '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
            'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
            'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
            Geral.FFT(Fator, 3, siNegativo) + ' = ' +
            Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
            sprocName + sLineBreak +
            sLineBreak + 'Avise a DERMATEK!!!');
            //
            //InsereAtual_2X5();
            InsereAtual_2X5(Data, DtCorrApo, GraGruX, Qtde, COD_INS_SUBST,
            ID_Item, Codigo, Controle, ClientMO, FornecMO, EntiSitio, MovimCod);
          //
          QrOri.Next;
        end;
      end else
      // ver se gastou insumo sem produzir produto acabado!
      begin
        // ?!
      end;
    end;
  end;
  //
  var
    SQL_PeriodoApo: String;
    DtCorrIni, DtCorrFim: TDateTime;
  procedure InsereAtualOpeProc_Prd(DtApoIni, DtApoFim: TDateTime);
  var
    SQL_PeriodoApo: String;
    IDSeq1: Integer;
    InsumoSemProducao: Boolean;
  begin
    SQL_PeriodoApo := dmkPF.SQL_Periodo('AND DtCorrApo ', DtApoIni, DtApoFim,
      True, True);
    //
    IDSeq1 := 0;
    InsumoSemProducao := False;
    Insere_EmOpeProc(TipoRegSPEDProd, MovimID, Qry, Fator,
    SQL_Periodo, SQL_PeriodoApo, OrigemOpeProc, InsumoSemProducao, IDSeq1);
    //
    InsumoSemProducao := IDSeq1 = 0;
    if InsumoSemProducao then
    begin
      // CUIDADO!!! Aqui esta duplicando!!!
      //Geral.MB_Info(Geral.FF0(Qry.FieldByName('MovimCod').AsInteger));
      VS_EFD_ICMS_IPI.ReopenVSOriPQx(QrOri,
        Qry.FieldByName('MovimCod').AsInteger, SQL_PeriodoPQ, False);
      InsumoSemProducao := QrOri.RecordCount > 0;
      if InsumoSemProducao then
      begin
        //Geral.MB_Info(Geral.FF0(Qry.FieldByName('MovimCod').AsInteger));
        //
        Insere_EmOpeProc(TipoRegSPEDProd, MovimID, Qry, Fator, SQL_Periodo,
        SQL_PeriodoApo, OrigemOpeProc, InsumoSemProducao, IDSeq1);
      end;
      //
    end;
    Insere_OrigeProcVS(TipoRegSPEDProd, MovimID, MovimNiv, Qry, IDSeq1,
    SQL_Periodo, SQL_PeriodoApo, OrigemOpeProc, InsumoSemProducao);
    // Como fazer??
    //Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminEmWEndBxa, MovimCod, LinArqPai, SQL_PeriodoPQ);
  end;
begin
 // Criar query que separa periodos + Periodos de correcao!
   UnDmkDAC_PF.AbreMySQLQuery0(QrPeriodos, Dmod.MyDB, [
  'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
  'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          //'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, ',
   EfdIcmsIpi_PF.SQLPeriodoFiscal(
     FTipoPeriodoFiscal, 'DtCorrApo', 'Periodo', False),
   'FROM ' + CO_TAB_VMI,
   'WHERE MovimCod>0 ',
   'AND DtCorrApo > 0 ',
   'GROUP BY Ano, Mes, Periodo ',
   '']);
   //
   DtCorrIni  := 0;
   DtCorrFim  := 0;
   InsereAtualOpeProc_Prd(0, 0); // Registro normal sem Correcao de apontamento!
   QrPeriodos.First;
   while not QrPeriodos.Eof do
   begin
     EfdIcmsIpi_PF.ObtemDatasDePeriodoSPED(Trunc(QrPeriodosAno.Value),
     Trunc(QrPeriodosMes.Value), QrPeriodosPeriodo.Value, FTipoPeriodoFiscal,
     DtCorrIni, DtCorrFim);
     // Registros com Correcao de apontamento!
     InsereAtualOpeProc_Prd(DtCorrIni, DtCorrFim);
     QrPeriodos.Next
   end;
end;

function TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9.Insere_OrigeProcPQ(TipoRegSPEDProd:
  TTipoRegSPEDProd; MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod:
  Integer; IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
  TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer): Boolean;
const
  Fator = -1;
  ESTSTabSorc = estsPQx;
  sProcName = 'FmSpedEfdIcmsIpiProducaoIsolada.Insere_OrigeProcPQ';
var
  Data, DtCorrApo: TDateTime;
  Codigo, Controle, ID_Item, GraGruX(*, ClientMO, FornecMO, EntiSitio*): Integer;
  MyQtd, Qtde: Double;
  //
  COD_INS_SUBST: String;
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := SQL_PeriodoPQ;
      trsp25X: Result := '';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (6)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
begin
  Result := True;
  VS_EFD_ICMS_IPI.ReopenVSOriPQx(QrOri, MovimCod, SQL_PeriodoPQ, False);
  //
  //Geral.MB_SQL(Self, QrOri); //Parei Aqui!
  QrOri.First;
  while not QrOri.Eof do
  begin
    COD_INS_SUBST := '';
    Codigo   := QrOri.FieldByName('OrigemCodi').AsInteger;
    Controle := QrOri.FieldByName('OrigemCtrl').AsInteger;
    GraGruX  := UnPQx.ObtemGGXdeInsumo(QrOri.FieldByName('Insumo').AsInteger);
    //qual usar? area ou peso?
    MyQtd     := QrOri.FieldByName('Peso').AsFloat;
    Qtde      := MyQtd * Fator;
    Data      := QrOri.FieldByName('DataX').AsDateTime;
    DtCorrApo := QrOri.FieldByName('DtCorrApo').AsDateTime;
    ID_Item   := QrOri.FieldByName('OrigemCtrl').AsInteger;
    //
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (12)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      //'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'ID Item: ' + Geral.FF0(ID_Item) + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Peso: ' + Geral.FFT(MyQtd, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    if Qtde > 0 then
    begin
      case TipoRegSPEDProd of
        //trsp21X:
        trsp23X:
        begin
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K235(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, (*LinArqPai*)IDSeq1, Data, DtCorrApo, GraGruX, Qtde,
          COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
        trsp25X:
        begin
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K255(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, (*LinArqPai*)IDSeq1, Data, DtCorrApo, GraGruX,
          Qtde, COD_INS_SUBST, ID_Item,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
        trsp26X:
        begin
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K265(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, IDSeq1, Data, DtCorrApo, GraGruX,
          (*QtdeCons*) Qtde, (*QtdeRet*)0, COD_INS_SUBST, ID_Item,
          Integer(MovimID), Codigo, MovimCod, Controle, OrigemOpeProc,
          ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(14)');
      end;
    end;
    //
    QrOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9.Insere_OrigeProcVS_210(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const MovimNivInn, MovimNivBxa: TEstqMovimNiv; const QrCab: TmySQLQuery;
  var IDSeq1: Integer; const SQL_PeriodoVS: String;
  const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  ESTSTabSorc = estsVMI;
  sProcName = 'FmSpedEfdIcmsIpiProducaoIsolada.Insere_OrigeProcVS_210';
var
  DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  Qtde, Pecas, PesoKg, AreaM2: Double;
  MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, ClientMO, FornecMO, EntiSitio,
  ID_Item, Controle: Integer;
  //
  procedure Mensagem(TextoBase: String);
  begin
    Geral.MB_ERRO(TextoBase + sLineBreak +
    'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
    'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
    sProcName + sLineBreak +
    sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
    QrProdParc.SQL.Text);
  end;

  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_PeriodoVS;
      //trsp23X: Result := SQL_PeriodoVS;
      //trsp25X: Result := SQL_PeriodoVS;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (7)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
begin
  Result := True;
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT cab.GraGruX GGXSrc, cab.GGXDst, ',
  'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
  //'SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, SUM(AreaM2) AreaM2, ',
  'DATE(DataHora) DATA, vmi.DtCorrApo, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, ',
      VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
      //'SUM(AreaM2)', 'SUM(PesoKg)', 'SUM(Pecas)'),
      'AreaM2', 'PesoKg', 'Pecas'),
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
  'LEFT JOIN vsopecab   cab ON cab.MovimCod=vmi.MovimCod  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=cab.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
  SQL_PeriodoVS,
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
  'GROUP BY DATA, Ano, Mes, GGXSrc ',
  '']);
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  while not QrOri.Eof do
  begin
    MyFator := -1;
    Qtde := 0;
    TipoEstq   := QrOri.FieldByName('Tipoestq').AsInteger;
    Codigo     := QrCab.FieldByName('Codigo').AsInteger;
    MovimCod   := QrCab.FieldByName('MovimCod').AsInteger;
    DtHrAberto := QrCab.FieldByName('DtHrAberto').AsDateTime;
    DtHrFimOpe := QrCab.FieldByName('DtHrFimOpe').AsDateTime;
    GGXSrc     := QrOri.FieldByName('GGXSrc').AsInteger;
    ClientMO   := QrOri.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrOri.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrOri.FieldByName('EntiSitio').AsInteger;
    //
    DtMovim    := QrOri.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrOri.FieldByName('DtCorrApo').AsDateTime;
    case MovimID of
      //emidEmProcCal:
      //emidEmProcCur:
      emidEmOperacao:
      begin
        MyFator := 1;
        UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
        'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
        'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
        'Pecas, AreaM2, PesoKg, ',
        'ClientMO, FornecMO, Controle ',
        'FROM ' + CO_SEL_TAB_VMI + ' ',
        'WHERE MovimCod=' + Geral.FF0(MovimCod),
        'AND MovimNiv=' + Geral.FF0(Integer(MovimNivBxa)),
        FiltroPeriodo(TipoRegSPEDProd),
        '']);
        //Geral.MB_SQL(Self, QrProdParc);
      end;
      //emidEmProcWE:
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //Geral.MB_SQL(self, QrProdParc);
    //
    QrProdParc.First;
    while not QrProdParc.Eof do
    begin
      AreaM2 := -QrProdParcAreaM2.Value;
      PesoKg := -QrProdParcPesoKg.Value;
      Pecas  := -QrProdParcPecas.Value;
      Controle := QrProdParcControle.Value;
      //
      //Parei aqui !!!  � asimm???
      if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: Qtde := Pecas;
          1: Qtde := AreaM2;
          2: Qtde := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrOri.SQL.Text);
            //
            Qtde := Pecas;
          end;
        end;
        Qtde := Qtde * MyFator;
      end else
        Qtde := 0;
      //
      if Qtde < 0 then
        Mensagem('Quantidade n�o pode ser negativa!!! (13)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo))
      else if (Qtde = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidEmProcCal: DtHrFimOpe := 0;
          emidEmProcCur: DtHrFimOpe := 0;
          else Mensagem('Quantidade zerada para OP encerrada!!! (7)');
        end;
      end;
      //
      case TipoRegSPEDProd of
        trsp21X:
        begin
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K210(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO,
          FornecMO, EntiSitio, Qtde, OrigemOpeProc, oidk210ProcVS, FDiaFim,
          FTipoPeriodoFiscal, MeAviso, IDSeq1);
        end;
  (*
        trsp23X:
        begin
          InsereItemAtual_K230(Integer(MovimID), Codigo, MovimCod, DtHrAberto,
          DtHrFimOpe, GGXDst, Qtde, LinArqPai);
        end;
        trsp25X:
        begin
          InsereItemAtual_K250(Integer(MovimID), Codigo, MovimCod, DtHrFimOpe,
          GGXDst, Qtde, LinArqPai);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(15)');
  *)
      end;
      QrProdParc.Next;
    end;
    //
    QrOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9.Insere_OrigeRibDTA_210(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const MovimNiv: TEstqMovimNiv; const SQL_PeriodoVS: String;
  const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  COD_INS_SUBST = '';
  ESTSTabSorc = estsVMI;
  sProcName = 'FmSpedEfdIcmsIpiProducaoIsolada.ProducaoSingular_Insere_OrigeRibDTA_210';
  //
  procedure ObtemDataIniEFim(const Codigo: Integer; var Ini, Fim: TDateTime);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDatas, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _PRE_DESC_ORI_MP_; ',
    'CREATE TABLE _PRE_DESC_ORI_MP_ ',
    'SELECT Controle  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND Codigo=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _PRE_DESC_DTIF_; ',
    ' ',
    'CREATE TABLE _PRE_DESC_DTIF_ ',
    'SELECT vmi.Controle, vmi.DataHora ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND vmi.SrcNivel1=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'INSERT IGNORE INTO _PRE_DESC_DTIF_ ',
    ' ',
    'SELECT vmi.Controle, vmi.DataHora  ',
    'FROM _PRE_DESC_ORI_MP_ ori  ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    '  ON ori.Controle=vmi.GSPJmpNiv2; ',
    ' ',
    'SELECT MIN(DataHora) MinDH,  ',
    'MAX(DataHora) MaxDH  ',
    'FROM _PRE_DESC_DTIF_ ',
    'WHERE Controle > 0 ',
    '']);
    //Geral.MB_SQL(Self, QrDatas);
    //
    Ini := QrDatasMinDH.Value;
    Fim := QrDatasMaxDH.Value;
  end;
  //
  function RegistroK210(const OriMovID: TEstqMovimID; const DtIniOS, DtFimOS:
  TDateTime; var IDSeq1: Integer): Boolean;
  var
  DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
  QtdeSrc, QtdeDst, PecasSrc, PesoKgSrc, AreaM2Src, PecasDst, PesoKgDst,
  AreaM2Dst: Double;
  MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, GGXDst, ID_Item, Controle,
  ClientMO, FornecMO, EntiSitio: Integer;
  EhDoMes: Boolean;
  begin
    IDSeq1     := 0;
    AreaM2Src  := 0;
    PesoKgSrc  := 0;
    PecasSrc   := 0;
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeSrc    := 0;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    DtHrAberto := DtIniOS;
    DtHrFimOpe := DtFimOS;
    GGXSrc     := QrIts.FieldByName('GGXSrc').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GGXDst').AsInteger;
    Controle   := QrIts.FieldByName('RmsNivel2').AsInteger;
    ID_Item    := Controle;
    // 270
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibDTA:
      begin
        AreaM2Src := -QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgSrc := -QrIts.FieldByName('PesoKg').AsFloat;
        PecasSrc  := -QrIts.FieldByName('Pecas').AsFloat;
        //
        AreaM2Dst := QrIts.FieldByName('QtdAntArM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('QtdAntPeso').AsFloat;
        PecasDst  := QrIts.FieldByName('QtdAntPeca').AsFloat;
      end;
      else
      begin
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        Exit;
      end;
    end;
    //
    EhDoMes := (Data >= FDiaIni) and (Data < FDiaPos);
    if EhDoMes then
    begin
      if (PecasSrc <> 0) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: QtdeSrc := PecasSrc;
          1: QtdeSrc := AreaM2Src;
          2: QtdeSrc := PesoKgSrc;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrIts.SQL.Text);
            //
            QtdeSrc := PecasSrc;
          end;
        end;
        QtdeSrc := QtdeSrc * MyFator;
      end else
        QtdeSrc := 0;
    end else
      QtdeSrc := 0;
    //
    if QtdeSrc < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (15)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo), QrIts);
    //
    case TipoRegSPEDProd of
      trsp21X:
      begin
        EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K210(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(OriMovID), Codigo, MovimCod, Controle, DtHrAberto,
        DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO, FornecMO, EntiSitio,
        QtdeSrc, OrigemOpeProc, oidk210RibDTA, FDiaFim, FTipoPeriodoFiscal,
        MeAviso, IDSeq1);
        if EhDoMes then
        begin
          if (PecasDst <> 0) (*and (DtHrFimOpe > 1)*) then
          begin
            case TipoEstq of
              0: QtdeDst := PecasDst;
              1: QtdeDst := AreaM2Dst;
              2: QtdeDst := PesoKgDst;
              else
              begin
                Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                QrIts.SQL.Text);
                //
                QtdeDst := PecasDst;
              end;
            end;
            //QtdeDst := QtdeDst * MyFator;
          end else
            QtdeDst := 0;
          //
          if QtdeDst < 0 then
            Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade n�o pode ser negativa!!! (16)' + sLineBreak +
            'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), QrIts)
          else if (QtdeDst = 0) and (DtHrFimOpe > 1) then
          begin
            case MovimID of
              emidEmProcCal: DtHrFimOpe := 0;
              emidEmProcCur: DtHrFimOpe := 0;
              else Mensagem(sProcName, MovimCod, Integer(MovimID),
              'Quantidade zerada para OP encerrada!!! (8)', QrIts);
            end;
          end;
          //
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, (*LinArqPai*)IDSeq1, Data, DtMovim, DtCorrApo, GGXDst,
          QtdeDst, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibDTA,
          ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
      end;
      else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(17)');
    end;
  end;
  //
  //
  function RegistroK215_SubPrd(const IDSeq1: Integer): Boolean;
  var
    Data, DtMovim, DtCorrApo: TDateTime;
    QtdeDst, PecasDst, PesoKgDst, AreaM2Dst: Double;
    Codigo, MovimCod, MyFator, TipoEstq, GGXDst, ID_Item, Controle, MyMovimID,
    ClientMO, FornecMO, EntiSitio: Integer;
  begin
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    MyMovimID  := QrIts.FieldByName('MovimID').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GraGruX').AsInteger;
    Controle   := QrIts.FieldByName('Controle').AsInteger;
    ID_Item    := Controle;
    //
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibDTA:
      begin
        AreaM2Dst := QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('PesoKg').AsFloat;
        PecasDst  := QrIts.FieldByName('Pecas').AsFloat;
      end;
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //
    //Parei aqui !!!  � asimm???
    if (PecasDst <> 0) or (PesoKgDst <> 0)(*and (DtHrFimOpe > 1)*) then
    begin
      case TipoEstq of
        0: QtdeDst := PecasDst;
        1: QtdeDst := AreaM2Dst;
        2: QtdeDst := PesoKgDst;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrIts.SQL.Text);
          //
          QtdeDst := PecasDst;
        end;
      end;
      //QtdeDst := QtdeDst * MyFator;
      QtdeDst := QtdeDst;
    end else
      QtdeDst := 0;
    //
    if QtdeDst < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (17)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), QrIts);
    //
    EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa, FPeriApu,
    (*LinArqPai*)IDSeq1, Data, DtMovim, DtCorrApo, GGXDst, QtdeDst,
    COD_INS_SUBST, ID_Item, MyMovimID, Codigo, MovimCod, Controle,
    ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibDTA, ESTSTabSorc,
    FTipoPeriodoFiscal, MeAviso);
  end;
  var
    LinArqK210: Integer;
    //Data
    DtIniOS, DtFimOS: TDateTime;
begin
  Result := True;
  // 1. Codigos das entradas de materia-prima que caleirou dentro do mes
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _PRE_GPS_; ',
  'CREATE TABLE _PRE_GPS_ ',
  'SELECT GSPJmpNiv2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE GSPJmpNiv2<>0 ',
  SQL_PeriodoVS,
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_; ',
  'CREATE TABLE _PRE_DESC_ ',
  ' ',
  'SELECT DISTINCT SrcNivel1, 1 Normal ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCur)),
  'AND SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  SQL_PeriodoVS,
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT vmi.Codigo, 2 Normal ',
  'FROM _PRE_GPS_ gps ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  '  ON gps.GSPJmpNiv2=vmi.Controle ',
  '; ',
  ' ',
  'SELECT DISTINCT SrcNivel1 ',
  'FROM _PRE_DESC_ ',
  'ORDER BY SrcNivel1; ',
  ' ',
  '']);
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  PB2.Position := 0;
  PB2.Max := QrOri.RecordCount;
  while not QrOri.Eof do
  begin
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    // 1a. Caleiros das entradas de couro in natura do codigo de entrada atual
    //     para saber o PDA(DTA) (campo RmsGGX)!
    UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
    'SELECT vmi.RmsGGX GGXDst, vmi.JmpGGX GGXSrc, ',
    'cab.Codigo, cab.MovimCod, vmi.SrcNivel2, ',
    'vmi.Controle VMICal, vmi.GraGruX GGXOriCal, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
       VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
       'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'vmi.QtdAntPeso, vmi.QtdAntPeca, vmi.QtdAntArM2, vmi.RmsNivel2, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN vscalcab   cab ON cab.Codigo=vmi.SrcNivel1 ',  // colorado!!!
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND vmi.SrcNivel1=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),  // 1...270...n
    '']);
    //Geral.MB_SQL(Self, QrIts);
    ObtemDataIniEFim(QrOri.FieldByName('SrcNivel1').AsInteger, DtIniOS, DtFimOS);
    //
    QrIts.First;
    while not QrIts.Eof do
    begin
      RegistroK210(TEstqMovimID.emidEmProcCal, DtIniOS, DtFimOS, LinArqK210);
      //
      QrIts.Next;
    end;
    //
    // 1b. Itens de caleiro do codigo atual
    UnDmkDAC_PF.AbreMySQLQuery0(QrMae, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND Codigo=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),
    //SQL_PeriodoVS,
    '']);
    QrMae.First;
    while not QrMae.Eof do
    begin
      // 1b1. Subprodutos gerados do item de entrada atual
      UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
      'SELECT vmi.GraGruX, ',
      'vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
      'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
         VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
         'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
      'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
      'WHERE vmi.GSPJmpNiv2=' + Geral.FF0(QrMae.FieldByName('Controle').AsInteger),
      SQL_PeriodoVS,
      '']);
      //if QrIts.RecordCount > 0 then
        //Geral.MB_SQL(Self, QrIts);
      QrIts.First;
      while not QrIts.Eof do
      begin
        RegistroK215_SubPrd(LinArqK210);
        //
        QrIts.Next;
      end;
      //
      QrMae.Next;
    end;
    //
    QrOri.Next;
  end;
end;

function TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9.Insere_OrigeRibPDA_210(const TipoRegSPEDProd: TTipoRegSPEDProd;
  const MovimID: TEstqMovimID; const MovimNiv: TEstqMovimNiv;
  const SQL_PeriodoVS: String; const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  COD_INS_SUBST = '';
  ESTSTabSorc = estsVMI;
  sProcName = 'FmSpedEfdIcmsIpiProducaoIsolada.ProducaoSingular_Insere_OrigeRibPDA_210';
  //
var
  QrIts: TmySQLQuery;
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_PeriodoVS;
      //trsp23X: Result := SQL_PeriodoVS;
      //trsp25X: Result := SQL_PeriodoVS;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (7)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
  //
  // Obtem data inicial e final de movimentos de caleiro e geracao de subprodutos da entrada selecionada!
  procedure ObtemDataIniEFim(const Codigo: Integer; var Ini, Fim: TDateTime);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDatas, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _PRE_DESC_ORI_MP_; ',
    'CREATE TABLE _PRE_DESC_ORI_MP_ ',
    'SELECT Controle  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND Codigo=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _PRE_DESC_DTIF_; ',
    ' ',
    'CREATE TABLE _PRE_DESC_DTIF_ ',
    'SELECT vmi.Controle, vmi.DataHora ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND vmi.SrcNivel1=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'INSERT IGNORE INTO _PRE_DESC_DTIF_ ',
    ' ',
    'SELECT vmi.Controle, vmi.DataHora  ',
    'FROM _PRE_DESC_ORI_MP_ ori  ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    '  ON ori.Controle=vmi.GSPSrcNiv2 ',
    'WHERE vmi.GSPSrcNiv2 <> 0 ',
    'AND vmi.GSPJmpNiv2=0; ',
    ' ',
    'SELECT MIN(DataHora) MinDH,  ',
    'MAX(DataHora) MaxDH  ',
    'FROM _PRE_DESC_DTIF_ ',
    'WHERE Controle > 0 ',
    '']);
    //Geral.MB_SQL(Self, QrDatas);
    //
    Ini := QrDatasMinDH.Value;
    Fim := QrDatasMaxDH.Value;
  end;
  //
  function RegistroK210(const OriMovID: TEstqMovimID; const DtIniOS, DtFimOS:
  TDateTime; var IDSeq1: Integer): Boolean;
  var
    DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
    QtdeSrc, QtdeDst, PecasSrc, PesoKgSrc, AreaM2Src, PecasDst, PesoKgDst,
    AreaM2Dst: Double;
    MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, GGXDst, ID_Item, Controle,
    ClientMO, FornecMO, EntiSitio: Integer;
    EhDoMes: Boolean;
  begin
    IDSeq1     := 0;
    AreaM2Src  := 0;
    PesoKgSrc  := 0;
    PecasSrc   := 0;
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := -1;
    QtdeSrc    := 0;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    DtHrAberto := DtIniOS;
    DtHrFimOpe := DtFimOS;
    GGXSrc     := QrIts.FieldByName('GGXSrc').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GGXDst').AsInteger;
    Controle   := QrIts.FieldByName('RmsNivel2').AsInteger;
    ID_Item    := Controle;
    // 270
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibPDA:
      begin
        AreaM2Src := QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgSrc := QrIts.FieldByName('PesoKg').AsFloat;
        PecasSrc  := QrIts.FieldByName('Pecas').AsFloat;
        //
        AreaM2Dst := QrIts.FieldByName('QtdAntArM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('QtdAntPeso').AsFloat;
        PecasDst  := QrIts.FieldByName('QtdAntPeca').AsFloat;
      end;
      else
      begin
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        Exit;
      end;
    end;
    //
    EhDoMes := (Data >= FDiaIni) and (Data < FDiaPos);
    if EhDoMes then
    begin
      if (PecasSrc <> 0) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          0: QtdeSrc := PecasSrc;
          1: QtdeSrc := AreaM2Src;
          2: QtdeSrc := PesoKgSrc;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrIts.SQL.Text);
            //
            QtdeSrc := PecasSrc;
          end;
        end;
        QtdeSrc := QtdeSrc * MyFator;
      end else
        QtdeSrc := 0;
    end else
      QtdeSrc := 0;
    //
    if QtdeSrc < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (18)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo), nil);
    //
    case TipoRegSPEDProd of
      trsp21X:
      begin
        EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K210(FImporExpor, FAnoMes, FEmpresa,
        FPeriApu, Integer(OriMovID), Codigo, MovimCod, Controle, DtHrAberto,
        DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO, FornecMO, EntiSitio,
        QtdeSrc, OrigemOpeProc, oidk210RibPDA, FDiaFim, FTipoPeriodoFiscal,
        MeAviso, IDSeq1);
        //
        if EhDoMes then
        begin
          if (PecasDst <> 0) (*and (DtHrFimOpe > 1)*) then
          begin
            case TipoEstq of
              0: QtdeDst := PecasDst;
              1: QtdeDst := AreaM2Dst;
              2: QtdeDst := PesoKgDst;
              else
              begin
                Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                QrIts.SQL.Text);
                //
                QtdeDst := PecasDst;
              end;
            end;
            //QtdeDst := QtdeDst * MyFator;
          end else
            QtdeDst := 0;
          //
          if QtdeDst < 0 then
            Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade n�o pode ser negativa!!! (19)' + sLineBreak +
            'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), nil)
          else if (QtdeDst = 0) and (DtHrFimOpe > 1) then
          begin
            case MovimID of
              emidEmProcCal: DtHrFimOpe := 0;
              emidEmProcCur: DtHrFimOpe := 0;
              else Mensagem(sProcName, MovimCod, Integer(MovimID),
              'Quantidade zerada para OP encerrada!!! (8)', nil);
            end;
          end;
          //
          EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa,
          FPeriApu, (*LinArqPai*)IDSeq1, Data, DtMovim, DtCorrApo, GGXDst,
          QtdeDst, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibPDA,
          ESTSTabSorc, FTipoPeriodoFiscal, MeAviso);
        end;
      end;
      else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(18)');
    end;
  end;
  //
  //
  function RegistroK215_SubPrd(const IDSeq1: Integer): Boolean;
  var
    Data, DtMovim, DtCorrApo: TDateTime;
    QtdeDst, PecasDst, PesoKgDst, AreaM2Dst: Double;
    Codigo, MovimCod, MyFator, TipoEstq, GGXDst, ID_Item, Controle,
    ClientMO, FornecMO, EntiSitio, MyMovimID: Integer;
  begin
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    MyMovimID  := QrIts.FieldByName('MovimID').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GraGruX').AsInteger;
    Controle   := QrIts.FieldByName('Controle').AsInteger;
    ID_Item    := Controle;
    //
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibPDA:
      begin
        AreaM2Dst := QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('PesoKg').AsFloat;
        PecasDst  := QrIts.FieldByName('Pecas').AsFloat;
      end;
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //
    //Parei aqui !!!  � asimm???
    if (PecasDst <> 0) or (PesoKgDst <> 0)(*and (DtHrFimOpe > 1)*) then
    begin
      case TipoEstq of
        0: QtdeDst := PecasDst;
        1: QtdeDst := AreaM2Dst;
        2: QtdeDst := PesoKgDst;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrIts.SQL.Text);
          //
          QtdeDst := PecasDst;
        end;
      end;
      //QtdeDst := QtdeDst * MyFator;
      QtdeDst := QtdeDst;
    end else
      QtdeDst := 0;
    //
    if QtdeDst < 0 then
      Mensagem(sProcName, MovimCod, Integer(MovimID),
      'Quantidade n�o pode ser negativa!!! (20)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), nil);
    //
    EfdIcmsIpi_PF_v03_0_9.InsereItemAtual_K215(FImporExpor, FAnoMes, FEmpresa, FPeriApu,
    (*LinArqPai*)IDSeq1, Data, DtMovim, DtCorrApo, GGXDst, QtdeDst,
    COD_INS_SUBST, ID_Item, MyMovimID, Codigo, MovimCod, Controle, ClientMO,
    FornecMO, EntiSitio, OrigemOpeProc, oidk210RibPDA, ESTSTabSorc,
    FTipoPeriodoFiscal, MeAviso);
  end;
var
  LinArqK210: Integer;
  //Data
  DtIniOS, DtFimOS: TDateTime;
begin
  QrIts := TmySQLQuery.Create(Dmod);
  try
  Result := True;
  // 1. Codigos das entradas de materia-prima que caleirou dentro do mes
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _PRE_GPS_; ',
  'CREATE TABLE _PRE_GPS_ ',
  'SELECT GSPSrcNiv2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE GSPSrcNiv2<>0 ',
  'AND GSPJmpNiv2=0 ',
  SQL_PeriodoVS,
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_; ',
  'CREATE TABLE _PRE_DESC_ ',
  ' ',
  'SELECT DISTINCT SrcNivel1, 1 Normal ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  'AND SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  SQL_PeriodoVS,
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT vmi.Codigo, 2 Normal ',
  'FROM _PRE_GPS_ gps ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  '  ON gps.GSPSrcNiv2=vmi.Controle ',
  '; ',
  ' ',
  'SELECT DISTINCT SrcNivel1 ',
  'FROM _PRE_DESC_ ',
  'ORDER BY SrcNivel1; ',
  ' ',
  '']);
  //if QrOri.RecordCount > 0 then
    //Geral.MB_Aviso('DEPRRCAR!!! 210 - 1');
  //
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  PB2.Position := 0;
  PB2.Max := QrOri.RecordCount;
  while not QrOri.Eof do
  begin
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    // 1a. Caleiros das entradas de couro in natura do codigo de entrada atual
    //     para saber o PDA (campo RmsGGX)!
    UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
    'SELECT vmi.RmsGGX GGXDst, vmi.GraGruX GGXSrc, ',
    'cab.Codigo, cab.MovimCod, vmi.SrcNivel2, ',
    'vmi.Controle VMICal, vmi.GraGruX GGXOriCal, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
       VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
       'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'vmi.QtdAntPeso, vmi.QtdAntPeca, vmi.QtdAntArM2, vmi.RmsNivel2, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN vsinncab   cab ON cab.Codigo=vmi.SrcNivel1 ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND vmi.SrcNivel1=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),  // 1...270...n
    //SQL_PeriodoVS,
    '']);
    //Geral.MB_SQL(Self, QrIts);
    ObtemDataIniEFim(QrOri.FieldByName('SrcNivel1').AsInteger, DtIniOS, DtFimOS);
    //
    QrIts.First;
    while not QrIts.Eof do
    begin
      //Data := QrIts.FieldByName('DATA').AsDateTime;
      //if (Data >= DiaIni) and (Data < DiaPos) then
      RegistroK210(TEstqMovimID.emidCompra, DtIniOS, DtFimOS, LinArqK210);
      //
      QrIts.Next;
    end;
    //
    // 1b. Itens de entrada de materia-prima do codigo atual
    UnDmkDAC_PF.AbreMySQLQuery0(QrMae, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND Codigo=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),  // 1...270...n
    //SQL_PeriodoVS,
    '']);
    //Geral.MB_SQL(Self, QrMae);
    QrMae.First;
    while not QrMae.Eof do
    begin
      // 1b1. Subprodutos gerados do item de entrada atual
      UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
      'SELECT vmi.GraGruX, ',
      'vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
      'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
         VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
         'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
      'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
      'WHERE vmi.GSPJmpNiv2=0 ',
      'AND vmi.GSPSrcNiv2=' + Geral.FF0(QrMae.FieldByName('Controle').AsInteger),
      SQL_PeriodoVS,
      '']);
      //Geral.MB_SQL(Self, QrIts);
      QrIts.First;
      while not QrIts.Eof do
      begin
        RegistroK215_SubPrd(LinArqK210);
        //
        QrIts.Next;
      end;
      //
      QrMae.Next;
    end;
    //
    QrOri.Next;
  end;
  finally
    QrIts.Free;
  end;
end;

procedure TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_9.Mensagem(ProcName: String; MovimCod,
  MovimID: Integer; TextoBase: String; Query: TmySQLQuery);
var
  Qry_Text: String;
begin
  if Query <> nil then
    Qry_Text := Query.SQL.Text
  else
    Qry_Text := '';
  //
  Geral.MB_ERRO(TextoBase + sLineBreak +
  'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
  'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
  ProcName + sLineBreak +
  sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
  ProcName + sLineBreak +
  Qry_Text);
end;

end.
