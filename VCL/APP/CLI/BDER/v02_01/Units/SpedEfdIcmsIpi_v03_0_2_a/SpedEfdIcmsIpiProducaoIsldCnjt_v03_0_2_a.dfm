object FmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a: TFmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a
  Left = 0
  Top = 0
  Caption = 'FmSpedEfdIcmsIpiProducaoIsldCnjt_v03_0_2_a'
  ClientHeight = 806
  ClientWidth = 894
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object QrDatas: TmySQLQuery
    Database = Dmod.MyDB
    Left = 40
    Top = 4
    object QrDatasMinDH: TDateTimeField
      FieldName = 'MinDH'
    end
    object QrDatasMaxDH: TDateTimeField
      FieldName = 'MaxDH'
    end
  end
  object QrOri: TmySQLQuery
    Database = Dmod.MyDB
    Left = 744
    Top = 432
  end
  object QrMae: TmySQLQuery
    Database = Dmod.MyDB
    Left = 744
    Top = 484
  end
  object QrMC: TmySQLQuery
    Database = Dmod.MyDB
    Left = 140
    Top = 4
    object QrMCMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
  end
  object QrProdRib: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _SPED_EFD_K2XX_O_P;'
      'CREATE TABLE _SPED_EFD_K2XX_O_P'
      'SELECT DATE(vmi.DataHora) Data,  cab.GGXDst GGX_Dst, '
      'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, med.Grandeza'
      'FROM bluederm_meza.vsopecab cab'
      
        'LEFT JOIN bluederm_meza.vsmovits vmi ON vmi.MovimCod=cab.MovimCo' +
        'd'
      'LEFT JOIN bluederm_meza.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm_meza.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm_meza.unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59"'
      'AND cab.GGXDst <> vmi.GragruX'
      'AND MovimNiv=9'
      'UNION'
      'SELECT DATE(vmi.DataHora) Data,  cab.GGXDst GGX_Dst, '
      'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, med.Grandeza'
      'FROM bluederm_meza.vspwecab cab'
      
        'LEFT JOIN bluederm_meza.vsmovits vmi ON vmi.MovimCod=cab.MovimCo' +
        'd'
      'LEFT JOIN bluederm_meza.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm_meza.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm_meza.unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59"'
      'AND cab.GGXDst <> vmi.GragruX'
      'AND MovimNiv=22'
      ';'
      'SELECT * FROM _SPED_EFD_K2XX_O_P')
    Left = 136
    Top = 52
    object QrProdRibData: TDateField
      FieldName = 'Data'
    end
    object QrProdRibGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrProdRibGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrProdRibPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrProdRibAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrProdRibPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrProdRibMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrProdRibCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdRibMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrProdRibControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProdRibEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrProdRibQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrProdRibQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrProdRibQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrProdRibsTipoEstq: TFloatField
      FieldName = 'sTipoEstq'
    end
    object QrProdRibdTipoEstq: TFloatField
      FieldName = 'dTipoEstq'
    end
    object QrProdRibMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrProdRibDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrProdRibClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrProdRibFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrProdRibEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
  end
  object QrObtCliForSite: TmySQLQuery
    Database = Dmod.MyDB
    Left = 136
    Top = 100
  end
  object QrIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 240
    Top = 4
  end
  object QrCalToCur: TmySQLQuery
    Database = Dmod.MyDB
    Left = 336
    Top = 4
    object QrCalToCurGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrConsParc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes,'
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, Sum(PesoKg) PesoKg '
      'FROM vsmovits '
      'GROUP BY Ano, Mes'
      '')
    Left = 336
    Top = 52
    object QrConsParcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrConsParcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrConsParcPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrConsParcAno: TFloatField
      FieldName = 'Ano'
    end
    object QrConsParcMes: TFloatField
      FieldName = 'Mes'
    end
  end
  object QrProdParc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes,'
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, Sum(PesoKg) PesoKg '
      'FROM vsmovits '
      'GROUP BY Ano, Mes'
      '')
    Left = 336
    Top = 100
    object QrProdParcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrProdParcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrProdParcPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrProdParcClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrProdParcFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrProdParcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProdParcAno: TFloatField
      FieldName = 'Ano'
    end
    object QrProdParcMes: TFloatField
      FieldName = 'Mes'
    end
  end
  object QrPeriodos: TmySQLQuery
    Database = Dmod.MyDB
    Left = 336
    Top = 148
    object QrPeriodosAno: TFloatField
      FieldName = 'Ano'
    end
    object QrPeriodosMes: TFloatField
      FieldName = 'Mes'
    end
    object QrPeriodosPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
  object QrVmiGArCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE(DataHora) Data, MovimCod, GraGruX, '
      'Pecas, AreaM2, PesoKg, MovimTwn '
      'FROM vsmovits '
      'WHERE MovimNiv=14 '
      'AND DATE(DataHora) BETWEEN "2016-09-01" AND "2016-09-30" '
      'AND Empresa=-11 '
      'ORDER BY Data, MovimCod, GraGruX ')
    Left = 744
    Top = 532
    object QrVmiGArCabData: TDateField
      FieldName = 'Data'
    end
    object QrVmiGArCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVmiGArCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVmiGArCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVmiGArCabPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVmiGArCabAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVmiGArCabPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVmiGArCabMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVmiGArCabUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrVmiGArCabmed_grandeza: TIntegerField
      FieldName = 'med_grandeza'
    end
    object QrVmiGArCabxco_grandeza: TIntegerField
      FieldName = 'xco_grandeza'
    end
    object QrVmiGArCabGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVmiGArCabCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrVmiGArCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVmiGArCabTipoEstq: TFloatField
      FieldName = 'TipoEstq'
    end
    object QrVmiGArCabDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVmiGArCabClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVmiGArCabFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVmiGArCabEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
    object QrVmiGArCabQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
  end
  object QrSumGAR: TmySQLQuery
    Database = Dmod.MyDB
    Left = 432
    Top = 52
    object QrSumGARPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumGARAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumGARPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object QrVmiGArIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE(DataHora) Data, MovimCod, GraGruX, '
      'Pecas, AreaM2, PesoKg, MovimTwn '
      'FROM vsmovits '
      'WHERE MovimNiv=14 '
      'AND DATE(DataHora) BETWEEN "2016-09-01" AND "2016-09-30" '
      'AND Empresa=-11 '
      'ORDER BY Data, MovimCod, GraGruX ')
    Left = 744
    Top = 580
    object QrVmiGArItsData: TDateField
      FieldName = 'Data'
    end
    object QrVmiGArItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVmiGArItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVmiGArItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVmiGArItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVmiGArItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVmiGArItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVmiGArItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVmiGArItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrVmiGArItsmed_grandeza: TIntegerField
      FieldName = 'med_grandeza'
    end
    object QrVmiGArItsxco_grandeza: TIntegerField
      FieldName = 'xco_grandeza'
    end
    object QrVmiGArItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVmiGArItsCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrVmiGArItsTipoEstq: TLargeintField
      FieldName = 'TipoEstq'
    end
    object QrVmiGArItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVmiGArItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVmiGArItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVmiGArItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVmiGArItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVmiGArItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVmiGArItsJmpGGX: TIntegerField
      FieldName = 'JmpGGX'
    end
  end
  object QrIMECs_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM  _seii_imecs_')
    Left = 740
    Top = 48
    object QrIMECs_MovCodPai: TIntegerField
      FieldName = 'MovCodPai'
    end
    object QrIMECs_Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrIMECs_MovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrIMECs_MovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrIMECs_AnoCA: TIntegerField
      FieldName = 'AnoCA'
      Required = True
    end
    object QrIMECs_MesCA: TIntegerField
      FieldName = 'MesCA'
      Required = True
    end
    object QrIMECs_TipoTab: TWideStringField
      FieldName = 'TipoTab'
      Required = True
      Size = 15
    end
    object QrIMECs_Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrIMECs_JmpMovID: TIntegerField
      FieldName = 'JmpMovID'
    end
    object QrIMECs_JmpNivel1: TIntegerField
      FieldName = 'JmpNivel1'
    end
    object QrIMECs_JmpNivel2: TIntegerField
      FieldName = 'JmpNivel2'
    end
  end
  object QrVSRibAtu_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 740
    Top = 145
    object QrVSRibAtu_Codigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRibAtu_Controle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRibAtu_MovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRibAtu_MovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRibAtu_MovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRibAtu_Empresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRibAtu_Terceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRibAtu_CliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRibAtu_MovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRibAtu_DataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRibAtu_Pallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRibAtu_GraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRibAtu_Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibAtu_PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibAtu_AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtu_AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtu_ValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtu_SrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRibAtu_SrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRibAtu_SrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRibAtu_SrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRibAtu_SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRibAtu_SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibAtu_SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtu_Observ: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRibAtu_SerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRibAtu_Ficha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRibAtu_Misturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRibAtu_FornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRibAtu_CustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSRibAtu_CustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSRibAtu_CustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtu_ValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtu_DstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRibAtu_DstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRibAtu_DstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRibAtu_DstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRibAtu_QtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRibAtu_QtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibAtu_QtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtu_QtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtu_QtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRibAtu_QtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibAtu_QtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtu_QtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtu_NotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSRibAtu_NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRibAtu_NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRibAtu_NO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRibAtu_ID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRibAtu_NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRibAtu_ReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRibAtu_CUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSRibAtu_CUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSRibAtu_NO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSRibAtu_Marca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSRibAtu_PedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrVSRibAtu_StqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSRibAtu_NO_FICHA: TWideStringField
      DisplayWidth = 80
      FieldName = 'NO_FICHA'
      Size = 80
    end
    object QrVSRibAtu_NO_FORNEC_MO: TWideStringField
      FieldName = 'NO_FORNEC_MO'
      Size = 100
    end
    object QrVSRibAtu_ClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSRibAtu_CUSTO_KG: TFloatField
      FieldName = 'CUSTO_KG'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSRibAtu_CustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSRibAtu_DtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
  object QrVSRibDst_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 744
    Top = 337
    object QrVSRibDst_Codigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRibDst_Controle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRibDst_MovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRibDst_MovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRibDst_MovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRibDst_Empresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRibDst_Terceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRibDst_CliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRibDst_MovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRibDst_DataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRibDst_Pallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRibDst_GraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRibDst_Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibDst_PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibDst_AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_ValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_SrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRibDst_SrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRibDst_SrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRibDst_SrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRibDst_SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRibDst_SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibDst_SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_Observ: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRibDst_SerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRibDst_Ficha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRibDst_Misturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRibDst_FornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRibDst_CustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_CustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_CustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_ValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_DstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRibDst_DstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRibDst_DstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRibDst_DstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRibDst_QtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRibDst_QtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibDst_QtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_QtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_QtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRibDst_QtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibDst_QtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_QtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_NotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDst_NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRibDst_NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRibDst_NO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRibDst_ID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRibDst_NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRibDst_NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRibDst_ReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRibDst_PedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSRibDst_Marca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSRibDst_StqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSRibDst_NO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 50
    end
    object QrVSRibDst_NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 50
    end
    object QrVSRibDst_DtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
  object QrProc_: TmySQLQuery
    Database = Dmod.MyDB
    Left = 740
    Top = 96
    object QrProc_DtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
    end
    object QrProc_DtHrFimOpe: TDateTimeField
      FieldName = 'DtHrFimOpe'
    end
  end
  object QrVmiTwn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 524
    Top = 244
  end
  object QrJmpA_: TmySQLQuery
    Database = Dmod.MyDB
    Left = 744
    Top = 240
    object QrJmpA_MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrJmpA_Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrJmpA_MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrJmpA_Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrJmpA_SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object QrJmpB_: TmySQLQuery
    Database = Dmod.MyDB
    Left = 744
    Top = 288
    object QrJmpB_MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrJmpB_Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrJmpB_MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrJmpB_Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrVSRibInd_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 744
    Top = 385
    object QrVSRibInd_Codigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRibInd_Controle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRibInd_MovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRibInd_MovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRibInd_MovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRibInd_Empresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRibInd_Terceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRibInd_CliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRibInd_MovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRibInd_DataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRibInd_Pallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRibInd_GraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRibInd_Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibInd_PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibInd_AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_ValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_SrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRibInd_SrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRibInd_SrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRibInd_SrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRibInd_SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRibInd_SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibInd_SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_Observ: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRibInd_SerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRibInd_Ficha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRibInd_Misturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRibInd_FornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRibInd_CustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_CustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_CustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_ValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_DstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRibInd_DstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRibInd_DstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRibInd_DstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRibInd_QtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRibInd_QtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibInd_QtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_QtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_QtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRibInd_QtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibInd_QtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_QtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_NotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibInd_NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRibInd_NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRibInd_NO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRibInd_ID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRibInd_NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRibInd_NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRibInd_ReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRibInd_PedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSRibInd_Marca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSRibInd_StqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSRibInd_NO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 50
    end
    object QrVSRibInd_NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 50
    end
    object QrVSRibInd_DtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSRibInd_JmpMovID: TLargeintField
      FieldName = 'JmpMovID'
    end
    object QrVSRibInd_JmpNivel1: TLargeintField
      FieldName = 'JmpNivel1'
    end
    object QrVSRibInd_JmpNivel2: TLargeintField
      FieldName = 'JmpNivel2'
    end
    object QrVSRibInd_JmpGGX: TLargeintField
      FieldName = 'JmpGGX'
    end
    object QrVSRibInd_RmsGGX: TLargeintField
      FieldName = 'RmsGGX'
    end
  end
  object QrFather_: TmySQLQuery
    Database = Dmod.MyDB
    Left = 740
    object QrFather_MovCodPai: TIntegerField
      FieldName = 'MovCodPai'
    end
  end
  object QrVSRibOriIMEI_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 744
    Top = 193
    object QrVSRibOriIMEI_Codigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRibOriIMEI_Controle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRibOriIMEI_MovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRibOriIMEI_MovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRibOriIMEI_MovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRibOriIMEI_Empresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRibOriIMEI_Terceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRibOriIMEI_CliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRibOriIMEI_MovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRibOriIMEI_DataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRibOriIMEI_Pallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRibOriIMEI_GraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRibOriIMEI_Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibOriIMEI_PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibOriIMEI_AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_ValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_SrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRibOriIMEI_SrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRibOriIMEI_SrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRibOriIMEI_SrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRibOriIMEI_SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRibOriIMEI_SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibOriIMEI_SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_Observ: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRibOriIMEI_SerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRibOriIMEI_Ficha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRibOriIMEI_Misturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRibOriIMEI_FornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRibOriIMEI_CustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_CustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_CustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_ValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_DstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRibOriIMEI_DstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRibOriIMEI_DstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRibOriIMEI_DstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRibOriIMEI_QtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRibOriIMEI_QtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibOriIMEI_QtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_QtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_QtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRibOriIMEI_QtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibOriIMEI_QtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_QtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_NotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSRibOriIMEI_NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRibOriIMEI_NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRibOriIMEI_NO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRibOriIMEI_ID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRibOriIMEI_NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRibOriIMEI_NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRibOriIMEI_ReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRibOriIMEI_CustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEI_VSMulFrnCab: TLargeintField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSRibOriIMEI_ClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSRibOriIMEI_RmsMovID: TLargeintField
      FieldName = 'RmsMovID'
    end
    object QrVSRibOriIMEI_RmsNivel1: TLargeintField
      FieldName = 'RmsNivel1'
    end
    object QrVSRibOriIMEI_RmsNivel2: TLargeintField
      FieldName = 'RmsNivel2'
    end
    object QrVSRibOriIMEI_DtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSRibOriIMEI_RmsGGX: TLargeintField
      FieldName = 'RmsGGX'
    end
  end
  object QrCab34: TmySQLQuery
    Database = Dmod.MyDB
    Left = 524
    Top = 484
    object QrCab34MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
  end
  object QrPQxMixOP: TmySQLQuery
    Database = Dmod.MyDB
    Left = 628
    object QrPQxMixOPOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQxMixOPCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQxMixOPDtCorrApo: TDateField
      FieldName = 'DtCorrApo'
    end
  end
  object QrPQMCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'lse.Nome NOMESETOR, reb.Linhas RebLin, '
      'emg.Nome NO_EMITGRU, gcc.Nome NoGraCorCad, pqm.*'
      'FROM pqm pqm'
      'LEFT JOIN entidades emp ON emp.Codigo=pqm.Empresa'
      'LEFT JOIN listasetores lse ON lse.Codigo=pqm.Setor'
      'LEFT JOIN emitgru emg ON emg.Codigo=pqm.EmitGru'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=pqm.GraCorCad'
      'LEFT JOIN espessuras reb ON reb.Codigo=pqm.SemiCodEspReb'
      'WHERE pqm.Codigo > 0'
      'AND pqm.Empresa=-11')
    Left = 628
    Top = 49
    object QrPQMCabDataB: TDateField
      FieldName = 'DataB'
    end
    object QrPQMCabVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
    end
  end
  object QrPQMBxa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  '
      'pqg.Nome NOMEGRUPO '
      'FROM pqx pqx '
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo '
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ '
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico '
      'WHERE pqx.Tipo=130 '
      'AND pqx.OrigemCodi>0 '
      ' ')
    Left = 628
    Top = 145
    object QrPQMBxaOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQMBxaDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQMBxaInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQMBxaPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object QrPQMGer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  '
      'pqg.Nome NOMEGRUPO '
      'FROM pqx pqx '
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo '
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ '
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico '
      'WHERE pqx.Tipo=130 '
      'AND pqx.OrigemCodi>0 '
      ' ')
    Left = 628
    Top = 97
    object QrPQMGerOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQMGerDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQMGerInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQMGerPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object Qry: TmySQLQuery
    Database = Dmod.MyDB
    Left = 36
    Top = 484
  end
  object DqFather: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 824
  end
  object DqIMECs: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 824
    Top = 48
  end
  object DqProc: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 824
    Top = 96
  end
  object DqVSRibAtu: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 824
    Top = 144
  end
  object DqVSRibOriIMEI: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 828
    Top = 192
  end
  object DqJmpA: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 828
    Top = 240
  end
  object DqJmpB: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 828
    Top = 288
  end
  object DqVSRibDst: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 832
    Top = 336
  end
  object DqVSRibInd: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 832
    Top = 384
  end
  object QrMinData: TmySQLQuery
    Database = Dmod.MyDB
    Left = 204
    Top = 352
  end
  object DqOri: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 832
    Top = 432
  end
  object DqIts: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 832
    Top = 480
  end
  object DqVmiGArCab: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 832
    Top = 532
  end
  object DqVmiGArIts: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 832
    Top = 580
  end
end
