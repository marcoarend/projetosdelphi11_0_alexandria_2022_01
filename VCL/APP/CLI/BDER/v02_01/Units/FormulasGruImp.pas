unit FormulasGruImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker,
  frxClass, frxDBSet, dmkDBLookupComboBox, dmkEditCB, Vcl.Mask, Vcl.Menus;

type
  TRelConsGru = (rcgNenhum=0,
    rcgConsPQGru=1, rcgConsPQLst=2,
    rcgProdPQGru=3, rcgProdPQLst=4);
  //
  TFmFormulasGruImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    Label4: TLabel;
    TPIni: TdmkEditDateTimePicker;
    Label5: TLabel;
    TPFim: TdmkEditDateTimePicker;
    QrFormulasGru: TMySQLQuery;
    QrFormulasGruCodigo: TIntegerField;
    QrFormulasGruNome: TWideStringField;
    DsFormulasGru: TDataSource;
    Panel6: TPanel;
    QrFormulasSel: TMySQLQuery;
    DsFormulasSel: TDataSource;
    QrFormulasSelNumero: TIntegerField;
    QrFormulasSelNome: TWideStringField;
    frxQUI_RECEI_027_00: TfrxReport;
    QrInsumos: TMySQLQuery;
    QrInsumosInsumo: TIntegerField;
    QrInsumosNome: TWideStringField;
    QrInsumosPeso: TFloatField;
    QrInsumosValor: TFloatField;
    QrInsumosCustoUni: TFloatField;
    frxDsInsumos: TfrxDBDataset;
    QrCI: TMySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    DsCI: TDataSource;
    LaCI: TLabel;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    DBGrid1: TDBGrid;
    Splitter2: TSplitter;
    DataSource1: TDataSource;
    QrGruSel: TMySQLQuery;
    QrGruSelCodigo: TIntegerField;
    QrGruSelNome: TWideStringField;
    DsGruSel: TDataSource;
    frxDsGruSel: TfrxDBDataset;
    PCRelatorios: TPageControl;
    TabSheet1: TTabSheet;
    Panel5: TPanel;
    Splitter1: TSplitter;
    Panel7: TPanel;
    Label1: TLabel;
    DBGFormulasGru: TDBGrid;
    Panel8: TPanel;
    Label2: TLabel;
    DBGFormulas: TDBGrid;
    TabSheet2: TTabSheet;
    QrFormulasLst: TMySQLQuery;
    DsFormulasLst: TDataSource;
    Panel9: TPanel;
    DBGFormulasLst: TDBGrid;
    QrFormulasLstNumero: TIntegerField;
    QrFormulasLstNome: TWideStringField;
    Panel10: TPanel;
    Label3: TLabel;
    Panel11: TPanel;
    Panel12: TPanel;
    MeFormulas: TMaskEdit;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    frxQUI_RECEI_027_01: TfrxReport;
    QrEmit0: TMySQLQuery;
    frxDsEmit0: TfrxDBDataset;
    PMImprime: TPopupMenu;
    ConsumodeInsumos1: TMenuItem;
    Produodospreocessos1: TMenuItem;
    QrEmit0DtaBaixa: TDateField;
    QrEmit0NOMESETOR: TWideStringField;
    QrEmit0Numero: TIntegerField;
    QrEmit0Nome: TWideStringField;
    QrEmit0Peso: TFloatField;
    QrEmit0Custo: TFloatField;
    QrEmit0Qtde: TFloatField;
    QrEmit0AreaM2: TFloatField;
    QrEmit0AREAP2: TFloatField;
    QrEmit0Codigo: TIntegerField;
    QrEmit0DefPeca: TWideStringField;
    QrEmit0FormulasGru: TIntegerField;
    QrEmit0NO_FormulasGru: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrFormulasGruBeforeClose(DataSet: TDataSet);
    procedure QrFormulasGruAfterScroll(DataSet: TDataSet);
    procedure frxQUI_RECEI_027_00GetValue(const VarName: string;
      var Value: Variant);
    procedure MeFormulasChange(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure ConsumodeInsumos1Click(Sender: TObject);
    procedure Produodospreocessos1Click(Sender: TObject);
  private
    { Private declarations }
    FRelConsGru: TRelConsGru;
    //
    function  ReopenInsumosGru(): Boolean;
    function  ReopenInsumosLst(): Boolean;
    procedure ReopenFormulasLst();
    //
    procedure ImprimeConsumoGrupoFormulas();
    procedure ImprimeConsumoListaFormulas();

    //procedure ImprimeProducaoGrupoFormulas();
    //procedure ImprimeProducaoListaFormulas();

    procedure ReopenEmitX(Qry: TmySQLQuery; RelConsGru: TRelConsGru);


  public
    { Public declarations }
    FDataI, FDataF, FCorda: String;
    FCI: Integer;
  end;

  var
  FmFormulasGruImp: TFmFormulasGruImp;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UnDmkProcFunc;

{$R *.DFM}

procedure TFmFormulasGruImp.BtNenhumClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGFormulasLst), False);
end;

procedure TFmFormulasGruImp.BtOKClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtOK);
end;

procedure TFmFormulasGruImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulasGruImp.BtTodosClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGFormulasLst), True);
end;

procedure TFmFormulasGruImp.ConsumodeInsumos1Click(Sender: TObject);
begin
  case PCRelatorios.ActivePageIndex of
    0: ImprimeConsumoGrupoFormulas();
    1: ImprimeConsumoListaFormulas();
    else Geral.MB_Erro('Tipo de relat�rio n�o definido (1)');
  end;
end;

procedure TFmFormulasGruImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormulasGruImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPIni.Date := Date - 30;
  TPFim.Date := Date;
  //
  UnDmkDAC_PF.AbreQuery(QrFormulasGru, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  ReopenFormulasLst();
end;

procedure TFmFormulasGruImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulasGruImp.frxQUI_RECEI_027_00GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt('Empresa: ', CBCI.Text, EdCI.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_PERIODO' then
    Value := dmkPF.PeriodoImp1(TPIni.Date, TPFim.Date, True, True, '', 'at�', '')
  else
  if VarName ='VARF_NOME_REL' then
  begin
    case FRelConsGru of
      TRelConsGru.rcgConsPQGru:
        Value := 'CONSUMO DE INSUMOS POR GRUPOS DE F�RMULAS';
      TRelConsGru.rcgConsPQLst:
        Value := 'CONSUMO DE INSUMOS POR LISTA DE F�RMULAS';
      TRelConsGru.rcgProdPQGru:
        Value := 'PRODU��O POR GRUPOS DE F�RMULAS';
      TRelConsGru.rcgProdPQLst:
        Value := 'PRODU��O POR LISTA DE F�RMULAS';
      else Value := '?1?1?1?';
    end;
  end else
  if VarName ='VARF_TITULO_1' then
  begin
    case FRelConsGru of
      TRelConsGru.rcgConsPQGru,
      TRelConsGru.rcgProdPQGru:
        Value := 'Nome do Grupo de F�rmulas';
      TRelConsGru.rcgConsPQLst,
      TRelConsGru.rcgProdPQLst:
        Value := 'Nome da F�rmula';
      else Value := '?2?2?2?';
    end;
  end else
end;

procedure TFmFormulasGruImp.ImprimeConsumoGrupoFormulas();
begin
  FRelConsGru := TRelConsGru.rcgConsPQGru;
  //
  if MyObjects.FIC(DBGFormulasGru.SelectedRows.Count = 0, nil,
  'Selecione pelo menos um grupo de f�rmulas!') then
    Exit;
  //
  FDataI := Geral.FDT(TPIni.Date, 1);
  FDataF := Geral.FDT(TPFim.Date, 1);
  FCI    := Geral.IMV(EdCI.Text);
  FCorda := MyObjects.CordaDeDBGridSelectedRows(DBGFormulasGru, QrFormulasGru, 'Codigo', True);
  //
  if not ReopenInsumosGru() then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGruSel, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM formulasgru ',
  'WHERE Codigo IN (' + FCorda + ')',
  'ORDER BY Nome ',
  '']);
  MyObjects.frxDefineDataSets(frxQUI_RECEI_027_00, [
  DModG.frxDsDono,
  frxDsGruSel,
  frxDsInsumos
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_027_00, 'Insumos utilizados');
end;

procedure TFmFormulasGruImp.ImprimeConsumoListaFormulas();
begin
  FRelConsGru := TRelConsGru.rcgConsPQLst;
  //
  if MyObjects.FIC(DBGFormulasLst.SelectedRows.Count = 0, nil,
  'Selecione pelo menos uma f�rmula!') then
    Exit;
  //
  FDataI := Geral.FDT(TPIni.Date, 1);
  FDataF := Geral.FDT(TPFim.Date, 1);
  FCI    := Geral.IMV(EdCI.Text);
  FCorda := MyObjects.CordaDeDBGridSelectedRows(DBGFormulasLst, QrFormulasLst, 'Numero', True);
  //
  if not ReopenInsumosLst() then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGruSel, Dmod.MyDB, [
  'SELECT Numero Codigo, Nome ',
  'FROM formulas ',
  'WHERE Numero IN (' + FCorda + ')',
  'ORDER BY Nome ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxQUI_RECEI_027_00, [
  DModG.frxDsDono,
  frxDsGruSel,
  frxDsInsumos
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_027_00, 'Insumos utilizados');
end;

procedure TFmFormulasGruImp.MeFormulasChange(Sender: TObject);
begin
  ReopenFormulasLst();
end;

procedure TFmFormulasGruImp.Produodospreocessos1Click(Sender: TObject);
{
procedure ImprimeProducaoGrupoFormulas();
begin
  ReopenEmitX(QrEmit0, TRelConsGru.rcgProdPQGru);
  //
  MyObjects.frxDefineDataSets(frxQUI_RECEI_027_01, [
  DModG.frxDsDono,
  frxDsEmit0
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_027_01, 'Insumos utilizados');
end;

procedure ImprimeProducaoListaFormulas();
begin
  ReopenEmitX(QrEmit0, TRelConsGru.rcgProdPQLst);
  //
  MyObjects.frxDefineDataSets(frxQUI_RECEI_027_01, [
  DModG.frxDsDono,
  frxDsEmit0
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_027_01, 'Insumos utilizados');
end;
}
var
  Grupo1, Grupo2: TfrxGroupHeader;
  Futer1, Futer2: TfrxGroupFooter;
  Me_GH1, Me_GF1: TfrxMemoView;
begin
  Grupo1 := frxQUI_RECEI_027_01.FindObject('GH_01') as TfrxGroupHeader;
  //Futer1 := frxWET_CURTI_018_13_A.FindObject('FT_01') as TfrxGroupFooter;
  //Futer1.Visible := Grupo1.Visible;
  Me_GH1 := frxQUI_RECEI_027_01.FindObject('Me_GH1') as TfrxMemoView;
  Me_GF1 := frxQUI_RECEI_027_01.FindObject('Me_GF1') as TfrxMemoView;

  case PCRelatorios.ActivePageIndex of
    0:
    begin
      ReopenEmitX(QrEmit0, TRelConsGru.rcgProdPQGru);
      Grupo1.Condition := 'frxDsEmit0."FormulasGru"';
      Me_GH1.Memo.Text := '[frxDsEmit0."NO_FormulasGru"]';
      Me_GF1.Memo.Text := 'TOTAL [frxDsEmit0."NO_FormulasGru"]: ';
    end;
    1:
    begin
      ReopenEmitX(QrEmit0, TRelConsGru.rcgProdPQLst);
      Grupo1.Condition := 'frxDsEmit0."Numero"';
      Me_GH1.Memo.Text := '[frxDsEmit0."Nome"]';
      Me_GF1.Memo.Text := 'TOTAL [frxDsEmit0."Nome"]: ';
    end
    else Geral.MB_Erro('Tipo de relat�rio n�o definido (2)');
  end;
  //
  MyObjects.frxDefineDataSets(frxQUI_RECEI_027_01, [
  DModG.frxDsDono,
  frxDsEmit0
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_027_01, 'Insumos utilizados');
end;

procedure TFmFormulasGruImp.QrFormulasGruAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulasSel, Dmod.MyDB, [
  'SELECT Numero, Nome ',
  'FROM formulas ',
  'WHERE FormulasGru=' + Geral.FF0(QrFormulasGruCodigo.Value),
  '']);
end;

procedure TFmFormulasGruImp.QrFormulasGruBeforeClose(DataSet: TDataSet);
begin
  QrFormulasSel.Close;
end;

procedure TFmFormulasGruImp.ReopenEmitX(Qry: TmySQLQuery; RelConsGru: TRelConsGru);
var
  SQL_OQue, SQL_OrderBy: String;
begin
  FRelConsGru := RelConsGru; //TRelConsGru.rcgProdPQLst;
  //
  FDataI := Geral.FDT(TPIni.Date, 1);
  FDataF := Geral.FDT(TPFim.Date, 1);
  FCI    := Geral.IMV(EdCI.Text);

  //
  SQL_OQue := '';
  SQL_OrderBy := '';
  case RelConsGru of
    TRelConsGru.rcgProdPQGru:
    begin
      if MyObjects.FIC(DBGFormulasGru.SelectedRows.Count = 0, nil,
      'Selecione pelo menos um grupo de f�rmulas!') then
        Exit;
      //
      FCorda := MyObjects.CordaDeDBGridSelectedRows(DBGFormulasGru, QrFormulasGru, 'Codigo', True);
      SQL_OQue := 'AND fgr.Codigo IN (' + FCorda + ') ';
      SQL_OrderBy := 'ORDER BY fgr.Nome, fgr.Codigo, emi.Nome, emi.Numero, emi.DtaBaixa, emi.HoraIni';
    end;
    TRelConsGru.rcgProdPQLst:
    begin
      if MyObjects.FIC(DBGFormulasLst.SelectedRows.Count = 0, nil,
      'Selecione pelo menos uma f�rmula!') then
        Exit;
      //
      FCorda := MyObjects.CordaDeDBGridSelectedRows(DBGFormulasLst, QrFormulasLst, 'Numero', True);
      SQL_OQue := 'AND emi.Numero IN (' + FCorda + ') ';
      SQL_OrderBy := 'ORDER BY emi.Nome, emi.Numero, emi.DtaBaixa, emi.HoraIni ';
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ROUND(emi.AreaM2 * 10000 / 929.0304 * 4) / 4 AREAP2, ',
  'emi.DtaBaixa, emi.HoraIni, emi.Codigo, lse.Nome NOMESETOR, emi.Numero, frm.Nome, ',
  'emi.Peso, emi.Custo, emi.Qtde, emi.AreaM2, emi.DefPeca, ',
  'frm.FormulasGru, fgr.Nome NO_FormulasGru ',
  'FROM emit emi  ',
  'LEFT JOIN formulas frm ON frm.Numero=emi.Numero  ',
  'LEFT JOIN formulasgru fgr ON fgr.Codigo=frm.FormulasGru  ',
  'LEFT JOIN listasetores lse ON lse.Codigo=frm.Setor ',
  'WHERE emi.DtaBaixa BETWEEN "' + FDataI + '" AND "' + FDataF + '" ',
  SQL_OQue,
  SQL_OrderBy,
  '']);
  //Geral.MB_SQL(Self, Qry);
end;

procedure TFmFormulasGruImp.ReopenFormulasLst();
var
  SQL_Pesq, Texto: String;
begin
  SQL_Pesq := '';
  Texto := MeFormulas.Text;
  if Trim(Texto) <> EmptyStr then
    SQL_Pesq := 'WHERE Nome LIKE "%' + Texto + '%"';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulasLst, Dmod.MyDB, [
  'SELECT Numero, Nome ',
  'FROM formulas ',
  SQL_Pesq,
  'ORDER BY Nome ',
  '']);
end;

function TFmFormulasGruImp.ReopenInsumosGru(): Boolean;
var
  SQL_Cor_PQX, SQL_Cor_PQO, SQL_Espessura_PQX, SQL_Espessura_PQO,
  SQL_GrupoQuimico, SQL_Periodo, SQL_FCI: String;
begin
  Result := False;
  SQL_Cor_PQX       := '';
  SQL_Cor_PQO       := '';
  SQL_Espessura_PQX := '';
  SQL_Espessura_PQO := '';
  SQL_GrupoQuimico  := '';
  SQL_Periodo       := '';
  SQL_FCI           := '';
  //
  if FCI <> 0 then
    if FCI <> 0 then SQL_FCI := 'AND pqx.CliOrig = ' + Geral.FF0(FCI);
  //

  if DBGFormulasGru.SelectedRows.Count > 0 then
  begin
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrInsumos, Dmod.MyDB, [
    'SELECT pqx.Insumo, pq_.Nome, SUM(-pqx.Peso) Peso, SUM(-pqx.Valor) Valor, ',
    'IF(SUM(-pqx.Peso) > 0, SUM(-pqx.Valor) / SUM(-pqx.Peso), 0) CustoUni  ',
    'FROM pqx pqx ',
    'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo ',
    'LEFT JOIN emit emi ON emi.Codigo=pqx.OrigemCodi ',
    '  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0110),
    'LEFT JOIN formulas frm ON frm.Numero=emi.Numero ',
    ' ',
    SQL_Cor_PQX,
    SQL_Espessura_PQX,
    ' ',
    'LEFT JOIN pqo pqo ON pqo.Codigo=pqx.OrigemCodi ',
    '  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
    ' ',
    SQL_Cor_PQO,
    SQL_Espessura_PQO,
    ' ',
    'WHERE DataX BETWEEN "' + FDataI + '" AND "' + FDataF + '" ',
    'AND (IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', frm.FormulasGru, ',
    '  IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0190) + ', pqo.FormulasGru, ',
    '  1=2))) IN (' + FCorda + ') ',
    SQL_GrupoQuimico,
    SQL_Periodo,
    SQL_FCI,
    'GROUP BY pqx.Insumo ',
    'ORDER BY pq_.Nome ',
    '']);
    //Geral.MB_SQL(Self, QrInsumos);
    Result := True;
  end else
    Geral.MB_Aviso('Selecione pelo menos um grupo de f�rmulas!');
end;

function TFmFormulasGruImp.ReopenInsumosLst(): Boolean;
var
  SQL_Cor_PQX, SQL_Cor_PQO, SQL_Espessura_PQX, SQL_Espessura_PQO,
  SQL_GrupoQuimico, SQL_Periodo, SQL_FCI: String;
begin
  Result := False;
  SQL_Cor_PQX       := '';
  SQL_Cor_PQO       := '';
  SQL_Espessura_PQX := '';
  SQL_Espessura_PQO := '';
  SQL_GrupoQuimico  := '';
  SQL_Periodo       := '';
  SQL_FCI           := '';
  //
  if FCI <> 0 then
    if FCI <> 0 then SQL_FCI := 'AND pqx.CliOrig = ' + Geral.FF0(FCI);
  //

  if DBGFormulasLst.SelectedRows.Count > 0 then
  begin
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrInsumos, Dmod.MyDB, [
    'SELECT pqx.Insumo, pq_.Nome, SUM(-pqx.Peso) Peso, SUM(-pqx.Valor) Valor, ',
    'IF(SUM(-pqx.Peso) > 0, SUM(-pqx.Valor) / SUM(-pqx.Peso), 0) CustoUni  ',
    'FROM pqx pqx ',
    'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo ',
    'LEFT JOIN emit emi ON emi.Codigo=pqx.OrigemCodi ',
    '  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0110),
    'LEFT JOIN formulas frm ON frm.Numero=emi.Numero ',
    ' ',
    SQL_Cor_PQX,
    SQL_Espessura_PQX,
    {
    'LEFT JOIN pqo pqo ON pqo.Codigo=pqx.OrigemCodi ',
    '  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
    }
    SQL_Cor_PQO,
    SQL_Espessura_PQO,
    ' ',
    'WHERE DataX BETWEEN "' + FDataI + '" AND "' + FDataF + '" ',
    //'AND (IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', frm.FormulasGru, ',
    'AND (IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', frm.Numero, ',
    //'  IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0190) + ', pqo.FormulasGru, ',
    //'  1=2))) IN (' + FCorda + ') ',
    '  1=2)) IN (' + FCorda + ') ',
    SQL_GrupoQuimico,
    SQL_Periodo,
    SQL_FCI,
    'GROUP BY pqx.Insumo ',
    'ORDER BY pq_.Nome ',
    '']);
    //Geral.MB_SQL(Self, QrInsumos);
    Result := True;
  end else
    Geral.MB_Aviso('Selecione pelo menos uma f�rmula!');
end;

end.
