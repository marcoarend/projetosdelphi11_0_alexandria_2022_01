unit ImportaFoxPro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkDBGridDAC, dmkDBGrid, Variants,
  CheckLst, Menus, frxClass, frxDBSet, UnDmkProcFunc, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmImportaFoxPro = class(TForm)
    TbClientes: TVKDBFNTX;
    DsTabelas: TDataSource;
    TbVendedor: TVKDBFNTX;
    DsClientes: TDataSource;
    DsVendedor: TDataSource;
    TbTabelas: TmySQLTable;
    TbTabelasCodigo: TIntegerField;
    TbTabelasNome: TWideStringField;
    TbTabelasAtivo: TSmallintField;
    TbClientesCODIGO: TWideStringField;
    TbClientesNOME: TWideStringField;
    TbClientesRAZO: TWideStringField;
    TbClientesTIPO: TIntegerField;
    TbClientesCPF: TWideStringField;
    TbClientesCNPJ: TWideStringField;
    TbClientesICMS: TWideStringField;
    TbClientesENDEREO: TWideStringField;
    TbClientesNUMEND: TWideStringField;
    TbClientesCOMPLEND: TWideStringField;
    TbClientesBAIRRO: TWideStringField;
    TbClientesCIDADE: TWideStringField;
    TbClientesUF: TWideStringField;
    TbClientesCEP: TWideStringField;
    TbClientesFONE: TWideStringField;
    TbClientesFAX: TWideStringField;
    TbClientesCELULAR: TWideStringField;
    TbClientesURL: TWideStringField;
    TbClientesEMAIL: TWideStringField;
    TbClientesCLASSE: TWideStringField;
    TbClientesPRAZO: TWideStringField;
    TbClientesTRANSPORTA: TWideStringField;
    TbClientesCONTATO: TWideStringField;
    TbClientesFONECONT: TWideStringField;
    TbClientesCELCONT: TWideStringField;
    TbClientesEMAILCONT: TWideStringField;
    TbClientesVENDEDOR: TWideStringField;
    TbClientesCOMISSAO: TWideStringField;
    TbClientesPRIMEIRA: TDateField;
    TbClientesULTIMA: TDateField;
    TbClientesVALOR: TFloatField;
    TbClientesNF: TWideStringField;
    TbClientesSERASA: TIntegerField;
    TbClientesDATASERASA: TDateField;
    TbClientesSITUAO: TIntegerField;
    TbClientesOBS: TWideMemoField;
    TbVendedorCODIGO: TWideStringField;
    TbVendedorNOME: TWideStringField;
    TbVendedorEMPRESA: TWideStringField;
    TbVendedorCPF: TWideStringField;
    TbVendedorCNPJ: TWideStringField;
    TbVendedorENDEREO: TWideStringField;
    TbVendedorBAIRRO: TWideStringField;
    TbVendedorCIDADE: TWideStringField;
    TbVendedorUF: TWideStringField;
    TbVendedorCEP: TWideStringField;
    TbVendedorFONE: TWideStringField;
    TbVendedorFAX: TWideStringField;
    TbVendedorCELULAR: TWideStringField;
    TbVendedorEMAIL: TWideStringField;
    TbVendedorSITUAO: TIntegerField;
    QrAnt: TmySQLQuery;
    TbTranspor: TVKDBFNTX;
    DsTranspor: TDataSource;
    TbTransporCODIGO: TWideStringField;
    TbTransporNOME: TWideStringField;
    TbTransporRAZO: TWideStringField;
    TbTransporENDEREO: TWideStringField;
    TbTransporBAIRRO: TWideStringField;
    TbTransporCIDADE: TWideStringField;
    TbTransporUF: TWideStringField;
    TbTransporCEP: TWideStringField;
    TbTransporCNPJ: TWideStringField;
    TbTransporICMS: TWideStringField;
    TbTransporFONE: TWideStringField;
    TbTransporFAX: TWideStringField;
    TbTransporCELULAR: TWideStringField;
    TbTransporCONTATO: TWideStringField;
    TbTransporEMAIL: TWideStringField;
    TbForneced: TVKDBFNTX;
    DsForneced: TDataSource;
    TbFornecedCODIGO: TWideStringField;
    TbFornecedNOME: TWideStringField;
    TbFornecedRAZO: TWideStringField;
    TbFornecedTIPO: TIntegerField;
    TbFornecedCPF: TWideStringField;
    TbFornecedCNPJ: TWideStringField;
    TbFornecedICMS: TWideStringField;
    TbFornecedENDEREO: TWideStringField;
    TbFornecedNUMEND: TWideStringField;
    TbFornecedCOMPEND: TWideStringField;
    TbFornecedBAIRRO: TWideStringField;
    TbFornecedCIDADE: TWideStringField;
    TbFornecedUF: TWideStringField;
    TbFornecedCEP: TWideStringField;
    TbFornecedFONE: TWideStringField;
    TbFornecedFAX: TWideStringField;
    TbFornecedCELULAR: TWideStringField;
    TbFornecedURL: TWideStringField;
    TbFornecedEMAIL: TWideStringField;
    TbFornecedCLASSE: TWideStringField;
    TbFornecedPRAZO: TWideStringField;
    TbFornecedTRANSPORTA: TWideStringField;
    TbFornecedCONTATO: TWideStringField;
    TbFornecedFONECONT: TWideStringField;
    TbFornecedCELCONT: TWideStringField;
    TbFornecedEMAILCONT: TWideStringField;
    TbFornecedPRIMEIRA: TDateField;
    TbFornecedULTIMA: TDateField;
    TbFornecedVALOR: TFloatField;
    TbFornecedNF: TWideStringField;
    TbFornecedSITUAO: TIntegerField;
    TbFornecedOBS: TWideMemoField;
    VKDBFNTX1: TVKDBFNTX;
    DataSource1: TDataSource;
    TbNatOper: TVKDBFNTX;
    DsNatOper: TDataSource;
    TbGruProd: TVKDBFNTX;
    DsGruProd: TDataSource;
    TbGruProdCODIGO: TWideStringField;
    TbGruProdDESCRIO: TWideStringField;
    TbProdutos: TVKDBFNTX;
    DsProdutos: TDataSource;
    TbProdutosCODIGO: TWideStringField;
    TbProdutosCODPESQ: TWideStringField;
    TbProdutosDESCRIO: TWideStringField;
    TbProdutosFABRICANTE: TWideStringField;
    TbProdutosGRUPO: TWideStringField;
    TbProdutosTIPO: TWideStringField;
    TbProdutosMEDIDA: TIntegerField;
    TbProdutosVLRUNI: TFloatField;
    TbProdutosQTESTOQUE: TFloatField;
    TbProdutosQTPEAS: TLargeintField;
    TbProdutosIPI: TFloatField;
    TbProdutosICMS: TIntegerField;
    TbProdutosICMSLC: TIntegerField;
    TbProdutosCLASFISC: TIntegerField;
    TbProdutosCODTRIB: TWideStringField;
    TbProdutosESTOQMIN: TLargeintField;
    TbProdutosORIGEM: TIntegerField;
    TbProdutosLOCAL: TWideStringField;
    TbProdutosDATAENT: TDateField;
    TbProdutosCOEFICIENT: TFloatField;
    TbProdutosD04UKEY: TWideStringField;
    TbTipoGrup: TVKDBFNTX;
    DsTipoGrup: TDataSource;
    TbTipoGrupCODIGO: TWideStringField;
    TbTipoGrupDESCRIO: TWideStringField;
    TbFabrica: TVKDBFNTX;
    DsFabrica: TDataSource;
    TbFabricaCODIGO: TWideStringField;
    TbFabricaNOME: TWideStringField;
    TbFabricaMARCA: TWideStringField;
    TbUnidade: TVKDBFNTX;
    TbUnidadeCODIGO: TIntegerField;
    TbUnidadeDESCRIO: TWideStringField;
    DsUnidade: TDataSource;
    TbClasFisc: TVKDBFNTX;
    DsClasFisc: TDataSource;
    TbClasFiscCODIGO: TIntegerField;
    TbClasFiscDESCRIO: TWideStringField;
    QrClasFisc: TmySQLQuery;
    QrClasFiscCodigo: TIntegerField;
    QrClasFiscNome: TWideStringField;
    QrClasFiscNCM: TWideStringField;
    TbFormaPag: TVKDBFNTX;
    DsFormaPag: TDataSource;
    TbCenCusto: TVKDBFNTX;
    DsCenCusto: TDataSource;
    TbItemCust: TVKDBFNTX;
    DsItemCust: TDataSource;
    TbFormaPagCODIGO: TWideStringField;
    TbFormaPagDESCRICAO: TWideStringField;
    TbCenCustoCODIGO: TWideStringField;
    TbCenCustoDESCRIO: TWideStringField;
    TbCenCustoTIPO: TIntegerField;
    TbCenCustoCATEGORIA: TIntegerField;
    Panel1: TPanel;
    Panel6: TPanel;
    Panel3: TPanel;
    Label64: TLabel;
    SbDir: TSpeedButton;
    LaTabela: TLabel;
    EdDir: TdmkEdit;
    PB1: TProgressBar;
    Panel4: TPanel;
    Panel7: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdIniClien: TdmkEdit;
    EdIniVende: TdmkEdit;
    EdIniTrans: TdmkEdit;
    EdIniForne: TdmkEdit;
    DBGrid1: TDBGrid;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    DBGrid3: TDBGrid;
    TabSheet3: TTabSheet;
    DBGrid2: TDBGrid;
    TabSheet4: TTabSheet;
    DBGrid4: TDBGrid;
    TabSheet5: TTabSheet;
    DBGrid5: TDBGrid;
    TabSheet12: TTabSheet;
    TbPagar: TVKDBFNTX;
    DsPagar: TDataSource;
    TbItemCustCODIGO: TWideStringField;
    TbItemCustCENTRO: TWideStringField;
    TbItemCustDESCRIO: TWideStringField;
    TbReceber: TVKDBFNTX;
    DsReceber: TDataSource;
    TbPedido: TVKDBFNTX;
    DsPedido: TDataSource;
    TbPagarDOCUMENTO: TWideStringField;
    TbPagarDATA: TDateField;
    TbPagarCLIENTE: TWideStringField;
    TbPagarNOTAFISCAL: TWideStringField;
    TbPagarVENCIMENTO: TDateField;
    TbPagarVALOR: TFloatField;
    TbPagarPAGAMENTO: TDateField;
    TbPagarVALORPGTO: TFloatField;
    TbPagarMULTA: TFloatField;
    TbPagarJUROS: TFloatField;
    TbPagarMORA: TIntegerField;
    TbPagarFORMAPGTO: TWideStringField;
    TbPagarCENTCUST: TWideStringField;
    TbPagarITEMCUST: TWideStringField;
    TbPagarPEDIDO: TWideStringField;
    TbPagarBANCO: TWideStringField;
    TbPagarOBS: TWideMemoField;
    TbPagarHISTORICO: TWideStringField;
    TbPagarCAIXA: TIntegerField;
    TbItensPed: TVKDBFNTX;
    DsItensPed: TDataSource;
    TbReceberDOCUMENTO: TWideStringField;
    TbReceberDATA: TDateField;
    TbReceberCLIENTE: TWideStringField;
    TbReceberNOTAFISCAL: TWideStringField;
    TbReceberVENCIMENTO: TDateField;
    TbReceberVALOR: TFloatField;
    TbReceberPAGAMENTO: TDateField;
    TbReceberVALORPGTO: TFloatField;
    TbReceberMULTA: TFloatField;
    TbReceberJUROS: TFloatField;
    TbReceberMORA: TIntegerField;
    TbReceberFORMAPGTO: TWideStringField;
    TbReceberCENTCUST: TWideStringField;
    TbReceberITEMCUST: TWideStringField;
    TbReceberPEDIDO: TWideStringField;
    TbReceberBANCO: TWideStringField;
    TbReceberCAIXA: TIntegerField;
    TbReceberHISTORICO: TWideStringField;
    TbReceberOBS: TWideMemoField;
    TbReceberDESCONTO: TIntegerField;
    Memo1: TMemo;
    TbPedidoNUMERO: TWideStringField;
    TbPedidoDATA: TDateField;
    TbPedidoTIPO: TIntegerField;
    TbPedidoENTSAI: TIntegerField;
    TbPedidoCLIENTE: TWideStringField;
    TbPedidoCONDPAG: TWideStringField;
    TbPedidoCENTROCUST: TWideStringField;
    TbPedidoITEMCUST: TWideStringField;
    TbPedidoFORMAPGTO: TWideStringField;
    TbPedidoVALOR: TFloatField;
    TbPedidoNUMNOTA: TWideStringField;
    TbPedidoVLRNOTA: TFloatField;
    TbPedidoVLRICMS: TFloatField;
    TbPedidoBANCO: TWideStringField;
    TbPedidoCP: TIntegerField;
    TbPedidoCOMISSAO: TWideStringField;
    TbPedidoOBS: TWideMemoField;
    TbPedidoOS: TWideStringField;
    TbPedidoAGRUPAR: TIntegerField;
    TbPedidoINSCOM: TIntegerField;
    TbPedidoVENDEDOR: TWideStringField;
    TbPedidoJ07_UKEY: TWideStringField;
    Panel5: TPanel;
    GradeTabelas: TdmkDBGrid;
    CLLista: TCheckListBox;
    TbNotaFisc: TVKDBFNTX;
    DsNotaFisc: TDataSource;
    TbItensPedPEDIDO: TWideStringField;
    TbItensPedORDEM: TIntegerField;
    TbItensPedPRODUTO: TWideStringField;
    TbItensPedCODPESQ: TWideStringField;
    TbItensPedMEDIDA: TWideStringField;
    TbItensPedPEAS: TIntegerField;
    TbItensPedQTDADE: TFloatField;
    TbItensPedVALOR: TFloatField;
    TbItensPedTOTAL: TFloatField;
    TbItensPedDESCRICAO: TWideStringField;
    QrEntidade: TmySQLQuery;
    QrEntidadeCodigo: TIntegerField;
    PMImprime: TPopupMenu;
    Lanamentosrfosdecontasapagar1: TMenuItem;
    QrLctOrf: TmySQLQuery;
    QrLctOrfDOCUMENTO: TWideStringField;
    QrLctOrfDATA: TDateField;
    QrLctOrfCLIENTE: TWideStringField;
    QrLctOrfNOTAFISCAL: TWideStringField;
    QrLctOrfVENCIMENTO: TDateField;
    QrLctOrfVALOR: TFloatField;
    QrLctOrfPAGAMENTO: TDateField;
    QrLctOrfVALORPGTO: TFloatField;
    QrLctOrfMULTA: TFloatField;
    QrLctOrfJUROS: TFloatField;
    QrLctOrfMORA: TIntegerField;
    QrLctOrfFORMAPGTO: TWideStringField;
    QrLctOrfCENTCUST: TWideStringField;
    QrLctOrfITEMCUST: TWideStringField;
    QrLctOrfPEDIDO: TWideStringField;
    QrLctOrfBANCO: TWideStringField;
    QrLctOrfCAIXA: TIntegerField;
    QrLctOrfHISTORICO: TWideStringField;
    QrLctOrfOBS: TWideMemoField;
    QrLctOrfDESCONTO: TIntegerField;
    frxLctOrf: TfrxReport;
    frxDsLctOrf: TfrxDBDataset;
    QrLctOrfNO_GENERO: TWideStringField;
    QrLctOrfNO_SUBGRUPO: TWideStringField;
    LanamentosrfosdecontasaReceber1: TMenuItem;
    QrLctOrfNO_CLI: TWideStringField;
    QrLctOrfNO_CAR: TWideStringField;
    TbNota_Ent: TVKDBFNTX;
    DsNota_Ent: TDataSource;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label9: TLabel;
    LaTamTot: TLabel;
    LaTamRec: TLabel;
    EdTamTot: TdmkEdit;
    EdTamRec: TdmkEdit;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EdTmpTot: TdmkEdit;
    EdTmpRun: TdmkEdit;
    EdTmpFal: TdmkEdit;
    GroupBox4: TGroupBox;
    Label10: TLabel;
    EdPerRec: TdmkEdit;
    ProgressBar1: TProgressBar;
    Panel8: TPanel;
    Label11: TLabel;
    EdEmpresa_11: TdmkEdit;
    TabSheet20: TTabSheet;
    TabSheet21: TTabSheet;
    TabSheet18: TTabSheet;
    PageControl4: TPageControl;
    TabSheet22: TTabSheet;
    DBGrid17: TDBGrid;
    TabSheet23: TTabSheet;
    DBGrid18: TDBGrid;
    PageControl3: TPageControl;
    TabSheet13: TTabSheet;
    DBGrid12: TDBGrid;
    TabSheet14: TTabSheet;
    DBGrid13: TDBGrid;
    TabSheet15: TTabSheet;
    DBGrid14: TDBGrid;
    TabSheet16: TTabSheet;
    DBGrid15: TDBGrid;
    TabSheet17: TTabSheet;
    DBGrid16: TDBGrid;
    PageControl5: TPageControl;
    TabSheet27: TTabSheet;
    DBGrid23: TDBGrid;
    TabSheet28: TTabSheet;
    DBGrid24: TDBGrid;
    TabSheet29: TTabSheet;
    DBGrid25: TDBGrid;
    TabSheet30: TTabSheet;
    DBGrid26: TDBGrid;
    TabSheet31: TTabSheet;
    DBGrid27: TDBGrid;
    TabSheet32: TTabSheet;
    DBGrid28: TDBGrid;
    PageControl6: TPageControl;
    TabSheet6: TTabSheet;
    DBGrid6: TDBGrid;
    TbNotaFiscNUMERO: TWideStringField;
    TbNotaFiscORDSERV: TWideStringField;
    TbNotaFiscSITUACAO: TIntegerField;
    TbNotaFiscNATOPER: TWideStringField;
    TbNotaFiscEMISSAO: TDateField;
    TbNotaFiscSAIDA: TDateField;
    TbNotaFiscHORA: TWideStringField;
    TbNotaFiscBASEICMS: TFloatField;
    TbNotaFiscVLRICMS: TFloatField;
    TbNotaFiscBCICMSSUB: TFloatField;
    TbNotaFiscVLRICMSSUB: TFloatField;
    TbNotaFiscTOTALPROD: TFloatField;
    TbNotaFiscVLRFRETE: TFloatField;
    TbNotaFiscVLRSEGURO: TFloatField;
    TbNotaFiscOUTRAS: TFloatField;
    TbNotaFiscIPI: TFloatField;
    TbNotaFiscTOTAL: TFloatField;
    TbNotaFiscTRANSPORTA: TWideStringField;
    TbNotaFiscFRETE: TIntegerField;
    TbNotaFiscPLACAS: TWideStringField;
    TbNotaFiscQTDADE: TIntegerField;
    TbNotaFiscESPECIE: TWideStringField;
    TbNotaFiscMARCA: TWideStringField;
    TbNotaFiscNUMPROD: TWideStringField;
    TbNotaFiscPESOBRUT: TFloatField;
    TbNotaFiscPESOLIQ: TFloatField;
    TbNotaFiscOBS: TWideMemoField;
    TbNotaFiscCORPO: TWideMemoField;
    TbNotaFiscCR: TIntegerField;
    TbNotaFiscTIPODESC: TIntegerField;
    TbNotaFiscNOTADESC: TFloatField;
    TbNotaFiscJ10UKEY: TWideStringField;
    TbItens_Ent: TVKDBFNTX;
    DsItens_Ent: TDataSource;
    TbNota_EntNUMERO: TWideStringField;
    TbNota_EntCNPJ: TWideStringField;
    TbNota_EntINSCEST: TWideStringField;
    TbNota_EntCFOP: TWideStringField;
    TbNota_EntRAZAO: TWideStringField;
    TbNota_EntENDERECO: TWideStringField;
    TbNota_EntBAIRRO: TWideStringField;
    TbNota_EntCEP: TWideStringField;
    TbNota_EntMUNICIO: TWideStringField;
    TbNota_EntFONE: TWideStringField;
    TbNota_EntUF: TWideStringField;
    TbNota_EntEMISSAO: TDateField;
    TbNota_EntBASEICMS: TFloatField;
    TbNota_EntVALORICMS: TFloatField;
    TbNota_EntBASESUB: TFloatField;
    TbNota_EntVALORSUB: TFloatField;
    TbNota_EntVALORPROD: TFloatField;
    TbNota_EntFRETE: TFloatField;
    TbNota_EntSEGURO: TFloatField;
    TbNota_EntOUTRAS: TFloatField;
    TbNota_EntIPI: TFloatField;
    TbNota_EntTOTAL: TFloatField;
    TbNota_EntQTDADE: TIntegerField;
    TbNota_EntESPECIE: TWideStringField;
    TbNota_EntMARCA: TWideStringField;
    TbNota_EntNUMMARC: TWideStringField;
    TbNota_EntPESOBRUTO: TFloatField;
    TbNota_EntPESOLIQUID: TFloatField;
    TbNota_EntDATACONTAB: TDateField;
    TbNota_EntSITUACAO: TIntegerField;
    TbNota_EntOBSTIT: TWideMemoField;
    TbNota_EntCREDICMS: TIntegerField;
    TbNota_EntCREDIPI: TIntegerField;
    TbNota_EntFRETE_CONT: TIntegerField;
    TabSheet7: TTabSheet;
    DBGrid7: TDBGrid;
    TbEmpresa: TVKDBFNTX;
    DsEmpresa: TDataSource;
    TbItens_EntNUMERO: TWideStringField;
    TbItens_EntITEM: TIntegerField;
    TbItens_EntCODPROD: TWideStringField;
    TbItens_EntDESCPROD: TWideStringField;
    TbItens_EntSITTRIB: TWideStringField;
    TbItens_EntUNIDADE: TWideStringField;
    TbItens_EntQTDADE: TFloatField;
    TbItens_EntVALORUNIT: TFloatField;
    TbItens_EntVALORTOT: TFloatField;
    TbItens_EntICMS: TFloatField;
    TbItens_EntIPI: TFloatField;
    TbItens_EntVALORIPI: TFloatField;
    TbItens_EntCNPJ: TWideStringField;
    TabSheet8: TTabSheet;
    DBGrid8: TDBGrid;
    QrMin: TmySQLQuery;
    QrMinData: TDateField;
    QrPedido: TmySQLQuery;
    QrPedidoNUMERO: TWideStringField;
    QrPedidoDATA: TDateField;
    QrPedidoTIPO: TIntegerField;
    QrPedidoENTSAI: TIntegerField;
    QrPedidoCLIENTE: TWideStringField;
    QrPedidoCONDPAG: TWideStringField;
    QrPedidoCENTROCUST: TWideStringField;
    QrPedidoITEMCUST: TWideStringField;
    QrPedidoFORMAPGTO: TWideStringField;
    QrPedidoVALOR: TFloatField;
    QrPedidoNUMNOTA: TWideStringField;
    QrPedidoVLRNOTA: TFloatField;
    QrPedidoVLRICMS: TFloatField;
    QrPedidoBANCO: TWideStringField;
    QrPedidoCP: TIntegerField;
    QrPedidoCOMISSAO: TWideStringField;
    QrPedidoOBS: TWideMemoField;
    QrPedidoOS: TWideStringField;
    QrPedidoAGRUPAR: TIntegerField;
    QrPedidoINSCOM: TIntegerField;
    QrPedidoVENDEDOR: TWideStringField;
    QrPedidoJ07_UKEY: TWideStringField;
    QrNotaFisc: TmySQLQuery;
    QrNotaFiscNUMERO: TWideStringField;
    QrNotaFiscORDSERV: TWideStringField;
    QrNotaFiscSITUACAO: TIntegerField;
    QrNotaFiscNATOPER: TWideStringField;
    QrNotaFiscEMISSAO: TDateField;
    QrNotaFiscSAIDA: TDateField;
    QrNotaFiscHORA: TWideStringField;
    QrNotaFiscBASEICMS: TFloatField;
    QrNotaFiscVLRICMS: TFloatField;
    QrNotaFiscBCICMSSUB: TFloatField;
    QrNotaFiscVLRICMSSUB: TFloatField;
    QrNotaFiscTOTALPROD: TFloatField;
    QrNotaFiscVLRFRETE: TFloatField;
    QrNotaFiscVLRSEGURO: TFloatField;
    QrNotaFiscOUTRAS: TFloatField;
    QrNotaFiscIPI: TFloatField;
    QrNotaFiscTOTAL: TFloatField;
    QrNotaFiscTRANSPORTA: TWideStringField;
    QrNotaFiscFRETE: TIntegerField;
    QrNotaFiscPLACAS: TWideStringField;
    QrNotaFiscQTDADE: TIntegerField;
    QrNotaFiscESPECIE: TWideStringField;
    QrNotaFiscMARCA: TWideStringField;
    QrNotaFiscNUMPROD: TWideStringField;
    QrNotaFiscPESOBRUT: TFloatField;
    QrNotaFiscPESOLIQ: TFloatField;
    QrNotaFiscOBS: TWideMemoField;
    QrNotaFiscCORPO: TWideMemoField;
    QrNotaFiscTIPODESC: TIntegerField;
    QrNotaFiscNOTADESC: TFloatField;
    QrNotaFiscJ10UKEY: TWideStringField;
    TbR10: TVKDBFNTX;
    DsR10: TDataSource;
    TbEmpresaEMPRESA: TWideStringField;
    TbEmpresaENDEREO: TWideStringField;
    TbEmpresaBAIRRO: TWideStringField;
    TbEmpresaCIDADE: TWideStringField;
    TbEmpresaUF: TWideStringField;
    TbEmpresaCEP: TWideStringField;
    TbEmpresaTELEFONE: TWideStringField;
    TbEmpresaFAX: TWideStringField;
    TbEmpresaLOGO: TBlobField;
    TbEmpresaBACKUP: TWideStringField;
    TbEmpresaCNPJ: TWideStringField;
    TbEmpresaINSCEST: TWideStringField;
    TbEmpresaTITULO: TWideStringField;
    TbEmpresaCELULAR: TWideStringField;
    TbEmpresaE_MAIL: TWideStringField;
    TbEmpresaRAMO: TWideStringField;
    TabSheet9: TTabSheet;
    DBGrid9: TDBGrid;
    Panel9: TPanel;
    EdLgr_Txt: TdmkEdit;
    EdLgr_Cod: TdmkEdit;
    EdRua: TdmkEdit;
    EdNumero: TdmkEdit;
    EdCompl: TdmkEdit;
    EdBairro: TdmkEdit;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EdCidade_Cod: TdmkEdit;
    QrEnti: TmySQLQuery;
    QrEntiTipo: TSmallintField;
    QrEntiNO_ENTI: TWideStringField;
    QrEntiCNPJ_CPF: TWideStringField;
    QrEntiIE_RG: TWideStringField;
    QrEntiNO_Lgr: TWideStringField;
    QrEntiRua: TWideStringField;
    QrEntiNumero: TFloatField;
    QrEntiCompl: TWideStringField;
    QrEntiBairro: TWideStringField;
    QrEntixCidade: TWideStringField;
    QrEnticCidade: TLargeintField;
    QrEntiNO_UF: TWideStringField;
    QrEntiCEP: TLargeintField;
    QrEnticPais: TLargeintField;
    QrEntixPais: TWideStringField;
    QrEntiTe1: TWideStringField;
    QrEntiISUF: TWideStringField;
    QrItensPed: TmySQLQuery;
    QrItensPedPEDIDO: TWideStringField;
    QrItensPedORDEM: TIntegerField;
    QrItensPedPRODUTO: TWideStringField;
    QrItensPedCODPESQ: TWideStringField;
    QrItensPedMEDIDA: TWideStringField;
    QrItensPedPECAS: TIntegerField;
    QrItensPedQTDADE: TFloatField;
    QrItensPedVALOR: TFloatField;
    QrItensPedTOTAL: TFloatField;
    QrItensPedDESCRICAO: TWideStringField;
    QrNota_Ent: TmySQLQuery;
    QrNota_EntNUMERO: TWideStringField;
    QrNota_EntCNPJ: TWideStringField;
    QrNota_EntINSCEST: TWideStringField;
    QrNota_EntCFOP: TWideStringField;
    QrNota_EntRAZAO: TWideStringField;
    QrNota_EntENDERECO: TWideStringField;
    QrNota_EntBAIRRO: TWideStringField;
    QrNota_EntCEP: TWideStringField;
    QrNota_EntMUNICIO: TWideStringField;
    QrNota_EntFONE: TWideStringField;
    QrNota_EntUF: TWideStringField;
    QrNota_EntEMISSAO: TDateField;
    QrNota_EntBASEICMS: TFloatField;
    QrNota_EntVALORICMS: TFloatField;
    QrNota_EntBASESUB: TFloatField;
    QrNota_EntVALORSUB: TFloatField;
    QrNota_EntVALORPROD: TFloatField;
    QrNota_EntFRETE: TFloatField;
    QrNota_EntSEGURO: TFloatField;
    QrNota_EntOUTRAS: TFloatField;
    QrNota_EntIPI: TFloatField;
    QrNota_EntTOTAL: TFloatField;
    QrNota_EntQTDADE: TIntegerField;
    QrNota_EntESPECIE: TWideStringField;
    QrNota_EntMARCA: TWideStringField;
    QrNota_EntNUMMARC: TWideStringField;
    QrNota_EntPESOBRUTO: TFloatField;
    QrNota_EntPESOLIQUID: TFloatField;
    QrNota_EntDATACONTAB: TDateField;
    QrNota_EntSITUACAO: TIntegerField;
    QrNota_EntOBSTIT: TWideMemoField;
    QrNota_EntCREDICMS: TIntegerField;
    QrNota_EntCREDIPI: TIntegerField;
    QrNota_EntFRETE_CONT: TIntegerField;
    QrEntiN2_ENTI: TWideStringField;
    QrEntiDoc: TmySQLQuery;
    QrEntiDocCodigo: TIntegerField;
    Label18: TLabel;
    EdInclEnts: TdmkEdit;
    QrItens_Ent: TmySQLQuery;
    QrItens_EntNUMERO: TWideStringField;
    QrItens_EntITEM: TIntegerField;
    QrItens_EntCODPROD: TWideStringField;
    QrItens_EntDESCPROD: TWideStringField;
    QrItens_EntSITTRIB: TWideStringField;
    QrItens_EntUNIDADE: TWideStringField;
    QrItens_EntQTDADE: TFloatField;
    QrItens_EntVALORUNIT: TFloatField;
    QrItens_EntVALORTOT: TFloatField;
    QrItens_EntICMS: TFloatField;
    QrItens_EntIPI: TFloatField;
    QrItens_EntVALORIPI: TFloatField;
    QrItens_EntCNPJ: TWideStringField;
    QrIts: TmySQLQuery;
    QrItsNUMERO: TWideStringField;
    QrItsITEM: TIntegerField;
    QrItsCODPROD: TWideStringField;
    QrItsDESCPROD: TWideStringField;
    QrItsSITTRIB: TWideStringField;
    QrItsUNIDADE: TWideStringField;
    QrItsQTDADE: TFloatField;
    QrItsVALORUNIT: TFloatField;
    QrItsVALORTOT: TFloatField;
    QrItsICMS: TFloatField;
    QrItsIPI: TFloatField;
    QrItsVALORIPI: TFloatField;
    QrItsCNPJ: TWideStringField;
    QrItsCodigo: TIntegerField;
    QrSel: TmySQLQuery;
    CkAtualiza: TCheckBox;
    TabSheet10: TTabSheet;
    PageControl7: TPageControl;
    TabSheet11: TTabSheet;
    TabSheet19: TTabSheet;
    DBGrid10: TDBGrid;
    DBGrid11: TDBGrid;
    TbR11: TVKDBFNTX;
    DsR11: TDataSource;
    TbR50: TVKDBFNTX;
    DsR50: TDataSource;
    TbR51: TVKDBFNTX;
    DsR51: TDataSource;
    TabSheet24: TTabSheet;
    TabSheet25: TTabSheet;
    TabSheet26: TTabSheet;
    TabSheet33: TTabSheet;
    TabSheet34: TTabSheet;
    TabSheet35: TTabSheet;
    DBGrid19: TDBGrid;
    TbR53: TVKDBFNTX;
    DsR53: TDataSource;
    TbR54: TVKDBFNTX;
    DsR54: TDataSource;
    TbR85: TVKDBFNTX;
    DsR85: TDataSource;
    DBGrid20: TDBGrid;
    DBGrid21: TDBGrid;
    DBGrid22: TDBGrid;
    TabSheet36: TTabSheet;
    TabSheet37: TTabSheet;
    TbR70: TVKDBFNTX;
    DsR70: TDataSource;
    TbR71: TVKDBFNTX;
    DsR71: TDataSource;
    TbR74: TVKDBFNTX;
    DsR74: TDataSource;
    TbR75: TVKDBFNTX;
    DsR75: TDataSource;
    DBGrid29: TDBGrid;
    DBGrid30: TDBGrid;
    DBGrid31: TDBGrid;
    DBGrid32: TDBGrid;
    TabSheet38: TTabSheet;
    TabSheet39: TTabSheet;
    TabSheet40: TTabSheet;
    TabSheet41: TTabSheet;
    TbR86: TVKDBFNTX;
    DsR86: TDataSource;
    TbR88: TVKDBFNTX;
    DsR88: TDataSource;
    TbR90: TVKDBFNTX;
    DsR90: TDataSource;
    DBGrid33: TDBGrid;
    DBGrid34: TDBGrid;
    DBGrid35: TDBGrid;
    DBGrid36: TDBGrid;
    TbR10TIPO: TWideStringField;
    TbR10CNPJ: TWideStringField;
    TbR10INSCEST: TWideStringField;
    TbR10NCONTRIB: TWideStringField;
    TbR10MUNICIPIO: TWideStringField;
    TbR10UF: TWideStringField;
    TbR10FAX: TWideStringField;
    TbR10DATAINI: TDateField;
    TbR10DATAFIN: TDateField;
    TbR10CODCONV: TWideStringField;
    TbR10CODNAT: TWideStringField;
    TbR10CODFIN: TWideStringField;
    TbR11TIPO: TWideStringField;
    TbR11LOGRADOURO: TWideStringField;
    TbR11NUMERO: TWideStringField;
    TbR11COMPLEMENT: TWideStringField;
    TbR11BAIRRO: TWideStringField;
    TbR11CEP: TWideStringField;
    TbR11CONTATO: TWideStringField;
    TbR11TELEFONE: TWideStringField;
    TbR50TIPO: TWideStringField;
    TbR50CNPJ: TWideStringField;
    TbR50INSCEST: TWideStringField;
    TbR50EMISSAO: TDateField;
    TbR50UF: TWideStringField;
    TbR50MODELO: TWideStringField;
    TbR50SERIE: TWideStringField;
    TbR50NUMNF: TWideStringField;
    TbR50CODNATOP: TWideStringField;
    TbR50EMITENTE: TWideStringField;
    TbR50VALORTOT: TWideStringField;
    TbR50BASEICMS: TWideStringField;
    TbR50VALORICMS: TWideStringField;
    TbR50VLRISENTO: TWideStringField;
    TbR50OUTROS: TWideStringField;
    TbR50ALIQUOTA: TWideStringField;
    TbR50SITUACAO: TWideStringField;
    TbR51TIPO: TWideStringField;
    TbR51CNPJ: TWideStringField;
    TbR51INSCEST: TWideStringField;
    TbR51EMISSAO: TDateField;
    TbR51UF: TWideStringField;
    TbR51SERIE: TWideStringField;
    TbR51NUMNF: TWideStringField;
    TbR51CODNATOP: TWideStringField;
    TbR51TOTALNF: TWideStringField;
    TbR51VLRTOTIPI: TWideStringField;
    TbR51VLRISENTO: TWideStringField;
    TbR51OUTROS: TWideStringField;
    TbR51BRANCOS: TWideStringField;
    TbR51SITUACAO: TWideStringField;
    TbR53TIPO: TWideStringField;
    TbR53CNPJ: TWideStringField;
    TbR53INSCEST: TWideStringField;
    TbR53EMISSAO: TDateField;
    TbR53UF: TWideStringField;
    TbR53MODELO: TWideStringField;
    TbR53SERIE: TWideStringField;
    TbR53NUMERO: TWideStringField;
    TbR53CFOP: TWideStringField;
    TbR53EMITENTE: TWideStringField;
    TbR53BASECALC: TWideStringField;
    TbR53ICMSRETIDO: TWideStringField;
    TbR53DESPACESS: TWideStringField;
    TbR53SITUACAO: TWideStringField;
    TbR53CODANTEC: TWideStringField;
    TbR53BRANCOS: TWideStringField;
    TbR54TIPO: TWideStringField;
    TbR54CNPJ: TWideStringField;
    TbR54MODELO: TWideStringField;
    TbR54SERIE: TWideStringField;
    TbR54NUMNF: TWideStringField;
    TbR54CODNATOP: TWideStringField;
    TbR54SITTRIB: TWideStringField;
    TbR54NUMITEM: TWideStringField;
    TbR54CODPROD: TWideStringField;
    TbR54QTDADE: TWideStringField;
    TbR54VLRPROD: TWideStringField;
    TbR54VLRDESC: TWideStringField;
    TbR54BASECALC: TWideStringField;
    TbR54BASESUBTRI: TWideStringField;
    TbR54VLRIPI: TWideStringField;
    TbR54ALIQICMS: TWideStringField;
    TbR70TIPO: TWideStringField;
    TbR70CNPJ: TWideStringField;
    TbR70INSCEST: TWideStringField;
    TbR70EMISSAO: TDateField;
    TbR70UF: TWideStringField;
    TbR70MODELO: TWideStringField;
    TbR70SERIE: TWideStringField;
    TbR70SUBSERIE: TWideStringField;
    TbR70NUMNF: TWideStringField;
    TbR70CODNATOP: TWideStringField;
    TbR70VALORTOT: TWideStringField;
    TbR70BASEICMS: TWideStringField;
    TbR70VALORICMS: TWideStringField;
    TbR70VLRISENTO: TWideStringField;
    TbR70OUTROS: TWideStringField;
    TbR70CIF_FOB: TWideStringField;
    TbR70SITUACAO: TWideStringField;
    TbR71TIPO: TWideStringField;
    TbR71CNPJ: TWideStringField;
    TbR71INSCEST: TWideStringField;
    TbR71EMISSAOC: TDateField;
    TbR71UF: TWideStringField;
    TbR71MODELOC: TWideStringField;
    TbR71SERIEC: TWideStringField;
    TbR71SUBSERIE: TWideStringField;
    TbR71NUMEROC: TWideStringField;
    TbR71UFREM: TWideStringField;
    TbR71CNPJREM: TWideStringField;
    TbR71INSCESTREM: TWideStringField;
    TbR71EMISSAON: TDateField;
    TbR71MODELON: TWideStringField;
    TbR71SERIEN: TWideStringField;
    TbR71NUMNF: TWideStringField;
    TbR71TOTALNF: TWideStringField;
    TbR71BRANCOS: TWideStringField;
    TbR74TIPO: TWideStringField;
    TbR74DATA: TDateField;
    TbR74CODIPROD: TWideStringField;
    TbR74QTDADE: TWideStringField;
    TbR74VLRPROD: TWideStringField;
    TbR74CODPOSSEM: TWideStringField;
    TbR74CNPJ: TWideStringField;
    TbR74INSCEST: TWideStringField;
    TbR74UF: TWideStringField;
    TbR74BRANCOS: TWideStringField;
    TbR85TIPO: TWideStringField;
    TbR85DECLARACAO: TWideStringField;
    TbR85DATADECLAR: TDateField;
    TbR85NATUREZA: TWideStringField;
    TbR85REGISTRO: TWideStringField;
    TbR85DATAREG: TDateField;
    TbR85CONHECIM: TWideStringField;
    TbR85DATACONHEC: TDateField;
    TbR85TIPOCONHEC: TWideStringField;
    TbR85PAIS: TWideStringField;
    TbR85RESERVADO: TWideStringField;
    TbR85DATAAVERB: TDateField;
    TbR85NUMNFEXP: TWideStringField;
    TbR85EMISSAO: TDateField;
    TbR85MODELO: TWideStringField;
    TbR85SERIE: TWideStringField;
    TbR85BRANCOS: TWideStringField;
    TbR86TIPO: TWideStringField;
    TbR86REGEXPORT: TWideStringField;
    TbR86DATAREG: TDateField;
    TbR86CNPJREM: TWideStringField;
    TbR86INSCEST: TWideStringField;
    TbR86UF: TWideStringField;
    TbR86NUMNF: TWideStringField;
    TbR86EMISSAO: TDateField;
    TbR86MODELO: TWideStringField;
    TbR86SERIE: TWideStringField;
    TbR86CODPROD: TWideStringField;
    TbR86QTDADE: TWideStringField;
    TbR86VLRUNIT: TWideStringField;
    TbR86VLRPROD: TWideStringField;
    TbR86RELACIONA: TWideStringField;
    TbR86BRANCOS: TWideStringField;
    TbR88TIPO: TWideStringField;
    TbR88SUBTIPO: TWideStringField;
    TbR88MODELO: TWideStringField;
    TbR88SERIE: TWideStringField;
    TbR88NUMERO: TWideStringField;
    TbR88CFOP: TWideStringField;
    TbR88CST: TWideStringField;
    TbR88NUMITEM: TWideStringField;
    TbR88CODPROD: TWideStringField;
    TbR88NUMSER: TWideStringField;
    TbR88BRANCOS: TWideStringField;
    TbR90TIPO: TWideStringField;
    TbR90CNPJ: TWideStringField;
    TbR90INSCEST: TWideStringField;
    TbR90TIPOREG: TWideStringField;
    TbR90TOTREG: TWideStringField;
    TbR90TIPOREG1: TWideStringField;
    TbR90TOTREG1: TWideStringField;
    TbR90TIPOREG2: TWideStringField;
    TbR90TOTREG2: TWideStringField;
    TbR90TIPOREG3: TWideStringField;
    TbR90TOTREG3: TWideStringField;
    TbR90TIPOREG4: TWideStringField;
    TbR90TOTREG4: TWideStringField;
    TbR90TIPOREG5: TWideStringField;
    TbR90TOTREG5: TWideStringField;
    TbR90TIPOREG6: TWideStringField;
    TbR90TOTREG6: TWideStringField;
    TbR90TIPOREG7: TWideStringField;
    TbR90TOTREG7: TWideStringField;
    TbR90TIPOREG8: TWideStringField;
    TbR90TOTREG8: TWideStringField;
    TbR90BRANCO: TWideStringField;
    TbR90NUMTIP90: TWideStringField;
    TbR75TIPO: TWideStringField;
    TbR75DATAINI: TDateField;
    TbR75DATAFIN: TDateField;
    TbR75CODPROD: TWideStringField;
    TbR75CODNCM: TWideStringField;
    TbR75DESCRICAO: TWideStringField;
    TbR75UNIDADE: TWideStringField;
    TbR75ALIQIPI: TWideStringField;
    TbR75ALIQICMS: TWideStringField;
    TbR75REDBASEICM: TWideStringField;
    TbR75BASESUBTRI: TWideStringField;
    TabSheet42: TTabSheet;
    DBGrid37: TDBGrid;
    TbNatOperCODIGO: TWideStringField;
    TbNatOperDESCRIO: TWideStringField;
    TbNatOperICMS: TIntegerField;
    TbNatOperIPI: TIntegerField;
    TbNatOperESPECIAL: TIntegerField;
    TbNatOperGERACR: TIntegerField;
    TbNatOperCODENT: TWideStringField;
    Memo2: TMemo;
    QrNotaFiscCR: TWideStringField;
    QrGGX: TmySQLQuery;
    QrMinX: TmySQLQuery;
    QrMinXNivel1: TIntegerField;
    QrGGXNivel1: TIntegerField;
    TabSheet43: TTabSheet;
    Label22: TLabel;
    EdEmpresa_12: TdmkEdit;
    EdBak: TdmkEdit;
    SpeedButton1: TSpeedButton;
    Panel10: TPanel;
    BtLista: TBitBtn;
    PMLista: TPopupMenu;
    Carrega1: TMenuItem;
    EdLista: TdmkEdit;
    Label23: TLabel;
    SbLista: TSpeedButton;
    PageControl8: TPageControl;
    TabSheet44: TTabSheet;
    GradeLista: TStringGrid;
    TabSheet45: TTabSheet;
    DBGrid38: TDBGrid;
    QrGG1: TmySQLQuery;
    EdTabela: TdmkEdit;
    Label24: TLabel;
    QrGG1Nivel1: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel11: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel12: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BtParar: TBitBtn;
    Button1: TButton;
    Label19: TLabel;
    EdNao: TdmkEdit;
    Label20: TLabel;
    EdSim: TdmkEdit;
    Label21: TLabel;
    EdVar: TdmkEdit;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbDirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure GradeTabelasCellClick(Column: TColumn);
    procedure CLListaClick(Sender: TObject);
    procedure BtPararClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure Lanamentosrfosdecontasapagar1Click(Sender: TObject);
    procedure frxLctOrfGetValue(const VarName: string; var Value: Variant);
    procedure LanamentosrfosdecontasaReceber1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Carrega1Click(Sender: TObject);
    procedure SbListaClick(Sender: TObject);
    procedure BtListaClick(Sender: TObject);
  private
    { Private declarations }
    FListaIgapo: String;
    FAtuData, FMinPedido, FMinNota_Ent: TDateTime;
    FAtuData_TXT: String;
    FImportaEmitida, FImportaEntrada, FParar: Boolean;
    FMaxRegistros, FRegistrosIns, FControle, FIDCtrl_SMI: Integer;
    FTimeIni: TDateTime;
    //
    FIniClien,
    FIniVende,
    FIniTrans,
    FIniForne,
    //
    FIniGProd,
    FIniTGrup,
    FIniFabri,
    FIniUnida,
    FIniClFis,
    FIniProds,
    //
    FIniFoPag,
    FIniCeCus,
    FIniItCus,
    FIniPagar,
    FIniReceb,
    //
    FIniClass: Integer;
    //
    FCodAnt: String;
    //
    FTituloLctOrf: String;
    //
    function DefineCodigosIniciais(): Boolean;
    //
    procedure ImportaClientes();
    procedure ImportaVendedor();
    procedure ImportaTranspor();
    procedure ImportaForneced();
    //
    procedure ImportaTipoGrup();
    procedure ImportaGruProd();
    procedure ImportaFabrica();
    procedure ImportaUnidade();
    procedure ImportaClasFisc();
    procedure ImportaProdutos();
    //
    procedure ImportaFormaPag();
    procedure ImportaCenCusto();
    procedure ImportaItemCust();
    procedure ImportaPagar(CliInt: Integer; Dir: String);
    procedure ImportaReceber(CliInt: Integer; Dir: String);
    //
    procedure ImportaPedido(CliInt: Integer; Dir: String);
    procedure ImportaItensPed(CliInt: Integer; Dir: String);
    //
    procedure ImportaNotaFisc();
    //
    procedure ImportaNota_Ent();
    procedure ImportaItens_Ent();
    //
    procedure ImportaMinhaNFe();
    procedure ImportaMinhaNFe_Emitida(Data_TXT: String);
    procedure ImportaMinhaNFe_Recebida(Data_TXT: String);
    //
    procedure ImportaR10();
    procedure ImportaR11();
    procedure ImportaR50();
    procedure ImportaR51();
    procedure ImportaR53();
    procedure ImportaR54();
    procedure ImportaR70();
    procedure ImportaR71();
    procedure ImportaR74();
    procedure ImportaR75();
    procedure ImportaR85();
    procedure ImportaR86();
    procedure ImportaR88();
    procedure ImportaR90();
    //
    procedure ImportaNatOper();
    //
    procedure AtualizaTempo(Incrementa: Boolean = True);
    //procedure AtualizaTodos(Status: Integer);
    procedure InsereEntiTrnasp(Codigo, Transp: Integer);
    procedure LimparDadosTabela(SQL: WideString);
    //
    function LocalizaAntigoStr(Tabela, Campo, Prefixo, Antigo: String): Boolean;
    function LocalizaAntigoInt(Tabela, Campo: String;
             Antigo: Integer): Boolean;
    function BuscaEntidadeAntiga(Sufixo, CodStr: String): Integer;
    function BuscaEntidadePeloCNPJ(CNPJ: String): Integer;
    function IncluiEntidadePeloCNPJ(CNPJ, RazaoSocial, Fantasia,
             IE, Endereco, Bairro, CEP, Cidade, Fone, UF: String): Integer;
    //
    procedure ImprimeLctOrf(Pagar: Boolean);
    procedure AtualizaEmpresa();
    function ObtemProximoGGXNeg(Referencia, Nome: String): Integer;

  public
    { Public declarations }
  end;

  var
  FmImportaFoxPro: TFmImportaFoxPro;

implementation

uses UnMyObjects, UMySQLModule, Module, ModuleGeral, UCreate, UnFinanceiro,
UnInternalConsts, ModuleNFe_0000, MyDBCheck, Grade_Create;

{$R *.DFM}

procedure TFmImportaFoxPro.AtualizaEmpresa();
const
  Cliente1 = 'V';
  Cliente2 = 'V';
  Cliente3 = 'V';
  Cliente4 = 'V';
  Fornece1 = 'V';
  Fornece2 = 'V';
  Fornece3 = 'V';
  Fornece4 = 'V';
  Fornece5 = 'V';
  Fornece6 = 'V';
  Fornece7 = 'V';
  Fornece8 = 'V';
var
  ECodMunici, ECodiPais, //CRT,
  Codigo, CodUsu, ELograd, ENumero, Tipo, Filial, EUF: Integer;
  ERua, ECompl, EBairro, ECidade, ECEP, EPais, ETe1, EFax, ECel, EEmail,
  RazaoSocial, Fantasia, CNPJ, IE, URL: String;
  SQLType: TSQLType;
begin
  Tipo        := 0;
  Codigo      := EdEmpresa_11.ValueVariant;
  CodUsu      := Codigo;
  RazaoSocial := TbEmpresaEMPRESA.Value;
  Fantasia    := TbEmpresaEMPRESA.Value;
  CNPJ        := Geral.SoNumero_TT(TbEmpresaCNPJ.Value);
  IE          := Geral.SoNumero_TT(TbEmpresaINSCEST.Value);
  ELograd     := EdLgr_Cod.ValueVariant;
  ENumero     := Geral.IMV(EdNumero.Text);
  ERua        := EdRua.Text;
  ECompl      := EdCompl.Text;
  EBairro     := TbEmpresaBAIRRO.Value;
  ECidade     := TbEmpresaCIDADE.Value;
  EPais       := 'BRASIL';
  EUF         := Geral.GetCodigoUF_da_SiglaUF(TbEmpresaUF.Value);
  ECEP        := Geral.SoNumero_TT(TbEmpresaCEP.Value);
  ETe1        := Geral.SoNumero_TT(TbEmpresaTELEFONE.Value);
  EFax        := Geral.SoNumero_TT(TbEmpresaFAX.Value);
  ECel        := Geral.SoNumero_TT(TbEmpresaCELULAR.Value);
  EEmail      := TbEmpresaE_MAIL.Value;
  Filial      := 1;
  ECodMunici  := 4113700;
  ECodiPais   := 1058;
  URL         := TbEmpresaRAMO.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'entidades', False, [
  'CodUsu', 'RazaoSocial', 'Fantasia',
  'CNPJ', 'IE', 'ELograd',
  'ERua', 'ENumero', 'ECompl',
  'EBairro', 'ECidade', 'EUF',
  'ECEP', 'EPais', 'ETe1',
  'ECel', 'EFax', 'EEmail',
  'Cliente1',
  'Cliente2', 'Cliente3', 'Cliente4',
  'Fornece1', 'Fornece2', 'Fornece3',
  'Fornece4', 'Fornece5', 'Fornece6',
  'Fornece7', 'Fornece8', 'Tipo',
  'Filial', 'ECodMunici', 'ECodiPais',
  'URL'(*, 'CRT'*)], [
  'Codigo'], [
  CodUsu, RazaoSocial, Fantasia,
  CNPJ, IE, ELograd,
  ERua, ENumero, ECompl,
  EBairro, ECidade, EUF,
  ECEP, EPais, ETe1,
  ECel, EFax, EEmail,
  Cliente1,
  Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3,
  Fornece4, Fornece5, Fornece6,
  Fornece7, Fornece8, Tipo,
  Filial, ECodMunici, ECodiPais,
  URL(*, CRT*)], [
  Codigo], True) then
  begin
    if UMyMod.RegistrosNaTabela('master', '', Dmod.MyDB) = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'master', False, [
    'Em', 'CNPJ'], [], [RazaoSocial, CNPJ], [], False) then ;
  end;
end;

procedure TFmImportaFoxPro.AtualizaTempo(Incrementa: Boolean);
var
  PerNum, Tam: Double;
  Niv: Integer;
  Nom: String;
  TimeAtu, TimeTot: TDateTime;
begin
  if Incrementa then
    FRegistrosIns := FRegistrosIns + 1;
  //
  PerNum     := 0;
  TimeAtu    := Now() - FTimeIni;
  if FMaxRegistros > 0 then
    PerNum := FRegistrosIns / FMaxRegistros;
  if PerNum > 0 then
    TimeTot := TimeAtu / PerNum
  else
    TimeTot := 0;
  PerNum := PerNum * 100;
  ProgressBar1.Position := FRegistrosIns;
  Geral.AdequaMedida(grandezaNone, FRegistrosIns, 0, Tam, Niv, Nom);
  EdTamRec.DecimalSize  := Niv;
  EdTamRec.ValueVariant := Tam;
  LaTamRec.Caption      := Nom;
  EdPerRec.ValueVariant := PerNum;
  EdTmpRun.ValueVariant := TimeAtu;
  EdTmpTot.ValueVariant := TimeTot;
  EdTmpFal.ValueVariant := TimeTot - TimeAtu;
  Update;
  Application.ProcessMessages;
  //
end;

{
procedure TFmImportaFoxPro.AtualizaTodos(Status: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FCodAnt + ' SET Ativo=:P0');
  DModG.QrUpdPID1.Params[00].AsInteger := Status;
  DModG.QrUpdPID1.ExecSQL;
  //
  TbTabelas.Refresh;
end;
}

procedure TFmImportaFoxPro.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmImportaFoxPro.BtListaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLista, BtLista);
end;

procedure TFmImportaFoxPro.BtNenhumClick(Sender: TObject);
begin
//  AtualizaTodos(0);
end;

procedure TFmImportaFoxPro.BtOKClick(Sender: TObject);
var
  Nom, NomeTab: String;
  I: Integer;
  Tam: Double;
  Niv: Integer;
begin
  DBCheck.VerificaEntiCliInt();
  Memo2.Lines.Clear;
  FImportaEmitida := False;
  FImportaEntrada := False;
  //
  if CkAtualiza.Checked then
    AtualizaEmpresa();
  //
  for I := 0 to CLLista.Items.Count - 1 do
  begin
    if CLLista.Checked[I] then
    begin
      case I of
        0:
        begin
          LimparDadosTabela('DELETE FROM entidades  WHERE Codigo > 0;   ');
          LimparDadosTabela('DELETE FROM enticlient WHERE Codigo > 0;   ');
          LimparDadosTabela('DELETE FROM entitransp WHERE Codigo > 0;   ');
          LimparDadosTabela('DELETE FROM entifornec WHERE Codigo > 0;   ');
        end;
        1:
        begin
          LimparDadosTabela('DELETE FROM prdgruptip WHERE Codigo > 0;   ');
          LimparDadosTabela('DELETE FROM gragru2    WHERE Nivel2 > 0;   ');
          LimparDadosTabela('DELETE FROM gragru1    WHERE Nivel1 > 0;   ');
          LimparDadosTabela('DELETE FROM gragruc    WHERE Controle > 0; ');
          LimparDadosTabela('DELETE FROM gragrux    WHERE Controle > 0; ');
          LimparDadosTabela('DELETE FROM gragruval  WHERE Controle > 0; ');
          LimparDadosTabela('DELETE FROM fabricas   WHERE Codigo > 0;   ');
          LimparDadosTabela('DELETE FROM unidmed    WHERE Codigo > 0;   ');
          LimparDadosTabela('DELETE FROM clasfisc   WHERE Codigo > 0;   ');
        end;
        2:
        begin
          LimparDadosTabela('DELETE FROM carteiras  WHERE Codigo <> 0;  ');
          LimparDadosTabela('DELETE FROM subgrupos  WHERE Codigo > 0;   ');
          LimparDadosTabela('DELETE FROM contas     WHERE Codigo > 0;   ');
          LimparDadosTabela('DELETE FROM ' + FTabLctA + ' WHERE Controle > 0; ');
          LimparDadosTabela('UPDATE Controle SET ' + LAN_CTOS + ' = 0;           ');
        end;
        3:
        begin
          LimparDadosTabela('DELETE FROM pedivda  WHERE Codigo <> 0;  ');
          LimparDadosTabela('DELETE FROM pedivdaits  WHERE Codigo <> 0;  ');
        end;
        4:
        begin
          //LimparDadosTabela('DELETE FROM ??????  WHERE Codigo <> 0;  ');
        end;
        7:
        begin
          LimparDadosTabela('DELETE FROM natoper;');
        end;
        8:
        begin
          LimparDadosTabela('DELETE FROM nfecaba WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfecabb WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfecabf WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfecabg WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfecabxreb WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfecabxvol WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfecabxlac WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfecaby WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfeitsi WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfeitsidi WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfeitsidia WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfeitsp WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfeitsn WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfeitso WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfeitsq WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfeitss WHERE FatID=1;');
          LimparDadosTabela('DELETE FROM nfeitsu WHERE FatID=1;');
          //
          LimparDadosTabela('DELETE FROM stqmovitsa WHERE Tipo=1;');
          LimparDadosTabela('DELETE FROM stqmovitsb WHERE Tipo=1;');
        end;
        9:
        begin
          LimparDadosTabela('DELETE FROM nfecaba WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfecabb WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfecabf WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfecabg WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfecabxreb WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfecabxvol WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfecabxlac WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfecaby WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfeitsi WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfeitsidi WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfeitsidia WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfeitsp WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfeitsn WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfeitso WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfeitsq WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfeitss WHERE FatID<>1;');
          LimparDadosTabela('DELETE FROM nfeitsu WHERE FatID<>1;');
          //
          LimparDadosTabela('DELETE FROM stqmovitsa WHERE Tipo<>1;');
          LimparDadosTabela('DELETE FROM stqmovitsb WHERE Tipo<>1;');
          //
          LimparDadosTabela('DELETE FROM gragru1 WHERE Nivel1<=-900000;');
          LimparDadosTabela('DELETE FROM gragruc WHERE Controle<=-900000;');
          LimparDadosTabela('DELETE FROM gragrux WHERE Controle<=-900000;');
          //
          LimparDadosTabela('DELETE FROM prdgruptip WHERE Codigo=-3;');
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO prdgruptip SET');
          Dmod.QrUpd.SQL.Add('Codigo=-3,  CodUsu=-3, Nome="-INDEFINIDOS-", MadeBy=2,');
          Dmod.QrUpd.SQL.Add('Fracio=3, Gradeado=0, TipPrd=3, NivCad=2, FaixaIni=0,');
          Dmod.QrUpd.SQL.Add('FaixaFim=0, TitNiv1="-Indefinidos-", TitNiv2="Grupo",');
          Dmod.QrUpd.SQL.Add('ImpedeCad=1');
          Dmod.QrUpd.ExecSQL;
        end;
      end;
    end;
  end;
  //
  //
  Memo1.Lines.Clear;
  ProgressBar1.Position := 0;
  FMaxRegistros := 0;
  FRegistrosIns := 0;
  if not DefineCodigosIniciais() then Exit;
  //
  TbTabelas.First;
  while not TbTabelas.Eof do
  begin
    if TbTabelasAtivo.Value = 1 then
    begin
      NomeTab := Uppercase(TbTabelasNome.Value);
      MyObjects.Informa(LaTabela, True, 'Calculando total de registros ' + TbTabelasNome.Value);
      for I := 0 to ComponentCount - 1 do
        if TComponent(Components[I]) is TVKDBFNTX then
        begin
          if pos(NomeTab, Uppercase(TVKDBFNTX(Components[I]).DBFFileName)) > 0 then
          try
            if TVKDBFNTX(Components[I]).State <> dsInactive then
              FMaxRegistros := FMaxRegistros + TVKDBFNTX(Components[I]).RecordCount;
          except

          end;
        end;
    end;
    //
    TbTabelas.Next;
  end;
  ProgressBar1.Max := FMaxRegistros;
  Geral.AdequaMedida(grandezaNone, FMaxRegistros, 0, Tam, Niv, Nom);
  EdTamTot.DecimalSize  := Niv;
  EdTamTot.ValueVariant := Tam;
  LaTamTot.Caption := Nom;
  Update;
  Application.ProcessMessages;
  //
  TbTabelas.First;
  FTimeIni := Now();
  while not TbTabelas.Eof do
  begin
    if TbTabelasAtivo.Value = 1 then
    begin
      NomeTab := Uppercase(TbTabelasNome.Value);
      MyObjects.Informa(LaTabela, True, 'Importando a tabela ' + TbTabelasNome.Value);
      // Entidades
      if NomeTab = 'CLIENTES.DBF'  then ImportaClientes()  else
      if NomeTab = 'VENDEDOR.DBF'  then ImportaVendedor()  else
      if NomeTab = 'TRANSPOR.DBF'  then ImportaTranspor()  else
      if NomeTab = 'FORNECED.DBF'  then ImportaForneced()  else
      // Grade
      if NomeTab = 'TIPOGRUP.DBF'  then ImportaTipoGrup()  else
      if NomeTab = 'GRUPROD.DBF'   then ImportaGruProd()   else
      if NomeTab = 'FABRICA.DBF'   then ImportaFabrica()   else
      if NomeTab = 'UNIDADE.DBF'   then ImportaUnidade()   else
      if NomeTab = 'CLASFISC.DBF'  then ImportaClasFisc()  else
      if NomeTab = 'PRODUTOS.DBF'  then ImportaProdutos()  else
      // Financeiro
      if NomeTab = 'FORMAPAG.DBF'  then ImportaFormaPag()  else
      if NomeTab = 'CENCUSTO.DBF'  then ImportaCenCusto()  else
      if NomeTab = 'ITEMCUST.DBF'  then ImportaItemCust()  else
      if NomeTab = 'PAGAR.DBF'     then
      begin
        ImportaPagar(-11, EdDir.Text);
        ImportaPagar(-12, EdBak.Text);
      end
      else
      if NomeTab = 'RECEBER.DBF'   then
      begin
        ImportaReceber(-11, EdDir.Text);
        ImportaReceber(-12, EdBak.Text);
      end
      else
      //  Pedidos
      if NomeTab = 'PEDIDO.DBF'    then
      begin
        ImportaPedido(-11, EdDir.Text);
        //ImportaPedido(-12, EdBak.Text);
      end
      else
      if NomeTab = 'ITENSPED.DBF'  then
      begin
        ImportaItensPed(-11, EdDir.Text);
        //ImportaItensPed(-12, EdBak.Text);
      end
      else
      //  NFs
      if NomeTab = 'NOTAFISC.DBF'  then ImportaNotaFisc()  else
      if NomeTab = 'NOTA_ENT.DBF'  then ImportaNota_Ent()  else
      if NomeTab = 'ITENS_ENT.DBF' then ImportaItens_Ent() else
      //
      //  SINTEGRA
      if NomeTab = 'R10.DBF'       then ImportaR10() else
      if NomeTab = 'R11.DBF'       then ImportaR11() else
      if NomeTab = 'R50.DBF'       then ImportaR50() else
      if NomeTab = 'R51.DBF'       then ImportaR51() else
      if NomeTab = 'R53.DBF'       then ImportaR53() else
      if NomeTab = 'R54.DBF'       then ImportaR54() else
      if NomeTab = 'R70.DBF'       then ImportaR70() else
      if NomeTab = 'R71.DBF'       then ImportaR71() else
      if NomeTab = 'R74.DBF'       then ImportaR74() else
      if NomeTab = 'R75.DBF'       then ImportaR75() else
      if NomeTab = 'R85.DBF'       then ImportaR85() else
      if NomeTab = 'R86.DBF'       then ImportaR86() else
      if NomeTab = 'R88.DBF'       then ImportaR88() else
      if NomeTab = 'R90.DBF'       then ImportaR90() else
      //
      //  Invers�o de CFOPS
      if NomeTab = 'NATOPER.DBF'   then ImportaNatOper()   else
      //
      //  Combina PEDIDO.DBF com NOTAFISC.DBF
      if NomeTab = 'NFS EMITIDAS'  then
        FImportaEmitida := True else
      if NomeTab = 'NFS ENTRADA'   then
        FImportaEntrada := True else
      Geral.MB_Aviso('Importa��o de tabela n�o implementada:' + sLineBreak +
      TbTabelasNome.Value);
    end;
    //
    TbTabelas.Next;
  end;
  if FImportaEmitida or FImportaEntrada then
    ImportaMinhaNFe();
  MyObjects.Informa(LaTabela, False, 'Importa��o OK');
  MyObjects.Informa2(LaAviso1, LaAviso2 , False, '');
  PB1.Position := 0;
  PB1.Max := 100;
{
  if FileExists(EdArquivo.Text) then
  begin
    case RGTipo.ItemIndex of
      0: ImportaClientes();
      else Geral.MB_Aviso('Tipo de dados n�o implementado!');
    end;
  end else Geral.MB_Aviso('O arquivo "' + EdArquivo.Text +
  '" n�o foi localizado!');
}
end;

procedure TFmImportaFoxPro.BtPararClick(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmImportaFoxPro.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmImportaFoxPro.BtTodosClick(Sender: TObject);
begin
//  AtualizaTodos(1);
end;

function TFmImportaFoxPro.BuscaEntidadeAntiga(Sufixo, CodStr: String): Integer;
begin
  Result := 0;
  if Trim(CodStr) <> '' then
  begin
    QrEntidade.Close;
    QrEntidade.Params[0].AsString := Sufixo + CodStr;
    QrEntidade . O p e n ;
    //
    if QrEntidade.RecordCount = 1 then
      Result := QrEntidadeCodigo.Value
    else
{    Geral.MB_Aviso('Foram localizados ' + FormatFloat('0',
    QrEntidade.RecordCount) + ' cadastros para o c�digo ' + Sufixo + CodStr +
    '!' + sLineBreak + 'Nenhum ser� considerado!');
}
    Memo1.Lines.Add(Sufixo + CodStr + ' N�o localizado');
  end;
end;

function TFmImportaFoxPro.BuscaEntidadePeloCNPJ(CNPJ: String): Integer;
var
  Doc: String;
begin
  Result := 0;
  Doc := Geral.SoNumero_TT(CNPJ);
  if Doc <> '' then
  begin
    QrEntiDoc.Close;
    QrEntiDoc.Params[00].AsString := Doc;
    QrEntiDoc.Params[01].AsString := Doc;
    QrEntiDoc . O p e n ;
    //
    if QrEntiDoc.RecordCount > 0 then
      Result := QrEntiDocCodigo.Value
    else
      Memo1.Lines.Add(CNPJ + ' N�o localizado');
  end;
end;

procedure TFmImportaFoxPro.Button1Click(Sender: TObject);
begin
  QrIts.Close;
  QrIts . O p e n ;
  QrIts.First;
  while not QrIts.Eof do
  begin
    QrSel.Close;
    QrSel.Params[00].AsString := QrItsDESCPROD.Value;
    QrSel.Params[01].AsString := QrItsCODPROD.Value;
    QrSel . O p e n ;
    //
    case QrSel.RecordCount of
      0: EdNao.ValueVariant := EdNao.ValueVariant + 1;
      1: EdSim.ValueVariant := EdSim.ValueVariant + 1;
      else EdVar.ValueVariant := EdVar.ValueVariant + 1;
    end;
    QrIts.Next;
  end;
end;

procedure TFmImportaFoxPro.Carrega1Click(Sender: TObject);
var
  I: Integer;
  Ref: String;
  Q0, Q1, Qn: Integer;
  ItensErr: String;
var
  Codigo, Nivel1: Integer;
  //Nome, Preco: String;
  Preco: Double;
begin
  FListaIgapo := GradeCriar.RecriaTempTableNovo(ntrttListaIgapo, DmodG.QrUpdPID1, False);
  Codigo := EdTabela.ValueVariant;
  if MyObjects.FIC(Codigo=0, EdTabela, 'Informe a tabela') then
    Exit;
  if MyObjects.Xls_To_StringGrid(GradeLista, EdLista.Text, PB1, LaAviso1, LaAviso2) then
  begin
{
    Nome := 'Tabela importada em ' + Geral.FDT(Now(), 2);
    Codigo := UMyMod.BuscaEmLivreY_Def('TabePrcCab', 'Codigo', stIns, 0);
    CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'TabePrcCab', 'CodUsu', [], [],
      stIns, 0, siPositivo, nil);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'tabeprccab', False, [
    'CodUsu', 'Nome'], ['Codigo'], [
    CodUsu, Nome], [Codigo], True) then ;
}
    Q0 := 0;
    Q1 := 0;
    Qn := 0;
    ItensErr := '';
    for I := 1 to GradeLista.RowCount - 1 do
    begin
      Ref := GradeLista.Cells[1, I];
      if Ref <> '' then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrGG1, Dmod.MyDB, [
        'SELECT Nivel1  ',
        'FROM gragru1 ',
        'WHERE Referencia="' + Ref + '"',
        'AND Nivel1 > 0',
        '']);
        case QrGG1.RecordCount of
          0: Q0 := Q0 + 1;
          1: Q1 := Q1 + 1;
          else Qn := Qn + 1;
        end;
        if QrGG1.RecordCount = 1 then
        begin
          Nivel1 := QrGG1Nivel1.Value;
          Preco := Geral.DMV(GradeLista.Cells[4, I]);
          {
          Preco := GradeLista.Cells[4, I];
          Preco := Geral.Substitui(Preco, '.', '');
          Preco := Geral.Substitui(Preco, ',', '.');
          }
          //
          UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'tabeprcgrg',
          ['Codigo', 'Nivel1'], ['=','='], [Codigo, Nivel1], '');
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'tabeprcgrg', False, [
          'Preco'], ['Codigo', 'Nivel1'], [
          Preco], [Codigo, Nivel1], True);
        end
        else
        begin
          ItensErr := ItensErr + '"' + Ref + '" = ' +
            IntToStr(QrGG1.RecordCount) + sLineBreak;
        end;
      end;
    end;
    Geral.MensagemArray(MB_OK+MB_ICONINFORMATION, 'Itens encontrados',
    ['Q0','Q1','Qn'], [IntToStr(Q0), IntToStr(Q1), IntToStr(Qn)], nil);
    if ItensErr <> '' then
      Geral.MB_Aviso(ItensErr);
 end;
end;

procedure TFmImportaFoxPro.CLListaClick(Sender: TObject);
var
  Ativa: Byte;
begin
  Screen.Cursor := crHourGlass;
  try
    CLLista.Enabled := False;
    Ativa := dmkPF.EscolhaDe2Int(CLLista.Checked[CLLista.ItemIndex], 1, 0);
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE ' + FCodAnt);
    DModG.QrUpdPID1.SQL.Add('SET Ativo=' + FormatFloat('0', Ativa));
    DModG.QrUpdPID1.SQL.Add('WHERE Grupo = ' + FormatFloat('0', CLLista.ItemIndex + 1));
    DModG.QrUpdPID1.ExecSQL;
    //
    TbTabelas.Refresh;
  finally
    CLLista.Enabled := True;
    Screen.Cursor := crDefault;
  end;
end;

function TFmImportaFoxPro.DefineCodigosIniciais(): Boolean;
begin
  FIniClien := EdIniClien.ValueVariant;
  FIniVende := EdIniVende.ValueVariant;
  FIniTrans := EdIniTrans.ValueVariant;
  FIniForne := EdIniForne.ValueVariant;
  //
  FIniGProd := 1;
  FIniTGrup := 1;
  FIniFabri := 1;
  FIniUnida := 1;
  FIniProds := 1;
  FIniClFis := 1;
  //
  FIniFoPag := 1;
  FIniCeCus := 1;
  FIniItCus := 1;
  FIniPagar := 1;
  FIniReceb := 1;
  //
  FIniClass := 1;
  //
  if (FIniClien = 0)
  or (FIniVende = 0)
  or (FIniTrans = 0)
  or (FIniForne = 0)
  //
  or (FIniGProd = 0)
  or (FIniTGrup = 0)
  or (FIniFabri = 0)
  or (FIniUnida = 0)
  or (FIniProds = 0)
  or (FIniClFis = 0)
  //
  or (FIniFoPag = 0)
  or (FIniCeCus = 0)
  or (FIniItCus = 0)
  or (FIniPagar = 0)
  or (FIniReceb = 0)
  //
  or (FIniClass = 0)
  then begin
    Result := False;
    Geral.MB_Aviso('Defina os c�digos iniciais!');
  end else Result := True;
end;

procedure TFmImportaFoxPro.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmImportaFoxPro.FormCreate(Sender: TObject);
const
  Ativo = 0;
  MaxTables = 37;
  TabNomes: array[1..MaxTables] of String = (
  (*1*)
  'vendedor.DBF', 'CLIENTES.DBF', 'transpor.DBF', 'forneced.DBF',
  (*2*)
  'TIPOGRUP.DBF', 'GRUPROD.DBF', 'FABRICA.DBF',
  'UNIDADE.DBF', 'CLASFISC.DBF', 'produtos.DBF',
  (*3*)
  'FORMAPAG.DBF', 'CENCUSTO.DBF', 'ITEMCUST.DBF', 'PAGAR.DBF', 'RECEBER.DBF',
  (*4*)
  'PEDIDO.DBF', 'ITENSPED.DBF',
  (*5*)
  'NOTAFISC.DBF',
  (*6*)
  'NOTA_ENT.DBF', 'ITENS_ENT.DBF',
  (*7*)
  'R10.DBF', 'R11.DBF', 'R50.DBF', 'R51.DBF', 'R53.DBF', 'R54.DBF', 'R70.DBF',
  'R71.DBF', 'R74.DBF', 'R75.DBF', 'R85.DBF', 'R86.DBF', 'R88.DBF', 'R90.DBF',
  (*8*)
  'NATOPER.DBF',
  (*9*)
  'NFs emitidas',
  (*10*)
  'NFs Entrada');
  TabGrupo: array [1..MaxTables] of Integer = (
  1, 1, 1, 1,                               // 1..4
  2, 2, 2, 2, 2, 2,                         // 5..10
  3, 3, 3, 3, 3,                            // 11..15
  4, 4,                                     // 16..17
  5,                                        // 18
  6, 6,                                     // 19..20
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, // 21..34
  8,                                        // 35
  9,                                        // 36
  10);                                      // 37

var
  Grupo, Codigo: Integer;
  Nome, Numero, Compl, Bairro, xLogr, nLogr, xRua: String;
begin
  ImgTipo.SQLType := stLok;
  //
  TbTabelas.Close;
  TbTabelas.Database := DmodG.MyPID_DB;
  FCodAnt := UCriar.RecriaTempTableNovo(ntrtt_CodAnt, DmodG.QrUpdPID1, False);
  Codigo := 1;
  while Codigo <= MaxTables do
  begin
    Nome := TabNomes[Codigo];
    Grupo := TabGrupo[Codigo];
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCodAnt, False, [
    'Grupo', 'Nome', 'Ativo'], ['Codigo'], [Grupo, Nome, Ativo], [Codigo], False);
    Codigo := Codigo + 1;
  end;
  //
  TbEmpresa.Close;
  TbEmpresa . O p e n ;
  //
  Geral.SeparaLogradouro(0, TbEmpresaENDEREO.Value, xLogr, nLogr, xRua);
  Geral.SeparaEndereco(0, TbEmpresaENDEREO.Value, Numero, Compl, Bairro,
    xLogr, nLogr, xRua, False);
  EdLgr_Txt.Text := xLogr;
  EdLgr_COd.Text := nLogr;
  EdRua.Text := xRua;
  EdNumero.Text := Numero;
  EdCompl.Text := Compl;
  EdBairro.Text := Bairro;
  try
    TbTabelas.Close;
    TbTabelas . O p e n ;
    //
    TbClientes.Close;
    TbClientes . O p e n ;
    //
    TbVendedor.Close;
    TbVendedor . O p e n ;
    //
    TbTranspor.Close;
    TbTranspor . O p e n ;
    //
    TbForneced.Close;
    TbForneced . O p e n ;
    //
    TbTipoGrup.Close;
    TbTipoGrup . O p e n ;
    //
    TbGruProd.Close;
    TbGruProd . O p e n ;
    //
    TbFabrica.Close;
    TbFabrica . O p e n ;
    //
    TbUnidade.Close;
    TbUnidade . O p e n ;
    //
    TbClasFisc.Close;
    TbClasFisc . O p e n ;
    //
    TbProdutos.Close;
    TbProdutos . O p e n ;
    //

    //

    TbFormaPag.Close;
    TbFormaPag . O p e n ;
    //
    TbCenCusto.Close;
    TbCenCusto . O p e n ;
    //
    TbItemCust.Close;
    TbItemCust . O p e n ;
    //
    TbPagar.Close;
    TbPagar . O p e n ;
    //
    TbReceber.Close;
    TbReceber . O p e n ;

    //

    TbPedido.Close;
    TbPedido . O p e n ;
    //
    TbItensPed.Close;
    TbItensPed . O p e n ;
    //

    //

    TbNotaFisc.Close;
    TbNotaFisc . O p e n ;
    //

    //

    TbNota_Ent.Close;
    TbNota_Ent . O p e n ;
    //
    TbItens_Ent.Close;
    TbItens_Ent . O p e n ;
    //

    //

    TbR10.Close;
    TbR10 . O p e n ;
    //
    TbR11.Close;
    TbR11 . O p e n ;
    //
    TbR50.Close;
    TbR50 . O p e n ;
    //
    TbR51.Close;
    TbR51 . O p e n ;
    //
    TbR53.Close;
    TbR53 . O p e n ;
    //
    TbR54.Close;
    TbR54 . O p e n ;
    //
    TbR70.Close;
    TbR70 . O p e n ;
    //
    TbR71.Close;
    TbR71 . O p e n ;
    //
    TbR74.Close;
    TbR74 . O p e n ;
    //
    TbR75.Close;
    TbR75 . O p e n ;
    //
    TbR85.Close;
    TbR85 . O p e n ;
    //
    TbR86.Close;
    TbR86 . O p e n ;
    //
    TbR88.Close;
    TbR88 . O p e n ;
    //
    TbR90.Close;
    TbR90 . O p e n ;
    //

    //

    TbNatOper.Close;
    TbNatOper . O p e n ;
    //
  finally

  end;
end;

procedure TFmImportaFoxPro.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImportaFoxPro.frxLctOrfGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULO' then
     Value := FTituloLctOrf;
end;

procedure TFmImportaFoxPro.GradeTabelasCellClick(Column: TColumn);
begin
  if TbTabelas.RecordCount > 0 then
  begin
    if (Column.Field.FieldName = 'Ativo') then
    begin
      TbTabelas.Edit;
      if TbTabelasAtivo.Value = 1 then
        TbTabelasAtivo.Value := 0
      else
        TbTabelasAtivo.Value := 1;
      TbTabelas.Post;
    end else
  end;
end;

procedure TFmImportaFoxPro.ImportaCenCusto();
const
  Grupo = 0;
  OrdemLista = 999;
  Prefixo = '';
  CtrlaSdo = 0;
var
  Codigo, Antigo: Integer;
  //
  Nome: String;
  SQLType: TSQLType;
  Continua: Boolean;
  //
  TipoAgrupa, _Categoria: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsCenCusto;
    TbCenCusto.Close;
    TbCenCusto.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbCenCusto . O p e n ;
    TbCenCusto.First;
    PB1.Position := 0;
    PB1.Max := TbCenCusto.RecordCount;
    //Antigo := -999999999;
    while not TbCenCusto.Eof do
    begin
      AtualizaTempo();
      //Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando CenCusto ' +
        TbCenCustoCODIGO.Value);
      //
      Continua := True;
      Antigo   := Geral.IMV(Trim(TbCenCustoCODIGO.Value));
      if LocalizaAntigoInt('subgrupos', 'Codigo', Antigo) then
      begin
        Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de CenCusto ' +
        FormatFloat('0', Antigo) + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        SQLType := stUpd;
      end else SQLType := stIns;
      if Continua then
      begin
        Codigo := Geral.IMV(Trim(TbCenCustoCODIGO.Value)) +
        FIniCeCus - 1;
        //
        Nome := TbCenCustoDESCRIO.Value;
        TipoAgrupa  := TbCenCustoTipo.Value;
        _Categoria  := TbCenCustoCATEGORIA.Value;
        //
        //CodUsu := Codigo;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'subgrupos', False, [
        'Nome', 'Grupo', 'OrdemLista',
        'TipoAgrupa', 'CtrlaSdo', '_Categoria'], [
        'Codigo'], [
        Nome, Grupo, OrdemLista,
        TipoAgrupa, CtrlaSdo, _Categoria], [
        Codigo], True) then ;
      end;
      //
      TbCenCusto.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaClasFisc();
const
  Prefixo = '';
var
  Codigo, CodUsu, Antigo: Integer;
  //
  Nome, NCM: String;
  SQLType: TSQLType;
  Continua: Boolean;
  //
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsClasFisc;
    TbClasFisc.Close;
    TbClasFisc.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbClasFisc . O p e n ;
    TbClasFisc.First;
    PB1.Position := 0;
    PB1.Max := TbClasFisc.RecordCount;
    //Antigo := -999999999;
    while not TbClasFisc.Eof do
    begin
      AtualizaTempo();
      //Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando ClasFisc ' +
        FormatFloat('0', TbClasFiscCODIGO.Value));
      //
      Continua := True;
      Antigo   := TbClasFiscCODIGO.Value;
      if LocalizaAntigoInt('ClasFisc', 'Codigo', Antigo) then
      begin
        Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de ClasFisc ' +
        FormatFloat('0', Antigo) + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        SQLType := stUpd;
      end else SQLType := stIns;
      if Continua then
      begin
        Codigo := TbClasFiscCODIGO.Value +
        FIniClFis - 1;
        //
        Nome := TbClasFiscDESCRIO.Value;
        NCM  := Nome;
        //
        CodUsu := Codigo;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ClasFisc', False, [
        'CodUsu', 'Nome', 'NCM'], ['Codigo'
        ], [CodUsu, Nome, NCM], [Codigo], True) then ;
      end;
      //
      TbClasFisc.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaNatOper();
const
  Prefixo = '';
var
   //Codigo, CodUsu: Integer;
  //
  //Nome,
  Antigo: String;
  SQLType: TSQLType;
  Continua: Boolean;
  //
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsNatOper;
    TbNatOper.Close;
    TbNatOper.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbNatOper . O p e n ;
    TbNatOper.First;
    PB1.Position := 0;
    PB1.Max := TbNatOper.RecordCount;
    Antigo := '';
    while not TbNatOper.Eof do
    begin
      AtualizaTempo();
      //Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando NatOper ' +
        TbNatOperCODIGO.Value);
      //
      Continua := True;
      Antigo        := TbNatOperCODIGO.Value;
      if LocalizaAntigoStr('natoper', 'Codigo', '', Antigo) then
      begin
        {
        if Trim(Antigo) = '6.202' then
          Continua := True
        else
        }
          Continua := Geral.MB_Pergunta(
          'Foi localizado um segundo c�digo para o cadastro de NatOper ' +
          Antigo + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        //  
        SQLType := stUpd;
      end else SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'natoper', False, [
        'Nome', 'ICMS', 'IPI',
        'Especial', 'GeraCR', 'CodEnt'], [
        'Codigo'], [
        TbNatOperDESCRIO.Value, TbNatOperICMS.Value, TbNatOperIPI.Value,
        TbNatOperEspecial.Value, TbNatOperGeraCR.Value, TbNatOperCodEnt.Value], [
        TbNatOperCodigo.Value], True) then ;
      end;
      //
      TbNatOper.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaClientes();
const
  Prefixo  = 'C';
  Cliente1 = 'V';
var
  EUF, PUF, Codigo, CodUsu, Tipo: Integer;
  RazaoSocial, Fantasia, CNPJ, IE, ERua, ENumero, ECompl, EBairro, ECidade,
  ECEP, ETe1, EFax, ECel, EEmail,
  Nome, Apelido, CPF, RG, PRua, PNumero, PCompl, PBairro, PCidade,
  PCEP, PTe1, PFAX, PCel, PEmail,
  Contato, Observacoes: String;
  //
  Prazo, Transporta, Vendedor, Comissao, Primeira, Ultima, DataSerasa, NF, URL,
  Antigo, Ant: String;
  Valor: Double;
  Situacao, Serasa, Vendedor_, Transporta_: Integer;
  SQLType: TSQLType;
  Continua: Boolean;
  //
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 0;
    DBGrid1.DataSource := DsClientes;
    TbClientes.Close;
    TbClientes.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbClientes . O p e n ;
    TbClientes.First;
    PB1.Position := 0;
    PB1.Max := TbClientes.RecordCount;
    Antigo := '';
    while not TbClientes.Eof do
    begin
      AtualizaTempo();
      Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando cliente ' +
        TbClientesCODIGO.Value);
      //
      Continua := True;
      Antigo        := Geral.SoNumero_TT(TbClientesCODIGO.Value);
(*mais r�pido*)
      if Ant = Antigo then
      begin
        {
        if (Geral.IMV(Antigo) = 3143)
        or (Geral.IMV(Antigo) = 3194)
        or (Geral.IMV(Antigo) = 3199) then
          Continua := True
        else
        }
          Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de cliente ' +
        Ant + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        //
        SQLType := stUpd;
      end else SQLType := stIns;
{}
{Muito lento
      if LocalizaAntigo(Prefixo, Antigo) then
      begin
        Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de cliente ' +
        Antigo + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        SQLType := stUpd;
      end else SQLType := stIns;
}
      Codigo := Geral.IMV(Trim(Geral.SoNumero_TT(TbClientesCODIGO.Value))) +
        FIniClien - 1;
      if Continua and (Codigo <> 0) then
      begin
        if (Codigo = 1973) and (Ant = '') then else
        begin
          ERua          := '';
          ENumero       := '';
          ECompl        := '';
          EBairro       := '';
          ECidade       := '';
          EUF           := 0;
          ECEP          := '';
          ETe1          := '';
          EFax          := '';
          ECel          := '';
          EEmail        := '';
          //
          PRua          := '';
          PNumero       := '';
          PCompl        := '';
          PBairro       := '';
          PCidade       := '';
          PUF           := 0;
          PCEP          := '';
          PTe1          := '';
          PFax          := '';
          PCel          := '';
          PEmail        := '';
          Nome          := '';
          Apelido       := '';
          CPF           := '';
          RG            := '';
          PRua          := '';
          RazaoSocial   := '';
          Fantasia      := '';
          CNPJ          := '';
          IE            := '';
          ERua          := '';
          //
          if TbClientesTIPO.Value = 1 then
          begin
            Tipo          := 0;
            RazaoSocial   := TbClientesRAZO.Value;
            Fantasia      := TbClientesNOME.Value;
            CNPJ          := Geral.SoNumero_TT(TbClientesCNPJ.Value);
            IE            := Geral.SoNumero_TT(TbClientesICMS.Value);
            ERua          := TbClientesENDEREO.Value;
            ENumero       := TbClientesNUMEND.Value;
            ECompl        := TbClientesCOMPLEND.Value;
            EBairro       := TbClientesBAIRRO.Value;
            ECidade       := TbClientesCIDADE.Value;
            EUF           := Geral.GetCodigoUF_da_SiglaUF(TbClientesUF.Value);
            ECEP          := TbClientesCEP.Value;
            ETe1          := Geral.SoNumero_TT(TbClientesFONE.Value);
            EFax          := Geral.SoNumero_TT(TbClientesFAX.Value);
            ECel          := Geral.SoNumero_TT(TbClientesCELULAR.Value);
            EEmail        := TbClientesEMAIL.Value;
            PTe1          := Geral.SoNumero_TT(TbClientesFONECONT.Value);
            PCel          := Geral.SoNumero_TT(TbClientesCELCONT.Value);
            PEmail        := TbClientesEMAILCONT.Value;
            //
          end else
          begin
            Tipo          := 1;
            //
            Nome          := TbClientesRAZO.Value;
            Apelido       := TbClientesNOME.Value;
            CPF           := Geral.SoNumero_TT(TbClientesCPF.Value);
            RG            := Geral.SoNumero_TT(TbClientesICMS.Value);
            PRua          := TbClientesENDEREO.Value;
            PNumero       := TbClientesNUMEND.Value;
            PCompl        := TbClientesCOMPLEND.Value;
            PBairro       := TbClientesBAIRRO.Value;
            PCidade       := TbClientesCIDADE.Value;
            PUF           := Geral.GetCodigoUF_da_SiglaUF(TbClientesUF.Value);
            PCEP          := TbClientesCEP.Value;
            PTe1          := Geral.SoNumero_TT(TbClientesFONE.Value);
            PFax          := Geral.SoNumero_TT(TbClientesFAX.Value);
            PCel          := Geral.SoNumero_TT(TbClientesCELULAR.Value);
            PEmail        := TbClientesEMAIL.Value;
            ETe1          := Geral.SoNumero_TT(TbClientesFONECONT.Value);
            ECel          := Geral.SoNumero_TT(TbClientesCELCONT.Value);
            EEmail        := TbClientesEMAILCONT.Value;
          end;
          URL           := TbClientesURL.Value;
          Prazo         := TbClientesPRAZO.Value;
          Transporta    := TbClientesTRANSPORTA.Value;
          Contato       := TbClientesCONTATO.Value;
          Vendedor      := TbClientesVENDEDOR.Value;
          Comissao      := TbClientesCOMISSAO.Value;
          Primeira      := Geral.FDT(TbClientesPrimeira.Value, 1);
          Ultima        := Geral.FDT(TbClientesUltima.Value, 1);
          Valor         := TbClientesValor.Value;
          NF            := TbClientesNF.Value;
          SERASA        := TbClientesSERASA.Value;
          DataSerasa    := Geral.FDT(TbClientesDATASERASA.Value, 1);
          Situacao      := TbClientesSITUAO.Value;
          Observacoes   := TbClientesOBS.Value;
          //
          Vendedor_     := Geral.IMV(Trim(Vendedor));
          if Vendedor_ > 0 then
            Vendedor_ := Vendedor_ + FIniVende - 1;
          //
          Transporta_     := Geral.IMV(Trim(Transporta));
          if Transporta_ > 0 then
          begin
            Transporta_ := Transporta_ + FIniTrans - 1;
            InsereEntiTrnasp(Codigo, Transporta_);
          end;
          //
          CodUsu := Codigo;

          if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entidades', False, [
          'RazaoSocial', 'Fantasia',
          'CNPJ', 'IE',
          'Nome', 'Apelido', 'CPF',
          'RG',
          'ERua', 'ENumero', 'ECompl',
          'EBairro', 'ECidade', 'EUF',
          'ECEP', 'ETe1',
          'ECel',
          'EFax', 'EEmail',
          'PRua',
          'PNumero', 'PCompl', 'PBairro',
          'PCidade', 'PUF', 'PCEP',
          'PTe1',
          'PCel', 'PFax',
          'PEmail',
          'Cliente1',
          'Observacoes',
          'Tipo', 'CodUsu',
          'URL', 'Antigo'
          ], [
          'Codigo'], [
          RazaoSocial, Fantasia,
          CNPJ, IE,
          Nome, Apelido, CPF,
          RG,
          ERua, ENumero, ECompl,
          EBairro, ECidade, EUF,
          ECEP, ETe1,
          ECel,
          EFax, EEmail,
          PRua,
          PNumero, PCompl, PBairro,
          PCidade, PUF, PCEP,
          PTe1,
          PCel, PFax,
          PEmail,
          Cliente1,
          Observacoes,
          Tipo, CodUsu,
          URL, Prefixo + Antigo
          ], [
          Codigo], True) then
          UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'enticlient', False, [
          'Prazo', 'CompraPrima', 'CompraUltim',
          'CompraValor', 'CompraNF', 'SerasaSit',
          'SerasaDta', 'Situacao', 'Vendedor'], [
          'Codigo'], [
          Prazo, Primeira, Ultima,
          Valor, NF, Serasa,
          DataSerasa, Situacao, Vendedor_], [
          Codigo], True);
        end;
      end;
      //
      TbClientes.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaFabrica();
const
  Prefixo = '';
var
  Codigo, CodUsu: Integer;
  //
  Nome, Marca, Antigo: String;
  SQLType: TSQLType;
  Continua: Boolean;
  //
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsFabrica;
    TbFabrica.Close;
    TbFabrica.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbFabrica . O p e n ;
    TbFabrica.First;
    PB1.Position := 0;
    PB1.Max := TbFabrica.RecordCount;
    Antigo := '';
    while not TbFabrica.Eof do
    begin
      AtualizaTempo();
      //Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando Fabrica ' +
        TbFabricaCODIGO.Value);
      //
      Continua := True;
      Antigo        := Geral.SoNumero_TT(TbFabricaCODIGO.Value);
      if LocalizaAntigoInt('Fabricas', 'Codigo', Geral.IMV(Trim(Antigo))) then
      begin
        Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de Fabrica ' +
        Antigo + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        SQLType := stUpd;
      end else SQLType := stIns;
      if Continua then
      begin
        Codigo := Geral.IMV(Trim(Geral.SoNumero_TT(TbFabricaCODIGO.Value))) +
        FIniFabri - 1;
        //
        Nome := TbFabricaNOME.Value;
        Marca := TbFabricaMARCA.Value;
        //
        CodUsu := Codigo;

        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'fabricas', False, [
        'CodUsu', 'Nome', 'Marca'], ['Codigo'], [
        CodUsu, Nome, Marca], [Codigo], True) then ;
      end;
      //
      TbFabrica.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaFormaPag();
const
  Prefixo = '';
  Banco = -1;
  ForneceI = -11;
  Tipo = 0;
var
  Codigo, Antigo: Integer;
  //
  Nome, NCM: String;
  SQLType: TSQLType;
  Continua: Boolean;
  //
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsFormaPag;
    TbFormaPag.Close;
    TbFormaPag.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbFormaPag . O p e n ;
    TbFormaPag.First;
    PB1.Position := 0;
    PB1.Max := TbFormaPag.RecordCount;
    //Antigo := -999999999;
    while not TbFormaPag.Eof do
    begin
      AtualizaTempo();
      //Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando FormaPag ' +
        TbFormaPagCODIGO.Value);
      //
      Continua := True;
      Antigo   := Geral.IMV(Trim(TbFormaPagCODIGO.Value));
      if LocalizaAntigoInt('carteiras', 'Codigo', Antigo) then
      begin
        Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de FormaPag ' +
        FormatFloat('0', Antigo) + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        SQLType := stUpd;
      end else SQLType := stIns;
      if Continua then
      begin
        Codigo := Geral.IMV(Trim(TbFormaPagCODIGO.Value)) +
        FIniFoPag - 1;
        //
        Nome := TbFormaPagDESCRICAO.Value;
        NCM  := Nome;
        //
        //CodUsu := Codigo;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'carteiras', False, [
        'Nome', 'Banco', 'ForneceI'], ['Codigo', 'Tipo'], [
        Nome, Banco, ForneceI], [Codigo, Tipo], True) then ;
      end;
      //
      TbFormaPag.Next;
    end;
    {
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'carteiras', False, [
    'Nome', 'Banco', 'ForneceI'], ['Codigo', 'Tipo'], [
    'A definir', 0, -11], [-1, 1], True);
    }
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaForneced();
const
  Prefixo  = 'F';
  Fornece2 = 'V';
var
  EUF, PUF, Codigo, CodUsu, Tipo: Integer;
  RazaoSocial, Fantasia, CNPJ, IE, ERua, ENumero, ECompl, EBairro, ECidade,
  ECEP, ETe1, EFax, ECel, EEmail,
  Nome, Apelido, CPF, RG, PRua, PNumero, PCompl, PBairro, PCidade,
  PCEP, PTe1, PFAX, PCel, PEmail,
  Contato, Observacoes: String;
  //
  Prazo, Transporta, Primeira, Ultima, NF, URL,
  Antigo, Ant, Classe: String;
  Valor: Double;
  Situacao, Transporta_: Integer;
  SQLType: TSQLType;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 0;
    DBGrid1.DataSource := DsForneced;
    TbForneced.Close;
    TbForneced.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbForneced . O p e n ;
    TbForneced.First;
    PB1.Position := 0;
    PB1.Max := TbForneced.RecordCount;
    Antigo := '';
    while not TbForneced.Eof do
    begin
      AtualizaTempo();
      Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando fornecedor ' +
        TbFornecedCODIGO.Value);
      //
      Continua := True;
      Antigo        := Geral.SoNumero_TT(TbFornecedCODIGO.Value);
(*mais r�pido*)
      if Ant = Antigo then
      begin
        Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de fornecedor ' +
        Ant + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        SQLType := stUpd;
      end else SQLType := stIns;

(*Muito lento
      if LocalizaAntigo(Prefixo, Antigo) then
      begin
        Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de fornecedor ' +
        Antigo + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        SQLType := stUpd;
      end else SQLType := stIns;
*)
      if Continua then
      begin
        Codigo := Geral.IMV(Trim(Geral.SoNumero_TT(TbFornecedCODIGO.Value))) +
        FIniForne - 1;
        //
        ERua          := '';
        ENumero       := '';
        ECompl        := '';
        EBairro       := '';
        ECidade       := '';
        EUF           := 0;
        ECEP          := '';
        ETe1          := '';
        EFax          := '';
        ECel          := '';
        EEmail        := '';
        //
        PRua          := '';
        PNumero       := '';
        PCompl        := '';
        PBairro       := '';
        PCidade       := '';
        PUF           := 0;
        PCEP          := '';
        PTe1          := '';
        PFax          := '';
        PCel          := '';
        PEmail        := '';
        Nome          := '';
        Apelido       := '';
        CPF           := '';
        RG            := '';
        PRua          := '';
        RazaoSocial   := '';
        Fantasia      := '';
        CNPJ          := '';
        IE            := '';
        ERua          := '';
        //
        if TbFornecedTIPO.Value = 1 then
        begin
          Tipo          := 0;
          RazaoSocial   := TbFornecedRAZO.Value;
          Fantasia      := TbFornecedNOME.Value;
          CNPJ          := Geral.SoNumero_TT(TbFornecedCNPJ.Value);
          IE            := Geral.SoNumero_TT(TbFornecedICMS.Value);
          ERua          := TbFornecedENDEREO.Value;
          ENumero       := TbFornecedNUMEND.Value;
          ECompl        := TbFornecedCOMPEND.Value;
          EBairro       := TbFornecedBAIRRO.Value;
          ECidade       := TbFornecedCIDADE.Value;
          EUF           := Geral.GetCodigoUF_da_SiglaUF(TbFornecedUF.Value);
          ECEP          := TbFornecedCEP.Value;
          ETe1          := Geral.SoNumero_TT(TbFornecedFONE.Value);
          EFax          := Geral.SoNumero_TT(TbFornecedFAX.Value);
          ECel          := Geral.SoNumero_TT(TbFornecedCELULAR.Value);
          EEmail        := TbFornecedEMAIL.Value;
          PTe1          := Geral.SoNumero_TT(TbFornecedFONECONT.Value);
          PCel          := Geral.SoNumero_TT(TbFornecedCELCONT.Value);
          PEmail        := TbFornecedEMAILCONT.Value;
          //
        end else
        begin
          Tipo          := 1;
          //
          Nome          := TbFornecedRAZO.Value;
          Apelido       := TbFornecedNOME.Value;
          CPF           := Geral.SoNumero_TT(TbFornecedCPF.Value);
          RG            := Geral.SoNumero_TT(TbFornecedICMS.Value);
          PRua          := TbFornecedENDEREO.Value;
          PNumero       := TbFornecedNUMEND.Value;
          PCompl        := TbFornecedCOMPEND.Value;
          PBairro       := TbFornecedBAIRRO.Value;
          PCidade       := TbFornecedCIDADE.Value;
          PUF           := Geral.GetCodigoUF_da_SiglaUF(TbFornecedUF.Value);
          PCEP          := TbFornecedCEP.Value;
          PTe1          := Geral.SoNumero_TT(TbFornecedFONE.Value);
          PFax          := Geral.SoNumero_TT(TbFornecedFAX.Value);
          PCel          := Geral.SoNumero_TT(TbFornecedCELULAR.Value);
          PEmail        := TbFornecedEMAIL.Value;
          ETe1          := Geral.SoNumero_TT(TbFornecedFONECONT.Value);
          ECel          := Geral.SoNumero_TT(TbFornecedCELCONT.Value);
          EEmail        := TbFornecedEMAILCONT.Value;
        end;
        URL           := TbFornecedURL.Value;
        Classe        := TbFornecedClasse.Value;
        Prazo         := TbFornecedPRAZO.Value;
        Transporta    := TbFornecedTRANSPORTA.Value;
        Contato       := TbFornecedCONTATO.Value;
        Primeira      := Geral.FDT(TbFornecedPrimeira.Value, 1);
        Ultima        := Geral.FDT(TbFornecedUltima.Value, 1);
        Valor         := TbFornecedValor.Value;
        NF            := TbFornecedNF.Value;
        Situacao      := TbFornecedSITUAO.Value;
        Observacoes   := TbFornecedOBS.Value;
        //
        Transporta_     := Geral.IMV(Trim(Transporta));
        if Transporta_ > 0 then
        begin
          Transporta_ := Transporta_ + FIniTrans - 1;
          InsereEntiTrnasp(Codigo, Transporta_);
        end;
        //
        CodUsu := Codigo;

        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entidades', False, [
        'RazaoSocial', 'Fantasia',
        'CNPJ', 'IE',
        'Nome', 'Apelido', 'CPF',
        'RG',
        'ERua', 'ENumero', 'ECompl',
        'EBairro', 'ECidade', 'EUF',
        'ECEP', 'ETe1',
        'ECel',
        'EFax', 'EEmail',
        'PRua',
        'PNumero', 'PCompl', 'PBairro',
        'PCidade', 'PUF', 'PCEP',
        'PTe1',
        'PCel', 'PFax',
        'PEmail',
        'Fornece2',
        'Observacoes',
        'Tipo', 'CodUsu',
        'URL', 'Antigo'
        ], [
        'Codigo'], [
        RazaoSocial, Fantasia,
        CNPJ, IE,
        Nome, Apelido, CPF,
        RG,
        ERua, ENumero, ECompl,
        EBairro, ECidade, EUF,
        ECEP, ETe1,
        ECel,
        EFax, EEmail,
        PRua,
        PNumero, PCompl, PBairro,
        PCidade, PUF, PCEP,
        PTe1,
        PCel, PFax,
        PEmail,
        Fornece2,
        Observacoes,
        Tipo, CodUsu,
        URL, Prefixo + Antigo
        ], [
        Codigo], True) then
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entifornec', False, [
        'Prazo', 'CompraPrima', 'CompraUltim',
        'CompraValor', 'CompraNF',
        'Situacao', 'Classe'], [
        'Codigo'], [
        Prazo, Primeira, Ultima,
        Valor, NF,
        Situacao, Classe], [
        Codigo], True);
      end;
      //
      TbForneced.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaTipoGrup();
const
  Prefixo = '';
  Nivel3 = 0;
var
  Nivel2, CodUsu: Integer;
  //
  Nome, Antigo: String;
  SQLType: TSQLType;
  Continua: Boolean;
  //
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsTipoGrup;
    TbTipoGrup.Close;
    TbTipoGrup.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbTipoGrup . O p e n ;
    TbTipoGrup.First;
    PB1.Position := 0;
    PB1.Max := TbTipoGrup.RecordCount;
    Antigo := '';
    while not TbTipoGrup.Eof do
    begin
      AtualizaTempo();
      //Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando TipoGrup ' +
        TbTipoGrupCODIGO.Value);
      //
      Continua := True;
      Antigo        := Geral.SoNumero_TT(TbTipoGrupCODIGO.Value);
      if LocalizaAntigoInt('gragru2', 'Nivel2', Geral.IMV(Trim(Antigo))) then
      begin
        Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de TipoGrup ' +
        Antigo + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        SQLType := stUpd;
      end else SQLType := stIns;
      if Continua then
      begin
        Nivel2 := Geral.IMV(Trim(Geral.SoNumero_TT(TbTipoGrupCODIGO.Value))) +
        FIniTGrup - 1;
        //
        Nome := TbTipoGrupDESCRIO.Value;
        //
        CodUsu := Nivel2;

        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru2', False, [
        'Nivel3', 'CodUsu', 'Nome'], ['Nivel2'
        ], [Nivel3, CodUsu, Nome], [Nivel2], True) then ;
      end;
      //
      TbTipoGrup.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaPagar(CliInt: Integer; Dir: String);
const
  OrdemLista = 999;
  Prefixo = '';
  CtrlaSdo = 0;
  //
  Tipo = 0;
  Sub = 0;
var
  Continua: Boolean;
  //
  NF_TXT, SerieCH, Pedido_TXT, Doc2: String;
  //
  //
  Data, Vencimento, Descricao, Compensado, Duplicata: String;
  Carteira, Genero, NotaFiscal, Sit, Controle, Cartao, Linha, Fornecedor,
  Banco, FatID, FatID_Sub: Integer;
  Debito, MoraDia, Pago, MultaVal, MoraVal: Double;
begin
  Screen.Cursor := crHourGlass;
  try
  if CliInt = -11 then
  begin
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _pagar;                                      ' + sLineBreak +
    'CREATE TABLE _PAGAR (                                             ' + sLineBreak +
    '  Empresa              integer                                   ,' + sLineBreak +
    '  DOCUMENTO            Varchar(8)                                ,' + sLineBreak +
    '  DATA                 Date                                      ,' + sLineBreak +
    '  CLIENTE              Varchar(6)                                ,' + sLineBreak +
    '  NOTAFISCAL           Varchar(6)                                ,' + sLineBreak +
    '  VENCIMENTO           Date                                      ,' + sLineBreak +
    '  VALOR                Float                                     ,' + sLineBreak +
    '  PAGAMENTO            Date                                      ,' + sLineBreak +
    '  VALORPGTO            Float                                     ,' + sLineBreak +
    '  MULTA                Float                                     ,' + sLineBreak +
    '  JUROS                Float                                     ,' + sLineBreak +
    '  MORA                 Integer                                   ,' + sLineBreak +
    '  FORMAPGTO            Varchar(2)                                ,' + sLineBreak +
    '  CENTCUST             Varchar(6)                                ,' + sLineBreak +
    '  ITEMCUST             Varchar(6)                                ,' + sLineBreak +
    '  PEDIDO               Varchar(6)                                ,' + sLineBreak +
    '  BANCO                Varchar(3)                                ,' + sLineBreak +
    '  CAIXA                Integer                                   ,' + sLineBreak +
    '  HISTORICO            Varchar(20)                               ,' + sLineBreak +
    '  OBS                  Text                                      ,' + sLineBreak +
    '  DESCONTO             Integer                                    ' + sLineBreak +
    ') TYPE=MyISAM;                                                    ');
  end;
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPagar;
    TbPagar.Close;
    TbPagar.DBFFileName := Dir + '\' + TbTabelasNome.Value;
    TbPagar . O p e n ;
    TbPagar.First;
    PB1.Position := 0;
    PB1.Max := TbPagar.RecordCount;
    UFinanceiro.LancamentoDefaultVARS;
    while not TbPagar.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando Pagar ' +
        TbPagarDOCUMENTO.Value);
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        //
if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_pagar', False, ['Empresa',
'DOCUMENTO', 'DATA', 'CLIENTE',
'NOTAFISCAL', 'VENCIMENTO', 'VALOR',
'PAGAMENTO', 'VALORPGTO', 'MULTA',
'JUROS', 'MORA', 'FORMAPGTO',
'CENTCUST', 'ITEMCUST', 'PEDIDO',
'BANCO', 'OBS', 'HISTORICO',
'CAIXA'], [
], [CliInt,
TbPagarDOCUMENTO.Value, TbPagarDATA.Value, TbPagarCLIENTE.Value,
TbPagarNOTAFISCAL.Value, TbPagarVENCIMENTO.Value, TbPagarVALOR.Value,
TbPagarPAGAMENTO.Value, TbPagarVALORPGTO.Value, TbPagarMULTA.Value,
TbPagarJUROS.Value, TbPagarMORA.Value, TbPagarFORMAPGTO.Value,
TbPagarCENTCUST.Value, TbPagarITEMCUST.Value, TbPagarPEDIDO.Value,
TbPagarBANCO.Value, TbPagarOBS.Value, TbPagarHISTORICO.Value,
TbPagarCAIXA.Value], [], False) then ;
  //
  NF_TXT := Trim(Geral.SoNumero_TT(TbPagarNOTAFISCAL.Value));
  if NF_TXT <> Trim(TbPagarNOTAFISCAL.Value) then
  begin
    Memo1.Lines.Add('Pagar: NF inv�lida > ' + TbPagarNOTAFISCAL.Value);
    NF_TXT  := '0';
    SerieCH := NF_TXT;
  end else begin
    SerieCH := '';
  end;
  //
  PEDIDO_TXT := Trim(Geral.SoNumero_TT(TbPagarPEDIDO.Value));
  if PEDIDO_TXT <> Trim(TbPagarPEDIDO.Value) then
  begin
    Memo1.Lines.Add('Pagar: Pedido inv�lido > ' + TbPagarPEDIDO.Value);
    PEDIDO_TXT  := '0';
    Doc2 := PEDIDO_TXT;
  end else begin
    Doc2 := '';
  end;
  //

{
  FLAN_Data          := Geral.FDT(TbPagarDATA.Value, 1);
  FLAN_Vencimento    := Geral.FDT(TbPagarVENCIMENTO.Value, 1);
  //FLAN_DataCad       := Geral.FDT(Date, 1);
  FLAN_Mez           := '0';
  FLAN_Descricao     := TbPagarOBS.Value;
  FLAN_Compensado    := Geral.FDT(TbPagarPAGAMENTO.Value, 1);
  FLAN_Duplicata     := TbPagarDOCUMENTO.Value;
  FLAN_Doc2          := Doc2;
  FLAN_SerieCH       := SerieCH;

  FLAN_Documento     := 0;
  FLAN_Tipo          := 0;
  FLAN_Carteira      := Geral.IMV(Trim(TbPagarFORMAPGTO.Value));
  FLAN_Credito       := 0;
  FLAN_Debito        := TbPagarVALOR.Value;
  FLAN_Genero        := Geral.IMV(Trim(TbPagarITEMCUST.Value));
  FLAN_SerieNF       := '';
  FLAN_NotaFiscal    := Geral.IMV(Trim(NF_TXT));
  FLAN_Sit           := dmkPF.EscolhaDe2Int(TbPagarPAGAMENTO.Value > 0, 2, 0);;
  FLAN_Controle      := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                        VAR_LCT, VAR_LCT, 'Controle');
  FLAN_Sub           := 0;
  FLAN_ID_Pgto       := 0;
  FLAN_Cartao        := Geral.IMV(Trim(TbPagarCENTCUST.Value));
  FLAN_Linha         := Geral.IMV(Trim(TbPagarITEMCUST.Value));
  FLAN_Fornecedor    := Geral.IMV(Trim(TbPagarCLIENTE.Value));
  FLAN_Cliente       := 0;
  FLAN_MoraDia       := TbPagarMORA.Value; // Tudo zerado
  FLAN_Multa         := 0;
  FLAN_UserCad       := VAR_USUARIO;
  FLAN_DataDoc       := Geral.FDT(Date, 1);
  FLAN_Vendedor      := 0;
  FLAN_Account       := 0;
  FLAN_ICMS_P        := 0;
  FLAN_ICMS_V        := 0;
  FLAN_CliInt        := VAR_CliIntUnico;
  FLAN_Depto         := 0;
  FLAN_DescoPor      := 0;
  FLAN_ForneceI      := 0;
  FLAN_DescoVal      := 0;
  FLAN_NFVal         := 0;
  FLAN_Unidade       := 0;
  FLAN_Qtde          := 0;
  FLAN_FatID         := VAR_FATID_1015;
  FLAN_FatID_Sub     := Geral.IMV(PEDIDO_TXT);
  FLAN_FatNum        := 0;
  FLAN_FatParcela    := 0;
  FLAN_Pago          := TbPagarVALORPGTO.Value;
  //
  FLAN_MultaVal      := TbPagarMULTA.Value;
  FLAN_MoraVal       := TbPagarJUROS.Value;
  FLAN_CtrlIni       := 0;
  FLAN_Nivel         := 0;
  FLAN_CNAB_Sit      := 0;
  FLAN_TipoCH        := 0;
  FLAN_Atrelado      := 0;
  FLAN_SubPgto1      := 0;
  FLAN_MultiPgto     := 0;
  FLAN_Protocolo     := 0;
  //
  FLAN_Emitente      := '';
  FLAN_CNPJCPF       := '';
  FLAN_Banco         := Geral.IMV(Trim(TbPagarBANCO.Value)); // Parei Aqui! Ver o que fazer!
  FLAN_Agencia       := '';
  FLAN_ContaCorrente := '';
  //

  //TbPagarCAIXA.Value; // Parei Aqui! est� tudo zerado!
  //TbPagarHISTORICO.Value; // Parei Aqui! est� tudo em branco!
  // Fazer depois! pelo cartao / Linha VerificaConta_x_SubGrupo(FLAN_Genero, TbPagarCENTCUST.Value, True);
        UFinanceiro.InsereLancamento();
}
  Data          := Geral.FDT(TbPagarDATA.Value, 1);
  Vencimento    := Geral.FDT(TbPagarVENCIMENTO.Value, 1);
  //DataCad       := Geral.FDT(Date, 1);
  Descricao     := TbPagarOBS.Value;
  Compensado    := Geral.FDT(TbPagarPAGAMENTO.Value, 1);
  Duplicata     := TbPagarDOCUMENTO.Value;

  //Documento     := 0;
  //Tipo          := 0;
  Carteira      := Geral.IMV(Trim(TbPagarFORMAPGTO.Value));
//  Credito       := 0;
  Debito        := TbPagarVALOR.Value;
  Genero        := Geral.IMV(Trim(TbPagarITEMCUST.Value));
//  SerieNF       := '';
  NotaFiscal    := Geral.IMV(Trim(NF_TXT));
  Sit           := dmkPF.EscolhaDe2Int(TbPagarPAGAMENTO.Value > 0, 2, 0);
  Controle      := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                        VAR_LCT, VAR_LCT, 'Controle');
  //Sub           := 0;
  //ID_Pgto       := 0;
  Cartao        := Geral.IMV(Trim(TbPagarCENTCUST.Value));
  Linha         := Geral.IMV(Trim(TbPagarITEMCUST.Value));
  Fornecedor    := Geral.IMV(Trim(TbPagarCLIENTE.Value));
  //Cliente       := 0;
  MoraDia       := TbPagarMORA.Value; // Tudo zerado
  //Multa         := 0;
  //UserCad       := VAR_USUARIO;
  //DataDoc       := Geral.FDT(Date, 1);
  //Vendedor      := 0;
  //Account       := 0;
  //ICMS_P        := 0;
  //ICMS_V        := 0;
  //CliInt        := VAR_CliIntUnico;
  //Depto         := 0;
  //DescoPor      := 0; //TbPagarDESCONTO.Value; // N�o tem!
  //ForneceI      := 0;
  //DescoVal      := 0;
  //NFVal         := 0;
  //Unidade       := 0;
  //Qtde          := 0;
  FatID         := VAR_FATID_1016;
  FatID_Sub     := Geral.IMV(PEDIDO_TXT);
  //FatNum        := 0;
  //FatParcela    := 0;
  Pago          := TbPagarVALORPGTO.Value;
  //
  MultaVal      := TbPagarMULTA.Value;
  MoraVal       := TbPagarJUROS.Value;
  //CtrlIni       := 0;
  //Nivel         := 0;
  //CNAB_Sit      := 0;
  //TipoCH        := 0;
  //Atrelado      := 0;
  //SubPgto1      := 0;
  //MultiPgto     := 0;
  //Protocolo     := 0;
  //
  //Emitente      := '';
  //CNPJCPF       := '';
  Banco         := Geral.IMV(Trim(TbPagarBANCO.Value)); // Parei Aqui! Ver o que fazer!
  //Agencia       := '';
  //ContaCorrente := '';
  //

if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, FTabLctA, False, [
'Genero',
'Descricao', 'NotaFiscal',
'Debito', 'Compensado',
'SerieCH', 'Sit',
'Vencimento', 'FatID', 'FatID_Sub',
'Banco',
'Cartao', 'Linha',
'Pago',
'Fornecedor', 'CliInt',
'MoraDia',
'MoraVal', 'MultaVal',
'DataDoc',
'Duplicata',
'Doc2'
], [
'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
Genero,
Descricao, NotaFiscal,
Debito, Compensado,
SerieCH, Sit,
Vencimento, FatID, FatID_Sub,
Banco,
Cartao, Linha,
Pago,
Fornecedor, CliInt,
MoraDia,
MoraVal, MultaVal,
Data,
Duplicata,
Doc2
], [
Data, Tipo, Carteira, Controle, Sub], True) then ;
      end;
      //
      TbPagar.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaPedido(CliInt: Integer; Dir: String);
var
  Continua: Boolean;
  //
  Tipo, EntSai: Byte;
  Codigo, CodUsu, Empresa, Represen, Cliente, CartEmis, Prioridade, CondicaoPG,
  Moeda, Situacao, TabelaPrc, MotivoSit, LoteProd, FretePor, Transporta,
  Redespacho, RegrFiscal, AFP_Sit, Antigo: Integer;
  DtaEmiss, DtaEntra, DtaInclu, DtaPrevi, CondPag, CentroCust, ItemCust,
  PedidoCli, Observa:  String;
  //
  Diferenca, Frete_P, Frete_V, Seguro_V, Seguro_P, DesoAces_V, DesoAces_P,
  TotalQtd, Total_Vlr, Total_Des, Total_Tot, AFP_Per, ComisFat, ComisRec,
  QuantP, ValLiq: Double;
  //
  SQLType: TSQLType;
begin
  Screen.Cursor := crHourGlass;
  try
    if CliInt = -11 then
    begin
      LimparDadosTabela(
      'DROP TABLE IF EXISTS _Pedido;                                    ' + sLineBreak +
      'CREATE TABLE _Pedido (                                           ' + sLineBreak +
      '  Empresa              integer                                   ,' + sLineBreak +
      '  NUMERO               Varchar(6)                                ,' + sLineBreak +
      '  DATA                 Date                                      ,' + sLineBreak +
      '  TIPO                 Integer                                   ,' + sLineBreak +
      '  ENTSAI               Integer                                   ,' + sLineBreak +
      '  CLIENTE              Varchar(6)                                ,' + sLineBreak +
      '  CONDPAG              Varchar(47)                               ,' + sLineBreak +
      '  CENTROCUST           Varchar(6)                                ,' + sLineBreak +
      '  ITEMCUST             Varchar(6)                                ,' + sLineBreak +
      '  FORMAPGTO            Varchar(2)                                ,' + sLineBreak +
      '  VALOR                Float                                     ,' + sLineBreak +
      '  NUMNOTA              Varchar(6)                                ,' + sLineBreak +
      '  VLRNOTA              Float                                     ,' + sLineBreak +
      '  VLRICMS              Float                                     ,' + sLineBreak +
      '  BANCO                Varchar(3)                                ,' + sLineBreak +
      '  CP                   Integer                                   ,' + sLineBreak +
      '  COMISSAO             Varchar(2)                                ,' + sLineBreak +
      '  OBS                  Text                                      ,' + sLineBreak +
      '  OS                   Varchar(5)                                ,' + sLineBreak +
      '  AGRUPAR              Integer                                   ,' + sLineBreak +
      '  INSCOM               Integer                                   ,' + sLineBreak +
      '  VENDEDOR             Varchar(3)                                ,' + sLineBreak +
      '  J07_UKEY             Varchar(60)                               ' + sLineBreak +
      ') TYPE=MyISAM');
    end;
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbPedido.Close;
    TbPedido.DBFFileName := Dir + '\' + TbTabelasNome.Value;
    TbPedido . O p e n ;
    TbPedido.First;
    PB1.Position := 0;
    PB1.Max := TbPedido.RecordCount;
    Antigo  := -1;
    while not TbPedido.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando Pedido ' +
        TbPedidoNUMERO.Value);
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_pedido', False, ['Empresa',
        'NUMERO', 'DATA', 'TIPO',
        'ENTSAI', 'CLIENTE', 'CONDPAG',
        'CENTROCUST', 'ITEMCUST', 'FORMAPGTO',
        'VALOR', 'NUMNOTA', 'VLRNOTA',
        'VLRICMS', 'BANCO', 'CP',
        'COMISSAO', 'OBS', 'OS',
        'AGRUPAR', 'INSCOM', 'VENDEDOR',
        'J07_UKEY'], [
        ], [CliInt,
        TbPedidoNUMERO.Value, TbPedidoDATA.Value, TbPedidoTIPO.Value,
        TbPedidoENTSAI.Value, TbPedidoCLIENTE.Value, TbPedidoCONDPAG.Value,
        TbPedidoCENTROCUST.Value, TbPedidoITEMCUST.Value, TbPedidoFORMAPGTO.Value,
        TbPedidoVALOR.Value, TbPedidoNUMNOTA.Value, TbPedidoVLRNOTA.Value,
        TbPedidoVLRICMS.Value, TbPedidoBANCO.Value, TbPedidoCP.Value,
        TbPedidoCOMISSAO.Value, TbPedidoOBS.Value, TbPedidoOS.Value,
        TbPedidoAGRUPAR.Value, TbPedidoINSCOM.Value, TbPedidoVENDEDOR.Value,
        TbPedidoJ07_UKEY.Value], [
        ], False) then ;

        //

        Codigo       := Geral.IMV(TbPedidoNUMERO.Value);
        CodUsu       := Codigo;
        //Empresa      := -11;
        DtaEmiss     := Geral.FDT(TbPedidoDATA.Value, 1);
        DtaEntra     := DtaEmiss;
        DtaInclu     := DtaEmiss;
        DtaPrevi     := DtaEmiss;
        Represen     := BuscaEntidadeAntiga('V', TbPedidoVENDEDOR.Value);
        Prioridade   := 0;
        CondicaoPG   := 0;// CondPag
        Tipo         := TbPedidoTIPO.Value;
        EntSai       := TbPedidoENTSAI.Value;
        if EntSai = 1 then // Saida
        begin
          EntSai     := 1; // sa�da como na NFe
          Empresa    := CliInt;
          Cliente    := BuscaEntidadeAntiga('C', TbPedidoCLIENTE.Value);
        end else begin
          EntSai     := 0; // entrada como na NFe
          Empresa    := BuscaEntidadeAntiga('F', TbPedidoCLIENTE.Value);
          Cliente    := CliInt;
        end;
        CondPag      := TbPedidoCONDPAG.Value;
        ItemCust     := TbPedidoITEMCUST.Value;
        CartEmis     := Geral.IMV(TbPedidoFORMAPGTO.Value);
        Diferenca    := TbPedidoVLRNOTA.Value - TbPedidoVALOR.Value;
        if Diferenca > 0 then
        begin
          Frete_V      := Diferenca;
          Total_Des    := 0;
        end else begin
          Frete_V      := 0.00;
          Total_Des    := Diferenca;
        end;
  {
        TbPedidoNUMNOTA.Value;
        TbPedidoVLRICMS.Value;
        TbPedidoBANCO.Value;
        TbPedidoCP.Value;
        TbPedidoCOMISSAO.Value;
        TbPedidoOS.Value;
        TbPedidoINSCOM.Value;
        TbPedidoJ07_UKEY.Value;
        TbPedidoCENTROCUST.Value;
  }
        Moeda        := 1;
        Situacao     := 2;
        TabelaPrc    := 0;
        MotivoSit    := 0;
        LoteProd     := 0;
        PedidoCli    := '';
        FretePor     := -1;
        Transporta   := 0;
        Redespacho   := 0;
        RegrFiscal   := 0;
        DesoAces_V   := 0.00;
        DesoAces_P   := 0.00;
        Frete_P      := 0.00;
        Seguro_V     := 0.00;
        Seguro_P     := 0.00;
        TotalQtd     := 0;
        Total_Vlr    := TbPedidoVALOR.Value;
        Total_Tot    := TbPedidoVLRNOTA.Value;
        Observa      := TbPedidoOBS.Value;
        AFP_Sit      := TbPedidoAGRUPAR.Value;
        AFP_Per      := 0.00;
        ComisFat     := 0.00;
        ComisRec     := 0.00;
        QuantP       := 0;
        ValLiq       := TbPedidoVALOR.Value;
        //
        if Antigo <> Codigo then
          SQLType := stIns
        else
          SQLType := stUpd;
        Antigo := Codigo;
        try
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pedivda', False, [
        'CodUsu', 'Empresa', 'Cliente',
        'DtaEmiss', 'DtaEntra', 'DtaInclu',
        'DtaPrevi', 'Represen', 'Prioridade',
        'CondicaoPG', 'Moeda', 'Situacao',
        'TabelaPrc', 'MotivoSit', 'LoteProd',
        'PedidoCli', 'FretePor', 'Transporta',
        'Redespacho', 'RegrFiscal', 'DesoAces_V',
        'DesoAces_P', 'Frete_V', 'Frete_P',
        'Seguro_V', 'Seguro_P', 'TotalQtd',
        'Total_Vlr', 'Total_Des', 'Total_Tot',
        'Observa', 'AFP_Sit', 'AFP_Per',
        'ComisFat', 'ComisRec', 'CartEmis',
        'QuantP', 'ValLiq', 'Tipo',
        'EntSai'], [
        'Codigo'], [
        CodUsu, Empresa, Cliente,
        DtaEmiss, DtaEntra, DtaInclu,
        DtaPrevi, Represen, Prioridade,
        CondicaoPG, Moeda, Situacao,
        TabelaPrc, MotivoSit, LoteProd,
        PedidoCli, FretePor, Transporta,
        Redespacho, RegrFiscal, DesoAces_V,
        DesoAces_P, Frete_V, Frete_P,
        Seguro_V, Seguro_P, TotalQtd,
        Total_Vlr, Total_Des, Total_Tot,
        Observa, AFP_Sit, AFP_Per,
        ComisFat, ComisRec, CartEmis,
        QuantP, ValLiq, Tipo,
        EntSai], [
        Codigo], True, '', (*InfoSQLOnError*)False, (*InfoErro*)True) then ;
              //
        except
        ;
        end;
      end;
      //
      TbPedido.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaProdutos();
const
  CST_A = '0';
  CST_B = '0';
  Prefixo = '';
  Nivel3 = 0;
  GraTamCad = 1;
  Peso = 0.000;
  PIS_AlqP = 0.00;
  COFINS_AlqP = 0.00;
  GraCorCad = 0;
  GraTamI = 1;
  GraCusPrc = 5; // Lista de Pre�os (pre�o m�dio)
var
  Nivel2, Nivel1, CodUsu, PrdGrupTip, Fabricante, UnidMed: Integer;
  //
  Nome, Antigo, Referencia, NCM: String;
  SQLType: TSQLType;
  Continua: Boolean;
  //
  NCMS: array of String;
  IPI_Alq: Double;
  //
  Controle, GraGruC, GraGru1, GraGruX, Entidade: Integer;
  CustoPreco: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    Entidade := -11;
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsProdutos;
    TbProdutos.Close;
    TbProdutos.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbProdutos . O p e n ;
    TbProdutos.First;
    PB1.Position := 0;
    PB1.Max := TbProdutos.RecordCount;
    Antigo := '';
    QrClasFisc.Close;
    QrClasFisc . O p e n ;
    if QrClasFisc.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Importa��o de produtos abortada!' + sLineBreak +
      '� necess�rio importar primeiro a tabela "ClasFisc.DBF"');
      Screen.Cursor := crDefault;
      Exit;
    end;
    QrClasFisc.Last;
    SetLength(NCMs, QrClasFiscCodigo.Value + 1);
    QrClasFisc.First;
    NCMs[0] := '';
    while not QrClasFisc.Eof do
    begin
      NCMs[QrClasFiscCodigo.Value] := QrClasFiscNCM.Value;
      //
      QrClasFisc.Next;
    end;
    while not TbProdutos.Eof do
    begin
      AtualizaTempo();
      //Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando Produtos ' +
        TbProdutosCODIGO.Value);
      //
      Continua := True;
      Antigo        := Geral.SoNumero_TT(TbProdutosCODIGO.Value);
      if LocalizaAntigoInt('gragru1', 'Nivel1', Geral.IMV(Trim(Antigo))) then
      begin
        case Geral.IMV(Antigo) of
          //0   : Continua := False;
          824 : Continua := False;
          //1593: Continua := True;
          else
          Continua := Geral.MB_Pergunta(
          'Foi localizado um segundo c�digo para o cadastro de Produtos ' +
          Antigo + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        end;
        SQLType := stUpd;
      end else SQLType := stIns;
      if Continua then
      begin
        Nivel1 := Geral.IMV(Trim(Geral.SoNumero_TT(TbProdutosCODIGO.Value))) + FIniProds - 1;
        Nivel2 := Geral.IMV(Trim(Geral.SoNumero_TT(TbProdutosTipo.Value))) + FIniTGrup - 1;
        //
        PrdGrupTip := Geral.IMV(Trim(Geral.SoNumero_TT(TbProdutosGrupo.Value)));
        Referencia := TbProdutosCodPesq.Value;
        Nome       := TbProdutosDESCRIO.Value;
        Fabricante := Geral.IMV(Trim(Geral.SoNumero_TT(TbProdutosFabricante.Value)));
        UnidMed    := TbProdutosMedida.Value;
        NCM := NCMs[TbProdutosClasFisc.Value];
        IPI_Alq    := TbProdutosIPI.Value;
        CustoPreco := TbProdutosVlrUni.Value;
{
        TbProdutosQtEstoque.Value,
        TbProdutosQtPeas.Value,
        TbProdutosIPI.Value,
        TbProdutosICMS.Value,
        TbProdutosICMSLC.Value,
        TbProdutosCodTrib.Value,
        TbProdutosEstoqMin.Value,
        TbProdutosOrigem.Value,
        TbProdutosLocal.Value,
        TbProdutosDataEnt.Value,
        TbProdutosCoeficient.Value,
        TbProdutosD04UKEY.Value
}
        //
        CodUsu := Nivel1;

        {  teste
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, '_produtos', False, [
        'Codigo', 'CodPesq', 'Descricao',
        'Fabricante', 'Grupo', 'Tipo',
        'Medida', 'VlrUni', 'QtEstoque',
        'QtPeas', 'IPI', 'ICMS',
        'ICMSLC', 'ClasFisc', 'CodTrib',
        'EstoqMin', 'Origem', 'Local',
        'DataEnt', 'Coeficient', 'D04UKEY'], [
        ], [
        TbProdutosCodigo.Value, TbProdutosCodPesq.Value, TbProdutosDESCRIO.Value,
        TbProdutosFabricante.Value, TbProdutosGrupo.Value, TbProdutosTipo.Value,
        TbProdutosMedida.Value, TbProdutosVlrUni.Value, TbProdutosQtEstoque.Value,
        TbProdutosQtPeas.Value, TbProdutosIPI.Value, TbProdutosICMS.Value,
        TbProdutosICMSLC.Value, TbProdutosClasFisc.Value, TbProdutosCodTrib.Value,
        TbProdutosEstoqMin.Value, TbProdutosOrigem.Value, TbProdutosLocal.Value,
        TbProdutosDataEnt.Value, TbProdutosCoeficient.Value, TbProdutosD04UKEY.Value], [
        ], False) then ;
        }
        //
        //   GRAGRU1
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru1', False, [
        'Nivel3', 'Nivel2', 'CodUsu',
        'Nome', 'PrdGrupTip', 'GraTamCad',
        'UnidMed', 'CST_A', 'CST_B',
        'NCM', 'Peso', 'IPI_Alq',
        (*'IPI_CST', 'IPI_cEnq', 'IPI_vUnid',
        'IPI_TpTrib', 'TipDimens', 'PerCuztMin',
        'PerCuztMax', 'MedOrdem', 'PartePrinc',
        'InfAdProd', 'SiglaCustm', 'HowBxaEstq',
        'GerBxaEstq', 'PIS_CST',*) 'PIS_AlqP',
        (*'PIS_AlqV', 'PISST_AlqP', 'PISST_AlqV',
        'COFINS_CST',*) 'COFINS_AlqP', (*'COFINS_AlqV',
        'COFINSST_AlqP', 'COFINSST_AlqV', 'ICMS_modBC',
        'ICMS_modBCST', 'ICMS_pRedBC', 'ICMS_pRedBCST',
        'ICMS_pMVAST', 'ICMS_pICMSST', 'ICMS_Pauta',
        'ICMS_MaxTab', 'IPI_pIPI', 'cGTIN_EAN',
        'EX_TIPI', 'PIS_pRedBC', 'PISST_pRedBCST',
        'COFINS_pRedBC', 'COFINSST_pRedBCST', 'ICMSRec_pRedBC',
        'IPIRec_pRedBC', 'PISRec_pRedBC', 'COFINSRec_pRedBC',
        'ICMSRec_pAliq', 'IPIRec_pAliq', 'PISRec_pAliq',
        'COFINSRec_pAliq', 'ICMSRec_tCalc', 'IPIRec_tCalc',
        'PISRec_tCalc', 'COFINSRec_tCalc', 'ICMSAliqSINTEGRA',
        'ICMSST_BaseSINTEGRA', 'FatorClas', 'prod_indTot',*)
        'Referencia', 'Fabricante'], [
        'Nivel1'], [
        Nivel3, Nivel2, CodUsu,
        Nome, PrdGrupTip, GraTamCad,
        UnidMed, CST_A, CST_B,
        NCM, Peso, IPI_Alq,
        (*IPI_CST, IPI_cEnq, IPI_vUnid,
        IPI_TpTrib, TipDimens, PerCuztMin,
        PerCuztMax, MedOrdem, PartePrinc,
        InfAdProd, SiglaCustm, HowBxaEstq,
        GerBxaEstq, PIS_CST,*) PIS_AlqP,
        (*PIS_AlqV, PISST_AlqP, PISST_AlqV,
        COFINS_CST,*) COFINS_AlqP, (*COFINS_AlqV,
        COFINSST_AlqP, COFINSST_AlqV, ICMS_modBC,
        ICMS_modBCST, ICMS_pRedBC, ICMS_pRedBCST,
        ICMS_pMVAST, ICMS_pICMSST, ICMS_Pauta,
        ICMS_MaxTab, IPI_pIPI, cGTIN_EAN,
        EX_TIPI, PIS_pRedBC, PISST_pRedBCST,
        COFINS_pRedBC, COFINSST_pRedBCST, ICMSRec_pRedBC,
        IPIRec_pRedBC, PISRec_pRedBC, COFINSRec_pRedBC,
        ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq,
        COFINSRec_pAliq, ICMSRec_tCalc, IPIRec_tCalc,
        PISRec_tCalc, COFINSRec_tCalc, ICMSAliqSINTEGRA,
        ICMSST_BaseSINTEGRA, FatorClas, prod_indTot,*)
        Referencia, Fabricante], [
        Nivel1], True) then ;
        //
        ///  GRAGRUC
        Controle  := Nivel1;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruc', False, [
        'Nivel1', 'GraCorCad'], ['Controle'], [
        Nivel1, GraCorCad], [Controle], True) then ;
        //
        //   GRAGRUX
        GraGruC := Nivel1;
        GraGru1 := Nivel1;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragrux', False, [
        'GraGruC', 'GraGru1', 'GraTamI'], ['Controle'], [
        GraGruC, GraGru1, GraTamI], [Controle], True) then ;
        //
        //   GRAGRUVAL = 5
        GraGruX := Controle;
        if CustoPreco >= 0.000001 then
        begin
          if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
          'GraGruX', 'GraGru1', 'GraCusPrc', 'CustoPreco', 'Entidade'], [
          'Controle'], [GraGruX, GraGru1, GraCusPrc, CustoPreco, Entidade], [
          Controle], True) then ;
        end;
        //
      end;
      //
      TbProdutos.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR10();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R10;                                     ' + sLineBreak +
    'CREATE TABLE _R10 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  CNPJ                 Varchar(14)                            ,' + sLineBreak +
    '  INSCEST              Varchar(14)                            ,' + sLineBreak +
    '  NCONTRIB             Varchar(35)                            ,' + sLineBreak +
    '  MUNICIPIO            Varchar(30)                            ,' + sLineBreak +
    '  UF                   Varchar(2)                             ,' + sLineBreak +
    '  FAX                  Varchar(10)                            ,' + sLineBreak +
    '  DATAINI              Date                                   ,' + sLineBreak +
    '  DATAFIN              Date                                   ,' + sLineBreak +
    '  CODCONV              Varchar(1)                             ,' + sLineBreak +
    '  CODNAT               Varchar(1)                             ,' + sLineBreak +
    '  CODFIN               Varchar(1)                              ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR10.Close;
    TbR10.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR10 . O p e n ;
    TbR10.First;
    PB1.Position := 0;
    PB1.Max := TbR10.RecordCount;
    //Antigo  := -1;
    while not TbR10.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R10 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_r10', False, [
        'TIPO', 'CNPJ', 'INSCEST',
        'NCONTRIB', 'MUNICIPIO', 'UF',
        'FAX', 'DATAINI', 'DATAFIN',
        'CODCONV', 'CODNAT', 'CODFIN'], [
        ], [
        TbR10TIPO.Value, TbR10CNPJ.Value, TbR10INSCEST.Value,
        TbR10NCONTRIB.Value, TbR10MUNICIPIO.Value, TbR10UF.Value,
        TbR10FAX.Value, TbR10DATAINI.Value, TbR10DATAFIN.Value,
        TbR10CODCONV.Value, TbR10CODNAT.Value, TbR10CODFIN.Value], [
        ], False) then ;
      end;
      //
      TbR10.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR11();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R11;                                     ' + sLineBreak +
    'CREATE TABLE _R11 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  LOGRADOURO           Varchar(34)                            ,' + sLineBreak +
    '  NUMERO               Varchar(5)                             ,' + sLineBreak +
    '  COMPLEMENT           Varchar(22)                            ,' + sLineBreak +
    '  BAIRRO               Varchar(15)                            ,' + sLineBreak +
    '  CEP                  Varchar(8)                             ,' + sLineBreak +
    '  CONTATO              Varchar(28)                            ,' + sLineBreak +
    '  TELEFONE             Varchar(12)                             ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR11.Close;
    TbR11.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR11 . O p e n ;
    TbR11.First;
    PB1.Position := 0;
    PB1.Max := TbR11.RecordCount;
   //Antigo  := -1;
    while not TbR11.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R11 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_r11', False, [
        'TIPO', 'LOGRADOURO', 'NUMERO',
        'COMPLEMENT', 'BAIRRO', 'CEP',
        'CONTATO', 'TELEFONE'], [
        ], [
        TbR11TIPO.Value, TbR11LOGRADOURO.Value, TbR11NUMERO.Value,
        TbR11COMPLEMENT.Value, TbR11BAIRRO.Value, TbR11CEP.Value,
        TbR11CONTATO.Value, TbR11TELEFONE.Value], [
        ], False) then ;
      end;
      //
      TbR11.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR50();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R50;                                     ' + sLineBreak +
    'CREATE TABLE _R50 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  CNPJ                 Varchar(14)                            ,' + sLineBreak +
    '  INSCEST              Varchar(14)                            ,' + sLineBreak +
    '  EMISSAO              Date                                   ,' + sLineBreak +
    '  UF                   Varchar(2)                             ,' + sLineBreak +
    '  MODELO               Varchar(2)                             ,' + sLineBreak +
    '  SERIE                Varchar(3)                             ,' + sLineBreak +
    '  NUMNF                Varchar(6)                             ,' + sLineBreak +
    '  CODNATOP             Varchar(4)                             ,' + sLineBreak +
    '  EMITENTE             Varchar(1)                             ,' + sLineBreak +
    '  VALORTOT             Varchar(13)                            ,' + sLineBreak +
    '  BASEICMS             Varchar(13)                            ,' + sLineBreak +
    '  VALORICMS            Varchar(13)                            ,' + sLineBreak +
    '  VLRISENTO            Varchar(13)                            ,' + sLineBreak +
    '  OUTROS               Varchar(13)                            ,' + sLineBreak +
    '  ALIQUOTA             Varchar(4)                             ,' + sLineBreak +
    '  SITUACAO             Varchar(1)                             ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR50.Close;
    TbR50.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR50 . O p e n ;
    TbR50.First;
    PB1.Position := 0;
    PB1.Max := TbR50.RecordCount;
    //Antigo  := -1;
    while not TbR50.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R50 ' +
      Geral.FDT(TbR50EMISSAO.Value, 1));
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_R50', False, [
        'TIPO', 'CNPJ', 'INSCEST',
        'EMISSAO', 'UF', 'MODELO',
        'SERIE', 'NUMNF', 'CODNATOP',
        'EMITENTE', 'VALORTOT', 'BASEICMS',
        'VALORICMS', 'VLRISENTO', 'OUTROS',
        'ALIQUOTA', 'SITUACAO'], [
        ], [
        TbR50TIPO.Value, TbR50CNPJ.Value, TbR50INSCEST.Value,
        TbR50EMISSAO.Value, TbR50UF.Value, TbR50MODELO.Value,
        TbR50SERIE.Value, TbR50NUMNF.Value, TbR50CODNATOP.Value,
        TbR50EMITENTE.Value, TbR50VALORTOT.Value, TbR50BASEICMS.Value,
        TbR50VALORICMS.Value, TbR50VLRISENTO.Value, TbR50OUTROS.Value,
        TbR50ALIQUOTA.Value, TbR50SITUACAO.Value], [
        ], False) then ;
      end;
      //
      TbR50.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR51();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R51;                                     ' + sLineBreak +
    'CREATE TABLE _R51 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  CNPJ                 Varchar(14)                            ,' + sLineBreak +
    '  INSCEST              Varchar(14)                            ,' + sLineBreak +
    '  EMISSAO              Date                                   ,' + sLineBreak +
    '  UF                   Varchar(2)                             ,' + sLineBreak +
    '  SERIE                Varchar(3)                             ,' + sLineBreak +
    '  NUMNF                Varchar(6)                             ,' + sLineBreak +
    '  CODNATOP             Varchar(4)                             ,' + sLineBreak +
    '  TOTALNF              Varchar(13)                            ,' + sLineBreak +
    '  VLRTOTIPI            Varchar(13)                            ,' + sLineBreak +
    '  VLRISENTO            Varchar(13)                            ,' + sLineBreak +
    '  OUTROS               Varchar(13)                            ,' + sLineBreak +
    '  BRANCOS              Varchar(20)                            ,' + sLineBreak +
    '  SITUACAO             Varchar(1)                              ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR51.Close;
    TbR51.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR51 . O p e n ;
    TbR51.First;
    PB1.Position := 0;
    PB1.Max := TbR51.RecordCount;
    //Antigo  := -1;
    while not TbR51.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R51 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_R51', False, [
        'TIPO', 'CNPJ', 'INSCEST',
        'EMISSAO', 'UF', 'SERIE',
        'NUMNF', 'CODNATOP', 'TOTALNF',
        'VLRTOTIPI', 'VLRISENTO', 'OUTROS',
        'BRANCOS', 'SITUACAO'], [
        ], [
        TbR51TIPO.Value, TbR51CNPJ.Value, TbR51INSCEST.Value,
        TbR51EMISSAO.Value, TbR51UF.Value, TbR51SERIE.Value,
        TbR51NUMNF.Value, TbR51CODNATOP.Value, TbR51TOTALNF.Value,
        TbR51VLRTOTIPI.Value, TbR51VLRISENTO.Value, TbR51OUTROS.Value,
        TbR51BRANCOS.Value, TbR51SITUACAO.Value], [
        ], False) then ;
      end;
      //
      TbR51.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR53();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R53;                                     ' + sLineBreak +
    'CREATE TABLE _R53 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  CNPJ                 Varchar(14)                            ,' + sLineBreak +
    '  INSCEST              Varchar(14)                            ,' + sLineBreak +
    '  EMISSAO              Date                                   ,' + sLineBreak +
    '  UF                   Varchar(2)                             ,' + sLineBreak +
    '  MODELO               Varchar(2)                             ,' + sLineBreak +
    '  SERIE                Varchar(3)                             ,' + sLineBreak +
    '  NUMERO               Varchar(6)                             ,' + sLineBreak +
    '  CFOP                 Varchar(4)                             ,' + sLineBreak +
    '  EMITENTE             Varchar(1)                             ,' + sLineBreak +
    '  BASECALC             Varchar(13)                            ,' + sLineBreak +
    '  ICMSRETIDO           Varchar(13)                            ,' + sLineBreak +
    '  DESPACESS            Varchar(13)                            ,' + sLineBreak +
    '  SITUACAO             Varchar(1)                             ,' + sLineBreak +
    '  CODANTEC             Varchar(1)                             ,' + sLineBreak +
    '  BRANCOS              Varchar(16)                            ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR53.Close;
    TbR53.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR53 . O p e n ;
    TbR53.First;
    PB1.Position := 0;
    PB1.Max := TbR53.RecordCount;
    //Antigo  := -1;
    while not TbR53.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R53 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_R53', False, [
        'TIPO', 'CNPJ', 'INSCEST',
        'EMISSAO', 'UF', 'MODELO',
        'SERIE', 'NUMERO', 'CFOP',
        'EMITENTE', 'BASECALC', 'ICMSRETIDO',
        'DESPACESS', 'SITUACAO', 'CODANTEC',
        'BRANCOS'], [
        ], [
        TbR53TIPO.Value, TbR53CNPJ.Value, TbR53INSCEST.Value,
        TbR53EMISSAO.Value, TbR53UF.Value, TbR53MODELO.Value,
        TbR53SERIE.Value, TbR53NUMERO.Value, TbR53CFOP.Value,
        TbR53EMITENTE.Value, TbR53BASECALC.Value, TbR53ICMSRETIDO.Value,
        TbR53DESPACESS.Value, TbR53SITUACAO.Value, TbR53CODANTEC.Value,
        TbR53BRANCOS.Value], [
        ], False) then ;
      end;
      //
      TbR53.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR54();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R54;                                     ' + sLineBreak +
    'CREATE TABLE _R54 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  CNPJ                 Varchar(14)                            ,' + sLineBreak +
    '  MODELO               Varchar(2)                             ,' + sLineBreak +
    '  SERIE                Varchar(3)                             ,' + sLineBreak +
    '  NUMNF                Varchar(6)                             ,' + sLineBreak +
    '  CODNATOP             Varchar(4)                             ,' + sLineBreak +
    '  SITTRIB              Varchar(3)                             ,' + sLineBreak +
    '  NUMITEM              Varchar(3)                             ,' + sLineBreak +
    '  CODPROD              Varchar(14)                            ,' + sLineBreak +
    '  QTDADE               Varchar(11)                            ,' + sLineBreak +
    '  VLRPROD              Varchar(12)                            ,' + sLineBreak +
    '  VLRDESC              Varchar(12)                            ,' + sLineBreak +
    '  BASECALC             Varchar(12)                            ,' + sLineBreak +
    '  BASESUBTRI           Varchar(12)                            ,' + sLineBreak +
    '  VLRIPI               Varchar(12)                            ,' + sLineBreak +
    '  ALIQICMS             Varchar(4)                             ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR54.Close;
    TbR54.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR54 . O p e n ;
    TbR54.First;
    PB1.Position := 0;
    PB1.Max := TbR54.RecordCount;
    //Antigo  := -1;
    while not TbR54.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R54 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_R54', False, [
        'TIPO', 'CNPJ', 'MODELO',
        'SERIE', 'NUMNF', 'CODNATOP',
        'SITTRIB', 'NUMITEM', 'CODPROD',
        'QTDADE', 'VLRPROD', 'VLRDESC',
        'BASECALC', 'BASESUBTRI', 'VLRIPI',
        'ALIQICMS'], [
        ], [
        TbR54TIPO.Value, TbR54CNPJ.Value, TbR54MODELO.Value,
        TbR54SERIE.Value, TbR54NUMNF.Value, TbR54CODNATOP.Value,
        TbR54SITTRIB.Value, TbR54NUMITEM.Value, TbR54CODPROD.Value,
        TbR54QTDADE.Value, TbR54VLRPROD.Value, TbR54VLRDESC.Value,
        TbR54BASECALC.Value, TbR54BASESUBTRI.Value, TbR54VLRIPI.Value,
        TbR54ALIQICMS.Value], [
        ], False) then ;
      end;
      //
      TbR54.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR70();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R70;                                     ' + sLineBreak +
    'CREATE TABLE _R70 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  CNPJ                 Varchar(14)                            ,' + sLineBreak +
    '  INSCEST              Varchar(14)                            ,' + sLineBreak +
    '  EMISSAO              Date                                   ,' + sLineBreak +
    '  UF                   Varchar(2)                             ,' + sLineBreak +
    '  MODELO               Varchar(2)                             ,' + sLineBreak +
    '  SERIE                Varchar(1)                             ,' + sLineBreak +
    '  SUBSERIE             Varchar(2)                             ,' + sLineBreak +
    '  NUMNF                Varchar(6)                             ,' + sLineBreak +
    '  CODNATOP             Varchar(4)                             ,' + sLineBreak +
    '  VALORTOT             Varchar(13)                            ,' + sLineBreak +
    '  BASEICMS             Varchar(14)                            ,' + sLineBreak +
    '  VALORICMS            Varchar(14)                            ,' + sLineBreak +
    '  VLRISENTO            Varchar(14)                            ,' + sLineBreak +
    '  OUTROS               Varchar(14)                            ,' + sLineBreak +
    '  CIF_FOB              Varchar(1)                             ,' + sLineBreak +
    '  SITUACAO             Varchar(1)                             ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR70.Close;
    TbR70.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR70 . O p e n ;
    TbR70.First;
    PB1.Position := 0;
    PB1.Max := TbR70.RecordCount;
    //Antigo  := -1;
    while not TbR70.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R70 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_R70', False, [
        'TIPO', 'CNPJ', 'INSCEST',
        'EMISSAO', 'UF', 'MODELO',
        'SERIE', 'SUBSERIE', 'NUMNF',
        'CODNATOP', 'VALORTOT', 'BASEICMS',
        'VALORICMS', 'VLRISENTO', 'OUTROS',
        'CIF_FOB', 'SITUACAO'], [
        ], [
        TbR70TIPO.Value, TbR70CNPJ.Value, TbR70INSCEST.Value,
        TbR70EMISSAO.Value, TbR70UF.Value, TbR70MODELO.Value,
        TbR70SERIE.Value, TbR70SUBSERIE.Value, TbR70NUMNF.Value,
        TbR70CODNATOP.Value, TbR70VALORTOT.Value, TbR70BASEICMS.Value,
        TbR70VALORICMS.Value, TbR70VLRISENTO.Value, TbR70OUTROS.Value,
        TbR70CIF_FOB.Value, TbR70SITUACAO.Value], [
        ], False) then ;
      end;
      //
      TbR70.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR71();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R71;                                     ' + sLineBreak +
    'CREATE TABLE _R71 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  CNPJ                 Varchar(14)                            ,' + sLineBreak +
    '  INSCEST              Varchar(14)                            ,' + sLineBreak +
    '  EMISSAOC             Date                                   ,' + sLineBreak +
    '  UF                   Varchar(2)                             ,' + sLineBreak +
    '  MODELOC              Varchar(2)                             ,' + sLineBreak +
    '  SERIEC               Varchar(1)                             ,' + sLineBreak +
    '  SUBSERIE             Varchar(2)                             ,' + sLineBreak +
    '  NUMEROC              Varchar(6)                             ,' + sLineBreak +
    '  UFREM                Varchar(2)                             ,' + sLineBreak +
    '  CNPJREM              Varchar(14)                            ,' + sLineBreak +
    '  INSCESTREM           Varchar(14)                            ,' + sLineBreak +
    '  EMISSAON             Date                                   ,' + sLineBreak +
    '  MODELON              Varchar(2)                             ,' + sLineBreak +
    '  SERIEN               Varchar(3)                             ,' + sLineBreak +
    '  NUMNF                Varchar(6)                             ,' + sLineBreak +
    '  TOTALNF              Varchar(14)                            ,' + sLineBreak +
    '  BRANCOS              Varchar(12)                             ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR71.Close;
    TbR71.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR71 . O p e n ;
    TbR71.First;
    PB1.Position := 0;
    PB1.Max := TbR71.RecordCount;
    //Antigo  := -1;
    while not TbR71.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R71 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_R71', False, [
        'TIPO', 'CNPJ', 'INSCEST',
        'EMISSAOC', 'UF', 'MODELOC',
        'SERIEC', 'SUBSERIE', 'NUMEROC',
        'UFREM', 'CNPJREM', 'INSCESTREM',
        'EMISSAON', 'MODELON', 'SERIEN',
        'NUMNF', 'TOTALNF', 'BRANCOS'], [
        ], [
        TbR71TIPO.Value, TbR71CNPJ.Value, TbR71INSCEST.Value,
        TbR71EMISSAOC.Value, TbR71UF.Value, TbR71MODELOC.Value,
        TbR71SERIEC.Value, TbR71SUBSERIE.Value, TbR71NUMEROC.Value,
        TbR71UFREM.Value, TbR71CNPJREM.Value, TbR71INSCESTREM.Value,
        TbR71EMISSAON.Value, TbR71MODELON.Value, TbR71SERIEN.Value,
        TbR71NUMNF.Value, TbR71TOTALNF.Value, TbR71BRANCOS.Value], [
        ], False) then ;
      end;
      //
      TbR71.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR74();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R74;                                     ' + sLineBreak +
    'CREATE TABLE _R74 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  DATA                 Date                                   ,' + sLineBreak +
    '  CODIPROD             Varchar(14)                            ,' + sLineBreak +
    '  QTDADE               Varchar(13)                            ,' + sLineBreak +
    '  VLRPROD              Varchar(13)                            ,' + sLineBreak +
    '  CODPOSSEM            Varchar(1)                             ,' + sLineBreak +
    '  CNPJ                 Varchar(14)                            ,' + sLineBreak +
    '  INSCEST              Varchar(14)                            ,' + sLineBreak +
    '  UF                   Varchar(2)                             ,' + sLineBreak +
    '  BRANCOS              Varchar(45)                             ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR74.Close;
    TbR74.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR74 . O p e n ;
    TbR74.First;
    PB1.Position := 0;
    PB1.Max := TbR74.RecordCount;
    //Antigo  := -1;
    while not TbR74.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R74 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_R74', False, [
        'TIPO', 'DATA', 'CODIPROD', 
        'QTDADE', 'VLRPROD', 'CODPOSSEM',
        'CNPJ', 'INSCEST', 'UF',
        'BRANCOS'], [
        ], [
        TbR74TIPO.Value, TbR74DATA.Value, TbR74CODIPROD.Value,
        TbR74QTDADE.Value, TbR74VLRPROD.Value, TbR74CODPOSSEM.Value,
        TbR74CNPJ.Value, TbR74INSCEST.Value, TbR74UF.Value,
        TbR74BRANCOS.Value], [
        ], False) then ;
      end;
      //
      TbR74.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR75();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R75;                                     ' + sLineBreak +
    'CREATE TABLE _R75 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  DATAINI              Date                                   ,' + sLineBreak +
    '  DATAFIN              Date                                   ,' + sLineBreak +
    '  CODPROD              Varchar(14)                            ,' + sLineBreak +
    '  CODNCM               Varchar(8)                             ,' + sLineBreak +
    '  DESCRICAO            Varchar(53)                            ,' + sLineBreak +
    '  UNIDADE              Varchar(6)                             ,' + sLineBreak +
    '  ALIQIPI              Varchar(5)                             ,' + sLineBreak +
    '  ALIQICMS             Varchar(4)                             ,' + sLineBreak +
    '  REDBASEICM           Varchar(5)                             ,' + sLineBreak +
    '  BASESUBTRI           Varchar(13)                             ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR75.Close;
    TbR75.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR75 . O p e n ;
    TbR75.First;
    PB1.Position := 0;
    PB1.Max := TbR75.RecordCount;
    //Antigo  := -1;
    while not TbR75.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R75 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_R75', False, [
        'TIPO', 'DATAINI', 'DATAFIN',
        'CODPROD', 'CODNCM', 'DESCRICAO',
        'UNIDADE', 'ALIQIPI', 'ALIQICMS',
        'REDBASEICM', 'BASESUBTRI'], [
        ], [
        TbR75TIPO.Value, TbR75DATAINI.Value, TbR75DATAFIN.Value,
        TbR75CODPROD.Value, TbR75CODNCM.Value, TbR75DESCRICAO.Value,
        TbR75UNIDADE.Value, TbR75ALIQIPI.Value, TbR75ALIQICMS.Value,
        TbR75REDBASEICM.Value, TbR75BASESUBTRI.Value], [
        ], False) then ;
      end;
      //
      TbR75.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR85();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R85;                                     ' + sLineBreak +
    'CREATE TABLE _R85 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  DECLARACAO           Varchar(11)                            ,' + sLineBreak +
    '  DATADECLAR           Date                                   ,' + sLineBreak +
    '  NATUREZA             Varchar(1)                             ,' + sLineBreak +
    '  REGISTRO             Varchar(12)                            ,' + sLineBreak +
    '  DATAREG              Date                                   ,' + sLineBreak +
    '  CONHECIM             Varchar(16)                            ,' + sLineBreak +
    '  DATACONHEC           Date                                   ,' + sLineBreak +
    '  TIPOCONHEC           Varchar(2)                             ,' + sLineBreak +
    '  PAIS                 Varchar(4)                             ,' + sLineBreak +
    '  RESERVADO            Varchar(8)                             ,' + sLineBreak +
    '  DATAAVERB            Date                                   ,' + sLineBreak +
    '  NUMNFEXP             Varchar(6)                             ,' + sLineBreak +
    '  EMISSAO              Date                                   ,' + sLineBreak +
    '  MODELO               Varchar(2)                             ,' + sLineBreak +
    '  SERIE                Varchar(3)                             ,' + sLineBreak +
    '  BRANCOS              Varchar(19)                            ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR85.Close;
    TbR85.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR85 . O p e n ;
    TbR85.First;
    PB1.Position := 0;
    PB1.Max := TbR85.RecordCount;
    //Antigo  := -1;
    while not TbR85.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R85 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_R85', False, [
        'TIPO', 'DECLARACAO', 'DATADECLAR',
        'NATUREZA', 'REGISTRO', 'DATAREG',
        'CONHECIM', 'DATACONHEC', 'TIPOCONHEC',
        'PAIS', 'RESERVADO', 'DATAAVERB',
        'NUMNFEXP', 'EMISSAO', 'MODELO',
        'SERIE', 'BRANCOS'], [
        ], [
        TbR85TIPO.Value, TbR85DECLARACAO.Value, TbR85DATADECLAR.Value,
        TbR85NATUREZA.Value, TbR85REGISTRO.Value, TbR85DATAREG.Value,
        TbR85CONHECIM.Value, TbR85DATACONHEC.Value, TbR85TIPOCONHEC.Value,
        TbR85PAIS.Value, TbR85RESERVADO.Value, TbR85DATAAVERB.Value,
        TbR85NUMNFEXP.Value, TbR85EMISSAO.Value, TbR85MODELO.Value,
        TbR85SERIE.Value, TbR85BRANCOS.Value], [
        ], False) then ;
      end;
      //
      TbR85.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR86();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R86;                                     ' + sLineBreak +
    'CREATE TABLE _R86 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  REGEXPORT            Varchar(12)                            ,' + sLineBreak +
    '  DATAREG              Date                                   ,' + sLineBreak +
    '  CNPJREM              Varchar(14)                            ,' + sLineBreak +
    '  INSCEST              Varchar(14)                            ,' + sLineBreak +
    '  UF                   Varchar(2)                             ,' + sLineBreak +
    '  NUMNF                Varchar(6)                             ,' + sLineBreak +
    '  EMISSAO              Date                                   ,' + sLineBreak +
    '  MODELO               Varchar(2)                             ,' + sLineBreak +
    '  SERIE                Varchar(3)                             ,' + sLineBreak +
    '  CODPROD              Varchar(14)                            ,' + sLineBreak +
    '  QTDADE               Varchar(11)                            ,' + sLineBreak +
    '  VLRUNIT              Varchar(12)                            ,' + sLineBreak +
    '  VLRPROD              Varchar(12)                            ,' + sLineBreak +
    '  RELACIONA            Varchar(1)                             ,' + sLineBreak +
    '  BRANCOS              Varchar(5)                              ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR86.Close;
    TbR86.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR86 . O p e n ;
    TbR86.First;
    PB1.Position := 0;
    PB1.Max := TbR86.RecordCount;
    //Antigo  := -1;
    while not TbR86.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R86 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_R86', False, [
        'TIPO', 'REGEXPORT', 'DATAREG',
        'CNPJREM', 'INSCEST', 'UF',
        'NUMNF', 'EMISSAO', 'MODELO',
        'SERIE', 'CODPROD', 'QTDADE',
        'VLRUNIT', 'VLRPROD', 'RELACIONA',
        'BRANCOS'], [
        ], [
        TbR86TIPO.Value, TbR86REGEXPORT.Value, TbR86DATAREG.Value,
        TbR86CNPJREM.Value, TbR86INSCEST.Value, TbR86UF.Value,
        TbR86NUMNF.Value, TbR86EMISSAO.Value, TbR86MODELO.Value,
        TbR86SERIE.Value, TbR86CODPROD.Value, TbR86QTDADE.Value,
        TbR86VLRUNIT.Value, TbR86VLRPROD.Value, TbR86RELACIONA.Value,
        TbR86BRANCOS.Value], [
        ], False) then ;
      end;
      //
      TbR86.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR88();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R88;                                     ' + sLineBreak +
    'CREATE TABLE _R88 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  SUBTIPO              Varchar(3)                             ,' + sLineBreak +
    '  CNPJ                 Varchar(14)                            ,' + sLineBreak +
    '  MODELO               Varchar(2)                             ,' + sLineBreak +
    '  SERIE                Varchar(3)                             ,' + sLineBreak +
    '  NUMERO               Varchar(6)                             ,' + sLineBreak +
    '  CFOP                 Varchar(4)                             ,' + sLineBreak +
    '  CST                  Varchar(3)                             ,' + sLineBreak +
    '  NUMITEM              Varchar(3)                             ,' + sLineBreak +
    '  CODPROD              Varchar(14)                            ,' + sLineBreak +
    '  NUMSER               Varchar(20)                            ,' + sLineBreak +
    '  BRANCOS              Varchar(52)                             ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR88.Close;
    TbR88.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR88 . O p e n ;
    TbR88.First;
    PB1.Position := 0;
    PB1.Max := TbR88.RecordCount;
    //Antigo  := -1;
    while not TbR88.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R88 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_R88', False, [
        'TIPO', 'SUBTIPO', 'CNPJ',
        'MODELO', 'SERIE', 'NUMERO',
        'CFOP', 'CST', 'NUMITEM',
        'CODPROD', 'NUMSER', 'BRANCOS'], [
        ], [
        TbR88TIPO.Value, TbR88SUBTIPO.Value, TbR88CNPJ.Value,
        TbR88MODELO.Value, TbR88SERIE.Value, TbR88NUMERO.Value,
        TbR88CFOP.Value, TbR88CST.Value, TbR88NUMITEM.Value,
        TbR88CODPROD.Value, TbR88NUMSER.Value, TbR88BRANCOS.Value], [
        ], False) then ;
      end;
      //
      TbR88.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaR90();
var
  //Antigo: Integer;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _R90;                                     ' + sLineBreak +
    'CREATE TABLE _R90 (                                            ' + sLineBreak +
    '  TIPO                 Varchar(2)                             ,' + sLineBreak +
    '  CNPJ                 Varchar(14)                            ,' + sLineBreak +
    '  INSCEST              Varchar(14)                            ,' + sLineBreak +
    '  TIPOREG              Varchar(2)                             ,' + sLineBreak +
    '  TOTREG               Varchar(8)                             ,' + sLineBreak +
    '  TIPOREG1             Varchar(2)                             ,' + sLineBreak +
    '  TOTREG1              Varchar(8)                             ,' + sLineBreak +
    '  TIPOREG2             Varchar(2)                             ,' + sLineBreak +
    '  TOTREG2              Varchar(8)                             ,' + sLineBreak +
    '  TIPOREG3             Varchar(2)                             ,' + sLineBreak +
    '  TOTREG3              Varchar(8)                             ,' + sLineBreak +
    '  TIPOREG4             Varchar(2)                             ,' + sLineBreak +
    '  TOTREG4              Varchar(8)                             ,' + sLineBreak +
    '  TIPOREG5             Varchar(2)                             ,' + sLineBreak +
    '  TOTREG5              Varchar(8)                             ,' + sLineBreak +
    '  TIPOREG6             Varchar(2)                             ,' + sLineBreak +
    '  TOTREG6              Varchar(8)                             ,' + sLineBreak +
    '  TIPOREG7             Varchar(2)                             ,' + sLineBreak +
    '  TOTREG7              Varchar(8)                             ,' + sLineBreak +
    '  TIPOREG8             Varchar(2)                             ,' + sLineBreak +
    '  TOTREG8              Varchar(8)                             ,' + sLineBreak +
    '  BRANCO               Varchar(5)                             ,' + sLineBreak +
    '  NUMTIP90             Varchar(1)                              ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsPedido;
    TbR90.Close;
    TbR90.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbR90 . O p e n ;
    TbR90.First;
    PB1.Position := 0;
    PB1.Max := TbR90.RecordCount;
    //Antigo  := -1;
    while not TbR90.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando R90 ');
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_R90', False, [
        'TIPO', 'CNPJ', 'INSCEST',
        'TIPOREG', 'TOTREG', 'TIPOREG1',
        'TOTREG1', 'TIPOREG2', 'TOTREG2',
        'TIPOREG3', 'TOTREG3', 'TIPOREG4',
        'TOTREG4', 'TIPOREG5', 'TOTREG5',
        'TIPOREG6', 'TOTREG6', 'TIPOREG7',
        'TOTREG7', 'TIPOREG8', 'TOTREG8',
        'BRANCO', 'NUMTIP90'], [
        ], [
        TbR90TIPO.Value, TbR90CNPJ.Value, TbR90INSCEST.Value,
        TbR90TIPOREG.Value, TbR90TOTREG.Value, TbR90TIPOREG1.Value,
        TbR90TOTREG1.Value, TbR90TIPOREG2.Value, TbR90TOTREG2.Value,
        TbR90TIPOREG3.Value, TbR90TOTREG3.Value, TbR90TIPOREG4.Value,
        TbR90TOTREG4.Value, TbR90TIPOREG5.Value, TbR90TOTREG5.Value,
        TbR90TIPOREG6.Value, TbR90TOTREG6.Value, TbR90TIPOREG7.Value,
        TbR90TOTREG7.Value, TbR90TIPOREG8.Value, TbR90TOTREG8.Value,
        TbR90BRANCO.Value, TbR90NUMTIP90.Value], [
        ], False) then ;
      end;
      //
      TbR90.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaReceber(CliInt: Integer; Dir: String);
const
  OrdemLista = 999;
  Prefixo = '';
  CtrlaSdo = 0;
  //
  Tipo = 0;
  Sub = 0;
var

  Continua: Boolean;
  //
  NF_TXT, SerieCH, Pedido_TXT, Doc2: String;
  //
  Data, Vencimento, Descricao, Compensado, Duplicata: String;
  Carteira, Genero, NotaFiscal, Sit, Controle, Cartao, Linha, Cliente,
  Banco, DescoPor, FatID, FatID_Sub: Integer;
  Credito, MoraDia, Pago, MultaVal, MoraVal: Double;
begin
  Screen.Cursor := crHourGlass;
  try
  if CliInt = -11 then
  begin
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _receber;                                    ' + sLineBreak +
    'CREATE TABLE _RECEBER (                                           ' + sLineBreak +
    '  Empresa              integer                                   ,' + sLineBreak +
    '  DOCUMENTO            Varchar(8)                                ,' + sLineBreak +
    '  DATA                 Date                                      ,' + sLineBreak +
    '  CLIENTE              Varchar(6)                                ,' + sLineBreak +
    '  NOTAFISCAL           Varchar(6)                                ,' + sLineBreak +
    '  VENCIMENTO           Date                                      ,' + sLineBreak +
    '  VALOR                Float                                     ,' + sLineBreak +
    '  PAGAMENTO            Date                                      ,' + sLineBreak +
    '  VALORPGTO            Float                                     ,' + sLineBreak +
    '  MULTA                Float                                     ,' + sLineBreak +
    '  JUROS                Float                                     ,' + sLineBreak +
    '  MORA                 Integer                                   ,' + sLineBreak +
    '  FORMAPGTO            Varchar(2)                                ,' + sLineBreak +
    '  CENTCUST             Varchar(6)                                ,' + sLineBreak +
    '  ITEMCUST             Varchar(6)                                ,' + sLineBreak +
    '  PEDIDO               Varchar(6)                                ,' + sLineBreak +
    '  BANCO                Varchar(3)                                ,' + sLineBreak +
    '  CAIXA                Integer                                   ,' + sLineBreak +
    '  HISTORICO            Varchar(20)                               ,' + sLineBreak +
    '  OBS                  Text                                      ,' + sLineBreak +
    '  DESCONTO             Integer                                    ' + sLineBreak +
    ') TYPE=MyISAM;');
  end;
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsReceber;
    TbReceber.Close;
    TbReceber.DBFFileName := Dir + '\' + TbTabelasNome.Value;
    TbReceber . O p e n ;
    TbReceber.First;
    PB1.Position := 0;
    PB1.Max := TbReceber.RecordCount;
    UFinanceiro.LancamentoDefaultVARS;
    while not TbReceber.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando Receber ' +
        TbReceberDOCUMENTO.Value);
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        //
if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_Receber', False, ['Empresa',
'DOCUMENTO', 'DATA', 'CLIENTE',
'NOTAFISCAL', 'VENCIMENTO', 'VALOR',
'PAGAMENTO', 'VALORPGTO', 'MULTA',
'JUROS', 'MORA', 'FORMAPGTO',
'CENTCUST', 'ITEMCUST', 'PEDIDO',
'BANCO', 'OBS', 'HISTORICO',
'CAIXA', 'DESCONTO'], [
], [CliInt,
TbReceberDOCUMENTO.Value, TbReceberDATA.Value, TbReceberCLIENTE.Value,
TbReceberNOTAFISCAL.Value, TbReceberVENCIMENTO.Value, TbReceberVALOR.Value,
TbReceberPAGAMENTO.Value, TbReceberVALORPGTO.Value, TbReceberMULTA.Value,
TbReceberJUROS.Value, TbReceberMORA.Value, TbReceberFORMAPGTO.Value,
TbReceberCENTCUST.Value, TbReceberITEMCUST.Value, TbReceberPEDIDO.Value,
TbReceberBANCO.Value, TbReceberOBS.Value, TbReceberHISTORICO.Value,
TbReceberCAIXA.Value, TbReceberDESCONTO.Value], [], False) then ;
  //
  NF_TXT := Trim(Geral.SoNumero_TT(TbReceberNOTAFISCAL.Value));
  if NF_TXT <> Trim(TbReceberNOTAFISCAL.Value) then
  begin
    Memo1.Lines.Add('Receber: NF inv�lida > ' + TbReceberNOTAFISCAL.Value);
    NF_TXT  := '0';
    SerieCH := NF_TXT;
  end else begin
    SerieCH := '';
  end;
  //
  PEDIDO_TXT := Trim(Geral.SoNumero_TT(TbReceberPEDIDO.Value));
  if PEDIDO_TXT <> Trim(TbReceberPEDIDO.Value) then
  begin
    Memo1.Lines.Add('Receber: Pedido inv�lido > ' + TbReceberPEDIDO.Value);
    PEDIDO_TXT  := '0';
    Doc2 := PEDIDO_TXT;
  end else begin
    Doc2 := '';
  end;
  //

{
  FLAN_Data          := Geral.FDT(TbReceberDATA.Value, 1);
  FLAN_Vencimento    := Geral.FDT(TbReceberVENCIMENTO.Value, 1);
  //FLAN_DataCad       := Geral.FDT(Date, 1);
  //FLAN_Mez           := '0';
  FLAN_Descricao     := TbReceberOBS.Value;
  FLAN_Compensado    := Geral.FDT(TbReceberPAGAMENTO.Value, 1);
  FLAN_Duplicata     := TbReceberDOCUMENTO.Value;
  FLAN_Doc2          := Doc2;
  FLAN_SerieCH       := SerieCH;

  //FLAN_Documento     := 0;
  //FLAN_Tipo          := 0;
  FLAN_Carteira      := Geral.IMV(Trim(TbReceberFORMAPGTO.Value));
  FLAN_Credito       := TbReceberVALOR.Value;
//  FLAN_Debito        := 0;
  FLAN_Genero        := Geral.IMV(Trim(TbReceberITEMCUST.Value));
//  FLAN_SerieNF       := '';
  FLAN_NotaFiscal    := Geral.IMV(Trim(NF_TXT));
  FLAN_Sit           := dmkPF.EscolhaDe2Int(TbReceberPAGAMENTO.Value > 0, 2, 0);
  FLAN_Controle      := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                        VAR_LCT, VAR_LCT, 'Controle');
  //FLAN_Sub           := 0;
  //FLAN_ID_Pgto       := 0;
  FLAN_Cartao        := Geral.IMV(Trim(TbReceberCENTCUST.Value));
  FLAN_Linha         := Geral.IMV(Trim(TbReceberITEMCUST.Value));
  //FLAN_Fornecedor    := 0;
  FLAN_Cliente       := Geral.IMV(Trim(TbReceberCLIENTE.Value));
  FLAN_MoraDia       := TbReceberMORA.Value; // Tudo zerado
  //FLAN_Multa         := 0;
  FLAN_UserCad       := VAR_USUARIO;
  //FLAN_DataDoc       := Geral.FDT(Date, 1);
  //FLAN_Vendedor      := 0;
  //FLAN_Account       := 0;
  //FLAN_ICMS_P        := 0;
  //FLAN_ICMS_V        := 0;
  FLAN_CliInt        := VAR_CliIntUnico;
  //FLAN_Depto         := 0;
  FLAN_DescoPor      := TbReceberDESCONTO.Value; // Parei Aqui! Ver o que fazer!
  //FLAN_ForneceI      := 0;
  //FLAN_DescoVal      := 0;
  //FLAN_NFVal         := 0;
  //FLAN_Unidade       := 0;
  //FLAN_Qtde          := 0;
  FLAN_FatID         := VAR_FATID_1016;
  FLAN_FatID_Sub     := Geral.IMV(PEDIDO_TXT);
  //FLAN_FatNum        := 0;
  //FLAN_FatParcela    := 0;
  FLAN_Pago          := TbReceberVALORPGTO.Value;
  //
  FLAN_MultaVal      := TbReceberMULTA.Value;
  FLAN_MoraVal       := TbReceberJUROS.Value;
  //FLAN_CtrlIni       := 0;
  //FLAN_Nivel         := 0;
  //FLAN_CNAB_Sit      := 0;
  //FLAN_TipoCH        := 0;
  //FLAN_Atrelado      := 0;
  //FLAN_SubPgto1      := 0;
  //FLAN_MultiPgto     := 0;
  //FLAN_Protocolo     := 0;
  //
  //FLAN_Emitente      := '';
  //FLAN_CNPJCPF       := '';
  FLAN_Banco         := Geral.IMV(Trim(TbReceberBANCO.Value)); // Parei Aqui! Ver o que fazer!
  //FLAN_Agencia       := '';
  //FLAN_ContaCorrente := '';
  //
}
  Data          := Geral.FDT(TbReceberDATA.Value, 1);
  Vencimento    := Geral.FDT(TbReceberVENCIMENTO.Value, 1);
  //DataCad       := Geral.FDT(Date, 1);
  Descricao     := TbReceberOBS.Value;
  Compensado    := Geral.FDT(TbReceberPAGAMENTO.Value, 1);
  Duplicata     := TbReceberDOCUMENTO.Value;

  //Documento     := 0;
  //Tipo          := 0;
  Carteira      := Geral.IMV(Trim(TbReceberFORMAPGTO.Value));
  Credito       := TbReceberVALOR.Value;
//  Debito        := 0;
  Genero        := Geral.IMV(Trim(TbReceberITEMCUST.Value));
//  SerieNF       := '';
  NotaFiscal    := Geral.IMV(Trim(NF_TXT));
  Sit           := dmkPF.EscolhaDe2Int(TbReceberPAGAMENTO.Value > 0, 2, 0);
  Controle      := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                        VAR_LCT, VAR_LCT, 'Controle');
  //Sub           := 0;
  //ID_Pgto       := 0;
  Cartao        := Geral.IMV(Trim(TbReceberCENTCUST.Value));
  Linha         := Geral.IMV(Trim(TbReceberITEMCUST.Value));
  //Fornecedor    := 0;
  Cliente       := Geral.IMV(Trim(TbReceberCLIENTE.Value));
  MoraDia       := TbReceberMORA.Value; // Tudo zerado
  //Multa         := 0;
  //UserCad       := VAR_USUARIO;
  //DataDoc       := Geral.FDT(Date, 1);
  //Vendedor      := 0;
  //Account       := 0;
  //ICMS_P        := 0;
  //ICMS_V        := 0;
  //CliInt        := VAR_CliIntUnico;
  //Depto         := 0;
  DescoPor      := TbReceberDESCONTO.Value; // Parei Aqui! Ver o que fazer!
  //ForneceI      := 0;
  //DescoVal      := 0;
  //NFVal         := 0;
  //Unidade       := 0;
  //Qtde          := 0;
  FatID         := VAR_FATID_1016;
  FatID_Sub     := Geral.IMV(PEDIDO_TXT);
  //FatNum        := 0;
  //FatParcela    := 0;
  Pago          := TbReceberVALORPGTO.Value;
  //
  MultaVal      := TbReceberMULTA.Value;
  MoraVal       := TbReceberJUROS.Value;
  //CtrlIni       := 0;
  //Nivel         := 0;
  //CNAB_Sit      := 0;
  //TipoCH        := 0;
  //Atrelado      := 0;
  //SubPgto1      := 0;
  //MultiPgto     := 0;
  //Protocolo     := 0;
  //
  //Emitente      := '';
  //CNPJCPF       := '';
  Banco         := Geral.IMV(Trim(TbReceberBANCO.Value)); // Parei Aqui! Ver o que fazer!
  //Agencia       := '';
  //ContaCorrente := '';
  //

  //TbReceberCAIXA.Value; // Parei Aqui! est� tudo zerado!
  //TbReceberHISTORICO.Value; // Parei Aqui! est� tudo em branco!
  // Fazer depois! pelo cartao / Linha VerificaConta_x_SubGrupo(Genero, TbReceberCENTCUST.Value, True);

{
        UFinanceiro.InsereLancamento();
}
if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, FTabLctA, False, [
'Genero',
'Descricao', 'NotaFiscal',
'Credito', 'Compensado',
'SerieCH', 'Sit',
'Vencimento', 'FatID', 'FatID_Sub',
'Banco',
'Cartao', 'Linha',
'Pago',
'Cliente', 'CliInt',
'MoraDia',
'MoraVal', 'MultaVal',
'DataDoc',
'Duplicata',
'DescoPor',
'Doc2'
], [
'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
Genero,
Descricao, NotaFiscal,
Credito, Compensado,
SerieCH, Sit,
Vencimento, FatID, FatID_Sub,
Banco,
Cartao, Linha,
Pago,
Cliente, CliInt,
MoraDia,
MoraVal, MultaVal,
Data,
Duplicata,
DescoPor,
Doc2
], [
Data, Tipo, Carteira, Controle, Sub], True) then ;
      end;
      //
      TbReceber.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaGruProd;
const
  Prefixo = '';
  NivCad = 2;
var
  Codigo, CodUsu: Integer;
  //
  Nome, Antigo: String;
  SQLType: TSQLType;
  Continua: Boolean;
  //
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsGruProd;
    TbGruProd.Close;
    TbGruProd.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbGruProd . O p e n ;
    TbGruProd.First;
    PB1.Position := 0;
    PB1.Max := TbGruProd.RecordCount;
    Antigo := '';
    while not TbGruProd.Eof do
    begin
      AtualizaTempo();
      //Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando GruProd ' +
        TbGruProdCODIGO.Value);
      //
      Continua := True;
      Antigo        := Geral.SoNumero_TT(TbGruProdCODIGO.Value);
      if LocalizaAntigoInt('PrdGrupTip', 'Codigo', Geral.IMV(Trim(Antigo))) then
      begin
        Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de GruProd ' +
        Antigo + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        SQLType := stUpd;
      end else SQLType := stIns;
      if Continua then
      begin
        Codigo := Geral.IMV(Trim(Geral.SoNumero_TT(TbGruProdCODIGO.Value))) +
        FIniGProd - 1;
        //
        Nome := TbGruProdDESCRIO.Value;
        //
        CodUsu := Codigo;

        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'prdgruptip', False, [
        'CodUsu', 'Nome',(* 'MadeBy',
        'Fracio', 'Gradeado', 'TipPrd',*)
        'NivCad' (*, 'FaixaIni', 'FaixaFim',
        'TitNiv1', 'TitNiv2', 'TitNiv3',
        'Customizav', 'ImpedeCad', 'LstPrcFisc'*)], [
        'Codigo'], [
        CodUsu, Nome, (* MadeBy,
        Fracio, Gradeado, TipPrd,*)
        NivCad (*, FaixaIni, FaixaFim,
        TitNiv1, TitNiv2, TitNiv3,
        Customizav, ImpedeCad, LstPrcFisc*)], [
        Codigo], True) then ;
      end;
      //
      TbGruProd.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaItemCust();
const
  OrdemLista = 999;
  Prefixo = '';
  CtrlaSdo = 0;
var
  Codigo, Antigo: Integer;
  //
  Nome, Nome2: String;
  SQLType: TSQLType;
  Continua: Boolean;
  //
  SubGrupo: Integer;
  Debito, Credito: Char;
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsItemCust;
    TbItemCust.Close;
    TbItemCust.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbItemCust . O p e n ;
    TbItemCust.First;
    PB1.Position := 0;
    PB1.Max := TbItemCust.RecordCount;
   // Antigo := -999999999;
    while not TbItemCust.Eof do
    begin
      AtualizaTempo();
      //Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando ItemCust ' +
        TbItemCustCODIGO.Value);
      //
      Continua := True;
      Antigo   := Geral.IMV(Trim(TbItemCustCODIGO.Value));
      if LocalizaAntigoInt('contas', 'Codigo', Antigo) then
      begin
        if Antigo = 56 then
          Continua := False
        else
          Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de ItemCust ' +
        FormatFloat('0', Antigo) + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        //
        SQLType := stUpd;
      end else SQLType := stIns;
      if Continua then
      begin
        Codigo := Geral.IMV(Trim(TbItemCustCODIGO.Value)) +
        FIniItCus - 1;
        //
        Nome := TbItemCustDESCRIO.Value;
        Nome2 := Nome;
        SubGrupo := Geral.IMV(Trim(TbItemCustCENTRO.Value));
        //
        //CodUsu := Codigo;
        //
        Debito := 'F';
        Credito:= 'F';
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'contas', False, [
        'Nome', 'Nome2', 'Subgrupo',
        'Credito', 'Debito', 'OrdemLista'], [
        'Codigo'], [
        Nome, Nome2, Subgrupo,
        Credito, Debito, OrdemLista], [
        Codigo], True) then ;
      end;
      //
      TbItemCust.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaItensPed(CliInt: Integer; Dir: String);
const
  Customizad = 0;
  InfAdCuztm = 0;
  MedidaC    = 0.0000;
  MedidaL    = 0.0000;
  MedidaA    = 0.0000;
  MedidaE    = 0.000000;
  PercCustom = 0.00;
var
  Continua: Boolean;
  SQLType: TSQLType;
  //
  Codigo, GraGruX, Ordem, Controle, Multi: Integer;
  PrecoO, PrecoR, QuantP, QuantC, QuantV, ValBru, DescoP, DescoV, ValLiq,
  PrecoF: Double;
  Referencia, GGX_TXT: String;
begin
  Screen.Cursor := crHourGlass;
  try
    if CliInt = -11 then
    begin
      LimparDadosTabela(
      'DROP TABLE IF EXISTS _ItensPed;                                    ' + sLineBreak +
      'CREATE TABLE _ItensPed (                                           ' + sLineBreak +
      '  Empresa              integer                                   ,' + sLineBreak +
      '  PEDIDO               Varchar(6)                                 ,' + sLineBreak +
      '  ORDEM                Integer                                    ,' + sLineBreak +
      '  PRODUTO              Varchar(6)                                 ,' + sLineBreak +
      '  CODPESQ              Varchar(5)                                 ,' + sLineBreak +
      '  MEDIDA               Varchar(3)                                 ,' + sLineBreak +
      '  PECAS                Integer                                    ,' + sLineBreak +
      '  QTDADE               Float                                      ,' + sLineBreak +
      '  VALOR                Float                                      ,' + sLineBreak +
      '  TOTAL                Float                                      ,' + sLineBreak +
      '  DESCRICAO            Varchar(35)                                 ' + sLineBreak +
      ') TYPE=MyISAM');
    end;

    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsItensPed;
    TbItensPed.Close;
    TbItensPed.DBFFileName := Dir + '\' + TbTabelasNome.Value;
    TbItensPed . O p e n ;
    TbItensPed.First;
    PB1.Position := 0;
    PB1.Max := TbItensPed.RecordCount;
    Controle := 0;
    while not TbItensPed.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando ItensPed ' +
        TbItensPedPEDIDO.Value + '-' + FormatFloat('000', TbItensPedORDEM.Value));
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_itensped', True, ['Empresa',
        'PEDIDO', 'ORDEM', 'PRODUTO',
        'CODPESQ', 'MEDIDA', 'PECAS',
        'QTDADE', 'VALOR', 'TOTAL',
        'DESCRICAO'], [
        ], [CliInt,
        TbItensPedPEDIDO.Value, TbItensPedORDEM.Value, TbItensPedPRODUTO.Value,
        TbItensPedCODPESQ.Value, TbItensPedMEDIDA.Value, TbItensPedPEAS.Value,
        TbItensPedQTDADE.Value, TbItensPedVALOR.Value, TbItensPedTOTAL.Value,
        TbItensPedDESCRICAO.Value], [
        ], False) then ;
      //

      SQLType := stIns;
      //
      // TbItensPedMEDIDA.Value ?
      // TbItensPedPEAS.Value ?
      Codigo     := Geral.IMV(TbItensPedPEDIDO.Value);
      GGX_TXT := Trim(Geral.SoNumero_TT(TbItensPedPRODUTO.Value));
      if GGX_TXT <> Trim(TbItensPedPRODUTO.Value) then
      begin
        Memo1.Lines.Add('itensped: produto inv�lido > ' + TbItensPedPRODUTO.Value);
        GGX_TXT  := '0';
      end;
      //
      GraGruX    := Geral.IMV(GGX_TXT);
      PrecoO     := TbItensPedVALOR.Value;
      PrecoR     := TbItensPedVALOR.Value;
      QuantP     := TbItensPedQTDADE.Value;
      QuantC     := 0;
      QuantV     := TbItensPedQTDADE.Value;
      ValBru     := TbItensPedTOTAL.Value;
      DescoP     := 0;
      DescoV     := 0;
      ValLiq     := TbItensPedTOTAL.Value;
      PrecoF     := TbItensPedVALOR.Value;
      Ordem      := TbItensPedORDEM.Value;
      Referencia := TbItensPedCODPESQ.Value;
      Controle   := Controle + 1;
      Multi      := Controle;

if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pedivdaits', False, [
'Codigo', 'GraGruX', 'PrecoO',
'PrecoR', 'QuantP', 'QuantC',
'QuantV', 'ValBru', 'DescoP',
'DescoV', 'ValLiq', 'PrecoF',
'MedidaC', 'MedidaL', 'MedidaA',
'MedidaE', 'PercCustom', 'Customizad',
'InfAdCuztm', 'Ordem', 'Referencia',
'Multi'], [
'Controle'], [
Codigo, GraGruX, PrecoO,
PrecoR, QuantP, QuantC,
QuantV, ValBru, DescoP,
DescoV, ValLiq, PrecoF,
MedidaC, MedidaL, MedidaA,
MedidaE, PercCustom, Customizad,
InfAdCuztm, Ordem, Referencia,
Multi], [
Controle], True) then ;
      //
      end;
      //
      TbItensPed.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaItens_Ent();
const
  Customizad = 0;
  InfAdCuztm = 0;
  MedidaC    = 0.0000;
  MedidaL    = 0.0000;
  MedidaA    = 0.0000;
  MedidaE    = 0.000000;
  PercCustom = 0.00;
var
  Continua: Boolean;
  //SQLType: TSQLType;
  //
  {
  Codigo, GraGruX, Ordem, Controle, Multi: Integer;
  PrecoO, PrecoR, QuantP, QuantC, QuantV, ValBru, DescoP, DescoV, ValLiq,
  PrecoF: Double;
  Referencia, GGX_TXT: String;
  }
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _Itens_Ent;                                    ' + sLineBreak +
    'CREATE TABLE _Itens_Ent (                                           ' + sLineBreak +
    '  NUMERO               Varchar(6)                                 ,' + sLineBreak +
    '  ITEM                 Integer                                    ,' + sLineBreak +
    '  CODPROD              Varchar(10)                                ,' + sLineBreak +
    '  DESCPROD             Varchar(60)                                ,' + sLineBreak +
    '  SITTRIB              Varchar(3)                                 ,' + sLineBreak +
    '  UNIDADE              Varchar(3)                                 ,' + sLineBreak +
    '  QTDADE               Float                                      ,' + sLineBreak +
    '  VALORUNIT            Float                                      ,' + sLineBreak +
    '  VALORTOT             Float                                      ,' + sLineBreak +
    '  ICMS                 Float                                      ,' + sLineBreak +
    '  IPI                  Float                                      ,' + sLineBreak +
    '  VALORIPI             Float                                      ,' + sLineBreak +
    '  CNPJ                 Varchar(14)                                ,' + sLineBreak +
    '  Codigo               int(11)                                     ' + sLineBreak +
    ') TYPE=MyISAM');

    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsItens_Ent;
    TbItens_Ent.Close;
    TbItens_Ent.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbItens_Ent . O p e n ;
    TbItens_Ent.First;
    PB1.Position := 0;
    PB1.Max := TbItens_Ent.RecordCount;
    //Controle := 0;
    while not TbItens_Ent.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando Itens_Ent ' +
        TbItens_EntNUMERO.Value);
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_itens_ent', False, [
        'NUMERO', 'ITEM', 'CODPROD',
        'DESCPROD', 'SITTRIB', 'UNIDADE',
        'QTDADE', 'VALORUNIT', 'VALORTOT',
        'ICMS', 'IPI', 'VALORIPI',
        'CNPJ'], [
        ], [
        TbItens_EntNUMERO.Value, TbItens_EntITEM.Value, TbItens_EntCODPROD.Value,
        TbItens_EntDESCPROD.Value, TbItens_EntSITTRIB.Value, TbItens_EntUNIDADE.Value,
        TbItens_EntQTDADE.Value, TbItens_EntVALORUNIT.Value, TbItens_EntVALORTOT.Value,
        TbItens_EntICMS.Value, TbItens_EntIPI.Value, TbItens_EntVALORIPI.Value,
        TbItens_EntCNPJ.Value], [
        ], False) then ;
      //
{
      SQLType := stIns;
      //
      // TbItens_EntMEDIDA.Value ?
      // TbItens_EntPEAS.Value ?
      Codigo     := Geral.IMV(TbItens_EntPEDIDO.Value);
      GGX_TXT := Trim(Geral.SoNumero_TT(TbItens_EntPRODUTO.Value));
      if GGX_TXT <> Trim(TbItens_EntPRODUTO.Value) then
      begin
        Memo1.Lines.Add('Itens_Ent: produto inv�lido > ' + TbItens_EntPRODUTO.Value);
        GGX_TXT  := '0';
      end;
      //
      GraGruX    := Geral.IMV(GGX_TXT);
      PrecoO     := TbItens_EntVALOR.Value;
      PrecoR     := TbItens_EntVALOR.Value;
      QuantP     := TbItens_EntQTDADE.Value;
      QuantC     := 0;
      QuantV     := TbItens_EntQTDADE.Value;
      ValBru     := TbItens_EntTOTAL.Value;
      DescoP     := 0;
      DescoV     := 0;
      ValLiq     := TbItens_EntTOTAL.Value;
      PrecoF     := TbItens_EntVALOR.Value;
      Ordem      := TbItens_EntORDEM.Value;
      Referencia := TbItens_EntCODPESQ.Value;
      Controle   := Controle + 1;
      Multi      := Controle;

(*
if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pedivdaits', False, [
'Codigo', 'GraGruX', 'PrecoO',
'PrecoR', 'QuantP', 'QuantC',
'QuantV', 'ValBru', 'DescoP',
'DescoV', 'ValLiq', 'PrecoF',
'MedidaC', 'MedidaL', 'MedidaA',
'MedidaE', 'PercCustom', 'Customizad',
'InfAdCuztm', 'Ordem', 'Referencia',
'Multi'], [
'Controle'], [
Codigo, GraGruX, PrecoO,
PrecoR, QuantP, QuantC,
QuantV, ValBru, DescoP,
DescoV, ValLiq, PrecoF,
MedidaC, MedidaL, MedidaA,
MedidaE, PercCustom, Customizad,
InfAdCuztm, Ordem, Referencia,
Multi], [
Controle], True) then ;
*)
}
      //
      end;
      //
      TbItens_Ent.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaMinhaNFe();
var
  Data_TXT: String;
  FFimData: Double;
begin
  //FIDCtrl_NF  := 0;    // nfecaba
  FControle   := 0;  // nfecabxvol
  FIDCtrl_SMI := 0;
  //FFatNum     := 0; // _nota_ent para nfecaba
  //
  QrMin.Close;
  QrMin.SQL.Clear;
  QrMin.SQL.Add('SELECT MIN(DATA) Data');
  QrMin.SQL.Add('FROM _pedido');
  QrMin.SQL.Add('WHERE Data > 1');
  QrMin . O p e n ;
  //
  FMinPedido := QrMinData.Value;
  if FMinPedido <2 then
  begin
    Geral.MB_Aviso('Data m�nima de pedido inv�lida:' + Geral.FDT(
    FMinPedido, 2) + '. Importa��o abortada');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  QrMin.Close;
  QrMin.SQL.Clear;
  QrMin.SQL.Add('SELECT MIN(EMISSAO) Data');
  QrMin.SQL.Add('FROM _nota_ent');
  QrMin.SQL.Add('WHERE EMISSAO > 2');
  QrMin . O p e n ;
  //
  FMinNota_Ent := QrMinData.Value;
  if FMinNota_Ent <2 then
  begin
    Geral.MB_Aviso('Data m�nima de nota de entrada inv�lida:' + Geral.FDT(
    FMinPedido, 2) + '. Importa��o abortada');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if FMinNota_Ent < FMinPedido then
    FAtuData := FMinNota_Ent
  else
    FAtuData := FMinPedido;
  FFimData := Int(Date);
  // Remover
  {
  FAtuData := Encodedate(2006, 01, 01);
  FFimData := Int(Encodedate(2011, 1, 5));
  }
  //
  while FAtuData <= FFimData do
  begin
    Data_TXT := Geral.FDT(FAtuData, 1);
    FAtuData_TXT := 'Importando NFs do dia ' + Data_TXT + '. ';
    MyObjects.Informa2(LaAviso1, LaAviso2 , False, '');
    if (FAtuData >= FMinNota_Ent) and FImportaEntrada then
      ImportaMinhaNFe_Recebida(Data_TXT);
    if (FAtuData >= FMinPedido) and FImportaEmitida then
      ImportaMinhaNFe_Emitida(Data_TXT);
    //
    FAtuData := FAtuData + 1;
  end;
end;

procedure TFmImportaFoxPro.ImportaMinhaNFe_Emitida(Data_TXT: String);
var
  Continua: Boolean;
  SQLType: TSQLType;
  //
  NNTxt: String;
  FatNum, FatID, Empresa, CodInfoTrsp, OrdServ, Situacao, IDCtrl, ide_nNF,
  ModFrete, NumNota: Integer;
  ide_natOp, ide_dEmi, ide_dSaiEnt, ide_hSaiEnt,
  InfAdic_InfCpl: String;
{
  ide_tpNF, ModFrete: Integer;
}
  ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST, ICMSTot_vProd,
  ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro, ICMSTot_vNF: Double;
  //
  qVol, pesoB, pesoL: Double;
  nVol, esp, marca: String;
  //
  Antigo: String;

  emit_CNPJ, emit_CPF, emit_xNome, emit_xFant, emit_xLgr, emit_nro, emit_xCpl,
  emit_xBairro, emit_cMun, emit_xMun, emit_UF, emit_CEP, emit_cPais, emit_xPais,
  emit_fone, emit_IE, emit_IEST, emit_IM, emit_CNAE: String;
  dest_CNPJ, dest_CPF, dest_xNome, dest_xLgr, dest_nro, dest_xCpl, dest_xBairro,
  dest_cMun, dest_xMun, dest_UF, dest_CEP, dest_cPais, dest_xPais, dest_fone,
  dest_IE, dest_ISUF: String;
  Cliente, CodInfoEmit, CodInfoDest, ide_mod, ide_serie: Integer;
  //
  prod_cProd, prod_xProd, prod_CFOP, prod_uCom: String;
  prod_qCom, prod_vUnCom, prod_vProd: Double;
  MeuID, Nivel1, nItem, Status, infProt_cStat, infCanc_cStat: Integer;
  //
  GraGruX, OriCnta, OriPart, Tipo, OriCodi, OriCtrl, StqCenCad, Baixa: Integer;
  DataHora, GraGruX_Txt, DataFiscal: String;
  Qtde, Pecas, Peso, CustoAll, Fator: Double;
  //
  ICMS_vBC, ICMS_pICMS: Double;
begin
  //FatNum := 0;
  Empresa := EdEmpresa_11.ValueVariant;

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//   SQL relacionamento com pedido para ver itens!                            //
//                                                                            //
//  SELECT nf.NUMERO, nf.ORDSERV, nf.TOTALPROD, nf.TOTAL, nf.J10UKEY,         //
//  pd.*                                                                      //
//  FROM _notafisc nf                                                         //
//  LEFT JOIN _pedido pd ON pd.NUMNOTA=nf.NUMERO AND pd.EntSai=1              //
//                                                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
  QrPedido.Close;
  QrPedido.Params[0].AsString := Data_TXT;
  QrPedido . O p e n ;
  while not QrPedido.Eof do
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, FAtuData_TXT + 'Pesquisando pedido: ' + QrPedidoNUMERO.Value);
    //
    NumNota := 0;
    NNTxt := Trim(QrPedidoNUMNOTA.Value);
    if NNTxt <> Geral.SoNumero_TT(NNTxt) then
      NumNota := 0
    else
      NumNota := Geral.IMV(NNTxt);
    //
    //if (QrPedidoENTSAI.Value = 1) and (NumNota <> 0) then
    //begin
(*
      QrNotaFisc.Close;
      QrNotaFisc.SQL.Clear;
      QrNotaFisc.SQL.Add('SELECT * FROM _notafisc');
      QrNotaFisc.SQL.Add('WHERE Numero=:P0');
      QrNotaFisc.Params[0].AsString := NNTxt;
      QrNotaFisc . O p e n ;
      //
      if QrNotaFisc.RecordCount = 0 then
      begin
*)
        QrNotaFisc.Close;
        QrNotaFisc.SQL.Clear;
        QrNotaFisc.SQL.Add('SELECT * FROM _notafisc');
        QrNotaFisc.SQL.Add('WHERE OrdServ=:P0');
        QrNotaFisc.SQL.Add('ORDER BY NUMERO, CR');
        QrNotaFisc.Params[0].AsString := QrPedidoNUMERO.Value;
        QrNotaFisc . O p e n ;
        //
        //if QrPedidoNUMERO.Value = '015611' then
          //ShowMessage('Ver exclus�o');

        {
        if QrNotaFisc.RecordCount > 0 then

        Memo2.Lines.Add('Encontrado ' + IntToStr(QrNotaFisc.RecordCount) +
        ' NF(s) para o pedido ' + QrPedidoNUMERO.Value + '. Primeira Nota: ' +
        QrNotaFiscNUMERO.Value);
        //, 'Aviso', MB_OK+MB_ICONWARNING);
        }
      //end;
      QrNotaFisc.First;
      while not QrNotaFisc.Eof do
      begin
        //if QrNotaFiscNUMERO.Value = '000872' then
          //ShowMessage('Ver 101');
        QrAnt.Close;
        QrAnt.SQL.Clear;
        QrAnt.SQL.Add('SELECT * FROM nfecaba');
        QrAnt.SQL.Add('WHERE FatID=1 AND ide_nNF=' + QrNotaFiscNUMERO.Value(*NNTxt*));
        QrAnt . O p e n ;
        //
        if QrAnt.RecordCount = 0 then
           SQLType := stIns
         else begin
           SQLType := stUpd;
           Memo1.Lines.Add('NF duplicada: ' + NNTxt);
         end;
         //
{
        if QrNotaFisc.RecNo = 1 then
           SQLType := stIns
         else begin
           SQLType := stUpd;
           ShowMessage('NF ' + NNTxt);
         end;
}
        {
        fazer update para 305 e 500
        if QrNotaFiscNATOPER.Value = '' then
          Exit;
        }
        MyObjects.Informa2(LaAviso1, LaAviso2, True, FAtuData_TXT + 'Pesquisando pedido: ' +
        QrPedidoNUMERO.Value + ' NF emitida: ' + QrPedidoNUMNOTA.Value);
        //
        // QrNotaFiscMEDIDA.Value ?
        // QrNotaFiscPEAS.Value ?

        Antigo := QrNotaFiscNUMERO.Value;
        //FatNum := Geral.IMV(QrNotaFiscNUMERO.Value);

        if QrPedidoENTSAI.Value = 1 then
        begin
          FatID  := 1;
          Tipo   := 1;
          Fator  := 1;
          FatNum := UMyMod.BuscaEmLivreY_Def('FatPedCab', 'Codigo', stIns, 0, nil, True);
      end else
        begin
          Tipo   := 151; // Parei aqui 151 = uso e consumo e pq?
          FatID  := 151;
          Fator  := -1;
          FatNum := UMyMod.BuscaEmLivreY_Def('pqe', 'Codigo', stIns, 0, nil, True);
        end;

        //
        CodInfoTrsp := Geral.IMV(QrNotaFiscTRANSPORTA.Value) + FIniTrans - 1;
  (*
        GGX_TXT := Trim(Geral.SoNumero_TT(QrNotaFiscPRODUTO.Value));
        if GGX_TXT <> Trim(QrNotaFiscPRODUTO.Value) then
        begin
          Memo1.Lines.Add('NotaFisc: produto inv�lido > ' + QrNotaFiscPRODUTO.Value);
          GGX_TXT  := '0';
        end;
  *)
        OrdServ      := Geral.IMV(QrNotaFiscORDSERV.Value);
        Situacao     := QrNotaFiscSITUACAO.Value;
        //
        //FIDCtrl_NF   := FIDCtrl_NF + 1;
        //IDCtrl       := FIDCtrl_NF;
        IDCtrl       := UMyMod.Busca_IDCtrl_NFe(stIns, 0);

        //Id, ide_cUF, ide_cNF,
        ide_natOp    := QrNotaFiscNATOPER.Value;
        //ide_indPag
        ide_mod      := 55; // Parei Aqui !!!
        ide_serie    := 1; // Parei Aqui !!!
        ide_nNF      := Geral.IMV(QrNotaFiscNUMERO.Value);
        ide_dEmi     := Geral.FDT(QrNotaFiscEMISSAO.Value, 1);
        ide_dSaiEnt  := Geral.FDT(QrNotaFiscSAIDA.Value, 1);
        //ide_tpNF     :=
        ide_hSaiEnt  := QrNotaFiscHORA.Value;
        //ide_cMunFG, ide_tpImp, ide_tpEmis, ide_cDV, ide_tpAmb, ide_finNFe, ide_procEmi,
        //ide_verProc,
        emit_CNPJ    := Geral.SoNumero_TT(TbEmpresaCNPJ.Value);
        emit_CPF     := '';
        emit_xNome   := TbEmpresaEMPRESA.Value;
        emit_xFant   := TbEmpresaEMPRESA.Value;
        emit_xLgr    := EdLgr_Txt.Text + ' ' + EdRua.Text;
        emit_nro     := EdNumero.Text;
        emit_xCpl    := '';
        emit_xBairro := TbEmpresaBAIRRO.Value;
        emit_cMun    := EdCidade_Cod.Text;
        emit_xMun    := TbEmpresaCIDADE.Value;
        emit_UF      := TbEmpresaUF.Value;
        emit_CEP     := Geral.SoNumero_TT(TbEmpresaCEP.Value);
        emit_cPais   := '1058';
        emit_xPais   := 'Brasil';
        emit_fone    := Geral.SoNumero_TT(TbEmpresaTELEFONE.Value);
        emit_IE      := Geral.SoNumero_TT(TbEmpresaINSCEST.Value);
        emit_IEST    := '';
        emit_IM      := '';
        emit_CNAE    := '';
        //
        Cliente      := BuscaEntidadeAntiga('C', QrPedidoCLIENTE.Value);
        if Cliente <> 0 then
        begin
          QrEnti.Close;
          QrEnti.Params[0].AsInteger := Cliente;
          QrEnti . O p e n ;
          //
          if QrEntiTipo.Value = 0 then
          begin
            dest_CNPJ    := QrEntiCNPJ_CPF.Value;
            dest_CPF     := ''
          end else begin
            dest_CNPJ    := '';
            dest_CPF     := QrEntiCNPJ_CPF.Value;
          end;
          dest_xNome   := QrEntiNO_ENTI.Value;
          dest_xLgr    := QrEntiNO_Lgr.Value + QrEntiRua.Value;
          dest_nro     := FormatFloat('0', QrEntiNumero.Value);
          dest_xCpl    := QrEntiCompl.Value;
          dest_xBairro := QrEntiBairro.Value;
          dest_cMun    := FormatFloat('0', QrEnticCidade.Value);
          dest_xMun    := QrEntixCidade.Value;
          dest_UF      := QrEntiNO_UF.Value;
          dest_CEP     := FormatFloat('0', QrEntiCEP.Value);
          dest_cPais   := FormatFloat('0', QrEnticPais.Value);
          dest_xPais   := QrEntixPais.Value;
          dest_fone    := QrEntiTe1.Value;
          dest_IE      := QrEntiIE_RG.Value;
          dest_ISUF    := QrEntiISUF.Value;
        end else begin
          dest_CNPJ    := '';
          dest_CPF     := '';
          dest_xNome   := '';
          dest_xLgr    := '';
          dest_nro     := '';
          dest_xCpl    := '';
          dest_xBairro := '';
          dest_cMun    := '';
          dest_xMun    := '';
          dest_UF      := '';
          dest_CEP     := '';
          dest_cPais   := '';
          dest_xPais   := '';
          dest_fone    := '';
          dest_IE      := '';
          dest_ISUF    := '';
        end;
        //
        //if ide_nNF = 806 then
          //ShowMessage('ver valores');
        ICMSTot_vBC     := QrNotaFiscBASEICMS.Value;
        ICMSTot_vICMS   := QrNotaFiscVLRICMS.Value;
        ICMSTot_vBCST   := QrNotaFiscBCICMSSUB.Value;
        ICMSTot_vST     := QrNotaFiscVLRICMSSUB.Value;
        ICMSTot_vProd   := QrNotaFiscTOTALPROD.Value;
        ICMSTot_vFrete  := QrNotaFiscVLRFRETE.Value;
        ICMSTot_vSeg    := QrNotaFiscVLRSEGURO.Value;
        //ICMSTot_vDesc   :=
        //ICMSTot_vII     :=
        ICMSTot_vIPI    := QrNotaFiscIPI.Value;
        //ICMSTot_vPIS    :=
        //ICMSTot_vCOFINS :=
        ICMSTot_vOutro  := QrNotaFiscOUTRAS.Value;
        ICMSTot_vNF     := QrNotaFiscTOTAL.Value;
        // Calculat pecentual de icms aqui
        if ICMSTot_vBC > 0 then
          ICMS_pICMS := ICMSTot_vICMS / ICMSTot_vBC * 100
        else
          ICMS_pICMS := 0;

        //ISSQNtot_vServ, ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS, ISSQNtot_vCOFINS,
        //RetTrib_vRetPIS, RetTrib_vRetCOFINS, RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF, RetTrib_vBCRetPrev, RetTrib_vRetPrev,
        //
        ModFrete        := QrNotaFiscFRETE.Value - 1; // Parei aqui! Certo?
  (*
        Transporta_CNPJ,
        Transporta_CPF,
        Transporta_XNome,
        Transporta_IE,
        Transporta_XEnder,
        Transporta_XMun,
        Transporta_UF,
          // Desconto ?? Parei Aqui!!
          //QrNotaFiscTIPODESC.Value, QrNotaFiscNOTADESC.Value, QrNotaFiscJ10UKEY.Value], [
        RetTransp_vServ,
        RetTransp_vBCRet,
        RetTransp_PICMSRet,
        RetTransp_vICMSRet,
        RetTransp_CFOP,
        RetTransp_CMunFG,
        //VeicTransp_Placa   := QrNotaFiscPLACAS.Value; n�o tem nada
        VeicTransp_UF,
        VeicTransp_RNTC,
        Cobr_Fat_nFat,
        Cobr_Fat_vOrig,
        Cobr_Fat_vDesc,
        Cobr_Fat_vLiq,
        InfAdic_InfAdFisco,
  *)
        InfAdic_InfCpl := QrNotaFiscOBS.Value;
        if (InfAdic_InfCpl <> '') and (QrNotaFiscCORPO.Value <> '') then
          InfAdic_InfCpl := InfAdic_InfCpl + sLineBreak;
        if (QrNotaFiscCORPO.Value <> '') then
          InfAdic_InfCpl := InfAdic_InfCpl + QrNotaFiscCORPO.Value;
(*
        Exporta_UFEmbarq,
        Exporta_XLocEmbarq,
        Compra_XNEmp,
        Compra_XPed,
        Compra_XCont,
*)
        //Status,  abaixo
(*
        infProt_Id, infProt_tpAmb, infProt_verAplic, infProt_dhRecbto, infProt_nProt,
        infProt_digVal,
*)
        //infProt_cStat  abaixo
(*
        infProt_xMotivo, infCanc_Id, infCanc_tpAmb,
        infCanc_verAplic, infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
*)
        //infCanc_cStat abaixo
(*
        infCanc_xMotivo, infCanc_cJust, infCanc_xJust, _Ativo_,
        FisRegCad, CartEmiss, TabelaPrc, CondicaoPg, FreteExtra, SegurExtra,
        ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS, IPIRec_pRedBC,
        IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
        PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
        COFINSRec_pAliq, COFINSRec_vCOFINS,*)
        DataFiscal := Geral.FDT(QrNotaFiscEMISSAO.Value, 1);
        (*protNFe_versao,
        retCancNFe_versao, SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta,
        SINTEGRA_ExpNat, SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta,
        SINTEGRA_ExpConhNum, SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip,
        SINTEGRA_ExpPais, SINTEGRA_ExpAverDta,
*)
        CodInfoEmit := EdEmpresa_11.ValueVariant;
        CodInfoDest := Cliente;
(*
        CriAForca, ide_dhCont, ide_xJust, emit_CRT, dest_email, Vagao, Balsa,
  *)
        //QrNotaFiscCR.Value; // Parei aqui!
        if Trim(QrNotaFiscCR.Value) <> '0' then
        begin
          case QrNotaFiscSITUACAO.Value of
            0:  // 1 registro
            begin
              Status        := 0;
              infProt_cStat := 0;
              infCanc_cStat := 0;
            end;
            1: // 12 registros
            begin
              Status        := 0;
              infProt_cStat := 0;
              infCanc_cStat := 0;
            end;
            2: // 1071 registros
            begin
              Status        := 100;
              infProt_cStat := 100;
              infCanc_cStat := 0;
            end;
            3: // 36 registros
            begin
              Status        := 101;
              infProt_cStat := 100;
              infCanc_cStat := 101;
            end;
            else  // nenhum registro
            begin
              Status        := 0;
              infProt_cStat := 0;
              infCanc_cStat := 0;
              Geral.MB_Erro('Situa��o n�o implementada! (NotaFisc)');
            end;
          end;
        end else
        begin
          Status        := 101;
          infProt_cStat := 100;
          infCanc_cStat := 101;
          Memo2.Lines.Add('Nota com CR zerado!  NF = ' + QrNotaFiscNUMERO.Value);
        end;
        // Evitar erro
        if SQLType = stIns then
          DmNFe_0000.ExcluiNfe(0(*Status*), FatID, FatNum, Empresa, True);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecaba', False, [
        'IDCtrl', (*'LoteEnv', 'versao',
        'Id', 'ide_cUF', 'ide_cNF',*)
        'ide_natOp', (*'ide_indPag',*) 'ide_mod',
        'ide_serie', 'ide_nNF', 'ide_dEmi',
        'ide_dSaiEnt', (*'ide_tpNF', 'ide_cMunFG',
        'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
        'ide_tpAmb', 'ide_finNFe', 'ide_procEmi',
        'ide_verProc',*) 'emit_CNPJ', 'emit_CPF',
        'emit_xNome', 'emit_xFant', 'emit_xLgr',
        'emit_nro', 'emit_xCpl', 'emit_xBairro',
        'emit_cMun', 'emit_xMun', 'emit_UF',
        'emit_CEP', 'emit_cPais', 'emit_xPais',
        'emit_fone', 'emit_IE', 'emit_IEST',
        'emit_IM', 'emit_CNAE', 'dest_CNPJ',
        'dest_CPF', 'dest_xNome', 'dest_xLgr',
        'dest_nro', 'dest_xCpl', 'dest_xBairro',
        'dest_cMun', 'dest_xMun', 'dest_UF',
        'dest_CEP', 'dest_cPais', 'dest_xPais',
        'dest_fone', 'dest_IE', 'dest_ISUF',
        'ICMSTot_vBC', 'ICMSTot_vICMS', 'ICMSTot_vBCST',
        'ICMSTot_vST', 'ICMSTot_vProd', 'ICMSTot_vFrete',
        'ICMSTot_vSeg', (*'ICMSTot_vDesc', 'ICMSTot_vII',*)
        'ICMSTot_vIPI', (*'ICMSTot_vPIS', 'ICMSTot_vCOFINS',*)
        'ICMSTot_vOutro', 'ICMSTot_vNF', (*'ISSQNtot_vServ',
        'ISSQNtot_vBC', 'ISSQNtot_vISS', 'ISSQNtot_vPIS',
        'ISSQNtot_vCOFINS', 'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS',
        'RetTrib_vRetCSLL', 'RetTrib_vBCIRRF', 'RetTrib_vIRRF',
        'RetTrib_vBCRetPrev', 'RetTrib_vRetPrev',*) 'ModFrete',
        (*'Transporta_CNPJ', 'Transporta_CPF', 'Transporta_XNome',
        'Transporta_IE', 'Transporta_XEnder', 'Transporta_XMun',
        'Transporta_UF', 'RetTransp_vServ', 'RetTransp_vBCRet',
        'RetTransp_PICMSRet', 'RetTransp_vICMSRet', 'RetTransp_CFOP',
        'RetTransp_CMunFG', 'VeicTransp_Placa', 'VeicTransp_UF',
        'VeicTransp_RNTC', 'Cobr_Fat_nFat', 'Cobr_Fat_vOrig',
        'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq', 'InfAdic_InfAdFisco',*)
        'InfAdic_InfCpl', (*'Exporta_UFEmbarq', 'Exporta_XLocEmbarq',
        'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',*)
        'Status', (*'infProt_Id', 'infProt_tpAmb',
        'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
        'infProt_digVal',*) 'infProt_cStat', (*'infProt_xMotivo',
        'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
        'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',*)
        'infCanc_cStat', (*'infCanc_xMotivo', 'infCanc_cJust',
        'infCanc_xJust', '_Ativo_', 'FisRegCad',
        'CartEmiss', 'TabelaPrc', 'CondicaoPg',
        'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
        'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
        'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
        'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
        'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
        'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
        'DataFiscal', (*'protNFe_versao', 'retCancNFe_versao',
        'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta', 'SINTEGRA_ExpNat',
        'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta', 'SINTEGRA_ExpConhNum',
        'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip', 'SINTEGRA_ExpPais',
        'SINTEGRA_ExpAverDta',*) 'CodInfoEmit', 'CodInfoDest',
        (*'CriAForca',*) 'ide_hSaiEnt', (*'ide_dhCont',
        'ide_xJust', 'emit_CRT', 'dest_email',
        'Vagao', 'Balsa',*) 'CodInfoTrsp'(*,
        'DataEstoq'*), 'Antigo'], [
        'FatID', 'FatNum', 'Empresa'], [
        IDCtrl, (*LoteEnv, versao,
        Id, ide_cUF, ide_cNF,*)
        ide_natOp, (*ide_indPag,*) ide_mod,
        ide_serie, ide_nNF, ide_dEmi,
        ide_dSaiEnt, (*ide_tpNF, ide_cMunFG,
        ide_tpImp, ide_tpEmis, ide_cDV,
        ide_tpAmb, ide_finNFe, ide_procEmi,
        ide_verProc,*) emit_CNPJ, emit_CPF,
        emit_xNome, emit_xFant, emit_xLgr,
        emit_nro, emit_xCpl, emit_xBairro,
        emit_cMun, emit_xMun, emit_UF,
        emit_CEP, emit_cPais, emit_xPais,
        emit_fone, emit_IE, emit_IEST,
        emit_IM, emit_CNAE, dest_CNPJ,
        dest_CPF, dest_xNome, dest_xLgr,
        dest_nro, dest_xCpl, dest_xBairro,
        dest_cMun, dest_xMun, dest_UF,
        dest_CEP, dest_cPais, dest_xPais,
        dest_fone, dest_IE, dest_ISUF,
        ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,
        ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete,
        ICMSTot_vSeg, (*ICMSTot_vDesc, ICMSTot_vII,*)
        ICMSTot_vIPI, (*ICMSTot_vPIS, ICMSTot_vCOFINS,*)
        ICMSTot_vOutro, ICMSTot_vNF, (*ISSQNtot_vServ,
        ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,
        ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS,
        RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,
        RetTrib_vBCRetPrev, RetTrib_vRetPrev,*) ModFrete,
        (*Transporta_CNPJ, Transporta_CPF, Transporta_XNome,
        Transporta_IE, Transporta_XEnder, Transporta_XMun,
        Transporta_UF, RetTransp_vServ, RetTransp_vBCRet,
        RetTransp_PICMSRet, RetTransp_vICMSRet, RetTransp_CFOP,
        RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
        VeicTransp_RNTC, Cobr_Fat_nFat, Cobr_Fat_vOrig,
        Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,*)
        InfAdic_InfCpl, (*Exporta_UFEmbarq, Exporta_XLocEmbarq,
        Compra_XNEmp, Compra_XPed, Compra_XCont,*)
        Status, (*infProt_Id, infProt_tpAmb,
        infProt_verAplic, infProt_dhRecbto, infProt_nProt,
        infProt_digVal,*) infProt_cStat, (*infProt_xMotivo,
        infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
        infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,*)
        infCanc_cStat, (*infCanc_xMotivo, infCanc_cJust,
        infCanc_xJust, _Ativo_, FisRegCad,
        CartEmiss, TabelaPrc, CondicaoPg,
        FreteExtra, SegurExtra, ICMSRec_pRedBC,
        ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
        IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
        IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
        PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
        COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
        DataFiscal, (*protNFe_versao, retCancNFe_versao,
        SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_ExpNat,
        SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum,
        SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais,
        SINTEGRA_ExpAverDta,*) CodInfoEmit, CodInfoDest,
        (*CriAForca,*) ide_hSaiEnt, (*ide_dhCont,
        ide_xJust, emit_CRT, dest_email,
        Vagao, Balsa,*) CodInfoTrsp(*,
        DataEstoq*), Antigo], [
        FatID, FatNum, Empresa], False) then
        begin
          qVol  := QrNotaFiscQTDADE.Value;
          esp   := QrNotaFiscESPECIE.Value;
          marca := QrNotaFiscMARCA.Value;
          nVol  := QrNotaFiscNUMPROD.Value;
          pesoB := QrNotaFiscPESOBRUT.Value;
          pesoL := QrNotaFiscPESOLIQ.Value;
          FControle := FControle + 1;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabxvol', False, [
          'qVol', 'esp', 'marca',
          'nVol', 'pesoL', 'pesoB'], [
          'FatID', 'FatNum', 'Empresa', 'Controle'], [
          qVol, esp, marca,
          nVol, pesoL, pesoB], [
          FatID, FatNum, Empresa, FControle], True) then ;
          //
          QrItensPed.Close;
          QrItensPed.Params[0].AsString := QrPedidoNUMERO.Value;
          QrItensPed . O p e n ;
          //
          while not QrItensPed.Eof do
          begin
            nItem := 0;
            try
            MyObjects.Informa2(LaAviso1, LaAviso2, True, FAtuData_TXT + 'Pesquisando pedido: ' +
            QrPedidoNUMERO.Value + ' NF emitida: ' + QrPedidoNUMNOTA.Value +
            ' Item: ' + FormatFloat('0', QrItensPedORDEM.Value));
            nItem          := QrItensPedORDEM.Value;
            prod_cProd     := QrItensPedPRODUTO.Value;
            prod_xProd     := QrItensPedDESCRICAO.Value;
            prod_CFOP      := Geral.SoNumero_TT(QrNotaFiscNATOPER.Value);
            prod_uCom      := QrItensPedMEDIDA.Value;
            prod_qCom      := QrItensPedQTDADE.Value;
            prod_vUnCom    := QrItensPedVALOR.Value;
            prod_vProd     := QrItensPedTOTAL.Value;
            MeuID          := 0;
            Nivel1         := Geral.IMV(QrItensPedPRODUTO.Value);
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
            'prod_cProd', (*'prod_cEAN',*) 'prod_xProd',
            (*'prod_NCM', 'prod_EXTIPI', 'prod_genero',*)
            'prod_CFOP', 'prod_uCom', 'prod_qCom',
            'prod_vUnCom', 'prod_vProd', (*'prod_cEANTrib',
            'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
            'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
            'prod_vOutro', 'prod_indTot', 'prod_xPed',
            'prod_nItemPed', 'Tem_IPI', '_Ativo_',
            'InfAdCuztm', 'EhServico', 'ICMSRec_pRedBC',
            'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
            'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
            'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
            'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
            'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
            'MeuID', 'Nivel1'], [
            'FatID', 'FatNum', 'Empresa', 'nItem'], [
            prod_cProd, (*prod_cEAN,*) prod_xProd,
            (*prod_NCM, prod_EXTIPI, prod_genero,*)
            prod_CFOP, prod_uCom, prod_qCom,
            prod_vUnCom, prod_vProd, (*prod_cEANTrib,
            prod_uTrib, prod_qTrib, prod_vUnTrib,
            prod_vFrete, prod_vSeg, prod_vDesc,
            prod_vOutro, prod_indTot, prod_xPed,
            prod_nItemPed, Tem_IPI, _Ativo_,
            InfAdCuztm, EhServico, ICMSRec_pRedBC,
            ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
            IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
            IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
            PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
            COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
            MeuID, Nivel1], [
            FatID, FatNum, Empresa, nItem], True, '', False, True) then
            begin
              ICMS_vBC := QrItensPedTOTAL.Value;
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsn', False, [
              (*'ICMS_Orig', 'ICMS_CST', 'ICMS_modBC',
              'ICMS_pRedBC',*) 'ICMS_vBC', 'ICMS_pICMS'(*,
              'ICMS_vICMS', 'ICMS_modBCST', 'ICMS_pMVAST',
              'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
              'ICMS_vICMSST', '_Ativo_', 'ICMS_CSOSN',
              'ICMS_UFST', 'ICMS_pBCOp', 'ICMS_vBCSTRet',
              'ICMS_vICMSSTRet', 'ICMS_motDesICMS', 'ICMS_pCredSN',
              'ICMS_vCredICMSSN'*)], [
              'FatID', 'FatNum', 'Empresa', 'nItem'], [
              (*ICMS_Orig, ICMS_CST, ICMS_modBC,
              ICMS_pRedBC,*) ICMS_vBC, ICMS_pICMS(*,
              ICMS_vICMS, ICMS_modBCST, ICMS_pMVAST,
              ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
              ICMS_vICMSST, _Ativo_, ICMS_CSOSN,
              ICMS_UFST, ICMS_pBCOp, ICMS_vBCSTRet,
              ICMS_vICMSSTRet, ICMS_motDesICMS, ICMS_pCredSN,
              ICMS_vCredICMSSN*)], [
              FatID, FatNum, Empresa, nItem], True) then ;
            end;
            //
            except
               Memo1.Lines.Add('Erro nfeitsi > ' + FormatFloat('0', FatNum) + '#' + FormatFloat('0', nItem));
            end;
            QrItensPed.Next;
          end;
        end;
        //
        QrNotaFisc.Next;
      end;
    //end; Fim NumNota <> 0
    //
    // Estoque
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, FAtuData_TXT + ' Incluindo no estoque: ' +
    QrPedidoNUMERO.Value);
    {
    if QrPedidoENTSAI.Value = 1 then
    begin
      Tipo  := 1;
      Fator := 1;
    end else
    begin
      Tipo  := 151; // Parei aqui 151 = uso e consumo e pq?
      Fator := -1;
    end;
    }
    DataHora    := Geral.FDT(QrPedidoDATA.Value, 1);
    OriCodi     := Geral.IMV(QrPedidoNUMERO.Value);
    OriCnta     := 0;
    OriPart     := 0;
    StqCenCad   := 1;
    //
    QrItensPed.Close;
    QrItensPed.Params[0].AsString := QrPedidoNUMERO.Value;
    QrItensPed . O p e n ;
    while not QrItensPed.Eof do
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, FAtuData_TXT +
      ' Incluindo no estoque: ' + QrPedidoNUMERO.Value +
      ' Item: ' + FormatFloat('0', QrItensPedORDEM.Value));
      //
      GraGruX_Txt := Trim(QrItensPedPRODUTO.Value);
      if GraGruX_TXT <> Geral.SoNumero_TT(GraGruX_TXT) then
        GraGruX := 0
      else
        GraGruX := Geral.IMV(GraGruX_Txt);
      //
      Qtde        := QrItensPedQTDADE.Value * Fator;
      Pecas       := QrItensPedPECAS.Value * Fator;
      Peso        := QrItensPedQTDADE.Value * Fator;
      CustoAll    := QrItensPedTOTAL.Value;
      Baixa       := QrPedidoTIPO.Value - 1;
      //
      FIDCtrl_SMI := FIDCtrl_SMI + 1;
      IDCtrl      := FIDCtrl_SMI;
      OriCtrl     := FIDCtrl_SMI;
      //? := UMyMod.BuscaEmLivreY_Def('stqmovitsa', ''IDCtrl', ImgTipo.SQLType, CodAtual);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
      'DataHora', 'Tipo', 'OriCodi',
      'OriCtrl', 'OriCnta', 'OriPart',
      'Empresa', 'StqCenCad', 'GraGruX',
      'Qtde', 'Pecas', 'Peso',
      (*'AreaM2', 'AreaP2', 'FatorClas',
      'QuemUsou', 'Retorno', 'ParTipo',
      'ParCodi', 'DebCtrl', 'SMIMultIns',*)
      'CustoAll', (*'ValorAll', 'GrupoBal',*)
      'Baixa'], [
      'IDCtrl'], [
      DataHora, Tipo, OriCodi,
      OriCtrl, OriCnta, OriPart,
      Empresa, StqCenCad, GraGruX,
      Qtde, Pecas, Peso,
      (*AreaM2, AreaP2, FatorClas,
      QuemUsou, Retorno, ParTipo,
      ParCodi, DebCtrl, SMIMultIns,*)
      CustoAll, (*ValorAll, GrupoBal,*)
      Baixa], [
      IDCtrl], False) then ;
      //
      QrItensPed.Next;
    end;
    QrPedido.Next;
  end;
end;

procedure TFmImportaFoxPro.ImportaMinhaNFe_Recebida(Data_TXT: String);
const
  Fator = 1; // Positivo porque � de entrada no estoque
var
  Continua: Boolean;
  SQLType: TSQLType;
  //
  NNTxt: String;
  FatNum, FatID, Empresa, CodInfoTrsp, OrdServ, Situacao, IDCtrl, ide_nNF,
  ModFrete, NumNota: Integer;
  ide_natOp, ide_dEmi, ide_dSaiEnt, ide_hSaiEnt,
  InfAdic_InfCpl: String;
{
  ide_tpNF, ModFrete: Integer;
}
  ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST, ICMSTot_vProd,
  ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro, ICMSTot_vNF: Double;
  //
  qVol, pesoB, pesoL: Double;
  nVol, esp, marca: String;
  //
  Antigo: String;

  emit_CNPJ, emit_CPF, emit_xNome, emit_xFant, emit_xLgr, emit_nro, emit_xCpl,
  emit_xBairro, emit_cMun, emit_xMun, emit_UF, emit_CEP, emit_cPais, emit_xPais,
  emit_fone, emit_IE, emit_IEST, emit_IM, emit_CNAE: String;
  dest_CNPJ, dest_CPF, dest_xNome, dest_xLgr, dest_nro, dest_xCpl, dest_xBairro,
  dest_cMun, dest_xMun, dest_UF, dest_CEP, dest_cPais, dest_xPais, dest_fone,
  dest_IE, dest_ISUF: String;
  Fornece, CodInfoEmit, CodInfoDest, ide_mod, ide_serie: Integer;
  //
  prod_cProd, prod_xProd, prod_CFOP, prod_uCom: String;
  prod_qCom, prod_vUnCom, prod_vProd: Double;
  MeuID, Nivel1, nItem, Status, infProt_cStat, infCanc_cStat: Integer;
  //
  GraGruX, OriCnta, OriPart, Tipo, OriCodi, OriCtrl, StqCenCad, Baixa: Integer;
  DataHora, DataFiscal, SITTRIB, ICMS_Orig, ICMS_CST: String;
  Qtde, Pecas, Peso, CustoAll: Double;
  ICMS_vBC, ICMS_pICMS, ICMS_vICMS, IPI_pIPI, IPI_vIPI, IPI_vBC: Double;
  GraGruX_Txt: String;
begin
  Empresa := EdEmpresa_11.ValueVariant;
  //FatNum  := 0;
  FatID   := 0;
  QrNota_Ent.Close;
  QrNota_Ent.Params[0].AsString := Data_TXT;
  QrNota_Ent . O p e n ;
  QrNota_Ent.First;
  while not QrNota_Ent.Eof do
  begin
    if QrNota_EntSituacao.Value = 1 then
    begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, FAtuData_TXT + 'Pesquisando  NF de entrada: ' +
    QrNota_EntNUMERO.Value + ' de ' + QrNota_EntCNPJ.Value);
    //
    // QrNota_EntMEDIDA.Value ?
    // QrNota_EntPEAS.Value ?
    FatID   := 151; // Parei aqui Fazer o que com o couro?
    //FFatNum  := FFatNum + 1;
    FatNum := UMyMod.BuscaEmLivreY_Def('pqe', 'Codigo', stIns, 0, nil, True);
    //FatNum := FFatNum;
    Antigo := QrNota_EntNUMERO.Value;
    //
    CodInfoTrsp := 0;//Geral.IMV(QrNota_EntTRANSPORTA.Value) + FIniTrans - 1;
    OrdServ      := 0;//Geral.IMV(QrNota_EntORDSERV.Value);
    Situacao     := QrNota_EntSITUACAO.Value;
    //
    //FIDCtrl_NF   := FIDCtrl_NF + 1;
    //IDCtrl       := FIDCtrl_NF;
    IDCtrl       := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
    //Id, ide_cUF, ide_cNF,
    ide_natOp    := QrNota_EntCFOP.Value;
    //ide_indPag
    ide_mod      := 55;// Parei aqui !!!
    ide_serie    := 1; // Parei Aqui !!!
    ide_nNF      := Geral.IMV(QrNota_EntNUMERO.Value);
    ide_dEmi     := Geral.FDT(QrNota_EntEMISSAO.Value, 1);
    ide_dSaiEnt  := '';//Geral.FDT(QrNota_EntSAIDA.Value, 1);
    //ide_tpNF     :=
    ide_hSaiEnt  := '';//QrNota_EntHORA.Value;
    //ide_cMunFG, ide_tpImp, ide_tpEmis, ide_cDV, ide_tpAmb, ide_finNFe, ide_procEmi,
    //ide_verProc,
    dest_CNPJ    := Geral.SoNumero_TT(TbEmpresaCNPJ.Value);
    dest_CPF     := '';
    dest_xNome   := TbEmpresaEMPRESA.Value;
    dest_xLgr    := EdLgr_Txt.Text + ' ' + EdRua.Text;
    dest_nro     := EdNumero.Text;
    dest_xCpl    := '';
    dest_xBairro := TbEmpresaBAIRRO.Value;
    dest_cMun    := EdCidade_Cod.Text;
    dest_xMun    := TbEmpresaCIDADE.Value;
    dest_UF      := TbEmpresaUF.Value;
    dest_CEP     := Geral.SoNumero_TT(TbEmpresaCEP.Value);
    dest_cPais   := '1058';
    dest_xPais   := 'Brasil';
    dest_fone    := Geral.SoNumero_TT(TbEmpresaTELEFONE.Value);
    dest_IE      := Geral.SoNumero_TT(TbEmpresaINSCEST.Value);
    dest_ISUF    := '';
    //
    Fornece      := BuscaEntidadePeloCNPJ(QrNota_EntCNPJ.Value);
    if Fornece = 0 then
      Fornece := IncluiEntidadePeloCNPJ(QrNota_EntCNPJ.Value,
      QrNota_EntRAZAO.Value, QrNota_EntRAZAO.Value, QrNota_EntINSCEST.Value,
      QrNota_EntENDERECO.Value, QrNota_EntBAIRRO.Value, QrNota_EntCEP.Value,
      QrNota_EntMUNICIO.Value, QrNota_EntFONE.Value, QrNota_EntUF.Value);
    if Fornece = 0 then
      ShowMessage('Fornece ainda ficou zerado!');
    (*
    if Fornece <> 0 then
    begin
      QrEnti.Close;
      QrEnti.Params[0].AsInteger := Fornece;
      QrEnti . O p e n ;
      //
      if QrEntiTipo.Value = 0 then
      begin
        emit_CNPJ    := QrEntiCNPJ_CPF.Value;
        emit_CPF     := ''
      end else begin
        emit_CNPJ    := '';
        emit_CPF     := QrEntiCNPJ_CPF.Value;
      end;
      emit_xNome   := QrEntiNO_ENTI.Value;
      emit_xFant   := QrEntiN2_ENTI.Value;
      emit_xLgr    := QrEntiNO_Lgr.Value + QrEntiRua.Value;
      emit_nro     := FormatFloat('0', QrEntiNumero.Value);
      emit_xCpl    := QrEntiCompl.Value;
      emit_xBairro := QrEntiBairro.Value;
      emit_cMun    := FormatFloat('0', QrEnticCidade.Value);
      emit_xMun    := QrEntixCidade.Value;
      emit_UF      := QrEntiNO_UF.Value;
      emit_CEP     := FormatFloat('0', QrEntiCEP.Value);
      emit_cPais   := FormatFloat('0', QrEnticPais.Value);
      emit_xPais   := QrEntixPais.Value;
      emit_fone    := QrEntiTe1.Value;
      emit_IE      := Geral.SoNumero_TT(QrNota_EntINSCEST.Value);//QrEntiIE_RG.Value;
      emit_IEST    := '';
      emit_IM      := '';
      emit_CNAE    := '';
    end else begin
    *)
      emit_CNPJ    := QrNota_EntCNPJ.Value;
      emit_CPF     := '';
      emit_xNome   := QrNota_EntRAZAO.Value;
      emit_xFant   := '';
      emit_xLgr    := QrNota_EntENDERECO.Value;
      emit_nro     := '';
      emit_xCpl    := '';
      emit_xBairro := QrNota_EntBAIRRO.Value;
      emit_cMun    := '';
      emit_xMun    := QrNota_EntMUNICIO.Value;
      emit_UF      := QrNota_EntUF.Value;
      emit_CEP     := QrNota_EntCEP.Value;
      emit_cPais   := '1058';
      emit_xPais   := 'BRASIL';
      emit_fone    := QrNota_EntFONE.Value;
      emit_IE      := QrNota_EntINSCEST.Value;
      emit_IEST    := '';
      emit_IM      := '';
      emit_CNAE    := '';
    (*
    end;
    *)
    //
    ICMSTot_vBC     := QrNota_EntBASEICMS.Value;
    ICMSTot_vICMS   := QrNota_EntVALORICMS.Value;
    ICMSTot_vBCST   := QrNota_EntBASESUB.Value;
    ICMSTot_vST     := QrNota_EntVALORSUB.Value;
    ICMSTot_vProd   := QrNota_EntVALORPROD.Value;
    ICMSTot_vFrete  := QrNota_EntFRETE.Value;
    ICMSTot_vSeg    := QrNota_EntSEGURO.Value;
    //ICMSTot_vDesc   :=
    //ICMSTot_vII     :=
    ICMSTot_vIPI    := QrNota_EntIPI.Value;
    //ICMSTot_vPIS    :=
    //ICMSTot_vCOFINS :=
    ICMSTot_vOutro  := QrNota_EntOUTRAS.Value;
    ICMSTot_vNF     := QrNota_EntTOTAL.Value;
    //ISSQNtot_vServ, ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS, ISSQNtot_vCOFINS,
    //RetTrib_vRetPIS, RetTrib_vRetCOFINS, RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF, RetTrib_vBCRetPrev, RetTrib_vRetPrev,
    //
    ModFrete        := QrNota_EntFRETE_CONT.Value - 1; // Parei aqui! Certo?
(*
    Transporta_CNPJ,
    Transporta_CPF,
    Transporta_XNome,
    Transporta_IE,
    Transporta_XEnder,
    Transporta_XMun,
    Transporta_UF,
      // Desconto ?? Parei Aqui!!
      //QrNota_EntTIPODESC.Value, QrNota_EntNOTADESC.Value, QrNota_EntJ10UKEY.Value], [
    RetTransp_vServ,
    RetTransp_vBCRet,
    RetTransp_PICMSRet,
    RetTransp_vICMSRet,
    RetTransp_CFOP,
    RetTransp_CMunFG,
    //VeicTransp_Placa   := QrNota_EntPLACAS.Value; n�o tem nada
    VeicTransp_UF,
    VeicTransp_RNTC,
    Cobr_Fat_nFat,
    Cobr_Fat_vOrig,
    Cobr_Fat_vDesc,
    Cobr_Fat_vLiq,
    InfAdic_InfAdFisco,
*)
(*
    InfAdic_InfCpl := QrNota_EntOBS.Value;
    if (InfAdic_InfCpl <> '') and (QrNota_EntCORPO.Value <> '') then
      InfAdic_InfCpl := InfAdic_InfCpl + sLineBreak;
    if (QrNota_EntCORPO.Value <> '') then
      InfAdic_InfCpl := InfAdic_InfCpl + QrNota_EntCORPO.Value;
    //QrNota_EntCR.Value; // Parei aqui!
*)
    InfAdic_InfCpl := '';
(*
    Exporta_UFEmbarq,
    Exporta_XLocEmbarq,
    Compra_XNEmp,
    Compra_XPed,
    Compra_XCont,
*)
    // Status, abaixo
(*    infProt_Id, infProt_tpAmb, infProt_verAplic, infProt_dhRecbto, infProt_nProt,
    infProt_digVal,
*)
    // infProt_cStat, abaixo
(*
    infProt_xMotivo, infCanc_Id, infCanc_tpAmb,
    infCanc_verAplic, infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
*)
    // infCanc_cStat, abaixo
(*
    infCanc_xMotivo, infCanc_cJust, infCanc_xJust, _Ativo_,
    FisRegCad, CartEmiss, TabelaPrc, CondicaoPg, FreteExtra, SegurExtra,
    ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS, IPIRec_pRedBC,
    IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
    PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
    COFINSRec_pAliq, COFINSRec_vCOFINS,*)
    DataFiscal := Geral.FDT(QrNota_EntDATACONTAB.Value, 1);
    (*protNFe_versao,
    retCancNFe_versao, SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta,
    SINTEGRA_ExpNat, SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta,
    SINTEGRA_ExpConhNum, SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip,
    SINTEGRA_ExpPais, SINTEGRA_ExpAverDta,
*)
    CodInfoEmit := Fornece;
    CodInfoDest := EdEmpresa_11.ValueVariant;
(*
    CriAForca, ide_dhCont, ide_xJust, emit_CRT, dest_email, Vagao, Balsa,
*)

    case QrNota_EntSITUACAO.Value of
      0:  // 5 registro
      begin
        Status        := 0;
        infProt_cStat := 0;
        infCanc_cStat := 0;
      end;
      1: // 5964 registros
      begin
        Status        := 100;
        infProt_cStat := 100;
        infCanc_cStat := 0;
      end;
{
      ?: // ?? registros
      begin
        Status        := 101;
        infProt_cStat := 100;
        infCanc_cStat := 101;
      end;
}
      else  // nenhum registro
      begin
        Status        := 0;
        infProt_cStat := 0;
        infCanc_cStat := 0;
        Geral.MB_Aviso('Situa��o n�o implementada! (Nota_Ent)');
      end;
    end;

    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecaba', False, [
    'IDCtrl', (*'LoteEnv', 'versao',
    'Id', 'ide_cUF', 'ide_cNF',*)
    'ide_natOp', (*'ide_indPag',*) 'ide_mod',
    'ide_serie', 'ide_nNF', 'ide_dEmi',
    'ide_dSaiEnt', (*'ide_tpNF', 'ide_cMunFG',
    'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
    'ide_tpAmb', 'ide_finNFe', 'ide_procEmi',
    'ide_verProc',*) 'emit_CNPJ', 'emit_CPF',
    'emit_xNome', 'emit_xFant', 'emit_xLgr',
    'emit_nro', 'emit_xCpl', 'emit_xBairro',
    'emit_cMun', 'emit_xMun', 'emit_UF',
    'emit_CEP', 'emit_cPais', 'emit_xPais',
    'emit_fone', 'emit_IE', 'emit_IEST',
    'emit_IM', 'emit_CNAE', 'dest_CNPJ',
    'dest_CPF', 'dest_xNome', 'dest_xLgr',
    'dest_nro', 'dest_xCpl', 'dest_xBairro',
    'dest_cMun', 'dest_xMun', 'dest_UF',
    'dest_CEP', 'dest_cPais', 'dest_xPais',
    'dest_fone', 'dest_IE', 'dest_ISUF',
    'ICMSTot_vBC', 'ICMSTot_vICMS', 'ICMSTot_vBCST',
    'ICMSTot_vST', 'ICMSTot_vProd', 'ICMSTot_vFrete',
    'ICMSTot_vSeg', (*'ICMSTot_vDesc', 'ICMSTot_vII',*)
    'ICMSTot_vIPI', (*'ICMSTot_vPIS', 'ICMSTot_vCOFINS',*)
    'ICMSTot_vOutro', 'ICMSTot_vNF', (*'ISSQNtot_vServ',
    'ISSQNtot_vBC', 'ISSQNtot_vISS', 'ISSQNtot_vPIS',
    'ISSQNtot_vCOFINS', 'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS',
    'RetTrib_vRetCSLL', 'RetTrib_vBCIRRF', 'RetTrib_vIRRF',
    'RetTrib_vBCRetPrev', 'RetTrib_vRetPrev',*) 'ModFrete',
    (*'Transporta_CNPJ', 'Transporta_CPF', 'Transporta_XNome',
    'Transporta_IE', 'Transporta_XEnder', 'Transporta_XMun',
    'Transporta_UF', 'RetTransp_vServ', 'RetTransp_vBCRet',
    'RetTransp_PICMSRet', 'RetTransp_vICMSRet', 'RetTransp_CFOP',
    'RetTransp_CMunFG', 'VeicTransp_Placa', 'VeicTransp_UF',
    'VeicTransp_RNTC', 'Cobr_Fat_nFat', 'Cobr_Fat_vOrig',
    'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq', 'InfAdic_InfAdFisco',*)
    'InfAdic_InfCpl', (*'Exporta_UFEmbarq', 'Exporta_XLocEmbarq',
    'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',*)
    'Status', (*'infProt_Id', 'infProt_tpAmb',
    'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
    'infProt_digVal',*) 'infProt_cStat', (*'infProt_xMotivo',
    'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
    'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',*)
    'infCanc_cStat', (*'infCanc_xMotivo', 'infCanc_cJust',
    'infCanc_xJust', '_Ativo_', 'FisRegCad',
    'CartEmiss', 'TabelaPrc', 'CondicaoPg',
    'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
    'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
    'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
    'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
    'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
    'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
    'DataFiscal', (*'protNFe_versao', 'retCancNFe_versao',
    'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta', 'SINTEGRA_ExpNat',
    'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta', 'SINTEGRA_ExpConhNum',
    'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip', 'SINTEGRA_ExpPais',
    'SINTEGRA_ExpAverDta',*) 'CodInfoEmit', 'CodInfoDest',
    (*'CriAForca',*) 'ide_hSaiEnt', (*'ide_dhCont',
    'ide_xJust', 'emit_CRT', 'dest_email',
    'Vagao', 'Balsa',*) 'CodInfoTrsp'(*,
    'DataEstoq'*), 'Antigo'], [
    'FatID', 'FatNum', 'Empresa'], [
    IDCtrl, (*LoteEnv, versao,
    Id, ide_cUF, ide_cNF,*)
    ide_natOp, (*ide_indPag,*) ide_mod,
    ide_serie, ide_nNF, ide_dEmi,
    ide_dSaiEnt, (*ide_tpNF, ide_cMunFG,
    ide_tpImp, ide_tpEmis, ide_cDV,
    ide_tpAmb, ide_finNFe, ide_procEmi,
    ide_verProc,*) emit_CNPJ, emit_CPF,
    emit_xNome, emit_xFant, emit_xLgr,
    emit_nro, emit_xCpl, emit_xBairro,
    emit_cMun, emit_xMun, emit_UF,
    emit_CEP, emit_cPais, emit_xPais,
    emit_fone, emit_IE, emit_IEST,
    emit_IM, emit_CNAE, dest_CNPJ,
    dest_CPF, dest_xNome, dest_xLgr,
    dest_nro, dest_xCpl, dest_xBairro,
    dest_cMun, dest_xMun, dest_UF,
    dest_CEP, dest_cPais, dest_xPais,
    dest_fone, dest_IE, dest_ISUF,
    ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,
    ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete,
    ICMSTot_vSeg, (*ICMSTot_vDesc, ICMSTot_vII,*)
    ICMSTot_vIPI, (*ICMSTot_vPIS, ICMSTot_vCOFINS,*)
    ICMSTot_vOutro, ICMSTot_vNF, (*ISSQNtot_vServ,
    ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,
    ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS,
    RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,
    RetTrib_vBCRetPrev, RetTrib_vRetPrev,*) ModFrete,
    (*Transporta_CNPJ, Transporta_CPF, Transporta_XNome,
    Transporta_IE, Transporta_XEnder, Transporta_XMun,
    Transporta_UF, RetTransp_vServ, RetTransp_vBCRet,
    RetTransp_PICMSRet, RetTransp_vICMSRet, RetTransp_CFOP,
    RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
    VeicTransp_RNTC, Cobr_Fat_nFat, Cobr_Fat_vOrig,
    Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,*)
    InfAdic_InfCpl, (*Exporta_UFEmbarq, Exporta_XLocEmbarq,
    Compra_XNEmp, Compra_XPed, Compra_XCont,*)
    Status, (*infProt_Id, infProt_tpAmb,
    infProt_verAplic, infProt_dhRecbto, infProt_nProt,
    infProt_digVal,*) infProt_cStat, (*infProt_xMotivo,
    infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
    infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,*)
    infCanc_cStat, (*infCanc_xMotivo, infCanc_cJust,
    infCanc_xJust, _Ativo_, FisRegCad,
    CartEmiss, TabelaPrc, CondicaoPg,
    FreteExtra, SegurExtra, ICMSRec_pRedBC,
    ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
    IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
    IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
    PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
    COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
    DataFiscal, (*protNFe_versao, retCancNFe_versao,
    SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_ExpNat,
    SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum,
    SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais,
    SINTEGRA_ExpAverDta,*) CodInfoEmit, CodInfoDest,
    (*CriAForca,*) ide_hSaiEnt, (*ide_dhCont,
    ide_xJust, emit_CRT, dest_email,
    Vagao, Balsa,*) CodInfoTrsp(*,
    DataEstoq*), Antigo], [
    FatID, FatNum, Empresa], False) then
    begin
      qVol  := QrNota_EntQTDADE.Value;
      esp   := QrNota_EntESPECIE.Value;
      marca := QrNota_EntMARCA.Value;
      nVol  := QrNota_EntNUMMARC.Value;
      pesoB := QrNota_EntPESOBRUTO.Value;
      pesoL := QrNota_EntPESOLIQUID.Value;
      FControle := FControle + 1;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabxvol', False, [
      'qVol', 'esp', 'marca',
      'nVol', 'pesoL', 'pesoB'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      qVol, esp, marca,
      nVol, pesoL, pesoB], [
      FatID, FatNum, Empresa, FControle], True) then ;
      //
      QrItens_Ent.Close;
      QrItens_Ent.Params[00].AsString := QrNota_EntNUMERO.Value;
      QrItens_Ent.Params[01].AsString := QrNota_EntCNPJ.Value;
      QrItens_Ent . O p e n ;
      //
      QrItens_Ent.First;
      while not QrItens_Ent.Eof do
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, FAtuData_TXT + 'Pesquisando  NF de entrada: ' +
        QrNota_EntNUMERO.Value + ' de ' + QrNota_EntCNPJ.Value +
            ' Item: ' + FormatFloat('0', QrItens_EntItem.Value));
        nItem          := QrItens_EntITEM.Value;
        prod_cProd     := QrItens_EntCODPROD.Value;
        prod_xProd     := QrItens_EntDESCPROD.Value;
        prod_CFOP      := Geral.SoNumero_TT(QrNota_EntCFOP.Value);
        prod_uCom      := QrItens_EntUNIDADE.Value;
        prod_qCom      := QrItens_EntQTDADE.Value;
        prod_vUnCom    := QrItens_EntVALORUNIT.Value;
        prod_vProd     := QrItens_EntVALORTOT.Value;
        MeuID          := 0;
        Nivel1         := 0; // N�o h� relacionamento!
        //
        try
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
        'prod_cProd', (*'prod_cEAN',*) 'prod_xProd',
        (*'prod_NCM', 'prod_EXTIPI', 'prod_genero',*)
        'prod_CFOP', 'prod_uCom', 'prod_qCom',
        'prod_vUnCom', 'prod_vProd', (*'prod_cEANTrib',
        'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
        'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
        'prod_vOutro', 'prod_indTot', 'prod_xPed',
        'prod_nItemPed', 'Tem_IPI', '_Ativo_',
        'InfAdCuztm', 'EhServico', 'ICMSRec_pRedBC',
        'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
        'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
        'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
        'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
        'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
        'MeuID', 'Nivel1'], [
        'FatID', 'FatNum', 'Empresa', 'nItem'], [
        prod_cProd, (*prod_cEAN,*) prod_xProd,
        (*prod_NCM, prod_EXTIPI, prod_genero,*)
        prod_CFOP, prod_uCom, prod_qCom,
        prod_vUnCom, prod_vProd, (*prod_cEANTrib,
        prod_uTrib, prod_qTrib, prod_vUnTrib,
        prod_vFrete, prod_vSeg, prod_vDesc,
        prod_vOutro, prod_indTot, prod_xPed,
        prod_nItemPed, Tem_IPI, _Ativo_,
        InfAdCuztm, EhServico, ICMSRec_pRedBC,
        ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
        IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
        IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
        PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
        COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
        MeuID, Nivel1], [
        FatID, FatNum, Empresa, nItem], True, '', False, True) then
        begin
          SITTRIB     := Geral.SoNumero_TT(QrItens_EntSITTRIB.Value);
          SITTRIB     := FormatFloat('000', Geral.IMV(SITTRIB));
          ICMS_Orig   := SITTRIB[1];
          ICMS_CST    := Copy(SITTRIB, 2);
          ICMS_vBC    := QrItens_EntVALORTOT.Value;
          ICMS_pICMS  := QrItens_EntICMS.Value;
          ICMS_vICMS  := 0; // ???

          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsn', False, [
          'ICMS_Orig', 'ICMS_CST', (*'ICMS_modBC',
          'ICMS_pRedBC',*) 'ICMS_vBC', 'ICMS_pICMS',
          'ICMS_vICMS'(*, 'ICMS_modBCST', 'ICMS_pMVAST',
          'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
          'ICMS_vICMSST', '_Ativo_', 'ICMS_CSOSN',
          'ICMS_UFST', 'ICMS_pBCOp', 'ICMS_vBCSTRet',
          'ICMS_vICMSSTRet', 'ICMS_motDesICMS', 'ICMS_pCredSN',
          'ICMS_vCredICMSSN'*)], [
          'FatID', 'FatNum', 'Empresa', 'nItem'], [
          ICMS_Orig, ICMS_CST, (*ICMS_modBC,
          ICMS_pRedBC,*) ICMS_vBC, ICMS_pICMS,
          ICMS_vICMS(*, ICMS_modBCST, ICMS_pMVAST,
          ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
          ICMS_vICMSST, _Ativo_, ICMS_CSOSN,
          ICMS_UFST, ICMS_pBCOp, ICMS_vBCSTRet,
          ICMS_vICMSSTRet, ICMS_motDesICMS, ICMS_pCredSN,
          ICMS_vCredICMSSN*)], [
          FatID, FatNum, Empresa, nItem], True) then ;
          //
          IPI_pIPI    := QrItens_EntIPI.Value;
          IPI_vIPI    := QrItens_EntVALORIPI.Value;
          if (IPI_pIPI <> 0) or (IPI_vIPI <> 0) then
          begin
            IPI_vBC := QrItens_EntVALORTOT.Value;
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitso', False, [
            (*'IPI_clEnq', 'IPI_CNPJProd', 'IPI_cSelo',
            'IPI_qSelo', 'IPI_cEnq', 'IPI_CST',*)
            'IPI_vBC', (*'IPI_qUnid', 'IPI_vUnid',*)
            'IPI_pIPI', 'IPI_vIPI'(*, '_Ativo_'*)], [
            'FatID', 'FatNum', 'Empresa', 'nItem'], [
            (*IPI_clEnq, IPI_CNPJProd, IPI_cSelo,
            IPI_qSelo, IPI_cEnq, IPI_CST,*)
            IPI_vBC, (*IPI_qUnid, IPI_vUnid,*)
            IPI_pIPI, IPI_vIPI(*, _Ativo_*)], [
            FatID, FatNum, Empresa, nItem], True) then ;
          end;
          //
        end;
        //
        except
           Memo1.Lines.Add('Erro nfeitsi > ' + FormatFloat('0', FatNum) + '#' + FormatFloat('0', nItem));
        end;
        QrItens_Ent.Next;
      end;
    end;
    end; // Situacao = 1
    //
    //
    // Estoque
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, FAtuData_TXT + ' Incluindo no estoque: ' +
    QrNota_EntNUMERO.Value);
    Tipo  := FatID; // Parei aqui 151 = uso e consumo e pq?
    //
    DataHora    := Geral.FDT(QrNota_EntDATACONTAB.Value, 1);
    OriCodi     := FatNum;//Geral.IMV(QrNota_EntNUMERO.Value);
    OriCnta     := 0;
    OriPart     := 0;
    StqCenCad   := 1;
    //
    QrItens_Ent.Close;
    QrItens_Ent.Params[00].AsString := QrNota_EntNUMERO.Value;
    QrItens_Ent.Params[01].AsString := QrNota_EntCNPJ.Value;
    QrItens_Ent . O p e n ;
    while not QrItens_Ent.Eof do
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, FAtuData_TXT +
      ' Incluindo no estoque: ' + QrItens_EntNUMERO.Value +
      ' Item: ' + FormatFloat('0', QrItens_EntITEM.Value));
      //
      // Localiza Gragrux
      QrGGX.Close;
      QrGGX.Params[00].AsString := QrItens_EntCODPROD.Value;
      QrGGX.Params[01].AsString := QrItens_EntDESCPROD.Value;
      QrGGX . O p e n ;
      if QrGGXNivel1.Value > -900000 then
        GraGruX := ObtemProximoGGXNeg(QrItens_EntCODPROD.Value, QrItens_EntDESCPROD.Value)
      else
        GraGruX := QrGGXNivel1.Value;

{
      GraGruX_Txt := Trim(QrItens_EntCODPROD.Value);
      if GraGruX_TXT <> Geral.SoNumero_TT(GraGruX_TXT) then
        GraGruX := 0
      else
        GraGruX := Geral.IMV(GraGruX_Txt);
      //
}
      Qtde        := QrItens_EntQTDADE.Value * Fator;
      Pecas       := QrItens_EntQTDADE.Value * Fator;
      Peso        := QrItens_EntQTDADE.Value * Fator;
      CustoAll    := QrItens_EntVALORTOT.Value;
      Baixa       := 1;
      //
      FIDCtrl_SMI := FIDCtrl_SMI + 1;
      IDCtrl      := FIDCtrl_SMI;
      OriCtrl     := FIDCtrl_SMI;
      //? := UMyMod.BuscaEmLivreY_Def('stqmovitsa', ''IDCtrl', ImgTipo.SQLType, CodAtual);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
      'DataHora', 'Tipo', 'OriCodi',
      'OriCtrl', 'OriCnta', 'OriPart',
      'Empresa', 'StqCenCad', 'GraGruX',
      'Qtde', 'Pecas', 'Peso',
      (*'AreaM2', 'AreaP2', 'FatorClas',
      'QuemUsou', 'Retorno', 'ParTipo',
      'ParCodi', 'DebCtrl', 'SMIMultIns',*)
      'CustoAll', (*'ValorAll', 'GrupoBal',*)
      'Baixa'], [
      'IDCtrl'], [
      DataHora, Tipo, OriCodi,
      OriCtrl, OriCnta, OriPart,
      Empresa, StqCenCad, GraGruX,
      Qtde, Pecas, Peso,
      (*AreaM2, AreaP2, FatorClas,
      QuemUsou, Retorno, ParTipo,
      ParCodi, DebCtrl, SMIMultIns,*)
      CustoAll, (*ValorAll, GrupoBal,*)
      Baixa], [
      IDCtrl], False) then ;
      //
      QrItens_Ent.Next;
    end;
    QrNota_Ent.Next;
  end;
end;

procedure TFmImportaFoxPro.ImportaNotaFisc();
var
  Continua: Boolean;
  //SQLType: TSQLType;
  //
  CR: String;
{
  FatNum, FatID, Empresa, CodInfoTrsp, OrdServ, Situacao, IDCtrl, ide_nNF,
  ModFrete: Integer;
  ide_natOp, ide_dEmi, ide_dSaiEnt, ide_hSaiEnt,
  InfAdic_InfCpl: String;
  ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST, ICMSTot_vProd,
  ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro, ICMSTot_vNF: Double;
  //
  qVol, pesoB, pesoL: Double;
  nVol, esp, marca: String;
}
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _NotaFisc;                                    ' + sLineBreak +
    'CREATE TABLE _NotaFisc (                                           ' + sLineBreak +
    '  NUMERO               Varchar(6)                                 ,' + sLineBreak +
    '  ORDSERV              Varchar(6)                                 ,' + sLineBreak +
    '  SITUACAO             Integer                                    ,' + sLineBreak +
    '  NATOPER              Varchar(6)                                 ,' + sLineBreak +
    '  EMISSAO              Date                                       ,' + sLineBreak +
    '  SAIDA                Date                                       ,' + sLineBreak +
    '  HORA                 Varchar(5)                                 ,' + sLineBreak +
    '  BASEICMS             Float                                      ,' + sLineBreak +
    '  VLRICMS              Float                                      ,' + sLineBreak +
    '  BCICMSSUB            Float                                      ,' + sLineBreak +
    '  VLRICMSSUB           Float                                      ,' + sLineBreak +
    '  TOTALPROD            Float                                      ,' + sLineBreak +
    '  VLRFRETE             Float                                      ,' + sLineBreak +
    '  VLRSEGURO            Float                                      ,' + sLineBreak +
    '  OUTRAS               Float                                      ,' + sLineBreak +
    '  IPI                  Float                                      ,' + sLineBreak +
    '  TOTAL                Float                                      ,' + sLineBreak +
    '  TRANSPORTA           Varchar(3)                                 ,' + sLineBreak +
    '  FRETE                Integer                                    ,' + sLineBreak +
    '  PLACAS               Varchar(8)                                 ,' + sLineBreak +
    '  QTDADE               Integer                                    ,' + sLineBreak +
    '  ESPECIE              Varchar(20)                                ,' + sLineBreak +
    '  MARCA                Varchar(20)                                ,' + sLineBreak +
    '  NUMPROD              Varchar(25)                                ,' + sLineBreak +
    '  PESOBRUT             Float                                      ,' + sLineBreak +
    '  PESOLIQ              Float                                      ,' + sLineBreak +
    '  OBS                  Text                                       ,' + sLineBreak +
    '  CORPO                Text                                       ,' + sLineBreak +
    '  CR                   char(1)                                    ,' + sLineBreak +
    '  TIPODESC             Integer                                    ,' + sLineBreak +
    '  NOTADESC             Float                                      ,' + sLineBreak +
    '  J10UKEY              Varchar(20)                                 ' + sLineBreak +
    ') TYPE=MyISAM');
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsNotaFisc;
    TbNotaFisc.Close;
    TbNotaFisc.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbNotaFisc . O p e n ;
    TbNotaFisc.First;
    PB1.Position := 0;
    PB1.Max := TbNotaFisc.RecordCount;
    while not TbNotaFisc.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando NotaFisc ' +
        TbNotaFiscNUMERO.Value);
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
        (*
        if Geral.IMV(TbNotaFiscNUMERO.Value) = 859 then
          CR := ' '
        else
          CR := IntToStr(TbNotaFiscCR.Value);
        *)

        CR := TbNotaFisc.FieldByName('CR').AsString;
        if Trim(CR) = '' then
           Memo2.lines.Add('Nota ' + TbNotaFiscNUMERO.Value + ' CR = "' + CR + '"');
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_notafisc', False, [
        'NUMERO', 'ORDSERV', 'SITUACAO',
        'NATOPER', 'EMISSAO', 'SAIDA',
        'HORA', 'BASEICMS', 'VLRICMS',
        'BCICMSSUB', 'VLRICMSSUB', 'TOTALPROD',
        'VLRFRETE', 'VLRSEGURO', 'OUTRAS',
        'IPI', 'TOTAL', 'TRANSPORTA',
        'FRETE', 'PLACAS', 'QTDADE',
        'ESPECIE', 'MARCA', 'NUMPROD',
        'PESOBRUT', 'PESOLIQ', 'OBS',
        'CORPO', 'CR', 'TIPODESC',
        'NOTADESC', 'J10UKEY'], [
        ], [
        TbNotaFiscNUMERO.Value, TbNotaFiscORDSERV.Value, TbNotaFiscSITUACAO.Value,
        TbNotaFiscNATOPER.Value, TbNotaFiscEMISSAO.Value, TbNotaFiscSAIDA.Value,
        TbNotaFiscHORA.Value, TbNotaFiscBASEICMS.Value, TbNotaFiscVLRICMS.Value,
        TbNotaFiscBCICMSSUB.Value, TbNotaFiscVLRICMSSUB.Value, TbNotaFiscTOTALPROD.Value,
        TbNotaFiscVLRFRETE.Value, TbNotaFiscVLRSEGURO.Value, TbNotaFiscOUTRAS.Value,
        TbNotaFiscIPI.Value, TbNotaFiscTOTAL.Value, TbNotaFiscTRANSPORTA.Value,
        TbNotaFiscFRETE.Value, TbNotaFiscPLACAS.Value, TbNotaFiscQTDADE.Value,
        TbNotaFiscESPECIE.Value, TbNotaFiscMARCA.Value, TbNotaFiscNUMPROD.Value,
        TbNotaFiscPESOBRUT.Value, TbNotaFiscPESOLIQ.Value, TbNotaFiscOBS.Value,
        TbNotaFiscCORPO.Value, (*TbNotaFiscCR.Value*)CR, TbNotaFiscTIPODESC.Value,
        TbNotaFiscNOTADESC.Value, TbNotaFiscJ10UKEY.Value], [
        ], False) then ;
      end;
      //
      TbNotaFisc.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;


procedure TFmImportaFoxPro.ImportaNota_Ent();
const
  Customizad = 0;
  InfAdCuztm = 0;
  MedidaC    = 0.0000;
  MedidaL    = 0.0000;
  MedidaA    = 0.0000;
  MedidaE    = 0.000000;
  PercCustom = 0.00;
var
  Continua: Boolean;
  //SQLType: TSQLType;
  //
{
  Codigo, GraGruX, Ordem, Controle, Multi: Integer;
  PrecoO, PrecoR, QuantP, QuantC, QuantV, ValBru, DescoP, DescoV, ValLiq,
  PrecoF: Double;
  Referencia, GGX_TXT: String;
}
begin
  Screen.Cursor := crHourGlass;
  try
    LimparDadosTabela(
    'DROP TABLE IF EXISTS _Nota_Ent;                                    ' + sLineBreak +
    'CREATE TABLE _Nota_Ent (                                           ' + sLineBreak +
    '  NUMERO               Varchar(6)                                 ,' + sLineBreak +
    '  CNPJ                 Varchar(14)                                ,' + sLineBreak +
    '  INSCEST              Varchar(14)                                ,' + sLineBreak +
    '  CFOP                 Varchar(5)                                 ,' + sLineBreak +
    '  RAZAO                Varchar(60)                                ,' + sLineBreak +
    '  ENDERECO             Varchar(60)                                ,' + sLineBreak +
    '  BAIRRO               Varchar(40)                                ,' + sLineBreak +
    '  CEP                  Varchar(8)                                 ,' + sLineBreak +
    '  MUNICIO              Varchar(50)                                ,' + sLineBreak +
    '  FONE                 Varchar(14)                                ,' + sLineBreak +
    '  UF                   Varchar(2)                                 ,' + sLineBreak +
    '  EMISSAO              Date                                       ,' + sLineBreak +
    '  BASEICMS             Float                                      ,' + sLineBreak +
    '  VALORICMS            Float                                      ,' + sLineBreak +
    '  BASESUB              Float                                      ,' + sLineBreak +
    '  VALORSUB             Float                                      ,' + sLineBreak +
    '  VALORPROD            Float                                      ,' + sLineBreak +
    '  FRETE                Float                                      ,' + sLineBreak +
    '  SEGURO               Float                                      ,' + sLineBreak +
    '  OUTRAS               Float                                      ,' + sLineBreak +
    '  IPI                  Float                                      ,' + sLineBreak +
    '  TOTAL                Float                                      ,' + sLineBreak +
    '  QTDADE               Integer                                    ,' + sLineBreak +
    '  ESPECIE              Varchar(10)                                ,' + sLineBreak +
    '  MARCA                Varchar(10)                                ,' + sLineBreak +
    '  NUMMARC              Varchar(12)                                ,' + sLineBreak +
    '  PESOBRUTO            Float                                      ,' + sLineBreak +
    '  PESOLIQUID           Float                                      ,' + sLineBreak +
    '  DATACONTAB           Date                                       ,' + sLineBreak +
    '  SITUACAO             Integer                                    ,' + sLineBreak +
    '  OBSTIT               Text                                       ,' + sLineBreak +
    '  CREDICMS             Integer                                    ,' + sLineBreak +
    '  CREDIPI              Integer                                    ,' + sLineBreak +
    '  FRETE_CONT           Integer                                     ' + sLineBreak +
    ') TYPE=MyISAM');

    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsNota_Ent;
    TbNota_Ent.Close;
    TbNota_Ent.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbNota_Ent . O p e n ;
    TbNota_Ent.First;
    PB1.Position := 0;
    PB1.Max := TbNota_Ent.RecordCount;
    //Controle := 0;
    while not TbNota_Ent.Eof do
    begin
      AtualizaTempo();
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando Nota_Ent ' +
        TbNota_EntNUMERO.Value);
      //
      Continua := True;
      //SQLType := stIns;
      if Continua then
      begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, '_nota_ent', False, [
      'NUMERO', 'CNPJ', 'INSCEST',
      'CFOP', 'RAZAO', 'ENDERECO',
      'BAIRRO', 'CEP', 'MUNICIO',
      'FONE', 'UF', 'EMISSAO',
      'BASEICMS', 'VALORICMS', 'BASESUB',
      'VALORSUB', 'VALORPROD', 'FRETE',
      'SEGURO', 'OUTRAS', 'IPI',
      'TOTAL', 'QTDADE', 'ESPECIE',
      'MARCA', 'NUMMARC', 'PESOBRUTO',
      'PESOLIQUID', 'DATACONTAB', 'SITUACAO',
      'OBSTIT', 'CREDICMS', 'CREDIPI',
      'FRETE_CONT'], [
      ], [
      TbNota_EntNUMERO.Value, TbNota_EntCNPJ.Value, TbNota_EntINSCEST.Value,
      TbNota_EntCFOP.Value, TbNota_EntRAZAO.Value, TbNota_EntENDERECO.Value,
      TbNota_EntBAIRRO.Value, TbNota_EntCEP.Value, TbNota_EntMUNICIO.Value,
      TbNota_EntFONE.Value, TbNota_EntUF.Value, TbNota_EntEMISSAO.Value,
      TbNota_EntBASEICMS.Value, TbNota_EntVALORICMS.Value, TbNota_EntBASESUB.Value,
      TbNota_EntVALORSUB.Value, TbNota_EntVALORPROD.Value, TbNota_EntFRETE.Value,
      TbNota_EntSEGURO.Value, TbNota_EntOUTRAS.Value, TbNota_EntIPI.Value,
      TbNota_EntTOTAL.Value, TbNota_EntQTDADE.Value, TbNota_EntESPECIE.Value,
      TbNota_EntMARCA.Value, TbNota_EntNUMMARC.Value, TbNota_EntPESOBRUTO.Value,
      TbNota_EntPESOLIQUID.Value, TbNota_EntDATACONTAB.Value, TbNota_EntSITUACAO.Value,
      TbNota_EntOBSTIT.Value, TbNota_EntCREDICMS.Value, TbNota_EntCREDIPI.Value,
      TbNota_EntFRETE_CONT.Value], [
      ], False) then ;
      //
{
      SQLType := stIns;
      //
      // TbNota_EntMEDIDA.Value ?
      // TbNota_EntPEAS.Value ?
      Codigo     := Geral.IMV(TbNota_EntPEDIDO.Value);
      GGX_TXT := Trim(Geral.SoNumero_TT(TbNota_EntPRODUTO.Value));
      if GGX_TXT <> Trim(TbNota_EntPRODUTO.Value) then
      begin
        Memo1.Lines.Add('Nota_Ent: produto inv�lido > ' + TbNota_EntPRODUTO.Value);
        GGX_TXT  := '0';
      end;
      //
      GraGruX    := Geral.IMV(GGX_TXT);
      PrecoO     := TbNota_EntVALOR.Value;
      PrecoR     := TbNota_EntVALOR.Value;
      QuantP     := TbNota_EntQTDADE.Value;
      QuantC     := 0;
      QuantV     := TbNota_EntQTDADE.Value;
      ValBru     := TbNota_EntTOTAL.Value;
      DescoP     := 0;
      DescoV     := 0;
      ValLiq     := TbNota_EntTOTAL.Value;
      PrecoF     := TbNota_EntVALOR.Value;
      Ordem      := TbNota_EntORDEM.Value;
      Referencia := TbNota_EntCODPESQ.Value;
      Controle   := Controle + 1;
      Multi      := Controle;

(*
if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pedivdaits', False, [
'Codigo', 'GraGruX', 'PrecoO',
'PrecoR', 'QuantP', 'QuantC',
'QuantV', 'ValBru', 'DescoP',
'DescoV', 'ValLiq', 'PrecoF',
'MedidaC', 'MedidaL', 'MedidaA',
'MedidaE', 'PercCustom', 'Customizad',
'InfAdCuztm', 'Ordem', 'Referencia',
'Multi'], [
'Controle'], [
Codigo, GraGruX, PrecoO,
PrecoR, QuantP, QuantC,
QuantV, ValBru, DescoP,
DescoV, ValLiq, PrecoF,
MedidaC, MedidaL, MedidaA,
MedidaE, PercCustom, Customizad,
InfAdCuztm, Ordem, Referencia,
Multi], [
Controle], True) then ;
*)
}
      //
      end;
      //
      TbNota_Ent.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaTranspor();
const
  Prefixo  = 'T';
  Fornece3 = 'V';
var
  EUF, Codigo, CodUsu, Tipo: Integer;
  RazaoSocial, Fantasia, CNPJ, ERua, EBairro, ECidade,
  ECEP, ETe1, EFax, ECel, EEmail, IE,
  Observacoes: String;
  //
  Antigo: String;
  SQLType: TSQLType;
  Continua: Boolean;
  //
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsTranspor;
    TbTranspor.Close;
    TbTranspor.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbTranspor . O p e n ;
    TbTranspor.First;
    PB1.Position := 0;
    PB1.Max := TbTranspor.RecordCount;
    Antigo := '';
    while not TbTranspor.Eof do
    begin
      AtualizaTempo();
      //Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando Transpor ' +
        TbTransporCODIGO.Value);
      //
      Continua := True;
      Antigo        := Geral.SoNumero_TT(TbTransporCODIGO.Value);
      if LocalizaAntigoStr('entidades', 'Antigo', Prefixo, Antigo) then
      begin
        Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de Transpor ' +
        Antigo + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        SQLType := stUpd;
      end else SQLType := stIns;
      if Continua then
      begin
        Codigo := Geral.IMV(Trim(Geral.SoNumero_TT(TbTransporCODIGO.Value))) +
        FIniTrans - 1;
        //
        {
        ENumero       := '';
        ECompl        := '';
        PNumero       := '';
        PCompl        := '';
        }
        ERua          := '';
        EBairro       := '';
        ECidade       := '';
        //EUF           := 0;
        ECEP          := '';
        ETe1          := '';
        EFax          := '';
        ECel          := '';
        EEmail        := '';
        //
{
        PRua          := '';
        PBairro       := '';
        PCidade       := '';
        PUF           := 0;
        PCEP          := '';
        PTe1          := '';
        PFax          := '';
        PCel          := '';
        PEmail        := '';
}
        //
{
        Nome          := TbTransporNOME.Value;
        Apelido       := TbTransporNOME.Value;
}
        RazaoSocial   := TbTransporRAZO.Value;
        Fantasia      := TbTransporNOME.Value;
        CNPJ          := Geral.SoNumero_TT(TbTransporCNPJ.Value);
{
        Nome          := TbTransporNOME.Value;
        Apelido       := TbTransporNOME.Value;
        CPF           := Geral.SoNumero_TT(TbTransporCPF.Value);
        if Length(CNPJ) > 11 then
        begin
}
          //
          Tipo          := 0;
          ERua          := TbTransporENDEREO.Value;
          EBairro       := TbTransporBAIRRO.Value;
          ECidade       := TbTransporCIDADE.Value;
          EUF           := Geral.GetCodigoUF_da_SiglaUF(TbTransporUF.Value);
          ECEP          := TbTransporCEP.Value;
          ETe1          := Geral.SoNumero_TT(TbTransporFONE.Value);
          EFax          := Geral.SoNumero_TT(TbTransporFAX.Value);
          ECel          := Geral.SoNumero_TT(TbTransporCELULAR.Value);
          EEmail        := TbTransporEMAIL.Value;
          IE            := Geral.SoNumero_TT(TbTransporICMS.Value);
          {
          ENumero       := TbTransporNUMEND.Value;
          ECompl        := TbTransporCOMPLEND.Value;
          PTe1          := Geral.SoNumero_TT(TbTransporFONECONT.Value);
          PCel          := Geral.SoNumero_TT(TbTransporCELCONT.Value);
          PEmail        := TbTransporEMAILCONT.Value;
          RG            := '';
          }
          //
{
        end else
        begin
          Tipo          := 1;
          //
          PRua          := TbTransporENDEREO.Value;
          PBairro       := TbTransporBAIRRO.Value;
          PCidade       := TbTransporCIDADE.Value;
          PUF           := Geral.GetCodigoUF_da_SiglaUF(TbTransporUF.Value);
          PCEP          := TbTransporCEP.Value;
          PTe1          := Geral.SoNumero_TT(TbTransporFONE.Value);
          PFax          := Geral.SoNumero_TT(TbTransporFAX.Value);
          PCel          := Geral.SoNumero_TT(TbTransporCELULAR.Value);
          PEmail        := TbTransporEMAIL.Value;
          IE            := '';
          RG            := Geral.SoNumero_TT(TbTransporICMS.Value);
          PNumero       := TbTransporNUMEND.Value;
          PCompl        := TbTransporCOMPLEND.Value;
          ETe1          := Geral.SoNumero_TT(TbTransporFONECONT.Value);
          ECel          := Geral.SoNumero_TT(TbTransporCELCONT.Value);
          EEmail        := TbTransporEMAILCONT.Value;
        end;
        {
        URL           := TbTransporURL.Value;
        Prazo         := TbTransporPRAZO.Value;
        Transporta    := TbTransporTRANSPORTA.Value;
        Contato       := TbTransporCONTATO.Value;
        Transpor      := TbTransporTranspor.Value;
        Comissao      := TbTransporCOMISSAO.Value;
        Primeira      := Geral.FDT(TbTransporPrimeira.Value, 1);
        Ultima        := Geral.FDT(TbTransporUltima.Value, 1);
        Valor         := TbTransporValor.Value;
        NF            := TbTransporNF.Value;
        SERASA        := TbTransporSERASA.Value;
        DataSerasa    := Geral.FDT(TbTransporDATASERASA.Value, 1);
        Situacao      := TbTransporSITUAO.Value;
        Observacoes   := TbTransporOBS.Value;
        }
        //
        CodUsu := Codigo;

        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entidades', False, [
        'RazaoSocial', 'Fantasia',
        'CNPJ', 'IE',
        'ERua',
        'EBairro', 'ECidade', 'EUF',
        'ECEP', 'ETe1',
        'ECel',
        'EFax', 'EEmail',
        'Fornece3',
        'Observacoes',
        'Tipo', 'CodUsu',
        'Antigo'
        ], [
        'Codigo'], [
        RazaoSocial, Fantasia,
        CNPJ, IE,
        ERua,
        EBairro, ECidade, EUF,
        ECEP, ETe1,
        ECel,
        EFax, EEmail,
        Fornece3,
        Observacoes,
        Tipo, CodUsu,
        Prefixo + Antigo
        ], [
        Codigo], True) then ;
      end;
      //
      TbTranspor.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaUnidade();
const
  Prefixo = '';
var
  Codigo, CodUsu, Antigo: Integer;
  //
  Nome: String;
  SQLType: TSQLType;
  Continua: Boolean;
  //
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsUnidade;
    TbUnidade.Close;
    TbUnidade.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbUnidade . O p e n ;
    TbUnidade.First;
    PB1.Position := 0;
    PB1.Max := TbUnidade.RecordCount;
    //Antigo := -999999999;
    while not TbUnidade.Eof do
    begin
      AtualizaTempo();
      //Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando Unidade ' +
        FormatFloat('0', TbUnidadeCODIGO.Value));
      //
      Continua := True;
      Antigo   := TbUnidadeCODIGO.Value;
      if LocalizaAntigoInt('UnidMed', 'Codigo', Antigo) then
      begin
        Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de Unidade ' +
        FormatFloat('0', Antigo) + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        SQLType := stUpd;
      end else SQLType := stIns;
      if Continua then
      begin
        Codigo := TbUnidadeCODIGO.Value +
        FIniUnida - 1;
        //
        Nome := TbUnidadeDESCRIO.Value;
        //
        CodUsu := Codigo;

        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'UnidMed', False, [
        'CodUsu', 'Nome'], ['Codigo'
        ], [CodUsu, Nome], [Codigo], True) then ;
      end;
      //
      TbUnidade.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImportaVendedor();
const
  Prefixo = 'V';
  Fornece6 = 'V';
var
  EUF, PUF, Codigo, CodUsu, Tipo: Integer;
  RazaoSocial, Fantasia, CNPJ, ERua, EBairro, ECidade,
  ECEP, ETe1, EFax, ECel, EEmail,
  Nome, Apelido, CPF, PRua, PBairro, PCidade,
  PCEP, PTe1, PFAX, PCel, PEmail,
  Observacoes: String;
  //
  Antigo: String;
  SQLType: TSQLType;
  Continua: Boolean;
  //
begin
  Screen.Cursor := crHourGlass;
  try
    //PageControl1.ActivePageIndex := 1;
    DBGrid1.DataSource := DsVendedor;
    TbVendedor.Close;
    TbVendedor.DBFFileName := EdDir.Text + '\' + TbTabelasNome.Value;
    TbVendedor . O p e n ;
    TbVendedor.First;
    PB1.Position := 0;
    PB1.Max := TbVendedor.RecordCount;
    Antigo := '';
    while not TbVendedor.Eof do
    begin
      AtualizaTempo();
      //Ant := Antigo;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando vendedor ' +
        TbVendedorCODIGO.Value);
      //
      Continua := True;
      Antigo        := Geral.SoNumero_TT(TbVendedorCODIGO.Value);
      if LocalizaAntigoStr('entidades', 'Antigo', Prefixo, Antigo) then
      begin
        if Geral.IMV(Antigo) in ([2,3,12]) then
          Continua := True
        else
          Continua := Geral.MB_Pergunta(
        'Foi localizado um segundo c�digo para o cadastro de vendedor ' +
        Antigo + '.' + sLineBreak + 'Deseja alter�-lo?') = ID_YES;
        //
        SQLType := stUpd;
      end else SQLType := stIns;
      if Continua then
      begin
        Codigo := Geral.IMV(Trim(Geral.SoNumero_TT(TbVendedorCODIGO.Value))) +
        FIniVende - 1;
        //
        {
        ENumero       := '';                            ECompl        := '';
        PNumero       := '';
        PCompl        := '';
        }
        ERua          := '';
        EBairro       := '';
        ECidade       := '';
        EUF           := 0;
        ECEP          := '';
        ETe1          := '';
        EFax          := '';
        ECel          := '';
        EEmail        := '';
        //
        PRua          := '';
        PBairro       := '';
        PCidade       := '';
        PUF           := 0;
        PCEP          := '';
        PTe1          := '';
        PFax          := '';
        PCel          := '';
        PEmail        := '';
        //
        Nome          := TbVendedorNOME.Value;
        Apelido       := TbVendedorNOME.Value;
        RazaoSocial   := TbVendedorEMPRESA.Value;
        Fantasia      := TbVendedorEMPRESA.Value;
        CNPJ          := Geral.SoNumero_TT(TbVendedorCNPJ.Value);
        Nome          := TbVendedorNOME.Value;
        Apelido       := TbVendedorNOME.Value;
        CPF           := Geral.SoNumero_TT(TbVendedorCPF.Value);
        if Length(CNPJ) > 11 then
        begin
          //
          Tipo          := 0;
          ERua          := TbVendedorENDEREO.Value;
          EBairro       := TbVendedorBAIRRO.Value;
          ECidade       := TbVendedorCIDADE.Value;
          EUF           := Geral.GetCodigoUF_da_SiglaUF(TbVendedorUF.Value);
          ECEP          := TbVendedorCEP.Value;
          ETe1          := Geral.SoNumero_TT(TbVendedorFONE.Value);
          EFax          := Geral.SoNumero_TT(TbVendedorFAX.Value);
          ECel          := Geral.SoNumero_TT(TbVendedorCELULAR.Value);
          EEmail        := TbVendedorEMAIL.Value;
          {
          IE            := Geral.SoNumero_TT(TbVendedorICMS.Value);
          ENumero       := TbVendedorNUMEND.Value;
          ECompl        := TbVendedorCOMPLEND.Value;
          PTe1          := Geral.SoNumero_TT(TbVendedorFONECONT.Value);
          PCel          := Geral.SoNumero_TT(TbVendedorCELCONT.Value);
          PEmail        := TbVendedorEMAILCONT.Value;
          RG            := '';
          }
          //
        end else
        begin
          Tipo          := 1;
          //
          PRua          := TbVendedorENDEREO.Value;
          PBairro       := TbVendedorBAIRRO.Value;
          PCidade       := TbVendedorCIDADE.Value;
          PUF           := Geral.GetCodigoUF_da_SiglaUF(TbVendedorUF.Value);
          PCEP          := TbVendedorCEP.Value;
          PTe1          := Geral.SoNumero_TT(TbVendedorFONE.Value);
          PFax          := Geral.SoNumero_TT(TbVendedorFAX.Value);
          PCel          := Geral.SoNumero_TT(TbVendedorCELULAR.Value);
          PEmail        := TbVendedorEMAIL.Value;
          {
          IE            := '';
          RG            := Geral.SoNumero_TT(TbVendedorICMS.Value);
          PNumero       := TbVendedorNUMEND.Value;
          PCompl        := TbVendedorCOMPLEND.Value;
          ETe1          := Geral.SoNumero_TT(TbVendedorFONECONT.Value);
          ECel          := Geral.SoNumero_TT(TbVendedorCELCONT.Value);
          EEmail        := TbVendedorEMAILCONT.Value;
          }
        end;
        {
        URL           := TbVendedorURL.Value;
        Prazo         := TbVendedorPRAZO.Value;
        Transporta    := TbVendedorTRANSPORTA.Value;
        Contato       := TbVendedorCONTATO.Value;
        Vendedor      := TbVendedorVENDEDOR.Value;
        Comissao      := TbVendedorCOMISSAO.Value;
        Primeira      := Geral.FDT(TbVendedorPrimeira.Value, 1);
        Ultima        := Geral.FDT(TbVendedorUltima.Value, 1);
        Valor         := TbVendedorValor.Value;
        NF            := TbVendedorNF.Value;
        SERASA        := TbVendedorSERASA.Value;
        DataSerasa    := Geral.FDT(TbVendedorDATASERASA.Value, 1);
        Situacao      := TbVendedorSITUAO.Value;
        Observacoes   := TbVendedorOBS.Value;
        }
        //
        CodUsu := Codigo;

        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entidades', False, [
        'RazaoSocial', 'Fantasia',
        'CNPJ',
        'Nome', 'Apelido', 'CPF',
        'ERua',
        'EBairro', 'ECidade', 'EUF',
        'ECEP', 'ETe1',
        'ECel',
        'EFax', 'EEmail',
        'PRua',
        'PBairro',
        'PCidade', 'PUF', 'PCEP',
        'PTe1',
        'PCel', 'PFax',
        'PEmail',
        'Fornece6',
        'Observacoes',
        'Tipo', 'CodUsu',
        'Antigo'
        ], [
        'Codigo'], [
        RazaoSocial, Fantasia,
        CNPJ,
        Nome, Apelido, CPF,
        ERua,
        EBairro, ECidade, EUF,
        ECEP, ETe1,
        ECel,
        EFax, EEmail,
        PRua,
        PBairro,
        PCidade, PUF, PCEP,
        PTe1,
        PCel, PFax,
        PEmail,
        Fornece6,
        Observacoes,
        Tipo, CodUsu,
        Prefixo + Antigo
        ], [
        Codigo], True) then ;
      end;
      //
      TbVendedor.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaFoxPro.ImprimeLctOrf(Pagar: Boolean);
begin
  QrLctOrf.Close;
  QrLctOrf.SQL.Clear;
  QrLctOrf.SQL.Add('SELECT cta.Nome NO_GENERO, sgr.Nome NO_SUBGRUPO,');
  QrLctOrf.SQL.Add('cta.Nome NO_CAR, pg.*,');
  QrLctOrf.SQL.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI');
  if Pagar then
  begin
    FTituloLctOrf := 'Lan�amentos Orf�os de Contas a Pagar';
    QrLctOrf.SQL.Add('FROM _pagar pg');
  end else begin
    FTituloLctOrf := 'Lan�amentos Orf�os de Contas a Receber';
    QrLctOrf.SQL.Add('FROM _receber pg');
  end;
  QrLctOrf.SQL.Add('LEFT JOIN contas cta ON cta.Codigo=ITEMCUST');
  QrLctOrf.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
  QrLctOrf.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=pg.FORMAPGTO');
  QrLctOrf.SQL.Add('LEFT JOIN entidades cli ON cli.Antigo=CONCAT("C",pg.CLIENTE)');
  QrLctOrf.SQL.Add('WHERE sgr.Codigo <> pg.CENTCUST');
  QrLctOrf.SQL.Add('ORDER BY ITEMCUST, CENTCUST, DATA ');
  //
  QrLctOrf . O p e n ;
  MyObjects.frxMostra(frxLctOrf, FTituloLctOrf)
end;

function TFmImportaFoxPro.IncluiEntidadePeloCNPJ(CNPJ, RazaoSocial, Fantasia,
IE, Endereco, Bairro, CEP, Cidade, Fone, UF: String): Integer;
const
 Fornece1 = 'V';
 Fornece2 = 'V';
 Fornece3 = 'V';
 Fornece4 = 'V';
var
  Codigo, CodUsu, ELograd, ENumero, ECEP, EUF: Integer;
  ERua, ECompl, EBairro, ECidade, EPais, ETe1, Antigo: String;
  //Numero, xLogr, nLogr: String;
begin
  Result := 0;
  Codigo := UMyMod.BuscaEmLivreY_Def('Entidades', 'Codigo', stIns, 0);
  CodUsu := Codigo;
{
  Geral.SeparaEndereco(Endereco, Numero, ECompl, Bairro, xLogr, nLogr, ERua, False);
  ENumero := Geral.IMV(Numero);
}
  ELograd := 0;
  ERua    := Endereco;
  ENumero := 0;
  ECompl  := '';
  EBairro := Bairro;
  ECidade := Cidade;
  ECEP    := Geral.IMV(Geral.SoNumero_TT(CEP));
  ETe1    := Geral.SoNumero_TT(Fone);
  EUF     := Geral.GetCodigoUF_da_SiglaUF(UF);
  Antigo  := Geral.SoNumero_TT(CNPJ);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
  'CodUsu', 'RazaoSocial', 'Fantasia',
  (*'Respons1', 'Respons2', 'Pai',
  'Mae',*) 'CNPJ', 'IE',
  (*'NIRE', 'FormaSociet', 'Simples',
  'IEST', 'Atividade', 'Nome',
  'Apelido', 'CPF', 'CPF_Pai',
  'CPF_Conjuge', 'RG', 'SSP',
  'DataRG', 'CidadeNatal', 'EstCivil',
  'UFNatal', 'Nacionalid',*) 'ELograd',
  'ERua', 'ENumero', 'ECompl',
  'EBairro', 'ECidade', 'EUF',
  'ECEP', 'EPais', 'ETe1',
  (*'Ete2', 'Ete3', 'ECel',
  'EFax', 'EEmail', 'EContato',
  'ENatal', 'PLograd', 'PRua',
  'PNumero', 'PCompl', 'PBairro',
  'PCidade', 'PUF', 'PCEP',
  'PPais', 'PTe1', 'Pte2',
  'Pte3', 'PCel', 'PFax',
  'PEmail', 'PContato', 'PNatal',
  'Sexo', 'Responsavel', 'Profissao',
  'Cargo', 'Recibo', 'DiaRecibo',
  'AjudaEmpV', 'AjudaEmpP', 'Cliente1',
  'Cliente2', 'Cliente3', 'Cliente4',*)
  'Fornece1', 'Fornece2', 'Fornece3',
  'Fornece4', (*'Fornece5', 'Fornece6',
  'Fornece7', 'Fornece8', 'Terceiro',
  'Cadastro', 'Informacoes', 'Logo',
  'Veiculo', 'Mensal', 'Observacoes',
  'Tipo', 'CLograd', 'CRua',
  'CNumero', 'CCompl', 'CBairro',
  'CCidade', 'CUF', 'CCEP',
  'CPais', 'CTel', 'CCel',
  'CFax', 'CContato', 'LLograd',
  'LRua', 'LNumero', 'LCompl',
  'LBairro', 'LCidade', 'LUF',
  'LCEP', 'LPais', 'LTel',
  'LCel', 'LFax', 'LContato',
  'Comissao', 'Situacao', 'Nivel',
  'Grupo', 'Account', 'Logo2',
  'ConjugeNome', 'ConjugeNatal', 'Nome1',
  'Natal1', 'Nome2', 'Natal2',
  'Nome3', 'Natal3', 'Nome4',
  'Natal4', 'CreditosI', 'CreditosL',
  'CreditosF2', 'CreditosD', 'CreditosU',
  'CreditosV', 'Motivo', 'QuantI1',
  'QuantI2', 'QuantI3', 'QuantI4',
  'QuantN1', 'QuantN2', 'QuantN3',
  'Agenda', 'SenhaQuer', 'Senha1',
  'LimiCred', 'Desco', 'CasasApliDesco',
  'TempA', 'TempD', 'Banco',
  'Agencia', 'ContaCorrente', 'FatorCompra',
  'AdValorem', 'DMaisC', 'DMaisD',
  'Empresa', 'CBE', 'SCB',
  'PAtividad', 'EAtividad', 'PCidadeCod',
  'ECidadeCod', 'PPaisCod', 'EPaisCod',*)
  'Antigo'(*, 'CUF2', 'Contab',
  'MSN1', 'PastaTxtFTP', 'PastaPwdFTP',
  'Protestar', 'MultaCodi', 'MultaDias',
  'MultaValr', 'MultaPerc', 'MultaTiVe',
  'JuroSacado', 'CPMF', 'Corrido',
  'CPF_Resp1', 'CliInt', 'AltDtPlaCt',
  'CartPref', 'RolComis', 'Filial',
  'EEndeRef', 'PEndeRef', 'CEndeRef',
  'LEndeRef', 'ECodMunici', 'PCodMunici',
  'CCodMunici', 'LCodMunici', 'CNAE',
  'SUFRAMA', 'ECodiPais', 'PCodiPais',
  'L_CNPJ', 'L_Ativo', 'CNAE 20',
  'URL', 'CRT'*)], [
  'Codigo'], [
  CodUsu, RazaoSocial, Fantasia,
  (*Respons1, Respons2, Pai,
  Mae,*) CNPJ, IE,
  (*NIRE, FormaSociet, Simples,
  IEST, Atividade, Nome,
  Apelido, CPF, CPF_Pai,
  CPF_Conjuge, RG, SSP,
  DataRG, CidadeNatal, EstCivil,
  UFNatal, Nacionalid,*) ELograd,
  ERua, ENumero, ECompl,
  EBairro, ECidade, EUF,
  ECEP, EPais, ETe1,
  (*Ete2, Ete3, ECel,
  EFax, EEmail, EContato,
  ENatal, PLograd, PRua,
  PNumero, PCompl, PBairro,
  PCidade, PUF, PCEP,
  PPais, PTe1, Pte2,
  Pte3, PCel, PFax,
  PEmail, PContato, PNatal,
  Sexo, Responsavel, Profissao,
  Cargo, Recibo, DiaRecibo,
  AjudaEmpV, AjudaEmpP, Cliente1,
  Cliente2, Cliente3, Cliente4,*)
  Fornece1, Fornece2, Fornece3, 
  Fornece4, (*Fornece5, Fornece6,
  Fornece7, Fornece8, Terceiro, 
  Cadastro, Informacoes, Logo, 
  Veiculo, Mensal, Observacoes, 
  Tipo, CLograd, CRua,
  CNumero, CCompl, CBairro,
  CCidade, CUF, CCEP, 
  CPais, CTel, CCel, 
  CFax, CContato, LLograd, 
  LRua, LNumero, LCompl, 
  LBairro, LCidade, LUF, 
  LCEP, LPais, LTel, 
  LCel, LFax, LContato, 
  Comissao, Situacao, Nivel, 
  Grupo, Account, Logo2, 
  ConjugeNome, ConjugeNatal, Nome1, 
  Natal1, Nome2, Natal2, 
  Nome3, Natal3, Nome4, 
  Natal4, CreditosI, CreditosL, 
  CreditosF2, CreditosD, CreditosU, 
  CreditosV, Motivo, QuantI1, 
  QuantI2, QuantI3, QuantI4, 
  QuantN1, QuantN2, QuantN3, 
  Agenda, SenhaQuer, Senha1, 
  LimiCred, Desco, CasasApliDesco, 
  TempA, TempD, Banco, 
  Agencia, ContaCorrente, FatorCompra,
  AdValorem, DMaisC, DMaisD, 
  Empresa, CBE, SCB,
  PAtividad, EAtividad, PCidadeCod, 
  ECidadeCod, PPaisCod, EPaisCod,*)
  Antigo(*, CUF2, Contab,
  MSN1, PastaTxtFTP, PastaPwdFTP, 
  Protestar, MultaCodi, MultaDias, 
  MultaValr, MultaPerc, MultaTiVe, 
  JuroSacado, CPMF, Corrido, 
  CPF_Resp1, CliInt, AltDtPlaCt, 
  CartPref, RolComis, Filial, 
  EEndeRef, PEndeRef, CEndeRef, 
  LEndeRef, ECodMunici, PCodMunici, 
  CCodMunici, LCodMunici, CNAE, 
  SUFRAMA, ECodiPais, PCodiPais, 
  L_CNPJ, L_Ativo, CNAE 20, 
  URL, CRT*)], [
  Codigo], True) then
  begin
    Result := Codigo;
    EdInclEnts.ValueVariant := EdInclEnts.ValueVariant + 1;
    Memo1.Lines.Add(CNPJ + ' Cadastrado c�d.:' + FormatFloat('0', Codigo));
  end;
end;

procedure TFmImportaFoxPro.InsereEntiTrnasp(Codigo, Transp: Integer);
const
  Ordem = 1;
var
  Conta: Integer;
begin
  Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'EntiTransp', 'EntiTransp', 'Codigo');
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entitransp', False, [
  'Codigo', 'Ordem', 'Transp'], ['Conta'
  ], [Codigo, Ordem, Transp], [Conta], False);
end;

function TFmImportaFoxPro.LocalizaAntigoStr(Tabela, Campo, Prefixo, Antigo: String): Boolean;
begin
  QrAnt.Close;
  QrAnt.SQL.Clear;
  QrAnt.SQL.Add('SELECT * FROM ' + Lowercase(Tabela));
  QrAnt.SQL.Add('WHERE ' + Campo + '="' + Prefixo + Antigo + '"');
  QrAnt . O p e n ;
  //
  Result := QrAnt.RecordCount > 0;
end;

function TFmImportaFoxPro.ObtemProximoGGXNeg(Referencia, Nome: String): Integer;
var
  X: String;
begin
  //Result := -900000;
  QrMinX.Close;
  QrMinX . O p e n ;
  if QrMinXNivel1.Value > -900000 then
    Result := -900001
  else
    Result := QrMinXNivel1.Value - 1;
  //
  X := FormatFloat('0', Result);
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO gragru1 SET');
  Dmod.QrUpd.SQL.Add('CodUsu=' + X + ',');
  Dmod.QrUpd.SQL.Add('Nivel1=' + X + ',');
  Dmod.QrUpd.SQL.Add('Nivel2=-9,');
  Dmod.QrUpd.SQL.Add('Nivel3=0,');
  Dmod.QrUpd.SQL.Add('Nome="' + dmkPF.DuplicaAspasDuplas(Nome) + '",');
  Dmod.QrUpd.SQL.Add('Referencia="' + dmkPF.DuplicaAspasDuplas(Referencia) + '",');
  //Dmod.QrUpd.SQL.Add('Referencia=:P0,');
  Dmod.QrUpd.SQL.Add('PrdGrupTip=-3,');
  Dmod.QrUpd.SQL.Add('GraTamCad=0;');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('INSERT INTO gragruc SET');
  Dmod.QrUpd.SQL.Add('Controle=' + X + ',');
  Dmod.QrUpd.SQL.Add('Nivel1=' + X + ',');
  Dmod.QrUpd.SQL.Add('GraCorCad=0;');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('INSERT INTO gragrux SET');
  Dmod.QrUpd.SQL.Add('Controle=' + X + ',');
  Dmod.QrUpd.SQL.Add('GraGru1=' + X + ',');
  Dmod.QrUpd.SQL.Add('GraGruC=' + X + ',');
  Dmod.QrUpd.SQL.Add('GraTamI=1;');
  Dmod.QrUpd.SQL.Add('');
  //Dmod.QrUpd.Params[0].AsString := '_' + Referencia;
  UMyMod.ExecutaQuery(Dmod.QrUpd);
  //
  Memo2.Lines.Add('Cadastrado produto ' + X + ' Regerencia = ' + Referencia);
end;

procedure TFmImportaFoxPro.Lanamentosrfosdecontasapagar1Click(Sender: TObject);
begin
  ImprimeLctOrf(True);
end;

procedure TFmImportaFoxPro.LanamentosrfosdecontasaReceber1Click(
  Sender: TObject);
begin
  ImprimeLctOrf(False);
end;

procedure TFmImportaFoxPro.LimparDadosTabela(SQL: WideString);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Executando limpeza: ' + SQL);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add(SQL);
  Dmod.QrUpd.ExecSQL;
end;

function TFmImportaFoxPro.LocalizaAntigoInt(Tabela, Campo: String;
Antigo: Integer): Boolean;
begin
  QrAnt.Close;
  QrAnt.SQL.Clear;
  QrAnt.SQL.Add('SELECT * FROM ' + Lowercase(Tabela));
  QrAnt.SQL.Add('WHERE ' + Campo + '=' + FormatFloat('0', Antigo));
  QrAnt . O p e n ;
  //
  Result := QrAnt.RecordCount > 0;
end;

{
procedure TFmImportaFoxPro.ReopenCodAnt(Codigo: Integer);
begin
  TbTabelas.Refresh;
  TbTabelas.Locate('Codigo', Codigo, []);
end;
}

procedure TFmImportaFoxPro.SbDirClick(Sender: TObject);
var
  Dir, Titulo, IniDir: String;
begin
  Dir     := EdDir.Text;
  Titulo  := 'Abrir Arquivo .DBF';
  IniDir  := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Dir, Titulo,
  'Arquivos DBF|*.DBF;Arquivos dbf|*.dbf', [], Dir) then
    EdDir.Text := ExtractFilePath(Dir);
end;

procedure TFmImportaFoxPro.SbListaClick(Sender: TObject);
var
  Lista, Titulo, IniLista: String;
begin
  Lista     := EdLista.Text;
  Titulo  := 'Abrir Arquivo .DBF';
  IniLista  := '';
  if MyObjects.FileOpenDialog(Self, IniLista, Lista, Titulo,
  'Arquivos Excel|*.XLS;Arquivos xlsx|*.XLSX', [], Lista) then
    EdLista.Text := Lista;
end;

procedure TFmImportaFoxPro.SpeedButton1Click(Sender: TObject);
var
  Bak, Titulo, IniBak: String;
begin
  Bak     := EdBak.Text;
  Titulo  := 'Abrir Arquivo .DBF';
  IniBak  := '';
  if MyObjects.FileOpenDialog(Self, IniBak, Bak, Titulo,
  'Arquivos DBF|*.DBF;Arquivos dbf|*.dbf', [], Bak) then
    EdBak.Text := ExtractFilePath(Bak);
end;

{
  if Nome = Lowercase('NomeTabela') then begin
    Qry.SQL.Add('  NUMERO               Varchar(6)                                ,');
    Qry.SQL.Add('  ORDSERV              Varchar(6)                                ,');
    Qry.SQL.Add('  SITUACAO             Integer                                   ,');
    Qry.SQL.Add('  NATOPER              Varchar(6)                                ,');
    Qry.SQL.Add('  EMISSAO              Date                                      ,');
    Qry.SQL.Add('  SAIDA                Date                                      ,');
    Qry.SQL.Add('  HORA                 Varchar(5)                                ,');
    Qry.SQL.Add('  BASEICMS             Float                                     ,');
    Qry.SQL.Add('  VLRICMS              Float                                     ,');
    Qry.SQL.Add('  BCICMSSUB            Float                                     ,');
    Qry.SQL.Add('  VLRICMSSUB           Float                                     ,');
    Qry.SQL.Add('  TOTALPROD            Float                                     ,');
    Qry.SQL.Add('  VLRFRETE             Float                                     ,');
    Qry.SQL.Add('  VLRSEGURO            Float                                     ,');
    Qry.SQL.Add('  OUTRAS               Float                                     ,');
    Qry.SQL.Add('  IPI                  Float                                     ,');
    Qry.SQL.Add('  TOTAL                Float                                     ,');
    Qry.SQL.Add('  TRANSPORTA           Varchar(3)                                ,');
    Qry.SQL.Add('  FRETE                Integer                                   ,');
    Qry.SQL.Add('  PLACAS               Varchar(8)                                ,');
    Qry.SQL.Add('  QTDADE               Integer                                   ,');
    Qry.SQL.Add('  ESPECIE              Varchar(20)                               ,');
    Qry.SQL.Add('  MARCA                Varchar(20)                               ,');
    Qry.SQL.Add('  NUMPROD              Varchar(25)                               ,');
    Qry.SQL.Add('  PESOBRUT             Float                                     ,');
    Qry.SQL.Add('  PESOLIQ              Float                                     ,');
    Qry.SQL.Add('  OBS                  Text                                      ,');
    Qry.SQL.Add('  CORPO                Text                                      ,');
    Qry.SQL.Add('  CR                   Integer                                   ,');
    Qry.SQL.Add('  TIPODESC             Integer                                   ,');
    Qry.SQL.Add('  NOTADESC             Float                                     ,');
    Qry.SQL.Add('  J10UKEY              Varchar(20)                               ');
    Qry.SQL.Add(') TYPE=MyISAM');
    Qry.ExecSQL;
  end else
}

end.
