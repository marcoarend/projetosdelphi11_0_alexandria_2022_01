object FmPQUsoDiaAtz: TFmPQUsoDiaAtz
  Left = 339
  Top = 185
  Caption = 'QUI-INSUM-010 :: Atualiza'#231#227'o de Consumo Di'#225'rio de Insumos'
  ClientHeight = 352
  ClientWidth = 661
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 661
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 613
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 565
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 530
        Height = 32
        Caption = 'Atualiza'#231#227'o de Consumo Di'#225'rio de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 530
        Height = 32
        Caption = 'Atualiza'#231#227'o de Consumo Di'#225'rio de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 530
        Height = 32
        Caption = 'Atualiza'#231#227'o de Consumo Di'#225'rio de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 219
    Width = 661
    Height = 63
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 155
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 657
      Height = 46
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 29
        Width = 657
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 282
    Width = 661
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 218
    object PnSaiDesis: TPanel
      Left = 515
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 513
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 661
    Height = 171
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitHeight = 107
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 661
      Height = 329
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 0
      object Label4: TLabel
        Left = 56
        Top = 8
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label5: TLabel
        Left = 173
        Top = 8
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object TPIni: TdmkEditDateTimePicker
        Left = 56
        Top = 24
        Width = 112
        Height = 21
        Date = 37670.521748657400000000
        Time = 37670.521748657400000000
        TabOrder = 0
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPFim: TdmkEditDateTimePicker
        Left = 173
        Top = 24
        Width = 112
        Height = 21
        Date = 37670.521748657400000000
        Time = 37670.521748657400000000
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object CGDiasSem: TdmkCheckGroup
        Left = 52
        Top = 48
        Width = 553
        Height = 53
        Caption = '  Dias trabalhados na semana: '
        Columns = 7
        ItemIndex = 1
        Items.Strings = (
          'Domingo'
          'Segunda'
          'Ter'#231'a'
          'Quarta'
          'Quinta'
          'Sexta'
          'S'#225'bado')
        TabOrder = 2
        OnClick = CGDiasSemClick
        UpdType = utYes
        Value = 2
        OldValor = 0
      end
      object RGFonte: TRadioGroup
        Left = 52
        Top = 108
        Width = 553
        Height = 53
        Caption = ' Origem: '
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          'Baixas (pesagens, ETE, outros)'
          'Previs'#245'es de consumos (pedidos)')
        TabOrder = 3
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 7
  end
  object QrPQUso: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM stqalocits')
    Left = 324
    Top = 56
    object QrPQUsoInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQUsoPeso: TFloatField
      FieldName = 'Peso'
    end
  end
end
