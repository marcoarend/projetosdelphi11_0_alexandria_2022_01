unit PQPedEditCalc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, dmkEdit, dmkGeral, UnDmkEnums;

type
  TFmPQPedEditCalc = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label14: TLabel;
    EdVolumes: TdmkEdit;
    Label4: TLabel;
    EdKgBruto: TdmkEdit;
    Label15: TLabel;
    EdkgLiq: TdmkEdit;
    Label18: TLabel;
    EdTotalkgBruto: TdmkEdit;
    Label19: TLabel;
    EdTotalkgLiq: TdmkEdit;
    RGCalc: TRadioGroup;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure RGCalcClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdVolumesExit(Sender: TObject);
    procedure EdKgBrutoExit(Sender: TObject);
    procedure EdkgLiqExit(Sender: TObject);
    procedure EdTotalkgBrutoExit(Sender: TObject);
    procedure EdTotalkgLiqExit(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaPesos(Tipo: Integer);
  public
    { Public declarations }
  end;

var
  FmPQPedEditCalc: TFmPQPedEditCalc;

implementation

uses UnMyObjects, PQPedEdit;

{$R *.DFM}

procedure TFmPQPedEditCalc.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQPedEditCalc.BtConfirmaClick(Sender: TObject);
begin
  FmPQPedEdit.EdVolumes.Text := EdVolumes.Text;
  FmPQPedEdit.EdkgBruto.Text := EdkgBruto.Text;
  FmPQPedEdit.EdkgLiq.Text := EdkgLiq.Text;
  FmPQPedEdit.EdTotalkgLiq.Text := EdTotalkgLiq.Text;
  FmPQPedEdit.EdTotalkgBruto.Text := EdTotalkgBruto.Text;
  FmPQPedEdit.EdkgBruto.SetFocus;
  FmPQPedEdit.EdkgLiq.SetFocus;
  Close;
end;

procedure TFmPQPedEditCalc.RGCalcClick(Sender: TObject);
begin
  EdVolumes.ReadOnly := True;
  EdkgBruto.ReadOnly := True;
  EdkgLiq.ReadOnly := True;
  EdTotalkgBruto.ReadOnly := True;
  EdTotalkgLiq.ReadOnly := True;
  case RGCalc.ItemIndex of
    0:
    begin
      EdVolumes.ReadOnly := False;
      EdkgBruto.ReadOnly := False;
      EdkgLiq.ReadOnly := False;
    end;
    1:
    begin
      EdVolumes.ReadOnly := False;
      EdTotalkgBruto.ReadOnly := False;
      EdTotalkgLiq.ReadOnly := False;
    end;
    2:
    begin
      EdkgBruto.ReadOnly := False;
      EdkgLiq.ReadOnly := False;
      EdTotalkgBruto.ReadOnly := False;
      EdTotalkgLiq.ReadOnly := False;
    end;
  end;
end;

procedure TFmPQPedEditCalc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  RGCalc.ItemIndex := 2;
end;

procedure TFmPQPedEditCalc.CalculaPesos(Tipo: Integer);
var
  Vol, Calc, Vol2: Integer;
  Liq, Bru, TLi, TBr: Double;
  kgB, kgL: Double;
begin
  Calc := RGCalc.ItemIndex;
  EdVolumes.Text := Geral.TFT(EdVolumes.Text, 0, siPositivo);
  EdkgLiq.Text := Geral.TFT(EdkgLiq.Text, 3, siPositivo);
  EdkgBruto.Text := Geral.TFT(EdkgBruto.Text, 3, siPositivo);
  EdTotalkgLiq.Text := Geral.TFT(EdTotalkgLiq.Text, 3, siPositivo);
  EdTotalkgBruto.Text := Geral.TFT(EdTotalkgBruto.Text, 3, siPositivo);
  //
  Vol := Geral.IMV(EdVolumes.Text);
  Liq := Geral.DMV(EdkgLiq.Text);
  Bru := Geral.DMV(EdkgBruto.Text);
  TLi := Geral.DMV(EdTotalkgLiq.Text);
  TBr := Geral.DMV(EdTotalkgBruto.Text);
  //
  kgB := 0;
  kgL := 0;
  //
  if (Tipo = 0) and (Calc = 0) then
  begin
    EdTotalkgLiq.Text := Geral.TFT(FloatToStr(Vol*Liq), 3, siPositivo);
    EdTotalkgBruto.Text := Geral.TFT(FloatToStr(Vol*Bru), 3, siPositivo);
  end;
  if (Tipo = 0) and (Calc = 1) then
  begin
    if Vol > 0 then
    begin
      kgB := TBr / Vol;
      kgL := TLi / Vol;
    end;
    EdkgLiq.Text := Geral.TFT(FloatToStr(kgL), 3, siPositivo);
    EdkgBruto.Text := Geral.TFT(FloatToStr(kgB), 3, siPositivo);
  end;
//  if (Tipo = 0) and (Calc = 2) then ShowMessage('Erro. C�lculo inv�lido');
  ///
  if (Tipo = 1) and (Calc = 0) then
    EdTotalkgBruto.Text := Geral.TFT(FloatToStr(Vol*Bru), 3, siPositivo);
//  if (Tipo = 1) and (Calc = 1) then ShowMessage('Erro. C�lculo inv�lido');
  if (Tipo = 1) and (Calc = 2) then
  begin
    if Bru > 0 then Vol2 := Round(TBr / bru) else Vol2 := 0;
    EdVolumes.Text := Geral.TFT(FloatToStr(Vol2), 0, siPositivo);
    EdTotalkgLiq.Text := Geral.TFT(FloatToStr(Vol2*Liq), 3, siPositivo);
    EdTotalkgBruto.Text := Geral.TFT(FloatToStr(Vol2*Bru), 3, siPositivo);
  end;
  ///
  if (Tipo = 2) and (Calc = 0) then
    EdTotalkgLiq.Text := Geral.TFT(FloatToStr(Vol*Liq), 3, siPositivo);
//  if (Tipo = 2) and (Calc = 1) then ShowMessage('Erro. C�lculo inv�lido');
  if (Tipo = 2) and (Calc = 2) then
  begin
    if Liq > 0 then Vol2 := Round(TLi / Liq) else Vol2 := 0;
    EdVolumes.Text := Geral.TFT(FloatToStr(Vol2), 0, siPositivo);
    EdTotalkgLiq.Text := Geral.TFT(FloatToStr(Vol2*Liq), 3, siPositivo);
    EdTotalkgBruto.Text := Geral.TFT(FloatToStr(Vol2*Bru), 3, siPositivo);
  end;
  ///
//  if (Tipo = 3) and (Calc = 0) then ShowMessage('Erro. C�lculo inv�lido');
  if (Tipo = 3) and (Calc = 1) then
  begin
    if Vol > 0 then kgB := TBr / Vol;
    EdkgBruto.Text := Geral.TFT(FloatToStr(kgB), 3, siPositivo);
  end;
  if (Tipo = 3) and (Calc = 2) then
  begin
    if Bru > 0 then Vol2 := Round(TBr / Bru) else Vol2 := 0;
    EdVolumes.Text := Geral.TFT(FloatToStr(Vol2), 0, siPositivo);
    if Vol2 > 0 then
    begin
      kgB := Tbr / Vol2;
      kgL := TLi / Vol2;
    end else begin
      kgB := 0;
      kgL := 0;
    end;
    EdkgLiq.Text := Geral.TFT(FloatToStr(kgL), 3, siPositivo);
    EdkgBruto.Text := Geral.TFT(FloatToStr(kgB), 3, siPositivo);
  end;
  ///
//  if (Tipo = 4) and (Calc = 0) then ShowMessage('Erro. C�lculo inv�lido');
  if (Tipo = 4) and (Calc = 1) then
  begin
    if Vol > 0 then kgL := TLi / Vol;
    EdkgLiq.Text := Geral.TFT(FloatToStr(kgL), 3, siPositivo);
  end;
  if (Tipo = 4) and (Calc = 2) then
  begin
    if Liq > 0 then Vol2 := Round(TLi / Liq) else Vol2 := 0;
    EdVolumes.Text := Geral.TFT(FloatToStr(Vol2), 0, siPositivo);
    if Vol2 > 0 then
    begin
      kgB := Tbr / Vol2;
      kgL := TLi / Vol2;
    end else begin
      kgB := 0;
      kgL := 0;
    end;
    EdkgLiq.Text := Geral.TFT(FloatToStr(kgL), 3, siPositivo);
    EdkgBruto.Text := Geral.TFT(FloatToStr(kgB), 3, siPositivo);
  end;
  ///
end;

procedure TFmPQPedEditCalc.EdVolumesExit(Sender: TObject);
begin
  CalculaPesos(0);
end;

procedure TFmPQPedEditCalc.EdKgBrutoExit(Sender: TObject);
begin
  CalculaPesos(1);
end;

procedure TFmPQPedEditCalc.EdkgLiqExit(Sender: TObject);
begin
  CalculaPesos(2);
end;

procedure TFmPQPedEditCalc.EdTotalkgBrutoExit(Sender: TObject);
begin
  CalculaPesos(3);
end;

procedure TFmPQPedEditCalc.EdTotalkgLiqExit(Sender: TObject);
begin
  CalculaPesos(4);
end;

end.
