unit Chequez;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, Db, mySQLDbTables, DmkDAC_PF,
  ComCtrls, DBCtrls, frxClass, frxDBSet, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnDmkProcFunc, dmkLabel, dmkImage,
  UnDmkEnums;

type
  TFmChequez = class(TForm)
    Panel1: TPanel;
    DBGrid5: TDBGrid;
    PnEdita: TPanel;
    Label9: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdCodigo: TdmkEdit;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdCheque: TdmkEdit;
    EdEmitente: TdmkEdit;
    EdCPF: TdmkEdit;
    EdCliente: TdmkEditCB;
    EdCidade: TdmkEdit;
    EdValor: TdmkEdit;
    EdObserv: TdmkEdit;
    Panel2: TPanel;
    PnControla: TPanel;
    Panel4: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    PnConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel6: TPanel;
    BtDesiste: TBitBtn;
    QrChequez: TmySQLQuery;
    QrChequezCodigo: TIntegerField;
    QrChequezBanco: TSmallintField;
    QrChequezAgencia: TSmallintField;
    QrChequezConta: TWideStringField;
    QrChequezCheque: TIntegerField;
    QrChequezCidade: TWideStringField;
    QrChequezEmitente: TWideStringField;
    QrChequezCPF: TWideStringField;
    QrChequezValor: TFloatField;
    QrChequezObserv: TWideStringField;
    QrChequezLk: TIntegerField;
    QrChequezDataCad: TDateField;
    QrChequezDataAlt: TDateField;
    QrChequezUserCad: TIntegerField;
    QrChequezUserAlt: TIntegerField;
    DsCheques: TDataSource;
    QrChequezCPF_TXT: TWideStringField;
    Label1: TLabel;
    Label2: TLabel;
    QrChequezDataEmi: TDateField;
    QrChequezDataVct: TDateField;
    CBCliente: TdmkDBLookupComboBox;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    QrChequezNOMECLI: TWideStringField;
    QrChequezCliente: TIntegerField;
    GBPesquisa: TGroupBox;
    PnPesq: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdPesqBanco: TdmkEdit;
    EdPesqAgencia: TdmkEdit;
    EdPesqConta: TdmkEdit;
    EdPesqCheque: TdmkEdit;
    EdPesqEmitente: TdmkEdit;
    EdPesqCPF: TdmkEdit;
    EdPesqCliente: TdmkEditCB;
    EdPesqCidade: TdmkEdit;
    CBPesqCliente: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    Label19: TLabel;
    Label12: TLabel;
    EdPesqValorMin: TdmkEdit;
    EdPesqValorMax: TdmkEdit;
    CkPesqValor: TCheckBox;
    GroupBox3: TGroupBox;
    Label20: TLabel;
    Label23: TLabel;
    TPPesqDataEmiMin: TDateTimePicker;
    TPPesqDataEmiMax: TDateTimePicker;
    CkPesqDataEmi: TCheckBox;
    GroupBox4: TGroupBox;
    Label21: TLabel;
    Label22: TLabel;
    TPPesqDataVctMin: TDateTimePicker;
    TPPesqDataVctMax: TDateTimePicker;
    CkPesqDataVct: TCheckBox;
    QrDatas: TmySQLQuery;
    QrDatasMaxEmi: TDateField;
    QrDatasMaxVct: TDateField;
    QrDatasMinEmi: TDateField;
    QrDatasMinVct: TDateField;
    CkPesquisa: TCheckBox;
    Panel14: TPanel;
    CBOrdem1: TComboBox;
    CkOrdem1: TCheckBox;
    CBOrdem2: TComboBox;
    CkOrdem2: TCheckBox;
    CBOrdem4: TComboBox;
    CkOrdem4: TCheckBox;
    CkOrdem3: TCheckBox;
    CBOrdem3: TComboBox;
    BitBtn1: TBitBtn;
    BtImprime: TBitBtn;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    frxChequez: TfrxReport;
    frxDsChequez: TfrxDBDataset;
    EdDataEmi: TdmkEdit;
    EdDataVct: TdmkEdit;
    Label28: TLabel;
    Label29: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdBancoExit(Sender: TObject);
    procedure EdAgenciaExit(Sender: TObject);
    procedure EdChequeExit(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrChequezCalcFields(DataSet: TDataSet);
    procedure RGOrdem1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure EdPesqBancoExit(Sender: TObject);
    procedure EdPesqAgenciaExit(Sender: TObject);
    procedure EdPesqChequeExit(Sender: TObject);
    procedure CkPesquisaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrChequezBeforeClose(DataSet: TDataSet);
    procedure QrChequezAfterOpen(DataSet: TDataSet);
    procedure CBOrdem1Change(Sender: TObject);
    procedure CkOrdem1Click(Sender: TObject);
    procedure EdPesqBancoChange(Sender: TObject);
    procedure EdDataEmiExit(Sender: TObject);
    procedure EdDataVctExit(Sender: TObject);
    procedure EdAgenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FCamConfigIni: String;
    procedure ReopenChequez(Codigo: Integer);
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure FechaPesquisa;
  public
    { Public declarations }
  end;

  var
  FmChequez: TFmChequez;



implementation

uses UnMyObjects, Module, UnInternalConsts, UMySQLModule, UnGOTOy;

{$R *.DFM}

procedure TFmChequez.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PnControla.Visible := True;
      PnConfirma.Visible := False;
      PnEdita.Visible    := False;
    end;
    1:
    begin
      PnConfirma.Visible   := True;
      PnEdita.Visible      := True;
      PnControla.Visible   := False;
      if SQLType = stIns then
      begin
        EdBanco.Text       := '';
        EdAgencia.Text     := '';
        EdConta.Text       := '';
        EdCheque.Text      := '';
        EdValor.Text       := '';
        EdEmitente.Text    := '';
        EdCPF.Text         := '';
        //
        EdCliente.Text  := '';
        CBCliente.KeyValue := NULL;
        EdCidade.Text      := '';
        EdObserv.Text      := '';
        //
        EdDataEmi.Text     := Geral.FDT(Date, 2);
        EdDataVct.Text     := Geral.FDT(Date, 2);
        //
        EdCodigo.Text      := '';
        //
      end else begin
        EdBanco.Text       := FormatFloat('000', QrChequezBanco.Value);
        EdAgencia.Text     := FormatFloat('0000', QrChequezAgencia.Value);
        EdConta.Text       := QrChequezConta.Value;
        EdCheque.Text      := FormatFloat('000000', QrChequezCheque.Value);
        EdValor.Text       := Geral.FFT(QrChequezValor.Value, 2, siPositivo);
        EdEmitente.Text    := QrChequezEmitente.Value;
        EdCPF.Text         := Geral.FormataCNPJ_TT(QrChequezCPF.Value);
        //
        EdCliente.Text     := IntToStr(QrChequezCliente.Value);
        CBCliente.KeyValue := QrChequezCliente.Value;
        EdCidade.Text      := QrChequezCidade.Value;
        EdObserv.Text      := QrChequezObserv.Value;
        //
        EdDataEmi.Text     := Geral.FDT(QrChequezDataEmi.Value, 2);
        EdDataVct.Text     := Geral.FDT(QrChequezDataVct.Value, 2);
        //
        EdCodigo.Text      := FormatFloat('000000', QrChequezCodigo.Value);
        //
      end;
      EdBanco.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  //GOTOy.BotoesSb(ImgTipo.SQLType);
  //if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmChequez.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChequez.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmChequez.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmChequez.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmChequez.FormCreate(Sender: TObject);
begin
  FCamConfigIni := Application.Title+'\ConfigIni';
  PnControla.Align := alClient;
  PnConfirma.Align := alClient;
  EdDataEmi.Text := Geral.FDT(Date, 2);
  EdDataVct.Text := Geral.FDT(Date, 2);
  UnDmkDAC_PF.AbreQuery(QrDatas, Dmod.MyDB);
  TPPesqDataEmiMin.Date := QrDatasMinEmi.Value;
  TPPesqDataEmiMax.Date := Date;
  //TPPesqDataVctMin.Date := QrDatasMinEmi.Value;
  TPPesqDataVctMin.Date := Date;
  TPPesqDataVctMax.Date := IncMonth(QrDatasMaxVct.Value, 60);
  CkPesquisa.Checked := True;
  CkPesqDataVct.Checked := True;
  //
  CkOrdem1.Checked := Geral.ReadAppKey(
    'CkOrdem1', FCamConfigIni, ktBoolean, False, HKEY_LOCAL_MACHINE);
  CkOrdem2.Checked := Geral.ReadAppKey(
    'CkOrdem2', FCamConfigIni, ktBoolean, False, HKEY_LOCAL_MACHINE);
  CkOrdem3.Checked := Geral.ReadAppKey(
    'CkOrdem3', FCamConfigIni, ktBoolean, False, HKEY_LOCAL_MACHINE);
  CkOrdem4.Checked := Geral.ReadAppKey(
    'CkOrdem4', FCamConfigIni, ktBoolean, False, HKEY_LOCAL_MACHINE);
  CBOrdem1.ItemIndex := Geral.ReadAppKey(
    'CBOrdem1', FCamConfigIni, ktInteger, -1, HKEY_LOCAL_MACHINE);
  CBOrdem2.ItemIndex := Geral.ReadAppKey(
    'CBOrdem2', FCamConfigIni, ktInteger, -1, HKEY_LOCAL_MACHINE);
  CBOrdem3.ItemIndex := Geral.ReadAppKey(
    'CBOrdem3', FCamConfigIni, ktInteger, -1, HKEY_LOCAL_MACHINE);
  CBOrdem4.ItemIndex := Geral.ReadAppKey(
    'CBOrdem4', FCamConfigIni, ktInteger, -1, HKEY_LOCAL_MACHINE);
  //
  EdBanco.Enabled := Geral.ReadAppKey(
    'EdBanco', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  EdAgencia.Enabled :=  Geral.ReadAppKey(
    'EdAgencia', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  EdConta.Enabled := Geral.ReadAppKey(
    'EdConta', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  EdCheque.Enabled := Geral.ReadAppKey(
    'EdCheque', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  EdValor.Enabled := Geral.ReadAppKey(
    'EdValor', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  EdEmitente.Enabled := Geral.ReadAppKey(
    'EdEmitente', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  EdCPF.Enabled := Geral.ReadAppKey(
    'EdCPF', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  EdDataEmi.Enabled := Geral.ReadAppKey(
    'EdDataEmi', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  EdCliente.Enabled := Geral.ReadAppKey(
    'EdCliente', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  CBCliente.Enabled := Geral.ReadAppKey(
    'CBCliente', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  EdCidade.Enabled := Geral.ReadAppKey(
    'EdCidade', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  EdObserv.Enabled := Geral.ReadAppKey(
    'EdObserv', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  EdBanco.Enabled := Geral.ReadAppKey(
    'EdBanco', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  EdDataVct.Enabled := Geral.ReadAppKey(
    'EdDataVct', FCamConfigIni, ktBoolean, True, HKEY_LOCAL_MACHINE);
  //
  ReopenChequez(0);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  //
end;

procedure TFmChequez.EdBancoExit(Sender: TObject);
begin
  EdBanco.Text := FormatFloat('000', Geral.IMV(EdBanco.Text));
end;

procedure TFmChequez.EdAgenciaExit(Sender: TObject);
begin
  EdAgencia.Text := FormatFloat('0000', Geral.IMV(EdAgencia.Text));
end;

procedure TFmChequez.EdChequeExit(Sender: TObject);
begin
  EdCheque.Text := FormatFloat('000000', Geral.IMV(EdCheque.Text));
end;

procedure TFmChequez.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Data, Vcto: TDateTime;
begin
  Vcto := Geral.ValidaDataSimples(EdDataVct.Text, True);
  Data := Geral.ValidaDataSimples(EdDataEmi.Text, True);
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO chequez SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Chequez', 'Chequez', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE chequez SET ');
    Codigo := QrChequezCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Banco=:P0, Agencia=:P1, Conta=:P2, Cheque=:P3, ');
  Dmod.QrUpdU.SQL.Add('Valor=:P4, Emitente=:P5, CPF=:P6, Cliente=:P7, ');
  Dmod.QrUpdU.SQL.Add('Cidade=:P8, Observ=:P9, DataEmi=:P10, DataVct=:P11, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsInteger := Geral.IMV(EdBanco.Text);
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdAgencia.Text);
  Dmod.QrUpdU.Params[02].AsString  := EdConta.Text;
  Dmod.QrUpdU.Params[03].AsInteger := Geral.IMV(EdCheque.Text);
  Dmod.QrUpdU.Params[04].AsFloat   := Geral.DMV(EdValor.Text);
  Dmod.QrUpdU.Params[05].AsString  := EdEmitente.Text;
  Dmod.QrUpdU.Params[06].AsString  := Geral.SoNumero_TT(EdCPF.Text);
  Dmod.QrUpdU.Params[07].AsString  := EdCliente.Text;
  Dmod.QrUpdU.Params[08].AsString  := EdCidade.Text;
  Dmod.QrUpdU.Params[09].AsString  := EdObserv.Text;
  Dmod.QrUpdU.Params[10].AsString  := Geral.FDT(Data, 1);
  Dmod.QrUpdU.Params[11].AsString  := Geral.FDT(Vcto, 1);
  //
  Dmod.QrUpdU.Params[12].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[13].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[14].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Chequez', 'Codigo');
  //
  if ImgTipo.SQLType = stIns then EdBanco.SetFocus
  else MostraEdicao(0, stLok, 0);
  ReopenChequez(Codigo);

  //LocCod(Codigo,Codigo);
  //if FSeq = 1 then Close;
end;

procedure TFmChequez.ReopenChequez(Codigo: Integer);
const
  ORDENS: array[0..11] of string = (
    'chz.Agencia',
    'chz.Banco',
    'chz.Cheque',
    'chz.Cidade',
    'chz.Cliente',
    'chz.CPF',
    'chz.Conta',
    'chz.DataEmi',
    'chz.Emitente',
    'chz.Observ',
    'chz.Valor',
    'chz.DataVct');
  SEQUES: array[0..1] of string = (' ', ' DESC ');
var
  Ordem, Conta, Emitente, Cidade, CNPJ: String;
  Banco, Agencia, Cheque, Cliente: Integer;
  ValMin, ValMax: String;
begin
  Screen.Cursor := crHourGlass;
  try
  Cliente := Geral.IMV(EdPesqCliente.Text);
  Banco   := Geral.IMV(EdPesqBanco.Text);
  Agencia := Geral.IMV(EdPesqAgencia.Text);
  Cheque  := Geral.IMV(EdPesqCheque.Text);
  Conta   := EdPesqConta.Text;
  Emitente:= EdPesqEmitente.Text;
  if Emitente <> '' then Emitente := '%' + Emitente + '%';
  Cidade  := EdPesqCidade.Text;
  if Cidade <> '' then Cidade := '%' + Cidade + '%';
  CNPJ := Geral.SoNumero_TT(EdPesqCPF.Text);
  ValMin := Geral.TFT_Dot(EdPesqValorMin.Text, 2, siPositivo);
  ValMax := Geral.TFT_Dot(EdPesqValorMax.Text, 2, siPositivo);
  Ordem := '';
  if CBOrdem1.ItemIndex > -1 then Ordem := Ordem + ORDENS[CBOrdem1.ItemIndex] +
    SEQUES[Geral.BoolToInt(CkOrdem1.Checked)] + ',';
  if CBOrdem2.ItemIndex > -1 then Ordem := Ordem + ORDENS[CBOrdem2.ItemIndex] +
    SEQUES[Geral.BoolToInt(CkOrdem2.Checked)] + ',';
  if CBOrdem3.ItemIndex > -1 then Ordem := Ordem + ORDENS[CBOrdem3.ItemIndex] +
    SEQUES[Geral.BoolToInt(CkOrdem3.Checked)] + ',';
  if CBOrdem4.ItemIndex > -1 then Ordem := Ordem + ORDENS[CBOrdem4.ItemIndex] +
    SEQUES[Geral.BoolToInt(CkOrdem4.Checked)] + ',';
  //
  if Ordem <> '' then Ordem := 'ORDER BY ' + Copy(Ordem, 1, Length(Ordem)-1);
  QrChequez.Close;
  QrChequez.SQL.Clear;
  QrChequez.SQL.Add('SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial');
  QrChequez.SQL.Add('ELSE cli.Nome END NOMECLI, chz.*');
  QrChequez.SQL.Add('FROM chequez chz');
  QrChequez.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=chz.Cliente');
  QrChequez.SQL.Add('WHERE chz.Codigo <> 0 ');
  //////////////////////////////////////////////////////////////////////////////
  if CkPesquisa.Checked then
  begin
    if Banco <> 0 then
      QrChequez.SQL.Add('AND chz.Banco='+IntToStr(Banco));
    if Agencia <> 0 then
      QrChequez.SQL.Add('AND chz.Agencia='+IntToStr(Agencia));
    if Conta <> '' then
      QrChequez.SQL.Add('AND chz.Conta='+Conta);
    if Cheque <> 0 then
      QrChequez.SQL.Add('AND chz.Cheque='+IntToStr(Cheque));
    if Cliente <> 0 then
      QrChequez.SQL.Add('AND chz.Cliente='+IntToStr(Cliente));
    if CNPJ <> '' then
      QrChequez.SQL.Add('AND chz.CPF='+CNPJ);
    if Emitente <> '' then
      QrChequez.SQL.Add('AND chz.Emitente LIKE "'+Emitente+'"');
    if Cidade <> '' then
      QrChequez.SQL.Add('AND chz.Cidade LIKE "'+Cidade+'"');
    if CkPesqValor.Checked then
      QrChequez.SQL.Add('AND chz.Valor BETWEEN '+ValMin+' AND '+ValMax);
    if CkPesqDataEmi.Checked then
      QrChequez.SQL.Add(dmkPF.SQL_Periodo('AND chz.DataEmi ',
      TPPesqDataEmiMin.Date, TPPesqDataEmiMax.Date, True, True));
    if CkPesqDataVct.Checked then
      QrChequez.SQL.Add(dmkPF.SQL_Periodo('AND chz.DataVct ',
      TPPesqDataVctMin.Date, TPPesqDataVctMax.Date, True, True));
  end;
  //////////////////////////////////////////////////////////////////////////////
  QrChequez.SQL.Add('');
  QrChequez.SQL.Add(Ordem);
  UnDmkDAC_PF.AbreQuery(QrChequez, Dmod.MyDB);
  if Codigo > 0 then QrChequez.Locate('Codigo', Codigo, []);
  Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmChequez.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Geral.WriteAppKey(
    'CkOrdem1', FCamConfigIni, CkOrdem1.Checked, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'CkOrdem2', FCamConfigIni, CkOrdem2.Checked, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'CkOrdem3', FCamConfigIni, CkOrdem3.Checked, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'CkOrdem4', FCamConfigIni, CkOrdem4.Checked, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'CBOrdem1', FCamConfigIni, CBOrdem1.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'CBOrdem2', FCamConfigIni, CBOrdem2.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'CBOrdem3', FCamConfigIni, CBOrdem3.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'CBOrdem4', FCamConfigIni, CBOrdem4.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);

  //VAR_CHEQUE := QrChequezCodigo.Value;

  Geral.WriteAppKey(
    'EdBanco', FCamConfigIni, EdBanco.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'EdAgencia', FCamConfigIni, EdAgencia.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'EdConta', FCamConfigIni, EdConta.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'EdCheque', FCamConfigIni, EdCheque.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'EdValor', FCamConfigIni, EdValor.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'EdEmitente', FCamConfigIni, EdEmitente.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'EdCPF', FCamConfigIni, EdCPF.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'EdDataEmi', FCamConfigIni, EdDataEmi.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'EdCliente', FCamConfigIni, EdCliente.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'CBCliente', FCamConfigIni, CBCliente.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'EdCidade', FCamConfigIni, EdCidade.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'EdObserv', FCamConfigIni, EdObserv.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'EdBanco', FCamConfigIni, EdBanco.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey(
    'EdDataVct', FCamConfigIni, EdDataVct.Enabled, ktBoolean, HKEY_LOCAL_MACHINE);
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmChequez.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmChequez.BtExcluiClick(Sender: TObject);
var
  ChequeD, ChequeP: Integer;
begin
  if Application.MessageBox('Confirma a exclus�o do cheque selecionado?',
  'Exclus�o', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    ChequeD := QrChequezCodigo.Value;
    ChequeP := UMyMod.ProximoRegistro(QrChequez, 'Codigo', ChequeD);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM chequez WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := ChequeD;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenChequez(ChequeP);
  end;
end;

procedure TFmChequez.QrChequezCalcFields(DataSet: TDataSet);
begin
  QrChequezCPF_TXT.Value := Geral.FormataCNPJ_TT(QrChequezCPF.Value);
end;

procedure TFmChequez.RGOrdem1Click(Sender: TObject);
begin
  ReopenChequez(QrChequezCodigo.Value);
end;

procedure TFmChequez.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(1, stUpd, 0);
end;

procedure TFmChequez.EdPesqBancoExit(Sender: TObject);
begin
  EdPesqBanco.Text := Geral.TFD(EdPesqBanco.Text, 3, siPositivo);
end;

procedure TFmChequez.EdPesqAgenciaExit(Sender: TObject);
begin
  EdPesqAgencia.Text := Geral.TFD(EdPesqAgencia.Text, 4, siPositivo);
end;

procedure TFmChequez.EdPesqChequeExit(Sender: TObject);
begin
  EdPesqCheque.Text := Geral.TFD(EdPesqCheque.Text, 3, siPositivo);
end;

procedure TFmChequez.CkPesquisaClick(Sender: TObject);
begin
  if CkPesquisa.Checked then
  begin
    PnPesq.Visible := True;
    GBPesquisa.Height := 120
  end else begin
    GBPesquisa.Height := 19;
    PnPesq.Visible := False;
  end;
  FechaPesquisa;
end;

procedure TFmChequez.BitBtn1Click(Sender: TObject);
begin
  ReopenChequez(QrChequezCodigo.Value);
end;

procedure TFmChequez.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxChequez, 'Controle de Cheques');
end;

procedure TFmChequez.QrChequezBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmChequez.QrChequezAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := True;
end;

procedure TFmChequez.CBOrdem1Change(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmChequez.CkOrdem1Click(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmChequez.EdPesqBancoChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmChequez.FechaPesquisa;
begin
  QrChequez.Close;
end;

procedure TFmChequez.EdDataEmiExit(Sender: TObject);
var
  Data: TDateTime;
begin
  Data := Geral.ValidaDataSimples(EdDataEmi.Text, False);
  if Data > 2 then
  begin
    EdDataEmi.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdDataEmi.SetFocus;
end;

procedure TFmChequez.EdDataVctExit(Sender: TObject);
var
  Data, Emis: TDateTime;
  Casas: Integer;
begin
  Casas := Length(EdDataVct.Text);
  Data := Geral.ValidaDataSimples(EdDataVct.Text, False);
  Emis := Geral.ValidaDataSimples(EdDataEmi.Text, False);
  if Data > 2 then
  begin
    if Data <= Emis then
      if Casas < 6 then Data := IncMonth(Data, 12);
    if Data < Emis then
    begin
      if Application.MessageBox('Data j� passada! Deseja incrementar um ano?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Data := IncMonth(Data, 12);
        EdDataVct.SetFocus;
      end else begin
        EdDataVct.SetFocus;
        Exit;
      end;
    end;
    EdDataVct.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdDataVct.SetFocus;
end;

procedure TFmChequez.EdAgenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
  begin
    if Sender is TdmkEdit then
      TdmkEdit(Sender).Enabled := not TdmkEdit(Sender).Enabled
    else
    if Sender is TDBLookupComboBox then
    begin
      if not TCustomComboBox(Sender).DroppedDown then
      TDBLookupComboBox(Sender).Enabled := not TDBLookupComboBox(Sender).Enabled
    end;
  end else if Key = VK_F6 then
  begin
    EdAgencia.Enabled  := True;
    EdConta.Enabled    := True;
    EdCheque.Enabled   := True;
    EdValor.Enabled    := True;
    EdEmitente.Enabled := True;
    EdCPF.Enabled      := True;
    EdDataEmi.Enabled  := True;
    EdCliente.Enabled  := True;
    CBCliente.Enabled  := True;
    EdCidade.Enabled   := True;
    EdObserv.Enabled   := True;
    EdDataVct.Enabled  := True;
  end;
end;

end.

