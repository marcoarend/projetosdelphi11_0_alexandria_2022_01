unit MPIn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, UnInternalConsts,
  Db, mySQLDbTables, ComCtrls, DBCtrls, Menus, Mask, frxClass, frxDBSet,
  dmkEdit, Variants, dmkDBLookupComboBox, dmkEditCB, dmkRadioGroup, dmkGeral,
  UnGrade_Tabs, dmkEditDateTimePicker, UnDmkProcFunc, DmkDAC_PF, dmkLabel,
  dmkImage, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TQualQuery = (qqMPIn, qqMulClas);
  TFmMPIn = class(TForm)
    Panel2: TPanel;
    Panel1: TPanel;
    TPIni: TdmkEditDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    TPFim: TdmkEditDateTimePicker;
    QrMPIn: TmySQLQuery;
    DsMPIn: TDataSource;
    QrMPInNOMECLIENTEI: TWideStringField;
    QrMPInNOMEPROCEDENCIA: TWideStringField;
    QrMPInCodigo: TIntegerField;
    QrMPInControle: TAutoIncField;
    QrMPInData: TDateField;
    QrMPInProcedencia: TIntegerField;
    QrMPInMarca: TWideStringField;
    QrMPInPecas: TFloatField;
    QrMPInPNF: TFloatField;
    QrMPInPLE: TFloatField;
    QrMPInPDA: TFloatField;
    QrMPInRecorte_PDA: TFloatField;
    QrMPInPTA: TFloatField;
    QrMPInRecorte_PTA: TFloatField;
    QrMPInLk: TIntegerField;
    QrMPInDataCad: TDateField;
    QrMPInDataAlt: TDateField;
    QrMPInUserCad: TIntegerField;
    QrMPInUserAlt: TIntegerField;
    DBGrid2: TDBGrid;
    QrMPInClienteI: TIntegerField;
    QrMPInRaspa_PTA: TFloatField;
    QrMPInTipificacao: TIntegerField;
    QrMPInAparasCabelo: TSmallintField;
    QrMPInSeboPreDescarne: TSmallintField;
    QrMPInM2: TFloatField;
    QrMPInValor: TFloatField;
    RGOrdem: TRadioGroup;
    Label18: TLabel;
    CBClienteI: TdmkDBLookupComboBox;
    QrClientesI: TmySQLQuery;
    DsClientesI: TDataSource;
    QrClientesINOMECLIENTEI: TWideStringField;
    QrClientesICodigo: TIntegerField;
    QrMPInNOMECIDADEF: TWideStringField;
    QrDefeitos: TmySQLQuery;
    DsDefeitos: TDataSource;
    QrDefLoc: TmySQLQuery;
    DsDefLoc: TDataSource;
    QrDefeitosNOMETIP: TWideStringField;
    QrDefeitosMPInCtrl: TIntegerField;
    QrDefeitosControle: TIntegerField;
    QrDefeitosDefeitoloc: TIntegerField;
    QrDefeitosDefeitotip: TIntegerField;
    QrDefeitosDefeitos: TIntegerField;
    QrDefeitosLk: TIntegerField;
    QrDefeitosDataCad: TDateField;
    QrDefeitosDataAlt: TDateField;
    QrDefeitosUserCad: TIntegerField;
    QrDefeitosUserAlt: TIntegerField;
    QrDefeitosNOMELOC: TWideStringField;
    QrDefTip: TmySQLQuery;
    DsDefTip: TDataSource;
    QrDefLocDEFEITOS: TFloatField;
    QrDefLocNOMELOC: TWideStringField;
    QrDefTipDEFEITOS: TFloatField;
    QrDefTipNOMETIP: TWideStringField;
    QrMPInPecasNF: TFloatField;
    QrMPInAbateTipo: TIntegerField;
    Panel6: TPanel;
    QrSumF: TmySQLQuery;
    QrSumFDebito: TFloatField;
    DsEmissF: TDataSource;
    QrEmissF: TmySQLQuery;
    QrEmissFData: TDateField;
    QrEmissFTipo: TSmallintField;
    QrEmissFCarteira: TIntegerField;
    QrEmissFAutorizacao: TIntegerField;
    QrEmissFGenero: TIntegerField;
    QrEmissFDescricao: TWideStringField;
    QrEmissFNotaFiscal: TIntegerField;
    QrEmissFDebito: TFloatField;
    QrEmissFCredito: TFloatField;
    QrEmissFCompensado: TDateField;
    QrEmissFDocumento: TFloatField;
    QrEmissFSit: TIntegerField;
    QrEmissFVencimento: TDateField;
    QrEmissFLk: TIntegerField;
    QrEmissFNOMESIT: TWideStringField;
    QrEmissFNOMETIPO: TWideStringField;
    QrEmissFNOMECARTEIRA: TWideStringField;
    QrEmissFFornecedor: TIntegerField;
    QrEmissFCliente: TIntegerField;
    QrEmissFControle: TIntegerField;
    QrEmissFSub: TSmallintField;
    QrEmissFFatID: TIntegerField;
    QrEmissFFatParcela: TIntegerField;
    QrEmissFID_Pgto: TIntegerField;
    QrEmissFID_Sub: TSmallintField;
    QrEmissFFatura: TWideStringField;
    QrEmissFBanco: TIntegerField;
    QrEmissFLocal: TIntegerField;
    QrEmissFCartao: TIntegerField;
    QrEmissFLinha: TIntegerField;
    QrEmissFOperCount: TIntegerField;
    QrEmissFLancto: TIntegerField;
    QrEmissFPago: TFloatField;
    QrEmissFMez: TIntegerField;
    QrEmissFMoraDia: TFloatField;
    QrEmissFMulta: TFloatField;
    QrEmissFProtesto: TDateField;
    QrEmissFDataCad: TDateField;
    QrEmissFDataAlt: TDateField;
    QrEmissFUserCad: TIntegerField;
    QrEmissFUserAlt: TIntegerField;
    QrEmissFDataDoc: TDateField;
    QrEmissFCtrlIni: TIntegerField;
    QrEmissFNivel: TIntegerField;
    QrEmissFVendedor: TIntegerField;
    QrEmissFAccount: TIntegerField;
    QrEmissFCliInt: TIntegerField;
    QrSumT: TmySQLQuery;
    QrSumTDebito: TFloatField;
    DsEmissT: TDataSource;
    QrEmissT: TmySQLQuery;
    QrEmissTCarteira: TIntegerField;
    QrEmissTDescricao: TWideStringField;
    QrEmissTNotaFiscal: TIntegerField;
    QrEmissTDebito: TFloatField;
    QrEmissTCompensado: TDateField;
    QrEmissTDocumento: TFloatField;
    QrEmissTSit: TIntegerField;
    QrEmissTVencimento: TDateField;
    QrEmissTData: TDateField;
    QrEmissTTipo: TSmallintField;
    QrEmissTAutorizacao: TIntegerField;
    QrEmissTGenero: TIntegerField;
    QrEmissTCredito: TFloatField;
    QrEmissTLk: TIntegerField;
    QrEmissTNOMESIT: TWideStringField;
    QrEmissTNOMETIPO: TWideStringField;
    QrEmissTNOMECARTEIRA: TWideStringField;
    QrEmissTFornecedor: TIntegerField;
    QrEmissTCliente: TIntegerField;
    QrEmissTControle: TIntegerField;
    QrEmissTSub: TSmallintField;
    QrEmissTFatID: TIntegerField;
    QrEmissTFatParcela: TIntegerField;
    QrEmissTID_Pgto: TIntegerField;
    QrEmissTID_Sub: TSmallintField;
    QrEmissTFatura: TWideStringField;
    QrEmissTBanco: TIntegerField;
    QrEmissTLocal: TIntegerField;
    QrEmissTCartao: TIntegerField;
    QrEmissTLinha: TIntegerField;
    QrEmissTOperCount: TIntegerField;
    QrEmissTLancto: TIntegerField;
    QrEmissTPago: TFloatField;
    QrEmissTMez: TIntegerField;
    QrEmissTMoraDia: TFloatField;
    QrEmissTMulta: TFloatField;
    QrEmissTProtesto: TDateField;
    QrEmissTDataCad: TDateField;
    QrEmissTDataAlt: TDateField;
    QrEmissTUserCad: TIntegerField;
    QrEmissTUserAlt: TIntegerField;
    QrEmissTDataDoc: TDateField;
    QrEmissTCtrlIni: TIntegerField;
    QrEmissTNivel: TIntegerField;
    QrEmissTVendedor: TIntegerField;
    QrEmissTAccount: TIntegerField;
    QrEmissTCliInt: TIntegerField;
    GroupBox7: TGroupBox;
    DBGrid5: TDBGrid;
    GroupBox8: TGroupBox;
    DBGrid6: TDBGrid;
    PMEdita: TPopupMenu;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    PMInsere: TPopupMenu;
    Matriaprima1: TMenuItem;
    Duplicatatranportadora1: TMenuItem;
    Duplicatafornecedor1: TMenuItem;
    N1: TMenuItem;
    Defeito1: TMenuItem;
    PMExclui: TPopupMenu;
    QrMPInTransportadora: TIntegerField;
    QrEmissFQtde: TFloatField;
    QrEmissFFatID_Sub: TIntegerField;
    QrEmissFEmitente: TWideStringField;
    QrEmissFContaCorrente: TWideStringField;
    QrEmissFCNPJCPF: TWideStringField;
    QrEmissFForneceI: TIntegerField;
    QrEmissFICMS_P: TFloatField;
    QrEmissFICMS_V: TFloatField;
    QrEmissFDuplicata: TWideStringField;
    QrEmissFDepto: TIntegerField;
    QrEmissFDescoPor: TIntegerField;
    QrEmissFDescoVal: TFloatField;
    QrEmissFDescoControle: TIntegerField;
    QrEmissFNFVal: TFloatField;
    QrEmissTQtde: TFloatField;
    QrEmissTFatID_Sub: TIntegerField;
    QrEmissTEmitente: TWideStringField;
    QrEmissTContaCorrente: TWideStringField;
    QrEmissTCNPJCPF: TWideStringField;
    QrEmissTForneceI: TIntegerField;
    QrEmissTICMS_P: TFloatField;
    QrEmissTICMS_V: TFloatField;
    QrEmissTDuplicata: TWideStringField;
    QrEmissTDepto: TIntegerField;
    QrEmissTDescoPor: TIntegerField;
    QrEmissTDescoVal: TFloatField;
    QrEmissTDescoControle: TIntegerField;
    QrEmissTNFVal: TFloatField;
    ExcluiEntrada1: TMenuItem;
    Excluipagamentotransporte1: TMenuItem;
    Excluipagamentomatriaprima1: TMenuItem;
    ExcluiDefeito1: TMenuItem;
    QrMPInFicha: TIntegerField;
    QrMPInFrete: TFloatField;
    QrMPInQuebraPLE: TFloatField;
    QrMPInQuebraPDA: TFloatField;
    QrMPInQuebraAPD: TFloatField;
    QrMPInQuebraPTA: TFloatField;
    QrMPInQuebraAPT: TFloatField;
    QrMPInQuebraCAL: TFloatField;
    GroupBox1: TGroupBox;
    DBGrid7: TDBGrid;
    QrEmitCus: TmySQLQuery;
    DsEmitCus: TDataSource;
    PnFormulas: TPanel;
    EdPesagem: TdmkEdit;
    Label3: TLabel;
    QrEmit: TmySQLQuery;
    Label4: TLabel;
    EdPesoF: TdmkEdit;
    QrEmitCodigo: TIntegerField;
    QrEmitDataEmis: TDateTimeField;
    QrEmitStatus: TSmallintField;
    QrEmitNumero: TIntegerField;
    QrEmitNOMECI: TWideStringField;
    QrEmitNOMESETOR: TWideStringField;
    QrEmitTecnico: TWideStringField;
    QrEmitNOME: TWideStringField;
    QrEmitClienteI: TIntegerField;
    QrEmitTipificacao: TSmallintField;
    QrEmitTempoR: TSmallintField;
    QrEmitTempoP: TSmallintField;
    QrEmitSetor: TSmallintField;
    QrEmitEspessura: TWideStringField;
    QrEmitPeso: TFloatField;
    QrEmitQtde: TFloatField;
    QrEmitLk: TIntegerField;
    QrEmitDataCad: TDateField;
    QrEmitDataAlt: TDateField;
    QrEmitUserCad: TIntegerField;
    QrEmitUserAlt: TIntegerField;
    QrEmitAreaM2: TFloatField;
    QrEmitFulao: TWideStringField;
    QrEmitDefPeca: TWideStringField;
    QrEmitCusto: TFloatField;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    DsEmit: TDataSource;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label9: TLabel;
    DBEdit6: TDBEdit;
    Pesagem1: TMenuItem;
    QrEmitCusCodigo: TIntegerField;
    QrEmitCusControle: TIntegerField;
    QrEmitCusMPIn: TIntegerField;
    QrEmitCusPeso: TFloatField;
    QrEmitCusCusto: TFloatField;
    QrEmitCusLk: TIntegerField;
    QrEmitCusDataCad: TDateField;
    QrEmitCusDataAlt: TDateField;
    QrEmitCusUserCad: TIntegerField;
    QrEmitCusUserAlt: TIntegerField;
    QrEmitCusFormula: TIntegerField;
    QrEmitCusDataEmis: TDateTimeField;
    QrMPInCustoPQ: TFloatField;
    RetiraPesagem1: TMenuItem;
    QrEmitCusNOMEFORMULA: TWideStringField;
    QrDefTot: TmySQLQuery;
    QrDefTotDEFEITOS: TFloatField;
    QrDefTipPercTot: TFloatField;
    QrDefLocPercTot: TFloatField;
    QrDefTotMEDIACOURO: TFloatField;
    QrMPInLote: TWideStringField;
    Itensdematriaprima1: TMenuItem;
    QrIts: TmySQLQuery;
    QrItsPecas: TFloatField;
    QrItsPecasNF: TFloatField;
    QrItsM2: TFloatField;
    QrItsPNF: TFloatField;
    QrItsPLE: TFloatField;
    QrItsPDA: TFloatField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrMPInIts: TmySQLQuery;
    DsMPInIts: TDataSource;
    DBGrid8: TDBGrid;
    QrMPInItsCodigo: TIntegerField;
    QrMPInItsControle: TIntegerField;
    QrMPInItsConta: TIntegerField;
    QrMPInItsPecas: TFloatField;
    QrMPInItsPecasNF: TFloatField;
    QrMPInItsM2: TFloatField;
    QrMPInItsPNF: TFloatField;
    QrMPInItsPLE: TFloatField;
    QrMPInItsPDA: TFloatField;
    QrMPInItsRecorte_PDA: TFloatField;
    QrMPInItsPTA: TFloatField;
    QrMPInItsRecorte_PTA: TFloatField;
    QrMPInItsRaspa_PTA: TFloatField;
    QrMPInItsLk: TIntegerField;
    QrMPInItsDataCad: TDateField;
    QrMPInItsDataAlt: TDateField;
    QrMPInItsUserCad: TIntegerField;
    QrMPInItsUserAlt: TIntegerField;
    QrMPInItsAlterWeb: TSmallintField;
    QrMPInItsAtivo: TSmallintField;
    QrMPInItsCaminhao: TIntegerField;
    Itemdematriaprima1: TMenuItem;
    Itemdematriaprima2: TMenuItem;
    QrMPInItsCustoInfo: TFloatField;
    QrMPInItsFreteInfo: TFloatField;
    QrMPInCustoInfo: TFloatField;
    QrMPInFreteInfo: TFloatField;
    QrItsCustoInfo: TFloatField;
    QrItsFreteInfo: TFloatField;
    QrEmitCusPecas: TFloatField;
    QrEmitCusAlterWeb: TSmallintField;
    QrEmitCusAtivo: TSmallintField;
    QrEmitCusFulao: TWideStringField;
    QrMPInQuebraPDA_0: TFloatField;
    QrMPInQuebraPDA_1: TFloatField;
    QrMPInPDA_0: TFloatField;
    QrMPInPDA_1: TFloatField;
    QrMPInPLE_0: TFloatField;
    QrMPInPLE_1: TFloatField;
    QrMPInCMP_PDA_INFO: TFloatField;
    QrMPInCMP_TOT_INFO: TFloatField;
    QrMPInCPQ_PNF: TFloatField;
    QrMPInItsCMPValor: TFloatField;
    QrMPInItsCMPFrete: TFloatField;
    QrMPInItsCPLValor: TFloatField;
    QrMPInItsCPLFrete: TFloatField;
    QrMPInItsAGIValor: TFloatField;
    QrMPInItsAGIFrete: TFloatField;
    QrMPInItsCMIValor: TFloatField;
    QrMPInItsCMIFrete: TFloatField;
    QrMPInItsTotalInfo: TFloatField;
    QrMPInItsFinalInfo: TFloatField;
    QrMax: TmySQLQuery;
    QrMaxCaminhao: TIntegerField;
    QrMPInCaminhoes: TIntegerField;
    QrMPInItsDolar: TFloatField;
    QrMPInItsEuro: TFloatField;
    QrMPInItsIndexador: TFloatField;
    QrMPInItsCMPDevol: TFloatField;
    QrMPInItsCPLDevol: TFloatField;
    QrMPInItsAGIDevol: TFloatField;
    QrMPInItsCMIDevol: TFloatField;
    frxPedidoG1_: TfrxReport;
    PMImprime: TPopupMenu;
    Defeitos1: TMenuItem;
    Entrada1: TMenuItem;
    Quebra1: TMenuItem;
    OS1: TMenuItem;
    Pedidodecompra1: TMenuItem;
    frxDsMPIn: TfrxDBDataset;
    QrMPInNOMEUFF: TWideStringField;
    QrMPInTEL1F: TWideStringField;
    QrMPInTEL2F: TWideStringField;
    dmkEdOS: TdmkEdit;
    Label10: TLabel;
    QrMPInQuemAssina: TIntegerField;
    QrMPInComisPer: TFloatField;
    QrMPInComisVal: TFloatField;
    QrMPInDescAdiant: TFloatField;
    QrMPInLocalEntrg: TWideStringField;
    QrMPInObserv: TWideStringField;
    QrMPInCorretor: TIntegerField;
    QrMPInCondPagto: TWideStringField;
    QrMPInCondComis: TWideStringField;
    QrMPInPecasOut: TFloatField;
    QrMPInPecasSal: TFloatField;
    QrMPInAlterWeb: TSmallintField;
    QrMPInAtivo: TSmallintField;
    QrMPInCMPUnida: TSmallintField;
    QrMPInCMPUnidaTXT: TWideStringField;
    RGTipifNome: TRadioGroup;
    QrMPInTipificDESCRI: TWideStringField;
    QrMPInAnimal: TSmallintField;
    QrMPInANIMALNOME: TWideStringField;
    RGAnimal: TRadioGroup;
    QrMPInDESCRI_ANIMAL: TWideStringField;
    frxDsMPInIts: TfrxDBDataset;
    QrMPInItsCMPUnida: TSmallintField;
    QrMPInItsCMPPreco: TFloatField;
    QrMPInItsSEQ: TIntegerField;
    QrMPInItsQUANTIDADE: TFloatField;
    QrItsCMPValor: TFloatField;
    QrMPInCMPValor: TFloatField;
    QrMPInCMP_IDQV: TSmallintField;
    QrMPInCMP_LPQV: TFloatField;
    QrMPInQuebrVReal: TFloatField;
    QrMPInQuebrVCobr: TFloatField;
    QrMPInCMPPagar: TFloatField;
    QrMPInCMPDesQV: TFloatField;
    QrMPInNOMECORRETOR: TWideStringField;
    QrMPInContatos: TWideStringField;
    QrMPInNOMEASSINA: TWideStringField;
    QrMPInItsCMPMoeda: TSmallintField;
    QrMPInItsCMPCotac: TFloatField;
    QrMPInItsCMPReais: TFloatField;
    QrItsCMPFrete: TFloatField;
    QrMPInCMPFrete: TFloatField;
    QrIn2: TmySQLQuery;
    QrIn2ComisPer: TFloatField;
    QrMPInItsFornece: TIntegerField;
    Rateiacaminho1: TMenuItem;
    QrMPInItsPNF_Fat: TFloatField;
    QrIn2CMP_IDQV: TSmallintField;
    QrIn2CMP_LPQV: TFloatField;
    QrIn2DescAdiant: TFloatField;
    frxPedidoU_: TfrxReport;
    frxPedidoF_: TfrxReport;
    QrMPInIts2: TmySQLQuery;
    frxDsMPInIts2: TfrxDBDataset;
    QrMPInIts2CMPUnida: TSmallintField;
    QrMPInIts2CMP_IDQV: TSmallintField;
    QrMPInIts2CMP_LPQV: TFloatField;
    QrMPInIts2Contatos: TWideStringField;
    QrMPInIts2NOMECLIENTEI: TWideStringField;
    QrMPInIts2NOMECIDADEF: TWideStringField;
    QrMPInIts2NOMECORRETOR: TWideStringField;
    QrMPInIts2NOMEASSINA: TWideStringField;
    QrMPInIts2NOMEUFF: TWideStringField;
    QrMPInIts2TEL1F: TWideStringField;
    QrMPInIts2TEL2F: TWideStringField;
    QrMPInIts2NOMEPROCEDENCIA: TWideStringField;
    QrMPInIts2Codigo: TIntegerField;
    QrMPInIts2Controle: TIntegerField;
    QrMPInIts2Ficha: TIntegerField;
    QrMPInIts2Data: TDateField;
    QrMPInIts2ClienteI: TIntegerField;
    QrMPInIts2Procedencia: TIntegerField;
    QrMPInIts2Transportadora: TIntegerField;
    QrMPInIts2Marca: TWideStringField;
    QrMPInIts2Pecas: TFloatField;
    QrMPInIts2PecasNF: TFloatField;
    QrMPInIts2M2: TFloatField;
    QrMPInIts2PNF: TFloatField;
    QrMPInIts2PLE: TFloatField;
    QrMPInIts2PDA: TFloatField;
    QrMPInIts2Recorte_PDA: TFloatField;
    QrMPInIts2PTA: TFloatField;
    QrMPInIts2Recorte_PTA: TFloatField;
    QrMPInIts2Raspa_PTA: TFloatField;
    QrMPInIts2Tipificacao: TIntegerField;
    QrMPInIts2AparasCabelo: TSmallintField;
    QrMPInIts2SeboPreDescarne: TSmallintField;
    QrMPInIts2Valor: TFloatField;
    QrMPInIts2CustoPQ: TFloatField;
    QrMPInIts2Frete: TFloatField;
    QrMPInIts2AbateTipo: TIntegerField;
    QrMPInIts2Lote: TWideStringField;
    QrMPInIts2PecasOut: TFloatField;
    QrMPInIts2PecasSal: TFloatField;
    QrMPInIts2Lk: TIntegerField;
    QrMPInIts2DataCad: TDateField;
    QrMPInIts2DataAlt: TDateField;
    QrMPInIts2UserCad: TIntegerField;
    QrMPInIts2UserAlt: TIntegerField;
    QrMPInIts2AlterWeb: TSmallintField;
    QrMPInIts2Ativo: TSmallintField;
    QrMPInIts2Caminhoes: TIntegerField;
    QrMPInIts2CustoInfo: TFloatField;
    QrMPInIts2FreteInfo: TFloatField;
    QrMPInIts2QuemAssina: TIntegerField;
    QrMPInIts2ComisPer: TFloatField;
    QrMPInIts2ComisVal: TFloatField;
    QrMPInIts2DescAdiant: TFloatField;
    QrMPInIts2LocalEntrg: TWideStringField;
    QrMPInIts2Observ: TWideStringField;
    QrMPInIts2Corretor: TIntegerField;
    QrMPInIts2CondPagto: TWideStringField;
    QrMPInIts2CondComis: TWideStringField;
    QrMPInIts2Animal: TSmallintField;
    QrMPInIts2CMPValor: TFloatField;
    QrMPInIts2QuebrVReal: TFloatField;
    QrMPInIts2QuebrVCobr: TFloatField;
    QrMPInIts2CMPPagar: TFloatField;
    QrMPInIts2CMPDesQV: TFloatField;
    QrMPInIts2CMPFrete: TFloatField;
    frxPedidoG2: TfrxReport;
    QrMPInItsNOMEPROCEDENCIA: TWideStringField;
    QrMPInFre: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    FloatField6: TFloatField;
    FloatField7: TFloatField;
    FloatField8: TFloatField;
    FloatField9: TFloatField;
    FloatField10: TFloatField;
    IntegerField4: TIntegerField;
    DateField1: TDateField;
    DateField2: TDateField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    SmallintField1: TSmallintField;
    SmallintField2: TSmallintField;
    IntegerField7: TIntegerField;
    FloatField11: TFloatField;
    FloatField12: TFloatField;
    FloatField13: TFloatField;
    FloatField14: TFloatField;
    FloatField15: TFloatField;
    FloatField16: TFloatField;
    FloatField17: TFloatField;
    FloatField18: TFloatField;
    FloatField19: TFloatField;
    FloatField20: TFloatField;
    FloatField21: TFloatField;
    FloatField22: TFloatField;
    FloatField23: TFloatField;
    FloatField24: TFloatField;
    FloatField25: TFloatField;
    FloatField26: TFloatField;
    FloatField27: TFloatField;
    FloatField28: TFloatField;
    FloatField29: TFloatField;
    SmallintField3: TSmallintField;
    FloatField30: TFloatField;
    IntegerField8: TIntegerField;
    FloatField31: TFloatField;
    SmallintField4: TSmallintField;
    FloatField32: TFloatField;
    FloatField33: TFloatField;
    IntegerField9: TIntegerField;
    FloatField34: TFloatField;
    StringField1: TWideStringField;
    QrMPInItsEmissao: TDateField;
    QrMPInItsVencto: TDateField;
    QrMPInItsNF: TIntegerField;
    QrMPInItsConheci: TIntegerField;
    frxPedidoF2: TfrxReport;
    PedidodeFrete1: TMenuItem;
    N3: TMenuItem;
    QrMPInItsEmisFrete: TDateField;
    QrMPInItsVctoFrete: TDateField;
    QrMPInItsVENCTO_TXT: TWideStringField;
    QrMPInItsVCTOFRETE_TXT: TWideStringField;
    QrMPInItsEMISFRETE_TXT: TWideStringField;
    Modelo11: TMenuItem;
    Modelo21: TMenuItem;
    frxDsEntrada: TfrxDBDataset;
    frxQuebra1: TfrxReport;
    frxQuebra2: TfrxReport;
    QrMPInFimM2: TFloatField;
    QrMPInFimP2: TFloatField;
    QrEmissFFatNum: TFloatField;
    QrEmissTFatNum: TFloatField;
    frxEntrada1A: TfrxReport;
    frxOS: TfrxReport;
    frxDefeitos: TfrxReport;
    frxDBDefeitos: TfrxDBDataset;
    frxDBDataset1: TfrxDBDataset;
    frxDBDataset2: TfrxDBDataset;
    frxDBDataset3: TfrxDBDataset;
    PMRecalcula: TPopupMenu;
    CustodePQtodoslotesmostradosnagrade1: TMenuItem;
    Perdadecouro1: TMenuItem;
    QrMPInPerdas: TmySQLQuery;
    DsMPInPerdas: TDataSource;
    QrMPInPerdasMPInCtrl: TIntegerField;
    QrMPInPerdasControle: TIntegerField;
    QrMPInPerdasPecas: TIntegerField;
    QrMPInPerdasM2: TFloatField;
    QrMPInPerdasP2: TFloatField;
    QrMPInPerdasDataHora: TDateTimeField;
    QrMPInPerdasSetor: TIntegerField;
    QrMPInPerdasMotivo: TIntegerField;
    QrMPInPerdasNOMESETOR: TWideStringField;
    QrMPInPerdasNOMEMOTIVO: TWideStringField;
    ExcluiPerdadecouro1: TMenuItem;
    QrMPInPerdasDH_TXT: TWideStringField;
    N4: TMenuItem;
    Label11: TLabel;
    CBProcedencia: TdmkDBLookupComboBox;
    EdLote: TEdit;
    EdMarca: TEdit;
    Label12: TLabel;
    Label13: TLabel;
    QrProcedencias: TmySQLQuery;
    DsProcedencias: TDataSource;
    QrProcedenciasCodigo: TIntegerField;
    QrProcedenciasNOMECLIENTEI: TWideStringField;
    EdClienteI: TdmkEditCB;
    EdProcedencia: TdmkEditCB;
    RGEncerrado: TdmkRadioGroup;
    N5: TMenuItem;
    Encerralote1: TMenuItem;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    QrEstq1: TmySQLQuery;
    frxDsEstq1: TfrxDBDataset;
    QrEstq1KGT: TIntegerField;
    Estoqueeproduodiria1: TMenuItem;
    QrEstq1NOMECLIENTEI: TWideStringField;
    QrEstq1NOMEPROCEDENCIA: TWideStringField;
    QrEstq1Codigo: TIntegerField;
    QrEstq1Controle: TIntegerField;
    QrEstq1Ficha: TIntegerField;
    QrEstq1Data: TDateField;
    QrEstq1ClienteI: TIntegerField;
    QrEstq1Procedencia: TIntegerField;
    QrEstq1Transportadora: TIntegerField;
    QrEstq1Marca: TWideStringField;
    QrEstq1Pecas: TFloatField;
    QrEstq1PecasNF: TFloatField;
    QrEstq1M2: TFloatField;
    QrEstq1P2: TFloatField;
    QrEstq1FimM2: TFloatField;
    QrEstq1FimP2: TFloatField;
    QrEstq1PNF: TFloatField;
    QrEstq1PLE: TFloatField;
    QrEstq1PDA: TFloatField;
    QrEstq1Recorte_PDA: TFloatField;
    QrEstq1PTA: TFloatField;
    QrEstq1Recorte_PTA: TFloatField;
    QrEstq1Raspa_PTA: TFloatField;
    QrEstq1Tipificacao: TIntegerField;
    QrEstq1AparasCabelo: TSmallintField;
    QrEstq1SeboPreDescarne: TSmallintField;
    QrEstq1CustoInfo: TFloatField;
    QrEstq1FreteInfo: TFloatField;
    QrEstq1Valor: TFloatField;
    QrEstq1CustoPQ: TFloatField;
    QrEstq1Frete: TFloatField;
    QrEstq1AbateTipo: TIntegerField;
    QrEstq1Lote: TWideStringField;
    QrEstq1PecasOut: TFloatField;
    QrEstq1PecasSal: TFloatField;
    QrEstq1Caminhoes: TIntegerField;
    QrEstq1QuemAssina: TIntegerField;
    QrEstq1Corretor: TIntegerField;
    QrEstq1ComisPer: TFloatField;
    QrEstq1ComisVal: TFloatField;
    QrEstq1DescAdiant: TFloatField;
    QrEstq1CondPagto: TWideStringField;
    QrEstq1CondComis: TWideStringField;
    QrEstq1LocalEntrg: TWideStringField;
    QrEstq1Observ: TWideStringField;
    QrEstq1Animal: TSmallintField;
    QrEstq1CMPValor: TFloatField;
    QrEstq1CMPFrete: TFloatField;
    QrEstq1QuebrVReal: TFloatField;
    QrEstq1QuebrVCobr: TFloatField;
    QrEstq1CMPDesQV: TFloatField;
    QrEstq1CMPPagar: TFloatField;
    QrEstq1PcBxa: TFloatField;
    QrEstq1kgBxa: TFloatField;
    QrEstq1M2Bxa: TFloatField;
    QrEstq1P2Bxa: TFloatField;
    QrEstq1Lk: TIntegerField;
    QrEstq1DataCad: TDateField;
    QrEstq1DataAlt: TDateField;
    QrEstq1UserCad: TIntegerField;
    QrEstq1UserAlt: TIntegerField;
    QrEstq1AlterWeb: TSmallintField;
    QrEstq1Ativo: TSmallintField;
    QrEstq1PecasCal: TFloatField;
    QrEstq1PecasCur: TFloatField;
    QrEstq1PecasNeg: TFloatField;
    QrEstq1PesoCal: TFloatField;
    QrEstq1PesoCur: TFloatField;
    QrEstq1CusPQCal: TFloatField;
    QrEstq1CusPQCur: TFloatField;
    QrEstq1MinDtaCal: TDateField;
    QrEstq1MinDtaCur: TDateField;
    QrEstq1MinDtaEnx: TDateField;
    QrEstq1MaxDtaCal: TDateField;
    QrEstq1MaxDtaCur: TDateField;
    QrEstq1MaxDtaEnx: TDateField;
    QrEstq1FuloesCal: TIntegerField;
    QrEstq1FuloesCur: TIntegerField;
    QrEstq1PecasExp: TFloatField;
    QrEstq1Encerrado: TSmallintField;
    QrEstq1M2Exp: TFloatField;
    QrEstq1P2Exp: TFloatField;
    QrEstq1ForcaEncer: TSmallintField;
    QrEstq1MINDTACAL_TXT: TWideStringField;
    QrEstq1MINDTACUR_TXT: TWideStringField;
    QrEstq1MINDTAENX_TXT: TWideStringField;
    QrEstq1MAXDTACAL_TXT: TWideStringField;
    QrEstq1MAXDTACUR_TXT: TWideStringField;
    QrEstq1MAXDTAENX_TXT: TWideStringField;
    frxEstq1: TfrxReport;
    QrEstq1MEDIA_PLE: TFloatField;
    QrEstq1CUSKGPQCAL: TFloatField;
    QrEstq1CUSKGPQCUR: TFloatField;
    QrEstq1CUSTOKGPLE: TFloatField;
    QrEstq1QBR_PLE_PDA: TFloatField;
    QrEstq1QBR_PDA_PTA: TFloatField;
    QrEstq1QBR_PLE_PTA: TFloatField;
    QrMPInItsPS_ValUni: TFloatField;
    QrMPInItsPS_QtdTot: TFloatField;
    QrMPInItsPS_ValTot: TFloatField;
    QrItsPS_QtdTot: TFloatField;
    QrItsPS_ValTot: TFloatField;
    QrMPInPS_ValUni: TFloatField;
    QrMPInPS_QtdTot: TFloatField;
    QrMPInPS_ValTot: TFloatField;
    QrEstq1Sado_Pc_Exp: TFloatField;
    QrEstq1Sado_Pc_Cur: TFloatField;
    QrEstq1Sado_Pc_Cal: TFloatField;
    QrEstq1Sado_Pc_Ini: TFloatField;
    TabSheet8: TTabSheet;
    QrSMI_101: TmySQLQuery;
    QrSMI_101GRATAMCAD: TIntegerField;
    QrSMI_101GRATAMITS: TIntegerField;
    QrSMI_101NO_TAM: TWideStringField;
    QrSMI_101NO_COR: TWideStringField;
    QrSMI_101CU_GraGru1: TIntegerField;
    QrSMI_101NO_GraGru1: TWideStringField;
    QrSMI_101DataHora: TDateTimeField;
    QrSMI_101IDCtrl: TIntegerField;
    QrSMI_101Tipo: TSmallintField;
    QrSMI_101OriCodi: TIntegerField;
    QrSMI_101OriCtrl: TIntegerField;
    QrSMI_101OriCnta: TIntegerField;
    QrSMI_101OriPart: TIntegerField;
    QrSMI_101Empresa: TIntegerField;
    QrSMI_101StqCenCad: TIntegerField;
    QrSMI_101GraGruX: TIntegerField;
    QrSMI_101Qtde: TFloatField;
    QrSMI_101Peso: TFloatField;
    DsSMI_101: TDataSource;
    QrMPInItsNF_Inn_Fut: TSmallintField;
    QrMPInItsNF_Inn_Its: TIntegerField;
    DadosdaNF1: TMenuItem;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAStatus: TSmallintField;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    QrNFeCabAinfCanc_cStat: TIntegerField;
    QrNFeCabAinfCanc_xMotivo: TWideStringField;
    QrNFeCabAcStat: TIntegerField;
    QrNFeCabAxMotivo: TWideStringField;
    QrNFeCabAcStat_xMotivo: TWideStringField;
    QrNFeCabAide_cNF: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabASTATUS_TXT: TWideStringField;
    DsNFeCabA: TDataSource;
    Panel7: TPanel;
    QrNFeCabAICMSRec_pRedBC: TFloatField;
    QrNFeCabAICMSRec_vBC: TFloatField;
    QrNFeCabAICMSRec_pAliq: TFloatField;
    QrNFeCabAICMSRec_vICMS: TFloatField;
    QrNFeCabAIPIRec_pRedBC: TFloatField;
    QrNFeCabAIPIRec_vBC: TFloatField;
    QrNFeCabAIPIRec_pAliq: TFloatField;
    QrNFeCabAIPIRec_vIPI: TFloatField;
    QrNFeCabAPISRec_pRedBC: TFloatField;
    QrNFeCabAPISRec_vBC: TFloatField;
    QrNFeCabAPISRec_pAliq: TFloatField;
    QrNFeCabAPISRec_vPIS: TFloatField;
    QrNFeCabACOFINSRec_pRedBC: TFloatField;
    QrNFeCabACOFINSRec_vBC: TFloatField;
    QrNFeCabACOFINSRec_pAliq: TFloatField;
    QrNFeCabACOFINSRec_vCOFINS: TFloatField;
    NFe1: TMenuItem;
    NF1: TMenuItem;
    DadosdaNF2: TMenuItem;
    QrNFeCabAIDCtrl: TIntegerField;
    N6: TMenuItem;
    N7: TMenuItem;
    RGtpNF: TdmkRadioGroup;
    Label19: TLabel;
    EdNF: TdmkEdit;
    NFe2: TMenuItem;
    NFNormal1: TMenuItem;
    GroupBox2: TGroupBox;
    Label14: TLabel;
    DBEdit7: TDBEdit;
    Label16: TLabel;
    DBEdit9: TDBEdit;
    Label17: TLabel;
    DBEdit10: TDBEdit;
    Label15: TLabel;
    DBEdit8: TDBEdit;
    QrSMI_101AreaM2: TFloatField;
    QrSMI_101AreaP2: TFloatField;
    QrMPInForcaEncer: TSmallintField;
    QrMPInEncerrado: TSmallintField;
    QrMPInEstq_Pecas: TFloatField;
    QrMPInEstq_M2: TFloatField;
    QrMPInEstq_P2: TFloatField;
    QrMPInEstq_Peso: TFloatField;
    ExcluiitemdeClassificao1: TMenuItem;
    ClassificaoExpedio1: TMenuItem;
    QrSMI_101Pecas: TFloatField;
    N2: TMenuItem;
    Estoque1: TMenuItem;
    QrIn2StqMovIts: TIntegerField;
    frxEntrada2A: TfrxReport;
    frxEntrada0A: TfrxReport;
    Itematual1: TMenuItem;
    porLote1: TMenuItem;
    QrSMI_101SMIMultIns: TIntegerField;
    QrMPInItsNF_modelo: TWideStringField;
    QrMPInItsNF_Serie: TWideStringField;
    QrMPInItsCFOP: TWideStringField;
    QrMPInItsEmitNFAvul: TIntegerField;
    Geraodesubprodutos1: TMenuItem;
    porLote2: TMenuItem;
    pelaPesquisa1: TMenuItem;
    QrIn2Tipificacao: TIntegerField;
    QrIn2Animal: TSmallintField;
    QrSMIA: TmySQLQuery;
    QrSMIAParCodi: TIntegerField;
    DsSMI_104: TDataSource;
    QrSMI_104: TmySQLQuery;
    TabSheet9: TTabSheet;
    Panel4: TPanel;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet4: TTabSheet;
    DBGrid4: TDBGrid;
    TabSheet5: TTabSheet;
    DBGrid3: TDBGrid;
    PageControl3: TPageControl;
    TabSheet6: TTabSheet;
    DBGrid9: TDBGrid;
    TabSheet7: TTabSheet;
    DBGrid10: TDBGrid;
    DBGStqMovIts: TDBGrid;
    DBGrid11: TDBGrid;
    QrSMI_104GRATAMCAD: TIntegerField;
    QrSMI_104GRATAMITS: TIntegerField;
    QrSMI_104NO_TAM: TWideStringField;
    QrSMI_104NO_COR: TWideStringField;
    QrSMI_104CU_GraGru1: TIntegerField;
    QrSMI_104NO_GraGru1: TWideStringField;
    QrSMI_104DataHora: TDateTimeField;
    QrSMI_104IDCtrl: TIntegerField;
    QrSMI_104Tipo: TSmallintField;
    QrSMI_104OriCodi: TIntegerField;
    QrSMI_104OriCtrl: TIntegerField;
    QrSMI_104OriCnta: TIntegerField;
    QrSMI_104OriPart: TIntegerField;
    QrSMI_104Empresa: TIntegerField;
    QrSMI_104StqCenCad: TIntegerField;
    QrSMI_104GraGruX: TIntegerField;
    QrSMI_104Qtde: TFloatField;
    QrSMI_104Peso: TFloatField;
    QrSMI_104AreaM2: TFloatField;
    QrSMI_104AreaP2: TFloatField;
    QrSMI_104Pecas: TFloatField;
    QrSMI_104SMIMultIns: TIntegerField;
    Baixadeestoque1: TMenuItem;
    Produtoclassificadoousubprodgerado1: TMenuItem;
    Panel8: TPanel;
    RGPesq_Animal: TRadioGroup;
    RGPesq_Tipificacao: TRadioGroup;
    CkPesq_Animal: TCheckBox;
    CkPesq_Tipificacao: TCheckBox;
    ModeloA1: TMenuItem;
    ModeloB1: TMenuItem;
    frxEntrada2B: TfrxReport;
    frxEntrada1B: TfrxReport;
    frxEntrada0B: TfrxReport;
    Label20: TLabel;
    EdSubFornece: TdmkEditCB;
    CBSubFornece: TdmkDBLookupComboBox;
    QrSubForneced: TmySQLQuery;
    DsSubForneced: TDataSource;
    QrSubFornecedCodigo: TIntegerField;
    QrSubFornecedNOMECLIENTEI: TWideStringField;
    QrEmissFAgencia: TIntegerField;
    QrEmissTAgencia: TIntegerField;
    RecebimentodeMP1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel9: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBControle: TGroupBox;
    Panel10: TPanel;
    PnSaiDesis: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtImprime: TBitBtn;
    BtRecalcula: TBitBtn;
    PB1: TProgressBar;
    BtCancela: TBitBtn;
    GBConfirma: TGroupBox;
    Panel5: TPanel;
    Panel11: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    QrMPInCodCliInt: TIntegerField;
    MeAvisos: TMemo;
    procedure FormActivate(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure RGOrdemClick(Sender: TObject);
    procedure EdClienteIChange(Sender: TObject);
    procedure QrMPInAfterScroll(DataSet: TDataSet);
    procedure N1Click(Sender: TObject);
    procedure Defeito1Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure Duplicatatranportadora1Click(Sender: TObject);
    procedure Duplicatafornecedor1Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure Excluipagamentotransporte1Click(Sender: TObject);
    procedure Excluipagamentomatriaprima1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMPInCalcFields(DataSet: TDataSet);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdPesagemExit(Sender: TObject);
    procedure EdPesoFExit(Sender: TObject);
    procedure Pesagem1Click(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure ExcluiDefeito1Click(Sender: TObject);
    procedure ExcluiEntrada1Click(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure RetiraPesagem1Click(Sender: TObject);
    procedure QrDefTipCalcFields(DataSet: TDataSet);
    procedure QrDefLocCalcFields(DataSet: TDataSet);
    procedure QrDefTotCalcFields(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure Itensdematriaprima1Click(Sender: TObject);
    procedure Itemdematriaprima1Click(Sender: TObject);
    procedure Itemdematriaprima2Click(Sender: TObject);
    procedure Defeitos1Click(Sender: TObject);
    procedure OS1Click(Sender: TObject);
    procedure QrMPInItsCalcFields(DataSet: TDataSet);
    procedure Rateiacaminho1Click(Sender: TObject);
    procedure Pedidodecompra1Click(Sender: TObject);
    procedure PedidodeFrete1Click(Sender: TObject);
    procedure Modelo11Click(Sender: TObject);
    procedure Modelo21Click(Sender: TObject);
    procedure frxQuebra2GetValue(const VarName: String;
      var Value: Variant);
    procedure BtRecalculaClick(Sender: TObject);
    procedure CustodePQtodoslotesmostradosnagrade1Click(Sender: TObject);
    procedure Perdadecouro1Click(Sender: TObject);
    procedure ExcluiPerdadecouro1Click(Sender: TObject);
    procedure EdLoteChange(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure QrEstq1CalcFields(DataSet: TDataSet);
    procedure Estoqueeproduodiria1Click(Sender: TObject);
    procedure QrMPInItsBeforeClose(DataSet: TDataSet);
    procedure QrMPInItsAfterScroll(DataSet: TDataSet);
    procedure NF1Click(Sender: TObject);
    procedure PMInserePopup(Sender: TObject);
    procedure PMEditaPopup(Sender: TObject);
    procedure DadosdaNF2Click(Sender: TObject);
    procedure EdNFExit(Sender: TObject);
    procedure QrMPInBeforeClose(DataSet: TDataSet);
    procedure NFNormal1Click(Sender: TObject);
    procedure NFe2Click(Sender: TObject);
    procedure ExcluiitemdeClassificao1Click(Sender: TObject);
    procedure Estoque1Click(Sender: TObject);
    procedure Itematual1Click(Sender: TObject);
    procedure porLote1Click(Sender: TObject);
    procedure porLote2Click(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure pelaPesquisa1Click(Sender: TObject);
    procedure Baixadeestoque1Click(Sender: TObject);
    procedure Produtoclassificadoousubprodgerado1Click(Sender: TObject);
    procedure CkPesq_AnimalClick(Sender: TObject);
    procedure RGPesq_AnimalClick(Sender: TObject);
    procedure CkPesq_TipificacaoClick(Sender: TObject);
    procedure RGPesq_TipificacaoClick(Sender: TObject);
    procedure ModeloA1Click(Sender: TObject);
    procedure ModeloB1Click(Sender: TObject);
    procedure frxEntrada0AGetValue(const VarName: string; var Value: Variant);
    procedure RecebimentodeMP1Click(Sender: TObject);
  private
    { Private declarations }
    //FCtrlT, FCtrlF: Integer;
    FFat1, FFat2: Double;
    FTabLctA: String;
    //
    procedure DefineVarDup;
    function DiferencasEmPagtos: Double;
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure ReopenEmissT(FatParcela: Integer);
    procedure ReopenEmissF(FatParcela: Integer);
    procedure ForcaEncerramento(Forca: Boolean);
    function ExcluirEntrada(FatID, FatNum, Empresa: Integer; Avisa: Boolean): Boolean;
  public
    { Public declarations }
    FDefeitos, FControle, FConta: Integer;
    FTabLoc_ntrttMPInRat, FFromMPIn: String;
    procedure ReopenPRi(QualQry: TQualQuery; Controle: Integer);
    procedure Defeitos(SQLType: TSQLType);
    procedure ReopenDefeitos;
    procedure ReopenEmitCus;
    procedure ReopenPagtos;
    procedure ReopenMPInIts(Conta: Integer);
    procedure AtualizaMPIn(Controle: Integer);
    procedure LimpaMPInRat();
    procedure ReopenMPInPerdas(Controle: Integer);
    procedure ReopenStqMovIts(IDCtrl: Integer);
    procedure ReopenNFeCabA();
  end;

var
  FmMPIn: TFmMPIn;

implementation

{$R *.DFM}

uses UnMyObjects, Module, MPInAdd, BlueDermConsts, MPInDefeitos,
  UnInternalConsts3, UnPagtos, UMySQLModule, UnGOTOy, MPInIts1, MPInAssina,
  Principal, MPInRat, MyDBCheck, MPInPerdas, StqClasse, NFeCabA_0000,
  NFeImporta_0400, ModuleNFe_0000, GraImpLista, MPClasMul, UnFinanceiro,
  ModuleGeral, MPClasMulSub, MPRecebImp, UCreate, UnGrade_PF, AppListas;

procedure TFmMPIn.DefineVarDup;
begin
  IC3_ED_FatNum := QrMPInControle.Value;
  IC3_ED_NF := 0;//Geral.IMV(EdNF.Text);
  IC3_ED_Data := QrMPInData.Value;
end;

procedure TFmMPIn.Desativa1Click(Sender: TObject);
begin
  ForcaEncerramento(False);
end;

procedure TFmMPIn.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPIn.BtCancelaClick(Sender: TObject);
begin
  BDC_MPINI := QrMPInControle.Value;
  Close;
end;

procedure TFmMPIn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPIn.ReopenPRi(QualQry: TQualQuery; Controle: Integer);
var
  DataI, DataF, Ordem: String;
  ClienteI, Procedencia, NF, tpNF, SubFornece: Integer;
begin
  case RGOrdem.ItemIndex of
    0: Ordem := 'ORDER BY Data DESC, Ficha DESC, Controle DESC';
    1: Ordem := 'ORDER BY NOMECLIENTEI, Data, Ficha, Controle';
    2: Ordem := 'ORDER BY Lote, Data, Ficha, Controle';
  end;
  DataI := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  DataF := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  //
  NF   := EdNF.ValueVariant;
  tpNF := RGtpNF.ItemIndex;
  SubFornece := EdSubFornece.ValueVariant;
  //
  QrMPIn.Close;
  QrMPIn.SQL.Clear;
  QrMPIn.SQL.Add('SELECT DISTINCT em.CMPUnida, em.CMP_IDQV, em.CMP_LPQV, em.Contatos, ');
  QrMPIn.SQL.Add('IF(ci.Tipo=0, ci.RazaoSocial, ci.Nome) NOMECLIENTEI,');
  QrMPIn.SQL.Add('IF(fo.Tipo=0, fo.ECidade, fo.PCidade) NOMECIDADEF,');
  QrMPIn.SQL.Add('IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMECORRETOR,');
  QrMPIn.SQL.Add('IF(qa.Tipo=0, qa.RazaoSocial, qa.Nome) NOMEASSINA,');
  QrMPIn.SQL.Add('IF(fo.Tipo=0, ufE.Nome, ufP.Nome) NOMEUFF,');
  QrMPIn.SQL.Add('IF(fo.Tipo=0, fo.ETe1, fo.PTe1) TEL1F,');
  QrMPIn.SQL.Add('IF(fo.Tipo=0, fo.ETe2, fo.PTe2) TEL2F,');
  QrMPIn.SQL.Add('CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial');
  QrMPIn.SQL.Add('ELSE fo.Nome END NOMEPROCEDENCIA, wi.*, ei.CodCliInt');
  QrMPIn.SQL.Add(FFromMPIn);
  QrMPIn.SQL.Add('LEFT JOIN enticliint ei ON ei.CodEnti=wi.ClienteI');
  QrMPIn.SQL.Add('LEFT JOIN entidades ci ON ci.Codigo=wi.ClienteI');
  QrMPIn.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=wi.Procedencia');
  QrMPIn.SQL.Add('LEFT JOIN entidades ve ON ve.Codigo=wi.Corretor');
  QrMPIn.SQL.Add('LEFT JOIN entidades qa ON qa.Codigo=wi.QuemAssina');
  QrMPIn.SQL.Add('LEFT JOIN entimp    em ON em.Codigo=fo.Codigo');
  QrMPIn.SQL.Add('LEFT JOIN ufs ufE ON ufE.Codigo=fo.EUF');
  QrMPIn.SQL.Add('LEFT JOIN ufs ufP ON ufP.Codigo=fo.PUF');
  if (NF > 0) or (tpNF > 0) or (SubFornece <> 0) then
    QrMPIn.SQL.Add('LEFT JOIN mpinits its ON its.Controle=wi.Controle');
  //
  //QrMPIn.SQL.Add('WHERE wi.Data BETWEEN :P0 AND :P1');
  QrMPIn.SQL.Add(dmkPF.SQL_Periodo('WHERE wi.Data ', TPIni.Date, TPFim.Date, True, True));
  ClienteI := Geral.IMV(EdClienteI.Text);
  if ClienteI <> 0 then
    QrMPIn.SQL.Add('AND wi.ClienteI=' + FormatFloat('0', ClienteI));
  Procedencia := Geral.IMV(EdProcedencia.Text);
  if Procedencia <> 0 then
    QrMPIn.SQL.Add('AND wi.Procedencia=' + FormatFloat('0', Procedencia));
  if dmkEdOS.ValueVariant > 0 then
    QrMPIn.SQL.Add('AND wi.Controle='+IntToStr(dmkEdOS.ValueVariant));
  if Trim(EdMarca.Text) <> '' then
    QrMPIn.SQL.Add('AND wi.Marca LIKE "%' + EdMarca.Text + '%"');
  if Trim(EdLote.Text) <> '' then
    QrMPIn.SQL.Add('AND wi.Lote LIKE "%' + EdLote.Text + '%"');
  if RGEncerrado.ItemIndex < 2 then
    QrMPIn.SQL.Add('AND wi.Encerrado=' + FormatFloat('0', RGEncerrado.ItemIndex));
  if (NF > 0) or (tpNF > 0) then
  begin
    if NF > 0 then
      QrMPIn.SQL.Add('AND its.NF =' + FormatFloat('0', NF));
    case tpNF of
      // Nada
      0: ;
      // Que entrou com NF
      1: QrMPIn.SQL.Add('AND its.NF_Inn_Fut = 0');
      // Que entrou sem NF mas foi gerada uma NF de entrada
      2: QrMPIn.SQL.Add('AND (its.NF_Inn_Fut = 1 AND its.NF_Inn_Its > 0)');
      // Que entrou sem NF e ainda n�o foi gerada uma NF de entrada
      3: QrMPIn.SQL.Add('AND (its.NF_Inn_Fut = 1 AND its.NF_Inn_Its = 0)');
    end;
  end;
  if CkPesq_Animal.Checked then
    QrMPIn.SQL.Add('AND wi.Animal=' + FormatFloat('0', RGPesq_Animal.ItemIndex));
  if CkPesq_Tipificacao.Checked then
    QrMPIn.SQL.Add('AND wi.Tipificacao=' + FormatFloat('0', RGPesq_Tipificacao.ItemIndex));
  if SubFornece <> 0 then
    QrMPIn.SQL.Add('AND its.Fornece=' + FormatFloat('0', SubFornece));
  QrMPIn.SQL.Add('');
  QrMPIn.SQL.Add(Ordem);
  //QrMPIn.Params[0].AsString := DataI;
  //QrMPIn.Params[1].AsString := DataF;
  try
    UnDmkDAC_PF.AbreQuery(QrMPIn, Dmod.MyDB);
  except
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrMPIn, '', nil, True, True);
    raise;
  end;
  //
  if Controle <> 0 then
    if not QrMPIn.Locate('Controle', Controle, []) then
       QrMPIn.Locate('Controle', FControle, []);
  // duplicando no RGNotaFiscal     
end;

procedure TFmMPIn.ReopenStqMovIts(IDCtrl: Integer);
begin
  QrSMI_104.Close;
  QrSMI_104.Params[00].AsInteger := QrMPInControle.Value;
  QrSMI_104.Params[01].AsInteger := QrMPInControle.Value;
  UMyMod.AbreQuery(QrSMI_104, Dmod.MyDB, 'TFmMPIn.ReopenStqMovIts()');
  //
  QrSMI_101.Close;
  QrSMI_101.Params[00].AsInteger := QrMPInControle.Value;
  QrSMI_101.Params[01].AsInteger := QrMPInControle.Value;
  UMyMod.AbreQuery(QrSMI_101, Dmod.MyDB, 'TFmMPIn.ReopenStqMovIts()');
  //
end;

procedure TFmMPIn.TPIniChange(Sender: TObject);
begin
  ReopenPri(qqMPIn, FControle);
end;

procedure TFmMPIn.TPFimChange(Sender: TObject);
begin
  ReopenPri(qqMPIn, FControle);
end;

procedure TFmMPIn.FormCreate(Sender: TObject);
var
  Data: Integer;
  CliInt: Integer;
begin
  if not DModG.SoUmaEmpresaLogada(True) then
  begin
    Halt(0);
    Exit;
  end else
    Geral.MB_Aviso('ATEN��O: Empresa logada: ' +
    Geral.FF0(DmodG.QrFiliLogFilial.Value) + sLineBreak +
    'Os lan�amentos financeiros ser�o gerados / alterados nas carteiras desta empresa!');
  //
  FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, DmodG.QrFiliLogFilial.Value);
  //////////////////////////////////////////////////////////////////////////////
  ///
  ///
  ImgTipo.SQLType := stLok;
  //
  FFromMPIn := 'FROM mpin wi';
  PageControl1.ActivePageIndex := 0;
  Data := Geral.ReadAppKey('DataMPIn', Application.Title, ktInteger,
    Int(Date), HKEY_LOCAL_MACHINE);
  TPIni.Date := Data;
  TPFim.Date := Date+30;
  UnDmkDAC_PF.AbreQuery(QrClientesI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcedencias, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSubForneced, Dmod.MyDB);
  ReopenPri(qqMPIn, 0);
  UnAppListas.ConfiguraTipificacao(-4, RGTipifNome, 3, 0);
  UnAppListas.ConfiguraTipificacao(-5, RGAnimal,    3, 0);
  UnAppListas.ConfiguraTipificacao(-3, RGPesq_Tipificacao, 6, 0);
  UnAppListas.ConfiguraTipificacao(-5, RGPesq_Animal, 5, 0);
end;

procedure TFmMPIn.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInsere, BtInclui);
end;

procedure TFmMPIn.BtRecalculaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRecalcula, BtRecalcula);
end;

procedure TFmMPIn.Baixadeestoque1Click(Sender: TObject);
begin
  Grade_PF.AlteraSMIA(QrSMI_104);
  ReopenStqMovIts(0);
end;

procedure TFmMPIn.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEdita, BtAltera);
end;

procedure TFmMPIn.BtExcluiClick(Sender: TObject);
begin
  //PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmMPIn.RGOrdemClick(Sender: TObject);
begin
  ReopenPRi(qqMPIn, FControle);
end;

procedure TFmMPIn.RGPesq_AnimalClick(Sender: TObject);
begin
  ReopenPRi(qqMPIn, FControle);
end;

procedure TFmMPIn.RGPesq_TipificacaoClick(Sender: TObject);
begin
  ReopenPRi(qqMPIn, FControle);
end;

procedure TFmMPIn.EdClienteIChange(Sender: TObject);
begin
  ReopenPRi(qqMPIn, FControle);
end;

procedure TFmMPIn.EdLoteChange(Sender: TObject);
begin
  ReopenPRi(qqMPIn, FControle);
end;

procedure TFmMPIn.EdNFExit(Sender: TObject);
begin
  ReopenPRi(qqMPIn, FControle);
end;

procedure TFmMPIn.CkPesq_AnimalClick(Sender: TObject);
begin
  ReopenPRi(qqMPIn, FControle);
end;

procedure TFmMPIn.CkPesq_TipificacaoClick(Sender: TObject);
begin
  ReopenPRi(qqMPIn, FControle);
end;

procedure TFmMPIn.CustodePQtodoslotesmostradosnagrade1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    FControle := QrMPInControle.Value;
    QrMPIn.DisableControls;
    QrMPIn.First;
    PB1.Position := 0;
    PB1.Max := QrMPIn.RecordCount;
    while not QrMPIn.Eof do
    begin
      LaAviso1.Caption := 'Atualizando itens...';
      PB1.Position := PB1.Position + 1;
      Update;
      Application.ProcessMessages;
      //
      Dmod.AtualizaMPIn(QrMPInControle.Value);
      QrMPIn.Next;
    end;
    ReopenPRi(qqMPIn, FControle);
    LaAviso1.Caption := 'Os itens selecionados foram atualizados com sucesso!';
  finally
    QrMPIn.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMPIn.QrMPInAfterScroll(DataSet: TDataSet);
begin
  ReopenMPInIts(0);
  ReopenDefeitos;
  ReopenEmitCus;
  ReopenPagtos;
  ReopenMPInPerdas(0);
  ReopenStqMovIts(0);
end;

procedure TFmMPIn.QrMPInBeforeClose(DataSet: TDataSet);
begin
  QrDefLoc.Close;
  QrDefTip.Close;
  QrDefeitos.Close;
  QrEmitCus.Close;
  QrEmissF.Close;
  QrEmissT.Close;
  QrMPInIts.Close;
  QrMPInPerdas.Close;
  QrSMI_101.Close;
  QrSMI_104.Close;
end;

procedure TFmMPIn.ReopenEmitCus;
begin
  QrEmitCus.Close;
  QrEmitCus.Params[00].AsInteger := QrMPInControle.Value;
  UnDmkDAC_PF.AbreQuery(QrEmitCus, Dmod.MyDB);
end;

procedure TFmMPIn.RecebimentodeMP1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPRecebImp, FmMPRecebImp, afmoNegarComAviso) then
  begin
    FmMPRecebImp.ShowModal;
    FmMPRecebImp.Destroy;
  end;
end;

procedure TFmMPIn.ReopenDefeitos;
begin
  QrDefTot.Close;
  QrDefTot.Params[0].AsInteger := QrMPInControle.Value;
  UnDmkDAC_PF.AbreQuery(QrDefTot, Dmod.MyDB);
  //
  QrDefLoc.Close;
  QrDefLoc.Params[0].AsInteger := QrMPInControle.Value;
  UnDmkDAC_PF.AbreQuery(QrDefLoc, Dmod.MyDB);
  //
  QrDefTip.Close;
  QrDefTip.Params[0].AsInteger := QrMPInControle.Value;
  UnDmkDAC_PF.AbreQuery(QrDefTip, Dmod.MyDB);
  //
  QrDefeitos.Close;
  QrDefeitos.Params[0].AsInteger := QrMPInControle.Value;
  UnDmkDAC_PF.AbreQuery(QrDefeitos, Dmod.MyDB);
  if FDefeitos <> 0 then QrDefeitos.Locate('Controle', FDefeitos, []);
end;

procedure TFmMPIn.Defeitos(SQLType: TSQLType);
begin
  Application.CreateForm(TFmMPInDefeitos, FmMPInDefeitos);
  FmMPInDefeitos.ImgTipo.SQLType := SQLType;
  if SQLType = stUpd then
  begin
    FmMPInDefeitos.EdDef.Text := IntToStr(QrDefeitosDefeitos.Value);
    FmMPInDefeitos.EdTip.Text := IntToStr(QrDefeitosDefeitotip.Value);
    FmMPInDefeitos.CBTip.KeyValue := QrDefeitosDefeitotip.Value;
    FmMPInDefeitos.EdLoc.Text := IntToStr(QrDefeitosDefeitoloc.Value);
    FmMPInDefeitos.CBLoc.KeyValue := QrDefeitosDefeitoloc.Value;
  end;
  FmMPInDefeitos.ShowModal;
  FmMPInDefeitos.Destroy;
end;

procedure TFmMPIn.N1Click(Sender: TObject);
begin
  Defeitos(stIns);
end;

procedure TFmMPIn.NF1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeCabA_0000, FmNFeCabA_0000, afmoNegarComAviso) then
  begin
    FmNFeCabA_0000.FForm := 'FmMPIn';
    PageControl1.ActivePageIndex := 0;
    PageControl2.ActivePageIndex := 5;
    if QrMPInClienteI.Value = 0 then
      Geral.MB_Aviso(
        'Para inclus�o de NF-e � necess�rio informar uma empresa!')
    else begin
      UMyMod.ConfigPanelInsUpd(stIns, FmNFeCabA_0000, FmNFeCabA_0000.PainelEdit,
        FmNFeCabA_0000.QrNFeCabA, [FmNFeCabA_0000.PainelDados], [FmNFeCabA_0000.PainelEdita],
        nil, FmNFeCabA_0000.ImgTipo, 'nfecaba');
      FmNFeCabA_0000.EdFatID.ValueVariant   := VAR_FATID_0113;
      FmNFeCabA_0000.EdFatNum.ValueVariant  := QrMPInItsConta.Value;
      FmNFeCabA_0000.EdEmpresa.ValueVariant := QrMPInClienteI.Value;
      FmNFeCabA_0000.EdCodInfoDest.ValueVariant    := QrMPInClienteI.Value;
      FmNFeCabA_0000.CBCodInfoDest.KeyValue        := QrMPInClienteI.Value;
      FmNFeCabA_0000.EdCodInfoEmit.ValueVariant    := QrMPInProcedencia.Value;
      FmNFeCabA_0000.CBCodInfoEmit.KeyValue        := QrMPInProcedencia.Value;
      FmNFeCabA_0000.Edide_verProc.Text     := '';
      FmNFeCabA_0000.Edide_natOp.Text       := '';
      FmNFeCabA_0000.Edide_mod.Text         := '1';
      FmNFeCabA_0000.Edide_tpNF.Text        := '1';
      FmNFeCabA_0000.TPide_dEmi.Date        := Date;
      FmNFeCabA_0000.TPide_dSaiEnt.Date     := Date;
      FmNFeCabA_0000.TPDataFiscal.Date      := Date;
      FmNFeCabA_0000.Edversao.ValueVariant  := 0;
      //
      FmNFeCabA_0000.Edide_CUF.ValueVariant          := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(FmNFeCabA_0000.Edemit_UF.Text);
      FmNFeCabA_0000.EdICMSRec_pAliq.ValueVariant    := 7;
      FmNFeCabA_0000.EdIPIRec_pAliq.ValueVariant     := 0;
      FmNFeCabA_0000.EdPISRec_pAliq.ValueVariant     := 1.65;
      FmNFeCabA_0000.EdCOFINSRec_pAliq.ValueVariant  := 7.6;
      FmNFeCabA_0000.EdICMSRec_pRedBC.ValueVariant   := 40;
      FmNFeCabA_0000.EdIPIRec_pRedBC.ValueVariant    := 100;
      FmNFeCabA_0000.EdPISRec_pRedBC.ValueVariant    := 40;
      FmNFeCabA_0000.EdCOFINSRec_pRedBC.ValueVariant := 40;
    end;
    FmNFeCabA_0000.ShowModal;
    FmNFeCabA_0000.Destroy;
    //
    ReopenNFeCabA();
  end;
end;

procedure TFmMPIn.NFe2Click(Sender: TObject);
var
  cab_qVol, FatID, FatNum, Empresa, IDCtrl, CliFor, Transporta: Integer;
  cab_PesoB, cab_PesoL, SumPesoLIts: Double;
  cab_Data, cab_DataE, cab_Cancelado, cab_refNFe: String;
  //
  Controle, Periodo, Conta: Integer;
  PC, kg, qVol, qCom: Double;
  DataFiscal: TDateTime;
begin
  FatID := VAR_FATID_0013;
  FatNum := UMyMod.BuscaEmLivreY_Def('mpinits', 'Conta', stIns, 0);
  //
  cab_PesoB := 0;
  cab_PesoL := 0;
  if UnNFeImporta_0400.ImportaDadosNFeDeXML(tpemiTerceiros, tpnfEntrada,
  MeAvisos, MeAvisos, MeAvisos, '', FmMPIn, FatID, FatNum, Empresa,
  IDCtrl, CliFor, Transporta, cab_qVol, cab_PesoB, cab_PesoL, SumPesoLIts,
  DataFiscal, FTabLcta) then
  begin
    cab_Data          := FormatDateTime('yyyy-mm-dd', Date); // Entrada
    //cab_TipoNF        := 1; // Nota Fiscal Eletr�nica
    //
    //cab_Pedido        := 0;
    //cab_Juros         := 0;
    //cab_RICMS         := 0;
    //cab_RICMSF        := 0;
    //cab_Conhecimento  := 0;
    cab_Cancelado     := 'V';
    //
    DmNFe_0000.ReopenQrA(FatID, FatNum, Empresa);
    //
    //cab_NF      := DmNFe_0000.QrAide_nNF.Value;
    cab_dataE   := Geral.FDT(DmNFe_0000.QrAide_dEmi.Value, 1);
    cab_refNFe  := DmNFe_0000.QrAId.Value;
    //cab_modNF   := DmNFe_0000.QrAide_mod.Value;
    //cab_Serie   := DmNFe_0000.QrAide_serie.Value;
    //cab_ICMS    := DmNFe_0000.QrAICMSTot_vICMS.Value;
    //cab_ValProd := DmNFe_0000.QrAICMSTot_vProd.Value;
    //cab_Frete   := DmNFe_0000.QrAICMSTot_vFrete.Value;
    //cab_Seguro  := DmNFe_0000.QrAICMSTot_vSeg.Value;
    //cab_Desconto:= DmNFe_0000.QrAICMSTot_vDesc.Value;
    //cab_IPI     := DmNFe_0000.QrAICMSTot_vIPI.Value;
    //cab_PIS     := DmNFe_0000.QrAICMSTot_vPIS.Value;
    //cab_COFINS  := DmNFe_0000.QrAICMSTot_vCOFINS.Value;
    //cab_Outros  := DmNFe_0000.QrAICMSTot_vOutro.Value;
    //cab_ValorNF := DmNFe_0000.QrAICMSTot_vNF.Value;

    Controle := UMyMod.BuscaEmLivreY_Def('mpin', 'Controle', stIns, 0);
    Application.CreateForm(TFmMPInAdd, FmMPInAdd);
    FmMPInAdd.FPeriodo                      := 0;
    FmMPInAdd.FControle                     := Controle;

    FmMPInAdd.ImgTipo.SQLType                := stIns;
    FmMPInAdd.TPData.Date                   := DataFiscal;
    FmMPInAdd.EdClienteI.ValueVariant       := Empresa;
    FmMPInAdd.CBClienteI.KeyValue           := Empresa;
    // Deve ser antes do SeboPreDescarne
    FmMPInAdd.EdProcedencia.ValueVariant    := CliFor;
    FmMPInAdd.CBProcedencia.KeyValue        := CliFor;
    //
    if Transporta <> 0 then
    begin
      FmMPInAdd.EdTransporte.ValueVariant   := Transporta;
      FmMPInAdd.CBTransporte.KeyValue       := Transporta;
    end;
    {
    Autom�tico?
    FmMPInAdd.EdMarca.Text                  := QrMPInMarca.Value;
    FmMPInAdd.EdLote.Text                   := QrMPInLote.Value;
    }
    { Manual?
    FmMPInAdd.EdCorretor.Text               := IntToStr(QrMPInCorretor.Value);
    FmMPInAdd.CBCorretor.KeyValue           := QrMPInCorretor.Value;
    FmMPInAdd.EdFicha.Text                  := Geral.FFT(QrMPInFicha.Value, 0, siPositivo);
    FmMPInAdd.RGTipificacao.ItemIndex       := QrMPInTipificacao.Value;
    FmMPInAdd.RGAbateTipo.ItemIndex         := QrMPInAbateTipo.Value;
    FmMPInAdd.RGAparasCabelo.ItemIndex      := QrMPInAparasCabelo.Value;
    FmMPInAdd.RGSeboPreDescarne.ItemIndex   := QrMPInSeboPreDescarne.Value;
    //
    FmMPInAdd.dmkEdComisPer.ValueVariant    := QrMPInComisPer.Value;
    FmMPInAdd.dmkEdDescAdiant.ValueVariant  := QrMPInDescAdiant.Value;
    FmMPInAdd.EdLocalEntrg.Text             := QrMPInLocalEntrg.Value;
    FmMPInAdd.MeObserv.Text                 := QrMPInObserv.Value;
    FmMPInAdd.EdCondPagto.Text              := QrMPInCondPagto.Value;
    FmMPInAdd.EdCondComis.Text              := QrMPInCondComis.Value;
    //
    FmMPInAdd.RGAnimal.ItemIndex            := QrMPInAnimal.Value;
    //
    FmMPInAdd.dmkEdRecorte_PDA.ValueVariant := QrMPInRecorte_PDA.Value;
    FmMPInAdd.dmkEdRecorte_PTA.ValueVariant := QrMPInRecorte_PTA.Value;
    FmMPInAdd.dmkEdRaspa_PTA.ValueVariant   := QrMPInRaspa_PTA.Value;
    FmMPInAdd.dmkEdPTA.ValueVariant         := QrMPInPTA.Value;
    FmMPInAdd.dmkEdFimM2.ValueVariant       := QrMPInFimM2.Value;
    FmMPInAdd.dmkEdFimP2.ValueVariant       := QrMPInFimP2.Value;
    }
    FmMPInAdd.ShowModal;
    FControle := FmMPInAdd.FControle;
    Periodo   := FmMPInAdd.FPeriodo;
    FmMPInAdd.Destroy;
    ReopenPri(qqMPIn, FControle);
    //
    if FControle = 0 then
    begin
      ExcluirEntrada(FatID, FatNum, Empresa, True);
    end else begin
      // ITENS
      DmNFe_0000.ReopenQrI(FatID, FatNum, Empresa);
      //
      DmNFe_0000.QrI.First;
      while not DmNFe_0000.QrI.Eof do
      begin
        if DmNFe_0000.QrI.RecNo > 1 then
          FatNum := UMyMod.BuscaEmLivreY_Def('mpinits', 'Conta', stIns, 0);
        Conta := FatNum;
        DmNFe_0000.ReopenQrN_QrS(FatID, FatNum, Empresa, DmNFe_0000.QrInItem.Value);
        //
        qCom := DmNFe_0000.QrIprod_qCom.Value;
        DmNFe_0000.QrXVol.Close;
        DmNFe_0000.QrXVol.SQL.Clear;
        DmNFe_0000.QrXVol.SQL.Add('SELECT *');
        DmNFe_0000.QrXVol.SQL.Add('FROM nfecabxvol');
        DmNFe_0000.QrXVol.SQL.Add('WHERE FatID=:P0');
        DmNFe_0000.QrXVol.SQL.Add('AND FatNum=:P1');
        DmNFe_0000.QrXVol.SQL.Add('AND Empresa=:P2');
        DmNFe_0000.QrXVol.SQL.Add('AND pesoL=:P3');
        DmNFe_0000.QrXVol.Params[00].AsInteger := FatID;
        DmNFe_0000.QrXVol.Params[01].AsInteger := FatNum;
        DmNFe_0000.QrXVol.Params[02].AsInteger := Empresa;
        DmNFe_0000.QrXVol.Params[03].AsFloat   := qCom;
        UnDmkDAC_PF.AbreQuery(DmNFe_0000.QrXVol, Dmod.MyDB);
        qVol := DmNFe_0000.QrXVolqVol.Value;
        if qVol > 0 then
        begin
          if qVol > qCom then
          begin
            kg := qVol;
            PC := qCom;
          end else begin
            kg := qCom;
            PC := qVol;
          end;
        end else begin
          kg := qVol;
          PC := qCom;
        end;
        {
        Geral.MB_Aviso('prod_qCom = ' + FloatToStr(DmNFe_0000.QrIprod_qCom.Value) +
        sLineBreak + 'qVol = ' + FloatToStr(DmNFe_0000.QrXVolqVol.Value);
        }

        LimpaMPInRat();
        Application.CreateForm(TFmMPInIts1, FmMPInIts1);
        FmMPInIts1.FConta                      := Conta;
        FmMPInIts1.FCodigo                     := Periodo;
        FmMPInIts1.FControle                   := FControle;
        FmMPInIts1.ImgTipo.SQLType              := stIns;
        FmMPInIts1.FEntradaPorXML              := True;
        //
        FmMPInIts1.EdFornece.ValueVariant      := CliFor;
        FmMPInIts1.CBFornece.KeyValue          := CliFor;
        FmMPInIts1.EdCaminhao.ValueVariant     := 1;

        FmMPInIts1.EdPecasNF.ValueVariant      := PC;
        FmMPInIts1.EdPNF.ValueVariant          := kg;
        //FmMPInIts1.EdPNF_Fat.ValueVariant      := _ITS_PNF_Fat.Value;
        FmMPInIts1.EdPecas.ValueVariant        := PC;
        FmMPInIts1.EdPLE.ValueVariant          := kg;
        FmMPInIts1.EdPDA.ValueVariant          := 0;
        FmMPInIts1.EdCMPValor.ValueVariant     := DmNFe_0000.QrIprod_vProd.Value;
        FmMPInIts1.EdCMPFrete.ValueVariant     := 0;
        //
        FmMPInIts1.EdNF_Modelo.Text            := IntToStr(DmNFe_0000.QrAide_mod.Value);
        FmMPInIts1.EdNF_Serie.Text             := IntToStr(DmNFe_0000.QrAide_serie.Value);
        FmMPInIts1.EdNF.ValueVariant           := DmNFe_0000.QrAide_nNF.Value;
        FmMPInIts1.EdConheci.ValueVariant      := 0;
        FmMPInIts1.TPEmissao.Date              := DmNFe_0000.QrAide_dEmi.Value;
        FmMPInIts1.TPVencto.Date               := DmNFe_0000.QrAide_dEmi.Value;
        FmMPInIts1.TPEmisFrete.Date            := DmNFe_0000.QrAide_dEmi.Value;
        FmMPInIts1.TPVctoFrete.Date            := DmNFe_0000.QrAide_dEmi.Value;
        //
        { ??? Presta��o de servi�o ????
        FmMPInIts1.EdPS_QtdTot.ValueVariant    := 0;
        FmMPInIts1.EdPS_ValTot.ValueVariant    := 0;
        FmMPInIts1.EdPS_ValUni.ValueVariant    := 0;
        }
        //
        FmMPInIts1.CkNF_Inn_Fut.Checked        := False;
        //
        FmMPInIts1.ShowModal;
        FmMPInIts1.Destroy;
        //
        DmNFe_0000.QrI.Next;
      end;
    end;
  end;
  ReopenPRi(qqMPIn, 0);
end;

procedure TFmMPIn.NFNormal1Click(Sender: TObject);
begin
  Application.CreateForm(TFmMPInAdd, FmMPInAdd);
  FmMPInAdd.FPeriodo            := 0;//QrWBResultCodigo.Value;
  FmMPInAdd.ImgTipo.SQLType      := stIns;
  FmMPInAdd.EdClienteI.Text     := EdClienteI.Text;
  FmMPInAdd.CBClienteI.KeyValue := CBClienteI.KeyValue;
  //
  FmMPInAdd.ShowModal;
  FControle := FmMPInAdd.FControle;
  FmMPInAdd.Destroy;
  ReopenPri(qqMPIn, FControle);
  //
  if FControle > 0 then Itensdematriaprima1Click(Sender);
end;

procedure TFmMPIn.DadosdaNF2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeCabA_0000, FmNFeCabA_0000, afmoNegarComAviso) then
  begin
    FmNFeCabA_0000.FForm := 'FmMPIn';
    PageControl1.ActivePageIndex := 0;
    PageControl2.ActivePageIndex := 0;
    if FmNFeCabA_0000.LocCod(QrNFeCabAIDCtrl.Value, QrNFeCabAIDCtrl.Value) then
    begin
      if QrMPInClienteI.Value = 0 then
        Geral.MB_Aviso(
          'Para inclus�o de NF-e � necess�rio informar uma empresa!')
      else begin
        UMyMod.ConfigPanelInsUpd(stUpd, FmNFeCabA_0000, FmNFeCabA_0000.PainelEdit,
          FmNFeCabA_0000.QrNFeCabA, [FmNFeCabA_0000.PainelDados], [FmNFeCabA_0000.PainelEdita],
          nil, FmNFeCabA_0000.ImgTipo, 'nfecaba');
      end;
      FmNFeCabA_0000.ShowModal;
      FmNFeCabA_0000.Destroy;
    end else begin
      Geral.MB_Aviso('N�o foi poss�vel localizar a NF!');
      FmNFeCabA_0000.Destroy;
    end;
    //
    ReopenNFeCabA();
  end;
end;

procedure TFmMPIn.DBGrid2DblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := Uppercase(DBGrid2.Columns[THackDBGrid(DBGrid2).Col -1].FieldName);
  if Campo = 'LOTE' then
  begin
    EdLote.Text := QrMPInLote.Value;
    EdClienteIChange(Self);
  end;
end;

procedure TFmMPIn.Defeito1Click(Sender: TObject);
begin
  Defeitos(stUpd);
end;

procedure TFmMPIn.MenuItem4Click(Sender: TObject);
begin
  Application.CreateForm(TFmMPInAdd, FmMPInAdd);
  FmMPInAdd.FPeriodo                      := QrMPInCodigo.Value;
  FmMPInAdd.FControle                     := QrMPInControle.Value;
  FmMPInAdd.ImgTipo.SQLType                := stUpd;
  FmMPInAdd.TPData.Date                   := QrMPInData.Value;
  FmMPInAdd.EdMarca.Text                  := QrMPInMarca.Value;
  FmMPInAdd.EdClienteI.Text               := IntToStr(QrMPInClienteI.Value);
  FmMPInAdd.CBClienteI.KeyValue           := QrMPInClienteI.Value;
  FmMPInAdd.EdTransporte.Text             := IntToStr(QrMPInTransportadora.Value);
  FmMPInAdd.CBTransporte.KeyValue         := QrMPInTransportadora.Value;
  FmMPInAdd.EdCorretor.Text               := IntToStr(QrMPInCorretor.Value);
  FmMPInAdd.CBCorretor.KeyValue           := QrMPInCorretor.Value;
  // Deve ser antes do SeboPreDescarne
  FmMPInAdd.EdProcedencia.Text            := IntToStr(QrMPInProcedencia.Value);
  FmMPInAdd.CBProcedencia.KeyValue        := QrMPInProcedencia.Value;
  //
  FmMPInAdd.EdFicha.Text                  := Geral.FFT(QrMPInFicha.Value, 0, siPositivo);
  FmMPInAdd.RGTipificacao.ItemIndex       := QrMPInTipificacao.Value;
  FmMPInAdd.RGAbateTipo.ItemIndex         := QrMPInAbateTipo.Value;
  FmMPInAdd.RGAparasCabelo.ItemIndex      := QrMPInAparasCabelo.Value;
  FmMPInAdd.RGSeboPreDescarne.ItemIndex   := QrMPInSeboPreDescarne.Value;
  FmMPInAdd.EdLote.Text                   := QrMPInLote.Value;
  //
  FmMPInAdd.dmkEdComisPer.ValueVariant    := QrMPInComisPer.Value;
  FmMPInAdd.dmkEdDescAdiant.ValueVariant  := QrMPInDescAdiant.Value;
  FmMPInAdd.EdLocalEntrg.Text             := QrMPInLocalEntrg.Value;
  FmMPInAdd.MeObserv.Text                 := QrMPInObserv.Value;
  FmMPInAdd.EdCondPagto.Text              := QrMPInCondPagto.Value;
  FmMPInAdd.EdCondComis.Text              := QrMPInCondComis.Value;
  //
  FmMPInAdd.RGAnimal.ItemIndex            := QrMPInAnimal.Value;
  //
  FmMPInAdd.dmkEdRecorte_PDA.ValueVariant := QrMPInRecorte_PDA.Value;
  FmMPInAdd.dmkEdRecorte_PTA.ValueVariant := QrMPInRecorte_PTA.Value;
  FmMPInAdd.dmkEdRaspa_PTA.ValueVariant   := QrMPInRaspa_PTA.Value;
  FmMPInAdd.dmkEdPTA.ValueVariant         := QrMPInPTA.Value;
  FmMPInAdd.dmkEdFimM2.ValueVariant       := QrMPInFimM2.Value;
  FmMPInAdd.dmkEdFimP2.ValueVariant       := QrMPInFimP2.Value;

  FmMPInAdd.ShowModal;
  FControle := FmMPInAdd.FControle;
  FmMPInAdd.Destroy;
  ReopenPri(qqMPIn, FControle);
end;

procedure TFmMPIn.Duplicatatranportadora1Click(Sender: TObject);
var
  CliFor, Cod, Genero: Integer;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  if FmPrincipal.AvisaFaltaDeContaDoPlano(VAR_FATID_1004) then Exit;
  DiferencasEmPagtos;
  DefineVarDup;
  Genero    := Dmod.QrControleCtaMPFrete.Value;
  Cod       := QrMPInControle.Value;
  CliFor    := QrMPInTransportadora.Value;
  UPagtos.Pagto(QrEmissT, tpDeb, Cod, CliFor, VAR_FATID_1004, Genero, 0(*GenCtb*), stIns,
  'Tranporte de Mat�ria-prima', FFat2, VAR_USUARIO, 0, 0, mmNenhum, 0, 0,
  True, False, 0, 0, 0, 0, 0, FTabLctA);
  AtualizaMPIn(QrMPInControle.Value);
end;

procedure TFmMPIn.Duplicatafornecedor1Click(Sender: TObject);
var
  CliFor, Cod, Genero: Integer;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  if FmPrincipal.AvisaFaltaDeContaDoPlano(VAR_FATID_1003) then Exit;
  DiferencasEmPagtos;
  DefineVarDup;
  Genero    := Dmod.QrControleCtaMPCompr.Value;
  Cod       := QrMPInControle.Value;
  CliFor    := QrMPInProcedencia.Value;
  UPagtos.Pagto(QrEmissF, tpDeb, Cod, CliFor, VAR_FATID_1003, Genero, 0(*GenCtb*), stIns,
  'Compra de Mat�ria-prima', FFat1, VAR_USUARIO, 0, 0, mmNenhum, 0, 0,
  True, False, 0, 0, 0, 0, 0, FTabLctA);
  AtualizaMPIn(QrMPInControle.Value);
end;

procedure TFmMPIn.MenuItem5Click(Sender: TObject);
var
  CliInt, CliFor, Cod, Genero: Integer;
  Valor: Double;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  IC3_ED_Controle := QrEmissTControle.Value;
  DefineVarDup;
  IC3_ED_NF := QrEmissTNotaFiscal.Value;
  Genero    := Dmod.QrControleCtaMPFrete.Value;
  Cod       := QrMPInControle.Value;
  CliFor    := QrMPInTransportadora.Value;
  CliInt    := QrEmissTCliInt.Value;
  Valor     := QrEmissTDebito.Value;
  UPagtos.Pagto(QrEmissT, tpDeb, Cod, CliFor, VAR_FATID_1004, Genero, 0(*GenCtb*), stUpd,
  'Tranporte de Mat�ria-prima', Valor, VAR_USUARIO, 0, CliInt, mmNenhum, 0, 0,
  True, False, 0, 0, 0, 0, 0, FTabLctA);
  AtualizaMPIn(QrMPInControle.Value);
end;

procedure TFmMPIn.MenuItem6Click(Sender: TObject);
var
  CliInt, CliFor, Cod, Genero: Integer;
  Valor: Double;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  IC3_ED_Controle := QrEmissFControle.Value;
  DefineVarDup;
  IC3_ED_NF := QrEmissFNotaFiscal.Value;
  Genero    := Dmod.QrControleCtaMPCompr.Value;
  Cod       := QrMPInControle.Value;
  CliFor    := QrMPInProcedencia.Value;
  CliInt    := QrEmissFCliInt.Value;
  Valor     := QrEmissFDebito.Value;
  UPagtos.Pagto(QrEmissF, tpDeb, Cod, CliFor, VAR_FATID_1003, Genero, 0(*GenCtb*), stUpd,
  'Compra de Mat�ria-prima', Valor, VAR_USUARIO, 0, CliInt, mmNenhum, 0, 0,
  True, False, 0, 0, 0, 0, 0, FTabLctA);
  AtualizaMPIn(QrMPInControle.Value);
end;

function TFmMPIn.DiferencasEmPagtos: Double;
begin
  Result := 0;
  //LocCod(QrMPECodigo.Value, QrMPECodigo.Value);

  //DistribuiCustoDoFrete;

  FFat1 := 0;//(QrMPECONFTNFS.Value-QrMPEFrete.Value) - QrMPECONFDUPLIF.Value;
  FFat2 := 0;//QrMPEFrete.Value - QrMPECONFDUPLIT.Value;
  if FFat1 < 0 then Result := Result-FFat1 else Result := Result+FFat1;
  if FFat2 < 0 then Result := Result-FFat2 else Result := Result+FFat2;
end;

procedure TFmMPIn.ReopenPagtos;
begin
  ReopenEmissT(QrEmissTFatParcela.Value);
  ReopenEmissF(QrEmissFFatParcela.Value);
end;

procedure TFmMPIn.Excluipagamentotransporte1Click(Sender: TObject);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  if UFinanceiro.ExcluiLct_FatParcela(QrEmissT, QrEmissTFatID.Value,
    QrEmissTFatNum.Value, QrEmissTFatParcela.Value, QrEmissTCarteira.Value,
    QrEmissTSit.Value, QrEmissTTipo.Value, dmkPF.MotivDel_ValidaCodigo(317),
    FTabLctA) then
  begin
    ReopenEmissT(QrEmissTFatParcela.Value);
  end;
  AtualizaMPIn(QrMPInControle.Value);
end;

procedure TFmMPIn.ExcluiPerdadecouro1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da perda de couro selecionada?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mpinperdas WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrMPInPerdasControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.AtualizaMPIn(QrMPInControle.Value);
    //
    ReopenPRi(qqMPIn, QrMPInControle.Value);
    ReopenMPInPerdas(0);
  end;
end;

function TFmMPIn.ExcluirEntrada(FatID, FatNum, Empresa: Integer;
  Avisa: Boolean): Boolean;
begin
  if Avisa then
  Geral.MB_Erro('Ocorreu um erro na importa��o do XML.' + sLineBreak +
  'Os dados da entrada ser�o exclu�dos, pois s�o parciais!');
  DmNFe_0000.ExcluiNFe(0(*Status*), FatID, FatNum, Empresa, True, True);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM mpin WHERE Controle=:P0');
  Dmod.QrUpd.Params[0].AsInteger := FatNum;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM mpinits WHERE Controle=:P0');
  Dmod.QrUpd.Params[0].AsInteger := FatNum;
  Dmod.QrUpd.ExecSQL;
  //
  Result := True;
  //
  QrMPIn.Close;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmMPIn.Excluipagamentomatriaprima1Click(Sender: TObject);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  if UFinanceiro.ExcluiLct_FatParcela(QrEmissF, QrEmissFFatID.Value,
    QrEmissFFatNum.Value, QrEmissFFatParcela.Value, QrEmissFCarteira.Value,
    QrEmissFSit.Value, QrEmissFTipo.Value, dmkPF.MotivDel_ValidaCodigo(316),
    FTabLctA) then
  begin
    ReopenEmissF(QrEmissFFatParcela.Value);
  end;
  AtualizaMPIn(QrMPInControle.Value);
end;

procedure TFmMPIn.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Data: Integer;
begin
  Data := Trunc(TPIni.Date);
  Geral.WriteAppKey('DataMPIn', Application.Title, Data, ktInteger,
    HKEY_LOCAL_MACHINE);
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMPIn.QrMPInCalcFields(DataSet: TDataSet);
var
  Liga: String;
  //Fator: Double;
begin
  if QrMPInPNF.Value <> 0 then QrMPInQuebraPLE.Value :=
    (QrMPInPLE.Value - QrMPInPNF.Value) / QrMPInPNF.Value * 100
  else QrMPInQuebraPLE.Value := 0;
  //
  if QrMPInPLE.Value <> 0 then QrMPInQuebraPDA.Value :=
    (QrMPInPDA.Value - QrMPInPLE.Value) / QrMPInPLE.Value * 100
  else QrMPInQuebraPDA.Value := 0;
  //
  if QrMPInPNF.Value <> 0 then QrMPInQuebraCAL.Value :=
    (QrMPInPDA.Value - QrMPInPNF.Value) / QrMPInPNF.Value * 100
  else QrMPInQuebraPDA.Value := 0;

  //

  // Quebras em fun��o da forma do pr�-descarna
  QrMPInPLE_0.Value := 0;
  QrMPInPDA_0.Value := 0;
  QrMPInQuebraPDA_0.Value := 0;
  if (QrMPInPLE.Value <> 0) and (QrMPInSeboPreDescarne.Value = 0) then
  begin
    // Pr�-descarnado no curtume
    QrMPInPLE_0.Value := QrMPInPLE.Value;
    QrMPInPDA_0.Value := QrMPInPDA.Value;
    QrMPInQuebraPDA_0.Value := (QrMPInPDA_0.Value - QrMPInPLE_0.Value) / QrMPInPLE_0.Value * 100;
  end;
  //
  QrMPInPLE_1.Value := 0;
  QrMPInPDA_1.Value := 0;
  QrMPInQuebraPDA_1.Value := 0;
  if (QrMPInPLE.Value <> 0) and (QrMPInSeboPreDescarne.Value <> 0) then
  begin
    // N�o pr�-descarnado ou pr�-descarnado fora do curtume
    QrMPInPLE_1.Value := QrMPInPLE.Value;
    QrMPInPDA_1.Value := QrMPInPDA.Value;
    QrMPInQuebraPDA_1.Value := (QrMPInPDA_1.Value - QrMPInPLE_1.Value) / QrMPInPLE_1.Value * 100;
  end;

  //

  QrMPInCMP_TOT_INFO.Value := QrMPInCustoInfo.Value + QrMPInFreteInfo.Value;
  if QrMPInPDA.Value = 0 then QrMPInCMP_PDA_INFO.Value := 0 else
    QrMPInCMP_PDA_INFO.Value := QrMPInCMP_TOT_INFO.Value / QrMPInPDA.Value;
  //
  if QrMPInPNF.Value = 0 then
    QrMPInCPQ_PNF.Value := 0
  else
    QrMPInCPQ_PNF.Value := QrMPInCustoPQ.Value / QrMPInPNF.Value;

  //

  QrMPInCMPUnidaTXT.Value := FmPrincipal.ObtemUnidadeTxt(QrMPInCMPUnida.Value);

  {
  case QrMPInCMPUnida.Value of
    0: Fator := QrMPInPNF.Value;
    1: Fator := QrMPInPecasNF.Value;
    else Fator := 0;
  end;
  }
  //

  QrMPInTipificDESCRI.Value := RGTipifNome.Items[QrMPInTipificacao.Value];
  QrMPInANIMALNOME.Value    := RGAnimal.Items[QrMPInAnimal.Value];
  QrMPInTipificDESCRI.Value := RGTipifNome.Items[QrMPInTipificacao.Value];
  if (Trim(QrMPInANIMALNOME.Value) <> '') and
     (Trim(QrMPInTipificDESCRI.Value) <> '') then Liga := ' de ' else Liga := '';
  QrMPInDESCRI_ANIMAL.Value :=
    QrMPInTipificDESCRI.Value + Liga + QrMPInANIMALNOME.Value;

  if QrMPInPDA.Value <> 0 then QrMPInQuebraPTA.Value :=
    (QrMPInPTA.Value - QrMPInPDA.Value) / QrMPInPDA.Value * 100
  else QrMPInQuebraPTA.Value := 0;

  //

end;

procedure TFmMPIn.BtConfirmaClick(Sender: TObject);
var
  OK: Boolean;
  Codigo, Controle, Pesagem, Formula, MPIn: Integer;
  PesoF, Custo, PercTotCus, Peso: Double;
  DataEmis: String;
begin
  if PnFormulas.Visible then
  begin
    OK := False;
    if QrEmit.State = dsBrowse then if QrEmit.RecordCount > 0 then OK := True;
    if not OK then
    begin
      Application.MessageBox('Informe o n�mero de pesagem!', 'Erro', MB_OK+
      MB_ICONERROR);
      Exit;
    end;
    Pesagem := Geral.IMV(EdPesagem.Text);
    PesoF   := Geral.DMV(EdPesoF.Text);
    if PesoF = QrEmitPeso.Value then
    begin
      Custo := QrEmitCusto.Value;
      PercTotCus := 100;
    end else
    begin
      Custo := QrEmitCusto.Value / QrEmitPeso.Value * PesoF;
      PercTotCus := QrEmitCusto.Value / QrEmitPeso.Value * 100;
    end;
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'EmitCus', 'EmitCus', 'Controle');
(*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO emitcus SET Peso=:P0, Custo=:P1, ');
    Dmod.QrUpd.SQL.Add('Formula=:P2, DataEmis=:P3, MPIn=:P4, ');
    Dmod.QrUpd.SQL.Add('Codigo=:Pa, Controle=:Pb');
    Dmod.QrUpd.Params[00].AsFloat    := PesoF;
    Dmod.QrUpd.Params[01].AsFloat    := Custo;
    Dmod.QrUpd.Params[02].AsInteger  := QrEmitNumero.Value;
    Dmod.QrUpd.Params[03].AsDateTime := QrEmitDataEmis.Value;
    Dmod.QrUpd.Params[04].AsInteger  := QrMPInControle.Value;
    Dmod.QrUpd.Params[05].AsInteger  := Pesagem;
    Dmod.QrUpd.Params[06].AsInteger  := Controle;
    Dmod.QrUpd.ExecSQL;
*)

    Peso     := PesoF;
    Formula  := QrEmitNumero.Value;
    DataEmis := Geral.FDT(QrEmitDataEmis.Value, 1);
    MPIn     := QrMPInControle.Value;
    Codigo   := Pesagem;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'emitcus', False, [
    'Codigo', 'MPIn', 'Formula',
    'DataEmis', 'Peso', 'Custo'(*,
    'Pecas', 'MPVIts', 'AreaM2',
    'AreaP2'*), 'PercTotCus'], [
    'Controle'], [
    Codigo, MPIn, Formula,
    DataEmis, Peso, Custo(*,
    Pecas, MPVIts, AreaM2,
    AreaP2*), PercTotCus], [
    Controle], True);
  end;
  Dmod.AtualizaMPIn(QrMPInControle.Value);
  ReopenPRi(qqMPIn, QrMPInControle.Value);
  MostraEdicao(0, stLok, 0);
end;

procedure TFmMPIn.EdPesagemExit(Sender: TObject);
var
  Pesagem: Integer;
begin
  Pesagem := Geral.IMV(EdPesagem.Text);
  EdPesagem.Text := IntToStr(Pesagem);
  QrEmit.Close;
  if Pesagem > 0 then
  begin
    QrEmit.Params[0].AsInteger := Pesagem;
    UnDmkDAC_PF.AbreQuery(QrEmit, Dmod.MyDB);
  end;
end;

procedure TFmMPIn.EdPesoFExit(Sender: TObject);
begin
  EdPesoF.Text := Geral.TFT(EdPesoF.Text, 3, siPositivo);
end;

procedure TFmMPIn.Pesagem1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmMPIn.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      GBControle.Visible := True;
      GBConfirma.Visible := False;
      PnFormulas.Visible := False;
      ReopenPRi(qqMPIn, QrMPInControle.Value);
    end;
    1:
    begin
      GBConfirma.Visible := True;
      PnFormulas.Visible := True;
      GBControle.Visible := False;
      if SQLType = stIns then
      begin
        EdPesagem.Text     := '';
        EdPesoF.Text       := '';
      end else begin
        // Fazer ?
      end;
      EdPesagem.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
end;

procedure TFmMPIn.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmMPIn.ExcluiDefeito1Click(Sender: TObject);
begin
  if Application.MessageBox(PChar('Confirma a exclus�o do defeito "'+
  QrDefeitosNOMETIP.Value+'" no local "'+QrDefeitosNOMELOC.Value+'"?'),
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)= ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mpindefeitos WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrDefeitosControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenDefeitos;
  end;
end;

procedure TFmMPIn.ExcluiEntrada1Click(Sender: TObject);
var
  FatID, FatNum, Empresa: Integer;
begin
  if QrMPInIts.RecordCount > 0 then
  begin
    Application.MessageBox(PCHar('A entrada atual n�o pode ser exclu�da pois ' +
    'cont�m itens (caminh�es) atrelados a ele!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Application.MessageBox(PChar('Confirma a exclus�o da entrada atual?'),
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)= ID_YES then
  begin
    FatID   := VAR_FATID_0013;
    FatNum  := QrMPInControle.Value;
    Empresa := QrMPInClienteI.Value;
    ExcluirEntrada(FatID, FatNum, Empresa, False);
    //
    FatID   := VAR_FATID_0113;
    FatNum  := QrMPInControle.Value;
    Empresa := QrMPInClienteI.Value;
    ExcluirEntrada(FatID, FatNum, Empresa, False);
    //
    FatID   := VAR_FATID_0213;
    FatNum  := QrMPInControle.Value;
    Empresa := QrMPInClienteI.Value;
    ExcluirEntrada(FatID, FatNum, Empresa, False);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mpin WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := FatNum;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenPRi(qqMPIn, 0);
  end;
end;

procedure TFmMPIn.ExcluiitemdeClassificao1Click(Sender: TObject);
  function ExcluiItem(): Boolean;
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE IDCtrl=:P0');
    Dmod.QrUpd.Params[0].AsInteger := FmMPIn.QrSMI_101IDCtrl.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsb WHERE IDCtrl=:P0');
    Dmod.QrUpd.Params[0].AsInteger := FmMPIn.QrSMI_101IDCtrl.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.AtualizaMPIn(QrMPInControle.Value);
    //
    Result := true;
  end;
var
  q: TSelType;
begin
  if DBCheck.Quais_Selecionou_ExecProc(QrSMI_101, DBGStqMovIts, q, @ExcluiItem) then
    ReopenPRi(qqMPIn, QrMPInControle.Value);
end;

procedure TFmMPIn.PMEditaPopup(Sender: TObject);
begin
  DadosdaNF2.Enabled :=
    (QrNFeCabA.State <> dsInactive) and
    (QrNFeCabA.RecordCount > 0);
  Baixadeestoque1.Enabled :=
    (QrSMI_104.State <> dsInactive) and
    (QrSMI_104.RecordCount > 0);
  Produtoclassificadoousubprodgerado1.Enabled :=
    (QrSMI_101.State <> dsInactive) and
    (QrSMI_101.RecordCount > 0);
end;

procedure TFmMPIn.PMExcluiPopup(Sender: TObject);
begin
  ExcluiEntrada1.Enabled := not Geral.IntToBool_0(QrDefeitos.RecordCount+
    QrEmissF.RecordCount + QrEmissT.RecordCount + QrEmitCus.RecordCount);
end;

procedure TFmMPIn.PMInserePopup(Sender: TObject);
begin
  DadosdaNF1.Enabled :=
    (QrNFeCabA.State <> dsInactive) and
    (QrNFeCabA.RecordCount = 0);
  porLote1.Enabled := Trim(EdLote.Text) <> '';
  porLote2.Enabled := Trim(EdLote.Text) <> '';
end;

procedure TFmMPIn.porLote1Click(Sender: TObject);
//var
  //Controles: String;
begin
{
  try
    QrMPIn.DisableControls;
    QrMPIn.First;
    while not QrMPIn.Eof do
    begin
      Controles := Controles + FormatFloat('0', QrMPInControle.Value) + ',';
      QrMPIn.Next;
    end;
  finally
    QrMPIn.EnableControls;
  end;
  /
  Geral.MB_Aviso(Controles);
}
  if DBCheck.CriaFm(TFmMPClasMul, FmMPClasMul, afmoNegarComAviso) then
  begin
    FmMPClasMul.ShowModal;
    FmMPClasMul.Destroy;
  end;
end;

procedure TFmMPIn.porLote2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPClasMulSub, FmMPClasMulSub, afmoNegarComAviso) then
  begin
    FmMPClasMulSub.ShowModal;
    FmMPClasMulSub.Destroy;
  end;
end;

procedure TFmMPIn.Produtoclassificadoousubprodgerado1Click(Sender: TObject);
begin
  Grade_PF.AlteraSMIA(QrSMI_101);
  ReopenStqMovIts(0);
end;

procedure TFmMPIn.RetiraPesagem1Click(Sender: TObject);
begin
  if Application.MessageBox(PChar('Confirma a retirada da pesagem da entrada '+
  'atual? A pesagem n�o ser� excluida!'),
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)= ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM emitcus WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrEmitCusControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.AtualizaMPIn(QrMPInControle.Value);
    ReopenPRi(qqMPIn, QrMPInControle.Value);
  end;
end;

procedure TFmMPIn.QrDefTipCalcFields(DataSet: TDataSet);
begin
  if QrDefTot.RecordCount > 0 then QrDefTipPercTot.Value :=
    QrDefTipDEFEITOS.Value / QrDefTotDEFEITOS.Value * 100
  else QrDefTipPercTot.Value := 0;
end;

procedure TFmMPIn.QrDefLocCalcFields(DataSet: TDataSet);
begin
  if QrDefTot.RecordCount > 0 then QrDefLocPercTot.Value :=
    QrDefLocDEFEITOS.Value / QrDefTotDEFEITOS.Value * 100
  else QrDefLocPercTot.Value := 0;
end;

procedure TFmMPIn.QrDefTotCalcFields(DataSet: TDataSet);
begin
  if QrMPInPecas.Value > 0 then
  QrDefTotMEDIACOURO.Value := QrDefTotDEFEITOS.Value / QrMPInPecas.Value
  else QrDefTotMEDIACOURO.Value := 0;
end;

procedure TFmMPIn.QrEstq1CalcFields(DataSet: TDataSet);
begin
  QrEstq1KGT.Value := 0;
  //
  QrEstq1MINDTACAL_TXT.Value := dmkPF.FDT_NULO(QrEstq1MinDtaCal.Value, 3);
  QrEstq1MINDTACUR_TXT.Value := dmkPF.FDT_NULO(QrEstq1MinDtaCur.Value, 3);
  QrEstq1MINDTAENX_TXT.Value := dmkPF.FDT_NULO(QrEstq1MinDtaEnx.Value, 3);
  //
  QrEstq1MAXDTACAL_TXT.Value := dmkPF.FDT_NULO(QrEstq1MaxDtaCal.Value, 3);
  QrEstq1MAXDTACUR_TXT.Value := dmkPF.FDT_NULO(QrEstq1MaxDtaCur.Value, 3);
  QrEstq1MAXDTAENX_TXT.Value := dmkPF.FDT_NULO(QrEstq1MaxDtaEnx.Value, 3);
  //   Parei aqui 25/07/2009
  //QrEstq1Sado_Pc_Exp.Value := 
end;

procedure TFmMPIn.Itensdematriaprima1Click(Sender: TObject);
var
  Controle: Integer;
begin
  LimpaMPInRat();
  if QrMPIn.RecordCount > 0 then
  begin
    Controle := QrMPInControle.Value;
    QrMax.Close;
    QrMax.Params[0].AsInteger := Controle;
    UnDmkDAC_PF.AbreQuery(QrMax, Dmod.MyDB);
    Application.CreateForm(TFmMPInIts1, FmMPInIts1);
    FmMPInIts1.FConta    := 0;
    FmMPInIts1.FCodigo   := QrMPInCodigo.Value;
    FmMPInIts1.FControle := Controle;
    FmMPInIts1.ImgTipo.SQLType := stIns;
    FmMPInIts1.EdFreteInfo.ValueVariant := FmMPInIts1.QrEntiMPCMPFrete.Value;
    FmMPInIts1.EdCaminhao.ValueVariant := QrMaxCaminhao.Value + 1;
    FmMPInIts1.EdFornece.Text := IntToStr(QrMPInProcedencia.Value);
    FmMPInIts1.CBFornece.KeyValue := QrMPInProcedencia.Value;
    FmMPInIts1.ShowModal;
    FmMPInIts1.Destroy;
    ReopenPri(qqMPIn, Controle);
    QrMax.Close;
  end;
end;

procedure TFmMPIn.Ativa1Click(Sender: TObject);
begin
  ForcaEncerramento(True);
end;

procedure TFmMPIn.AtualizaMPIn(Controle: Integer);
var
  QuebrVReal, QuebrVCobr, CMPPagar, CMPDesQV, ComisVal,
  PS_ValUni, PS_QtdTot, PS_ValTot: Double;
  Qtde, Pecas, Peso, AreaM2, AreaP2: Double;
  IDCtrl, GraGruX: Integer;
begin
  // Define mpinits.fornece = mpin.procedencia quando mpinits.fornece for = 0
  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE mpin mpi, mpinits its');
  Dmod.QrUpdM.SQL.Add('SET its.Fornece=mpi.Procedencia');
  Dmod.QrUpdM.SQL.Add('WHERE its.Controle=mpi.Controle');
  Dmod.QrUpdM.SQL.Add('AND its.Fornece=0');
  Dmod.QrUpdM.SQL.Add('AND its.Controle=' + IntToStr(Controle));
  Dmod.QrUpdM.ExecSQL;

  // Somas de MPInIts e pagamentos para atualizar totais do MPIn
  QrIn2.Close;
  QrIn2.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrIn2, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumF, Dmod.MyDB, [
  'SELECT SUM(Debito) Debito ',
  'FROM ' + FTabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FATID_1003),
  'AND FatNum=' + Geral.FF0(Controle),
  '']);
  (*
  QrSumF.Close;
  QrSumF.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrSumF, Dmod.MyDB);
  *)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumT, Dmod.MyDB, [
  'SELECT SUM(Debito) Debito ',
  'FROM ' + FTabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FATID_1004),
  'AND FatNum=' + Geral.FF0(Controle),
  '']);
  (*
  QrSumT.Close;
  QrSumT.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrSumT, Dmod.MyDB);
  *)
  //
  QrIts.Close;
  QrIts.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrIts, Dmod.MyDB);
  PS_QtdTot := QrItsPS_QtdTot.Value;
  PS_ValTot := QrItsPS_ValTot.Value;
  if PS_QtdTot = 0 then
    PS_ValUni := 0
  else
    PS_ValUni := PS_ValTot / PS_QtdTot;
  if QrItsPNF.Value = 0 then
  begin
    QuebrVReal := 0;
    QuebrVCobr := 0;
  end else begin
    QuebrVReal := (QrItsPNF.Value - QrItsPLE.Value) / QrItsPNF.Value * 100;
    if QuebrVReal > QrIn2CMP_LPQV.Value then
    begin
      case QrIn2CMP_IDQV.Value of
        0: QuebrVCobr := 0;
        1: QuebrVCobr := QuebrVReal;
        2: QuebrVCobr := QuebrVReal - QrIn2CMP_LPQV.Value;
        else QuebrVCobr := 0;
      end;
    end else QuebrVCobr := 0;
  end;
  CMPDEsQV := QrItsCMPValor.Value * QuebrVCobr / 100;
  CMPPagar := QrItsCMPValor.Value - CMPDEsQV - QrIn2DescAdiant.Value;
  ComisVal := QrItsCMPValor.Value * QrIn2ComisPer.Value / 100;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE mpin SET Valor=:p0, Frete=:P1, ');
  Dmod.QrUpd.SQL.Add('Pecas=:P2, PecasNF=:P3, M2=:P4, ');
  Dmod.QrUpd.SQL.Add('PNF=:P5, PLE=:P6, PDA=:P7, CMPValor=:P8, ');
  Dmod.QrUpd.SQL.Add('CMPFrete=:P9, CustoInfo=:P10, ');
  Dmod.QrUpd.SQL.Add('QuebrVReal=:P11, QuebrVCobr=:P12, ');
  Dmod.QrUpd.SQL.Add('CMPDesQV=:P13, CMPPagar=:P14, ComisVal=:P15, ');
  Dmod.QrUpd.SQL.Add('PS_ValUni=:P16, PS_QtdTot=:P17, PS_ValTot=:P18 ');
  //
  Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
  Dmod.QrUpd.Params[00].AsFloat   := QrSumFDebito.Value; // Valor efetivo pago
  Dmod.QrUpd.Params[01].AsFloat   := QrSumTDebito.Value;
  Dmod.QrUpd.Params[02].AsFloat   := QrItsPecas.Value;
  Dmod.QrUpd.Params[03].AsFloat   := QrItsPecasNF.Value;
  Dmod.QrUpd.Params[04].AsFloat   := QrItsM2.Value;
  Dmod.QrUpd.Params[05].AsFloat   := QrItsPNF.Value;
  Dmod.QrUpd.Params[06].AsFloat   := QrItsPLE.Value;
  Dmod.QrUpd.Params[07].AsFloat   := QrItsPDA.Value;
  Dmod.QrUpd.Params[08].AsFloat   := QrItsCMPValor.Value;
  Dmod.QrUpd.Params[09].AsFloat   := QrItsCMPFrete.Value;
  Dmod.QrUpd.Params[10].AsFloat   := QrItsCustoInfo.Value;
  Dmod.QrUpd.Params[11].AsFloat   := QuebrVReal;
  Dmod.QrUpd.Params[12].AsFloat   := QuebrVCobr;
  Dmod.QrUpd.Params[13].AsFloat   := CMPDesQV;
  Dmod.QrUpd.Params[14].AsFloat   := CMPPagar;
  Dmod.QrUpd.Params[15].AsFloat   := ComisVal;
  Dmod.QrUpd.Params[16].AsFloat   := PS_ValUni;
  Dmod.QrUpd.Params[17].AsFloat   := PS_QtdTot;
  Dmod.QrUpd.Params[18].AsFloat   := PS_ValTot;
  //
  Dmod.QrUpd.Params[19].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  //Atualizar stqmovitsa
  IDCtrl      := QrIn2StqMovIts.Value;
  if IDCtrl <> 0 then
  begin
    GraGruX     := Dmod.ObtemGraGruXDeCouro(QrIn2Tipificacao.Value, QrIn2Animal.Value);
    // 2011-04-20
    //Qtde        := QrItsPecas.Value;
    Dmod.QrGGX_MP.Close;
    Dmod.QrGGX_MP.Params[0].AsInteger := GraGruX;
    UnDmkDAC_PF.AbreQuery(Dmod.QrGGX_MP, Dmod.MyDB);
    case Dmod.QrGGX_MPGerBxaEstq.Value of
      0: (* ? ? ?   *) Qtde := QrItsPecas.Value;
      1: (* Pe�a    *) Qtde := QrItsPecas.Value;
      2: (* m�      *) Qtde := QrItsM2.Value;
      3: (* kg      *) Qtde := QrItsPLE.Value;
      4: (* t (ton) *) Qtde := QrItsPLE.Value / 1000;
      5: (* ft�     *) Qtde := Geral.ConverteArea(QrItsM2.Value, ctM2toP2, cfQuarto);
      else Qtde := QrItsPecas.Value;
    end;
    // Fim 2011-04-20
    Pecas       := QrItsPecas.Value;
    Peso        := QrItsPLE.Value;
    AreaM2      := QrItsM2.Value;
    AreaP2      := Geral.ConverteArea(QrItsM2.Value, ctM2toP2, cfQuarto);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovitsa', False, [
    'Qtde', 'Pecas', 'Peso', 'AreaM2', 'AreaP2',
    'GraGruX'], ['IDCtrl'], [
    Qtde, Pecas, Peso, AreaM2, AreaP2,
    GraGruX], [IDCtrl], False) then
    begin
      // 2011-04-20
      // Arrumar itens baixados!
      QrSMIA.Close;
      QrSMIA.Params[0].AsInteger := IDCtrl;
      UnDmkDAC_PF.AbreQuery(QrSMIA, Dmod.MyDB);
      //
      Dmod.QrSMPIBxa.Close;
      Dmod.QrSMPIBxa.Params[0].AsInteger := QrSMIAParCodi.Value;
      UnDmkDAC_PF.AbreQuery(Dmod.QrSMPIBxa, Dmod.MyDB);
      if Dmod.QrSMPIBxa.RecordCount > 0 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('UPDATE stqmovitsa SET GraGruX=:P0 WHERE IDCtrl=:P1');
        Dmod.QrSMPIBxa.First;
        while not Dmod.QrSMPIBxa.Eof do
        begin
          if Dmod.QrSMPIBxaGraGruX.Value <> GraGruX then
          begin
            DMod.QrUpd.Params[00].AsInteger := GraGruX;
            DMod.QrUpd.Params[01].AsInteger := Dmod.QrSMPIBxaIDCtrl.Value;
            DMod.QrUpd.ExecSQL;
          end;
          Dmod.QrSMPIBxa.Next;
        end;
      end;
      // Fim 2011-04-20
    end;
  end else Dmod.AtualizaStqMovIts_MPI(nil);
  //
  FControle := Controle;
  ReopenPri(qqMPIn, FControle);
  //DMod.AtualizaMPIn(CLi?, FControle);
end;

procedure TFmMPIn.ReopenMPInIts(Conta: Integer);
var
  NF, tpNF: Integer;
begin
  //
  NF   := EdNF.ValueVariant;
  tpNF := RGtpNF.ItemIndex;
  //
  QrMPInIts.Close;
  QrMPInIts.SQL.Clear;
  QrMPInIts.SQL.Add('SELECT its.*,');
  QrMPInIts.SQL.Add('IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEPROCEDENCIA');
  QrMPInIts.SQL.Add('FROM mpinits its');
  QrMPInIts.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=its.Fornece');
  QrMPInIts.SQL.Add('WHERE its.Controle=' + FormatFloat('0', QrMPInControle.Value));
  QrMPInIts.SQL.Add('');
  QrMPInIts.SQL.Add('');
  QrMPInIts.SQL.Add('');
  QrMPInIts.SQL.Add('');
  //
  if (NF > 0) or (tpNF > 0) then
  begin
    if NF > 0 then
      QrMPInIts.SQL.Add('AND its.NF =' + FormatFloat('0', NF));
    case tpNF of
      // Nada
      0: ;
      // Que entrou com NF
      1: QrMPInIts.SQL.Add('AND its.NF_Inn_Fut = 0');
      // Que entrou sem NF mas foi gerada uma NF de entrada
      2: QrMPInIts.SQL.Add('AND (its.NF_Inn_Fut = 1 AND its.NF_Inn_Its > 0)');
      // Que entrou sem NF e ainda n�o foi gerada uma NF de entrada
      3: QrMPInIts.SQL.Add('AND (its.NF_Inn_Fut = 1 AND its.NF_Inn_Its = 0)');
    end;
  end;
  if (QrMPIn.State <> dsInactive) and (QrMPIn.RecordCount > 0) then
  begin
    UnDmkDAC_PF.AbreQuery(QrMPInIts, Dmod.MyDB);
    //
    if not QrMPInIts.Locate('Conta', Conta, []) then
      QrMPInIts.Locate('Conta', FConta, []);
  end;
end;

procedure TFmMPIn.Itematual1Click(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  Codigo   := QrMPInCodigo.Value;
  Controle := QrMPInControle.Value;
  //
  if not Dmod.ReopenSMI(Codigo, Controle) then
    Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mpin', False, [
  'EmClassif'], ['Controle'], [
  1], [Controle], True) then
  begin
    try
      if DBCheck.CriaFm(TFmStqClasse, FmStqClasse, afmoNegarComAviso) then
      begin
        FmStqClasse.FOriCodi       := Controle;
        //FmStqClasse.FOriCtrl     := 0;
        FmStqClasse.FOriCnta       := 0;
        FmStqClasse.FOriPart       := 0;
        FmStqClasse.FStqCenCad     := Dmod.QrSMIStqCenCad.Value;
        FmStqClasse.FGraGruX       := Dmod.QrSMIGraGruX.Value;
        FmStqClasse.FDebCtrl       := Dmod.QrSMIIDCtrl.Value;
        FmStqClasse.FTPecas        := QrMPInPecas.Value;
        FmStqClasse.FTPLE          := QrMPInPLE.Value;
        //
        FmStqClasse.ShowModal;
        FmStqClasse.Destroy;
        //
        ReopenPRi(qqMPIn, Controle);
      end;
    finally
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mpin', False, [
      'EmClassif'], ['Controle'], [0], [Controle], True);
    end;
  end;
end;

procedure TFmMPIn.Itemdematriaprima1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if QrMPInIts.RecordCount > 0 then
  begin
    LimpaMPInRat();
    Controle := QrMPInControle.Value;
    Application.CreateForm(TFmMPInIts1, FmMPInIts1);
    FmMPInIts1.FConta    := QrMPInItsConta.Value;
    FmMPInIts1.FCodigo   := QrMPInCodigo.Value;
    FmMPInIts1.FControle := Controle;
    FmMPInIts1.ImgTipo.SQLType := stUpd;
    //
    FmMPInIts1.EdFornece.Text              := IntToStr(QrMPInItsFornece.Value);
    FmMPInIts1.CBFornece.KeyValue          := QrMPInItsFornece.Value;
    FmMPInIts1.EdCaminhao.ValueVariant     := QrMPInItsCaminhao.Value;
    FmMPInIts1.EdPecasNF.ValueVariant      := QrMPInItsPecasNF.Value;
    FmMPInIts1.EdPNF.ValueVariant          := QrMPInItsPNF.Value;
    //FmMPInIts1.EdPNF_Fat.ValueVariant      := QrMPInItsPNF_Fat.Value;
    FmMPInIts1.EdPecas.ValueVariant        := QrMPInItsPecas.Value;
    FmMPInIts1.EdPLE.ValueVariant          := QrMPInItsPLE.Value;
    FmMPInIts1.EdPDA.ValueVariant          := QrMPInItsPDA.Value;
    FmMPInIts1.EdCMPValor.ValueVariant     := QrMPInItsCMPValor.Value;
    FmMPInIts1.EdCMPFrete.ValueVariant     := QrMPInItsCMPFrete.Value;
    //
    FmMPInIts1.EdNF_Modelo.Text            := QrMPInItsNF_Modelo.Value;
    FmMPInIts1.EdNF_Serie.Text             := QrMPInItsNF_Serie.Value;
    FmMPInIts1.EdNF.ValueVariant           := QrMPInItsNF.Value;
    FmMPInIts1.EdConheci.ValueVariant      := QrMPInItsConheci.Value;
    FmMPInIts1.TPEmissao.Date              := QrMPInItsEmissao.Value;
    FmMPInIts1.TPVencto.Date               := QrMPInItsVencto.Value;
    FmMPInIts1.TPEmisFrete.Date            := QrMPInItsEmisFrete.Value;
    FmMPInIts1.TPVctoFrete.Date            := QrMPInItsVctoFrete.Value;
    //
    FmMPInIts1.EdPS_QtdTot.ValueVariant    := QrMPInItsPS_QtdTot.Value;
    FmMPInIts1.EdPS_ValTot.ValueVariant    := QrMPInItsPS_ValTot.Value;
    FmMPInIts1.EdPS_ValUni.ValueVariant    := QrMPInItsPS_ValUni.Value;
    //
    FmMPInIts1.CkNF_Inn_Fut.Checked        := dmkPF.ITB(QrMPInItsNF_Inn_Fut.Value);
    //
    FmMPInIts1.EdCFOP.ValueVariant         := QrMPInItsCFOP.Value;
    //
    FmMPInIts1.EdEmitNFAvul.ValueVariant   := QrMPInItsEmitNFAvul.Value;
    FmMPInIts1.CBEmitNFAvul.KeyValue       := QrMPInItsEmitNFAvul.Value;
    //
    FmMPInIts1.ShowModal;
    FmMPInIts1.Destroy;
    ReopenPri(qqMPIn, Controle);
  end else Application.MessageBox('N�o h� item a ser editado!', 'Aviso',
  MB_OK+MB_ICONWARNING);
end;

procedure TFmMPIn.Itemdematriaprima2Click(Sender: TObject);
begin
  if Application.MessageBox(PChar('Confirma a exclus�o do item de entrada de ' +
  'mat�ria-prima selecionada?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mpinits WHERE Conta=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrMPInItsConta.Value;
    Dmod.QrUpd.ExecSQL;
    //
    AtualizaMPIn(QrMPInControle.Value);
  end;
end;

procedure TFmMPIn.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmMPIn.Defeitos1Click(Sender: TObject);
begin
  if QrDefeitos.State = dsBrowse then
    MyObjects.frxMostra(frxDefeitos, 'Relat�rio de Defeitos');
end;

procedure TFmMPIn.Estoque1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraImpLista, FmGraImpLista, afmoNegarComAviso) then
  begin
    FmGraImpLista.ShowModal;
    FmGraImpLista.Destroy;
  end;
end;

procedure TFmMPIn.Estoqueeproduodiria1Click(Sender: TObject);
var
  DataI, DataF, Ordem: String;
  ClienteI, Procedencia: Integer;
begin
  case RGOrdem.ItemIndex of
    0: Ordem := 'ORDER BY Data DESC, Ficha DESC, Controle DESC';
    1: Ordem := 'ORDER BY NOMECLIENTEI, Data, Ficha, Controle';
    2: Ordem := 'ORDER BY Lote, Data, Ficha, Controle';
  end;
  DataI := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  DataF := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  //
  QrEstq1.Close;
  QrEstq1.SQL.Clear;
  QrEstq1.SQL.Add('SELECT IF(ci.Tipo=0, ci.RazaoSocial, ci.Nome) NOMECLIENTEI,');
  QrEstq1.SQL.Add('IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEPROCEDENCIA,');
  QrEstq1.SQL.Add('IF(wi.Pecas=0, 0, wi.PLE/wi.Pecas) MEDIA_PLE,');
  QrEstq1.SQL.Add('IF(wi.PesoCal=0, 0, wi.CusPQCal/wi.PesoCal) CUSKGPQCAL,');
  QrEstq1.SQL.Add('IF(wi.PesoCur=0, 0, wi.CusPQCur/wi.PesoCur) CUSKGPQCUR,');
  QrEstq1.SQL.Add('IF(wi.PLE=0, 0, (wi.CusPQCal+wi.CusPQCur)/wi.PLE) CUSTOKGPLE,');
  QrEstq1.SQL.Add('IF(wi.PLE=0, 0, (wi.PLE-wi.PDA)/wi.PLE*100) QBR_PLE_PDA,');
  QrEstq1.SQL.Add('IF(wi.PDA=0, 0, (wi.PDA-wi.PTA)/wi.PDA*100) QBR_PDA_PTA,');
  QrEstq1.SQL.Add('IF(wi.PLE=0, 0, (wi.PLE-wi.PTA)/wi.PLE*100) QBR_PLE_PTA,');
  QrEstq1.SQL.Add('wi.*');
  QrEstq1.SQL.Add('FROM mpin wi');
  QrEstq1.SQL.Add('LEFT JOIN entidades ci ON ci.Codigo=wi.ClienteI');
  QrEstq1.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=wi.Procedencia');
  QrEstq1.SQL.Add('WHERE wi.Data BETWEEN :P0 AND :P1');
  //
  ClienteI := Geral.IMV(EdClienteI.Text);
  if ClienteI <> 0 then
    QrEstq1.SQL.Add('AND wi.ClienteI=' + FormatFloat('0', ClienteI));
  Procedencia := Geral.IMV(EdProcedencia.Text);
  if Procedencia <> 0 then
    QrEstq1.SQL.Add('AND wi.Procedencia=' + FormatFloat('0', Procedencia));
  if dmkEdOS.ValueVariant > 0 then
    QrEstq1.SQL.Add('AND wi.Controle='+IntToStr(dmkEdOS.ValueVariant));
  if Trim(EdMarca.Text) <> '' then
    QrEstq1.SQL.Add('AND wi.Marca LIKE "%' + EdMarca.Text + '%"');
  if Trim(EdLote.Text) <> '' then
    QrEstq1.SQL.Add('AND wi.Lote LIKE "%' + EdLote.Text + '%"');
  if RGEncerrado.ItemIndex < 2 then
    QrEstq1.SQL.Add('AND wi.Encerrado=' + FormatFloat('0', RGEncerrado.ItemIndex));
  QrEstq1.SQL.Add('');
  QrEstq1.SQL.Add(Ordem);
  QrEstq1.Params[0].AsString := DataI;
  QrEstq1.Params[1].AsString := DataF;
  try
    UnDmkDAC_PF.AbreQuery(QrEstq1, Dmod.MyDB);
    MyObjects.frxMostra(frxEstq1, 'Estoque e produ��o di�ria');
  except
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrEstq1, '', nil, True, True);
    raise;
  end;
end;

procedure TFmMPIn.OS1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxOS, 'Ordem de servi�o');
end;

procedure TFmMPIn.QrMPInItsAfterScroll(DataSet: TDataSet);
begin
  ReopenNFeCabA();
end;

procedure TFmMPIn.ReopenNFeCabA();
begin
  QrNFeCabA.Close;
  QrNFeCabA.SQL.Clear;
  QrNFeCabA.SQL.Add('SELECT *');
  QrNFeCabA.SQL.Add('FROM nfecaba');
  QrNFeCabA.SQL.Add('WHERE FatID in (:P0, :P1, :P2)');
  QrNFeCabA.SQL.Add('AND FatNum=:P3');
  QrNFeCabA.SQL.Add('AND Empresa=:P4');
  //QrNFeCabA.SQL.Add('AND ide_nNF=:P3');
  QrNFeCabA.SQL.Add('');
  QrNFeCabA.SQL.Add('');
  QrNFeCabA.Params[00].AsInteger := VAR_FATID_0013;
  QrNFeCabA.Params[01].AsInteger := VAR_FATID_0113;
  QrNFeCabA.Params[02].AsInteger := VAR_FATID_0213;
  QrNFeCabA.Params[03].AsInteger := QrMPInItsConta.Value;
  QrNFeCabA.Params[04].AsInteger := QrMPInClienteI.Value;
  //QrNFeCabA.Params[03].AsInteger := QrFatPedNFsNumeroNF.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeCabA, Dmod.MyDB);
end;

procedure TFmMPIn.QrMPInItsBeforeClose(DataSet: TDataSet);
begin
  QrNFeCabA.Close;
end;

procedure TFmMPIn.QrMPInItsCalcFields(DataSet: TDataSet);
begin
  QrMPInItsSEQ.Value := QrMPInIts.RecNo;
  case QrMPInItsCMPUnida.Value of
    // Precisa ser _Fat?
    //0: QrMPInItsQUANTIDADE.Value := QrMPInItsPNF_Fat.Value;
    0: QrMPInItsQUANTIDADE.Value := QrMPInItsPNF.Value;
    1: QrMPInItsQUANTIDADE.Value := QrMPInItsPecasNF.Value;
    else QrMPInItsQUANTIDADE.Value := 0;
  end;
  QrMPInItsVENCTO_TXT.Value := dmkPF.FDT_NULO(QrMPInItsVencto.Value, 3);
  QrMPInItsVCTOFRETE_TXT.Value := dmkPF.FDT_NULO(QrMPInItsVctoFrete.Value, 3);
end;

procedure TFmMPIn.ReopenEmissT(FatParcela: Integer);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissT, Dmod.MyDB, [
    'SELECT la.*, ca.Nome NOMECARTEIRA ',
    'FROM ' + FTabLctA + ' la, Carteiras ca ',
    'WHERE ca.Codigo=la.Carteira ',
    'AND la.FatID=' + Geral.FF0(VAR_FATID_1004),
    'AND la.FatNum=' + Geral.FF0(QrMPInControle.Value),
    'AND la.ID_Pgto = 0 ',
    '']);
  (*
  QrEmissT.Close;
  QrEmissT.Params[00].AsInteger := QrMPInControle.Value;
  UnDmkDAC_PF.AbreQuery(QrEmissT, Dmod.MyDB);
  *)
  //
  QrEmissT.Locate('FatParcela', FatParcela, []);
end;

procedure TFmMPIn.ReopenEmissF(FatParcela: Integer);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissF, Dmod.MyDB, [
    'SELECT la.*, ca.Nome NOMECARTEIRA ',
    'FROM ' + FTabLctA + ' la, Carteiras ca ',
    'WHERE ca.Codigo=la.Carteira ',
    'AND la.FatID=' + Geral.FF0(VAR_FATID_1003),
    'AND la.FatNum=' + Geral.FF0(QrMPInControle.Value),
    'AND la.ID_Pgto = 0 ',
    '']);
  //
  (*
  QrEmissF.Close;
  QrEmissF.Params[00].AsInteger := QrMPInControle.Value;
  UnDmkDAC_PF.AbreQuery(QrEmissF, Dmod.MyDB);
  *)
  //
  QrEmissF.Locate('FatParcela', FatParcela, []);
end;

procedure TFmMPIn.Rateiacaminho1Click(Sender: TObject);
begin
{
  if QrMPIn.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� entrada selecionada!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end else
  if QrMPInIts.RecordCount > 0 then
  begin
    Application.MessageBox('J� existem itens na entrada selecionada!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end else begin
    begin
      if DBCheck.CriaForm(TFmMPInRat, FmMPInRat, afmNegarComAviso) then
      begin
        FmMPInRat.FTabLoc_ntrttMPInRat := FTabLoc_ntrttMPInRat;
        FmMPInRat.ShowModal;
        FmMPInRat.Destroy;
      end;
    end;
  end;
  }
end;

procedure TFmMPIn.LimpaMPInRat();
begin
  FTabLoc_ntrttMPInRat := UCriar.RecriaTempTableNovo(ntrttMPInRat, DmodG.QrUpdPID1, False, 0);
  //
  //FmPrincipal.RecriaTabelaLocal('mpinrat', 1);
  //
  {
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('DELETE FROM mpinrat');
  Dmod.QrUpdL.ExecSQL;
  }
  //
end;

procedure TFmMPIn.Pedidodecompra1Click(Sender: TObject);
begin
  Application.CreateForm(TFmMPInAssina, FmMPInAssina);
  FmMPInAssina.ShowModal;
  FmMPInAssina.Destroy;
  MyObjects.frxMostra(frxPedidoG2, 'Pedido de compra de mat�ria-prima (Grupo)');
end;

procedure TFmMPIn.PedidodeFrete1Click(Sender: TObject);
begin
  Application.CreateForm(TFmMPInAssina, FmMPInAssina);
  FmMPInAssina.ShowModal;
  FmMPInAssina.Destroy;
  //
  DmodG.ReopenEndereco(QrMPInTransportadora.Value);
  //
  MyObjects.frxMostra(frxPedidoF2, 'Pedido de frete de compra de mat�ria-prima');
end;

procedure TFmMPIn.pelaPesquisa1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPClasMulSub, FmMPClasMulSub, afmoNegarComAviso) then
  begin
    FmMPClasMulSub.ShowModal;
    FmMPClasMulSub.Destroy;
  end;
end;

procedure TFmMPIn.Perdadecouro1Click(Sender: TObject);
begin
  Application.CreateForm(TFmMPInPerdas, FmMPInPerdas);
  FmMPInPerdas.FMPInCtrl := QrMPInControle.Value;
  FmMPInPerdas.FControle := QrMPInPerdasControle.Value;
  FmMPInPerdas.ShowModal;
  FmMPInPerdas.Destroy;
end;

procedure TFmMPIn.frxEntrada0AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_PERIODO' then Value := dmkPF.PeriodoImp(TPIni.Date,
  TPFim.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'VARF_CLIENTE' then
  begin
    if CBClienteI.KeyValue = NULL then Value := 'TODOS' else
    Value := CBClienteI.Text;
  end
end;

procedure TFmMPIn.frxQuebra2GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_PERIODO' then Value := dmkPF.PeriodoImp(TPIni.Date,
  TPFim.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'VARF_CLIENTE' then
  begin
    if CBClienteI.KeyValue = NULL then Value := 'TODOS' else
    Value := CBClienteI.Text;
  end
end;

procedure TFmMPIn.Modelo11Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxQuebra1, 'Quebra de entrada de mat�ria-prima');
end;

procedure TFmMPIn.Modelo21Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxQuebra2, 'Quebra de entrada de mat�ria-prima');
end;

procedure TFmMPIn.ModeloA1Click(Sender: TObject);
begin
  case RGOrdem.ItemIndex of
    0: MyObjects.frxMostra(frxEntrada0A, 'Entrada de couros pela data');
    1: MyObjects.frxMostra(frxEntrada1A, 'Entrada de couros por cliente interno');
    2: MyObjects.frxMostra(frxEntrada2A, 'Entrada de couros por lote');
  end;
end;

procedure TFmMPIn.ModeloB1Click(Sender: TObject);
begin
  case RGOrdem.ItemIndex of
    0: MyObjects.frxMostra(frxEntrada0B, 'Entrada de couros pela data');
    1: MyObjects.frxMostra(frxEntrada1B, 'Entrada de couros por cliente interno');
    2: MyObjects.frxMostra(frxEntrada2B, 'Entrada de couros por lote');
  end;
end;

procedure TFmMPIn.ReopenMPInPerdas(Controle: Integer);
begin
  QrMPInPerdas.Close;
  QrMPInPerdas.Params[0].AsInteger := QrMPInControle.Value;
  UnDmkDAC_PF.AbreQuery(QrMPInPerdas, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrMPInPerdas.Locate('Controle', Controle, []);
end;

procedure TFmMPIn.ForcaEncerramento(Forca: Boolean);
var
  Texto: String;
  Valor: Integer;
begin
  if Forca then
  begin
    Texto := '';
    Valor := 1;
  end else begin
    Texto := 'cancelamento do ';
    Valor := 0;
  end;
  if Application.MessageBox(PChar('Confirma o ' + Texto +
  'encerramento do lote selecionado?'), 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE mpin SET ForcaEncer=:P0 WHERE Controle=:P1');
    Dmod.QrUpd.Params[00].AsInteger := Valor;
    Dmod.QrUpd.Params[01].AsInteger := QrMPInControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.AtualizaMPIn(QrMPInControle.Value);
    ReopenPri(qqMPIn, QrMPInControle.Value);
  end;
end;

end.

