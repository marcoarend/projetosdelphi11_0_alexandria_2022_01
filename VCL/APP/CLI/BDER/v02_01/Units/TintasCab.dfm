object FmTintasCab: TFmTintasCab
  Left = 368
  Top = 194
  Caption = 'QUI-RECEI-101 :: Receitas de Acabamento'
  ClientHeight = 830
  ClientWidth = 1241
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 712
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 133
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 5
        Top = 5
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 79
        Top = 5
        Width = 69
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Refer'#234'ncia:'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 182
        Top = 5
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label24: TLabel
        Left = 645
        Top = 5
        Width = 68
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Espessura:'
      end
      object Label25: TLabel
        Left = 916
        Top = 5
        Width = 35
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Setor:'
      end
      object Label26: TLabel
        Left = 1073
        Top = 55
        Width = 130
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'T'#233'cnico respons'#225'vel:'
      end
      object Label29: TLabel
        Left = 5
        Top = 54
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = #193'rea (m'#178'):'
      end
      object Label30: TLabel
        Left = 133
        Top = 54
        Width = 73
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Quantidade:'
      end
      object Label31: TLabel
        Left = 261
        Top = 54
        Width = 210
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente interno (Dono dos insumos):'
      end
      object Label32: TLabel
        Left = 916
        Top = 54
        Width = 50
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cria'#231#227'o:'
      end
      object Label33: TLabel
        Left = 994
        Top = 54
        Width = 61
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Altera'#231#227'o:'
      end
      object SpeedButton5: TSpeedButton
        Left = 886
        Top = 25
        Width = 25
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object SpeedButton6: TSpeedButton
        Left = 1186
        Top = 25
        Width = 25
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object EdNumero: TdmkEdit
        Left = 5
        Top = 25
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Numero'
        UpdCampo = 'Numero'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 182
        Top = 25
        Width = 458
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdTxtUsu: TdmkEdit
        Left = 79
        Top = 25
        Width = 98
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'TxtUsu'
        UpdCampo = 'TxtUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdAreaM2: TdmkEdit
        Left = 5
        Top = 74
        Width = 123
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdQtde: TdmkEdit
        Left = 133
        Top = 74
        Width = 123
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'Qtde'
        UpdCampo = 'Qtde'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdClienteI: TdmkEditCB
        Left = 261
        Top = 74
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClienteI'
        UpdCampo = 'ClienteI'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClienteI
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClienteI: TdmkDBLookupComboBox
        Left = 330
        Top = 74
        Width = 518
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsEntidades
        TabOrder = 10
        dmkEditCB = EdClienteI
        QryCampo = 'ClienteI'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object DBEdit19: TDBEdit
        Left = 916
        Top = 74
        Width = 74
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Color = clBtnFace
        DataField = 'DataCad'
        DataSource = DsTintasCab
        ReadOnly = True
        TabOrder = 12
      end
      object DBEdit20: TDBEdit
        Left = 994
        Top = 74
        Width = 74
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Color = clBtnFace
        DataField = 'DataAlt'
        DataSource = DsTintasCab
        ReadOnly = True
        TabOrder = 13
      end
      object EdTecnico: TdmkEdit
        Left = 1073
        Top = 74
        Width = 138
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 14
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Tecnico'
        UpdCampo = 'Tecnico'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEspessura: TdmkEditCB
        Left = 645
        Top = 25
        Width = 49
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Espessura'
        UpdCampo = 'Espessura'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEspessura
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEspessura: TdmkDBLookupComboBox
        Left = 694
        Top = 25
        Width = 192
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Linhas'
        ListSource = DsEspessuras
        TabOrder = 4
        dmkEditCB = EdEspessura
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdSetor: TdmkEditCB
        Left = 916
        Top = 25
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Setor'
        UpdCampo = 'Setor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSetor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSetor: TdmkDBLookupComboBox
        Left = 985
        Top = 25
        Width = 201
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsListaSetores
        TabOrder = 6
        dmkEditCB = EdSetor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CkAtiva: TdmkCheckBox
        Left = 857
        Top = 79
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativa'
        Checked = True
        State = cbChecked
        TabOrder = 11
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 634
      Width = 1241
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 18
        Width = 1237
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 1058
          Top = 0
          Width = 179
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 9
            Top = 2
            Width = 147
            Height = 50
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 10
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 712
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 108
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 5
        Top = 5
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 182
        Top = 5
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 79
        Top = 5
        Width = 69
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Refer'#234'ncia:'
        FocusControl = DBEdit1
      end
      object Label13: TLabel
        Left = 714
        Top = 5
        Width = 68
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Espessura:'
        FocusControl = DBEdit6
      end
      object Label14: TLabel
        Left = 916
        Top = 5
        Width = 35
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Setor:'
        FocusControl = DBEdit7
      end
      object Label15: TLabel
        Left = 1073
        Top = 55
        Width = 130
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'T'#233'cnico respons'#225'vel:'
        FocusControl = DBEdit8
      end
      object Label19: TLabel
        Left = 5
        Top = 54
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = #193'rea (m'#178'):'
        FocusControl = DBEdit9
      end
      object Label20: TLabel
        Left = 133
        Top = 54
        Width = 73
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Quantidade:'
        FocusControl = DBEdit11
      end
      object Label27: TLabel
        Left = 261
        Top = 54
        Width = 210
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente interno (Dono dos insumos):'
      end
      object Label21: TLabel
        Left = 916
        Top = 54
        Width = 50
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cria'#231#227'o:'
        FocusControl = DBEdit16
      end
      object Label28: TLabel
        Left = 994
        Top = 54
        Width = 61
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Altera'#231#227'o:'
        FocusControl = DBEdit17
      end
      object DBEdCodigo: TDBEdit
        Left = 5
        Top = 25
        Width = 69
        Height = 24
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Numero'
        DataSource = DsTintasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 182
        Top = 25
        Width = 527
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsTintasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 79
        Top = 25
        Width = 98
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TxtUsu'
        DataSource = DsTintasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdit6: TDBEdit
        Left = 714
        Top = 25
        Width = 198
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'LINHAS'
        DataSource = DsTintasCab
        TabOrder = 3
      end
      object DBEdit7: TDBEdit
        Left = 916
        Top = 25
        Width = 296
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NOMESETOR'
        DataSource = DsTintasCab
        TabOrder = 4
      end
      object DBEdit8: TDBEdit
        Left = 1073
        Top = 74
        Width = 138
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Tecnico'
        DataSource = DsTintasCab
        TabOrder = 5
      end
      object DBEdit9: TDBEdit
        Left = 5
        Top = 74
        Width = 123
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'AreaM2'
        DataSource = DsTintasCab
        TabOrder = 6
      end
      object DBEdit11: TDBEdit
        Left = 133
        Top = 74
        Width = 123
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Qtde'
        DataSource = DsTintasCab
        TabOrder = 7
      end
      object DBEdit15: TDBEdit
        Left = 261
        Top = 74
        Width = 587
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NOMECLII'
        DataSource = DsTintasCab
        TabOrder = 8
      end
      object DBEdit16: TDBEdit
        Left = 916
        Top = 74
        Width = 74
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Color = clBtnFace
        DataField = 'DataCad'
        DataSource = DsTintasCab
        ReadOnly = True
        TabOrder = 9
      end
      object DBEdit17: TDBEdit
        Left = 994
        Top = 74
        Width = 74
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Color = clBtnFace
        DataField = 'DataAlt'
        DataSource = DsTintasCab
        ReadOnly = True
        TabOrder = 10
      end
      object DBCheckBox1: TDBCheckBox
        Left = 852
        Top = 76
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativa'
        DataField = 'Ativo'
        DataSource = DsTintasCab
        TabOrder = 11
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object PnDados: TPanel
      Left = 0
      Top = 108
      Width = 1241
      Height = 425
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Splitter1: TSplitter
        Left = 0
        Top = 143
        Width = 1241
        Height = 6
        Cursor = crVSplit
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
      end
      object Panel1: TPanel
        Left = 0
        Top = 149
        Width = 1241
        Height = 276
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        BevelOuter = bvNone
        Caption = 'Panel1'
        TabOrder = 0
        object Label6: TLabel
          Left = 0
          Top = 0
          Width = 60
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = 'FLUXO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBGrid3: TDBGrid
          Left = 0
          Top = 20
          Width = 1241
          Height = 256
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsTintasFlu
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Nome da Opera'#231#227'o'
              Width = 160
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descri'
              Title.Caption = 'Descri'#231#227'o do Fluxo'
              Width = 356
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InfoCargM2_TXT'
              Title.Caption = 'Carga g/m'#178
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GramasM2_TXT'
              Title.Caption = 'Cons. g/m'#178
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROCESSO'
              Title.Caption = 'Processo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TintasTin_TXT'
              Title.Caption = 'ID Tinta'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID item fluxo'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OpProcStr'
              Title.Caption = 'Tipo'
              Width = 100
              Visible = True
            end>
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 561
        Height = 143
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label4: TLabel
          Left = 0
          Top = 0
          Width = 99
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Alignment = taCenter
          Caption = 'PROCESSO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 20
          Width = 561
          Height = 123
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          DataSource = DsTintasTin
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Processo'
              Width = 321
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GramasTi'
              Title.Caption = 'g/volume'
              Width = 60
              Visible = True
            end>
        end
      end
      object Panel9: TPanel
        Left = 561
        Top = 0
        Width = 680
        Height = 143
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel9'
        TabOrder = 2
        object Label5: TLabel
          Left = 0
          Top = 0
          Width = 269
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Alignment = taCenter
          Caption = 'ITENS (do processo selecionado)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBGrid2: TDBGrid
          Left = 0
          Top = 20
          Width = 680
          Height = 123
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsTintasIts
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGrid2DrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Status_TXT'
              Title.Caption = 'Status do prod.'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GramasTi'
              Title.Caption = 'g/vol'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Produto'
              Title.Caption = 'Insumo'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPQ'
              Title.Caption = 'Nome do insumo'
              Width = 294
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GramasKg'
              Title.Caption = 'g/kg'
              Width = 76
              Visible = True
            end>
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 634
      Width = 1241
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 35
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 469
        Top = 18
        Width = 770
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 607
          Top = 0
          Width = 163
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtReceita: TBitBtn
          Left = 5
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Receita'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtReceitaClick
        end
        object BtProcesso: TBitBtn
          Left = 153
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Processo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtProcessoClick
        end
        object BtItens: TBitBtn
          Left = 300
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Itens'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtItensClick
        end
        object BtFluxo: TBitBtn
          Left = 448
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Fluxo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtFluxoClick
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 916
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 355
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Receitas de Acabamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 355
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Receitas de Acabamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 355
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Receitas de Acabamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DsTintasCab: TDataSource
    DataSet = QrTintasCab
    Left = 56
    Top = 24
  end
  object QrTintasCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTintasCabBeforeOpen
    AfterOpen = QrTintasCabAfterOpen
    BeforeClose = QrTintasCabBeforeClose
    AfterScroll = QrTintasCabAfterScroll
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, Cli.RazaoSocial, cli.Nome)'
      'NOMECLII, lse.Nome NOMESETOR, esp.LINHAS, cab.*'
      'FROM tintascab cab'
      'LEFT JOIN entidades cli ON cli.Codigo=cab.ClienteI'#10
      'LEFT JOIN listasetores lse ON lse.Codigo=cab.Setor'#10
      'LEFT JOIN espessuras esp ON esp.Codigo=cab.Espessura'#10
      'WHERE cab.Numero>0'
      '')
    Left = 28
    Top = 24
    object QrTintasCabNOMECLII: TWideStringField
      FieldName = 'NOMECLII'
      Size = 100
    end
    object QrTintasCabNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrTintasCabLINHAS: TWideStringField
      FieldName = 'LINHAS'
      Size = 7
    end
    object QrTintasCabNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrTintasCabTxtUsu: TWideStringField
      FieldName = 'TxtUsu'
      Required = True
      Size = 10
    end
    object QrTintasCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrTintasCabClienteI: TIntegerField
      FieldName = 'ClienteI'
      Required = True
    end
    object QrTintasCabTipificacao: TIntegerField
      FieldName = 'Tipificacao'
      Required = True
    end
    object QrTintasCabDataI: TDateField
      FieldName = 'DataI'
    end
    object QrTintasCabDataA: TDateField
      FieldName = 'DataA'
    end
    object QrTintasCabTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrTintasCabSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrTintasCabEspessura: TIntegerField
      FieldName = 'Espessura'
    end
    object QrTintasCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTintasCabQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.0'
    end
    object QrTintasCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTintasCabDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTintasCabDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTintasCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTintasCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTintasCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrTintasCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtReceita
    CanUpd01 = BtItens
    Left = 84
    Top = 24
  end
  object PMReceita: TPopupMenu
    OnPopup = PMReceitaPopup
    Left = 344
    Top = 384
    object Incluinovareceita1: TMenuItem
      Caption = '&Inclui nova receita'
      OnClick = Incluinovareceita1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera cabe'#231'alho da receita atual'
      OnClick = Altera1Click
    end
    object Excluireceitaatual1: TMenuItem
      Caption = '&Exclui receita atual'
      OnClick = Excluireceitaatual1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Excluidiversasreceitas1: TMenuItem
      Caption = 'E&xclui diversas receitas'
      OnClick = Excluidiversasreceitas1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Duplicareceitaatual1: TMenuItem
      Caption = '&Duplica receita atual'
      OnClick = Duplicareceitaatual1Click
    end
  end
  object QrTintasIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq_.Nome NOMEPQ, tii.*'
      'FROM tintasits tii'
      ''
      'LEFT JOIN pq pq_ ON pq_.Codigo=tii.Produto'
      ''
      'WHERE tii.Codigo=:P0'
      'ORDER BY tii.Ordem')
    Left = 640
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTintasItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrTintasItsNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrTintasItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTintasItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrTintasItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTintasItsReordem: TIntegerField
      FieldName = 'Reordem'
      Required = True
    end
    object QrTintasItsProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrTintasItsGramasTi: TFloatField
      FieldName = 'GramasTi'
      DisplayFormat = '#,###,##0.000'
    end
    object QrTintasItsGramasKg: TFloatField
      FieldName = 'GramasKg'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrTintasItsObs: TWideStringField
      FieldName = 'Obs'
      Size = 30
    end
    object QrTintasItsCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrTintasItsStatus_TXT: TWideStringField
      FieldName = 'Status_TXT'
      Size = 30
    end
  end
  object DsTintasIts: TDataSource
    DataSet = QrTintasIts
    Left = 640
    Top = 56
  end
  object QrTintasFlu: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTintasFluCalcFields
    SQL.Strings = (
      'SELECT tin.Nome NOMEPROCESSO, flu.*'
      'FROM tintasflu flu'
      'LEFT JOIN tintastin tin ON tin.Codigo=flu.TintasTin'
      'WHERE flu.Numero=:P0'
      'ORDER BY flu.Ordem')
    Left = 700
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTintasFluNOMEPROCESSO: TWideStringField
      FieldName = 'NOMEPROCESSO'
      Size = 30
    end
    object QrTintasFluOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrTintasFluReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrTintasFluNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrTintasFluCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTintasFluNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrTintasFluTintasTin: TIntegerField
      FieldName = 'TintasTin'
    end
    object QrTintasFluGramasM2: TFloatField
      FieldName = 'GramasM2'
      DisplayFormat = '#,###,##0.000'
    end
    object QrTintasFluInfoCargM2: TFloatField
      FieldName = 'InfoCargM2'
      DisplayFormat = '#,###,##0.000'
    end
    object QrTintasFluDescri: TWideStringField
      FieldName = 'Descri'
      Size = 255
    end
    object QrTintasFluOpProc: TSmallintField
      FieldName = 'OpProc'
    end
    object QrTintasFluOpProcStr: TWideStringField
      FieldName = 'OpProcStr'
      Size = 30
    end
    object QrTintasFluInfoCargM2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'InfoCargM2_TXT'
      Calculated = True
    end
    object QrTintasFluGramasM2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'GramasM2_TXT'
      Calculated = True
    end
    object QrTintasFluTintasTin_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TintasTin_TXT'
      Size = 15
      Calculated = True
    end
    object QrTintasFluDESCRI_TXT: TWideStringField
      FieldName = 'DESCRI_TXT'
      Size = 255
    end
  end
  object DsTintasFlu: TDataSource
    DataSet = QrTintasFlu
    Left = 704
    Top = 56
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 208
    Top = 320
  end
  object DsListaSetores: TDataSource
    DataSet = QrListaSetores
    Left = 208
    Top = 348
  end
  object DsEspessuras: TDataSource
    DataSet = QrEspessuras
    Left = 208
    Top = 376
  end
  object QrEspessuras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Linhas, EMCM'
      'FROM espessuras'
      'ORDER BY EMCM')
    Left = 180
    Top = 376
    object QrEspessurasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEspessurasLinhas: TWideStringField
      FieldName = 'Linhas'
      Size = 7
    end
    object QrEspessurasEMCM: TFloatField
      FieldName = 'EMCM'
    end
  end
  object QrListaSetores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 180
    Top = 348
    object QrListaSetoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaSetoresNome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 180
    Top = 320
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object PMProcesso: TPopupMenu
    OnPopup = PMProcessoPopup
    Left = 432
    Top = 388
    object Incluinovoprocesso1: TMenuItem
      Caption = '&Inclui novo processo'
      OnClick = Incluinovoprocesso1Click
    end
    object Alteraprocessoatual1: TMenuItem
      Caption = '&Altera processo atual'
      OnClick = Alteraprocessoatual1Click
    end
    object Excluiprocessoatual1: TMenuItem
      Caption = '&Exclui processo atual'
      Enabled = False
      OnClick = Excluiprocessoatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Duplicaprocessoatual1: TMenuItem
      Caption = '&Duplica processo atual'
      OnClick = Duplicaprocessoatual1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Reordenaprocessos1: TMenuItem
      Caption = '&Reordena processos'
      OnClick = Reordenaprocessos1Click
    end
  end
  object QrTintasTin: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrTintasTinAfterOpen
    BeforeClose = QrTintasTinBeforeClose
    AfterScroll = QrTintasTinAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM tintastin'
      'WHERE Numero=:P0'
      'ORDER BY Ordem')
    Left = 580
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTintasTinOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrTintasTinReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrTintasTinNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrTintasTinCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTintasTinNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrTintasTinObserv: TWideStringField
      FieldName = 'Observ'
      Size = 50
    end
    object QrTintasTinGramasTi: TFloatField
      FieldName = 'GramasTi'
      DisplayFormat = '#,###,##0.000'
    end
  end
  object DsTintasTin: TDataSource
    DataSet = QrTintasTin
    Left = 580
    Top = 56
  end
  object PMItens: TPopupMenu
    OnPopup = PMItensPopup
    Left = 520
    Top = 384
    object Incluinovoitemdereceita1: TMenuItem
      Caption = '&Inclui novo item de receita'
    end
    object Alteraitemdereceitaatual1: TMenuItem
      Caption = '&Altera item de receita atual'
    end
    object Excluiitemdereceitaatual1: TMenuItem
      Caption = '&Exclui item de receita atual'
    end
  end
  object PMFluxo: TPopupMenu
    OnPopup = PMFluxoPopup
    Left = 664
    Top = 400
    object Incluinovoitemdefluxo1: TMenuItem
      Caption = '&Inclui novo item de fluxo'
      OnClick = Incluinovoitemdefluxo1Click
    end
    object Alteraitemdefluxoatual1: TMenuItem
      Caption = '&Altera item de fluxo atual'
      OnClick = Alteraitemdefluxoatual1Click
    end
    object Excluiitemdefluxoatual1: TMenuItem
      Caption = '&Exclui item de fluxo atual'
      OnClick = Excluiitemdefluxoatual1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Reordenafluxo1: TMenuItem
      Caption = '&Reordena fluxo'
      OnClick = Reordenafluxo1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Copianomedaoperaoparadescriodofluxo1: TMenuItem
      Caption = 'Copia nome da opera'#231#227'o para descri'#231#227'o do fluxo'
      OnClick = Copianomedaoperaoparadescriodofluxo1Click
    end
  end
  object QrDupl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM tintascab'
      'WHERE TxtUsu=:P0'
      '')
    Left = 436
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
end
