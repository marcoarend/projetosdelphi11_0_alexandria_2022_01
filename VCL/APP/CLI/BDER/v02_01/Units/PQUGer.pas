unit PQUGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, dmkDBGridZTO, frxClass,
  frxDBSet, mySQLDbTables, Vcl.Menus, Variants, dmkCheckGroup, UnAppEnums;

type
  TFmPQUGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    DsCI2A: TDataSource;
    QrCI: TMySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    DsDefPecas: TDataSource;
    QrDefPecas: TMySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrPQURec: TMySQLQuery;
    QrPQURecInsumo: TIntegerField;
    QrPQURecNomePQ: TWideStringField;
    QrPQURecCliInt: TIntegerField;
    QrPQURecNomeCI: TWideStringField;
    QrPQURecPesoUso: TFloatField;
    QrPQURecKgEstq: TFloatField;
    QrPQURecKgSaldoFut: TFloatField;
    QrPQURecKgLiqEmb: TFloatField;
    QrPQURecInsumM2PesoKg: TFloatField;
    QrPQURecIQ: TIntegerField;
    QrPQURecNOMEIQ: TWideStringField;
    QrPQUCab: TMySQLQuery;
    QrPQUCabCodigo: TIntegerField;
    QrPQUCabNome: TWideStringField;
    QrPQUCabAplicacao: TSmallintField;
    QrPQUCabOrdem: TIntegerField;
    QrPQUCabDtPedido: TDateField;
    QrPQUCabEmitGru: TSmallintField;
    QrPQUCabNO_EmitGru: TWideStringField;
    QrPQUCabCliInt: TIntegerField;
    QrPQUCabDtIniProd: TDateTimeField;
    QrPQUCabDtEntrega: TDateTimeField;
    QrPQUFrm1: TMySQLQuery;
    QrPQUFrm1NO_Grandeza: TWideStringField;
    QrPQUFrm1NOMERECEITA: TWideStringField;
    QrPQUFrm1UNIDADE: TWideStringField;
    QrPQUFrm1Codigo: TIntegerField;
    QrPQUFrm1Receita: TIntegerField;
    QrPQUFrm1DefPeca: TIntegerField;
    QrPQUFrm1Media: TFloatField;
    QrPQUFrm1Lk: TIntegerField;
    QrPQUFrm1DataCad: TDateField;
    QrPQUFrm1DataAlt: TDateField;
    QrPQUFrm1UserCad: TIntegerField;
    QrPQUFrm1UserAlt: TIntegerField;
    QrPQUFrm1Pecas1: TIntegerField;
    QrPQUFrm1Pecas2: TIntegerField;
    QrPQUFrm1Pecas3: TIntegerField;
    QrPQUFrm1Pecas4: TIntegerField;
    QrPQUFrm1Base1: TFloatField;
    QrPQUFrm1Base2: TFloatField;
    QrPQUFrm1Base3: TFloatField;
    QrPQUFrm1Base4: TFloatField;
    QrPQUFrm1KGT: TIntegerField;
    QrPQUFrm1DMais2: TIntegerField;
    QrPQUFrm1DMais3: TIntegerField;
    QrPQUFrm1DMais4: TIntegerField;
    QrPQUFrm1Controle: TIntegerField;
    QrPQUFrm1PedGrandeza: TIntegerField;
    QrPQUFrm1PedQtdeGdz: TFloatField;
    QrPQUFrm1PedLinhasCul: TFloatField;
    QrPQUFrm1PedLinhasCab: TFloatField;
    QrPQUFrm1PedAreaM2: TFloatField;
    QrPQUFrm1PedAreaP2: TFloatField;
    QrPQUFrm1PedPesoKg: TFloatField;
    QrPQUFrm1AlterWeb: TSmallintField;
    QrPQUFrm1AWServerID: TIntegerField;
    QrPQUFrm1AWStatSinc: TSmallintField;
    QrPQUFrm1Ativo: TSmallintField;
    QrProdz: TMySQLQuery;
    QrProdzNumero: TIntegerField;
    QrProdzPeso: TFloatField;
    QrProdzQtde: TFloatField;
    QrProdzAreaM2: TFloatField;
    QrProdzNome: TWideStringField;
    QrProdzMin_DataEmis: TWideStringField;
    DsPQUCab: TDataSource;
    DsPQUFrm1: TDataSource;
    DsProdz: TDataSource;
    QrPedPedEPQ: TMySQLQuery;
    QrPedPedEPQInsumo: TIntegerField;
    QrPedPedEPQPesoUso: TFloatField;
    QrPedPedEPQPedidoCod: TIntegerField;
    QrPedPedEPQNomePQ: TWideStringField;
    QrPedPedEPQNomeCI: TWideStringField;
    QrPedPedEPQPedidoTxt: TWideStringField;
    QrPedPedEPQCliInt: TIntegerField;
    QrPedPedEPQPedidoOrd: TIntegerField;
    QrPedPedEPQInsumM2PesoKg: TFloatField;
    QrPedPedEPQIQ: TIntegerField;
    QrPedPedEPQNOMEIQ: TWideStringField;
    QrPQUFrm2: TMySQLQuery;
    QrPQUFrm2NOMERECEITA: TWideStringField;
    QrPQUFrm2Codigo: TIntegerField;
    QrPQUFrm2Controle: TIntegerField;
    QrPQUFrm2Receita: TIntegerField;
    QrPQUFrm2DefPeca: TIntegerField;
    QrPQUFrm2Media: TFloatField;
    QrPQUFrm2Lk: TIntegerField;
    QrPQUFrm2DataCad: TDateField;
    QrPQUFrm2DataAlt: TDateField;
    QrPQUFrm2UserCad: TIntegerField;
    QrPQUFrm2UserAlt: TIntegerField;
    QrPQUFrm2Pecas1: TIntegerField;
    QrPQUFrm2Pecas2: TIntegerField;
    QrPQUFrm2Pecas3: TIntegerField;
    QrPQUFrm2Pecas4: TIntegerField;
    QrPQUFrm2Base1: TFloatField;
    QrPQUFrm2Base2: TFloatField;
    QrPQUFrm2Base3: TFloatField;
    QrPQUFrm2Base4: TFloatField;
    QrPQUFrm2KGT: TIntegerField;
    QrPQUFrm2DMais2: TIntegerField;
    QrPQUFrm2DMais3: TIntegerField;
    QrPQUFrm2DMais4: TIntegerField;
    QrPQUFrm2PedGrandeza: TIntegerField;
    QrPQUFrm2PedQtdeGdz: TFloatField;
    QrPQUFrm2PedLinhasCul: TFloatField;
    QrPQUFrm2PedLinhasCab: TFloatField;
    QrPQUFrm2PedAreaM2: TFloatField;
    QrPQUFrm2PedAreaP2: TFloatField;
    QrPQUFrm2PedPesoKg: TFloatField;
    QrPQUFrm2AlterWeb: TSmallintField;
    QrPQUFrm2AWServerID: TIntegerField;
    QrPQUFrm2AWStatSinc: TSmallintField;
    QrPQUFrm2Ativo: TSmallintField;
    QrPQUFrm2NO_Grandeza: TWideStringField;
    QrPQUFrm2PedQtPdGdz: TFloatField;
    QrPQUFrm2PedQtPrGdz: TFloatField;
    QrPQUFrm2PedPdArM2: TFloatField;
    QrPQUFrm2PedPdArP2: TFloatField;
    QrPQUFrm2PedPdPeKg: TFloatField;
    QrPQUFrm2PedPrArM2: TFloatField;
    QrPQUFrm2PedPrArP2: TFloatField;
    QrPQUFrm2PedPrPeKg: TFloatField;
    QrPQUFrm2RendEsperado: TFloatField;
    QrPQUFrm2MargemDBI: TFloatField;
    frxPedOpen: TfrxReport;
    frxDsPedOpen: TfrxDBDataset;
    QrPedComprar: TMySQLQuery;
    QrPedComprarInsumM2PesoKg: TFloatField;
    QrPedComprarCodigo: TIntegerField;
    QrPedComprarControle: TIntegerField;
    QrPedComprarInsumo: TIntegerField;
    QrPedComprarPesoEstq: TFloatField;
    QrPedComprarPesoUso: TFloatField;
    QrPedComprarPesoFut: TFloatField;
    QrPedComprarPesoNeed: TFloatField;
    QrPedComprarCliInt: TIntegerField;
    QrPedComprarRepet: TFloatField;
    QrPedComprarNomePQ: TWideStringField;
    QrPedComprarNomeCI: TWideStringField;
    QrPedComprarLk: TIntegerField;
    QrPedComprarDataCad: TDateField;
    QrPedComprarDataAlt: TDateField;
    QrPedComprarUserCad: TIntegerField;
    QrPedComprarUserAlt: TIntegerField;
    QrPedComprarAlterWeb: TSmallintField;
    QrPedComprarAWServerID: TIntegerField;
    QrPedComprarAWStatSinc: TSmallintField;
    QrPedComprarAtivo: TSmallintField;
    QrPedComprarNivel: TIntegerField;
    QrPedComprarKgLiqEmb: TFloatField;
    QrPedComprarPesoCompra: TFloatField;
    QrPedComprarIQ: TIntegerField;
    QrPedComprarNOMEIQ: TWideStringField;
    DsPQUFrm2: TDataSource;
    QrPedOpen: TMySQLQuery;
    QrPedOpenInsumM2PesoKg: TFloatField;
    QrPedOpenCodigo: TIntegerField;
    QrPedOpenControle: TIntegerField;
    QrPedOpenInsumo: TIntegerField;
    QrPedOpenPesoEstq: TFloatField;
    QrPedOpenPesoUso: TFloatField;
    QrPedOpenPesoFut: TFloatField;
    QrPedOpenPesoNeed: TFloatField;
    QrPedOpenCliInt: TIntegerField;
    QrPedOpenRepet: TFloatField;
    QrPedOpenNomePQ: TWideStringField;
    QrPedOpenNomeCI: TWideStringField;
    QrPedOpenLk: TIntegerField;
    QrPedOpenDataCad: TDateField;
    QrPedOpenDataAlt: TDateField;
    QrPedOpenUserCad: TIntegerField;
    QrPedOpenUserAlt: TIntegerField;
    QrPedOpenAlterWeb: TSmallintField;
    QrPedOpenAWServerID: TIntegerField;
    QrPedOpenAWStatSinc: TSmallintField;
    QrPedOpenAtivo: TSmallintField;
    QrPedOpenNivel: TIntegerField;
    QrPedOpenKgLiqEmb: TFloatField;
    QrPedOpenPesoCompra: TFloatField;
    QrPedOpenIQ: TIntegerField;
    QrPedOpenNOMEIQ: TWideStringField;
    frxDsPedComprar: TfrxDBDataset;
    frxDsPQUFrm2: TfrxDBDataset;
    frxPedSeq: TfrxReport;
    QrFormulas: TMySQLQuery;
    QrFormulasNumero: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrPedSeq: TMySQLQuery;
    QrPedSeqCodigo: TIntegerField;
    QrPedSeqControle: TIntegerField;
    QrPedSeqInsumo: TIntegerField;
    QrPedSeqPesoEstq: TFloatField;
    QrPedSeqPesoUso: TFloatField;
    QrPedSeqPesoFut: TFloatField;
    QrPedSeqPesoNeed: TFloatField;
    QrPedSeqCliInt: TIntegerField;
    QrPedSeqRepet: TFloatField;
    QrPedSeqNomePQ: TWideStringField;
    QrPedSeqNomeCI: TWideStringField;
    QrPedSeqLk: TIntegerField;
    QrPedSeqDataCad: TDateField;
    QrPedSeqDataAlt: TDateField;
    QrPedSeqUserCad: TIntegerField;
    QrPedSeqUserAlt: TIntegerField;
    QrPedSeqAlterWeb: TSmallintField;
    QrPedSeqAWServerID: TIntegerField;
    QrPedSeqAWStatSinc: TSmallintField;
    QrPedSeqAtivo: TSmallintField;
    QrPedSeqNivel: TIntegerField;
    QrPedSeqKgLiqEmb: TFloatField;
    QrPedSeqPesoCompra: TFloatField;
    QrPedSeqPedidoCod: TIntegerField;
    QrPedSeqPedidoTxt: TWideStringField;
    QrPedSeqPedidoOrd: TIntegerField;
    QrPedSeqDtPedido: TDateField;
    QrPedSeqInsumM2PercRetr: TFloatField;
    QrPedSeqInsumM2PedKgM2: TFloatField;
    QrPedSeqInsumM2PesoKg: TFloatField;
    QrPedSeqDtIniProd: TDateTimeField;
    QrPedSeqDtEntrega: TDateTimeField;
    DsFormulas: TDataSource;
    frxDsPedSeq: TfrxDBDataset;
    PCPrevisao: TPageControl;
    TabSheet12: TTabSheet;
    Panel5: TPanel;
    GradePrev: TdmkDBGridZTO;
    PnPrevAdd: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdReceita1: TdmkEditCB;
    CBReceita1: TdmkDBLookupComboBox;
    EdUnidade: TdmkEditCB;
    CBUnidade: TdmkDBLookupComboBox;
    EdQtde1: TdmkEdit;
    EdMedia: TdmkEdit;
    EdQtde2: TdmkEdit;
    EdQtde3: TdmkEdit;
    EdQtde4: TdmkEdit;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    StaticText1: TStaticText;
    GBConf1: TGroupBox;
    Panel19: TPanel;
    Panel20: TPanel;
    BtDesiste1: TBitBtn;
    BtConf1: TBitBtn;
    EdDMais2: TdmkEdit;
    EdDMais3: TdmkEdit;
    EdDMais4: TdmkEdit;
    Panel21: TPanel;
    Panel28: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    dmkDBGridZTO1: TdmkDBGridZTO;
    TabSheet13: TTabSheet;
    Panel27: TPanel;
    Splitter1: TSplitter;
    GradePedOpn: TdmkDBGridZTO;
    Panel31: TPanel;
    PnPrevPed: TPanel;
    BtPrevGrupo: TBitBtn;
    BtItemGrupo: TBitBtn;
    BtAtzProduzidos: TBitBtn;
    DBGPrevPed: TdmkDBGridZTO;
    TabSheet14: TTabSheet;
    Panel22: TPanel;
    Splitter2: TSplitter;
    Panel29: TPanel;
    GroupBox2: TGroupBox;
    Panel30: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    TPProdzIni: TdmkEditDateTimePicker;
    TPProdzFim: TdmkEditDateTimePicker;
    BtPesquisar: TBitBtn;
    DBGProdz: TdmkDBGridZTO;
    Panel32: TPanel;
    dmkDBGridZTO2: TdmkDBGridZTO;
    dmkDBGridZTO3: TdmkDBGridZTO;
    PMPrevGrupo: TPopupMenu;
    Incluinovogrupo1: TMenuItem;
    Manual1: TMenuItem;
    Incluicomplementodeproduo1: TMenuItem;
    Alteragruposelecionado1: TMenuItem;
    Excluigruposelecionado1: TMenuItem;
    Excluigrupoetodosseusitens1: TMenuItem;
    PMItemGrupo: TPopupMenu;
    Incluinovoitem1: TMenuItem;
    Alteraitemselecionado1: TMenuItem;
    Excluiitemselecionado1: TMenuItem;
    PB1: TProgressBar;
    PB2: TProgressBar;
    PB3: TProgressBar;
    QrRec: TMySQLQuery;
    QrRecClienteI: TIntegerField;
    QrRecProduto: TIntegerField;
    QrRecPorcent: TFloatField;
    QrRecCI: TIntegerField;
    QrRecEstSegur: TIntegerField;
    QrRecPeso: TFloatField;
    QrRecNOMEPQ: TWideStringField;
    QrRecNOMECI: TWideStringField;
    QrRecIQ: TIntegerField;
    QrRecNOMEIQ: TWideStringField;
    TabSheet1: TTabSheet;
    CGTipoCad: TdmkCheckGroup;
    frxDsPQUCab: TfrxDBDataset;
    QrPQUClc: TMySQLQuery;
    QrPQUClcInsumo: TIntegerField;
    QrPQUClcCliInt: TIntegerField;
    QrPQUClcddSegur: TIntegerField;
    QrPQUClcPeso: TFloatField;
    QrPQUClcNomePQ: TWideStringField;
    QrPQUClcNomeCI: TWideStringField;
    QrPQUClcddSegur_1: TIntegerField;
    QrPQUClcddSegur2: TIntegerField;
    QrPQUClcddSegur3: TIntegerField;
    QrPQUClcddSegur4: TIntegerField;
    QrPQUClcPeso1: TFloatField;
    QrPQUClcPeso2: TFloatField;
    QrPQUClcPeso3: TFloatField;
    QrPQUClcPeso4: TFloatField;
    QrPQUClcNeed1: TFloatField;
    QrPQUClcNeed2: TFloatField;
    QrPQUClcNeed3: TFloatField;
    QrPQUClcNeed4: TFloatField;
    QrPQUClcDias1: TFloatField;
    QrPQUClcDias2: TFloatField;
    QrPQUClcDias3: TFloatField;
    QrPQUClcDias4: TFloatField;
    frxDsPQUIts: TfrxDBDataset;
    frxPrevisao: TfrxReport;
    QrSumCab: TMySQLQuery;
    QrSumCabCodigo: TIntegerField;
    QrSumCabPedPdArM2: TFloatField;
    frxDsPQU: TfrxDBDataset;
    QrSumProd: TMySQLQuery;
    QrSumProdPeso: TFloatField;
    QrSumProdQtde: TFloatField;
    QrSumProdAreaM2: TFloatField;
    QrSumProdMediaM2Couro: TFloatField;
    QrSumProdDtIni: TDateTimeField;
    QrSumProdDtFim: TDateTimeField;
    QrSumProdSemanas: TFloatField;
    QrSumProdMedia5: TFloatField;
    DsSumProd: TDataSource;
    QrProducao: TMySQLQuery;
    QrProducaoSemana: TLargeintField;
    QrProducaoPeso: TFloatField;
    QrProducaoQtde: TFloatField;
    QrProducaoAreaM2: TFloatField;
    QrProducaoMediaM2Couro: TFloatField;
    QrProducaoQtFuloes: TLargeintField;
    QrProducaoMedia5: TFloatField;
    QrProducaoDiaSemana: TLargeintField;
    DsProducao: TDataSource;
    Panel6: TPanel;
    BtTodosTipCad: TBitBtn;
    BtNenhumTipCad: TBitBtn;
    QrPedComprarPesComprCarga: TFloatField;
    QrPedOpenPesComprCarga: TFloatField;
    QrPedSeqPesComprCarga: TFloatField;
    QrPQUCabQtProdDia: TFloatField;
    QrPQUCabPedQtPdGdz: TFloatField;
    QrPQUCabPedGrandeza: TIntegerField;
    QrSumCarga: TMySQLQuery;
    QrSumCargaCargaKgCompra: TFloatField;
    Panel7: TPanel;
    DBGPQURecCarga: TdmkDBGridZTO;
    QrPQURecCarga: TMySQLQuery;
    DsPQURecCarga: TDataSource;
    QrPQURecCargaNivel: TIntegerField;
    QrPQURecCargaCodigo: TIntegerField;
    QrPQURecCargaControle: TIntegerField;
    QrPQURecCargaInsumo: TIntegerField;
    QrPQURecCargaKgLiqEmb: TFloatField;
    QrPQURecCargaPesoEstq: TFloatField;
    QrPQURecCargaPesoUso: TFloatField;
    QrPQURecCargaPesoFut: TFloatField;
    QrPQURecCargaPesoNeed: TFloatField;
    QrPQURecCargaPesoCompra: TFloatField;
    QrPQURecCargaCliInt: TIntegerField;
    QrPQURecCargaRepet: TFloatField;
    QrPQURecCargaNomePQ: TWideStringField;
    QrPQURecCargaNomeCI: TWideStringField;
    QrPQURecCargaLk: TIntegerField;
    QrPQURecCargaDataCad: TDateField;
    QrPQURecCargaDataAlt: TDateField;
    QrPQURecCargaUserCad: TIntegerField;
    QrPQURecCargaUserAlt: TIntegerField;
    QrPQURecCargaAlterWeb: TSmallintField;
    QrPQURecCargaAWServerID: TIntegerField;
    QrPQURecCargaAWStatSinc: TSmallintField;
    QrPQURecCargaAtivo: TSmallintField;
    QrPQURecCargaPedidoCod: TIntegerField;
    QrPQURecCargaPedidoTxt: TWideStringField;
    QrPQURecCargaPedidoOrd: TIntegerField;
    QrPQURecCargaInsumM2PercRetr: TFloatField;
    QrPQURecCargaInsumM2PedKgM2: TFloatField;
    QrPQURecCargaInsumM2PesoKg: TFloatField;
    QrPQURecCargaAreaM2Pedido: TFloatField;
    QrPQURecCargaIQ: TIntegerField;
    QrPQURecCargaNomeIQ: TWideStringField;
    QrPQURecCargaQtEmbalagens: TFloatField;
    QrPQURecCargaPesComprCarga: TFloatField;
    QrPQURecCargaPesComprPreDef: TFloatField;
    QrPQURecCargaCargaKgUso: TFloatField;
    QrPQURecCargaCargaKgNeed: TFloatField;
    QrPQURecCargaCargaKgCompra: TFloatField;
    QrPQURecCargaCargaKgQtEmb: TFloatField;
    QrPQURecCargaCargaPercInc: TFloatField;
    QrPQURecCargaAplicacao: TIntegerField;
    Panel8: TPanel;
    LaCI: TLabel;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel9: TPanel;
    LaQtPedido: TLabel;
    EdCargaLiqTransp: TdmkEdit;
    BtPedItem: TBitBtn;
    BtCalculaCarga: TBitBtn;
    BtCalculaConsumo: TBitBtn;
    PMPedItem: TPopupMenu;
    Alteraitem1: TMenuItem;
    Incluiitemavulso1: TMenuItem;
    QrPedComprarPesoPedido: TFloatField;
    QrFornecedores: TMySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOME: TWideStringField;
    DsFornecedores: TDataSource;
    Label4: TLabel;
    EdIQ: TdmkEditCB;
    CBIQ: TdmkDBLookupComboBox;
    MeAvisos: TMemo;
    Usaclculoautomtico1: TMenuItem;
    EdIncremento: TdmkEdit;
    Label5: TLabel;
    dmkDBGridZTO4: TdmkDBGridZTO;
    QrUso: TMySQLQuery;
    DsUso: TDataSource;
    QrUsoFormula: TIntegerField;
    QrUsoPorcentUso: TFloatField;
    Splitter3: TSplitter;
    RGPQUCabSit: TRadioGroup;
    N1: TMenuItem;
    Abrejaneladocadastrodoinsumo1: TMenuItem;
    EdEmpresas: TdmkEdit;
    GroupBox3: TGroupBox;
    EdDdConsumo: TdmkEdit;
    EdDdCompra: TdmkEdit;
    Label8: TLabel;
    Label15: TLabel;
    QrAvulso: TMySQLQuery;
    QrAvulsoInsumo: TIntegerField;
    QrAvulsos: TMySQLQuery;
    QrAvulsosInsumo: TIntegerField;
    QrAvulsosNome: TWideStringField;
    QrAvulsosKgConsumo: TFloatField;
    QrAvulsosKg_dia: TFloatField;
    QrAvulsosKgEstq: TFloatField;
    QrAvulsosDiasEstq: TFloatField;
    QrAvulsosDiasPeriod: TIntegerField;
    QrAvulsosNecesPeriod: TFloatField;
    QrAvulsosComprar: TFloatField;
    QrAvulsosCI: TIntegerField;
    QrAvulsosIQ: TIntegerField;
    TabSheet2: TTabSheet;
    DBGAvulsos: TDBGrid;
    DsAvulsos: TDataSource;
    Memo1: TMemo;
    QrAvulsosNomeIQ: TWideStringField;
    QrAvulsosNomeCI: TWideStringField;
    QrAvulsosKgLiqEmb: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Manual1Click(Sender: TObject);
    procedure Incluicomplementodeproduo1Click(Sender: TObject);
    procedure Alteragruposelecionado1Click(Sender: TObject);
    procedure Excluigruposelecionado1Click(Sender: TObject);
    procedure Excluigrupoetodosseusitens1Click(Sender: TObject);
    procedure Incluinovoitem1Click(Sender: TObject);
    procedure Alteraitemselecionado1Click(Sender: TObject);
    procedure Excluiitemselecionado1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtConf1Click(Sender: TObject);
    procedure BtDesiste1Click(Sender: TObject);
    procedure BtPrevGrupoClick(Sender: TObject);
    procedure BtItemGrupoClick(Sender: TObject);
    procedure BtAtzProduzidosClick(Sender: TObject);
    procedure BtPesquisarClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdCIRedefinido(Sender: TObject);
    procedure frxPrevisaoGetValue(const VarName: string; var Value: Variant);
    procedure BtTodosTipCadClick(Sender: TObject);
    procedure BtNenhumTipCadClick(Sender: TObject);
    procedure QrPQUCabAfterScroll(DataSet: TDataSet);
    procedure QrPQUCabBeforeClose(DataSet: TDataSet);
    procedure QrPQUCabCalcFields(DataSet: TDataSet);
    procedure QrPQUFrm1CalcFields(DataSet: TDataSet);
    procedure DBGPQURecCargaDblClick(Sender: TObject);
    procedure BtPedItemClick(Sender: TObject);
    procedure BtCalculaCargaClick(Sender: TObject);
    procedure BtCalculaConsumoClick(Sender: TObject);
    procedure CGTipoCadClick(Sender: TObject);
    procedure CGTipoCadDblClick(Sender: TObject);
    procedure CGTipoCadExit(Sender: TObject);
    procedure Alteraitem1Click(Sender: TObject);
    procedure Incluiitemavulso1Click(Sender: TObject);
    procedure Usaclculoautomtico1Click(Sender: TObject);
    procedure QrPQURecCargaAfterScroll(DataSet: TDataSet);
    procedure QrPQURecCargaBeforeClose(DataSet: TDataSet);
    procedure dmkDBGridZTO4DblClick(Sender: TObject);
    procedure GradePedOpnDblClick(Sender: TObject);
    procedure DBGPrevPedDblClick(Sender: TObject);
    procedure RGPQUCabSitClick(Sender: TObject);
    procedure Abrejaneladocadastrodoinsumo1Click(Sender: TObject);
  private
    { Private declarations }
    FCI: Integer;
    FTC, FCINome: String;
    //
    procedure AlteraGrupo(Aplicacao: Integer);
    procedure ImprimePrevisao();
    //procedure ImprimePrvPedOpenSelected();
    procedure ImprimePrvPedOpenTodos();
    procedure ImprimePrvPedProducao();
    procedure IncluiGrupo(Aplicacao: Integer);
    procedure MostraEdicao1(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure MostraEdicao2(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure MostraFormPQUCab(SQLType: TSQLType);
    procedure MostraFormPQUCaH(SQLType: TSQLType);
    procedure MostraFormPQUFrm(SQLType: TSQLType);
    procedure DefineDadosSequencia(var Ordem: Integer; var M2Dia, CourosDia,
              KgDia: Double; var DataIniProd: TDateTime);
    procedure ReopenPQUCab(Codigo: Integer);
    procedure ReopenPQUFrm(Controle: Integer);
    procedure PreDefinePesoItem();
    procedure CalculaConsumoNivel2();
    procedure DefineFTC();
    procedure MostraFormPQURec(SQLType: TSQLType);
    procedure ReabreAvulsos();
    function  DefineCIs(): String;

  public
    { Public declarations }
    procedure ReopenQrPQURecCarga(Insumo: Integer);
  end;

  var
  FmPQUGer: TFmPQUGer;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, UnGOTOy, MyDBCheck,
  UnDmkProcFunc, UnPQ_PF, AppListas, UnApp_Jan,
  PQUCab, PQUCaH, PQUFrm, PQURec, ModuleGeral;

const
  CO_ProducaoDiaria = 1;
  CO_PedidoEmAberto = 2;
var
  VAR_LAST_Prev_Prev  : Integer = 0;
  VAR_LAST_Prev_PdOpn : Integer = 0;


{$R *.DFM}

procedure TFmPQUGer.Abrejaneladocadastrodoinsumo1Click(Sender: TObject);
begin
  App_Jan.MostraFormPQ(QrPQURecCargaInsumo.Value);
end;

procedure TFmPQUGer.AlteraGrupo(Aplicacao: Integer);
var
  Nome: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := stUpd;
  Codigo         := QrPQUCabCodigo.Value;
  Nome           := QrPQUCabNome.Value;
  if InputQuery('Altera Grupo', 'Informe o novo nome do grupo selecionado:', Nome) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqucab', False, [
    'Nome', 'Aplicacao'], [
    'Codigo'], [
    Nome, Aplicacao], [
    Codigo], True) then
      ReopenPQUCab(Codigo);
  end;
end;

procedure TFmPQUGer.Alteragruposelecionado1Click(Sender: TObject);
var
  PageIndex: Integer;
begin
  PageIndex := PCPrevisao.ActivePageIndex + 1;
  case PageIndex of
    CO_ProducaoDiaria: AlteraGrupo(PageIndex);
    CO_PedidoEmAberto: MostraFormPQUCab(stUpd);
    else Geral.MB_Aviso('"PCPrevisao.ActivePageIndex" n�o implementado (3)');
  end;
end;

procedure TFmPQUGer.Alteraitem1Click(Sender: TObject);
begin
  //PreDefinePesoItem();
  MostraFormPQURec(stUpd);
end;

procedure TFmPQUGer.Alteraitemselecionado1Click(Sender: TObject);
var
  PageIndex: Integer;
begin
  PageIndex := PCPrevisao.ActivePageIndex + 1;
  case PageIndex of
    CO_ProducaoDiaria: MostraEdicao1(1, stUpd, 0);
    CO_PedidoEmAberto: MostraFormPQUFrm(stUpd);
    else Geral.MB_Aviso('"PCPrevisao.ActivePageIndex" n�o implementado (4)');
  end;
end;

procedure TFmPQUGer.BitBtn1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPrevGrupo, BtPrevGrupo);
end;

procedure TFmPQUGer.BitBtn2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItemGrupo, BtItemGrupo);
end;

procedure TFmPQUGer.BtAtzProduzidosClick(Sender: TObject);
  function ObtemProducaoItemAtual(var PedQtPrGdz, ArM2ComRend, ArP2ComRend, PesoComRend: Double): Boolean;
  var
    Codigo, Controle, EmitGru, CliOrig, Formula, PedGrandeza: Integer;
    RendEsperado, LinhasCul, LinhasCab: Double;
    InMultiExec: Boolean;
    ErrTxt: String;
  begin
    Codigo       := QrPQUCabCodigo.Value;
    Controle     := QrPQUFrm2Controle.Value;
    EmitGru      := QrPQUCabEmitGru.Value;
    CliOrig      := QrPQUCabCliInt.Value;
    Formula      := QrPQUFrm2Receita.Value;
    RendEsperado := QrPQUFrm2RendEsperado.Value;
    PedGrandeza  := QrPQUFrm2PedGrandeza.Value;
    LinhasCul    := QrPQUFrm2PedLinhasCul.Value;
    LinhasCab    := QrPQUFrm2PedLinhasCab.Value;
    InMultiExec  := False;
    //
    Result := PQ_PF.CalculaProducaoDeItemDePedidoDePrevisaoDeCompraDeInsumos(Codigo,
    Controle, EmitGru, CliOrig, Formula, PedGrandeza, LinhasCul, LinhasCab,
    RendEsperado, InMultiExec, PedQtPrGdz, ArM2ComRend, ArP2ComRend, PesoComRend,
    ErrTxt);
  end;
  //
(*
  procedure SaldoQt();
  var
    PedQtdeGdz, PedQtPdGdz, PedQtPrGdz: Double;
  begin
    PedQtPdGdz := EdPedQtPdGdz.ValueVariant;
    PedQtPrGdz := EdPedQtPrGdz.ValueVariant;
    if PedQtPdGdz > PedQtPrGdz then
      PedQtdeGdz := PedQtPdGdz - PedQtPrGdz
    else
      PedQtdeGdz := 0;
    EdPedQtdeGdz.ValueVariant := PedQtdeGdz;
  end;
  //
*)
var
  Controle, PedGrandeza: Integer;
  PedQtdeGdz, PedLinhasCul, PedLinhasCab, PedAreaM2, PedAreaP2, PedPesoKg,
  PedQtPdGdz: Double;
  PedPdArM2, PedPdArP2, PedPdPeKg, PedPrArM2,
  PedPrArP2, PedPrPeKg: Double;
  //
  PedQtPrGdz, Old_PedQtPrGdz,
  ArM2ComRend, ArP2ComRend, PesoComRend: Double;
  ItensModificacdos: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    ItensModificacdos := 0;
    //
    PB1.Position := 0;
    PB1.Max := QrPQUCab.RecordCount;
    QrPQUCab.First;
    while not QrPQUCab.Eof do
    begin
      MyObjects.UpdPBOnly(PB1);
      PB2.Position := 0;
      PB2.Max := QrPQUCab.RecordCount;
      QrPQUFrm2.First;
      while not QrPQUFrm2.Eof do
      begin
        MyObjects.UpdPBOnly(PB2);
        Controle       := QrPQUFrm2Controle.Value;
(*
        if Controle = 32 then
          Geral.MB_Info('Controle = ' + Geral.FF0(Controle));
*)
        PedGrandeza    := QrPQUFrm2PedGrandeza.Value;
        PedQtdeGdz     := QrPQUFrm2PedQtdeGdz.Value;
        PedLinhasCul   := QrPQUFrm2PedLinhasCul.Value;
        PedLinhasCab   := QrPQUFrm2PedLinhasCab.Value;
        PedAreaM2      := QrPQUFrm2PedAreaM2.Value;
        PedAreaP2      := QrPQUFrm2PedAreaP2.Value;
        PedPesoKg      := QrPQUFrm2PedPesoKg.Value;
        PedQtPdGdz     := QrPQUFrm2PedQtPdGdz.Value;
        PedQtPrGdz     := QrPQUFrm2PedQtPrGdz.Value;
        //
        Old_PedQtPrGdz := PedQtPrGdz;
        //
        if ObtemProducaoItemAtual(PedQtPrGdz, ArM2ComRend, ArP2ComRend, PesoComRend) then
        begin
          //if Old_PedQtPrGdz <> PedQtPrGdz then
          begin
            PedPrArM2 := ArM2ComRend;
            PedPrArP2 := ArP2ComRend;
            PedPrPeKg := PesoComRend;
            //
            PedQtdeGdz := PedQtPdGdz - PedQtPrGdz;
            if PedQtdeGdz < 0 then
              PedQtdeGdz := 0;
            //
            if PQ_PF.CalculaDadosPrevConsPed(PedGrandeza, PedQtdeGdz, PedLinhasCul,
            PedLinhasCab, PedAreaM2, PedAreaP2, PedPesoKg, PedQtPdGdz, PedQtPrGdz,
            PedPdArM2, PedPdArP2, PedPdPeKg, PedPrArM2, PedPrArP2, PedPrPeKg) then
            begin
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pqufrm', False, [
              'PedAreaM2', 'PedAreaP2', 'PedPesoKg',
              'PedPrArM2', 'PedPrArP2', 'PedPrPeKg',
              'PedAreaM2', 'PedAreaP2', 'PedPesoKg',
              'PedQtPrGdz'], [
              'Controle'], [
              PedAreaM2, PedAreaP2, PedPesoKg,
              PedPrArM2, PedPrArP2, PedPrPeKg,
              PedAreaM2, PedAreaP2, PedPesoKg,
              PedQtPrGdz], [
              Controle], True) then
              begin
                ItensModificacdos := ItensModificacdos + 1;
              end;
            end;
          end;
          QrPQUFrm2.Next;
        end else
          QrPQUFrm2.Last;
      end;
      //
      QrPQUCab.Next;
    end;
    PB1.Position := 0;
    PB2.Position := 0;
  finally
    Screen.Cursor := crDefault;
    case ItensModificacdos of
      0: Geral.MB_Info('nenhum item foi modificado!');
      1: Geral.MB_Info('1 item foi modificado!');
      else Geral.MB_Info(IntToStr(ItensModificacdos) + ' itens foram modificados!');
    end;
  end;
end;

procedure TFmPQUGer.BtCalculaCargaClick(Sender: TObject);
const
  Aplicacao = Integer(TPQUAplicacao.pquaplicRecurtimento);
var
  CargaLiqTransp, CargaKgCompra, Incremento, StepInc: Double;
  OK: Boolean;
  Sinal, IQ: Integer;
  SQL_Inc, SQL_IQ: String;
  //
  procedure Executa();
  begin
    SQL_Inc    := Geral.FFT_Dot(1 + (Incremento / 100), 2, siNegativo);
    //
    Dmod.MyDB.Execute(Geral.ATS([
    'UPDATE pqurec  ',
    'SET CargaKgUso=PesoUso*' + SQL_Inc +',  ',
    'CargaPercInc=' + Geral.FFT_Dot(Incremento, 2, siNegativo),
    'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
    'AND Nivel=2; ',
    ' ',
    'UPDATE pqurec  ',
    'SET CargaKgNeed=CargaKgUso-PesoEstq ',
    'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
    'AND Nivel=2; ',
    ' ',
    'UPDATE pqurec  ',
    'SET CargaKgCompra=CargaKgNeed, ',
    'PesoPedido=CargaKgNeed, ',
    'CargaKgQtEmb=CargaKgNeed ',
    'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
    'AND Nivel=2 ',
    'AND PesComprPreDef=0 ',
    'AND CargaKgNeed>0 ',
    'AND KgLiqEmb<=0; ',
    ' ',
    'UPDATE pqurec  ',
    'SET CargaKgCompra=(CEILING( ',
    '  CargaKgNeed/KgLiqEmb) * KgLiqEmb ',
    '  ), ',
    'PesoPedido=(CEILING( ',
    '  CargaKgNeed/KgLiqEmb) * KgLiqEmb ',
    '  ), ',
    'CargaKgQtEmb=CEILING(CargaKgNeed/KgLiqEmb) ',
    'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
    'AND Nivel=2 ',
    'AND PesComprPreDef=0 ',
    'AND CargaKgNeed>0 ',
    'AND KgLiqEmb>0; ',
    '']));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumCarga, Dmod.MyDB, [
    'SELECT SUM(CargaKgCompra) CargaKgCompra ',
    'FROM pqurec  ',
    'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
    'AND Nivel=2 ',
    SQL_IQ,
    '']);
    CargaKgCompra := QrSumCargaCargaKgCompra.Value;
    //ShowMessage(Geral.FFT(CargaKgCompra, 2, siNegativo));
    Application.ProcessMessages;
    MeAvisos.Text := 'C�lculo ' + Geral.FFT(Incremento, 2, siNegativo) + ': ' +
      Geral.FFT(CargaKgCompra, 3, siNegativo) + ' kg' + sLineBreak + MeAvisos.Text;
    Application.ProcessMessages;
  end;
begin
  CargaLiqTransp := EdCargaLiqTransp.ValueVariant;
  if CargaLiqTransp <= 0 then Exit;
  //
  IQ := EdIQ.ValueVariant;
  if IQ <> 0 then
    SQL_IQ := 'AND IQ=' + Geral.FF0(IQ)
  else
    SQL_IQ := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumCarga, Dmod.MyDB, [
  'SELECT SUM(IF(PesComprPreDef > 0, PesComprPreDef, PesoCompra)) CargaKgCompra ',
  'FROM pqurec ',
  'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
  'AND Nivel=2 ',
  SQL_IQ,
  '']);
  CargaKgCompra := QrSumCargaCargaKgCompra.Value;
  OK := False;
  if CargaLiqTransp > CargaKgCompra then
    Sinal := 1
  else
    Sinal := -1;
  StepInc := EdIncremento.ValueVariant;
  if StepInc = 0 then
    StepInc := 1;
  //
  Incremento := 0;
  while not OK do
  begin
    Incremento := Incremento + (StepInc * sinal);
    //
    if (Incremento = 100) or (Incremento = -100) then
    begin
      OK := True;
    end else
    begin
      Executa();
      //
      if sinal = 1 then
      begin
        OK := CargaKgCompra >= CargaLiqTransp;
        if CargaKgCompra > CargaLiqTransp then
        begin
          Incremento := Incremento - StepInc;
          Executa();
        end;
      end else
      begin
        OK := CargaKgCompra <= CargaLiqTransp;
        if CargaKgCompra < CargaLiqTransp then
        begin
          Incremento := Incremento + StepInc;
          Executa();
        end;
      end;
    end;
  end;
  ReopenQrPQURecCarga(0);
end;

procedure TFmPQUGer.BtCalculaConsumoClick(Sender: TObject);
begin
  BtCalculaConsumo.Enabled := False;
  BtOK.Enabled := False;
  Screen.Cursor := crHourGlass;
  try
    CalculaConsumoNivel2();
    BtOK.Enabled := True;
  finally
    Screen.Cursor := crDefault;
    BtCalculaConsumo.Enabled := True;
  end;
end;

procedure TFmPQUGer.BtConf1Click(Sender: TObject);
var
  Codigo, Receita, DefPeca, Pecas1, Pecas2, Pecas3, Pecas4, DMais2, DMais3,
  DMais4, Controle: Integer;
  Media, Base1, Base2, Base3, Base4: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := QrPQUCabCodigo.Value;
  Controle       := QrPQUFrm1Controle.Value;
  Receita        := EdReceita1.ValueVariant;
  DefPeca        := EdUnidade.ValueVariant;
  Pecas1         := EdQtde1.ValueVariant;
  Pecas2         := EdQtde2.ValueVariant;
  Pecas3         := EdQtde3.ValueVariant;
  Pecas4         := EdQtde4.ValueVariant;
  Media          := EdMedia.ValueVariant;
  Base1          := Pecas1 * Media;
  Base2          := Pecas2 * Media;
  Base3          := Pecas3 * Media;
  Base4          := Pecas4 * Media;
  DMais2         := EdDMais2.ValueVariant;
  DMais3         := EdDMais3.ValueVariant;
  DMais4         := EdDMais4.ValueVariant;
  ///
  if MyObjects.FIC(Receita = 0, EdReceita1, 'Defina a receita!') then Exit;
  if MyObjects.FIC(DefPeca = 0, EdUnidade, 'Defina a unidade!') then Exit;
  {
  CliInt := Geral.IMV(EdCliInt.Text);
  if CliInt = 0 then
  begin
    Geral.MB_Aviso('Defina o cliente interno!');
    EdCliInt.SetFocus;
    Exit;
  end;
  }

  // Parei aqui fazer por cliente interno 2008 05 05

  //Controle := UMyMod.BuscaEmLivreY_Def('pqufrm', 'Controle', SQLType, Controle);
  Controle := UMyMod.BPGS1I32('pqufrm', 'Controle', '', '', tsPos, SQLType, Controle);
  Codigo := QrPQUCabCodigo.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqufrm', False, [
  'Receita', 'DefPeca', 'Pecas1',
  'Pecas2', 'Pecas3', 'Pecas4',
  'Media', 'Base1', 'Base2',
  'Base3', 'Base4', 'DMais2',
  'DMais3', 'DMais4', 'Codigo'], [
  'Controle'], [
  Receita, DefPeca, Pecas1,
  Pecas2, Pecas3, Pecas4,
  Media, Base1, Base2,
  Base3, Base4, DMais2,
  DMais3, DMais4, Codigo], [
  Controle], True) then
  begin
{
  if ImgTipo.SQLType = stIns then
  begin
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'PQU', 'PQU', 'Codigo');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO pqu SET ');
  end else begin
    Codigo := QrPQUFrm1Codigo.Value;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE pqu SET ');
  end;
  Dmod.QrUpd.SQL.Add('Receita=:P0, DefPeca=:P1, Pecas1=:P2, Pecas2=:P3, ');
  Dmod.QrUpd.SQL.Add('Pecas3=:P4, Pecas4=:P5, Media=:P6, Base1=:P7, Base2=:P8, ');
  Dmod.QrUpd.SQL.Add('Base3=:P9, Base4=:P10 ');
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa')
  else Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  Dmod.QrUpd.Params[00].AsInteger := Receita;
  Dmod.QrUpd.Params[01].AsInteger := DefPeca;
  Dmod.QrUpd.Params[02].AsInteger := Pecas1;
  Dmod.QrUpd.Params[03].AsInteger := Pecas2;
  Dmod.QrUpd.Params[04].AsInteger := Pecas3;
  Dmod.QrUpd.Params[05].AsInteger := Pecas4;
  Dmod.QrUpd.Params[06].AsFloat   := Media;
  Dmod.QrUpd.Params[07].AsFloat   := Base1;
  Dmod.QrUpd.Params[08].AsFloat   := Base2;
  Dmod.QrUpd.Params[09].AsFloat   := Base3;
  Dmod.QrUpd.Params[10].AsFloat   := Base4;
  Dmod.QrUpd.Params[11].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
}
{
var
  Codigo, Receita, DefPeca, Pecas1, Pecas2, Pecas3, Pecas4, AWServerID, AWStatSinc, DMais2, DMais3, DMais4: Integer;
  Media, Base1, Base2, Base3, Base4: Double;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType?;
  Codigo         := ;
  Receita        := ;
  DefPeca        := ;
  Pecas1         := ;
  Pecas2         := ;
  Pecas3         := ;
  Pecas4         := ;
  Media          := ;
  Base1          := ;
  Base2          := ;
  Base3          := ;
  Base4          := ;
  AWServerID     := ;
  AWStatSinc     := ;
  DMais2         := ;
  DMais3         := ;
  DMais4         := ;

  //
? := UMyMod.BuscaEmLivreY_Def('pqu', 'Codigo', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('pqu', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'pqu', auto_increment?[
'Receita', 'DefPeca', 'Pecas1',
'Pecas2', 'Pecas3', 'Pecas4',
'Media', 'Base1', 'Base2',
'Base3', 'Base4', 'AWServerID',
'AWStatSinc', 'DMais2', 'DMais3',
'DMais4'], [
'Codigo'], [
Receita, DefPeca, Pecas1,
Pecas2, Pecas3, Pecas4,
Media, Base1, Base2,
Base3, Base4, AWServerID,
AWStatSinc, DMais2, DMais3,
DMais4], [
Codigo], UserDataAlterweb?, IGNORE?
}
    PnPrevAdd.Visible := False;
    GradePrev.Visible := True;
    //
    MostraEdicao1(0, stLok, 0);
    ReopenPQUFrm(0);
  end;
end;

procedure TFmPQUGer.BtDesiste1Click(Sender: TObject);
begin
  MostraEdicao1(0, stLok, 0);
end;

procedure TFmPQUGer.BtItemGrupoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItemGrupo, BtItemGrupo);
end;

procedure TFmPQUGer.BtNenhumTipCadClick(Sender: TObject);
begin
  CGTipoCad.Value := 0;
end;

procedure TFmPQUGer.BtOKClick(Sender: TObject);
var
  I, K: Integer;
begin
  if MyObjects.FIC(FCI = 0, EdCI, 'Defina o cliente interno!') then Exit;
  //
  try
    QrPQUCab.DisableControls;
    QrPQUFrm2.DisableControls;
    //
    case PCPrevisao.ActivePageIndex of
     0: ImprimePrevisao();
     //1: ImprimePrvPedOpenSelected();
     1: ImprimePrvPedOpenTodos();
     2: ImprimePrvPedProducao();
     else Geral.MB_Erro('Sub-relat�rio de previs�o n�o definido!');
    end;
  finally
    QrPQUCab.EnableControls;
    QrPQUFrm2.EnableControls;
  end;
end;

procedure TFmPQUGer.BtPesquisarClick(Sender: TObject);
var
  SQL_Periodo: String;
begin
  SQL_Periodo := dmkPF.SQL_Periodo('AND DataEmis ',
    TPProdzIni.Date, TPProdzFim.Date, True, True);
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrProdz, Dmod.MyDB, [
  'SELECT emi.Numero, SUM(emi.Peso) Peso,  ',
  'SUM(emi.Qtde) Qtde, SUM(emi.AreaM2) AreaM2,  ',
  'rec.Nome, DATE_FORMAT(MIN(DataEmis), "%Y/%m/%d") Min_DataEmis  ',
  'FROM ' + TMeuDB + '.emit emi ',
  'LEFT JOIN ' + TMeuDB + '.listasetores lse ON lse.Codigo=emi.Setor ',
  'LEFT JOIN ' + TMeuDB + '.formulas rec ON rec.Numero=emi.Numero ',
  'WHERE lse.TpReceita=2 ',
  //'AND DataEmis BETWEEN "2020-06-29" AND "2020-08-29" ',
  SQL_Periodo,
  'GROUP BY emi.Numero ',
  'ORDER BY Nome ',
  '']);
  //
end;

procedure TFmPQUGer.BtPedItemClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPedItem, BtPedItem);
end;

procedure TFmPQUGer.BtPrevGrupoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPrevGrupo, BtPrevGrupo);
end;

procedure TFmPQUGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQUGer.BtTodosTipCadClick(Sender: TObject);
begin
  CGTipoCad.SetMaxValue;
end;

procedure TFmPQUGer.CalculaConsumoNivel2();
const
  Aplicacao = Integer(TPQUAplicacao.pquaplicRecurtimento);
  Nivel1    = 1;
  Nivel2    = 2;
  Nivel3    = 3;
var
  NomePQ, NomeCI, NomeIQ: String;
  Codigo, Controle, Insumo, CliInt, IQ: Integer;
  PesoEstq, PesoUso, PesoFut, PesoNeed, Repet, KgLiqEmb, PesoCompra, PesoPedido:
  Double;
  SQLType: TSQLType;
  ArrEstoqueCod: array of Integer;
  ArrEstoquePes: array of Double;
  I, PedidoOrd, PedidoCod, Acao, QtEmbalagens, Formula: Integer;
  xTeste, PedidoTxt: String;
  InsumM2PercRetr, InsumM2PesoKg, InsumM2PedKgM2, AreaM2Pedido, PedPdArM2,
  PorcentUso: Double;
  Calcula: Boolean;
begin
  Acao := MyObjects.SelRadioGroup('A��o', 'Selecione a a��o desejada:', [
  'Recalcular tudo',
  'Recalcular apenas itens de pedido com couro a produzir'
  ], 1(*Colunas*), -1(*Default*), True(*SelOnClick*));
  //
  //
  SQLType        := stIns;
  Codigo         := 0;
  Controle       := 0;
  Insumo         := 0;
  PesoEstq       := 0;
  PesoUso        := 0;
  PesoFut        := 0;
  PesoNeed       := 0;
  CliInt         := 0;
  Repet          := 0;
  NomePQ         := '';
  NomeCI         := '';
  QtEmbalagens   := 0;
  if (Acao in [0, 1]) then
  begin
    if MyObjects.FIC(FCI = 0, EdCI, 'Defina o cliente interno!') then
      Exit;
    PB1.Position := 0;
    PB2.Position := 0;
    PB3.Position := 0;
    PB1.Max := 4;
    Dmod.MyDB.Execute('DELETE FROM pqurec');
    //
    QrPQUCab.First;
    //
    while not QrPQUCab.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumCab, Dmod.MyDB, [
      'SELECT frm.Codigo, SUM(frm.PedPdArM2) PedPdArM2  ',
      'FROM pqufrm frm ',
      'WHERE frm.Codigo=' + Geral.FF0(QrPQUCabCodigo.Value),
      '']);
      //
      PedPdArM2 := QrSumCabPedPdArM2.Value;
      PB2.Max := QrPQUFrm2.RecordCount;
      QrPQUFrm2.First;
      while not QrPQUFrm2.Eof do
      begin
        MyObjects.UpdPB(PB2, Label1, Label2);
        //
        Formula := QrPQUFrm2Receita.Value;
        //
        if Acao = 1 then
          Calcula := QrPQUFrm2PedPesoKg.Value > 0
        else
          Calcula := True;
        if Calcula then
        begin
          Codigo := QrPQUFrm2Codigo.Value;
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QrRec, Dmod.MyDB, [
          'SELECT foc.ClienteI, foi.Produto, SUM(foi.Porcent) Porcent,',
          'pqc.CI, pqc.EstSegur, pqc.Peso, pq_.Nome NOMEPQ, ',
          'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECI, ',
          'pq_.IQ, IF(fop.Tipo=0, fop.RazaoSocial, fop.Nome) NOMEIQ ',
          'FROM formulasits foi',
          'LEFT JOIN formulas foc ON foc.Numero=foi.Numero',
          'LEFT JOIN pq pq_ ON pq_.Codigo=foi.Produto',
          'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI=' + Geral.FF0(FCI),
          //'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI IN (' + DefineCIs() + ')',
          'LEFT JOIN entidades ent ON ent.Codigo=foc.ClienteI',
          'LEFT JOIN entidades fop ON fop.Codigo=pq_.IQ',
          'WHERE pq_.GGXNiv2 IN (' + FTC + ') ',
          //'WHERE pq_.Codigo > 0 ',
          'AND foi.Numero=' + Geral.FF0(QrPQUFrm2Receita.Value),
          //'GROUP BY foi.Produto, pqc.CI',
          'AND foi.Produto > 0 ',
          'GROUP BY foi.Produto, foc.ClienteI',
          '']);
          //
          PB3.Position := 0;
          PB3.Max := QrRec.RecordCount;
          while not QrRec.Eof do
          begin
            PB3.Position := PB3.Position + 1;
            PB3.Update;
            Application.ProcessMessages;
            if QrRecPorcent.Value > 0 then
            begin
              CliInt      := QrRecClienteI.Value;
              NomePQ      := QrRecNOMEPQ.Value;
              NomeCI      := QrRecNOMECI.Value;
              Insumo      := QrRecProduto.Value;
              NomeIQ      := QrRecNOMEIQ.Value;
              IQ          := QrRecIQ.Value;
              PorcentUso  := QrRecPorcent.Value;
              //
              PesoUso     := PorcentUso * QrPQUFrm2PedPesoKg.Value / 100;
              //
              //
              InsumM2PercRetr := QrPQUFrm2MargemDBI.Value;//50; //%
              if QrPQUFrm2PedPdArM2.Value > 0 then
                InsumM2PedKgM2  := QrPQUFrm2PedPdPeKg.Value / QrPQUFrm2PedPdArM2.Value
              else
                InsumM2PedKgM2  := 0;
              //
          (*
              if InsumM2PedKgM2 > 0 then
                InsumM2PesoKg   :=  (PesoUso / InsumM2PedKgM2) * (InsumM2PercRetr / 100)
          *)
              if PedPdArM2 > 0 then
                InsumM2PesoKg   :=  (PesoUso / PedPdArM2) * (1 + (InsumM2PercRetr / 100))
              else
                InsumM2PesoKg   := 0;
              //
              AreaM2Pedido := QrPQUFrm2PedPdArM2.Value;
              //
              if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqurec', False, [
              'Codigo', 'Controle', 'Insumo',
              'PesoEstq', 'PesoUso', 'PesoFut',
              'PesoNeed', 'CliInt', 'Repet',
              'NomePQ', 'NomeCI', 'Nivel',
              'PedidoCod', 'PedidoTxt', 'PedidoOrd',
              'InsumM2PercRetr', 'InsumM2PedKgM2', 'InsumM2PesoKg',
              'AreaM2Pedido', 'IQ', 'NomeIQ',
              'Aplicacao', 'Formula', 'PorcentUso'], [
              ], [
              Codigo, Controle, Insumo,
              PesoEstq, PesoUso, PesoFut,
              PesoNeed, CliInt, Repet,
              NomePQ, NomeCI, Nivel1,
              QrPQUCabCodigo.Value, QrPQUCabNome.Value, QrPQUCabOrdem.Value,
              InsumM2PercRetr, InsumM2PedKgM2, InsumM2PesoKg,
              AreaM2Pedido, IQ, NomeIQ,
              Aplicacao, Formula, PorcentUso], [
              ], False) then
              begin
              //
              end;
            end;
            //
            QrRec.Next;
          end;
        end; // if calcula
        QrPQUFrm2.Next;
      end;
      QrPQUCab.Next;
    end;


    // ini 2023-05-25
////////////////////////////////////////////////////////////////////////////////
    // desabilitado em 2023-12-08 - Vinicius do noroeste est� baixando
    // indevidamente produtos no 150 e 190!
////////////////////////////////////////////////////////////////////////////////
{
    ReabreAvulsos();
    Memo1.Lines.Clear;
    QrAvulsos.First;
    while not QrAvulsos.Eof do
    begin
      (*
      UnDmkDAC_PF.AbreMySQLQuery0(QrAvulso, Dmod.MyDB, [
      'SELECT Insumo ',
      'FROM pqurec ',
      'WHERE Insumo=' + Geral.FF0(QrAvulsosInsumo.Value),
      '']);
      if QrAvulso.RecordCount > 0 then
      begin
        Memo1.Lines.Add('Insumo j� calculado e presente no avulso:' + Geral.FF0(QrAvulsosInsumo.Value));
      end else
      *)
      begin
        Codigo    := 0;
        Controle  := 0;
        Insumo    := QrAvulsosInsumo.Value;
        PesoEstq  := QrAvulsosKgEstq.Value;
        PesoUso   := ABS(QrAvulsosKgConsumo.Value);
        PesoFut   := PesoEstq - PesoUso;
        PesoNeed  := QrAvulsosNecesPeriod.Value;
        CliInt     := QrAvulsosCI.Value;
        Repet      := 0;
        NomePQ     := QrAvulsosNome.Value;
        NomeCI     := QrAvulsosNomeCI.Value;
        NomeIQ     := QrAvulsosNomeIQ.Value;
        IQ         := QrAvulsosIQ.Value;
        //Nivel      := 2;
        PedidoCod  := 0;
        PedidoTxt  := '';
        PedidoOrd  := 0;
        InsumM2PercRetr := 0;
        InsumM2PedKgM2  := 0;
        InsumM2PesoKg   := 0;
        AreaM2Pedido    := 0;
        //Aplicacao  :=  Integer(TPQUAplicacao.pquaplicRecurtimento);
        Formula    := 0;
        PorcentUso  := 0;
        KgLiqEmb    := QrAvulsosKgLiqEmb.Value;
        if KgLiqEmb > 0 then
        begin
          QtEmbalagens := Trunc((PesoNeed / KgLiqEmb) + 0.999999999999999);
          PesoCompra   := QtEmbalagens * KgLiqEmb;
        end else
        begin
          QtEmbalagens := 0;
          PesoCompra   := PesoNeed;
        end;
        //PesoCompra := QrAvulsosComprar.Value;
        PesoPedido := PesoCompra;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqurec', False, [
        'Codigo', 'Controle', 'Insumo',
        'PesoEstq', 'PesoUso', 'PesoFut',
        'PesoNeed', 'CliInt', 'Repet',
        'NomePQ', 'NomeCI', 'Nivel',
        'PedidoCod', 'PedidoTxt', 'PedidoOrd',
        'InsumM2PercRetr', 'InsumM2PedKgM2', 'InsumM2PesoKg',
        'AreaM2Pedido', 'IQ', 'NomeIQ',
        'Aplicacao', 'Formula', 'PorcentUso',
        'PesoPedido'], [
        ], [
        Codigo, Controle, Insumo,
        PesoEstq, PesoUso, PesoFut,
        PesoNeed, CliInt, Repet,
        NomePQ, NomeCI, Nivel2,
        PedidoCod, PedidoTxt, PedidoOrd,
        InsumM2PercRetr, InsumM2PedKgM2, InsumM2PesoKg,
        AreaM2Pedido, IQ, NomeIQ,
        Aplicacao, Formula, PorcentUso,
        PesoPedido], [
        ], False) then
        begin
        //
        end;
      end;
      QrAvulsos.Next;
    end;
      // fim 2023-05-25
}


    PB1.Position := 1;
    PB2.Position := 0;
    PB3.Position := 0;
    //
    if Trim(EdEmpresas.Text) <> EmptyStr then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DModG.MyPID_DB, [
      'DROP TABLE IF EXISTS _pqcli_; ',
      'CREATE TABLE _pqcli_ ',
      'SELECT PQ, SUM(Peso) Peso,  ',
      'SUM(KgLiqEmb) / SUM(IF(KgLiqEmb>0, 1, 0)) KgLiqEmb ',
      'FROM ' + TMeuDB + '.pqcli ',
      'WHERE CI IN (' + EdEmpresas.Text + ') ',
      'GROUP BY PQ; ',
      '']);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQURec, DmodG.MyPID_DB, [
      'SELECT rec.Insumo, rec.NomePQ, rec.CliInt, ',
      'rec.NomeCI, pqc.Peso KgEstq, ',

      //'pqc.peso - SUM(rec.PesoUso) KgSaldoFut,',

      'IF(pqc.Peso IS NULL ,0 - SUM(rec.PesoUso), ',
      'pqc.peso - SUM(rec.PesoUso)) KgSaldoFut,',

      'SUM(rec.PesoUso) PesoUso, pqc.KgLiqEmb, ',
      'SUM(rec.InsumM2PesoKg) InsumM2PesoKg, ',


      'rec.IQ, rec.NomeIQ ',

      'FROM ' + TMeuDB + '.pqurec rec',
      'LEFT JOIN _pqcli_ pqc ON pqc.PQ=rec.Insumo ',
      //'          AND pqc.CI=rec.CliInt',
      'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
      'AND Nivel=' + Geral.FF0(Nivel1),
      'GROUP BY rec.Insumo, rec.CliInt ',
      '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQURec, Dmod.MyDB, [
      'SELECT rec.Insumo, rec.NomePQ, rec.CliInt, ',
      'rec.NomeCI, pqc.Peso KgEstq, ',

      //'pqc.peso - SUM(rec.PesoUso) KgSaldoFut,',

      'IF(pqc.Peso IS NULL ,0 - SUM(rec.PesoUso), ',
      'pqc.peso - SUM(rec.PesoUso)) KgSaldoFut,',

      'SUM(rec.PesoUso) PesoUso, pqc.KgLiqEmb, ',
      'SUM(rec.InsumM2PesoKg) InsumM2PesoKg, ',


      'rec.IQ, rec.NomeIQ ',

      //'FROM .pqurec rec',
      'FROM pqurec rec',
      'LEFT JOIN pqcli pqc ON pqc.PQ=rec.Insumo ',
      '          AND pqc.CI=rec.CliInt',
      'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
      'AND Nivel=' + Geral.FF0(Nivel1),
      'GROUP BY rec.Insumo, rec.CliInt ',
      '']);
    end;
    //Geral.MB_Teste(QrPQURec.SQL.Text);
    PB2.Max := QrPQURec.RecordCount;
    QrPQURec.First;
    while not QrPQURec.Eof do
    begin
      MyObjects.UpdPBOnly(PB2);
      //
      Codigo     := 0;
      Controle   := 0;
      Insumo     := QrPQURecInsumo.Value;
      PesoEstq   := QrPQURecKgEstq.Value;
      PesoUso    := QrPQURecPesoUso.Value;
      if QrPQURecKgSaldoFut.Value > 0 then
      begin
        PesoFut   := QrPQURecKgSaldoFut.Value;
        PesoNeed  := 0;
        Repet     := PesoFut / PesoUso;
        if Repet < 0.00001 then
          Repet := 0;
        if Repet > 9999999999 then
          Repet := 9999999999;
      end else
      begin
        PesoFut   := 0;
        PesoNeed  := -QrPQURecKgSaldoFut.Value;
        Repet     := 0;
      end;
      KgLiqEmb    := QrPQURecKgLiqEmb.Value;
      if KgLiqEmb > 0 then
      begin
        QtEmbalagens := Trunc((PesoNeed / KgLiqEmb) + 0.999999999999999);
        PesoCompra   := QtEmbalagens * KgLiqEmb;
      end else
      begin
        QtEmbalagens := 0;
        PesoCompra   := PesoNeed;
      end;
      CliInt      := QrPQURecCliInt.Value;
      NomePQ      := QrPQURecNomePQ.Value;
      NomeCI      := QrPQURecNomeCI.Value;
      NomeIQ      := QrPQURecNOMEIQ.Value;
      IQ          := QrPQURecIQ.Value;
      PesoPedido  := PesoCompra;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqurec', False, [
      'Codigo', 'Controle', 'Insumo',
      'PesoEstq', 'PesoUso', 'PesoFut',
      'PesoNeed', 'CliInt', 'Repet',
      'NomePQ', 'NomeCI', 'Nivel',
      'KgLiqEmb', 'PesoCompra',
      'IQ', 'NomeIQ', 'QtEmbalagens',
      'Aplicacao', 'PesoPedido'], [
      ], [
      Codigo, Controle, Insumo,
      PesoEstq, PesoUso, PesoFut,
      PesoNeed, CliInt, Repet,
      NomePQ, NomeCI, Nivel2,
      KgLiqEmb, PesoCompra,
      IQ, NomeIQ, QtEmbalagens,
      Aplicacao, PesoPedido], [
      ], False) then
      begin
      //
      end;

      //
      QrPQURec.Next;
    end;
    PB1.Position := 2;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPedOpen, Dmod.MyDB, [
    'SELECT *',
    'FROM pqurec rec',
    'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
    'AND Nivel=2',
    'ORDER BY NomeCI, NomeIQ, NOMEPQ',
    '']);
    PB2.Max := QrPedOpen.RecordCount;
    PB2.Position := 0;
    //
    //MyObjects.frxMostra(frxPedOpen, 'Previs�o para Compra de Insumos Qu�micos por Pedidos Abertos');
    //
    SetLength(ArrEstoqueCod, QrPedOpen.RecordCount);
    SetLength(ArrEstoquePes, QrPedOpen.RecordCount);
    QrPedOpen.First;
    while not QrPedOpen.Eof do
    begin
      MyObjects.UpdPBOnly(PB2);
      //
      ArrEstoqueCod[QrPedOpen.RecNo -1] := QrPedOpenInsumo.Value;
      ArrEstoquePes[QrPedOpen.RecNo -1] := QrPedOpenPesoEstq.Value;
      //
      QrPedOpen.Next;
    end;
    PB1.Position := 3;

    UnDmkDAC_PF.AbreMySQLQuery0(QrPedPedEPQ, Dmod.MyDB, [
    'SELECT cab.DtIniProd, cab.DtPedido, rec.PedidoOrd, ',
    'rec.PedidoCod, rec.PedidoTxt, rec.CliInt, rec.Insumo, ',
    'rec.NomePQ, rec.NomeCI, rec.PedidoOrd, ',
    'SUM(rec.PesoUso) PesoUso, ',
    'SUM(InsumM2PesoKg) InsumM2PesoKg, ',
    'AreaM2Pedido, IQ, NomeIQ ',
    'FROM pqurec rec',
    'LEFT JOIN pqucab cab ON cab.Codigo=rec.Codigo',
    'WHERE rec.Aplicacao=' + Geral.FF0(Aplicacao),
    'AND rec.Nivel=1',
    'GROUP BY cab.Codigo, rec.CliInt, rec.Insumo',
    'ORDER BY cab.DtIniProd, cab.DtPedido, rec.PedidoOrd, rec.PedidoCod, rec.NomeCI, rec.NomePQ',
    '']);
    PB2.Max := QrPedPedEPQ.RecordCount;
    PB2.Position := 0;

    QrPedPedEPQ.First;
    while not QrPedPedEPQ.Eof do
    begin
      MyObjects.UpdPBOnly(PB2);
      //
      PesoEstq := 0;
      PesoFut  := - QrPedPedEPQPesoUso.Value;
      for I := Low(ArrEstoqueCod) to High(ArrEstoqueCod) do
      begin
        if ArrEstoqueCod[I] = QrPedPedEPQInsumo.Value then
        begin
          PesoEstq         := ArrEstoquePes[I];
          ArrEstoquePes[I] := PesoEstq - QrPedPedEPQPesoUso.Value;
          PesoFut          := ArrEstoquePes[I];
          Break;
        end;
      end;
      Codigo     := QrPedPedEPQPedidoCod.Value;
      Insumo     := QrPedPedEPQInsumo.Value;
      PesoUso    := QrPedPedEPQPesoUso.Value;
      CliInt     := QrPedPedEPQCliInt.Value;
      NomePQ     := QrPedPedEPQNomePQ.Value;
      NomeCI     := QrPedPedEPQNomeCI.Value;
      PedidoCod  := QrPedPedEPQPedidoCod.Value;
      PedidoTxt  := QrPedPedEPQPedidoTxt.Value;
      PedidoOrd  := QrPedPedEPQPedidoOrd.Value;
      InsumM2PesoKg := QrPedPedEPQInsumM2PesoKg.Value;
      IQ            := QrPedPedEPQIQ.Value;
      NomeIQ        := QrPedPedEPQNomeIQ.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqurec', False, [
      'Codigo', 'Insumo', 'PesoEstq',
      'PesoUso', 'PesoFut', 'CliInt',
      'NomePQ', 'NomeCI', 'Nivel',
      'PedidoCod', 'PedidoTxt', 'PedidoOrd',
      'InsumM2PesoKg', 'IQ', 'NomeIQ',
      'Aplicacao'], [
      ], [
      Codigo, Insumo, PesoEstq,
      PesoUso, PesoFut, CliInt,
      NomePQ, NomeCI, Nivel3,
      PedidoCod, PedidoTxt, PedidoOrd,
      InsumM2PesoKg, IQ, NomeIQ,
      Aplicacao], [
      ], False) then
      begin
      //
      end;


      QrPedPedEPQ.Next;
    end;
    PB1.Position := 4;
  end; //
  //
  ReopenQrPQURecCarga(0);
end;


procedure TFmPQUGer.CGTipoCadClick(Sender: TObject);
begin
  DefineFTC();
end;

procedure TFmPQUGer.CGTipoCadDblClick(Sender: TObject);
begin
  DefineFTC();
end;

procedure TFmPQUGer.CGTipoCadExit(Sender: TObject);
begin
  DefineFTC();
end;

procedure TFmPQUGer.DBGPQURecCargaDblClick(Sender: TObject);
var
  Campo: String;
  Coluna: Integer;
begin
  Coluna := TStringGrid(DBGPQURecCarga).Col;
  Campo := Uppercase(TDBGrid(DBGPQURecCarga).Columns[Coluna -1].FieldName);
  //
  if Campo = Uppercase('Insumo') then
    App_Jan.MostraFormPQ(QrPQURecCargaInsumo.Value)
  else
  if Campo = Uppercase('PesComprPreDef') then
    MostraFormPQURec(stUpd)
  else
    Geral.MB_Info('Selecione (duplo clique) a coluna "Insumo" ou a coluna "kg pr�-definido"');
end;

procedure TFmPQUGer.DBGPrevPedDblClick(Sender: TObject);
begin
  if (QrPQUCab.State <> dsInactive) and (QrPQUCab.RecordCount > 0) then
  begin
    if QrPQUCabEmitGru.Value = 0 then
      Geral.MB_Info('Este pedido n�o est� atrelado a nenhum Grupo de Emiss�o!')
    else
      App_jan.MostraFormEmitGru(QrPQUCabEmitGru.Value);
  end;
end;

function TFmPQUGer.DefineCIs(): String;
begin
  if Trim(EdEmpresas.Text) <> EmptyStr then
    Result := EdEmpresas.Text
  else
    Result := Geral.FF0(FCI);
end;

procedure TFmPQUGer.DefineDadosSequencia(var Ordem: Integer; var M2Dia,
  CourosDia, KgDia: Double; var DataIniProd: TDateTime);
var
  Qry: TmySQLQuery;
begin
  QrPQUCab.Last;
  //
  Ordem       := QrPQUCabOrdem.Value + 1;
  DataIniProd := QrPQUCabDtEntrega.Value;
  if DataIniProd < 2 then
    DataIniProd := Date;
  case DayOfWeek(DataIniProd) of
    (*domingo*)1: DataIniProd := DataIniProd + 1;
    (*s�bado*) 7: DataIniProd := DataIniProd + 2;
  end;
  //
  M2Dia     := 0;
  CourosDia := 0;
  KgDia     := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT lse.m2Dia, lse.CourosDia, lse.KgDia',
    'FROM listasetores lse ',
    'WHERE lse.TpReceita=2 ',
    'ORDER BY m2Dia DESC',
    '']);
    M2Dia     := Qry.FieldByName('m2Dia').AsFloat;
    CourosDia := Qry.FieldByName('CourosDia').AsFloat;
    KgDia     := Qry.FieldByName('KgDia').AsFloat;
  finally
    Qry.Free;
  end;
{
  case DayOfWeek(DataFimProd) of
    (*domingo*)1: DataFimProd := DataIniProd + 1;
    (*s�bado*) 7: DataFimProd := DataIniProd + 2;
  end;
}
end;

procedure TFmPQUGer.DefineFTC();
var
  I, K: Integer;
begin
  FTC := '';
  //
  for I := 0 to CGTipoCad.Items.Count -1 do
  begin
    K := CGTipoCad.GetIndexOfCheckedStatus(I);
    if K > -1 then
      FTC := FTC + ',' + FormatFloat('0', K);
  end;
  if FTC <> '' then
    FTC := Copy(FTC, 2);
end;

procedure TFmPQUGer.dmkDBGridZTO4DblClick(Sender: TObject);
begin
  if (QrUso.State <> dsInactive) and (QrUso.RecordCount > 0) then
    App_jan.MostraFormFormulas(QrUsoFormula.Value);
end;

procedure TFmPQUGer.EdCIRedefinido(Sender: TObject);
begin
  FCI := EdCI.ValueVariant;
  FCINome := CBCI.KeyValue;
  PCPrevisao.Visible := FCI <> 0;
end;

procedure TFmPQUGer.Excluigrupoetodosseusitens1Click(Sender: TObject);
var
  Codigo, PageIndex: Integer;
begin
  PageIndex := PCPrevisao.ActivePageIndex + 1;
  case PageIndex of
    //CO_ProducaoDiaria,
    CO_PedidoEmAberto:
    begin
      if Geral.MB_Pergunta(
      'Confirma a exclus�o do grupo selecionado e de TODOS seus itens?') =
      ID_YES then
      begin
        QrPQUFrm2.First;
        while not QrPQUFrm2.Eof do
        begin
          UMyMod.ExcluiRegistroInt1('', 'pqufrm', 'Controle',
          QrPQUFrm2Controle.Value, Dmod.MyDB);
          //
          QrPQUFrm2.Next;
        end;
        if UMyMod.ExcluiRegistroInt1('', 'pqucab', 'Codigo', QrPQUCabCodigo.Value,
        Dmod.MyDB) = ID_YES then
        begin
          Codigo := GOTOy.LocalizaPriorNextIntQr(QrPQUCab,
            QrPQUCabCodigo, QrPQUCabCodigo.Value);
          ReopenPQUCab(Codigo);
        end;
      end;
    end
    else Geral.MB_Aviso('"PCPrevisao.ActivePageIndex" n�o implementado (1)');
  end;
end;

procedure TFmPQUGer.Excluigruposelecionado1Click(Sender: TObject);
var
  Codigo, PageIndex: Integer;
begin
  PageIndex := PCPrevisao.ActivePageIndex + 1;
  case PageIndex of
    CO_ProducaoDiaria,
    CO_PedidoEmAberto:
    begin
      if UMyMod.ExcluiRegistroInt1(
      'Confirma a exclus�o do grupo selecionado?',
      'pqucab', 'Codigo', QrPQUCabCodigo.Value, Dmod.MyDB) = ID_YES then
      begin
        Codigo := GOTOy.LocalizaPriorNextIntQr(QrPQUCab,
          QrPQUCabCodigo, QrPQUCabCodigo.Value);
        ReopenPQUCab(Codigo);
      end;
    end
    else Geral.MB_Aviso('"PCPrevisao.ActivePageIndex" n�o implementado (1)');
  end;
end;

procedure TFmPQUGer.Excluiitemselecionado1Click(Sender: TObject);
var
  Controle, PageIndex: Integer;
begin
  PageIndex := PCPrevisao.ActivePageIndex + 1;
  case PageIndex of
    CO_ProducaoDiaria:
    begin
      if UMyMod.ExcluiRegistroInt1(
      'Confirma a exclus�o da base de previs�o selecionada?',
      'pqufrm', 'Controle', QrPQUFrm1Controle.Value, Dmod.MyDB) = ID_YES then
      begin
        Controle := GOTOy.LocalizaPriorNextIntQr(QrPQUFrm1,
          QrPQUFrm1Controle, QrPQUFrm1Controle.Value);
        ReopenPQUFrm(Controle);
      end;
    end;
    CO_PedidoEmAberto:
    begin
      if UMyMod.ExcluiRegistroInt1(
      'Confirma a exclus�o da base de previs�o selecionada?',
      'pqufrm', 'Controle', QrPQUFrm2Controle.Value, Dmod.MyDB) = ID_YES then
      begin
        Controle := GOTOy.LocalizaPriorNextIntQr(QrPQUFrm2,
          QrPQUFrm2Controle, QrPQUFrm2Controle.Value);
        ReopenPQUFrm(Controle);
      end;
    end
    else Geral.MB_Aviso('"PCPrevisao.ActivePageIndex" n�o implementado (2)');
  end;
end;

procedure TFmPQUGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQUGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCI := 0;
  FCINome := '';
  //
  QrPQUFrm1.Database := Dmod.MyDB;
  QrPQUFrm2.Database := Dmod.MyDB;
  //;;QrPQ.Database := Dmod.MyDB;
  QrPQUFrm2.Database := Dmod.MyDB;

  PCPrevisao.ActivePageIndex  := 1;
  //
  TPProdzIni.Date := Date - 92;
  TPProdzFim.Date := Date;
  //
  ReopenPQUCab(0);
  //

  MyObjects.PreencheComponente(CGTipoCad, sListaTipoCadPQ, 2);
  CGTipoCad.Value := 55;
  //
{
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  //CreateBlueDerm.RecriaTabelaLocal('setsetores', 1);
  FSetores := UnCreateBlueDerm.RecriaTempTableNovo(ntrttSetores, DModG.QrUpdPID1, False);
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FSetores,
    'SELECT Codigo, 1 Ativo ',
    'FROM ' + TMeuDB + '.listasetores ',
    '']);
  ReopenSetor(0);
  //
  TPIni.Date := Date;
  TPFim.Date := Date;
  PCRelatorio.ActivePageIndex := 0;
}
  //UnDMkDAC_PF.AbreQuery(QrListaSetores, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  //;;UnDMkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrFormulas, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrFornecedores, Dmod.MyDB);
  ReopenPQUCab(0);
  //
  EdCI.Text := dmkPF.ObtemInfoRegEdit('CI', Caption, '', ktString);
  FCI := Geral.IMV(EdCI.Text);
  if FCI <> 0 then CBCI.KeyValue := FCI;
{
  TPIni.Date := 0;
  TPFim.Date := Date;
  PnOrfaosNao.Align := alClient;
  VerificaOrfaos;
}
  DefineFTC();
  ReopenQrPQURecCarga(0);
end;

procedure TFmPQUGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQUGer.frxPrevisaoGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_NO_EMPRESA') = 0 then
    Value := CBCI.Text
  else
{
  if AnsiCompareText(VarName, 'VFR_AGR1NOME') = 0 then
  begin
    case PCRelatorio.ActivePageIndex of
      0:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrEstqNOMECI.Value;
          1: Value := QrEstqNOMESE.Value;
          2: Value := QrEstqNOMEPQ.Value;
          3: Value := QrEstqNOMEFO.Value;
          else Value := '';
        end;
      end;
      2:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrUsoTNOMEORI.Value;
          1: Value := QrUsoTNOMESE.Value;
          2: Value := QrUsoTNOMEPQ.Value;
          3: Value := QrUsoTNOMEFO.Value;
          else Value := '';
        end;
      end;
      3,5:
      begin
        if CkNovaForma6.Checked and (PCRelatorio.ActivePageIndex = 5) then
        begin
          case RGOrdem1.ItemIndex of
            0: Value := QrMoviPQNomeCI.Value;
            1: Value := QrMoviPQNomeSE.Value;
            2: Value := QrMoviPQNomePQ.Value;
            3: Value := QrMoviPQNomeFO.Value;
            else Value := '';
          end;
        end else
        begin
          case RGOrdem1.ItemIndex of
            0: Value := QrMoviPQ3NomeCI.Value;
            1: Value := QrMoviPQ3NomeSE.Value;
            2: Value := QrMoviPQ3NomePQ.Value;
            3: Value := QrMoviPQ3NomeFO.Value;
            else Value := '';
          end;
        end;
      end;
      9:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrPQRSumNOMECI.Value;
          1: Value := QrPQRSumNOMESE.Value;
          2: Value := QrPQRSumNOMEPQ.Value;
          3: Value := QrPQRSumNOMEFO.Value;
          else Value := '';
        end;
      end;
    end;
  end else if AnsiCompareText(VarName, 'VFR_AGR2NOME') = 0 then
  begin
    case PCRelatorio.ActivePageIndex of
      0:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrEstqNOMECI.Value;
          1: Value := QrEstqNOMESE.Value;
          2: Value := QrEstqNOMEPQ.Value;
          3: Value := QrEstqNOMEFO.Value;
          else Value := '';
        end;
      end;
      2:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrUsoTNOMEORI.Value;
          1: Value := QrUsoTNOMESE.Value;
          2: Value := QrUsoTNOMEPQ.Value;
          3: Value := QrUsoTNOMEFO.Value;
          else Value := '';
        end;
      end;
      3,5:
      begin
        if CkNovaForma6.Checked and (PCRelatorio.ActivePageIndex = 5) then
        begin
          case RGOrdem1.ItemIndex of
            0: Value := QrMoviPQ3NomeCI.Value;
            1: Value := QrMoviPQ3NomeSE.Value;
            2: Value := QrMoviPQ3NomePQ.Value;
            3: Value := QrMoviPQ3NomeFO.Value;
            else Value := '';
          end;
        end else
        begin
          case RGOrdem2.ItemIndex of
            0: Value := QrMoviPQNomeCI.Value;
            1: Value := QrMoviPQNomeSE.Value;
            2: Value := QrMoviPQNomePQ.Value;
            3: Value := QrMoviPQNomeFO.Value;
            else Value := '';
          end;
        end;
      end;
      9:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrPQRSumNOMECI.Value;
          1: Value := QrPQRSumNOMESE.Value;
          2: Value := QrPQRSumNOMEPQ.Value;
          3: Value := QrPQRSumNOMEFO.Value;
          else Value := '';
        end;
      end;
    end;
  end else} if AnsiCompareText(VarName, 'VAR_DATA_HORA') = 0 then
    Value := Geral.Maiusculas(Geral.FDT(Now(), 8), False)
(*
  else if AnsiCompareText(VarName, 'VAR_PQNOME') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBPQ.Text, EdPQ.ValueVariant)*)
  else if AnsiCompareText(VarName, 'VAR_CINOME') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBCI.Text, EdCI.ValueVariant)
  else if AnsiCompareText(VarName, 'VAR_AMOSTRAS') = 0 then
    Value := ''
  (*else if AnsiCompareText(VarName, 'VAR_SETORESTXT') = 0 then
    Value := FSetoresTxt*)
  (*else if AnsiCompareText(VarName, 'VAR_PERIODO') = 0 then
    Value := dmkPF.PeriodoImp(TPIni.Date, TPFim.Date, 0, 0, True, True,
    False, False, '', '')*)


  (*else if AnsiCompareText(VarName, 'VFR_AGR')  = 0 then Value := RGAgrupa.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD1') = 0 then Value := RGOrdem1.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD2') = 0 then Value := RGOrdem2.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD3') = 0 then Value := RGOrdem3.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_GRD') = 0 then
    Value := dmkPF.BoolToInt2(CkGrade.Checked, 15, 0)
  //else if AnsiCompareText(VarName, 'VARF_PECA') = 0 then
    //Value := CBUnidade.Text;*)

  else if AnsiCompareText(VarName, 'VFR_EMPRESA') = 0 then Value := CBCI.Text
  else if AnsiCompareText(VarName, 'VARF_GRUPO') = 0 then
    Value := QrPQUCabNome.Value
  else
end;

procedure TFmPQUGer.GradePedOpnDblClick(Sender: TObject);
begin
  if (QrPQUFrm2.State <> dsInactive) and (QrPQUFrm2.RecordCount > 0) then
    App_jan.MostraFormFormulas(QrPQUFrm2Receita.Value);
end;

procedure TFmPQUGer.ImprimePrevisao;
var
  NomePQ, NomeCI: String;
  Codigo, Insumo, ddSegur, CliInt, ddSegur2, ddSegur3, ddSegur4: Integer;
  Peso1, Peso2, Peso3, Peso4, Need1, Need2, Need3, Need4, Dias1, Dias2, Dias3,
  Dias4, Peso: Double;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := 0;
  Insumo         := 0;
  Peso1          := 0;
  Peso2          := 0;
  Peso3          := 0;
  Peso4          := 0;
  ddSegur        := 0;
  Need1          := 0;
  Need2          := 0;
  Need3          := 0;
  Need4          := 0;
  CliInt         := 0;
  Dias1          := 0;
  Dias2          := 0;
  Dias3          := 0;
  Dias4          := 0;
  Peso           := 0;
  NomePQ         := '';
  NomeCI         := '';
  ddSegur2       := 0;
  ddSegur3       := 0;
  ddSegur4       := 0;
  //
  if MyObjects.FIC(FCI = 0, EdCI, 'Defina o cliente interno!') then Exit;
  PB1.Position := 0;
  PB1.Max := QrPQUFrm1.RecordCount;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM pquclc');
  Dmod.QrUpd.ExecSQL;
  QrPQUFrm1.First;
  while not QrPQUFrm1.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    //
    Codigo := QrPQUFrm1Codigo.Value;
    //
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrRec, Dmod.MyDB, [
    'SELECT foc.ClienteI, foi.Produto, SUM(foi.Porcent) Porcent, ',
    'pqc.CI, pqc.EstSegur, pqc.Peso, pq_.Nome NOMEPQ, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECI, ',
    'pq_.IQ, IF(fop.Tipo=0, fop.RazaoSocial, fop.Nome) NOMEIQ ',
    'FROM formulasits foi',
    'LEFT JOIN formulas foc ON foc.Numero=foi.Numero',
    'LEFT JOIN pq pq_ ON pq_.Codigo=foi.Produto',
    'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI=' + Geral.FF0(FCI),
    //'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI IN (' + DefineCIs() + ')',
    'LEFT JOIN entidades ent ON ent.Codigo=foc.ClienteI',
    'LEFT JOIN entidades fop ON fop.Codigo=pq_.IQ',
    'WHERE pq_.GGXNiv2 IN (' + FTC + ') ',
    //'WHERE pq_.Codigo > 0 ',
    'AND foi.Numero=' + Geral.FF0(QrPQUFrm1Receita.Value),
    //'GROUP BY foi.Produto, pqc.CI',
    'GROUP BY foi.Produto, foc.ClienteI',
    '']);
    //
    PB2.Position := 0;
    PB2.Max := QrRec.RecordCount;
    while not QrRec.Eof do
    begin
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
      if QrRecPorcent.Value > 0 then
      begin
        Peso     := QrRecPeso.Value;
        //CliInt   := QrRecCI.Value;
        CliInt   := QrRecClienteI.Value;
        NomePQ   := QrRecNOMEPQ.Value;
        NomeCI   := QrRecNOMECI.Value;
        Insumo   := QrRecProduto.Value;
        //
        ddSegur  := QrRecEstSegur.Value;
        ddSegur2 := QrPQUFrm1DMais2.Value + ddSegur;
        ddSegur3 := QrPQUFrm1DMais3.Value + ddSegur;
        ddSegur4 := QrPQUFrm1DMais4.Value + ddSegur;
        //
        Peso1    := QrRecPorcent.Value * QrPQUFrm1Base1.Value / 100;
        Peso2    := QrRecPorcent.Value * QrPQUFrm1Base2.Value / 100;
        Peso3    := QrRecPorcent.Value * QrPQUFrm1Base3.Value / 100;
        Peso4    := QrRecPorcent.Value * QrPQUFrm1Base4.Value / 100;
        //
        if Peso1 > 0 then Dias1 := Peso / Peso1 else Dias1 := 0;
        if Peso2 > 0 then Dias2 := Peso / Peso2 else Dias2 := 0;
        if Peso3 > 0 then Dias3 := Peso / Peso3 else Dias3 := 0;
        if Peso4 > 0 then Dias4 := Peso / Peso4 else Dias4 := 0;
        //
        Need1 := Peso1 * ddSegur  (*- Peso*); if Need1 < 0 then  Need1 := 0;
        Need2 := Peso2 * ddSegur2 (*- Peso*); if Need2 < 0 then  Need2 := 0;
        Need3 := Peso3 * ddSegur3 (*- Peso*); if Need3 < 0 then  Need3 := 0;
        Need4 := Peso4 * ddSegur4 (*- Peso*); if Need4 < 0 then  Need4 := 0;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pquclc', False, [
        'Codigo', 'Insumo', 'Peso1',
        'Peso2', 'Peso3', 'Peso4',
        'ddSegur', 'Need1', 'Need2',
        'Need3', 'Need4', 'CliInt',
        'Dias1', 'Dias2', 'Dias3',
        'Dias4', 'Peso', 'NomePQ',
        'NomeCI',
        'ddSegur2', 'ddSegur3', 'ddSegur4'], [
        ], [
        Codigo, Insumo, Peso1,
        Peso2, Peso3, Peso4,
        ddSegur, Need1, Need2,
        Need3, Need4, CliInt,
        Dias1, Dias2, Dias3,
        Dias4, Peso, NomePQ,
        NomeCI,
        ddSegur2, ddSegur3, ddSegur4], [
        ], True) then
        begin
        //
        end;
      end;
      //
      QrRec.Next;
    end;
    QrPQUFrm1.Next;
  end;
  PB1.Position := 0;
  PB2.Position := 0;
{
  QrPQUClc.Close;
  UnDMkDAC_PF.AbreQuery(QrPQUClc, Dmod.MyDB);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQUClc, Dmod.MyDB, [
  'SELECT pui.Insumo, pui.CliInt, pui.ddSegur, pui.Peso,',
  'pui.NomePQ, pui.NomeCI, ddSegur, ddSegur2, ddSegur3, ',
  'ddSegur4,',
  'SUM(pui.Peso1) Peso1,',
  'SUM(pui.Peso2) Peso2,',
  'SUM(pui.Peso3) Peso3,',
  'SUM(pui.Peso4) Peso4,',
  '',
  'pui.Peso / SUM(pui.Peso1) Dias1,',
  'pui.Peso / SUM(pui.Peso2) Dias2,',
  'pui.Peso / SUM(pui.Peso3) Dias3,',
  'pui.Peso / SUM(pui.Peso4) Dias4,',
  '',
  'IF((SUM(pui.Peso1) * pui.ddSegur ) < pui.Peso, 0,',
  '(SUM(pui.Peso1) * pui.ddSegur ) - pui.Peso) Need1,',
  '',
  'IF((SUM(pui.Peso2) * pui.ddSegur2 ) < pui.Peso, 0,',
  '(SUM(pui.Peso2) * pui.ddSegur2) - pui.Peso) Need2,',
  '',
  'IF((SUM(pui.Peso3) * pui.ddSegur3 ) < pui.Peso, 0,',
  '(SUM(pui.Peso3) * pui.ddSegur3) - pui.Peso) Need3,',
  '',
  'IF((SUM(pui.Peso4) * pui.ddSegur4 ) < pui.Peso, 0,',
  '(SUM(pui.Peso4) * pui.ddSegur4) - pui.Peso) Need4',
  '',
  'FROM pquclc pui',
  'LEFT JOIN pq    pq_ ON pq_.Codigo=pui.Insumo',
  //'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI=' + Geral.FF0(FCI),
  'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI IN (' + DefineCIs() + ')',
  'GROUP BY pui.Insumo, pui.CliInt',
  'ORDER BY pui.NomeCI, pui.NOMEPQ',
  (*
  'SELECT pui.Insumo, pui.CliInt, pui.ddSegur, pui.Peso,',
  'pui.NomePQ, pui.NomeCI, ddSegur, ddSegur2, ddSegur3, ',
  'ddSegur4,',
  'SUM(pui.Peso1) Peso1,',
  'SUM(pui.Peso2) Peso2,',
  'SUM(pui.Peso3) Peso3,',
  'SUM(pui.Peso4) Peso4,',
  '',
  'pui.Peso / SUM(pui.Peso1) Dias1,',
  'pui.Peso / SUM(pui.Peso2) Dias2,',
  'pui.Peso / SUM(pui.Peso3) Dias3,',
  'pui.Peso / SUM(pui.Peso4) Dias4,',
  '',
  '(SUM(pui.Peso1) * pui.ddSegur ) - pui.Peso Need1,',
  '(SUM(pui.Peso2) * pui.ddSegur2) - pui.Peso Need2,',
  '(SUM(pui.Peso3) * pui.ddSegur3) - pui.Peso Need3,',
  '(SUM(pui.Peso4) * pui.ddSegur4) - pui.Peso Need4',
  '',
  'FROM pquclc pui',
  'LEFT JOIN pq    pq_ ON pq_.Codigo=pui.Insumo',
  'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI=' + Geral.FF0(FCI),
  'GROUP BY pui.Insumo, pui.CliInt',
  'ORDER BY pui.NomeCI, pui.NOMEPQ',
*)
(*
  'SELECT pui.Insumo, pui.CliInt, pui.ddSegur, pui.Peso,',
  'pui.NomePQ, pui.NomeCI, ddSegur, ddSegur2, ddSegur3, ',
  'ddSegur4,',
  'SUM(pui.Peso1) Peso1,',
  'SUM(pui.Peso2) Peso2,',
  'SUM(pui.Peso3) Peso3,',
  'SUM(pui.Peso4) Peso4,',
  'SUM(pui.Need1) Need1,',
  'SUM(pui.Need2) Need2,',
  'SUM(pui.Need3) Need3,',
  'SUM(pui.Need4) Need4,',
  'SUM(pui.Dias1) Dias1,',
  'SUM(pui.Dias2) Dias2,',
  'SUM(pui.Dias3) Dias3,',
  'SUM(pui.Dias4) Dias4',
  'FROM pquclc pui',
  'LEFT JOIN pq    pq_ ON pq_.Codigo=pui.Insumo',
  'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI=' + Geral.FF0(FCI),
  'GROUP BY pui.Insumo, pui.CliInt',
  'ORDER BY pui.NomeCI, pui.NOMEPQ',
*)
  '']);
  //
  MyObjects.frxMostra(frxPrevisao, 'Previs�o para Compra de Insumos Qu�micos');
end;

{
procedure TFmPQUGer.ImprimePrvPedOpenSelected;
const
  Nivel1 = 1;
  Nivel2 = 2;
var
  NomePQ, NomeCI, NOMEIQ: String;
  Codigo, Controle, Insumo, CliInt, IQ: Integer;
  PesoEstq, PesoUso, PesoFut, PesoNeed, Repet, KgLiqEmb, PesoCompra,
  QtEmbalagens: Double;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := 0;
  Controle       := 0;
  Insumo         := 0;
  PesoEstq       := 0;
  PesoUso        := 0;
  PesoFut        := 0;
  PesoNeed       := 0;
  CliInt         := 0;
  Repet          := 0;
  NomePQ         := '';
  NomeCI         := '';
  IQ             := 0;
  NOMEIQ         := '';
  QtEmbalagens   := 0;
  if MyObjects.FIC(FCI = 0, EdCI, 'Defina o cliente interno!') then
    Exit;
  PB1.Position := 0;
  PB1.Max := QrPQUFrm2.RecordCount;
  Dmod.MyDB.Execute('DELETE FROM pqurec');
  QrPQUFrm2.First;
  while not QrPQUFrm2.Eof do
  begin
    MyObjects.UpdPB(PB1, Label1, Label2);
    //
    Codigo := QrPQUFrm2Codigo.Value;
    UnDmkDAC_PF.AbreMySQLQuery0(QrRec, Dmod.MyDB, [
    'SELECT foc.ClienteI, foi.Produto, SUM(foi.Porcent) Porcent,,',
    'pqc.CI, pqc.EstSegur, pqc.Peso, pq_.Nome NOMEPQ,',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECI, ',
    'pq_.IQ, IF(fop.Tipo=0, fop.RazaoSocial, fop.Nome) NOMEIQ ',
    'FROM formulasits foi',
    'LEFT JOIN formulas foc ON foc.Numero=foi.Numero',
    'LEFT JOIN pq pq_ ON pq_.Codigo=foi.Produto',
    //'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo',
    'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI=' + Geral.FF0(FCI),
    //'LEFT JOIN entidades ent ON ent.Codigo=pqc.CI',
    'LEFT JOIN entidades ent ON ent.Codigo=foc.ClienteI',
    'LEFT JOIN entidades fop ON fop.Codigo=pq_.IQ',
    'WHERE pq_.GGXNiv2 IN (' + FTC + ') ',
    //'WHERE pq_.Codigo > 0 ',
    'AND foi.Numero=' + Geral.FF0(QrPQUFrm2Receita.Value),
    //'GROUP BY foi.Produto, pqc.CI',
    'AND foi.Produto > 0 ',
    'GROUP BY foi.Produto, foc.ClienteI',
    '']);
    //
    PB2.Position := 0;
    PB2.Max := QrRec.RecordCount;
    while not QrRec.Eof do
    begin
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
      if QrRecPorcent.Value > 0 then
      begin
        CliInt      := QrRecClienteI.Value;
        NomePQ      := QrRecNOMEPQ.Value;
        NomeCI      := QrRecNOMECI.Value;
        Insumo      := QrRecProduto.Value;
        NomeIQ      := QrRecNOMEIQ.Value;
        IQ          := QrRecIQ.Value;
        //
        PesoUso     := QrRecPorcent.Value * QrPQUFrm2PedPesoKg.Value / 100;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqurec', False, [
        'Codigo', 'Controle', 'Insumo',
        'PesoEstq', 'PesoUso', 'PesoFut',
        'PesoNeed', 'CliInt', 'Repet',
        'NomePQ', 'NomeCI', 'Nivel',
        'IQ', 'NOMEIQ'], [
        ], [
        Codigo, Controle, Insumo,
        PesoEstq, PesoUso, PesoFut,
        PesoNeed, CliInt, Repet,
        NomePQ, NomeCI, Nivel1,
        IQ, NOMEIQ], [
        ], False) then         begin
        //
        end;
      end;
      //
      QrRec.Next;
    end;
    QrPQUFrm2.Next;
  end;
  PB1.Position := 0;
  PB2.Position := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQURec, Dmod.MyDB, [
  'SELECT rec.Insumo, rec.NomePQ, rec.CliInt, ',
  'rec.NomeCI, pqc.Peso KgEstq, ',

  //'pqc.peso - SUM(rec.PesoUso) KgSaldoFut,',

  'IF(pqc.Peso IS NULL ,0 - SUM(rec.PesoUso), ',
  'pqc.peso - SUM(rec.PesoUso)) KgSaldoFut,',

  'SUM(rec.PesoUso) PesoUso, pqc.KgLiqEmb, ',

  'rec.IQ, rec.NomeIQ ',

  'FROM pqurec rec',
  'LEFT JOIN pqcli pqc ON pqc.PQ=rec.Insumo ',
  '          AND pqc.CI=rec.CliInt',
  'WHERE Nivel=' + Geral.FF0(Nivel1),
  'GROUP BY rec.Insumo, rec.CliInt ',
  '']);
  QrPQURec.First;
  while not QrPQURec.Eof do
  begin
    Codigo     := 0;
    Controle   := 0;
    Insumo     := QrPQURecInsumo.Value;
    PesoEstq   := QrPQURecKgEstq.Value;
    PesoUso    := QrPQURecPesoUso.Value;
    if QrPQURecKgSaldoFut.Value > 0 then
    begin
      PesoFut   := QrPQURecKgSaldoFut.Value;
      PesoNeed  := 0;
      Repet     := PesoFut / PesoUso;
    end else
    begin
      PesoFut   := 0;
      PesoNeed  := -QrPQURecKgSaldoFut.Value;
      Repet     := 0;
    end;
    KgLiqEmb    := QrPQURecKgLiqEmb.Value;
    if KgLiqEmb > 0 then
    begin
      QtEmbalagens := Trunc((PesoNeed / KgLiqEmb) + 0.999999999999999);
      PesoCompra  := QtEmbalagens  * KgLiqEmb;
    end else
    begin
      QtEmbalagens := PesoNeed;
      PesoCompra   := PesoNeed;
    end;
    CliInt      := QrPQURecCliInt.Value;
    NomePQ      := QrPQURecNomePQ.Value;
    NomeCI      := QrPQURecNomeCI.Value;
    IQ          := QrPQURecIQ.Value;
    NomeIQ      := QrPQURecNomeIQ.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqurec', False, [
    'Codigo', 'Controle', 'Insumo',
    'PesoEstq', 'PesoUso', 'PesoFut',
    'PesoNeed', 'CliInt', 'Repet',
    'NomePQ', 'NomeCI', 'Nivel',
    'KgLiqEmb', 'PesoCompra',
    'IQ', 'NomeIQ', 'QtEmbalagens'], [
    ], [
    Codigo, Controle, Insumo,
    PesoEstq, PesoUso, PesoFut,
    PesoNeed, CliInt, Repet,
    NomePQ, NomeCI, Nivel2,
    KgLiqEmb, PesoCompra,
    IQ, NomeIQ, QtEmbalagens], [
    ], False) then
    begin
    //
    end;

    //
    QrPQURec.Next;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPedOpen, Dmod.MyDB, [
  'SELECT *',
  'FROM pqurec rec',
  'WHERE Nivel=2',
  'ORDER BY NomeCI, NomeIQ, NOMEPQ',
  '']);
  MyObjects.frxMostra(frxPedOpen, 'Previs�o para Compra de Insumos Qu�micos por Pedidos Abertos');
end;
}

procedure TFmPQUGer.ImprimePrvPedOpenTodos;
const
  Aplicacao = Integer(TPQUAplicacao.pquaplicRecurtimento);
//var
  //Nivel: Integer;
begin
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPedSeq, Dmod.MyDB, [
  'SELECT cab.DtPedido, cab.DtIniProd, ',
  'cab.DtEntrega, rec.*',
  'FROM pqurec rec',
  'LEFT JOIN pqucab cab ON cab.Codigo=rec.Codigo',
  'WHERE rec.Aplicacao=' + Geral.FF0(Aplicacao),
  'AND rec.Nivel=3',
  'ORDER BY DtIniProd, DtPedido, PedidoOrd, PedidoCod, NomePQ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPedComprar, Dmod.MyDB, [
  'SELECT *',
  'FROM pqurec rec',
  'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
  'AND Nivel=2',
  'AND PesoPedido > 0',
  'ORDER BY NomeIQ, NOMEPQ',
  '']);
  //Geral.MB_Teste(QrPedComprar.SQL.Text);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPedOpen, Dmod.MyDB, [
  'SELECT *',
  'FROM pqurec rec',
  'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
  'AND Nivel=2',
  'ORDER BY NomeCI, NomeIQ, NOMEPQ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxPedSeq, [
  Dmod.frxDsMaster,
  frxDsPedSeq,
  frxDsPedOpen,
  frxDsPQU,
  frxDsPQUCab,
  frxDsPQUFrm2,
  frxDsPedComprar
  ]);
  PB1.Position := 0;
  PB2.Position := 0;
  PB3.Position := 0;
  //
  //erro dataHora na impress�o - datahora queries de impressao



  MyObjects.frxMostra(frxPedSeq, 'Previs�o para Compra de Insumos Qu�micos por Pedidos Abertos Sequenciais');
  //

end;

procedure TFmPQUGer.ImprimePrvPedProducao;
var
  Corda, SQL_Periodo: String;
begin
  SQL_Periodo := dmkPF.SQL_Periodo('AND DataEmis ',
    TPProdzIni.Date, TPProdzFim.Date, True, True);
  //
  Corda := MyObjects.CordaDeZTOChecks(DBGProdz, QrProdz, 'Numero', True);
  if Length(Corda) > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrProducao, Dmod.MyDB, [
    'SELECT WEEK(emi.DataEmis) Semana, SUM(emi.Peso) Peso,   ',
    'SUM(emi.Qtde) Qtde, SUM(emi.AreaM2) AreaM2,   ',
    'SUM(IF(emi.AreaM2>0, emi.AreaM2, 0)) /  ',
    'SUM(IF(emi.AreaM2>0, emi.Qtde, 0)) MediaM2Couro, ',
    'COUNT(emi.Numero) QtFuloes, SUM(emi.Qtde)/5 Media5, ',
    'COUNT(DISTINCT(DAYOFWEEK(emi.DataEmis))) DiaSemana   ',
    '/*, rec.Nome,  ',
    'DATE_FORMAT(MIN(DataEmis), "%Y/%m/%d") Min_DataEmis */ ',
    'FROM emit emi  ',
    'LEFT JOIN listasetores lse ON lse.Codigo=emi.Setor  ',
    'LEFT JOIN formulas rec ON rec.Numero=emi.Numero  ',
    'WHERE emi.Numero IN (' + Corda + ')  ',
    //'AND DataEmis BETWEEN "2020-06-29" AND "2020-08-29"  ',
    SQL_Periodo,
    'GROUP BY Semana ',
    'ORDER BY Semana ',
    '']);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumProd, Dmod.MyDB, [
    'SELECT SUM(emi.Peso) Peso,   ',
    'SUM(emi.Qtde) Qtde, SUM(emi.AreaM2) AreaM2,   ',
    'SUM(IF(emi.AreaM2>0, emi.AreaM2, 0)) /  ',
    'SUM(IF(emi.AreaM2>0, emi.Qtde, 0)) MediaM2Couro,',
    'MIN(emi.DataEmis) DtIni, MAX(emi.DataEmis) DtFim, ',
    '(TO_DAYS(MAX(emi.DataEmis)) - TO_DAYS(MIN(emi.DataEmis)) + 1) / 7',
    'Semanas,',
    'SUM(emi.Qtde)  / ',
    '((TO_DAYS(MAX(emi.DataEmis)) - TO_DAYS(MIN(emi.DataEmis)) + 1) / 7)',
    '/ 5 Media5',
    'FROM emit emi  ',
    'LEFT JOIN listasetores lse ON lse.Codigo=emi.Setor  ',
    'LEFT JOIN formulas rec ON rec.Numero=emi.Numero  ',
    'WHERE emi.Numero IN (' + Corda + ')  ',
    //'AND DataEmis BETWEEN "2020-06-29" AND "2020-08-29"  ',
    SQL_Periodo,
    '']);
  end;
end;

procedure TFmPQUGer.Incluicomplementodeproduo1Click(Sender: TObject);
var
  PageIndex: Integer;
begin
  PageIndex := PCPrevisao.ActivePageIndex + 1;
  case PageIndex of
    //CO_ProducaoDiaria:
    CO_PedidoEmAberto: MostraFormPQUCaH(stIns);
    else Geral.MB_Aviso('"PCPrevisao.ActivePageIndex" n�o implementado (7)');
  end;
end;

procedure TFmPQUGer.IncluiGrupo(Aplicacao: Integer);
var
  Nome: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := 0;
  Nome           := '';
  if InputQuery('Novo Grupo', 'Informe o nome do novo grupo:', Nome) then
  begin
    Codigo := UMyMod.BPGS1I32('pqucab', 'Codigo', '', '', tsPos, SQLType, Codigo);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqucab', False, [
    'Nome', 'Aplicacao'], [
    'Codigo'], [
    Nome, Aplicacao], [
    Codigo], True) then
      ReopenPQUCab(Codigo);
  end;
end;

procedure TFmPQUGer.Incluiitemavulso1Click(Sender: TObject);
begin
  MostraFormPQURec(stIns);
end;

procedure TFmPQUGer.Incluinovoitem1Click(Sender: TObject);
var
  PageIndex: Integer;
begin
  PageIndex := PCPrevisao.ActivePageIndex + 1;
  case PageIndex of
    CO_ProducaoDiaria: MostraEdicao1(1, stIns, 0);      //IncluiGrupo(PageIndex);
    CO_PedidoEmAberto: MostraFormPQUFrm(stIns);
    else Geral.MB_Aviso('"PCPrevisao.ActivePageIndex" n�o implementado (8)');
  end;
end;

procedure TFmPQUGer.Manual1Click(Sender: TObject);
var
  PageIndex: Integer;
begin
  PageIndex := PCPrevisao.ActivePageIndex + 1;
  case PageIndex of
    CO_ProducaoDiaria: MostraEdicao1(1, stIns, 0);
    CO_PedidoEmAberto: MostraFormPQUCab(stIns);
    else Geral.MB_Aviso('"PCPrevisao.ActivePageIndex" n�o implementado (7)');
  end;
end;

procedure TFmPQUGer.MostraEdicao1(Mostra: Integer; SQLType: TSQLType;
  Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      //GBConfirma.Enabled := True;
      GradePrev.Visible  := True;
      //GBCtrl1.Visible    := True;
      GBConf1.Visible    := False;
      PnPrevAdd.Visible  := False;
    end;
    1:
    begin
      GBConf1.Visible    := True;
      PnPrevAdd.Visible  := True;
      GradePrev.Visible  := False;
      //GBCtrl1.Visible    := False;
      //GBConfirma.Enabled := False;
      if SQLType = stIns then
      begin
        EdReceita1.ValueVariant := 0;
        CBReceita1.KeyValue     := NULL;
        EdUnidade.ValueVariant := 0;
        CBUnidade.KeyValue     := NULL;
        EdQtde1.ValueVariant   := 0;
        EdQtde2.ValueVariant   := 0;
        EdQtde3.ValueVariant   := 0;
        EdQtde4.ValueVariant   := 0;
        EdMedia.ValueVariant   := 0;
        EdDMais2.ValueVariant  := 0;
        EdDMais3.ValueVariant  := 0;
        EdDMais4.ValueVariant  := 0;
      end else begin
        EdReceita1.ValueVariant := QrPQUFrm1Receita.Value;
        CBReceita1.KeyValue     := QrPQUFrm1Receita.Value;
        EdUnidade.ValueVariant := QrPQUFrm1DefPeca.Value;
        CBUnidade.KeyValue     := QrPQUFrm1DefPeca.Value;
        EdQtde1.ValueVariant   := QrPQUFrm1Pecas1.Value;
        EdQtde2.ValueVariant   := QrPQUFrm1Pecas2.Value;
        EdQtde3.ValueVariant   := QrPQUFrm1Pecas3.Value;
        EdQtde4.ValueVariant   := QrPQUFrm1Pecas4.Value;
        EdMedia.ValueVariant   := QrPQUFrm1Media.Value;
        EdDMais2.ValueVariant  := QrPQUFrm1DMais2.Value;
        EdDMais3.ValueVariant  := QrPQUFrm1DMais3.Value;
        EdDMais4.ValueVariant  := QrPQUFrm1DMais4.Value;
      end;
      EdReceita1.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
end;

procedure TFmPQUGer.MostraEdicao2(Mostra: Integer; SQLType: TSQLType;
  Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      //GBConfirma.Enabled := True;
      GradePedOpn.Visible  := True;
      //GBCtrl2.Visible    := True;
      //GBConf2.Visible    := False;
    end;
  end;
  ImgTipo.SQLType := SQLType;
end;

procedure TFmPQUGer.MostraFormPQUCab(SQLType: TSQLType);
var
  Ordem, Codigo: Integer;
  //QtProdDia
  M2Dia, CourosDia, KgDia: Double;
  DataIniProd, DataFimProd: TDateTime;
begin
  Codigo := QrPQUCabCodigo.Value;
  //
  if DBCheck.CriaFm(TFmPQUCab, FmPQUCab, afmoSoBoss) then
  begin
    FmPQUCab.ImgTipo.SQLType := SQLType;
    FmPQUCab.FAplicacao := Integer(TPQUAplicacao.pquaplicRecurtimento);
    FmPQUCab.FQrPQUCab := QrPQUCab;
    DefineDadosSequencia(Ordem, M2Dia, CourosDia, KgDia, DataIniProd);
    QrPQUCab.Locate('Codigo', Codigo, []);
    if SQLType = stIns then
    begin
      FmPQUCab.TPDtPedido.Date          := Date + 1;
      FmPQUCab.TPDtIniProd.Date         := DataIniProd;
      FmPQUCab.EdDtIniProd.ValueVariant := DataIniProd;
      FmPQUCab.TPDtEntrega.Date         := DataIniProd + 7;
      FmPQUCab.EdDtIniProd.ValueVariant := DataIniProd + 7;
      //
      FmPQUCab.EdCliInt.ValueVariant      := EdCI.ValueVariant;
      FmPQUCab.CBCliInt.KeyValue          := EdCI.ValueVariant;
      FmPQUCab.EdOrdem.ValueVariant       := Ordem;
      FmPQUCab.FM2Dia                     := M2Dia;
      FmPQUCab.FCourosDia                 := CourosDia;
      FmPQUCab.FKgDia                     := KgDia;
      FmPQUCab.RGPedGrandeza.ItemIndex    := 1;
      FmPQUCab.EdQtProdDia.ValueVariant   := M2Dia;
    end else
    begin
      FmPQUCab.EdQtProdDia.ValueVariant  := QrPQUCabQtProdDia.Value;
      FmPQUCab.EdPedQtPdGdz.ValueVariant := QrPQUCabPedQtPdGdz.Value;
      FmPQUCab.RGPedGrandeza.ItemIndex   := QrPQUCabPedGrandeza.Value;
      FmPQUCab.EdCodigo.ValueVariant     := QrPQUCabCodigo.Value;
      FmPQUCab.EdNome.ValueVariant       := QrPQUCabNome.Value;
      FmPQUCab.EdOrdem.ValueVariant      := QrPQUCabOrdem.Value;
      FmPQUCab.TPDtPedido.Date           := QrPQUCabDtPedido.Value;
      FmPQUCab.TPDtIniProd.Date          := QrPQUCabDtIniProd.Value;
      FmPQUCab.EdDtIniProd.ValueVariant  := QrPQUCabDtIniProd.Value;
      FmPQUCab.TPDtEntrega.Date          := QrPQUCabDtEntrega.Value;
      FmPQUCab.EdDtEntrega.ValueVariant  := QrPQUCabDtEntrega.Value;
      FmPQUCab.EdEmitGru.ValueVariant    := QrPQUCabEmitGru.Value;
      FmPQUCab.CBEmitGru.KeyValue        := QrPQUCabEmitGru.Value;
      FmPQUCab.EdCliInt.ValueVariant     := QrPQUCabCliInt.Value;
      FmPQUCab.CBCliInt.KeyValue         := QrPQUCabCliInt.Value;
    end;
    //
    FmPQUCab.ShowModal;
    FmPQUCab.Destroy;
  end;
end;

procedure TFmPQUGer.MostraFormPQUCaH(SQLType: TSQLType);
var
  Ordem: Integer;
  //QtProdDia
  M2Dia, CourosDia, KgDia: Double;
  DataIniProd, DataFimProd: TDateTime;
begin
  if DBCheck.CriaFm(TFmPQUCaH, FmPQUCaH, afmoSoBoss) then
  begin
    FmPQUCaH.ImgTipo.SQLType := SQLType;
    FmPQUCaH.FAplicacao := Integer(TPQUAplicacao.pquaplicRecurtimento);
    FmPQUCaH.FQrPQUCab := QrPQUCab;
    DefineDadosSequencia(Ordem, M2Dia, CourosDia, KgDia, DataIniProd);
    QrPQUCab.Last;
    if SQLType = stIns then
    begin
      FmPQUCaH.TPDtPedido.Date          := Date + 1;
      FmPQUCaH.TPDtIniProd.Date         := DataIniProd;
      FmPQUCaH.EdDtIniProd.ValueVariant := DataIniProd;
      FmPQUCaH.TPDtEntrega.Date         := DataIniProd + 7;
      FmPQUCaH.EdDtIniProd.ValueVariant := DataIniProd + 7;
      //
      FmPQUCaH.EdCliInt.ValueVariant      := EdCI.ValueVariant;
      FmPQUCaH.CBCliInt.KeyValue          := EdCI.ValueVariant;
      FmPQUCaH.EdOrdem.ValueVariant       := Ordem;
      FmPQUCaH.FM2Dia                     := M2Dia;
      FmPQUCaH.FCourosDia                 := CourosDia;
      FmPQUCaH.FKgDia                     := KgDia;
      FmPQUCaH.RGPedGrandeza.ItemIndex    := 1;
      FmPQUCaH.EdQtProdDia.ValueVariant   := M2Dia;
    end else
    begin
      FmPQUCaH.EdQtProdDia.ValueVariant  := QrPQUCabQtProdDia.Value;
      FmPQUCaH.EdPedQtPdAll.ValueVariant := QrPQUCabPedQtPdGdz.Value;
      FmPQUCaH.RGPedGrandeza.ItemIndex   := QrPQUCabPedGrandeza.Value;
      FmPQUCaH.EdCodigo.ValueVariant     := QrPQUCabCodigo.Value;
      FmPQUCaH.EdNome.ValueVariant       := QrPQUCabNome.Value;
      FmPQUCaH.EdOrdem.ValueVariant      := QrPQUCabOrdem.Value;
      FmPQUCaH.TPDtPedido.Date           := QrPQUCabDtPedido.Value;
      FmPQUCaH.TPDtIniProd.Date          := QrPQUCabDtIniProd.Value;
      FmPQUCaH.EdDtIniProd.ValueVariant  := QrPQUCabDtIniProd.Value;
      FmPQUCaH.TPDtEntrega.Date          := QrPQUCabDtEntrega.Value;
      FmPQUCaH.EdDtEntrega.ValueVariant  := QrPQUCabDtEntrega.Value;
      FmPQUCaH.EdEmitGru.ValueVariant    := QrPQUCabEmitGru.Value;
      FmPQUCaH.CBEmitGru.KeyValue        := QrPQUCabEmitGru.Value;
      FmPQUCaH.EdCliInt.ValueVariant     := QrPQUCabCliInt.Value;
      FmPQUCaH.CBCliInt.KeyValue         := QrPQUCabCliInt.Value;
    end;
    //
    FmPQUCaH.ShowModal;
    FmPQUCaH.Destroy;
  end;
end;

procedure TFmPQUGer.MostraFormPQUFrm(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmPQUFrm, FmPQUFrm, afmoNegarComAviso) then
  begin
    FmPQUFrm.ImgTipo.SQLType := SQLType;
    FmPQUFrm.FQrCab := QrPQUCab;
    FmPQUFrm.FDsCab := DsPQUCab;
    FmPQUFrm.FQrIts := QrPQUFrm2;
    if SQLType = stIns then
    begin
      FmPQUFrm.RGPedGrandeza.ItemIndex     := 0;
      FmPQUFrm.EdReceita2.ValueVariant     := 0;
      FmPQUFrm.CBReceita2.KeyValue         := 0;
      FmPQUFrm.EdPedQtPdGdz.ValueVariant   := 0;
      FmPQUFrm.EdPedQtPrGdz.ValueVariant   := 0;
      FmPQUFrm.EdPedQtdeGdz.ValueVariant   := 0;
      FmPQUFrm.EdPedLinhasCul.ValueVariant := 0;
      FmPQUFrm.EdPedLinhasCab.ValueVariant := 0;
      FmPQUFrm.EdPedAreaM2.ValueVariant    := 0;
      FmPQUFrm.EdPedAreaP2.ValueVariant    := 0;
      FmPQUFrm.EdPedPesoKg.ValueVariant    := 0;
      //
      FmPQUFrm.EdPedPdArM2.ValueVariant    := 0;
      FmPQUFrm.EdPedPdArP2.ValueVariant    := 0;
      FmPQUFrm.EdPedPdPeKg.ValueVariant    := 0;
      FmPQUFrm.EdPedPrArM2.ValueVariant    := 0;
      FmPQUFrm.EdPedPrArP2.ValueVariant    := 0;
      FmPQUFrm.EdPedPrPeKg.ValueVariant    := 0;
      //
      FmPQUFrm.EdRendEsperado.ValueVariant := 0;
      FmPQUFrm.EdMargemDBI.ValueVariant    := 30;
      //
    end else
    begin
      FmPQUFrm.EdControle.ValueVariant := QrPQUFrm2Controle.Value;
      //
      FmPQUFrm.RGPedGrandeza.ItemIndex     := QrPQUFrm2PedGrandeza.Value;
      FmPQUFrm.EdReceita2.ValueVariant     := QrPQUFrm2Receita.Value;
      FmPQUFrm.CBReceita2.KeyValue         := QrPQUFrm2Receita.Value;
      FmPQUFrm.EdPedQtdeGdz.ValueVariant   := QrPQUFrm2PedQtdeGdz.Value;
      FmPQUFrm.EdPedQtPdGdz.ValueVariant   := QrPQUFrm2PedQtPdGdz.Value;
      FmPQUFrm.EdPedQtPrGdz.ValueVariant   := QrPQUFrm2PedQtPrGdz.Value;
      FmPQUFrm.EdPedLinhasCul.ValueVariant := QrPQUFrm2PedLinhasCul.Value;
      FmPQUFrm.EdPedLinhasCab.ValueVariant := QrPQUFrm2PedLinhasCab.Value;
      FmPQUFrm.EdPedAreaM2.ValueVariant    := QrPQUFrm2PedAreaM2.Value;
      FmPQUFrm.EdPedAreaP2.ValueVariant    := QrPQUFrm2PedAreaP2.Value;
      FmPQUFrm.EdPedPesoKg.ValueVariant    := QrPQUFrm2PedPesoKg.Value;
      //
      FmPQUFrm.EdPedPdArM2.ValueVariant    := QrPQUFrm2PedPdArM2.Value;
      FmPQUFrm.EdPedPdArP2.ValueVariant    := QrPQUFrm2PedPdArP2.Value;
      FmPQUFrm.EdPedPdPeKg.ValueVariant    := QrPQUFrm2PedPdPeKg.Value;
      FmPQUFrm.EdPedPrArM2.ValueVariant    := QrPQUFrm2PedPrArM2.Value;
      FmPQUFrm.EdPedPrArP2.ValueVariant    := QrPQUFrm2PedPrArP2.Value;
      FmPQUFrm.EdPedPrPeKg.ValueVariant    := QrPQUFrm2PedPrPeKg.Value;
      //
      FmPQUFrm.EdRendEsperado.ValueVariant := QrPQUFrm2RendEsperado.Value;
      FmPQUFrm.EdMargemDBI.ValueVariant    := QrPQUFrm2MargemDBI.Value;
      //
      FmPQUFrm.CalculaDadosPrevConsPed();
    end;
    FmPQUFrm.ShowModal;
    FmPQUFrm.Destroy;
  end;
end;

procedure TFmPQUGer.MostraFormPQURec(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmPQURec, FmPQURec, afmoNegarComAviso) then
  begin
    FmPQURec.ImgTipo.SQLType := SQLType;
    FmPQURec.FAplicacao  := Integer(TPQUAplicacao.pquaplicRecurtimento);
    FmPQURec.FNivel      := 2;
    FmPQURec.FCliIntCod  := FCI;
    FmPQURec.FCliIntNom  := FCINome;
    FmPQURec.ReopenPQ(FmPQURec.FCliIntCod);
    //

    if SQLType = stIns then
    begin
      //nada
    end else
    begin
      FmPQURec.LaPQ.Enabled := False;
      FmPQURec.EdPQ.Enabled := False;
      FmPQURec.CBPQ.Enabled := False;
      FmPQURec.SbPQ.Enabled := False;
      //
      FmPQURec.EdPQ.ValueVariant         := QrPQURecCargaInsumo.Value;
      FmPQURec.CBPQ.KeyValue             := QrPQURecCargaInsumo.Value;;
      FmPQURec.EdPesComprPreDef.ValueVariant := QrPQURecCargaPesComprPreDef.Value;
    end;
    FmPQURec.ShowModal;
    FmPQURec.Destroy;
  end;
end;

procedure TFmPQUGer.PreDefinePesoItem();
const
  Aplicacao = Integer(TPQUAplicacao.pquaplicRecurtimento);
  Nivel     = 2;
var
  sPeso: String;
  PesComprPreDef, CargaKgCompra, CargaKgQtEmb, PesoPedido: Double;
  Insumo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := stUpd;
  Insumo         := QrPQURecCargaInsumo.Value;
  PesComprPreDef := QrPQURecCargaPesComprPreDef.Value;
  if InputQuery('Altera kg pr�-definido', 'Informe o novo kg pr�-definido:', sPeso) then
  begin
    PesComprPreDef := Geral.DMV(sPeso);
    CargaKgCompra  := PesComprPreDef;
    CargaKgQtEmb   := PesComprPreDef;
    PesoPedido     := PesComprPreDef;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqurec', False, [
    'PesComprPreDef', 'CargaKgCompra', 'CargaKgQtEmb',
    'PesoPedido'], [
    'Aplicacao', 'Nivel', 'Insumo'], [
    PesComprPreDef, CargaKgCompra, CargaKgQtEmb,
    PesoPedido], [
    Aplicacao, Nivel, Insumo], True) then
    begin
      Dmod.MyDB.Execute(Geral.ATS([
      'UPDATE pqurec SET ',
      'CargaKgQtEmb=CargaKgCompra/KgLiqEmb ',
      'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
      'AND Nivel=2 ',
      'AND PesComprPreDef>0 ',
      'AND KgLiqEmb>0 ',
      '']));
      ReopenQrPQURecCarga(Insumo);
    end;
  end;
end;

procedure TFmPQUGer.QrPQUCabAfterScroll(DataSet: TDataSet);
begin
  case PCPrevisao.ActivePageIndex of
    0: VAR_LAST_Prev_Prev  := QrPQUCabCodigo.Value;
    1: VAR_LAST_Prev_PdOpn := QrPQUCabCodigo.Value;
  end;
  ReopenPQUFrm(0);
end;

procedure TFmPQUGer.QrPQUCabBeforeClose(DataSet: TDataSet);
begin
  QrPQUFrm1.Close;
  QrPQUFrm2.Close;
end;

procedure TFmPQUGer.QrPQUCabCalcFields(DataSet: TDataSet);
begin
  QrPQUFrm1KGT.Value := 0;
end;

procedure TFmPQUGer.QrPQUFrm1CalcFields(DataSet: TDataSet);
begin
  QrPQUFrm1KGT.Value := 0;
end;

procedure TFmPQUGer.QrPQURecCargaAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUso, Dmod.MyDB, [
  'SELECT DISTINCT Formula, PorcentUso',
  'FROM pqurec ',
  'WHERE Aplicacao=2',
  'AND Nivel=1 ',
  'AND Insumo=' + Geral.FF0(QrPQURecCargaInsumo.Value),
  'ORDER BY Formula, PorcentUso',
  '']);
end;

procedure TFmPQUGer.QrPQURecCargaBeforeClose(DataSet: TDataSet);
begin
  QrUso.Close;
end;

procedure TFmPQUGer.RGPQUCabSitClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrPQUCab.State = dsInactive) and (QrPQUCab.RecordCount > 0) then
    Codigo := QrPQUCabCodigo.Value
  else
    Codigo := 0;
  //
  ReopenPQUCab(Codigo);
end;

procedure TFmPQUGer.ReabreAvulsos();
var
  DdConsumo, DdCompra, SQL_Periodo: String;
  DataI, DataF: Integer;
begin
  DdConsumo := EdDdConsumo.Text;
  DdCompra  := EdDdCompra.Text;
  DataF := Trunc(DModG.ObtemAgora()) - 1;
  DataI := DataF - Geral.IMV(DdConsumo);
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataX ', DataI, DataF, True, True);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAvulsos, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _pqs_sem_receita_1_; ',
  'CREATE TABLE _pqs_sem_receita_1_ ',
  'SELECT Insumo, SUM(Peso) Peso ',
  'FROM ' + TMeuDB + '.pqx ',
  SQL_Periodo,
  'AND Tipo IN (150,190) ',
  'AND Peso < 0 ',
  'GROUP BY Insumo ',
  '; ',
  'DROP TABLE IF EXISTS _pqs_sem_receita_2_; ',
  'CREATE TABLE _pqs_sem_receita_2_ ',
  'SELECT IF(iq.Tipo=0, iq.RazaoSocial, iq.Nome) NomeIQ, ',
  'IF(ci.Tipo=0, ci.RazaoSocial, ci.Nome) NomeCI, pc.KgLiqEmb, ',
  'pc.CI, pq.IQ, p1.Insumo, pq.Nome, SUM(p1.Peso) KgConsumo, ',
  'ABS(SUM(p1.Peso) / ' + DdConsumo + ') Kg_dia, SUM(pc.Peso) KgEstq,  ',
  'ABS(SUM(pc.Peso) / (SUM(p1.Peso) / ' + DdConsumo + ')) DiasEstq, ',
  'IF(MAX(pc.EstSegur)<' + DdCompra + ', ' + DdCompra + ', MAX(pc.EstSegur)) DiasPeriod, ',
  'ABS(SUM(p1.Peso) / ' + DdConsumo + ') * IF(MAX(pc.EstSegur)<' + DdCompra + ', ' + DdCompra + ', MAX(pc.EstSegur)) NecesPeriod,  ',
  'IF( ',
  '  ((ABS(SUM(p1.Peso) / ' + DdConsumo + ') * IF(MAX(pc.EstSegur)<' + DdCompra + ', ' + DdCompra + ', MAX(pc.EstSegur))) - SUM(pc.Peso)) > 0, ',
  '  (ABS(SUM(p1.Peso) / ' + DdConsumo + ') * IF(MAX(pc.EstSegur)<' + DdCompra + ', ' + DdCompra + ', MAX(pc.EstSegur))) - SUM(pc.Peso), 0 ',
  ') Comprar ',
  'FROM _pqs_sem_receita_1_ p1 ',
  'LEFT JOIN ' + TMeuDB + '.pq pq ON pq.Codigo=p1.Insumo ',
  'LEFT JOIN ' + TMeuDB + '.pqcli pc ON pc.PQ=p1.Insumo  ',
  '  AND pc.CI IN (' + DefineCIs() + ') ',
  'LEFT JOIN ' + TMeuDB + '.entidades iq ON iq.Codigo=pq.IQ ',
  'LEFT JOIN ' + TMeuDB + '.entidades ci ON ci.Codigo=pc.CI ',
  'WHERE NOT p1.Insumo IN ( ',
  '  SELECT DISTINCT Insumo ',
  '  FROM ' + TMeuDB + '.pqurec ',
  ')   ',
  'GROUP BY p1.Insumo ',
  '; ',
  'SELECT *  ',
  'FROM  _pqs_sem_receita_2_ ',
  'ORDER BY Nome ',
  '; ',
  '']);
  //
  //Geral.MB_Teste(QrAvulsos.SQL.Text);
end;

procedure TFmPQUGer.ReopenPQUCab(Codigo: Integer);
var
  Aplicacao: Integer;
  SQL_Encerrado: String;
begin
  case RGPQUCabSit.ItemIndex of
    0: SQL_Encerrado := 'AND puc.Encerrado=0 ';
    1: SQL_Encerrado := 'AND puc.Encerrado=1 ';
    2: SQL_Encerrado := '';
    else
    begin
      SQL_Encerrado := '';
      Geral.MB_Erro('Item de "Mostrar pedido" n�o implementado!');
    end;
  end;
  Aplicacao := PCPrevisao.ActivePageIndex + 1;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQUCab, Dmod.MyDB, [
  'SELECT puc.*, emg.Nome NO_EmitGru ',
  'FROM pqucab puc ',
  'LEFT JOIN emitgru emg ON emg.Codigo=puc.EmitGru ',
  'WHERE puc.Aplicacao=' + Geral.FF0(Aplicacao),
  SQL_ENcerrado,
  'ORDER BY puc.Ordem, puc.DtIniProd, puc.Codigo',
  '']);
  //
  if Codigo <> 0 then
    QrPQUCab.Locate('Codigo', Codigo, []);
end;

procedure TFmPQUGer.ReopenPQUFrm(Controle: Integer);
const
  sAplicacao: array[0..3] of string = ('Indefinido', '�rea m�', '�rea ft�', 'Peso kg');
var
  ATT_Grandeza: String;

begin
  case PCPrevisao.ActivePageIndex of
    0:
    begin
      QrPQUFrm2.Close;
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQUFrm1, Dmod.MyDB, [
      'SELECT " " NO_Grandeza, ',
      'rec.Nome NOMERECEITA, dpe.Nome UNIDADE, pqf.* ',
      'FROM pqufrm pqf',
      'LEFT JOIN formulas rec ON rec.Numero=pqf.Receita ',
      'LEFT JOIN defpecas dpe ON dpe.Codigo=pqf.DefPeca ',
      'WHERE pqf.Codigo=' + Geral.FF0(QrPQUCabCodigo.Value),
      '']);
      //
      if Controle <> 0 then
        QrPQUFrm1.Locate('Controle', Controle, []);
      //
    end;
    1:
    begin
      QrPQUFrm1.Close;
      ATT_Grandeza := dmkPF.ArrayToTexto('pqf.PedGrandeza', 'NO_Grandeza', pvPos, True,
        sAplicacao);
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQUFrm2, Dmod.MyDB, [
      'SELECT ' + ATT_Grandeza,
      'rec.Nome NOMERECEITA, pqf.* ',
      'FROM pqufrm pqf',
      'LEFT JOIN pqucab cab ON cab.Codigo=pqf.Codigo',
      'LEFT JOIN formulas rec ON rec.Numero=pqf.Receita ',
      'LEFT JOIN defpecas dpe ON dpe.Codigo=pqf.DefPeca ',
      'WHERE pqf.Codigo=' + Geral.FF0(QrPQUCabCodigo.Value),
      '']);
      if Controle <> 0 then
        QrPQUFrm2.Locate('Controle', Controle, []);
      //
    end;
  end;
end;

procedure TFmPQUGer.ReopenQrPQURecCarga(Insumo: Integer);
const
  Aplicacao = Integer(TPQUAplicacao.pquaplicRecurtimento);
  Nivel     = 2;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQURecCarga, Dmod.MyDB, [
  'SELECT * ',
  'FROM pqurec ',
  'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
  'AND Nivel=' + Geral.FF0(Nivel),
  '']);
  if Insumo <> 0 then
    QrPQURecCarga.Locate('Insumo', Insumo, []);
end;

procedure TFmPQUGer.Usaclculoautomtico1Click(Sender: TObject);
const
  Aplicacao = Integer(TPQUAplicacao.pquaplicRecurtimento);
  Nivel     = 2;
var
  PesComprPreDef, CargaKgCompra, CargaKgQtEmb, PesoPedido: Double;
  Insumo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := stUpd;
  Insumo         := QrPQURecCargaInsumo.Value;
  PesComprPreDef := 0;
  CargaKgCompra  := QrPQURecCargaPesoCompra.Value;
  CargaKgQtEmb   := QrPQURecCargaQtEmbalagens.Value;
  PesoPedido     := CargaKgCompra;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqurec', False, [
  'PesComprPreDef', 'CargaKgCompra', 'CargaKgQtEmb',
  'PesoPedido'], [
  'Aplicacao', 'Nivel', 'Insumo'], [
  PesComprPreDef, CargaKgCompra, CargaKgQtEmb,
  PesoPedido], [
  Aplicacao, Nivel, Insumo], True) then
  begin
    Dmod.MyDB.Execute(Geral.ATS([
    'UPDATE pqurec SET ',
    'CargaKgQtEmb=CargaKgCompra/KgLiqEmb ',
    'WHERE Aplicacao=' + Geral.FF0(Aplicacao),
    'AND Nivel=2 ',
    'AND PesComprPreDef>0 ',
    'AND KgLiqEmb>0 ',
    '']));
    ReopenQrPQURecCarga(Insumo);
  end;
end;

end.
