unit EmitUsoSbt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, Vcl.Mask, dmkEdit,
  mySQLDbTables, dmkEditCB, dmkDBLookupComboBox, dmkEditCalc, dmkCheckBox,
  UnDmkProcFunc, Variants, dmkRadioGroup;

type
  TFmEmitUsoSbt = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrEmit: TmySQLQuery;
    QrEmitCodigo: TIntegerField;
    QrEmitDataEmis: TDateTimeField;
    QrEmitStatus: TSmallintField;
    QrEmitNumero: TIntegerField;
    QrEmitNOMECI: TWideStringField;
    QrEmitNOMESETOR: TWideStringField;
    QrEmitTecnico: TWideStringField;
    QrEmitNOME: TWideStringField;
    QrEmitClienteI: TIntegerField;
    QrEmitTipificacao: TSmallintField;
    QrEmitTempoR: TSmallintField;
    QrEmitTempoP: TSmallintField;
    QrEmitSetor: TSmallintField;
    QrEmitEspessura: TWideStringField;
    QrEmitPeso: TFloatField;
    QrEmitQtde: TFloatField;
    QrEmitLk: TIntegerField;
    QrEmitDataCad: TDateField;
    QrEmitDataAlt: TDateField;
    QrEmitUserCad: TIntegerField;
    QrEmitUserAlt: TIntegerField;
    QrEmitAreaM2: TFloatField;
    QrEmitFulao: TWideStringField;
    QrEmitDefPeca: TWideStringField;
    QrEmitCusto: TFloatField;
    QrEmitObs: TWideStringField;
    QrEmitAlterWeb: TSmallintField;
    QrEmitTipific: TSmallintField;
    QrEmitAtivo: TSmallintField;
    QrEmitSetrEmi: TSmallintField;
    QrEmitCod_Espess: TIntegerField;
    QrEmitCodDefPeca: TIntegerField;
    QrEmitSourcMP: TSmallintField;
    QrEmitCustoTo: TFloatField;
    QrEmitCustoKg: TFloatField;
    QrEmitCustoM2: TFloatField;
    QrEmitCusUSM2: TFloatField;
    QrEmitEmitGru: TIntegerField;
    QrEmitRetrabalho: TSmallintField;
    QrEmitNO_EmitGru: TWideStringField;
    DsEmit: TDataSource;
    Panel5: TPanel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label15: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit7: TDBEdit;
    Label1: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    Label2: TLabel;
    DBEdit11: TDBEdit;
    QrEmitSemiCodPeca: TIntegerField;
    QrEmitSemiTxtPeca: TWideStringField;
    QrEmitSemiPeso: TFloatField;
    QrEmitSemiQtde: TFloatField;
    QrEmitSemiAreaM2: TFloatField;
    QrEmitSemiRendim: TFloatField;
    QrEmitSemiCodEspe: TIntegerField;
    QrEmitSemiTxtEspe: TWideStringField;
    QrEmitBRL_USD: TFloatField;
    QrEmitBRL_EUR: TFloatField;
    QrEmitDtaCambio: TDateField;
    QrEmitVSMovCod: TIntegerField;
    QrEmitHoraIni: TTimeField;
    QrEmitGraCorCad: TIntegerField;
    QrEmitNoGraCorCad: TWideStringField;
    QrEmitSemiCodEspReb: TIntegerField;
    EdControle: TdmkEdit;
    Label26: TLabel;
    Panel6: TPanel;
    Label4: TLabel;
    Label10: TLabel;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    EdIDEmitSbtQtd: TdmkEditCB;
    CBIDEmitSbtQtd: TdmkDBLookupComboBox;
    EdCouIntros: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    EdPesoKg: TdmkEdit;
    QrEmitSbtQtd: TMySQLQuery;
    QrEmitSbtQtdControle: TIntegerField;
    QrEmitSbtQtdNO_EmitSbtCad: TWideStringField;
    QrEmitSbtQtdSdoCouIntros: TFloatField;
    QrEmitSbtQtdSdoAreaM2: TFloatField;
    QrEmitSbtQtdSdoAreaP2: TFloatField;
    DsEmitSbtQtd: TDataSource;
    Label17: TLabel;
    DBEdit12: TDBEdit;
    Label20: TLabel;
    DBEdit13: TDBEdit;
    Label21: TLabel;
    DBEdit14: TDBEdit;
    Label11: TLabel;
    QrEmitSbtQtdPercEsperRend: TFloatField;
    DBEdit15: TDBEdit;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtCopiarClick(Sender: TObject);
    procedure EdAreaM2Change(Sender: TObject);
    procedure EdSemiAreaM2Change(Sender: TObject);
    procedure EdGraCorCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    FPesagem: Integer;
    REICalArea: Boolean;
    //
    procedure VerificaEspessura();
    procedure CalculaRendimento();
  public
    { Public declarations }
    FEmitGru: Integer;
    procedure ReopenEmit(Codigo: Integer);
    procedure ReopenEmitSbtQtd();
  end;

  var
  FmEmitUsoSbt: TFmEmitUsoSbt;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule, MyDBCheck, UnPQ_PF;

{$R *.DFM}

procedure TFmEmitUsoSbt.BtCopiarClick(Sender: TObject);
begin
{
  EdSemiQtde.ValueVariant       := QrEmitQtde.Value;
  EdSemiDefPeca.ValueVariant    := EdPeca.ValueVariant;
  CBSemiDefPeca.KeyValue        := EdPeca.ValueVariant;
  EdSemiEspessura.ValueVariant  := EdEspessura.ValueVariant;
  CBSemiEspessura.KeyValue      := EdEspessura.ValueVariant;
  //
  EdSemiPeso.SetFocus;
}
end;

procedure TFmEmitUsoSbt.BtOKClick(Sender: TObject);
var
  Codigo, Controle, IDEmitSbtQtd: Integer;
  CouIntros, AreaM2, AreaP2, PesoKg, PercEsperRend: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FPesagem;
  Controle       := EdControle.ValueVariant;
  IDEmitSbtQtd   := EdIDEmitSbtQtd.ValueVariant;
  CouIntros      := EdCouIntros.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  PercEsperRend  := QrEmitSbtQtdPercEsperRend.Value;
  //
  if MyObjects.FIC(IDEmitSbtQtd=0, EdIDEmitSbtQtd, 'Informe o substrato de rendimento') then
    Exit;
  //
  if MyObjects.FIC(AreaM2 < 0.01, EdAreaM2, 'Informe a �rea') then
    Exit;
  //
  if MyObjects.FIC(CouIntros <= 0, EdCouIntros, 'Informe a quantidade de couros inteiros!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('emitusosbt', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'emitusosbt', False, [
  'Codigo', 'IDEmitSbtQtd',
  'CouIntros', 'AreaM2', 'AreaP2',
  'PesoKg', 'PercEsperRend'], [
  'Controle'], [
  Codigo, IDEmitSbtQtd,
  CouIntros, AreaM2, AreaP2,
  PesoKg, PercEsperRend], [
  Controle], True) then
  begin
    PQ_PF.AtualizaSaldo_Emit_EMitUsoSbt(FPesagem);
    PQ_PF.AtualizaSaldo_EmitSbtQtd(IDEmitSbtQtd);
    //
    Close;
  end;

{var
  //DataEmis, NOMECI, NOMESETOR, Tecnico, NOME, Obs,
  Espessura, DefPeca, Fulao, SemiTxtPeca, SemiTxtEspe, NoGraCorCad: String;
  // Status, Numero, ClienteI, Tipificacao, TempoR, TempoP, Setor, Tipific, SetrEmi, SourcMP,
  Codigo, Cod_Espess, CodDefPeca, EmitGru, Retrabalho, SemiCodPeca, GraCorCad,
  SemiCodEspe, SemiCodEspReb: Integer;
  // Peso, Custo, CustoTo, CustoKg, CustoM2, CusUSM2,
  Qtde, AreaM2, SemiPeso, SemiQtde, SemiAreaM2, SemiRendim: Double;
begin }
{
  Codigo         := FPesagem;
  (*DataEmis       := ;
  Status         := ;
  Numero         := ;
  NOMECI         := ;
  NOMESETOR      := ;
  Tecnico        := ;
  NOME           := ;
  ClienteI       := ;
  Tipificacao    := ;
  TempoR         := ;
  TempoP         := ;
  Setor          := ;
  Tipific        := ;*)
  Espessura      := CBEspessura.Text;
  DefPeca        := CBPeca.Text;
  (*Peso           := ;
  Custo          := ;*)
  Qtde           := EdQtde.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  Fulao          := EdFulao.ValueVariant;
  (*Obs            := ;
  SetrEmi        := ;
  SourcMP        := ;*)
  Cod_Espess     := EdEspessura.ValueVariant;
  CodDefPeca     := EdPeca.ValueVariant;
  (*CustoTo        := ;
  CustoKg        := ;
  CustoM2        := ;
  CusUSM2        := ;*)
  EmitGru        := EdEmitGru.ValueVariant;
  Retrabalho     := Geral.BoolToInt(CkRetrabalho.Checked);
  SemiCodPeca    := EdSemiDefPeca.ValueVariant;
  SemiTxtPeca    := CBSemiDefPeca.Text;
  SemiPeso       := EdSemiPeso.ValueVariant;
  SemiQtde       := EdSemiQtde.ValueVariant;
  SemiAreaM2     := EdSemiAreaM2.ValueVariant;
  SemiCodEspe    := EdSemiEspessura.ValueVariant;
  SemiTxtEspe    := CBSemiEspessura.Text;
  SemiRendim     := EdSemiRendim.ValueVariant;
  SemiCodEspReb  := EdSemiCodEspReb.ValueVariant;
  GraCorCad      := EdGraCorCad.ValueVariant;
  NoGraCorCad    := EdNoGraCorCad.Text;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'emit', False, [
  (*'DataEmis', 'Status', 'Numero',
  'NOMECI', 'NOMESETOR', 'Tecnico',
  'NOME', 'ClienteI', 'Tipificacao',
  'TempoR', 'TempoP', 'Setor',
  'Tipific',*) 'Espessura', 'DefPeca',
  (*'Peso', 'Custo',*) 'Qtde',
  'AreaM2', 'Fulao', (*'Obs',
  'SetrEmi', 'SourcMP',*) 'Cod_Espess',
  'CodDefPeca', (*'CustoTo', 'CustoKg',
  'CustoM2', 'CusUSM2',*) 'EmitGru',
  'Retrabalho', 'SemiCodPeca', 'SemiTxtPeca',
  'SemiPeso', 'SemiQtde', 'SemiAreaM2',
  'SemiRendim', 'SemiCodEspe', 'SemiTxtEspe',
  'GraCorCad', 'NoGraCorCad', 'SemiCodEspReb'], [
  'Codigo'], [
  (*DataEmis, Status, Numero,
  NOMECI, NOMESETOR, Tecnico,
  NOME, ClienteI, Tipificacao,
  TempoR, TempoP, Setor,
  Tipific,*) Espessura, DefPeca,
  (*Peso, Custo,*) Qtde,
  AreaM2, Fulao, (*Obs,
  SetrEmi, SourcMP,*) Cod_Espess,
  CodDefPeca, (*CustoTo, CustoKg,
  CustoM2, CusUSM2,*) EmitGru,
  Retrabalho, SemiCodPeca, SemiTxtPeca,
  SemiPeso, SemiQtde, SemiAreaM2,
  SemiRendim, SemiCodEspe, SemiTxtEspe,
  GraCorCad, NoGraCorCad, SemiCodEspReb], [
  Codigo], True) then
  begin
    Close;
  end;
}
end;

procedure TFmEmitUsoSbt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmitUsoSbt.CalculaRendimento();
var
  M2Ini, M2Fim: Double;
begin
{
  M2Ini := EdAreaM2.ValueVariant;
  M2Fim := EdSemiAreaM2.ValueVariant;
  //
  EdSemiRendim.ValueVariant := dmkPF.CalculaRendimento(M2Ini, M2Fim);
}
end;

procedure TFmEmitUsoSbt.EdAreaM2Change(Sender: TObject);
begin
//  CalculaRendimento();
end;

procedure TFmEmitUsoSbt.EdGraCorCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Cor';
  Prompt = 'Informe a Cor';
  Campo  = 'Descricao';
var
  Resp: Variant;
begin
{
  if Key = VK_F4 then
  begin
    Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo, Nome ' + Campo,
    'FROM gracorcad ',
    'ORDER BY Nome ',
    ''], Dmod.MyDB, True);
    if Resp <> Null then
    begin
      EdGraCorCad.ValueVariant := VAR_SELCOD;
      EdNoGraCorCad.Text       := VAR_SELNOM;
    end;
  end;
}
end;

procedure TFmEmitUsoSbt.EdSemiAreaM2Change(Sender: TObject);
begin
//  CalculaRendimento();
end;

procedure TFmEmitUsoSbt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmitUsoSbt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
  //
  REICalArea := False;
{
  UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDefPecas1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDefPecas2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEspessuras1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEspessuras2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrRebaixe, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
}
end;

procedure TFmEmitUsoSbt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmitUsoSbt.ReopenEmit(Codigo: Integer);
begin
  FPesagem := Codigo;
  QrEmit.Close;
  if FPesagem > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
    'SELECT emi.*, gru.Nome NO_EmitGru  ',
    'FROM emit emi ',
    'LEFT JOIN emitgru gru ON gru.Codigo=emi.EmitGru ',
    'WHERE emi.Codigo=' + Geral.FF0(FPesagem),
    '']);
    //
{
    EdQtde.ValueVariant       := QrEmitQtde.Value;
    EdPeca.ValueVariant       := QrEmitCodDefPeca.Value;
    CBPeca.KeyValue           := QrEmitCodDefPeca.Value;
    EdAreaM2.ValueVariant     := QrEmitAreaM2.Value;
    //EdAreaP2.ValueVariant   := ??;
    EdFulao.ValueVariant      := QrEmitFulao.Value;
    EdEspessura.ValueVariant  := QrEmitCod_Espess.Value;
    CBEspessura.KeyValue      := QrEmitCod_Espess.Value;
    EdEmitGru.ValueVariant    := QrEmitEmitGru.Value;
    CBEmitGru.KeyValue        := QrEmitEmitGru.Value;
    CkRetrabalho.Checked      := Geral.IntToBool(QrEmitRetrabalho.Value);
    //
    EdSemiQtde.ValueVariant      := QrEmitSemiQtde.Value;
    EdSemiDefPeca.ValueVariant   := QrEmitSemiCodPeca.Value;
    CBSemiDefPeca.KeyValue       := QrEmitSemiCodPeca.Value;
    EdSemiAreaM2.ValueVariant    := QrEmitSemiAreaM2.Value;
    //EdSemiAreaP2.ValueVariant    := ??
    EdSemiEspessura.ValueVariant := QrEmitSemiCodEspe.Value;
    CBSemiEspessura.KeyValue     := QrEmitSemiCodEspe.Value;
    EdSemiPeso.ValueVariant      := QrEmitSemiPeso.Value;
    //
    EdGraCorCad.ValueVariant     := QrEmitGraCorCad.Value;
    EdNoGraCorCad.Text           := QrEmitNoGraCorCad.Value;
    EdSemiCodEspReb.ValueVariant := QrEmitSemiCodEspReb.Value;
    CBSemiCodEspReb.KeyValue     := QrEmitSemiCodEspReb.Value;
    VerificaEspessura();
}
  end else
    Geral.MB_Erro('Pesagem n�o localizada!');
end;

procedure TFmEmitUsoSbt.ReopenEmitSbtQtd();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitSbtQtd, Dmod.MyDB, [
  'SELECT esq.Controle, sbt.Nome NO_EmitSbtCad,  ',
  //'esq.SdoCouIntros, esq.SdoAreaM2, esq.SdoAreaP2, esq.SdoPesoKg ',
  'esq.SdoCouIntros, esq.SdoAreaM2, esq.SdoAreaP2, esq.PercEsperRend ',
  'FROM emitsbtqtd esq ',
  'LEFT JOIN emitsbtcad sbt ON sbt.Codigo=esq.EmitSbtCad ',
  'WHERE esq.Codigo=' + Geral.FF0(FEmitGru),
  '']);
end;

procedure TFmEmitUsoSbt.SpeedButton1Click(Sender: TObject);
begin
  EdCouIntros.ValueVariant := QrEmitSbtQtdSdoCouIntros.Value;
  EdAreaM2.ValueVariant    := QrEmitSbtQtdSdoAreaM2.Value;
  EdAreaP2.ValueVariant    := QrEmitSbtQtdSdoAreaP2.Value;
end;

procedure TFmEmitUsoSbt.VerificaEspessura();
begin
{
  if EdEspessura.ValueVariant = 0 then
  begin
    if (QrEmitCod_Espess.Value = 0) and (QrEmitEspessura.Value <> '') then
    begin
      if QrEspessuras1.Locate('Linhas', QrEmitEspessura.Value, []) then
      begin
        EdEspessura.ValueVariant  := QrEspessuras1Codigo.Value;
        CBEspessura.KeyValue      := QrEspessuras1Codigo.Value;
      end;
    end;
  end;
}
end;

{
object QrEmitGru: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT Codigo, Nome'
    'FROM emitgru'
    'WHERE Ativo=1'
    'ORDER BY Nome'
    '')
  Left = 108
  Top = 72
  object QrEmitGruCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrEmitGruNome: TWideStringField
    FieldName = 'Nome'
    Size = 60
  end
end
object DsEmitGru: TDataSource
  DataSet = QrEmitGru
  Left = 108
  Top = 116
end
object QrDefPecas1: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT * From DefPecas')
  Left = 160
  Top = 76
  object QrDefPecas1Codigo: TIntegerField
    FieldName = 'Codigo'
    Origin = 'DBMBWET.defpecas.Codigo'
  end
  object QrDefPecas1Nome: TWideStringField
    FieldName = 'Nome'
    Origin = 'DBMBWET.defpecas.Nome'
    Size = 6
  end
  object QrDefPecas1Grandeza: TSmallintField
    FieldName = 'Grandeza'
    Origin = 'DBMBWET.defpecas.Grandeza'
  end
  object QrDefPecas1Lk: TIntegerField
    FieldName = 'Lk'
    Origin = 'DBMBWET.defpecas.Lk'
  end
end
object DsDefPecas1: TDataSource
  DataSet = QrDefPecas1
  Left = 160
  Top = 120
end
object QrDefPecas2: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT * From DefPecas')
  Left = 232
  Top = 80
  object QrDefPecas2Codigo: TIntegerField
    FieldName = 'Codigo'
    Origin = 'DBMBWET.defpecas.Codigo'
  end
  object QrDefPecas2Nome: TWideStringField
    FieldName = 'Nome'
    Origin = 'DBMBWET.defpecas.Nome'
    Size = 6
  end
  object QrDefPecas2Grandeza: TSmallintField
    FieldName = 'Grandeza'
    Origin = 'DBMBWET.defpecas.Grandeza'
  end
  object QrDefPecas2Lk: TIntegerField
    FieldName = 'Lk'
    Origin = 'DBMBWET.defpecas.Lk'
  end
end
object DsDefPecas2: TDataSource
  DataSet = QrDefPecas2
  Left = 232
  Top = 124
end
object QrEspessuras1: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT * From Espessuras')
  Left = 312
  Top = 80
  object QrEspessuras1Codigo: TIntegerField
    FieldName = 'Codigo'
    Origin = 'DBMBWET.espessuras.Codigo'
  end
  object QrEspessuras1Linhas: TWideStringField
    DisplayWidth = 20
    FieldName = 'Linhas'
    Origin = 'DBMBWET.espessuras.Linhas'
  end
  object QrEspessuras1EMCM: TFloatField
    FieldName = 'EMCM'
    Origin = 'DBMBWET.espessuras.EMCM'
  end
  object QrEspessuras1Lk: TIntegerField
    FieldName = 'Lk'
    Origin = 'DBMBWET.espessuras.Lk'
  end
end
object DsEspessuras1: TDataSource
  DataSet = QrEspessuras1
  Left = 312
  Top = 124
end
object QrEspessuras2: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT * From Espessuras')
  Left = 368
  Top = 84
  object QrEspessuras2Codigo: TIntegerField
    FieldName = 'Codigo'
    Origin = 'DBMBWET.espessuras.Codigo'
  end
  object QrEspessuras2Linhas: TWideStringField
    DisplayWidth = 20
    FieldName = 'Linhas'
    Origin = 'DBMBWET.espessuras.Linhas'
  end
  object QrEspessuras2EMCM: TFloatField
    FieldName = 'EMCM'
    Origin = 'DBMBWET.espessuras.EMCM'
  end
  object QrEspessuras2Lk: TIntegerField
    FieldName = 'Lk'
    Origin = 'DBMBWET.espessuras.Lk'
  end
end
object DsEspessuras2: TDataSource
  DataSet = QrEspessuras2
  Left = 368
  Top = 128
end
object QrRebaixe: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT Codigo, Linhas, EMCM'
    'FROM espessuras'
    'ORDER BY EMCM')
  Left = 440
  Top = 65524
  object QrRebaixeCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrRebaixeLinhas: TWideStringField
    FieldName = 'Linhas'
    Size = 7
  end
  object QrRebaixeEMCM: TFloatField
    FieldName = 'EMCM'
  end
end
object DsRebaixe: TDataSource
  DataSet = QrRebaixe
  Left = 440
  Top = 36
end
object DsGraCorCad: TDataSource
  DataSet = QrGraCorCad
  Left = 492
  Top = 40
end
object QrGraCorCad: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT Codigo, Nome'
    'FROM gracorcad'
    'ORDER BY Nome')
  Left = 496
  Top = 65528
  object QrGraCorCadCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrGraCorCadNome: TWideStringField
    FieldName = 'Nome'
    Size = 50
  end
end
}

end.
