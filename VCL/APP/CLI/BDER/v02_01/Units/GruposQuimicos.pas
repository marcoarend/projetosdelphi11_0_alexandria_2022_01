unit GruposQuimicos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, DmkDAC_PF,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, dmkGeral, dmkEdit, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmGruposQuimicos = class(TForm)
    PainelDados: TPanel;
    DsGruposQuimicos: TDataSource;
    QrGruposQuimicos: TmySQLQuery;
    QrGruposQuimicosLk: TIntegerField;
    QrGruposQuimicosDataCad: TDateField;
    QrGruposQuimicosDataAlt: TDateField;
    QrGruposQuimicosUserCad: TIntegerField;
    QrGruposQuimicosUserAlt: TIntegerField;
    QrGruposQuimicosCodigo: TSmallintField;
    QrGruposQuimicosNome: TWideStringField;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrIts: TmySQLQuery;
    DsIts: TDataSource;
    DBGrid1: TDBGrid;
    QrItsNOMEIQ: TWideStringField;
    QrItsCodigo: TIntegerField;
    QrItsNome: TWideStringField;
    QrItsIQ: TIntegerField;
    QrItsAtivo: TSmallintField;
    QrItsLk: TIntegerField;
    QrItsDataCad: TDateField;
    QrItsDataAlt: TDateField;
    QrItsUserCad: TIntegerField;
    QrItsUserAlt: TIntegerField;
    QrItsGrupoQuimico: TIntegerField;
    QrItsNOMEATIVO: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGruposQuimicosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrGruposQuimicosAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGruposQuimicosBeforeOpen(DataSet: TDataSet);
    procedure QrItsCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenIts;
  public
    { Public declarations }
  end;

var
  FmGruposQuimicos: TFmGruposQuimicos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGruposQuimicos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGruposQuimicos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGruposQuimicosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGruposQuimicos.DefParams;
begin
  VAR_GOTOTABELA := 'GruposQuimicos';
  VAR_GOTOMYSQLTABLE := QrGruposQuimicos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM gruposquimicos');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmGruposQuimicos.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      if SQLType = stIns then
      begin
        EdCodigo.Text := '';
        EdNome.Text   := '';
      end else begin
        EdCodigo.Text := IntToStr(QrGruposQuimicosCodigo.Value);
        EdNome.Text   := QrGruposQuimicosNome.Value;
      end;
      EdNome.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmGruposQuimicos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGruposQuimicos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGruposQuimicos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmGruposQuimicos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGruposQuimicos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGruposQuimicos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGruposQuimicos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGruposQuimicos.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmGruposQuimicos.BtAlteraClick(Sender: TObject);
var
  GruposQuimicos : Integer;
begin
  GruposQuimicos := QrGruposQuimicosCodigo.Value;
  if not UMyMod.SelLockY(GruposQuimicos, Dmod.MyDB, 'GruposQuimicos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(GruposQuimicos, Dmod.MyDB, 'GruposQuimicos', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmGruposQuimicos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGruposQuimicosCodigo.Value;
  Close;
end;

procedure TFmGruposQuimicos.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descrição.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO gruposquimicos SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'GruposQuimicos', 'GruposQuimicos', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE gruposquimicos SET ');
    Codigo := QrGruposQuimicosCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Nome=:P0, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  //
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[03].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GruposQuimicos', 'Codigo');
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmGruposQuimicos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'GruposQuimicos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GruposQuimicos', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GruposQuimicos', 'Codigo');
end;

procedure TFmGruposQuimicos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  CriaOForm;
end;

procedure TFmGruposQuimicos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGruposQuimicosCodigo.Value,LaRegistro.Caption);
end;

procedure TFmGruposQuimicos.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmGruposQuimicos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGruposQuimicos.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmGruposQuimicos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGruposQuimicos.QrGruposQuimicosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  if QrGruposQuimicos.RecordCount = 0 then ReopenIts;
end;

procedure TFmGruposQuimicos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'GruposQuimicos', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmGruposQuimicos.QrGruposQuimicosAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrGruposQuimicosCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrGruposQuimicosCodigo.Value, False);
  ReopenIts;
end;

procedure TFmGruposQuimicos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGruposQuimicosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'GruposQuimicos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGruposQuimicos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGruposQuimicos.QrGruposQuimicosBeforeOpen(DataSet: TDataSet);
begin
  QrGruposQuimicosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmGruposQuimicos.ReopenIts;
begin
  QrIts.Close;
  QrIts.Params[0].AsInteger := QrGruposQuimicosCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrIts, Dmod.MyDB);
end;

procedure TFmGruposQuimicos.QrItsCalcFields(DataSet: TDataSet);
const
  Status: array[0..2] of String = ('Inativo', 'Amostra', 'Ativo');
begin
  QrItsNOMEATIVO.Value := Status[QrItsAtivo.Value];
end;

end.

