unit MPInAssina;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, DBCtrls, DmkDAC_PF,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmMPInAssina = class(TForm)
    Panel1: TPanel;
    StaticText1: TStaticText;
    QrAssinam: TmySQLQuery;
    QrAssinamCodigo: TIntegerField;
    QrAssinamNOMEENTI: TWideStringField;
    DsAssinam: TDataSource;
    EdAssina: TdmkEditCB;
    Label5: TLabel;
    CBAssina: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmMPInAssina: TFmMPInAssina;

implementation

uses UnMyObjects, MPIn, UMySQLModule, Module, UnInternalConsts, Principal, ModuleGeral;

{$R *.DFM}

procedure TFmMPInAssina.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPInAssina.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPInAssina.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPInAssina.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  if VAR_CADASTRO <> 0 then
  begin
    EdAssina.Text     := IntToStr(VAR_CADASTRO);
    CBAssina.KeyValue := VAR_CADASTRO;
  end;
end;

procedure TFmMPInAssina.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := FmMPIn.QrMPInControle.Value;
  while not FmMPIn.QrMPIn.Eof do
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'MPIn', False,
    ['QuemAssina'
    ], ['Controle'],
    [Geral.IMV(EdAssina.Text)
    ], [FmMPIn.QrMPInControle.Value], True);
    //
    FmMPIn.QrMPIn.Next;
  end;
  FmMPIn.ReopenPRi(qqMPIn, Controle);
  Close;
end;

procedure TFmMPInAssina.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrAssinam, Dmod.MyDB);
end;

end.
