object FmPQEEdit2: TFmPQEEdit2
  Left = 368
  Top = 178
  Caption = 'QUI-ENTRA-003 :: Item de Entrada de Uso e Consumo'
  ClientHeight = 751
  ClientWidth = 1067
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1067
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Painel1: TPanel
      Left = 0
      Top = 0
      Width = 1067
      Height = 29
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 76
        Top = 8
        Width = 29
        Height = 13
        Caption = 'Linha:'
      end
      object EdCodigo: TdmkEdit
        Left = 112
        Top = 4
        Width = 57
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object PainelDados: TPanel
      Left = 0
      Top = 29
      Width = 1067
      Height = 152
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 76
        Top = 8
        Width = 131
        Height = 13
        Caption = 'Produto: [F4 - Mostra todos]'
      end
      object Label14: TLabel
        Left = 76
        Top = 48
        Width = 43
        Height = 13
        Caption = 'Volumes:'
      end
      object Label15: TLabel
        Left = 172
        Top = 48
        Width = 83
        Height = 13
        Caption = 'Qtde l'#237'quido/vol.:'
      end
      object Label10: TLabel
        Left = 76
        Top = 88
        Width = 50
        Height = 13
        Caption = 'Valor Item:'
      end
      object Label19: TLabel
        Left = 296
        Top = 48
        Width = 86
        Height = 13
        Caption = 'Total qtde l'#237'quido:'
      end
      object Label5: TLabel
        Left = 172
        Top = 88
        Width = 64
        Height = 13
        Caption = 'Valor unit'#225'rio:'
      end
      object SpeedButton1: TSpeedButton
        Left = 556
        Top = 24
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object Label3: TLabel
        Left = 392
        Top = 48
        Width = 76
        Height = 13
        Caption = 'Qtde bruto/Vol.:'
      end
      object Label6: TLabel
        Left = 488
        Top = 48
        Width = 82
        Height = 13
        Caption = 'Total qtde  Bruto:'
      end
      object Label4: TLabel
        Left = 296
        Top = 88
        Width = 27
        Height = 13
        Caption = '% IPI:'
      end
      object Label7: TLabel
        Left = 392
        Top = 88
        Width = 25
        Height = 13
        Caption = '$ IPI:'
      end
      object Label8: TLabel
        Left = 488
        Top = 88
        Width = 61
        Height = 13
        Caption = '$ Custo item:'
      end
      object EdProduto: TdmkEditCB
        Left = 76
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdProdutoChange
        OnKeyDown = EdProdutoKeyDown
        DBLookupComboBox = CBProduto
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBProduto: TdmkDBLookupComboBox
        Left = 136
        Top = 24
        Width = 421
        Height = 21
        Color = clWhite
        KeyField = 'PQ'
        ListField = 'NOMEPQ'
        ListSource = DsPQ1
        TabOrder = 1
        OnKeyDown = CBProdutoKeyDown
        dmkEditCB = EdProduto
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdVolumes: TdmkEdit
        Left = 76
        Top = 64
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '1'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.000000000000000000
        ValWarn = False
        OnChange = EdVolumesChange
        OnExit = EdVolumesExit
      end
      object EdkgLiq: TdmkEdit
        Left = 172
        Top = 64
        Width = 120
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdkgLiqChange
        OnExit = EdkgLiqExit
      end
      object EdVlrItem: TdmkEdit
        Left = 76
        Top = 104
        Width = 92
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdVlrItemChange
      end
      object EdTotalkgLiq: TdmkEdit
        Left = 296
        Top = 64
        Width = 92
        Height = 21
        Alignment = taRightJustify
        Color = clWhite
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdTotalkgLiqChange
        OnExit = EdTotalkgLiqExit
      end
      object EdValorkg: TdmkEdit
        Left = 172
        Top = 104
        Width = 120
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 10
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkAmostra: TCheckBox
        Left = 76
        Top = 128
        Width = 70
        Height = 17
        Caption = #201' amostra.'
        TabOrder = 12
        Visible = False
      end
      object EdTotalkgBruto: TdmkEdit
        Left = 488
        Top = 64
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdTotalkgBrutoChange
      end
      object EdKgBruto: TdmkEdit
        Left = 392
        Top = 64
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdKgBrutoChange
      end
      object EdIPI_pIPI: TdmkEdit
        Left = 296
        Top = 104
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdIPI_pIPIChange
      end
      object EdIPI_vIPI: TdmkEdit
        Left = 392
        Top = 104
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdIPI_vIPIChange
      end
      object EdCustoItem: TdmkEdit
        Left = 488
        Top = 104
        Width = 92
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object REWarning: TRichEdit
        Left = 661
        Top = 0
        Width = 406
        Height = 152
        Align = alRight
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Lines.Strings = (
          'ATEN'#199#195'O:'
          
            'Verifique os c'#225'lculos de impostos. Caso estajam errados contate ' +
            'seu '
          'contador e informe-os manualmente.'
          'Informe a DERMATEK a forma correta de calcul'#225'-los.!!!')
        ParentFont = False
        ReadOnly = True
        TabOrder = 13
        Zoom = 100
      end
    end
    object Panel25: TPanel
      Left = 0
      Top = 181
      Width = 1067
      Height = 414
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1067
        Height = 46
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label231: TLabel
          Left = 160
          Top = 4
          Width = 23
          Height = 13
          Caption = 'Item:'
        end
        object Label235: TLabel
          Left = 804
          Top = 4
          Width = 27
          Height = 13
          Caption = 'NCM:'
        end
        object Label236: TLabel
          Left = 872
          Top = 4
          Width = 37
          Height = 13
          Caption = 'EXTIPI:'
        end
        object Label237: TLabel
          Left = 920
          Top = 4
          Width = 38
          Height = 13
          Caption = 'Genero:'
        end
        object Label238: TLabel
          Left = 964
          Top = 4
          Width = 31
          Height = 13
          Caption = 'CFOP:'
        end
        object Label13: TLabel
          Left = 112
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          Enabled = False
        end
        object Label12: TLabel
          Left = 44
          Top = 4
          Width = 46
          Height = 13
          Caption = 'Fat.Num.:'
          Enabled = False
        end
        object Label9: TLabel
          Left = 8
          Top = 4
          Width = 29
          Height = 13
          Caption = 'FatID:'
          Enabled = False
        end
        object Label232: TLabel
          Left = 196
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label234: TLabel
          Left = 280
          Top = 4
          Width = 157
          Height = 13
          Caption = 'Descri'#231#227'o do produto ou servi'#231'o:'
        end
        object SpeedButton2: TSpeedButton
          Left = 254
          Top = 20
          Width = 23
          Height = 22
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object EdnItem: TdmkEdit
          Left = 160
          Top = 20
          Width = 32
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          QryName = 'QrNFeItsI'
          QryCampo = 'nItem'
          UpdCampo = 'nItem'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object Edprod_NCM: TdmkEdit
          Left = 804
          Top = 20
          Width = 64
          Height = 21
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 8
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00000000'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_NCM'
          UpdCampo = 'prod_NCM'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '00000000'
          ValWarn = False
        end
        object Edprod_EXTIPI: TdmkEdit
          Left = 872
          Top = 20
          Width = 43
          Height = 21
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_EXTIPI'
          UpdCampo = 'prod_EXTIPI'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0'
          ValWarn = False
        end
        object Edprod_genero: TdmkEdit
          Left = 920
          Top = 20
          Width = 41
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_genero'
          UpdCampo = 'prod_genero'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object Edprod_CFOP: TdmkEdit
          Left = 964
          Top = 20
          Width = 32
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_CFOP'
          UpdCampo = 'prod_CFOP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdFatNum: TdmkEdit
          Left = 44
          Top = 20
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrNFeItsI'
          QryCampo = 'FatNum'
          UpdCampo = 'FatNum'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdEmpresa: TdmkEdit
          Left = 112
          Top = 20
          Width = 45
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrNFeItsI'
          QryCampo = 'Empresa'
          UpdCampo = 'Empresa'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdFatID: TdmkEdit
          Left = 8
          Top = 20
          Width = 33
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrNFeItsI'
          QryCampo = 'FatID'
          UpdCampo = 'FatID'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object Edprod_cProd: TdmkEdit
          Left = 196
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_cProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object Edprod_xProd: TdmkEdit
          Left = 280
          Top = 20
          Width = 521
          Height = 21
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_xProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 46
        Width = 1067
        Height = 62
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox11: TGroupBox
          Left = 0
          Top = 0
          Width = 684
          Height = 62
          Align = alLeft
          Caption = ' Dados comerciais: '
          TabOrder = 0
          object Label242: TLabel
            Left = 344
            Top = 16
            Width = 54
            Height = 13
            Caption = 'Valor bruto:'
          end
          object Label241: TLabel
            Left = 240
            Top = 16
            Width = 40
            Height = 13
            Caption = '$ Pre'#231'o:'
          end
          object Label240: TLabel
            Left = 156
            Top = 16
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label239: TLabel
            Left = 100
            Top = 16
            Width = 43
            Height = 13
            Caption = 'Unidade:'
          end
          object Label233: TLabel
            Left = 4
            Top = 16
            Width = 78
            Height = 13
            Caption = 'GTIN, EAN, etc.'
          end
          object Label247: TLabel
            Left = 452
            Top = 16
            Width = 36
            Height = 13
            Caption = '$ Frete:'
          end
          object Label248: TLabel
            Left = 528
            Top = 16
            Width = 46
            Height = 13
            Caption = '$ Seguro:'
          end
          object Label249: TLabel
            Left = 604
            Top = 16
            Width = 58
            Height = 13
            Caption = '$ Desconto:'
          end
          object Edprod_qCom: TdmkEdit
            Left = 156
            Top = 32
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_qCom'
            UpdCampo = 'prod_qCom'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object Edprod_vProd: TdmkEdit
            Left = 344
            Top = 32
            Width = 104
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_vProd'
            UpdCampo = 'prod_vProd'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object Edprod_vUnCom: TdmkEdit
            Left = 240
            Top = 32
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 10
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000000000'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_vUnCom'
            UpdCampo = 'prod_vUnCom'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object Edprod_uCom: TdmkEdit
            Left = 100
            Top = 32
            Width = 54
            Height = 21
            MaxLength = 6
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_uCom'
            UpdCampo = 'prod_uCom'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object Edprod_cEAN: TdmkEdit
            Left = 4
            Top = 32
            Width = 92
            Height = 21
            MaxLength = 14
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_cEAN'
            UpdCampo = 'prod_cEAN'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object Edprod_vFrete: TdmkEdit
            Left = 452
            Top = 32
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_vFrete'
            UpdCampo = 'prod_vFrete'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object Edprod_vSeg: TdmkEdit
            Left = 528
            Top = 32
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_vSeg'
            UpdCampo = 'prod_vSeg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object Edprod_vDesc: TdmkEdit
            Left = 604
            Top = 32
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_vDesc'
            UpdCampo = 'prod_vDesc'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox12: TGroupBox
          Left = 684
          Top = 0
          Width = 383
          Height = 62
          Align = alClient
          Caption = ' Dados tribut'#225'rios: '
          TabOrder = 1
          object Label001: TLabel
            Left = 4
            Top = 16
            Width = 78
            Height = 13
            Caption = 'GTIN, EAN, etc.'
          end
          object Label11: TLabel
            Left = 100
            Top = 16
            Width = 43
            Height = 13
            Caption = 'Unidade:'
          end
          object Label16: TLabel
            Left = 156
            Top = 16
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label17: TLabel
            Left = 240
            Top = 16
            Width = 31
            Height = 13
            Caption = 'Pre'#231'o:'
          end
          object Edprod_qTrib: TdmkEdit
            Left = 156
            Top = 32
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_qTrib'
            UpdCampo = 'prod_qTrib'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object Edprod_vUnTrib: TdmkEdit
            Left = 240
            Top = 32
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 10
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000000000'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_vUnTrib'
            UpdCampo = 'prod_vUnTrib'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object Edprod_uTrib: TdmkEdit
            Left = 100
            Top = 32
            Width = 54
            Height = 21
            MaxLength = 6
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_uTrib'
            UpdCampo = 'prod_uTrib'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object Edprod_cEANTrib: TdmkEdit
            Left = 4
            Top = 32
            Width = 92
            Height = 21
            MaxLength = 14
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_cEANTrib'
            UpdCampo = 'prod_cEANTrib'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object PageControl3: TPageControl
        Left = 0
        Top = 108
        Width = 1067
        Height = 306
        ActivePage = TabSheet24
        Align = alClient
        TabOrder = 2
        object TabSheet18: TTabSheet
          Caption = ' ICMS '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 1059
            Height = 278
            Align = alClient
            TabOrder = 0
            object Panel31: TPanel
              Left = 1
              Top = 1
              Width = 1057
              Height = 276
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object GroupBox13: TGroupBox
                Left = 0
                Top = 0
                Width = 818
                Height = 276
                Align = alClient
                Caption = 
                  ' CST - ICMS (Imposto sobre Circula'#231#227'o de Mercadorias e Servi'#231'os)' +
                  ': '
                TabOrder = 0
                object GroupBox14: TGroupBox
                  Left = 2
                  Top = 101
                  Width = 814
                  Height = 64
                  Align = alTop
                  Caption = ' ICMS Normal: '
                  TabOrder = 1
                  object STAviso1: TStaticText
                    Left = 465
                    Top = 15
                    Width = 347
                    Height = 47
                    Align = alClient
                    Alignment = taCenter
                    AutoSize = False
                    BevelInner = bvNone
                    BevelOuter = bvSpace
                    BorderStyle = sbsSunken
                    Caption = '...'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clPurple
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 0
                  end
                  object Panel7: TPanel
                    Left = 2
                    Top = 15
                    Width = 463
                    Height = 47
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 1
                    object LaICMS_pRedBC: TLabel
                      Left = 368
                      Top = 8
                      Width = 83
                      Height = 13
                      Caption = 'N14: % Red. BC'#178':'
                    end
                    object RGICMS_modBC: TdmkRadioGroup
                      Left = 0
                      Top = 0
                      Width = 359
                      Height = 47
                      Align = alLeft
                      Caption = ' N13 - Modalidade de determina'#231#227'o da BC do ICMS: '
                      Columns = 2
                      ItemIndex = 3
                      Items.Strings = (
                        'Margem valor agregado (%)'
                        'Pauta (valor)'
                        'Pre'#231'o tabelado m'#225'ximo (valor)'
                        'Valor da opera'#231#227'o')
                      TabOrder = 0
                      QryName = 'QrNFeItsN'
                      QryCampo = 'ICMS_modBC'
                      UpdCampo = 'ICMS_modBC'
                      UpdType = utYes
                      OldValor = 0
                    end
                    object EdICMS_pRedBC: TdmkEdit
                      Left = 367
                      Top = 22
                      Width = 84
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      QryName = 'QrNFeItsN'
                      QryCampo = 'ICMS_pRedBC'
                      UpdCampo = 'ICMS_pRedBC'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                  end
                end
                object Panel38: TPanel
                  Left = 2
                  Top = 15
                  Width = 814
                  Height = 86
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label244: TLabel
                    Left = 8
                    Top = 4
                    Width = 156
                    Height = 13
                    Caption = 'N11 - Origem da mercadoria: [F3]'
                  end
                  object Label245: TLabel
                    Left = 8
                    Top = 44
                    Width = 202
                    Height = 13
                    Caption = 'N12 - C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                  end
                  object EdICMS_Orig: TdmkEdit
                    Left = 8
                    Top = 20
                    Width = 45
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ValMax = '2'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_Orig'
                    UpdCampo = 'ICMS_Orig'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object EdTextoA: TdmkEdit
                    Left = 56
                    Top = 20
                    Width = 681
                    Height = 21
                    ReadOnly = True
                    TabOrder = 1
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object EdICMS_CST: TdmkEdit
                    Left = 8
                    Top = 61
                    Width = 45
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 2
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ValMax = '90'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '00'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_CST'
                    UpdCampo = 'ICMS_CST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object EdTextoB: TdmkEdit
                    Left = 56
                    Top = 61
                    Width = 681
                    Height = 21
                    ReadOnly = True
                    TabOrder = 3
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                end
                object GroupBox15: TGroupBox
                  Left = 2
                  Top = 165
                  Width = 814
                  Height = 64
                  Align = alTop
                  Caption = ' ICMS ST ( Substitui'#231#227'o Tribut'#225'ria): '
                  TabOrder = 2
                  object LaICMS_pMVAST: TLabel
                    Left = 372
                    Top = 20
                    Width = 86
                    Height = 13
                    Caption = 'N19: % MVA. BC'#178':'
                  end
                  object LaICMS_pRedBCST: TLabel
                    Left = 460
                    Top = 20
                    Width = 83
                    Height = 13
                    Caption = 'N20: % Red. BC'#178':'
                  end
                  object RGICMS_modBCST: TdmkRadioGroup
                    Left = 2
                    Top = 15
                    Width = 359
                    Height = 47
                    Align = alLeft
                    Caption = ' N18 - Modalidade de determina'#231#227'o da BC do ICMS: '
                    Columns = 2
                    ItemIndex = 3
                    Items.Strings = (
                      'Margem valor agregado (%)'
                      'Pauta (valor)'
                      'Pre'#231'o tabelado m'#225'ximo (valor)'
                      'Valor da opera'#231#227'o')
                    TabOrder = 0
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_modBCST'
                    UpdCampo = 'ICMS_modBCST'
                    UpdType = utYes
                    OldValor = 0
                  end
                  object EdICMS_pRedBCST: TdmkEdit
                    Left = 460
                    Top = 36
                    Width = 84
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_pRedBCST'
                    UpdCampo = 'ICMS_pRedBCST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMS_pMVAST: TdmkEdit
                    Left = 372
                    Top = 36
                    Width = 84
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_pMVAST'
                    UpdCampo = 'ICMS_pMVAST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
              end
              object GroupBox16: TGroupBox
                Left = 818
                Top = 0
                Width = 119
                Height = 276
                Align = alRight
                Caption = ' ICMS:'
                TabOrder = 1
                object LaICMS_VBC: TLabel
                  Left = 8
                  Top = 60
                  Width = 59
                  Height = 13
                  Caption = 'Valor da BC:'
                end
                object LaICMS_pICMS: TLabel
                  Left = 8
                  Top = 20
                  Width = 40
                  Height = 13
                  Caption = '% ICMS:'
                end
                object Label258: TLabel
                  Left = 8
                  Top = 100
                  Width = 56
                  Height = 13
                  Caption = 'Valor ICMS:'
                end
                object EdICMS_vBC: TdmkEdit
                  Left = 8
                  Top = 76
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsN'
                  QryCampo = 'ICMS_vBC'
                  UpdCampo = 'ICMS_vBC'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdICMS_vICMS: TdmkEdit
                  Left = 8
                  Top = 116
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsN'
                  QryCampo = 'ICMS_vICMS'
                  UpdCampo = 'ICMS_vICMS'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdICMS_pICMS: TdmkEdit
                  Left = 8
                  Top = 36
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsN'
                  QryCampo = 'ICMS_pICMS'
                  UpdCampo = 'ICMS_pICMS'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object GroupBox17: TGroupBox
                Left = 937
                Top = 0
                Width = 120
                Height = 276
                Align = alRight
                Caption = 'ICMS ST:'
                TabOrder = 2
                object Label259: TLabel
                  Left = 8
                  Top = 60
                  Width = 76
                  Height = 13
                  Caption = 'Valor da BC ST:'
                end
                object LaICMS_pICMSST: TLabel
                  Left = 8
                  Top = 20
                  Width = 57
                  Height = 13
                  Caption = '% ICMS ST:'
                end
                object Label261: TLabel
                  Left = 8
                  Top = 100
                  Width = 73
                  Height = 13
                  Caption = 'Valor ICMS ST:'
                end
                object EdICMS_vBCST: TdmkEdit
                  Left = 8
                  Top = 76
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsN'
                  QryCampo = 'ICMS_vBCST'
                  UpdCampo = 'ICMS_vBCST'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdICMS_pICMSST: TdmkEdit
                  Left = 8
                  Top = 36
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsN'
                  QryCampo = 'ICMS_pICMSST'
                  UpdCampo = 'ICMS_pICMSST'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdICMS_vICMSST: TdmkEdit
                  Left = 8
                  Top = 116
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsN'
                  QryCampo = 'ICMS_vICMSST'
                  UpdCampo = 'ICMS_vICMSST'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
        end
        object TabSheet19: TTabSheet
          Caption = ' IPI '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel34: TPanel
            Left = 0
            Top = 0
            Width = 1059
            Height = 278
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel39: TPanel
              Left = 0
              Top = 0
              Width = 1059
              Height = 278
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object GroupBox18: TGroupBox
                Left = 0
                Top = 0
                Width = 1059
                Height = 61
                Align = alTop
                Caption = ' CST - IPI (Imposto sobre produtos industrializados): '
                TabOrder = 0
                object Label264: TLabel
                  Left = 8
                  Top = 16
                  Width = 199
                  Height = 13
                  Caption = 'O09: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                end
                object EdIPI_CST: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 45
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '99'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryName = 'QrNFeItsO'
                  QryCampo = 'IPI_CST'
                  UpdCampo = 'IPI_CST'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdTextoIPI_CST: TdmkEdit
                  Left = 56
                  Top = 32
                  Width = 613
                  Height = 21
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object GroupBox19: TGroupBox
                Left = 0
                Top = 141
                Width = 1059
                Height = 137
                Align = alClient
                Caption = ' Outras informa'#231#245'es: '
                TabOrder = 2
                ExplicitHeight = 140
                object Label265: TLabel
                  Left = 12
                  Top = 24
                  Width = 299
                  Height = 13
                  Caption = 'O02: Classe de enquadramento de IPI para cigarros e bebidas: '
                end
                object Label266: TLabel
                  Left = 12
                  Top = 48
                  Width = 327
                  Height = 13
                  Caption = 
                    'O03: CNPJ do produtor da mercadoria quando diferente do emitente' +
                    '. '
                end
                object Label267: TLabel
                  Left = 12
                  Top = 72
                  Width = 177
                  Height = 13
                  Caption = 'O04: C'#243'digo do seleo de controle IPI:'
                end
                object Label268: TLabel
                  Left = 12
                  Top = 96
                  Width = 177
                  Height = 13
                  Caption = 'O05: Quantidade de selo de controle:'
                end
                object Label273: TLabel
                  Left = 452
                  Top = 48
                  Width = 248
                  Height = 13
                  Caption = 'Somente nos casos de exporta'#231#227'o direta ou indireta.'
                end
                object EdIPI_clEnq: TdmkEdit
                  Left = 344
                  Top = 21
                  Width = 104
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrNFeItsO'
                  QryCampo = 'IPI_clEnq'
                  UpdCampo = 'IPI_clEnq'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdIPI_CNPJProd: TdmkEdit
                  Left = 344
                  Top = 45
                  Width = 104
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrNFeItsO'
                  QryCampo = 'IPI_CNPJProd'
                  UpdCampo = 'IPI_CNPJProd'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdIPI_cSelo: TdmkEdit
                  Left = 344
                  Top = 69
                  Width = 104
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrNFeItsO'
                  QryCampo = 'IPI_cSelo'
                  UpdCampo = 'IPI_cSelo'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdIPI_qSelo: TdmkEdit
                  Left = 344
                  Top = 93
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryName = 'QrNFeItsO'
                  QryCampo = 'IPI_qSelo'
                  UpdCampo = 'IPI_qSelo'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object Panel40: TPanel
                Left = 0
                Top = 61
                Width = 1059
                Height = 80
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object Label251: TLabel
                  Left = 332
                  Top = 8
                  Width = 158
                  Height = 13
                  Caption = 'O12: Valor por unidade tribut'#225'vel:'
                end
                object Label262: TLabel
                  Left = 16
                  Top = 56
                  Width = 109
                  Height = 13
                  Caption = 'O13: % Aliquota de IPI:'
                end
                object Label263: TLabel
                  Left = 16
                  Top = 8
                  Width = 119
                  Height = 13
                  Caption = 'O06: C'#243'd. enq. legal IPI'#185':'
                end
                object Label269: TLabel
                  Left = 16
                  Top = 32
                  Width = 116
                  Height = 13
                  Caption = 'O10: Valor da BC do IPI:'
                end
                object Label270: TLabel
                  Left = 332
                  Top = 32
                  Width = 273
                  Height = 13
                  Caption = 'O11: Quantidade total na unidade padr'#227'o para tributa'#231#227'o:'
                end
                object Label271: TLabel
                  Left = 332
                  Top = 56
                  Width = 84
                  Height = 13
                  Caption = 'O14: Valor do IPI:'
                end
                object Label272: TLabel
                  Left = 720
                  Top = 32
                  Width = 222
                  Height = 13
                  Caption = '(somente para produtos tributados por unidade)'
                end
                object EdIPI_cEnq: TdmkEdit
                  Left = 180
                  Top = 4
                  Width = 104
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '999'
                  QryName = 'QrNFeItsO'
                  QryCampo = 'IPI_cEnq'
                  UpdCampo = 'IPI_cEnq'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = '999'
                  ValWarn = False
                end
                object dmkEdit1: TdmkEdit
                  Left = 180
                  Top = 52
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsO'
                  QryCampo = 'IPI_pIPI'
                  UpdCampo = 'IPI_pIPI'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdIPI_pIPIChange
                end
                object EdIPI_vUnid: TdmkEdit
                  Left = 612
                  Top = 4
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryName = 'QrNFeItsO'
                  QryCampo = 'IPI_vUnid'
                  UpdCampo = 'IPI_vUnid'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdIPI_vBC: TdmkEdit
                  Left = 180
                  Top = 28
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsO'
                  QryCampo = 'IPI_vBC'
                  UpdCampo = 'IPI_vBC'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdIPI_qUnid: TdmkEdit
                  Left = 612
                  Top = 28
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryName = 'QrNFeItsO'
                  QryCampo = 'IPI_qUnid'
                  UpdCampo = 'IPI_qUnid'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object dmkEdit2: TdmkEdit
                  Left = 612
                  Top = 52
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsO'
                  QryCampo = 'IPI_vIPI'
                  UpdCampo = 'IPI_vIPI'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
        end
        object TabSheet20: TTabSheet
          Caption = ' II '
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 281
          object Panel33: TPanel
            Left = 0
            Top = 0
            Width = 1059
            Height = 278
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitHeight = 281
            object Label301: TLabel
              Left = 12
              Top = 12
              Width = 59
              Height = 13
              Caption = 'Valor da BC:'
            end
            object Label302: TLabel
              Left = 12
              Top = 36
              Width = 145
              Height = 13
              Caption = 'Valor das depesas aduaneiras:'
            end
            object Label303: TLabel
              Left = 12
              Top = 60
              Width = 151
              Height = 13
              Caption = 'Valor do imposto de importa'#231#227'o:'
            end
            object Label304: TLabel
              Left = 12
              Top = 84
              Width = 62
              Height = 13
              Caption = 'Valor do IOF:'
            end
            object EdII_vBC: TdmkEdit
              Left = 180
              Top = 8
              Width = 104
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsP'
              QryCampo = 'II_vBC'
              UpdCampo = 'II_vBC'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdII_vDespAdu: TdmkEdit
              Left = 180
              Top = 32
              Width = 104
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsP'
              QryCampo = 'II_vDespAdu'
              UpdCampo = 'II_vDespAdu'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdII_vII: TdmkEdit
              Left = 180
              Top = 56
              Width = 104
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsP'
              QryCampo = 'II_vII'
              UpdCampo = 'II_vII'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdII_vIOF: TdmkEdit
              Left = 180
              Top = 80
              Width = 104
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsP'
              QryCampo = 'II_vIOF'
              UpdCampo = 'II_vIOF'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
        object TabSheet21: TTabSheet
          Caption = ' PIS '
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 281
          object Panel32: TPanel
            Left = 0
            Top = 0
            Width = 1059
            Height = 278
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitHeight = 281
            object GroupBox20: TGroupBox
              Left = 0
              Top = 0
              Width = 1059
              Height = 129
              Align = alTop
              Caption = ' CST - PIS (Programa de Integra'#231#227'o Social): '
              TabOrder = 0
              object Label274: TLabel
                Left = 8
                Top = 16
                Width = 199
                Height = 13
                Caption = 'Q06: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
              end
              object Label281: TLabel
                Left = 560
                Top = 72
                Width = 88
                Height = 13
                Caption = 'Q09: Valor do PIS:'
              end
              object EdTextoPIS_CST: TdmkEdit
                Left = 56
                Top = 32
                Width = 613
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdPIS_CST: TdmkEdit
                Left = 8
                Top = 32
                Width = 45
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 2
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ValMax = '99'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '01'
                QryName = 'QrNFeItsQ'
                QryCampo = 'PIS_CST'
                UpdCampo = 'PIS_CST'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1
                ValWarn = False
              end
              object GroupBox22: TGroupBox
                Left = 8
                Top = 56
                Width = 268
                Height = 65
                Caption = ' Por Al'#237'quota: '
                TabOrder = 2
                object Label279: TLabel
                  Left = 8
                  Top = 16
                  Width = 85
                  Height = 13
                  Caption = 'Q07: Valor da BC:'
                end
                object Label276: TLabel
                  Left = 136
                  Top = 16
                  Width = 86
                  Height = 13
                  Caption = 'Q08: Aliq. PIS (%):'
                end
                object EdPIS_vBC: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsQ'
                  QryCampo = 'PIS_vBC'
                  UpdCampo = 'PIS_vBC'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPIS_pPIS: TdmkEdit
                  Left = 136
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsQ'
                  QryCampo = 'PIS_pPIS'
                  UpdCampo = 'PIS_pPIS'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object GroupBox23: TGroupBox
                Left = 280
                Top = 56
                Width = 268
                Height = 65
                Caption = ' Por Unidade: '
                TabOrder = 3
                object Label275: TLabel
                  Left = 136
                  Top = 16
                  Width = 95
                  Height = 13
                  Caption = 'Q11: % Aliq. PIS ($):'
                end
                object Label282: TLabel
                  Left = 8
                  Top = 16
                  Width = 93
                  Height = 13
                  Caption = 'Q10: Qtde vendida:'
                end
                object EdPIS_qBCProd: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryName = 'QrNFeItsQ'
                  QryCampo = 'PIS_qBCProd'
                  UpdCampo = 'PIS_qBCProd'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPIS_vAliqProd: TdmkEdit
                  Left = 136
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryName = 'QrNFeItsQ'
                  QryCampo = 'PIS_vAliqProd'
                  UpdCampo = 'PIS_vAliqProd'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object EdPIS_vPIS: TdmkEdit
                Left = 560
                Top = 88
                Width = 104
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsQ'
                QryCampo = 'PIS_vPIS'
                UpdCampo = 'PIS_vPIS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
            object GroupBox21: TGroupBox
              Left = 0
              Top = 129
              Width = 1059
              Height = 149
              Align = alClient
              Caption = ' PIS ST: '
              TabOrder = 1
              ExplicitHeight = 152
              object Label284: TLabel
                Left = 560
                Top = 32
                Width = 88
                Height = 13
                Caption = 'R06: Valor do PIS:'
              end
              object GroupBox24: TGroupBox
                Left = 8
                Top = 16
                Width = 268
                Height = 65
                Caption = ' Por Al'#237'quota: '
                TabOrder = 0
                object Label277: TLabel
                  Left = 8
                  Top = 16
                  Width = 85
                  Height = 13
                  Caption = 'R02: Valor da BC:'
                end
                object Label278: TLabel
                  Left = 136
                  Top = 16
                  Width = 86
                  Height = 13
                  Caption = 'R03: Aliq. PIS (%):'
                end
                object EdPISST_vBC: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsQ'
                  QryCampo = 'PISST_vBC'
                  UpdCampo = 'PISST_vBC'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPISST_pPIS: TdmkEdit
                  Left = 136
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsQ'
                  QryCampo = 'PISST_pPIS'
                  UpdCampo = 'PISST_pPIS'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object GroupBox25: TGroupBox
                Left = 280
                Top = 16
                Width = 268
                Height = 65
                Caption = ' Por Unidade: '
                TabOrder = 1
                object Label280: TLabel
                  Left = 136
                  Top = 16
                  Width = 95
                  Height = 13
                  Caption = 'R05: % Aliq. PIS ($):'
                end
                object Label283: TLabel
                  Left = 8
                  Top = 16
                  Width = 93
                  Height = 13
                  Caption = 'R04: Qtde vendida:'
                end
                object EdPISST_qBCProd: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryName = 'QrNFeItsQ'
                  QryCampo = 'PISST_qBCProd'
                  UpdCampo = 'PISST_qBCProd'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPISST_vAliqProd: TdmkEdit
                  Left = 136
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryName = 'QrNFeItsQ'
                  QryCampo = 'PISST_vAliqProd'
                  UpdCampo = 'PISST_vAliqProd'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object EdPISST_vPIS: TdmkEdit
                Left = 560
                Top = 48
                Width = 104
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsQ'
                QryCampo = 'PISST_vPIS'
                UpdCampo = 'PISST_vPIS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
        end
        object TabSheet22: TTabSheet
          Caption = ' COFINS '
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 299
          object Panel35: TPanel
            Left = 0
            Top = 0
            Width = 1059
            Height = 278
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitHeight = 299
            object Panel41: TPanel
              Left = 0
              Top = 0
              Width = 1059
              Height = 278
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitHeight = 299
              object GroupBox26: TGroupBox
                Left = 0
                Top = 0
                Width = 1059
                Height = 129
                Align = alTop
                Caption = ' CST - COFINS (Programa de Integra'#231#227'o Social): '
                TabOrder = 0
                object Label285: TLabel
                  Left = 8
                  Top = 16
                  Width = 199
                  Height = 13
                  Caption = 'Q06: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                end
                object Label286: TLabel
                  Left = 560
                  Top = 72
                  Width = 110
                  Height = 13
                  Caption = 'Q09: Valor do COFINS:'
                end
                object EdTextoCOFINS_CST: TdmkEdit
                  Left = 56
                  Top = 32
                  Width = 613
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrNFeItsS'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCOFINS_CST: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 45
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 2
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '1'
                  ValMax = '99'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '01'
                  QryName = 'QrNFeItsS'
                  QryCampo = 'COFINS_CST'
                  UpdCampo = 'COFINS_CST'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1
                  ValWarn = False
                end
                object GroupBox27: TGroupBox
                  Left = 8
                  Top = 56
                  Width = 268
                  Height = 65
                  Caption = ' Por Al'#237'quota: '
                  TabOrder = 2
                  object Label287: TLabel
                    Left = 8
                    Top = 16
                    Width = 85
                    Height = 13
                    Caption = 'Q07: Valor da BC:'
                  end
                  object Label288: TLabel
                    Left = 136
                    Top = 16
                    Width = 108
                    Height = 13
                    Caption = 'Q08: Aliq. COFINS (%):'
                  end
                  object EdCOFINS_vBC: TdmkEdit
                    Left = 8
                    Top = 32
                    Width = 124
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsS'
                    QryCampo = 'COFINS_vBC'
                    UpdCampo = 'COFINS_vBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdCOFINS_pCOFINS: TdmkEdit
                    Left = 136
                    Top = 32
                    Width = 124
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsS'
                    QryCampo = 'COFINS_pCOFINS'
                    UpdCampo = 'COFINS_pCOFINS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
                object GroupBox28: TGroupBox
                  Left = 280
                  Top = 56
                  Width = 268
                  Height = 65
                  Caption = ' Por Unidade: '
                  TabOrder = 3
                  object Label289: TLabel
                    Left = 136
                    Top = 16
                    Width = 117
                    Height = 13
                    Caption = 'Q11: % Aliq. COFINS ($):'
                  end
                  object Label290: TLabel
                    Left = 8
                    Top = 16
                    Width = 93
                    Height = 13
                    Caption = 'Q10: Qtde vendida:'
                  end
                  object EdCOFINS_qBCProd: TdmkEdit
                    Left = 8
                    Top = 32
                    Width = 124
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 4
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,0000'
                    QryName = 'QrNFeItsS'
                    QryCampo = 'COFINS_qBCProd'
                    UpdCampo = 'COFINS_qBCProd'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdCOFINS_vAliqProd: TdmkEdit
                    Left = 136
                    Top = 32
                    Width = 124
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 4
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,0000'
                    QryName = 'QrNFeItsS'
                    QryCampo = 'COFINS_vAliqProd'
                    UpdCampo = 'COFINS_vAliqProd'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
                object EdCOFINS_vCOFINS: TdmkEdit
                  Left = 560
                  Top = 88
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsS'
                  QryCampo = 'COFINS_vCOFINS'
                  UpdCampo = 'COFINS_vCOFINS'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object GroupBox29: TGroupBox
                Left = 0
                Top = 129
                Width = 1059
                Height = 149
                Align = alClient
                Caption = ' COFINS ST: '
                TabOrder = 1
                ExplicitHeight = 170
                object Label291: TLabel
                  Left = 560
                  Top = 32
                  Width = 110
                  Height = 13
                  Caption = 'R06: Valor do COFINS:'
                end
                object GroupBox30: TGroupBox
                  Left = 8
                  Top = 16
                  Width = 268
                  Height = 65
                  Caption = ' Por Al'#237'quota: '
                  TabOrder = 0
                  object Label292: TLabel
                    Left = 8
                    Top = 16
                    Width = 85
                    Height = 13
                    Caption = 'R02: Valor da BC:'
                  end
                  object Label293: TLabel
                    Left = 136
                    Top = 16
                    Width = 108
                    Height = 13
                    Caption = 'R03: Aliq. COFINS (%):'
                  end
                  object EdCOFINSST_vBC: TdmkEdit
                    Left = 8
                    Top = 32
                    Width = 124
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsS'
                    QryCampo = 'COFINSST_vBC'
                    UpdCampo = 'COFINSST_vBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdCOFINSST_pCOFINS: TdmkEdit
                    Left = 136
                    Top = 32
                    Width = 124
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsS'
                    QryCampo = 'COFINSST_pCOFINS'
                    UpdCampo = 'COFINSST_pCOFINS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
                object GroupBox31: TGroupBox
                  Left = 280
                  Top = 16
                  Width = 268
                  Height = 65
                  Caption = ' Por Unidade: '
                  TabOrder = 1
                  object Label294: TLabel
                    Left = 136
                    Top = 16
                    Width = 117
                    Height = 13
                    Caption = 'R05: % Aliq. COFINS ($):'
                  end
                  object Label295: TLabel
                    Left = 8
                    Top = 16
                    Width = 93
                    Height = 13
                    Caption = 'R04: Qtde vendida:'
                  end
                  object EdCOFINSST_qBCProd: TdmkEdit
                    Left = 8
                    Top = 32
                    Width = 124
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 4
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,0000'
                    QryName = 'QrNFeItsS'
                    QryCampo = 'COFINSST_qBCProd'
                    UpdCampo = 'COFINSST_qBCProd'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdCOFINSST_vAliqProd: TdmkEdit
                    Left = 136
                    Top = 32
                    Width = 124
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 4
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,0000'
                    QryName = 'QrNFeItsS'
                    QryCampo = 'COFINSST_vAliqProd'
                    UpdCampo = 'COFINSST_vAliqProd'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
                object EdCOFINSST_vCOFINS: TdmkEdit
                  Left = 560
                  Top = 48
                  Width = 104
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsS'
                  QryCampo = 'COFINSST_vCOFINS'
                  UpdCampo = 'COFINSST_vCOFINS'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
        end
        object TabSheet24: TTabSheet
          Caption = ' Informa'#231#245'es adicionais do produto '
          ImageIndex = 8
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel37: TPanel
            Left = 0
            Top = 0
            Width = 1059
            Height = 278
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object MeInfAdProd: TdmkMemo
              Left = 0
              Top = 17
              Width = 1059
              Height = 261
              Align = alClient
              TabOrder = 0
              QryName = 'QrNFeItsV'
              QryCampo = 'InfAdProd'
              UpdCampo = 'InfAdProd'
              UpdType = utYes
            end
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 1059
              Height = 17
              Align = alTop
              BevelOuter = bvNone
              Caption = 
                'Para iniciar nova linha mantenha pressionado a tecla Ctrl e tecl' +
                'e enter.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1067
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1019
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 971
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 435
        Height = 32
        Caption = 'Item de Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 435
        Height = 32
        Caption = 'Item de Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 435
        Height = 32
        Caption = 'Item de Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 643
    Width = 1067
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 1063
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 687
    Width = 1067
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 1063
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 919
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrPQ1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Nome NOMEPQ, pfo.* '
      'FROM pqfor pfo'
      'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo'
      'WHERE pfo.CI=:P0'
      'AND pfo.IQ=:P1'
      'ORDER BY pq.Nome')
    Left = 260
    Top = 46
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
    object QrPQ1NOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQ1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQ1IQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQ1PQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrPQ1CI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQ1Prazo: TWideStringField
      FieldName = 'Prazo'
    end
    object QrPQ1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQ1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQ1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQ1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQ1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsPQ1: TDataSource
    DataSet = QrPQ1
    Left = 288
    Top = 46
  end
  object QrLocalizaPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM pq'
      'WHERE Codigo=:P0'
      'AND IQ=:P1')
    Left = 316
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
end
