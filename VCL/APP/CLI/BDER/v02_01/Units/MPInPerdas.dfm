object FmMPInPerdas: TFmMPInPerdas
  Left = 339
  Top = 185
  Caption = 'COU-MP_IN-008 :: Perda de Couros'
  ClientHeight = 251
  ClientWidth = 534
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 534
    Height = 95
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label8: TLabel
      Left = 8
      Top = 8
      Width = 28
      Height = 13
      Caption = 'Setor:'
    end
    object Label1: TLabel
      Left = 8
      Top = 48
      Width = 38
      Height = 13
      Caption = 'Motivo: '
    end
    object Label2: TLabel
      Left = 300
      Top = 8
      Width = 33
      Height = 13
      Caption = 'Pe'#231'as:'
    end
    object Label3: TLabel
      Left = 368
      Top = 8
      Width = 50
      Height = 13
      Caption = 'Data hora:'
    end
    object SpeedButton1: TSpeedButton
      Left = 272
      Top = 24
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 500
      Top = 64
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton2Click
    end
    object EdSetor: TdmkEditCB
      Left = 8
      Top = 24
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBSetor
      IgnoraDBLookupComboBox = False
    end
    object CBSetor: TdmkDBLookupComboBox
      Left = 64
      Top = 24
      Width = 205
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsListaSetores
      TabOrder = 1
      dmkEditCB = EdSetor
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdMotivo: TdmkEditCB
      Left = 8
      Top = 64
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBMotivo
      IgnoraDBLookupComboBox = False
    end
    object CBMotivo: TdmkDBLookupComboBox
      Left = 64
      Top = 64
      Width = 433
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsRolPerdas
      TabOrder = 5
      dmkEditCB = EdMotivo
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdPecas: TdmkEdit
      Left = 300
      Top = 24
      Width = 61
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 1
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdDataHora: TdmkEdit
      Left = 368
      Top = 24
      Width = 153
      Height = 21
      TabOrder = 3
      FormatType = dmktfDateTime
      MskType = fmtNone
      DecimalSize = 1
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '30/12/1899 00:00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 534
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 486
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 438
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 205
        Height = 32
        Caption = 'Perda de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 205
        Height = 32
        Caption = 'Perda de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 205
        Height = 32
        Caption = 'Perda de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 143
    Width = 534
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 530
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 187
    Width = 534
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 530
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 386
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrListaSetores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 8
    Top = 8
    object QrListaSetoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaSetoresNome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object DsListaSetores: TDataSource
    DataSet = QrListaSetores
    Left = 36
    Top = 8
  end
  object QrRolPerdas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM rolperdas'
      'ORDER BY Nome')
    Left = 64
    Top = 8
    object QrRolPerdasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRolPerdasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsRolPerdas: TDataSource
    DataSet = QrRolPerdas
    Left = 92
    Top = 8
  end
end
