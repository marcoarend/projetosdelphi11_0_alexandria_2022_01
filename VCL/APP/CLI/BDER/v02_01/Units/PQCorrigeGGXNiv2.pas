unit PQCorrigeGGXNiv2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGrid,
  Vcl.Menus, dmkDBLookupComboBox;

type
  TFmPQCorrigeGGXNiv2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAcao: TBitBtn;
    LaTitulo1C: TLabel;
    QrPQ: TmySQLQuery;
    DsPQ: TDataSource;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    QrPQGGXNiv2: TIntegerField;
    dmkDBGrid1: TdmkDBGrid;
    PMAcao: TPopupMenu;
    QrPQAtivo: TSmallintField;
    Corrigirgrupo1: TMenuItem;
    PnDados: TPanel;
    LaGrupo: TLabel;
    CBGrupoDest: TComboBox;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PB1: TProgressBar;
    LaAviso: TLabel;
    Panel5: TPanel;
    Label1: TLabel;
    CBGrupoOrig: TComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure PMAcaoPopup(Sender: TObject);
    procedure Corrigirgrupo1Click(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure CBGrupoOrigChange(Sender: TObject);
  private
    { Private declarations }
    procedure ConfiguraPnDados(Mostra: Boolean);
  public
    { Public declarations }
    function ReopenPQ(): Boolean;
  end;

  var
  FmPQCorrigeGGXNiv2: TFmPQCorrigeGGXNiv2;

implementation

uses UnMyObjects, UMySQLModule, Module, DmkDAC_PF, AppListas;

{$R *.DFM}

procedure TFmPQCorrigeGGXNiv2.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmPQCorrigeGGXNiv2.BtConfirmaClick(Sender: TObject);

  function AtualizaProduto(Grupo: Integer): Boolean;
  var
    Codigo, Nivel2: Integer;
  begin
    Result := False;
    Codigo := QrPQCodigo.Value;
    Nivel2 := Grupo * -1;
    //
    if Grupo > 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pq', False,
        ['GGXNiv2'], ['Codigo'], [Grupo], [Codigo], True);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False,
        ['Nivel2'], ['Nivel1'], [Nivel2], [Codigo], True);
      //
      Result := True;
    end;
  end;

var
  I, Grupo: Integer;
begin
  Grupo := CBGrupoDest.ItemIndex;
  //
  if MyObjects.FIC(Grupo <= 0, CBGrupoDest, 'Informe o grupo!') then Exit;
  //
  if dmkDBGrid1.SelectedRows.Count > 1 then
  begin
    with dmkDBGrid1.DataSource.DataSet do
    begin
      try
        PB1.Position := 0;
        PB1.Max      := dmkDBGrid1.SelectedRows.Count;
        //
        for I := 0 to dmkDBGrid1.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(dmkDBGrid1.SelectedRows.Items[I]));
          GotoBookmark(dmkDBGrid1.SelectedRows.Items[I]);
          //
          if AtualizaProduto(Grupo) then
          begin
            PB1.Position := PB1.Position + 1;
            PB1.Update;
            Application.ProcessMessages;
          end else
          begin
            Geral.MB_Erro('Falha ao atualizar grupo do produto!');
            Exit;
          end;
        end;
      finally
        PB1.Position := 0;
      end;
    end;
  end else
  begin
    if not AtualizaProduto(Grupo) then
      Geral.MB_Erro('Falha ao atualizar grupo do produto!');
  end;
  ReopenPQ;
  ConfiguraPnDados(False);
end;

procedure TFmPQCorrigeGGXNiv2.BtDesisteClick(Sender: TObject);
begin
  ConfiguraPnDados(False);
end;

procedure TFmPQCorrigeGGXNiv2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQCorrigeGGXNiv2.CBGrupoOrigChange(Sender: TObject);
begin
  LaAviso.Visible := CBGrupoOrig.ItemIndex = 0;
  ReopenPQ();
end;

procedure TFmPQCorrigeGGXNiv2.ConfiguraPnDados(Mostra: Boolean);
begin
  if Mostra then
    MyObjects.PreencheComponente(CBGrupoDest, sListaTipoCadPQ);
  //
  PB1.Position      := 0;
  PnDados.Visible   := Mostra;
  CBGrupoDest.ItemIndex := 0;
  //
  BtAcao.Enabled  := not Mostra;
  BtSaida.Enabled := not Mostra;
end;

procedure TFmPQCorrigeGGXNiv2.Corrigirgrupo1Click(Sender: TObject);
begin
  ConfiguraPnDados(True);
end;

procedure TFmPQCorrigeGGXNiv2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQCorrigeGGXNiv2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType       := stLok;
  dmkDBGrid1.DataSource := DsPQ;
  //
  MyObjects.PreencheComponente(CBGrupoOrig, sListaTipoCadPQ);
  CBGrupoOrig.ItemIndex := 0;
  ConfiguraPnDados(False);
end;

procedure TFmPQCorrigeGGXNiv2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQCorrigeGGXNiv2.PMAcaoPopup(Sender: TObject);
begin
  Corrigirgrupo1.Enabled := dmkDBGrid1.SelectedRows.Count > 0;
end;

function TFmPQCorrigeGGXNiv2.ReopenPQ(): Boolean;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQ, Dmod.MyDB, [
    'SELECT * ',
    'FROM pq ',
    'WHERE GGXNiv2=' + Geral.FF0(CBGrupoOrig.ItemIndex),
    'AND Codigo > 0 ',
    '']);
  if QrPQ.RecordCount > 0 then
    Result := True
  else
    QrPQ.Close;
end;

end.
