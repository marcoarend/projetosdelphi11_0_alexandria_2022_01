object FmPQSubstitui: TFmPQSubstitui
  Left = 339
  Top = 185
  Caption = 'QUI-INSUM-005 :: Substitui'#231#227'o de PQ em Receitas'
  ClientHeight = 774
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 464
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Substitui'#231#227'o de PQ em Receitas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 464
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Substitui'#231#227'o de PQ em Receitas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 464
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Substitui'#231#227'o de PQ em Receitas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 64
    Width = 1241
    Height = 570
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 570
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1241
        Height = 129
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        TabOrder = 0
        object PainelDados: TPanel
          Left = 2
          Top = 18
          Width = 1237
          Height = 109
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaOld: TLabel
            Left = 15
            Top = 0
            Width = 146
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Insumo a ser substitu'#237'do:'
          end
          object EdPQ: TdmkEditCB
            Left = 15
            Top = 20
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdPQChange
            DBLookupComboBox = CBPQ
            IgnoraDBLookupComboBox = False
          end
          object CBPQ: TdmkDBLookupComboBox
            Left = 84
            Top = 20
            Width = 901
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPQ
            TabOrder = 1
            dmkEditCB = EdPQ
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdClienteI: TdmkEditCB
            Left = 15
            Top = 74
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClienteIChange
            DBLookupComboBox = CBClienteI
            IgnoraDBLookupComboBox = False
          end
          object CBClienteI: TdmkDBLookupComboBox
            Left = 84
            Top = 74
            Width = 901
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCI
            TabOrder = 3
            dmkEditCB = EdClienteI
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CkClienteI: TCheckBox
            Left = 15
            Top = 49
            Width = 119
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente interno:'
            TabOrder = 4
            OnClick = CkClienteIClick
          end
        end
      end
      object PnPsq: TPanel
        Left = 0
        Top = 129
        Width = 1241
        Height = 441
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object GroupBox2: TGroupBox
          Left = 0
          Top = 50
          Width = 464
          Height = 391
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Ribeira:'
          TabOrder = 0
          object DBGrid1: TDBGrid
            Left = 2
            Top = 18
            Width = 460
            Height = 371
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsFormulasIts
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid1DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Numero'
                Title.Caption = 'Receita'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FRM'
                Title.Caption = 'Nome da receita'
                Width = 192
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ordem'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Porcent'
                Title.Caption = '% uso'
                Width = 60
                Visible = True
              end>
          end
        end
        object GroupBox3: TGroupBox
          Left = 464
          Top = 50
          Width = 777
          Height = 391
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = 'Acabamento:'
          TabOrder = 1
          object DBGrid2: TDBGrid
            Left = 2
            Top = 18
            Width = 773
            Height = 371
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsTintasIts
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid2DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Numero'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FRM'
                Title.Caption = 'Nome da receita'
                Width = 184
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TIN'
                Title.Caption = 'Nome da tinta'
                Width = 184
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ordem'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GramasTi'
                Title.Caption = 'g/vol'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GramasKg'
                Title.Caption = 'g/Kg'
                Width = 76
                Visible = True
              end>
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 1241
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object LaNew: TLabel
            Left = 15
            Top = 0
            Width = 105
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Insumo substituto:'
          end
          object Label4: TLabel
            Left = 994
            Top = 0
            Width = 104
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Propor'#231#227'o em %:'
          end
          object EdPrd: TdmkEditCB
            Left = 15
            Top = 20
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPrd
            IgnoraDBLookupComboBox = False
          end
          object CBPrd: TdmkDBLookupComboBox
            Left = 84
            Top = 20
            Width = 901
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPrd
            TabOrder = 1
            dmkEditCB = EdPrd
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdProporcao: TdmkEdit
            Left = 994
            Top = 20
            Width = 124
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 10
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '100,0000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 100.000000000000000000
            ValWarn = False
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 634
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 688
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1062
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1060
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 18
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtExecuta: TBitBtn
        Tag = 10
        Left = 167
        Top = 4
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Executa'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtExecutaClick
      end
    end
  end
  object QrPQ: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 460
    Top = 12
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 460
    Top = 60
  end
  object QrCI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END Nome'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 504
    Top = 12
    object QrCINome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.artigosgrupos.Nome'
      Size = 32
    end
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.artigosgrupos.Codigo'
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 504
    Top = 60
  end
  object QrFormulasIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT foi.Numero, foi.Ordem, foi.Controle, foi.Porcent,'
      'frm.Nome NO_FRM '
      'FROM formulasits foi'
      'LEFT JOIN formulas frm ON frm.Numero=foi.Numero'
      'WHERE foi.Produto=-2'
      'AND frm.ClienteI=-11'
      'ORDER BY foi.Numero, foi.Ordem')
    Left = 80
    Top = 256
    object QrFormulasItsNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrFormulasItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFormulasItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFormulasItsPorcent: TFloatField
      FieldName = 'Porcent'
      DisplayFormat = '#,###,##0.000'
    end
    object QrFormulasItsNO_FRM: TWideStringField
      FieldName = 'NO_FRM'
      Size = 30
    end
  end
  object DsFormulasIts: TDataSource
    DataSet = QrFormulasIts
    Left = 80
    Top = 304
  end
  object QrTintasIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT foi.Numero, foi.Codigo, foi.Ordem, foi.Controle, foi.Gram' +
        'asTi,'
      'foi.GramasKg, frm.Nome NO_FRM, tin.Nome NO_TIN '
      'FROM tintasits foi'
      'LEFT JOIN tintascab frm ON frm.Numero=foi.Numero'
      'LEFT JOIN tintastin tin ON tin.Codigo=foi.Codigo'
      'WHERE foi.Produto=-2'
      'AND frm.ClienteI=-11'
      'ORDER BY foi.Numero, foi.Ordem')
    Left = 156
    Top = 256
    object QrTintasItsNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrTintasItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrTintasItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTintasItsGramasTi: TFloatField
      FieldName = 'GramasTi'
      DisplayFormat = '#,###,##0.000'
    end
    object QrTintasItsGramasKg: TFloatField
      FieldName = 'GramasKg'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrTintasItsNO_FRM: TWideStringField
      FieldName = 'NO_FRM'
      Size = 100
    end
    object QrTintasItsNO_TIN: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIN'
      Size = 100
      Calculated = True
    end
    object QrTintasItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsTintasIts: TDataSource
    DataSet = QrTintasIts
    Left = 156
    Top = 304
  end
  object QrPrd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 544
    Top = 12
    object QrPrdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPrd: TDataSource
    DataSet = QrPrd
    Left = 544
    Top = 60
  end
  object frxQUI_INSUM_005_01: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41602.674881805560000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxQUI_INSUM_005_01GetValue
    Left = 80
    Top = 356
    Datasets = <
      item
        DataSet = frxDsAcaIts
        DataSetName = 'frxDsAcaIts'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsRibIts
        DataSetName = 'frxDsRibIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Insumos Ativos em Receitas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NO_EMPRESA]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 45.354360000000000000
          Top = 68.031540000000010000
          Width = 521.575140000000100000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Insumo')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 566.929499999999900000
          Top = 68.031540000000010000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Itens em receitas')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Top = 68.031540000000010000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        DataSet = frxDsRibIts
        DataSetName = 'frxDsRibIts'
        RowCount = 0
        object Memo55: TfrxMemoView
          Left = 45.354360000000000000
          Width = 521.575140000000100000
          Height = 15.118120000000000000
          DataField = 'Nome'
          DataSet = frxDsRibIts
          DataSetName = 'frxDsRibIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRibIts."Nome"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 566.929499999999900000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataField = 'ITENS'
          DataSet = frxDsRibIts
          DataSetName = 'frxDsRibIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRibIts."ITENS"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DataField = 'Produto'
          DataSet = frxDsRibIts
          DataSetName = 'frxDsRibIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRibIts."Produto"]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        DataSet = frxDsAcaIts
        DataSetName = 'frxDsAcaIts'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 45.354360000000000000
          Width = 521.575140000000100000
          Height = 15.118120000000000000
          DataField = 'Nome'
          DataSet = frxDsAcaIts
          DataSetName = 'frxDsAcaIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAcaIts."Nome"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 566.929499999999900000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataField = 'ITENS'
          DataSet = frxDsAcaIts
          DataSetName = 'frxDsAcaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAcaIts."ITENS"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DataField = 'Produto'
          DataSet = frxDsAcaIts
          DataSetName = 'frxDsAcaIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAcaIts."Produto"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        object Memo200: TfrxMemoView
          Left = 3.779530000000000000
          Top = 11.338590000000010000
          Width = 676.535870000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'RIBEIRA')
          ParentFont = False
        end
      end
      object Footer4: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 257.008040000000000000
        Width = 680.315400000000000000
      end
      object Header4: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 283.464750000000000000
        Width = 680.315400000000000000
        StartNewPage = True
        object Memo203: TfrxMemoView
          Left = 3.779530000000000000
          Width = 676.535870000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'ACABAMENTO')
          ParentFont = False
        end
      end
      object Footer6: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 366.614410000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrRibIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT foi.Produto, '
      'COUNT(foi.Produto) ITENS, pq_.Nome '
      'FROM formulasits foi'
      'LEFT JOIN pq pq_ ON pq_.Codigo=foi.Produto'
      'WHERE Produto > 0'
      'GROUP BY foi.Produto'
      'ORDER BY pq_.Nome')
    Left = 80
    Top = 404
    object QrRibItsProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrRibItsITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrRibItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrAcaIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tin.Produto, '
      'COUNT(tin.Produto) ITENS, pq_.Nome'
      'FROM tintasits tin'
      'LEFT JOIN pq pq_ ON pq_.Codigo=tin.Produto'
      'WHERE Produto > 0'
      'GROUP BY tin.Produto'
      'ORDER BY pq_.Nome')
    Left = 156
    Top = 404
    object QrAcaItsProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrAcaItsITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrAcaItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object frxDsRibIts: TfrxDBDataset
    UserName = 'frxDsRibIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Produto=Produto'
      'ITENS=ITENS'
      'Nome=Nome')
    DataSet = QrRibIts
    BCDToCurrency = False
    Left = 80
    Top = 452
  end
  object frxDsAcaIts: TfrxDBDataset
    UserName = 'frxDsAcaIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Produto=Produto'
      'ITENS=ITENS'
      'Nome=Nome')
    DataSet = QrAcaIts
    BCDToCurrency = False
    Left = 156
    Top = 452
  end
  object frxQUI_INSUM_005_02: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41602.674881805560000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxQUI_INSUM_005_01GetValue
    Left = 268
    Top = 360
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsRoll
        DataSetName = 'frxDsRoll'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 60.472480000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista de Receitas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 75.590600000000000000
          Top = 45.354360000000000000
          Width = 604.724800000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da Receita')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Top = 45.354360000000000000
          Width = 75.590551181102360000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        DataSet = frxDsRoll
        DataSetName = 'frxDsRoll'
        RowCount = 0
        object Memo55: TfrxMemoView
          Left = 75.590600000000000000
          Width = 604.724800000000000000
          Height = 15.118120000000000000
          DataField = 'Nome'
          DataSet = frxDsRoll
          DataSetName = 'frxDsRoll'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRoll."Nome"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Width = 75.590551181102360000
          Height = 15.118120000000000000
          DataField = 'Numero'
          DataSet = frxDsRoll
          DataSetName = 'frxDsRoll'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRoll."Numero"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 389.291590000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 139.842610000000000000
        Width = 680.315400000000000000
        object Memo200: TfrxMemoView
          Left = 3.779530000000000000
          Top = 11.338590000000010000
          Width = 676.535870000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TIPO_RECEITA]')
          ParentFont = False
          VAlign = vaBottom
        end
      end
      object Footer4: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 196.535560000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsRoll."ClienteI"'
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DataSet = frxDsRibIts
          DataSetName = 'frxDsRibIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRoll."ClienteI"] - [frxDsRoll."NO_EMPRESA"]')
          ParentFont = False
          VAlign = vaBottom
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrRoll: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frm.Numero, frm.Nome, frm.ClienteI,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA '
      'FROM tintascab frm'
      'LEFT JOIN entidades ent ON ent.Codigo=frm.ClienteI'
      'ORDER BY NO_EMPRESA, Nome')
    Left = 268
    Top = 408
    object QrRollNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrRollNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrRollClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrRollNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
  end
  object frxDsRoll: TfrxDBDataset
    UserName = 'frxDsRoll'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Numero=Numero'
      'Nome=Nome'
      'ClienteI=ClienteI'
      'NO_EMPRESA=NO_EMPRESA')
    DataSet = QrRoll
    BCDToCurrency = False
    Left = 268
    Top = 456
  end
  object PMImprime: TPopupMenu
    Left = 156
    Top = 48
    object Insumosativosemreceitas1: TMenuItem
      Caption = 'Insumos ativos em receitas'
      OnClick = Insumosativosemreceitas1Click
    end
    object Listadereceitasderibeira1: TMenuItem
      Caption = 'Lista de receitas de ribeira'
      OnClick = Listadereceitasderibeira1Click
    end
    object ListadereceitasdeAcabamento1: TMenuItem
      Caption = 'Lista de receitas de acabamento'
      OnClick = ListadereceitasdeAcabamento1Click
    end
  end
end
