unit UsoConsImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, dmkEditDateTimePicker, frxClass, frxDBSet;

type
  TFmUsoConsImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    QrSE: TMySQLQuery;
    QrSECodigo: TIntegerField;
    QrSENome: TWideStringField;
    DsSE: TDataSource;
    QrFunci: TMySQLQuery;
    QrFunciCodigo: TIntegerField;
    QrFunciNO_Funci: TWideStringField;
    DsFunci: TDataSource;
    Label3: TLabel;
    EdSetor: TdmkEditCB;
    CBSetor: TdmkDBLookupComboBox;
    EdFunci: TdmkEditCB;
    Label4: TLabel;
    CBFunci: TdmkDBLookupComboBox;
    Label1: TLabel;
    TPIni: TdmkEditDateTimePicker;
    Label5: TLabel;
    TPFim: TdmkEditDateTimePicker;
    QrPQ: TMySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    Label10: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    QrCI: TMySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    DsCI: TDataSource;
    Label12: TLabel;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    frxDsPeriodoA: TfrxDBDataset;
    frxUSO_CONSU_005_A: TfrxReport;
    QrPeriodoA: TMySQLQuery;
    QrPeriodoASetor: TIntegerField;
    QrPeriodoAFunci: TIntegerField;
    QrPeriodoADataX: TDateField;
    QrPeriodoAOrigemCodi: TIntegerField;
    QrPeriodoAOrigemCtrl: TIntegerField;
    QrPeriodoATipo: TIntegerField;
    QrPeriodoACliOrig: TIntegerField;
    QrPeriodoACliDest: TIntegerField;
    QrPeriodoAInsumo: TIntegerField;
    QrPeriodoAPeso: TFloatField;
    QrPeriodoAValor: TFloatField;
    QrPeriodoARetorno: TSmallintField;
    QrPeriodoAStqMovIts: TIntegerField;
    QrPeriodoAAlterWeb: TSmallintField;
    QrPeriodoAAtivo: TSmallintField;
    QrPeriodoARetQtd: TFloatField;
    QrPeriodoAHowLoad: TSmallintField;
    QrPeriodoAStqCenLoc: TIntegerField;
    QrPeriodoASdoPeso: TFloatField;
    QrPeriodoASdoValr: TFloatField;
    QrPeriodoAAcePeso: TFloatField;
    QrPeriodoAAceValr: TFloatField;
    QrPeriodoADtCorrApo: TDateField;
    QrPeriodoAEmpresa: TIntegerField;
    QrPeriodoAAWServerID: TIntegerField;
    QrPeriodoAAWStatSinc: TSmallintField;
    QrPeriodoANOMEPQ: TWideStringField;
    QrPeriodoANO_FUNCI: TWideStringField;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    frxReport1: TfrxReport;
    QrPeriodoANO_EMP: TWideStringField;
    QrPeriodoADataX_TXT: TWideStringField;
    QrPeriodoANO_SETOR: TWideStringField;
    Panel5: TPanel;
    RGAgrup: TRadioGroup;
    RGDetalha: TRadioGroup;
    QrPeriodoS: TMySQLQuery;
    QrPeriodoSSetor: TIntegerField;
    QrPeriodoSFunci: TIntegerField;
    QrPeriodoSDataX: TDateField;
    QrPeriodoSTipo: TIntegerField;
    QrPeriodoSCliOrig: TIntegerField;
    QrPeriodoSInsumo: TIntegerField;
    QrPeriodoSPeso: TFloatField;
    QrPeriodoSValor: TFloatField;
    QrPeriodoSEmpresa: TIntegerField;
    QrPeriodoSNOMEPQ: TWideStringField;
    QrPeriodoSNO_FUNCI: TWideStringField;
    QrPeriodoSNO_EMP: TWideStringField;
    QrPeriodoSDataX_TXT: TWideStringField;
    QrPeriodoSNO_SETOR: TWideStringField;
    frxDsPeriodoS: TfrxDBDataset;
    frxUSO_CONSU_005_S: TfrxReport;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxUSO_CONSU_005_AGetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    FTabela, FSQL_Ordem, FDataI, FDataF,
    FSQL_Insumo, FSQL_CliOri, FSQL_Setor, FSQL_Funci: String;
    //
    procedure DefineFuiltros();
    procedure PreviewAnalitico();
    procedure PreparaOrdemEGrupoA();
    procedure PreparaOrdemEGrupoS();
    //
    procedure PreviewSintetico();
  public
    { Public declarations }
    FSet_FATID: Integer;
  end;

  var
  FmUsoConsImp: TFmUsoConsImp;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UnDmkProcFunc;

{$R *.DFM}

const
  OrdFields: array[0..4] of String = ('NOMEPQ', 'NO_FUNCI', 'NO_SETOR', 'DataX', 'NO_EMP');

procedure TFmUsoConsImp.BtOKClick(Sender: TObject);
begin
  DefineFuiltros();
  //
  case RGDetalha.ItemIndex of
    0: PreviewAnalitico();
    1: PreviewSintetico();
    else Geral.MB_Info('Detalhamento n�o implementado: ' +
      RGDetalha.Items[RGDetalha.ItemIndex]);
  end;
end;

procedure TFmUsoConsImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmUsoConsImp.DefineFuiltros();
var
  Insumo, CliOri, Setor, Funci: integer;
begin
  case FSet_FATID of
    VAR_FATID_0180: FTabela := 'pqi';
    VAR_FATID_0185: FTabela := 'pqn';
    else FTabela := 'pq?';
  end;
  FDataI := Geral.FDT(TPIni.Date, 1);
  FDataF := Geral.FDT(TPFim.Date, 1) + ' 23:59:59';
  //
  FSQL_Insumo := '';
  FSQL_CliOri := '';
  FSQL_Setor  := '';
  FSQL_Funci  := '';
  //
  Insumo := EdPQ.ValueVariant;
  CliOri := EdCI.ValueVariant;
  Setor  := EdSetor.ValueVariant;
  Funci  := EdFunci.ValueVariant;
  //
  if Insumo <> 0 then
    FSQL_Insumo := 'AND pqx.Insumo=' + Geral.FF0(Insumo);
  if CliOri <> 0 then
    FSQL_CliOri := 'AND pqx.CliOrig=' + Geral.FF0(CliOri);
  if Setor <> 0 then
    FSQL_Setor  := 'AND uso.Setor=' + Geral.FF0(Setor);
  if Funci <> 0 then
    FSQL_Funci  := 'AND uso.Funci=' + Geral.FF0(Funci);
  //
  FSQL_Ordem := 'ORDER BY ' +
               OrdFields[RGOrdem1.ItemIndex] + ', ' +
               OrdFields[RGOrdem2.ItemIndex] + ', ' +
               OrdFields[RGOrdem3.ItemIndex];
end;

procedure TFmUsoConsImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmUsoConsImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FSet_FATID := -99999999;
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSE, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFunci, Dmod.MyDB);
  //
  TPIni.Date := Date - 30;
  TPFim.Date := Date;
end;

procedure TFmUsoConsImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmUsoConsImp.frxUSO_CONSU_005_AGetValue(const VarName: string;
  var Value: Variant);
const
  CabFields: array[0..4] of String = ('Material', 'Entidade', 'Setor', 'Data', 'Cliente interno');
begin
  if VarName = 'VFR_GRD' then Value := 15 else
  if VarName = 'VARF_DATA' then Value := Now() else
  if VarName = 'VARF_TITULO' then
  begin
    case FSet_FATID of
      VAR_FATID_0180: Value := 'Baixa de EPI';
      VAR_FATID_0185: Value := 'Baixa de Material de Manuten��o';
      else Value := 'Baixa de Material de ????';
    end;
  end else
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      '', CBCI.Text, EdCI.ValueVariant, 'TODAS')
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp1(TPIni.Date, TPFim.Date,
    (*CkDataIni.Checked, CkDataFim.Checked*)True, True, '', 'at�', '')
  else
  if VarName = 'VARF_SETOR' then
    Value := dmkPF.ParValueCodTxt(
      '', CBSetor.Text, EdSetor.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_FUNCI' then
    Value := dmkPF.ParValueCodTxt(
      '', CBFunci.Text, EdFunci.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_TIT_FLD' then
  begin
    case RGAgrup.ItemIndex of
      0: Value := CabFields[RGOrdem1.ItemIndex];
      1: Value := CabFields[RGOrdem2.ItemIndex];
      2: Value := CabFields[RGOrdem3.ItemIndex];
      else Value := '?????';
    end;
  end;
end;

procedure TFmUsoConsImp.PreparaOrdemEGrupoA();
const
  // Material, Entidade, Setor, Data, Cliente interno
  ID_Fields: array[0..4] of String = ('Insumo', 'Funci', 'Setor', 'DataX', 'CliOrig');
  TxtFields: array[0..4] of String = ('NOMEPQ', 'NO_FUNCI', 'NO_SETOR', 'DataX_TXT', 'NO_EMP');
var
  GH_01, GH_02: TfrxGroupHeader;
  GF_01, GF_02: TfrxGroupFooter;
  Me01_GH01, Me01_GH02, Me01_GF01, Me01_GF02: TfrxMemoView;
  ID_Fld, TxtFld: String;
begin
  GH_01 := frxUSO_CONSU_005_A.FindObject('GH_01') as TfrxGroupHeader;
  GH_02 := frxUSO_CONSU_005_A.FindObject('GH_02') as TfrxGroupHeader;
  GF_01 := frxUSO_CONSU_005_A.FindObject('GF_01') as TfrxGroupFooter;
  GF_02 := frxUSO_CONSU_005_A.FindObject('GF_02') as TfrxGroupFooter;
  Me01_GH01 := frxUSO_CONSU_005_A.FindObject('Me01_GH01') as TfrxMemoView;
  Me01_GH02 := frxUSO_CONSU_005_A.FindObject('Me01_GH02') as TfrxMemoView;
  Me01_GF01 := frxUSO_CONSU_005_A.FindObject('Me01_GF01') as TfrxMemoView;
  Me01_GF02 := frxUSO_CONSU_005_A.FindObject('Me01_GF02') as TfrxMemoView;
  //
  GH_01.Visible := RGAgrup.ItemIndex >= 1;
  GF_01.Visible := RGAgrup.ItemIndex >= 1;
  GH_02.Visible := RGAgrup.ItemIndex >= 2;
  GF_02.Visible := RGAgrup.ItemIndex >= 2;
  //
  ID_Fld := ID_Fields[RGOrdem1.ItemIndex];
  TxtFld := TxtFields[RGOrdem1.ItemIndex];
  GH_01.Condition := 'frxDsPeriodoA."' + ID_Fld + '"';
  Me01_GH01.Memo.Text := '[frxDsPeriodoA."' + TxtFld + '"]';
  Me01_GF01.Memo.Text := 'TOTAL [frxDsPeriodoA."' + TxtFld + '"]: ';
  //
  ID_Fld := ID_Fields[RGOrdem2.ItemIndex];
  TxtFld := TxtFields[RGOrdem2.ItemIndex];
  GH_02.Condition := 'frxDsPeriodoA."' + ID_Fld + '"';
  Me01_GH02.Memo.Text := '[frxDsPeriodoA."' + TxtFld + '"]';
  Me01_GF02.Memo.Text := 'TOTAL [frxDsPeriodoA."' + TxtFld + '"]: ';
end;

procedure TFmUsoConsImp.PreparaOrdemEGrupoS();
const
  // Material, Entidade, Setor, Data, Cliente interno
  ID_Fields: array[0..4] of String = ('Insumo', 'Funci', 'Setor', 'DataX', 'CliOrig');
  TxtFields: array[0..4] of String = ('NOMEPQ', 'NO_FUNCI', 'NO_SETOR', 'DataX_TXT', 'NO_EMP');
var
  GH_01, GH_02: TfrxGroupHeader;
  GF_01, GF_02: TfrxGroupFooter;
  Me01_GH01, Me01_GH02, Me01_GF01, Me01_GF02, MD01_ME01: TfrxMemoView;
  ID_Fld, TxtFld: String;
begin
  GH_01 := frxUSO_CONSU_005_S.FindObject('GH_01') as TfrxGroupHeader;
  GH_02 := frxUSO_CONSU_005_S.FindObject('GH_02') as TfrxGroupHeader;
  GF_01 := frxUSO_CONSU_005_S.FindObject('GF_01') as TfrxGroupFooter;
  GF_02 := frxUSO_CONSU_005_S.FindObject('GF_02') as TfrxGroupFooter;
  Me01_GH01 := frxUSO_CONSU_005_S.FindObject('Me01_GH01') as TfrxMemoView;
  Me01_GH02 := frxUSO_CONSU_005_S.FindObject('Me01_GH02') as TfrxMemoView;
  Me01_GF01 := frxUSO_CONSU_005_S.FindObject('Me01_GF01') as TfrxMemoView;
  Me01_GF02 := frxUSO_CONSU_005_S.FindObject('Me01_GF02') as TfrxMemoView;
  ////////////////////////////////////////////////////////////////////////
  MD01_ME01 := frxUSO_CONSU_005_S.FindObject('MD01_ME01') as TfrxMemoView;
  ////////////////////////////////////////////////////////////////////////
  //
  GH_01.Visible := RGAgrup.ItemIndex >= 1;
  GF_01.Visible := RGAgrup.ItemIndex >= 1;
  GH_02.Visible := RGAgrup.ItemIndex >= 2;
  GF_02.Visible := RGAgrup.ItemIndex >= 2;
  //
  ID_Fld := ID_Fields[RGOrdem1.ItemIndex];
  TxtFld := TxtFields[RGOrdem1.ItemIndex];
  GH_01.Condition := 'frxDsPeriodoS."' + ID_Fld + '"';
  Me01_GH01.Memo.Text := '[frxDsPeriodoS."' + TxtFld + '"]';
  Me01_GF01.Memo.Text := 'TOTAL [frxDsPeriodoS."' + TxtFld + '"]: ';
  //
  ID_Fld := ID_Fields[RGOrdem2.ItemIndex];
  TxtFld := TxtFields[RGOrdem2.ItemIndex];
  GH_02.Condition := 'frxDsPeriodoS."' + ID_Fld + '"';
  Me01_GH02.Memo.Text := '[frxDsPeriodoS."' + TxtFld + '"]';
  Me01_GF02.Memo.Text := 'TOTAL [frxDsPeriodoS."' + TxtFld + '"]: ';
  ////////////////////////////////////////////////////////////////////////
  case RGAgrup.ItemIndex of
    0: TxtFld := TxtFields[RGOrdem1.ItemIndex];
    1: TxtFld := TxtFields[RGOrdem2.ItemIndex];
    2: TxtFld := TxtFields[RGOrdem3.ItemIndex];
    else TxtFld := '???';
  end;
  MD01_ME01.Memo.Text := '[frxDsPeriodoS."' + TxtFld + '"]';
  ////////////////////////////////////////////////////////////////////////
end;

procedure TFmUsoConsImp.PreviewSintetico();
var
  SQL_Group: String;
begin
  SQL_Group := 'GROUP BY ' + OrdFields[RGOrdem1.ItemIndex];
  if RGAgrup.ItemIndex >=1 then
    SQL_Group := SQL_Group + ', ' + OrdFields[RGOrdem2.ItemIndex];
  if RGAgrup.ItemIndex >=2 then
    SQL_Group := SQL_Group + ', ' + OrdFields[RGOrdem3.ItemIndex];
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPeriodoS, Dmod.MyDB, [
  'SELECT uso.Setor, uso.Funci, pqx.DataX, pqx.Tipo, ',
  'pqx.CliOrig, pqx.Insumo, SUM(Peso) Peso, ',
  'SUM(Valor) Valor, pq_.Nome NOMEPQ, lse.Nome NO_SETOR,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FUNCI, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP, ',
  'DATE_FORMAT(DataX, "%d/%m/%Y") DataX_TXT, ',
  'uso.Empresa ',
  'FROM pqx pqx ',
  'LEFT JOIN pq    pq_ ON pq_.Codigo=pqx.Insumo',
  'LEFT JOIN ' + FTabela + ' uso ON uso.Codigo=pqx.OrigemCodi ',
  'LEFT JOIN entidades ent ON ent.Codigo=uso.Funci',
  'LEFT JOIN entidades emp ON ent.Codigo=pqx.CliOrig',
  'LEFT JOIN listasetores lse ON lse.Codigo=uso.Setor ',
  'WHERE pqx.Tipo=' + Geral.FF0(FSet_FATID),
  'AND pqx.DataX BETWEEN "' + FDataI + '" AND "' + FDataF + '" ',
  FSQL_Insumo,
  FSQL_CliOri,
  FSQL_Setor,
  FSQL_Funci,
  //...
  SQL_Group,
  FSQL_Ordem,
  '']);
  //
  MyObjects.frxDefineDatasets(frxUSO_CONSU_005_S, [
  DModG.frxDsDono,
  frxDsPeriodoS
  ]);
  PreparaOrdemEGrupoS();
  MyObjects.frxMostra(frxUSO_CONSU_005_S, 'Baixa de Material por Per�odo - Sint�tico');
end;

procedure TFmUsoConsImp.PreviewAnalitico();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPeriodoA, Dmod.MyDB, [
  'SELECT DISTINCT uso.Setor, uso.Funci, ',
  'pq_.Nome NOMEPQ, lse.Nome NO_SETOR,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FUNCI, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP, ',
  'DATE_FORMAT(DataX, "%d/%m/%Y") DataX_TXT, pqx.* ',
  'FROM pqx pqx ',
  'LEFT JOIN pq    pq_ ON pq_.Codigo=pqx.Insumo',
  'LEFT JOIN ' + FTabela + ' uso ON uso.Codigo=pqx.OrigemCodi ',
  'LEFT JOIN entidades ent ON ent.Codigo=uso.Funci',
  'LEFT JOIN entidades emp ON ent.Codigo=pqx.CliOrig',
  'LEFT JOIN listasetores lse ON lse.Codigo=uso.Setor ',
  'WHERE pqx.Tipo=' + Geral.FF0(FSet_FATID),
  'AND pqx.DataX BETWEEN "' + FDataI + '" AND "' + FDataF + '" ',
  FSQL_Insumo,
  FSQL_CliOri,
  FSQL_Setor,
  FSQL_Funci,
  //...
  FSQL_Ordem,
  '']);
  //
  MyObjects.frxDefineDatasets(frxUSO_CONSU_005_A, [
  DModG.frxDsDono,
  frxDsPeriodoA
  ]);
  PreparaOrdemEGrupoA();
  MyObjects.frxMostra(frxUSO_CONSU_005_A, 'Baixa de Material por Per�odo - Anal�tico');
end;

end.
