unit MPClasMulSub;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, dmkImage, UnDmkEnums,
  AppListas, DmkDAC_PF;

type
  TFmMPClasMulSub = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    DsMPInClasMul: TDataSource;
    QrMPInClasMul: TmySQLQuery;
    QrMPInClasMulCodigo: TIntegerField;
    QrMPInClasMulControle: TIntegerField;
    QrMPInClasMulData: TDateField;
    QrMPInClasMulClienteI: TIntegerField;
    QrMPInClasMulProcedencia: TIntegerField;
    QrMPInClasMulTransportadora: TIntegerField;
    QrMPInClasMulMarca: TWideStringField;
    QrMPInClasMulLote: TWideStringField;
    QrMPInClasMulPecasOut: TFloatField;
    QrMPInClasMulPecasSal: TFloatField;
    QrMPInClasMulEstq_Pecas: TFloatField;
    QrMPInClasMulEstq_M2: TFloatField;
    QrMPInClasMulEstq_P2: TFloatField;
    QrMPInClasMulEstq_Peso: TFloatField;
    QrMPInClasMulStqMovIts: TIntegerField;
    QrMPInClasMulClas_Pecas: TFloatField;
    QrMPInClasMulClas_M2: TFloatField;
    QrMPInClasMulClas_P2: TFloatField;
    QrMPInClasMulClas_Peso: TFloatField;
    QrMPInClasMulDebCtrl: TIntegerField;
    QrMPInClasMulDebStqCenCad: TIntegerField;
    QrMPInClasMulDebGraGruX: TIntegerField;
    QrMPInClasMulPecas: TFloatField;
    QrMPInClasMulPLE: TFloatField;
    QrMPInClasMulBxa_Pecas: TFloatField;
    QrMPInClasMulBxa_M2: TFloatField;
    QrMPInClasMulBxa_P2: TFloatField;
    QrMPInClasMulBxa_Peso: TFloatField;
    QrMPInClasMulGerBxaEstq: TIntegerField;
    QrMPInClasMulClas_Qtde: TFloatField;
    QrMPInClasMulBxa_Qtde: TFloatField;
    QrMPInClasMulDataHora: TDateTimeField;
    DBGMPInClasMul: TDBGrid;
    Panel4: TPanel;
    QrSoma: TmySQLQuery;
    QrSomaEstq_Pecas: TFloatField;
    QrSomaEstq_M2: TFloatField;
    QrSomaEstq_P2: TFloatField;
    QrSomaEstq_Peso: TFloatField;
    QrSomaClas_Pecas: TFloatField;
    QrSomaClas_M2: TFloatField;
    QrSomaClas_P2: TFloatField;
    QrSomaClas_Peso: TFloatField;
    DsSoma: TDataSource;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    Label4: TLabel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    BtRateia: TBitBtn;
    QrMPSubProd: TmySQLQuery;
    QrMPSubProdCodigo: TIntegerField;
    QrMPSubProdNome: TWideStringField;
    QrMPSubProdGGX_Ori: TIntegerField;
    QrMPSubProdGGX_Ger: TIntegerField;
    QrMPSubProdGrndzaBase: TSmallintField;
    QrMPSubProdFator: TFloatField;
    MeAvisos: TMemo;
    QrMPSubProdArredonda: TFloatField;
    QrMPSubProdGER_GerBxaEstq: TSmallintField;
    QrMPInSubPMul: TmySQLQuery;
    QrMPInSubPMulNO_PRD: TWideStringField;
    QrMPInSubPMulSIGLA: TWideStringField;
    QrMPInSubPMulQtde: TFloatField;
    QrMPInSubPMulPecas: TFloatField;
    QrMPInSubPMulPeso: TFloatField;
    DBGrid3: TDBGrid;
    DsMPInSubPMul: TDataSource;
    QrMPInSubPMulGGX_Ger: TIntegerField;
    Label12: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    QrMPInClasMulNO_TIPIFICACAO: TWideStringField;
    QrMPInClasMulNO_ANIMAL: TWideStringField;
    QrMPInClasMulGraGruX: TIntegerField;
    QrMPInClasMulTipificacao: TIntegerField;
    QrMPInClasMulAnimal: TIntegerField;
    Label1: TLabel;
    RGData: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    Label2: TLabel;
    EdRegA: TdmkEdit;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMPSubProdChange(Sender: TObject);
    procedure QrMPInClasMulAfterOpen(DataSet: TDataSet);
    procedure BtRateiaClick(Sender: TObject);
    procedure QrTipAniBeforeClose(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure EdStqCenCadChange(Sender: TObject);
    procedure QrMPInClasMulCalcFields(DataSet: TDataSet);
    procedure QrMPInClasMulAfterScroll(DataSet: TDataSet);
    procedure QrMPInClasMulBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FMPInClasMul, FMPInSubPMul: String;
    FItens: Integer;
    procedure ReabreMPInClasMul();
    procedure ReopenMPInClasMul((*GraGruX: Integer*));
    procedure HabilitaBtOK();
  public
    { Public declarations }
  end;

  var
  FmMPClasMulSub: TFmMPClasMulSub;

implementation

uses UnMyObjects, Module, ModuleGeral, MPIn, UCreate, UMySQLModule, UnInternalConsts,
ModProd, UnGrade_Tabs;

{$R *.DFM}

procedure TFmMPClasMulSub.BtOKClick(Sender: TObject);
const
  OriCnta = 1; // Identificar que foi nesta janela!
  OriPart = 0;
var
  IDCtrl, OriCtrl, OriCodi, Empresa, StqCenCad, GraGruX, DebCtrl, SMIMultIns: Integer;
  QtdeGE, QtdeQI, QtdeQK, AreaM2, AreaP2: Double;
  DataHora: String;
  FatorClas: Double;
  //Continua: Boolean;
begin
  //FatorClas := 1;
  StqCenCad := EdStqCenCad.ValueVariant;
  if MyObjects.FIC(StqCenCad = 0, EdStqCenCad, 'Informe o centro de estoque') then Exit;
  //
  SMIMultIns := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'StqMovItsA', 'SMIMultIns', 'SMIMultIns');
  QrMPInClasMul.First;
  while not QrMPInClasMul.Eof do
  begin
    //Application.ProcessMessages;
    QrMPInSubPMul.First;
    while not QrMPInSubPMul.Eof do
    begin
      IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
      OriCtrl := IDCtrl;
      //
      OriCodi := QrMPInClasMulControle.Value;
      Empresa := QrMPInClasMulClienteI.Value;
      //
      DebCtrl := QrMPInClasMulDebCtrl.Value;
      // obrigat�rio a cada registro pois os dados s�o modificados na segunda SQL abaixo
      FatorClas := 1;
      StqCenCad := QrStqCenCadCodigo.Value;
      GraGruX   := QrMPInSubPMulGGX_Ger.Value;
      //
      QtdeQI    := QrMPInSubPMulPecas.Value;
      QtdeQK    := QrMPInSubPMulPeso.Value;
      AreaM2    := 0;
      AreaP2    := 0;
      QtdeGE    := QrMPInSubPMulQtde.Value;
      //
      //DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', QrMPInClasMulDataHora.Value);
      case RGData.Itemindex of
        0: DataHora := Geral.FDT(TPData.Date, 1) + ' ' + EdHora.Text;
        1: DataHora := Geral.FDT(QrMPInClasMulData.Value + 2, 1);
        2: DataHora := Geral.FDT(QrMPInClasMulData.Value + 3, 1);
      end;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
        'DataHora', 'Tipo', 'OriCodi',
        'OriCtrl', 'OriCnta', 'OriPart',
        'Empresa', 'StqCenCad', 'GraGruX',
        'Qtde', 'Pecas', 'Peso', 'AreaM2', 'AreaP2',
        'FatorClas', 'DebCtrl', 'SMIMultIns'
      ], ['IDCtrl'], [
        DataHora, VAR_FATID_0101, OriCodi,
        OriCtrl, OriCnta, OriPart,
        Empresa, StqCenCad, GraGruX,
        //Qtde, Pecas,  Peso,   Area
        QtdeGE, QtdeQI, QtdeQK, AreaM2, AreaP2,
        FatorClas, DebCtrl, SMIMultIns
      ], [IDCtrl], False);
      // N�o precisa, pois n�o baixa!
      //Dmod.AtualizaMPIn(OriCodi);
      //
      QrMPInSubPMul.Next;
    end;
    //
    QrMPInClasMul.Next;
  end;
  FmMPIn.ReopenPRi(qqMPIn, FmMPIn.FControle);
  Close;
end;

procedure TFmMPClasMulSub.BtRateiaClick(Sender: TObject);
var
  Codigo, Conta, GGX_Ori, GGX_Ger, Controle: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2, CustoAll, ValorAll: Double;
  //
  cQtd, cFat, cAre: Double;
  NaoHa: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    FItens := 0;
    MeAvisos.Lines.Clear;
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('DELETE FROM ' + FMPInSubPMul);
    DModG.QrUpdPID1.ExecSQL;
    //
    QrMPInClasMul.First;
    Conta := 0;
    while not QrMPInClasMul.Eof do
    begin
      Codigo   := QrMPInClasMulCodigo.Value;
      Controle := QrMPInClasMulControle.Value;
      //
      QrMPSubProd.Close;
      QrMPSubProd.Params[0].AsInteger := QrMPInClasMulGraGruX.Value;
      UnDmkDAC_PF.AbreQuery(QrMPSubProd, Dmod.MyDB);
      //
      if QrMPSubProd.RecordCount = 0 then
        MeAvisos.Lines.Add('A mat�ria-prima com o reduzido = ' + FormatFloat('0',
        QrMPInClasMulGraGruX.Value) + ' n�o tem nenhum subproduto cadastrado!')
      else
      begin
        QrMPSubProd.First;
        while not QrMPSubProd.Eof do
        begin
          Conta    := Conta + 1;
          //GraGruX  := QrMPSubProdGGX_Ger.Value;
          GGX_Ori  := QrMPSubProdGGX_Ori.Value;
          GGX_Ger  := QrMPSubProdGGX_Ger.Value;
          //
          NaoHa := False;
          cQtd  := 0;
          case QrMPSubProdGrndzaBase.Value of
            //? ? ?
            1: (*Pe�a   *) cQtd  := QrMPInClasMulPecas.Value;
            2: (*m�     *) NaoHa := True;
            3: (*kg     *) cQtd  := QrMPInClasMulPLE.Value;
            4: (*t (ton)*) cQtd  := QrMPInClasMulPLE.Value * 1000;
            5: (*ft�    *) NaoHa := True;
            else           NaoHa := True;
          end;
          if NaoHa then
          begin
            Geral.MB_Aviso('Grandeza inv�lida para mat�ria-prima: "' +
              DmProd.DefineDescriGerBxaEstq(QrMPSubProdGrndzaBase.Value) + '".');
          end else
          begin
            cFat := QrMPSubProdFator.Value;
            cAre := QrMPSubProdArredonda.Value;
            //
            if cAre = 0 then
              Qtde := 0
            else
              Qtde := Round(cQtd * cFat / cAre) * cAre;
          end;
          Pecas    := 0;
          Peso     := 0;
          AreaM2   := 0;
          AreaP2   := 0;
          CustoAll := 0;
          ValorAll := 0;
          //case QrMPSubProdGrndzaBase.Value of
          case QrMPSubProdGER_GerBxaEstq.Value of
            //? ? ?
            1: (*Pe�a   *) Pecas := Qtde;
            2: (*m�     *) ;//
            3: (*kg     *) Peso  := Qtde;
            4: (*t (ton)*) Peso  := Qtde * 1000;// / 1000;
            5: (*ft�    *) ;
            else           ;
          end;
          //
          if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FMPInSubPMul, False, [
          'Codigo', 'Controle',
          'GGX_Ori', 'GGX_Ger',
          'Qtde', 'Pecas', 'Peso',
          'AreaM2', 'AreaP2', 'CustoAll',
          'ValorAll'], [
          'Conta'], [
          Codigo, Controle,
          GGX_Ori, GGX_Ger,
          Qtde, Pecas, Peso,
          AreaM2, AreaP2, CustoAll,
          ValorAll], [
          Conta], False) then
            FItens := FItens + 1;
          //
          QrMPSubProd.Next;
        end;
      end;
      QrMPInClasMul.Next;
    end;
    ReopenMPInClasMul();
    HabilitaBtOK();
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMPClasMulSub.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPClasMulSub.EdMPSubProdChange(Sender: TObject);
begin
  //BtOK.Enabled := EdMPSubProd.ValueVariant;
end;

procedure TFmMPClasMulSub.EdStqCenCadChange(Sender: TObject);
begin
  HabilitaBtOK();
end;

procedure TFmMPClasMulSub.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPClasMulSub.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FItens := 0;
  //
  //UnDmkDAC_PF.AbreQuery(QrMPSubProd, Dmod.MyDB);
  //
  FMPInClasMul := UCriar.RecriaTempTableNovo(ntrttMPInClasMul, DmodG.QrUpdPID1, False);
  FMPInSubPMul := UCriar.RecriaTempTableNovo(ntrttMPInSubPMul, DmodG.QrUpdPID1, False);
  //
  ReabreMPInClasMul();
  //
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  if QrStqCenCad.Locate('Codigo', 1, []) then
  begin
    EdStqCenCad.ValueVariant := 1;
    CBStqCenCad.KeyValue     := 1;
  end;
  //
  TPData.Date := Date;
  EdHora.ValueVariant := Now();
  //
  MeAvisos.Lines.Clear;
end;

procedure TFmMPClasMulSub.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPClasMulSub.HabilitaBtOK();
begin
  BtOK.Enabled := (FItens > 0) and (EdStqCenCad.ValueVariant <> 0);
end;

procedure TFmMPClasMulSub.QrMPInClasMulAfterOpen(DataSet: TDataSet);
begin
  EdRegA.ValueVariant := QrMPInClasMul.RecordCount;
end;

procedure TFmMPClasMulSub.QrMPInClasMulAfterScroll(DataSet: TDataSet);
begin
  QrMPInSubPMul.Close;
  QrMPInSubPMul.SQL.Clear;
  QrMPInSubPMul.SQL.Add('SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,');
  QrMPInSubPMul.SQL.Add('gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,');
  QrMPInSubPMul.SQL.Add('gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,');
  QrMPInSubPMul.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, ');
  QrMPInSubPMul.SQL.Add('med.Grandeza, smia.*');
  QrMPInSubPMul.SQL.Add('FROM ' + VAR_MyPID_DB_NOME + '.mpinsubpmul smia');
  QrMPInSubPMul.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GGX_Ger');
  QrMPInSubPMul.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  QrMPInSubPMul.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  QrMPInSubPMul.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  QrMPInSubPMul.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  QrMPInSubPMul.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
  QrMPInSubPMul.SQL.Add('LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed');
  QrMPInSubPMul.SQL.Add('WHERE smia.Controle=' + FormatFloat('0', QrMPInClasMulControle.Value));
  QrMPInSubPMul.SQL.Add('ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GGX_Ger');
  QrMPInSubPMul.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrMPInSubPMul, DModG.MyPID_DB);
end;

procedure TFmMPClasMulSub.QrMPInClasMulBeforeClose(DataSet: TDataSet);
begin
  EdRegA.ValueVariant := 0;
end;

procedure TFmMPClasMulSub.QrMPInClasMulCalcFields(DataSet: TDataSet);
begin
  QrMPInClasMulGraGruX.Value := Dmod.ObtemGraGruXDeCouro(QrMPInClasMulTipificacao.Value, QrMPInClasMulAnimal.Value);
  QrMPInClasMulNO_TIPIFICACAO.Value := UnAppListas.DefineNOMETipificacaoCouro(-3, QrMPInClasMulTipificacao.Value);
  QrMPInClasMulNO_ANIMAL.Value := UnAppListas.DefineNOMETipificacaoCouro(-5, QrMPInClasMulAnimal.Value);
end;

procedure TFmMPClasMulSub.QrTipAniBeforeClose(DataSet: TDataSet);
begin
  QrMPInSubPMul.Close;
end;

procedure TFmMPClasMulSub.ReabreMPInClasMul();
var
  Texto: String;
  p: Integer;
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM mpinclasmul');
  DModG.QrUpdPID1.ExecSQL;
  //
  Texto := FmMPIn.QrMPIn.SQL.Text;
  p := Pos(FmMPIn.FFromMPIn, Texto);
  Texto := Copy(Texto, p);// + Length(FmMPIn.FFromMPIn));
  Texto := 'INSERT INTO ' + DModG.MyPID_DB.DatabaseName + '.' + FMPInClasMul + sLineBreak +
  'SELECT wi.Codigo, wi.Controle, wi.Data, wi.ClienteI,' + sLineBreak +
  'wi.Procedencia, wi.Transportadora, wi.Marca, wi.Lote,' + sLineBreak +
  'wi.Pecas, wi.PLE, ' + sLineBreak +
  'wi.PecasOut, wi.PecasSal, wi.Estq_Pecas, wi.Estq_M2,' + sLineBreak +
  'wi.Estq_P2, wi.Estq_Peso, wi.StqMovIts, ' + sLineBreak +
  '0 Debctrl, 0 DebStqCenCad, 0 DebGraGruX, 1 GerBxaEstq, ' + sLineBreak +     // GerBxaEstq > Couro
  '0 Clas_Qtde, 0 Clas_Pecas, 0 Clas_M2, 0 Clas_P2, 0 Clas_Peso, ' + sLineBreak +
  '0 Bxa_Qtde, 0 Bxa_Pecas, 0 Bxa_M2, 0 Bxa_P2, 0 Bxa_Peso, ' + sLineBreak +
  '0 DataHora, wi.Tipificacao, wi.Animal ' + sLineBreak + Texto;
  //'FROM ' + TMeuDB + '.mpin wi' + Texto;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Text := Texto;
  Dmod.QrUpd.ExecSQL;
  //
{
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM mpinclasmul');
  case RGRateio.ItemIndex of
    0: DModG.QrUpdPID1.SQL.Add('WHERE Estq_Pecas < 1');
    1: DModG.QrUpdPID1.SQL.Add('WHERE Estq_Peso <= 0');
  end;
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.ExecSQL;
}
  //
  ReopenMPInClasMul();
  QrSoma.Close;
  UnDmkDAC_PF.AbreQuery(QrSoma, DModG.MyPID_DB);
end;

procedure TFmMPClasMulSub.ReopenMPInClasMul((*GraGruX: Integer*));
begin
  QrMPInClasMul.Close;
  UnDmkDAC_PF.AbreQuery(QrMPInClasMul, DModG.MyPID_DB);
{
  QrTipAni.Close;
  UnDmkDAC_PF.AbreQuery(QrTipAni, ?????);
  // N�o pode! � calculado
  //QrTipAni.Locate('GraGruX', GraGruX, []);
}
end;

{
object QrMPSubProd: TmySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT * FROM mpsubprod'
    '')
  Left = 8
  Top = 8
  object QrMPSubProdCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrMPSubProdNome: TWideStringField
    FieldName = 'Nome'
    Size = 50
  end
  object QrMPSubProdGGX_Ori: TIntegerField
    FieldName = 'GGX_Ori'
  end
  object QrMPSubProdGGX_Ger: TIntegerField
    FieldName = 'GGX_Ger'
  end
  object QrMPSubProdGrndzaBase: TSmallintField
    FieldName = 'GrndzaBase'
  end
  object QrMPSubProdFator: TFloatField
    FieldName = 'Fator'
  end
end
object DsMPSubProd: TDataSource
  DataSet = QrMPSubProd
  Left = 36
  Top = 8
end
}
{
object Label1: TLabel
  Left = 8
  Top = 4
  Width = 142
  Height = 13
  Caption = 'Configura'#231#227'o de Subproduto:'
end
object EdMPSubProd: TdmkEditCB
  Left = 8
  Top = 20
  Width = 56
  Height = 21
  Alignment = taRightJustify
  TabOrder = 0
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  ValMin = '-2147483647'
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  OnChange = EdMPSubProdChange
  DBLookupComboBox = CBMPSubProd
end
object CBMPSubProd: TdmkDBLookupComboBox
  Left = 64
  Top = 20
  Width = 709
  Height = 21
  KeyField = 'Codigo'
  ListField = 'Nome'
  TabOrder = 1
  dmkEditCB = EdMPSubProd
  UpdType = utYes
end
}
end.
