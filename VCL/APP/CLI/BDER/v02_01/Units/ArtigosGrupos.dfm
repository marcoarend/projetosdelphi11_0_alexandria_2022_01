object FmArtigosGrupos: TFmArtigosGrupos
  Left = 368
  Top = 194
  Caption = 'ART-CADAS-001 :: Artigos'
  ClientHeight = 347
  ClientWidth = 624
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 624
    Height = 251
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 180
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 76
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 57
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 24
        Width = 300
        Height = 21
        MaxLength = 255
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 48
        Width = 361
        Height = 121
        Caption = ' Nota fiscal Modelo 1 ou 1A: '
        TabOrder = 2
        object Label3: TLabel
          Left = 12
          Top = 42
          Width = 88
          Height = 13
          Caption = 'Situa'#231#227'o tribut'#225'ria:'
        end
        object Label4: TLabel
          Left = 12
          Top = 18
          Width = 92
          Height = 13
          Caption = 'Classifica'#231#227'o fiscal:'
        end
        object Label5: TLabel
          Left = 180
          Top = 18
          Width = 40
          Height = 13
          Caption = '% ICMS:'
        end
        object Label6: TLabel
          Left = 180
          Top = 42
          Width = 27
          Height = 13
          Caption = '% IPI:'
        end
        object Label7: TLabel
          Left = 12
          Top = 68
          Width = 92
          Height = 13
          Caption = 'C'#243'digo (impress'#227'o):'
        end
        object Label8: TLabel
          Left = 180
          Top = 66
          Width = 31
          Height = 13
          Caption = 'Pre'#231'o:'
        end
        object EdCF: TEdit
          Left = 108
          Top = 16
          Width = 56
          Height = 21
          TabOrder = 0
        end
        object EdST: TEdit
          Left = 108
          Top = 40
          Width = 56
          Height = 21
          TabOrder = 1
        end
        object dmkEdICMS: TdmkEdit
          Left = 224
          Top = 16
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object dmkEdIPI: TdmkEdit
          Left = 224
          Top = 40
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCodImp: TEdit
          Left = 108
          Top = 64
          Width = 56
          Height = 21
          TabOrder = 2
        end
        object dmkEdPreco: TdmkEdit
          Left = 224
          Top = 64
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object CkPrecoICM: TCheckBox
          Left = 12
          Top = 92
          Width = 329
          Height = 17
          Caption = 'O pre'#231'o indicado j'#225' cont'#233'm o ICMS padr'#227'o.'
          TabOrder = 6
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 187
      Width = 624
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 620
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 476
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 10
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 624
    Height = 251
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 188
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 57
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsArtigosGrupos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 76
        Top = 24
        Width = 300
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsArtigosGrupos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object GroupBox2: TGroupBox
        Left = 16
        Top = 48
        Width = 361
        Height = 121
        Caption = ' Nota fiscal: '
        TabOrder = 2
        object Label11: TLabel
          Left = 12
          Top = 42
          Width = 88
          Height = 13
          Caption = 'Situa'#231#227'o tribut'#225'ria:'
        end
        object Label12: TLabel
          Left = 12
          Top = 18
          Width = 92
          Height = 13
          Caption = 'Classifica'#231#227'o fiscal:'
        end
        object Label13: TLabel
          Left = 180
          Top = 18
          Width = 82
          Height = 13
          Caption = '% ICMS (padr'#227'o):'
        end
        object Label17: TLabel
          Left = 180
          Top = 42
          Width = 27
          Height = 13
          Caption = '% IPI:'
        end
        object Label18: TLabel
          Left = 12
          Top = 68
          Width = 92
          Height = 13
          Caption = 'C'#243'digo (impress'#227'o):'
        end
        object Label19: TLabel
          Left = 180
          Top = 66
          Width = 31
          Height = 13
          Caption = 'Pre'#231'o:'
        end
        object DBEdit1: TDBEdit
          Left = 108
          Top = 16
          Width = 56
          Height = 21
          DataField = 'CF'
          DataSource = DsArtigosGrupos
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 108
          Top = 40
          Width = 56
          Height = 21
          DataField = 'ST'
          DataSource = DsArtigosGrupos
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 268
          Top = 16
          Width = 72
          Height = 21
          DataField = 'ICMS'
          DataSource = DsArtigosGrupos
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 268
          Top = 40
          Width = 72
          Height = 21
          DataField = 'IPI'
          DataSource = DsArtigosGrupos
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 268
          Top = 64
          Width = 72
          Height = 21
          DataField = 'Preco'
          DataSource = DsArtigosGrupos
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 108
          Top = 64
          Width = 56
          Height = 21
          DataField = 'CodImp'
          DataSource = DsArtigosGrupos
          TabOrder = 5
        end
        object DBCheckBox1: TDBCheckBox
          Left = 12
          Top = 92
          Width = 329
          Height = 17
          Caption = 'O pre'#231'o indicado j'#225' cont'#233'm o ICMS padr'#227'o.'
          DataField = 'PrecoICM'
          DataSource = DsArtigosGrupos
          TabOrder = 6
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 187
      Width = 624
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 236
        Top = 15
        Width = 386
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 253
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 124
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 624
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 360
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 85
        Height = 32
        Caption = 'Artigos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 85
        Height = 32
        Caption = 'Artigos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 85
        Height = 32
        Caption = 'Artigos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DsArtigosGrupos: TDataSource
    DataSet = QrArtigosGrupos
    Left = 488
    Top = 161
  end
  object QrArtigosGrupos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrArtigosGruposBeforeOpen
    AfterOpen = QrArtigosGruposAfterOpen
    AfterScroll = QrArtigosGruposAfterScroll
    SQL.Strings = (
      'SELECT * FROM artigosgrupos'
      'WHERE Codigo > 0')
    Left = 460
    Top = 161
    object QrArtigosGruposLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'artigosgrupos.Lk'
    end
    object QrArtigosGruposDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'artigosgrupos.DataCad'
    end
    object QrArtigosGruposDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'artigosgrupos.DataAlt'
    end
    object QrArtigosGruposUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'artigosgrupos.UserCad'
    end
    object QrArtigosGruposUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'artigosgrupos.UserAlt'
    end
    object QrArtigosGruposCodigo: TSmallintField
      FieldName = 'Codigo'
      Origin = 'artigosgrupos.Codigo'
    end
    object QrArtigosGruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'artigosgrupos.Nome'
      Size = 30
    end
    object QrArtigosGruposFluxo: TIntegerField
      FieldName = 'Fluxo'
      Origin = 'artigosgrupos.Fluxo'
    end
    object QrArtigosGruposReceitaR: TIntegerField
      FieldName = 'ReceitaR'
      Origin = 'artigosgrupos.ReceitaR'
    end
    object QrArtigosGruposReceitaA: TIntegerField
      FieldName = 'ReceitaA'
      Origin = 'artigosgrupos.ReceitaA'
    end
    object QrArtigosGruposGrandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'artigosgrupos.Grandeza'
    end
    object QrArtigosGruposAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'artigosgrupos.AlterWeb'
    end
    object QrArtigosGruposAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'artigosgrupos.Ativo'
    end
    object QrArtigosGruposCF: TWideStringField
      FieldName = 'CF'
      Origin = 'artigosgrupos.CF'
      Size = 10
    end
    object QrArtigosGruposST: TWideStringField
      FieldName = 'ST'
      Origin = 'artigosgrupos.ST'
      Size = 10
    end
    object QrArtigosGruposICMS: TFloatField
      FieldName = 'ICMS'
      Origin = 'artigosgrupos.ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrArtigosGruposIPI: TFloatField
      FieldName = 'IPI'
      Origin = 'artigosgrupos.IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrArtigosGruposPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'artigosgrupos.Preco'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrArtigosGruposCodImp: TWideStringField
      FieldName = 'CodImp'
      Origin = 'artigosgrupos.CodImp'
      Size = 10
    end
    object QrArtigosGruposPrecoICM: TSmallintField
      FieldName = 'PrecoICM'
    end
  end
end
