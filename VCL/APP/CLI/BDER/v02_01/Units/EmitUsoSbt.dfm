object FmEmitUsoSbt: TFmEmitUsoSbt
  Left = 339
  Top = 185
  Caption = 'QUI-RECEI-024 :: Uso de Substrato de Rendimento'
  ClientHeight = 359
  ClientWidth = 790
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 790
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 742
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 694
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 403
        Height = 32
        Caption = 'Uso de Substrato de Rendimento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 403
        Height = 32
        Caption = 'Uso de Substrato de Rendimento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 403
        Height = 32
        Caption = 'Uso de Substrato de Rendimento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 790
    Height = 197
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 790
      Height = 197
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 790
        Height = 101
        Align = alTop
        Caption = ' Dados atuais:'
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 786
          Height = 84
          Align = alClient
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 0
          object Label3: TLabel
            Left = 4
            Top = 4
            Width = 47
            Height = 13
            Caption = 'Pesagem:'
          end
          object Label5: TLabel
            Left = 72
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Emiss'#227'o:'
            FocusControl = DBEdit1
          end
          object Label6: TLabel
            Left = 496
            Top = 4
            Width = 40
            Height = 13
            Caption = 'Receita:'
            FocusControl = DBEdit2
          end
          object Label7: TLabel
            Left = 128
            Top = 44
            Width = 35
            Height = 13
            Caption = 'Cliente:'
            FocusControl = DBEdit4
          end
          object Label8: TLabel
            Left = 208
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Peso:'
            FocusControl = DBEdit5
          end
          object Label9: TLabel
            Left = 276
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
            FocusControl = DBEdit6
          end
          object Label15: TLabel
            Left = 84
            Top = 44
            Width = 29
            Height = 13
            Caption = 'Ful'#227'o:'
            FocusControl = DBEdit10
          end
          object Label1: TLabel
            Left = 392
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
            FocusControl = DBEdit8
          end
          object Label2: TLabel
            Left = 4
            Top = 44
            Width = 52
            Height = 13
            Caption = 'Espessura:'
            FocusControl = DBEdit11
          end
          object Label26: TLabel
            Left = 620
            Top = 44
            Width = 42
            Height = 13
            Caption = 'Controle:'
          end
          object DBEdit1: TDBEdit
            Left = 72
            Top = 20
            Width = 133
            Height = 21
            DataField = 'DataEmis'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 496
            Top = 20
            Width = 40
            Height = 21
            DataField = 'Numero'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 536
            Top = 20
            Width = 237
            Height = 21
            DataField = 'NOME'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 128
            Top = 60
            Width = 488
            Height = 21
            DataField = 'NOMECI'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 3
          end
          object DBEdit5: TDBEdit
            Left = 208
            Top = 20
            Width = 64
            Height = 21
            DataField = 'Peso'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 4
          end
          object DBEdit6: TDBEdit
            Left = 276
            Top = 20
            Width = 40
            Height = 21
            DataField = 'Qtde'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 5
          end
          object DBEdit10: TDBEdit
            Left = 84
            Top = 60
            Width = 40
            Height = 21
            DataField = 'Fulao'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 6
          end
          object DBEdit7: TDBEdit
            Left = 4
            Top = 20
            Width = 64
            Height = 21
            DataField = 'Codigo'
            DataSource = DsEmit
            TabOrder = 7
          end
          object DBEdit8: TDBEdit
            Left = 392
            Top = 20
            Width = 100
            Height = 21
            DataField = 'AreaM2'
            DataSource = DsEmit
            TabOrder = 8
          end
          object DBEdit9: TDBEdit
            Left = 316
            Top = 20
            Width = 73
            Height = 21
            DataField = 'DefPeca'
            DataSource = DsEmit
            TabOrder = 9
          end
          object DBEdit11: TDBEdit
            Left = 4
            Top = 60
            Width = 77
            Height = 21
            DataField = 'Espessura'
            DataSource = DsEmit
            TabOrder = 10
          end
          object EdControle: TdmkEdit
            Left = 620
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 101
        Width = 790
        Height = 96
        Align = alClient
        TabOrder = 1
        object Label4: TLabel
          Left = 12
          Top = 8
          Width = 118
          Height = 13
          Caption = 'Substrato de rendimento:'
        end
        object Label10: TLabel
          Left = 276
          Top = 8
          Width = 72
          Height = 13
          Caption = 'Couros inteiros:'
        end
        object LaAreaM2: TLabel
          Left = 372
          Top = 8
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object LaAreaP2: TLabel
          Left = 468
          Top = 8
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
        end
        object LaPeso: TLabel
          Left = 572
          Top = 8
          Width = 42
          Height = 13
          Caption = 'Peso kg:'
        end
        object Label17: TLabel
          Left = 276
          Top = 48
          Width = 82
          Height = 13
          Caption = 'Saldo couros int.:'
          FocusControl = DBEdit12
        end
        object Label20: TLabel
          Left = 372
          Top = 48
          Width = 68
          Height = 13
          Caption = 'Saldo '#225'rea m'#178':'
          FocusControl = DBEdit13
        end
        object Label21: TLabel
          Left = 468
          Top = 48
          Width = 66
          Height = 13
          Caption = 'Saldo '#225'rea ft'#178':'
          FocusControl = DBEdit14
        end
        object Label11: TLabel
          Left = 88
          Top = 48
          Width = 85
          Height = 13
          Caption = '% rend. esperado:'
        end
        object SpeedButton1: TSpeedButton
          Left = 564
          Top = 63
          Width = 23
          Height = 23
          Caption = 'c'
          OnClick = SpeedButton1Click
        end
        object EdIDEmitSbtQtd: TdmkEditCB
          Left = 12
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBIDEmitSbtQtd
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBIDEmitSbtQtd: TdmkDBLookupComboBox
          Left = 68
          Top = 24
          Width = 205
          Height = 21
          KeyField = 'Controle'
          ListField = 'NO_EmitSbtCad'
          ListSource = DsEmitSbtQtd
          TabOrder = 1
          dmkEditCB = EdIDEmitSbtQtd
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCouIntros: TdmkEdit
          Left = 276
          Top = 24
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdAreaM2: TdmkEditCalc
          Left = 372
          Top = 24
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          dmkEditCalcA = EdAreaP2
          CalcType = ctM2toP2
          CalcFrac = cfQuarto
        end
        object EdAreaP2: TdmkEditCalc
          Left = 468
          Top = 24
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          dmkEditCalcA = EdAreaM2
          CalcType = ctP2toM2
          CalcFrac = cfCento
        end
        object EdPesoKg: TdmkEdit
          Left = 572
          Top = 24
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object DBEdit12: TDBEdit
          Left = 276
          Top = 64
          Width = 92
          Height = 21
          DataField = 'SdoCouIntros'
          DataSource = DsEmitSbtQtd
          TabOrder = 6
        end
        object DBEdit13: TDBEdit
          Left = 372
          Top = 64
          Width = 92
          Height = 21
          DataField = 'SdoAreaM2'
          DataSource = DsEmitSbtQtd
          TabOrder = 7
        end
        object DBEdit14: TDBEdit
          Left = 468
          Top = 64
          Width = 92
          Height = 21
          DataField = 'SdoAreaP2'
          DataSource = DsEmitSbtQtd
          TabOrder = 8
        end
        object DBEdit15: TDBEdit
          Left = 92
          Top = 64
          Width = 105
          Height = 21
          DataField = 'PercEsperRend'
          DataSource = DsEmitSbtQtd
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 9
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 245
    Width = 790
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 786
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 289
    Width = 790
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 644
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 642
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEmit: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emi.*, gru.Nome NO_EmitGru '
      'FROM emit emi'
      'LEFT JOIN emitgru gru ON gru.Codigo=emi.EmitGru')
    Left = 560
    Top = 65532
    object QrEmitCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrEmitStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrEmitNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrEmitNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrEmitNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrEmitTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrEmitNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrEmitClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrEmitTipificacao: TSmallintField
      FieldName = 'Tipificacao'
    end
    object QrEmitTempoR: TSmallintField
      FieldName = 'TempoR'
    end
    object QrEmitTempoP: TSmallintField
      FieldName = 'TempoP'
    end
    object QrEmitSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrEmitEspessura: TWideStringField
      FieldName = 'Espessura'
      Size = 15
    end
    object QrEmitPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmitLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmitDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmitUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,#00.00'
    end
    object QrEmitFulao: TWideStringField
      FieldName = 'Fulao'
      Size = 5
    end
    object QrEmitDefPeca: TWideStringField
      FieldName = 'DefPeca'
      Size = 15
    end
    object QrEmitCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrEmitObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrEmitAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmitTipific: TSmallintField
      FieldName = 'Tipific'
    end
    object QrEmitAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitSetrEmi: TSmallintField
      FieldName = 'SetrEmi'
    end
    object QrEmitCod_Espess: TIntegerField
      FieldName = 'Cod_Espess'
    end
    object QrEmitCodDefPeca: TIntegerField
      FieldName = 'CodDefPeca'
    end
    object QrEmitSourcMP: TSmallintField
      FieldName = 'SourcMP'
    end
    object QrEmitCustoTo: TFloatField
      FieldName = 'CustoTo'
    end
    object QrEmitCustoKg: TFloatField
      FieldName = 'CustoKg'
    end
    object QrEmitCustoM2: TFloatField
      FieldName = 'CustoM2'
    end
    object QrEmitCusUSM2: TFloatField
      FieldName = 'CusUSM2'
    end
    object QrEmitEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrEmitRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
    end
    object QrEmitNO_EmitGru: TWideStringField
      FieldName = 'NO_EmitGru'
      Size = 60
    end
    object QrEmitSemiCodPeca: TIntegerField
      FieldName = 'SemiCodPeca'
    end
    object QrEmitSemiTxtPeca: TWideStringField
      FieldName = 'SemiTxtPeca'
      Size = 15
    end
    object QrEmitSemiPeso: TFloatField
      FieldName = 'SemiPeso'
    end
    object QrEmitSemiQtde: TFloatField
      FieldName = 'SemiQtde'
    end
    object QrEmitSemiAreaM2: TFloatField
      FieldName = 'SemiAreaM2'
    end
    object QrEmitSemiRendim: TFloatField
      FieldName = 'SemiRendim'
    end
    object QrEmitSemiCodEspe: TIntegerField
      FieldName = 'SemiCodEspe'
    end
    object QrEmitSemiTxtEspe: TWideStringField
      FieldName = 'SemiTxtEspe'
      Size = 15
    end
    object QrEmitBRL_USD: TFloatField
      FieldName = 'BRL_USD'
    end
    object QrEmitBRL_EUR: TFloatField
      FieldName = 'BRL_EUR'
    end
    object QrEmitDtaCambio: TDateField
      FieldName = 'DtaCambio'
    end
    object QrEmitVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
    end
    object QrEmitHoraIni: TTimeField
      FieldName = 'HoraIni'
    end
    object QrEmitGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrEmitNoGraCorCad: TWideStringField
      FieldName = 'NoGraCorCad'
      Size = 30
    end
    object QrEmitSemiCodEspReb: TIntegerField
      FieldName = 'SemiCodEspReb'
    end
  end
  object DsEmit: TDataSource
    DataSet = QrEmit
    Left = 560
    Top = 40
  end
  object QrEmitSbtQtd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sbc.Nome NO_EmitSbtCad, sbq.*  '
      'FROM emitsbtqtd sbq '
      'LEFT JOIN emitsbtcad sbc ON sbc.Codigo=sbq.EmitSbtCad '
      'WHERE sbq.Codigo=12324 '
      ' ')
    Left = 633
    Top = 1
    object QrEmitSbtQtdControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmitSbtQtdNO_EmitSbtCad: TWideStringField
      FieldName = 'NO_EmitSbtCad'
      Size = 60
    end
    object QrEmitSbtQtdSdoCouIntros: TFloatField
      FieldName = 'SdoCouIntros'
      Required = True
    end
    object QrEmitSbtQtdSdoAreaM2: TFloatField
      FieldName = 'SdoAreaM2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEmitSbtQtdSdoAreaP2: TFloatField
      FieldName = 'SdoAreaP2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEmitSbtQtdPercEsperRend: TFloatField
      FieldName = 'PercEsperRend'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsEmitSbtQtd: TDataSource
    DataSet = QrEmitSbtQtd
    Left = 632
    Top = 49
  end
end
