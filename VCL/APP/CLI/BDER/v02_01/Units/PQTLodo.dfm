object FmPQTLodo: TFmPQTLodo
  Left = 339
  Top = 185
  Caption = 'QUI-RECEI-031 :: Transporte de Lodo ETE'
  ClientHeight = 415
  ClientWidth = 604
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 270
    Width = 604
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 604
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 556
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 508
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 296
        Height = 32
        Caption = 'Transporte de Lodo ETE'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 296
        Height = 32
        Caption = 'Transporte de Lodo ETE'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 296
        Height = 32
        Caption = 'Transporte de Lodo ETE'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 301
    Width = 604
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 600
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 345
    Width = 604
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 458
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 456
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 97
    Width = 604
    Height = 168
    Align = alTop
    TabOrder = 0
    object Label6: TLabel
      Left = 8
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label1: TLabel
      Left = 92
      Top = 4
      Width = 40
      Height = 13
      Caption = 'Ve'#237'culo:'
    end
    object SbVeiculo: TSpeedButton
      Left = 428
      Top = 20
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbVeiculoClick
    end
    object Label3: TLabel
      Left = 92
      Top = 44
      Width = 128
      Height = 13
      Caption = 'Motorista (Fornece Outros):'
    end
    object SbMotorista: TSpeedButton
      Left = 428
      Top = 60
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbMotoristaClick
    end
    object Label4: TLabel
      Left = 92
      Top = 84
      Width = 29
      Height = 13
      Caption = 'Local:'
    end
    object SbLocal: TSpeedButton
      Left = 428
      Top = 100
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbLocalClick
    end
    object SbUnidMed: TSpeedButton
      Left = 324
      Top = 140
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbUnidMedClick
    end
    object Label7: TLabel
      Left = 320
      Top = 124
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label17: TLabel
      Left = 92
      Top = 124
      Width = 117
      Height = 13
      Caption = 'Unidade de Medida [F3]:'
    end
    object EdControle: TdmkEdit
      Left = 8
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVeiculo: TdmkEditCB
      Left = 92
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdVeiculoRedefinido
      DBLookupComboBox = CBVeiculo
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBVeiculo: TdmkDBLookupComboBox
      Left = 148
      Top = 20
      Width = 280
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Descricao'
      ListSource = DsVeiculos
      TabOrder = 2
      dmkEditCB = EdVeiculo
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdMotorista: TdmkEditCB
      Left = 92
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBMotorista
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBMotorista: TdmkDBLookupComboBox
      Left = 148
      Top = 60
      Width = 280
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsMotorista
      TabOrder = 4
      dmkEditCB = EdMotorista
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdLocal: TdmkEditCB
      Left = 92
      Top = 100
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBLocal
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBLocal: TdmkDBLookupComboBox
      Left = 148
      Top = 100
      Width = 280
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_LOC_CEN'
      ListSource = DsStqCenLoc
      TabOrder = 6
      dmkEditCB = EdLocal
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdQtde: TdmkEdit
      Left = 348
      Top = 140
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdUnidMed: TdmkEditCbUM
      Left = 92
      Top = 140
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBUnidMed
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
      EditUM = EdSigla
    end
    object EdSigla: TdmkEditUM
      Left = 148
      Top = 140
      Width = 40
      Height = 21
      TabOrder = 9
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      EditCbUM = EdUnidMed
      DBLookupCbUM = CBUnidMed
    end
    object CBUnidMed: TdmkDBLookupCbUM
      Left = 188
      Top = 140
      Width = 137
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsUnidMed
      TabOrder = 10
      dmkEditCB = EdUnidMed
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 48
    Width = 604
    Height = 49
    Align = alTop
    TabOrder = 5
    object Label2: TLabel
      Left = 8
      Top = 4
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object EdCodigo: TdmkEdit
      Left = 8
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNO_: TdmkEdit
      Left = 92
      Top = 20
      Width = 337
      Height = 21
      Enabled = False
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object QrVeiculos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vei.Codigo, CONCAT(vei.placa, '#39' - '#39','
      'vma.Nome, '#39' '#39', vmo.Nome) Descricao, MotorisPadr'
      'FROM veiculos vei'
      'LEFT JOIN veicmarcas vma ON vma.Codigo=vei.Marca'
      'LEFT JOIN veicmodels vmo ON vmo.Codigo=vei.Modelo'
      'ORDER BY vei.placa')
    Left = 216
    Top = 44
    object QrVeiculosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVeiculosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 131
    end
    object QrVeiculosMotorisPadr: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'MotorisPadr'
      Calculated = True
    end
  end
  object DsVeiculos: TDataSource
    DataSet = QrVeiculos
    Left = 216
    Top = 92
  end
  object QrMotorista: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 324
    Top = 44
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsMotorista: TDataSource
    DataSet = QrMotorista
    Left = 324
    Top = 92
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT scl.Controle, scl.CodUsu,  scc.EntiSitio, '
      'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN '
      'FROM stqcenloc scl '
      'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo '
      'ORDER BY NO_LOC_CEN ')
    Left = 216
    Top = 144
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 216
    Top = 192
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 324
    Top = 144
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 324
    Top = 192
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu'
      'FROM unidmed'
      'WHERE Sigla = :P0')
    Left = 24
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Sigla'
      'FROM unidmed'
      'WHERE CodUsu = :P0')
    Left = 24
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
  end
end
