unit Defeitosloc;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnDmkProcFunc, UnGOTOy,  UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkImage, UnDmkEnums;

type
  TFmDefeitosloc = class(TForm)
    PainelDados: TPanel;
    DsDefeitosloc: TDataSource;
    QrDefeitosloc: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrDefeitoslocCodigo: TIntegerField;
    QrDefeitoslocNome: TWideStringField;
    QrDefeitoslocLk: TIntegerField;
    QrDefeitoslocDataCad: TDateField;
    QrDefeitoslocDataAlt: TDateField;
    QrDefeitoslocUserCad: TIntegerField;
    QrDefeitoslocUserAlt: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrDefeitoslocAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrDefeitoslocAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrDefeitoslocBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmDefeitosloc: TFmDefeitosloc;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmDefeitosloc.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmDefeitosloc.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrDefeitoslocCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmDefeitosloc.DefParams;
begin
  VAR_GOTOTABELA := 'Defeitosloc';
  VAR_GOTOMYSQLTABLE := QrDefeitosloc;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM defeitosloc');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmDefeitosloc.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      if SQLType = stIns then
      begin
        EdCodigo.Text := '';
        EdNome.Text   := '';
      end else begin
        EdCodigo.Text := IntToStr(QrDefeitoslocCodigo.Value);
        EdNome.Text   := QrDefeitoslocNome.Value;
      end;
      EdNome.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmDefeitosloc.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmDefeitosloc.QueryPrincipalAfterOpen;
begin
end;

procedure TFmDefeitosloc.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmDefeitosloc.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmDefeitosloc.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmDefeitosloc.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmDefeitosloc.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmDefeitosloc.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmDefeitosloc.BtAlteraClick(Sender: TObject);
var
  Defeitosloc : Integer;
begin
  Defeitosloc := QrDefeitoslocCodigo.Value;
  if not UMyMod.SelLockY(Defeitosloc, Dmod.MyDB, 'Defeitosloc', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Defeitosloc, Dmod.MyDB, 'Defeitosloc', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmDefeitosloc.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrDefeitoslocCodigo.Value;
  Close;
end;

procedure TFmDefeitosloc.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descrição.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO defeitosloc SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Defeitosloc', 'Defeitosloc', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE defeitosloc SET ');
    Codigo := QrDefeitoslocCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Nome=:P0, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  //
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[03].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Defeitosloc', 'Codigo');
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmDefeitosloc.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Defeitosloc', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Defeitosloc', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Defeitosloc', 'Codigo');
end;

procedure TFmDefeitosloc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmDefeitosloc.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrDefeitoslocCodigo.Value,LaRegistro.Caption);
end;

procedure TFmDefeitosloc.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmDefeitosloc.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmDefeitosloc.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmDefeitosloc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmDefeitosloc.QrDefeitoslocAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmDefeitosloc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Defeitosloc', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmDefeitosloc.QrDefeitoslocAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrDefeitoslocCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrDefeitoslocCodigo.Value, False);
end;

procedure TFmDefeitosloc.SbQueryClick(Sender: TObject);
begin
  LocCod(QrDefeitoslocCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Defeitosloc', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmDefeitosloc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDefeitosloc.QrDefeitoslocBeforeOpen(DataSet: TDataSet);
begin
  QrDefeitoslocCodigo.DisplayFormat := FFormatFloat;
end;

end.

