unit MPClasMulGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, mySQLDbTables,
  Menus, ComCtrls, dmkEditDateTimePicker, dmkRadioGroup, dmkEditCB, dmkEdit,
  DBCtrls, dmkDBLookupComboBox, UnInternalConsts, UnDmkProcFunc, dmkImage,
  UnDmkEnums, UnGrade_PF, DmkDAC_PF;

type
  TFmMPClasMulGer = class(TForm)
    Panel1: TPanel;
    QrSMI: TmySQLQuery;
    DsSMI: TDataSource;
    Panel3: TPanel;
    QrNeg: TmySQLQuery;
    DsNeg: TDataSource;
    QrPos: TmySQLQuery;
    DsPos: TDataSource;
    QrSMISMIMultIns: TIntegerField;
    QrPosNivel1: TIntegerField;
    QrPosNO_PRD: TWideStringField;
    QrPosPrdGrupTip: TIntegerField;
    QrPosUnidMed: TIntegerField;
    QrPosNO_PGT: TWideStringField;
    QrPosSIGLA: TWideStringField;
    QrPosNO_TAM: TWideStringField;
    QrPosNO_COR: TWideStringField;
    QrPosGraCorCad: TIntegerField;
    QrPosGraGruC: TIntegerField;
    QrPosGraGru1: TIntegerField;
    QrPosGraTamI: TIntegerField;
    QrPosDataHora: TDateTimeField;
    QrPosIDCtrl: TIntegerField;
    QrPosTipo: TIntegerField;
    QrPosOriCodi: TIntegerField;
    QrPosOriCtrl: TIntegerField;
    QrPosOriCnta: TIntegerField;
    QrPosOriPart: TIntegerField;
    QrPosEmpresa: TIntegerField;
    QrPosStqCenCad: TIntegerField;
    QrPosGraGruX: TIntegerField;
    QrPosQtde: TFloatField;
    QrPosPecas: TFloatField;
    QrPosPeso: TFloatField;
    QrPosAlterWeb: TSmallintField;
    QrPosAtivo: TSmallintField;
    QrPosAreaM2: TFloatField;
    QrPosAreaP2: TFloatField;
    QrPosFatorClas: TFloatField;
    QrPosQuemUsou: TIntegerField;
    QrPosRetorno: TSmallintField;
    QrPosParTipo: TIntegerField;
    QrPosParCodi: TIntegerField;
    QrPosDebCtrl: TIntegerField;
    QrPosSMIMultIns: TIntegerField;
    QrNegNivel1: TIntegerField;
    QrNegNO_PRD: TWideStringField;
    QrNegPrdGrupTip: TIntegerField;
    QrNegUnidMed: TIntegerField;
    QrNegNO_PGT: TWideStringField;
    QrNegSIGLA: TWideStringField;
    QrNegNO_TAM: TWideStringField;
    QrNegNO_COR: TWideStringField;
    QrNegGraCorCad: TIntegerField;
    QrNegGraGruC: TIntegerField;
    QrNegGraGru1: TIntegerField;
    QrNegGraTamI: TIntegerField;
    QrNegDataHora: TDateTimeField;
    QrNegIDCtrl: TIntegerField;
    QrNegTipo: TIntegerField;
    QrNegOriCodi: TIntegerField;
    QrNegOriCtrl: TIntegerField;
    QrNegOriCnta: TIntegerField;
    QrNegOriPart: TIntegerField;
    QrNegEmpresa: TIntegerField;
    QrNegStqCenCad: TIntegerField;
    QrNegGraGruX: TIntegerField;
    QrNegQtde: TFloatField;
    QrNegPecas: TFloatField;
    QrNegPeso: TFloatField;
    QrNegAlterWeb: TSmallintField;
    QrNegAtivo: TSmallintField;
    QrNegAreaM2: TFloatField;
    QrNegAreaP2: TFloatField;
    QrNegFatorClas: TFloatField;
    QrNegQuemUsou: TIntegerField;
    QrNegRetorno: TSmallintField;
    QrNegParTipo: TIntegerField;
    QrNegParCodi: TIntegerField;
    QrNegDebCtrl: TIntegerField;
    QrNegSMIMultIns: TIntegerField;
    PMAltera: TPopupMenu;
    Data1: TMenuItem;
    Xdiasapsentrada1: TMenuItem;
    QrFonte: TmySQLQuery;
    QrFonteData: TDateField;
    Panel4: TPanel;
    Panel5: TPanel;
    DBGrid3: TDBGrid;
    Label1: TLabel;
    Panel6: TPanel;
    Label2: TLabel;
    DBGrid2: TDBGrid;
    Splitter1: TSplitter;
    Panel7: TPanel;
    DBGrid1: TDBGrid;
    QrClientesI: TmySQLQuery;
    QrClientesINOMECLIENTEI: TWideStringField;
    QrClientesICodigo: TIntegerField;
    DsClientesI: TDataSource;
    QrProcedencias: TmySQLQuery;
    QrProcedenciasCodigo: TIntegerField;
    QrProcedenciasNOMECLIENTEI: TWideStringField;
    DsProcedencias: TDataSource;
    Panel9: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    Label18: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label19: TLabel;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    RGOrdem: TRadioGroup;
    CBClienteI: TdmkDBLookupComboBox;
    dmkEdOS: TdmkEdit;
    CBProcedencia: TdmkDBLookupComboBox;
    EdLote: TEdit;
    EdMarca: TEdit;
    EdClienteI: TdmkEditCB;
    EdProcedencia: TdmkEditCB;
    RGEncerrado: TdmkRadioGroup;
    RGtpNF: TdmkRadioGroup;
    EdNF: TdmkEdit;
    Label3: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel10: TPanel;
    PnSaiDesis: TPanel;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSMIBeforeClose(DataSet: TDataSet);
    procedure QrSMIAfterScroll(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrSMIAfterOpen(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure Xdiasapsentrada1Click(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure DBGrid3DblClick(Sender: TObject);
    procedure RGOrdemClick(Sender: TObject);
    procedure Panel4Resize(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenSMI(SMIMultIns, IDCtrl_Neg, IDCtrl_Pos: Integer);
    //procedure AlteraSMIA(Tipo: Integer);

  public
    { Public declarations }
  end;

  var
  FmMPClasMulGer: TFmMPClasMulGer;

implementation

uses UnMyObjects, Module, dmkGeral, UnGrade_Tabs, MyDBCheck, SMI_Edita;

{$R *.DFM}

{
procedure TFmMPClasMulGer.AlteraSMIA(Tipo: Integer);
var
  IDCtrl, GraGruX, StqCenCad: Integer;
  Qtde, Pecas, AreaM2, AreaP2, Peso: Double;
begin
  case Tipo of
    VAR_FATID_0101:
    begin
      IDCtrl    := QrPosIDCtrl.Value;
      Qtde      := QrPosQtde.Value;
      Pecas     := QrPosPecas.Value;
      AreaM2    := QrPosAreaM2.Value;
      AreaP2    := QrPosAreaP2.Value;
      Peso      := QrPosPeso.Value;
      GraGruX   := QrPosGraGruX.Value;
      StqCenCad := QrPosStqCenCad.Value;
    end;
    VAR_FATID_0104:
    begin
      IDCtrl    := QrNegIDCtrl.Value;
      Qtde      := - QrNegQtde.Value;
      Pecas     := - QrNegPecas.Value;
      AreaM2    := - QrNegAreaM2.Value;
      AreaP2    := - QrNegAreaP2.Value;
      Peso      := - QrNegPeso.Value;
      GraGruX   := QrNegGraGruX.Value;
      StqCenCad := QrNegStqCenCad.Value;
    end;
    else begin
      Geral.MB_Aviso('Altera��o cancelada!' + sLineBreak +
      'Tipo de lan�amento n�o implementado: ' + FormatFloat('0', Tipo) + sLineBreak
      + 'AVISE A DERMATEK!');
      Exit;
    end;
  end;
  if DBCheck.CriaFm(TFmSMI_Edita, FmSMI_Edita, afmoNegarComAviso) then
  begin
    FmSMI_Edita.FNegativos := Tipo = VAR_FATID_0104;
    //
    FmSMI_Edita.QrSMIA.Close;
    FmSMI_Edita.QrSMIA.Params[0].AsInteger := IDCtrl;
    UnDmkDAC_PF.AbreQuery(FmSMI_Edita.QrSMIA, Dmod.MyDB);
    //
    FmSMI_Edita.EdGraGruX.ValueVariant   := GraGruX;
    FmSMI_Edita.CBGraGruX.KeyValue       := GraGruX;
    //
    FmSMI_Edita.EdStqCenCad.ValueVariant := StqCenCad;
    FmSMI_Edita.CBStqCenCad.KeyValue     := StqCenCad;

    FmSMI_Edita.EdQtde.ValueVariant      := Qtde;
    FmSMI_Edita.EdPecas.ValueVariant     := Pecas;
    FmSMI_Edita.EdAreaM2.ValueVariant    := AreaM2;
    FmSMI_Edita.EdAreaP2.ValueVariant    := AreaP2;
    FmSMI_Edita.EdPeso.ValueVariant      := Peso;
    //
    FmSMI_Edita.ShowModal;
    FmSMI_Edita.Destroy;
  end;
  ReopenSMI(QrSMISMIMultIns.Value, QrNegIDCtrl.Value, QrPosIDCtrl.Value);
end;
}

procedure TFmMPClasMulGer.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmMPClasMulGer.BtExcluiClick(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o de TODA classifica��o m�ltipla selecionada?'
  ) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE SMIMultIns > 0');
      Dmod.QrUpd.SQL.Add('AND SMIMultIns=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrSMISMIMultIns.Value;
      Dmod.QrUpd.ExecSQL;
      //
      QrNeg.First;
      while not QrNeg.Eof do
      begin
        Dmod.AtualizaMPIn(QrNegOriCodi.Value);
        //
        QrNeg.Next;
      end;
      //
      QrPos.First;
      while not QrPos.Eof do
      begin
        Dmod.AtualizaMPIn(QrPosOriCodi.Value);
        //
        QrPos.Next;
      end;
      //
      ReopenSMI(QrSMISMIMultIns.Value, 0, 0);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmMPClasMulGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPClasMulGer.DBGrid2DblClick(Sender: TObject);
begin
  Grade_PF.AlteraSMIA(QrNeg);
  ReopenSMI(QrSMISMIMultIns.Value, QrNegIDCtrl.Value, QrPosIDCtrl.Value);
end;

procedure TFmMPClasMulGer.DBGrid3DblClick(Sender: TObject);
begin
  Grade_PF.AlteraSMIA(QrPos);
  ReopenSMI(QrSMISMIMultIns.Value, QrNegIDCtrl.Value, QrPosIDCtrl.Value);
end;

procedure TFmMPClasMulGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPClasMulGer.FormCreate(Sender: TObject);
var
  Data: Integer;
begin
  ImgTipo.SQLType := stLok;
  //
{
  FFromMPIn := 'FROM mpin wi';
  PageControl1.ActivePageIndex := 0;
  UnAppListas.ConfiguraTipificacao(-4, RGTipifNome, 3, 0);
  UnAppListas.ConfiguraTipificacao(-5, RGAnimal,    3, 0);
}
  Data := Geral.ReadAppKey('DataMPIn', Application.Title, ktInteger,
    Int(Date), HKEY_LOCAL_MACHINE);
  TPIni.Date := Data;
  TPFim.Date := Date+30;
  UnDmkDAC_PF.AbreQuery(QrClientesI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcedencias, Dmod.MyDB);
  ReopenSMI(0, 0, 0);
end;

procedure TFmMPClasMulGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPClasMulGer.Panel4Resize(Sender: TObject);
begin
  try
    DBGrid2.Height := (DBGrid2.Height + DBGrid3.Height) div 2;
  except
    ;
  end;
end;

procedure TFmMPClasMulGer.QrSMIAfterOpen(DataSet: TDataSet);
begin
  BtExclui.Enabled := QrSMI.RecordCount > 0;
  BtAltera.Enabled := QrSMI.RecordCount > 0;
end;

procedure TFmMPClasMulGer.QrSMIAfterScroll(DataSet: TDataSet);
begin
  QrNeg.Close;
  QrNeg.Params[0].AsInteger := QrSMISMIMultIns.Value;
  UnDmkDAC_PF.AbreQuery(QrNeg, Dmod.MyDB);
  //
  QrPos.Close;
  QrPos.Params[0].AsInteger := QrSMISMIMultIns.Value;
  UnDmkDAC_PF.AbreQuery(QrPos, Dmod.MyDB);
end;

procedure TFmMPClasMulGer.QrSMIBeforeClose(DataSet: TDataSet);
begin
  BtExclui.Enabled := False;
  BtAltera.Enabled := False;
  QrNeg.Close;
  QrPos.Close;
end;

procedure TFmMPClasMulGer.ReopenSMI(SMIMultIns, IDCtrl_Neg, IDCtrl_Pos: Integer);
var
  DataI, DataF: String;
  ClienteI, Procedencia, NF, tpNF: Integer;
begin
  DataI := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  DataF := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  //
  NF   := EdNF.ValueVariant;
  tpNF := RGtpNF.ItemIndex;
  //
  QrSMI.Close;
  QrSMI.SQL.Clear;
  QrSMI.SQL.Add('SELECT DISTINCT smia.SMIMultIns');
  QrSMI.SQL.Add('FROM stqmovitsa smia');
  QrSMI.SQL.Add('LEFT JOIN mpin mpi ON mpi.Controle=smia.OriCodi');
  if (NF > 0) or (tpNF > 0) then
    QrSMI.SQL.Add('LEFT JOIN mpinits its ON its.Controle=mpi.Controle');
  QrSMI.SQL.Add('WHERE smia.SMIMultIns > 0');
  QrSMI.SQL.Add('AND smia.Tipo IN (' +
                 FormatFloat('0', VAR_FATID_0101) + ',' +
                 FormatFloat('0', VAR_FATID_0104) + ')');
  //QrSMI.SQL.Add('WHERE mpi.Data BETWEEN :P0 AND :P1');
  QrSMI.SQL.Add(dmkPF.SQL_Periodo('AND mpi.Data ', TPIni.Date, TPFim.Date, True, True));
  ClienteI := Geral.IMV(EdClienteI.Text);
  if ClienteI <> 0 then
    QrSMI.SQL.Add('AND mpi.ClienteI=' + FormatFloat('0', ClienteI));
  Procedencia := Geral.IMV(EdProcedencia.Text);
  if Procedencia <> 0 then
    QrSMI.SQL.Add('AND mpi.Procedencia=' + FormatFloat('0', Procedencia));
  if dmkEdOS.ValueVariant > 0 then
    QrSMI.SQL.Add('AND mpi.Controle='+IntToStr(dmkEdOS.ValueVariant));
  if Trim(EdMarca.Text) <> '' then
    QrSMI.SQL.Add('AND mpi.Marca LIKE "%' + EdMarca.Text + '%"');
  if Trim(EdLote.Text) <> '' then
    QrSMI.SQL.Add('AND mpi.Lote LIKE "%' + EdLote.Text + '%"');
  if RGEncerrado.ItemIndex < 2 then
    QrSMI.SQL.Add('AND mpi.Encerrado=' + FormatFloat('0', RGEncerrado.ItemIndex));
  if (NF > 0) or (tpNF > 0) then
  begin
    if NF > 0 then
      QrSMI.SQL.Add('AND its.NF =' + FormatFloat('0', NF));
    case tpNF of
      // Nada
      0: ;
      // Que entrou com NF
      1: QrSMI.SQL.Add('AND its.NF_Inn_Fut = 0');
      // Que entrou sem NF mas foi gerada uma NF de entrada
      2: QrSMI.SQL.Add('AND (its.NF_Inn_Fut = 1 AND its.NF_Inn_Its > 0)');
      // Que entrou sem NF e ainda n�o foi gerada uma NF de entrada
      3: QrSMI.SQL.Add('AND (its.NF_Inn_Fut = 1 AND its.NF_Inn_Its = 0)');
    end;
  end;
  QrSMI.SQL.Add('ORDER BY smia.SMIMultIns DESC');
  UnDmkDAC_PF.AbreQuery(QrSMI, Dmod.MyDB);
  //
  QrSMI.Locate('SMIMultIns', SMIMultIns, []);
  if QrNeg.State <> dsInactive then
    QrNeg.Locate('IDCtrl', IDCtrl_Neg, []);
  if QrPos.State <> dsInactive then
    QrPos.Locate('IDCtrl', IDCtrl_Pos, []);
end;

procedure TFmMPClasMulGer.RGOrdemClick(Sender: TObject);
begin
  ReopenSMI(0, 0, 0);
end;

procedure TFmMPClasMulGer.Xdiasapsentrada1Click(Sender: TObject);
var
  DataHora, Dias: String;
  DD: Integer;
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET DataHora=:P0');
  Dmod.QrUpd.SQL.Add('WHERE IDCtrl=:P1');
  //
  Dias := '3';
  if InputQuery('Altera��o de Data', 'Inorme os dias:', Dias) then
  begin
    DD := Geral.IMV(Dias);
    //
    QrNeg.First;
    while not QrNeg.Eof do
    begin
      QrFonte.Close;
      QrFonte.Params[00].AsInteger := QrNegOriCodi.Value;
      UnDmkDAC_PF.AbreQuery(QrFonte, Dmod.MyDB);
      //
      DataHora := FormatDateTime('yyyy-mm-dd hh:nn:ss', QrFonteData.Value + DD);
      //
      Dmod.QrUpd.Params[00].AsString  := DataHora;
      Dmod.QrUpd.Params[01].AsInteger := QrNegIDCtrl.Value;
      Dmod.QrUpd.ExecSQL;
      //
      QrNeg.Next;
    end;
    //
    QrPos.First;
    while not QrPos.Eof do
    begin
      QrFonte.Close;
      QrFonte.Params[00].AsInteger := QrPosOriCodi.Value;
      UnDmkDAC_PF.AbreQuery(QrFonte, Dmod.MyDB);
      //
      DataHora := FormatDateTime('yyyy-mm-dd hh:nn:ss', QrFonteData.Value + DD);
      //
      Dmod.QrUpd.Params[00].AsString  := DataHora;
      Dmod.QrUpd.Params[01].AsInteger := QrPosIDCtrl.Value;
      Dmod.QrUpd.ExecSQL;
      //
      QrPos.Next;
    end;
    //
    ReopenSMI(QrSMISMIMultIns.Value, 0, 0);
  end;
end;

end.
