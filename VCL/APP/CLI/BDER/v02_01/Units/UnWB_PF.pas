unit UnWB_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkDBLookupComboBox, mySQLDbTables;

type
  TUnWB_PF = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure ReopenWBPallet(Qry: TmySQLQuery; EdPallet: TdmkEditCB;
              CBPallet: TdmkDBLookupComboBox; Empresa, Fornece, GraGruX:
              Integer);
  end;

var
  WB_PF: TUnWB_PF;


implementation

uses DmkDAC_PF, Module;

{ TUnWB_PF }

procedure TUnWB_PF.ReopenWBPallet(Qry: TmySQLQuery; EdPallet: TdmkEditCB;
CBPallet: TdmkDBLookupComboBox; Empresa, Fornece, GraGruX: Integer);
var
  Pallet: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT DISTINCT let.Codigo, let.Nome ',
  'FROM wbpallet let',
  'LEFT JOIN wbpalart art ON art.Codigo=let.Codigo',
  'LEFT JOIN wbpalfrn frn ON frn.Controle=art.Controle',
  'WHERE let.Ativo=1 ',
  'AND let.Empresa=' + Geral.FF0(Empresa),
  'AND art.GraGruX=' + Geral.FF0(GraGruX),
  'AND frn.Fornece=' + Geral.FF0(Fornece),
  'ORDER BY let.Nome ',
  '']);
  //
  Pallet := EdPallet.ValueVariant;
  if Pallet <> 0 then
  begin
    if not Qry.Locate('Codigo', Pallet, []) then
    begin
      EdPallet.ValueVariant := 0;
      CBPallet.KeyValue     := Null;
    end;
  end;
end;

end.
