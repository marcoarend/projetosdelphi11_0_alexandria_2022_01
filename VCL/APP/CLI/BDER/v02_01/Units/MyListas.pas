unit MyListas;

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, mysqlDBTables, UnMyLinguas,
  UnInternalConsts, dmkGeral, UnDmkProcFunc, UnDmkEnums, BlueDermConsts,
  UnProjGroup_Consts;

type
  TMyListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function CriaListaImpDOS(FImpDOS: TStringList): Boolean;
    function CriaListaUserSets(FUserSets: TStringList): Boolean;
    function CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
    //
    function CriaListaTabelas(Database: TmySQLDatabase; Lista:
             TList<TTabelas>): Boolean;
    //function CriaListaTabelas(Database: TmySQLDatabase; FTabelas: TStringList): Boolean;
    function CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
    function CriaListaIndices(TabelaBase, TabelaNome: String; FLIndices: TList<TIndices>):
             Boolean;
    function  CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
              var TemControle: TTemControle): Boolean;
    function CriaListaQeiLnk(TabelaCria: String; FLQeiLnk: TList<TQeiLnk>): Boolean;
    function CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function ExcluiTab: Boolean;
    function ExcluiReg: Boolean;
    function ExcluiIdx: Boolean;
    procedure VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
              DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ConfiguracoesIniciais(UsaCoresRel: Integer);
  end;

{ Movido para BlueDermConsts
const
  CO_VERMCW: Int64 = 1905160647;
  CO_VERMLA: Int64 = 1912310001;
  CO_VERM28: Int64 = 2201271652;
  CO_SIGLA_APP = 'BDER';
  CO_DMKID_APP = 2;
  CO_GRADE_APP = False;
  CO_VLOCAL = False;
  CO_EXTRA_LCT_003 = False;
  //
}
var
  MyList: TMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;
  FRJanelas : TJanelas;
  //
  _ArrClieSets: array[01..32] of String;
  _MaxClieSets: Integer;
  _ArrFornSets: array[01..32] of String;
  _MaxFornSets: Integer;

implementation

uses UnMyObjects, ModuleGeral, MyDBCheck, UMySQLModule,
  UnIBGE_DTB_Tabs, NFe_Tabs,
  // SINTEGRA_Tabs,
  UnAnotacoes_Tabs, UnConsumoGerl_Tabs, UnConsumo_Tabs,
  UnGrade_Tabs, CMPT_Tabs,
  SPED_EFD_II_Tabs, SPED_Geral_Tabs, SPED_EFD_PC_Tabs,
  UnPerfJan_Tabs, Module, Geral_TbTerc, Mail_Tabs,
  UnAll_Tabs, UnEnti_Tabs,
  UnFinance_Tabs,
  UnImprime_Tabs, UnMoedas_Tabs,
  UnFiscal_Tabs, UnGFat_Tabs, UnGPed_Tabs,
  UnEmpresas_Tabs,
  UnCNAB_Tabs, AppListas,
  UnTributos_Tabs, UnPraz_Tabs,
  UnUMedi_Tabs, UnEntities,
  UnPQ_Tabs, UnVS_Tabs, UnERPSinc_Tabs,
  UnContab_Tabs, UnDmkACBr_Tabs,
  UnSrvTom_Tabs, NFSe_TbTerc;

function TMyListas.CriaListaTabelas(Database: TmySQLDatabase; Lista:
  TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    if Database = Dmod.MyDB then
    begin
      All_Tabs.CarregaListaTabelas(FTabelas);
      ERPSinc_Tabs.CarregaListaTabelas(FTabelas);
      Enti_Tabs.CarregaListaTabelas(FTabelas);
      PerfJan_Tabs.CarregaListaTabelas(FTabelas);
      Finance_Tabs.CarregaListaTabelas(FTabelas);
      Finance_Tabs.ComplementaListaComLcts(Lista);
      NFe_Tb.CarregaListaTabelas(Database, FTabelas);
      CMPT_Tb.CarregaListaTabelas(FTabelas);
      Grade_Tabs.CarregaListaTabelas(FTabelas);
      SPED_Geral_Tb.CarregaListaTabelas(Database, FTabelas);
      SPED_EFD_II_Tb.CarregaListaTabelas(Database, FTabelas);
      SPED_EFD_PC_Tb.CarregaListaTabelas(Database, FTabelas);
      //SINTEGRA_Tb.CarregaListaTabelas(FTabelas);
      Mail_Tb.CarregaListaTabelas(FTabelas);
      Imprime_Tabs.CarregaListaTabelas(FTabelas);
      Moedas_Tabs.CarregaListaTabelas(FTabelas);
      Fiscal_Tabs.CarregaListaTabelas(FTabelas);
      GFat_Tabs.CarregaListaTabelas(FTabelas);
      GPed_Tabs.CarregaListaTabelas(FTabelas);
      CNAB_Tabs.CarregaListaTabelas(Database, FTabelas);
      Anotacoes_Tabs.CarregaListaTabelas(FTabelas);
      Empresas_Tabs.CarregaListaTabelas(FTabelas);
      Tributos_Tabs.CarregaListaTabelas(FTabelas);
      Praz_Tabs.CarregaListaTabelas(Database, FTabelas);
      UMedi_Tabs.CarregaListaTabelas(Database, FTabelas);
      ConsumoGerl_Tabs.CarregaListaTabelas(FTabelas);
      Consumo_Tabs.CarregaListaTabelas(FTabelas);
      VS_Tabs.CarregaListaTabelas(FTabelas);
      PQ_Tabs.CarregaListaTabelas(FTabelas);
      Contab_Tabs.CarregaListaTabelas(Database, FTabelas);
      DmkACBr_Tabs.CarregaListaTabelas(FTabelas);
      SrvTom_Tabs.CarregaListaTabelas(Database, FTabelas);
      UnNFSe_TbTerc.CarregaListaTabelas(Lista);
      //
      //s� teste MyLinguas.AdTbLst(Lista, False, Lowercase('_produtos'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('ArtigosGrupos'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Balancos'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('BalancosIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('BalanCli'), '');
      //Linguas.AdTbLst(Lista, False, Lowercase('CFOP2003'), ''); Cashier
      //Linguas.AdTbLst(Lista, False, Lowercase('CECT'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('CECTInn'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('CECTOut'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('CECTOutIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Consts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Controle'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('DefPecas'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Equipes'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Espessuras'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Fabricas'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Fluxos'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('FluxosIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('FluxosICt'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('FluxosSet'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('ListaSetores'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('FluxProCab'), 'Fluxos');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('FluxProIts'), 'FluxosIts');
      MyLinguas.AdTbLst(Lista, False, Lowercase('FluxPcpCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('FluxPcpIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('FluxPcpStg'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('MPArti'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPIn'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPInIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPInDefeitos'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPInExp'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPInPerdas'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPClas'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPEstagios'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPGrup'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPP'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPSubProd'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPV'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPVIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPVItVS'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPVPss'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPV2'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPV2Its'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPV2Bxa'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('MPVImpOpc'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('NFeMPInn'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('NFeMPIts'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('Pallets'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('PalletsIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Ponto'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('PontoIts'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PortSerBal'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('PortSerLei'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('PortSerIts'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('RolPerdas'), '');
      //


    end else
    if Database = DModG.AllID_DB then
    begin
      IBGE_DTB_Tabs.CarregaListaTabelas(FTabelas);
      UnGeral_TbTerc.CarregaListaTabelas(FTabelas);
      NFe_Tb.CarregaListaTabelas(Database, FTabelas);
      SPED_Geral_Tb.CarregaListaTabelas(Database, FTabelas);
      SPED_EFD_II_Tb.CarregaListaTabelas(Database, FTabelas);
      SPED_EFD_PC_Tb.CarregaListaTabelas(Database, FTabelas);
      UnNFSe_TbTerc.CarregaListaTabelas(Lista);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    Finance_Tabs.CarregaListaTabelasLocais(Lista);
    MyLinguas.AdTbLst(Lista, False, 'ImpBMZ', '');
    MyLinguas.AdTbLst(Lista, False, 'ImpEquipe', '');
    MyLinguas.AdTbLst(Lista, False, 'ImpEquMes', '');
    ///Linguas.AdTbLst(Lista, False, 'MoviPQ', '');
    //Linguas.AdTbLst(Lista, False, 'SetSetores', '');
    MyLinguas.AdTbLst(Lista, False, 'Palet', '');
    //Linguas.AdTbLst(Lista, False, 'mpinccc', '');
    //MyLinguas.AdTbLst(Lista, False, 'mpinrat', '');
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
var
  I: Integer;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('XXXXX') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"(N�o informado)"');
    end else if Uppercase(Tabela) = Uppercase('ArtigosGrupos') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('-1|" SERVICOS "');
      FListaSQL.Add('0|" NAO DEFINIDO"');
    end else if Uppercase(Tabela) = Uppercase('Balancos') then
    begin
      FListaSQL.Add('Periodo|Confirmado');
      FListaSQL.Add('0|"V"');
    (*end else if Uppercase(Tabela) = Uppercase('CFOP2003') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"(N�O DEFINIDA)"');*)
    end else if Uppercase(Tabela) = Uppercase('Consts') then
    begin
      FListaSQL.Add('Compras|Caleiro|Curtimento|Recurtimento|Acabamento|CaldeiraETE');
      FListaSQL.Add('"Compras"|"Caleiro"|"Curtimento"|"Recurtimento"|"Acabamento"|"CaldeiraETE"');
    end else if Uppercase(Tabela) = Uppercase('CouNiv2') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add(Geral.FF0(CO_COU_NIV2_000_INDEF) + '|"Indefinido"');
      FListaSQL.Add(Geral.FF0(CO_COU_NIV2_001_COURO) + '|"Couro"');
      FListaSQL.Add(Geral.FF0(CO_COU_NIV2_002_RASPA) + '|"Raspa"');
      FListaSQL.Add(Geral.FF0(CO_COU_NIV2_003_SUBPR) + '|"Subproduto"');
    end else if Uppercase(Tabela) = Uppercase('DefPecas') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"(N�O DEFINIDO)"');
    end else if Uppercase(Tabela) = Uppercase('EntiTransfere') then
    begin
      FListaSQL.Add('Tabela|Campo');
      FListaSQL.Add('"Contas"| "Empresa"');
      FListaSQL.Add('"Dividas"| "Terceiro"');
      FListaSQL.Add('"Emit"| "ClienteI"');
      FListaSQL.Add('"EmitIts"| "PQCI"');
      FListaSQL.Add('"EmitIts"| "Cli_Orig"');
      FListaSQL.Add('"EmitIts"| "Cli_Dest"');
      FListaSQL.Add('"EntiTransp"| "Transp"');
      FListaSQL.Add('"Formulas"| "ClienteI"');
      FListaSQL.Add('"' + VAR_LCT + '"| "Fornecedor"');
      FListaSQL.Add('"' + VAR_LCT + '"| "Cliente"');
      FListaSQL.Add('"' + VAR_LCT + '"| "CliInt"');
      FListaSQL.Add('"' + VAR_LCT + '"| "ForneceI"');
      FListaSQL.Add('"' + VAR_LCT + '"| "Vendedor"');
      FListaSQL.Add('"' + VAR_LCT + '"| "Account"');
      FListaSQL.Add('"' + VAR_LCT + '"| ""');
      FListaSQL.Add('"MPIn"| "ClienteI"');
      FListaSQL.Add('"MPIn"| "Procedencia"');
      FListaSQL.Add('"MPIn"| "Transportadora"');
      FListaSQL.Add('"MPV"| "Cliente"');
      FListaSQL.Add('"MPV"| "Transp"');
      FListaSQL.Add('"MPV"| "Vendedor"');
      FListaSQL.Add('"MPV2"| "Cliente"');
      FListaSQL.Add('"MPV2"| "Transp"');
      FListaSQL.Add('"MPV2"| "Vendedor"');
      FListaSQL.Add('"MPP"| "Cliente"');
      FListaSQL.Add('"MPP"| "Transp"');
      FListaSQL.Add('"MPP"| "Vendedor"');
      FListaSQL.Add('"Pallets"| "CliInt"');
      FListaSQL.Add('"PQ"| "IQ"');
      FListaSQL.Add('"PQCli"| "CI"');
      FListaSQL.Add('"PQCot"| "IQ"');
      FListaSQL.Add('"PQCot"| "CI"');
      FListaSQL.Add('"PQE"| "IQ"');
      FListaSQL.Add('"PQE"| "CI"');
      FListaSQL.Add('"PQE"| "Transportadora"');
      FListaSQL.Add('"PQFor"| "IQ"');
      FListaSQL.Add('"PQFor"| "CI"');
      FListaSQL.Add('"PQPed"| "Fornece"');
      FListaSQL.Add('"PQPed"| "Transporte"');
      FListaSQL.Add('"PQX"| "CliOrig"');
      FListaSQL.Add('"PQX"| "CliDest"');
    end else if Uppercase(Tabela) = Uppercase('Espessuras') then
    begin
      FListaSQL.Add('Codigo|Linhas');
      FListaSQL.Add('-1|""');
      FListaSQL.Add('0|""');
    end else if Uppercase(Tabela) = Uppercase('Fabricas') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|""');
    end else if Uppercase(Tabela) = Uppercase('Equipes') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"�nica"');
    end else if Uppercase(Tabela) = Uppercase('Fluxos') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"(N�O DEFINIDO)"');
    end else
    if Uppercase(Tabela) = Uppercase('FluxPcpCab') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"(N�O DEFINIDO)"');
    end else
    if Uppercase(Tabela) = Uppercase('PrdGrupTip') then
    begin
      //FListaSQL.Add('Codigo|CodUsu|Nome|MadeBy|Fracio|Gradeado|TipPrd|NivCad|FaixaIni|FaixaFim|TitNiv1|TitNiv2|ImpedeCad|Tipo_Item');
      // Igap� > FListaSQL.Add('-3|-3|"-INDEFINIDOS-"|2|3|0|3|2|0|0|"-Indefinidos-"|"Grupo"|1');
    end else if Uppercase(Tabela) = Uppercase('GraGru1') then
    begin
      FListaSQL.Add('CodUsu|Nivel1|Nivel2|Nivel3|Nivel4|Nivel5|Nome|PrdGrupTip|GraTamCad|GerBxaEstq');
      FListaSQL.Add(   '-1|-0001|-3|0|0|0|"."|-2|0|0');  // Vago
      FListaSQL.Add(   '-2|-0002|-3|0|0|0|"�gua"|-2|0|0');
      FListaSQL.Add(   '-3|-0003|-3|0|0|0|"Reciclo"|-2|0|0');
      FListaSQL.Add(   '-4|-0004|-3|0|0|0|"Sobra de Banho"|-2|0|0');
      FListaSQL.Add(   '-5|-0005|-3|0|0|0|""|-2|0|0'); // Vazio N�O USAR !!!!
      FListaSQL.Add(  '-10|-0010|-3|0|0|0|"ESCORRER"|-2|0|0');
      FListaSQL.Add(  '-11|-0011|-3|0|0|0|"ESCORRER BEM"|-2|0|0');
      FListaSQL.Add(  '-12|-0012|-3|0|0|0|"ESCORRER PARADO"|-2|0|0');
      FListaSQL.Add(  '-13|-0013|-3|0|0|0|"ESCORRER RODANDO"|-2|0|0');
      FListaSQL.Add(  '-20|-0020|-3|0|0|0|"ESGOTAR"|-2|0|0');
      FListaSQL.Add(  '-21|-0021|-3|0|0|0|"ESGOTAR BEM"|-2|0|0');
      FListaSQL.Add(  '-22|-0022|-3|0|0|0|"ESGOTAR PARADO"|-2|0|0');
      FListaSQL.Add(  '-23|-0023|-3|0|0|0|"ESGOTAR RODANDO"|-2|0|0');
      FListaSQL.Add(  '-30|-0030|-3|0|0|0|"DRENAR"|-2|0|0');
      FListaSQL.Add(  '-31|-0031|-3|0|0|0|"DRENAR BEM"|-2|0|0');
      FListaSQL.Add(  '-32|-0032|-3|0|0|0|"DRENAR PARADO"|-2|0|0');
      FListaSQL.Add(  '-33|-0033|-3|0|0|0|"DRENAR RODANDO"|-2|0|0');
      FListaSQL.Add(  '-99|-0099|-3|0|0|0|""|-2|0|0'); // Linha em branco
      //
      FListaSQL.Add( '-101|-0101|0|0|0|0|"Couro bovino N.D."|-1|0|3');
      FListaSQL.Add( '-201|-0201|0|0|0|0|"Couro bovino fresco"|-1|0|3');
      FListaSQL.Add( '-301|-0301|0|0|0|0|"Couro bovino salgado"|-1|0|3');
      FListaSQL.Add( '-401|-0401|0|0|0|0|"Couro bovino salmourado"|-1|0|3');
      FListaSQL.Add( '-501|-0501|0|0|0|0|"Couro bovino salmourado salgado"|-1|0|3');
      FListaSQL.Add( '-601|-0601|0|0|0|0|"Couro bovino seco"|-1|0|3');
      FListaSQL.Add( '-701|-0701|0|0|0|0|"Couro bovino congelado"|-1|0|3');
      FListaSQL.Add( '-801|-0801|0|0|0|0|"Couro bovino aparas"|-1|0|3');
      FListaSQL.Add( '-901|-0901|0|0|0|0|"Couro bovino outros"|-1|0|3');
      FListaSQL.Add('-1001|-1001|0|0|0|0|"Couro bovino fresco tratado"|-1|0|3');
      FListaSQL.Add('-1101|-1101|0|0|0|0|"Couro bovino resfriado"|-1|0|3');
      FListaSQL.Add('-1201|-1201|0|0|0|0|"Couro bovino resfriado tratado"|-1|0|3');
      //
(*
      FListaSQL.Add('-102|-0102|0|0|"Couro suino N.D."|-1|0|3');
      FListaSQL.Add('-202|-0202|0|0|"Couro suino fresco"|-1|0|3');
      FListaSQL.Add('-302|-0302|0|0|"Couro suino salgado"|-1|0|3');
      FListaSQL.Add('-402|-0402|0|0|"Couro suino salmourado"|-1|0|3');
      FListaSQL.Add('-502|-0502|0|0|"Couro suino salmourado salgado"|-1|0|3');
      FListaSQL.Add('-602|-0602|0|0|"Couro suino seco"|-1|0|3');
      FListaSQL.Add('-702|-0702|0|0|"Couro suino congelado"|-1|0|3');
      FListaSQL.Add('-802|-0802|0|0|"Couro suino aparas"|-1|0|3');
      FListaSQL.Add('-902|-0902|0|0|"Couro suino outros"|-1|0|3');
      FListaSQL.Add('-1002|-1002|0|0|"Couro suino fresco tratado"|-1|0|3');
      FListaSQL.Add('-1102|-1102|0|0|"Couro suino resfriado"|-1|0|3');
      FListaSQL.Add('-1202|-1202|0|0|"Couro suino resfriado tratado"|-1|0|3');
      //
      FListaSQL.Add('-103|-0103|0|0|"Couro caprino N.D."|-1|0|3');
      FListaSQL.Add('-203|-0203|0|0|"Couro caprino fresco"|-1|0|3');
      FListaSQL.Add('-303|-0303|0|0|"Couro caprino salgado"|-1|0|3');
      FListaSQL.Add('-403|-0403|0|0|"Couro caprino salmourado"|-1|0|3');
      FListaSQL.Add('-503|-0503|0|0|"Couro caprino salmourado salgado"|-1|0|3');
      FListaSQL.Add('-603|-0603|0|0|"Couro caprino seco"|-1|0|3');
      FListaSQL.Add('-703|-0703|0|0|"Couro caprino congelado"|-1|0|3');
      FListaSQL.Add('-803|-0803|0|0|"Couro caprino aparas"|-1|0|3');
      FListaSQL.Add('-903|-0903|0|0|"Couro caprino outros"|-1|0|3');
      FListaSQL.Add('-1003|-1003|0|0|"Couro caprino fresco tratado"|-1|0|3');
      FListaSQL.Add('-1103|-1103|0|0|"Couro caprino resfriado"|-1|0|3');
      FListaSQL.Add('-1203|-1203|0|0|"Couro caprino resfriado tratado"|-1|0|3');
      //
      FListaSQL.Add('-104|-0104|0|0|"Couro bubalino N.D."|-1|0|3');
      FListaSQL.Add('-204|-0204|0|0|"Couro bubalino fresco"|-1|0|3');
      FListaSQL.Add('-304|-0304|0|0|"Couro bubalino salgado"|-1|0|3');
      FListaSQL.Add('-404|-0404|0|0|"Couro bubalino salmourado"|-1|0|3');
      FListaSQL.Add('-504|-0504|0|0|"Couro bubalino salmourado salgado"|-1|0|3');
      FListaSQL.Add('-604|-0604|0|0|"Couro bubalino seco"|-1|0|3');
      FListaSQL.Add('-704|-0704|0|0|"Couro bubalino congelado"|-1|0|3');
      FListaSQL.Add('-804|-0804|0|0|"Couro bubalino aparas"|-1|0|3');
      FListaSQL.Add('-904|-0904|0|0|"Couro bubalino outros"|-1|0|3');
      FListaSQL.Add('-1004|-1004|0|0|"Couro bubalino fresco tratado"|-1|0|3');
      FListaSQL.Add('-1104|-1104|0|0|"Couro bubalino resfriado"|-1|0|3');
      FListaSQL.Add('-1204|-1204|0|0|"Couro bubalino resfriado tratado"|-1|0|3');
      //                0     0
      FListaSQL.Add('-105|-0105|0|0|"Couro ovino N.D."|-1|0|3');
      FListaSQL.Add('-205|-0205|0|0|"Couro ovino fresco"|-1|0|3');
      FListaSQL.Add('-305|-0305|0|0|"Couro ovino salgado"|-1|0|3');
      FListaSQL.Add('-405|-0405|0|0|"Couro ovino salmourado"|-1|0|3');
      FListaSQL.Add('-505|-0505|0|0|"Couro ovino salmourado salgado"|-1|0|3');
      FListaSQL.Add('-605|-0605|0|0|"Couro ovino seco"|-1|0|3');
      FListaSQL.Add('-705|-0705|0|0|"Couro ovino congelado"|-1|0|3');
      FListaSQL.Add('-805|-0805|0|0|"Couro ovino aparas"|-1|0|3');
      FListaSQL.Add('-905|-0905|0|0|"Couro ovino outros"|-1|0|3');
      FListaSQL.Add('-1005|-1005|0|0|"Couro ovino fresco tratado"|-1|0|3');
      FListaSQL.Add('-1105|-1105|0|0|"Couro ovino resfriado"|-1|0|3');
      FListaSQL.Add('-1205|-1205|0|0|"Couro ovino resfriado tratado"|-1|0|3');
      //                0     0
      FListaSQL.Add('-106|-0106|0|0|"Couro de avestruz N.D."|-1|0|3');
      FListaSQL.Add('-206|-0206|0|0|"Couro de avestruz fresco"|-1|0|3');
      FListaSQL.Add('-306|-0306|0|0|"Couro de avestruz salgado"|-1|0|3');
      FListaSQL.Add('-406|-0406|0|0|"Couro de avestruz salmourado"|-1|0|3');
      FListaSQL.Add('-506|-0506|0|0|"Couro de avestruz salmourado salgado"|-1|0|3');
      FListaSQL.Add('-606|-0606|0|0|"Couro de avestruz seco"|-1|0|3');
      FListaSQL.Add('-706|-0706|0|0|"Couro de avestruz congelado"|-1|0|3');
      FListaSQL.Add('-806|-0806|0|0|"Couro de avestruz aparas"|-1|0|3');
      FListaSQL.Add('-906|-0906|0|0|"Couro de avestruz outros"|-1|0|3');
      FListaSQL.Add('-1006|-1006|0|0|"Couro de avestruz fresco tratado"|-1|0|3');
      FListaSQL.Add('-1106|-1106|0|0|"Couro de avestruz resfriado"|-1|0|3');
      FListaSQL.Add('-1206|-1206|0|0|"Couro de avestruz resfriado tratado"|-1|0|3');
      //                0     0
      FListaSQL.Add('-107|-0107|0|0|"Couro de coelho N.D."|-1|0|3');
      FListaSQL.Add('-207|-0207|0|0|"Couro de coelho fresco"|-1|0|3');
      FListaSQL.Add('-307|-0307|0|0|"Couro de coelho salgado"|-1|0|3');
      FListaSQL.Add('-407|-0407|0|0|"Couro de coelho salmourado"|-1|0|3');
      FListaSQL.Add('-507|-0507|0|0|"Couro de coelho salmourado salgado"|-1|0|3');
      FListaSQL.Add('-607|-0607|0|0|"Couro de coelho seco"|-1|0|3');
      FListaSQL.Add('-707|-0707|0|0|"Couro de coelho congelado"|-1|0|3');
      FListaSQL.Add('-807|-0807|0|0|"Couro de coelho aparas"|-1|0|3');
      FListaSQL.Add('-907|-0907|0|0|"Couro de coelho outros"|-1|0|3');
      FListaSQL.Add('-1007|-1007|0|0|"Couro de coelho fresco tratado"|-1|0|3');
      FListaSQL.Add('-1107|-1107|0|0|"Couro de coelho resfriado"|-1|0|3');
      FListaSQL.Add('-1207|-1207|0|0|"Couro de coelho resfriado tratado"|-1|0|3');
      //                0     0
      FListaSQL.Add('-108|-0108|0|0|"Couro de jacar� N.D."|-1|0|3');
      FListaSQL.Add('-208|-0208|0|0|"Couro de jacar� fresco"|-1|0|3');
      FListaSQL.Add('-308|-0308|0|0|"Couro de jacar� salgado"|-1|0|3');
      FListaSQL.Add('-408|-0408|0|0|"Couro de jacar� salmourado"|-1|0|3');
      FListaSQL.Add('-508|-0508|0|0|"Couro de jacar� salmourado salgado"|-1|0|3');
      FListaSQL.Add('-608|-0608|0|0|"Couro de jacar� seco"|-1|0|3');
      FListaSQL.Add('-708|-0708|0|0|"Couro de jacar� congelado"|-1|0|3');
      FListaSQL.Add('-808|-0808|0|0|"Couro de jacar� aparas"|-1|0|3');
      FListaSQL.Add('-908|-0908|0|0|"Couro de jacar� outros"|-1|0|3');
      FListaSQL.Add('-1008|-1008|0|0|"Couro de jacar� fresco tratado"|-1|0|3');
      FListaSQL.Add('-1108|-1108|0|0|"Couro de jacar� resfriado"|-1|0|3');
      FListaSQL.Add('-1208|-1208|0|0|"Couro de jacar� resfriado tratado"|-1|0|3');
      //                0     0
      FListaSQL.Add('-109|-0109|0|0|"Couro de r�ptil N.D."|-1|0|3');
      FListaSQL.Add('-209|-0209|0|0|"Couro de r�ptil fresco"|-1|0|3');
      FListaSQL.Add('-309|-0309|0|0|"Couro de r�ptil salgado"|-1|0|3');
      FListaSQL.Add('-409|-0409|0|0|"Couro de r�ptil salmourado"|-1|0|3');
      FListaSQL.Add('-509|-0509|0|0|"Couro de r�ptil salmourado salgado"|-1|0|3');
      FListaSQL.Add('-609|-0609|0|0|"Couro de r�ptil seco"|-1|0|3');
      FListaSQL.Add('-709|-0709|0|0|"Couro de r�ptil congelado"|-1|0|3');
      FListaSQL.Add('-809|-0809|0|0|"Couro de r�ptil aparas"|-1|0|3');
      FListaSQL.Add('-909|-0909|0|0|"Couro de r�ptil outros"|-1|0|3');
      FListaSQL.Add('-1009|-1009|0|0|"Couro de r�ptil fresco tratado"|-1|0|3');
      FListaSQL.Add('-1109|-1109|0|0|"Couro de r�ptil resfriado"|-1|0|3');
      FListaSQL.Add('-1209|-1209|0|0|"Couro de r�ptil resfriado tratado"|-1|0|3');
      //
      FListaSQL.Add('-110|-0110|0|0|"Couro outros N.D."|-1|0|3');
      FListaSQL.Add('-210|-0210|0|0|"Couro outros fresco"|-1|0|3');
      FListaSQL.Add('-310|-0310|0|0|"Couro outros salgado"|-1|0|3');
      FListaSQL.Add('-410|-0410|0|0|"Couro outros salmourado"|-1|0|3');
      FListaSQL.Add('-510|-0510|0|0|"Couro outros salmourado salgado"|-1|0|3');
      FListaSQL.Add('-610|-0610|0|0|"Couro outros seco"|-1|0|3');
      FListaSQL.Add('-710|-0710|0|0|"Couro outros congelado"|-1|0|3');
      FListaSQL.Add('-810|-0810|0|0|"Couro outros aparas"|-1|0|3');
      FListaSQL.Add('-910|-0910|0|0|"Couro outros outros"|-1|0|3');
      FListaSQL.Add('-1010|-1010|0|0|"Couro outros fresco tratado"|-1|0|3');
      FListaSQL.Add('-1110|-1110|0|0|"Couro outros resfriado"|-1|0|3');
      FListaSQL.Add('-1210|-1210|0|0|"Couro outros resfriado tratado"|-1|0|3');
      //
*)
    end else if Uppercase(Tabela) = Uppercase('GraGru2') then
    begin
      FListaSQL.Add('Nivel2|CodUsu|Nivel3|Tipo_Item|Nome|PrdGrupTip');
      FListaSQL.Add('-1|-1|0|7|"Amostra de PQ"|-2');
      FListaSQL.Add('-2|-2|0|7|"Produto Qu�mico"|-2');
      FListaSQL.Add('-3|-3|0|-2|"Texto"|0');
      FListaSQL.Add('-4|-4|0|7|"Material de uso e consumo"|-2');
      FListaSQL.Add('-5|-5|0|10|"Outros"|-2');
      FListaSQL.Add('-6|-6|0|9|"Servi�o"|0');
      FListaSQL.Add('-7|-7|0|7|"Material de uso imediato"|-2'); // usar para produtos com entrada nula (sem estoque)
      FListaSQL.Add('-8|-8|0|2|"Embalagem"|0');
      FListaSQL.Add('-9|-9|0|99|"-Indefinido-"|0');
      FListaSQL.Add('-10|-10|0|10|"Insumo modificado"|-2');
      FListaSQL.Add('-11|-11|0|7|"EPI"|-2');
      FListaSQL.Add('-12|-12|0|7|"Manuten��o"|-2');
    end else if Uppercase(Tabela) = Uppercase('GraGruX') then
    begin
      FListaSQL.Add('Controle|GraGru1|GraGruC|GraTamI');
      //
      FListaSQL.Add('-1|-0001|-0001|1');
      FListaSQL.Add('-2|-0002|-0002|1');
      FListaSQL.Add('-3|-0003|-0003|1');
      FListaSQL.Add('-4|-0004|-0004|1');
      FListaSQL.Add('-5|-0005|-0005|1');
      FListaSQL.Add('-10|-0010|-0010|1');
      FListaSQL.Add('-11|-0011|-0011|1');
      FListaSQL.Add('-12|-0012|-0012|1');
      FListaSQL.Add('-13|-0013|-0013|1');
      FListaSQL.Add('-20|-0020|-0020|1');
      FListaSQL.Add('-21|-0021|-0021|1');
      FListaSQL.Add('-22|-0022|-0022|1');
      FListaSQL.Add('-123|-0123|-0123|1');
      FListaSQL.Add('-30|-0030|-0030|1');
      FListaSQL.Add('-31|-0031|-0031|1');
      FListaSQL.Add('-32|-0032|-0032|1');
      FListaSQL.Add('-33|-0033|-0033|1');
      FListaSQL.Add('-99|-0099|-0099|1');
      //
      FListaSQL.Add('-101|-0101|-0101|1');
      FListaSQL.Add('-201|-0201|-0201|1');
      FListaSQL.Add('-301|-0301|-0301|1');
      FListaSQL.Add('-401|-0401|-0401|1');
      FListaSQL.Add('-501|-0501|-0501|1');
      FListaSQL.Add('-601|-0601|-0601|1');
      FListaSQL.Add('-701|-0701|-0701|1');
      FListaSQL.Add('-801|-0801|-0801|1');
      FListaSQL.Add('-901|-0901|-0901|1');
      FListaSQL.Add('-1001|-1001|-1001|1');
      FListaSQL.Add('-1101|-1101|-1101|1');
      FListaSQL.Add('-1201|-1201|-1201|1');
      //
      FListaSQL.Add('-102|-0102|-0102|1');
(*
      FListaSQL.Add('-202|-0202|-0202|1');
      FListaSQL.Add('-302|-0302|-0302|1');
      FListaSQL.Add('-402|-0402|-0402|1');
      FListaSQL.Add('-502|-0502|-0502|1');
      FListaSQL.Add('-602|-0602|-0602|1');
      FListaSQL.Add('-702|-0702|-0702|1');
      FListaSQL.Add('-802|-0802|-0802|1');
      FListaSQL.Add('-902|-0902|-0902|1');
      FListaSQL.Add('-1002|-1002|-1002|1');
      FListaSQL.Add('-1102|-1102|-1102|1');
      FListaSQL.Add('-1202|-1202|-1202|1');
      //
      FListaSQL.Add('-103|-0103|-0103|1');
      FListaSQL.Add('-203|-0203|-0203|1');
      FListaSQL.Add('-303|-0303|-0303|1');
      FListaSQL.Add('-403|-0403|-0403|1');
      FListaSQL.Add('-503|-0503|-0503|1');
      FListaSQL.Add('-603|-0603|-0603|1');
      FListaSQL.Add('-703|-0703|-0703|1');
      FListaSQL.Add('-803|-0803|-0803|1');
      FListaSQL.Add('-903|-0903|-0903|1');
      FListaSQL.Add('-1003|-1003|-1003|1');
      FListaSQL.Add('-1103|-1103|-1103|1');
      FListaSQL.Add('-1203|-1203|-1203|1');
      //
      FListaSQL.Add('-104|-0104|-0104|1');
      FListaSQL.Add('-204|-0204|-0204|1');
      FListaSQL.Add('-304|-0304|-0304|1');
      FListaSQL.Add('-404|-0404|-0404|1');
      FListaSQL.Add('-504|-0504|-0504|1');
      FListaSQL.Add('-604|-0604|-0604|1');
      FListaSQL.Add('-704|-0704|-0704|1');
      FListaSQL.Add('-804|-0804|-0804|1');
      FListaSQL.Add('-904|-0904|-0904|1');
      FListaSQL.Add('-1004|-1004|-1004|1');
      FListaSQL.Add('-1104|-1104|-1104|1');
      FListaSQL.Add('-1204|-1204|-1204|1');
      //
      FListaSQL.Add('-105|-0105|-0105|1');
      FListaSQL.Add('-205|-0205|-0205|1');
      FListaSQL.Add('-305|-0305|-0305|1');
      FListaSQL.Add('-405|-0405|-0405|1');
      FListaSQL.Add('-505|-0505|-0505|1');
      FListaSQL.Add('-605|-0605|-0605|1');
      FListaSQL.Add('-705|-0705|-0705|1');
      FListaSQL.Add('-805|-0805|-0805|1');
      FListaSQL.Add('-905|-0905|-0905|1');
      FListaSQL.Add('-1005|-1005|-1005|1');
      FListaSQL.Add('-1105|-1105|-1105|1');
      FListaSQL.Add('-1205|-1205|-1205|1');
      //
      FListaSQL.Add('-106|-0106|-0106|1');
      FListaSQL.Add('-206|-0206|-0206|1');
      FListaSQL.Add('-306|-0306|-0306|1');
      FListaSQL.Add('-406|-0406|-0406|1');
      FListaSQL.Add('-506|-0506|-0506|1');
      FListaSQL.Add('-606|-0606|-0606|1');
      FListaSQL.Add('-706|-0706|-0706|1');
      FListaSQL.Add('-806|-0806|-0806|1');
      FListaSQL.Add('-906|-0906|-0906|1');
      FListaSQL.Add('-1006|-1006|-1006|1');
      FListaSQL.Add('-1106|-1106|-1106|1');
      FListaSQL.Add('-1206|-1206|-1206|1');
      //
      FListaSQL.Add('-107|-0107|-0107|1');
      FListaSQL.Add('-207|-0207|-0207|1');
      FListaSQL.Add('-307|-0307|-0307|1');
      FListaSQL.Add('-407|-0407|-0407|1');
      FListaSQL.Add('-507|-0507|-0507|1');
      FListaSQL.Add('-607|-0607|-0607|1');
      FListaSQL.Add('-707|-0707|-0707|1');
      FListaSQL.Add('-807|-0807|-0807|1');
      FListaSQL.Add('-907|-0907|-0907|1');
      FListaSQL.Add('-1007|-1007|-1007|1');
      FListaSQL.Add('-1107|-1107|-1107|1');
      FListaSQL.Add('-1207|-1207|-1207|1');
      //
      FListaSQL.Add('-108|-0108|-0108|1');
      FListaSQL.Add('-208|-0208|-0208|1');
      FListaSQL.Add('-308|-0308|-0308|1');
      FListaSQL.Add('-408|-0408|-0408|1');
      FListaSQL.Add('-508|-0508|-0508|1');
      FListaSQL.Add('-608|-0608|-0608|1');
      FListaSQL.Add('-708|-0708|-0708|1');
      FListaSQL.Add('-808|-0808|-0808|1');
      FListaSQL.Add('-908|-0908|-0908|1');
      FListaSQL.Add('-1008|-1008|-1008|1');
      FListaSQL.Add('-1108|-1108|-1108|1');
      FListaSQL.Add('-1208|-1208|-1208|1');
      //
      FListaSQL.Add('-109|-0109|-0109|1');
      FListaSQL.Add('-209|-0209|-0209|1');
      FListaSQL.Add('-309|-0309|-0309|1');
      FListaSQL.Add('-409|-0409|-0409|1');
      FListaSQL.Add('-509|-0509|-0509|1');
      FListaSQL.Add('-609|-0609|-0609|1');
      FListaSQL.Add('-709|-0709|-0709|1');
      FListaSQL.Add('-809|-0809|-0809|1');
      FListaSQL.Add('-909|-0909|-0909|1');
      FListaSQL.Add('-1009|-1009|-1009|1');
      FListaSQL.Add('-1109|-1109|-1109|1');
      FListaSQL.Add('-1209|-1209|-1209|1');
      //
      FListaSQL.Add('-110|-0110|-0110|1');
      FListaSQL.Add('-210|-0210|-0210|1');
      FListaSQL.Add('-310|-0310|-0310|1');
      FListaSQL.Add('-410|-0410|-0410|1');
      FListaSQL.Add('-510|-0510|-0510|1');
      FListaSQL.Add('-610|-0610|-0610|1');
      FListaSQL.Add('-710|-0710|-0710|1');
      FListaSQL.Add('-810|-0810|-0810|1');
      FListaSQL.Add('-910|-0910|-0910|1');
      FListaSQL.Add('-1010|-1010|-1010|1');
      FListaSQL.Add('-1110|-1110|-1110|1');
      FListaSQL.Add('-1210|-1210|-1210|1');
*)
      //
    end else if Uppercase(Tabela) = Uppercase('GraGruC') then
    begin
      FListaSQL.Add('Controle|Nivel1|GraCorCad');
      //
      FListaSQL.Add('-1|-0001|0');
      FListaSQL.Add('-2|-0002|0');
      FListaSQL.Add('-3|-0003|0');
      FListaSQL.Add('-4|-0004|0');
      FListaSQL.Add('-5|-0005|0');
      FListaSQL.Add('-10|-0010|0');
      FListaSQL.Add('-11|-0011|0');
      FListaSQL.Add('-12|-0012|0');
      FListaSQL.Add('-13|-0013|0');
      FListaSQL.Add('-20|-0020|0');
      FListaSQL.Add('-21|-0021|0');
      FListaSQL.Add('-22|-0022|0');
      FListaSQL.Add('-23|-0023|0');
      FListaSQL.Add('-30|-0030|0');
      FListaSQL.Add('-31|-0031|0');
      FListaSQL.Add('-32|-0032|0');
      FListaSQL.Add('-33|-0033|0');
      FListaSQL.Add('-99|-0099|0');
      //
      FListaSQL.Add('-101|-0101|0');
      FListaSQL.Add('-201|-0201|0');
      FListaSQL.Add('-301|-0301|0');
      FListaSQL.Add('-401|-0401|0');
      FListaSQL.Add('-501|-0501|0');
      FListaSQL.Add('-601|-0601|0');
      FListaSQL.Add('-701|-0701|0');
      FListaSQL.Add('-801|-0801|0');
      FListaSQL.Add('-901|-0901|0');
      FListaSQL.Add('-1001|-1001|0');
      FListaSQL.Add('-1101|-1101|0');
      FListaSQL.Add('-1201|-1201|0');
      //
(*
      FListaSQL.Add('-102|-0102|0');
      FListaSQL.Add('-202|-0202|0');
      FListaSQL.Add('-302|-0302|0');
      FListaSQL.Add('-402|-0402|0');
      FListaSQL.Add('-502|-0502|0');
      FListaSQL.Add('-602|-0602|0');
      FListaSQL.Add('-702|-0702|0');
      FListaSQL.Add('-802|-0802|0');
      FListaSQL.Add('-902|-0902|0');
      FListaSQL.Add('-1002|-1002|0');
      FListaSQL.Add('-1102|-1102|0');
      FListaSQL.Add('-1202|-1202|0');
      //
      FListaSQL.Add('-103|-0103|0');
      FListaSQL.Add('-203|-0203|0');
      FListaSQL.Add('-303|-0303|0');
      FListaSQL.Add('-403|-0403|0');
      FListaSQL.Add('-503|-0503|0');
      FListaSQL.Add('-603|-0603|0');
      FListaSQL.Add('-703|-0703|0');
      FListaSQL.Add('-803|-0803|0');
      FListaSQL.Add('-903|-0903|0');
      FListaSQL.Add('-1003|-1003|0');
      FListaSQL.Add('-1103|-1103|0');
      FListaSQL.Add('-1203|-1203|0');
      //
      FListaSQL.Add('-104|-0104|0');
      FListaSQL.Add('-204|-0204|0');
      FListaSQL.Add('-304|-0304|0');
      FListaSQL.Add('-404|-0404|0');
      FListaSQL.Add('-504|-0504|0');
      FListaSQL.Add('-604|-0604|0');
      FListaSQL.Add('-704|-0704|0');
      FListaSQL.Add('-804|-0804|0');
      FListaSQL.Add('-904|-0904|0');
      FListaSQL.Add('-1004|-1004|0');
      FListaSQL.Add('-1104|-1104|0');
      FListaSQL.Add('-1204|-1204|0');
      //
      FListaSQL.Add('-105|-0105|0');
      FListaSQL.Add('-205|-0205|0');
      FListaSQL.Add('-305|-0305|0');
      FListaSQL.Add('-405|-0405|0');
      FListaSQL.Add('-505|-0505|0');
      FListaSQL.Add('-605|-0605|0');
      FListaSQL.Add('-705|-0705|0');
      FListaSQL.Add('-805|-0805|0');
      FListaSQL.Add('-905|-0905|0');
      FListaSQL.Add('-1005|-1005|0');
      FListaSQL.Add('-1105|-1105|0');
      FListaSQL.Add('-1205|-1205|0');
      //
      FListaSQL.Add('-106|-0106|0');
      FListaSQL.Add('-206|-0206|0');
      FListaSQL.Add('-306|-0306|0');
      FListaSQL.Add('-406|-0406|0');
      FListaSQL.Add('-506|-0506|0');
      FListaSQL.Add('-606|-0606|0');
      FListaSQL.Add('-706|-0706|0');
      FListaSQL.Add('-806|-0806|0');
      FListaSQL.Add('-906|-0906|0');
      FListaSQL.Add('-1006|-1006|0');
      FListaSQL.Add('-1106|-1106|0');
      FListaSQL.Add('-1206|-1206|0');
      //
      FListaSQL.Add('-107|-0107|0');
      FListaSQL.Add('-207|-0207|0');
      FListaSQL.Add('-307|-0307|0');
      FListaSQL.Add('-407|-0407|0');
      FListaSQL.Add('-507|-0507|0');
      FListaSQL.Add('-607|-0607|0');
      FListaSQL.Add('-707|-0707|0');
      FListaSQL.Add('-807|-0807|0');
      FListaSQL.Add('-907|-0907|0');
      FListaSQL.Add('-1007|-1007|0');
      FListaSQL.Add('-1107|-1107|0');
      FListaSQL.Add('-1207|-1207|0');
      //
      FListaSQL.Add('-108|-0108|0');
      FListaSQL.Add('-208|-0208|0');
      FListaSQL.Add('-308|-0308|0');
      FListaSQL.Add('-408|-0408|0');
      FListaSQL.Add('-508|-0508|0');
      FListaSQL.Add('-608|-0608|0');
      FListaSQL.Add('-708|-0708|0');
      FListaSQL.Add('-808|-0808|0');
      FListaSQL.Add('-908|-0908|0');
      FListaSQL.Add('-1008|-1008|0');
      FListaSQL.Add('-1108|-1108|0');
      FListaSQL.Add('-1208|-1208|0');
      //
      FListaSQL.Add('-109|-0109|0');
      FListaSQL.Add('-209|-0209|0');
      FListaSQL.Add('-309|-0309|0');
      FListaSQL.Add('-409|-0409|0');
      FListaSQL.Add('-509|-0509|0');
      FListaSQL.Add('-609|-0609|0');
      FListaSQL.Add('-709|-0709|0');
      FListaSQL.Add('-809|-0809|0');
      FListaSQL.Add('-909|-0909|0');
      FListaSQL.Add('-1009|-1009|0');
      FListaSQL.Add('-1109|-1109|0');
      FListaSQL.Add('-1209|-1209|0');
      //
      FListaSQL.Add('-110|-0110|0');
      FListaSQL.Add('-210|-0210|0');
      FListaSQL.Add('-310|-0310|0');
      FListaSQL.Add('-410|-0410|0');
      FListaSQL.Add('-510|-0510|0');
      FListaSQL.Add('-610|-0610|0');
      FListaSQL.Add('-710|-0710|0');
      FListaSQL.Add('-810|-0810|0');
      FListaSQL.Add('-910|-0910|0');
      FListaSQL.Add('-1010|-1010|0');
      FListaSQL.Add('-1110|-1110|0');
      FListaSQL.Add('-1210|-1210|0');
      //
*)
    end else if Uppercase(Tabela) = Uppercase('ListaSetores') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('-9|"ETE"');
      FListaSQL.Add('0|"(N�O DEFINIDO)"');
    end else if Uppercase(Tabela) = Uppercase('MPArti') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|""');
    end else if Uppercase(Tabela) = Uppercase('MPClas') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|""');
    end else if Uppercase(Tabela) = Uppercase('MPEstagios') then
    begin
      FListaSQL.Add('Codigo|Ordem|Nome');
      FListaSQL.Add(' 0|   0|""');
      FListaSQL.Add(' 1| 100|"Verde"');
      FListaSQL.Add(' 2| 200|"Tratado"');
      FListaSQL.Add(' 3| 300|"Congelado"');
      FListaSQL.Add(' 4| 400|"Salmourado"');
      FListaSQL.Add(' 5| 500|"Salgado"');
      FListaSQL.Add(' 6| 600|"Salmourado salgado"');
      FListaSQL.Add(' 7| 700|"Caleirado"');
      FListaSQL.Add(' 8| 800|"Pr�-curtido"');
      FListaSQL.Add(' 9| 900|"Curtido"');
      FListaSQL.Add('10|1000|"Recurtido"');
      FListaSQL.Add('11|1100|"Semi-acabado"');
      FListaSQL.Add('12|1200|"Estucado"');
      FListaSQL.Add('13|1300|"Corrigido"');
      FListaSQL.Add('14|1400|"Acabado"');
    end else if Uppercase(Tabela) = Uppercase('MPGrup') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|""');
    end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
    begin
      FListaSQL.Add('Janela|Descricao'); //N�O PODE TER V�RGULA NOS TEXTOS!!!!!!
      FListaSQL.Add('"EntiMP"|"Configura��o de fornecedores de mat�ria-prima"');
      FListaSQL.Add('"ItemPedidoOK"|"Indica na produ��o que um item pedido j� foi produzido."');
      FListaSQL.Add('"MPIn"|"Entrada e controle de de mat�ria-prima"');
      FListaSQL.Add('"PortSerBal"|"Cadastro de balan�as comunic�veis"');
      FListaSQL.Add('"Receitas Rib"|"Cadastro de receitas de ribeira"');
    end else if Uppercase(Tabela) = Uppercase('VSOpeSeq') then
    begin
      FListaSQL.Add('Codigo|Nome');

      FListaSQL.Add(Geral.FF0(CO_SMIS_01000_AjusteEstoque        ) + '|"' + CO_TXT_SMIS_01000_AjusteEstoque         + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_02000_Compra               ) + '|"' + CO_TXT_SMIS_02000_Compra                + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_02250_EntradaPorRemessa    ) + '|"' + CO_TXT_SMIS_02250_EntradaPorRemessa     + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_02500_EntradaPorDevolucao  ) + '|"' + CO_TXT_SMIS_02500_EntradaPorDevolucao   + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_02750_EntradaParaRetrabalho) + '|"' + CO_TXT_SMIS_02750_EntradaParaRetrabalho + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_03000_Classificar          ) + '|"' + CO_TXT_SMIS_03000_Classificar           + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_04000_Reclassificar        ) + '|"' + CO_TXT_SMIS_04000_Reclassificar         + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_05000_BaterSal             ) + '|"' + CO_TXT_SMIS_05000_BaterSal              + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_06000_PreRemolho           ) + '|"' + CO_TXT_SMIS_06000_PreRemolho            + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_07000_PreDescarne          ) + '|"' + CO_TXT_SMIS_07000_PreDescarne           + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_08000_Caleiro              ) + '|"' + CO_TXT_SMIS_08000_Caleiro               + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_09000_Empilhar             ) + '|"' + CO_TXT_SMIS_09000_Empilhar              + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_10000_Redescarne           ) + '|"' + CO_TXT_SMIS_10000_Redescarne            + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_11000_DivisaoTripa         ) + '|"' + CO_TXT_SMIS_11000_DivisaoTripa          + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_12000_CompraTripa          ) + '|"' + CO_TXT_SMIS_12000_CompraTripa           + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_13000_Classificar          ) + '|"' + CO_TXT_SMIS_13000_Classificar           + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_14000_Reclassificar        ) + '|"' + CO_TXT_SMIS_14000_Reclassificar         + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_15000_Curtimento           ) + '|"' + CO_TXT_SMIS_15000_Curtimento            + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_16000_Empilhar             ) + '|"' + CO_TXT_SMIS_16000_Empilhar              + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_17000_Descansar            ) + '|"' + CO_TXT_SMIS_17000_Descansar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_18000_Enxugar              ) + '|"' + CO_TXT_SMIS_18000_Enxugar               + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_19000_Descansar            ) + '|"' + CO_TXT_SMIS_19000_Descansar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_20000_Medir                ) + '|"' + CO_TXT_SMIS_20000_Medir                 + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_21000_Descansar            ) + '|"' + CO_TXT_SMIS_21000_Descansar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_22000_Classificar          ) + '|"' + CO_TXT_SMIS_22000_Classificar           + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_23000_Armazenar            ) + '|"' + CO_TXT_SMIS_23000_Armazenar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_24000_VendaCurtido         ) + '|"' + CO_TXT_SMIS_24000_VendaCurtido          + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_25000_CompraCurtido        ) + '|"' + CO_TXT_SMIS_25000_CompraCurtido         + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_26000_Armazenar            ) + '|"' + CO_TXT_SMIS_26000_Armazenar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_27000_Medir                ) + '|"' + CO_TXT_SMIS_27000_Medir                 + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_28000_Remolhar             ) + '|"' + CO_TXT_SMIS_28000_Remolhar              + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_29000_Empilhar             ) + '|"' + CO_TXT_SMIS_29000_Empilhar              + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_30000_Descansar            ) + '|"' + CO_TXT_SMIS_30000_Descansar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_31000_Reclassificar        ) + '|"' + CO_TXT_SMIS_31000_Reclassificar         + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_32000_Descansar            ) + '|"' + CO_TXT_SMIS_32000_Armazenar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_33000_Medir                ) + '|"' + CO_TXT_SMIS_33000_Medir                 + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_34000_Descansar            ) + '|"' + CO_TXT_SMIS_34000_Armazenar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_35000_DividirCurtido       ) + '|"' + CO_TXT_SMIS_35000_DividirCurtido        + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_36000_Refilar              ) + '|"' + CO_TXT_SMIS_36000_Refilar               + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_37000_Rebaixar             ) + '|"' + CO_TXT_SMIS_37000_Rebaixar              + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_38000_Refilar              ) + '|"' + CO_TXT_SMIS_38000_Refilar               + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_39000_WetStrech            ) + '|"' + CO_TXT_SMIS_39000_WetStrech             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_39500_CosturarWB           ) + '|"' + CO_TXT_SMIS_39500_CosturarWB            + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_40000_Refilar              ) + '|"' + CO_TXT_SMIS_40000_Refilar               + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_40500_PesarWB              ) + '|"' + CO_TXT_SMIS_40500_PesarWB               + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_41000_Recurtir             ) + '|"' + CO_TXT_SMIS_41000_Recurtir              + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_42000_Empilhar             ) + '|"' + CO_TXT_SMIS_42000_Empilhar              + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_43000_Descansar            ) + '|"' + CO_TXT_SMIS_43000_Descansar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_44000_Estirar              ) + '|"' + CO_TXT_SMIS_44000_Estirar               + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_45000_WetStrech            ) + '|"' + CO_TXT_SMIS_45000_WetStrech             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_46000_Aereo                ) + '|"' + CO_TXT_SMIS_46000_Aereo                 + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_47000_Pasting              ) + '|"' + CO_TXT_SMIS_47000_Pasting               + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_48000_Vacuo                ) + '|"' + CO_TXT_SMIS_48000_Vacuo                 + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_49000_Toggling             ) + '|"' + CO_TXT_SMIS_49000_Toggling              + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_50000_TAIC                 ) + '|"' + CO_TXT_SMIS_50000_TAIC                  + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_51000_CompraSemiAcabado    ) + '|"' + CO_TXT_SMIS_51000_CompraSemiAcabado     + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_52000_Classificar          ) + '|"' + CO_TXT_SMIS_52000_Classificar           + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_53000_Armazenar            ) + '|"' + CO_TXT_SMIS_53000_Armazenar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_54000_Refilar              ) + '|"' + CO_TXT_SMIS_54000_Refilar               + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_54500_CosturarSemi         ) + '|"' + CO_TXT_SMIS_54500_CosturarSemi          + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_55000_Reclassificar        ) + '|"' + CO_TXT_SMIS_55000_Reclassificar         + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_56000_Reumectar            ) + '|"' + CO_TXT_SMIS_56000_Reumectar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_57000_Descansar            ) + '|"' + CO_TXT_SMIS_57000_Descansar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_58000_Reclassificar        ) + '|"' + CO_TXT_SMIS_58000_Reclassificar         + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_59000_Mollissar            ) + '|"' + CO_TXT_SMIS_59000_Mollissar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_60000_Reclassificar        ) + '|"' + CO_TXT_SMIS_60000_Reclassificar         + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_61000_Armazenar            ) + '|"' + CO_TXT_SMIS_61000_Armazenar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_62000_Refilar              ) + '|"' + CO_TXT_SMIS_62000_Refilar               + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_63000_Reclassificar        ) + '|"' + CO_TXT_SMIS_63000_Reclassificar         + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_64000_Stucco               ) + '|"' + CO_TXT_SMIS_64000_Stucco                + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_65000_Descansar            ) + '|"' + CO_TXT_SMIS_65000_Descansar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_66000_Lixar                ) + '|"' + CO_TXT_SMIS_66000_Lixar                 + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_67000_Refilar              ) + '|"' + CO_TXT_SMIS_67000_Refilar               + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_68000_Armazenar            ) + '|"' + CO_TXT_SMIS_68000_Armazenar             + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_69000_CompraPreAcabado     ) + '|"' + CO_TXT_SMIS_69000_CompraPreAcabado      + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_70000_Reclassificar        ) + '|"' + CO_TXT_SMIS_70000_Reclassificar         + '"');
      //
      FListaSQL.Add(Geral.FF0(CO_SMIS_80000_CouroAcabado         ) + '|"' + CO_TXT_SMIS_80000_CouroAcabado          + '"');
      //
      FListaSQL.Add(Geral.FF0(CO_SMIS_99000_VENDA                ) + '|"' + CO_TXT_SMIS_99000_VENDA                 + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_99100_BaixaForcada         ) + '|"' + CO_TXT_SMIS_99100_BaixaForcada          + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_99200_BaixaExtravio        ) + '|"' + CO_TXT_SMIS_99200_BaixaExtravio         + '"');
      FListaSQL.Add(Geral.FF0(CO_SMIS_99900_Reprocessar          ) + '|"' + CO_TXT_SMIS_99900_Reprocessar           + '"');

    end else
    if Uppercase(Tabela) = Uppercase('GraGruY') then
    begin
      FListaSQL.Add('Codigo|Tabela|Nome|Ordem');
      FListaSQL.Add('0|""|"(N�o atrelado)"|0');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_0512_VSSubPrd) +'|"vssubprd"|"Subproduto In Natura"|999990512');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_0683_VSPSPPro) +'|"vspsppro"|"Subproduto em Processo"|999990683');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_0853_VSPSPEnd) +'|"vspspend"|"Subproduto Processado"|999990853');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_1024_VSNatCad) +'|"vsnatcad"|"Mat�ria-prima In Natura"|1');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_1072_VSNatInC) +'|"vsnatinc"|"Mat�ria-prima em Conserva��o"|1');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_1088_VSNatCon) +'|"vsnatcon"|"Mat�ria-prima Conservada"|1');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_1195_VSNatPDA) +'|"vsnatpda"|"Mat�ria-prima PDA"|1');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_1365_VSProCal) +'|"vsprocal"|"Couro em Caleiro"|2');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_1536_VSCouCal) +'|"vscoucal"|"Couro Caleirado"|3');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_1621_VSCouDTA) +'|"vscoudta"|"Couro Caleirado DTA"|3');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_1707_VSProCur) +'|"vsprocur"|"Couro em Curtimento"|4');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_1877_VSCouCur) +'|"vscoucur"|"Couro Curtido"|5');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_2048_VSRibCad) +'|"vsribcad"|"Artigo de Ribeira"|6');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_3072_VSRibCla) +'|"vsribcla"|"Artigo de Ribeira Classificado"|7');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_4096_VSRibOpe) +'|"vsribope"|"Artigo de Ribeira Em Opera��o"|8');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_5120_VSWetEnd) +'|"vswetend"|"Artigo de Semi em Processo"|9');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_6144_VSFinCla) +'|"vsfincla"|"Artigo Acabado Classificado"|10');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_7168_VSRepMer) +'|"vsrepmer"|"Reparo de Mercadoria"|11');
    end;
    All_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    ERPSinc_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Enti_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    PerfJan_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Finance_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    IBGE_DTB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    NFe_Tb.CarregaListaSQL(Tabela, FListaSQL);
    CMPT_Tb.CarregaListaSQL(Tabela, FListaSQL);
    Grade_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    SPED_Geral_Tb.CarregaListaSQL(Tabela, FListaSQL);
    SPED_EFD_II_Tb.CarregaListaSQL(Tabela, FListaSQL);
    SPED_EFD_PC_Tb.CarregaListaSQL(Tabela, FListaSQL);
    //SINTEGRA_Tb.CarregaListaSQL(Tabela, FListaSQL);
    Mail_Tb.CarregaListaSQL(Tabela, FListaSQL);
    Imprime_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Moedas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Fiscal_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    GFat_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    GPed_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    CNAB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Anotacoes_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Empresas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //
    UnGeral_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
    Tributos_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Praz_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    UMedi_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    ConsumoGerl_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Consumo_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    VS_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    PQ_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Contab_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    DmkACBr_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    SrvTom_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    UnNFSe_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Contas') then
    begin
      (*FListaSQL.Add('-129|"Compra de mercadorias diversas"');
      FListaSQL.Add('-130|"Frete de mercadorias"');
      FListaSQL.Add('-131|"Compra de filmes"');
      FListaSQL.Add('-132|"Frete de filmes"');
      FListaSQL.Add('-133|"Inv�lido"');
      FListaSQL.Add('-134|"Venda e/ou loca��o"');*)
    end;
    All_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    ERPSinc_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Enti_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Mail_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    PerfJan_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Imprime_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Moedas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Fiscal_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    GFat_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    GPed_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    CNAB_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Anotacoes_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Empresas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Tributos_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Praz_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    ConsumoGerl_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Consumo_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    VS_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    PQ_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    DmkACBr_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    SrvTom_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnNFSe_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('ArtigosGrupos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Nome';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Balancos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Periodo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Empresa';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('BalanCli') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Periodo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'CliInt';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 3;
      FRIndices.Column_name   := 'Empresa';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CECTInn') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CECTout') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CECTOutIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('DefPecas') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Espessuras') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Fabricas') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Equipes') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('Fluxos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
    end else
    if Uppercase(TabelaBase) = Uppercase('FluxosIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
    end else
    if Uppercase(TabelaBase) = Uppercase('FluxosICt') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
    end else
    if Uppercase(TabelaBase) = Uppercase('FluxosSet') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('FluxPcpCab') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('FluxPcpIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('FluxPcpStg') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ListaSetores') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('NFeMPInn') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('NFeMPIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPIn') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPInIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPInDefeitos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPInExp') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPInPerdas') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPArti') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPGrup') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPClas') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPEstagios') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPP') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPSubProd') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPV') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPVIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPVItVS') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPVPss') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPV2') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPV2Its') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPV2Bxa') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('MPVImpOpc') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Pallets') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('PalletsIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Ponto') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('PontoIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Dia';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('PortSerBal') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('PortSerLei') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('PortSerIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    begin
      All_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      ERPSinc_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Enti_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      PerfJan_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Finance_Tabs.CarregaListaFRIndices(TabelaBase, TabelaNome, FRIndices, FLindices);
      IBGE_DTB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      NFe_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      CMPT_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Grade_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      SPED_Geral_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      SPED_EFD_II_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      SPED_EFD_PC_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      //SINTEGRA_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Mail_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnGeral_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      //
      Imprime_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Praz_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
  	  Moedas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Fiscal_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      GFat_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      GPed_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      CNAB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Anotacoes_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Empresas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Tributos_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UMedi_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      ConsumoGerl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Consumo_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      VS_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      PQ_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Contab_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      DmkACBr_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      SrvTom_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnNFSe_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.ConfiguracoesIniciais(UsaCoresRel: integer);
begin
  dmkPF.ConfigIniApp(UsaCoresRel);
  //
  (*
  if (Uppercase(AppIDtxt) = 'BLUEWET')
  or  (Uppercase(AppIDtxt) = 'BLUESKIN')
  or  (Uppercase(AppIDtxt) = 'BLUEDERM')
  or  (Uppercase(AppIDtxt) = 'BLUEDER2')
  or  (Uppercase(AppIDtxt) = 'SKINTAN') then
  begin
    VAR_CLIENTE1 := CO_CLIENTE + ' Produtos';
    VAR_CLIENTE2 := CO_CLIENTE + ' Servi�os';
    VAR_FORNECE1 := CO_FORNECE + ' Mat�ria-prima';
    VAR_FORNECE2 := CO_FORNECE + ' Uso / Consumo';
    VAR_FORNECE3 := CO_TRANSPORTADOR;
    VAR_FORNECE4 := CO_FORNECE + ' Outros';
    VAR_FORNECE5 := 'Funcion�rio';
    VAR_FORNECE6 := 'Vendedor';
    VAR_FORNECE7 := CO_FORNECE + ' M�o de obra';
    VAR_CLIENTEC := '1';
    VAR_CLIENTEI := '1';
    VAR_FORNECEF := '1';
    VAR_FORNECEV := '3';
    VAR_FORNECET := '3';
    VAR_CXEDIT_ICM := True;
  end else
  *)
  if not Entities.ConfiguracoesIniciaisEntidades(CO_DMKID_APP) then
    Geral.MB_Aviso('Database para configura��es de "CheckBox" n�o definidos!');
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
  var Temcontrole: TTemControle): Boolean;
begin
  TemControle := TemControle + cTemControleNao;
  try
{   s� teste
    if Uppercase(Tabela) = Uppercase('_Produtos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodPesq';
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fabricante';
      FRCampos.Tipo       := 'int(4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Grupo';
      FRCampos.Tipo       := 'int(4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Medida';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrUni';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtEstoque';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtPeas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ICMS';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ICMSLC';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClasFisc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodTrib';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstoqMin';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Origem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Local';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataEnt';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Coeficient';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'D04UKEY';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
}
    if Uppercase(Tabela) = Uppercase('ArtigosGrupos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := 'TESTE';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
     New(FRCampos);
      FRCampos.Field      := 'Fluxo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceitaR';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceitaA';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Grandeza';
      FRCampos.Tipo       := 'tinyint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);

      //
      New(FRCampos); // Nota fiscal
      FRCampos.Field      := 'CF';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // Nota fiscal
      FRCampos.Field      := 'ST';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // Nota fiscal
      FRCampos.Field      := 'ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // Nota fiscal
      FRCampos.Field      := 'IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // Nota fiscal
      FRCampos.Field      := 'Preco';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // Nota fiscal
      FRCampos.Field      := 'CodImp';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // Nota fiscal
      FRCampos.Field      := 'PrecoICM';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Balancos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqQ';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqV';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Confirmado';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'V';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GrupoBal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('BalanCli') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GerouDif';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('BalancosIts') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Produto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqQ';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqV';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnteQ';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnteV';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqV_G';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqQ_G';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CFOP2003') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Complementacao';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Consts') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Compras';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'Compras';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Caleiro';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'Caleiro';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Curtimento';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'Curtimento';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Recurtimento';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'Recurtimento';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Acabamento';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'Acabamento';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CaldeiraETE';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'Caldeira/ETE';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'MPSubProd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fabricas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeMPInn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeMPIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPTInn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPTOut';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPTOutIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CanAltBalA';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SetorCal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SetorCur';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CECTInn';  // Entrada
      FRCampos.Tipo       := 'int(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CECTOut'; // Sa�da
      FRCampos.Tipo       := 'int(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CECTOutIts'; // itens de Sa�da
      FRCampos.Tipo       := 'int(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VendaCouro';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPVImpOpc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PortSerBal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PortSerLei';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PortSerIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CtaPQCompr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CtaPQFrete';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CtaMPCompr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CtaMPFrete';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CtaCVVenda';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CtaCVFrete';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImpRecRib';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImpRecAcabto';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Percula';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '70';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PQNegBaixa';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PQNegEntra';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PathLogoDupl';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PathLogoRece';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArtigosGrupos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fluxos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FluxosIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Operacoes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPV';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPV2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPP';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPVIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPV2Its';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPV2Bxa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPPIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pallets';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PalletsIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPClas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPEstagios';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPGrup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPArti';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPIn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPInIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPInDefeitos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPInExp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPInPerdas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DefPecas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BalancosIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DOSGrades';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DOSDotLinTop';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DOSDotLinBot';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Equipes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Espessuras';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ListaSetores';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RolPerdas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      New(FRCampos);
      FRCampos.Field      := 'PQ_StqCenCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MP_StqCenCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      {
      New(FRCampos);
      FRCampos.Field      := 'PQ_Amostra';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PQ_PQ';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PQ_Material';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PQ_Outros';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      }
      //
      New(FRCampos);
      FRCampos.Field      := 'WBNivGer';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OSImpInfWB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsaBRLMedM2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsaM2Medio';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsaEmitGru';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VerImprRecRib';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CanAltBalVS';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfoMulFrnImpVS';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdBoxClas';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSWarSemNF';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSWarNoFrn';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSInsPalManu';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'VSImpRandStr';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NObrigNFeVS';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'DBClarecoCDR';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := 'clarecocdr_ NomeEmp ';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2DiaAcabPCP';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CtaComisCMP'; // Conta comissao compra Mat�ria-Prima
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_CFOP_DE';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '1101';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_CFOP_FE';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '2101';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_ICMS_CST';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_IPI_CST';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '03';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_PIS_CST';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '70';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_COFINS_CST';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '70';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_ICMS_ALIQ';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_ICMS_ALIQ_ST';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_IPI_ALIQ';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_PIS_ALIQ';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_COFINS_ALIQ';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
      New(FRCampos);
      FRCampos.Field      := 'RemTrib_MP_CFOP_DE';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '1101';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RemTrib_MP_CFOP_FE';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '2101';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RemTrib_MP_ICMS_CST';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RemTrib_MP_IPI_CST';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '03';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RemTrib_MP_PIS_CST';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '70';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RemTrib_MP_COFINS_CST';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '70';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RemTrib_MP_ICMS_ALIQ';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RemTrib_MP_ICMS_ALIQ_ST';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RemTrib_MP_IPI_ALIQ';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RemTrib_MP_PIS_ALIQ';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RemTrib_MP_COFINS_ALIQ';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_TES_ICMS';
      FRCampos.Tipo       := 'tinyint(1)'; // 0=Nao, 1=Sim pela NFe 2=Sim Pela Regra Fiscal
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_TES_IPI';
      FRCampos.Tipo       := 'tinyint(1)'; // 0=Nao, 1=Sim pela NFe 2=Sim Pela Regra Fiscal
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_TES_PIS';
      FRCampos.Tipo       := 'tinyint(1)'; // 0=Nao, 1=Sim pela NFe 2=Sim Pela Regra Fiscal
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CreTrib_MP_TES_COFINS';
      FRCampos.Tipo       := 'tinyint(1)'; // 0=Nao, 1=Sim pela NFe 2=Sim Pela Regra Fiscal
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StqCenCadEstqPall';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GruUsrEdtRecei';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '0';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PQBCiManual'; // Edita estoque anterior manualmente (Vitapelli?)
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '0';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CECTInn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataE';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFInn';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRef';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InnQtdkg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InnQtdPc';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InnQtdM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InnQtdVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoQtdkg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoQtdPc';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoQtdM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoQtdVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoNF';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'refNFe';
      FRCampos.Tipo       := 'varchar(44)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'modNF';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CECTOut') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataS';
      FRCampos.Tipo       := 'Date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFOut';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TotQtdkg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TotQtdPc';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TotQtdM2';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TotQtdVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Abertura';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Encerrou';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvValTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondicaoPg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Carteira';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FretePor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transporta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FisRegCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataFat';
      FRCampos.Tipo       := 'Date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CECTOutIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CECTInn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OutQtdkg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OutQtdPc';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OutQtdM2';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OutQtdVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Benefikg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BenefiPc';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BenefiM2';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvCodigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvValUni';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComoCobra'; // 0 = Qtd entrada // 1 = Qtd sa�da 
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvValTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SitDevolu';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('DefPecas') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'ND';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Grandeza';
      FRCampos.Tipo       := 'tinyint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Equipes') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntraA1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SaidaA1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntraA2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SaidaA2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntraB1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SaidaB1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntraB2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SaidaB2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntraC1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SaidaC1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntraC2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SaidaC2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Espessuras') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Linhas';
      FRCampos.Tipo       := 'varchar(7)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EMCM';
      FRCampos.Tipo       := 'double(10,4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('Fabricas') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Marca';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('NFeMPInn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornece';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transporta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PLE';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPValor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Abertura';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Encerrou';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RegrFiscal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FretePor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondicaoPG';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CartEmiss';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataFat';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieDesfe';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFDesfeita';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('NFeMPIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PLE';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPValor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GerBxaEstq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Fluxos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('FluxosIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Operacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Acao1';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Acao2';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Acao3';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Acao4';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('FluxosICt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Acao1';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('FluxosSet') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Setor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('FluxPcpCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('FluxPcpIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DiaSeq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IniStg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FimStg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('FluxPcpStg') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ListaSetores') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'm2Dia';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TpReceita';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TpSetor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CourosDia';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KgDia';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OperPosProcDescr';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '???????';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPIn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClienteI';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Procedencia';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transportadora';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Marca';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasNF';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'P2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FimM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FimP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PNF';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      {
      New(FRCampos);
      FRCampos.Field      := 'PNF_Fat';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      }
      //
      New(FRCampos);
      FRCampos.Field      := 'PLE';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PDA';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Recorte_PDA';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PTA';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Recorte_PTA';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Raspa_PTA';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (*New(FRCampos);
      FRCampos.Field      := 'Furos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Bernes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Raias';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);*)
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipificacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AparasCabelo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SeboPreDescarne';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoInfo';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FreteInfo';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoPQ';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Frete';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AbateTipo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Lote';
      FRCampos.Tipo       := 'varchar(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasOut';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasSal';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Caminhoes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QuemAssina';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Corretor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComisPer';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComisVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescAdiant';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondPagto';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondComis';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LocalEntrg';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Animal';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPValor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPFrete';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QuebrVReal';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QuebrVCobr';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPDesQV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPPagar';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PcBxa';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'kgBxa';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2Bxa';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'P2Bxa';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasCal';  // Pecas caleiro
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasCur';  // Pecas curtimento
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasExp';  // Pe�as Medidora
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2Exp';  // m2 Medidora
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'P2Exp';  // P2 Medidora
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoExp';  // kg Medidora
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasNeg'; // Pe�as perdidas, estragadas etc.
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoCal';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoCur';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusPQCal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusPQCur';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FuloesCal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FuloesCur';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MinDtaCal';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MinDtaCur';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MinDtaEnx';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MaxDtaCal';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MaxDtaCur';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MaxDtaEnx';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ForcaEncer';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Encerrado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PS_ValUni';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PS_QtdTot';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PS_ValTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // Evitar balancete durante a classifica��o
      FRCampos.Field      := 'EmClassif';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estq_Pecas';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estq_M2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estq_P2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estq_Peso';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StqMovIts';//StqMovItsA ou StqMovItsB
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPInIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornece';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Caminhao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Emissao';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vencto';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmisFrete';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VctoFrete';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conheci';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasNF';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PNF';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PNF_Fat';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PLE';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PDA';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Recorte_PDA';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PTA';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Recorte_PTA';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Raspa_PTA';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (*
      New(FRCampos);
      FRCampos.Field      := 'AparasCabelo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SeboPreDescarne';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      *)
      New(FRCampos);
      FRCampos.Field      := 'CustoInfo';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FreteInfo';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TotalInfo';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPDevol';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLDevol';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGIDevol';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMIDevol';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FinalInfo';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Compra mat�ria-prima
      New(FRCampos);
      FRCampos.Field      := 'CMPValor';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPFrete';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Complemento compra mat�ria-prima
      New(FRCampos);
      FRCampos.Field      := 'CPLValor';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLFrete';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // �gio compra mat�ria-prima
      New(FRCampos);
      FRCampos.Field      := 'AGIValor';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGIFrete';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Comiss�o compra mat�ria-prima
      New(FRCampos);
      FRCampos.Field      := 'CMIValor';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMIFrete';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // C�mbio
      New(FRCampos);
      FRCampos.Field      := 'Dolar';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Euro';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Indexador';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPUnida';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPPreco';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPMoeda';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPCotac';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPReais';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      {
      New(FRCampos);
      FRCampos.Field      := 'CMPFinal';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      }
      New(FRCampos);
      FRCampos.Field      := 'PS_ValUni';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PS_QtdTot';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PS_ValTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NF_Inn_Fut';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NF_Inn_Its';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NF_modelo';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NF_Serie';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1101';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmitNFAvul';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StqMovIts';//StqMovItsA ou StqMovItsB
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EFD_INN_AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EFD_INN_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EFD_INN_LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPInDefeitos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'MPInCtrl';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Defeitoloc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Defeitotip';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Defeitos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPInExp') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'MPInCtrl';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'P2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Classe';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      {
      New(FRCampos);
      FRCampos.Field      := 'DefPeca';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      }
    end else if Uppercase(Tabela) = Uppercase('MPInPerdas') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'MPInCtrl';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'P2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Setor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPArti') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPClas') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPEstagios') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPGrup') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPP') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vendedor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataF';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      {New(FRCampos);
      FRCampos.Field      := 'ComissV_Per';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComissV_Val';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);}
      //
      {New(FRCampos);
      FRCampos.Field      := 'DescoExtra';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);}
      //
      {New(FRCampos);
      FRCampos.Field      := 'Volumes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);}
      //
      {New(FRCampos);
      FRCampos.Field      := 'Obs';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);}
      //
      New(FRCampos);
      FRCampos.Field      := 'Obz';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondicaoPg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PedidCli';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPSubProd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGX_Ori';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGX_Ger';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GrndzaBase';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fator';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Arredonda';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '10.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPV') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vendedor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataF';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComissV_Per';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComissV_Val';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescoExtra';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Volumes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Obs';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Obz';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondicaoPg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPVIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo'; // MPV
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pedido'; // MPP
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MP';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Preco';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Desco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entrega';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaCrust';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pronto';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fluxo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Classe';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesTxt';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CorTxt';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2Pedido';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoPed';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorPed';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Unidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Complementacao';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoProd';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoPQ';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRecu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRefu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiAcab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtMPs';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoWB';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSArtCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSArtGGX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FluxPcpCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPVItVS') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPVPss') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'MPVIts'; // MPVIts.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DiaSeq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';  // Indice primario!
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataSeq';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IniStg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FimStg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrReal';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPV2') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vendedor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataF';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComissV_Per';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComissV_Val';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ICMS_Val';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IPI_Val';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescoExtra';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Volumes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Obs';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Obz';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Seguro';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Desp_Acess';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conhecim';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pago';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FretePg';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FreteNF';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CobraFrete';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CobraSegur';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Placa';
      FRCampos.Tipo       := 'varchar(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UFPlaca';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Especie';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Marca';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Numero';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'kgBruto';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'kgLiqui';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPV2Its') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo'; // MPV
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pedido'; // MPP
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MP';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estagio';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Peso';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'P2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Grandeza';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Unidade';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Preco';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Total';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ICMS_Per';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ICMS_Val';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IPI_Per';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IPI_Val';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPV2Bxa') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo'; // MPV.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle'; // MPVIts.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estagio'; // MPVIts.Estagio
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Marca_ID'; // Controle da marca
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Peso';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'P2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('MPVImpOpc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Usuario';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Pallets') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pallet';
      FRCampos.Tipo       := 'varchar(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PalletsIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPIn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPArti';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPGrup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPClass';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2';
      FRCampos.Tipo       := 'Double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'P2';
      FRCampos.Tipo       := 'Double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Ponto') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (*New(FRCampos);
      FRCampos.Field      := 'Descansa';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entra1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Saida1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entra2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Saida2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MinExtra';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '30';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);*)
      //
      New(FRCampos);
      FRCampos.Field      := 'Equipe';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PontoIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dia';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';  // 1-Presen�a 2-Atestado 3-Falta
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entra1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Saida1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entra2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Saida2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entra3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Saida3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entra4';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Saida4';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Minutos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PortSerBal') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CommPort';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BaudRate';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '5';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StopBits'; // 1 - 1.5 - 2
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataBits'; // 5, 6, 7, 8
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parity';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RTS_input';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DTR_input';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CTS_output';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DSR_output';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XonXof_input';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XonXof_output';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PortSerLei') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PortSerIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Seq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Faixa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fulao';
      FRCampos.Tipo       := 'varchar(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Couro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Peso';
      FRCampos.Tipo       := 'double(15,1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Mensagem';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('RolPerdas') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    end else
    begin
      All_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      ERPSinc_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Enti_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      PerfJan_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Finance_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      IBGE_DTB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      NFe_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      CMPT_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Grade_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      SPED_Geral_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, temControle);
      SPED_EFD_II_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, temControle);
      SPED_EFD_PC_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, temControle);
      //SINTEGRA_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Mail_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //
      UnGeral_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Imprime_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Moedas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Fiscal_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      GFat_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      GPed_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      CNAB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Anotacoes_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Empresas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Tributos_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Praz_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UMedi_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      ConsumoGerl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Consumo_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      VS_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      PQ_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Contab_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      DmkACBr_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      SrvTom_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnNFSe_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
    All_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    ERPSinc_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Enti_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    PerfJan_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Finance_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Grade_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    CMPT_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    NFe_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Mail_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    UnGeral_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Imprime_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Moedas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Fiscal_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    GFat_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    GPed_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    CNAB_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Anotacoes_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Empresas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Tributos_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Praz_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UMedi_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    ConsumoGerl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Consumo_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    VS_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    PQ_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Contab_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    DmkACBr_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    SrvTom_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnNFSe_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.ExcluiTab: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiReg: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiIdx: Boolean;
begin
  Result := True;
end;

function TMyListas.CriaListaUserSets(FUserSets: TStringList): Boolean;
begin
  Result := True;
(* Desqabilitado sem ver se pode desabilitar!
  try
    FUserSets.Add('Edi��o de Itens de Mercadoria;C�digos Fiscais');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Representante');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Vendedor');
    FUserSets.Add('Edi��o de Itens de Mercadoria;IPI');
    FUserSets.Add('Edi��o de Itens de Mercadoria;ICMS');
    //
    FUserSets.Add('Impress�o de receita sem baixa;Artigo e Cor');
    FUserSets.Add('Impress�o de receita sem baixa;Espessura e �rea');
    FUserSets.Add('Impress�o de receita sem baixa;Observa��es e configura��es');
    //
    FUserSets.Add('Impress�o de receita em edi��o;Artigo e Cor');
    FUserSets.Add('Impress�o de receita em edi��o;Espessura e �rea');
    FUserSets.Add('Impress�o de receita em edi��o;Observa��es e configura��es');
    //
    FUserSets.Add('Impress�o de receita com baixa;Artigo e Cor');
    FUserSets.Add('Impress�o de receita com baixa;Espessura e �rea');
    FUserSets.Add('Impress�o de receita com baixa;Observa��es e configura��es');
    FUserSets.Add('Impress�o de receita com baixa;Adi��o de lotes');
    //
    FUserSets.Add('Impress�o de receita com lotes;Artigo e Cor');
    FUserSets.Add('Impress�o de receita com lotes;Espessura e �rea');
    FUserSets.Add('Impress�o de receita com lotes;Observa��es e configura��es');
    FUserSets.Add('Impress�o de receita com lotes;Adi��o de lotes');
    //
  except
    raise;
    Result := False;
  end;
*)
end;

function TMyListas.CriaListaImpDOS(FImpDOS: TStringList): Boolean;
begin
  Result := True;
(* Desqabilitado sem ver se pode desabilitar!
  try
    FImpDOS.Add('1001;01;NF (Topo)');
    FImpDOS.Add('1002;01;Sa�da [X]');
    FImpDOS.Add('1003;01;Entrada [X]');
    FImpDOS.Add('1004;01;Data emiss�o');
    FImpDOS.Add('1005;01;Data entra/sai');
    FImpDOS.Add('1006;01;C�digo CFOP');
    FImpDOS.Add('1007;01;Descri��o CFOP');
    FImpDOS.Add('1008;01;Base c�lculo ICMS');
    FImpDOS.Add('1009;01;Valor ICMS');
    FImpDOS.Add('1010;01;Base c�lc. ICMS subst.');
    FImpDOS.Add('1011;01;Valor ICMS subst.');
    FImpDOS.Add('1012;01;Valor frete');
    FImpDOS.Add('1013;01;Valor seguro');
    FImpDOS.Add('1014;01;Outras desp. aces.');
    FImpDOS.Add('1015;01;Valor total IPI');
    FImpDOS.Add('1016;01;Valor total produtos');
    FImpDOS.Add('1017;01;Valor total servicos');
    FImpDOS.Add('1018;01;Valor total nota');
    FImpDOS.Add('1019;01;Placa ve�culo');
    FImpDOS.Add('1020;01;UF placa ve�culo');
    FImpDOS.Add('1021;01;Vol. Quantidade');
    FImpDOS.Add('1022;01;Vol. Esp�cie');
    FImpDOS.Add('1023;01;Vol. Marca');
    FImpDOS.Add('1024;01;Vol. N�mero');
    FImpDOS.Add('1025;01;Vol. kg bruto');
    FImpDOS.Add('1026;01;Vol. kg l�quido');
    FImpDOS.Add('1027;01;Dados adicionais');
    FImpDOS.Add('1028;01;Frete por conta de ...');
    FImpDOS.Add('1029;01;Desconto especial');
    FImpDOS.Add('1030;01;NF (rodap�)');
    //
    FImpDOS.Add('2001;02;Nome ou Raz�o Social');
    FImpDOS.Add('2002;02;CNPJ ou CPF');
    FImpDOS.Add('2003;02;Endere�o');
    FImpDOS.Add('2004;02;Bairro');
    FImpDOS.Add('2005;02;CEP');
    FImpDOS.Add('2006;02;Cidade');
    FImpDOS.Add('2007;02;Telefone');
    FImpDOS.Add('2008;02;UF');
    FImpDOS.Add('2009;02;I.E. ou RG');
    FImpDOS.Add('2010;02;I.E.S.T.');
    //
    FImpDOS.Add('6001;06;Nome ou Raz�o Social');
    FImpDOS.Add('6002;06;CNPJ ou CPF');
    FImpDOS.Add('6003;06;Endere�o');
    FImpDOS.Add('6004;06;Bairro');
    FImpDOS.Add('6005;06;CEP');
    FImpDOS.Add('6006;06;Cidade');
    FImpDOS.Add('6007;06;Telefone');
    FImpDOS.Add('6008;06;UF');
    FImpDOS.Add('6009;06;I.E. ou RG');
    FImpDOS.Add('6010;06;I.E.S.T.');
    //
    FImpDOS.Add('3001;03;Descri��o');
    FImpDOS.Add('3002;03;Classifica��o Fiscal');
    FImpDOS.Add('3003;03;Situa��o Tribut�ria');
    FImpDOS.Add('3004;03;Unidade');
    FImpDOS.Add('3005;03;Quantidade');
    FImpDOS.Add('3006;03;Valor unit�rio');
    FImpDOS.Add('3007;03;Valor total');
    FImpDOS.Add('3008;03;Aliquota ICMS');
    FImpDOS.Add('3009;03;Aliquota IPI');
    FImpDOS.Add('3010;03;Valor IPI');
    FImpDOS.Add('3011;03;CFOP');
    FImpDOS.Add('3012;03;Refer�ncia');
    //
    FImpDOS.Add('4001;04;Descri��o');
    FImpDOS.Add('4002;04;Classifica��o Fiscal');
    FImpDOS.Add('4003;04;Situa��o Tribut�ria');
    FImpDOS.Add('4004;04;Unidade');
    FImpDOS.Add('4005;04;Quantidade');
    FImpDOS.Add('4006;04;Valor unit�rio');
    FImpDOS.Add('4007;04;Valor total');
    FImpDOS.Add('4008;04;Aliquota ICMS');
    FImpDOS.Add('4009;04;Aliquota IPI');
    FImpDOS.Add('4010;04;Valor IPI');
    FImpDOS.Add('4011;04;CFOP');
    FImpDOS.Add('4012;04;Refer�ncia');
    //
    FImpDOS.Add('5001;05;Parcela');
    FImpDOS.Add('5002;05;Valor');
    FImpDOS.Add('5003;05;Vencimento');
    //
  except
    raise;
    Result := False;
  end;
*)
end;

procedure TMyListas.VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  //
end;

procedure TMyListas.VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

procedure TMyListas.ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
  DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

// XXX-XXXXX-000
function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // ART-CADAS-001 :: Artigos
  New(FRJanelas);
  FRJanelas.ID        := 'ART-CADAS-001';
  FRJanelas.Nome      := 'FmArtigosGrupos';
  FRJanelas.Descricao := 'Artigos';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-BDERM-001 :: Pallets
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-BDERM-001';
  FRJanelas.Nome      := 'FmPallets';
  FRJanelas.Descricao := 'Pallets';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-BDERM-002 :: Tamanhos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-BDERM-002';
  FRJanelas.Nome      := 'FmMPGrup';
  FRJanelas.Descricao := 'Tamanhos';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-BDERM-003 :: Espessuras
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-BDERM-003';
  FRJanelas.Nome      := 'FmEspessuras';
  FRJanelas.Descricao := 'Espessuras';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-BDERM-005 :: Cadastro de F�bricas
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-BDERM-005';
  FRJanelas.Nome      := 'FmFabricas';
  FRJanelas.Descricao := 'Cadastro de F�bricas';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-BDERM-006 :: Escolha de Lote
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-BDERM-006';
  FRJanelas.Nome      := 'FmPalletsOrigem';
  FRJanelas.Descricao := 'Pallets de Origem';
  FLJanelas.Add(FRJanelas);
(*
  //
  // COU-PARTE-001 :: Cadastro de Parti��es de Couro
  New(FRJanelas);
  FRJanelas.ID        := 'COU-PARTE-001';
  FRJanelas.Nome      := 'FmCouNiv1';
  FRJanelas.Descricao := 'Cadastro de Parti��es de Couro';
  FLJanelas.Add(FRJanelas);
  //
*)
  // COU-CLASS-001 :: Cadastro de Classes
  New(FRJanelas);
  FRJanelas.ID        := 'COU-CLASS-001';
  FRJanelas.Nome      := 'FmMPClas';
  FRJanelas.Descricao := 'Cadastro de Classes';
  FLJanelas.Add(FRJanelas);
  //
  // COU-CLASS-002 :: Classifica��o Multipla
  New(FRJanelas);
  FRJanelas.ID        := 'COU-CLASS-002';
  FRJanelas.Nome      := 'FmMPClasMul';
  FRJanelas.Descricao := 'Classifica��o Multipla';
  FLJanelas.Add(FRJanelas);
  //
  // COU-CLASS-003 :: Reclassifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'COU-CLASS-003';
  FRJanelas.Nome      := 'FmMPClasReclas';
  FRJanelas.Descricao := 'Reclassifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // COU-CLASS-004 :: Gerenciamento de Classifica��o M�ltipla
  New(FRJanelas);
  FRJanelas.ID        := 'COU-CLASS-004';
  FRJanelas.Nome      := 'FmMPClasMulGer';
  FRJanelas.Descricao := 'Gerenciamento de Classifica��o M�ltipla';
  FLJanelas.Add(FRJanelas);
  //
  // COU-CLASS-005 :: Gerenciamento de Reclassifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'COU-CLASS-005';
  FRJanelas.Nome      := 'FmMPReclasGer';
  FRJanelas.Descricao := 'Gerenciamento de Reclassifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // COU-CLASS-006 :: Gera��o M�ltipla de Subprodutos
  New(FRJanelas);
  FRJanelas.ID        := 'COU-CLASS-006';
  FRJanelas.Nome      := 'FmMPClasMulSub';
  FRJanelas.Descricao := 'Gera��o M�ltipla de Subprodutos';
  FLJanelas.Add(FRJanelas);
  //
  // COU-MP_IN-001 :: Gerenciamento de Lotes de Couro
  New(FRJanelas);
  FRJanelas.ID        := 'COU-MP_IN-001';
  FRJanelas.Nome      := 'FmMPIn';
  FRJanelas.Descricao := 'Gerenciamento de Lotes de Couro';
  FLJanelas.Add(FRJanelas);
  //
  // COU-MP_IN-002 :: Fornecedores de Mat�ria-prima
  New(FRJanelas);
  FRJanelas.ID        := 'COU-MP_IN-002';
  FRJanelas.Nome      := 'FmEntiMP';
  FRJanelas.Descricao := 'Fornecedores de Mat�ria-prima';
  FLJanelas.Add(FRJanelas);
  //
  // COU-MP_IN-003 :: Entrada de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'COU-MP_IN-003';
  FRJanelas.Nome      := 'FmMPInAdd';
  FRJanelas.Descricao := 'Entrada de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // COU-MP_IN-004 :: Avalia��o de Mat�ria-prima
  New(FRJanelas);
  FRJanelas.ID        := 'COU-MP_IN-004';
  FRJanelas.Nome      := 'FmMPInDefeitos';
  FRJanelas.Descricao := 'Avalia��o de Mat�ria-prima';
  FLJanelas.Add(FRJanelas);
  //
  // COU-MP_IN-005 :: Quem Assina?
  New(FRJanelas);
  FRJanelas.ID        := 'COU-MP_IN-005';
  FRJanelas.Nome      := 'FmMPInAssina';
  FRJanelas.Descricao := 'Quem Assina?';
  FLJanelas.Add(FRJanelas);
  //
  // COU-MP_IN-006 :: Rateio de Caminh�o
  New(FRJanelas);
  FRJanelas.ID        := 'COU-MP_IN-006';
  FRJanelas.Nome      := 'FmMPInRat';
  FRJanelas.Descricao := 'Rateio de Caminh�o';
  FLJanelas.Add(FRJanelas);
  //
  // COU-MP_IN-007 :: Itens de Mat�ria-prima
  New(FRJanelas);
  FRJanelas.ID        := 'COU-MP_IN-007';
  FRJanelas.Nome      := 'FmMPInIts1';
  FRJanelas.Descricao := 'Itens de Mat�ria-prima';
  FLJanelas.Add(FRJanelas);
  //
  // COU-MP_IN-008 :: Perda de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'COU-MP_IN-00';
  FRJanelas.Nome      := 'FmMPInExp';
  FRJanelas.Descricao := 'Perda de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // COU-MP_IN-009 :: Palletiza��o
  New(FRJanelas);
  FRJanelas.ID        := 'COU-MP_IN-009';
  FRJanelas.Nome      := 'FmMPInExp';
  FRJanelas.Descricao := 'Palletiza��o';
  FLJanelas.Add(FRJanelas);
  //
  // COU-MP_IN-012 :: Lista de Perda de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'COU-MP_IN-012';
  FRJanelas.Nome      := 'FmRolPerdas';
  FRJanelas.Descricao := 'Lista de Perda de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // DEF-PECAS-001 :: Unidades de Grandeza
  New(FRJanelas);
  FRJanelas.ID        := 'DEF-PECAS-001';
  FRJanelas.Nome      := 'FmDefPecas';
  FRJanelas.Descricao := 'Cadastro de Grandeza';
  FLJanelas.Add(FRJanelas);
  //
  // FER-OPCAO-002 :: Op��es Espec�ficas
  New(FRJanelas);
  FRJanelas.ID        := 'FER-OPCAO-002';
  FRJanelas.Nome      := 'FmOpcoeBlueDerm';
  FRJanelas.Descricao := 'Op��es Espec�ficas do Aplicativo';
  FLJanelas.Add(FRJanelas);
  //
  // FLU-PRODU-001 :: Cadastro de Fluxos de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'FLU-PRODU-001';
  FRJanelas.Nome      := 'FmFluxosCab';
  FRJanelas.Descricao := 'Cadastro de Fluxos de Produ��o';
  FLJanelas.Add(FRJanelas);
  //
  // FLU-PRODU-002 :: Opera��o de Fluxo de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'FLU-PRODU-002';
  FRJanelas.Nome      := 'FmFluxosIts';
  FRJanelas.Descricao := 'Opera��o de Fluxo de Produ��o';
  FLJanelas.Add(FRJanelas);
  //
  // FLU-PRODU-003 :: Setor de Passagem de Fluxo de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'FLU-PRODU-003';
  FRJanelas.Nome      := 'FmFluxosSet';
  FRJanelas.Descricao := 'Setor de Passagem de Fluxo de Produ��o';
  FLJanelas.Add(FRJanelas);
  //
  // FLU-PRODU-004 :: Opera��o de Fluxo de Produ��o - Edi��o m�ltipla
  New(FRJanelas);
  FRJanelas.ID        := 'FLU-PRODU-004';
  FRJanelas.Nome      := 'FmFluxosItsMul';
  FRJanelas.Descricao := 'Opera��o de Fluxo de Produ��o - Edi��o m�ltipla';
  FLJanelas.Add(FRJanelas);
  //
  // FLU-PRODU-004 :: Opera��o de Fluxo de Produ��o - Controle
  New(FRJanelas);
  FRJanelas.ID        := 'FLU-PRODU-005';
  FRJanelas.Nome      := 'FmFluxosICt';
  FRJanelas.Descricao := 'Opera��o de Fluxo de Produ��o - Controle';
  FLJanelas.Add(FRJanelas);
  //

{
  // FLU-OPERA-001 :: Opera��es de Fluxos
  New(FRJanelas);
  FRJanelas.ID        := 'FLU-OPERA-001';
  FRJanelas.Nome      := 'FmOperacoes';
  FRJanelas.Descricao := 'Opera��es de Fluxos';
  FLJanelas.Add(FRJanelas);
  //
}
  //
  ///
  //////////////////////////////////////////////////////////////////////////////
  ///
  //
  // PCP-FLUXO-001 :: Cadastro de Fluxos de PCP
  New(FRJanelas);
  FRJanelas.ID        := 'PCP-FLUXO-001';
  FRJanelas.Nome      := 'FmFluxPcpCab';
  FRJanelas.Descricao := 'Cadastro de Fluxos de PCP';
  FLJanelas.Add(FRJanelas);
  //
  // PCP-FLUXO-002 :: Item de Fluxo de PCP
  New(FRJanelas);
  FRJanelas.ID        := 'PCP-FLUXO-002';
  FRJanelas.Nome      := 'FmFluxPcpIts';
  FRJanelas.Descricao := 'Item de Fluxo de PCP';
  FLJanelas.Add(FRJanelas);
  //
  // PCP-FLUXO-003 :: Est�gios de Fluxos de PCP
  New(FRJanelas);
  FRJanelas.ID        := 'PCP-FLUXO-003';
  FRJanelas.Nome      := 'FmFluxPcpStg';
  FRJanelas.Descricao := 'Est�gios de Fluxos de PCP';
  FLJanelas.Add(FRJanelas);
  //
  // PCP-FLUXO-004 :: Passos de Fluxos de PCP Realizados
  New(FRJanelas);
  FRJanelas.ID        := 'PCP-FLUXO-004';
  FRJanelas.Nome      := 'FmMPVPss';
  FRJanelas.Descricao := 'Passos de Fluxos de PCP Realizados';
  FLJanelas.Add(FRJanelas);
  //
  // PCP-FLUXO-005 :: Impress�es de PCP
  New(FRJanelas);
  FRJanelas.ID        := 'PCP-FLUXO-005';
  FRJanelas.Nome      := 'FmFluxPcpImp';
  FRJanelas.Descricao := 'Impress�es de PCP ';
  FLJanelas.Add(FRJanelas);
  //
  ///
  //////////////////////////////////////////////////////////////////////////////
  ///
  //
  // FUN-CADAS-001 :: Equipes
  New(FRJanelas);
  FRJanelas.ID        := 'FUN-CADAS-001';
  FRJanelas.Nome      := 'FmEquipes';
  FRJanelas.Descricao := 'Equipes';
  FLJanelas.Add(FRJanelas);
  //
  // FUN-CADAS-002 :: Funcion�rios
  New(FRJanelas);
  FRJanelas.ID        := 'FUN-CADAS-002';
  FRJanelas.Nome      := 'FmPontoF';
  FRJanelas.Descricao := 'Funcion�rios';
  FLJanelas.Add(FRJanelas);
  //
  // IMP-MPINN-001 :: Recebimento de Mat�ria-Prima
  New(FRJanelas);
  FRJanelas.ID        := 'IMP-MPINN-001';
  FRJanelas.Nome      := 'FmMPRecebImp';
  FRJanelas.Descricao := 'Recebimento de Mat�ria-Prima';
  FLJanelas.Add(FRJanelas);
  //
{  Desativado em 2014-10-17 - Alguem usa?
  // NFe-MPINN-001 :: Agrupamento de Entrada de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'NFe-MPINN-001';
  FRJanelas.Nome      := 'FmNFeMPInn';
  FRJanelas.Descricao := 'Agrupamento de Entrada de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // NFe-MPINN-002 :: Item de Entrada de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'NFe-MPINN-002';
  FRJanelas.Nome      := 'FmNFeMPIts';
  FRJanelas.Descricao := 'Item de Entrada de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // NFe-MPINN-003 :: Adi��o de Entrada de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'NFe-MPINN-003';
  FRJanelas.Nome      := 'FmNFeMPAdd';
  FRJanelas.Descricao := 'Adi��o de Entrada de Couros';
  FLJanelas.Add(FRJanelas);
  //
}
  // IND-GERAL-001 :: Cadastro de Setores
  New(FRJanelas);
  FRJanelas.ID        := 'IND-GERAL-001';
  FRJanelas.Nome      := 'FmListaSetores';
  FRJanelas.Descricao := 'Cadastro de Setores';
  FLJanelas.Add(FRJanelas);
  //
  // INN-GERAL-001 :: Entrada de ???
  New(FRJanelas);
  FRJanelas.ID        := 'INN-GERAL-001';
  FRJanelas.Nome      := 'FmEntradaCab';
  FRJanelas.Descricao := 'Entrada de ???';
  FLJanelas.Add(FRJanelas);
  //
  // INN_GERAL-002 :: Item de Entrada de ???
  New(FRJanelas);
  FRJanelas.ID        := 'INN_GERAL-002';
  FRJanelas.Nome      := 'FmEntradaIts';
  FRJanelas.Descricao := 'Item de Entrada de ???';
  FLJanelas.Add(FRJanelas);
  //
  // INN_GERAL-003 :: Pesquisa de Entrada de ???
  New(FRJanelas);
  FRJanelas.ID        := 'INN_GERAL-003';
  FRJanelas.Nome      := 'FmEntradaPesq';
  FRJanelas.Descricao := 'Pesquisa de Entrada de ???';
  FLJanelas.Add(FRJanelas);
  //
  // OSS-ABERT-001 :: OSs Abertas
  New(FRJanelas);
  FRJanelas.ID        := 'OSS-ABERT-001';
  FRJanelas.Nome      := 'FmOSsAbertas';
  FRJanelas.Descricao := 'OSs Abertas';
  FLJanelas.Add(FRJanelas);
  //
  // PST-_CECT-001 :: Controle de Estoque de Couros de Terceiros - Entrada
  New(FRJanelas);
  FRJanelas.ID        := 'PST-_CECT-001';
  FRJanelas.Nome      := 'FmCECTInn';
  FRJanelas.Descricao := 'Controle de Estoque de Couros de Terceiros - Entrada';
  FLJanelas.Add(FRJanelas);
  //
  // PST-_CECT-002 :: Controle de Estoque de Couros de Terceiros - Devolu��o
  New(FRJanelas);
  FRJanelas.ID        := 'PST-_CECT-002';
  FRJanelas.Nome      := 'FmCECTOut';
  FRJanelas.Descricao := 'Controle de Estoque de Couros de Terceiros - Devolu��o';
  FLJanelas.Add(FRJanelas);
  //
  // PST-_CECT-003 :: Edi��o de Item de Devolu��o de CECT
  New(FRJanelas);
  FRJanelas.ID        := 'PST-_CECT-003';
  FRJanelas.Nome      := 'FmCECTOutIts';
  FRJanelas.Descricao := 'Edi��o de Item de Devolu��o de CECT';
  FLJanelas.Add(FRJanelas);
  //
  // PST-_CECT-004 :: Edi��o de Item de Entrada de CECT
  New(FRJanelas);
  FRJanelas.ID        := 'PST-_CECT-004';
  FRJanelas.Nome      := 'FmCECTInnAdd';
  FRJanelas.Descricao := 'Edi��o de Item de Entrada de CECT';
  FLJanelas.Add(FRJanelas);
  //
  // SUB-PRODU-001 :: Cadastro de Gera��o de Subprodutos
  New(FRJanelas);
  FRJanelas.ID        := 'SUB-PRODU-001';
  FRJanelas.Nome      := 'FmMPSubProd';
  FRJanelas.Descricao := 'Cadastro de Gera��o de Subprodutos';
  FLJanelas.Add(FRJanelas);
  //
  // USO-ENTRA-001 :: Entrada de Uso e Consumo
  New(FRJanelas);
  FRJanelas.ID        := 'USO-ENTRA-001';
  FRJanelas.Nome      := 'FmUsoConsCab';
  FRJanelas.Descricao := 'Entrada de Uso e Consumo';
  FLJanelas.Add(FRJanelas);
  //
  // USO-ENTRA-002 :: Item de Entrada de Uso e Consumo
  New(FRJanelas);
  FRJanelas.ID        := 'USO-ENTRA-002';
  FRJanelas.Nome      := 'FmUsoConsIts';
  FRJanelas.Descricao := 'Item de Entrada de Uso e Consumo';
  FLJanelas.Add(FRJanelas);
  //
  // USO-ENTRA-003 :: Pesquisa de Entrada de Uso e Consumo
  New(FRJanelas);
  FRJanelas.ID        := 'USO-ENTRA-003';
  FRJanelas.Nome      := 'FmUsoConsPesq';
  FRJanelas.Descricao := 'Pesquisa de Entrada de Uso e Consumo';
  FLJanelas.Add(FRJanelas);
  //
  // VEN-COURO-101 :: Cadastro de Pedidos
  New(FRJanelas);
  FRJanelas.ID        := 'VEN-COURO-101';
  FRJanelas.Nome      := 'FmMPP';
  FRJanelas.Descricao := 'Cadastro de Pedidos';
  FLJanelas.Add(FRJanelas);
  //
  // VEN-COURO-102 :: Relat�rios  de Pedidos
  New(FRJanelas);
  FRJanelas.ID        := 'VEN-COURO-102';
  FRJanelas.Nome      := 'FmMPPImp';
  FRJanelas.Descricao := 'Relat�rios de Pedidos';
  FLJanelas.Add(FRJanelas);
  //
  // VEN-COURO-103 :: Localiza Pedido por Cliente
  New(FRJanelas);
  FRJanelas.ID        := 'VEN-COURO-103';
  FRJanelas.Nome      := 'FmMPPImp';
  FRJanelas.Descricao := 'Localiza Pedido por Cliente';
  FLJanelas.Add(FRJanelas);
  //
  // VEN-COURO-104 :: Programa��o de Entregas
  New(FRJanelas);
  FRJanelas.ID        := 'VEN-COURO-104';
  FRJanelas.Nome      := 'FmMPPProgEntreg';
  FRJanelas.Descricao := 'Programa��o de Entregas';
  FLJanelas.Add(FRJanelas);
  //
  // VEN-COURO-151 :: Faturamento de Pedidos
  New(FRJanelas);
  FRJanelas.ID        := 'VEN-COURO-151';
  FRJanelas.Nome      := 'FmMPVn';
  FRJanelas.Descricao := 'Faturamento de Pedidos';
  FLJanelas.Add(FRJanelas);
  //
  // VEN-COURO-152 :: Relat�rios de Faturamentos de Pedidos
  New(FRJanelas);
  FRJanelas.ID        := 'VEN-COURO-152';
  FRJanelas.Nome      := 'FmMPVImpFat';
  FRJanelas.Descricao := 'Relat�rios de Faturamentos de Pedidos';
  FLJanelas.Add(FRJanelas);
  //
  // VEN-COURO-153 :: Usu�rios de Faturamentos de Pedidos
  New(FRJanelas);
  FRJanelas.ID        := 'VEN-COURO-153';
  FRJanelas.Nome      := 'FmMPVImpOpc';
  FRJanelas.Descricao := 'Usu�rios de Faturamentos de Pedidos';
  FLJanelas.Add(FRJanelas);
  //
  // VEN-COURO-154 :: Relat�rios de Expedi��o de Pedidos
  New(FRJanelas);
  FRJanelas.ID        := 'VEN-COURO-154';
  FRJanelas.Nome      := 'FmMPVImpExp';
  FRJanelas.Descricao := 'Relat�rios de Expedi��o de Pedidos';
  FLJanelas.Add(FRJanelas);
  //
  // VEN-COURO-201 :: Venda de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'VEN-COURO-201';
  FRJanelas.Nome      := 'FmMPV2';
  FRJanelas.Descricao := 'Venda de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // VEN-COURO-202 :: Baixa de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'VEN-COURO-202';
  FRJanelas.Nome      := 'FmMPV2Bxa';
  FRJanelas.Descricao := 'Baixa de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // VEN-COURO-203 :: Estagios de Processamento
  New(FRJanelas);
  FRJanelas.ID        := 'VEN-COURO-203';
  FRJanelas.Nome      := 'FmMPEstagios';
  FRJanelas.Descricao := 'Estagios de Processamento';
  FLJanelas.Add(FRJanelas);
  //
  //
  //
  All_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  ERPSinc_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Enti_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  PerfJan_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  IBGE_DTB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  NFe_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  CMPT_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Grade_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  SPED_Geral_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  SPED_EFD_II_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  SPED_EFD_PC_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //SINTEGRA_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Mail_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  UnGeral_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  Finance_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Imprime_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Moedas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Fiscal_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  GFat_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  GPed_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  CNAB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Anotacoes_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Empresas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Tributos_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Praz_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UMedi_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  ConsumoGerl_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Consumo_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  VS_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  PQ_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Contab_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  DmkACBr_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  SrvTom_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnNFSe_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  Result := True;
end;


function TMyListas.CriaListaQeiLnk(TabelaCria: String;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
  Result := True;
  try
(*
    if Uppercase(TabelaCria) = Uppercase('VSEntiMP') then
    begin
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'Codigo';
      FRQeiLnk.RplyTab       := '';
      FRQeiLnk.RplyCol       := '';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpUSGSrvrInc;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'MPVImpOpc';
      FRQeiLnk.RplyTab       := 'MPVImpOpc';
      FRQeiLnk.RplyCol       := 'Codigo';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpRelatUsrSrv;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      //
    //end else if Uppercase(TabelaBase) = Uppercase('?) then
    //begin
    end else
*)
    begin
      All_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      ERPSinc_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Enti_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      PerfJan_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Finance_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      IBGE_DTB_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      NFe_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      CMPT_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Grade_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      SPED_Geral_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      SPED_EFD_II_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      SPED_EFD_PC_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //SINTEGRA_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Mail_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      UnGeral_TbTerc.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //
      Imprime_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Praz_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
  	  Moedas_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Fiscal_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      GFat_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      GPed_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      CNAB_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Anotacoes_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Empresas_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Tributos_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      UMedi_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      ConsumoGerl_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Consumo_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      VS_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      PQ_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      DmkACBr_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      SrvTom_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
    end;
  except
    raise;
    Result := False;
  end;
end;

(*
/*DELETE FROM vsfincla;*/
/*DELETE FROM vsnatart;*/
/*DELETE FROM vsnatcad;*/
/*DELETE FROM vspalsta;*/
/*DELETE FROM vsribcad;*/
/*DELETE FROM vsribcla;*/
/*DELETE FROM vsribope;*/
/*DELETE FROM vsserfch;*/
/*DELETE FROM vssubprd;*/
DELETE FROM vstrfloccab;
/*DELETE FROM vswetend;*/
DELETE FROM vsajscab;
DELETE FROM vsgerarta;
/*DELETE FROM vsgerartb;*/
DELETE FROM vsgerrcla;
/*DELETE FROM vsgerrclb;*/
DELETE FROM vsinncab;
DELETE FROM vsmovcab;
DELETE FROM vs movits;
DELETE FROM vspalleta;
DELETE FROM vsbxacab;
DELETE FROM vspaclacaba;
/*DELETE FROM vspaclacabb;*/
DELETE FROM vspaclaitsa;
/*DELETE FROM vspaclaitsb;*/
DELETE FROM vsparclcaba;
DELETE FROM vsparclitsa;
/*DELETE FROM vsparclitsb;*/
DELETE FROM vscaccab;
DELETE FROM vscacitsa;
DELETE FROM vscacitsb;
DELETE FROM vspwecab;
DELETE FROM vsdvlcab;
DELETE FROM vsrtbcab;
DELETE FROM vspamulcaba;
DELETE FROM vspamulItsa;
DELETE FROM vspamulcabr;
DELETE FROM vspamulItsr;
DELETE FROM vsfchrmpcab;
DELETE FROM vsmovdif;
DELETE FROM vsmovitz;
DELETE FROM vsopecab;
DELETE FROM vsoutcab;
DELETE FROM vsplccab;
DELETE FROM vsprepalcab;
DELETE FROM vssubprdcab;
DELETE FROM vscalcab;
DELETE FROM vscaldta;
DELETE FROM vscaljmp;
DELETE FROM vscalpda;
DELETE FROM vscurcab;
DELETE FROM vscurjmp;





*)
(* TODO

- Nao permitir duas vezes o mesmo palleta na configuracao de classificacao!

*)

end.

