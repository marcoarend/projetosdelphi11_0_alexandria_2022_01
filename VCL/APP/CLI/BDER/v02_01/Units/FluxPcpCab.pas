unit FluxPcpCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmFluxPcpCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrFluxPcpCab: TMySQLQuery;
    DsFluxPcpCab: TDataSource;
    QrFluxPcpIts: TMySQLQuery;
    DsFluxPcpIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrFluxPcpCabCodigo: TIntegerField;
    QrFluxPcpCabNome: TWideStringField;
    QrFluxPcpCabLk: TIntegerField;
    QrFluxPcpCabDataCad: TDateField;
    QrFluxPcpCabDataAlt: TDateField;
    QrFluxPcpCabUserCad: TIntegerField;
    QrFluxPcpCabUserAlt: TIntegerField;
    QrFluxPcpCabAlterWeb: TSmallintField;
    QrFluxPcpCabAWServerID: TIntegerField;
    QrFluxPcpCabAWStatSinc: TSmallintField;
    QrFluxPcpCabAtivo: TSmallintField;
    QrFluxPcpItsNO_IniStg: TWideStringField;
    QrFluxPcpItsNO_FImStg: TWideStringField;
    QrFluxPcpItsCodigo: TIntegerField;
    QrFluxPcpItsControle: TIntegerField;
    QrFluxPcpItsDiaSeq: TIntegerField;
    QrFluxPcpItsIniStg: TIntegerField;
    QrFluxPcpItsFimStg: TIntegerField;
    QrFluxPcpItsLk: TIntegerField;
    QrFluxPcpItsDataCad: TDateField;
    QrFluxPcpItsDataAlt: TDateField;
    QrFluxPcpItsUserCad: TIntegerField;
    QrFluxPcpItsUserAlt: TIntegerField;
    QrFluxPcpItsAlterWeb: TSmallintField;
    QrFluxPcpItsAWServerID: TIntegerField;
    QrFluxPcpItsAWStatSinc: TSmallintField;
    QrFluxPcpItsAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFluxPcpCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFluxPcpCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrFluxPcpCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrFluxPcpCabBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFluxPcpIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenFluxPcpIts(Controle: Integer);
    procedure ReordenaItens(Controle: Integer);

  end;

var
  FmFluxPcpCab: TFmFluxPcpCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, FluxPcpIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFluxPcpCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFluxPcpCab.MostraFluxPcpIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFluxPcpIts, FmFluxPcpIts, afmoNegarComAviso) then
  begin
    FmFluxPcpIts.ImgTipo.SQLType := SQLType;
    FmFluxPcpIts.FQrCab := QrFluxPcpCab;
    FmFluxPcpIts.FDsCab := DsFluxPcpCab;
    FmFluxPcpIts.FQrIts := QrFluxPcpIts;
    if SQLType = stIns then
    begin
      FmFluxPcpIts.EdDiaSeq.ValueVariant     := QrFluxPcpIts.RecordCount + 1;
    end else
    begin
      FmFluxPcpIts.EdControle.ValueVariant := QrFluxPcpItsControle.Value;
      //
      FmFluxPcpIts.EdDiaSeq.ValueVariant     := QrFluxPcpItsDiaSeq.Value;
      //
      FmFluxPcpIts.EdIniStg.ValueVariant     := QrFluxPcpItsIniStg.Value;
      FmFluxPcpIts.CBIniStg.KeyValue         := QrFluxPcpItsIniStg.Value;
      //
      FmFluxPcpIts.EdFimStg.ValueVariant     := QrFluxPcpItsFimStg.Value;
      FmFluxPcpIts.CBFimStg.KeyValue         := QrFluxPcpItsFimStg.Value;
      //
    end;
    FmFluxPcpIts.ShowModal;
    FmFluxPcpIts.Destroy;
  end;
end;

procedure TFmFluxPcpCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrFluxPcpCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrFluxPcpCab, QrFluxPcpIts);
end;

procedure TFmFluxPcpCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrFluxPcpCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrFluxPcpIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrFluxPcpIts);
end;

procedure TFmFluxPcpCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFluxPcpCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFluxPcpCab.DefParams;
begin
  VAR_GOTOTABELA := 'fluxpcpcab';
  VAR_GOTOMYSQLTABLE := QrFluxPcpCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM fluxpcpcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmFluxPcpCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFluxPcpIts(stUpd);
end;

procedure TFmFluxPcpCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmFluxPcpCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFluxPcpCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFluxPcpCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FluxPcpIts', 'Controle', QrFluxPcpItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFluxPcpIts,
      QrFluxPcpItsControle, QrFluxPcpItsControle.Value);
    ReopenFluxPcpIts(Controle);
    ReordenaItens(Controle);
  end;
end;

procedure TFmFluxPcpCab.ReopenFluxPcpIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFluxPcpIts, Dmod.MyDB, [
  'SELECT fsi.Nome NO_IniStg, fsf.Nome NO_FImStg, fpi.* ',
  'FROM fluxpcpits fpi ',
  'LEFT JOIN fluxpcpstg fsi ON fsi.Codigo=fpi.IniStg ',
  'LEFT JOIN fluxpcpstg fsf ON fsf.Codigo=fpi.FimStg ',
  'WHERE fpi.Codigo=' + Geral.FF0(QrFluxPcpCabCodigo.Value),
  'ORDER BY fpi.DiaSeq, fpi.Controle ',
  '']);
  //
  QrFluxPcpIts.Locate('Controle', Controle, []);
end;

procedure TFmFluxPcpCab.ReordenaItens(Controle: Integer);
begin
  Screen.Cursor := crHourGlass;
  try
    QrFluxPcpIts.DisableControls;
    QrFluxPcpIts.First;
    while not QrFluxPcpIts.Eof do
    begin
      Dmod.MyDB.Execute('UPDATE fluxpcpits SET DiaSeq=' +
      Geral.FF0(QrFluxPcpIts.RecNo) + ' WHERE Controle=' +
      Geral.FF0(QrFluxPcpItsControle.Value));
      //
      QrFluxPcpIts.Next;
    end;
    ReopenFluxPcpIts(Controle);
  finally
    QrFluxPcpIts.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFluxPcpCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFluxPcpCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFluxPcpCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFluxPcpCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFluxPcpCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFluxPcpCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxPcpCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFluxPcpCabCodigo.Value;
  Close;
end;

procedure TFmFluxPcpCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFluxPcpIts(stIns);
end;

procedure TFmFluxPcpCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFluxPcpCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'fluxpcpcab');
end;

procedure TFmFluxPcpCab.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('fluxpcpcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'fluxpcpcab', False, [
  'Nome'], ['Codigo'], [Nome], [Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmFluxPcpCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'fluxpcpcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'fluxpcpcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFluxPcpCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmFluxPcpCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmFluxPcpCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmFluxPcpCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFluxPcpCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFluxPcpCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFluxPcpCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFluxPcpCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFluxPcpCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFluxPcpCab.QrFluxPcpCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFluxPcpCab.QrFluxPcpCabAfterScroll(DataSet: TDataSet);
begin
  ReopenFluxPcpIts(0);
end;

procedure TFmFluxPcpCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrFluxPcpCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmFluxPcpCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFluxPcpCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'fluxpcpcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFluxPcpCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFluxPcpCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFluxPcpCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'fluxpcpcab');
end;

procedure TFmFluxPcpCab.QrFluxPcpCabBeforeClose(
  DataSet: TDataSet);
begin
  QrFluxPcpIts.Close;
end;

procedure TFmFluxPcpCab.QrFluxPcpCabBeforeOpen(DataSet: TDataSet);
begin
  QrFluxPcpCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

