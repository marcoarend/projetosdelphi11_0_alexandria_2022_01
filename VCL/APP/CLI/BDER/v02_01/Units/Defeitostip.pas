unit Defeitostip;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnDmkProcFunc, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkEdit, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmDefeitostip = class(TForm)
    PainelDados: TPanel;
    DsDefeitostip: TDataSource;
    QrDefeitostip: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrDefeitostipCodigo: TIntegerField;
    QrDefeitostipNome: TWideStringField;
    QrDefeitostipLk: TIntegerField;
    QrDefeitostipDataCad: TDateField;
    QrDefeitostipDataAlt: TDateField;
    QrDefeitostipUserCad: TIntegerField;
    QrDefeitostipUserAlt: TIntegerField;
    QrDefeitostipProcede: TIntegerField;
    QrDefeitostipNOMEPROCEDE: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    QrDefeitostipSigla: TWideStringField;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label10: TLabel;
    EdNome: TdmkEdit;
    Label4: TLabel;
    EdSigla: TdmkEdit;
    RGProcede: TRadioGroup;
    QrDefeitostipSimbolo: TIntegerField;
    ShCorBord2: TShape;
    Label6: TLabel;
    ShCorMiol2: TShape;
    Label7: TLabel;
    PnImageDado: TPanel;
    ImgDado: TImage;
    DBRadioGroup1: TDBRadioGroup;
    QrDefeitostipCorBorda: TIntegerField;
    QrDefeitostipCorMiolo: TIntegerField;
    SbCorBorda: TSpeedButton;
    SbCorMiolo: TSpeedButton;
    ShCorBorda: TShape;
    LaCorMiolo: TLabel;
    ShCorMiolo: TShape;
    LaCorBorda: TLabel;
    RGSimbolo: TdmkRadioGroup;
    PnImageEdit: TPanel;
    ImgEdita: TImage;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrDefeitostipAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrDefeitostipAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrDefeitostipBeforeOpen(DataSet: TDataSet);
    procedure QrDefeitostipCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure RGSimboloClick(Sender: TObject);
    procedure ImgCouroClick(Sender: TObject);
    procedure ImgCouroMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure SbCorBordaClick(Sender: TObject);
    procedure SbCorMioloClick(Sender: TObject);
  private
    //
    //FX, FY: Integer;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure DesenhaDefeito();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmDefeitostip: TFmDefeitostip;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, UnVS_CRC_PF, CoresVCLSkin;

{$R *.DFM}

procedure TFmDefeitostip.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmDefeitostip.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrDefeitostipCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmDefeitostip.DefParams;
begin
  VAR_GOTOTABELA := 'Defeitostip';
  VAR_GOTOMYSQLTABLE := QrDefeitostip;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM defeitostip');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmDefeitostip.DesenhaDefeito();
begin
  MyObjects.CarregaBitmapBranco(ImgEdita, 49, 49);
  VS_CRC_PF.DesenhaSimboloDefeito_Image(ImgEdita, (*X*)25, (*Y*)25,
  RGSimbolo.ItemIndex, ShCorBorda.Brush.Color, ShCorMiolo.Brush.Color);
end;

procedure TFmDefeitostip.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := '';
      EdSigla.Text := '';
      EdNome.Visible := True;
      if SQLType = stIns then
      begin
        EdCodigo.Text := '';
        EdNome.Text   := '';
        EdSigla.Text   := '';
        RGProcede.ItemIndex := -1;
        MyObjects.CarregaBitmapBranco(ImgEdita, 49, 49);
      end else begin
        EdCodigo.Text := IntToStr(QrDefeitostipCodigo.Value);
        EdNome.Text   := QrDefeitostipNome.Value;
        EdSigla.Text  := QrDefeitostipSigla.Value;
        RGProcede.ItemIndex := QrDefeitostipProcede.Value;
        RGSimbolo.ItemIndex := QrDefeitostipSimbolo.Value;
{
        MyObjects.CarregaBitmapBranco(ImgEdita, 49, 49);
        VS_CRC_PF.DesenhaSimboloDefeito(ImgEdita, (*X*)25, (*Y*)25,
          QrDefeitosTipSimbolo.Value);
}
        ShCorBorda.Brush.Color := QrDefeitosTipCorBorda.Value;
        ShCorBorda.Pen.Color   := QrDefeitosTipCorBorda.Value;
        ShCorMiolo.Brush.Color := QrDefeitosTipCorMiolo.Value;
        ShCorMiolo.Pen.Color   := QrDefeitosTipCorMiolo.Value;
      end;
      EdNome.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmDefeitostip.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmDefeitostip.QueryPrincipalAfterOpen;
begin
end;

procedure TFmDefeitostip.RGSimboloClick(Sender: TObject);
begin
  DesenhaDefeito;
end;

procedure TFmDefeitostip.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmDefeitostip.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmDefeitostip.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmDefeitostip.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmDefeitostip.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmDefeitostip.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmDefeitostip.BtAlteraClick(Sender: TObject);
var
  Defeitostip : Integer;
begin
  Defeitostip := QrDefeitostipCodigo.Value;
  if not UMyMod.SelLockY(Defeitostip, Dmod.MyDB, 'Defeitostip', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Defeitostip, Dmod.MyDB, 'Defeitostip', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmDefeitostip.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrDefeitostipCodigo.Value;
  Close;
end;

procedure TFmDefeitostip.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla: String;
  Codigo, Procede, Simbolo, CorBorda, CorMiolo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  Procede        := RGProcede.ItemIndex;
  Sigla          := EdSigla.Text;
  Simbolo        := RGSimbolo.ItemIndex;
  CorBorda       := ShCorBorda.Brush.Color;
  CorMiolo       := ShCorMiolo.Brush.Color;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome,
  'Defina uma descrição.') then Exit;
  if MyObjects.FIC(Trim(Sigla) = '', EdSigla,
  'Defina uma sigla.') then Exit;
  if MyObjects.FIC(RGProcede.ItemIndex = -1, RGProcede,
  'Defina uma procedência.') then Exit;
  //
  if DModG.SiglaDuplicada(Sigla, 'Sigla', 'Codigo', 'Nome', 'defeitostip',
  Codigo) then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('defeitostip', 'Codigo', SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'defeitostip', False, [
  'Nome', 'Procede', 'Sigla',
  'Simbolo', 'CorBorda', 'CorMiolo'], [
  'Codigo'], [
  Nome, Procede, Sigla,
  Simbolo, CorBorda, CorMiolo], [
  Codigo], True) then
(*
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO defeitostip SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Defeitostip', 'Defeitostip', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE defeitostip SET ');
    Codigo := QrDefeitostipCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Procede=:P1, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := RGProcede.ItemIndex;
  //
  Dmod.QrUpdU.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[03].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[04].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  *)
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'defeitostip', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmDefeitostip.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Defeitostip', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Defeitostip', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Defeitostip', 'Codigo');
end;

procedure TFmDefeitostip.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
  //
end;

procedure TFmDefeitostip.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrDefeitostipCodigo.Value,LaRegistro.Caption);
end;

procedure TFmDefeitostip.SbCorBordaClick(Sender: TObject);
begin
  if MyObjects.CriaForm_AcessoTotal(TFmCoresVCLSkin, FmCoresVCLSkin) then
  begin
    FmCoresVCLSkin.ShowModal;
    ShCorBorda.Brush.Color := FmCoresVCLSkin.FCor;
    ShCorBorda.Pen.Color   := FmCoresVCLSkin.FCor;
    FmCoresVCLSkin.Destroy;
  end;
  DesenhaDefeito();
end;

procedure TFmDefeitostip.SbCorMioloClick(Sender: TObject);
begin
  if MyObjects.CriaForm_AcessoTotal(TFmCoresVCLSkin, FmCoresVCLSkin) then
  begin
    FmCoresVCLSkin.ShowModal;
    ShCorMiolo.Brush.Color := FmCoresVCLSkin.FCor;
    ShCorMiolo.Pen.Color   := FmCoresVCLSkin.FCor;
    //
    FmCoresVCLSkin.Destroy;
  end;
  DesenhaDefeito();
end;

procedure TFmDefeitostip.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmDefeitostip.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmDefeitostip.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmDefeitostip.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmDefeitostip.QrDefeitostipAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmDefeitostip.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Defeitostip', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmDefeitostip.QrDefeitostipAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrDefeitostipCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrDefeitostipCodigo.Value, False);
  //
  MyObjects.CarregaBitmapBranco(ImgDado, 49, 49);
  VS_CRC_PF.DesenhaSimboloDefeito_Image(ImgDado, (*X*)25, (*Y*)25,
    QrDefeitosTipSimbolo.Value, QrDefeitosTipCorBorda.Value,
    QrDefeitosTipCorMiolo.Value);
  ShCorBord2.Brush.Color := QrDefeitosTipCorBorda.Value;
  ShCorBord2.Pen.Color   := QrDefeitosTipCorBorda.Value;
  ShCorMiol2.Brush.Color := QrDefeitosTipCorMiolo.Value;
  ShCorMiol2.Pen.Color   := QrDefeitosTipCorMiolo.Value;
end;


procedure TFmDefeitostip.SbQueryClick(Sender: TObject);
begin
  LocCod(QrDefeitostipCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Defeitostip', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmDefeitostip.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDefeitostip.ImgCouroClick(Sender: TObject);
begin
{
  LaX.Caption := IntToStr(FX);
  LaY.Caption := IntToStr(FY);
  //
  case RGDefeito.ItemIndex of
    0:  // Risco
    begin
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Style := bsSolid;
      with ImgCouro.Picture.Bitmap do
        Canvas.Polygon([
          Point(FX+4, FY-5),
          Point(FX+5, FY-4),
          Point(FX-4, FY+5),
          Point(FX-5, FY+4),
          Point(FX+4, FY-5)
        ]);
    end;
    1:  // Rasgo
    begin
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Style := bsSolid;
      with ImgCouro.Picture.Bitmap do
        Canvas.Polygon([
          Point(FX+0, FY-5),
          Point(FX+5, FY+5),
          Point(FX+5, FY-5),
          Point(FX+0, FY-5)
        ]);
    end;
    2:
    begin
      ImgCouro.Picture.Bitmap.Canvas.Brush.Color := clred;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Style := bsSolid;
      ImgCouro.Picture.Bitmap.Canvas.Ellipse(FX-5, FY-5, FX+5, FY+5);
    end;
    3: // Quadrado
    begin
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Pen.Style := psSolid;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Style := bsSolid;
      with ImgCouro.Picture.Bitmap do
        Canvas.Polygon([
          Point(FX-5, FY-5),
          Point(FX+5, FY-5),
          Point(FX+5, FY+5),
          Point(FX-5, FY+5),
          Point(FX-5, FY-5)
        ]);
    end;
    4: // Estrela
    begin
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Style := bsSolid;
      with ImgCouro.Picture.Bitmap do
        Canvas.Polygon([
          Point(FX+0, FY-5),
          Point(FX+2, FY-1),
          Point(FX+6, FY-1),
          Point(FX+3, FY+2),
          Point(FX+4, FY+5),
          Point(FX+0, FY+3),
          Point(FX-4, FY+5),
          Point(FX+0, FY+3),
          Point(FX-4, FY+5),
          Point(FX-3, FY+2),
          Point(FX-6, FY-1),
          Point(FX-2, FY-1),
          Point(FX+0, FY-5)
        ]);
    end;
    5: // Cruz
    begin
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Style := bsSolid;
      with ImgCouro.Picture.Bitmap do
        Canvas.Polygon([
          Point(FX-1, FY-5),
          Point(FX+1, FY-5),
          Point(FX+1, FY-1),
          Point(FX+5, FY-1),
          Point(FX+5, FY+1),
          Point(FX+1, FY+1),
          Point(FX+1, FY+5),
          Point(FX-1, FY+5),
          Point(FX-1, FY+1),
          Point(FX-5, FY+1),
          Point(FX-5, FY-1),
          Point(FX-1, FY-1),
          Point(FX-1, FY-5)
        ]);
    end;
    6: // Losango
    begin
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Style := bsSolid;
      with ImgCouro.Picture.Bitmap do
        Canvas.Polygon([
          Point(FX-0, FY-5),
          Point(FX+5, FY+0),
          Point(FX+0, FY+5),
          Point(FX-5, FY+0),
          Point(FX+0, FY-5)
        ]);
    end;
    7: // Triangulo
    begin
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Style := bsSolid;
      with ImgCouro.Picture.Bitmap do
        Canvas.Polygon([
          Point(FX+0, FY-5),
          Point(FX+5, FY+5),
          Point(FX-5, FY+5),
          Point(FX+0, FY-5)
        ]);
    end;
    8: // Circulo atravessado
    begin
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Style := bsSolid;
      with ImgCouro.Picture.Bitmap do
        Canvas.Polygon([
          Point(FX+4, FY-5),
          Point(FX+5, FY-4),
          Point(FX+3, FY-2),
          Point(FX+3, FY+1),
          Point(FX+1, FY+3),
          Point(FX-2, FY+3),
          Point(FX-4, FY+5),
          Point(FX-5, FY+4),
          Point(FX-3, FY+2),
          Point(FX-3, FY-1),
          Point(FX-1, FY-3),
          Point(FX+2, FY-3),
          Point(FX+4, FY-5)
        ]);
    end;
    9: // Circulo atravessado
    begin
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Style := bsSolid;
      with ImgCouro.Picture.Bitmap do
        Canvas.Polygon([
          Point(FX-6, FY-3),
          Point(FX+6, FY-3),
          Point(FX+6, FY+3),
          Point(FX-6, FY+3),
          Point(FX-6, FY-3)
        ]);
    end;
    10: // Nave Atari
    begin
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Pen.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Color := clRed;
      ImgCouro.Picture.Bitmap.Canvas.Brush.Style := bsSolid;
      with ImgCouro.Picture.Bitmap do
        Canvas.Polygon([
          Point(FX-5, FY-5),
          Point(FX+0, FY+0),
          Point(FX+5, FY-5),
          Point(FX+5, FY+5),
          Point(FX+0, FY+0),
          Point(FX-5, FY+5),
          Point(FX-5, FY-5)
        ]);
    end;
  end;
  ImgCouro.Invalidate;
}
end;

procedure TFmDefeitostip.ImgCouroMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
{
  FY := Y;
  FX := X;
}
end;

procedure TFmDefeitostip.QrDefeitostipBeforeOpen(DataSet: TDataSet);
begin
  QrDefeitostipCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmDefeitostip.QrDefeitostipCalcFields(DataSet: TDataSet);
begin
  case QrDefeitostipProcede.Value of
    0: QrDefeitostipNOMEPROCEDE.Value := 'CRIAÇÃO';
    1: QrDefeitostipNOMEPROCEDE.Value := 'FRIGORÍFICO';
    2: QrDefeitostipNOMEPROCEDE.Value := 'CURTUME';
    else QrDefeitostipNOMEPROCEDE.Value := '?????';
  end;  
end;

end.

