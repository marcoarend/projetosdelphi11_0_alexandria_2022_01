unit FormulasAlt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkMemo;

type
  TFmFormulasAlt = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    MeHistAlter: TdmkMemo;
    EdVersaoAtu: TdmkEdit;
    Label1: TLabel;
    EdVersaoNew: TdmkEdit;
    Label2: TLabel;
    EdNumero: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    EdNome: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    //function DuplicaRegistro3(): Boolean;
  public
    { Public declarations }
  end;

  var
  FmFormulasAlt: TFmFormulasAlt;

implementation

uses UnMyObjects, Module, DmkDAC_PF, MyDBCheck, UMySQLModule, ModuleGeral,
  UnPQ_PF, Formulas;

{$R *.DFM}

procedure TFmFormulasAlt.BtOKClick(Sender: TObject);
//begin
  //DuplicaRegistro3();
const
  Motivo = 0;
var
  Numero, VerNew, VerAtu: Integer;
  HistAlter: String;
begin
  Numero := EdNumero.ValueVariant;
  VerNew := EdVersaoNew.ValueVariant;
  VerAtu := EdVersaoAtu.ValueVariant;
  HistAlter := MeHistAlter.Text;
  //
  if MyObjects.FIC(Length(HistAlter) < 15, MeHistAlter,
  'Informe pelo menos 15 caracteres na descri��o das altera��es!') then Exit;
  //
  Dmod.MyDB.Execute('DELETE FROM formulavits WHERE Numero=' +
    Geral.FF0(Numero) + ' AND Versao=' + Geral.FF0(VerAtu));
  Dmod.MyDB.Execute('DELETE FROM formulav WHERE Numero=' +
    Geral.FF0(Numero) + ' AND Versao=' + Geral.FF0(VerAtu));
  //
  if PQ_PF.TransfereReceitaDeArquivo(TOrientTabArqBD.stabdAcessivelToVersoes,
  FmFormulas.TbFormulasNumero.Value, Motivo) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'formulasits', False, [
    'Versao'], [
    'Numero'], [
    VerNew], [
    Numero], False) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'formulas', False, [
      'Versao', 'HistAlter'], [
      'Numero'], [
      VerNew, HistAlter], [
      Numero], True);
    end;
    FmFormulas.TbFormulas.Refresh;
    FmFormulas.TbFormulas.Locate('Numero', Numero, []);
    if FmFormulas.TbFormulasIts.State = dsInactive then
      UnDmkDAC_PF.AbreTable(FmFormulas.TbFormulasIts, Dmod.MyDB);
    UMyMod.AlteraRegistroTb(FmFormulas, FmFormulas.TbFormulas, nil, ImgTipo);
    FmFormulas.ReopenGraGruX();
    FmFormulas.ImgTipo.SQLType := stUpd;
    FmFormulas.DBGrid1.Visible  := FmFormulas.ImgTipo.SQLType <> stIns;
    FmFormulas.DBGrid1.ReadOnly := FmFormulas.ImgTipo.SQLType = stLok;
    Close;
  end;
end;

procedure TFmFormulasAlt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

{
function TFmFormulasAlt.DuplicaRegistro3: Boolean;
var
  //Formula,
  Atual: Integer;
begin
  if DBCheck.AcessoNegadoAoForm_4(afmoNegarComAviso, 'QUI-RECEI-DUP') then Exit;
  //
  Atual := FmFormulas.TbFormulasNumero.Value;
  //
  if(FmFormulas.TbFormulas.State <> dsInactive) and (FmFormulas.TbFormulas.RecordCount > 0) and (Atual <> 0) then
  begin
    if Geral.MB_Pergunta('Confirma a duplica��o da receita atual?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        FmFormulas.TbFormulas.DisableControls;
        FmFormulas.TbFormulasIts.DisableControls;
        //
        //
        if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'formulas', TMeuDB,
          ['Numero'], [Atual],
          [], //['Numero', 'Nome', 'DataI', 'DataCad'],
          [], //[Formula, Nome, Date, Date],
          '', True, nil, nil) then
        begin
          if (FmFormulas.TbFormulasIts.State <> dsInactive) and (FmFormulas.TbFormulasIts.RecordCount > 0) then
          begin
            FmFormulas.TbFormulasIts.First;
            while not FmFormulas.TbFormulasIts.Eof do
            begin
              //Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'FormulasIts','FormulasIts', 'Controle');
              //
              if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'formulasits', TMeuDB,
                ['Controle'], [FmFormulas.TbFormulasItsControle.Value],
                [], //['Numero', 'Controle'],
                [], //[Formula, Controle],
                '', False, nil, nil) then
              begin
                //
              end;
              FmFormulas.TbFormulasIts.Next;
            end;
          end;
        end;
      finally
        FmFormulas.TbFormulas.EnableControls;
        FmFormulas.TbFormulasIts.EnableControls;
        //
        Screen.Cursor := crDefault;
      end;
      FmFormulas.TbFormulas.Refresh;
      FmFormulas.TbFormulas.Locate('Numero', Formula, []);
      AtualizaFmFormulas.TbFormulas;
      FmFormulas.TbFormulas.Refresh;
      //
      Geral.MB_Aviso('A receita n� ' + Geral.FF0(Atual) +
        ' foi duplicada com sucesso!'+sLineBreak+'O n�mero da nova f�rmula � ' +
        Geral.FF0(Formula) + '!');
    end;
  end;
end;
}

procedure TFmFormulasAlt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormulasAlt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmFormulasAlt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
