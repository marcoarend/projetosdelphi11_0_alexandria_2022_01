unit PQEmbFor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, Mask, DBCtrls, dmkEdit, DB,
  mySQLDbTables, dmkRadioGroup, dmkEditCB, dmkDBLookupComboBox, Menus, dmkImage,
  UnDmkEnums, DmkDAC_PF;

type
  TFmPQEmbFor = class(TForm)
    DsEmbalagens: TDataSource;
    QrEmbalagens: TmySQLQuery;
    Panel3: TPanel;
    CBEmbalagem: TdmkDBLookupComboBox;
    EdEmbalagem: TdmkEditCB;
    Label1: TLabel;
    QrEmbalagensCodigo: TIntegerField;
    QrEmbalagensNome: TWideStringField;
    EdcProd: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdObservacao: TdmkEdit;
    SpeedButton1: TSpeedButton;
    Label4: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    SbPQ: TSpeedButton;
    QrPQ: TmySQLQuery;
    DsPQ: TDataSource;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    Label5: TLabel;
    EdxProd: TdmkEdit;
    Memo1: TMemo;
    PMPQ: TPopupMenu;
    CadastroAutomtico1: TMenuItem;
    CadastroManual1: TMenuItem;
    QrLoc: TmySQLQuery;
    QrLocControle: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbPQClick(Sender: TObject);
    procedure CadastroManual1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    F_CI, F_IQ: Integer;
  end;

  var
  FmPQEmbFor: TFmPQEmbFor;

implementation

uses UnMyObjects, MyDBCheck, UMySQLModule, Module, Embalagens, PQ, UnInternalConsts, dmkGeral,
  ModProd;

{$R *.DFM}

procedure TFmPQEmbFor.BtOKClick(Sender: TObject);
var
  cProd, Observacao: String;
  Controle, Embalagem, PQ: Integer;
begin
  cProd      := EdcProd.Text;
  Observacao := EdObservacao.Text;
  Embalagem  := EdEmbalagem.ValueVariant;
  //
  PQ := EdPQ.ValueVariant;
  QrLoc.Close;
  QrLoc.Params[00].AsInteger := F_CI;
  QrLoc.Params[01].AsInteger := F_IQ;
  QrLoc.Params[02].AsInteger := PQ;
  UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
  //
  Controle := QrLocControle.Value;
  if Controle = 0 then
  begin
    Geral.MB_Aviso(
    'N�o h� cadastro deste produto para o cliente interno / fornecedor selecionado!'
    );
    Exit;
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pqforemb', False, [
  'Embalagem', 'Observacao'], ['cProd', 'Controle'], [
  Embalagem, Observacao], [cProd, Controle], True) then
    Close;
end;

procedure TFmPQEmbFor.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQEmbFor.CadastroManual1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmPQ, FmPQ, afmoNegarComAviso) then
  begin
    DmProd.FPrdGrupTip_Nome := EdxProd.Text;
    FmPQ.ShowModal;
    FmPQ.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrPQ.Close;
      UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
      EdPQ.ValueVariant := VAR_CADASTRO;
      CBPQ.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmPQEmbFor.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQEmbFor.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
end;

procedure TFmPQEmbFor.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQEmbFor.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEmbalagens, FmEmbalagens, afmoNegarComAviso) then
  begin
    FmEmbalagens.ShowModal;
    FmEmbalagens.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrEmbalagens.Close;
      UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
      EdEmbalagem.ValueVariant := VAR_CADASTRO;
      CBEmbalagem.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmPQEmbFor.SbPQClick(Sender: TObject);
begin
  //MyObjects.MostraPopUpDeBotao(PMPQ, SbPQ);
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmPQ, FmPQ, afmoNegarComAviso) then
  begin
    DmProd.FPrdGrupTip_Nome := EdxProd.Text;
    FmPQ.ShowModal;
    FmPQ.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrPQ.Close;
      UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
      EdPQ.ValueVariant := VAR_CADASTRO;
      CBPQ.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

end.
