unit PQB2Mul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, mySQLDbTables,
  dmkGeral, dmkImage, UnDmkEnums, UnInternalConsts;

type
  THackDBGrid = class(TDBGrid);
  TFmPQB2Mul = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    RadioGroup1: TRadioGroup;
    DBGrid1: TDBGrid;
    DsPQBIts: TDataSource;
    TbPQBIts: TmySQLTable;
    TbPQBItsControle: TIntegerField;
    TbPQBItsPQ: TIntegerField;
    TbPQBItsCI: TIntegerField;
    TbPQBItsCustoPadrao: TFloatField;
    TbPQBItsPeso: TFloatField;
    TbPQBItsValor: TFloatField;
    TbPQBItsCusto: TFloatField;
    TbPQBItsNO_PQ: TWideStringField;
    TbPQBItsAtivo: TSmallintField;
    TbPQBItsAjPeso: TFloatField;
    TbPQBItsAjValor: TFloatField;
    TbPQBItsAjCusto: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbPQBItsBeforePost(DataSet: TDataSet);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FCampoAtual: String;
  public
    { Public declarations }
    FPQBIts: String;
    //
    procedure ReopenPQBIts();
  end;

  var
  FmPQB2Mul: TFmPQB2Mul;

implementation

uses UnMyObjects, ModuleGeral, PQB2, Module, UMySQLModule, PQx, DmkDAC_PF;

{$R *.DFM}

procedure TFmPQB2Mul.BtOKClick(Sender: TObject);
const
  Tipo      = VAR_FATID_0000;
  Retorno   = 0;
  StqMovIts = 0;
var
  Peso, Valor, AjPeso, AjValor, OrigemCtrl: Double;
  CliOrig, CliDest, Insumo, OrigemCodi: Integer;
  DataX: String;
begin
{
  OrigemCodi := FmPQB.QrBalancosPeriodo.Value;
  DataX      := dmkPF.PrimeiroDiaDoPeriodo(FmPQB.QrBalancosPeriodo.Value, dtSystem);
  //
  TbPQBIts.First;
  while not TbPQBIts.Eof do
  begin
    CliOrig    := TbPQBItsCI.Value;
    CliDest    := TbPQBItsCI.Value;
    Insumo     := TbPQBItsPQ.Value;
    Peso    := TbPQBItsPeso.Value;
    Valor   := TbPQBItsValor.Value;
    AjPeso  := TbPQBItsAjPeso.Value;
    AjValor := TbPQBItsAjValor.Value;
    //AjCusto := TbPQBItsAjCusto.Value;
 (*
    if (Peso <> AjPeso) or (Valor <> AjValor) then
    begin
      Peso := AjPeso - Peso;
      Valor := AjValor - Valor;
      OrigemCtrl := UMyMod.BuscaEmLivreY_Double(
        Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'p q x', False, [
      'DataX', 'CliOrig', 'CliDest',
      'Insumo', 'Peso', 'Valor',
      'Retorno', 'StqMovIts'], [
      'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
      DataX, CliOrig, CliDest,
      Insumo, Peso, Valor,
      Retorno, StqMovIts], [
      OrigemCodi, OrigemCtrl, Tipo], False);
      UnPQx.AtualizaEstoquePQ(CliOrig, Insumo, aeNenhum, '');
    end;
*)
/
    if ImgTipo.SQLType = stIns then
    begin
      Conta := UMyMod.BuscaEmLivreY_Double(
      Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('INSERT INTO p q x SET Tipo=' + Geral.FF0(VAR_FATID_0000) + ', Peso=:P0, Valor=:P1, ');
      Dmod.QrUpdU.SQL.Add('Insumo=:P2, CliOrig=:P3, CliDest=:P4, Datax=:P5, ');
      Dmod.QrUpdU.SQL.Add('OrigemCodi=:P6, OrigemCtrl=:P7 ');
/
    end else begin
    TbPQBIts.Next;
  end;
  FmPQB.SomaBalanco();
  FmPQB.ReindexaTabela(True);
  Close;
}
end;

procedure TFmPQB2Mul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQB2Mul.DBGrid1ColEnter(Sender: TObject);
begin
  FCampoAtual := DBGrid1.Columns[THackDBGrid(DBGrid1).Col-1].FieldName;
end;

procedure TFmPQB2Mul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQB2Mul.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmPQB2Mul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQB2Mul.ReopenPQBIts();
begin
  TbPQBIts.Close;
  TbPQBIts.TableName := FPQBIts;
  UnDmkDAC_PF.AbreTable(TbPQBIts, DModG.MyPID_DB, '');
end;

procedure TFmPQB2Mul.TbPQBItsBeforePost(DataSet: TDataSet);
begin
  if FCampoAtual = 'AjValor' then
  begin
    if TbPQBItsAjPeso.Value <> 0 then
      TbPQBItsAjCusto.Value := TbPQBItsAjValor.Value / TbPQBItsAjPeso.Value;
     //  n�o zerar pre�o!!!
    //else
      //TbPQBItsAjCusto.Value := 0;
  end else
  if (FCampoAtual = 'AjCusto') or (FCampoAtual = 'AjPeso') then
    TbPQBItsAjValor.Value := TbPQBItsAjCusto.Value * TbPQBItsAjPeso.Value;
end;

end.
