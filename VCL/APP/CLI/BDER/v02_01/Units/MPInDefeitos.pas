unit MPInDefeitos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkGeral, dmkLabel, dmkImage, UnDmkEnums;

type
  TFmMPInDefeitos = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    CBTip: TdmkDBLookupComboBox;
    DsTip: TDataSource;
    EdTip: TdmkEditCB;
    EdDef: TdmkEdit;
    Label3: TLabel;
    QrTip: TmySQLQuery;
    Label4: TLabel;
    EdLoc: TdmkEditCB;
    CBLoc: TdmkDBLookupComboBox;
    DsLoc: TDataSource;
    QrLoc: TmySQLQuery;
    QrTipCodigo: TIntegerField;
    QrTipNome: TWideStringField;
    QrLocCodigo: TIntegerField;
    QrLocNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmMPInDefeitos: TFmMPInDefeitos;

implementation

uses UnMyObjects, Module, Entidades, MPIn, Principal;

{$R *.DFM}

procedure TFmMPInDefeitos.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPInDefeitos.BtConfirmaClick(Sender: TObject);
var
  Def, Loc, Tip, Controle: Integer;
begin
  Def := Geral.IMV(EdDef.Text);
  Loc := Geral.IMV(EdLoc.Text);
  Tip := Geral.IMV(EdTip.Text);
  if Def=0 then
  begin
    Application.MessageBox(PChar('Informe a quantidade de defeitos!'),
      'Erro', MB_OK+MB_ICONERROR);
    EdDef.SetFocus;
    Exit;
  end;
  if Tip=0 then
  begin
    Application.MessageBox(PChar('Informe o tipo de defeito!'),
      'Erro', MB_OK+MB_ICONERROR);
    EdTip.SetFocus;
    Exit;
  end;
  (*if Loc=0 then
  begin
    Application.MessageBox(PChar('Informe o local dos defeitos!'),
      'Erro', MB_OK+MB_ICONERROR);
    EdLoc.SetFocus;
    Exit;
  end;*)
  Dmod.QrUpdM.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'MPInDefeitos', 'MPInDefeitos', 'Controle');
    Dmod.QrUpdM.SQL.Add('INSERT INTO MPInDefeitos SET ');
  end else begin
    Controle := FmMPIn.QrDefeitosControle.Value;
    Dmod.QrUpdM.SQL.Add('UPDATE MPInDefeitos SET ');
  end;
  Dmod.QrUpdM.SQL.Add('Defeitos=:P0, Defeitoloc=:P1, Defeitotip=:P2 ');
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdM.SQL.Add(', MPInCtrl=:Pa, Controle=:Pb')
  else
    Dmod.QrUpdM.SQL.Add('WHERE MPInCtrl=:Pa AND Controle=:Pb');
  //
  Dmod.QrUpdM.Params[00].AsInteger := Def;
  Dmod.QrUpdM.Params[01].AsInteger := Loc;
  Dmod.QrUpdM.Params[02].AsInteger := Tip;
  Dmod.QrUpdM.Params[03].AsInteger := FmMPIn.QrMPInControle.Value;
  Dmod.QrUpdM.Params[04].AsInteger := Controle;
  Dmod.QrUpdM.ExecSQL;
  EdDef.Text := '';
  EdDef.SetFocus;
  FmMPIn.FDefeitos := Controle;
  FmMPIn.ReopenDefeitos;
  if ImgTipo.SQLType = stUpd then Close;
end;

procedure TFmMPInDefeitos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrLoc, DMod.MyDB);
  UMyMod.AbreQuery(QrTip, DMod.MyDB);
end;

procedure TFmMPInDefeitos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPInDefeitos.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraDefeitostip(EdTip.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrTip, DMod.MyDB);
    //
    EdTip.ValueVariant := VAR_CADASTRO;
    CBTip.KeyValue     := VAR_CADASTRO;
    EdTip.SetFocus
  end;
end;

procedure TFmMPInDefeitos.SpeedButton2Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraDefeitosloc(EdLoc.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrLoc, DMod.MyDB);
    //
    EdLoc.ValueVariant := VAR_CADASTRO;
    CBLoc.KeyValue     := VAR_CADASTRO;
    EdLoc.SetFocus;
  end;
end;

procedure TFmMPInDefeitos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.
