unit PQN;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, DmkDAC_PF,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, Menus, frxClass, frxDBSet, Variants, dmkGeral, UnDmkProcFunc,
  dmkDBLookupComboBox, dmkEditCB, dmkDBEdit, dmkEdit, dmkEditDateTimePicker,
  dmkLabel, dmkImage, UnDmkEnums, UnEmpresas;

type
  TFmPQN = class(TForm)
    PnDados: TPanel;
    DsPQN: TDataSource;
    QrPQN: TMySQLQuery;
    PnEdita: TPanel;
    PnEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    PnData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrPQNCodigo: TIntegerField;
    QrPQNDataB: TDateField;
    QrPQNLk: TIntegerField;
    QrPQNDataCad: TDateField;
    QrPQNDataAlt: TDateField;
    QrPQNUserCad: TIntegerField;
    QrPQNUserAlt: TIntegerField;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label2: TLabel;
    TPDataB: TdmkEditDateTimePicker;
    QrPQNIts: TMySQLQuery;
    QrPQNItsDataX: TDateField;
    QrPQNItsTipo: TSmallintField;
    QrPQNItsCliOrig: TIntegerField;
    QrPQNItsCliDest: TIntegerField;
    QrPQNItsInsumo: TIntegerField;
    QrPQNItsPeso: TFloatField;
    QrPQNItsValor: TFloatField;
    QrPQNItsOrigemCodi: TIntegerField;
    QrPQNItsOrigemCtrl: TIntegerField;
    QrPQNItsNOMEPQ: TWideStringField;
    QrPQNItsGrupoQuimico: TIntegerField;
    QrPQNItsNOMEGRUPO: TWideStringField;
    QrPQNItsEmpresa: TIntegerField;
    DsPQNIts: TDataSource;
    DBGIts: TDBGrid;
    QrPQ: TmySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    DsCI: TDataSource;
    QrCI: TmySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    QrSaldo: TmySQLQuery;
    QrSaldoPeso: TFloatField;
    QrSaldoValor: TFloatField;
    QrSaldoCUSTO: TFloatField;
    QrSaldoCI: TIntegerField;
    QrSaldoPQ: TIntegerField;
    DsSaldo: TDataSource;
    PnInsumos: TPanel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    EdPesoAdd: TdmkEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    EdSaldoFut: TdmkEdit;
    PMInclui: TPopupMenu;
    IncluiNovaBaixa1: TMenuItem;
    IncluiInsumobaixaatual1: TMenuItem;
    QrSumPQ: TmySQLQuery;
    QrSumPQCUSTO: TFloatField;
    PMExclui: TPopupMenu;
    ExcluiItemdabaixa1: TMenuItem;
    ExcluiBaixa1: TMenuItem;
    PMAltera: TPopupMenu;
    AlteraBaixa1: TMenuItem;
    PMImprime: TPopupMenu;
    Estabaixa1: TMenuItem;
    Outros1: TMenuItem;
    frxDsPQIIts: TfrxDBDataset;
    frxDsPQI: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBConfirm2: TGroupBox;
    Panel6: TPanel;
    Panel7: TPanel;
    BtConfirma2: TBitBtn;
    BtDesiste2: TBitBtn;
    GBControle: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Label14: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrSE: TMySQLQuery;
    QrSECodigo: TIntegerField;
    QrSENome: TWideStringField;
    DsSE: TDataSource;
    Label3: TLabel;
    EdSetor: TdmkEditCB;
    CBSetor: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdFunci: TdmkEditCB;
    CBFunci: TdmkDBLookupComboBox;
    SpeedButton6: TSpeedButton;
    Label5: TLabel;
    EdNome: TdmkEdit;
    QrFunci: TMySQLQuery;
    DsFunci: TDataSource;
    QrFunciCodigo: TIntegerField;
    QrFunciNO_Funci: TWideStringField;
    QrPQNNome: TWideStringField;
    QrPQNSetor: TIntegerField;
    QrPQNFunci: TIntegerField;
    QrPQNNO_FUNCI: TWideStringField;
    QrPQNNO_SETOR: TWideStringField;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    QrPQNTipoEnt: TSmallintField;
    frxUSO_CONSU_003_A: TfrxReport;
    QrPeriodo: TMySQLQuery;
    QrPeriodoCUSTO_m3: TFloatField;
    QrPeriodoCodigo: TIntegerField;
    QrPeriodoETE_m3: TFloatField;
    QrPeriodoLodo: TFloatField;
    QrPeriodoHorasTrab: TFloatField;
    QrPeriodoDataB: TDateField;
    QrPeriodoLk: TIntegerField;
    QrPeriodoDataCad: TDateField;
    QrPeriodoDataAlt: TDateField;
    QrPeriodoUserCad: TIntegerField;
    QrPeriodoUserAlt: TIntegerField;
    QrPeriodoValorHora: TFloatField;
    QrPeriodoCustoHora: TFloatField;
    QrPeriodoCustoInsumo: TFloatField;
    QrPeriodoCustoTotal: TFloatField;
    frxPeriodo: TfrxReport;
    frxDsPeriodo: TfrxDBDataset;
    SintticoCustos1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPQNAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrPQNAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPQNBeforeOpen(DataSet: TDataSet);
    procedure QrPQNAfterClose(DataSet: TDataSet);
    procedure EdPQChange(Sender: TObject);
    procedure EdCIChange(Sender: TObject);
    procedure EdPesoAddExit(Sender: TObject);
    procedure QrSaldoAfterOpen(DataSet: TDataSet);
    procedure BtDesiste2Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure IncluiNovaBaixa1Click(Sender: TObject);
    procedure IncluiInsumobaixaatual1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure ExcluiItemdabaixa1Click(Sender: TObject);
    procedure AlteraBaixa1Click(Sender: TObject);
    procedure Estabaixa1Click(Sender: TObject);
    procedure Outros1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frBaixaAtualUserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure frxUSO_CONSU_003_AGetValue(const VarName: String;
      var Value: Variant);
    procedure SbNovoClick(Sender: TObject);
    procedure ExcluiBaixa1Click(Sender: TObject);
    procedure CkDtCorrApoClick(Sender: TObject);
    procedure SintticoCustos1Click(Sender: TObject);
  private
    FPQTIts: Integer;
    FEmpresa: Integer;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenPQTIts;
    procedure CalculaSaldoFuturo;
    procedure AtualizaCusto;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmPQN: TFmPQN;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, PQx, Principal, GetPeriodo, UnPQ_PF, ModuleGeral;

{$R *.DFM}


const
  FThis_FATID = VAR_FATID_0185;


/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPQN.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQN.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPQNCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPQN.DefParams;
begin
  VAR_GOTOTABELA := 'pqn';
  VAR_GOTOMYSQLTABLE := QrPQN;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := 'Empresa=' + Geral.FF0(FEmpresa);

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FUNCI,');
  VAR_SQLx.Add('ent.Tipo TipoEnt, lse.Nome NO_SETOR, pqn.*');
  VAR_SQLx.Add('FROM pqn pqn');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=pqn.Funci');
  VAR_SQLx.Add('LEFT JOIN listasetores lse ON lse.Codigo=pqn.Setor');
  VAR_SQLx.Add('WHERE pqn.Codigo > -1000');
  VAR_SQLx.Add('AND pqn.Empresa=' + Geral.FF0(FEmpresa));
  //
  VAR_SQL1.Add('AND pqn.Codigo=:P0');
  //
  VAR_SQLa.Add('AND pqn.Nome Like :P0');
  //
end;

procedure TFmPQN.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      GBControle.Visible := True;
      PnDados.Visible    := True;
      PnEdita.Visible    := False;
      PnInsumos.Visible  := False;
      GBConfirm2.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      GBControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant  := '';
        TPDataB.Date           := Date;
        EdNome.Text            := '';
        EdSetor.ValueVariant   := 0;
        CBSetor.KeyValue       := null;
        EdFunci.ValueVariant   := 0;
        CBFunci.KeyValue       := null;
      end else begin
        EdCodigo.ValueVariant  := QrPQNCodigo.Value;
        //
        TPDataB.Date           := QrPQNDataB.Value;
        EdNome.Text            := QrPQNNome.Value;
        EdSetor.ValueVariant   := QrPQNSetor.Value;
        CBSetor.KeyValue       := QrPQNSetor.Value;
        EdFunci.ValueVariant   := QrPQNFunci.Value;
        CBFunci.KeyValue       := QrPQNFunci.Value;
      end;
      TPDataB.SetFocus;
    end;
    2:
    begin
      // 2023-09-09
      if UnPQx.ImpedePeloBalanco(QrPQNDataB.Value) then Exit;
      // 2023-09-09
      PnInsumos.Visible  := True;
      GBConfirm2.Visible := True;
      GBControle.Visible := False;
      //
      EdCI.Text := IntToStr(Dmod.QrMasterDono.Value);
      CBCI.KeyValue := Dmod.QrMasterDono.Value;
      EdCI.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmPQN.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPQN.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPQN.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQN.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQN.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQN.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQN.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQN.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmPQN.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmPQN.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPQNCodigo.Value;
  Close;
end;

procedure TFmPQN.BtConfirmaClick(Sender: TObject);
var
  Nome, DataB: String;
  Codigo, Empresa, Funci, Setor: Integer;
  SQLType: TSQLType;
begin
  if UnPQx.ImpedePeloBalanco(TPDataB.Date) then Exit;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  Funci          := EdFunci.ValueVariant;
  Setor          := EdSetor.ValueVariant;
  DataB          := Geral.FDT(TPDataB.Date, 1);

  //
  Codigo := UMyMod.BPGS1I32('pqn', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqn', False, [
  'Empresa', 'DataB',
  'Nome', 'Funci', 'Setor'], [
  'Codigo'], [
  Empresa, DataB,
  Nome, Funci, Setor], [
  Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqn', 'Codigo');
    AtualizaCusto;
    MostraEdicao(0, stLok, Codigo);
  end;
end;

procedure TFmPQN.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'pqn', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqn', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqn', 'Codigo');
end;

procedure TFmPQN.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  TPDtCorrApo.Date  := 0;
  TPDataB.Date      := Date;
  PnEdit.Align      := alClient;
  PnDados.Align     := alClient;
  PnEdita.Align     := alClient;
  DBGIts.Align      := alClient;
  LaRegistro.Align  := alClient;
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSE, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFunci, Dmod.MyDB);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  CriaOForm;
end;

procedure TFmPQN.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPQNCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPQN.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPQN.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmPQN.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQN.QrPQNAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPQN.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'pqn', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmPQN.QrPQNAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrPQNCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrPQNCodigo.Value, False);
  ReopenPQTIts;
end;

procedure TFmPQN.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPQNCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'pqn', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPQN.SintticoCustos1Click(Sender: TObject);
begin
  PQ_PF.MostraFormUsoConsImp(FThis_FATID);
end;

procedure TFmPQN.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQN.QrPQNBeforeOpen(DataSet: TDataSet);
begin
  QrPQNCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPQN.QrPQNAfterClose(DataSet: TDataSet);
begin
  QrPQNIts.Close;
end;

procedure TFmPQN.ReopenPQTIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQNIts, Dmod.MyDB, [
  'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  ',
  'pqg.Nome NOMEGRUPO ',
  'FROM pqx pqx ',
  'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo ',
  'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ ',
  'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico ',
  'WHERE pqx.Tipo=' + Geral.FF0(FThis_FATID),
  'AND pqx.OrigemCodi=' + Geral.FF0(QrPQNCodigo.Value),
  '']);
  //
  if FPQTIts <> 0 then
    QrPQNIts.Locate('OrigemCtrl', FPQTIts, []);
end;

procedure TFmPQN.EdPQChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQN.EdCIChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQN.EdPesoAddExit(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQN.CalculaSaldoFuturo;
var
  CI, PQ: Integer;
  PesoAdd, SaldoFut: Double;
begin
  CI := Geral.IMV(EdCI.Text);
  PQ := Geral.IMV(EdPQ.Text);
(*
  QrSaldo.Close;
  QrSaldo.Params[0].AsInteger := CI;
  QrSaldo.Params[1].AsInteger := PQ;
  UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSaldo, Dmod.MyDB, [
  'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO ',
  'FROM pqcli ',
  'WHERE CI=' + Geral.FF0(CI),
  'AND PQ=' + Geral.FF0(PQ),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  '']);
  //
  PesoAdd := Geral.DMV(EdPesoAdd.Text);
  SaldoFut := QrSaldoPeso.Value - PesoAdd;
  EdSaldoFut.Text := Geral.FFT(SaldoFut, 3, siNegativo);
end;

procedure TFmPQN.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmPQN.QrSaldoAfterOpen(DataSet: TDataSet);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQN.BtDesiste2Click(Sender: TObject);
begin
  AtualizaCusto;
  MostraEdicao(0, stLok, 0);
end;

procedure TFmPQN.BtConfirma2Click(Sender: TObject);
const
  Unitario = True;
var
  Controle: Integer;
  KG, RS, SF: Double;
  //
  DataX: String;
  OriCodi, OriCtrl, OriTipo, CliOrig, CliDest, Insumo, Empresa: Integer;
  Peso, Valor: Double;
  DtCorrApo: String;
begin
  if PnInsumos.Visible then
  begin
    DataX      := Geral.FDT(QrPQNDataB.Value, 1);
    OriCodi    := QrPQNCodigo.Value;
    OriTipo    := FThis_FATID;
    CliOrig    := QrSaldoCI.Value;
    CliDest    := Dmod.QrMasterDono.Value;
    Insumo     := EdPQ.ValueVariant;
    //
    KG := Geral.DMV(EdPesoAdd.Text);
    RS := KG * QrSaldoCUSTO.Value;
    Peso       := -KG;
    Valor      := -RS;
    DtCorrApo  := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
    //
    if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
    //
    if MyObjects.FIC(Insumo = 0, EdPQ, 'Informe o material!') then Exit;
    if MyObjects.FIC(QrSaldo.RecordCount = 0, EdPQ, 'Saldo zerado!') then Exit;
    SF := Geral.DMV(EdSaldoFut.Text);
    if MyObjects.FIC(SF < 0, EdPesoAdd, 'Saldo insuficiente!') then Exit;
    if MyObjects.FIC(KG <= 0, EdPesoAdd, 'Quantidade inv�lida!') then Exit;

    //
    Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'EmitIts',
      'EmitIts', 'Controle');
    //
    OriCtrl    := Controle;
    PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
      OriCtrl, OriTipo, Unitario, DtCorrApo, Empresa);
    //
    FPQTIts := Controle;
    UnPQx.AtualizaEstoquePQ(QrSaldoCI.Value, QrSaldoPQ.Value, Empresa, aeMsg, '');
    QrSaldo.Close;
    UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
    ReopenPQTIts;
    AtualizaCusto;
    EdPesoAdd.ValueVariant := 0.000;
    EdPQ.ValueVariant := 0;
    CBPQ.KeyValue     := NULL;
    //
    EdPQ.SetFocus;
  end;
end;

procedure TFmPQN.IncluiNovaBaixa1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmPQN.IncluiInsumobaixaatual1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmPQN.AtualizaCusto;
var
  CustoHora: Double;
begin
(*
  LocCod(QrPQNCodigo.Value, QrPQNCodigo.Value);
  CustoHora := QrPQNHorasTrab.Value * QrPQNValorHora.Value;
  //
  QrSumPQ.Close;
  QrSumPQ.Params[0].AsInteger := QrPQNCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSumPQ, Dmod.MyDB);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE pqn SET CustoHora=:P0, CustoInsumo=:P1, ');
  Dmod.QrUpd.SQL.Add('CustoTotal=:P2 WHERE Codigo=:Pa ');
  Dmod.QrUpd.Params[00].AsFloat   := CustoHora;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPQCUSTO.Value;
  Dmod.QrUpd.Params[02].AsFloat   := CustoHora + QrSumPQCUSTO.Value;
  Dmod.QrUpd.Params[03].AsInteger := QrPQNCodigo.Value;
  Dmod.QrUpd.ExecSQL;
*)
  LocCod(QrPQNCodigo.Value, QrPQNCodigo.Value);
end;

procedure TFmPQN.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmPQN.PMExcluiPopup(Sender: TObject);
begin
  ExcluiBaixa1.Enabled := not Geral.IntToBool_0(QrPQNIts.RecordCount);
  ExcluiItemdabaixa1.Enabled := Geral.IntToBool_0(QrPQNIts.RecordCount);
end;

procedure TFmPQN.ExcluiBaixa1Click(Sender: TObject);
begin
  // quando fazer cuidar controle de estoque por item de entrada de NFe!
  // Ver se d� para fazer como no PQO
end;

procedure TFmPQN.ExcluiItemdabaixa1Click(Sender: TObject);
var
  Atual, OriCodi, OriCtrl, OriTipo, Insumo, CliInt, Empresa: Integer;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQNDataB.Value) then Exit;
  // 2023-09-09
  if Application.MessageBox(PChar('Confirma a exclus�o do item selecionado '+
  'nesta baixa?'), 'Pergunta', MB_YESNO+MB_ICONQUESTION)= ID_YES then
  begin
    Insumo  := QrPQNItsInsumo.Value;
    CliInt  := QrPQNItsCliOrig.Value;
    Empresa := QrPQNItsEmpresa.Value;
    Atual   := QrPQNItsOrigemCtrl.Value;
    FPQTIts := UMyMod.ProximoRegistro(QrPQNIts, 'OrigemCtrl', Atual);
    OriCodi := QrPQNCodigo.Value;
    OriCtrl := Atual;
    OriTipo := FThis_FATID;
    PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
    //
    AtualizaCusto;
    UnPQx.AtualizaEstoquePQ(CliInt, Insumo, Empresa, aeMsg, '');
  end;
end;

procedure TFmPQN.AlteraBaixa1Click(Sender: TObject);
var
  PQT : Integer;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQNDataB.Value) then Exit;
  // 2023-09-09
  PQT := QrPQNCodigo.Value;
  if not UMyMod.SelLockY(PQT, Dmod.MyDB, 'pqn', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PQT, Dmod.MyDB, 'pqn', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPQN.Estabaixa1Click(Sender: TObject);
begin
  MyObjects.frxDefineDatasets(frxUSO_CONSU_003_A, [
  DModG.frxDSDono,
  frxDsPQI,
  frxDsPQIIts
  ]);
  MyObjects.frxMostra(frxUSO_CONSU_003_A, 'Baixa de Material para Manuten��o');
end;

procedure TFmPQN.Outros1Click(Sender: TObject);
begin
  PQ_PF.MostraFormPQImp();
end;

procedure TFmPQN.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPQN.frBaixaAtualUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if AnsiCompareText(Name, 'VFR_GRD') = 0 then Val := 15//Geral.BoolToInt2(CkGrade.Checked, 15, 0)
end;

procedure TFmPQN.frxUSO_CONSU_003_AGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VFR_GRD' then Value := 15 else
  if VarName = 'VARF_DATA' then Value := Now() else
  if VarName ='VARF_FUNCI_TERC' then
  begin
    if QrPQNTipoEnt.Value = 0 then
      Value := 'Empresa'
    else
      Value := 'Funcion�rio';
  end else
  //
end;

end.

