unit MPInRat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, DBGrids, DBCtrls,
  dmkEdit, Db, mySQLDbTables, Menus, Variants, dmkDBLookupComboBox, dmkEditCB,
  dmkLabel, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmMPInRat = class(TForm)
    Panel1: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    Label10: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    dmkEdCaminhao: TdmkEdit;
    dmkEdPecasNF: TdmkEdit;
    dmkEdPNF_Far: TdmkEdit;
    dmkEdPecas: TdmkEdit;
    dmkEdPLE: TdmkEdit;
    dmkEdPDA: TdmkEdit;
    BitBtn1: TBitBtn;
    DBGrid1: TDBGrid;
    QrProcede: TmySQLQuery;
    QrProcedeCodigo: TIntegerField;
    QrProcedeNOMEENTIDADE: TWideStringField;
    QrProcedePreDescarn: TSmallintField;
    QrProcedeMaskLetras: TWideStringField;
    QrProcedeMaskFormat: TWideStringField;
    QrProcedeCorretor: TIntegerField;
    QrProcedeComissao: TFloatField;
    QrProcedeDescAdiant: TFloatField;
    QrProcedeCondPagto: TWideStringField;
    QrProcedeCondComis: TWideStringField;
    QrProcedeLocalEntrg: TWideStringField;
    QrProcedeAbate: TIntegerField;
    QrProcedeTransporte: TIntegerField;
    DsFornece: TDataSource;
    QrMPInRat: TmySQLQuery;
    DsMPInRat: TDataSource;
    PnInsUpd: TPanel;
    EdProcede: TdmkEditCB;
    Label35: TLabel;
    CBProcede: TdmkDBLookupComboBox;
    EdFornece: TdmkEditCB;
    Label2: TLabel;
    CBFornece: TdmkDBLookupComboBox;
    DsProcede: TDataSource;
    Label3: TLabel;
    dmkEdPNF_Fat2: TdmkEdit;
    Label5: TLabel;
    dmkEdCusto: TdmkEdit;
    QrMPInRatProcedeCod: TIntegerField;
    QrMPInRatProcedeNom: TWideStringField;
    QrMPInRatPecas: TFloatField;
    QrMPInRatPecasNF: TFloatField;
    QrMPInRatPNF: TFloatField;
    QrMPInRatPLE: TFloatField;
    QrMPInRatPDA: TFloatField;
    QrMPInRatPreco: TFloatField;
    QrMPInRatCusto: TFloatField;
    dmkEdPreco: TdmkEdit;
    Label9: TLabel;
    mySQLQuery1: TmySQLQuery;
    QrProcedeCMPPreco: TFloatField;
    PMFornece: TPopupMenu;
    Entidades1: TMenuItem;
    FornecedoresdeMP1: TMenuItem;
    QrA: TmySQLQuery;
    QrB: TmySQLQuery;
    QrAProcedeCod: TIntegerField;
    QrAProcedeNom: TWideStringField;
    QrAPecas: TFloatField;
    QrAPecasNF: TFloatField;
    QrAPNF: TFloatField;
    QrAPLE: TFloatField;
    QrAPDA: TFloatField;
    QrAPreco: TFloatField;
    QrACusto: TFloatField;
    QrBProcedeCod: TIntegerField;
    QrBProcedeNom: TWideStringField;
    QrBPecas: TFloatField;
    QrBPecasNF: TFloatField;
    QrBPNF: TFloatField;
    QrBPLE: TFloatField;
    QrBPDA: TFloatField;
    QrBPreco: TFloatField;
    QrBCusto: TFloatField;
    QrAControle: TAutoIncField;
    QrBControle: TAutoIncField;
    QrMPInRatControle: TAutoIncField;
    dmkEdPNF: TdmkEdit;
    Label11: TLabel;
    QrMPInRatPNF_Fat: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBControla: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtRateia: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BtSaida: TBitBtn;
    GBConfirma: TGroupBox;
    Panelxxx: TPanel;
    Panel5: TPanel;
    BitBtn4: TBitBtn;
    BitBtn3: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdProcedeChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dmkEdPecasNFChange(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure QrMPInRatAfterOpen(DataSet: TDataSet);
    procedure Entidades1Click(Sender: TObject);
    procedure FornecedoresdeMP1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaCouroInfo;
    procedure MostraEdicaoRat(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure ReopenMPInRat(Codigo: Integer);
    procedure Rateia();
  public
    { Public declarations }
  end;

  var
  FmMPInRat: TFmMPInRat;

implementation

uses UnMyObjects, Module, UnInternalConsts, UMySQLModule, Principal, EntiMP, MyDBCheck,
  MPIn, ModuleGeral;

{$R *.DFM}

procedure TFmMPInRat.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPInRat.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPInRat.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPInRat.EdProcedeChange(Sender: TObject);
begin
  dmkEdPreco.ValueVariant := QrProcedeCMPPreco.Value;
  //ReopenMPInRat(QrMPInRatProcedeCod.Value);
end;

procedure TFmMPInRat.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrProcede, Dmod.MyDB);
  //
  (*
  QrMPInRat.Close;
  UnDmkDAC_PF.AbreQuery(QrMPInRat, DModG.MyPID_DB);
  *)
  //
  ReopenMPInRat(0);
  {
  dmkEdCaminhao.ValueVariant := 1;
  dmkEdPecas.ValueVariant    := 550;
  dmkEdPecasNF.ValueVariant  := 555;
  dmkEdPNF_Far.ValueVariant  := 17000;
  dmkEdPNF.ValueVariant      := 16967;
  dmkEdPLE.ValueVariant      := 16800;
  dmkEdPDA.ValueVariant      := 13569;
  EdFornece.Text             := '46';
  CBFornece.KeyValue         := 46;
  }
  EdFornece.Text             := IntToStr(FmMPIn.QrMPInProcedencia.Value);
  CBFornece.KeyValue         := FmMPIn.QrMPInProcedencia.Value;
  dmkEdCaminhao.ValueVariant := 0;
  dmkEdPecas.ValueVariant    := FmMPIn.QrMPInPecas.Value;
  dmkEdPecasNF.ValueVariant  := FmMPIn.QrMPInPecasNF.Value;
  //dmkEdPNF_Far.ValueVariant  := FmMPIn.QrMPInPNF_Fat.Value;
  dmkEdPNF.ValueVariant      := FmMPIn.QrMPInPNF.Value;
  dmkEdPLE.ValueVariant      := FmMPIn.QrMPInPLE.Value;
  dmkEdPDA.ValueVariant      := FmMPIn.QrMPInPDA.Value;
end;

procedure TFmMPInRat.dmkEdPecasNFChange(Sender: TObject);
begin
  CalculaCouroInfo;
end;

procedure TFmMPInRat.CalculaCouroInfo;
//var
  //Fator: Double;
  //Fonte: Integer;
begin
  (*
  Fonte := QrEntiMPCPLSit.Value;
  dmkEdCMPQuebra.ValueVariant := 0;
  dmkEdCPLQuebra.ValueVariant := 0;
  dmkEdAGIQuebra.ValueVariant := 0;
  dmkEdCMIQuebra.ValueVariant := 0;
  //
  dmkEdCMPValor.ValueVariant := CalculaCusto(QrEntiMPCMPUnida.Value,
    0,     CalculaPreco(QrEntiMPCMPPreco.Value, QrEntiMPCMPMoeda.Value));
  //
  dmkEdCPLValor.ValueVariant := CalculaCusto(QrEntiMPCPLUnida.Value,
    Fonte, CalculaPreco(QrEntiMPCPLPreco.Value, QrEntiMPCPLMoeda.Value));
  //
  dmkEdAGIValor.ValueVariant := CalculaCusto(QrEntiMPAGIUnida.Value,
    0,     CalculaPreco(QrEntiMPAGIPreco.Value, QrEntiMPAGIMoeda.Value));
  //
  dmkEdCMIValor.ValueVariant := CalculaCusto(QrEntiMPCMIUnida.Value,
    0,     CalculaPreco(QrEntiMPCMIPreco.Value, QrEntiMPCMIMoeda.Value));

  {  Devolu��o de quebra de viagem }
  // Calcula % de quebra de viagem
  if dmkEdPNF.ValueVariant = 0 then Fator := 0 else Fator := 100 *
    (dmkEdPNF.ValueVariant - dmkEdPLE.ValueVariant) / dmkEdPNF.ValueVariant;
  dmkEdQuebraViagm.ValueVariant := Fator;
  { calcula devolu��o conforme IDQV (Indice de devolu-
    ��o de quebra de viagem: Nada, tudo ou excedente}
  //  CMP
  if Fator > QrEntiMPCMP_LPQV.Value then
  begin
    case QrEntiMPCMP_IDQV.Value of
      0: Fator := 0;
      //1: Fator := Fator;
      2: Fator := Fator - QrEntiMPCMP_LPQV.Value;
    end;
    dmkEdCMPQuebra.ValueVariant := Fator;
    if Fator > 0 then
    //dmkEdDevolInfo.ValueVariant := Fator * dmkEdCustoInfo.ValueVariant / 100;
      dmkEdCMPDevol.ValueVariant := Fator * dmkEdCMPValor.ValueVariant / 100;
  end;
  //  CPL
  if Fator > QrEntiMPCPL_LPQV.Value then
  begin
    case QrEntiMPCPL_IDQV.Value of
      0: Fator := 0;
      //1: Fator := Fator;
      2: Fator := Fator - QrEntiMPCPL_LPQV.Value;
    end;
    dmkEdCPLQuebra.ValueVariant := Fator;
    if Fator > 0 then
    //dmkEdDevolInfo.ValueVariant := Fator * dmkEdCustoInfo.ValueVariant / 100;
      dmkEdCPLDevol.ValueVariant := Fator * dmkEdCPLValor.ValueVariant / 100;
  end;
  //
  //  AGI
  if Fator > QrEntiMPAGI_LPQV.Value then
  begin
    case QrEntiMPAGI_IDQV.Value of
      0: Fator := 0;
      //1: Fator := Fator;
      2: Fator := Fator - QrEntiMPAGI_LPQV.Value;
    end;
    dmkEdAGIQuebra.ValueVariant := Fator;
    if Fator > 0 then
    //dmkEdDevolInfo.ValueVariant := Fator * dmkEdCustoInfo.ValueVariant / 100;
      dmkEdAGIDevol.ValueVariant := Fator * dmkEdAGIValor.ValueVariant / 100;
  end;
  //  CMI
  if Fator > QrEntiMPCMI_LPQV.Value then
  begin
    case QrEntiMPCMI_IDQV.Value of
      0: Fator := 0;
      //1: Fator := Fator;
      2: Fator := Fator - QrEntiMPCMI_LPQV.Value;
    end;
    dmkEdCMIQuebra.ValueVariant := Fator;
    if Fator > 0 then
    //dmkEdDevolInfo.ValueVariant := Fator * dmkEdCustoInfo.ValueVariant / 100;
      dmkEdCMIDevol.ValueVariant := Fator * dmkEdCMIValor.ValueVariant / 100;
  end;
  CalculaTotalInfo;
  *)
end;

procedure TFmMPInRat.BitBtn2Click(Sender: TObject);
begin
  MostraEdicaoRat(1, stIns, 0);
end;

procedure TFmMPInRat.MostraEdicaoRat(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      Rateia();
      //DBGrid1.Enabled      := True;
      GBControla.Visible   := True;
      GBConfirma.Visible   := False;
      PnInsUpd.Visible     := False;
    end;
    1:
    begin
      GBConfirma.Visible := True;
      PnInsUpd.Visible   := True;
      GBControla.Visible := False;
      if SQLType = stIns then
      begin
        EdProcede.Text              := '';
        CBProcede.KeyValue          := NULL;
        //
        dmkEdPNF_Fat2.ValueVariant  := 0;
        dmkEdCusto.ValueVariant     := 0;
        dmkEdPreco.ValueVariant     := 0;
        //
      end else begin
        EdProcede.Text              := Geral.FF0(QrMPInRatProcedeCod.Value);
        CBProcede.KeyValue          := QrMPInRatProcedeCod.Value;
        //
        dmkEdPNF_Fat2.ValueVariant  := QrMPInRatPNF.Value;
        dmkEdCusto.ValueVariant     := QrMPInRatCusto.Value;
        dmkEdPreco.ValueVariant     := QrMPInRatPreco.Value;
        //
      end;
      EdProcede.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  //GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo > 0 then ReopenMPInRat(Codigo)
end;

procedure TFmMPInRat.BitBtn4Click(Sender: TObject);
var
  Fornece: Integer;
  x: TSQLType;
begin
  Fornece := Geral.IMV(EdProcede.Text);
  if Fornece = 0 then
  begin
    Application.MessageBox('Informe o fornecedor!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //UMyMod.BuscaEmLivreY_Def(Table, Field, LaTipo: String; Atual: Integer): Integer;
  if UMyMod.SQLInsUpdL((*Dmod.QrUpdL*)DModG.QrUpdPID1, DmkEnums.NomeTipoSQL(ImgTipo.SQLType), FmMPIn.FTabLoc_ntrttMPInRat, True, [
    (*'Pecas', 'PecasNF',*) 'PNF_Fat',
    (*'PLE', 'PDA', 'Preco',*) 'Custo', 'Preco',
    'ProcedeNom', 'ProcedeCod'], ['Controle'], [
    dmkEdPNF_Fat2.ValueVariant, dmkEdCusto.ValueVariant,
    dmkEdpreco.ValueVariant, CBProcede.Text, Fornece
    ], [QrMPInRatControle.Value]) then
    begin
     // Antes de abrir para calcular correto
     //Rateia();
     x := ImgTipo.SQLType;
     MostraEdicaoRat(0, stLok, Fornece);
     if x = stIns then
       MostraEdicaoRat(1, stIns, 0);
    end;
end;

procedure TFmMPInRat.ReopenMPInRat(Codigo: Integer);
begin
  (*
  QrMPInRat.Close;
  UnDmkDAC_PF.AbreQuery(QrMPInRat, DModG.MyPID_DB);
  *)
  UnDmkDAC_PF.AbreMySQLQuery0(QrMPInRat, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + FmMPIn.FTabLoc_ntrttMPInRat,
    'ORDER BY Controle ',
    '']);
  //
  if Codigo <> 0 then QrMPInRat.Locate('ProcedeCod', Codigo, []);
end;

procedure TFmMPInRat.BitBtn5Click(Sender: TObject);
begin
  MostraEdicaoRat(1, stUpd, 0);
end;

procedure TFmMPInRat.QrMPInRatAfterOpen(DataSet: TDataSet);
var
  Codigo: Integer;
begin
  Codigo := Geral.IMV(EdFornece.Text);
  if Codigo = 0 then BtRateia.Enabled := False else
    BtRateia.Enabled := QrMPInRat.Locate('ProcedeCod', Codigo, []);
end;

procedure TFmMPInRat.Entidades1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  QrProcede.Close;
  UnDmkDAC_PF.AbreQuery(QrProcede, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdProcede.Text := IntToStr(VAR_ENTIDADE);
    CBProcede.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmMPInRat.FornecedoresdeMP1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiMP, FmEntiMP, afmoNegarComAviso) then
  begin
    FmEntiMP.ShowModal;
    FmEntiMP.Destroy;
  end;
  QrProcede.Close;
  UnDmkDAC_PF.AbreQuery(QrProcede, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdProcede.Text := IntToStr(VAR_ENTIDADE);
    CBProcede.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmMPInRat.BitBtn1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFornece, BitBtn1);
end;

procedure TFmMPInRat.BitBtn6Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a retirada do fornecedor selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Clear;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('DELETE FROM ' + FmMPIn.FTabLoc_ntrttMPInRat);
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('WHERE Controle=:P0');
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('');
    //
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[0].AsInteger := QrMPInRatControle.Value;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.ExecSQL;
  end;
  Rateia();
  //
  ReopenMPInRat(0);
end;

procedure TFmMPInRat.BitBtn3Click(Sender: TObject);
begin
  MostraEdicaoRat(0, stLok, 0);
end;

procedure TFmMPInRat.Rateia();
var
  Codigo, ProcedeCod, CNFt, CLEt, CNFs, CNFi, i: Integer;
  PNFt, PLEt, PDAt, PLEi, PDAi, kgPNF, kgPLE, kgPDA, PNFi, PNFs, PLEs, PDAs: Double;
begin
  kgPNF := 0;
  kgPLE := 0;
  kgPDA := 0;
  PNFs := 0;
  PLEs := 0;
  PDAs := 0;
  Codigo := Geral.IMV(EdFornece.Text);
  ProcedeCod := QrMPInRatProcedeCod.Value;
  ReopenMPInRat(0);
  CNFt := dmkEdPecasNF.ValueVariant;
  CLEt := dmkEdPecas.ValueVariant;
  PNFt := dmkEdPNF.ValueVariant;
  PLEt := dmkEdPLE.ValueVariant;
  PDAt := dmkEdPDA.ValueVariant;
  //
  if CNFt = 0 then CNFt := 0 else kgPNF := PNFt / CNFt;
  if CNFt = 0 then CNFt := 0 else kgPLE := PLEt / CNFt;
  if CNFt = 0 then CNFt := 0 else kgPDA := PDAt / CNFt;
  //
  CNFs := 0;
  //CLEs := 0;
  //
  (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Clear;
  (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('UPDATE ' + FmMPIn.FTabLoc_ntrttMPInRat + ' SET ');
  (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('PecasNF=:P0, Pecas=:P1, PLE=:P2, PDA=:P3, ');
  (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('PNF=:P4 ');
  (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('WHERE Controle=:Pa');
  //
  QrA.Close;
  QrA.Params[0].AsInteger := Codigo;
  UnDmkDAC_PF.AbreQuery(QrA, DModG.MyPID_DB);
  //
  QrA.First;
  while not QrA.Eof do
  begin
    if QrAPreco.Value = 0 then CNFi := 0 else
      CNFi := Round(QrACusto.Value / QrAPreco.Value);
    PNFi := Round(kgPNF * CNFi);
    PLEi := Round(kgPLE * CNFi);
    PDAi := Round(kgPDA * CNFi);
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[00].AsFloat   := CNFi;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[01].AsFloat   := CNFi;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[02].AsFloat   := PLEi;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[03].AsFloat   := PDAi;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[04].AsFloat   := PNFi;
    //
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[05].AsInteger := QrAControle.Value;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.ExecSQL;
    //
    CNFs := CNFs + CNFi;
    //CLEs := CLEs + CLEi;
    //PNFs := PNFs + PNFi;
    //PLEs := PLEs + PLEi;
    //PDAs := PDAs + PDAi;
    QrA.Next;
  end;
  //
  QrB.Close;
  QrB.Params[0].AsInteger := Codigo;
  UnDmkDAC_PF.AbreQuery(QrB, DModG.MyPID_DB);
  i := 0;
  QrB.First;
  while not QrB.Eof do
  begin
    inc(i, 1);
    if i < QrB.RecordCount then
    begin
      if QrBPreco.Value = 0 then CNFi := 0 else
        CNFi := Round(QrBCusto.Value / QrBPreco.Value);
      PNFi := Round(kgPNF * CNFi);
      PLEi := Round(kgPLE * CNFi);
      PDAi := Round(kgPDA * CNFi);
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[00].AsFloat   := CNFi;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[01].AsFloat   := CNFi;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[02].AsFloat   := PLEi;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[03].AsFloat   := PDAi;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[04].AsFloat   := PNFi;
      //
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[05].AsInteger := QrBControle.Value;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.ExecSQL;
      //
      CNFs := CNFs + CNFi;
      //CLEs := CLEs + CLEi;
      PNFs := PNFs + PNFi;
      PLEs := PLEs + PLEi;
      PDAs := PDAs + PDAi;
    end else begin
      //
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[00].AsFloat   := CNFt - CNFs;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[01].AsFloat   := CLEt - CNFs;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[02].AsFloat   := PLEt - PLEs;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[03].AsFloat   := PDAt - PDAs;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[04].AsFloat   := PNFt - PNFs;
      //
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[05].AsInteger := QrBControle.Value;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.ExecSQL;
      //
    end;
    QrB.Next;
  end;
  ReopenMPInRat(ProcedeCod);
  //
  //end;
end;

end.

