unit PQB3AdEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnInternalConsts,
  UnMsgInt, ExtCtrls, UnInternalConsts2, mySQLDbTables, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, UMySQLModule, dmkGeral, dmkLabel, dmkImage, UnDmkEnums,
  UnDmkProcFunc, DmkDAC_PF, Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmPQB3AdEdit = class(TForm)
    PainelDados: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Qry: TMySQLQuery;
    Panel3: TPanel;
    Label2: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EdEmpresa: TdmkEdit;
    EdNO_Empresa: TdmkEdit;
    EdCliInt: TdmkEdit;
    EdNO_CliInt: TdmkEdit;
    EdInsumo: TdmkEdit;
    EdNO_Insumo: TdmkEdit;
    Panel2: TPanel;
    Edide_serie: TdmkEdit;
    Label5: TLabel;
    TPdVal: TdmkEditDateTimePicker;
    TPdFab: TdmkEditDateTimePicker;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    EdxLote: TdmkEdit;
    Label12: TLabel;
    Edide_nNF: TdmkEdit;
    Label1: TLabel;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //FBalanco, FControle, FTipo: Integer;
    // Retornos
    FConfirmou: Boolean;
    FxLote, FdFab, FdVal: String;
    Fide_serie, Fide_nNF: Integer;
  end;

var
  FmPQB3AdEdit: TFmPQB3AdEdit;

implementation

uses UnMyObjects, Module, PQB3, PQB3AdSel, PQx, UnPQ_PF, ModuleGeral, MyDBCheck;

{$R *.DFM}


procedure TFmPQB3AdEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQB3AdEdit.BtConfirmaClick(Sender: TObject);
var
  Controle, Tipo, Serie, NF: Integer;
  SQLType: TSQLType;
  OriCodi, OriCtrl, OriTipo: Integer;
begin
  (*
  SQLType        := ImgTipo.SQLType;
  OriCodi        := FBalanco;
  Controle       := FControle;
  OriCtrl        := Controle;
  OriTipo        := FTipo;
  *)
  Fide_serie     := Edide_serie.ValueVariant;
  Fide_nNF       := Edide_nNF.ValueVariant;
  FxLote         := EdxLote.Text;
  FdFab          := Geral.FDT(TPdFab.Date, 1);
  FdVal          := Geral.FDT(TPdVal.Date, 1);
  (*
  Tipo           := FTipo;
  Serie          := ide_serie;
  NF             := ide_nNF;
  *)//
  if MyObjects.FIC(Fide_nNF = 0, Edide_nNF, 'Informe a NF (VP)!') then Exit;
  if MyObjects.FIC(FxLote = EmptyStr, EdxLote, 'Informe o Lote!') then Exit;
  //
(*
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqx', False, [
    'NF', 'xLote', 'Serie'], [
    'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
    NF, xLote, Serie], [
    OriCodi, OriCtrl, Tipo], False) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqbadx', False, [
      'ide_serie', 'ide_nNF',
      'xLote', 'dFab', 'dVal'], [
      'Controle'], [
      ide_serie, ide_nNF,
      xLote, dFab, dVal], [
      Controle], True) then
      begin
        //
      end;
    end;
  except
    Screen.Cursor := Cursor;
    Exit;
  end;
*)
  FConfirmou := True;
  Close;
end;

procedure TFmPQB3AdEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Edide_serie.SetFocus;
end;

procedure TFmPQB3AdEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQB3AdEdit.SpeedButton1Click(Sender: TObject);
var
  Filial, Empresa, CliInt, Insumo: Integer;
begin
  Filial  := EdEmpresa.ValueVariant;
  Empresa := DModG.ObtemEntidadeDeFilial(Filial);
  CliInt  := EdCliInt.ValueVariant;
  Insumo  := EdInsumo.ValueVariant;
  //
  if DBCheck.CriaFm(TFmPQB3AdSel, FmPQB3AdSel, afmoNegarComAviso) then
  begin
    FmPQB3AdSel.EdEmpresa.ValueVariant := Empresa;
    FmPQB3AdSel.EdNO_Empresa.Text      := EdNO_Empresa.Text;
    FmPQB3AdSel.EdCliInt.ValueVariant  := CliInt;
    FmPQB3AdSel.EdNO_CliInt.Text       := EdNO_CliInt.Text;
    FmPQB3AdSel.EdInsumo.ValueVariant  := Insumo;
    FmPQB3AdSel.EdNO_Insumo.Text       := EdNO_Insumo.Text;
    //
    FmPQB3AdSel.ReopenPqx(Empresa, CliInt, Insumo);
    //
    FmPQB3AdSel.ShowModal;
    //
    if FmPQB3AdSel.FSelecionou = True then
    begin
      Edide_serie.ValueVariant := FmPQB3AdSel.QrPqxSerie.Value;
      Edide_nNF.ValueVariant   := FmPQB3AdSel.QrPqxNF.Value;
      EdxLote.ValueVariant     := FmPQB3AdSel.QrPqxxLote.Value;
      TPdFab.Date              := FmPQB3AdSel.QrPqxdFab.Value;
      TPdVal.Date              := FmPQB3AdSel.QrPqxdVal.Value;
    end;
    //
    FmPQB3AdSel.Destroy;
  end;
end;

procedure TFmPQB3AdEdit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TPdFab.Date := 0;
  TPdVal.Date := 0;
  //
  FConfirmou := False;
  FxLote     := '';
  FdFab      := '0000-00-00';
  FdVal      := '0000-00-00';
  Fide_serie := -1;
  Fide_nNF   := -1;
end;

end.
