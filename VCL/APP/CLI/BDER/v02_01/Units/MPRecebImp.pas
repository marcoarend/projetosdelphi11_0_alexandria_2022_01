unit MPRecebImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, frxClass, UnDmkEnums;

type
  TFmMPRecebImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    frxIMP_MPINN_001_001_A: TfrxReport;
    QrClientesI: TmySQLQuery;
    QrClientesINOMECLIENTEI: TWideStringField;
    QrClientesICodigo: TIntegerField;
    DsClientesI: TDataSource;
    QrProcedencias: TmySQLQuery;
    QrProcedenciasCodigo: TIntegerField;
    QrProcedenciasNOMECLIENTEI: TWideStringField;
    DsProcedencias: TDataSource;
    Panel5: TPanel;
    Label18: TLabel;
    EdClienteI: TdmkEditCB;
    CBClienteI: TdmkDBLookupComboBox;
    Label11: TLabel;
    EdProcedencia: TdmkEditCB;
    CBProcedencia: TdmkDBLookupComboBox;
    EdOS: TdmkEdit;
    Label10: TLabel;
    Label19: TLabel;
    EdNF: TdmkEdit;
    Label12: TLabel;
    EdLote: TEdit;
    Label13: TLabel;
    EdMarca: TEdit;
    Label1: TLabel;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label3: TLabel;
    EdQtdB: TdmkEdit;
    Label4: TLabel;
    EdQtdV: TdmkEdit;
    Label5: TLabel;
    EdQtdD: TdmkEdit;
    Label6: TLabel;
    EdQtdT: TdmkEdit;
    Label7: TLabel;
    EdCaminhao: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxIMP_MPINN_001_001_AGetValue(const VarName: string;
      var Value: Variant);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmMPRecebImp: TFmMPRecebImp;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmMPRecebImp.BtOKClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxIMP_MPINN_001_001_A, [DModG.frxDsDono]);
  MyObjects.frxMostra(frxIMP_MPINN_001_001_A, 'Recebimento de Mat�ria-Prima');
end;

procedure TFmMPRecebImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPRecebImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPRecebImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPIni.Date := Date;
  TPFim.Date := Date;
  UnDmkDAC_PF.AbreQuery(QrClientesI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcedencias, Dmod.MyDB);
  //
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
end;

procedure TFmMPRecebImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPRecebImp.frxIMP_MPINN_001_001_AGetValue(const VarName: string;
  var Value: Variant);
  function DT(Condicao: Boolean; Texto: String): String;
  begin
    if Condicao then
      Result := Texto
    else
      Result := ' ';
  end;
begin
  if VarName = 'VARF_CLI_INT' then
    Value := DT(EdClienteI.ValueVariant <> 0, CBClienteI.Text)
  else
  if VarName = 'VARF_FORNECE' then
    Value := DT(EdProcedencia.ValueVariant <> 0, CBProcedencia.Text)
  else
  if VarName = 'VARF_MARCA' then
    Value := DT(EdCaminhao.ValueVariant > 0, EdCaminhao.Text + '�  ') + EdMarca.Text
  else
  if VarName = 'VARF_DATAF' then
    Value := DT(TPFIm.Date > 2, Geral.FDT(TPFim.Date, 2))
  else
  if VarName = 'VARF_DATAI' then
    Value := DT(TPIni.Date > 2, Geral.FDT(TPIni.Date, 2))
  else
  if VarName = 'VARF_QTD_B' then
    Value := DT(EdQtdB.ValueVariant > 0, EdQtdB.Text)
  else
  if VarName = 'VARF_QTD_V' then
    Value := DT(EdQtdV.ValueVariant > 0, EdQtdV.Text)
  else
  if VarName = 'VARF_QTD_D' then
    Value := DT(EdQtdD.ValueVariant > 0, EdQtdD.Text)
  else
  if VarName = 'VARF_QTD_T' then
    Value := DT(EdQtdT.ValueVariant > 0, EdQtdT.Text)
  else
  if VarName = 'VARF_OS' then
    Value := DT(EdOS.ValueVariant > 0, EdOS.Text)
  else
  if VarName = 'VARF_LOTE' then
    Value := EdLote.Text
  else
  if VarName = 'VARF_NF' then
    Value := DT(EdNF.ValueVariant > 0, EdNF.Text)
  else
  if VarName = 'PictureTitL1_Existe' then
    Value := FileExists(DModG.QrPrmsEmpNFePathLogoNF.Value)
  else
  if VarName = 'PictureTitL1_Load' then
    Value := DModG.QrPrmsEmpNFePathLogoNF.Value
end;

end.
