unit AppFormularios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass;

type
  TFmAppFormularios = class(TForm)
    frxSolidos: TfrxReport;
    frxControles: TfrxReport;
    procedure frxSolidosGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ImprimeMonitoramentoSolidos();
    procedure ImprimeControlesDiarios();
  end;

var
  FmAppFormularios: TFmAppFormularios;

implementation

uses ModuleGeral, UnMyObjects;

{$R *.dfm}

procedure TFmAppFormularios.frxSolidosGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_NO_EMPRESA') = 0 then
    Value := DModG.QrEmpresasNOMEFILIAL.Value
end;

procedure TFmAppFormularios.ImprimeControlesDiarios();
begin
  MyObjects.frxDefineDataSets(frxControles, []);
  MyObjects.frxMostra(frxControles, 'Lista pesagem');
end;

procedure TFmAppFormularios.ImprimeMonitoramentoSolidos();
begin
  MyObjects.frxDefineDataSets(frxSolidos, []);
  MyObjects.frxMostra(frxSolidos, 'Lista pesagem');
end;

end.
