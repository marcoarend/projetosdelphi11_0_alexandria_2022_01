unit WBPalFrn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmWBPalFrn = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNomeEmp: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    CBFornece: TdmkDBLookupComboBox;
    EdFornece: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTIDADE: TWideStringField;
    DsFornece: TDataSource;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    DBEdControle: TdmkDBEdit;
    DBEdNomeArt: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenWBPalFrn(Conta: Integer);
  public
    { Public declarations }
    FQrCab, FQrArt, FQrFrn: TmySQLQuery;
    FDsCab, FDsArt: TDataSource;
  end;

  var
  FmWBPalFrn: TFmWBPalFrn;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmWBPalFrn.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, Fornece: Integer;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := Geral.IMV(DBEdControle.Text);
  Conta          := EdConta.ValueVariant;
  Fornece        := EdFornece.ValueVariant;
  //
  if MyObjects.FIC(Fornece = 0, EdFornece, 'Informe uma mat�ria-prima!') then
    Exit;
  Conta := UMyMod.BPGS1I32('wbpalfrn', 'Conta', '', '', tsPos,
    ImgTipo.SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'wbpalfrn', False, [
  'Codigo', 'Controle', 'Fornece'], [
  'Conta'], [
  Codigo, Controle, Fornece], [
  Conta], True) then
  begin
    ReopenWBPalFrn(Conta);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdConta.ValueVariant     := 0;
      EdFornece.ValueVariant   := 0;
      CBFornece.KeyValue       := Null;
    end else Close;
  end;
end;

procedure TFmWBPalFrn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWBPalFrn.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNomeEMp.DataSource := FDsCab;
  //
  DBEdControle.DataSource := FDsArt;
  DBEdNomeArt.DataSource := FDsArt;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmWBPalFrn.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
end;

procedure TFmWBPalFrn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBPalFrn.ReopenWBPalFrn(Conta: Integer);
begin
  if FQrFrn <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrFrn, FQrFrn.DataBase);
    //
    if Conta <> 0 then
      FQrFrn.Locate('Conta', Conta, []);
  end;
end;

end.
