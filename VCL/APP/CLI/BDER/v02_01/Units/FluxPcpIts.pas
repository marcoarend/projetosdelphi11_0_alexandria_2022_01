unit FluxPcpIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFluxPcpIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBIniStg: TdmkDBLookupComboBox;
    EdIniStg: TdmkEditCB;
    Label1: TLabel;
    SbIniStg: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrFluxPcpStgIni: TMySQLQuery;
    DsFluxPcpStgIni: TDataSource;
    EdDiaSeq: TdmkEdit;
    Label2: TLabel;
    CBFimStg: TdmkDBLookupComboBox;
    SbFimStg: TSpeedButton;
    EdFimStg: TdmkEditCB;
    Label4: TLabel;
    QrFluxPcpStgIniCodigo: TIntegerField;
    QrFluxPcpStgIniNome: TWideStringField;
    QrFluxPcpStgFim: TMySQLQuery;
    QrFluxPcpStgFimCodigo: TIntegerField;
    QrFluxPcpStgFimNome: TWideStringField;
    DsFluxPcpStgFim: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbIniStgClick(Sender: TObject);
    procedure SbFimStgClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFluxPcpIts: TFmFluxPcpIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnApp_Jan, FluxPcpCab;

{$R *.DFM}

procedure TFmFluxPcpIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, DiaSeq, IniStg, FimStg: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  DiaSeq         := EdDiaSeq.ValueVariant;
  IniStg         := EdIniStg.ValueVariant;
  FimStg         := EdFimStg.ValueVariant;
  if MyObjects.FIC(IniStg = 0, EdIniStg, 'Informe o est�gio inicial do dia!') then
    Exit;
  if MyObjects.FIC(FimStg = 0, EdIniStg, 'Informe o est�gio final do dia!') then
    Exit;

  //
  Controle := UMyMod.BPGS1I32('fluxpcpits', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'fluxpcpits', False, [
  'Codigo', 'DiaSeq', 'IniStg',
  'FimStg'], [
  'Controle'], [
  Codigo, DiaSeq, IniStg,
  FimStg], [
  Controle], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    FmFluxPcpCab.ReordenaItens(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdDiaSeq.ValueVariant    := EdDiaSeq.ValueVariant + 1;
      //EdIniStg.ValueVariant    := 0;  // pode ficar varios dias no mesmo setor!
      //CBIniStg.KeyValue        := 0;  // pode ficar varios dias no mesmo setor!
      //EdFimStg.ValueVariant    := 0;  // pode ficar varios dias no mesmo setor!
      //CBFimStg.KeyValue        := 0;  // pode ficar varios dias no mesmo setor!
      //
      EdDiaSeq.SetFocus;
    end else Close;
  end;
end;

procedure TFmFluxPcpIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxPcpIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFluxPcpIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrFluxPcpStgIni, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFluxPcpStgFim, Dmod.MyDB);
end;

procedure TFmFluxPcpIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFluxPcpIts.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmFluxPcpIts.SbFimStgClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormFluxPcpStg();
  UMyMod.SetaCodigoPesquisado(EdFimStg, CBFimStg, QrFluxPcpStgFim, VAR_CADASTRO);
end;

procedure TFmFluxPcpIts.SbIniStgClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormFluxPcpStg();
  UMyMod.SetaCodigoPesquisado(EdIniStg, CBIniStg, QrFluxPcpStgIni, VAR_CADASTRO);
end;

end.
