unit EmitGruFim;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkEditCalc;

type
  TFmEmitGruFim = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label33: TLabel;
    EdEncerrado: TdmkEdit;
    EdUSD_BRL: TdmkEdit;
    Label1: TLabel;
    EdAreaManM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    EdAreaManP2: TdmkEditCalc;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmEmitGruFim: TFmEmitGruFim;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule;

{$R *.DFM}

procedure TFmEmitGruFim.BtOKClick(Sender: TObject);
var
  Encerrado: String;
  Codigo: Integer;
  USD_BRL, AreaManM2, AreaManP2, AreaIniM2, AreaIniP2, AreaAutM2, AreaAutP2: Double;
begin
  Codigo         := FCodigo;
  Encerrado      := Geral.FDT(EdEncerrado.ValueVariant, 109);
  USD_BRL        := EdUSD_BRL.ValueVariant;
  Encerrado      := Geral.FDT(EdEncerrado.ValueVariant, 109);
  AreaManM2      := EdAreaManM2.ValueVariant;
  AreaManP2      := EdAreaManP2.ValueVariant;
  // � complicado gerenciar se fizer fixo. � melhor dinamico!
  AreaIniM2      := 0;
  AreaIniP2      := 0;
  AreaAutM2      := 0;
  AreaAutP2      := 0;

  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'emitgru', False, [
  'Encerrado', 'USD_BRL',
  'AreaManM2', 'AreaManP2', 'AreaIniM2',
  'AreaIniP2', 'AreaAutM2', 'AreaAutP2'], [
  'Codigo'], [
  Encerrado, USD_BRL,
  AreaManM2, AreaManP2, AreaIniM2,
  AreaIniP2, AreaAutM2, AreaAutP2], [
  Codigo], True) then
  begin
    //  Precisa faze?
    //DMod.AualizaTotaiEmiGru(Codigo);
    Close;
  end;
end;

procedure TFmEmitGruFim.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmitGruFim.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmitGruFim.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  EdEncerrado.ValueVariant  := DModG.ObtemAgora();
end;

procedure TFmEmitGruFim.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
