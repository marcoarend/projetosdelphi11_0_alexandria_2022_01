object FmPQB3CiIts: TFmPQB3CiIts
  Left = 426
  Top = 332
  Caption = 'QUI-BALAN-012 :: Ciclo de Balan'#231'o - Item'
  ClientHeight = 494
  ClientWidth = 662
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 662
    Height = 341
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnDadosAdx: TPanel
      Left = 48
      Top = 224
      Width = 482
      Height = 44
      TabOrder = 2
      Visible = False
      object Label1: TLabel
        Left = 16
        Top = 4
        Width = 76
        Height = 13
        Caption = 'S'#233'rie e NF (VP):'
      end
      object Label12: TLabel
        Left = 126
        Top = 4
        Width = 24
        Height = 13
        Caption = 'Lote:'
      end
      object SpeedButton1: TSpeedButton
        Left = 280
        Top = 20
        Width = 23
        Height = 22
        Caption = '?'
        OnClick = SpeedButton1Click
      end
      object Label3: TLabel
        Left = 304
        Top = 4
        Width = 94
        Height = 13
        Caption = 'Data de fabrica'#231#227'o:'
      end
      object Label5: TLabel
        Left = 408
        Top = 4
        Width = 84
        Height = 13
        Caption = 'Data de validade:'
      end
      object EdSerie: TdmkEdit
        Left = 16
        Top = 20
        Width = 37
        Height = 21
        Alignment = taRightJustify
        MaxLength = 9
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNF: TdmkEdit
        Left = 56
        Top = 20
        Width = 66
        Height = 21
        Alignment = taRightJustify
        MaxLength = 9
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdxLote: TdmkEdit
        Left = 125
        Top = 20
        Width = 152
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object TPdFab: TdmkEditDateTimePicker
        Left = 304
        Top = 20
        Width = 100
        Height = 21
        Date = 45219.000000000000000000
        Time = 0.383424131941865200
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPdVal: TdmkEditDateTimePicker
        Left = 408
        Top = 20
        Width = 100
        Height = 21
        Date = 45219.000000000000000000
        Time = 0.383424131941865200
        TabOrder = 4
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 662
      Height = 121
      Align = alTop
      Enabled = False
      TabOrder = 0
      object LaEmpresa: TLabel
        Left = 16
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object LaCliente: TLabel
        Left = 16
        Top = 40
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object LaInsumo: TLabel
        Left = 16
        Top = 80
        Width = 37
        Height = 13
        Caption = 'Insumo:'
      end
      object Label9: TLabel
        Left = 476
        Top = 0
        Width = 24
        Height = 13
        Caption = 'Tipo:'
      end
      object Label11: TLabel
        Left = 476
        Top = 40
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label13: TLabel
        Left = 476
        Top = 80
        Width = 42
        Height = 13
        Caption = 'Controle:'
      end
      object EdTipo: TdmkEdit
        Left = 476
        Top = 16
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdDstCodi: TdmkEdit
        Left = 476
        Top = 56
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdDstCtrl: TdmkEdit
        Left = 476
        Top = 96
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEdit
        Left = 16
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdEmpresaRedefinido
      end
      object EdCliInt: TdmkEdit
        Left = 16
        Top = 56
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCliIntChange
      end
      object EdInsumo: TdmkEdit
        Left = 16
        Top = 96
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdInsumoChange
      end
      object EdNO_Empresa: TdmkEdit
        Left = 72
        Top = 16
        Width = 402
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNO_CliInt: TdmkEdit
        Left = 72
        Top = 56
        Width = 402
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNO_Insumo: TdmkEdit
        Left = 72
        Top = 96
        Width = 402
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 121
      Width = 662
      Height = 84
      Align = alTop
      TabOrder = 1
      object Label10: TLabel
        Left = 108
        Top = 0
        Width = 67
        Height = 13
        Caption = 'Custo unit'#225'rio:'
        Enabled = False
      end
      object Label4: TLabel
        Left = 16
        Top = 40
        Width = 75
        Height = 13
        Caption = 'Qtde verificada:'
      end
      object Label2: TLabel
        Left = 16
        Top = 0
        Width = 71
        Height = 13
        Caption = 'Qtde escritural:'
        Enabled = False
      end
      object Label7: TLabel
        Left = 224
        Top = 40
        Width = 75
        Height = 13
        Caption = 'Qtde Diferen'#231'a:'
      end
      object LaValorDife: TLabel
        Left = 316
        Top = 40
        Width = 77
        Height = 13
        Caption = 'Custo diferen'#231'a:'
        Enabled = False
      end
      object LaValorInfo: TLabel
        Left = 108
        Top = 40
        Width = 79
        Height = 13
        Caption = 'Custo verificado:'
        Enabled = False
      end
      object Label14: TLabel
        Left = 196
        Top = 0
        Width = 75
        Height = 13
        Caption = 'Custo escritural:'
        Enabled = False
      end
      object SbValorInfo: TSpeedButton
        Left = 198
        Top = 56
        Width = 23
        Height = 22
        Caption = 'i'
        OnClick = SbValorInfoClick
      end
      object SbValorDife: TSpeedButton
        Left = 406
        Top = 56
        Width = 23
        Height = 22
        Caption = 'i'
        OnClick = SbValorDifeClick
      end
      object EdPesoInfo: TdmkEdit
        Left = 16
        Top = 56
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdPesoInfoExit
      end
      object EdPreco: TdmkEdit
        Left = 108
        Top = 16
        Width = 86
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdPrecoExit
      end
      object CkLancar: TdmkCheckBox
        Left = 436
        Top = 60
        Width = 161
        Height = 17
        Caption = 'Inserir diferen'#231'a no estoque.'
        Enabled = False
        TabOrder = 6
        OnClick = CkLancarClick
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object EdPesoAtuT: TdmkEdit
        Left = 16
        Top = 16
        Width = 90
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPesoDife: TdmkEdit
        Left = 224
        Top = 56
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdPesoDifeExit
        OnRedefinido = EdPesoDifeRedefinido
      end
      object EdValorDife: TdmkEdit
        Left = 316
        Top = 56
        Width = 90
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnEnter = EdValorDifeEnter
        OnExit = EdValorDifeExit
      end
      object EdValorInfo: TdmkEdit
        Left = 108
        Top = 56
        Width = 90
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnEnter = EdValorInfoEnter
        OnExit = EdValorInfoExit
      end
      object EdValorAtuT: TdmkEdit
        Left = 196
        Top = 16
        Width = 90
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object PnDadosBxa: TPanel
      Left = 52
      Top = 280
      Width = 481
      Height = 61
      TabOrder = 3
      Visible = False
      object Label17: TLabel
        Left = 8
        Top = 4
        Width = 98
        Height = 13
        Caption = 'Estoque quantidade:'
        FocusControl = DBEdit7
      end
      object Label18: TLabel
        Left = 116
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Estoque $:'
        FocusControl = DBEdit8
      end
      object Label19: TLabel
        Left = 224
        Top = 4
        Width = 76
        Height = 13
        Caption = 'Custo unit'#225'rio $:'
        FocusControl = DBEdit9
      end
      object Label20: TLabel
        Left = 360
        Top = 4
        Width = 93
        Height = 13
        Caption = 'Saldo futuro quant.:'
      end
      object DBEdit7: TDBEdit
        Left = 8
        Top = 20
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'Peso'
        DataSource = DsSaldo
        TabOrder = 0
      end
      object DBEdit8: TDBEdit
        Left = 116
        Top = 20
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'Valor'
        DataSource = DsSaldo
        TabOrder = 1
      end
      object DBEdit9: TDBEdit
        Left = 224
        Top = 20
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'CUSTO'
        DataSource = DsSaldo
        TabOrder = 2
      end
      object EdSaldoFut: TdmkEdit
        Left = 360
        Top = 20
        Width = 100
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object PnVazio: TPanel
      Left = 572
      Top = 272
      Width = 185
      Height = 41
      TabOrder = 4
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 662
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 614
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 566
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 283
        Height = 32
        Caption = 'Ciclo de Balan'#231'o - Item'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 283
        Height = 32
        Caption = 'Ciclo de Balan'#231'o - Item'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 283
        Height = 32
        Caption = 'Ciclo de Balan'#231'o - Item'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 389
    Width = 662
    Height = 41
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 658
      Height = 24
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 430
    Width = 662
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 658
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 514
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrSaldo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO '
      'FROM pqcli'
      'WHERE CI=:P0'
      'AND PQ=:P1')
    Left = 212
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSaldoPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSaldoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSaldoCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
    object QrSaldoCI: TIntegerField
      FieldName = 'CI'
    end
    object QrSaldoPQ: TIntegerField
      FieldName = 'PQ'
    end
  end
  object DsSaldo: TDataSource
    DataSet = QrSaldo
    Left = 212
    Top = 100
  end
  object Qry: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 408
    Top = 52
  end
  object QrExiste: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 404
    Top = 124
    object QrExisteControle: TIntegerField
      FieldName = 'Controle'
    end
  end
end
