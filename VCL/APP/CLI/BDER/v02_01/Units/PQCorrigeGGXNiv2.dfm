object FmPQCorrigeGGXNiv2: TFmPQCorrigeGGXNiv2
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Corrige Grupo em Produtos de Uso e Consumo'
  ClientHeight = 420
  ClientWidth = 691
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 691
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 643
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 595
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 574
        Height = 32
        Caption = 'Corrige Grupo em Produtos de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 574
        Height = 32
        Caption = 'Corrige Grupo em Produtos de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 574
        Height = 32
        Caption = 'Corrige Grupo em Produtos de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 691
    Height = 145
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 691
      Height = 145
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 691
        Height = 145
        Align = alClient
        TabOrder = 0
        object LaAviso: TLabel
          Left = 2
          Top = 15
          Width = 687
          Height = 16
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alTop
          Caption = 
            'Segue a baixo lista de produtos que est'#227'o sem grupo definido e p' +
            'recisam ser regularizados!'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitTop = 7
        end
        object dmkDBGrid1: TdmkDBGrid
          Left = 2
          Top = 77
          Width = 687
          Height = 66
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsPQ
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 31
          Width = 687
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label1: TLabel
            Left = 12
            Top = 4
            Width = 81
            Height = 13
            Caption = 'Grupo de origem:'
          end
          object CBGrupoOrig: TComboBox
            Left = 12
            Top = 20
            Width = 246
            Height = 21
            Style = csDropDownList
            TabOrder = 0
            OnChange = CBGrupoOrigChange
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 306
    Width = 691
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 687
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 440
        Height = 17
        Caption = 
          'Selecione os produtos na grade e em seguida clique no bot'#227'o: A'#231#227 +
          'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 440
        Height = 17
        Caption = 
          'Selecione os produtos na grade e em seguida clique no bot'#227'o: A'#231#227 +
          'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 350
    Width = 691
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 545
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 543
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAcao: TBitBtn
        Tag = 294
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&A'#231#227'o'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAcaoClick
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 193
    Width = 691
    Height = 113
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alBottom
    TabOrder = 4
    object LaGrupo: TLabel
      Left = 12
      Top = 4
      Width = 84
      Height = 13
      Caption = 'Grupo de destino:'
    end
    object CBGrupoDest: TComboBox
      Left = 12
      Top = 20
      Width = 246
      Height = 21
      Style = csDropDownList
      TabOrder = 0
    end
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 12
      Top = 66
      Width = 120
      Height = 40
      Caption = '&Confirma'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 138
      Top = 66
      Width = 120
      Height = 40
      Caption = '&Desiste'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtDesisteClick
    end
    object PB1: TProgressBar
      Left = 12
      Top = 45
      Width = 246
      Height = 17
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      TabOrder = 3
    end
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM pq'
      'WHERE GGXNiv2 = 0')
    Left = 344
    Top = 200
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPQGGXNiv2: TIntegerField
      FieldName = 'GGXNiv2'
    end
    object QrPQAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 372
    Top = 200
  end
  object PMAcao: TPopupMenu
    OnPopup = PMAcaoPopup
    Left = 72
    Top = 456
    object Corrigirgrupo1: TMenuItem
      Caption = '&Corrigir grupo de produtos de uso e consumo selecionados'
      OnClick = Corrigirgrupo1Click
    end
  end
end
